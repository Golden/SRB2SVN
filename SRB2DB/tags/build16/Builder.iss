[_ISTool]
EnableISX=false

[Setup]
OutputDir=E:\Projects\Builder\Package
SourceDir=E:\Projects\Builder\Package\Support
OutputBaseFilename=builder_setup
Compression=zip/9
AppName=Doom Builder
AppVerName=Doom Builder
AppMutex=DoomBuilder
AllowNoIcons=true
DefaultGroupName=CodeImp

AlwaysShowComponentsList=false
AppPublisher=CodeImp
AppPublisherURL=http://www.codeimp.com
AppSupportURL=http://www.doombuilder.com
AppUpdatesURL=http://www.doombuilder.com
EnableDirDoesntExistWarning=false
DirExistsWarning=auto
MinVersion=4.1.1998,5.0.2195
InfoBeforeFile=E:\Projects\Singleline Disclaimer.txt
ChangesAssociations=false
BackColor=clMaroon
WizardImageBackColor=$000080
UninstallDisplayIcon={app}\Builder.exe
DefaultDirName={pf}\Doom Builder\

[Dirs]

[Files]
Source: COMCAT.DLL; DestDir: {sys}; CopyMode: alwaysskipifsameorolder; Flags: regserver restartreplace sharedfile uninsneveruninstall noregerror
Source: ASYCFILT.DLL; DestDir: {sys}; CopyMode: alwaysskipifsameorolder; Flags: regserver restartreplace sharedfile uninsneveruninstall noregerror
Source: Msvbvm60.dll; DestDir: {sys}; CopyMode: alwaysskipifsameorolder; Flags: regserver restartreplace sharedfile uninsneveruninstall noregerror
Source: OLEAUT32.DLL; DestDir: {sys}; CopyMode: alwaysskipifsameorolder; Flags: regserver restartreplace sharedfile uninsneveruninstall
Source: OLEPRO32.DLL; DestDir: {sys}; CopyMode: alwaysskipifsameorolder; Flags: regserver restartreplace sharedfile uninsneveruninstall
Source: scrrun.dll; DestDir: {sys}; CopyMode: alwaysskipifsameorolder; Flags: regserver restartreplace sharedfile uninsneveruninstall
Source: STDOLE2.TLB; DestDir: {sys}; CopyMode: alwaysskipifsameorolder; Flags: restartreplace sharedfile uninsneveruninstall regtypelib noregerror
Source: mscomctl.ocx; DestDir: {sys}; CopyMode: alwaysskipifsameorolder; Flags: regserver restartreplace sharedfile uninsneveruninstall overwritereadonly
Source: dx8vb.dll; DestDir: {sys}; CopyMode: alwaysskipifsameorolder; Flags: restartreplace sharedfile uninsneveruninstall overwritereadonly regserver
Source: Builder.exe; DestDir: {app}; CopyMode: alwaysoverwrite
Source: Boom.cfg; DestDir: {app}; CopyMode: alwaysoverwrite
Source: Builder.cfg; DestDir: {app}; CopyMode: onlyifdoesntexist
Source: Builder.dll; DestDir: {app}; CopyMode: alwaysoverwrite
Source: Crosshair.bmp; DestDir: {app}; CopyMode: alwaysoverwrite
Source: Doom.cfg; DestDir: {app}; CopyMode: alwaysoverwrite
Source: Doom2.cfg; DestDir: {app}; CopyMode: alwaysoverwrite
Source: Font.fnt; DestDir: {app}; CopyMode: alwaysoverwrite
Source: GNU_GPL.txt; DestDir: {app}; CopyMode: alwaysoverwrite
Source: Heretic.cfg; DestDir: {app}; CopyMode: alwaysoverwrite
Source: Shortcuts.cfg; DestDir: {app}; CopyMode: alwaysoverwrite
Source: UltDoom.cfg; DestDir: {app}; CopyMode: alwaysoverwrite
Source: acc.exe; DestDir: {app}; CopyMode: alwaysoverwrite
Source: bsp-w32.exe; DestDir: {app}; CopyMode: alwaysoverwrite
Source: common.acs; DestDir: {app}; CopyMode: alwaysoverwrite
Source: deacc.exe; DestDir: {app}; CopyMode: alwaysoverwrite
Source: defs.acs; DestDir: {app}; CopyMode: alwaysoverwrite
Source: Eternity.cfg; DestDir: {app}; CopyMode: alwaysoverwrite
Source: Hexen.cfg; DestDir: {app}; CopyMode: alwaysoverwrite
Source: Legacy.cfg; DestDir: {app}; CopyMode: alwaysoverwrite
Source: Missing.bmp; DestDir: {app}; CopyMode: alwaysoverwrite
Source: specials.acs; DestDir: {app}; CopyMode: alwaysoverwrite
Source: Unknown.bmp; DestDir: {app}; CopyMode: alwaysoverwrite
Source: wvars.acs; DestDir: {app}; CopyMode: alwaysoverwrite
Source: zcommon.acs; DestDir: {app}; CopyMode: alwaysoverwrite
Source: zdefs.acs; DestDir: {app}; CopyMode: alwaysoverwrite
Source: ZDoom_Doom.cfg; DestDir: {app}; CopyMode: alwaysoverwrite
Source: ZDoom_DoomHexen.cfg; DestDir: {app}; CopyMode: alwaysoverwrite
Source: ZDoom_Hexen.cfg; DestDir: {app}; CopyMode: alwaysoverwrite
Source: zspecial.acs; DestDir: {app}; CopyMode: alwaysoverwrite
Source: zwvars.acs; DestDir: {app}; CopyMode: alwaysoverwrite
Source: fbase6.txt; DestDir: {app}; CopyMode: alwaysoverwrite
Source: fbase6.wad; DestDir: {app}; CopyMode: alwaysoverwrite
Source: jDoom.cfg; DestDir: {app}; CopyMode: alwaysoverwrite
Source: Skulltag_Doom.cfg; DestDir: {app}; CopyMode: alwaysoverwrite
Source: Skulltag_DoomHexen.cfg; DestDir: {app}; CopyMode: alwaysoverwrite
Source: ACS.cfg; DestDir: {app}; CopyMode: alwaysoverwrite
Source: FS.cfg; DestDir: {app}; CopyMode: alwaysoverwrite
Source: zdbsp.exe; DestDir: {app}; CopyMode: alwaysoverwrite
Source: ZenNode.exe; DestDir: {app}; CopyMode: alwaysoverwrite
Source: Parameters.cfg; DestDir: {app}; CopyMode: onlyifdoesntexist
Source: Font.tga; DestDir: {app}; CopyMode: alwaysoverwrite
Source: GDIPlus.dll; DestDir: {app}; CopyMode: alwaysoverwrite
Source: GDIPlus.tlb; DestDir: {sys}; CopyMode: alwaysskipifsameorolder; Flags: restartreplace sharedfile uninsneveruninstall regtypelib noregerror
Source: Strife.cfg; DestDir: {app}; CopyMode: alwaysoverwrite
Source: cmcs21.dll; DestDir: {sys}; CopyMode: alwaysskipifsameorolder; Flags: restartreplace sharedfile uninsneveruninstall overwritereadonly
Source: cmcs21.ocx; DestDir: {sys}; Flags: regserver restartreplace sharedfile uninsneveruninstall overwritereadonly
Source: Risen3D.cfg; DestDir: {app}; CopyMode: alwaysoverwrite
Source: DED.cfg; DestDir: {app}; CopyMode: alwaysoverwrite
Source: Dehacked.cfg; DestDir: {app}; CopyMode: alwaysoverwrite
Source: Thingarrow.tga; DestDir: {app}; CopyMode: alwaysoverwrite
Source: Thingbox.tga; DestDir: {app}; CopyMode: alwaysoverwrite
Source: ZDoom_StrifeHexen.cfg; DestDir: {app}; CopyMode: alwaysoverwrite
Source: ZDoom_HereticHexen.cfg; DestDir: {app}; CopyMode: alwaysoverwrite

[Icons]

Name: {group}\Doom Builder; Filename: {app}\Builder.exe; WorkingDir: {app}; IconFilename: {app}\Builder.exe; Comment: Doom Builder - The cornerstone for every map author; IconIndex: 0; Flags: createonlyiffileexists
Name: {group}\CodeImp website; Filename: http://www.codeimp.com/; IconIndex: 0
Name: {group}\Doom Builder website; Filename: http://www.doombuilder.com/; IconIndex: 0

[UninstallDelete]

[Run]
Filename: {app}\Builder.exe; WorkingDir: {app}; Flags: unchecked skipifdoesntexist postinstall skipifsilent; Description: Launch Doom Builder now
