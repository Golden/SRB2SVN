VERSION 5.00
Begin VB.Form frmMapOptions 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Map Options"
   ClientHeight    =   2925
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   6015
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   HasDC           =   0   'False
   Icon            =   "frmMapOptions.frx":0000
   KeyPreview      =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   195
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   401
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.Frame fraDefaults 
      Caption         =   "Defaults"
      Height          =   2535
      Left            =   120
      TabIndex        =   12
      Top             =   2640
      Visible         =   0   'False
      Width           =   5775
      Begin DoomBuilder.ctlValueBox ctlValCeil 
         Height          =   375
         Left            =   1320
         TabIndex        =   29
         Top             =   1980
         Width           =   735
         _ExtentX        =   1296
         _ExtentY        =   661
         Max             =   9999
      End
      Begin VB.PictureBox picDefFlat 
         BackColor       =   &H8000000C&
         CausesValidation=   0   'False
         ClipControls    =   0   'False
         HasDC           =   0   'False
         Height          =   1020
         Index           =   1
         Left            =   4560
         ScaleHeight     =   64
         ScaleMode       =   3  'Pixel
         ScaleWidth      =   64
         TabIndex        =   22
         TabStop         =   0   'False
         ToolTipText     =   "Floor Texture"
         Top             =   480
         Width           =   1020
         Begin VB.Image imgDefFlat 
            Height          =   960
            Index           =   1
            Left            =   0
            Stretch         =   -1  'True
            ToolTipText     =   "Ceiling Texture"
            Top             =   0
            Width           =   960
         End
      End
      Begin VB.TextBox txtDefFlat 
         Height          =   315
         Index           =   1
         Left            =   4560
         MaxLength       =   8
         TabIndex        =   21
         Text            =   "-"
         Top             =   1560
         Width           =   1020
      End
      Begin VB.PictureBox picDefFlat 
         BackColor       =   &H8000000C&
         CausesValidation=   0   'False
         ClipControls    =   0   'False
         HasDC           =   0   'False
         Height          =   1020
         Index           =   0
         Left            =   3480
         ScaleHeight     =   64
         ScaleMode       =   3  'Pixel
         ScaleWidth      =   64
         TabIndex        =   20
         TabStop         =   0   'False
         ToolTipText     =   "Ceiling Texture"
         Top             =   480
         Width           =   1020
         Begin VB.Image imgDefFlat 
            Height          =   960
            Index           =   0
            Left            =   0
            Stretch         =   -1  'True
            ToolTipText     =   "Ceiling Texture"
            Top             =   0
            Width           =   960
         End
      End
      Begin VB.TextBox txtDefFlat 
         Height          =   315
         Index           =   0
         Left            =   3480
         MaxLength       =   8
         TabIndex        =   19
         Text            =   "-"
         Top             =   1560
         Width           =   1020
      End
      Begin VB.PictureBox picDefTex 
         BackColor       =   &H8000000C&
         CausesValidation=   0   'False
         ClipControls    =   0   'False
         HasDC           =   0   'False
         Height          =   1020
         Index           =   2
         Left            =   2400
         ScaleHeight     =   64
         ScaleMode       =   3  'Pixel
         ScaleWidth      =   64
         TabIndex        =   18
         TabStop         =   0   'False
         ToolTipText     =   "Lower Texture"
         Top             =   480
         Width           =   1020
         Begin VB.Image imgDefTex 
            Height          =   960
            Index           =   2
            Left            =   0
            Stretch         =   -1  'True
            ToolTipText     =   "Ceiling Texture"
            Top             =   0
            Width           =   960
         End
      End
      Begin VB.TextBox txtDefTex 
         Height          =   315
         Index           =   2
         Left            =   2400
         MaxLength       =   8
         TabIndex        =   17
         Text            =   "-"
         Top             =   1560
         Width           =   1020
      End
      Begin VB.PictureBox picDefTex 
         BackColor       =   &H8000000C&
         CausesValidation=   0   'False
         ClipControls    =   0   'False
         HasDC           =   0   'False
         Height          =   1020
         Index           =   1
         Left            =   1320
         ScaleHeight     =   64
         ScaleMode       =   3  'Pixel
         ScaleWidth      =   64
         TabIndex        =   16
         TabStop         =   0   'False
         ToolTipText     =   "Main Texture"
         Top             =   480
         Width           =   1020
         Begin VB.Image imgDefTex 
            Height          =   960
            Index           =   1
            Left            =   0
            Stretch         =   -1  'True
            ToolTipText     =   "Ceiling Texture"
            Top             =   0
            Width           =   960
         End
      End
      Begin VB.TextBox txtDefTex 
         Height          =   315
         Index           =   1
         Left            =   1320
         MaxLength       =   8
         TabIndex        =   15
         Text            =   "-"
         Top             =   1560
         Width           =   1020
      End
      Begin VB.PictureBox picDefTex 
         BackColor       =   &H8000000C&
         CausesValidation=   0   'False
         ClipControls    =   0   'False
         HasDC           =   0   'False
         Height          =   1020
         Index           =   0
         Left            =   240
         ScaleHeight     =   64
         ScaleMode       =   3  'Pixel
         ScaleWidth      =   64
         TabIndex        =   14
         TabStop         =   0   'False
         ToolTipText     =   "Upper Texture"
         Top             =   480
         Width           =   1020
         Begin VB.Image imgDefTex 
            Height          =   960
            Index           =   0
            Left            =   0
            Stretch         =   -1  'True
            ToolTipText     =   "Ceiling Texture"
            Top             =   0
            Width           =   960
         End
      End
      Begin VB.TextBox txtDefTex 
         Height          =   315
         Index           =   0
         Left            =   240
         MaxLength       =   8
         TabIndex        =   13
         Text            =   "-"
         Top             =   1560
         Width           =   1020
      End
      Begin DoomBuilder.ctlValueBox ctlValFlr 
         Height          =   375
         Left            =   3120
         TabIndex        =   31
         Top             =   1980
         Width           =   735
         _ExtentX        =   1296
         _ExtentY        =   661
         Max             =   9999
      End
      Begin DoomBuilder.ctlValueBox ctlValBright 
         Height          =   375
         Left            =   4800
         TabIndex        =   33
         Top             =   1980
         Width           =   735
         _ExtentX        =   1296
         _ExtentY        =   661
         Max             =   9999
      End
      Begin VB.Label LabelDef 
         Caption         =   "Brightness:"
         Height          =   255
         Index           =   7
         Left            =   3960
         TabIndex        =   32
         Top             =   2040
         Width           =   975
      End
      Begin VB.Label LabelDef 
         Caption         =   "Floor height:"
         Height          =   255
         Index           =   6
         Left            =   2160
         TabIndex        =   30
         Top             =   2040
         Width           =   975
      End
      Begin VB.Label LabelDef 
         Caption         =   "Ceiling height:"
         Height          =   255
         Index           =   5
         Left            =   240
         TabIndex        =   28
         Top             =   2040
         Width           =   1215
      End
      Begin VB.Label LabelDef 
         Caption         =   "Floor:"
         Height          =   255
         Index           =   4
         Left            =   4560
         TabIndex        =   27
         Top             =   240
         Width           =   975
      End
      Begin VB.Label LabelDef 
         Caption         =   "Ceiling:"
         Height          =   255
         Index           =   3
         Left            =   3480
         TabIndex        =   26
         Top             =   240
         Width           =   975
      End
      Begin VB.Label LabelDef 
         Caption         =   "Lower:"
         Height          =   255
         Index           =   2
         Left            =   2400
         TabIndex        =   25
         Top             =   240
         Width           =   975
      End
      Begin VB.Label LabelDef 
         Caption         =   "Main:"
         Height          =   255
         Index           =   1
         Left            =   1320
         TabIndex        =   24
         Top             =   240
         Width           =   975
      End
      Begin VB.Label LabelDef 
         Caption         =   "Upper:"
         Height          =   255
         Index           =   0
         Left            =   240
         TabIndex        =   23
         Top             =   240
         Width           =   975
      End
   End
   Begin VB.CommandButton cmdBrowseWAD 
      Caption         =   "Browse..."
      Height          =   345
      Left            =   4800
      TabIndex        =   3
      Top             =   2070
      Width           =   1110
   End
   Begin VB.TextBox txtWAD 
      Height          =   315
      Left            =   240
      TabIndex        =   2
      Top             =   2085
      Width           =   4380
   End
   Begin VB.TextBox txtMapLumpName 
      Height          =   315
      Left            =   1860
      MaxLength       =   8
      TabIndex        =   1
      Top             =   1320
      Width           =   1425
   End
   Begin VB.ComboBox cmbGameConfig 
      Height          =   315
      IntegralHeight  =   0   'False
      ItemData        =   "frmMapOptions.frx":000C
      Left            =   1860
      List            =   "frmMapOptions.frx":000E
      Sorted          =   -1  'True
      Style           =   2  'Dropdown List
      TabIndex        =   0
      Top             =   810
      Width           =   3450
   End
   Begin VB.PictureBox picWarning 
      Appearance      =   0  'Flat
      BackColor       =   &H80000018&
      CausesValidation=   0   'False
      ClipControls    =   0   'False
      ForeColor       =   &H80000008&
      HasDC           =   0   'False
      Height          =   510
      Left            =   60
      ScaleHeight     =   32
      ScaleMode       =   3  'Pixel
      ScaleWidth      =   391
      TabIndex        =   6
      Top             =   60
      Width           =   5895
      Begin VB.Image imgWarning 
         Height          =   240
         Left            =   45
         Picture         =   "frmMapOptions.frx":0010
         Top             =   90
         Width           =   240
      End
      Begin VB.Label lblWarning 
         BackStyle       =   0  'Transparent
         Caption         =   "Warning: These settings control the way your map is saved. Be sure to configure these correctly."
         ForeColor       =   &H80000017&
         Height          =   450
         Left            =   375
         TabIndex        =   7
         Top             =   30
         UseMnemonic     =   0   'False
         Width           =   5430
      End
   End
   Begin VB.CommandButton cmdCancel 
      Cancel          =   -1  'True
      Caption         =   "Cancel"
      Height          =   345
      Left            =   2985
      TabIndex        =   5
      Top             =   5355
      Width           =   1515
   End
   Begin VB.CommandButton cmdOK 
      Caption         =   "OK"
      Default         =   -1  'True
      Enabled         =   0   'False
      Height          =   345
      Left            =   1320
      TabIndex        =   4
      Top             =   5355
      Width           =   1515
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      Caption         =   "(example: MAP01)"
      Height          =   210
      Left            =   3480
      TabIndex        =   11
      Top             =   1365
      UseMnemonic     =   0   'False
      Width           =   1320
   End
   Begin VB.Label lblLumpName 
      Alignment       =   1  'Right Justify
      AutoSize        =   -1  'True
      Caption         =   "Level name:"
      Height          =   210
      Left            =   855
      TabIndex        =   10
      Top             =   1365
      UseMnemonic     =   0   'False
      Width           =   870
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      Caption         =   "Additional Textures and Flats from WAD file:"
      Height          =   210
      Left            =   240
      TabIndex        =   9
      Top             =   1845
      Width           =   3195
   End
   Begin VB.Label lblGameConfig 
      Alignment       =   1  'Right Justify
      AutoSize        =   -1  'True
      Caption         =   "Configuration:"
      Height          =   210
      Left            =   600
      TabIndex        =   8
      Top             =   870
      UseMnemonic     =   0   'False
      Width           =   1125
   End
End
Attribute VB_Name = "frmMapOptions"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'
'    Doom Builder
'    Copyright (c) 2003 Pascal vd Heiden, www.codeimp.com
'    This program is released under GNU General Public License
'
'    This program is distributed in the hope that it will be useful,
'    but WITHOUT ANY WARRANTY; without even the implied warranty of
'    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
'    GNU General Public License for more details.
'


'Do not allow any undeclared variables
Option Explicit

'Case sensitive comparisions
Option Compare Binary


Private NeedGetDefaults As Boolean
Private NewMapMode As Boolean


Public Loading As Boolean

Private Sub cmbGameConfig_Change()
     Dim GameCFGFile As String
     Dim GameCFG As New clsConfiguration
     
     'Set OK button enabled/disabled
     cmdOK.Enabled = (Trim$(cmbGameConfig.Text) <> "") And (Trim$(txtMapLumpName) <> "")
     
     'Check if not loading
     If (Not Loading) And (Val(tag) = 1) Then
          
          'Change the extra wad file to the default for this config
          GameCFGFile = GetGameConfigFile(cmbGameConfig.Text)
          If (Trim$(GameCFGFile) <> "") Then
               
               'Load config
               GameCFG.LoadConfiguration GameCFGFile
               
               'Get the file
               txtWAD.Text = GameCFG.ReadSetting("texturesfile", "")
               
               'Change the default map lump name
               txtMapLumpName.Text = GameCFG.ReadSetting("defaultlumpname", "")
          End If
     End If
End Sub

Private Sub cmbGameConfig_Click()
     cmbGameConfig_Change
End Sub

Private Sub cmbGameConfig_KeyUp(KeyCode As Integer, Shift As Integer)
     cmbGameConfig_Change
End Sub

Private Sub cmdBrowseWAD_Click()
     Dim NewFile As String
     
     'Browse for new file
     NewFile = OpenFile(Me.hWnd, "Select Extra WAD File", "Doom/Heretic/Hexen WAD Files   *.wad|*.wad|All Files|*.*", "", cdlOFNExplorer Or cdlOFNFileMustExist Or cdlOFNHideReadOnly Or cdlOFNLongNames)
     
     'Check if not cancelled
     If (Trim$(NewFile) <> "") Then
          
          'Set the new file in textbox
          txtWAD.Text = NewFile
          txtWAD.SelStart = Len(txtWAD.Text)
          txtWAD.SetFocus
     End If
End Sub

Private Sub cmdCancel_Click()
     tag = 0
     NeedGetDefaults = True
     Hide
End Sub

Private Sub cmdOK_Click()

     Dim DefaultTexture As Dictionary, DefaultSector As Dictionary
     
     'Check if the lump name changed
     If (StrComp(Trim$(maplumpname), Trim$(txtMapLumpName.Text), vbTextCompare) <> 0) Then
          
          'Check if a map is open
          If (mapfile <> "") Then
               
               'Check if the map is in a file
               If mapsaved Then
                    
                    'Check if the given lump name exists in file
                    If (FindLumpIndex(MapWAD, 1, Trim$(txtMapLumpName.Text)) > 0) Then
                         
                         'Lump already exists, ask confirmation
                         If (MsgBox("The map lump name you entered already exists in the current WAD file." & vbLf & "Saving your map will replace that lump or map with the current map." & vbLf & "Do you want to continue?", vbExclamation Or vbYesNo) = vbNo) Then Exit Sub
                    End If
               End If
          End If
     End If
     
     If (Val(Config("storeeditinginfo"))) And Not NewMapMode Then
          
          If Not WadSettings.Exists("defaulttexture") Then WadSettings.Add "defaulttexture", New Dictionary
          If Not WadSettings.Exists("defaultsector") Then WadSettings.Add "defaultsector", New Dictionary

          Set DefaultTexture = WadSettings("defaulttexture")
          Set DefaultSector = WadSettings("defaultsector")
          
          DefaultTexture("upper") = txtDefTex(0).Text
          DefaultTexture("middle") = txtDefTex(1).Text
          DefaultTexture("lower") = txtDefTex(2).Text
          DefaultSector("tceiling") = txtDefFlat(0).Text
          DefaultSector("tfloor") = txtDefFlat(1).Text
          DefaultSector("brightness") = ctlValBright.Value
          DefaultSector("hceiling") = ctlValCeil.Value
          DefaultSector("hfloor") = ctlValFlr.Value
          
     End If
     
     NeedGetDefaults = True
     tag = 1
     Hide
End Sub

Private Sub Form_Activate()

     Dim DefaultTexture As Dictionary, DefaultSector As Dictionary
     Dim i As Long

     cmbGameConfig_Change
     
     If NeedGetDefaults Then
          'Make build defaults from S1 if requested
          If (Val(Config("storeeditinginfo"))) Then
          
               If WadSettings Is Nothing Then
                    height = 3300
                    cmdOK.top = 168
                    cmdCancel.top = 168
                    EnableDefControls False
                    fraDefaults.visible = False
                    NewMapMode = True
               Else
                    height = 6195
                    cmdOK.top = 357
                    cmdCancel.top = 357
                    fraDefaults.visible = True
                    
                    If Not WadSettings.Exists("defaulttexture") Then
                         Set DefaultTexture = Config("defaulttexture")
                    Else
                         Set DefaultTexture = WadSettings("defaulttexture")
                    End If
                    If Not WadSettings.Exists("defaultsector") Then
                         Set DefaultSector = Config("defaultsector")
                    Else
                         Set DefaultSector = WadSettings("defaultsector")
                    End If
                    
                    EnableDefControls True
               
                    txtDefTex(0).Text = DefaultTexture("upper")
                    txtDefTex(1).Text = DefaultTexture("middle")
                    txtDefTex(2).Text = DefaultTexture("lower")
                    txtDefFlat(0).Text = DefaultSector("tceiling")
                    txtDefFlat(1).Text = DefaultSector("tfloor")
                    ctlValBright.Value = DefaultSector("brightness")
                    ctlValCeil.Value = DefaultSector("hceiling")
                    ctlValFlr.Value = DefaultSector("hfloor")
                    
                    NewMapMode = False
                    
               End If
               
          Else
               EnableDefControls False
          End If
          
          
          
          NeedGetDefaults = False
          
     End If

End Sub


Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
     
     'Adjust shift mask
     CurrentShiftMask = Shift
End Sub

Private Sub Form_KeyUp(KeyCode As Integer, Shift As Integer)
     
     'Adjust shift mask
     CurrentShiftMask = Shift
End Sub


Private Sub Form_Load()

     NeedGetDefaults = True

End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
     If (UnloadMode = 0) Then
          Cancel = True
          cmdCancel_Click
     End If
End Sub

Private Sub imgDefFlat_Click(Index As Integer)

     txtDefFlat(Index).Text = SelectFlat(txtDefFlat(Index).Text, Me)

End Sub

Private Sub imgDefTex_Click(Index As Integer)

     txtDefTex(Index).Text = SelectTexture(txtDefTex(Index).Text, Me)

End Sub


Private Sub txtDefFlat_Change(Index As Integer)

     GetScaledFlatPicture txtDefFlat(Index).Text, imgDefFlat(Index)

End Sub

Private Sub txtDefFlat_KeyUp(Index As Integer, KeyCode As Integer, Shift As Integer)

     If Val(Config("autocompletetypetex")) Then CompleteFlatName KeyCode, Shift, txtDefFlat(Index)

End Sub

Private Sub txtDefFlat_Validate(Index As Integer, Cancel As Boolean)

     If Val(Config("autocompletetex")) Then txtDefFlat(Index).Text = GetNearestFlatName(txtDefFlat(Index).Text)

End Sub

Private Sub txtDefTex_Change(Index As Integer)

     GetScaledTexturePicture txtDefTex(Index).Text, imgDefTex(Index)

End Sub

Private Sub txtDefTex_KeyUp(Index As Integer, KeyCode As Integer, Shift As Integer)

     If Val(Config("autocompletetypetex")) Then CompleteTextureName KeyCode, Shift, txtDefTex(Index)

End Sub

Private Sub txtDefTex_Validate(Index As Integer, Cancel As Boolean)

     If Val(Config("autocompletetex")) Then txtDefTex(Index).Text = GetNearestTextureName(txtDefTex(Index).Text)

End Sub

Private Sub txtMapLumpName_Change()
     cmdOK.Enabled = (Trim$(cmbGameConfig.Text) <> "") And (Trim$(txtMapLumpName) <> "")
End Sub

Private Sub txtMapLumpName_GotFocus()
     SelectAllText txtMapLumpName
End Sub


Private Sub txtMapLumpName_KeyPress(KeyAscii As Integer)
     KeyAscii = Asc(UCase$(Chr$(KeyAscii)))
End Sub

Private Sub txtWAD_GotFocus()
     SelectAllText txtWAD
End Sub


Private Sub EnableDefControls(ByVal enable As Boolean)
        
     Dim i As Long

     For i = txtDefFlat.LBound To txtDefFlat.UBound
          txtDefFlat(i).Enabled = enable
          picDefFlat(i).Enabled = enable
     Next i
     
     For i = txtDefTex.LBound To txtDefTex.UBound
          txtDefTex(i).Enabled = enable
          picDefTex(i).Enabled = enable
     Next i
     
     For i = LabelDef.LBound To LabelDef.UBound
          LabelDef(i).Enabled = enable
     Next i
     
     ctlValCeil.Enabled = enable
     ctlValFlr.Enabled = enable
     ctlValBright.Enabled = enable

End Sub
