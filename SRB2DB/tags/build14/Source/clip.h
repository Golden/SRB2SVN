/*
'    Doom Builder
'    Copyright (c) 2003 Pascal vd Heiden, www.codeimp.com
'    This program is released under GNU General Public License
'
'    This program is distributed in the hope that it will be useful,
'    but WITHOUT ANY WARRANTY; without even the implied warranty of
'    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
'    GNU General Public License for more details.
*/


// Function declarations
void __fastcall InitializeClipper();
void __fastcall TerminateClipper();
int __fastcall TestClipRange(float a1, float a2);
void __fastcall WriteClipRange(float a1, float a2);
int __fastcall TestFullClipbuffer();



