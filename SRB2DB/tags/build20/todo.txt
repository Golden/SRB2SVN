Bugs I haven't been able to reproduce yet:

Crash: Creation of zero-height FOF. #71928
Crash: Undo during paste.
Crash: Placing a thing on a FOF in 3D mode.
Apparent multiple selection after creating new sector (dialogue displayed immediately?). #74097
	When drawing one sector around another?
Overflow in StoreMapLumps when nodebuilding.
Ident. sector set fails in certain circumstances: large maps? #77739


Bugs I have reproduced but haven't got round to fixing yet:

Sector description references aren't updated when sector numbers change.
Selection offset creep.
Big, horrible polygons in 3D mode. #nnnnn


Features I might add, if I feel like it:

Instant death for FOFs.
Handle F_SKY1 in 3D mode, if only partially.
Linedef executor toolbox.
MS Paint-style copying. #74070
Choose thing type at random when placing.
Float deaf rings in 3D mode.
Show FOF info for selected sector(s).
Option to use old info bar behaviour.
Convert the *.bmp to resources.
MDI.
Option to disable colouring of zero-height lines as impassable.
Actually make Convert to Stairs useful.
Control sector size customisation.
Make the quick-FOF feature of 3D mode less stupid.
Spruce up the error detection result list.
Restore MAPnnD editing, but without the huge menu.
F(OF)^n editing.
Rename headers when changing map numbers.
Tidy the menus.
'Save Into' feature.
New icon.
Documentation.
Remember toolbar layout.
Move FOF definitions into a config file.
Align textures on FOFs.
Draw complete NiGHTS track.
