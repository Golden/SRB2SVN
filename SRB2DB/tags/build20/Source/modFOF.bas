Attribute VB_Name = "modFOF"
'
'    Doom Builder
'    Copyright (c) 2003 Pascal vd Heiden, www.codeimp.com
'    This program is released under GNU General Public License
'
'    This program is distributed in the hope that it will be useful,
'    but WITHOUT ANY WARRANTY; without even the implied warranty of
'    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
'    GNU General Public License for more details.
'

' This module by Oogaland

' FOF Stuff.


'Do not allow any undeclared variables
Option Explicit

'Case sensitive comparisions
Option Compare Binary


Public Const FF_EXISTS = &H1&                     ' Always set, to check for validity.
Public Const FF_SOLID = &H2&                      ' Clips things.
Public Const FF_RENDERSIDES = &H4&                ' Renders the sides.
Public Const FF_RENDERPLANES = &H8&               ' Renders the floor/ceiling.
Public Const FF_RENDERALL = &HC&                  ' Renders everything.
Public Const FF_SWIMMABLE = &H10&                 ' Is a water block.
Public Const FF_NOSHADE = &H20&                   ' Messes with the lighting?
Public Const FF_CUTSOLIDS = &H40&                 ' Cuts out hidden solid pixels.
Public Const FF_CUTEXTRA = &H80&                  ' Cuts out hidden translucent pixels.
Public Const FF_CUTLEVEL = &HC0&                  ' Cuts out all hidden pixels.
Public Const FF_CUTSPRITES = &H100&               ' Final step in making 3D water.
Public Const FF_BOTHPLANES = &H200&               ' Renders both planes all the time.
Public Const FF_EXTRA = &H400&                    ' Gets cut by ::FF_CUTEXTRA.
Public Const FF_TRANSLUCENT = &H800&              ' See through!
Public Const FF_FOG = &H1000&                     ' Fog "brush."
Public Const FF_INVERTPLANES = &H2000&            ' Reverse the plane visibility rules.
Public Const FF_ALLSIDES = &H4000&                ' Render inside and outside sides.
Public Const FF_INVERTSIDES = &H8000&             ' Only render inside sides.
Public Const FF_DOUBLESHADOW = &H10000            ' Make two lightlist entries to reset light?
Public Const FF_FLOATBOB = &H20000                ' Floats on water and bobs if you step on it.
Public Const FF_NORETURN = &H40000                ' Used with ::FF_CRUMBLE. Will not return to its original position after falling.
Public Const FF_CRUMBLE = &H80000                 ' Falls 2 seconds after being stepped on, and randomly brings all touching crumbling 3dfloors down with it, providing their master sectors share the same tag (allows crumble platforms above or below, to also exist).
Public Const FF_SHATTERBOTTOM = &H100000          ' Used with ::FF_BUSTUP. Like FF_SHATTER, but only breaks from the bottom. Good for springing up through rubble.
Public Const FF_MARIO = &H200000                  ' Acts like a question block when hit from underneath. Goodie spawned at top is determined by master sector.
Public Const FF_BUSTUP = &H400000                 ' You can spin through/punch this block and it will crumble!
Public Const FF_QUICKSAND = &H800000              ' Quicksand!
Public Const FF_PLATFORM = &H1000000              ' You can jump up through this to the top.
Public Const FF_SHATTER = &H2000000               ' Used with ::FF_BUSTUP. Thinks everyone's Knuckles.
Public Const FF_SPINBUST = &H4000000              ' Used with ::FF_BUSTUP. Jump or fall onto it while curled in a ball.
Public Const FF_ONLYKNUX = &H8000000              ' Used with ::FF_BUSTUP. Only Knuckles can break this rock.


Public Const LF_NOCLIMB = &H40&

Public Const LE_COLOURMAP = 16
Public Const LE_CUSTOM = 87

Public Const LE_W_TRANS = 45
Public Const LE_W_OPAQUE = 48
Public Const LE_W_TRANSNOSIDES = 74
Public Const LE_W_OPAQUENOSIDES = 75

Public Const SS_FIREHURT = 7
Public Const SS_NONELEMHURT = 11
Public Const SS_SLIMEHURT = 983

' These must match the combo box list indices.
Public Const FC_NONE = -1
Public Const FC_BLOCK = 0
Public Const FC_WATER = 1
Public Const FC_MAX = 1


Public FOFTypes(40, 1) As Long
Public WaterTypes(3) As Long


' Used when we're not using selection
Public ForceLD As Long
Public ParentSector As Long



Public Sub InitFOFTypes()

     Dim i As Long

     FOFTypes(0, 0) = 25
     FOFTypes(0, 1) = FF_EXISTS Or FF_SOLID Or FF_RENDERALL Or FF_CUTLEVEL
          
     FOFTypes(1, 0) = 33
     FOFTypes(1, 1) = FF_EXISTS Or FF_SOLID Or FF_RENDERALL Or FF_NOSHADE Or FF_CUTLEVEL
     
     FOFTypes(2, 0) = 34
     FOFTypes(2, 1) = FF_EXISTS Or FF_SOLID Or FF_RENDERALL Or FF_CUTLEVEL Or FF_FLOATBOB
          
     FOFTypes(3, 0) = 35
     FOFTypes(3, 1) = FF_EXISTS Or FF_SOLID Or FF_RENDERALL Or FF_CUTLEVEL Or FF_CRUMBLE Or FF_NORETURN
     
     FOFTypes(4, 0) = 36
     FOFTypes(4, 1) = FF_EXISTS Or FF_SOLID Or FF_RENDERALL Or FF_CUTLEVEL Or FF_CRUMBLE
     
     FOFTypes(5, 0) = 37
     FOFTypes(5, 1) = FF_EXISTS Or FF_SOLID Or FF_RENDERALL Or FF_CUTLEVEL Or FF_CRUMBLE Or FF_FLOATBOB
     
     FOFTypes(6, 0) = -1 '39
     FOFTypes(6, 1) = 0 'FF_EXISTS Or FF_SOLID Or FF_RENDERALL Or FF_CUTLEVEL Or FF_FLOATBOB Or FF_AIRBOB Or FF_CRUMBLE
     
     FOFTypes(7, 0) = -1 '40
     FOFTypes(7, 1) = 0 'FF_EXISTS Or FF_SOLID Or FF_RENDERALL Or FF_CUTLEVEL Or FF_AIRBOB Or FF_CRUMBLE
     
     FOFTypes(8, 0) = 41
     FOFTypes(8, 1) = FF_EXISTS Or FF_SOLID Or FF_RENDERALL Or FF_CUTLEVEL Or FF_MARIO
     
     FOFTypes(9, 0) = 42
     FOFTypes(9, 1) = FF_EXISTS Or FF_SOLID Or FF_RENDERALL Or FF_CUTLEVEL Or FF_CRUMBLE Or FF_FLOATBOB Or FF_NORETURN
     
     FOFTypes(10, 0) = 44
     FOFTypes(10, 1) = FF_EXISTS Or FF_SOLID Or FF_RENDERALL Or FF_NOSHADE Or FF_TRANSLUCENT Or FF_EXTRA Or FF_CUTEXTRA
     
     FOFTypes(11, 0) = 51
     FOFTypes(11, 1) = FF_EXISTS Or FF_SOLID Or FF_RENDERPLANES Or FF_CUTLEVEL
     
     FOFTypes(12, 0) = 52
     FOFTypes(12, 1) = FF_EXISTS Or FF_RENDERALL Or FF_TRANSLUCENT Or FF_EXTRA Or FF_CUTEXTRA Or FF_CUTSPRITES
     
     FOFTypes(13, 0) = 55
     FOFTypes(13, 1) = FF_EXISTS Or FF_SOLID Or FF_RENDERALL Or FF_BUSTUP
     
     FOFTypes(14, 0) = -1 '56
     FOFTypes(14, 1) = 0 ' FF_EXISTS Or FF_QUICKSAND Or FF_RENDERALL Or FF_ALLSIDES Or FF_CUTSPRITES
     
     FOFTypes(15, 0) = 57
     FOFTypes(15, 1) = FF_EXISTS Or FF_SOLID Or FF_NOSHADE
     
     FOFTypes(16, 0) = 58
     FOFTypes(16, 1) = FF_EXISTS Or FF_NOSHADE
     
     FOFTypes(17, 0) = 59
     FOFTypes(17, 1) = FF_EXISTS Or FF_SOLID Or FF_RENDERALL Or FF_CUTLEVEL Or FF_PLATFORM Or FF_BOTHPLANES Or FF_ALLSIDES
     
     FOFTypes(18, 0) = 62
     FOFTypes(18, 1) = FF_EXISTS Or FF_RENDERALL Or FF_BOTHPLANES Or FF_ALLSIDES Or FF_CUTEXTRA Or FF_EXTRA Or FF_CUTSPRITES
     
     FOFTypes(19, 0) = 67
     FOFTypes(19, 1) = FF_EXISTS Or FF_RENDERSIDES Or FF_NOSHADE Or FF_ALLSIDES
     
     FOFTypes(20, 0) = 68
     FOFTypes(20, 1) = FF_EXISTS Or FF_SOLID Or FF_RENDERSIDES Or FF_NOSHADE Or FF_CUTLEVEL
     
     FOFTypes(21, 0) = 76
     FOFTypes(21, 1) = FF_EXISTS Or FF_SOLID Or FF_RENDERALL Or FF_BUSTUP Or FF_SHATTER
     
     FOFTypes(22, 0) = 77
     FOFTypes(22, 1) = FF_EXISTS Or FF_SOLID Or FF_CUTLEVEL Or FF_RENDERPLANES Or FF_TRANSLUCENT Or FF_PLATFORM Or FF_BOTHPLANES
     
     FOFTypes(23, 0) = 78
     FOFTypes(23, 1) = FF_EXISTS Or FF_SOLID Or FF_RENDERALL Or FF_BUSTUP Or FF_SPINBUST
     
     FOFTypes(24, 0) = 79
     FOFTypes(24, 1) = FF_EXISTS Or FF_SOLID Or FF_RENDERALL Or FF_CUTLEVEL Or FF_PLATFORM Or FF_CRUMBLE Or FF_BOTHPLANES Or FF_ALLSIDES
     
     FOFTypes(25, 0) = 80
     FOFTypes(25, 1) = FF_EXISTS Or FF_SOLID Or FF_RENDERALL Or FF_CUTLEVEL Or FF_PLATFORM Or FF_CRUMBLE Or FF_NORETURN Or FF_BOTHPLANES Or FF_ALLSIDES
     
     FOFTypes(26, 0) = 81
     FOFTypes(26, 1) = FF_EXISTS Or FF_SOLID Or FF_RENDERALL Or FF_CUTLEVEL Or FF_PLATFORM Or FF_TRANSLUCENT Or FF_BOTHPLANES Or FF_ALLSIDES
     
     FOFTypes(27, 0) = 82
     FOFTypes(27, 1) = FF_EXISTS Or FF_SOLID Or FF_RENDERALL Or FF_CUTLEVEL Or FF_PLATFORM Or FF_CRUMBLE Or FF_TRANSLUCENT Or FF_BOTHPLANES Or FF_ALLSIDES
     
     FOFTypes(28, 0) = 83
     FOFTypes(28, 1) = FF_EXISTS Or FF_SOLID Or FF_RENDERALL Or FF_CUTLEVEL Or FF_PLATFORM Or FF_CRUMBLE Or FF_NORETURN Or FF_TRANSLUCENT Or FF_BOTHPLANES Or FF_ALLSIDES
     
     FOFTypes(29, 0) = 84
     FOFTypes(29, 1) = FF_EXISTS Or FF_SOLID Or FF_RENDERALL Or FF_BUSTUP Or FF_SPINBUST Or FF_TRANSLUCENT
     
     FOFTypes(30, 0) = 86
     FOFTypes(30, 1) = FF_EXISTS Or FF_SOLID Or FF_RENDERALL Or FF_BUSTUP Or FF_SHATTER Or FF_TRANSLUCENT
     
     ' Update this when adding new types.
     For i = 31 To UBound(FOFTypes, 1)
          FOFTypes(i, 0) = -1
     Next i
     
     WaterTypes(0) = LE_W_TRANS
     WaterTypes(1) = LE_W_OPAQUE
     WaterTypes(2) = LE_W_TRANSNOSIDES
     WaterTypes(3) = LE_W_OPAQUENOSIDES
     
     ' Update this when adding new types.
     For i = 4 To UBound(WaterTypes)
          WaterTypes(i) = -1
     Next i

End Sub



Public Function SearchFOFList(ByVal effect As Long) As Long

     Dim i As Long
     
     For i = LBound(FOFTypes, 1) To UBound(FOFTypes, 1)
          
          If effect = FOFTypes(i, 0) Then
               SearchFOFList = i
               Exit Function
          End If
          
     Next i
     
     SearchFOFList = -1
     
End Function

Public Function SearchWaterList(ByVal effect As Long) As Long

     Dim i As Long
     
     For i = LBound(WaterTypes) To UBound(WaterTypes)
          
          If effect = WaterTypes(i) Then
               SearchWaterList = i
               Exit Function
          End If
          
     Next i
     
     SearchWaterList = -1
     
End Function



Public Function FindFOFLinedefs(ByVal ctrlTag As Long, ByRef matches As Long, ByRef Flags As Long, ByRef FOFClass As Long) As Long

     Dim i As Long, j As Long
     
     matches = 0
     FindFOFLinedefs = -1
     FOFClass = FC_NONE
     
     For i = 0 To numlinedefs - 1
     
          If linedefs(i).Tag = ctrlTag And linedefs(i).effect <> 0 Then
          
               j = GetFOFFlagsAndClassByLDIndex(i, Flags, FOFClass)
               
               If j > 0 Then
                    FindFOFLinedefs = i
                    matches = matches + j
               End If
               
          End If
     
     Next i

End Function



Public Function GetFOFFlagsAndClassByLDIndex(ByVal ldindex As Long, ByRef Flags As Long, ByRef FOFClass As Long) As Long

     Dim effect As Long, Index As Long
     
     effect = linedefs(ldindex).effect
     
     GetFOFFlagsAndClassByLDIndex = 0

     If effect = LE_CUSTOM Then
               
          Flags = Val("&H" & sidedefs(linedefs(ldindex).s2).Upper)
          GetFOFFlagsAndClassByLDIndex = GetFOFFlagsAndClassByLDIndex + 1
          FOFClass = FC_BLOCK
          
     End If

     Index = SearchFOFList(effect)
     If Index <> -1 Then
     
          Flags = FOFTypes(Index, 1)
          GetFOFFlagsAndClassByLDIndex = GetFOFFlagsAndClassByLDIndex + 1
          FOFClass = FC_BLOCK
          
     End If
     
     Index = SearchWaterList(effect)
     If Index <> -1 Then
     
          GetFOFFlagsAndClassByLDIndex = GetFOFFlagsAndClassByLDIndex + 1
          FOFClass = FC_WATER
          
     End If

End Function



Public Function IsFOFLinedef(ByVal i As Long) As Boolean
     
     IsFOFLinedef = False
          
     If linedefs(i).effect = LE_CUSTOM _
        Or SearchFOFList(linedefs(i).effect) <> -1 _
        Or SearchWaterList(linedefs(i).effect) <> -1 Then

          IsFOFLinedef = True
          
     End If

End Function


Public Function ShouldDrawFOFSides(fofld As Long) As Boolean

     Dim Index As Long

     If linedefs(fofld).effect = LE_CUSTOM Then
          ShouldDrawFOFSides = Val("&H" & sidedefs(linedefs(fofld).s2).Upper) And FF_RENDERSIDES
     ElseIf linedefs(fofld).effect = LE_W_OPAQUE Or linedefs(fofld).effect = LE_W_TRANS Then
          ShouldDrawFOFSides = True
     Else
          Index = SearchFOFList(linedefs(fofld).effect)
          If Index <> -1 Then
               ShouldDrawFOFSides = FOFTypes(Index, 1) And FF_RENDERSIDES
          Else
               ShouldDrawFOFSides = False
          End If
     End If

End Function



Public Function ShouldDrawFOFPlanes(fofld As Long) As Boolean

     Dim Index As Long

     If linedefs(fofld).effect = LE_CUSTOM Then
          ShouldDrawFOFPlanes = Val("&H" & sidedefs(linedefs(fofld).s2).Upper) And FF_RENDERPLANES
     ElseIf linedefs(fofld).effect = LE_W_OPAQUE Or linedefs(fofld).effect = LE_W_TRANS Or linedefs(fofld).effect = LE_W_OPAQUENOSIDES Or linedefs(fofld).effect = LE_W_TRANSNOSIDES Then
          ShouldDrawFOFPlanes = True
     Else
          Index = SearchFOFList(linedefs(fofld).effect)
          If Index <> -1 Then
               ShouldDrawFOFPlanes = FOFTypes(Index, 1) And FF_RENDERPLANES
          Else
               ShouldDrawFOFPlanes = False
          End If
     End If

End Function



Public Sub FOFDialogueBySelection()

     ForceLD = -1
     ParentSector = -1
     frmFOF.Show 1, frmMain

End Sub

Public Sub FOFDialogueByFOFLDAndSector(ByVal fofld As Long, ByVal sector As Long)

     ForceLD = fofld
     ParentSector = sector
     frmFOF.Show 1, frmMain

End Sub


Public Function CreateControlSector(Optional ByVal radius As Long = 16) As Long

     Static x As Long, y As Long        ' Not reset on loading another map, mind.
     Dim i As Long
     Dim rc As RECT
     Dim DX As Long, dy As Long
     
     ' Assume we move 3 * radius px to the left each time.
     DX = -3 * radius
     dy = 0
     
     ' Search for a position hinter, and use it if we find one.
     For i = LBound(things) To UBound(things)
          If things(i).thing = mapconfig("ctrlsechint") Then
               x = things(i).x
               y = -things(i).y
               
               DX = 3 * radius * Cos(things(i).angle * pi / 180)
               dy = -3 * radius * sIn(things(i).angle * pi / 180)
               Exit For
          End If
     Next i
     
     rc.bottom = -(y - (radius + 1))
     rc.top = -(y + (radius + 1))
     rc.left = x - (radius + 1)
     rc.right = x + (radius + 1)
    
     ' Keep moving left until we're outside a sector.
     ' If this ends up being too slow, try hacking in a short-circuit for the
     ' huge conditional. Or rewrite it in C++, or something.
     Do While CheckCSecSpace(x, y, radius) Or ExistVerticesInRect(rc)
              
          x = x + DX
          y = y + dy
          rc.bottom = -(y - (radius + 1))
          rc.top = -(y + (radius + 1))
          rc.left = x - (radius + 1)
          rc.right = x + (radius + 1)
          
     Loop
    
     ' Make the sector.
     CreateControlSector = CreateSectorHereParams(x, y, 4, radius, False, False)

End Function

Private Function CheckCSecSpace(ByVal x As Long, ByVal y As Long, ByVal radius As Long)

     Dim i As Long
     
     For i = 0 To radius * 2 + 4
     
          If IntersectSector(x - (radius + 2) + i, y - (radius + 2) + i, vertexes(0), linedefs(0), VarPtr(sidedefs(0)), numlinedefs, 0) >= 0 Then
               CheckCSecSpace = True
               Exit Function
          End If
          
          If IntersectSector(x - (radius + 2) + i, y + (radius + 2) - i, vertexes(0), linedefs(0), VarPtr(sidedefs(0)), numlinedefs, 0) >= 0 Then
               CheckCSecSpace = True
               Exit Function
          End If
          
     Next i
     
     CheckCSecSpace = False

End Function
