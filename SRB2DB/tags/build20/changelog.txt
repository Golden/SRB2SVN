Doom Builder - SRB2 Edition
CHANGELOG

Key:
! Bug fixed.
+ Feature added.

17/01/2006

! Crash in texture/flat browser when pressing tab with no image selected.
+ 'Split Linedefs' feature.


18/01/2006

! Textures incorrectly reported as missing in zero-height sectors.


20/01/2006

+ Z Offset feature added; no more messing with flags.
! Crash in browser when no textures/flats currently used in map.
+ 'Cascade tags upwards' option for sectors. Like 'Gradient xxxx', but upper
  bound automatic.
! Fields in new map config dialogue not filled at start for default config.
+ Tag cascading for linedefs.


23/02/2006

! Crash with custom things and Z offsets. Tip of the hat to Ritz.


26/02/2006

! Z offsets were masked to 15 bits instead of 16. (Thanks to ST218)
+ ++ and -- can be used in Z offsets in a similar fashion to sectors.
+ Preliminary handling of Z offsets in 3D mode: things display at the right
  height, but you still have to aim at the ground to select them.


10/03/2006

+ 'Thing paintbrush' mode. Hold shift and right-drag to create lots of things
  at once.


19/03/2006

+ FOF Setup dialogue finished, or at least usable.


20/03/2006

+ Added extra comparative operators (>, < etc.) to the Find/Replace dialogue.
+ Added sector floors and ceilings to Find/Replace.


21/03/2006

! Crash when selecting a previously-selected sector after cancelling the FOF
  setup dialogue.
+ Added shortcut key support for various things.
+ Altered the splash screen.


22/03/06

+ Z offset displays when hovering over things.
+ Improved 3D mode support for things with Z offsets. You can point at them,
  now.


23/03/06

! Nasty linedef hovering crash. Thanks to Sonict for the crash report.
+ Partial support for displaying FOFs in 3D mode.


24/03/06

! Some FOF sidedefs were missing.
! Certain extraneous FOFs were drawn.
! Wrong textures were being used on FOF sidedefs.
+ Added support for all FOFs, not just ld effect 25.
! Some FOF flats were missing.
! FOF dialogue: Linedefs with Normal (0) effect that are tagged to a sector
  were identified incorrectly as FOF-effect linedefs.
+ Don't render sidedefs on FOFs without FF_RENDERSIDES set.


27/03/06

+ Thing height can be altered in 3D mode.
! Removed some (but not all) extraneous sidedefs.
+ Optimised FOF sidedef generation. Why loop through all linedefs when you
  only want the ones for this sector?


28/03/06

! Some FOF sidedefs were drawn multiple times.
+ Autosave feature.
+ Fixed the need to have ML_RESPECTED set on MAPnnD. This breaks compatibility
  with vanilla DB, of course, but that was going to have to happen eventually,
  and you shouldn't be using any other configs with this, anyway. :P
+ Added an option to merge MAPnnD map headers into MAINCFG, mostly in order to
  clean up the Scripts menu.


30/03/06

! FOF dialogue didn't read in control sector brightness.
+ Existing FOFs can be edited in 3D mode.
+ Changed Ctrl+ and Shift+Right-click behaviour for sector and linedef modes:
  this now always inserts a sector, even if something's selected.
! The value boxes on the FOF dialogue behaved incorrectly in some circumstances.
! The FOF dialogue would sometimes create control sectors inside sector 0.
! Status bar failed to update when new sector inserted.


03/04/06

+ Pattern-matching employed on lump names when detecting game config.
! Extended map numbers were handled incorrectly when testing maps.


04/04/06

+ Sector and texture defaults are now applied on a per-wad basis.


05/04/06

+ No-planes flag for FOFs in 3D mode is now accounted for.
+ Changed interface typeface. Not sure whether this is an improvement; feel free
  to let me know what you think.
+ Customisable FOF presets in the FOF editor.
+ Toolbar can be customised, but this isn't saved on exit yet.


06/04/06

+ FOFs can now be pointed at from the side.
! Per-wad defaults weren't initialised for new maps.
+ Sector descriptions: Set in the sector dialogue box, and displayed in a
  tooltip on mouseover.
+ FOF display in 3D mode can now be disabled.
+ Selecting a linedef and then hovering over an adjacent linedef will now show
  the angle they make with one another.


07/04/06

! Autosaving during certain operations caused problems, so we now check the
  mode.
! Crash when finding angles between collinear linedefs.


08/04/06

+ Toolbar buttons for some of the new features.
! Yesterday's angle bugfix wasn't sufficient.
! Dragging sometimes caused crashes. Kludgey fix implemented, but doesn't really
  get to the root of the problem. Yesterday's autosave fix might have done the
  job anyway.
! Multiple MAINCFG lumps were sometimes saved in one wad.
+ MAINCFG reclassified in the config to take advantage of DB's DEHACKED editing
  facilities (PvdH's, not mine!). Renamed Dehacked.cfg to DehackedSRB2.cfg and
  updated it appropriately. It's far from complete, but most of the common stuff
  is in there.
+ FOFs can be created in 3D mode, but the *parent sector* needs to have been
  created in 2D mode, as there's still no way of drawing ordinary sectors in 3D
  mode. To create a FOF, assign a shortcut key to FOF Setup (Tools/
  Configuration/Shortcut Keys), and press it while pointing at the appropriate
  sector in 3D mode.


10/04/06

+ Clicking part of a FOF now brings up the texture/flat browser. Press the FOF
  setup key for the old behaviour.


11/04/06

+ Added *.srb to the IWAD browse dialogue. Also added option for *.*.
+ Added slime and lava to the FOF presets.
! FOF sidedefs were drawn backwards when parent sector's linedef faced inwards.
+ Rotation of things using the mouse.


12/04/06

+ In 3D mode, FOF ceiling and floor heights can be altered using the raise/lower
  keys (mouse wheel by default).


13/04/06

+ Support for multiple editing colour schemes.


14/04/06

+ Finished DehackedSRB2.cfg.
! FOF setup undo and cancelling had several problems, particularly in 3D mode.


15/04/06

+ Added option to highlight the linedefs of a sector when pointed at in 3D mode.


16/04/06

+ Draw sectors in 3D mode. Set a key, point at a sector, then press it to begin.
  Click to place vertices. Close the polygon or press the key again to complete.


17/04/06

! Video mode not restored on 3D mode exit.
! Beeping when keys pressed in full-screen 3D mode after drawing a sector.
+ Config file updated for 1.09.3.4; thanks to Foxboy for this.
+ FOF textures remembered per-class for each session in the FOF dialogue.
+ Support for FF_SHATTERBELOW.
! Last week's FOF sd texture fix actually caused *all* sidedefs to be drawn
  backwards... Oops.
! Alt+tab messed up the mouse if used while in a dialogue in 3D mode.
+ FOF collision-detection.


18/04/06

! All linedefs were identified as FOF linedefs.
! Disabling FOFs disabled ceilings.


19/04/06

+ Facility to find sets of identical sectors.
! Control sector search could crash if control sector not last numbered.
! Water blocks whose sides shouldn't be rendered weren't rendered at all.


20/04/06

! Further control sector crash fix.


24/04/06

! New global lumps weren't found by the script editor if the map hadn't been
  saved.
! Map header deletion code could cause other lumps to be deleted.
! Textures weren't set correctly when choosing a FOF preset of a different
  class to that currently being edited.


25/04/06

+ Different colour for axes.
! Autosave routine ran even if map not changed.
+ Copy/paste textures and flats on FOFs in 3D mode.
! Selections behaved incorrectly after splitting linedefs.


29/04/06

+ WA-esque 'convert sector to stairs' option, with support for incrementing
  heights elliptically.


07/05/06

+ Defaults can be changed from the Map Options dialogue.
+ Facility to insert FOF in 3D mode without invoking the dialogue.
+ Shortcut keys for FOF quick-insert and stair conversion.
+ Made identical-sector-finding a little more useful: repeating (eventually)
  finds a different set.


09/05/06

+ Switch Map option.
+ Circles are drawn around NiGHTS axes. Turn on 'Outline all Things in Things
  Mode' to see them. It makes things look a little messy, though.
+ A little more work on Convert to Stairs.


12/05/06

+ Multi-key search in the texture browser.


13/05/06

! Fixed some Convert to Stairs weirdness.


16/05/06

! File menu behaviour was messed up.
! Flipping was broken following a rotation while pasting.
! Linedef merging was broken following a rotation while pasting.
+ The error-checker now picks up crossed linedefs.


18/05/06

! Splitting very small sectors resulted in incorrect sector references. This
  also affected stair conversion.
+ Shortcut key for identical sector search.


19/05/06

! Rounding error in crossed linedef detection.
! Crash in 3D browser when no textures/flats currently used in map (cf. 20/01).
+ If no tex/flats used when texture browser opened, all are displayed.
! Floating flag was ignored for FOFs that didn't crumble.


25/05/06

! Colourmap colour chooser always started with black.


26/05/06

+ Selected linedef info shown in main window (not just on hover).


27/05/06

! Changing flats using the FOF dialogue in 3D mode didn't update the display
  correctly.
! Cancelling the FOF dialogue in 3D mode crashed.
+ Sidedef textures can be changed from the main window.


28/05/06

! Renderer no longer crashes in Wine. Still can't load wads, though.
+ Selected sector info shown in main window (not just on hover).
+ Sector flats can be changed from the main window.


31/05/06

+ Deaf/multi text can now be customised. Updated game config accordingly.
! Texture selecter not invoked when preview in main window clicked in frame but
  outside image.
+ Numerous very minor interface adjustments.


03/06/06

+ Scroll key. While held, mousing to the edge of the map causes it to scroll.
! Selected item info was wrong if the mouse was moved out of the map directly
  from a highlighted item.


04/06/06

+ Linedefs whose floor heights are the same on both sides are drawn in a
  different colour.
+ Linedefs which are impassable due to delimiting a sector with equal floor
  and ceiling heights are drawn in the impassable colour.


06/06/06

! Rewrote the control sector positioning algorithm in an attempt to quell its
  bugginess.
+ Control sector positioning hint object.
! Fixed a problem with custom deaf/multi text.
! Selected item info not shown immediately after mode change.


07/06/06

! Selections could be messed up internally (leading to crashes) when linedefs
  merged while in sector mode.
+ Grid-snapping can be toggled in the time-honoured fashion while drawing lines.
+ Holding Ctrl while drawing a line constrains it to a multiple of 45-degrees
  from the source vertex.
! Changing ceiling flats from the main window actually changed the floor flat...


10/06/06

+ Merged in Doom Builder 1.68 (except for the flat/texture mixing changes, which
  don't apply to SRB2 anyway).
+ Cleaned up the linedef/sector specials display a little.
! Concurrent linedefs weren't merged when pasting following a flip.
+ Added some defaults for config options and keyboard shortcuts that had been
  added in the past.


14/06/06

! Texture previews weren't updated correctly when the info bar was vertical.
! CS space-finding algorithm was STILL buggy.
! CS hint thing being outside a sector was flagged as an error.
! Silly bug that would prevent DB from loading if show3dsechighlight wasn't set.
+ Sectors containing FOFs are drawn in a different colour.


18/06/06

! When lassoing linedefs, non-lassoed linedefs whose vertices were previously
  selected would always become selected themselves.
+ Adjacent sector info shown in sector dialogue.
! Additive selection couldn't be forced when it was off by default.


19/06/06

! Drawing linedefs on top of existing ones was broken.
! Texture requirement identification wasn't working correctly with zero-height
  sky-containing sectors.
+ Revamped the testing options.


20/06/06

+ Axis-drawing and FOF-colouring can now be disabled.
+ Light levels can now be searched for.
! Find-dialogue height behaved a little strangely.
+ Added Select All, Select None and Invert Selection options to the Edit menu.
+ Added an 'allowed to be missing after nodebuild' flag for map lumps. Reflagged
  BLOCKMAP accordingly.


21/06/06

! Default map gametype wasn't remembered.
! Specifying additional parameters when testing would mess up the command line.
! Using the desktop resolution when testing was broken.
+ Added difficulty selection to testing.
+ Adding of the resources WAD can now be suppressed when testing.


22/06/06

! Selection count wasn't updated correctly when lassoing or inverting a
  selection.


24/06/06

! Partial regression of 30/03's mouse behaviour change.


28/06/06

! No default skin was selected.


29/06/06

! Drawing sectors in sectors mode could upset sector references.


01/07/06

! Default gametype wasn't remembered if specified when creating a map.
! Lassoing a selection didn't update the info bar.


03/07/06

! Lassoing linedefs still didn't update the info bar.
! Crash in Select Map dialogue.


12/07/06

! Selecting 320x200 caused the game to run at 320x240.
+ Prettied-up the error-check dialogue a little.
! A couple of crashes when default gametype not set, which happened when the
  map was created in another editor.


13/07/06

! Selection was cleared when finding identical sectors.


25/07/06

+ Updated the config for the new NiGHTS engine.
+ Path-drawing updated for the new NiGHTS engine.
+ Zoom Tube Wizard.
