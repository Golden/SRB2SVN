VERSION 5.00
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Begin VB.Form frmFOF 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Setup FOF"
   ClientHeight    =   6900
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   6435
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmFOF.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   NegotiateMenus  =   0   'False
   ScaleHeight     =   6900
   ScaleWidth      =   6435
   StartUpPosition =   3  'Windows Default
   Begin VB.Frame Frame1 
      Caption         =   "Presets"
      Height          =   735
      Left            =   120
      TabIndex        =   75
      Top             =   5640
      Width           =   6195
      Begin VB.CommandButton cmdDelPreset 
         Caption         =   "Delete"
         Height          =   315
         Left            =   5400
         TabIndex        =   78
         Top             =   240
         Width           =   615
      End
      Begin VB.CommandButton cmdSavePreset 
         Caption         =   "Save"
         Height          =   315
         Left            =   4680
         TabIndex        =   77
         Top             =   240
         Width           =   615
      End
      Begin VB.ComboBox cmbPresets 
         Height          =   315
         Left            =   120
         Style           =   2  'Dropdown List
         TabIndex        =   76
         Top             =   240
         Width           =   4455
      End
   End
   Begin MSComDlg.CommonDialog CommonDialog1 
      Left            =   5880
      Top             =   6420
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin VB.CommandButton cmdCancel 
      Cancel          =   -1  'True
      Caption         =   "Cancel"
      Height          =   315
      Left            =   3450
      TabIndex        =   55
      Top             =   6480
      Width           =   1815
   End
   Begin VB.CommandButton cmdOK 
      Caption         =   "OK"
      Default         =   -1  'True
      Height          =   315
      Left            =   1170
      TabIndex        =   54
      Top             =   6480
      Width           =   1815
   End
   Begin VB.Frame fraAdjacent 
      Caption         =   "Adjacent Sector Information"
      Height          =   795
      Left            =   3900
      TabIndex        =   51
      Top             =   4740
      Width           =   2415
      Begin VB.Label lblCeilRange 
         AutoSize        =   -1  'True
         Caption         =   "Ceiling range:"
         Height          =   225
         Left            =   120
         TabIndex        =   53
         Top             =   480
         Width           =   975
      End
      Begin VB.Label lblFloorRange 
         AutoSize        =   -1  'True
         Caption         =   "Floor range:"
         Height          =   210
         Left            =   120
         TabIndex        =   52
         Top             =   240
         Width           =   870
      End
   End
   Begin VB.Frame fraBotHeights 
      Caption         =   "Bottom Heights"
      Height          =   2055
      Left            =   3900
      TabIndex        =   21
      Top             =   2640
      Width           =   2415
      Begin DoomBuilder.ctlValueBox ctlValueBoxBotBelowTop 
         Height          =   375
         Left            =   1440
         TabIndex        =   22
         Top             =   1560
         Width           =   855
         _ExtentX        =   1508
         _ExtentY        =   661
         Max             =   9999
         SmallChange     =   8
      End
      Begin DoomBuilder.ctlValueBox ctlValueBoxBotBelowCeil 
         Height          =   375
         Left            =   1440
         TabIndex        =   23
         Top             =   1140
         Width           =   855
         _ExtentX        =   1508
         _ExtentY        =   661
         Max             =   9999
         Min             =   -9999
         SmallChange     =   8
      End
      Begin DoomBuilder.ctlValueBox ctlValueBoxBotAboveFlr 
         Height          =   375
         Left            =   1440
         TabIndex        =   24
         Top             =   720
         Width           =   855
         _ExtentX        =   1508
         _ExtentY        =   661
         Max             =   9999
         Min             =   -9999
         SmallChange     =   8
      End
      Begin DoomBuilder.ctlValueBox ctlValueBoxBotAbs 
         Height          =   375
         Left            =   1440
         TabIndex        =   25
         Top             =   300
         Width           =   855
         _ExtentX        =   1508
         _ExtentY        =   661
         Max             =   9999
         SmallChange     =   8
      End
      Begin VB.Label Label12 
         Caption         =   "Absolute:"
         Height          =   195
         Left            =   120
         TabIndex        =   29
         Top             =   360
         Width           =   735
      End
      Begin VB.Label Label11 
         Caption         =   "Above floor:"
         Height          =   195
         Left            =   120
         TabIndex        =   28
         Top             =   780
         Width           =   975
      End
      Begin VB.Label Label10 
         Caption         =   "Below ceiling:"
         Height          =   255
         Left            =   120
         TabIndex        =   27
         Top             =   1200
         Width           =   1035
      End
      Begin VB.Label Label9 
         Caption         =   "Below top:"
         Height          =   255
         Left            =   120
         TabIndex        =   26
         Top             =   1620
         Width           =   1155
      End
   End
   Begin VB.Frame fraTopHeights 
      Caption         =   "Top Heights"
      Height          =   2055
      Left            =   3900
      TabIndex        =   12
      Top             =   480
      Width           =   2415
      Begin DoomBuilder.ctlValueBox ctlValueBoxTopAboveBot 
         Height          =   375
         Left            =   1440
         TabIndex        =   20
         Top             =   1560
         Width           =   855
         _ExtentX        =   1508
         _ExtentY        =   661
         Max             =   9999
         SmallChange     =   8
      End
      Begin DoomBuilder.ctlValueBox ctlValueBoxTopBelowCeil 
         Height          =   375
         Left            =   1440
         TabIndex        =   18
         Top             =   1140
         Width           =   855
         _ExtentX        =   1508
         _ExtentY        =   661
         Max             =   9999
         Min             =   -9999
         SmallChange     =   8
      End
      Begin DoomBuilder.ctlValueBox ctlValueBoxTopAboveFlr 
         Height          =   375
         Left            =   1440
         TabIndex        =   16
         Top             =   720
         Width           =   855
         _ExtentX        =   1508
         _ExtentY        =   661
         Max             =   9999
         Min             =   -9999
         SmallChange     =   8
      End
      Begin DoomBuilder.ctlValueBox ctlValueBoxTopAbs 
         Height          =   375
         Left            =   1440
         TabIndex        =   14
         Top             =   300
         Width           =   855
         _ExtentX        =   1508
         _ExtentY        =   661
         Max             =   9999
         SmallChange     =   8
      End
      Begin VB.Label Label8 
         Caption         =   "Above bottom:"
         Height          =   255
         Left            =   120
         TabIndex        =   19
         Top             =   1620
         Width           =   1155
      End
      Begin VB.Label Label7 
         Caption         =   "Below ceiling:"
         Height          =   255
         Left            =   120
         TabIndex        =   17
         Top             =   1200
         Width           =   1035
      End
      Begin VB.Label Label6 
         Caption         =   "Above floor:"
         Height          =   195
         Left            =   120
         TabIndex        =   15
         Top             =   780
         Width           =   975
      End
      Begin VB.Label Label5 
         Caption         =   "Absolute:"
         Height          =   195
         Left            =   120
         TabIndex        =   13
         Top             =   360
         Width           =   735
      End
   End
   Begin VB.Frame fraFlatText 
      Caption         =   "Flats and Textures"
      Height          =   2055
      Left            =   120
      TabIndex        =   2
      Top             =   480
      Width           =   3675
      Begin VB.TextBox txtTSides 
         Height          =   315
         Left            =   2520
         MaxLength       =   8
         TabIndex        =   8
         Text            =   "-"
         Top             =   1620
         Width           =   1020
      End
      Begin VB.TextBox txtTFloor 
         Height          =   315
         Left            =   1320
         MaxLength       =   8
         TabIndex        =   7
         Text            =   "-"
         Top             =   1620
         Width           =   1020
      End
      Begin VB.TextBox txtTCeiling 
         Height          =   315
         Left            =   120
         MaxLength       =   8
         TabIndex        =   6
         Text            =   "-"
         Top             =   1620
         Width           =   1020
      End
      Begin VB.PictureBox picTSides 
         BackColor       =   &H8000000C&
         CausesValidation=   0   'False
         ClipControls    =   0   'False
         HasDC           =   0   'False
         Height          =   1020
         Left            =   2520
         ScaleHeight     =   64
         ScaleMode       =   3  'Pixel
         ScaleWidth      =   64
         TabIndex        =   5
         TabStop         =   0   'False
         ToolTipText     =   "Side Texture"
         Top             =   540
         Width           =   1020
         Begin VB.Image imgTSides 
            Height          =   960
            Left            =   0
            Stretch         =   -1  'True
            ToolTipText     =   "Ceiling Texture"
            Top             =   0
            Width           =   960
         End
      End
      Begin VB.PictureBox picTFloor 
         BackColor       =   &H8000000C&
         CausesValidation=   0   'False
         ClipControls    =   0   'False
         HasDC           =   0   'False
         Height          =   1020
         Left            =   1320
         ScaleHeight     =   64
         ScaleMode       =   3  'Pixel
         ScaleWidth      =   64
         TabIndex        =   4
         TabStop         =   0   'False
         ToolTipText     =   "Bottom Texture"
         Top             =   540
         Width           =   1020
         Begin VB.Image imgTFloor 
            Height          =   960
            Left            =   0
            Stretch         =   -1  'True
            ToolTipText     =   "Ceiling Texture"
            Top             =   0
            Width           =   960
         End
      End
      Begin VB.PictureBox picTCeiling 
         BackColor       =   &H8000000C&
         CausesValidation=   0   'False
         ClipControls    =   0   'False
         HasDC           =   0   'False
         Height          =   1020
         Left            =   120
         ScaleHeight     =   64
         ScaleMode       =   3  'Pixel
         ScaleWidth      =   64
         TabIndex        =   3
         TabStop         =   0   'False
         ToolTipText     =   "Top Texture"
         Top             =   540
         Width           =   1020
         Begin VB.Image imgTCeiling 
            Height          =   960
            Left            =   0
            Stretch         =   -1  'True
            ToolTipText     =   "Ceiling Texture"
            Top             =   0
            Width           =   960
         End
      End
      Begin VB.Label Label4 
         Caption         =   "Sides:"
         Height          =   195
         Left            =   2520
         TabIndex        =   11
         Top             =   300
         Width           =   975
      End
      Begin VB.Label Label3 
         Caption         =   "Bottom:"
         Height          =   255
         Left            =   1320
         TabIndex        =   10
         Top             =   300
         Width           =   975
      End
      Begin VB.Label Label2 
         Caption         =   "Top:"
         Height          =   195
         Left            =   120
         TabIndex        =   9
         Top             =   300
         Width           =   975
      End
   End
   Begin VB.ComboBox cmbFOFType 
      Height          =   330
      ItemData        =   "frmFOF.frx":000C
      Left            =   1560
      List            =   "frmFOF.frx":0016
      Style           =   2  'Dropdown List
      TabIndex        =   1
      Top             =   60
      Width           =   4755
   End
   Begin VB.Frame fraWater 
      Caption         =   "Water Options"
      Height          =   2895
      Left            =   120
      TabIndex        =   56
      Top             =   2640
      Width           =   3675
      Begin DoomBuilder.ctlValueBox ctlValueBoxWaterLight 
         Height          =   375
         Left            =   2760
         TabIndex        =   74
         Top             =   720
         Width           =   675
         _ExtentX        =   1191
         _ExtentY        =   661
         Max             =   255
      End
      Begin VB.PictureBox picColourmap 
         Height          =   375
         Left            =   2340
         ScaleHeight     =   315
         ScaleWidth      =   735
         TabIndex        =   71
         Top             =   1560
         Width           =   795
         Begin VB.Label lblColour 
            Alignment       =   2  'Center
            BackStyle       =   0  'Transparent
            Caption         =   "#0000000"
            BeginProperty Font 
               Name            =   "Small Fonts"
               Size            =   6.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Left            =   0
            TabIndex        =   72
            Top             =   60
            Width           =   735
         End
      End
      Begin DoomBuilder.ctlValueBox ctlValueBoxWaterAlpha 
         Height          =   375
         Left            =   2760
         TabIndex        =   70
         Top             =   2040
         Width           =   675
         _ExtentX        =   1191
         _ExtentY        =   661
         Max             =   35
      End
      Begin VB.CheckBox chkClrmap 
         Caption         =   "Colourmap"
         Height          =   210
         Left            =   1740
         TabIndex        =   68
         Top             =   1200
         Width           =   1455
      End
      Begin VB.Frame Frame2 
         BorderStyle     =   0  'None
         Caption         =   "Frame2"
         Height          =   795
         Left            =   180
         TabIndex        =   64
         Top             =   1560
         Width           =   1575
         Begin VB.OptionButton optNonElem 
            Caption         =   "Non-Elemental"
            Height          =   210
            Left            =   120
            TabIndex        =   67
            Top             =   480
            Width           =   1335
         End
         Begin VB.OptionButton optFire 
            Caption         =   "Fire"
            Height          =   210
            Left            =   120
            TabIndex        =   66
            Top             =   240
            Width           =   915
         End
         Begin VB.OptionButton optSlime 
            Caption         =   "Slime"
            Height          =   210
            Left            =   120
            TabIndex        =   65
            Top             =   0
            Width           =   975
         End
      End
      Begin VB.CheckBox chkDamage 
         Caption         =   "Damage"
         Height          =   210
         Left            =   120
         TabIndex        =   63
         Top             =   1200
         Width           =   1095
      End
      Begin VB.CommandButton cmdPresetLava 
         Caption         =   "L"
         Height          =   315
         Left            =   3360
         TabIndex        =   62
         TabStop         =   0   'False
         Top             =   2520
         Visible         =   0   'False
         Width           =   255
      End
      Begin VB.CommandButton cmdPresetSlime 
         Caption         =   "S"
         Height          =   315
         Left            =   3000
         TabIndex        =   61
         TabStop         =   0   'False
         Top             =   2520
         Visible         =   0   'False
         Width           =   255
      End
      Begin VB.CommandButton cmdPresetWater 
         Caption         =   "W"
         Height          =   315
         Left            =   2640
         TabIndex        =   60
         TabStop         =   0   'False
         Top             =   2520
         Visible         =   0   'False
         Width           =   255
      End
      Begin VB.CheckBox chkWaterSides 
         Caption         =   "Render sides"
         Height          =   255
         Left            =   1740
         TabIndex        =   59
         Top             =   420
         Width           =   1275
      End
      Begin VB.OptionButton optTranslucent 
         Caption         =   "Translucent"
         Height          =   255
         Left            =   120
         TabIndex        =   58
         Top             =   720
         Width           =   1215
      End
      Begin VB.OptionButton optOpaque 
         Caption         =   "Opaque"
         Height          =   255
         Left            =   120
         TabIndex        =   57
         Top             =   420
         Width           =   915
      End
      Begin VB.Label Label16 
         Caption         =   "Brightness:"
         Height          =   195
         Left            =   1740
         TabIndex        =   73
         Top             =   780
         Width           =   855
      End
      Begin VB.Label Label17 
         Caption         =   "Alpha:"
         Height          =   195
         Left            =   1980
         TabIndex        =   69
         Top             =   2100
         Width           =   495
      End
   End
   Begin VB.Frame fraBlock 
      Caption         =   "Block Options"
      Height          =   2895
      Left            =   120
      TabIndex        =   30
      Top             =   2640
      Width           =   3675
      Begin VB.CheckBox chkShatterBottom 
         Caption         =   "Below only"
         Height          =   210
         Left            =   420
         TabIndex        =   34
         Top             =   2280
         Width           =   1515
      End
      Begin DoomBuilder.ctlValueBox ctlValueBoxLight 
         Height          =   375
         Left            =   2880
         TabIndex        =   49
         Top             =   2040
         Width           =   675
         _ExtentX        =   1191
         _ExtentY        =   661
         Max             =   255
         SmallChange     =   16
      End
      Begin DoomBuilder.ctlValueBox ctlValueBoxAlpha 
         Height          =   375
         Left            =   2880
         TabIndex        =   47
         Top             =   1320
         Width           =   675
         _ExtentX        =   1191
         _ExtentY        =   661
         Max             =   255
         SmallChange     =   16
      End
      Begin VB.CheckBox chkTrans 
         Caption         =   "Translucent:"
         Height          =   210
         Left            =   1860
         TabIndex        =   46
         Top             =   1080
         Width           =   1695
      End
      Begin VB.CheckBox chkQuicksand 
         Caption         =   "Quicksand"
         Height          =   210
         Left            =   2400
         TabIndex        =   45
         Top             =   2520
         Visible         =   0   'False
         Width           =   1095
      End
      Begin VB.CheckBox chkMario 
         Caption         =   "Mario"
         Height          =   210
         Left            =   1860
         TabIndex        =   44
         Top             =   360
         Width           =   1635
      End
      Begin VB.CheckBox chkPlanes 
         Caption         =   "Render top/bottom"
         Height          =   210
         Left            =   1860
         TabIndex        =   43
         Top             =   840
         Width           =   1755
      End
      Begin VB.CheckBox chkSides 
         Caption         =   "Render sides"
         Height          =   210
         Left            =   1860
         TabIndex        =   42
         Top             =   600
         Width           =   1695
      End
      Begin VB.CheckBox chkSolid 
         Caption         =   "Solid"
         Height          =   210
         Left            =   120
         TabIndex        =   41
         Top             =   360
         Width           =   1695
      End
      Begin VB.CheckBox chkIntBelow 
         Caption         =   "Intangible from below"
         Height          =   210
         Left            =   120
         TabIndex        =   40
         Top             =   2520
         Width           =   1875
      End
      Begin VB.CheckBox chkShatter 
         Caption         =   "Shatter"
         Height          =   210
         Left            =   420
         TabIndex        =   39
         Top             =   2040
         Width           =   1455
      End
      Begin VB.CheckBox chkSpinBust 
         Caption         =   "Spin Bust"
         Height          =   210
         Left            =   420
         TabIndex        =   38
         Top             =   1800
         Width           =   1335
      End
      Begin VB.CheckBox chkBreak 
         Caption         =   "Breakable"
         Height          =   210
         Left            =   120
         TabIndex        =   37
         Top             =   1320
         Width           =   1395
      End
      Begin VB.CheckBox chkBreakKnux 
         Caption         =   "By Knux only"
         Height          =   210
         Left            =   420
         TabIndex        =   36
         Top             =   1560
         Width           =   1275
      End
      Begin VB.CheckBox chkFloat 
         Caption         =   "Floats in water"
         Height          =   210
         Left            =   120
         TabIndex        =   35
         Top             =   1080
         Width           =   1695
      End
      Begin VB.CheckBox chkRespawn 
         Caption         =   "Respawn"
         Height          =   195
         Left            =   120
         TabIndex        =   33
         Top             =   840
         Width           =   1515
      End
      Begin VB.CheckBox chkCrumble 
         Caption         =   "Crumbling"
         Height          =   210
         Left            =   120
         TabIndex        =   32
         Top             =   600
         Width           =   1275
      End
      Begin VB.CheckBox chkShadow 
         Caption         =   "Shadowcasting"
         Height          =   195
         Left            =   1860
         TabIndex        =   31
         Top             =   1800
         Width           =   1575
      End
      Begin VB.Label Label14 
         Caption         =   "Light:"
         Height          =   195
         Left            =   2220
         TabIndex        =   50
         Top             =   2100
         Width           =   555
      End
      Begin VB.Label Label13 
         Caption         =   "Alpha:"
         Height          =   195
         Left            =   2220
         TabIndex        =   48
         Top             =   1380
         Width           =   495
      End
   End
   Begin VB.Label Label1 
      Caption         =   "FOF Type:"
      Height          =   195
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   1335
   End
End
Attribute VB_Name = "frmFOF"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit



Private Enum DAMAGE
     DAMAGE_NONE = 0
     DAMAGE_NONELEM = 1
     DAMAGE_SLIME = 2
     DAMAGE_FIRE = 3
End Enum

Private Type FOFTEXTURESET
     Floor As String
     Ceil As String
     Sides As String
End Type


Private Floor, Ceil, ctrlTag As Long
Private needNewCtrlSector As Boolean


Private FCPreviouslySelected(FC_MAX) As Boolean
Private FCTextures(FC_MAX) As FOFTEXTURESET
Private FCPrev As Long

Private bomb As Boolean
Private maxflr As Long, minceil As Long
Private noAutoCalc As Boolean
Private ldindex As Long
Private ctrlSectorIndex As Long

Private PresetsFile As clsConfiguration, Presets As Dictionary

Public Cancelled As Boolean


Private Sub chkBreak_Click()

     If chkBreak.Value = vbChecked Then
          chkBreakKnux.Enabled = True
          chkShatter.Enabled = True
          chkSpinBust.Enabled = True
          chkShatterBottom.Enabled = True
     Else
          chkBreakKnux.Enabled = False
          chkShatter.Enabled = False
          chkSpinBust.Enabled = False
          chkShatterBottom.Enabled = False
     End If

End Sub

Private Sub chkClrmap_Click()

     If chkClrmap.Value <> vbChecked Then
     
          picColourmap.Enabled = False
          lblColour.Enabled = False
          picColourmap.BackColor = SystemColorConstants.vbButtonFace
          lblColour.ForeColor = SystemColorConstants.vbGrayText
          ctlValueBoxWaterAlpha.Enabled = False
          
     Else
     
          picColourmap.Enabled = True
          lblColour.Enabled = True
          ctlValueBoxWaterAlpha.Enabled = True
          UpdateColourmapControls
          
     End If

End Sub

Private Sub chkDamage_Click()

     If chkDamage.Value = vbChecked Then
     
          optSlime.Enabled = True
          optFire.Enabled = True
          optNonElem.Enabled = True
          
     Else
     
          optSlime.Enabled = False
          optFire.Enabled = False
          optNonElem.Enabled = False
          
     End If

End Sub

Private Sub chkTrans_Click()

     If chkTrans.Value <> vbChecked Then
          ctlValueBoxAlpha.Enabled = False
     Else
          ctlValueBoxAlpha.Enabled = True
     End If

End Sub



Private Sub cmbFOFType_Click()
     
     ' Remember texs by class.
     If FCPrev <> -1 Then
          FCTextures(FCPrev).Ceil = txtTCeiling.Text
          FCTextures(FCPrev).Floor = txtTFloor.Text
          FCTextures(FCPrev).Sides = txtTSides.Text
     End If
     
     Select Case cmbFOFType.ListIndex
     
     Case FC_NONE        ' Shouldn't happen.
     
          Exit Sub
     
     Case FC_BLOCK
          fraBlock.ZOrder 0
          
          If Not FCPreviouslySelected(FC_BLOCK) Then
               SetupWithBlockDefaults
          End If
          
     Case FC_WATER
          fraWater.ZOrder 0
          
          If Not FCPreviouslySelected(FC_WATER) Then
               SetupWithWaterDefaults
          End If
     End Select
     
     If FCPreviouslySelected(cmbFOFType.ListIndex) Then
          txtTCeiling.Text = FCTextures(cmbFOFType.ListIndex).Ceil
          txtTFloor.Text = FCTextures(cmbFOFType.ListIndex).Floor
          txtTSides.Text = FCTextures(cmbFOFType.ListIndex).Sides
     End If
     
     FCPrev = cmbFOFType.ListIndex
     FCPreviouslySelected(cmbFOFType.ListIndex) = True
     
End Sub



Private Sub cmbPresets_Click()

     Dim Preset As Dictionary
     
     ' Disable deletion if the default preset is selected.
     cmdDelPreset.Enabled = (cmbPresets.ListIndex > 0)

     If cmbPresets.ListIndex >= 0 Then
          Set Preset = Presets(Trim$(Str$(cmbPresets.ItemData(cmbPresets.ListIndex))))
          
          ' Do this first so that its texture-setting is overridden.
          cmbFOFType.ListIndex = Preset("class")
          
          txtTFloor.Text = Preset("floor")
          txtTCeiling.Text = Preset("ceiling")
          txtTSides.Text = Preset("sides")
          FCPreviouslySelected(Preset("class")) = True      ' Stop resetting of stuff.
          
          Select Case cmbFOFType.ListIndex
          Case FC_BLOCK
               ctlValueBoxLight.Value = Preset("bright")
               SetInterfaceFromFlags Preset("flags")
               
          Case FC_WATER
               ctlValueBoxWaterLight.Value = Preset("bright")
               If Preset("translucent") <> 0 Then optTranslucent.Value = True Else optOpaque.Value = True
               
               Select Case Preset("damage")
               Case DAMAGE_NONE
                    chkDamage.Value = vbUnchecked
               Case DAMAGE_NONELEM
                    chkDamage.Value = vbChecked
                    optNonElem.Value = True
               Case DAMAGE_SLIME
                    chkDamage.Value = vbChecked
                    optSlime.Value = True
               Case DAMAGE_FIRE
                    chkDamage.Value = vbChecked
                    optFire.Value = True
               End Select
               chkDamage_Click
               
               If Preset("usecolmap") <> 0 Then
                    chkClrmap.Value = vbChecked
                    SetColourmapControlsFromString Preset("colourmap")
               Else
                    chkClrmap.Value = vbUnchecked
               End If
               chkClrmap_Click
               
               If Preset("rendersides") <> 0 Then chkWaterSides.Value = vbChecked Else chkWaterSides.Value = vbUnchecked
               
          End Select
               
     End If

End Sub

Private Sub cmdCancel_Click()

     frmMain.RemoveHighlight
     RemoveSelection False

     ' Make sure we're not in 3D mode.
     ' Inelegant...
     If ForceLD < 0 Then
     
          PerformUndo
          WithdrawRedo
     
     End If
     
     SaveFOFPresets
     
     Cancelled = True
     
     Unload Me

End Sub

Private Sub cmdDelPreset_Click()

     If cmbPresets.ListIndex > 0 Then
          Presets.Remove Trim$(Str$(cmbPresets.ItemData(cmbPresets.ListIndex)))
          cmbPresets.RemoveItem cmbPresets.ListIndex
     End If

End Sub

Private Sub cmdOK_Click()

     Dim sd As MAPSIDEDEF
     Dim FOFFlags As Long
     Dim flagsWeDontUse As Long
     Dim i As Long, effect As Long
     Dim colourmapld As Long
     Dim newCMSIndex As Long
     
     CreateUndo "FOF OK"

     ' We already have a control sector at this point.
     
     sd = sidedefs(linedefs(ldindex).s1)
     
     sectors(ctrlSectorIndex).hceiling = ctlValueBoxTopAbs.Value
     sectors(ctrlSectorIndex).HFloor = ctlValueBoxBotAbs.Value
     sectors(ctrlSectorIndex).tceiling = txtTCeiling.Text
     sectors(ctrlSectorIndex).TFloor = txtTFloor.Text
     sidedefs(linedefs(ldindex).s1).middle = txtTSides.Text

     Select Case cmbFOFType.ListIndex
     Case FC_BLOCK
     
          sectors(ctrlSectorIndex).Brightness = ctlValueBoxLight.Value
          
          sd.middle = txtTSides.Text
          If chkTrans.Value = 1 Then sd.upper = "#" & right$("00" & Hex(ctlValueBoxAlpha.Value), 3)
          
          ' Build FOFFlags from selected values.
          FOFFlags = BuildFOFFlagsFromInterface()
          
          ' Now, look through our array to see if we match any.
          flagsWeDontUse = FF_CUTLEVEL Or FF_CUTSPRITES Or FF_BOTHPLANES Or FF_ALLSIDES Or FF_INVERTSIDES
          
          effect = LE_CUSTOM
          For i = LBound(FOFTypes) To UBound(FOFTypes)
          
               If (FOFTypes(i, 1) And (Not flagsWeDontUse)) = FOFFlags Then
               
                    effect = FOFTypes(i, 0)
                    
               End If
               
          Next i
          
          linedefs(ldindex).effect = effect
          
          If effect = LE_CUSTOM Then
          
               ' The FOFFlags go on s2.
               If linedefs(ldindex).s2 < 0 Then
               
                    linedefs(ldindex).s2 = CreateSidedef
                    With sidedefs(linedefs(ldindex).s2)
                         .linedef = ldindex
                         .sector = ctrlSectorIndex     ' Hmmm... should be okay: not rendered
                    End With
                    
               End If
               
               If (Not FF_TRANSLUCENT) And (Not FF_BUSTUP) And FF_RENDERALL And (Not FF_BOTHPLANES) Then FOFFlags = FOFFlags Or FF_CUTLEVEL
               sidedefs(linedefs(ldindex).s2).upper = Hex(FOFFlags)
               
          Else
          
               ' Special checks: Knux, shadows.
               If (Not (FOFFlags And FF_RENDERSIDES)) Or (FOFFlags And FF_PLATFORM) Then
                    If chkShadow.Value = 1 Then
                         linedefs(ldindex).Flags = linedefs(ldindex).Flags Or LF_NOCLIMB
                    Else
                         linedefs(ldindex).Flags = linedefs(ldindex).Flags And (Not LF_NOCLIMB)
                    End If
               End If
               
          End If
          
          
          
     Case FC_WATER
     
          sectors(ctrlSectorIndex).Brightness = ctlValueBoxWaterLight.Value
          
          If chkDamage.Value = 1 Then
               If optNonElem.Value = True Then
                    sectors(ctrlSectorIndex).special = SS_NONELEMHURT
               ElseIf optFire.Value = True Then
                    sectors(ctrlSectorIndex).special = SS_FIREHURT
               ElseIf optSlime.Value = True Then
                    sectors(ctrlSectorIndex).special = SS_SLIMEHURT
               End If
          End If
          
          If optOpaque.Value = True Then
               If chkWaterSides.Value = 1 Then
                    linedefs(ldindex).effect = LE_W_OPAQUE
               Else
                    linedefs(ldindex).effect = LE_W_OPAQUENOSIDES
               End If
          Else
               If chkWaterSides.Value = 1 Then
                    linedefs(ldindex).effect = LE_W_TRANS
               Else
                    linedefs(ldindex).effect = LE_W_TRANSNOSIDES
               End If
          End If
          
          ' TODO: If one colourmap is tagged to multiple sectors, we might get undersirable
          ' effects.
          If chkClrmap.Value = vbChecked Then
          
               colourmapld = GetColourmapLD(sectors(ctrlSectorIndex).Tag)
               If colourmapld = -1 Then
               
                    ' Make a cm sector and ld.
                    newCMSIndex = CreateControlSector
                    
                    If newCMSIndex < 0 Then
                         MsgBox "Unable to create colourmap control sector.", vbCritical
                         
                         PerformUndo
                         WithdrawRedo
                         
                         Exit Sub
                    End If
                    
                    ' Give the control sector a tag if necessary.
                    If sectors(ctrlSectorIndex).Tag = 0 Then
                         sectors(ctrlSectorIndex).Tag = NextUnusedTag
                    End If
                    
                    For i = numlinedefs To 0 Step -1
                    
                         If linedefs(i).s1 >= 0 Then
                              If sidedefs(linedefs(i).s1).sector = newCMSIndex Then
                              
                                   colourmapld = i
                                   linedefs(colourmapld).effect = LE_COLOURMAP
                                   linedefs(colourmapld).Tag = sectors(ctrlSectorIndex).Tag
                                   Exit For
                              
                              End If
                         End If
                         
                    Next i
                    
               End If
               
               sidedefs(linedefs(colourmapld).s1).upper = lblColour.Caption
                    
          End If
     
     End Select
     
     SaveFOFPresets
     
     UpdateFOFTagCache
     
     WithdrawUndo        ' FOF OK.
     mapchanged = True
     Cancelled = False
     RedrawMap True
     
     Unload Me

End Sub

Private Sub cmdPresetLava_Click()

     chkClrmap.Value = 1
     chkClrmap_Click
     
     CommonDialog1.Color = &H77FF&
     ctlValueBoxWaterAlpha.Value = Asc("J") - Asc("A")
     
     optOpaque.Value = True
     
     chkDamage.Value = 1
     optFire.Value = True
     chkDamage_Click
     
     txtTCeiling.Text = "LAVA1"
     txtTFloor.Text = "LAVA1"
     txtTSides.Text = "-"
     
     UpdateColourmapControls
     
End Sub

Private Sub cmdPresetSlime_Click()

     chkClrmap.Value = 1
     chkClrmap_Click
     
     CommonDialog1.Color = &HE000E0
     ctlValueBoxWaterAlpha.Value = Asc("S") - Asc("A")
     
     optTranslucent.Value = True
     
     chkDamage.Value = 1
     optSlime.Value = True
     chkDamage_Click
     
     txtTCeiling.Text = "CHEMG01"
     txtTFloor.Text = "CHEMG01"
     txtTSides.Text = "-"
     
     UpdateColourmapControls

End Sub

Private Sub cmdPresetWater_Click()

     chkClrmap.Value = 1
     chkClrmap_Click
     
     CommonDialog1.Color = &HFF0000
     ctlValueBoxWaterAlpha.Value = Asc("J") - Asc("A")
     
     optTranslucent.Value = True
     chkDamage.Value = 0
     chkDamage_Click
     
     txtTCeiling.Text = "FWATER1"
     txtTFloor.Text = "FWATER1"
     txtTSides.Text = "-"
     
     UpdateColourmapControls

End Sub

Private Sub SetControlValuesFromAbs()

     ctlValueBoxBotBelowTop.Value = Val(ctlValueBoxTopAbs.Value) - Val(ctlValueBoxBotAbs.Value)
     ctlValueBoxBotBelowCeil.Value = minceil - Val(ctlValueBoxBotAbs.Value)
     ctlValueBoxBotAboveFlr.Value = Val(ctlValueBoxBotAbs.Value) - maxflr
     ctlValueBoxTopAboveBot.Value = Val(ctlValueBoxTopAbs.Value) - Val(ctlValueBoxBotAbs.Value)
     ctlValueBoxBotBelowTop.Value = Val(ctlValueBoxTopAbs.Value) - Val(ctlValueBoxBotAbs.Value)
     ctlValueBoxTopBelowCeil.Value = minceil - Val(ctlValueBoxTopAbs.Value)
     ctlValueBoxTopAboveFlr.Value = Val(ctlValueBoxTopAbs.Value) - maxflr
     ctlValueBoxTopAboveBot.Value = Val(ctlValueBoxTopAbs.Value) - Val(ctlValueBoxBotAbs.Value)

End Sub

Private Sub cmdSavePreset_Click()

     Dim Response As String, i As Long, NewPresetIndex As Long, NewListIndex As Long
     Dim Preset As Dictionary

     Response = InputBox("Please enter a name for the new preset. If a preset of that name already exists, it will be replaced.", , cmbPresets.List(cmbPresets.ListIndex))
     
     If Response <> "" Then
          ' Check to see if we already have a preset of that name.
          NewPresetIndex = -1
          For i = 0 To cmbPresets.ListCount - 1
               If StrComp(cmbPresets.List(i), Response, vbTextCompare) = 0 Then
                    NewPresetIndex = cmbPresets.ItemData(i)
                    NewListIndex = i
               End If
          Next i
          
          ' If it's a new preset.
          If NewPresetIndex < 0 Then
               NewPresetIndex = Presets("count")
               Presets("count") = NewPresetIndex + 1
               
               cmbPresets.AddItem Response
               NewListIndex = cmbPresets.ListCount - 1
               cmbPresets.ItemData(NewListIndex) = NewPresetIndex
               
               Presets.Add Trim$(Str$(NewPresetIndex)), New Dictionary
          End If
          
          ' Here we go...
          Set Preset = Presets(Trim$(Str$(NewPresetIndex)))
          Preset("name") = Response
          Preset("floor") = txtTFloor.Text
          Preset("ceiling") = txtTCeiling.Text
          Preset("sides") = txtTSides.Text
          Preset("class") = cmbFOFType.ListIndex
          
          Select Case cmbFOFType.ListIndex
          Case FC_BLOCK
               Preset("bright") = ctlValueBoxLight.Value
               Preset("flags") = BuildFOFFlagsFromInterface()
               
          Case FC_WATER
               Preset("bright") = ctlValueBoxWaterLight.Value
               If optTranslucent.Value = True Then Preset("translucent") = 1 Else Preset("translucent") = 0
               
               If chkDamage.Value = vbUnchecked Then
                    Preset("damage") = DAMAGE_NONE
               ElseIf optNonElem.Value = True Then
                    Preset("damage") = DAMAGE_NONELEM
               ElseIf optSlime.Value = True Then
                    Preset("damage") = DAMAGE_SLIME
               ElseIf optFire.Value = True Then
                    Preset("damage") = DAMAGE_FIRE
               End If
               
               If chkClrmap.Value = vbChecked Then
                    Preset("usecolmap") = 1
                    Preset("colourmap") = lblColour.Caption
               Else
                    Preset("usecolmap") = 0
               End If
                              
               If chkWaterSides.Value = vbChecked Then Preset("rendersides") = 1 Else Preset("rendersides") = 0
               
          End Select
          
          cmbPresets.ListIndex = NewListIndex
          
     End If

End Sub

Private Sub ctlValueBoxBotAboveFlr_Change()

     If Not noAutoCalc Then
          noAutoCalc = True
          ctlValueBoxBotAbs.Value = maxflr + Val(ctlValueBoxBotAboveFlr.Value)
          SetControlValuesFromAbs
          noAutoCalc = False
     End If

End Sub


Private Sub ctlValueBoxBotAbs_Change()

     If Not Me.ActiveControl Is Nothing Then
          If Me.ActiveControl.Name = ctlValueBoxBotAbs.Name Then Exit Sub
     End If

     If Not noAutoCalc Then
     
          noAutoCalc = True
          
          If Val(ctlValueBoxBotAbs.Value) > Val(ctlValueBoxTopAbs.Value) Then
               ctlValueBoxBotAbs.Value = ctlValueBoxTopAbs.Value
          End If
          
          SetControlValuesFromAbs
          
          noAutoCalc = False
          
     End If

End Sub


Private Sub ctlValueBoxBotAbs_LostFocus()

     ctlValueBoxBotAbs_Change

End Sub

Private Sub ctlValueBoxBotBelowCeil_Change()

     If Not noAutoCalc Then
          noAutoCalc = True
          ctlValueBoxBotAbs.Value = minceil - Val(ctlValueBoxBotBelowCeil.Value)
          SetControlValuesFromAbs
          noAutoCalc = False
     End If

End Sub

Private Sub ctlValueBoxBotBelowTop_Change()

     If Not noAutoCalc Then
          noAutoCalc = True
          ctlValueBoxBotAbs.Value = Val(ctlValueBoxTopAbs.Value) - Val(ctlValueBoxBotBelowTop.Value)
          SetControlValuesFromAbs
          noAutoCalc = False
     End If

End Sub

Private Sub ctlValueBoxTopAboveBot_Change()

     If Not noAutoCalc Then
          noAutoCalc = True
          ctlValueBoxTopAbs.Value = Val(ctlValueBoxBotAbs.Value) + Val(ctlValueBoxTopAboveBot.Value)
          SetControlValuesFromAbs
          noAutoCalc = False
     End If

End Sub

Private Sub ctlValueBoxTopAboveFlr_Change()

     If Not noAutoCalc Then
          noAutoCalc = True
          ctlValueBoxTopAbs.Value = maxflr + Val(ctlValueBoxTopAboveFlr.Value)
          SetControlValuesFromAbs
          noAutoCalc = False
     End If

End Sub


Private Sub ctlValueBoxTopAbs_Change()

     If Not Me.ActiveControl Is Nothing Then
          If Me.ActiveControl.Name = ctlValueBoxTopAbs.Name Then Exit Sub
     End If

     If Not noAutoCalc Then
     
          noAutoCalc = True
          
          If Val(ctlValueBoxTopAbs.Value) < Val(ctlValueBoxBotAbs.Value) Then
               ctlValueBoxTopAbs.Value = ctlValueBoxBotAbs.Value
          End If
          
          SetControlValuesFromAbs
     
          noAutoCalc = False
          
     End If

End Sub


Private Sub ctlValueBoxTopAbs_LostFocus()

     ctlValueBoxTopAbs_Change

End Sub

Private Sub ctlValueBoxTopBelowCeil_Change()

     If Not noAutoCalc Then
          noAutoCalc = True
          ctlValueBoxTopAbs.Value = minceil - ctlValueBoxTopBelowCeil.Value
          SetControlValuesFromAbs
          noAutoCalc = False
          
     End If

End Sub

Private Sub ctlValueBoxWaterAlpha_Change()

     UpdateColourmapControls

End Sub

Private Sub Form_Activate()

     If bomb Then cmdCancel_Click

End Sub

Private Sub Form_Load()

     Dim i As Long, newCIIndex As Long
     Dim f As String, c As String, t As String
     Dim matches As Long, FOFFlags As Long, ret As Long, FOFClass As Long
     
     bomb = False        ' Globals persist after unloading, apparently...
     
     ' Listen to me when I tell you what colour to use!
     CommonDialog1.Flags = cdlCCRGBInit
     
     
     ' Clear the visited flags.
     For i = 0 To FC_MAX
          FCPreviouslySelected(i) = False
     Next i
     
     ' This is set later.
     FCPrev = -1
     
     ' Populate the presets listbox.
     PopulatePresetList
     
     
     needNewCtrlSector = False
     
     If ForceLD >= 0 Then

          fraAdjacent.Enabled = False
          lblCeilRange.Enabled = False
          lblFloorRange.Enabled = False
          ldindex = ForceLD
          ctrlTag = linedefs(ldindex).Tag
          Floor = sectors(ParentSector).HFloor
          Ceil = sectors(ParentSector).hceiling
          GetFOFFlagsAndClassByLDIndex ldindex, FOFFlags, FOFClass
          
     Else
     
          ' Not 3D-mode safe.
          CreateUndo "FOF setup", , , False
     
          ' We do this first, since we need maxflr and minceil
          f = frmSector.CheckSectorHFloor
          c = frmSector.CheckSectorHFloor
          
          If f = "" Then
          
               Floor = -1
               ctlValueBoxBotAboveFlr.Enabled = False
               ctlValueBoxTopAboveFlr.Enabled = False
               
          Else
               
               Floor = Val(f)
               
               
          End If
          
          If c = "" Then
          
               Ceil = -1
               ctlValueBoxBotBelowCeil.Enabled = False
               ctlValueBoxTopBelowCeil.Enabled = False
               
          Else
               
               Ceil = Val(c)
               
               
          End If
          
          lblCeilRange.Caption = GetCeilRange
          lblFloorRange.Caption = GetFloorRange
          GetSelectedFCRange
     
          t = frmSector.CheckSectorTag
          matches = 0
          
          If t = "" Then
          
               needNewCtrlSector = True
               
          Else
     
               ctrlTag = Val(t)
               ldindex = FindFOFLinedefs(ctrlTag, matches, FOFFlags, FOFClass)
               
               If ctrlTag = 0 Or matches = 0 Then
                    needNewCtrlSector = True
               ElseIf matches > 1 Then
                    ret = MsgBox("There are multiple FOF effect linedefs tagged to these sectors. Editing existing FOFs might have unexpected results. Do you wish to create a new FOF, replacing any existing ones in these sectors?", vbExclamation Or vbYesNoCancel)
                    
                    Select Case ret
                    Case vbYes
                         needNewCtrlSector = True
                    Case vbCancel
                         bomb = True
                         Exit Sub
                    End Select
                    
               End If
               
          End If
          
     End If
     
     If needNewCtrlSector Then
     
          
          ctrlTag = NextUnusedTag
          
          ' Set all selected sectors' tags to ctrlTag
          For i = 0 To numsectors
               If sectors(i).selected Then sectors(i).Tag = ctrlTag
          Next i
     
          newCIIndex = CreateControlSector()
          
          If newCIIndex <> -1 Then
               
               ctrlSectorIndex = newCIIndex
               
               ' Pick a linedef from the new sector.
               For i = numlinedefs To 0 Step -1        ' I assume the new one's at the end.
               
                    If linedefs(i).s1 >= 0 Then
                         If sidedefs(linedefs(i).s1).sector = newCIIndex Then
                         
                              ldindex = i
                              Exit For
                              
                         End If
                    End If
                                   
               Next i
               
               linedefs(ldindex).Tag = ctrlTag
          
               If cmbPresets.Enabled Then
                    cmbPresets.ListIndex = 0
                    cmbPresets_Click
                    FOFClass = cmbFOFType.ListIndex
                    FCPrev = cmbFOFType.ListIndex
               Else
                    SetupWithBlockDefaults
                    FCPreviouslySelected(FC_BLOCK) = True
                    FCPrev = FC_BLOCK
                    FOFClass = FC_BLOCK
                    
                    ' Set textures/flats
                    txtTSides.Text = "GFZROCK"
                    txtTCeiling.Text = "FLOOR0_3"
                    txtTFloor.Text = "FLOOR0_3"
               End If
               
               noAutoCalc = True
               ctlValueBoxBotAbs.Value = maxflr
               ctlValueBoxTopAbs.Value = minceil
               noAutoCalc = False
               
               ctlValueBoxBotAbs_Change
               ctlValueBoxTopAbs_Change
               
               cmbFOFType.ListIndex = FOFClass
               
          Else
          
               MsgBox "Couldn't create control sector.", vbCritical
               bomb = True
               Exit Sub
               
          End If
     
     Else
     
          ctrlSectorIndex = sidedefs(linedefs(ldindex).s1).sector
          
          If FOFClass <> FC_NONE Then FCPreviouslySelected(FOFClass) = True
          
          Select Case FOFClass
          Case FC_BLOCK
               ' Parse FOFFlags.
               SetInterfaceFromFlags FOFFlags
               
               ' Get alpha.
               If FOFFlags And FF_TRANSLUCENT Then
                    ctlValueBoxAlpha.Value = Val("&H" + Mid$(sidedefs(linedefs(ldindex).s1).upper, 2))
               Else
                    ctlValueBoxAlpha.Value = 0
               End If
               
               ' Get brightness.
               ctlValueBoxLight.Value = sectors(ctrlSectorIndex).Brightness
               
          Case FC_WATER
          
               SetWaterInterface
          
          End Select
          
          FCPrev = FOFClass
          
          ' Set textures/flats
          txtTSides.Text = sidedefs(linedefs(ldindex).s1).middle
          txtTCeiling.Text = sectors(ctrlSectorIndex).tceiling
          txtTFloor.Text = sectors(ctrlSectorIndex).TFloor
          
          noAutoCalc = True
          ctlValueBoxBotAbs.Value = Val(sectors(ctrlSectorIndex).HFloor)
          ctlValueBoxTopAbs.Value = Val(sectors(ctrlSectorIndex).hceiling)
          noAutoCalc = False
          
          ctlValueBoxBotAbs_Change
          ctlValueBoxTopAbs_Change
          
          cmbFOFType.ListIndex = FOFClass
     
     End If
          

End Sub


Private Sub picColourmap_Click()

     CommonDialog1.ShowColor
     UpdateColourmapControls

End Sub


Private Sub imgTCeiling_Click()

     txtTCeiling.Text = SelectFlat(txtTCeiling.Text, Me)

End Sub

Private Sub imgTFloor_Click()

     txtTFloor.Text = SelectFlat(txtTFloor.Text, Me)

End Sub

Private Sub imgTSides_Click()

     txtTSides.Text = SelectTexture(txtTSides.Text, Me)

End Sub



Private Sub GetSelectedFCRange()

     Dim i As Long
     Dim first As Boolean
     
     
     ' Go through all selected linedefs
     i = 0
     first = True
     For i = 0 To numsectors - 1
     
          If sectors(i).selected Then

               If sectors(i).selected Then
          
                    If (first) Or sectors(i).hceiling < minceil Then minceil = sectors(i).hceiling
                    If (first) Or sectors(i).HFloor > maxflr Then maxflr = sectors(i).HFloor
                    
                    first = False
                    
               End If
               
               
          End If
     
     Next i

End Sub






Private Sub SetInterfaceFromFlags(ByVal Flags As Long)

     If (Flags And FF_SOLID) Then chkSolid.Value = vbChecked Else chkSolid.Value = vbUnchecked
     If (Flags And FF_RENDERSIDES) Then chkSides.Value = vbChecked Else chkSides.Value = vbUnchecked
     If (Flags And FF_RENDERPLANES) Then chkPlanes.Value = vbChecked Else chkPlanes.Value = vbUnchecked
     If Not (Flags And FF_NOSHADE) Then chkShadow.Value = vbChecked Else chkShadow.Value = vbUnchecked
     
     If (Flags And FF_TRANSLUCENT) Then chkTrans.Value = vbChecked Else chkTrans.Value = vbUnchecked
     chkTrans_Click
     
     If (Flags And FF_FLOATBOB) Then chkFloat.Value = vbChecked Else chkFloat.Value = vbUnchecked
     If (Flags And FF_CRUMBLE) Then
          chkCrumble.Value = vbChecked
          If Not (Flags And FF_NORETURN) Then chkRespawn.Value = vbChecked Else chkRespawn.Value = vbUnchecked
     Else
          chkCrumble.Value = vbUnchecked
     End If
     If (Flags And FF_SHATTERBOTTOM) Then chkShatterBottom.Value = vbChecked
     If (Flags And FF_MARIO) Then chkMario.Value = vbChecked Else chkMario.Value = vbUnchecked
     
     If (Flags And FF_BUSTUP) Then
          chkBreak.Value = vbChecked
          If (Flags And FF_ONLYKNUX) Then chkBreakKnux.Value = vbChecked Else chkBreakKnux.Value = vbUnchecked
     Else
          chkBreak.Value = vbUnchecked
     End If
     chkBreak_Click
     
     'If (flags And FF_QUICKSAND) Then chkQuicksand.Value = vbChecked
     If (Flags And FF_PLATFORM) Then chkIntBelow.Value = vbChecked Else chkIntBelow.Value = vbUnchecked
     If (Flags And FF_SHATTER) Then chkShatter.Value = vbChecked Else chkShatter.Value = vbUnchecked
     If (Flags And FF_SPINBUST) Then chkSpinBust.Value = vbChecked Else chkSpinBust.Value = vbUnchecked

End Sub

Private Sub lblColour_Click()

     picColourmap_Click

End Sub

Private Sub txtTCeiling_Change()

     GetScaledFlatPicture txtTCeiling.Text, imgTCeiling

End Sub

Private Sub txtTFloor_Change()

     GetScaledFlatPicture txtTFloor.Text, imgTFloor

End Sub

Private Sub txtTSides_Change()

     GetScaledTexturePicture txtTSides.Text, imgTSides

End Sub


Private Sub UpdateColourmapControls()

     lblColour.Caption = "#" & _
                         right$("0" & Hex(CommonDialog1.Color And &HFF&), 2) & _
                         right$("0" & Hex((CommonDialog1.Color And &HFF00&) / 256), 2) & _
                         right$("0" & Hex((CommonDialog1.Color And &HFF0000) / 65536), 2)
                         
     If ctlValueBoxWaterAlpha.Value >= 26 Then
     
          lblColour.Caption = lblColour.Caption & Trim$(Str$(ctlValueBoxWaterAlpha.Value - 26))
          
     Else
     
          lblColour.Caption = lblColour.Caption & Chr$(Asc("A") + ctlValueBoxWaterAlpha.Value)
          
     End If
     
     
     picColourmap.BackColor = CommonDialog1.Color
     
     ' Algorithm from comments on a blog somewhere.
     If 2 * (CommonDialog1.Color And &HFF&) + _
        5 * ((CommonDialog1.Color And &HFF00&) / 256) + _
            (CommonDialog1.Color And &HFF0000) / 65536 > &H400& Then
        
          lblColour.ForeColor = vbBlack
     
     Else
     
          lblColour.ForeColor = vbWhite
          
     End If
          
End Sub


Private Sub SetWaterInterface()

     Dim substr As String
     Dim i As Long
     Dim colourmapld As Long

     Select Case linedefs(ldindex).effect
     Case LE_W_TRANS
          optTranslucent.Value = True
          chkWaterSides.Value = 1
     Case LE_W_TRANSNOSIDES
          optTranslucent.Value = True
          chkWaterSides.Value = 0
     Case LE_W_OPAQUE
          optOpaque.Value = True
          chkWaterSides.Value = 1
     Case LE_W_OPAQUENOSIDES
          optOpaque.Value = True
          chkWaterSides.Value = 0
     End Select
     
     Select Case sectors(ctrlSectorIndex).special
     Case SS_FIREHURT
          chkDamage.Value = 1
          optFire.Value = True
     Case SS_SLIMEHURT
          chkDamage.Value = 1
          optSlime.Value = True
     Case SS_NONELEMHURT
          chkDamage.Value = 1
          optNonElem.Value = True
     End Select
     chkDamage_Click
     
     ctlValueBoxWaterLight.Value = sectors(ctrlSectorIndex).Brightness
     
     ' Find linedefs tagged to the control sector, and check if they're colourmaps.
     colourmapld = GetColourmapLD(sectors(ctrlSectorIndex).Tag)
     
     If colourmapld <> -1 Then
     
          chkClrmap.Value = vbChecked
     
          SetColourmapControlsFromString sidedefs(linedefs(colourmapld).s1).upper
     
     End If

End Sub


Private Function GetColourmapLD(secTag As Long) As Long

     Dim i As Long
     
     GetColourmapLD = -1
     If secTag <> 0 Then
     
          For i = 0 To numlinedefs - 1
          
               If linedefs(i).Tag = secTag And linedefs(i).effect = LE_COLOURMAP Then
               
                    GetColourmapLD = i
                    
               End If
          
          Next i
          
     End If

End Function


Private Sub SetupWithBlockDefaults()

     ' Everything should be unchecked to start with. I hope...
     chkSolid.Value = 1
     chkSides.Value = 1
     chkPlanes.Value = 1
     chkShadow.Value = 1
     ctlValueBoxLight.Value = 128

End Sub

Private Sub SetupWithWaterDefaults()

     cmdPresetWater_Click
     chkWaterSides.Value = 1
     ctlValueBoxWaterLight.Value = 255

End Sub


Private Sub PopulatePresetList()

     Dim PresetKeys As Variant, v As Variant

     Set PresetsFile = New clsConfiguration
     PresetsFile.LoadConfiguration App.Path & "\FOFPresets.cfg"
     If PresetsFile.ReadSetting("type", "") <> FOFPRESET_CONFIG_TYPE Then
          cmbPresets.Enabled = False
          cmdSavePreset.Enabled = False
          cmdDelPreset.Enabled = False
     Else
          Set Presets = PresetsFile.Root(True)
          PresetKeys = Presets.Keys()
          For Each v In PresetKeys
               If v <> "count" And v <> "type" Then
                    cmbPresets.AddItem (Presets(v)("name"))
                    cmbPresets.ItemData(cmbPresets.ListCount - 1) = Val(v)
               End If
          Next v
     End If
     
End Sub

Private Sub SetColourmapControlsFromString(ByVal ClrmapStr As String)

     Dim substr As String

     CommonDialog1.Color = RGB(Val("&H" & Mid$(ClrmapStr, 2, 2)), _
                               Val("&H" & Mid$(ClrmapStr, 4, 2)), _
                               Val("&H" & Mid$(ClrmapStr, 6, 2)))
                              
     substr = Mid$(ClrmapStr, 8, 1)
     If Asc(substr) >= Asc("0") And Asc(substr) <= Asc("9") Then
          ctlValueBoxWaterAlpha.Value = Asc(substr) - Asc("0") + 26
     Else
          ctlValueBoxWaterAlpha.Value = Asc(substr) - Asc("A")
     End If

End Sub


Private Function BuildFOFFlagsFromInterface() As Long

     Dim FOFFlags As Long

     FOFFlags = FF_EXISTS
     If chkShatterBottom.Value = vbChecked Then FOFFlags = FOFFlags Or FF_SHATTERBOTTOM
     If chkBreak.Value = vbChecked Or chkBreakKnux.Value = vbChecked Then FOFFlags = FOFFlags Or FF_BUSTUP
     If chkCrumble.Value = vbChecked Then
          FOFFlags = FOFFlags Or FF_CRUMBLE
          If Not chkRespawn.Value = vbChecked Then FOFFlags = FOFFlags Or FF_NORETURN
     End If
     If chkFloat.Value = vbChecked Then FOFFlags = FOFFlags Or FF_FLOATBOB
     If chkIntBelow.Value = vbChecked Then FOFFlags = FOFFlags Or FF_PLATFORM
     If chkMario.Value = vbChecked Then FOFFlags = FOFFlags Or FF_MARIO
     If chkPlanes.Value = vbChecked Then FOFFlags = FOFFlags Or FF_RENDERPLANES
     'If chkQuicksand.Value = vbChecked Then FOFFlags = FOFFlags Or FF_QUICKSAND
     If Not chkShadow.Value = vbChecked Then FOFFlags = FOFFlags Or FF_NOSHADE
     If chkShatter.Value = vbChecked Then FOFFlags = FOFFlags Or FF_SHATTER
     If chkSides.Value = vbChecked Then FOFFlags = FOFFlags Or FF_RENDERSIDES
     If chkSolid.Value = vbChecked Then FOFFlags = FOFFlags Or FF_SOLID
     If chkSpinBust.Value = vbChecked Then FOFFlags = FOFFlags Or FF_SPINBUST
     If chkTrans.Value = vbChecked Then FOFFlags = FOFFlags Or FF_TRANSLUCENT
     
     BuildFOFFlagsFromInterface = FOFFlags

End Function


Private Sub SaveFOFPresets()

     PresetsFile.SaveConfiguration App.Path & "\FOFPresets.cfg"

End Sub
