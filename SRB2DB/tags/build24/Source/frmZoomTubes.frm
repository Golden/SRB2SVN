VERSION 5.00
Begin VB.Form frmZoomTubes 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Zoom Tubes Wizard"
   ClientHeight    =   3195
   ClientLeft      =   2760
   ClientTop       =   3750
   ClientWidth     =   6030
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmZoomTubes.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3195
   ScaleWidth      =   6030
   ShowInTaskbar   =   0   'False
   Begin VB.Frame fraSpeed 
      BorderStyle     =   0  'None
      Height          =   2415
      Left            =   120
      TabIndex        =   15
      Top             =   120
      Width           =   5775
      Begin DoomBuilder.ctlValueBox ctlValueBoxSpeed 
         Height          =   375
         Left            =   2040
         TabIndex        =   18
         Top             =   870
         Width           =   975
         _ExtentX        =   1720
         _ExtentY        =   661
         SmallChange     =   8
         Value           =   "64"
      End
      Begin VB.Label Label11 
         AutoSize        =   -1  'True
         Caption         =   "Movement speed:"
         Height          =   195
         Left            =   600
         TabIndex        =   17
         Top             =   960
         Width           =   1290
      End
      Begin VB.Label Label10 
         Caption         =   "To continue, enter a speed for the movement of the player within the zoom tube, and click Next."
         Height          =   495
         Left            =   240
         TabIndex        =   16
         Top             =   1680
         Width           =   5175
         WordWrap        =   -1  'True
      End
   End
   Begin VB.Frame fraWaypoints 
      BorderStyle     =   0  'None
      Height          =   2415
      Left            =   120
      TabIndex        =   7
      Top             =   120
      Width           =   5775
      Begin VB.Label Label8 
         Caption         =   $"frmZoomTubes.frx":000C
         Height          =   975
         Left            =   240
         TabIndex        =   14
         Top             =   840
         Width           =   5175
         WordWrap        =   -1  'True
      End
      Begin VB.Label Label7 
         Caption         =   "To complete the wizard, select your waypoints in order and click Finish. You may create them now if you have not done so already."
         Height          =   495
         Left            =   240
         TabIndex        =   13
         Top             =   1920
         Width           =   5175
         WordWrap        =   -1  'True
      End
      Begin VB.Label Label6 
         Caption         =   $"frmZoomTubes.frx":0144
         Height          =   855
         Left            =   240
         TabIndex        =   12
         Top             =   0
         Width           =   5175
         WordWrap        =   -1  'True
      End
   End
   Begin VB.Frame fraEndSector 
      BorderStyle     =   0  'None
      Height          =   2415
      Left            =   120
      TabIndex        =   6
      Top             =   120
      Width           =   5775
      Begin VB.Label Label5 
         Caption         =   $"frmZoomTubes.frx":0236
         Height          =   615
         Left            =   240
         TabIndex        =   11
         Top             =   240
         Width           =   5175
      End
      Begin VB.Label Label4 
         Caption         =   "To continue, select your end sector and click Next. You may create it now if you have not done so already."
         Height          =   495
         Left            =   240
         TabIndex        =   10
         Top             =   840
         Width           =   5175
         WordWrap        =   -1  'True
      End
   End
   Begin VB.Frame fraStartSector 
      BorderStyle     =   0  'None
      Height          =   2415
      Left            =   120
      TabIndex        =   5
      Top             =   120
      Width           =   5775
      Begin VB.Label Label3 
         Caption         =   "To continue, select your start sector and click Next. You may create it now if you have not done so already."
         Height          =   495
         Left            =   240
         TabIndex        =   9
         Top             =   1080
         Width           =   5175
         WordWrap        =   -1  'True
      End
      Begin VB.Label Label2 
         Caption         =   $"frmZoomTubes.frx":02C3
         Height          =   675
         Left            =   240
         TabIndex        =   8
         Top             =   240
         Width           =   5385
         WordWrap        =   -1  'True
      End
   End
   Begin VB.Frame fraInit 
      BorderStyle     =   0  'None
      Height          =   2415
      Left            =   120
      TabIndex        =   3
      Top             =   120
      Width           =   5775
      Begin VB.Label Label1 
         Caption         =   "Preparing, please wait..."
         Height          =   255
         Left            =   120
         TabIndex        =   4
         Top             =   120
         Width           =   3975
      End
   End
   Begin VB.CommandButton cmdNext 
      Caption         =   "&Next >"
      Default         =   -1  'True
      Enabled         =   0   'False
      Height          =   375
      Left            =   3360
      TabIndex        =   0
      Top             =   2760
      Width           =   1215
   End
   Begin VB.CommandButton cmdBack 
      Caption         =   "< &Back"
      Enabled         =   0   'False
      Height          =   375
      Left            =   2160
      TabIndex        =   2
      Top             =   2760
      Width           =   1215
   End
   Begin VB.CommandButton cmdCancel 
      Cancel          =   -1  'True
      Caption         =   "Cancel"
      Enabled         =   0   'False
      Height          =   375
      Left            =   4680
      TabIndex        =   1
      Top             =   2760
      Width           =   1215
   End
End
Attribute VB_Name = "frmZoomTubes"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Enum ENUM_ZTWIZ_STATE
     ZTWIZ_STARTSEC
     ZTWIZ_ENDSEC
     ZTWIZ_SPEED
     ZTWIZ_WAYPOINTS
End Enum

Private Const MT_ZOOMTUBEWAYPOINT = 18
Private Const LT_ZOOMTUBEPARAMS = 18
Private Const ST_ZOOMTUBESTART = 998
Private Const ST_ZOOMTUBEEND = 999

Private State As ENUM_ZTWIZ_STATE
Private StartSec As Long, EndSec As Long, Speed As Long

Private Sub cmdBack_Click()

     Select Case State
     Case ZTWIZ_ENDSEC:
          cmdBack.Enabled = False
          fraStartSector.ZOrder 0
          State = ZTWIZ_STARTSEC
          frmMain.SetFocus
          RemoveSelection True
          
     Case ZTWIZ_SPEED:
          fraEndSector.ZOrder 0
          State = ZTWIZ_ENDSEC
          frmMain.SetFocus
          
     Case ZTWIZ_WAYPOINTS:
          fraSpeed.ZOrder 0
          State = ZTWIZ_SPEED
          cmdNext.Caption = "&Next >"
          RemoveSelection True
     End Select
     
     frmMain.SetFocus

End Sub

Private Sub cmdCancel_Click()

     Unload Me

End Sub

Private Sub cmdNext_Click()

     Dim i As Long
     Dim Indices As Variant
     Dim AllWaypoints As Boolean

     Select Case State
     Case ZTWIZ_STARTSEC:
          If numselected <> 1 Or mode <> EM_SECTORS Then
               MsgBox "You must select one sector to continue.", vbExclamation
               Exit Sub
          ElseIf sectors(selected.Items(0)).Tag <> 0 Then
               If MsgBox("That sector already has a tag. Using this as a start sector will remove existing effects tagged to it. Do you wish to continue?", vbExclamation Or vbYesNo) = vbNo Then
                    Exit Sub
               End If
          End If
          
          cmdBack.Enabled = True
          StartSec = selected.Items(0)
          fraEndSector.ZOrder 0
          State = ZTWIZ_ENDSEC
          frmMain.SetFocus
          RemoveSelection True
          
     Case ZTWIZ_ENDSEC:
          If numselected <> 1 Or mode <> EM_SECTORS Then
               MsgBox "You must select one sector to continue.", vbExclamation
               Exit Sub
          ElseIf sectors(selected.Items(0)).Tag <> 0 Then
               If MsgBox("That sector already has a tag. Using this as an end sector will remove existing effects tagged to it. Do you wish to continue?", vbExclamation Or vbYesNo) = vbNo Then
                    Exit Sub
               End If
          End If
          
          EndSec = selected.Items(0)
          fraSpeed.ZOrder 0
          State = ZTWIZ_SPEED
          RemoveSelection True
          
     Case ZTWIZ_SPEED:
          Speed = Val(ctlValueBoxSpeed.Value)
          fraWaypoints.ZOrder 0
          State = ZTWIZ_WAYPOINTS
          cmdNext.Caption = "&Finish"
          frmMain.SetFocus
          
     Case ZTWIZ_WAYPOINTS:
          If numselected = 0 Or mode <> EM_THINGS Then
               MsgBox "You must select at least one thing to continue.", vbExclamation
               Exit Sub
          Else
               Indices = selected.Items
               AllWaypoints = True
               For i = LBound(Indices) To UBound(Indices)
                    AllWaypoints = True And (things(Indices(i)).thing = MT_ZOOMTUBEWAYPOINT)
                    If Not AllWaypoints Then Exit For
               Next i
               
               If Not AllWaypoints Then
                    MsgBox "You may only select waypoint things.", vbExclamation
                    Exit Sub
               End If
          End If
          
          SetUpZTSequence
          Unload Me
          RedrawMap True
          
     End Select

End Sub



Private Sub ctlValueBoxSpeed_Validate(Cancel As Boolean)

     If Val(ctlValueBoxSpeed.Value) <> ctlValueBoxSpeed.Value Then Cancel = True

End Sub



Private Sub Form_Load()
     
     fraStartSector.ZOrder 0
     State = ZTWIZ_STARTSEC
     
     cmdNext.Enabled = True
     cmdCancel.Enabled = True

End Sub

Private Sub SetUpZTSequence()

     Dim i As Long
     Dim UsedSequences As New Dictionary
     Dim NewTag As Long
     Dim Sequence As Long
     Dim NewSec As Long
     Dim NewLinedefs As New collection
     Dim ControlLD As Long
     Dim Indices As Variant
     Dim CurrentWaypoint As Long
     Dim v As Variant
     Dim AllWaypoints As New Dictionary
     
     
     ' Take a copy of the selected waypoints, since selected gets clobbered.
     Indices = selected.Keys()
     For i = LBound(Indices) To UBound(Indices)
          AllWaypoints.Add Indices(i), selected(Indices(i))
     Next i
     

     ' Find the numbers of existing sequences.
     For i = 0 To numthings - 1
          If things(i).thing = MT_ZOOMTUBEWAYPOINT Then UsedSequences.Add i, i
     Next i
     
     ' Work out the number for the new sequence.
     i = 0
     Do While UsedSequences.Exists(i)
          i = i + 1
     Loop
     
     Sequence = i
     
     NewTag = NextUnusedTag()
     
     
     ' We now know the sequence number. Start changing stuff.
     

     ' Configure the sectors first.
     With sectors(StartSec)
          .special = ST_ZOOMTUBESTART
          .Tag = NewTag
     End With
     
     With sectors(EndSec)
          .special = ST_ZOOMTUBEEND
          .Tag = NewTag
     End With
     
     
     
     ' Create and configure the control sector.
     NewSec = CreateControlSector(Maximum(Sequence, Speed))
     
     If NewSec < 0 Then
          MsgBox "Unable to create control sector.", vbCritical
          Exit Sub
     End If
     
     ' Find the linedefs belonging to the new control sec.
     For i = 0 To numlinedefs - 1
          If linedefs(i).s1 >= 0 Then
               If sidedefs(linedefs(i).s1).sector = NewSec Then
                    NewLinedefs.Add i
               End If
          End If
     Next i
     
     ControlLD = -1
     For Each v In NewLinedefs
          If vertexes(linedefs(v).V2).x > vertexes(linedefs(v).V1).x Then
               ControlLD = v
               Exit For
          End If
     Next v
     
     If ControlLD < 0 Then
          ' Shouldn't happen.
          MsgBox "Unable to find suitable control linedef.", vbCritical
          Exit Sub
     End If
     
     With linedefs(ControlLD)
          vertexes(.V2).x = vertexes(.V1).x + Speed
          vertexes(.V2).y = vertexes(.V1).y + Sequence
          .effect = LT_ZOOMTUBEPARAMS
          .Tag = NewTag
     End With
     


     ' And finally, we get to the waypoints.
     Indices = AllWaypoints.Items()
     CurrentWaypoint = 0
     For Each v In Indices
          things(v).angle = (CurrentWaypoint And &HFF) Or (Sequence * &H100)
          CurrentWaypoint = CurrentWaypoint + 1
     Next v
          
End Sub
