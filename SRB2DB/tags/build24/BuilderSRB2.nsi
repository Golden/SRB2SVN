Page license
Page components
Page directory
Page instfiles
UninstPage uninstConfirm
UninstPage instfiles

Name "SRB2 Doom Builder r24"
OutFile "builder_srb2-setup-r24.exe"
LicenseData "GNU_GPL.txt"
XPStyle on
SetCompressor /SOLID lzma

Section "SRB2 Doom Builder"

	# Put these files in the installation directory.
	SetOutPath $INSTDIR
	
	# These files will be overwritten if they already exist.
	File "BuilderSRB2.exe"
	File "BuilderSRB2Lib.dll"
	
	File "FOFs.cfg"
	File "Shortcuts.cfg"
	File "SOC.cfg"
	File "SRB2ex.cfg"
	File "Font.fnt"
	File "*.tga"
	File "*.bmp"
	File "changelog.txt"
	File "GNU_GPL.txt"
	File "readme.txt"
	
	# Only install the following if they don't already exist.
	SetOverwrite off
	File "Builder.cfg"
	File "ColourSchemes.cfg"
	File "FOFPresets.cfg"
	File "Parameters.cfg"
SectionEnd

Section "ZenNode"
	SetOverwrite on
	File "ZenNode.exe"
SectionEnd
