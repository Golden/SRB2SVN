SRB2 Doom Builder
-----------------

SRB2 Doom Builder is a modification of Pascal vd Heiden's excellent Doom Builder
(www.doombuilder.com), designed to cater for the adaptations made to the Doom
engine by Sonic Robo Blast 2, as well as offering some more general
enhancements.

See the project's homepage at http://homepages.inf.ed.ac.uk/s0569864/ for more.

--
Gregor "Oogaland" Dick
