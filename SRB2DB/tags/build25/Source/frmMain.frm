VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form frmMain 
   Caption         =   "SRB2 Doom Builder"
   ClientHeight    =   7650
   ClientLeft      =   165
   ClientTop       =   450
   ClientWidth     =   11115
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   HasDC           =   0   'False
   Icon            =   "frmMain.frx":0000
   KeyPreview      =   -1  'True
   NegotiateMenus  =   0   'False
   ScaleHeight     =   510
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   741
   Begin VB.Timer tmrStupidToolbarBugWorkaround 
      Enabled         =   0   'False
      Interval        =   100
      Left            =   3120
      Top             =   2760
   End
   Begin VB.Timer tmrAutoSave 
      Interval        =   60000
      Left            =   2640
      Top             =   2760
   End
   Begin VB.Timer tmr3DRedraw 
      Enabled         =   0   'False
      Interval        =   1
      Left            =   2160
      Top             =   2760
   End
   Begin VB.Timer tmrTerminate 
      Enabled         =   0   'False
      Interval        =   1
      Left            =   1680
      Top             =   2760
   End
   Begin VB.PictureBox picMask 
      AutoRedraw      =   -1  'True
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   0  'None
      CausesValidation=   0   'False
      ClipControls    =   0   'False
      ForeColor       =   &H00000000&
      Height          =   375
      Left            =   1125
      ScaleHeight     =   25
      ScaleMode       =   3  'Pixel
      ScaleWidth      =   50
      TabIndex        =   157
      TabStop         =   0   'False
      Top             =   2115
      Visible         =   0   'False
      Width           =   750
   End
   Begin MSComctlLib.ImageList imglstToolbar 
      Left            =   8430
      Top             =   480
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   38
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":1CFA
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":2294
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":282E
            Key             =   ""
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":2DC8
            Key             =   ""
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":3362
            Key             =   ""
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":38FC
            Key             =   ""
         EndProperty
         BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":3E96
            Key             =   ""
         EndProperty
         BeginProperty ListImage8 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":4430
            Key             =   ""
         EndProperty
         BeginProperty ListImage9 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":49CA
            Key             =   ""
         EndProperty
         BeginProperty ListImage10 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":4F64
            Key             =   ""
         EndProperty
         BeginProperty ListImage11 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":54FE
            Key             =   ""
         EndProperty
         BeginProperty ListImage12 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":5A98
            Key             =   ""
         EndProperty
         BeginProperty ListImage13 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":5E32
            Key             =   ""
         EndProperty
         BeginProperty ListImage14 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":61CC
            Key             =   ""
         EndProperty
         BeginProperty ListImage15 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":6766
            Key             =   ""
         EndProperty
         BeginProperty ListImage16 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":6D00
            Key             =   ""
         EndProperty
         BeginProperty ListImage17 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":729A
            Key             =   ""
         EndProperty
         BeginProperty ListImage18 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":7834
            Key             =   ""
         EndProperty
         BeginProperty ListImage19 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":7DCE
            Key             =   ""
         EndProperty
         BeginProperty ListImage20 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":8368
            Key             =   ""
         EndProperty
         BeginProperty ListImage21 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":8902
            Key             =   ""
         EndProperty
         BeginProperty ListImage22 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":8E9C
            Key             =   ""
         EndProperty
         BeginProperty ListImage23 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":9436
            Key             =   ""
         EndProperty
         BeginProperty ListImage24 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":99D0
            Key             =   ""
         EndProperty
         BeginProperty ListImage25 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":9F6A
            Key             =   ""
         EndProperty
         BeginProperty ListImage26 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":A504
            Key             =   ""
         EndProperty
         BeginProperty ListImage27 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":AA9E
            Key             =   ""
         EndProperty
         BeginProperty ListImage28 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":B038
            Key             =   ""
         EndProperty
         BeginProperty ListImage29 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":B5D2
            Key             =   ""
         EndProperty
         BeginProperty ListImage30 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":BB6C
            Key             =   ""
         EndProperty
         BeginProperty ListImage31 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":C106
            Key             =   ""
         EndProperty
         BeginProperty ListImage32 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":C6A0
            Key             =   ""
         EndProperty
         BeginProperty ListImage33 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":CC3A
            Key             =   ""
         EndProperty
         BeginProperty ListImage34 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":D1D4
            Key             =   ""
         EndProperty
         BeginProperty ListImage35 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":D76E
            Key             =   ""
         EndProperty
         BeginProperty ListImage36 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":DD08
            Key             =   ""
         EndProperty
         BeginProperty ListImage37 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":E2A2
            Key             =   ""
         EndProperty
         BeginProperty ListImage38 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":E3FC
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin VB.PictureBox picMap 
      BackColor       =   &H8000000C&
      CausesValidation=   0   'False
      ClipControls    =   0   'False
      FillColor       =   &H00FFFFFF&
      ForeColor       =   &H00FFFFFF&
      HasDC           =   0   'False
      Height          =   2145
      Left            =   5025
      ScaleHeight     =   139
      ScaleMode       =   3  'Pixel
      ScaleWidth      =   263
      TabIndex        =   1
      TabStop         =   0   'False
      Top             =   3555
      Width           =   4005
   End
   Begin VB.PictureBox picSBar 
      Align           =   4  'Align Right
      BorderStyle     =   0  'None
      CausesValidation=   0   'False
      ClipControls    =   0   'False
      HasDC           =   0   'False
      Height          =   5400
      Left            =   9090
      ScaleHeight     =   360
      ScaleMode       =   3  'Pixel
      ScaleWidth      =   135
      TabIndex        =   62
      TabStop         =   0   'False
      Top             =   360
      Visible         =   0   'False
      Width           =   2025
      Begin VB.Frame fraSThingPreview 
         Caption         =   " Sprite "
         Height          =   1590
         Left            =   0
         TabIndex        =   154
         Top             =   1950
         Visible         =   0   'False
         Width           =   1980
         Begin VB.PictureBox picSThing 
            BackColor       =   &H8000000C&
            CausesValidation=   0   'False
            ClipControls    =   0   'False
            HasDC           =   0   'False
            Height          =   1020
            Left            =   480
            ScaleHeight     =   64
            ScaleMode       =   3  'Pixel
            ScaleWidth      =   64
            TabIndex        =   155
            TabStop         =   0   'False
            ToolTipText     =   "Sector Floor Texture"
            Top             =   240
            Width           =   1020
            Begin VB.Image imgSThing 
               Height          =   960
               Left            =   0
               Stretch         =   -1  'True
               Top             =   0
               Width           =   960
            End
         End
         Begin VB.Label lblSThing 
            Alignment       =   2  'Center
            Caption         =   "STARTAN3"
            Height          =   210
            Left            =   480
            TabIndex        =   156
            Top             =   1260
            UseMnemonic     =   0   'False
            Width           =   1020
         End
      End
      Begin VB.CommandButton cmdToggleSBar 
         Caption         =   "4"
         BeginProperty Font 
            Name            =   "Marlett"
            Size            =   9.75
            Charset         =   2
            Weight          =   500
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   0
         TabIndex        =   147
         TabStop         =   0   'False
         Tag             =   "0"
         Top             =   300
         Width           =   270
      End
      Begin VB.Frame fraSThing 
         Caption         =   " Thing 0 "
         Height          =   1905
         Left            =   0
         TabIndex        =   110
         Top             =   0
         Visible         =   0   'False
         Width           =   1980
         Begin VB.Label lblSThingTag 
            AutoSize        =   -1  'True
            Caption         =   "0"
            Height          =   210
            Left            =   900
            TabIndex        =   144
            Top             =   1260
            Width           =   90
         End
         Begin VB.Label lblLabel 
            AutoSize        =   -1  'True
            Caption         =   "Tag:"
            Height          =   210
            Index           =   43
            Left            =   495
            TabIndex        =   143
            Top             =   1260
            Width           =   315
         End
         Begin VB.Label lblSThingAction 
            Caption         =   "0 - Normal"
            Height          =   210
            Left            =   900
            TabIndex        =   142
            Top             =   540
            UseMnemonic     =   0   'False
            Width           =   3105
         End
         Begin VB.Label lblLabel 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "Action:"
            Height          =   210
            Index           =   42
            Left            =   300
            TabIndex        =   141
            Top             =   540
            Width           =   510
         End
         Begin VB.Label lblSThingType 
            Caption         =   "0 - Normal"
            Height          =   210
            Left            =   900
            TabIndex        =   118
            Top             =   300
            UseMnemonic     =   0   'False
            Width           =   1005
         End
         Begin VB.Label lblLabel 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "Angle:"
            Height          =   210
            Index           =   25
            Left            =   345
            TabIndex        =   117
            Top             =   780
            UseMnemonic     =   0   'False
            Width           =   465
         End
         Begin VB.Label lblSThingAngle 
            Caption         =   "0"
            Height          =   210
            Left            =   900
            TabIndex        =   116
            Top             =   780
            UseMnemonic     =   0   'False
            Width           =   1005
         End
         Begin VB.Label lblLabel 
            AutoSize        =   -1  'True
            Caption         =   "Flags:"
            Height          =   210
            Index           =   31
            Left            =   375
            TabIndex        =   115
            Top             =   1020
            Width           =   435
         End
         Begin VB.Label lblLabel 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "Type:"
            Height          =   210
            Index           =   21
            Left            =   405
            TabIndex        =   114
            Top             =   300
            UseMnemonic     =   0   'False
            Width           =   405
         End
         Begin VB.Label lblSThingFlags 
            AutoSize        =   -1  'True
            Caption         =   "0"
            Height          =   210
            Left            =   900
            TabIndex        =   113
            Top             =   1020
            UseMnemonic     =   0   'False
            Width           =   90
         End
         Begin VB.Label lblLabel 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "X,Y,Z:"
            Height          =   210
            Index           =   17
            Left            =   345
            TabIndex        =   112
            Top             =   1500
            UseMnemonic     =   0   'False
            Width           =   465
         End
         Begin VB.Label lblSThingXY 
            AutoSize        =   -1  'True
            Caption         =   "0, 0"
            Height          =   210
            Left            =   915
            TabIndex        =   111
            Top             =   1500
            UseMnemonic     =   0   'False
            Width           =   270
         End
      End
      Begin VB.Frame fraSBackSidedef 
         Caption         =   " Back Side "
         Height          =   4170
         Left            =   0
         TabIndex        =   84
         Top             =   6345
         Width           =   1980
         Begin VB.PictureBox picSS2Lower 
            BackColor       =   &H8000000C&
            CausesValidation=   0   'False
            ClipControls    =   0   'False
            HasDC           =   0   'False
            Height          =   1020
            Left            =   480
            MouseIcon       =   "frmMain.frx":E996
            ScaleHeight     =   64
            ScaleMode       =   3  'Pixel
            ScaleWidth      =   64
            TabIndex        =   87
            TabStop         =   0   'False
            ToolTipText     =   "Back Side Lower Texture"
            Top             =   2805
            Width           =   1020
            Begin VB.Image imgSS2Lower 
               Height          =   960
               Left            =   0
               MouseIcon       =   "frmMain.frx":ECA0
               MousePointer    =   99  'Custom
               Stretch         =   -1  'True
               Top             =   0
               Width           =   960
            End
         End
         Begin VB.PictureBox picSS2Middle 
            BackColor       =   &H8000000C&
            CausesValidation=   0   'False
            ClipControls    =   0   'False
            HasDC           =   0   'False
            Height          =   1020
            Left            =   480
            MouseIcon       =   "frmMain.frx":EFAA
            ScaleHeight     =   64
            ScaleMode       =   3  'Pixel
            ScaleWidth      =   64
            TabIndex        =   86
            TabStop         =   0   'False
            ToolTipText     =   "Back Side Middle Texture"
            Top             =   1530
            Width           =   1020
            Begin VB.Image imgSS2Middle 
               Height          =   960
               Left            =   0
               MouseIcon       =   "frmMain.frx":F2B4
               MousePointer    =   99  'Custom
               Stretch         =   -1  'True
               Top             =   0
               Width           =   960
            End
         End
         Begin VB.PictureBox picSS2Upper 
            BackColor       =   &H8000000C&
            CausesValidation=   0   'False
            ClipControls    =   0   'False
            HasDC           =   0   'False
            Height          =   1020
            Left            =   480
            MouseIcon       =   "frmMain.frx":F5BE
            ScaleHeight     =   64
            ScaleMode       =   3  'Pixel
            ScaleWidth      =   64
            TabIndex        =   85
            TabStop         =   0   'False
            ToolTipText     =   "Back Side Upper Texture"
            Top             =   240
            Width           =   1020
            Begin VB.Image imgSS2Upper 
               Height          =   960
               Left            =   0
               MouseIcon       =   "frmMain.frx":F8C8
               MousePointer    =   99  'Custom
               Stretch         =   -1  'True
               Top             =   0
               Width           =   960
            End
         End
         Begin VB.Label lblSS2Lower 
            Alignment       =   2  'Center
            Caption         =   "STARTAN3"
            Height          =   210
            Left            =   480
            TabIndex        =   90
            Top             =   3825
            UseMnemonic     =   0   'False
            Width           =   1020
         End
         Begin VB.Label lblSS2Middle 
            Alignment       =   2  'Center
            Caption         =   "STARTAN3"
            Height          =   210
            Left            =   480
            TabIndex        =   89
            Top             =   2550
            UseMnemonic     =   0   'False
            Width           =   1020
         End
         Begin VB.Label lblSS2Upper 
            Alignment       =   2  'Center
            Caption         =   "STARTAN3"
            Height          =   210
            Left            =   480
            TabIndex        =   88
            Top             =   1260
            UseMnemonic     =   0   'False
            Width           =   1020
         End
      End
      Begin VB.Frame fraSLinedef 
         Caption         =   " Linedef 0 "
         Height          =   2100
         Left            =   0
         TabIndex        =   66
         Top             =   0
         Visible         =   0   'False
         Width           =   1980
         Begin VB.Label lblSS2Sector 
            AutoSize        =   -1  'True
            Caption         =   "0"
            Height          =   210
            Left            =   900
            TabIndex        =   127
            Top             =   1770
            UseMnemonic     =   0   'False
            Width           =   90
         End
         Begin VB.Label lblLabel 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "B Sector:"
            Height          =   210
            Index           =   12
            Left            =   135
            TabIndex        =   126
            Top             =   1770
            Width           =   675
         End
         Begin VB.Label lblSS1Sector 
            AutoSize        =   -1  'True
            Caption         =   "0"
            Height          =   210
            Left            =   900
            TabIndex        =   125
            Top             =   1530
            UseMnemonic     =   0   'False
            Width           =   90
         End
         Begin VB.Label lblLabel 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "F Sector:"
            Height          =   210
            Index           =   32
            Left            =   150
            TabIndex        =   124
            Top             =   1530
            Width           =   660
         End
         Begin VB.Label lblSLinedefTag 
            AutoSize        =   -1  'True
            Caption         =   "0"
            Height          =   210
            Left            =   900
            TabIndex        =   76
            Top             =   795
            UseMnemonic     =   0   'False
            Width           =   90
         End
         Begin VB.Label lblLabel 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "Tag:"
            Height          =   210
            Index           =   27
            Left            =   480
            TabIndex        =   75
            Top             =   795
            UseMnemonic     =   0   'False
            Width           =   315
         End
         Begin VB.Label lblLabel 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "Action:"
            Height          =   210
            Index           =   11
            Left            =   300
            TabIndex        =   74
            Top             =   300
            UseMnemonic     =   0   'False
            Width           =   510
         End
         Begin VB.Label lblSLinedefLength 
            AutoSize        =   -1  'True
            Caption         =   "0"
            Height          =   210
            Left            =   900
            TabIndex        =   73
            Top             =   540
            UseMnemonic     =   0   'False
            Width           =   90
         End
         Begin VB.Label lblLabel 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "Length:"
            Height          =   210
            Index           =   18
            Left            =   270
            TabIndex        =   72
            Top             =   540
            UseMnemonic     =   0   'False
            Width           =   540
         End
         Begin VB.Label lblSLinedefType 
            Caption         =   "0 - Normal"
            Height          =   210
            Left            =   900
            TabIndex        =   71
            Top             =   300
            UseMnemonic     =   0   'False
            Width           =   1000
         End
         Begin VB.Label lblLabel 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "F Height:"
            Height          =   210
            Index           =   15
            Left            =   180
            TabIndex        =   70
            Top             =   1050
            Width           =   630
         End
         Begin VB.Label lblSS1Height 
            AutoSize        =   -1  'True
            Caption         =   "0"
            Height          =   210
            Left            =   900
            TabIndex        =   69
            Top             =   1050
            UseMnemonic     =   0   'False
            Width           =   90
         End
         Begin VB.Label lblLabel 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "B Height:"
            Height          =   210
            Index           =   13
            Left            =   165
            TabIndex        =   68
            Top             =   1290
            Width           =   645
         End
         Begin VB.Label lblSS2height 
            AutoSize        =   -1  'True
            Caption         =   "0"
            Height          =   210
            Left            =   900
            TabIndex        =   67
            Top             =   1290
            UseMnemonic     =   0   'False
            Width           =   90
         End
      End
      Begin VB.Frame fraSVertex 
         Caption         =   " Vertex 0 "
         Height          =   765
         Left            =   0
         TabIndex        =   63
         Top             =   0
         Width           =   1980
         Begin VB.Label lblSVertexXY 
            AutoSize        =   -1  'True
            Caption         =   "0, 0"
            Height          =   210
            Left            =   930
            TabIndex        =   65
            Top             =   330
            UseMnemonic     =   0   'False
            Width           =   270
         End
         Begin VB.Label lblLabel 
            AutoSize        =   -1  'True
            Caption         =   "X,Y:"
            Height          =   210
            Index           =   14
            Left            =   510
            TabIndex        =   64
            Top             =   330
            UseMnemonic     =   0   'False
            Width           =   315
         End
      End
      Begin VB.Frame fraSSectorFloor 
         Caption         =   " Floor "
         Height          =   1590
         Left            =   0
         TabIndex        =   107
         Top             =   3525
         Visible         =   0   'False
         Width           =   1980
         Begin VB.PictureBox picSFloor 
            BackColor       =   &H8000000C&
            CausesValidation=   0   'False
            ClipControls    =   0   'False
            HasDC           =   0   'False
            Height          =   1020
            Left            =   480
            MouseIcon       =   "frmMain.frx":FBD2
            ScaleHeight     =   64
            ScaleMode       =   3  'Pixel
            ScaleWidth      =   64
            TabIndex        =   108
            TabStop         =   0   'False
            ToolTipText     =   "Sector Ceiling Texture"
            Top             =   240
            Width           =   1020
            Begin VB.Image imgSFloor 
               Height          =   960
               Left            =   0
               MouseIcon       =   "frmMain.frx":FEDC
               MousePointer    =   99  'Custom
               Stretch         =   -1  'True
               Top             =   0
               Width           =   960
            End
         End
         Begin VB.Label lblSFloor 
            Alignment       =   2  'Center
            Caption         =   "STARTAN3"
            Height          =   210
            Left            =   480
            TabIndex        =   109
            Top             =   1260
            UseMnemonic     =   0   'False
            Width           =   1020
         End
      End
      Begin VB.Frame fraSSectorCeiling 
         Caption         =   " Ceiling "
         Height          =   1590
         Left            =   0
         TabIndex        =   104
         Top             =   1905
         Visible         =   0   'False
         Width           =   1980
         Begin VB.PictureBox picSCeiling 
            BackColor       =   &H8000000C&
            CausesValidation=   0   'False
            ClipControls    =   0   'False
            HasDC           =   0   'False
            Height          =   1020
            Left            =   480
            MouseIcon       =   "frmMain.frx":101E6
            ScaleHeight     =   64
            ScaleMode       =   3  'Pixel
            ScaleWidth      =   64
            TabIndex        =   105
            TabStop         =   0   'False
            ToolTipText     =   "Sector Floor Texture"
            Top             =   240
            Width           =   1020
            Begin VB.Image imgSCeiling 
               Height          =   960
               Left            =   0
               MouseIcon       =   "frmMain.frx":104F0
               MousePointer    =   99  'Custom
               Stretch         =   -1  'True
               Top             =   0
               Width           =   960
            End
         End
         Begin VB.Label lblSCeiling 
            Alignment       =   2  'Center
            Caption         =   "STARTAN3"
            Height          =   210
            Left            =   480
            TabIndex        =   106
            Top             =   1260
            UseMnemonic     =   0   'False
            Width           =   1020
         End
      End
      Begin VB.Frame fraSSector 
         Caption         =   " Sector 0 "
         Height          =   1875
         Left            =   0
         TabIndex        =   91
         Top             =   0
         Visible         =   0   'False
         Width           =   1980
         Begin VB.Label lblSSectorType 
            Caption         =   "0 - Normal"
            Height          =   210
            Left            =   900
            TabIndex        =   103
            Top             =   300
            UseMnemonic     =   0   'False
            Width           =   1005
         End
         Begin VB.Label lblLabel 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "Ceiling:"
            Height          =   210
            Index           =   22
            Left            =   300
            TabIndex        =   102
            Top             =   540
            UseMnemonic     =   0   'False
            Width           =   510
         End
         Begin VB.Label lblSSectorCeiling 
            AutoSize        =   -1  'True
            Caption         =   "0"
            Height          =   210
            Left            =   900
            TabIndex        =   101
            Top             =   540
            UseMnemonic     =   0   'False
            Width           =   90
         End
         Begin VB.Label lblLabel 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "Floor:"
            Height          =   210
            Index           =   23
            Left            =   405
            TabIndex        =   100
            Top             =   780
            Width           =   405
         End
         Begin VB.Label lblLabel 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "Effect:"
            Height          =   210
            Index           =   30
            Left            =   330
            TabIndex        =   99
            Top             =   300
            UseMnemonic     =   0   'False
            Width           =   480
         End
         Begin VB.Label lblSSectorFloor 
            AutoSize        =   -1  'True
            Caption         =   "0"
            Height          =   210
            Left            =   900
            TabIndex        =   98
            Top             =   780
            UseMnemonic     =   0   'False
            Width           =   90
         End
         Begin VB.Label lblLabel 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "Tag:"
            Height          =   210
            Index           =   26
            Left            =   480
            TabIndex        =   97
            Top             =   1020
            UseMnemonic     =   0   'False
            Width           =   315
         End
         Begin VB.Label lblSSectorTag 
            AutoSize        =   -1  'True
            Caption         =   "0"
            Height          =   210
            Left            =   900
            TabIndex        =   96
            Top             =   1020
            UseMnemonic     =   0   'False
            Width           =   90
         End
         Begin VB.Label lblLabel 
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "Height:"
            Height          =   210
            Index           =   19
            Left            =   315
            TabIndex        =   95
            Top             =   1275
            UseMnemonic     =   0   'False
            Width           =   495
         End
         Begin VB.Label lblSSectorHeight 
            AutoSize        =   -1  'True
            Caption         =   "0"
            Height          =   210
            Left            =   900
            TabIndex        =   94
            Top             =   1275
            UseMnemonic     =   0   'False
            Width           =   90
         End
         Begin VB.Label lblLabel 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "Light:"
            Height          =   210
            Index           =   16
            Left            =   420
            TabIndex        =   93
            Top             =   1515
            UseMnemonic     =   0   'False
            Width           =   390
         End
         Begin VB.Label lblSSectorLight 
            AutoSize        =   -1  'True
            Caption         =   "0"
            Height          =   210
            Left            =   900
            TabIndex        =   92
            Top             =   1515
            UseMnemonic     =   0   'False
            Width           =   90
         End
      End
      Begin VB.Frame fraSFrontSidedef 
         Caption         =   " Front Side "
         Height          =   4170
         Left            =   0
         TabIndex        =   77
         Top             =   2145
         Width           =   1980
         Begin VB.PictureBox picSS1Upper 
            BackColor       =   &H8000000C&
            CausesValidation=   0   'False
            ClipControls    =   0   'False
            HasDC           =   0   'False
            Height          =   1020
            Left            =   480
            MouseIcon       =   "frmMain.frx":107FA
            ScaleHeight     =   64
            ScaleMode       =   3  'Pixel
            ScaleWidth      =   64
            TabIndex        =   80
            TabStop         =   0   'False
            ToolTipText     =   "Front Side Upper Texture"
            Top             =   240
            Width           =   1020
            Begin VB.Image imgSS1Upper 
               Height          =   960
               Left            =   0
               MouseIcon       =   "frmMain.frx":10B04
               MousePointer    =   99  'Custom
               Stretch         =   -1  'True
               Top             =   0
               Width           =   960
            End
         End
         Begin VB.PictureBox picSS1Middle 
            BackColor       =   &H8000000C&
            CausesValidation=   0   'False
            ClipControls    =   0   'False
            HasDC           =   0   'False
            Height          =   1020
            Left            =   480
            MouseIcon       =   "frmMain.frx":10E0E
            ScaleHeight     =   64
            ScaleMode       =   3  'Pixel
            ScaleWidth      =   64
            TabIndex        =   79
            TabStop         =   0   'False
            ToolTipText     =   "Front Side Middle Texture"
            Top             =   1530
            Width           =   1020
            Begin VB.Image imgSS1Middle 
               Height          =   960
               Left            =   0
               MouseIcon       =   "frmMain.frx":11118
               MousePointer    =   99  'Custom
               Stretch         =   -1  'True
               Top             =   0
               Width           =   960
            End
         End
         Begin VB.PictureBox picSS1Lower 
            BackColor       =   &H8000000C&
            CausesValidation=   0   'False
            ClipControls    =   0   'False
            HasDC           =   0   'False
            Height          =   1020
            Left            =   480
            MouseIcon       =   "frmMain.frx":11422
            ScaleHeight     =   64
            ScaleMode       =   3  'Pixel
            ScaleWidth      =   64
            TabIndex        =   78
            TabStop         =   0   'False
            ToolTipText     =   "Front Side Lower Texture"
            Top             =   2820
            Width           =   1020
            Begin VB.Image imgSS1Lower 
               Height          =   960
               Left            =   0
               MouseIcon       =   "frmMain.frx":1172C
               MousePointer    =   99  'Custom
               Stretch         =   -1  'True
               Top             =   0
               Width           =   960
            End
         End
         Begin VB.Label lblSS1Upper 
            Alignment       =   2  'Center
            Caption         =   "STARTAN3"
            Height          =   210
            Left            =   480
            TabIndex        =   83
            Top             =   1260
            UseMnemonic     =   0   'False
            Width           =   1020
         End
         Begin VB.Label lblSS1Middle 
            Alignment       =   2  'Center
            Caption         =   "STARTAN3"
            Height          =   210
            Left            =   480
            TabIndex        =   82
            Top             =   2550
            UseMnemonic     =   0   'False
            Width           =   1020
         End
         Begin VB.Label lblSS1Lower 
            Alignment       =   2  'Center
            Caption         =   "STARTAN3"
            Height          =   210
            Left            =   480
            TabIndex        =   81
            Top             =   3840
            UseMnemonic     =   0   'False
            Width           =   1020
         End
      End
   End
   Begin VB.Timer tmrAutoScroll 
      Enabled         =   0   'False
      Interval        =   50
      Left            =   1200
      Top             =   2760
   End
   Begin VB.PictureBox picTexture 
      AutoRedraw      =   -1  'True
      BackColor       =   &H8000000C&
      BorderStyle     =   0  'None
      CausesValidation=   0   'False
      ClipControls    =   0   'False
      Height          =   375
      Left            =   240
      ScaleHeight     =   25
      ScaleMode       =   3  'Pixel
      ScaleWidth      =   50
      TabIndex        =   0
      TabStop         =   0   'False
      Top             =   2115
      Visible         =   0   'False
      Width           =   750
   End
   Begin VB.PictureBox picThings 
      AutoSize        =   -1  'True
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   0  'None
      CausesValidation=   0   'False
      ClipControls    =   0   'False
      FontTransparent =   0   'False
      HasDC           =   0   'False
      Height          =   285
      Index           =   3
      Left            =   225
      Picture         =   "frmMain.frx":11A36
      ScaleHeight     =   19
      ScaleMode       =   3  'Pixel
      ScaleWidth      =   192
      TabIndex        =   45
      TabStop         =   0   'False
      Top             =   1560
      Visible         =   0   'False
      Width           =   2880
   End
   Begin VB.PictureBox picThings 
      AutoSize        =   -1  'True
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   0  'None
      CausesValidation=   0   'False
      ClipControls    =   0   'False
      FontTransparent =   0   'False
      HasDC           =   0   'False
      Height          =   195
      Index           =   2
      Left            =   225
      Picture         =   "frmMain.frx":12CB8
      ScaleHeight     =   13
      ScaleMode       =   3  'Pixel
      ScaleWidth      =   132
      TabIndex        =   44
      TabStop         =   0   'False
      Top             =   1230
      Visible         =   0   'False
      Width           =   1980
   End
   Begin VB.PictureBox picThings 
      AutoSize        =   -1  'True
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   0  'None
      CausesValidation=   0   'False
      ClipControls    =   0   'False
      FontTransparent =   0   'False
      HasDC           =   0   'False
      Height          =   105
      Index           =   1
      Left            =   225
      Picture         =   "frmMain.frx":137AE
      ScaleHeight     =   7
      ScaleMode       =   3  'Pixel
      ScaleWidth      =   72
      TabIndex        =   43
      TabStop         =   0   'False
      Top             =   990
      Visible         =   0   'False
      Width           =   1080
   End
   Begin VB.PictureBox picThings 
      AutoSize        =   -1  'True
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   0  'None
      CausesValidation=   0   'False
      ClipControls    =   0   'False
      FontTransparent =   0   'False
      HasDC           =   0   'False
      Height          =   45
      Index           =   0
      Left            =   225
      Picture         =   "frmMain.frx":13DE8
      ScaleHeight     =   3
      ScaleMode       =   3  'Pixel
      ScaleWidth      =   32
      TabIndex        =   42
      TabStop         =   0   'False
      Top             =   840
      Visible         =   0   'False
      Width           =   480
   End
   Begin VB.Timer tmrMouseTimeout 
      Enabled         =   0   'False
      Interval        =   10
      Left            =   240
      Top             =   2760
   End
   Begin VB.Timer tmrMouseOutside 
      Interval        =   107
      Left            =   720
      Top             =   2760
   End
   Begin VB.PictureBox picNumbers 
      AutoSize        =   -1  'True
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   0  'None
      CausesValidation=   0   'False
      ClipControls    =   0   'False
      FontTransparent =   0   'False
      HasDC           =   0   'False
      Height          =   135
      Left            =   240
      Picture         =   "frmMain.frx":1428A
      ScaleHeight     =   9
      ScaleMode       =   3  'Pixel
      ScaleWidth      =   60
      TabIndex        =   5
      TabStop         =   0   'False
      Top             =   600
      Visible         =   0   'False
      Width           =   900
   End
   Begin MSComctlLib.Toolbar tlbToolbar 
      Align           =   1  'Align Top
      Height          =   360
      Left            =   0
      TabIndex        =   4
      Top             =   0
      Width           =   11115
      _ExtentX        =   19606
      _ExtentY        =   635
      ButtonWidth     =   609
      ButtonHeight    =   582
      Wrappable       =   0   'False
      Appearance      =   1
      Style           =   1
      ImageList       =   "imglstToolbar"
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   43
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "FileNewMap"
            Description     =   "New Map..."
            Object.ToolTipText     =   "New Map..."
            ImageIndex      =   1
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "FileOpenMap"
            Description     =   "Open Map..."
            Object.ToolTipText     =   "Open Map..."
            ImageIndex      =   2
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "FileSaveMap"
            Description     =   "Save Map"
            Object.ToolTipText     =   "Save Map"
            ImageIndex      =   3
         EndProperty
         BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Description     =   "Separator"
            ImageIndex      =   1
            Style           =   4
         EndProperty
         BeginProperty Button5 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "ModeMove"
            Description     =   "Move Mode"
            Object.ToolTipText     =   "Move Mode"
            ImageIndex      =   4
            Style           =   2
         EndProperty
         BeginProperty Button6 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "ModeVertices"
            Description     =   "Vertices Mode"
            Object.ToolTipText     =   "Vertices Mode"
            ImageIndex      =   5
            Style           =   2
         EndProperty
         BeginProperty Button7 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "ModeLines"
            Description     =   "Lines Mode"
            Object.ToolTipText     =   "Lines Mode"
            ImageIndex      =   6
            Style           =   2
         EndProperty
         BeginProperty Button8 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "ModeSectors"
            Description     =   "Sectors Mode"
            Object.ToolTipText     =   "Sectors Mode"
            ImageIndex      =   7
            Style           =   2
         EndProperty
         BeginProperty Button9 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "ModeThings"
            Description     =   "Things Mode"
            Object.ToolTipText     =   "Things Mode"
            ImageIndex      =   8
            Style           =   2
         EndProperty
         BeginProperty Button10 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "Mode3D"
            Description     =   "3D Mode"
            Object.ToolTipText     =   "3D Mode"
            ImageIndex      =   9
            Style           =   2
         EndProperty
         BeginProperty Button11 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Description     =   "Separator"
            ImageIndex      =   1
            Style           =   4
         EndProperty
         BeginProperty Button12 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "FileBuild"
            Description     =   "Build Nodes"
            Object.ToolTipText     =   "Build Nodes"
            ImageIndex      =   10
         EndProperty
         BeginProperty Button13 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "FileTest"
            Description     =   "Test Map"
            Object.ToolTipText     =   "Test Map"
            ImageIndex      =   11
         EndProperty
         BeginProperty Button14 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Description     =   "Separator"
            ImageIndex      =   1
            Style           =   4
         EndProperty
         BeginProperty Button15 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "EditUndo"
            Description     =   "Undo"
            Object.ToolTipText     =   "Undo"
            ImageIndex      =   12
         EndProperty
         BeginProperty Button16 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "EditRedo"
            Description     =   "Redo"
            Object.ToolTipText     =   "Redo"
            ImageIndex      =   13
         EndProperty
         BeginProperty Button17 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Description     =   "Separator"
            ImageIndex      =   1
            Style           =   4
         EndProperty
         BeginProperty Button18 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "EditFlipH"
            Description     =   "Flip Selection Horizontally"
            Object.ToolTipText     =   "Flip Selection Horizontally"
            ImageIndex      =   14
         EndProperty
         BeginProperty Button19 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "EditFlipV"
            Description     =   "Flip Selection Vertically"
            Object.ToolTipText     =   "Flip Selection Vertically"
            ImageIndex      =   15
         EndProperty
         BeginProperty Button20 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "EditRotate"
            Description     =   "Rotate Selection..."
            Object.ToolTipText     =   "Rotate Selection..."
            ImageIndex      =   16
         EndProperty
         BeginProperty Button21 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "EditResize"
            Description     =   "Resize Selection..."
            Object.ToolTipText     =   "Resize Selection..."
            ImageIndex      =   29
         EndProperty
         BeginProperty Button22 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Description     =   "Separator"
            ImageIndex      =   1
            Style           =   4
         EndProperty
         BeginProperty Button23 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "EditGrid"
            Description     =   "Grid Settings..."
            Object.ToolTipText     =   "Grid Settings..."
            ImageIndex      =   17
         EndProperty
         BeginProperty Button24 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "EditSnap"
            Description     =   "Snap To Grid"
            Object.ToolTipText     =   "Snap To Grid"
            ImageIndex      =   18
            Style           =   1
            Value           =   1
         EndProperty
         BeginProperty Button25 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "EditStitch"
            Description     =   "Stitch Vertices"
            Object.ToolTipText     =   "Stitch Vertices"
            ImageIndex      =   19
            Style           =   1
            Value           =   1
         EndProperty
         BeginProperty Button26 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "EditCenterView"
            Description     =   "Center View"
            Object.ToolTipText     =   "Center View"
            ImageIndex      =   33
            Object.Width           =   14
         EndProperty
         BeginProperty Button27 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Description     =   "Separator"
            ImageIndex      =   1
            Style           =   4
         EndProperty
         BeginProperty Button28 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "PrefabsInsert"
            Description     =   "Insert Prefab from File..."
            Object.ToolTipText     =   "Insert Prefab from File..."
            ImageIndex      =   20
         EndProperty
         BeginProperty Button29 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "PrefabsInsertPrevious"
            Description     =   "Insert Previous Prefab"
            Object.ToolTipText     =   "Insert Previous Prefab"
            ImageIndex      =   21
         EndProperty
         BeginProperty Button30 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Description     =   "Separator"
            ImageIndex      =   1
            Style           =   4
         EndProperty
         BeginProperty Button31 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "LinesFlip"
            Description     =   "Flip Linedefs"
            Object.ToolTipText     =   "Flip Linedefs"
            ImageIndex      =   22
         EndProperty
         BeginProperty Button32 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "LinesCurve"
            Description     =   "Curve Linedefs..."
            Object.ToolTipText     =   "Curve Linedefs..."
            ImageIndex      =   23
         EndProperty
         BeginProperty Button33 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "LinesSplit"
            Description     =   "Split Linedefs"
            Object.ToolTipText     =   "Split Linedefs"
            ImageIndex      =   36
         EndProperty
         BeginProperty Button34 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Description     =   "Separator"
            ImageIndex      =   1
            Style           =   4
         EndProperty
         BeginProperty Button35 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "SectorsJoin"
            Description     =   "Join Sectors"
            Object.ToolTipText     =   "Join Sectors"
            ImageIndex      =   25
         EndProperty
         BeginProperty Button36 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "SectorsMerge"
            Description     =   "Merge Sectors"
            Object.ToolTipText     =   "Merge Sectors"
            ImageIndex      =   26
         EndProperty
         BeginProperty Button37 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "SectorsGradientBrightness"
            Description     =   "Gradient Brightness"
            Object.ToolTipText     =   "Gradient Brightness"
            ImageIndex      =   30
         EndProperty
         BeginProperty Button38 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "SectorsGradientFloors"
            Description     =   "Gradient Floors"
            Object.ToolTipText     =   "Gradient Floors"
            ImageIndex      =   31
         EndProperty
         BeginProperty Button39 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "SectorsGradientCeilings"
            Description     =   "Gradient Ceilings"
            Object.ToolTipText     =   "Gradient Ceilings"
            ImageIndex      =   32
         EndProperty
         BeginProperty Button40 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "GradientTags"
            Description     =   "Cascade Tags"
            Object.ToolTipText     =   "Cascade Tags"
            ImageIndex      =   37
         EndProperty
         BeginProperty Button41 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "SectorsSetupFOF"
            Description     =   "Setup FOF..."
            Object.ToolTipText     =   "Setup FOF..."
            ImageIndex      =   38
         EndProperty
         BeginProperty Button42 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Description     =   "Separator"
            ImageIndex      =   1
            Style           =   4
         EndProperty
         BeginProperty Button43 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "ThingsFilter"
            Description     =   "Things Filter..."
            Object.ToolTipText     =   "Things Filter..."
            ImageIndex      =   27
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.StatusBar stbStatus 
      Align           =   2  'Align Bottom
      Height          =   300
      Left            =   0
      TabIndex        =   3
      Top             =   7350
      Width           =   11115
      _ExtentX        =   19606
      _ExtentY        =   529
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   11
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            Object.Width           =   2381
            MinWidth        =   2381
            Text            =   "0 vertices"
            TextSave        =   "0 vertices"
            Key             =   "numvertexes"
         EndProperty
         BeginProperty Panel2 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            Object.Width           =   2381
            MinWidth        =   2381
            Text            =   "0 linedefs"
            TextSave        =   "0 linedefs"
            Key             =   "numlinedefs"
         EndProperty
         BeginProperty Panel3 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            Object.Width           =   2381
            MinWidth        =   2381
            Text            =   "0 sidedefs"
            TextSave        =   "0 sidedefs"
            Key             =   "numsidedefs"
         EndProperty
         BeginProperty Panel4 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            Object.Width           =   2381
            MinWidth        =   2381
            Text            =   "0 sectors"
            TextSave        =   "0 sectors"
            Key             =   "numsectors"
         EndProperty
         BeginProperty Panel5 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            Object.Width           =   2381
            MinWidth        =   2381
            Text            =   "0 things"
            TextSave        =   "0 things"
            Key             =   "numthings"
         EndProperty
         BeginProperty Panel6 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            Object.Width           =   2381
            MinWidth        =   2381
            Text            =   "Grid: 64"
            TextSave        =   "Grid: 64"
            Key             =   "gridsize"
         EndProperty
         BeginProperty Panel7 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            Object.Width           =   2381
            MinWidth        =   2381
            Text            =   "Snap: OFF"
            TextSave        =   "Snap: OFF"
            Key             =   "snapmode"
         EndProperty
         BeginProperty Panel8 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            Object.Width           =   2381
            MinWidth        =   2381
            Text            =   "Stitch: OFF"
            TextSave        =   "Stitch: OFF"
            Key             =   "stitchmode"
         EndProperty
         BeginProperty Panel9 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            Object.Width           =   2381
            MinWidth        =   2381
            Text            =   "Zoom: 100%"
            TextSave        =   "Zoom: 100%"
            Key             =   "viewzoom"
         EndProperty
         BeginProperty Panel10 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            Object.Width           =   2381
            MinWidth        =   2381
            Key             =   "mousex"
         EndProperty
         BeginProperty Panel11 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            Object.Width           =   2381
            MinWidth        =   2381
            Key             =   "mousey"
         EndProperty
      EndProperty
   End
   Begin VB.PictureBox picBar 
      Align           =   2  'Align Bottom
      BorderStyle     =   0  'None
      CausesValidation=   0   'False
      ClipControls    =   0   'False
      FontTransparent =   0   'False
      HasDC           =   0   'False
      Height          =   1590
      Left            =   0
      ScaleHeight     =   106
      ScaleMode       =   3  'Pixel
      ScaleWidth      =   741
      TabIndex        =   2
      TabStop         =   0   'False
      Top             =   5760
      Visible         =   0   'False
      Width           =   11115
      Begin VB.Frame fraSector 
         Caption         =   " Sector 0 "
         Height          =   1590
         Left            =   45
         TabIndex        =   30
         Top             =   0
         Visible         =   0   'False
         Width           =   4065
         Begin VB.Label lblSectorLight 
            AutoSize        =   -1  'True
            Caption         =   "0"
            Height          =   210
            Left            =   2670
            TabIndex        =   49
            Top             =   1230
            UseMnemonic     =   0   'False
            Width           =   90
         End
         Begin VB.Label lblLabel 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "Light:"
            Height          =   210
            Index           =   8
            Left            =   2175
            TabIndex        =   48
            Top             =   1230
            UseMnemonic     =   0   'False
            Width           =   390
         End
         Begin VB.Label lblSectorHeight 
            AutoSize        =   -1  'True
            Caption         =   "0"
            Height          =   210
            Left            =   900
            TabIndex        =   47
            Top             =   1230
            UseMnemonic     =   0   'False
            Width           =   90
         End
         Begin VB.Label lblLabel 
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "Height:"
            Height          =   210
            Index           =   7
            Left            =   300
            TabIndex        =   46
            Top             =   1230
            UseMnemonic     =   0   'False
            Width           =   495
         End
         Begin VB.Label lblSectorTag 
            AutoSize        =   -1  'True
            Caption         =   "0"
            Height          =   210
            Left            =   900
            TabIndex        =   38
            Top             =   990
            UseMnemonic     =   0   'False
            Width           =   90
         End
         Begin VB.Label lblLabel 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "Tag:"
            Height          =   210
            Index           =   6
            Left            =   480
            TabIndex        =   37
            Top             =   990
            UseMnemonic     =   0   'False
            Width           =   315
         End
         Begin VB.Label lblSectorFloor 
            AutoSize        =   -1  'True
            Caption         =   "0"
            Height          =   210
            Left            =   900
            TabIndex        =   36
            Top             =   750
            UseMnemonic     =   0   'False
            Width           =   90
         End
         Begin VB.Label lblLabel 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "Effect:"
            Height          =   210
            Index           =   39
            Left            =   330
            TabIndex        =   35
            Top             =   270
            UseMnemonic     =   0   'False
            Width           =   480
         End
         Begin VB.Label lblLabel 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "Floor:"
            Height          =   210
            Index           =   5
            Left            =   405
            TabIndex        =   34
            Top             =   750
            Width           =   405
         End
         Begin VB.Label lblSectorCeiling 
            AutoSize        =   -1  'True
            Caption         =   "0"
            Height          =   210
            Left            =   900
            TabIndex        =   33
            Top             =   510
            UseMnemonic     =   0   'False
            Width           =   90
         End
         Begin VB.Label lblLabel 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "Ceiling:"
            Height          =   210
            Index           =   4
            Left            =   300
            TabIndex        =   32
            Top             =   510
            UseMnemonic     =   0   'False
            Width           =   510
         End
         Begin VB.Label lblSectorType 
            Caption         =   "0 - Normal"
            Height          =   210
            Left            =   900
            TabIndex        =   31
            Top             =   270
            UseMnemonic     =   0   'False
            Width           =   3105
         End
      End
      Begin VB.CommandButton cmdToggleBar 
         Caption         =   "6"
         BeginProperty Font 
            Name            =   "Marlett"
            Size            =   9.75
            Charset         =   2
            Weight          =   500
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   10605
         TabIndex        =   145
         TabStop         =   0   'False
         Tag             =   "0"
         Top             =   0
         Width           =   300
      End
      Begin VB.Frame fraLinedef 
         Caption         =   " Linedef 0 "
         Height          =   1590
         Left            =   45
         TabIndex        =   23
         Top             =   0
         Visible         =   0   'False
         Width           =   4065
         Begin VB.Label lblS2Y 
            AutoSize        =   -1  'True
            Caption         =   "0"
            Height          =   210
            Left            =   3600
            TabIndex        =   136
            Top             =   750
            UseMnemonic     =   0   'False
            Width           =   90
         End
         Begin VB.Label lblS2X 
            AutoSize        =   -1  'True
            Caption         =   "0"
            Height          =   210
            Left            =   3600
            TabIndex        =   135
            Top             =   510
            UseMnemonic     =   0   'False
            Width           =   90
         End
         Begin VB.Label lblS1Y 
            AutoSize        =   -1  'True
            Caption         =   "0"
            Height          =   210
            Left            =   2520
            TabIndex        =   134
            Top             =   750
            UseMnemonic     =   0   'False
            Width           =   90
         End
         Begin VB.Label lblS1X 
            AutoSize        =   -1  'True
            Caption         =   "0"
            Height          =   210
            Left            =   2520
            TabIndex        =   133
            Top             =   510
            UseMnemonic     =   0   'False
            Width           =   90
         End
         Begin VB.Label lblLabel 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "Back X:"
            Height          =   210
            Index           =   24
            Left            =   2955
            TabIndex        =   132
            Top             =   510
            Width           =   555
         End
         Begin VB.Label lblLabel 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "Front X:"
            Height          =   210
            Index           =   33
            Left            =   1860
            TabIndex        =   131
            Top             =   510
            Width           =   570
         End
         Begin VB.Label lblLabel 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "Back Y:"
            Height          =   210
            Index           =   20
            Left            =   2955
            TabIndex        =   129
            Top             =   750
            Width           =   570
         End
         Begin VB.Label lblLabel 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "Front Y:"
            Height          =   210
            Index           =   29
            Left            =   1860
            TabIndex        =   128
            Top             =   750
            Width           =   585
         End
         Begin VB.Label lblS2Sector 
            AutoSize        =   -1  'True
            Caption         =   "0"
            Height          =   210
            Left            =   2880
            TabIndex        =   123
            Top             =   1005
            UseMnemonic     =   0   'False
            Width           =   90
         End
         Begin VB.Label lblLabel 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "Back Sector:"
            Height          =   210
            Index           =   28
            Left            =   1860
            TabIndex        =   121
            Top             =   1005
            Width           =   930
         End
         Begin VB.Label lblS2height 
            AutoSize        =   -1  'True
            Caption         =   "0"
            Height          =   210
            Left            =   2880
            TabIndex        =   52
            Top             =   1245
            UseMnemonic     =   0   'False
            Width           =   90
         End
         Begin VB.Label lblLinedefType 
            Caption         =   "0 - Normal"
            Height          =   210
            Left            =   1215
            TabIndex        =   29
            Top             =   270
            UseMnemonic     =   0   'False
            Width           =   2790
         End
         Begin VB.Label lblLabel 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "Action:"
            Height          =   210
            Index           =   0
            Left            =   600
            TabIndex        =   26
            Top             =   270
            UseMnemonic     =   0   'False
            Width           =   510
         End
         Begin VB.Label lblS1Height 
            AutoSize        =   -1  'True
            Caption         =   "0"
            Height          =   210
            Left            =   1215
            TabIndex        =   50
            Top             =   1245
            UseMnemonic     =   0   'False
            Width           =   90
         End
         Begin VB.Label lblLabel 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "Length:"
            Height          =   210
            Index           =   1
            Left            =   570
            TabIndex        =   28
            Top             =   510
            UseMnemonic     =   0   'False
            Width           =   540
         End
         Begin VB.Label lblLabel 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "Tag:"
            Height          =   210
            Index           =   38
            Left            =   795
            TabIndex        =   25
            Top             =   750
            UseMnemonic     =   0   'False
            Width           =   315
         End
         Begin VB.Label lblLinedefLength 
            AutoSize        =   -1  'True
            Caption         =   "0"
            Height          =   210
            Left            =   1215
            TabIndex        =   27
            Top             =   510
            UseMnemonic     =   0   'False
            Width           =   90
         End
         Begin VB.Label lblLinedefTag 
            AutoSize        =   -1  'True
            Caption         =   "0"
            Height          =   210
            Left            =   1215
            TabIndex        =   24
            Top             =   750
            UseMnemonic     =   0   'False
            Width           =   90
         End
         Begin VB.Label lblS1Sector 
            AutoSize        =   -1  'True
            Caption         =   "0"
            Height          =   210
            Left            =   1215
            TabIndex        =   122
            Top             =   1005
            UseMnemonic     =   0   'False
            Width           =   90
         End
         Begin VB.Label lblLabel 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "Front Sector:"
            Height          =   210
            Index           =   2
            Left            =   165
            TabIndex        =   120
            Top             =   1005
            Width           =   945
         End
         Begin VB.Label lblLabel 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "Front Height:"
            Height          =   210
            Index           =   9
            Left            =   195
            TabIndex        =   130
            Top             =   1245
            Width           =   915
         End
         Begin VB.Label lblLabel 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "Back Height:"
            Height          =   210
            Index           =   10
            Left            =   1860
            TabIndex        =   51
            Top             =   1245
            Width           =   900
         End
      End
      Begin VB.Frame fraVertex 
         Caption         =   " Vertex 0 "
         Height          =   1590
         Left            =   45
         TabIndex        =   6
         Top             =   0
         Width           =   3705
         Begin VB.Label lblVertexXY 
            AutoSize        =   -1  'True
            Caption         =   "0, 0"
            Height          =   210
            Left            =   1455
            TabIndex        =   8
            Top             =   510
            UseMnemonic     =   0   'False
            Width           =   270
         End
         Begin VB.Label lblLabel 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "Coordinates:"
            Height          =   210
            Index           =   3
            Left            =   360
            TabIndex        =   7
            Top             =   510
            UseMnemonic     =   0   'False
            Width           =   915
         End
      End
      Begin VB.Frame fraSectorFloor 
         Caption         =   " Floor "
         Height          =   1590
         Left            =   5535
         TabIndex        =   39
         Top             =   0
         Visible         =   0   'False
         Width           =   1305
         Begin VB.PictureBox picFloor 
            BackColor       =   &H8000000C&
            CausesValidation=   0   'False
            ClipControls    =   0   'False
            HasDC           =   0   'False
            Height          =   1020
            Left            =   150
            MouseIcon       =   "frmMain.frx":148E8
            ScaleHeight     =   64
            ScaleMode       =   3  'Pixel
            ScaleWidth      =   64
            TabIndex        =   40
            TabStop         =   0   'False
            ToolTipText     =   "Sector Floor Texture"
            Top             =   240
            Width           =   1020
            Begin VB.Image imgFloor 
               Height          =   960
               Left            =   0
               MouseIcon       =   "frmMain.frx":14BF2
               MousePointer    =   99  'Custom
               Stretch         =   -1  'True
               ToolTipText     =   "Sector Floor Texture"
               Top             =   0
               Width           =   960
            End
         End
         Begin VB.Label lblFloor 
            Alignment       =   2  'Center
            Caption         =   "STARTAN3"
            Height          =   210
            Left            =   150
            TabIndex        =   41
            Top             =   1260
            UseMnemonic     =   0   'False
            Width           =   1020
         End
      End
      Begin VB.Frame fraBackSidedef 
         Caption         =   " Back Side "
         Height          =   1590
         Left            =   7695
         TabIndex        =   16
         Top             =   0
         Visible         =   0   'False
         Width           =   3465
         Begin VB.PictureBox picS2Upper 
            BackColor       =   &H8000000C&
            CausesValidation=   0   'False
            ClipControls    =   0   'False
            HasDC           =   0   'False
            Height          =   1020
            Left            =   150
            MouseIcon       =   "frmMain.frx":14EFC
            ScaleHeight     =   64
            ScaleMode       =   3  'Pixel
            ScaleWidth      =   64
            TabIndex        =   19
            TabStop         =   0   'False
            ToolTipText     =   "Back Side Upper Texture"
            Top             =   240
            Width           =   1020
            Begin VB.Image imgS2Upper 
               Height          =   960
               Left            =   0
               MouseIcon       =   "frmMain.frx":15206
               MousePointer    =   99  'Custom
               Stretch         =   -1  'True
               Top             =   0
               Width           =   960
            End
         End
         Begin VB.PictureBox picS2Middle 
            BackColor       =   &H8000000C&
            CausesValidation=   0   'False
            ClipControls    =   0   'False
            HasDC           =   0   'False
            Height          =   1020
            Left            =   1230
            MouseIcon       =   "frmMain.frx":15510
            ScaleHeight     =   64
            ScaleMode       =   3  'Pixel
            ScaleWidth      =   64
            TabIndex        =   18
            TabStop         =   0   'False
            ToolTipText     =   "Back Side Middle Texture"
            Top             =   240
            Width           =   1020
            Begin VB.Image imgS2Middle 
               Height          =   960
               Left            =   0
               MouseIcon       =   "frmMain.frx":1581A
               MousePointer    =   99  'Custom
               Stretch         =   -1  'True
               Top             =   0
               Width           =   960
            End
         End
         Begin VB.PictureBox picS2Lower 
            BackColor       =   &H8000000C&
            CausesValidation=   0   'False
            ClipControls    =   0   'False
            HasDC           =   0   'False
            Height          =   1020
            Left            =   2310
            MouseIcon       =   "frmMain.frx":15B24
            ScaleHeight     =   64
            ScaleMode       =   3  'Pixel
            ScaleWidth      =   64
            TabIndex        =   17
            TabStop         =   0   'False
            ToolTipText     =   "Back Side Lower Texture"
            Top             =   240
            Width           =   1020
            Begin VB.Image imgS2Lower 
               Height          =   960
               Left            =   0
               MouseIcon       =   "frmMain.frx":15E2E
               MousePointer    =   99  'Custom
               Stretch         =   -1  'True
               Top             =   0
               Width           =   960
            End
         End
         Begin VB.Label lblS2Upper 
            Alignment       =   2  'Center
            Caption         =   "STARTAN3"
            Height          =   210
            Left            =   150
            TabIndex        =   22
            Top             =   1260
            UseMnemonic     =   0   'False
            Width           =   1020
         End
         Begin VB.Label lblS2Middle 
            Alignment       =   2  'Center
            Caption         =   "STARTAN3"
            Height          =   210
            Left            =   1230
            TabIndex        =   21
            Top             =   1260
            UseMnemonic     =   0   'False
            Width           =   1020
         End
         Begin VB.Label lblS2Lower 
            Alignment       =   2  'Center
            Caption         =   "STARTAN3"
            Height          =   210
            Left            =   2310
            TabIndex        =   20
            Top             =   1260
            UseMnemonic     =   0   'False
            Width           =   1020
         End
      End
      Begin VB.Frame fraThing 
         Caption         =   " Thing 0 "
         Height          =   1590
         Left            =   45
         TabIndex        =   53
         Top             =   0
         Visible         =   0   'False
         Width           =   4065
         Begin VB.Label lblThingAction 
            Caption         =   "0 - Normal"
            Height          =   210
            Left            =   900
            TabIndex        =   140
            Top             =   510
            UseMnemonic     =   0   'False
            Width           =   3105
         End
         Begin VB.Label lblLabel 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "Action:"
            Height          =   210
            Index           =   41
            Left            =   300
            TabIndex        =   139
            Top             =   510
            Width           =   510
         End
         Begin VB.Label lblThingTag 
            AutoSize        =   -1  'True
            Caption         =   "0"
            Height          =   210
            Left            =   2535
            TabIndex        =   138
            Top             =   750
            Width           =   90
         End
         Begin VB.Label lblLabel 
            AutoSize        =   -1  'True
            Caption         =   "Tag:"
            Height          =   210
            Index           =   40
            Left            =   2130
            TabIndex        =   137
            Top             =   750
            Width           =   315
         End
         Begin VB.Label lblThingXY 
            AutoSize        =   -1  'True
            Caption         =   "0, 0, 0"
            Height          =   210
            Left            =   2565
            TabIndex        =   61
            Top             =   990
            UseMnemonic     =   0   'False
            Width           =   450
         End
         Begin VB.Label lblLabel 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "X,Y,Z:"
            Height          =   210
            Index           =   37
            Left            =   1995
            TabIndex        =   60
            Top             =   990
            UseMnemonic     =   0   'False
            Width           =   465
         End
         Begin VB.Label lblThingFlags 
            AutoSize        =   -1  'True
            Caption         =   "0"
            Height          =   210
            Left            =   900
            TabIndex        =   59
            Top             =   990
            UseMnemonic     =   0   'False
            Width           =   90
         End
         Begin VB.Label lblLabel 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "Type:"
            Height          =   210
            Index           =   34
            Left            =   405
            TabIndex        =   58
            Top             =   270
            UseMnemonic     =   0   'False
            Width           =   405
         End
         Begin VB.Label lblLabel 
            AutoSize        =   -1  'True
            Caption         =   "Flags:"
            Height          =   210
            Index           =   36
            Left            =   375
            TabIndex        =   57
            Top             =   990
            Width           =   435
         End
         Begin VB.Label lblThingAngle 
            Caption         =   "0"
            Height          =   210
            Left            =   900
            TabIndex        =   56
            Top             =   750
            UseMnemonic     =   0   'False
            Width           =   975
         End
         Begin VB.Label lblLabel 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "Angle:"
            Height          =   210
            Index           =   35
            Left            =   345
            TabIndex        =   55
            Top             =   750
            UseMnemonic     =   0   'False
            Width           =   465
         End
         Begin VB.Label lblThingType 
            Caption         =   "0 - Normal"
            Height          =   210
            Left            =   900
            TabIndex        =   54
            Top             =   270
            UseMnemonic     =   0   'False
            Width           =   3105
         End
      End
      Begin VB.Frame fraThingPreview 
         Caption         =   " Sprite "
         Height          =   1590
         Left            =   4170
         TabIndex        =   151
         Top             =   0
         Visible         =   0   'False
         Width           =   1305
         Begin VB.PictureBox picThing 
            BackColor       =   &H8000000C&
            CausesValidation=   0   'False
            ClipControls    =   0   'False
            HasDC           =   0   'False
            Height          =   1020
            Left            =   150
            ScaleHeight     =   64
            ScaleMode       =   3  'Pixel
            ScaleWidth      =   64
            TabIndex        =   152
            TabStop         =   0   'False
            ToolTipText     =   "Thing Sprite Preview"
            Top             =   240
            Width           =   1020
            Begin VB.Image imgThing 
               Height          =   960
               Left            =   0
               Stretch         =   -1  'True
               ToolTipText     =   "Thing Sprite Preview"
               Top             =   0
               Width           =   960
            End
         End
         Begin VB.Label lblThing 
            Alignment       =   2  'Center
            Caption         =   "STARTAN3"
            Height          =   210
            Left            =   150
            TabIndex        =   153
            Top             =   1260
            UseMnemonic     =   0   'False
            Width           =   1020
         End
      End
      Begin VB.Frame fraFrontSidedef 
         Caption         =   " Front Side "
         Height          =   1590
         Left            =   4170
         TabIndex        =   9
         Top             =   0
         Visible         =   0   'False
         Width           =   3465
         Begin VB.PictureBox picS1Lower 
            BackColor       =   &H8000000C&
            CausesValidation=   0   'False
            ClipControls    =   0   'False
            HasDC           =   0   'False
            Height          =   1020
            Left            =   2310
            MouseIcon       =   "frmMain.frx":16138
            ScaleHeight     =   64
            ScaleMode       =   3  'Pixel
            ScaleWidth      =   64
            TabIndex        =   12
            TabStop         =   0   'False
            ToolTipText     =   "Front Side Lower Texture"
            Top             =   240
            Width           =   1020
            Begin VB.Image imgS1Lower 
               Height          =   960
               Left            =   0
               MouseIcon       =   "frmMain.frx":16442
               MousePointer    =   99  'Custom
               Stretch         =   -1  'True
               Top             =   0
               Width           =   960
            End
         End
         Begin VB.PictureBox picS1Middle 
            BackColor       =   &H8000000C&
            CausesValidation=   0   'False
            ClipControls    =   0   'False
            HasDC           =   0   'False
            Height          =   1020
            Left            =   1230
            MouseIcon       =   "frmMain.frx":1674C
            ScaleHeight     =   64
            ScaleMode       =   3  'Pixel
            ScaleWidth      =   64
            TabIndex        =   11
            TabStop         =   0   'False
            ToolTipText     =   "Front Side Middle Texture"
            Top             =   240
            Width           =   1020
            Begin VB.Image imgS1Middle 
               Height          =   960
               Left            =   0
               MouseIcon       =   "frmMain.frx":16A56
               MousePointer    =   99  'Custom
               Stretch         =   -1  'True
               Top             =   0
               Width           =   960
            End
         End
         Begin VB.PictureBox picS1Upper 
            BackColor       =   &H8000000C&
            CausesValidation=   0   'False
            ClipControls    =   0   'False
            HasDC           =   0   'False
            Height          =   1020
            Left            =   150
            MouseIcon       =   "frmMain.frx":16D60
            ScaleHeight     =   64
            ScaleMode       =   3  'Pixel
            ScaleWidth      =   64
            TabIndex        =   10
            TabStop         =   0   'False
            ToolTipText     =   "Front Side Upper Texture"
            Top             =   240
            Width           =   1020
            Begin VB.Image imgS1Upper 
               Height          =   960
               Left            =   0
               MouseIcon       =   "frmMain.frx":1706A
               MousePointer    =   99  'Custom
               Stretch         =   -1  'True
               Top             =   0
               Width           =   960
            End
         End
         Begin VB.Label lblS1Lower 
            Alignment       =   2  'Center
            Caption         =   "STARTAN3"
            Height          =   210
            Left            =   2310
            TabIndex        =   15
            Top             =   1260
            UseMnemonic     =   0   'False
            Width           =   1020
         End
         Begin VB.Label lblS1Middle 
            Alignment       =   2  'Center
            Caption         =   "STARTAN3"
            Height          =   210
            Left            =   1230
            TabIndex        =   14
            Top             =   1260
            UseMnemonic     =   0   'False
            Width           =   1020
         End
         Begin VB.Label lblS1Upper 
            Alignment       =   2  'Center
            Caption         =   "STARTAN3"
            Height          =   210
            Left            =   150
            TabIndex        =   13
            Top             =   1260
            UseMnemonic     =   0   'False
            Width           =   1020
         End
      End
      Begin VB.Frame fraSectorCeiling 
         Caption         =   " Ceiling "
         Height          =   1590
         Left            =   4170
         TabIndex        =   148
         Top             =   0
         Visible         =   0   'False
         Width           =   1305
         Begin VB.PictureBox picCeiling 
            BackColor       =   &H8000000C&
            CausesValidation=   0   'False
            ClipControls    =   0   'False
            HasDC           =   0   'False
            Height          =   1020
            Left            =   150
            MouseIcon       =   "frmMain.frx":17374
            ScaleHeight     =   64
            ScaleMode       =   3  'Pixel
            ScaleWidth      =   64
            TabIndex        =   149
            TabStop         =   0   'False
            ToolTipText     =   "Sector Floor Texture"
            Top             =   240
            Width           =   1020
            Begin VB.Image imgCeiling 
               Height          =   960
               Left            =   0
               MouseIcon       =   "frmMain.frx":1767E
               MousePointer    =   99  'Custom
               Stretch         =   -1  'True
               ToolTipText     =   "Sector Ceiling Texture"
               Top             =   0
               Width           =   960
            End
         End
         Begin VB.Label lblCeiling 
            Alignment       =   2  'Center
            Caption         =   "STARTAN3"
            Height          =   210
            Left            =   150
            TabIndex        =   150
            Top             =   1260
            UseMnemonic     =   0   'False
            Width           =   1020
         End
      End
      Begin VB.Label lblBarText 
         AutoSize        =   -1  'True
         Height          =   210
         Left            =   60
         TabIndex        =   146
         Top             =   15
         UseMnemonic     =   0   'False
         Visible         =   0   'False
         Width           =   480
      End
      Begin VB.Label lblMode 
         AutoSize        =   -1  'True
         Caption         =   "Vertices"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   24
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000011&
         Height          =   555
         Left            =   720
         TabIndex        =   119
         Top             =   540
         Width           =   1905
      End
   End
   Begin VB.Image imgCursor 
      Height          =   480
      Index           =   2
      Left            =   750
      Picture         =   "frmMain.frx":17988
      Top             =   3375
      Visible         =   0   'False
      Width           =   480
   End
   Begin VB.Image imgCursor 
      Height          =   480
      Index           =   1
      Left            =   240
      Picture         =   "frmMain.frx":17C92
      Top             =   3375
      Visible         =   0   'False
      Width           =   480
   End
   Begin VB.Image imgMissingTexture 
      Height          =   960
      Left            =   105
      Picture         =   "frmMain.frx":17F9C
      Top             =   3750
      Visible         =   0   'False
      Width           =   960
   End
   Begin VB.Image imgUnknownFlat 
      Height          =   960
      Left            =   2025
      Picture         =   "frmMain.frx":180B3
      Top             =   3750
      Visible         =   0   'False
      Width           =   960
   End
   Begin VB.Image imgUnknownTexture 
      Height          =   960
      Left            =   1065
      Picture         =   "frmMain.frx":181C0
      Top             =   3750
      Visible         =   0   'False
      Width           =   960
   End
   Begin VB.Menu mnuFile 
      Caption         =   " &File "
      Begin VB.Menu itmFile 
         Caption         =   "&New Map"
         Index           =   0
      End
      Begin VB.Menu itmFile 
         Caption         =   "&Open Map..."
         Index           =   1
      End
      Begin VB.Menu itmFile 
         Caption         =   "S&witch Map..."
         Index           =   2
      End
      Begin VB.Menu itmFile 
         Caption         =   "&Close Map"
         Index           =   3
      End
      Begin VB.Menu itmFile 
         Caption         =   "-"
         Index           =   4
      End
      Begin VB.Menu itmFile 
         Caption         =   "&Save Map"
         Index           =   5
      End
      Begin VB.Menu itmFile 
         Caption         =   "Save Map &As..."
         Index           =   6
      End
      Begin VB.Menu itmFile 
         Caption         =   "Save Map &Into...   "
         Index           =   7
      End
      Begin VB.Menu itmFile 
         Caption         =   "-"
         Index           =   8
      End
      Begin VB.Menu itmFile 
         Caption         =   "&Export Map..."
         Index           =   9
      End
      Begin VB.Menu itmFile 
         Caption         =   "Export &Picture..."
         Index           =   10
      End
      Begin VB.Menu itmFile 
         Caption         =   "-"
         Index           =   11
      End
      Begin VB.Menu itmFile 
         Caption         =   "&Build Nodes"
         Index           =   12
      End
      Begin VB.Menu itmFile 
         Caption         =   "&Test Map"
         Index           =   13
      End
      Begin VB.Menu itmFile 
         Caption         =   "-"
         Index           =   14
      End
      Begin VB.Menu itmFileRecent 
         Caption         =   "itmFileRecent"
         Index           =   0
      End
      Begin VB.Menu itmFileLine2 
         Caption         =   "-"
      End
      Begin VB.Menu itmFileExit 
         Caption         =   "E&xit"
      End
   End
   Begin VB.Menu mnuEdit 
      Caption         =   " &Edit "
      Begin VB.Menu itmEditUndo 
         Caption         =   "&Undo ... "
      End
      Begin VB.Menu itmEditRedo 
         Caption         =   "&Redo ..."
      End
      Begin VB.Menu itmEditLine2 
         Caption         =   "-"
      End
      Begin VB.Menu itmEditMode 
         Caption         =   "&Move Mode"
         Index           =   0
      End
      Begin VB.Menu itmEditMode 
         Caption         =   "&Vertices Mode"
         Index           =   1
      End
      Begin VB.Menu itmEditMode 
         Caption         =   "&Lines Mode"
         Index           =   2
      End
      Begin VB.Menu itmEditMode 
         Caption         =   "&Sectors Mode"
         Index           =   3
      End
      Begin VB.Menu itmEditMode 
         Caption         =   "&Things Mode"
         Index           =   4
      End
      Begin VB.Menu itmEditMode 
         Caption         =   "&3D Mode"
         Index           =   5
      End
      Begin VB.Menu itmEdit 
         Caption         =   "-"
         Index           =   0
      End
      Begin VB.Menu itmEdit 
         Caption         =   "Cut"
         Index           =   1
      End
      Begin VB.Menu itmEdit 
         Caption         =   "Copy"
         Index           =   2
      End
      Begin VB.Menu itmEdit 
         Caption         =   "Paste"
         Index           =   3
      End
      Begin VB.Menu itmEdit 
         Caption         =   "Delete"
         Index           =   4
      End
      Begin VB.Menu itmEdit 
         Caption         =   "-"
         Index           =   5
      End
      Begin VB.Menu itmEdit 
         Caption         =   "Find..."
         Index           =   6
      End
      Begin VB.Menu itmEdit 
         Caption         =   "Find and Replace..."
         Index           =   7
      End
      Begin VB.Menu itmEdit 
         Caption         =   "-"
         Index           =   8
      End
      Begin VB.Menu itmEdit 
         Caption         =   "Flip Horizontally"
         Index           =   9
      End
      Begin VB.Menu itmEdit 
         Caption         =   "Flip Vertically"
         Index           =   10
      End
      Begin VB.Menu itmEdit 
         Caption         =   "Rotate"
         Index           =   11
      End
      Begin VB.Menu itmEdit 
         Caption         =   "Resize"
         Index           =   12
      End
      Begin VB.Menu itmEdit 
         Caption         =   "-"
         Index           =   13
      End
      Begin VB.Menu itmEdit 
         Caption         =   "Snap To Grid"
         Checked         =   -1  'True
         Index           =   14
      End
      Begin VB.Menu itmEdit 
         Caption         =   "Snap To Vertices"
         Checked         =   -1  'True
         Index           =   15
      End
      Begin VB.Menu itmEdit 
         Caption         =   "Stitch Vertices"
         Checked         =   -1  'True
         Index           =   16
      End
      Begin VB.Menu itmEdit 
         Caption         =   "Center View"
         Index           =   17
      End
      Begin VB.Menu itmEdit 
         Caption         =   "-"
         Index           =   18
      End
      Begin VB.Menu itmEdit 
         Caption         =   "Select All"
         Index           =   19
      End
      Begin VB.Menu itmEdit 
         Caption         =   "Select None"
         Index           =   20
      End
      Begin VB.Menu itmEdit 
         Caption         =   "Invert Selection"
         Index           =   21
      End
      Begin VB.Menu itmEdit 
         Caption         =   "-"
         Index           =   22
      End
      Begin VB.Menu itmEdit 
         Caption         =   "Map &Options...   "
         Index           =   23
      End
   End
   Begin VB.Menu mnuVertices 
      Caption         =   " &Vertices "
      Begin VB.Menu itmVerticesSnapToGrid 
         Caption         =   "&Snap to Grid"
      End
      Begin VB.Menu itmVerticesLine2 
         Caption         =   "-"
      End
      Begin VB.Menu itmVerticesStitch 
         Caption         =   "Stitch Vertices"
      End
      Begin VB.Menu itmVerticesLine1 
         Caption         =   "-"
      End
      Begin VB.Menu itmVerticesClearUnused 
         Caption         =   "&Clear Unused Vertices"
      End
   End
   Begin VB.Menu mnuLines 
      Caption         =   " &Lines "
      Begin VB.Menu itmLinesSnapToGrid 
         Caption         =   "&Snap to Grid"
      End
      Begin VB.Menu itmLinesLine1 
         Caption         =   "-"
      End
      Begin VB.Menu itmLinesAlign 
         Caption         =   "Autoalign Textures..."
      End
      Begin VB.Menu itmLinesLine2 
         Caption         =   "-"
      End
      Begin VB.Menu itmLinesSelect 
         Caption         =   "Select only 1-sided lines"
         Index           =   0
      End
      Begin VB.Menu itmLinesSelect 
         Caption         =   "Select only 2-sided lines"
         Index           =   1
      End
      Begin VB.Menu itmLinesLine3 
         Caption         =   "-"
      End
      Begin VB.Menu itmLinesFlipLinedefs 
         Caption         =   "Flip &Linedefs"
      End
      Begin VB.Menu itmLinesFlipSidedefs 
         Caption         =   "Flip &Sidedefs"
      End
      Begin VB.Menu itmLinesCurve 
         Caption         =   "Curve Linedefs"
      End
      Begin VB.Menu itmLinesSplit 
         Caption         =   "Spli&t Linedefs"
      End
      Begin VB.Menu itmLinesLine4 
         Caption         =   "-"
      End
      Begin VB.Menu itmLinesCascadeTags 
         Caption         =   "Cascade tags upwards"
      End
      Begin VB.Menu itmLinesLine5 
         Caption         =   "-"
      End
      Begin VB.Menu itmLinesCopy 
         Caption         =   "&Copy Linedef Properties"
      End
      Begin VB.Menu itmLinesPaste 
         Caption         =   "&Paste Linedef Properties"
      End
   End
   Begin VB.Menu mnuSectors 
      Caption         =   " &Sectors "
      Begin VB.Menu itmSectorsSnapToGrid 
         Caption         =   "&Snap to Grid"
      End
      Begin VB.Menu itmSectorsLine1 
         Caption         =   "-"
      End
      Begin VB.Menu itmSectorsJoin 
         Caption         =   "Join Sectors"
      End
      Begin VB.Menu itmSectorsMerge 
         Caption         =   "Merge Sectors"
      End
      Begin VB.Menu itmSectorsLine5 
         Caption         =   "-"
      End
      Begin VB.Menu itmSectorsGradientBrightness 
         Caption         =   "Gradient Brightness"
      End
      Begin VB.Menu itmSectorsGradientFloors 
         Caption         =   "Gradient Floors"
      End
      Begin VB.Menu itmSectorsGradientCeilings 
         Caption         =   "Gradient Ceilings"
      End
      Begin VB.Menu itmSectorsCascadeTags 
         Caption         =   "Cascade Tags Upwards"
      End
      Begin VB.Menu itmSectorsConvertToStairs 
         Caption         =   "Convert to Stairs"
      End
      Begin VB.Menu itmSectorsLine2 
         Caption         =   "-"
      End
      Begin VB.Menu itmSectorsRaiseFloor 
         Caption         =   "Raise Floor by 8px"
      End
      Begin VB.Menu itmSectorsLowerFloor 
         Caption         =   "Lower Floor by 8px"
      End
      Begin VB.Menu itmSectorsRaiseCeiling 
         Caption         =   "Raise Ceiling by 8px"
      End
      Begin VB.Menu itmSectorsLowerCeiling 
         Caption         =   "Lower Ceiling by 8px"
      End
      Begin VB.Menu itmSectorsLine3 
         Caption         =   "-"
      End
      Begin VB.Menu itmSectorsIncBrightness 
         Caption         =   "Increase Brightness"
      End
      Begin VB.Menu itmSectorsDecBrightness 
         Caption         =   "Decrease Brightness"
      End
      Begin VB.Menu itmSectorsLine4 
         Caption         =   "-"
      End
      Begin VB.Menu itmSectorsCopy 
         Caption         =   "&Copy Sector Properties"
      End
      Begin VB.Menu itmSectorsPaste 
         Caption         =   "&Paste Sector Properties"
      End
      Begin VB.Menu itmSectorsLine6 
         Caption         =   "-"
      End
      Begin VB.Menu itmSectorsFindIdenticalSectorSet 
         Caption         =   "Find set of &identical sectors"
      End
      Begin VB.Menu itmSectorsLine7 
         Caption         =   "-"
      End
      Begin VB.Menu itmSectorsFOF 
         Caption         =   "Setup &FOF..."
      End
   End
   Begin VB.Menu mnuThings 
      Caption         =   " &Things "
      Begin VB.Menu itmThingsSnapToGrid 
         Caption         =   "&Snap to Grid"
      End
      Begin VB.Menu itmThingsLine1 
         Caption         =   "-"
      End
      Begin VB.Menu itmThingsRotateCW 
         Caption         =   "Rotate angle clockwise"
      End
      Begin VB.Menu itmThingsRotateCCW 
         Caption         =   "Rotate angle counterclockwise"
      End
      Begin VB.Menu itmThingsMouseRotate 
         Caption         =   "Mouse rotate"
      End
      Begin VB.Menu itmThingsLine3 
         Caption         =   "-"
      End
      Begin VB.Menu itmThingsCopy 
         Caption         =   "&Copy Thing Properties"
      End
      Begin VB.Menu itmThingsPaste 
         Caption         =   "&Paste Thing Properties"
      End
      Begin VB.Menu itmThingsLine2 
         Caption         =   "-"
      End
      Begin VB.Menu itmThingsFilter 
         Caption         =   "&Filter Things..."
      End
   End
   Begin VB.Menu mnuPrefabs 
      Caption         =   " &Prefabs "
      Begin VB.Menu itmPrefabQuick 
         Caption         =   "itmPrefabQuick"
         Index           =   0
      End
      Begin VB.Menu itmPrefabQuick 
         Caption         =   "itmPrefabQuick"
         Index           =   1
      End
      Begin VB.Menu itmPrefabQuick 
         Caption         =   "itmPrefabQuick"
         Index           =   2
      End
      Begin VB.Menu itmPrefabQuick 
         Caption         =   "itmPrefabQuick"
         Index           =   3
      End
      Begin VB.Menu itmPrefabQuick 
         Caption         =   "itmPrefabQuick"
         Index           =   4
      End
      Begin VB.Menu itmPrefabLine1 
         Caption         =   "-"
      End
      Begin VB.Menu itmPrefabPrevious 
         Caption         =   "Insert &Previous Prefab"
      End
      Begin VB.Menu itmPrefabInsert 
         Caption         =   "&Insert Prefab from File..."
      End
      Begin VB.Menu itmPrefabLine2 
         Caption         =   "-"
      End
      Begin VB.Menu itmPrefabSaveSel 
         Caption         =   "Save &selection as Prefab..."
      End
      Begin VB.Menu itmPrefabSaveMap 
         Caption         =   "Save &map as Prefab..."
         Visible         =   0   'False
      End
   End
   Begin VB.Menu mnuScripts 
      Caption         =   " &Scripts "
      Begin VB.Menu itmScriptEdit 
         Caption         =   "itmScriptEdit"
         Index           =   0
      End
      Begin VB.Menu itmScriptsLine1 
         Caption         =   "-"
      End
      Begin VB.Menu itmScriptsMerge 
         Caption         =   "&Merge MAPnnD into MAINCFG"
      End
   End
   Begin VB.Menu mnuTools 
      Caption         =   " &Tools "
      Begin VB.Menu itmToolsFindErrors 
         Caption         =   "&Find map errors..."
      End
      Begin VB.Menu itmToolsTagBrowser 
         Caption         =   "Tag bro&wser"
      End
      Begin VB.Menu itmToolsLine3 
         Caption         =   "-"
      End
      Begin VB.Menu itmToolsClearTextures 
         Caption         =   "&Remove Unused Textures"
      End
      Begin VB.Menu itmToolsFixTextures 
         Caption         =   "Fix &Missing Textures"
      End
      Begin VB.Menu itmToolsFixZeroLinedefs 
         Caption         =   "Fix &Zero-Length Linedefs"
      End
      Begin VB.Menu itmToolsReloadResources 
         Caption         =   "Reloa&d Resources"
      End
      Begin VB.Menu itmToolsLine1 
         Caption         =   "-"
      End
      Begin VB.Menu mnuToolsEffects 
         Caption         =   "&Effects"
         Begin VB.Menu itmToolsEffectsZoomTube 
            Caption         =   "Zoom Tube Wizard..."
         End
      End
      Begin VB.Menu itmToolsLine2 
         Caption         =   "-"
      End
      Begin VB.Menu itmToolsCustomiseTB 
         Caption         =   "Customise &Toolbar..."
      End
      Begin VB.Menu itmToolsConfiguration 
         Caption         =   "&Configuration..."
      End
   End
   Begin VB.Menu mnuHelp 
      Caption         =   " &Help "
      Begin VB.Menu itmHelpWebsite 
         Caption         =   "Doom Builder &Website..."
      End
      Begin VB.Menu itmHelpFAQ 
         Caption         =   "Frequently Asked Questions..."
      End
      Begin VB.Menu itmHelpLine1 
         Caption         =   "-"
      End
      Begin VB.Menu itmHelpAbout 
         Caption         =   "&About..."
      End
   End
   Begin VB.Menu mnuToolbar 
      Caption         =   "Toolbar Popup"
      Visible         =   0   'False
      Begin VB.Menu itmToolbarCustomise 
         Caption         =   "&Customise..."
      End
   End
End
Attribute VB_Name = "frmMain"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'
'    Doom Builder
'    Copyright (c) 2003 Pascal vd Heiden, www.codeimp.com
'    This program is released under GNU General Public License
'
'    This program is distributed in the hope that it will be useful,
'    but WITHOUT ANY WARRANTY; without even the implied warranty of
'    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
'    GNU General Public License for more details.
'


'Do not allow any undeclared variables
Option Explicit

'Case sensitive comparisions
Option Compare Binary



Private Enum LDDISPLAY
     LDD_S1U = 1
     LDD_S1M = 2
     LDD_S1L = 4
     LDD_S2U = 8
     LDD_S2M = 16
     LDD_S2L = 32
     LDD_LENGTH = 64
     LDD_FRONTHEIGHT = 128
     LDD_BACKHEIGHT = 256
     LDD_SPECIAL = 512
     LDD_V1 = 1024
     LDD_V2 = 2048
     LDD_S1 = 4096            ' These two control the entire panels.
     LDD_S2 = 8192            ' Useful for single-sided linedefs.
     LDD_FRONTSEC = 16384
     LDD_BACKSEC = 32768
     LDD_TAG = 65536
     LDD_FRONTX = 131072
     LDD_BACKX = 262144
     LDD_FRONTY = 524288
     LDD_BACKY = 1048576
     LDD_NUM = 2097152        ' Clearly only ever useful for a single ld.
     LDD_ALL = 4194303
End Enum

' Strings all round.
Private Type LDDISPINFO
     S1U As String
     S1M As String
     S1L As String
     S2U As String
     S2M As String
     S2L As String
     Length As String
     FrontHeight As String
     BackHeight As String
     special As String
     V1 As String
     V2 As String
     FrontSec As String
     BackSec As String
     Tag As String
     FrontX As String
     BackX As String
     FrontY As String
     BackY As String
     num As String
End Type

Private Enum SECDISPLAY
     SCD_NUM = 1         ' Clearly only ever useful for a single sector.
     SCD_HFLOOR = 2
     SCD_HCEIL = 4
     SCD_TAG = 8
     SCD_TYPE = 16
     SCD_TFLOOR = 32
     SCD_TCEIL = 64
     SCD_HEIGHT = 128
     SCD_LIGHT = 256
     SCD_ALL = 511
End Enum

Private Type SECDISPINFO
     num As String
     HFloor As String
     HCeil As String
     Tag As String
     Type As String
     TFloor As String
     TCeil As String
     Height As String
     Light As String
End Type
     

Private Enum TEXREQFLAGS
     REQ_S1U = 1
     REQ_S1M = 2
     REQ_S1L = 4
     REQ_S2U = 8
     REQ_S2M = 16
     REQ_S2L = 32
End Enum
     


Private DeselectAfterEdit As Boolean
Public OriginalMessageHandler As Long

Private GrabX As Single
Private GrabY As Single

Private DragStartPxX As Single
Private DragStartPxY As Single

Private StartSelection As Boolean
Private LastSelX As Single
Private LastSelY As Single

Private AutoScrollX As Long
Private AutoScrollY As Long
Private ForceScroll As Boolean

Private NoEditing As Boolean

Private F7Count As Long

Private LastMouseButton As Integer
Private LastMouseShift As Integer
Public LastMouseX As Single, LastMouseY As Single

Private DrawingCoords() As POINT
Private NumDrawingCoords As Long

Private ASMinutes As Long

Public EatMouseUp As Boolean
Private ShiftMaskAtDrawStart As Long

Public NewSec As Long

Public Sub ApplyInterfaceConfiguration()
     
     'Show/hide toolbar
     tlbToolbar.visible = (Val(Config("showtoolbar")) = vbChecked)
     
     ' Restore the toolbar layout. Yes, really.
     tmrStupidToolbarBugWorkaround.Enabled = True
     
     
     'Show hide and position the details bar
     Select Case Val(Config("detailsbar"))
          
          Case 0:   'No details bar
               picBar.visible = False
               picSBar.visible = False
               
          Case 1:   'Bottom
               picBar.visible = True
               picSBar.visible = False
               picBar.Align = vbAlignBottom
               
          Case 2:   'Top
               picBar.visible = True
               picSBar.visible = False
               picBar.Align = vbAlignTop
               
          Case 3:   'Left
               picBar.visible = False
               picSBar.visible = True
               picSBar.Align = vbAlignLeft
               
               
          Case 4:   'Right
               picBar.visible = False
               picSBar.visible = True
               picSBar.Align = vbAlignRight
               
     End Select
     
     'Toggle bar
     If (Val(Config("togglebar")) = 0) Then InfoBarOpen Else InfoBarClose
End Sub

Public Sub CancelCurrentOperation()
     
     'Can only be done if a map is open
     If (mapfile <> "") Then
          
          'Cancel if in drawing operation
          Select Case submode
               Case ESM_SELECTING: CancelSelectOperation: RedrawMap False
               Case ESM_DRAGGING: CancelDragOperation: RedrawMap False
               Case ESM_DRAWING: CancelDrawOperation: RedrawMap False
               Case ESM_PASTING: CancelDragOperation: RemoveSelection True
          End Select
          
          'Show highlight
          ShowHighlight LastX, LastY
          
          'Update status
          UpdateStatusBar
     End If
     
     'Not scrolling anymore
     Scrolling = False
     Screen.MousePointer = vbNormal
End Sub

Private Sub CancelDragOperation()
     
     'End of drag operation, set mode back to normal
     submode = ESM_NONE
     
     'Map was changed
     mapnodeschanged = True
     mapchanged = True
     
     'We dont need these anymore
     ReDim changedlines(0)
     numchangedlines = 0
     
     'Perform undo
     PerformUndo
     
     'Withdraw the redo (like nothing happend)
     WithdrawRedo
End Sub

Public Sub CancelDrawOperation()
     
     'Deselect all
     RemoveSelection False
     
     'End of drawing operation, set mode back to normal
     submode = ESM_NONE
     
     'Map was changed
     mapnodeschanged = True
     mapchanged = True
     
     'We dont need these anymore
     ReDim changedlines(0)
     numchangedlines = 0
     ReDim DrawingCoords(0)
     NumDrawingCoords = 0
     
     'Perform undo
     PerformUndo
     
     'Withdraw the redo (like nothing happend)
     WithdrawRedo
End Sub

Public Sub CancelSelectOperation()
     
     'End of select operation, set mode back to normal
     submode = ESM_NONE
End Sub

Public Sub ChangeAutoscroll(ByVal ForceDisable As Boolean)
     Dim px As Long, py As Long
     Dim sx As Long, sy As Long
     
     Const ScrollBounds As Long = 100
     Const ScrollMultiplier As Single = 0.6
     
     'Check if we should stop autoscroling
     If (ForceDisable = True) Or _
        ( _
          (Not ForceScroll) And _
          ((Config("autoscroll") = vbUnchecked) Or (mode = EM_MOVE) Or (submode = ESM_NONE)) _
        ) Then
          
          'Stop autoscrolling
          tmrAutoScroll.Enabled = False
          
          'Leave now
          Exit Sub
     End If
     
     'Get pixel coordinates
     px = -(ViewLeft - LastX) * ViewZoom
     py = -(ViewTop - LastY) * ViewZoom
     
     
     ' ForceScroll is set when using the scroll key. So we scroll then whenever
     ' we're at the edge, not just beyond where we started.
     
     'Determine scrolling in X
     If (px < ScrollBounds And (px <= DragStartPxX Or ForceScroll)) Then
          
          'Scroll to the left
          sx = -(ScrollBounds - px) * ScrollMultiplier
          If (sx < -ScrollBounds) Then sx = 0
          
     ElseIf (px > (picMap.width - 4) - ScrollBounds And (px >= DragStartPxX Or ForceScroll)) Then
          
          'Scroll to the right
          sx = (px - ((picMap.width - 4) - ScrollBounds)) * ScrollMultiplier
          If (sx > ScrollBounds) Then sx = 0
     End If
     
     'Determine scrolling in Y
     If (py < ScrollBounds And (py <= DragStartPxY Or ForceScroll)) Then
          
          'Scroll to the top
          sy = -(ScrollBounds - py) * ScrollMultiplier
          If (sy < -ScrollBounds) Then sy = 0
          
     ElseIf (py > (picMap.Height - 4) - ScrollBounds And (py >= DragStartPxY Or ForceScroll)) Then
          
          'Scroll to the bottom
          sy = (py - ((picMap.Height - 4) - ScrollBounds)) * ScrollMultiplier
          If (sy > ScrollBounds) Then sy = 0
     End If
     
     'Check for scrolling
     If (sx <> 0) Or (sy <> 0) Then
          
          'Set scrolling
          AutoScrollX = sx / ViewZoom
          AutoScrollY = sy / ViewZoom
          tmrAutoScroll.Enabled = True
     Else
          
          'No scrolling
          tmrAutoScroll.Enabled = False
     End If
End Sub

Private Sub ChangeLinesHighlight(ByVal x As Long, ByVal y As Long, Optional ByVal ForceUpdate As Boolean)
     Dim distance As Long
     Dim nearest As Long
     Dim OldSelected As Long
     Dim xl As Long, yl As Long
     
     Dim Action As String
     Dim Length As Long
     Dim sTag As String
     Dim FHeight As String
     Dim bheight As String
     Dim FSector As String
     Dim BSector As String
     Dim fx As String
     Dim fy As String
     Dim bx As String
     Dim by As String
     Dim S1U As String
     Dim S1M As String
     Dim S1L As String
     Dim S2U As String
     Dim S2M As String
     Dim S2L As String
     
     'Keep old selection ifnot forced to update
     If (Not ForceUpdate) Then OldSelected = currentselected Else OldSelected = -2
     
     'Check if a previous linedef was selected
     If (currentselected > -1) Then
          
          'Render the last selected linedef to normal (also vertices, those have been overdrawn)
          Render_AllLinedefs vertexes(0), linedefs(0), VarPtr(sectors(0)), VarPtr(sidedefs(0)), currentselected, currentselected, submode, indicatorsize
          If (Config("mode1vertices")) Then
               Render_AllVertices vertexes(0), linedefs(currentselected).V1, linedefs(currentselected).V1, vertexsize
               Render_AllVertices vertexes(0), linedefs(currentselected).V2, linedefs(currentselected).V2, vertexsize
          End If
          
          'Check if linedef has an action
          If (linedefs(currentselected).effect > 0) Then
               
               'Render the last tagged sectors
               If (linedefs(currentselected).Tag <> 0) Then Render_TaggedSectors vertexes(0), linedefs(0), VarPtr(sidedefs(0)), VarPtr(sectors(0)), numsectors, numlinedefs, linedefs(currentselected).Tag, 0, indicatorsize, Config("mode1vertices"), vertexsize
               
               'Check if line has a known effect
               If (mapconfig("linedeftypes").Exists(CStr(linedefs(currentselected).effect)) = True) Then
                    
                    'Render by hexen format
                    If (mapconfig("linedeftypes")(CStr(linedefs(currentselected).effect))("mark1") = 1) And (linedefs(currentselected).arg0 > 0) Then Render_TaggedSectors vertexes(0), linedefs(0), VarPtr(sidedefs(0)), VarPtr(sectors(0)), numsectors, numlinedefs, linedefs(currentselected).arg0, 0, indicatorsize, Config("mode1vertices"), vertexsize
                    If (mapconfig("linedeftypes")(CStr(linedefs(currentselected).effect))("mark2") = 1) And (linedefs(currentselected).arg1 > 0) Then Render_TaggedSectors vertexes(0), linedefs(0), VarPtr(sidedefs(0)), VarPtr(sectors(0)), numsectors, numlinedefs, linedefs(currentselected).arg1, 0, indicatorsize, Config("mode1vertices"), vertexsize
                    If (mapconfig("linedeftypes")(CStr(linedefs(currentselected).effect))("mark3") = 1) And (linedefs(currentselected).arg2 > 0) Then Render_TaggedSectors vertexes(0), linedefs(0), VarPtr(sidedefs(0)), VarPtr(sectors(0)), numsectors, numlinedefs, linedefs(currentselected).arg2, 0, indicatorsize, Config("mode1vertices"), vertexsize
                    If (mapconfig("linedeftypes")(CStr(linedefs(currentselected).effect))("mark4") = 1) And (linedefs(currentselected).arg3 > 0) Then Render_TaggedSectors vertexes(0), linedefs(0), VarPtr(sidedefs(0)), VarPtr(sectors(0)), numsectors, numlinedefs, linedefs(currentselected).arg3, 0, indicatorsize, Config("mode1vertices"), vertexsize
                    If (mapconfig("linedeftypes")(CStr(linedefs(currentselected).effect))("mark5") = 1) And (linedefs(currentselected).arg4 > 0) Then Render_TaggedSectors vertexes(0), linedefs(0), VarPtr(sidedefs(0)), VarPtr(sectors(0)), numsectors, numlinedefs, linedefs(currentselected).arg4, 0, indicatorsize, Config("mode1vertices"), vertexsize
                    
                    'Check if things are shown as well
                    If (Val(Config("modethings"))) Then
                         If ((mapconfig("linedeftypes")(CStr(linedefs(currentselected).effect))("mark1") = 2) And (linedefs(currentselected).arg0 > 0)) Or _
                            ((mapconfig("linedeftypes")(CStr(linedefs(currentselected).effect))("mark2") = 2) And (linedefs(currentselected).arg1 > 0)) Or _
                            ((mapconfig("linedeftypes")(CStr(linedefs(currentselected).effect))("mark3") = 2) And (linedefs(currentselected).arg2 > 0)) Or _
                            ((mapconfig("linedeftypes")(CStr(linedefs(currentselected).effect))("mark4") = 2) And (linedefs(currentselected).arg3 > 0)) Or _
                            ((mapconfig("linedeftypes")(CStr(linedefs(currentselected).effect))("mark5") = 2) And (linedefs(currentselected).arg4 > 0)) Then
                              
                              'Redraw map
                              Render_AllThingsDarkened things(0), 0, numthings - 1, ThingBitmapData(0), frmMain.picThings(thingsize).width, frmMain.picThings(thingsize).Height, filterthings, filtersettings
                              Render_AllLinedefs vertexes(0), linedefs(0), VarPtr(sectors(0)), VarPtr(sidedefs(0)), 0, numlinedefs - 1, submode, indicatorsize
                              If (Config("mode1vertices")) Then Render_AllVertices vertexes(0), 0, numvertexes - 1, vertexsize
                         End If
                    End If
               End If
          End If
     End If
     
     'Get the nearest linedef and select it if within allowed distance
     nearest = NearestLinedef(x, y, vertexes(0), linedefs(0), numlinedefs, distance)
     If (distance <= Config("lineselectdistance") / ViewZoom) Then currentselected = nearest Else currentselected = -1
     
     'Check if a new selection is made
     If (currentselected > -1) Then
          
          'Check if linedef has an action
          If (linedefs(currentselected).effect > 0) Then
               
               'Render the tagged sectors if line has a tag
               If (linedefs(currentselected).Tag <> 0) Then Render_TaggedSectors vertexes(0), linedefs(0), VarPtr(sidedefs(0)), VarPtr(sectors(0)), numsectors, numlinedefs, linedefs(currentselected).Tag, CLR_SECTORTAG, indicatorsize, Config("mode1vertices"), vertexsize
               
               'Check if we can render by hexen format
               If (mapconfig("linedeftypes").Exists(CStr(linedefs(currentselected).effect)) = True) Then
                    
                    'Render by hexen format
                    If (mapconfig("linedeftypes")(CStr(linedefs(currentselected).effect))("mark1") = 1) And (linedefs(currentselected).arg0 > 0) Then Render_TaggedSectors vertexes(0), linedefs(0), VarPtr(sidedefs(0)), VarPtr(sectors(0)), numsectors, numlinedefs, linedefs(currentselected).arg0, CLR_SECTORTAG, indicatorsize, Config("mode1vertices"), vertexsize
                    If (mapconfig("linedeftypes")(CStr(linedefs(currentselected).effect))("mark2") = 1) And (linedefs(currentselected).arg1 > 0) Then Render_TaggedSectors vertexes(0), linedefs(0), VarPtr(sidedefs(0)), VarPtr(sectors(0)), numsectors, numlinedefs, linedefs(currentselected).arg1, CLR_SECTORTAG, indicatorsize, Config("mode1vertices"), vertexsize
                    If (mapconfig("linedeftypes")(CStr(linedefs(currentselected).effect))("mark3") = 1) And (linedefs(currentselected).arg2 > 0) Then Render_TaggedSectors vertexes(0), linedefs(0), VarPtr(sidedefs(0)), VarPtr(sectors(0)), numsectors, numlinedefs, linedefs(currentselected).arg2, CLR_SECTORTAG, indicatorsize, Config("mode1vertices"), vertexsize
                    If (mapconfig("linedeftypes")(CStr(linedefs(currentselected).effect))("mark4") = 1) And (linedefs(currentselected).arg3 > 0) Then Render_TaggedSectors vertexes(0), linedefs(0), VarPtr(sidedefs(0)), VarPtr(sectors(0)), numsectors, numlinedefs, linedefs(currentselected).arg3, CLR_SECTORTAG, indicatorsize, Config("mode1vertices"), vertexsize
                    If (mapconfig("linedeftypes")(CStr(linedefs(currentselected).effect))("mark5") = 1) And (linedefs(currentselected).arg4 > 0) Then Render_TaggedSectors vertexes(0), linedefs(0), VarPtr(sidedefs(0)), VarPtr(sectors(0)), numsectors, numlinedefs, linedefs(currentselected).arg4, CLR_SECTORTAG, indicatorsize, Config("mode1vertices"), vertexsize
                    
                    'Check if things are shown as well
                    If (Val(Config("modethings"))) Then
                         
                         'Render things by hexen format
                         If (mapconfig("linedeftypes")(CStr(linedefs(currentselected).effect))("mark1") = 2) And (linedefs(currentselected).arg0 > 0) Then Render_TaggedThings things(0), numthings - 1, linedefs(currentselected).arg0, ThingBitmapData(0), frmMain.picThings(thingsize).width, frmMain.picThings(thingsize).Height, Val(Config("allthingsrects")), ViewZoom, filterthings, filtersettings
                         If (mapconfig("linedeftypes")(CStr(linedefs(currentselected).effect))("mark2") = 2) And (linedefs(currentselected).arg1 > 0) Then Render_TaggedThings things(0), numthings - 1, linedefs(currentselected).arg1, ThingBitmapData(0), frmMain.picThings(thingsize).width, frmMain.picThings(thingsize).Height, Val(Config("allthingsrects")), ViewZoom, filterthings, filtersettings
                         If (mapconfig("linedeftypes")(CStr(linedefs(currentselected).effect))("mark3") = 2) And (linedefs(currentselected).arg2 > 0) Then Render_TaggedThings things(0), numthings - 1, linedefs(currentselected).arg2, ThingBitmapData(0), frmMain.picThings(thingsize).width, frmMain.picThings(thingsize).Height, Val(Config("allthingsrects")), ViewZoom, filterthings, filtersettings
                         If (mapconfig("linedeftypes")(CStr(linedefs(currentselected).effect))("mark4") = 2) And (linedefs(currentselected).arg3 > 0) Then Render_TaggedThings things(0), numthings - 1, linedefs(currentselected).arg3, ThingBitmapData(0), frmMain.picThings(thingsize).width, frmMain.picThings(thingsize).Height, Val(Config("allthingsrects")), ViewZoom, filterthings, filtersettings
                         If (mapconfig("linedeftypes")(CStr(linedefs(currentselected).effect))("mark5") = 2) And (linedefs(currentselected).arg4 > 0) Then Render_TaggedThings things(0), numthings - 1, linedefs(currentselected).arg4, ThingBitmapData(0), frmMain.picThings(thingsize).width, frmMain.picThings(thingsize).Height, Val(Config("allthingsrects")), ViewZoom, filterthings, filtersettings
                    End If
               End If
          End If
          
          'Render the selected linedef to selected (also vertices, those have been overdrawn)
          Render_LinedefLine vertexes(linedefs(currentselected).V1).x, vertexes(linedefs(currentselected).V1).y, vertexes(linedefs(currentselected).V2).x, vertexes(linedefs(currentselected).V2).y, CLR_LINEHIGHLIGHT, indicatorsize
          If (Config("mode1vertices")) Then
               Render_AllVertices vertexes(0), linedefs(currentselected).V1, linedefs(currentselected).V1, vertexsize
               Render_AllVertices vertexes(0), linedefs(currentselected).V2, linedefs(currentselected).V2, vertexsize
          End If
     End If
     
     'Show the rendered changes
     If (OldSelected <> currentselected) Then picMap.Refresh
     
     'Check if we should show the info
     If (currentselected > -1) Then
          
          'Only update when changed
          If (OldSelected <> currentselected) Then ShowLinedefInfo currentselected
     Else
          
          If numselected = 0 Then
               HideLinedefInfo
          ElseIf (OldSelected <> currentselected) Then
               ShowSelectedLinedefInfo
          End If
     End If
End Sub

Private Sub ChangeSectorsHighlight(ByVal x As Long, ByVal y As Long, Optional ByVal ForceUpdate As Boolean)
     Dim nearest As Long
     Dim OldSelected As Long
     Dim ld As Long, ldfound As Long
     
     Dim effect As String
     Dim Ceiling As Long
     Dim Floor As Long
     Dim sTag As Long
     Dim sHeight As Long
     Dim Brightness As Long
     Dim tceiling As String
     Dim TFloor As String
     
     'Keep old selection ifnot forced to update
     If (Not ForceUpdate) Then OldSelected = currentselected Else OldSelected = -2
     
     'Check if a previous sector was selected
     If (currentselected > -1) Then
          
          'Go for all linedefs
          For ld = 0 To (numlinedefs - 1)
               
               'Check if one of the sidedefs belong to this sector
               ldfound = 0
               If (linedefs(ld).s1 > -1) Then If (sidedefs(linedefs(ld).s1).sector = currentselected) Then ldfound = 1
               If (linedefs(ld).s2 > -1) Then If (sidedefs(linedefs(ld).s2).sector = currentselected) Then ldfound = 1
               
               If (ldfound) Then
                    
                    'Render this linedef to normal (also vertices, those have been overdrawn)
                    Render_AllLinedefs vertexes(0), linedefs(0), VarPtr(sectors(0)), VarPtr(sidedefs(0)), ld, ld, submode, indicatorsize
                    If (Config("mode2vertices")) Then
                         Render_AllVertices vertexes(0), linedefs(ld).V1, linedefs(ld).V1, vertexsize
                         Render_AllVertices vertexes(0), linedefs(ld).V2, linedefs(ld).V2, vertexsize
                    End If
               End If
          Next ld
          
          'Check if sector has a tag
          If (sectors(currentselected).Tag <> 0) Then
               
               'Render the last tagged linedefs
               Render_TaggedLinedefs vertexes(0), linedefs(0), VarPtr(sectors(0)), VarPtr(sidedefs(0)), numlinedefs, sectors(currentselected).Tag, 1, 0, indicatorsize, 0, vertexsize
               
               'Check if things are shown as well
               If (Val(Config("modethings"))) Then
                    
                    'Redraw map
                    Render_AllThingsDarkened things(0), 0, numthings - 1, ThingBitmapData(0), frmMain.picThings(thingsize).width, frmMain.picThings(thingsize).Height, filterthings, filtersettings
                    Render_AllLinedefs vertexes(0), linedefs(0), VarPtr(sectors(0)), VarPtr(sidedefs(0)), 0, numlinedefs - 1, submode, indicatorsize
                    If (Config("mode2vertices")) Then Render_AllVertices vertexes(0), 0, numvertexes - 1, vertexsize
               End If
          End If
     End If
     
     'Get the intersecting sector and select it
     nearest = IntersectSector(x, y, vertexes(0), linedefs(0), VarPtr(sidedefs(0)), numlinedefs, 0)
     currentselected = nearest
     
     'Check if a new sector is selected
     If (currentselected > -1) Then
          
          'Check if sector has a tag
          If (sectors(currentselected).Tag <> 0) Then
               
               'Render the tagged linedefs
               Render_TaggedLinedefs vertexes(0), linedefs(0), VarPtr(sectors(0)), VarPtr(sidedefs(0)), numlinedefs, sectors(currentselected).Tag, 1, CLR_SECTORTAG, indicatorsize, 0, vertexsize
               
               'Check if things are shown as well
               If (Val(Config("modethings"))) Then Render_TaggedArgThings things(0), numthings, sectors(currentselected).Tag, 1, ThingBitmapData(0), frmMain.picThings(thingsize).width, frmMain.picThings(thingsize).Height, Val(Config("allthingsrects")), ViewZoom, filterthings, filtersettings
          End If
          
          'Go for all linedefs
          For ld = 0 To (numlinedefs - 1)
               
               'Check if one of the sidedefs belong to this sector
               ldfound = 0
               If (linedefs(ld).s1 > -1) Then If (sidedefs(linedefs(ld).s1).sector = currentselected) Then ldfound = 1
               If (linedefs(ld).s2 > -1) Then If (sidedefs(linedefs(ld).s2).sector = currentselected) Then ldfound = 1
               
               If (ldfound) Then
                    
                    'Render this linedef to selected (also vertices, those have been overdrawn)
                    Render_LinedefLine vertexes(linedefs(ld).V1).x, vertexes(linedefs(ld).V1).y, vertexes(linedefs(ld).V2).x, vertexes(linedefs(ld).V2).y, CLR_LINEHIGHLIGHT, indicatorsize
                    If (Config("mode2vertices")) Then
                         Render_AllVertices vertexes(0), linedefs(ld).V1, linedefs(ld).V1, vertexsize
                         Render_AllVertices vertexes(0), linedefs(ld).V2, linedefs(ld).V2, vertexsize
                    End If
               End If
          Next ld
     End If
     
     'Show the rendered changes
     If (OldSelected <> currentselected) Then picMap.Refresh
     
     'Check if we should show the info
     If (currentselected > -1) Then
          
          'Only update when changed
          If (OldSelected <> currentselected) Then
               
               'Show the information
               ShowSectorInfo currentselected
          End If
     Else
          
          'Hide the info
          If numselected = 0 Then
               HideSectorInfo
          ElseIf OldSelected <> currentselected Then
               ShowSelectedSectorInfo
          End If
     End If
End Sub

Private Sub ChangeThingsHighlight(ByVal x As Long, ByVal y As Long, Optional ByVal ForceUpdate As Boolean)
     Dim distance As Long
     Dim nearest As Long
     Dim OldSelected As Long
     Dim tType As String
     Dim angle As String
     Dim Flags As String
     Dim nType As Long
     Dim spritename As String
     Dim tx As Long
     Dim ty As Long
     Dim SectorUndrawn As Boolean
     Dim sTag As String
     Dim sAction As String
     
     'Keep old selection if not forced to update
     If (Not ForceUpdate) Then OldSelected = currentselected Else OldSelected = -1
     
     'Anything selected before?
     If (currentselected > -1) Then
          
          'Render the last current selected to normal
          Render_AllThings things(0), currentselected, currentselected, ThingBitmapData(0), frmMain.picThings(thingsize).width, frmMain.picThings(thingsize).Height, Val(Config("allthingsrects")), ViewZoom, filterthings, filtersettings
          If Config("thingrects") Then Render_BoxSwitched things(currentselected).x, things(currentselected).y, things(currentselected).size * ViewZoom, PAL_NORMAL, (Config("thingrects") - 1), PAL_NORMAL
          
          'Check if thing has a known effect
          If (mapconfig("linedeftypes").Exists(CStr(things(currentselected).effect)) = True) Then
               
               'Render by hexen format
               If (mapconfig("linedeftypes")(CStr(things(currentselected).effect))("mark1") = 1) And (things(currentselected).arg0 > 0) Then Render_TaggedSectors vertexes(0), linedefs(0), VarPtr(sidedefs(0)), VarPtr(sectors(0)), numsectors, numlinedefs, things(currentselected).arg0, 0, indicatorsize, 0, vertexsize: SectorUndrawn = True
               If (mapconfig("linedeftypes")(CStr(things(currentselected).effect))("mark2") = 1) And (things(currentselected).arg1 > 0) Then Render_TaggedSectors vertexes(0), linedefs(0), VarPtr(sidedefs(0)), VarPtr(sectors(0)), numsectors, numlinedefs, things(currentselected).arg1, 0, indicatorsize, 0, vertexsize: SectorUndrawn = True
               If (mapconfig("linedeftypes")(CStr(things(currentselected).effect))("mark3") = 1) And (things(currentselected).arg2 > 0) Then Render_TaggedSectors vertexes(0), linedefs(0), VarPtr(sidedefs(0)), VarPtr(sectors(0)), numsectors, numlinedefs, things(currentselected).arg2, 0, indicatorsize, 0, vertexsize: SectorUndrawn = True
               If (mapconfig("linedeftypes")(CStr(things(currentselected).effect))("mark4") = 1) And (things(currentselected).arg3 > 0) Then Render_TaggedSectors vertexes(0), linedefs(0), VarPtr(sidedefs(0)), VarPtr(sectors(0)), numsectors, numlinedefs, things(currentselected).arg3, 0, indicatorsize, 0, vertexsize: SectorUndrawn = True
               If (mapconfig("linedeftypes")(CStr(things(currentselected).effect))("mark5") = 1) And (things(currentselected).arg4 > 0) Then Render_TaggedSectors vertexes(0), linedefs(0), VarPtr(sidedefs(0)), VarPtr(sectors(0)), numsectors, numlinedefs, things(currentselected).arg4, 0, indicatorsize, 0, vertexsize: SectorUndrawn = True
               
               'When sector was undawn, redraw all things
               If (SectorUndrawn) Then
                    
                    'Redraw all things
                    Render_AllThings things(0), 0, numthings - 1, ThingBitmapData(0), frmMain.picThings(thingsize).width, frmMain.picThings(thingsize).Height, Val(Config("allthingsrects")), ViewZoom, filterthings, filtersettings
               Else
                    
                    'Check if things are shown as well
                    If (mapconfig("linedeftypes")(CStr(things(currentselected).effect))("mark1") = 2) And (things(currentselected).arg0 > 0) Then Render_TaggedThingsNormal things(0), numthings - 1, things(currentselected).arg0, ThingBitmapData(0), frmMain.picThings(thingsize).width, frmMain.picThings(thingsize).Height, Val(Config("allthingsrects")), ViewZoom, filterthings, filtersettings
                    If (mapconfig("linedeftypes")(CStr(things(currentselected).effect))("mark2") = 2) And (things(currentselected).arg1 > 0) Then Render_TaggedThingsNormal things(0), numthings - 1, things(currentselected).arg1, ThingBitmapData(0), frmMain.picThings(thingsize).width, frmMain.picThings(thingsize).Height, Val(Config("allthingsrects")), ViewZoom, filterthings, filtersettings
                    If (mapconfig("linedeftypes")(CStr(things(currentselected).effect))("mark3") = 2) And (things(currentselected).arg2 > 0) Then Render_TaggedThingsNormal things(0), numthings - 1, things(currentselected).arg2, ThingBitmapData(0), frmMain.picThings(thingsize).width, frmMain.picThings(thingsize).Height, Val(Config("allthingsrects")), ViewZoom, filterthings, filtersettings
                    If (mapconfig("linedeftypes")(CStr(things(currentselected).effect))("mark4") = 2) And (things(currentselected).arg3 > 0) Then Render_TaggedThingsNormal things(0), numthings - 1, things(currentselected).arg3, ThingBitmapData(0), frmMain.picThings(thingsize).width, frmMain.picThings(thingsize).Height, Val(Config("allthingsrects")), ViewZoom, filterthings, filtersettings
                    If (mapconfig("linedeftypes")(CStr(things(currentselected).effect))("mark5") = 2) And (things(currentselected).arg4 > 0) Then Render_TaggedThingsNormal things(0), numthings - 1, things(currentselected).arg4, ThingBitmapData(0), frmMain.picThings(thingsize).width, frmMain.picThings(thingsize).Height, Val(Config("allthingsrects")), ViewZoom, filterthings, filtersettings
                    
                    'Check if thing has a tag
                    If (things(currentselected).Tag <> 0) Then
                                        
                         'Render things that refer to me
                         Render_TaggedArgThingsNormal things(0), numthings, things(currentselected).Tag, 2, ThingBitmapData(0), frmMain.picThings(thingsize).width, frmMain.picThings(thingsize).Height, Val(Config("allthingsrects")), ViewZoom, filterthings, filtersettings
                    End If
               End If
          End If
     End If
     
     'Get the nearest thing and select it if within allowed distance
     nearest = NearestThing(x, y, things(0), numthings, distance, filterthings, filtersettings)
     If (distance <= Config("thingselectdistance") / ViewZoom) Then currentselected = nearest Else currentselected = -1
     
     'Check if selection is made
     If (currentselected > -1) Then
          
          'Check if thing has an action
          If (things(currentselected).effect > 0) Then
               
               'Check if thing has a known effect
               If (mapconfig("linedeftypes").Exists(CStr(things(currentselected).effect)) = True) Then
                    
                    'Render sectors by hexen format
                    If (mapconfig("linedeftypes")(CStr(things(currentselected).effect))("mark1") = 1) And (things(currentselected).arg0 > 0) Then Render_TaggedSectors vertexes(0), linedefs(0), VarPtr(sidedefs(0)), VarPtr(sectors(0)), numsectors, numlinedefs, things(currentselected).arg0, CLR_SECTORTAG, indicatorsize, 0, vertexsize
                    If (mapconfig("linedeftypes")(CStr(things(currentselected).effect))("mark2") = 1) And (things(currentselected).arg1 > 0) Then Render_TaggedSectors vertexes(0), linedefs(0), VarPtr(sidedefs(0)), VarPtr(sectors(0)), numsectors, numlinedefs, things(currentselected).arg1, CLR_SECTORTAG, indicatorsize, 0, vertexsize
                    If (mapconfig("linedeftypes")(CStr(things(currentselected).effect))("mark3") = 1) And (things(currentselected).arg2 > 0) Then Render_TaggedSectors vertexes(0), linedefs(0), VarPtr(sidedefs(0)), VarPtr(sectors(0)), numsectors, numlinedefs, things(currentselected).arg2, CLR_SECTORTAG, indicatorsize, 0, vertexsize
                    If (mapconfig("linedeftypes")(CStr(things(currentselected).effect))("mark4") = 1) And (things(currentselected).arg3 > 0) Then Render_TaggedSectors vertexes(0), linedefs(0), VarPtr(sidedefs(0)), VarPtr(sectors(0)), numsectors, numlinedefs, things(currentselected).arg3, CLR_SECTORTAG, indicatorsize, 0, vertexsize
                    If (mapconfig("linedeftypes")(CStr(things(currentselected).effect))("mark5") = 1) And (things(currentselected).arg4 > 0) Then Render_TaggedSectors vertexes(0), linedefs(0), VarPtr(sidedefs(0)), VarPtr(sectors(0)), numsectors, numlinedefs, things(currentselected).arg4, CLR_SECTORTAG, indicatorsize, 0, vertexsize
                    
                    'Render things by hexen format
                    If (mapconfig("linedeftypes")(CStr(things(currentselected).effect))("mark1") = 2) And (things(currentselected).arg0 > 0) Then Render_TaggedThings things(0), numthings - 1, things(currentselected).arg0, ThingBitmapData(0), frmMain.picThings(thingsize).width, frmMain.picThings(thingsize).Height, Val(Config("allthingsrects")), ViewZoom, filterthings, filtersettings
                    If (mapconfig("linedeftypes")(CStr(things(currentselected).effect))("mark2") = 2) And (things(currentselected).arg1 > 0) Then Render_TaggedThings things(0), numthings - 1, things(currentselected).arg1, ThingBitmapData(0), frmMain.picThings(thingsize).width, frmMain.picThings(thingsize).Height, Val(Config("allthingsrects")), ViewZoom, filterthings, filtersettings
                    If (mapconfig("linedeftypes")(CStr(things(currentselected).effect))("mark3") = 2) And (things(currentselected).arg2 > 0) Then Render_TaggedThings things(0), numthings - 1, things(currentselected).arg2, ThingBitmapData(0), frmMain.picThings(thingsize).width, frmMain.picThings(thingsize).Height, Val(Config("allthingsrects")), ViewZoom, filterthings, filtersettings
                    If (mapconfig("linedeftypes")(CStr(things(currentselected).effect))("mark4") = 2) And (things(currentselected).arg3 > 0) Then Render_TaggedThings things(0), numthings - 1, things(currentselected).arg3, ThingBitmapData(0), frmMain.picThings(thingsize).width, frmMain.picThings(thingsize).Height, Val(Config("allthingsrects")), ViewZoom, filterthings, filtersettings
                    If (mapconfig("linedeftypes")(CStr(things(currentselected).effect))("mark5") = 2) And (things(currentselected).arg4 > 0) Then Render_TaggedThings things(0), numthings - 1, things(currentselected).arg4, ThingBitmapData(0), frmMain.picThings(thingsize).width, frmMain.picThings(thingsize).Height, Val(Config("allthingsrects")), ViewZoom, filterthings, filtersettings
               End If
          End If
          
          'Check if thing has a tag
          If (things(currentselected).Tag <> 0) Then
                              
               'Render things that refer to me
               Render_TaggedArgThings things(0), numthings, things(currentselected).Tag, 2, ThingBitmapData(0), frmMain.picThings(thingsize).width, frmMain.picThings(thingsize).Height, Val(Config("allthingsrects")), ViewZoom, filterthings, filtersettings
          End If
          
          'Render the new current selected to selected
          If Config("thingrects") Then Render_BoxSwitched things(currentselected).x, things(currentselected).y, things(currentselected).size * ViewZoom, PAL_THINGSELECTION, (Config("thingrects") - 1), PAL_THINGSELECTION
          Render_Bitmap ThingBitmapData(0), frmMain.picThings(thingsize).width, _
                        frmMain.picThings(thingsize).Height, _
                        things(currentselected).image * frmMain.picThings(thingsize).Height, 0, _
                        frmMain.picThings(thingsize).Height, frmMain.picThings(thingsize).Height, _
                        things(currentselected).x, things(currentselected).y, _
                        CLR_THINGHIGHLIGHT, CLR_BACKGROUND
     End If
     
     'Show the rendered changes
     If (OldSelected <> currentselected) Then picMap.Refresh
     
     'Check if we should show the info
     If (currentselected > -1) Then
          
          'Only update when changed
          If (OldSelected <> currentselected) Then
               
               'Show the info
               ShowThingInfo currentselected
          End If
     Else
          
          'Hide the info
          HideThingInfo
     End If
End Sub

Private Sub ChangeVertexHighlight(ByVal x As Long, ByVal y As Long, Optional ByVal ForceUpdate As Boolean)
     Dim distance As Long
     Dim nearest As Long
     Dim OldSelected As Long
     
     Dim vx As Long, vy As Long
     
     'Keep old selection ifnot forced to update
     If (Not ForceUpdate) Then OldSelected = currentselected Else OldSelected = -1
     
     'Render the last current selected to normal
     If (currentselected > -1) Then Render_AllVertices vertexes(0), currentselected, currentselected, vertexsize
     
     'Get the nearest vertex and select it if within allowed distance
     nearest = NearestVertex(x, y, vertexes(0), numvertexes, distance)
     If (distance <= Config("vertexselectdistance") / ViewZoom) Then currentselected = nearest Else currentselected = -1
     
     'Render the new current selected to selected
     If (currentselected > -1) Then Render_Box vertexes(currentselected).x, vertexes(currentselected).y, vertexsize, CLR_VERTEXHIGHLIGHT, 1, CLR_VERTEXHIGHLIGHT
     
     'Show the rendered changes
     If (OldSelected <> currentselected) Then picMap.Refresh
     
     
     'Check if anything is selected
     If (currentselected > -1) Then
          
          'Only update when changed
          If (OldSelected <> currentselected) Then
               
               'Get information
               vx = vertexes(currentselected).x
               vy = vertexes(currentselected).y
               
               'Coordiantes on Tooltip
               picMap.ToolTipText = ""
               If (Val(Config("showtooltips")) <> 0) Then picMap.ToolTipText = vx & ", " & vy
               
               'Check what panel to show info on
               If picBar.visible Then
                    
                    'Check if the bar is fully shown
                    If (cmdToggleBar.Tag = "0") Then
                         
                         'Set the info
                         fraVertex.Caption = " Vertex " & currentselected & " "
                         lblVertexXY.Caption = vx & ", " & vy
                         
                         'Show the info
                         fraVertex.visible = True
                    Else
                         
                         'Single line only
                         lblBarText.Caption = " Vertex " & currentselected & ":  " & vx & ", " & vy
                    End If
               ElseIf picSBar.visible Then
                    
                    'Check if the bar is fully shown
                    If (cmdToggleBar.Tag = "0") Then
                         
                         'Set the info
                         fraSVertex.Caption = " Vertex " & currentselected & " "
                         lblSVertexXY.Caption = vx & ", " & vy
                         
                         'Show the info
                         fraSVertex.visible = True
                    Else
                         
                         'Single line only
                         lblBarText.Caption = " Vertex " & currentselected & ":  " & vx & ", " & vy
                    End If
               End If
          End If
     Else
          
          'Hide the info
          HideVertexInfo
     End If
End Sub

Private Function CreateSectorHere(ByVal x As Long, ByVal y As Long) As Long

    Dim distance As Single

    'Remove higlight
     RemoveHighlight True
     
     'No more selection
     ResetSelections things(0), numthings, linedefs(0), numlinedefs, vertexes(0), numvertexes, VarPtr(sectors(0)), numsectors
     Set selected = New Dictionary
     numselected = 0
     selectedtype = EM_LINES
     
     'Check if we should snap the center to grid
     If snapmode Then
          
          'Snap X and Y to grid
          x = SnappedToGridX(x)
          y = SnappedToGridY(y)
     End If
     
     'Show indicator
     Render_Line x - 10 / ViewZoom, -y, x + 10 / ViewZoom, -y, CLR_MULTISELECT
     Render_Line x, -y - 10 / ViewZoom, x, -y + 10 / ViewZoom, CLR_MULTISELECT
     picMap.Refresh
     
     'Load the create sector dialog
     Load frmMakeSector
     
     'Set snap option default
     If snapmode Then frmMakeSector.chkSnap.Value = vbChecked
     
     'Show the dialog
     frmMakeSector.Show 1, Me
     
     CreateSectorHere = -1
     
     'Check if not cancelled
     If (frmMakeSector.Tag = "1") Then
     
        'Get diameter
        distance = frmMakeSector.scrDiameter.Value - 0.1
        
        'Make undo
        CreateUndo "sector insert"
        
        CreateSectorHere = CreateSectorHereParams(x, y, frmMakeSector.scrVertices.Value, distance, (vbChecked = frmMakeSector.chkSnap.Value))
        
    End If
        
        
    'Unload dialog
    Unload frmMakeSector
    Set frmMakeSector = Nothing

End Function


Private Sub CreateSubclassing()
     
     'Check if we are allowed to do subclassing
     If (CommandSwitch("-nosubclass") = False) Then
          
          'Keep original messages handler
          OriginalMessageHandler = GetWindowLong(Me.hWnd, GWL_WNDPROC)
          
          'Set our own messages handler
          SetWindowLong Me.hWnd, GWL_WNDPROC, AddressOf MainMessageHandler
     End If
End Sub

Private Sub DestroySubclassing()
     
     'Check if we are allowed to do subclassing
     If (CommandSwitch("-nosubclass") = False) Then
          
          'Restore original messages handler
          SetWindowLong Me.hWnd, GWL_WNDPROC, OriginalMessageHandler
     End If
End Sub

Private Sub DragSelection(ByVal Shift As ShiftConstants, ByVal x As Single, ByVal y As Single)
     Dim i As Long, s As Long
     Dim DragSel As Variant
     Dim ox As Single, oy As Single
     
     If grabobject = -1 Then Exit Sub
     
     'Check if dragging vertices
     If ((mode = EM_LINES) Or (mode = EM_SECTORS) Or (mode = EM_VERTICES)) Then
          
          'Go for dragging selected vertices
          DragSel = dragselected.Items
          For i = LBound(DragSel) To UBound(DragSel)
               
               'Get vertex
               s = DragSel(i)
               
               'Do not move the grabbed vertex yet
               If (s <> grabobject) Then
                    
                    'Calculate offset from this vertex to grabbed vertex
                    ox = vertexes(s).x - vertexes(grabobject).x
                    oy = vertexes(s).y - vertexes(grabobject).y
                    
                    'Move vertex
                    If (snapmode) And ((Shift And vbShiftMask) = 0) Then
                         vertexes(s).x = SnappedToGridX(x - GrabX) + ox
                         vertexes(s).y = SnappedToGridY(-y - GrabY) + oy
                    Else
                         vertexes(s).x = (x - GrabX) + ox
                         vertexes(s).y = (-y - GrabY) + oy
                    End If
               End If
          Next i
          
          'Move the grabbed vertex
          If (snapmode) And ((Shift And vbShiftMask) = 0) Then
               vertexes(grabobject).x = SnappedToGridX(x - GrabX)
               vertexes(grabobject).y = SnappedToGridY(-y - GrabY)
          Else
               vertexes(grabobject).x = x - GrabX
               vertexes(grabobject).y = -y - GrabY
          End If
     Else
          
          'Go for dragging selected things
          DragSel = dragselected.Items
          For i = LBound(DragSel) To UBound(DragSel)
               
               'Get thing
               s = DragSel(i)
               
               'Do not move the grabbed thing yet
               If (s <> grabobject) Then
                    
                    'Calculate offset from this thing to grabbed thing
                    ox = things(s).x - things(grabobject).x
                    oy = things(s).y - things(grabobject).y
                    
                    'Move thing
                    If (snapmode) And ((Shift And vbShiftMask) = 0) Then
                         things(s).x = SnappedToGridX(x - GrabX) + ox
                         things(s).y = SnappedToGridY(-y - GrabY) + oy
                    Else
                         things(s).x = (x - GrabX) + ox
                         things(s).y = (-y - GrabY) + oy
                    End If
                    
                    'Check if this is the 3D start position
                    If (things(s).thing = mapconfig("start3dmode")) Then ApplyPositionFromThing s
               End If
          Next i
          
          'Move the grabbed thing
          If (snapmode) And ((Shift And vbShiftMask) = 0) Then
               things(grabobject).x = SnappedToGridX(x - GrabX)
               things(grabobject).y = SnappedToGridY(-y - GrabY)
          Else
               things(grabobject).x = x - GrabX
               things(grabobject).y = -y - GrabY
          End If
          
          'Check if this is the 3D start position
          If (things(grabobject).thing = mapconfig("start3dmode")) Then ApplyPositionFromThing grabobject
     End If
     
     'Redraw map
     RedrawMap
End Sub

Public Sub DrawVertexHere(ByVal x As Long, ByVal y As Long, Optional ByVal RefreshMap As Boolean = True, Optional ByVal SimpleInsert As Boolean = False, Optional ByVal DCKBox As Boolean = False)
     Dim vdistance As Long
     Dim ldistance As Long
     Dim ld As Long
     Dim nv As Long
     Dim nrv As Long
     Dim ShouldSnap45 As Boolean

     
     ' If we're doing a simple insert, don't mess with the co-ordinates.
     If Not SimpleInsert Then
          DrawingSnap x, y, CurrentShiftMask
     End If
     
     
     ' If we're in DCK box mode, call ourselves four times in *normal* mode,
     ' and then leave.
     If DCKBox Then
     
          If selected.Count > 0 And x <> vertexes(selected.Items(0)).x And y <> -vertexes(selected.Items(0)).y Then
          
               ' Draw second vertex of box in DCK box mode.
               DrawVertexHere x, -vertexes(selected.Items(0)).y, False, True, False
               
               ' Draw third.
               DrawVertexHere x, y, False, True, False
               
               ' Draw fourth.
               DrawVertexHere vertexes(selected.Items(0)).x, y, False, True, False
               
               ' Close box in DCK box mode.
               DrawVertexHere vertexes(selected.Items(0)).x, -vertexes(selected.Items(0)).y, False, True, False
          End If
          
          Exit Sub
     End If
     
     'Record drawing coordinates
     ReDim Preserve DrawingCoords(0 To NumDrawingCoords)
     DrawingCoords(NumDrawingCoords).x = x
     DrawingCoords(NumDrawingCoords).y = y
     NumDrawingCoords = NumDrawingCoords + 1
     
     'No vertex inserted yet
     nv = -1
     
     'Get the nearest vertex
     nrv = NearestVertex(x, y, vertexes(0), numvertexes, vdistance)
     
     'Check if vertex found
     If (nrv > -1) Then
          
          'Check if we should auto-stitch vertices
          If (stitchmode) Or (vertexes(nrv).selected <> 0) Then
               
               'Use this vertex instead of a new one if close enough for stitching
               ' In simple mode, only stitch if we're bang on.
               If vdistance = 0 Or ((Not SimpleInsert) And vdistance <= Config("autostitchdistance")) Then nv = nrv
          End If
     End If
     
     'Check if we should insert a new vertex
     If (nv = -1) Then
          
          'Insert a vertex now
          nv = InsertVertex(x, -y)
          
          'Get the nearest linedef
          ld = NearestLinedef(x, y, vertexes(0), linedefs(0), numlinedefs, ldistance)
          
          'Check if distance is close enough for linedef split
          If (ldistance <= Config("linesplitdistance")) Then
               
               'Split the linedef with this vertex
               SplitLinedef ld, nv
               
               'Redraw the map
               If (RefreshMap) Then RedrawMap False
          End If
     End If
     
     'Select the vertex
     vertexes(nv).selected = 1
     
     'Draw the vertex
     If (RefreshMap) Then Render_AllVertices vertexes(0), nv, nv, vertexsize
     
     'Check if a vertex was previously drawn
     If (numselected > 0) Then
          
          'Insert a linedef
          ld = CreateLinedef
          
          'Set the linedef properties
          With linedefs(ld)
               
               'From previous vertex, to this vertex
               .V1 = selected.Items(selected.Count - 1)
               .V2 = nv
               
               'Set other line properties
               .Flags = LDF_IMPASSIBLE
               .effect = 0
               .Tag = 0
               .selected = 1
               .s1 = -1
               .s2 = -1
          End With
          
          'Add to changing lines
          ReDim Preserve changedlines(0 To numchangedlines)
          changedlines(numchangedlines) = ld
          numchangedlines = numchangedlines + 1
          
          'Update status
          UpdateStatusBar
          
          'Draw the linedef
          If (RefreshMap) Then
               Render_AllLinedefs vertexes(0), linedefs(0), VarPtr(sectors(0)), VarPtr(sidedefs(0)), ld, ld, submode, indicatorsize
               Render_AllVertices vertexes(0), linedefs(ld).V1, linedefs(ld).V1, vertexsize
               Render_AllVertices vertexes(0), linedefs(ld).V2, linedefs(ld).V2, vertexsize
               
               'Render changing linedef lengths
               Render_ChangingLengths vertexes(0), linedefs(0), changedlines(0), numchangedlines, NumbersBitmapData(0), frmMain.picNumbers.width, frmMain.picNumbers.Height, frmMain.picNumbers.width / 10, frmMain.picNumbers.Height
          End If
          
          'Check if the new vertex is same as any drawn vertex
          If (selected.Exists(CStr(nv)) = True) Then
               
               'Refresh
               If (RefreshMap) Then picMap.Refresh
               
               'End drawing mode
               EndDrawOperation True
               
               'Leave now
               Exit Sub
          End If
     Else
          
          'Update status only (for the new vertex)
          UpdateStatusBar
          
     End If
     
     'Add vertex to selection list if not already in there
     If (selected.Exists(CStr(nv)) = False) Then
          selected.Add CStr(nv), nv
          numselected = numselected + 1
     End If
End Sub

Private Sub EndDragOperation()
     Dim result As Boolean
     Dim DraggedSelection As Dictionary
     Dim DraggedSelectionType As ENUM_EDITMODE
     Dim v As Variant
     Dim sx As Single, sy As Single
     
     'Change mousepointer
     Screen.MousePointer = vbHourglass
     
     'Check if we were dragging vertices
     If ((mode = EM_LINES) Or (mode = EM_SECTORS) Or (mode = EM_VERTICES)) Then
          
          'Round all vertices
          RoundVertices vertexes(0), numvertexes
          
          'When user was pasting, replace -1 sectors with their parent
          If (submode = ESM_PASTING) Then ApplyParentSectors
          
          'Check if sector heights must be adjusted
          If (submode = ESM_PASTING) And (PrefabAdjustHeights = True) And _
             ((mode = EM_LINES) Or (mode = EM_SECTORS)) Then
               
               'Check if user wants sector heights to be adjusted
               If (Val(Config("pasteadjustsheights"))) Then
                    
                    'Check if selection must be converted
                    If (mode = EM_LINES) Then
                         
                         'Keep original selection
                         Set DraggedSelection = selected
                         DraggedSelectionType = selectedtype
                         
                         'Select dragged sectors only
                         SelectSectorsFromLinedefs True
                    End If
                    
                    'Apply heights adjustments
                    ApplySectorHeightAdjustments
                    
                    'Check if selection must be restored
                    If (mode = EM_LINES) Then
                         
                         'Restore original selection
                         Set selected = DraggedSelection
                         numselected = selected.Count
                         selectedtype = DraggedSelectionType
                    End If
               End If
          End If
          
          'Check if we should auto-stitch vertices
          If (stitchmode) Then
               
               'Make undo
               If (Config("subundos") = vbChecked) Then CreateUndo "stitch vertices"
               
               'Find all changing lines
               FindChangingLines True, True
               
               'Split linedefs with overlapping vertices
               result = SelectedVerticesSplitLinedefs
               
               'Auto-stich vertices
               result = result Or AutoStitchDraggedSelection
               
               'Check result
               If (result) Then
                    
                    'Remove looped linedefs
                    RemoveLoopedLinedefs
                    
                    'Find all changing lines
                    FindChangingLines True, True
                    
                    'Due to auto-stitch, linedefs could be overlapping
                    'Combine these into one now
                    MergeDoubleLinedefs
                    
                    ' Reset the vertex selection now, if we were forced to
                    ' preserve it despite being in ld or sec mode.
                    ResetSelections things(0), 0, linedefs(0), 0, vertexes(0), numvertexes, VarPtr(sectors(0)), 0
               Else
                    
                    'No stitching was done, no undo needed
                    If (Config("subundos") = vbChecked) Then WithdrawUndo
               End If
          End If
          
          'Fix glitches that other routines may cause
          SolveGlitches
          
          'When dragging lines or sector, deselect all vertices
          'that were temporarely selected for dragging
          ResetSelections things(0), 0, linedefs(0), 0, vertexes(0), numvertexes, VarPtr(sectors(0)), 0
          If (mode = EM_VERTICES) Then ReapplyVerticesSelection
          
          'DEBUG
          'DEBUG_FindUnusedSectors
     End If
     
     ' Change sector references if lines dragged between sectors.
     ' Decidedly experimental!
     ' Hey! If the sector's selected, it's fine.
     ' Hey again! SelectSectorsFromLinedefs doesn't do what you want here!
     'If (mode = EM_SECTORS Or mode = EM_LINES Or mode = EM_VERTICES) Then
     '
     '     Select Case mode
     '     Case EM_SECTORS
     '          SelectLinedefsFromSectors
     '     Case EM_LINES
     '          SelectSectorsFromLinedefs
     '          SelectLinedefsFromSectors
     '     Case EM_VERTICES
     '          SelectLinedefsFromVertices
     '          SelectSectorsFromLinedefs
     '          SelectLinedefsFromSectors
     '     End Select
     '
     '     For Each v In selected.Keys
     '          If linedefs(v).s1 >= 0 Then
     '               GetLineSideSpot v, 0.5, True, sx, sy
     '               NearestUnselectedLinedef
     '               sidedefs(linedefs(v).s1).sector = IntersectSector(
     '
     '
     'End If
     
     'Check if we should deselect
     If (DeselectAfterEdit) Then RemoveSelection False
     
     'End of drag operation, set mode back to normal
     submode = ESM_NONE
     
     'Map was changed
     mapnodeschanged = True
     mapchanged = True
     
     'We dont need these anymore
     ReDim changedlines(0)
     numchangedlines = 0
     
     'Redraw the map
     RedrawMap
     
     'Show highlight
     ShowHighlight LastX, LastY
     
     'Update status
     UpdateStatusBar
     
     'Reset mousepointer
     Screen.MousePointer = vbNormal
End Sub

' Now we return the index of the new sector.
Public Sub EndDrawOperation(ByVal NewSector As Boolean)
     Dim sc1 As Long
     Dim sc2 As Long
     Dim ns As Long, os As Long
     Dim selectsectorindex As Long
     Dim isrelatedline As Long
     Dim ld As Long
     Dim sx As Single, sy As Single
     'Dim sxOn As Single, syOn As Single
     Dim sxSide As Single, sySide As Single
     Dim Indices As Variant
     Dim i As Long
     Dim CopyFromSidedef As Long
     Dim DefaultTexture As Dictionary
     
     
     'Change mousepointer
     Screen.MousePointer = vbHourglass
     
     'Go for all lines to make a new selection list
     'so the selection list reflects the drawn lines
     Set selected = New Dictionary
     For ld = 0 To (numlinedefs - 1)
          
          'Check if selected
          If (linedefs(ld).selected <> 0) Then
               
               'Normal selection
               linedefs(ld).selected = 1
               
               'Add to list
               selected.Add CStr(ld), ld
               
          End If
     Next ld
     
     'Count selected linedefs
     numselected = selected.Count
     selectedtype = EM_LINES
     
     'Do not copy properties from
     'any sidedef/sector yet
     CopyFromSidedef = -1
     
     ' Assume we haven't created a new sector.
     ns = -1
     
     'Check if any lines were drawn
     If (numselected > 0) Then
          
          ' Get two points on front side of the line: one certainly in the sector, and one
          ' not necessarily so, but integral.
          GetLineSideSpot selected.Items(0), 0.5, True, sx, sy
          GetLineSideSpot selected.Items(0), 1, True, sxSide, sySide
          
          'Get the sector in which was drawn
          os = IntersectSectorFloat(sx, sy, vertexes(0), linedefs(0), VarPtr(sidedefs(0)), numlinedefs, 1)
          sc2 = os
          
          'Check if we should create a sector
          If (NewSector) Then
               
               'Check if drawn counterclockwise
               If Not point_in_polygon(selected.Items, selected.Count, sx, -sy) Then
                    
                    'Get a spot on the other side to ensure having a spot inside
                    GetLineSideSpot selected.Items(0), 0.5, False, sx, sy
                    
                    'Re-get the sector in which was drawn
                    'Added 6-5-2005
                    os = IntersectSectorFloat(sx, sy, vertexes(0), linedefs(0), VarPtr(sidedefs(0)), numlinedefs, 1)
                    sc2 = os
                    
                    'Check if a parent sector exists
                    If (sc2 > -1) Then
                         
                         'sc1 will be the parent sector
                         sc1 = sc2
                         
                         'Make new sector at sc2
                         ns = CreateSector
                         sc2 = ns
                         
                         'Copy sector properties
                         sectors(sc2) = sectors(sc1)
                    Else
                         
                         'Go for all selected linedefs to flip
                         Indices = selected.Items
                         For ld = 0 To (numselected - 1)
                              
                              'Flip this linedef
                              FlipLinedefVertices Indices(ld)
                         Next ld
                         
                         'Make new sector at sc1
                         ns = CreateSector
                         sc1 = ns
                    End If
               Else
                    
                    'Make sector at sc1
                    ns = CreateSector
                    sc1 = ns
               End If
               
               'Select the new sector for editing
               selectsectorindex = ns
               
               'Check if we should copy properties from outer sector
               If (sc2 > -1) Then
                    
                    'Copy sector properties
                    sectors(sc1) = sectors(sc2)
               Else
                    
                    'Get index of first adjoining sidedef if any
                    CopyFromSidedef = FindAdjoiningSidedef(sxSide, sySide)
                    
                    'Check if there is an adjacent line to copy properties from
                    If (CopyFromSidedef > -1) Then
                         
                         'Copy sector properties from adjoining sector
                         sectors(sc1) = sectors(sidedefs(CopyFromSidedef).sector)
                         
                         'Check if we should erase tag and actions
                         If (Config("copytagdraw") = vbUnchecked) Then
                              With sectors(sc1)
                                   .special = 0
                                   .Tag = 0
                              End With
                         End If
                    Else
                         
                         'Set sector properties to defaults
                         With sectors(sc1)
                              If (Val(Config("storeeditinginfo"))) And WadSettings.Exists("defaultsector") Then
                                   .Brightness = WadSettings("defaultsector")("brightness")
                                   .hceiling = WadSettings("defaultsector")("hceiling")
                                   .HFloor = WadSettings("defaultsector")("hfloor")
                                   .tceiling = UCase$(WadSettings("defaultsector")("tceiling"))
                                   .TFloor = UCase$(WadSettings("defaultsector")("tfloor"))
                              Else
                                   .Brightness = Config("defaultsector")("brightness")
                                   .hceiling = Config("defaultsector")("hceiling")
                                   .HFloor = Config("defaultsector")("hfloor")
                                   .tceiling = UCase$(Config("defaultsector")("tceiling"))
                                   .TFloor = UCase$(Config("defaultsector")("tfloor"))
                              End If
                              .selected = 0
                              .special = 0
                              .Tag = 0
                         End With
                    End If
               End If
               
               'Setup the new lines
               SetupNewLinedefs sc1, sc2, CopyFromSidedef
               
               'All other lines (unselected) referring to the old sector must
               'be tested if inside new polygon and changed as needed
               
               'Check if we should auto-stitch vertices
               If (stitchmode) Then
                    
                    'Make dragged selection same as current selection
                    Set dragselected = selected
                    dragnumselected = numselected
                    
                    'Find and keep lines that have changed (added)
                    FindChangingLines True, True
                    
                    'Due to auto-stitch, linedefs could be overlapping
                    'Combine these into one now
                    MergeDoubleLinedefs
               End If
               
               'Go for all path linedefs
               For ld = 0 To (numlinedefs - 1)
                    
                    'Check if unselected
                    If (linedefs(ld).selected = 0) Then
                         
                         'Check sectors of the line
                         isrelatedline = False
                         If (linedefs(ld).s1 > -1) Then isrelatedline = (sidedefs(linedefs(ld).s1).sector = os) Else isrelatedline = (os = -1)
                         If (linedefs(ld).s2 > -1) Then isrelatedline = isrelatedline Or (sidedefs(linedefs(ld).s2).sector = os) Else isrelatedline = isrelatedline Or (os = -1)
                         
                         'Check if line is related to old sector
                         If isrelatedline Then
                              
                              'Get spot point on middle of the line
                              GetLineSideSpot ld, 0, True, sx, sy
                              
                              'Check if inside new sector
                              If point_in_polygon(selected.Items, numselected, sx, -sy) Then

                                   'Check if line has a front side
                                   If (linedefs(ld).s1 > -1) Then
                                        
                                        'Change sector if this refers to the old sector
                                        If (sidedefs(linedefs(ld).s1).sector = os) Then sidedefs(linedefs(ld).s1).sector = ns
                                        
                                   'Check if old sector is void
                                   ElseIf (os = -1) Then
                                        
                                        'Add sidedef
                                        linedefs(ld).s1 = CreateSidedef
                                        
                                        'Apply building defaults on sidedef
                                        If (Val(Config("storeeditinginfo"))) And WadSettings.Exists("defaulttexture") Then
                                             Set DefaultTexture = WadSettings("defaulttexture")
                                        Else
                                             Set DefaultTexture = Config("defaulttexture")
                                        End If
                                        
                                        With sidedefs(linedefs(ld).s1)
                                             .linedef = ld
                                             .Lower = UCase$(DefaultTexture("lower"))
                                             If (linedefs(ld).s2 = -1) Then .Middle = UCase$(DefaultTexture("middle")) Else .Middle = "-"
                                             .Upper = UCase$(DefaultTexture("upper"))
                                             .sector = ns
                                             .tx = 0
                                             .ty = 0
                                        End With
                                        
                                        'Line becomes doublesided?
                                        If (linedefs(ld).s2 > -1) Then
                                             sidedefs(linedefs(ld).s2).Middle = "-"
                                             linedefs(ld).Flags = linedefs(ld).Flags Or LDF_TWOSIDED
                                             linedefs(ld).Flags = linedefs(ld).Flags And Not LDF_IMPASSIBLE
                                        End If
                                   End If
                                   
                                   'Check if line has a back side
                                   If (linedefs(ld).s2 > -1) Then
                                        
                                        'Change sector if this refers to the old sector
                                        If (sidedefs(linedefs(ld).s2).sector = os) Then sidedefs(linedefs(ld).s2).sector = ns
                                        
                                   'Check if old sector is void
                                   ElseIf (os = -1) Then
                                        
                                        'Add sidedef
                                        linedefs(ld).s2 = CreateSidedef
                                        
                                        'Apply building defaults on sidedef
                                        If (Val(Config("storeeditinginfo"))) And WadSettings.Exists("defaulttexture") Then
                                             Set DefaultTexture = WadSettings("defaulttexture")
                                        Else
                                             Set DefaultTexture = Config("defaulttexture")
                                        End If
                                        
                                        With sidedefs(linedefs(ld).s2)
                                             .linedef = ld
                                             .Lower = UCase$(DefaultTexture("lower"))
                                             If (linedefs(ld).s1 = -1) Then .Middle = UCase$(DefaultTexture("middle")) Else .Middle = "-"
                                             .Upper = UCase$(DefaultTexture("upper"))
                                             .sector = ns
                                             .tx = 0
                                             .ty = 0
                                        End With
                                        
                                        'Line becomes doublesided?
                                        If (linedefs(ld).s1 > -1) Then
                                             sidedefs(linedefs(ld).s1).Middle = "-"
                                             linedefs(ld).Flags = linedefs(ld).Flags Or LDF_TWOSIDED
                                             linedefs(ld).Flags = linedefs(ld).Flags And Not LDF_IMPASSIBLE
                                        End If
                                   End If
                              End If
                         End If
                    End If
               Next ld
          Else
               
               'Check if inside a sector
               If (sc2 > -1) Then
                    
                    'Do a trace from one vertex to the other
                    ReDim SectorSplitLinesList(0)
                    SectorSplitNumLines = 0
                    TerminateRecursion = False
                    TraceSectorSplitVertex linedefs(selected.Items(0)).V1, linedefs(selected.Items(numselected - 1)).V2, sc2, SectorSplitLinesList(), 0
                    
                    'Remove line selection properties
                    ResetSelections things(0), 0, linedefs(0), numlinedefs, vertexes(0), 0, VarPtr(sectors(0)), 0
                    
                    'Check if a path was found
                    If (SectorSplitNumLines > 0) Then
                         
                         'We create a new sector
                         NewSector = True
                         
                         'Allocate memory for drawn lines into this array
                         ReDim Preserve SectorSplitLinesList(0 To SectorSplitNumLines + numselected - 1)
                         
                         'Go for all drawn linedefs
                         Indices = selected.Items
                         For i = 0 To (numselected - 1)
                              
                              'Add to the array to create a closed polygon
                              SectorSplitLinesList(SectorSplitNumLines + i) = Indices(i)
                              
                              'Select linedef
                              linedefs(Indices(i)).selected = 1
                         Next i
                         
                         'Check what side of line will be the new sector
                         If point_in_polygon(SectorSplitLinesList(), SectorSplitNumLines + numselected, sx, -sy) Then
                              
                              'Create new sector on front side
                              ns = CreateSector
                              sc1 = ns
                              
                              'Select the new sector for editing
                              selectsectorindex = ns
                              
                              'Copy sector properties
                              sectors(sc1) = sectors(sc2)
                         Else
                              
                              'sc1 will be the parent sector
                              sc1 = sc2
                              
                              'Create new sector on back side
                              ns = CreateSector
                              sc2 = ns
                              
                              'Select the old sector for editing
                              selectsectorindex = os
                              
                              'Copy sector properties
                              sectors(sc2) = sectors(sc1)
                         End If
                         
                         'All path lines referring to the old sector must
                         'now refer to the new created sector
                         
                         'Go for all path linedefs
                         For i = 0 To (SectorSplitNumLines - 1)
                              
                              'Get linedef
                              ld = SectorSplitLinesList(i)
                              
                              'Select linedef
                              linedefs(ld).selected = 1
                              
                              'Get spot point on front side of the line
                              GetLineSideSpot ld, 0.5, True, sx, sy
                              
                              'Check what side of the line is in the new polygon
                              If point_in_polygon(SectorSplitLinesList(), SectorSplitNumLines + numselected, sx, -sy) Then
                                   
                                   'Check if line has a front side
                                   If (linedefs(ld).s1 > -1) Then
                                        
                                        'Change sector if this refers to the old sector
                                        If (sidedefs(linedefs(ld).s1).sector = os) Then sidedefs(linedefs(ld).s1).sector = ns
                                   End If
                              Else
                                   
                                   'Check if line has a back side
                                   If (linedefs(ld).s2 > -1) Then
                                        
                                        'Change sector if this refers to the old sector
                                        If (sidedefs(linedefs(ld).s2).sector = os) Then sidedefs(linedefs(ld).s2).sector = ns
                                   End If
                              End If
                         Next i
                         
                         'Setup the new lines
                         SetupNewLinedefs sc1, sc2, CopyFromSidedef
                         
                         'All other lines (unselected) referring to the old sector must
                         'be tested if inside new polygon and changed as needed
                         
                         'Check if we should auto-stitch vertices
                         If (stitchmode) Then
                              
                              'Make dragged selection same as current selection
                              Set dragselected = selected
                              dragnumselected = numselected
                              
                              'Find and keep lines that have changed (added)
                              FindChangingLines True, True
                              
                              'Due to auto-stitch, linedefs could be overlapping
                              'Combine these into one now
                              MergeDoubleLinedefs
                         End If
                         
                         'Go for all path linedefs
                         For ld = 0 To (numlinedefs - 1)
                              
                              'Check if unselected
                              If (linedefs(ld).selected = 0) Then
                                   
                                   'Check sectors of the line
                                   isrelatedline = False
                                   If (linedefs(ld).s1 > -1) Then isrelatedline = (sidedefs(linedefs(ld).s1).sector = os)
                                   If (linedefs(ld).s2 > -1) Then isrelatedline = isrelatedline Or (sidedefs(linedefs(ld).s2).sector = os)
                                   
                                   'Check if line is related to old sector
                                   If isrelatedline Then
                                        
                                        'Get spot point on middle of the line
                                        GetLineSideSpot ld, 0, True, sx, sy
                                        
                                        'Check if inside new sector
                                        If point_in_polygon(SectorSplitLinesList(), SectorSplitNumLines + numselected, sx, -sy) Then
                                             
                                             'Check if line has a front side
                                             If (linedefs(ld).s1 > -1) Then
                                                  
                                                  'Change sector if this refers to the old sector
                                                  If (sidedefs(linedefs(ld).s1).sector = os) Then sidedefs(linedefs(ld).s1).sector = ns
                                             End If
                                             
                                             'Check if line has a back side
                                             If (linedefs(ld).s2 > -1) Then
                                                  
                                                  'Change sector if this refers to the old sector
                                                  If (sidedefs(linedefs(ld).s2).sector = os) Then sidedefs(linedefs(ld).s2).sector = ns
                                             End If
                                        End If
                                   End If
                              End If
                         Next ld
                    Else
                         
                         'Other side of the line is at the same sector
                         sc1 = sc2
                         
                         'Do not select a specific sector
                         selectsectorindex = -1
                         
                         'Setup the new lines
                         SetupNewLinedefs sc1, sc2, CopyFromSidedef
                    End If
               Else
                    
                    'Other side of the line is at the same sector
                    sc1 = sc2
                    
                    'Do not select a specific sector
                    selectsectorindex = -1
                    
                    'Setup the new lines
                    SetupNewLinedefs sc1, sc2, CopyFromSidedef
               End If
          End If
          
          'Remove line selection properties
          ResetSelections things(0), 0, linedefs(0), numlinedefs, vertexes(0), 0, VarPtr(sectors(0)), 0
          
          'Select a specific sector if given
          If (selectsectorindex > -1) Then
               
               'Select this sector
               SelectSector selectsectorindex
               Set selected = New Dictionary
               selected.Add CStr(selectsectorindex), selectsectorindex
               numselected = selected.Count
               
               'Convert selection to lines
               SelectLinedefsFromSectors
               selectedtype = EM_LINES
          End If
          
          'Setup the new sector
          NewSectorSetup (os = -1)
          
          'Remove looped linedefs
          RemoveLoopedLinedefs
          
          'Fix glitches that other routines may cause
          SolveGlitches
          
          'DEBUG
          'DEBUG_FindUnusedSectors
     End If
     
     'Deselect all
     RemoveSelection False
     
     'Rename the undo description
     If (NewSector) Then
          RenameUndo "sector draw"
     Else
          RenameUndo "line draw"
     End If
     
     'End of drawing operation, set mode back to normal
     submode = ESM_NONE
     
     'Map was changed
     mapnodeschanged = True
     mapchanged = True
     
     'We dont need these anymore
     ReDim changedlines(0)
     numchangedlines = 0
     ReDim DrawingCoords(0)
     NumDrawingCoords = 0
     
     ' Clear shift mask.
     ShiftMaskAtDrawStart = 0

     'Redraw the map
     RedrawMap
     
     'Show highlight
     ShowHighlight LastX, LastY
     
     'Update status
     UpdateStatusBar
     
     'Reset mousepointer
     Screen.MousePointer = vbNormal
     
     ' Set global. Icky, but other ways are too fiddly to be worthwhile.
     NewSec = ns
End Sub


Public Sub EndSelectOperation(ByVal Shift As Integer, ByVal x As Single, ByVal y As Single)
     Dim SelectRect As RECT
     Dim c As Long
     Dim OldSelDict As Dictionary
     Dim Keys As Variant
     Dim i As Long
     
     'Change mousepointer
     Screen.MousePointer = vbHourglass
     
     'Make rectangle from selection
     With SelectRect
          If (GrabX > x) Then .left = x: .right = GrabX Else .left = GrabX: .right = x
          If (-GrabY > -y) Then .top = -y: .bottom = -GrabY Else .top = -GrabY: .bottom = -y
     End With
     
     'Check if we select vertices or things
     If (mode = EM_THINGS) Then
          
          'Select things in rectangle
          c = SelectThingsFromRect(SelectRect, (Shift And vbShiftMask) Or (Shift And vbCtrlMask))
     Else
          
          ' Preserve the old selection dictionary. The selected flags will never be touched.
          If (mode = EM_LINES) Or (mode = EM_SECTORS) Then
               Set OldSelDict = selected
               Set selected = New Dictionary
          End If
          
          'Select vertices in rectangle
          c = SelectVerticesFromRect(SelectRect, (Shift And vbShiftMask) Or (Shift And vbCtrlMask))
          
          'Check if we should "upgrade" selection to lines
          If (mode = EM_LINES) Then
               
               'Upgrade to lines
               SelectLinedefsFromVertices
               
          'Check if we should "upgrade" selection to sectors
          ElseIf (mode = EM_SECTORS) Then
               
               'Upgrade to lines, then to sectors
               SelectLinedefsFromVertices
               SelectSectorsFromLinedefs
          End If
          
          ' Add old lines or sectors, if desired.
          If ((mode = EM_LINES) Or (mode = EM_SECTORS)) And Not ((Config("additiveselect") = vbUnchecked) And (Shift = False)) Then
               Keys = OldSelDict.Keys
               For i = LBound(Keys) To UBound(Keys)
                    If Not selected.Exists(Keys(i)) Then selected.Add Keys(i), OldSelDict(Keys(i))
               Next i
          End If
          
          numselected = selected.Count
          
     End If
     
     'Deselect if preferred
     If (c = 0) And (Config("nothingdeselects") = vbChecked) Then RemoveSelection True
     
     'End of select operation, set mode back to normal
     submode = ESM_NONE
     
     'Redraw the map
     RedrawMap
     
     'Show highlight
     ShowHighlight x, y, True
     
     'Update status
     UpdateStatusBar
     
     'Reset mousepointer
     Screen.MousePointer = vbNormal
End Sub


Public Sub HideFOFInfo()
     
     'Hide the info
     fraSector.visible = False
     fraSectorCeiling.visible = False
     fraSectorFloor.visible = False
     fraBackSidedef.visible = False
     fraBackSidedef.Caption = " Back Side "
     fraSSector.visible = False
     fraSSectorCeiling.visible = False
     fraSSectorFloor.visible = False
     fraSBackSidedef.visible = False
     fraSBackSidedef.Caption = " Back Side "
     picMap.ToolTipText = ""
     lblBarText.Caption = ""
     Set imgCeiling.Picture = Nothing
     Set imgFloor.Picture = Nothing
     Set imgSCeiling.Picture = Nothing
     Set imgSFloor.Picture = Nothing
End Sub

Public Sub HideLinedefInfo()
     
     'Hide the info
     fraLinedef.visible = False
     fraFrontSidedef.visible = False
     fraBackSidedef.visible = False
     fraSLinedef.visible = False
     fraSFrontSidedef.visible = False
     fraSBackSidedef.visible = False
     picMap.ToolTipText = ""
     lblBarText.Caption = ""
     Set imgS1Lower.Picture = Nothing
     Set imgS1Middle.Picture = Nothing
     Set imgS1Upper.Picture = Nothing
     Set imgS2Lower.Picture = Nothing
     Set imgS2Middle.Picture = Nothing
     Set imgS2Upper.Picture = Nothing
     Set imgSS1Lower.Picture = Nothing
     Set imgSS1Middle.Picture = Nothing
     Set imgSS1Upper.Picture = Nothing
     Set imgSS2Lower.Picture = Nothing
     Set imgSS2Middle.Picture = Nothing
     Set imgSS2Upper.Picture = Nothing
End Sub

Public Sub HideSectorInfo()
     
     'Hide the info
     fraSector.visible = False
     fraSectorCeiling.visible = False
     fraSectorFloor.visible = False
     fraSSector.visible = False
     fraSSectorCeiling.visible = False
     fraSSectorFloor.visible = False
     picMap.ToolTipText = ""
     lblBarText.Caption = ""
     Set imgCeiling.Picture = Nothing
     Set imgFloor.Picture = Nothing
     Set imgSCeiling.Picture = Nothing
     Set imgSFloor.Picture = Nothing
End Sub


Public Sub HideThingInfo()
     
     'Hide the info
     fraThing.visible = False
     fraSThing.visible = False
     fraThingPreview.visible = False
     fraSThingPreview.visible = False
     picMap.ToolTipText = ""
     lblBarText.Caption = ""
     Set imgThing.Picture = Nothing
     Set imgSThing.Picture = Nothing
End Sub

Public Sub HideVertexInfo()
     
     'Hide the info
     fraVertex.visible = False
     fraSVertex.visible = False
     picMap.ToolTipText = ""
     lblBarText.Caption = ""
End Sub

Private Sub InfoBarClose()
     
     'Close the bar
     Select Case Val(Config("detailsbar"))
          
          Case 1:   'Bottom
               picBar.Align = vbAlignNone
               picBar.Height = cmdToggleBar.Height
               picBar.Align = vbAlignBottom
               cmdToggleBar.Caption = "5"
               lblBarText.visible = True
               
          Case 2:   'Top
               picBar.Align = vbAlignNone
               picBar.Height = cmdToggleBar.Height
               picBar.Align = vbAlignTop
               cmdToggleBar.Caption = "6"
               lblBarText.visible = True
               
               'Change view so that the map doesnt appear to move
               If (mapfile <> "") Then ChangeView ViewLeft, ViewTop - (106 - cmdToggleBar.Height) / ViewZoom, ViewZoom
               
          Case 3:   'Left
               picSBar.Align = vbAlignNone
               picSBar.width = cmdToggleSBar.width
               picSBar.Align = vbAlignLeft
               cmdToggleSBar.Caption = "4"
               
               'Change view so that the map doesnt appear to move
               If (mapfile <> "") Then ChangeView ViewLeft - (135 - cmdToggleSBar.width) / ViewZoom, ViewTop, ViewZoom
               
          Case 4:   'Right
               picSBar.Align = vbAlignNone
               picSBar.width = cmdToggleSBar.width
               picSBar.Align = vbAlignRight
               cmdToggleSBar.Caption = "3"
               
     End Select
     
     'Closed
     cmdToggleBar.Tag = "1"
End Sub

Private Sub InfoBarOpen()
     
     'Open the bar
     Select Case Val(Config("detailsbar"))
          
          Case 1:   'Bottom
               picBar.Align = vbAlignNone
               picBar.Height = 106
               picBar.Align = vbAlignBottom
               cmdToggleBar.Caption = "6"
               lblBarText.visible = False
               
          Case 2:   'Top
               picBar.Align = vbAlignNone
               picBar.Height = 106
               picBar.Align = vbAlignTop
               cmdToggleBar.Caption = "5"
               lblBarText.visible = False
               
               'Change view so that the map doesnt appear to move
               If (mapfile <> "") Then ChangeView ViewLeft, ViewTop + (106 - cmdToggleBar.Height) / ViewZoom, ViewZoom
               
          Case 3:   'Left
               picSBar.Align = vbAlignNone
               picSBar.width = 135
               picSBar.Align = vbAlignLeft
               cmdToggleSBar.Caption = "3"
               
               'Change view so that the map doesnt appear to move
               If (mapfile <> "") Then ChangeView ViewLeft + (135 - cmdToggleSBar.width) / ViewZoom, ViewTop, ViewZoom
               
          Case 4:   'Right
               picSBar.Align = vbAlignNone
               picSBar.width = 135
               picSBar.Align = vbAlignRight
               cmdToggleSBar.Caption = "4"
               
     End Select
     
     'Opened
     cmdToggleBar.Tag = "0"
End Sub

Public Sub InfoBarToggle()
     
     'Tag of cmdToggleBar tells in what state the bar is
     '0 = Open
     '1 = Closed
     
     'Toggle bar
     If (Val(cmdToggleBar.Tag) = 0) Then InfoBarClose Else InfoBarOpen
     
     'Save status
     Config("togglebar") = Val(cmdToggleBar.Tag)
     
     'Remove highlight
     RemoveHighlight True
     
     'Call form resize to adjust sizes
     Form_Resize
     
     'Get rid of focus
     On Error Resume Next
     picSBar.SetFocus
     picBar.SetFocus
End Sub

Private Sub itmFileExportPicture_Click()
     Dim result As String
     Dim FilterIndex As Long
     Dim GDIBitmap As clsGDIBitmap
     
     'Leave when map edit is disabled
     If (picMap.Enabled = False) Then Exit Sub
     
     'Ensure the splash dialog is gone
     Unload frmSplash: Set frmSplash = Nothing
     
     'Cancel if in drawing operation
     CancelCurrentOperation
     
     'Show save dialog
     result = SaveFile(Me.hWnd, "Export Picture As", "Windows Bitmap   *.bmp|*.bmp|Portable Network Graphic   *.png|*.png|JPEG Image   *.jpg|*.jpg", mapfilename, cdlOFNExplorer Or cdlOFNHideReadOnly Or cdlOFNLongNames Or cdlOFNPathMustExist Or cdlOFNOverwritePrompt, FilterIndex)
     frmMain.Refresh
     
     'Check if not cancelled
     If result <> "" Then
          
          'Add extension
          If (LCase$(right$(result, 4)) <> ".bmp") And (FilterIndex = 1) Then result = result & ".bmp"
          If (LCase$(right$(result, 4)) <> ".png") And (FilterIndex = 2) Then result = result & ".png"
          If (LCase$(right$(result, 4)) <> ".jpg") And (FilterIndex = 3) Then result = result & ".jpg"
          
          'Give export dialog
          Load frmExportPicture
          
          'Show dialog
          frmExportPicture.Show 1, Me
          
          'Check result
          If (frmExportPicture.Tag = "OK") Then
               
               'Hourglass
               Screen.MousePointer = vbHourglass
               
               'Make the picture
               If RenderExportPicture() Then
                    
                    'Make GDI+ bitmap from picture
                    Set GDIBitmap = New clsGDIBitmap
                    GDIBitmap.CreateFromHBitmap picTexture.Picture.Handle
                    
                    'Kill file if already exists
                    If (Dir(result) <> "") Then Kill result
                    
                    'Convert and save the bitmap
                    Select Case FilterIndex
                         Case 1: GDIBitmap.SaveToFile result, GDIBitmap.EncoderGuid(GDIBitmap.ExtensionExists("*.bmp")), 0
                         Case 2: GDIBitmap.SaveToFile result, GDIBitmap.EncoderGuid(GDIBitmap.ExtensionExists("*.png")), 0
                         Case 3: GDIBitmap.SaveToFile result, GDIBitmap.EncoderGuid(GDIBitmap.ExtensionExists("*.jpg")), 0
                    End Select
                    
                    'Clean up
                    Set GDIBitmap = Nothing
               End If
               
               'Normal mouse
               Screen.MousePointer = vbNormal
          End If
          
          'Unload dialog
          Unload frmExportPicture
          Set frmExportPicture = Nothing
     End If
End Sub

Private Sub RevertDrawingOperation()
     Dim RecordedCoords() As POINT
     Dim NumRecordedCoords As Long
     Dim i As Long
     Dim oldsnap As Boolean
     
     'Copy the recorded coordinates
     RecordedCoords = DrawingCoords
     NumRecordedCoords = NumDrawingCoords
     
     'Undo complete drawing operation
     CancelDrawOperation
     
     'Restart drawing
     StartDrawOperation False
     
     'Anything to draw?
     If (NumRecordedCoords > 0) Then
          
          'Insert vertices at all recorded coords, except the last
          For i = 0 To NumRecordedCoords - 2
          
               ' Temporarily turn off snapping.
               oldsnap = snapmode
               snapmode = False
               
               'Repeat this vertex draw
               DrawVertexHere RecordedCoords(i).x, RecordedCoords(i).y, False
               
               ' Restore snapping.
               snapmode = oldsnap
          Next i
     End If
     
     'Redraw map
     RedrawMap
     
     'Render the line being drawn
     RenderNewDrawingLine LastX, LastY
     
     'Show changes
     picMap.Refresh
End Sub


Public Sub ShowFOFInfo(ByVal fofld As Long, ByVal CtrlSector As Long)
     Dim nearest As Long
     Dim OldSelected As Long
     Dim ld As Long, ldfound As Long
     
     Dim effect As String
     Dim Ceiling As Long
     Dim Floor As Long
     Dim sTag As Long
     Dim sHeight As Long
     Dim Brightness As Long
     Dim tceiling As String
     Dim TFloor As String
     Dim tLower As String, tMiddle As String, tUpper As String
     
     'Get the information
     Ceiling = sectors(CtrlSector).hceiling
     Floor = sectors(CtrlSector).HFloor
     sTag = linedefs(fofld).Tag
     sHeight = sectors(CtrlSector).hceiling - sectors(CtrlSector).HFloor
     Brightness = sectors(CtrlSector).Brightness
     tceiling = sectors(CtrlSector).tceiling
     TFloor = sectors(CtrlSector).TFloor
     tLower = sidedefs(linedefs(fofld).s1).Lower
     tMiddle = sidedefs(linedefs(fofld).s1).Middle
     tUpper = sidedefs(linedefs(fofld).s1).Upper
     
     'Check if the FOF effect can be found
     If (Trim$(mapconfig("linedeftypes").Exists(CStr(linedefs(fofld).effect)))) Then
          effect = linedefs(fofld).effect & " - " & Trim$(mapconfig("linedeftypes")(CStr(linedefs(fofld).effect))("title"))
     Else
          effect = linedefs(fofld).effect & " - Unknown"
     End If
     
     'Effect on Tooltip
     picMap.ToolTipText = ""
     If (linedefs(fofld).effect > 0) And (Val(Config("showtooltips")) <> 0) Then
          
          'Only when not in 3D Mode
          If (mode <> EM_3D) Then picMap.ToolTipText = effect
     End If
     
     'Check what panel to show the info on
     If picBar.visible Then
          
          'Check if the bar is fully shown
          If (cmdToggleBar.Tag = "0") Then
               
               'Set the info
               fraSector.Caption = " FOF (linedef " & fofld & ") "
               lblSectorType = ShortedText(effect, lblSectorType.width \ Screen.TwipsPerPixelX)
               lblSectorTag.Caption = sTag
               lblSectorCeiling.Caption = Ceiling
               lblSectorFloor.Caption = Floor
               lblSectorHeight.Caption = sHeight
               lblSectorLight.Caption = Brightness
               lblCeiling.Caption = tceiling
               lblFloor.Caption = TFloor
               lblS2Lower.Caption = tLower
               lblS2Middle.Caption = tMiddle
               lblS2Upper.Caption = tUpper
               GetScaledFlatPicture tceiling, imgCeiling
               GetScaledFlatPicture TFloor, imgFloor
               GetScaledTexturePicture tLower, imgS2Lower
               GetScaledTexturePicture tMiddle, imgS2Middle
               GetScaledTexturePicture tUpper, imgS2Upper
               
               
               'Show the info
               fraSector.visible = True
               fraSectorCeiling.visible = True
               fraSectorFloor.visible = True
               fraBackSidedef.Caption = " Control Sidedef "
               fraBackSidedef.visible = True      ' Although we use s1, this is the one.
          Else
               
               'Show single line only
               lblBarText.Caption = " FOF - " & effect
          End If
          
     ElseIf picSBar.visible Then
          
          'Check if the bar is fully shown
          If (cmdToggleBar.Tag = "0") Then
               
               'Set the info
               fraSSector.Caption = " FOF (linedef " & fofld & ") "
               lblSSectorType = ShortedText(effect, lblSSectorType.width \ Screen.TwipsPerPixelX)
               lblSSectorTag.Caption = sTag
               lblSSectorCeiling.Caption = Ceiling
               lblSSectorFloor.Caption = Floor
               lblSSectorHeight.Caption = sHeight
               lblSSectorLight.Caption = Brightness
               lblSCeiling.Caption = tceiling
               lblSFloor.Caption = TFloor
               lblSS2Lower.Caption = tLower
               lblSS2Middle.Caption = tMiddle
               lblSS2Upper.Caption = tUpper
               GetScaledFlatPicture tceiling, imgSCeiling
               GetScaledFlatPicture TFloor, imgSFloor
               GetScaledTexturePicture tLower, imgSS2Lower
               GetScaledTexturePicture tMiddle, imgSS2Middle
               GetScaledTexturePicture tUpper, imgSS2Upper
               
               'Show the info
               fraSSector.visible = True
               fraSSectorCeiling.visible = True
               fraSSectorFloor.visible = True
               fraSBackSidedef.Caption = " Control Sidedef "
               fraSBackSidedef.visible = True      ' Although we use s1, this is the one.
          Else
               
               'Show single line only
               lblBarText.Caption = " FOF - " & effect
          End If
     End If
End Sub


Public Sub ShowLinedefInfo(ByVal ld As Long)

     Dim lddi As LDDISPINFO
     Dim texflags As TEXREQFLAGS
     Dim dispflags As LDDISPLAY
     Dim otherld As Long
     Dim vc As POINT, vm As POINT, vy As POINT
     Dim AngleText As String
     Dim c2, m2, y2 As Long
     
     ' Begin by assuming we show everything.
     dispflags = LDD_ALL

     ' Get information
     lddi.Tag = linedefs(ld).Tag
     
     ' Calculate linedef length
     lddi.Length = LinedefLength(ld)
     
     ' Make ld special text.
     lddi.special = MakeNiceActionText(linedefs(ld).effect)
     
     'Action on Tooltip
     picMap.ToolTipText = ""
     If (mode <> EM_3D) And (Val(Config("showtooltips")) <> 0) Then

          If selected.Count = 1 Then
               otherld = selected(selected.Keys(0))
               If otherld <> ld And _
                 (linedefs(otherld).V1 = linedefs(ld).V1 Or _
                  linedefs(otherld).V1 = linedefs(ld).V2 Or _
                  linedefs(otherld).V2 = linedefs(ld).V1 Or _
                  linedefs(otherld).V2 = linedefs(ld).V2) Then
                    
                    ' Get the triangle's vertices.
                    If linedefs(otherld).V1 = linedefs(ld).V1 Then
                         vc.x = vertexes(linedefs(otherld).V1).x
                         vc.y = vertexes(linedefs(otherld).V1).y
                         vy.x = vertexes(linedefs(otherld).V2).x
                         vy.y = vertexes(linedefs(otherld).V2).y
                         vm.x = vertexes(linedefs(ld).V2).x
                         vm.y = vertexes(linedefs(ld).V2).y
                    ElseIf linedefs(otherld).V1 = linedefs(ld).V2 Then
                         vc.x = vertexes(linedefs(otherld).V1).x
                         vc.y = vertexes(linedefs(otherld).V1).y
                         vy.x = vertexes(linedefs(otherld).V2).x
                         vy.y = vertexes(linedefs(otherld).V2).y
                         vm.x = vertexes(linedefs(ld).V1).x
                         vm.y = vertexes(linedefs(ld).V1).y
                    ElseIf linedefs(otherld).V2 = linedefs(ld).V1 Then
                         vc.x = vertexes(linedefs(otherld).V2).x
                         vc.y = vertexes(linedefs(otherld).V2).y
                         vy.x = vertexes(linedefs(otherld).V1).x
                         vy.y = vertexes(linedefs(otherld).V1).y
                         vm.x = vertexes(linedefs(ld).V2).x
                         vm.y = vertexes(linedefs(ld).V2).y
                    Else
                         vc.x = vertexes(linedefs(otherld).V2).x
                         vc.y = vertexes(linedefs(otherld).V2).y
                         vy.x = vertexes(linedefs(otherld).V1).x
                         vy.y = vertexes(linedefs(otherld).V1).y
                         vm.x = vertexes(linedefs(ld).V1).x
                         vm.y = vertexes(linedefs(ld).V1).y
                    End If
                    
                    m2 = (vy.x - vc.x) ^ 2 + (vy.y - vc.y) ^ 2
                    y2 = (vm.x - vc.x) ^ 2 + (vm.y - vc.y) ^ 2
                    c2 = (vm.x - vy.x) ^ 2 + (vm.y - vy.y) ^ 2
                    If y2 <> 0 And m2 <> 0 Then        ' No division by zero, please.
                         AngleText = CStr(Round(Arccos((m2 + y2 - c2) / (2 * Sqr(m2) * Sqr(y2))) * 180 / pi, 1)) & Chr$(176)
                    Else
                         AngleText = ""
                    End If
                    
               Else
                    AngleText = ""
               End If
          
          Else
               AngleText = ""
          End If
          
          If (lddi.special <> "") Then
               If AngleText = "" Then
                    picMap.ToolTipText = lddi.special
               Else
                    picMap.ToolTipText = lddi.special & " (" & AngleText & ")"
               End If
          ElseIf AngleText <> "" Then
               picMap.ToolTipText = AngleText
          End If
          
     End If
     
     'Check if there is a front sidedef
     If (linedefs(ld).s1 > -1) Then
          
          'Check for sector
          If (sidedefs(linedefs(ld).s1).sector > -1) Then
               
               'Get the front height
               lddi.FrontHeight = sectors(sidedefs(linedefs(ld).s1).sector).hceiling - sectors(sidedefs(linedefs(ld).s1).sector).HFloor
               lddi.FrontSec = sidedefs(linedefs(ld).s1).sector
          End If
          
          'Get offsets
          lddi.FrontX = sidedefs(linedefs(ld).s1).tx
          lddi.FrontY = sidedefs(linedefs(ld).s1).ty
          
          'Get textures
          lddi.S1U = sidedefs(linedefs(ld).s1).Upper
          lddi.S1M = sidedefs(linedefs(ld).s1).Middle
          lddi.S1L = sidedefs(linedefs(ld).s1).Lower
     Else
          lddi.FrontHeight = "-"
          lddi.FrontSec = "-"
          lddi.FrontX = "-"
          lddi.FrontY = "-"
          dispflags = dispflags Xor (LDD_S1 Or LDD_S1U Or LDD_S1M Or LDD_S1L)
     End If
     
     'Check if there is a back sidedef
     If (linedefs(ld).s2 > -1) Then
          
          'Check for sector
          If (sidedefs(linedefs(ld).s2).sector > -1) Then
               
               'Get the front height
               lddi.BackHeight = sectors(sidedefs(linedefs(ld).s2).sector).hceiling - sectors(sidedefs(linedefs(ld).s2).sector).HFloor
               lddi.BackSec = sidedefs(linedefs(ld).s2).sector
          End If
          
          'Get offsets
          lddi.BackX = sidedefs(linedefs(ld).s2).tx
          lddi.BackY = sidedefs(linedefs(ld).s2).ty
          
          'Get textures
          lddi.S2U = sidedefs(linedefs(ld).s2).Upper
          lddi.S2M = sidedefs(linedefs(ld).s2).Middle
          lddi.S2L = sidedefs(linedefs(ld).s2).Lower
     Else
          lddi.BackHeight = "-"
          lddi.BackSec = "-"
          lddi.BackX = "-"
          lddi.BackY = "-"
          dispflags = dispflags Xor (LDD_S2 Or LDD_S2U Or LDD_S2M Or LDD_S2L)
     End If

     lddi.num = ld
     lddi.V1 = linedefs(ld).V1
     lddi.V2 = linedefs(ld).V2
     
     ' Texture requirement flags.
     If RequiresS1Lower(ld) Then texflags = texflags Or REQ_S1L
     If RequiresS1Middle(ld) Then texflags = texflags Or REQ_S1M
     If RequiresS1Upper(ld) Then texflags = texflags Or REQ_S1U
     If RequiresS2Lower(ld) Then texflags = texflags Or REQ_S2L
     If RequiresS2Middle(ld) Then texflags = texflags Or REQ_S2M
     If RequiresS2Upper(ld) Then texflags = texflags Or REQ_S2U

     ShowPartialLinedefInfo lddi, dispflags, texflags

End Sub

Public Sub ShowSelectedLinedefInfo()
     
     Dim lddi As LDDISPINFO
     Dim dispflags As LDDISPLAY
     Dim texflags As TEXREQFLAGS
     
     ' Special handling for exactly one linedef.
     If selected.Count = 1 Then
          ShowLinedefInfo selected.Keys(0)
          Exit Sub
     End If
     
     ' Grab the attributes that fit in the structure.
     lddi.BackHeight = CheckLinedefSide2Height
     lddi.BackSec = CheckLinedefSide2Sector
     lddi.BackX = CheckLinedefSide2OffsetX
     lddi.BackY = CheckLinedefSide2OffsetY
     lddi.FrontHeight = CheckLinedefSide1Height
     lddi.FrontSec = CheckLinedefSide1Sector
     lddi.FrontX = CheckLinedefSide1OffsetX
     lddi.FrontY = CheckLinedefSide1OffsetY
     lddi.Length = CheckLinedefLength
     lddi.S1L = CheckLinedefSide1Lower
     lddi.S1M = CheckLinedefSide1Middle
     lddi.S1U = CheckLinedefSide1Upper
     lddi.S2L = CheckLinedefSide2Lower
     lddi.S2M = CheckLinedefSide2Middle
     lddi.S2U = CheckLinedefSide2Upper
     lddi.special = CheckLinedefType
     lddi.Tag = CheckLinedefTag
     lddi.V1 = CheckLinedefVertex1
     lddi.V2 = CheckLinedefVertex2
     
     
     ' Check whether we have agreement for each field.
     If lddi.BackHeight <> "" Then dispflags = dispflags Or LDD_BACKHEIGHT
     If lddi.BackSec <> "" Then dispflags = dispflags Or LDD_BACKSEC
     If lddi.BackX <> "" Then dispflags = dispflags Or LDD_BACKX
     If lddi.BackY <> "" Then dispflags = dispflags Or LDD_BACKY
     If lddi.FrontHeight <> "" Then dispflags = dispflags Or LDD_FRONTHEIGHT
     If lddi.FrontSec <> "" Then dispflags = dispflags Or LDD_FRONTSEC
     If lddi.FrontX <> "" Then dispflags = dispflags Or LDD_FRONTX
     If lddi.FrontY <> "" Then dispflags = dispflags Or LDD_FRONTY
     If lddi.Length <> "" Then dispflags = dispflags Or LDD_LENGTH
     If lddi.S1L <> "" Then dispflags = dispflags Or LDD_S1L
     If lddi.S1M <> "" Then dispflags = dispflags Or LDD_S1M
     If lddi.S1U <> "" Then dispflags = dispflags Or LDD_S1U
     If lddi.S2L <> "" Then dispflags = dispflags Or LDD_S2L
     If lddi.S2M <> "" Then dispflags = dispflags Or LDD_S2M
     If lddi.S2U <> "" Then dispflags = dispflags Or LDD_S2U
     
     If lddi.special <> "" Then
          dispflags = dispflags Or LDD_SPECIAL
          lddi.special = MakeNiceActionText(Val(lddi.special))
     End If
     
     If lddi.Tag <> "" Then dispflags = dispflags Or LDD_TAG
     If lddi.V1 <> "" Then dispflags = dispflags Or LDD_V1
     If lddi.V2 <> "" Then dispflags = dispflags Or LDD_V2
     If CheckLinedefsHaveSide1 Then dispflags = dispflags Or LDD_S1
     If CheckLinedefsHaveSide2 Then dispflags = dispflags Or LDD_S2
     
     
     ' Texture requirement flags.
     If LinedefsSide1LowerRequired Then texflags = texflags Or REQ_S1L
     If LinedefsSide1MiddleRequired Then texflags = texflags Or REQ_S1M
     If LinedefsSide1UpperRequired Then texflags = texflags Or REQ_S1U
     If LinedefsSide2LowerRequired Then texflags = texflags Or REQ_S2L
     If LinedefsSide2MiddleRequired Then texflags = texflags Or REQ_S2M
     If LinedefsSide2UpperRequired Then texflags = texflags Or REQ_S2U
     
     ShowPartialLinedefInfo lddi, dispflags, texflags
     
End Sub


Private Sub ShowPartialLinedefInfo(ByRef lddi As LDDISPINFO, ByVal dispflags As LDDISPLAY, ByVal texflags As TEXREQFLAGS)
     Dim distance As Long
     Dim OldSelected As Long
     
     
     'Check on what panel to show the info
     If picBar.visible Then
          
          'Check if the bar is fully shown
          If (cmdToggleBar.Tag = "0") Then
               
               'Set the info
               If (dispflags And LDD_NUM) Then fraLinedef.Caption = " Linedef " & lddi.num & " " Else fraLinedef.Caption = " Multiple Linedefs "
               If (dispflags And LDD_SPECIAL) Then lblLinedefType.Caption = ShortedText(lddi.special, lblLinedefType.width \ Screen.TwipsPerPixelX) Else lblLinedefType.Caption = ""
               If (dispflags And LDD_LENGTH) Then lblLinedefLength.Caption = lddi.Length Else lblLinedefLength.Caption = ""
               If (dispflags And LDD_TAG) Then lblLinedefTag.Caption = lddi.Tag Else lblLinedefTag.Caption = ""
               If (dispflags And LDD_FRONTHEIGHT) Then lblS1Height.Caption = lddi.FrontHeight Else lblS1Height.Caption = ""
               If (dispflags And LDD_BACKHEIGHT) Then lblS2height.Caption = lddi.FrontHeight Else lblS2height.Caption = ""
               If (dispflags And LDD_FRONTSEC) Then lblS1Sector.Caption = lddi.FrontSec Else lblS1Sector.Caption = ""
               If (dispflags And LDD_BACKSEC) Then lblS2Sector.Caption = lddi.BackSec Else lblS2Sector.Caption = ""
               If (dispflags And LDD_FRONTX) Then lblS1X.Caption = lddi.FrontX Else lblS1X.Caption = ""
               If (dispflags And LDD_FRONTY) Then lblS1Y.Caption = lddi.FrontY Else lblS1Y.Caption = ""
               If (dispflags And LDD_BACKX) Then lblS2X.Caption = lddi.BackX Else lblS2X.Caption = ""
               If (dispflags And LDD_BACKY) Then lblS2Y.Caption = lddi.BackY Else lblS2Y.Caption = ""
               
               If (dispflags And LDD_S1) Then
                    If (dispflags And LDD_S1U) Then lblS1Upper.Caption = lddi.S1U Else lblS1Upper.Caption = ""
                    If (dispflags And LDD_S1M) Then lblS1Middle.Caption = lddi.S1M Else lblS1Middle.Caption = ""
                    If (dispflags And LDD_S1L) Then lblS1Lower.Caption = lddi.S1L Else lblS1Lower.Caption = ""
                    
                    ' Even if we don't display it, still get the picture, to
                    ' clear it.
                    GetScaledTexturePicture lddi.S1U, imgS1Upper, , (texflags And REQ_S1U)
                    GetScaledTexturePicture lddi.S1M, imgS1Middle, , (texflags And REQ_S1M)
                    GetScaledTexturePicture lddi.S1L, imgS1Lower, , (texflags And REQ_S1L)
               End If
               
               If (dispflags And LDD_S2) Then
                    If (dispflags And LDD_S2U) Then lblS2Upper.Caption = lddi.S2U Else lblS2Upper.Caption = ""
                    If (dispflags And LDD_S2M) Then lblS2Middle.Caption = lddi.S2M Else lblS2Middle.Caption = ""
                    If (dispflags And LDD_S2L) Then lblS2Lower.Caption = lddi.S2L Else lblS2Lower.Caption = ""
                    
                    ' Even if we don't display it, still get the picture, to
                    ' clear it.
                    GetScaledTexturePicture lddi.S2U, imgS2Upper, , (texflags And REQ_S2U)
                    GetScaledTexturePicture lddi.S2M, imgS2Middle, , (texflags And REQ_S2M)
                    GetScaledTexturePicture lddi.S2L, imgS2Lower, , (texflags And REQ_S2L)
               End If
               
               'Show panels
               fraLinedef.visible = True
               fraFrontSidedef.visible = (dispflags And LDD_S1)
               fraBackSidedef.visible = (dispflags And LDD_S2)
          Else
               
               'Only show a sinle line
               If (dispflags And (LDD_NUM Or LDD_SPECIAL)) = (LDD_NUM Or LDD_SPECIAL) Then
                    lblBarText.Caption = " Linedef " & lddi.num & ":  " & lddi.special
               Else
                    lblBarText.Caption = " Multiple Linedefs "
               End If
          End If
          
     ElseIf picSBar.visible Then
     
          'Check if the bar is fully shown
          If (cmdToggleBar.Tag = "0") Then
               
               'Set the info
               If (dispflags And LDD_NUM) Then fraSLinedef.Caption = " Linedef " & lddi.num & " " Else fraSLinedef.Caption = " Multiple Linedefs "
               If (dispflags And LDD_SPECIAL) Then lblSLinedefType.Caption = ShortedText(lddi.special, lblSLinedefType.width \ Screen.TwipsPerPixelX) Else lblSLinedefType.Caption = ""
               If (dispflags And LDD_LENGTH) Then lblSLinedefLength.Caption = lddi.Length Else lblSLinedefLength.Caption = ""
               If (dispflags And LDD_TAG) Then lblSLinedefTag.Caption = lddi.Tag Else lblSLinedefTag.Caption = ""
               If (dispflags And LDD_FRONTHEIGHT) Then lblSS1Height.Caption = lddi.FrontHeight Else lblSS1Height.Caption = ""
               If (dispflags And LDD_BACKHEIGHT) Then lblSS2height.Caption = lddi.FrontHeight Else lblSS2height.Caption = ""
               If (dispflags And LDD_FRONTSEC) Then lblSS1Sector.Caption = lddi.FrontSec Else lblSS1Sector.Caption = ""
               If (dispflags And LDD_BACKSEC) Then lblSS2Sector.Caption = lddi.BackSec Else lblSS2Sector.Caption = ""
               'If (dispflags And LDD_FRONTX) Then lblSS1X.Caption = lddi.FrontX Else lblSS1X.Caption = ""
               'If (dispflags And LDD_FRONTY) Then lblSS1Y.Caption = lddi.FrontY Else lblSS1Y.Caption = ""
               'If (dispflags And LDD_BACKX) Then lblSS2X.Caption = lddi.BackX Else lblSS2X.Caption = ""
               'If (dispflags And LDD_BACKY) Then lblSS2Y.Caption = lddi.BackY Else lblSS2Y.Caption = ""
               
               If (dispflags And LDD_S1) Then
                    If (dispflags And LDD_S1U) Then lblSS1Upper.Caption = lddi.S1U Else lblSS1Upper.Caption = ""
                    If (dispflags And LDD_S1M) Then lblSS1Middle.Caption = lddi.S1M Else lblSS1Middle.Caption = ""
                    If (dispflags And LDD_S1L) Then lblSS1Lower.Caption = lddi.S1L Else lblSS1Lower.Caption = ""
                    
                    ' Even if we don't display it, still get the picture, to
                    ' clear it.
                    GetScaledTexturePicture lddi.S1U, imgSS1Upper, , (texflags And REQ_S1U)
                    GetScaledTexturePicture lddi.S1M, imgSS1Middle, , (texflags And REQ_S1M)
                    GetScaledTexturePicture lddi.S1L, imgSS1Lower, , (texflags And REQ_S1L)
               End If
               
               If (dispflags And LDD_S2) Then
                    If (dispflags And LDD_S2U) Then lblSS2Upper.Caption = lddi.S2U Else lblSS2Upper.Caption = ""
                    If (dispflags And LDD_S2M) Then lblSS2Middle.Caption = lddi.S2M Else lblSS2Middle.Caption = ""
                    If (dispflags And LDD_S2L) Then lblSS2Lower.Caption = lddi.S2L Else lblSS2Lower.Caption = ""
                    
                    ' Even if we don't display it, still get the picture, to
                    ' clear it.
                    GetScaledTexturePicture lddi.S2U, imgSS2Upper, , (texflags And REQ_S2U)
                    GetScaledTexturePicture lddi.S2M, imgSS2Middle, , (texflags And REQ_S2M)
                    GetScaledTexturePicture lddi.S2L, imgSS2Lower, , (texflags And REQ_S2L)
               End If
               
               'Show panels
               fraSLinedef.visible = True
               fraSFrontSidedef.visible = (dispflags And LDD_S1)
               fraSBackSidedef.visible = (dispflags And LDD_S2)
          Else
               
               'Only show a sinle line
               If (dispflags And (LDD_NUM Or LDD_SPECIAL)) = (LDD_NUM Or LDD_SPECIAL) Then
                    lblBarText.Caption = " Linedef " & lddi.num & ":  " & lddi.special
               Else
                    lblBarText.Caption = " Multiple Linedefs "
               End If
          End If
          
     End If
End Sub


Public Sub ShowSectorInfo(ByVal sc As Long)

     Dim scdi As SECDISPINFO
     Dim desc As String
     
     'Get the information
     scdi.HCeil = sectors(sc).hceiling
     scdi.HFloor = sectors(sc).HFloor
     scdi.Tag = sectors(sc).Tag
     scdi.Height = sectors(sc).hceiling - sectors(sc).HFloor
     scdi.Light = sectors(sc).Brightness
     scdi.TCeil = sectors(sc).tceiling
     scdi.TFloor = sectors(sc).TFloor
     scdi.Type = MakeNiceSecTypeText(sectors(sc).special)
     scdi.num = sc
     
     ' Effect and/or description on Tooltip
     picMap.ToolTipText = ""
     desc = GetSectorDescription(sc)
     If (Val(Config("showtooltips")) <> 0) And (mode <> EM_3D) Then
          
          If (sectors(sc).special > 0) And desc <> "" Then
               ' No multi-line tooltips without API calls. Awww...
               picMap.ToolTipText = desc & " (" & scdi.Type & ")"
          ElseIf (sectors(sc).special > 0) Then
               picMap.ToolTipText = scdi.Type
          ElseIf desc <> "" Then
               picMap.ToolTipText = desc
          End If
          
     End If
     
     ShowPartialSectorInfo scdi, SCD_ALL
     
End Sub

Private Sub ShowSelectedSectorInfo()

     Dim scdi As SECDISPINFO
     Dim dispflags As SECDISPLAY
     
     ' Special handling for exactly one linedef.
     If selected.Count = 1 Then
          ShowSectorInfo selected.Keys(0)
          Exit Sub
     End If
     
     scdi.HCeil = frmSector.CheckSectorHCeiling
     scdi.HFloor = frmSector.CheckSectorHFloor
     scdi.Light = frmSector.CheckSectorBrightness
     scdi.Tag = frmSector.CheckSectorTag
     scdi.TCeil = frmSector.CheckSectorTCeiling
     scdi.TFloor = frmSector.CheckSectorTFloor
     scdi.Type = frmSector.CheckSectorType
     
     If scdi.HCeil <> "" Then dispflags = dispflags Or SCD_HCEIL
     If scdi.HFloor <> "" Then dispflags = dispflags Or SCD_HFLOOR
     If scdi.HCeil <> "" And scdi.HFloor <> "" Then
          scdi.Height = Val(scdi.HCeil) - Val(scdi.HFloor)
          dispflags = dispflags Or SCD_HEIGHT
     End If
     If scdi.Light <> "" Then dispflags = dispflags Or SCD_LIGHT
     If scdi.Tag <> "" Then dispflags = dispflags Or SCD_TAG
     If scdi.TCeil <> "" Then dispflags = dispflags Or SCD_TCEIL
     If scdi.TFloor <> "" Then dispflags = dispflags Or SCD_TFLOOR
     If scdi.Type <> "" Then
          dispflags = dispflags Or SCD_TYPE
          scdi.Type = MakeNiceSecTypeText(Val(scdi.Type))
     End If
     
     ShowPartialSectorInfo scdi, dispflags

End Sub

Private Sub ShowPartialSectorInfo(scdi As SECDISPINFO, ByVal dispflags As SECDISPLAY)

     'Check what panel to show the info on
     If picBar.visible Then
          
          'Check if the bar is fully shown
          If (cmdToggleBar.Tag = "0") Then
               
               'Set the info
               If (dispflags And SCD_NUM) Then fraSector.Caption = " Sector " & scdi.num & " " Else fraSector.Caption = "Multiple Sectors"
               If (dispflags And SCD_TYPE) Then lblSectorType.Caption = ShortedText(scdi.Type, lblSectorType.width \ Screen.TwipsPerPixelX) Else lblSectorType.Caption = ""
               If (dispflags And SCD_TAG) Then lblSectorTag.Caption = scdi.Tag Else lblSectorTag.Caption = ""
               If (dispflags And SCD_HCEIL) Then lblSectorCeiling.Caption = scdi.HCeil Else lblSectorCeiling.Caption = ""
               If (dispflags And SCD_HFLOOR) Then lblSectorFloor.Caption = scdi.HFloor Else lblSectorFloor.Caption = ""
               If (dispflags And SCD_HEIGHT) Then lblSectorHeight.Caption = scdi.Height Else lblSectorHeight.Caption = ""
               If (dispflags And SCD_LIGHT) Then lblSectorLight.Caption = scdi.Light Else lblSectorLight.Caption = ""
               
               If (dispflags And SCD_TCEIL) Then
                    lblCeiling.Caption = scdi.TCeil
               Else
                    lblCeiling.Caption = ""
               End If
               GetScaledFlatPicture scdi.TCeil, imgCeiling
                    
               If (dispflags And SCD_TFLOOR) Then
                    lblFloor.Caption = scdi.TFloor
               Else
                    lblFloor.Caption = ""
               End If
               GetScaledFlatPicture scdi.TFloor, imgFloor
               
               'Show the info
               fraSector.visible = True
               fraSectorCeiling.visible = True
               fraSectorFloor.visible = True
          Else
               
               'Show single line only
               If (dispflags And (SCD_NUM Or SCD_TYPE)) = (SCD_NUM Or SCD_TYPE) Then
                    lblBarText.Caption = " Sector " & scdi.num & ":  " & scdi.Type
               Else
                    lblBarText.Caption = "Multiple Sectors"
               End If
          End If
          
     ElseIf picSBar.visible Then
          
          'Check if the bar is fully shown
          If (cmdToggleBar.Tag = "0") Then
               
               'Set the info
               If (dispflags And SCD_NUM) Then fraSSector.Caption = " Sector " & scdi.num & " " Else fraSSector.Caption = "Multiple Sectors"
               If (dispflags And SCD_TYPE) Then lblSSectorType.Caption = ShortedText(scdi.Type, lblSSectorType.width \ Screen.TwipsPerPixelX) Else lblSSectorType.Caption = ""
               If (dispflags And SCD_TAG) Then lblSSectorTag.Caption = scdi.Tag Else lblSSectorTag.Caption = ""
               If (dispflags And SCD_HCEIL) Then lblSSectorCeiling.Caption = scdi.HCeil Else lblSSectorCeiling.Caption = ""
               If (dispflags And SCD_HFLOOR) Then lblSSectorFloor.Caption = scdi.HFloor Else lblSSectorFloor.Caption = ""
               If (dispflags And SCD_HEIGHT) Then lblSSectorHeight.Caption = scdi.Height Else lblSSectorHeight.Caption = ""
               If (dispflags And SCD_LIGHT) Then lblSSectorLight.Caption = scdi.Light Else lblSSectorLight.Caption = ""
               
               If (dispflags And SCD_TCEIL) Then
                    lblSCeiling.Caption = scdi.TCeil
               Else
                    lblSCeiling.Caption = ""
               End If
               GetScaledFlatPicture scdi.TCeil, imgSCeiling
                    
               If (dispflags And SCD_TFLOOR) Then
                    lblSFloor.Caption = scdi.TFloor
               Else
                    lblSFloor.Caption = ""
               End If
               GetScaledFlatPicture scdi.TFloor, imgSFloor
               
               'Show the info
               fraSSector.visible = True
               fraSSectorCeiling.visible = True
               fraSSectorFloor.visible = True
          Else
               
               'Show single line only
               If (dispflags And (SCD_NUM Or SCD_TYPE)) = (SCD_NUM Or SCD_TYPE) Then
                    lblBarText.Caption = " Sector " & scdi.num & ":  " & scdi.Type
               Else
                    lblBarText.Caption = "Multiple Sectors"
               End If
          End If
          
     End If

End Sub

Public Sub ShowThingInfo(ByVal th As Long)
     Dim distance As Long
     Dim nearest As Long
     Dim OldSelected As Long
     Dim tType As String
     Dim angle As String
     Dim Flags As String
     Dim nType As Long
     Dim spritename As String
     Dim tx As Long
     Dim ty As Long
     Dim zfactor, tz As Long
     Dim SectorUndrawn As Boolean
     Dim sTag As String
     Dim sAction As String
     
     'Get information
     nType = things(th).thing
     spritename = GetThingTypeSpriteName(things(th).thing)
     tType = GetThingTypeDesc(things(th).thing) & " (" & things(th).thing & ")"
     angle = GetThingAngleDesc(things(th).angle) & " (" & things(th).angle & ")"
     Flags = Hexadecimal(things(th).Flags, 4) & " (" & things(th).Flags & ")"
     tx = things(th).x
     ty = things(th).y
     zfactor = GetZFactorFromType(things(th).thing)
     
     tz = (things(th).Flags And (Not (zfactor - 1))) / zfactor
     
     sTag = things(th).Tag
     
     'Check if Thing has an action
     If (things(th).effect > 0) Then
          
          'Check if the linedef type can be found
          If (mapconfig("linedeftypes").Exists(CStr(things(th).effect))) Then
               sAction = things(th).effect & " - " & Trim$(mapconfig("linedeftypes")(CStr(things(th).effect))("title"))
          Else
               sAction = things(th).effect & " - Unknown"
          End If
     Else
          sAction = "0 - None"
     End If
     
     'Check what panel to show the info on
     If picBar.visible Then
          
          'Check if the bar is fully shown
          If (cmdToggleBar.Tag = "0") Then
               
               'Set the info
               fraThing.Caption = " Thing " & th & " "
               lblThingType = ShortedText(tType, lblThingType.width \ Screen.TwipsPerPixelX)
               lblThingAction.Caption = ShortedText(sAction, lblThingAction.width \ Screen.TwipsPerPixelX)
               lblThingAngle = ShortedText(angle, lblThingAngle.width \ Screen.TwipsPerPixelX)
               lblThingFlags = Flags
               lblThingTag.Caption = sTag
               lblThingXY = tx & ", " & ty & ", " & tz
               lblThing.Caption = spritename
               GetScaledSpritePicture nType, imgThing, picThing.ScaleWidth, picThing.ScaleHeight
               
               'Show the info
               fraThing.visible = True
               fraThingPreview.visible = True
          Else
               
               'Show single line only
               lblBarText.Caption = " Thing " & th & ":  " & tType
          End If
          
     ElseIf picSBar.visible Then
          
          'Check if the bar is fully shown
          If (cmdToggleBar.Tag = "0") Then
               
               'Set the info
               fraSThing.Caption = " Thing " & th & " "
               lblSThingType = ShortedText(tType, lblSThingType.width \ Screen.TwipsPerPixelX)
               lblSThingAction.Caption = ShortedText(sAction, lblSThingAction.width \ Screen.TwipsPerPixelX)
               lblSThingAngle = ShortedText(angle, lblSThingAngle.width \ Screen.TwipsPerPixelX)
               lblSThingFlags = Flags
               lblSThingTag.Caption = sTag
               lblSThingXY = tx & ", " & ty & ", " & tz
               lblSThing.Caption = spritename
               GetScaledSpritePicture nType, imgSThing, picSThing.ScaleWidth, picSThing.ScaleHeight
               
               'Show the info
               fraSThing.visible = True
               fraSThingPreview.visible = True
          Else
               
               'Show single line only
               lblBarText.Caption = " Thing " & th & ":  " & tType
          End If
     End If
     
     'Type on tooltip
     'Only when not in 3D Mode
     picMap.ToolTipText = ""
     If (Val(Config("showtooltips")) <> 0) And (mode <> EM_3D) Then picMap.ToolTipText = tType
End Sub

Private Sub cmdToggleBar_Click()
     InfoBarToggle
End Sub

Private Sub cmdToggleSBar_Click()
     InfoBarToggle
End Sub

Public Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
     Dim ShortcutCode As Long
     Dim DoUpdateStatusBar As Boolean
     Dim DoRedrawMap As Boolean
     
     ' Clear drawing line
     If submode = ESM_DRAWING Then
          'Remove previous drawn line
          ClearOldDrawingLine
     End If
     
     'Adjust shift mask
     CurrentShiftMask = Shift
     
     ' Reinstate drawing line - CurrentShiftMask might have changed!
     If submode = ESM_DRAWING Then
          'Remove previous drawn line
          RenderNewDrawingLine LastX, LastY
          picMap.Refresh
     End If
     
     'Ignore shift keys alone
     If (KeyCode = 16) Or (KeyCode = 17) Or (KeyCode = 18) Then Exit Sub
     
     'Make the shortcut code from keycode and shift
     ShortcutCode = KeyCode Or (Shift * (2 ^ 16))
     
     
     'Check if in 3D Mode
     If (mode = EM_3D) Then
          
          'Leave immediately if no map is loaded
          If (mapfile = "") Then Exit Sub
          
          'Leave when map edit is disabled
          If (picMap.Enabled = False) Then Exit Sub
          
          'Check how we should process data
          If TextureSelecting Then
               
               'Perform the action associated with the key
               KeydownTextureSelect ShortcutCode
               
          ElseIf DrawingSector Then
          
               KeydownDrawSector ShortcutCode
               
          Else
               
               'Perform the action associated with the key
               Keydown3D ShortcutCode
          End If
          
          'Leave when form is unloaded
          If (IsLoaded(frmMain) = False) Then Exit Sub
     Else
          
          'Do menu keys
          KeypressMenus ShortcutCode
          
          'Leave immediately if no map is loaded
          If (mapfile = "") Then Exit Sub
          
          'Leave when map edit is disabled
          If (picMap.Enabled = False) Then Exit Sub
          
          'Do general keys
          KeypressGeneral ShortcutCode, DoUpdateStatusBar, DoRedrawMap
          
          'Leave when form is unloaded
          If (IsLoaded(frmMain) = False) Then Exit Sub

          'Do mode-specific keys
          Select Case mode
               Case EM_VERTICES: KeypressVertexes ShortcutCode
               Case EM_LINES: KeypressLines ShortcutCode
               Case EM_SECTORS: KeypressSectors ShortcutCode
               Case EM_THINGS: KeypressThings ShortcutCode
          End Select
          
          'Leave when form is unloaded
          If (IsLoaded(frmMain) = False) Then Exit Sub
     End If
     
     
     'Update status
     If DoUpdateStatusBar Then UpdateStatusBar
     
     'Check if we should redraw
     If DoRedrawMap Then
          
          'Redraw map
          RedrawMap
          
          'Show highlight
          If (submode = ESM_NONE) Then ShowHighlight LastX, LastY
     End If
End Sub

Private Sub Form_KeyPress(KeyAscii As Integer)
     
     'Check if in 3D Mode
     If (mode = EM_3D) Then
          
          'Check how we should process data
          If TextureSelecting Then
               
               'Perform the action associated with the key
               KeypressTextureSelect KeyAscii
               
               'Leave when form is unloaded
               If (IsLoaded(frmMain) = False) Then Exit Sub
          End If
     End If
End Sub


Public Sub Form_KeyUp(KeyCode As Integer, Shift As Integer)
     Dim ShortcutCode As Long
     Dim DoUpdateStatusBar As Boolean
     Dim DoRedrawMap As Boolean
     
     ' Clear drawing line
     If submode = ESM_DRAWING And numselected > 0 Then
          'Remove previous drawn line
          ClearOldDrawingLine
     End If
     
     'Adjust shift mask
     CurrentShiftMask = Shift
     
     ' Reinstate drawing line - CurrentShiftMask might have changed!
     If submode = ESM_DRAWING And numselected > 0 Then
          'Remove previous drawn line
          RenderNewDrawingLine LastX, LastY
          picMap.Refresh
     End If
     
     'Ignore shift keys alone
     If (KeyCode = 16) Or (KeyCode = 17) Or (KeyCode = 18) Then Exit Sub
     
     'Make the shortcut code from keycode and shift
     ShortcutCode = KeyCode Or (Shift * (2 ^ 16))
     
     
     'Leave immediately if no map is loaded
     If (mapfile = "") Then Exit Sub
     
     'Leave when map edit is disabled
     If (picMap.Enabled = False) Then Exit Sub
     
     
     'Check if in 3D Mode
     If (mode = EM_3D) Then
          
          'Check how we should process data
          If Not TextureSelecting Then
               
               'Perform the action associated with the key
               Keyrelease3D ShortcutCode
          End If
     Else
          
          'Do general keys
          KeyreleaseGeneral ShortcutCode, DoUpdateStatusBar, DoRedrawMap
     End If
     
     'Leave when form is unloaded
     If (IsLoaded(frmMain) = False) Then Exit Sub
     
     'Update status
     If DoUpdateStatusBar Then UpdateStatusBar
     
     'Check if we should redraw
     If DoRedrawMap Then
          
          'Redraw map
          RedrawMap
          
          'Show highlight
          If (submode = ESM_NONE) Then ShowHighlight LastX, LastY
     End If
     
     'Reset the F7 count
     F7Count = 0
End Sub


Private Sub Form_Load()

     Dim hBitmap As Long
     
     'Make subclassing
     CreateSubclassing
     
     'Apply configuration on interface
     ApplyInterfaceConfiguration
     
     'Disable controls for map editing (no map loaded yet)
     DisableMapEditing
     
     'Make menu item names with shortcuts
     UpdateMenuShortcuts
     
     'Update status bar
     UpdateStatusBar
     
     'Show recent files
     UpdateRecentFilesMenu
     
     ' Load some bitmaps - for Wine compatibility.
     Set picThings(0).Picture = LoadPictureEx(App.Path & "\Thing_tiny.bmp", hBitmap)
     picThings(0).Tag = hBitmap
     Set picThings(1).Picture = LoadPictureEx(App.Path & "\Thing_small.bmp", hBitmap)
     picThings(1).Tag = hBitmap
     Set picThings(2).Picture = LoadPictureEx(App.Path & "\Thing_medium.bmp", hBitmap)
     picThings(2).Tag = hBitmap
     Set picThings(3).Picture = LoadPictureEx(App.Path & "\Thing_big.bmp", hBitmap)
     picThings(3).Tag = hBitmap
     Set picNumbers.Picture = LoadPictureEx(App.Path & "\Font4.bmp", hBitmap)
     picNumbers.Tag = hBitmap
     
     'Apply window sizes
     With frmMain
          .left = Config("mainwindow")("left")
          .top = Config("mainwindow")("top")
          If (Config("mainwindow")("width") > 1500) Then .width = Config("mainwindow")("width")
          If (Config("mainwindow")("height") > 1500) Then .Height = Config("mainwindow")("height")
          .WindowState = Config("mainwindow")("windowstate")
     End With
End Sub


Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)

     Dim tblayout As String
     Dim b As Button
     
     'Check if not currently busy
     If (StatusDisplayed = False) Then
          
          'When in 3D mode, leave first
          If (mode = EM_3D) Then Stop3DMode
          
          'Ensure the splash dialog is gone
          Unload frmSplash: Set frmSplash = Nothing
          
          'Unload map and terminate when unloaded
          If (MapUnload) Then
          
               ' Save the toolbar layout.
               SaveToolbarLayout
               
               'Remove subclassing
               DestroySubclassing
               
               'Do a resize to keep window sizes
               Form_Resize
               
               'Terminate program
               Terminate
          Else
               
               'Cancel the unload
               Cancel = True
          End If
     End If
End Sub

Public Sub Form_Resize()
     On Local Error Resume Next
     Dim ToolbarHeight As Long
     Dim BottomHeight As Long
     Dim TopHeight As Long
     Dim LeftHeight As Long
     Dim RightHeight As Long
     
     'Lock the viewport
     If (Running3D = False) Then LockWindowUpdate Me.hWnd 'picMap.hWnd
     
     'Move the bar toggle button
     cmdToggleBar.left = ScaleWidth - cmdToggleBar.width - 4
     If (Val(Config("detailsbar")) = 2) Then cmdToggleBar.top = picBar.ScaleHeight - cmdToggleBar.Height Else cmdToggleBar.top = 0
     cmdToggleSBar.top = ScaleHeight - stbStatus.Height - tlbToolbar.Height - cmdToggleSBar.Height - 4
     If (Val(Config("detailsbar")) = 3) Then cmdToggleSBar.left = picSBar.ScaleWidth - cmdToggleSBar.width Else cmdToggleSBar.left = 0
     
     'Make sure the renderer is terminated
     TerminateMapRenderer
     
     'Determine space to reserve
     If tlbToolbar.visible Then ToolbarHeight = tlbToolbar.Height
     If picBar.visible And (picBar.Align = vbAlignBottom) Then BottomHeight = picBar.Height
     If picBar.visible And (picBar.Align = vbAlignTop) Then TopHeight = picBar.Height
     If picSBar.visible And (picSBar.Align = vbAlignLeft) Then LeftHeight = picSBar.width
     If picSBar.visible And (picSBar.Align = vbAlignRight) Then RightHeight = picSBar.width
     
     'Map screen
     With picMap
          .top = ToolbarHeight + TopHeight + 2
          .left = LeftHeight + 2
          .width = ScaleWidth - RightHeight - picMap.left - 2
          .Height = ScaleHeight - BottomHeight - picMap.top - stbStatus.Height - 2
     End With
     
     'Check if in 3D Mode
     If (Running3D) Then
          
          'Free the mouse
          FreeMouse
          
          'Determine rendering area
          DetermineRenderScreenSize frmMain.picMap
          
          'Reclaim mouse
          CaptureMouse
          
          'Render now to update
          RunSingleFrame False, True
     Else
          
          'Only initialize renderer when a map is loaded
          If (mapfile <> "") Then
               
               'Initialize the map screen
               InitializeMapRenderer frmMain.picMap
               
               'Set the viewport
               ChangeView ViewLeft, ViewTop, ViewZoom
               
               'Redraw entire map
               RedrawMap
          End If
     End If
     
     'Unlock the viewport
     If (Running3D = False) Then LockWindowUpdate 0
     
     'Check if visible
     If (frmMain.visible = True) Then
          
          'Save windowstate
          Config("mainwindow")("windowstate") = frmMain.WindowState
          
          'Check if it has a valid size now
          If (frmMain.WindowState = vbNormal) Then
               
               'Save window size
               Config("mainwindow")("left") = frmMain.left
               Config("mainwindow")("top") = frmMain.top
               Config("mainwindow")("width") = frmMain.width
               Config("mainwindow")("height") = frmMain.Height
          End If
     End If
End Sub

Private Function InsertThingHere(ByVal x As Long, ByVal y As Long) As Long
     Dim t As Long
     
     'Remove higlight
     RemoveHighlight True
     
     'No more selection
     ResetSelections things(0), numthings, linedefs(0), numlinedefs, vertexes(0), numvertexes, VarPtr(sectors(0)), numsectors
     Set selected = New Dictionary
     numselected = 0
     selectedtype = EM_THINGS
     
     'Make undo
     CreateUndo "thing insert"
     
     'Snap X and Y if snap mode is on
     If snapmode Then
          x = SnappedToGridX(x)
          y = SnappedToGridY(y)
     End If
     
     'Create a thing now
     t = CreateThing
     InsertThingHere = t
     
     'Set thing defaults
     things(t) = LastThing
     With things(t)
          .selected = 0
          .x = x
          .y = -y
     End With
     
     'Check if we should erase tag and actions
     If (Config("copytagdraw") = vbUnchecked) Then
          With things(t)
               .arg0 = 0
               .arg1 = 0
               .arg2 = 0
               .arg3 = 0
               .arg4 = 0
               .effect = 0
               .Tag = 0
          End With
     End If
     
     'Update thing image and color
     UpdateThingImageColor t
     UpdateThingSize t
     UpdateThingCategory t
     
     'Check if we should edit the thing
     If (Config("newthingdialog") = vbChecked) Then
          
          'Select thing
          things(t).selected = 1
          selected.Add CStr(t), t
          numselected = 1
          
          'Show this
          RedrawMap
          
          'Load dialog
          Load frmThing
          
          'Dont make undo for this edit
          frmThing.lblMakeUndo.Caption = "No"
          
          'Show dialog
          picMap.Enabled = False
          frmThing.Show 1, Me
          picMap.Enabled = True
          
          'No more selection
          ResetSelections things(0), numthings, linedefs(0), numlinedefs, vertexes(0), numvertexes, VarPtr(sectors(0)), numsectors
          Set selected = New Dictionary
          numselected = 0
     End If
     
     'Map has changed
     mapchanged = True
End Function

Private Sub InsertVertexHere(ByVal x As Long, ByVal y As Long)
     Dim distance As Long
     Dim nl As Long
     Dim nv As Long
     Dim d As Long

     'Snap X and Y if snap mode is on
     If snapmode Then
          x = SnappedToGridX(x)
          y = SnappedToGridY(y)
     End If
     
     'Get the nearest vertex
     nv = NearestVertex(x, y, vertexes(0), numvertexes, d)
     
     'Check if no vertex already exists at these coordinates
     If (nv = -1) Or (d > 0) Then
          
          'Make undo
          CreateUndo "vertex insert"
          
          'Insert a vertex now
          nv = InsertVertex(x, -y)
          
          'Draw the vertex
          Render_AllVertices vertexes(0), nv, nv, vertexsize
          
          'Get the nearest linedef
          nl = NearestLinedef(x, y, vertexes(0), linedefs(0), numlinedefs, distance)
          
          'Check if distance is close enough for linedef split
          If (distance <= Config("linesplitdistance")) Then
               
               'Split the linedef with this vertex
               SplitLinedef nl, nv
               
               'Redraw the map
               RedrawMap False
                
               'DEBUG
               'DEBUG_FindUnusedSectors
              
               'Highlight whatever is under teh mouz0r
               If MouseInside Then picMap_MouseMove 0, 0, LastX, LastY
          End If
          
          'Map has changed
          mapchanged = True
          mapnodeschanged = True
     End If
End Sub

Private Sub itmEditCenterView_Click()
     Dim MapRect As RECT
     
     'Calculating map rect
     MapRect = CalculateMapRect
     
     'Center map in view
     CenterViewAt MapRect, True
     
     'Redraw map
     RedrawMap False
     
     'Show highlight
     ShowHighlight LastX, LastY
End Sub

Private Sub itmEditCopy_Click()
     Dim DeselectAfterCopy As Boolean
     
     'Leave if no selection is made
     If ((numselected = 0) And (currentselected = -1)) Then Exit Sub
     
     'Leave when map edit is disabled
     If (picMap.Enabled = False) Then Exit Sub
     
     'Switch back if in move mode
     If (mode = EM_MOVE) Then itmEditMode_Click CInt(PreviousMode)
     
     'Check if not in normal mode
     If (submode <> ESM_NONE) Then
          
          'Show message, cant right now
          MsgBox "Cannot perform Copy while in an operation." & vbLf & "Please finish your operation first!", vbExclamation
          
          'Leave here
          Exit Sub
     End If
     
     'Change mousepointer
     Screen.MousePointer = vbHourglass
     
     'Check if no selection is made
     If (numselected = 0) Then
          
          'Temporarely select highlighted object for the mode we are in
          Select Case mode
               Case EM_VERTICES: SelectCurrentVertex
               Case EM_LINES: SelectCurrentLine
               Case EM_SECTORS: SelectCurrentSector
               Case EM_THINGS: SelectCurrentThing
          End Select
          
          'After copy, deselect this
          DeselectAfterCopy = True
     End If
     
     'Clear clipboard
     Clipboard.Clear
     
     'Clear file
     ClipboardCleanup
     
     'Save the selection to file
     SavePrefabSelection ClipboardFile
     
     'Set descriptor on clipboard
     ClipboardSetDescriptor
     
     'Check if we should deselect temporary selection
     If DeselectAfterCopy Then RemoveSelection True
     
     'Reset mousepointer
     Screen.MousePointer = vbNormal
End Sub

Private Sub itmEditCut_Click()
     
     'Leave if no selection is made
     If (numselected = 0) Then Exit Sub
     
     'Leave when map edit is disabled
     If (picMap.Enabled = False) Then Exit Sub
     
     'Switch back if in move mode
     If (mode = EM_MOVE) Then itmEditMode_Click CInt(PreviousMode)
     
     'Check if not in normal mode
     If (submode <> ESM_NONE) Then
          
          'Show message, cant right now
          MsgBox "Cannot perform Cut while in an operation." & vbLf & "Please finish your operation first!", vbExclamation
          
          'Leave here
          Exit Sub
     End If
     
     
     'First copy
     itmEditCopy_Click
     
     'Delete selection/highlight
     DeleteSelection "cut"
     
     'Redraw map
     RedrawMap
End Sub

Private Sub itmEditDelete_Click()
     
     'Leave when map edit is disabled
     If (picMap.Enabled = False) Then Exit Sub
     
     'Switch back if in move mode
     If (mode = EM_MOVE) Then itmEditMode_Click CInt(PreviousMode)
     
     'Check if not in normal mode
     If (submode <> ESM_NONE) And (submode <> ESM_PASTING) Then
          
          'Show message, cant right now
          MsgBox "Cannot perform Delete while in an operation." & vbLf & "Please finish your operation first!", vbExclamation
          
          'Leave here
          Exit Sub
     End If
     
     'Check if pasting
     If (submode = ESM_PASTING) Then
          
          'Just cancel
          CancelCurrentOperation
     Else
          
          'Delete selection/highlight
          DeleteSelection "delete"
          
          'Update status bar
          UpdateStatusBar
          
          'Redraw map
          RedrawMap
     End If
End Sub

Private Sub itmEditFind_Click()
     
     'Switch back if in move mode
     If (mode = EM_MOVE) Then itmEditMode_Click CInt(PreviousMode)
     
     'Cancel if in drawing operation
     CancelCurrentOperation
     
     'Load dialog
     Load frmFind
     
     'Set the height and hide replace controls
     frmFind.Height = 1860
     frmFind.txtReplace.visible = False
     frmFind.cmdBrowseReplace.visible = False
     frmFind.chkReplaceOnly.visible = False
     
     'Show dialog
     frmFind.Show 1, Me
End Sub

Private Sub itmEditFlipH_Click()
     Dim Deselect As Boolean
     
     'Switch back if in move mode
     If (mode = EM_MOVE) Then itmEditMode_Click CInt(PreviousMode)
     
     'Cancel if in drawing operation
     If (submode <> ESM_PASTING) Then CancelCurrentOperation
     
     'Check if no selection is made
     If (numselected = 0) And (submode <> ESM_PASTING) Then
          
          'If no highlight exists, leave
          If (currentselected = -1) Then Exit Sub
          
          'Make selection from highlight
          Select Case mode
               Case EM_VERTICES: SelectCurrentVertex
               Case EM_LINES: SelectCurrentLine
               Case EM_SECTORS: SelectCurrentSector
               Case EM_THINGS: SelectCurrentThing
          End Select
          
          'Remove selection after edit
          Deselect = True
     End If
     
     'Make undo
     If (submode <> ESM_PASTING) Then CreateUndo "flip horizontally"
     
     'Check if flipping things
     If (mode = EM_THINGS) Then
          
          'Perform the flip
          FlipThingsHorizontal
          
          'Map changed
          mapchanged = True
     Else
          
          'Check if the selection should be modified
          'If ((mode = EM_LINES) Or (mode = EM_SECTORS)) And (submode <> ESM_PASTING) Then SelectVerticesFromLinedefs
          If ((mode = EM_LINES) Or (mode = EM_SECTORS)) Then SelectVerticesFromLinedefs
          
          'Perform the flip
          FlipVerticesHorizontal
          
          'Check if the selection should be reversed
          'If (submode <> ESM_PASTING) Then
               ' We preserve the selected vertices when selecting lds iff we're
               ' pasting. This is not a state we want to keep ourselves in for
               ' too long, however, so must we reset the vertex selection at the
               ' first opportunity.
               If (mode = EM_LINES) Or (mode = EM_SECTORS) Then SelectLinedefsFromVertices (submode = ESM_PASTING)
               If (mode = EM_SECTORS) Then SelectSectorsFromLinedefs
          'End If
          
          'Map changed
          mapchanged = True
          mapnodeschanged = True
     End If
     
     'Check if we should deselect
     If Deselect Then
          
          'Remove selection
          RemoveSelection False
     End If
     
     'Redraw map
     RedrawMap
End Sub

Private Sub itmEditFlipV_Click()
     Dim Deselect As Boolean
     
     'Switch back if in move mode
     If (mode = EM_MOVE) Then itmEditMode_Click CInt(PreviousMode)
     
     'Cancel if in drawing operation
     If (submode <> ESM_PASTING) Then CancelCurrentOperation
     
     'Check if no selection is made
     If (numselected = 0) And (submode <> ESM_PASTING) Then
          
          'If no highlight exists, leave
          If (currentselected = -1) Then Exit Sub
          
          'Make selection from highlight
          Select Case mode
               Case EM_VERTICES: SelectCurrentVertex
               Case EM_LINES: SelectCurrentLine
               Case EM_SECTORS: SelectCurrentSector
               Case EM_THINGS: SelectCurrentThing
          End Select
          
          'Remove selection after edit
          Deselect = True
     End If
     
     'Make undo
     If (submode <> ESM_PASTING) Then CreateUndo "flip horizontally"
     
     'Check if flipping things
     If (mode = EM_THINGS) Then
          
          'Perform the flip
          FlipThingsVertical
          
          'Map changed
          mapchanged = True
     Else
          
          'Check if the selection should be modified
          'If ((mode = EM_LINES) Or (mode = EM_SECTORS)) And (submode <> ESM_PASTING) Then SelectVerticesFromLinedefs
          If ((mode = EM_LINES) Or (mode = EM_SECTORS)) Then SelectVerticesFromLinedefs
          
          'Perform the flip
          FlipVerticesVertical
          
          'Check if the selection should be reversed
          'If (submode <> ESM_PASTING) Then
               ' We preserve the selected vertices when selecting lds iff we're
               ' pasting. This is not a state we want to keep ourselves in for
               ' too long, however, so must we reset the vertex selection at the
               ' first opportunity.
               If (mode = EM_LINES) Or (mode = EM_SECTORS) Then SelectLinedefsFromVertices (submode = ESM_PASTING)
               If (mode = EM_SECTORS) Then SelectSectorsFromLinedefs
          'End If
          
          'Map changed
          mapchanged = True
          mapnodeschanged = True
     End If
     
     'Check if we should deselect
     If Deselect Then
          
          'Remove selection
          RemoveSelection False
     End If
     
     'Redraw map
     RedrawMap
End Sub

Public Sub itmEditMapOptions_Click()
#If Not CCDEBUG Then
     On Error GoTo MapOptionsError
#End If
     Dim OldGame As String
     Dim OldLumpName As String
     Dim MapLumpIndex As Long
     Dim OldMapFormat As Long
     Dim CurIWADFile As String
     Dim OldAddWad As String
     
     'Leave when map edit is disabled
     If (picMap.Enabled = False) Then Exit Sub
     
     'Remove highlight
     If (mapfile <> "") Then RemoveHighlight True
     
     'Cancel if in drawing operation
     CancelCurrentOperation
     
     'Keep old settings
     OldGame = mapgame
     OldLumpName = maplumpname
     OldMapFormat = mapconfig("mapformat")
     OldAddWad = addwadfile
     
     'Change the map options
     If (ChangeMapOptions) And (mapfile <> "") Then
          
          'Change mousepointer
          Screen.MousePointer = vbHourglass
          
          'Show status dialog
          frmStatus.Show 0, frmMain
          frmMain.SetFocus
          frmMain.Refresh
          
          'Load the error log
          ErrorLog_Load
          
          'Load new map configuration
          DisplayStatus "Loading configuration..."
          LoadMapConfiguration mapgame
          
          'Check if the header lump was renamed
          If (OldLumpName <> maplumpname) Then
               
               'Remember the lump name as it was saved so we can remove that later
               If (mapoldlumpname = "") Then mapoldlumpname = OldLumpName
          End If
          
          'Check if map format changed
          If (OldMapFormat <> mapconfig("mapformat")) Then
               
               'Show warning
               MsgBox "WARNING: You have changed the map file format!" & vbLf & "Because your map is not designed for this format, it may not work correctly in the game!", vbCritical
          End If
          
          'Only reload textures and flat when game (IWAD) changed or additional file changed
          If (OldGame <> mapgame) Or (OldAddWad <> addwadfile) Then
               
               'Close additional wads
               IWAD.CloseFile
               AddWAD.CloseFile
               
               'Open additional wads
               OpenIWADFile
               OpenADDWADFile
               
               'Precache resources
               MapLoadResources
          End If
          
          'Create data structure optimizations
          DisplayStatus "Optimizing data structures..."
          CreateOptimizations
          
          'Unload status dialog
          Unload frmStatus: Set frmSplash = Nothing
          
          'Reset mousepointer
          Screen.MousePointer = vbDefault
          
          'Show the errors and warnings dialog
          ErrorLog_DisplayAndFlush
          
          'Update scripts menu
          UpdateScriptLumpsMenu
          
          'Re-select current editing mode
          'This will clear selection and redraw map
          itmEditMode_Click CInt(mode)
     End If
     
     'Map changed
     mapchanged = True
     
     'We're done here
     Exit Sub
     
     
MapOptionsError:
     
     'Show error message
     MsgBox "Error " & Err.number & " while changing map options: " & Err.Description, vbCritical
     
     'Unload dialog
     Unload frmStatus: Set frmSplash = Nothing
     
     'Reset mousepointer
     Screen.MousePointer = vbDefault
End Sub

Private Sub imgCeiling_Click()

     Dim NewFlat As String
     Dim v As Variant
     
     imgCeiling.MousePointer = vbNormal
     
     NewFlat = SelectFlat(lblCeiling.Caption, Me)
     
     ' New texture selected?
     If NewFlat <> lblCeiling.Caption Then
     
          ' Update panel.
          lblCeiling.Caption = NewFlat
          GetScaledFlatPicture NewFlat, imgCeiling
          imgCeiling.Refresh
          
          ' Set all selected linedefs' textures.
          For Each v In selected.Keys
               sectors(v).tceiling = NewFlat
          Next v
          
     End If
     
     imgCeiling.MousePointer = vbCustom
     
     mapchanged = True

End Sub

Private Sub imgFloor_Click()

     Dim NewFlat As String
     Dim v As Variant
     
     imgFloor.MousePointer = vbNormal
     
     NewFlat = SelectFlat(lblFloor.Caption, Me)
     
     ' New texture selected?
     If NewFlat <> lblFloor.Caption Then
     
          ' Update panel.
          lblFloor.Caption = NewFlat
          GetScaledFlatPicture NewFlat, imgFloor
          imgFloor.Refresh
          
          ' Set all selected linedefs' textures.
          For Each v In selected.Keys
               sectors(v).TFloor = NewFlat
          Next v
          
     End If
     
     imgFloor.MousePointer = vbCustom
     
     mapchanged = True

End Sub

Private Sub imgS1Lower_Click()

     Dim NewTex As String
     Dim v As Variant
     
     imgS1Lower.MousePointer = vbNormal
     
     NewTex = SelectTexture(lblS1Lower.Caption, Me)
     
     ' New texture selected?
     If NewTex <> lblS1Lower.Caption Then
     
          ' Update panel.
          lblS1Lower.Caption = NewTex
          GetScaledTexturePicture NewTex, imgS1Lower
          imgS1Lower.Refresh
          
          ' Set all selected linedefs' textures.
          For Each v In selected.Keys
               sidedefs(linedefs(v).s1).Lower = NewTex
          Next v
          
     End If
     
     imgS1Lower.MousePointer = vbCustom
     
     mapchanged = True

End Sub

Private Sub imgS1Middle_Click()

     Dim NewTex As String
     Dim v As Variant
     
     imgS1Middle.MousePointer = vbNormal
     
     NewTex = SelectTexture(lblS1Middle.Caption, Me)
     
     ' New texture selected?
     If NewTex <> lblS1Middle.Caption Then
     
          ' Update panel.
          lblS1Middle.Caption = NewTex
          GetScaledTexturePicture NewTex, imgS1Middle
          imgS1Middle.Refresh
          
          ' Set all selected linedefs' textures.
          For Each v In selected.Keys
               sidedefs(linedefs(v).s1).Middle = NewTex
          Next v
          
     End If
     
     imgS1Middle.MousePointer = vbCustom
     
     mapchanged = True

End Sub



Private Sub imgS1Upper_Click()

     Dim NewTex As String
     Dim v As Variant
     
     imgS1Upper.MousePointer = vbNormal
     
     NewTex = SelectTexture(lblS1Upper.Caption, Me)
     
     ' New texture selected?
     If NewTex <> lblS1Upper.Caption Then
     
          ' Update panel.
          lblS1Upper.Caption = NewTex
          GetScaledTexturePicture NewTex, imgS1Upper
          imgS1Upper.Refresh
          
          ' Set all selected linedefs' textures.
          For Each v In selected.Keys
               sidedefs(linedefs(v).s1).Upper = NewTex
          Next v
          
     End If
     
     imgS1Upper.MousePointer = vbCustom
     
     mapchanged = True

End Sub



Private Sub imgS2Lower_Click()

     Dim NewTex As String
     Dim v As Variant
     
     imgS2Lower.MousePointer = vbNormal
     
     NewTex = SelectTexture(lblS2Lower.Caption, Me)
     
     ' New texture selected?
     If NewTex <> lblS2Lower.Caption Then
     
          ' Update panel.
          lblS2Lower.Caption = NewTex
          GetScaledTexturePicture NewTex, imgS2Lower
          imgS2Lower.Refresh
          
          ' Set all selected linedefs' textures.
          For Each v In selected.Keys
               sidedefs(linedefs(v).s2).Lower = NewTex
          Next v
          
     End If
     
     imgS2Lower.MousePointer = vbCustom
     
     mapchanged = True

End Sub

Private Sub imgS2Middle_Click()

     Dim NewTex As String
     Dim v As Variant
     
     imgS2Middle.MousePointer = vbNormal
     
     NewTex = SelectTexture(lblS2Middle.Caption, Me)
     
     ' New texture selected?
     If NewTex <> lblS2Middle.Caption Then
     
          ' Update panel.
          lblS2Middle.Caption = NewTex
          GetScaledTexturePicture NewTex, imgS2Middle
          imgS2Middle.Refresh
          
          ' Set all selected linedefs' textures.
          For Each v In selected.Keys
               sidedefs(linedefs(v).s2).Middle = NewTex
          Next v
          
     End If
     
     imgS2Middle.MousePointer = vbCustom
     
     mapchanged = True

End Sub

Private Sub imgS2Upper_Click()

     Dim NewTex As String
     Dim v As Variant
     
     imgS2Upper.MousePointer = vbNormal
     
     NewTex = SelectTexture(lblS2Upper.Caption, Me)
     
     ' New texture selected?
     If NewTex <> lblS2Upper.Caption Then
     
          ' Update panel.
          lblS2Upper.Caption = NewTex
          GetScaledTexturePicture NewTex, imgS2Upper
          imgS2Upper.Refresh
          
          ' Set all selected linedefs' textures.
          For Each v In selected.Keys
               sidedefs(linedefs(v).s2).Upper = NewTex
          Next v
          
     End If
     
     imgS2Upper.MousePointer = vbCustom
     
     mapchanged = True

End Sub

Private Sub imgSCeiling_Click()

     Dim NewFlat As String
     Dim v As Variant
     
     imgSCeiling.MousePointer = vbNormal
     
     NewFlat = SelectFlat(lblSCeiling.Caption, Me)
     
     ' New texture selected?
     If NewFlat <> lblSCeiling.Caption Then
     
          ' Update panel.
          lblSCeiling.Caption = NewFlat
          GetScaledFlatPicture NewFlat, imgSCeiling
          imgSCeiling.Refresh
          
          ' Set all selected linedefs' textures.
          For Each v In selected.Keys
               sectors(v).tceiling = NewFlat
          Next v
          
     End If
     
     imgSCeiling.MousePointer = vbCustom
     
     mapchanged = True

End Sub

Private Sub imgSFloor_Click()

     Dim NewFlat As String
     Dim v As Variant
     
     imgSFloor.MousePointer = vbNormal
     
     NewFlat = SelectFlat(lblSFloor.Caption, Me)
     
     ' New texture selected?
     If NewFlat <> lblSFloor.Caption Then
     
          ' Update panel.
          lblSFloor.Caption = NewFlat
          GetScaledFlatPicture NewFlat, imgSFloor
          imgSFloor.Refresh
          
          ' Set all selected linedefs' textures.
          For Each v In selected.Keys
               sectors(v).TFloor = NewFlat
          Next v
          
     End If
     
     imgSFloor.MousePointer = vbCustom
     
     mapchanged = True

End Sub

Private Sub imgSS1Lower_Click()

     Dim NewTex As String
     Dim v As Variant
     
     imgSS1Lower.MousePointer = vbNormal
     
     NewTex = SelectTexture(lblSS1Lower.Caption, Me)
     
     ' New texture selected?
     If NewTex <> lblSS1Lower.Caption Then
     
          ' Update panel.
          lblSS1Lower.Caption = NewTex
          GetScaledTexturePicture NewTex, imgSS1Lower
          imgSS1Lower.Refresh
          
          ' Set all selected linedefs' textures.
          For Each v In selected.Keys
               sidedefs(linedefs(v).s1).Lower = NewTex
          Next v
          
     End If
     
     imgSS1Lower.MousePointer = vbCustom
     
     mapchanged = True

End Sub

Private Sub imgSS1Middle_Click()

     Dim NewTex As String
     Dim v As Variant
     
     imgSS1Middle.MousePointer = vbNormal
     
     NewTex = SelectTexture(lblSS1Middle.Caption, Me)
     
     ' New texture selected?
     If NewTex <> lblSS1Middle.Caption Then
     
          ' Update panel.
          lblSS1Middle.Caption = NewTex
          GetScaledTexturePicture NewTex, imgSS1Middle
          imgSS1Middle.Refresh
          
          ' Set all selected linedefs' textures.
          For Each v In selected.Keys
               sidedefs(linedefs(v).s1).Middle = NewTex
          Next v
          
     End If
     
     imgSS1Middle.MousePointer = vbCustom
     
     mapchanged = True

End Sub



Private Sub imgSS1Upper_Click()

     Dim NewTex As String
     Dim v As Variant
     
     imgSS1Upper.MousePointer = vbNormal
     
     NewTex = SelectTexture(lblSS1Upper.Caption, Me)
     
     ' New texture selected?
     If NewTex <> lblSS1Upper.Caption Then
     
          ' Update panel.
          lblSS1Upper.Caption = NewTex
          GetScaledTexturePicture NewTex, imgSS1Upper
          imgSS1Upper.Refresh
          
          ' Set all selected linedefs' textures.
          For Each v In selected.Keys
               sidedefs(linedefs(v).s1).Upper = NewTex
          Next v
          
     End If
     
     imgSS1Upper.MousePointer = vbCustom
     
     mapchanged = True

End Sub

Private Sub imgSS2Lower_Click()

     Dim NewTex As String
     Dim v As Variant
     
     imgSS2Lower.MousePointer = vbNormal
     
     NewTex = SelectTexture(lblSS2Lower.Caption, Me)
     
     ' New texture selected?
     If NewTex <> lblSS2Lower.Caption Then
     
          ' Update panel.
          lblSS2Lower.Caption = NewTex
          GetScaledTexturePicture NewTex, imgSS2Lower
          imgSS2Lower.Refresh
          
          ' Set all selected linedefs' textures.
          For Each v In selected.Keys
               sidedefs(linedefs(v).s2).Lower = NewTex
          Next v
          
     End If
     
     imgSS2Lower.MousePointer = vbCustom
     
     mapchanged = True

End Sub

Private Sub imgSS2Middle_Click()

     Dim NewTex As String
     Dim v As Variant
     
     imgSS2Middle.MousePointer = vbNormal
     
     NewTex = SelectTexture(lblSS2Middle.Caption, Me)
     
     ' New texture selected?
     If NewTex <> lblSS2Middle.Caption Then
     
          ' Update panel.
          lblSS2Middle.Caption = NewTex
          GetScaledTexturePicture NewTex, imgSS2Middle
          imgSS2Middle.Refresh
          
          ' Set all selected linedefs' textures.
          For Each v In selected.Keys
               sidedefs(linedefs(v).s2).Middle = NewTex
          Next v
          
     End If
     
     imgSS2Middle.MousePointer = vbCustom
     
     mapchanged = True

End Sub

Private Sub imgSS2Upper_Click()

     Dim NewTex As String
     Dim v As Variant
     
     imgSS2Upper.MousePointer = vbNormal
     
     NewTex = SelectTexture(lblSS2Upper.Caption, Me)
     
     ' New texture selected?
     If NewTex <> lblSS2Upper.Caption Then
     
          ' Update panel.
          lblSS2Upper.Caption = NewTex
          GetScaledTexturePicture NewTex, imgSS2Upper
          imgSS2Upper.Refresh
          
          ' Set all selected linedefs' textures.
          For Each v In selected.Keys
               sidedefs(linedefs(v).s2).Upper = NewTex
          Next v
          
     End If
     
     imgSS2Upper.MousePointer = vbCustom
     
     mapchanged = True

End Sub

Private Sub itmEdit_Click(Index As Integer)

     ' Control limit workaround.

     Select Case Index
          ' Sep
          Case 1: itmEditCut_Click
          Case 2: itmEditCopy_Click
          Case 3: itmEditPaste_Click
          Case 4: itmEditDelete_Click
          ' Sep
          Case 6: itmEditFind_Click
          Case 7: itmEditReplace_Click
          ' Sep
          Case 9: itmEditFlipH_Click
          Case 10: itmEditFlipV_Click
          Case 11: itmEditRotate_Click
          Case 12: itmEditResize_Click
          ' Sep
          Case 14: itmEditSnapToGrid_Click
          Case 15: itmEditSnapToVertices_Click
          Case 16: itmEditStitch_Click
          Case 17: itmEditCenterView_Click
          ' Sep
          Case 19: itmEditSelectAll_Click
          Case 20: itmEditSelectNone_Click
          Case 21: itmEditSelectInvert_Click
          ' Sep
          Case 23: itmEditMapOptions_Click
     End Select

End Sub

Public Sub itmEditMode_Click(Index As Integer)
     
     'Cancel if in drawing operation
     CancelCurrentOperation
     
     'Remove highlight
     RemoveHighlight True
     
     'Toggle to previous mode?
     If (mode = EM_MOVE) And (Index = EM_MOVE) Then
          
          'Go to previous mode!
          Index = PreviousMode
          
     'From move mode?
     ElseIf (mode = EM_MOVE) And (PreviousMode <> EM_3D) Then
          
          'Go to previous mode first!
          itmEditMode_Click EM_MOVE
     End If
     
     'Keep previous mode
     PreviousMode = mode
     
     'Check what to convert From
     Select Case mode
          
          Case EM_VERTICES ', EM_MOVE
               
               'Check what to convert To
               Select Case Index
                    Case EM_LINES: SelectLinedefsFromVertices
                    Case EM_SECTORS: SelectLinedefsFromVertices: SelectSectorsFromLinedefs
                    Case EM_THINGS: RemoveSelection False   'Dont convert, just deselect
               End Select
               
               HideVertexInfo
               
          Case EM_LINES
               
               'Check what to convert To
               Select Case Index
                    Case EM_VERTICES: SelectVerticesFromLinedefs      ', EM_MOVE
                    Case EM_SECTORS: SelectSectorsFromLinedefs
                    Case EM_THINGS: RemoveSelection False   'Dont convert, just deselect
               End Select
               
               HideLinedefInfo
               
               ' Disable toolbar buttons
               tlbToolbar.Buttons("LinesFlip").Enabled = False
               tlbToolbar.Buttons("LinesCurve").Enabled = False
               tlbToolbar.Buttons("LinesSplit").Enabled = False
               tlbToolbar.Buttons("GradientTags").Enabled = False
               
          Case EM_SECTORS
               
               'Check what to convert To
               Select Case Index
                    Case EM_VERTICES: SelectVerticesFromLinedefs      ', EM_MOVE
                    Case EM_LINES: SelectLinedefsFromSectors
                    Case EM_THINGS: RemoveSelection False   'SelectThingsFromSectors is too slow, must find a faster solution
               End Select
               
               HideSectorInfo
               
               ' Disable toolbar buttons
               tlbToolbar.Buttons("SectorsJoin").Enabled = False
               tlbToolbar.Buttons("SectorsMerge").Enabled = False
               tlbToolbar.Buttons("SectorsGradientBrightness").Enabled = False
               tlbToolbar.Buttons("SectorsGradientFloors").Enabled = False
               tlbToolbar.Buttons("SectorsGradientCeilings").Enabled = False
               tlbToolbar.Buttons("GradientTags").Enabled = False
               tlbToolbar.Buttons("SectorsSetupFOF").Enabled = False
               
          Case EM_THINGS
               
               ' Don't convert, just deselect
               RemoveSelection False
               
               ' Disable toolbar buttons
               tlbToolbar.Buttons("ThingsFilter").Enabled = False
               
               HideThingInfo
               
          Case EM_3D
               
               'Stop 3D Mode
               Stop3DMode
               
     End Select
     
     'Deselect current mode
     itmEditMode(mode).Checked = False
     
     'Hide menus
     mnuVertices.visible = False
     mnuLines.visible = False
     mnuSectors.visible = False
     mnuThings.visible = False
     
     'Change mode
     mode = Index
     submode = ESM_NONE
     
     'Select current mode
     itmEditMode(mode).Checked = True
     
     'Do stuff for this new mode
     Select Case Index
          
          Case EM_MOVE
               Set picMap.MouseIcon = imgCursor(1).Picture
               picMap.MousePointer = vbCustom
               tlbToolbar.Buttons("ModeMove").Value = tbrPressed
               selectedtype = EM_MOVE
               lblMode.Caption = "Move"
               
          Case EM_VERTICES
               picMap.MousePointer = vbNormal
               tlbToolbar.Buttons("ModeVertices").Value = tbrPressed
               mnuVertices.visible = True
               selectedtype = EM_VERTICES
               lblMode.Caption = "Vertices"
               
          Case EM_LINES
               picMap.MousePointer = vbNormal
               tlbToolbar.Buttons("ModeLines").Value = tbrPressed
               mnuLines.visible = True
               selectedtype = EM_LINES
               lblMode.Caption = "Lines"
               
               ' Enable toolbar buttons
               tlbToolbar.Buttons("LinesFlip").Enabled = True
               tlbToolbar.Buttons("LinesCurve").Enabled = True
               tlbToolbar.Buttons("GradientTags").Enabled = True
               tlbToolbar.Buttons("LinesSplit").Enabled = True
               
               ' Show selection info.
               If numselected > 0 Then ShowSelectedLinedefInfo
               
          Case EM_SECTORS
               picMap.MousePointer = vbNormal
               tlbToolbar.Buttons("ModeSectors").Value = tbrPressed
               mnuSectors.visible = True
               selectedtype = EM_SECTORS
               lblMode.Caption = "Sectors"
               
               ' Enable toolbar buttons
               tlbToolbar.Buttons("SectorsJoin").Enabled = True
               tlbToolbar.Buttons("SectorsMerge").Enabled = True
               tlbToolbar.Buttons("SectorsGradientBrightness").Enabled = True
               tlbToolbar.Buttons("SectorsGradientFloors").Enabled = True
               tlbToolbar.Buttons("SectorsGradientCeilings").Enabled = True
               tlbToolbar.Buttons("GradientTags").Enabled = True
               tlbToolbar.Buttons("SectorsSetupFOF").Enabled = True
               
               ' Show selection info.
               If numselected > 0 Then ShowSelectedSectorInfo
               
          Case EM_THINGS
               picMap.MousePointer = vbNormal
               tlbToolbar.Buttons("ModeThings").Value = tbrPressed
               mnuThings.visible = True
               selectedtype = EM_THINGS
               lblMode.Caption = "Things"
               
               ' Enable toolbar buttons
               tlbToolbar.Buttons("ThingsFilter").Enabled = True
               
          Case EM_3D
               picMap.MousePointer = vbNormal
               lblMode.Caption = "3D Mode"
               tlbToolbar.Buttons("Mode3D").Value = tbrPressed
               
               'Check if anything exists
               If (numsectors < 2) Then
                    
                    'Cant build nodes
                    MsgBox "You need at least 2 sectors before going into 3D mode.", vbExclamation
                    
                    'Switch back to previous mode
                    itmEditMode_Click CInt(PreviousMode)
                    Exit Sub
               Else
                    
                    'Check if 3D mode is configured
                    While (Trim$(Config("videoadapterdesc")) = "") And (Val(Config("windowedvideo")) = 0)
                         
                         'Ask the user now
                         If (MsgBox("You have not yet configured your 3D Mode settings." & vbLf & _
                                   "Please click OK to configure the settings now.", vbInformation Or vbOKCancel) = VbMsgBoxResult.vbOK) Then
                              
                              'Show configuration
                              ShowConfiguration 8
                         Else
                              
                              'Switch back to previous mode
                              itmEditMode_Click CInt(PreviousMode)
                              Exit Sub
                         End If
                    Wend
                    
                    'Show status dialog
                    frmStatus.Show 0, frmMain
                    frmStatus.Refresh
                    frmMain.SetFocus
                    frmMain.Refresh
                    
                    'Must build nodes
                    If (mapnodeschanged) Or (TestStructures(TempWAD) = False) Then
                         
                         'Build nodes and check for errors
                         If MapBuild(True, False) = False Then
                              
                              'Unload status dialog
                              Screen.MousePointer = vbNormal
                              Unload frmStatus
                              Set frmStatus = Nothing
                              
                              'Nodebuilder failed!
                              MsgBox "The nodebuilder did not build the required structures." & vbLf & "Please check your map for errors or select a different nodebuilder!", vbCritical
                              
                              'Switch back to previous mode
                              itmEditMode_Click CInt(PreviousMode)
                              Exit Sub
                         End If
                    End If
                    
                    'Disable editing
                    picMap.Enabled = False
                    
                    'Set status
                    DisplayStatus "Building structures..."
                    
                    'Make triangles from SSECTORS
                    If PrepareStructures(TempWAD) Then
                         
                         'Set status
                         DisplayStatus "Switching to 3D mode..."
                         
                         'Hide
                         PreviousWindowstate = frmMain.WindowState
                         If (Val(Config("windowedvideo")) = 0) Then frmMain.WindowState = vbMinimized
                         
                         'Set the default settings
                         Init3DModeDefaults
                         
                         'Start the 3D Mode
                         If (Start3DMode) Then
                              
                              'Enable editing
                              picMap.Enabled = True
                              
                              'Run it now
                              Run3DMode
                         Else
                              
                              'Switch back to previous mode
                              itmEditMode_Click CInt(PreviousMode)
                         End If
                    Else
                         
                         'Switch back to previous mode
                         itmEditMode_Click CInt(PreviousMode)
                    End If
               End If
               
               'Leave here
               Exit Sub
     End Select
     
     'Redraw entire map
     RedrawMap
End Sub

Private Sub itmEditPaste_Click()
     Dim PasteMode As ENUM_PREFABINCLUDEMODE
     Dim PasteX As Long, PasteY As Long
     
     'Leave when map edit is disabled
     If (picMap.Enabled = False) Then Exit Sub
     
     'Check if not in normal mode
     If (submode <> ESM_NONE) Then
          
          'Show message, cant right now
          MsgBox "Cannot perform Paste while in an operation." & vbLf & "Please finish your operation first!", vbExclamation
          
          'Leave here
          Exit Sub
     End If
     
     'Remove highlight
     If (mapfile <> "") Then RemoveHighlight True
     
     'Switch back if in move mode
     If (mode = EM_MOVE) Then itmEditMode_Click CInt(PreviousMode)
     
     'Make undo
     CreateUndo "paste"
     
     'Check if we can paste at mouse location
     If MouseInside Then
          
          'Mouse position
          PasteX = LastX
          PasteY = LastY
     Else
          
          'Center of screen
          PasteX = picMap.ScaleLeft + picMap.ScaleWidth / 2
          PasteY = picMap.ScaleTop + picMap.ScaleHeight / 2
     End If
     
     'Determine paste mode by mode
     Select Case mode
          Case EM_VERTICES: PasteMode = PIM_VERTICES   'Paste vertices only
          Case EM_LINES: PasteMode = PIM_STRUCTURE
          Case EM_SECTORS: PasteMode = PIM_STRUCTURE
          Case EM_THINGS: PasteMode = PIM_THINGS
     End Select
     
     'Paste and check if anything was pasted
     If (InsertPrefab(ClipboardFile, PasteX, PasteY, PasteMode) > 0) Then
          
          'If in lines mode
          If (mode = EM_LINES) Then
               
               'Select lines from vertices
               SelectLinedefsFromVertices
               
          'If in sectors mode
          ElseIf (mode = EM_SECTORS) Then
               
               'Select sectors from vertices
               SelectLinedefsFromVertices
               SelectSectorsFromLinedefs
          End If
          
          'Grab it
          GrabX = PasteX
          GrabY = PasteY
          
          'Start pasting
          StartPasteOperation PasteX, PasteY
          
          UpdateFOFTagCache
          UpdateNiGHTSLineCache
          If IsLoaded(frmTags) Then frmTags.RefreshTagList
     Else
          
          'Nothing to paste
          MsgBox "Nothing to paste in the current mode.", vbInformation
     End If
     
     'Redraw map
     RedrawMap
End Sub

Private Sub itmEditRedo_Click()
     
     'Leave when map edit is disabled
     If (picMap.Enabled = False) Then Exit Sub
     
     'Remove highlight
     If (mapfile <> "") Then RemoveHighlight True
     
     'Check if not in normal mode
     If (submode <> ESM_NONE) Then
          
          'Show message, cant right now
          MsgBox "Cannot perform Redo while in an operation." & vbLf & "Please finish your operation first!", vbExclamation
          
          'Leave here
          Exit Sub
     End If
     
     'Change mousepointer
     Screen.MousePointer = vbHourglass
     
     'Remove selection and highlight
     RemoveHighlight
     RemoveSelection False
     
     'Do the redo
     PerformRedo
     
     ' Recalc drawing caches.
     UpdateFOFTagCache
     UpdateNiGHTSLineCache
     If IsLoaded(frmTags) Then frmTags.RefreshTagList
     
     'Update statusbar
     UpdateStatusBar
     
     'Redraw map
     RedrawMap
     
     'Reset mousepointer
     Screen.MousePointer = vbNormal
End Sub

Private Sub itmEditReplace_Click()
     
     'Switch back if in move mode
     If (mode = EM_MOVE) Then itmEditMode_Click CInt(PreviousMode)
     
     'Cancel if in drawing operation
     CancelCurrentOperation
     
     'Load dialog
     Load frmFind
     frmFind.Caption = "Find and Replace"
     
     'Show dialog
     frmFind.Show 1, Me
End Sub

Private Sub itmEditResize_Click()
     Dim Deselect As Boolean
     
     'Switch back if in move mode
     If (mode = EM_MOVE) Then itmEditMode_Click CInt(PreviousMode)
     
     'Cancel if in drawing operation
     If (submode <> ESM_PASTING) Then CancelCurrentOperation
     
     'Check if no selection is made
     If (numselected = 0) And (submode <> ESM_PASTING) Then
          
          'If no highlight exists, leave
          If (currentselected = -1) Then Exit Sub
          
          'Make selection from highlight
          Select Case mode
               Case EM_VERTICES: SelectCurrentVertex
               Case EM_LINES: SelectCurrentLine
               Case EM_SECTORS: SelectCurrentSector
               Case EM_THINGS: SelectCurrentThing
          End Select
          
          'Remove selection after edit
          Deselect = True
     End If
     
     'Make undo
     CreateUndo "resize"
     
     'Rotate
     Load frmResize
     frmResize.Show 1, Me
     
     'Check if we should deselect
     If Deselect Then
          
          'Remove selection
          RemoveSelection False
     End If
     
     'Redraw
     RedrawMap
End Sub

Private Sub itmEditRotate_Click()
     Dim Deselect As Boolean
     
     'Switch back if in move mode
     If (mode = EM_MOVE) Then itmEditMode_Click CInt(PreviousMode)
     
     'Cancel if in drawing operation
     If (submode <> ESM_PASTING) Then CancelCurrentOperation
     
     'Check if no selection is made
     If (numselected = 0) And (submode <> ESM_PASTING) Then
          
          'If no highlight exists, leave
          If (currentselected = -1) Then Exit Sub
          
          'Make selection from highlight
          Select Case mode
               Case EM_VERTICES: SelectCurrentVertex
               Case EM_LINES: SelectCurrentLine
               Case EM_SECTORS: SelectCurrentSector
               Case EM_THINGS: SelectCurrentThing
          End Select
          
          'Remove selection after edit
          Deselect = True
     End If
     
     'Make undo
     CreateUndo "rotate"
     
     'Rotate
     Load frmRotate
     frmRotate.Show 1, Me
     
     'Check if we should deselect
     If Deselect Then
          
          'Remove selection
          RemoveSelection False
     End If
     
     'Redraw
     RedrawMap
End Sub

Private Sub itmEditSelectAll_Click()

     Select Case mode
          Case EM_SECTORS: SelectAllSectors
          Case EM_LINES: SelectAllLinedefs
          Case EM_VERTICES: SelectAllVertices
          Case EM_THINGS: SelectAllThings
     End Select
     
     RedrawMap True

End Sub

Private Sub itmEditSelectNone_Click()

     RemoveSelection True

End Sub

Private Sub itmEditSelectInvert_Click()

     Select Case mode
          Case EM_SECTORS: InvertSectorsSelection
          Case EM_LINES: InvertLinedefsSelection
          Case EM_VERTICES: InvertVerticesSelection
          Case EM_THINGS: InvertThingsSelection
     End Select
     
     RedrawMap True

End Sub

Private Sub itmEditSnapToGrid_Click()
     
     'Toggle snap mode
     snapmode = Not snapmode
     itmEdit(14).Checked = snapmode
     tlbToolbar.Buttons("EditSnap").Value = Abs(snapmode)
     UpdateStatusBar
End Sub

Private Sub itmEditSnapToVertices_Click()
     
     'Toggle vertex-snapping
     snaptovertices = Not snaptovertices
     itmEdit(15).Checked = snaptovertices
End Sub

Private Sub itmEditStitch_Click()
     
     'Toggle stitch mode
     stitchmode = Not stitchmode
     itmEdit(16).Checked = stitchmode
     tlbToolbar.Buttons("EditStitch").Value = Abs(stitchmode)
     UpdateStatusBar
End Sub

Private Sub itmEditUndo_Click()
     
     'Leave when map edit is disabled
     If (picMap.Enabled = False) Or mapfile = "" Then Exit Sub
     
     RemoveHighlight

     'Check if not in normal mode
     If (submode <> ESM_NONE) Then
          
          'Just cancel the operation
          CancelCurrentOperation
     Else
          
          RemoveSelection False
          
          'Change mousepointer
          Screen.MousePointer = vbHourglass
          
          'Do the undo
          PerformUndo
          
          ' Recalc drawing caches.
          UpdateFOFTagCache
          UpdateNiGHTSLineCache
          If IsLoaded(frmTags) Then frmTags.RefreshTagList
          
          'Update statusbar
          UpdateStatusBar
          
          'Redraw map
          RedrawMap
          
          'Reset mousepointer
          Screen.MousePointer = vbNormal
     End If
     
End Sub

Private Sub itmFileBuild_Click()
     
     'Leave when map edit is disabled
     If (picMap.Enabled = False) Then Exit Sub
     
     'Cancel if in drawing operation
     CancelCurrentOperation
     
     'Change mousepointer
     Screen.MousePointer = vbHourglass
     
     'Check if anything exists
     If (numsectors < 2) Then
          
          'Cant build nodes
          MsgBox "You need at least 2 sectors before nodes can be build.", vbExclamation
     Else
          
          'Disable editing
          picMap.Enabled = False
          
          'Build nodes
          If MapBuild(False, False) = False Then
               
               'Nodebuilder failed!
               MsgBox "The nodebuilder did not build the required structures." & vbLf & "Please ensure you do not have any errors in your map!", vbCritical
          Else
               
               'Map changed
               mapchanged = True
          End If
     End If
     
     'Reset mousepointer
     Screen.MousePointer = vbDefault
     
     'Enable editing
     picMap.Enabled = True
End Sub

Private Sub itmFileCloseMap_Click()
     
     'Leave when map edit is disabled
     If (picMap.Enabled = False) Then Exit Sub
     
     'Cancel if in drawing operation
     CancelCurrentOperation
     
     'Unload map
     MapUnload
End Sub

Private Sub itmFile_Click(Index As Integer)
     Select Case Index
          
          Case 0: itmFileNew_Click
          Case 1: itmFileOpenMap_Click
          Case 2: itmFileSwitchMap_Click
          Case 3: itmFileCloseMap_Click
          
          Case 5: itmFileSaveMap_Click
          Case 6: itmFileSaveMapAs_Click
          Case 7: itmFileSaveMapInto_Click
          
          Case 9: itmFileExportMap_Click
          Case 10: itmFileExportPicture_Click
          
          Case 12: itmFileBuild_Click
          Case 13: itmFileTest_Click False
          
     End Select
End Sub

Private Sub itmFileExit_Click()
     Unload Me
End Sub

Private Sub itmFileExportMap_Click()
     Dim result As String
     Dim FilterIndex As Long
     Dim HasChanged As Boolean
     
     'Leave when map edit is disabled
     If (picMap.Enabled = False) Then Exit Sub
     
     'Ensure the splash dialog is gone
     'It would be odd if it was still displayed though
     Unload frmSplash: Set frmSplash = Nothing
     
     'Cancel if in drawing operation
     CancelCurrentOperation
     
     'Show save dialog
     result = SaveFile(Me.hWnd, "Export Map As", "Doom/Heretic/Hexen WAD Files   *.wad|*.wad|Wavefront OBJ Files   *.obj|*.obj|All Files|*.*", mapfilename, cdlOFNExplorer Or cdlOFNHideReadOnly Or cdlOFNLongNames Or cdlOFNPathMustExist Or cdlOFNOverwritePrompt, FilterIndex)
     frmMain.Refresh

     'Check if not cancelled
     If result <> "" Then
          
          'Add extension if needed
          If (LCase$(right$(result, 4)) <> ".wad") And (FilterIndex = 1) Then result = result & ".wad"
          If (LCase$(right$(result, 4)) <> ".obj") And (FilterIndex = 2) Then result = result & ".obj"
          
          'Check if exporting as OBJ
          If (FilterIndex = 2) Then
               
               'Delete file if it exists
               If (Dir(result) <> "") Then Kill result
               
               'Change mousepointer
               Screen.MousePointer = vbHourglass
               
               'Call the DLL API for writing the file
               ExportWavefrontObj result, vertexes(0), linedefs(0), VarPtr(sidedefs(0)), VarPtr(sectors(0)), things(0), numvertexes, numlinedefs, numsidedefs, numsectors, numthings
               
               'Reset mousepointer
               Screen.MousePointer = vbDefault
               
          Else
               
               'Make undo
               CreateUndo "Sidedefs compression", UGRP_NONE, 0, False
               
     '          'Give export dialog
     '          Load frmExport
     '          frmExport.txtTargetFile = Result
     '
     '          'Show dialog
     '          frmExport.Show 1, Me
     '
     '          'Check result
     '          If (frmExport.tag = "OK") Then
                    
                    'Keep changed status
                    HasChanged = mapchanged
                    
                    'Export the map
                    MapSave result, SM_EXPORT, (Config("buildexportcompression") = vbChecked)
                    
                    'Restore changed status
                    '(The map is not saved to its original file)
                    mapchanged = HasChanged
     '          End If
     '
     '          'Unload dialog
     '          Unload frmExport
               
               'Restore undo
               PerformUndo False
               
               'Remove the redo
               WithdrawRedo
          End If
     End If
End Sub

Private Sub itmFileNew_Click()
     
     'Leave when map edit is disabled
     If (picMap.Enabled = False) Then Exit Sub
     
     'Ensure the splash dialog is gone
     Unload frmSplash
     
     'Cancel if in drawing operation
     CancelCurrentOperation
     
     'Unload old map
     If (MapUnload) Then
          
          'Make new map
          MapNew "MAP01", True, True
     End If
End Sub

Private Sub itmFileOpenMap_Click()
     Dim result As String
     Dim BeginFile As String
     
     'Leave when map edit is disabled
     If (picMap.Enabled = False) Then Exit Sub
     
     'Ensure the splash dialog is gone
     Unload frmSplash: Set frmSplash = Nothing
     
     'Cancel if in drawing operation
     CancelCurrentOperation
     
     'Open dialog
     If (Config("recent").Exists("1") = True) Then BeginFile = Config("recent")("1")
     result = OpenFile(Me.hWnd, "Open Map", "Doom/Heretic/Hexen WAD Files   *.wad|*.wad|All Files|*.*", BeginFile, cdlOFNExplorer Or cdlOFNFileMustExist Or cdlOFNHideReadOnly Or cdlOFNLongNames)
     frmMain.Refresh
     
     'Check if not cancelled
     If result <> "" Then
          
          'Load the select map dialog
          Load frmMapSelect
          
          'Set the tag and caption
          frmMapSelect.Tag = result
          frmMapSelect.Caption = "Select Map from " & Dir(result)
          
          'Show the select dialog
          frmMapSelect.Show 1, Me
     End If
End Sub

Private Sub itmFileRecent_Click(Index As Integer)
     
     'Leave when map edit is disabled
     If (picMap.Enabled = False) Then Exit Sub
     
     'Ensure the splash dialog is gone
     Unload frmSplash: Set frmSplash = Nothing
     
     'Cancel if in drawing operation
     CancelCurrentOperation
     
     'Load the select map dialog
     Load frmMapSelect
     
     'Set the tag and caption
     frmMapSelect.Tag = itmFileRecent(Index).Tag
     frmMapSelect.Caption = "Select Map from " & Dir(itmFileRecent(Index).Tag)
     
     'Show the select dialog
     frmMapSelect.Show 1, Me
End Sub

Public Sub itmFileSaveMap_Click()
     
     'Leave when map edit is disabled
     If (picMap.Enabled = False) Then Exit Sub
     
     'Cancel if in drawing operation
     CancelCurrentOperation
     
     'Check if changes were made
     If mapchanged Then
          
          'Check if the filename is different from filetitle
          If (mapsaved = True) And (Dir(mapfile) <> "") Then
               
               'Check if file is read-only
               If ((GetAttr(mapfile) And vbReadOnly) = vbReadOnly) Then
                    
                    'Save As...
                    itmFileSaveMapAs_Click
               Else
                    
                    'Save right away
                    If MapSave(mapfile, SM_SAVE) Then
                         
                         'Change the map filename
                         mapfilename = Dir(mapfile)
                         frmMain.Caption = App.Title & " - " & mapfilename & " (" & maplumpname & ")"
                         mapchanged = False
                         mapsaved = True
                         EnableMapEditing    ' Enables map switch now that we've saved.
                    End If
               End If
          Else
               
               'Do a Save As...
               itmFileSaveMapAs_Click
          End If
     End If
End Sub

Private Sub itmFileSaveMapAs_Click()
     Dim result As String
     Dim FilterIndex As Long
     Dim ResultOK As Boolean
     Dim SaveMethod As ENUM_SAVEMODES
     
     'Leave when map edit is disabled
     If (picMap.Enabled = False) Then Exit Sub
     
     'Ensure the splash dialog is gone
     'It would be odd if it was still displayed though
     Unload frmSplash: Set frmSplash = Nothing
     
     'Cancel if in drawing operation
     CancelCurrentOperation
     
     'Continue
     Do
          'Show save dialog
          result = SaveFile(Me.hWnd, "Save Map As", "Doom/Heretic/Hexen WAD Files   *.wad|*.wad|All Files|*.*", mapfilename, cdlOFNExplorer Or cdlOFNHideReadOnly Or cdlOFNLongNames Or cdlOFNPathMustExist Or cdlOFNOverwritePrompt, FilterIndex)
          frmMain.Refresh
          
          'Check if not cancelled
          If result <> "" Then
               
               'Add extension
               If (LCase$(right$(result, 4)) <> ".wad") And (FilterIndex = 1) Then result = result & ".wad"
               
               'Check if file exists
               If (Dir(result) <> "") Then
                    
                    'Check if file is read-only
                    If ((GetAttr(result) And vbReadOnly) = vbReadOnly) Then
                         
                         'Cannot save to read-only file
                         If (MsgBox("The file you selected is marked Read Only and cannot be changed." & vbLf & "Please enter a new file or select another file to overwrite.", vbExclamation Or vbOKCancel) = vbCancel) Then result = ""
                    Else
                         
                         'OK
                         ResultOK = True
                    End If
               Else
                    
                    'OK
                    ResultOK = True
               End If
          End If
          
     'Continue until cancelled or OK
     Loop Until (result = "") Or (ResultOK = True)
     
     'Check if not cancelled
     If (result <> "") Then
          
          'Check if the same file as current file
          If (StrComp(Trim$(result), Trim$(mapfile), vbTextCompare) = 0) Then
               
               'Save normally
               SaveMethod = SM_SAVE
          Else
               
               'Save as new file
               SaveMethod = SM_SAVEAS
          End If
          
          'Save the map here
          If MapSave(result, SaveMethod) Then
               
               'Change the map filename
               mapfile = result
               mapfilename = Dir(mapfile)
               frmMain.Caption = App.Title & " - " & mapfilename & " (" & maplumpname & ")"
               mapchanged = False
               mapsaved = True
               EnableMapEditing    ' Enables map switch now that we've saved.
          End If
     End If
End Sub

Private Sub itmFileSaveMapInto_Click()
     Dim result As String
     Dim FilterIndex As Long
     
     'Leave when map edit is disabled
     If (picMap.Enabled = False) Then Exit Sub
     
     'Ensure the splash dialog is gone
     'It would be odd if it was still displayed though
     Unload frmSplash: Set frmSplash = Nothing
     
     'Cancel if in drawing operation
     CancelCurrentOperation
     
     'Show save dialog
     result = SaveFile(Me.hWnd, "Save Map Into", "Doom/Heretic/Hexen WAD Files   *.wad|*.wad|All Files|*.*", mapfilename, cdlOFNExplorer Or cdlOFNHideReadOnly Or cdlOFNLongNames Or cdlOFNFileMustExist, FilterIndex)
     frmMain.Refresh
     
     'Check if not cancelled
     If result <> "" Then
          
          'Add extension
          If (LCase$(right$(result, 4)) <> ".wad") And (FilterIndex = 1) Then result = result & ".wad"
          
          'Save the map here
          If MapSave(result, SM_SAVEINTO) Then
               
               'Change the map filename
               mapfile = result
               mapfilename = Dir(mapfile)
               frmMain.Caption = App.Title & " - " & mapfilename & " (" & maplumpname & ")"
               mapchanged = False
               mapsaved = True
               EnableMapEditing    ' Enables map switch now that we've saved.
          End If
     End If
End Sub

Private Sub itmFileSwitchMap_Click()

     'Load the select map dialog
     Load frmMapSelect
     
     'Set the tag and caption
     frmMapSelect.Tag = mapfile
     frmMapSelect.Caption = "Select Map from " & Dir(mapfile)
     
     'Show the select dialog
     frmMapSelect.Show 1, Me

End Sub

Public Sub itmFileTest_Click(ByVal ForceShowOptions As Boolean)
     Dim Parameters As String
     Dim TempMapFile As String
     Dim ExePath As String
     Dim OldWindowState As Long
     Dim OldScriptWindowState As Long
     Dim OldSelectColor As Long
     Dim TestingDict As Dictionary
     Dim Skins(0 To 2) As String, Gametypes(0 To 8) As String
     Dim gt As Long
     
     'Leave when map edit is disabled
     If (picMap.Enabled = False) Then Exit Sub
     
     'Cancel if in drawing operation
     CancelCurrentOperation
     
     ' Get testing options.
     Set TestingDict = Config("testing")
     
     'Check if we should show testing options first
     If (Config("testdialog") = vbChecked) Or (ForceShowOptions = True) Then ShowConfiguration 6
     
     'Check if not cancelled
     If (Not OptionsCancelled) Or (Config("testdialog") = vbUnchecked) Then
          
          'Check if the executable can be found
          If ((TestingDict("exec") <> "") And (Dir(TestingDict("exec")) <> "")) Then
               
               ' Fill arrays of useful strings.
               Skins(SKN_SONIC) = "Sonic"
               Skins(SKN_TAILS) = "Tails"
               Skins(SKN_KNUCKLES) = "Knuckles"
               
               Gametypes(GT_COOP) = "Coop"
               Gametypes(GT_CTF) = "CTF"
               Gametypes(GT_DEFAULT) = "[Default]"
               Gametypes(GT_FULLRACE) = "Race"
               Gametypes(GT_MATCH) = "Match"
               Gametypes(GT_SINGLEPLAYER) = "[Single-player]"
               Gametypes(GT_TAG) = "Tag"
               Gametypes(GT_TEAMMATCH) = "Team Match"
               Gametypes(GT_TIMERACE) = "Time-Only Race"
               
               
               'Get executable path
               ExePath = PathOf(TestingDict("exec"))
               
               'Make temp filename
               TempMapFile = ExePath & "tempmap.wad"
               
               'Remove if file already exists
               If (Dir(TempMapFile) <> "") Then Kill TempMapFile
               
               'Save the map to temp file
               MapSave TempMapFile, SM_TEST
               
               ' Build parameters.
               Parameters = "-iwad %D -file"
               If TestingDict("addresources") = vbChecked Then Parameters = Parameters & " ""%A"""
               Parameters = Parameters & " ""%F"" -warp %M"
               
               ' Renderer
               If TestingDict("rend") = 1 Then Parameters = Parameters & " -opengl"
               
               
               ' Resolution
               
               If TestingDict("vidwidth") > -1 Then
                    If TestingDict("vidwidth") > 0 Then
                         Parameters = Parameters & " -width " & TestingDict("vidwidth")
                    End If
               Else
                    Parameters = Parameters & " -width " & Screen.width \ Screen.TwipsPerPixelX
               End If
               
               If TestingDict("vidheight") > -1 Then
                    If TestingDict("vidheight") > 0 Then
                         Parameters = Parameters & " -height " & TestingDict("vidheight")
                    End If
               Else
                    Parameters = Parameters & " -height " & Screen.Height \ Screen.TwipsPerPixelY
               End If
               
               
               ' Windowed?
               If TestingDict("windowed") = vbChecked Then
                    Parameters = Parameters & " -win"
               End If
               
               ' Audio
               If Not (TestingDict("music") = vbChecked) Then
                    ' No music.
                    Parameters = Parameters & " -nomusic"
               ElseIf TestingDict("digmusic") = 0 Then
                    ' MIDI.
                    Parameters = Parameters & " -nodigmusic"
               End If
               
               If Not (TestingDict("sound") = vbChecked) Then Parameters = Parameters & " -nosound"
               
               
               ' Skin
               Parameters = Parameters & " +skin " & Skins(TestingDict("skin"))
               
               
               ' Gametype
               
               If TestingDict("gametype") = GT_DEFAULT Then
                    If (Val(Config("storeeditinginfo"))) Then
                         gt = WadSettings("defgt")
                    Else
                         gt = GT_SINGLEPLAYER
                    End If
               Else
                    gt = TestingDict("gametype")
               End If
               
               If gt <> GT_SINGLEPLAYER Then
                    Parameters = Parameters & " -server -gametype """ & Gametypes(gt) & """"
               End If
               
               ' Skill
               Parameters = Parameters & " -skill " & Trim$(Str$(TestingDict("skill")))
               
               ' Anything else, sir?
               Parameters = Parameters & " " & TestingDict("addparams")
               
               
               'Replace placeholders in parameters
               Parameters = Replace$(Parameters, "%F", GetShortFileName(TempMapFile), , , vbTextCompare)
               Parameters = Replace$(Parameters, "%W", GetShortFileName(GetCurrentIWADFile), , , vbTextCompare)
               Parameters = Replace$(Parameters, "%D", Dir(GetCurrentIWADFile), , , vbTextCompare)
               Parameters = Replace$(Parameters, "%L", maplumpname, , , vbTextCompare)
               Parameters = Replace$(Parameters, "%A", GetShortFileName(addwadfile), , , vbTextCompare)
               Parameters = Replace$(Parameters, "%E", GetEpisodeNum(), , , vbTextCompare)
               Parameters = Replace$(Parameters, "%M", GetMapNum(), , , vbTextCompare)
               
               'Save selection color
               'This solves a bug with many software renderers on some systems
               OldSelectColor = GetSysColor(WCOLOR_HIGHLIGHT)
               
               'Minimize and hide
               OldWindowState = WindowState
               WindowState = vbMinimized
               
               'Same with Script editor if shown
               If (ScriptEditor) Then
                    OldScriptWindowState = frmScript.WindowState
                    frmScript.WindowState = vbMinimized
               End If
               
               'Disable editing
               picMap.Enabled = False
               
               'Focus to main window. This is a workaround from some
               'driver issues with microsoft mouse scrollwheel
               AppActivate frmMain.Caption
               frmMain.SetFocus
               
               'Launch
               If (Execute(TestingDict("exec"), Parameters, SW_SHOW, True) = False) Then MsgBox "Warning: Could not run the engine executable! Please try again later.", vbExclamation
               
               'Restore selection color
               SetSysColors 1, WCOLOR_HIGHLIGHT, OldSelectColor
               
               'Enable editing
               picMap.Enabled = True
               
               'Restore Script editor if shown
               If (ScriptEditor) Then frmScript.WindowState = OldScriptWindowState
               
               'Restore
               WindowState = OldWindowState
               Unload frmStatus
               
               'Remove temp map
               If (Dir(TempMapFile) <> "") Then Kill TempMapFile
          Else
               
               'Cant find engine
               MsgBox "Warning: Could not find the engine executable." & vbLf & "Please check your configuration!", vbExclamation
          End If
     End If
End Sub

Private Sub itmHelpAbout_Click()
     
     'Leave when map edit is disabled
     If (picMap.Enabled = False) Then Exit Sub
     
     'Ensure the splash dialog is gone
     Unload frmSplash: Set frmSplash = Nothing
     
     'Load splash
     Load frmSplash
     
     'Set version
     frmSplash.lblVersion = "Doom Builder version " & App.Major & "." & Format$(App.Minor, "00")
     frmSplash.lblStatus = "SRB2 Edition build " & App.Revision - 375 & "; insignificant modifications by Oogaland"
     
     'Change labels
     With frmSplash
          .lblStatus.visible = True
          .lblVersion.visible = True
          .lblWebsite.visible = True
          .lblAbout1.visible = True
     End With
     
     'Show about dialog
     frmSplash.Show 0, Me
End Sub

Private Sub itmHelpFAQ_Click()
     
     'Change mousepointer
     Screen.MousePointer = vbHourglass
     
     'Go to website
     Execute "http://www.doombuilder.com/builder_faq.php", "", SW_SHOW, False
     
     'Change mousepointer
     Screen.MousePointer = vbNormal
End Sub

Private Sub itmHelpWebsite_Click()
     
     'Change mousepointer
     Screen.MousePointer = vbHourglass
     
     'Go to website
     Execute "http://www.doombuilder.com", "", SW_SHOW, False
     
     'Change mousepointer
     Screen.MousePointer = vbNormal
End Sub

Private Sub itmLinesAlign_Click()
     Dim Deselect As Boolean
     Dim Indices As Variant
     Dim i As Long
     
     'Switch back if in move mode
     If (mode = EM_MOVE) Then itmEditMode_Click CInt(PreviousMode)
     
     'Cancel if in drawing operation
     CancelCurrentOperation
     
     'Check if no selection is made
     If (numselected = 0) Then
          
          'If no highlight exists, leave
          If (currentselected = -1) Then Exit Sub
          
          'Make selection from highlight
          SelectCurrentLine
          
          'Remove selection after edit
          Deselect = True
     End If
     
     'Show autoalign form
     frmAutoalign.Show 1, Me
     
     'Check result
     If (frmAutoalign.Tag = "OK") Then
          
          'Make undo
          CreateUndo "autoalign textures", UGRP_TEXTUREALIGNMENT, -1, True
          
          'Get listing of selection
          Indices = selected.Items
          
          'Go for all linedefs
          For i = LBound(Indices) To UBound(Indices)
               
               'Align in X offsets?
               If (frmAutoalign.chkX.Value = vbChecked) Then
                    
                    'Remove linedef selections
                    ResetSelections things(0), 0, linedefs(0), numlinedefs, vertexes(0), 0, VarPtr(sectors(0)), 0
                    
                    'Check if it has a front side
                    If (linedefs(Indices(i)).s1 > -1) And (frmAutoalign.chkFront = vbChecked) Then
                         
                         'Autoalign this side
                         AlignTexturesX linedefs(Indices(i)).V1, sidedefs(linedefs(Indices(i)).s1).tx, frmAutoalign.lstTextures.List(frmAutoalign.lstTextures.ListIndex), False, Indices(i)
                    End If
                    
                    'Remove linedef selections
                    ResetSelections things(0), 0, linedefs(0), numlinedefs, vertexes(0), 0, VarPtr(sectors(0)), 0
                    
                    'Check if it has a back side
                    If (linedefs(Indices(i)).s2 > -1) And (frmAutoalign.chkBack = vbChecked) Then
                         
                         'Autoalign this side
                         AlignTexturesX linedefs(Indices(i)).V1, sidedefs(linedefs(Indices(i)).s2).tx, frmAutoalign.lstTextures.List(frmAutoalign.lstTextures.ListIndex), True, Indices(i)
                    End If
               End If
               
               'Align in Y offsets?
               If (frmAutoalign.chkY.Value = vbChecked) Then
                    
                    'Remove linedef selections
                    ResetSelections things(0), 0, linedefs(0), numlinedefs, vertexes(0), 0, VarPtr(sectors(0)), 0
                    
                    'Check if it has a front side
                    If (linedefs(Indices(i)).s1 > -1) And (frmAutoalign.chkFront = vbChecked) Then
                         
                         'Autoalign this side
                         AlignTexturesY linedefs(Indices(i)).V1, sidedefs(linedefs(Indices(i)).s1).ty, frmAutoalign.lstTextures.List(frmAutoalign.lstTextures.ListIndex), False, Indices(i)
                    End If
                    
                    'Remove linedef selections
                    ResetSelections things(0), 0, linedefs(0), numlinedefs, vertexes(0), 0, VarPtr(sectors(0)), 0
                    
                    'Check if it has a back side
                    If (linedefs(Indices(i)).s2 > -1) And (frmAutoalign.chkBack = vbChecked) Then
                         
                         'Autoalign this side
                         AlignTexturesY linedefs(Indices(i)).V1, sidedefs(linedefs(Indices(i)).s2).ty, frmAutoalign.lstTextures.List(frmAutoalign.lstTextures.ListIndex), True, Indices(i)
                    End If
               End If
          Next i
          
          'Remove linedef selections
          ResetSelections things(0), 0, linedefs(0), numlinedefs, vertexes(0), 0, VarPtr(sectors(0)), 0
          
          'Reselect lines
          ReselectLinedefs Indices(0)
     End If
     
     'Unload dialog
     Unload frmAutoalign
     
     'Check if we should deselect
     If Deselect Then
          
          'Remove selection
          RemoveSelection False
     End If
End Sub

Private Sub itmLinesCascadeTags_Click()
     Dim i As Long
     Dim iLinedefs As Variant
     Dim V1 As Single, V2 As Single
     
     'Switch back if in move mode
     If (mode = EM_MOVE) Then itmEditMode_Click CInt(PreviousMode)
     
     'Cancel if in drawing operation
     CancelCurrentOperation
     
     'Need at least 2 selected linedefs
     If (numselected >= 2) Then
          
          'Make undo
          CreateUndo "cascade tags"
          
          'Get linedef indices
          iLinedefs = selected.Items
          
          'Get first and last values
          V1 = linedefs(iLinedefs(LBound(iLinedefs))).Tag
          V2 = V1 + selected.Count - 1
          
          'Go for all selected sectors
          For i = LBound(iLinedefs) To UBound(iLinedefs)
               
               'Apply the new value
               linedefs(iLinedefs(i)).Tag = V1 + i - LBound(iLinedefs)
          Next i
     End If
     
     UpdateFOFTagCache
     If IsLoaded(frmTags) Then frmTags.RefreshTagList
     
     'Redraw map
     RedrawMap False
     
     'Show highlight
     ShowHighlight LastX, LastY
     
     'Map changed
     mapchanged = True
     mapnodeschanged = True

End Sub

Private Sub itmLinesCopy_Click()
     Dim CopyIndex As Long
          
     'Switch back if in move mode
     If (mode = EM_MOVE) Then itmEditMode_Click CInt(PreviousMode)
     
     'Check if not in normal mode
     If (submode <> ESM_NONE) Then
          
          'Show message, cant right now
          MsgBox "Cannot perform Copy Properties while in an operation." & vbLf & "Please finish your operation first!", vbExclamation
          
          'Leave here
          Exit Sub
     End If
     
     'Check if a selection is made
     If (numselected > 0) Then
          
          'Copy first selected
          CopyIndex = selected.Items(0)
          
     ElseIf (currentselected > -1) Then
          
          'Copy highlighted
          CopyIndex = currentselected
          
     Else
          
          'Nothing selected or highlighted
          Exit Sub
     End If
     
     'Copy the line properties
     CopiedLinedef = linedefs(CopyIndex)
     
     'Copy sidedefs if any
     If (linedefs(CopyIndex).s1 > -1) Then CopiedSidedef1 = sidedefs(linedefs(CopyIndex).s1)
     If (linedefs(CopyIndex).s2 > -1) Then CopiedSidedef2 = sidedefs(linedefs(CopyIndex).s2)
End Sub

Private Sub itmLinesCurve_Click()
     Dim Deselect As Boolean
     
     'Switch back if in move mode
     If (mode = EM_MOVE) Then itmEditMode_Click CInt(PreviousMode)
     
     'Cancel if in drawing operation
     CancelCurrentOperation
     
     'Check if no selection is made
     If (numselected = 0) Then
          
          'If no highlight exists, leave
          If (currentselected = -1) Then Exit Sub
          
          'Make selection from highlight
          SelectCurrentLine
          
          'Remove selection after edit
          Deselect = True
     End If
     
     'Make undo
     CreateUndo "curve"
     
     'Rotate
     Load frmCurve
     frmCurve.Show 1, Me
     
     'Check if we should deselect
     If Deselect Then
          
          'Remove selection
          RemoveSelection False
     End If
     
     'Redraw
     RedrawMap
End Sub

Private Sub itmLinesFlipLinedefs_Click()
     Dim Indices As Variant
     Dim i As Long
     
     'Switch back if in move mode
     If (mode = EM_MOVE) Then itmEditMode_Click CInt(PreviousMode)
     
     'Cancel if in drawing operation
     CancelCurrentOperation
     
     'Check if a selection is made
     If (numselected > 0) Then
          
          'Make undo
          CreateUndo "flip linedefs"
          
          'Go for all selected linedefs
          Indices = selected.Items
          For i = LBound(Indices) To UBound(Indices)
               
               'Flip vertices
               FlipLinedefVertices Indices(i)
               
               'Flip sidedefs (because they flipped with the linedef's vertices)
               FlipLinedefSidedefs Indices(i)
          Next i
          
     'Otherwise, check if a highlight is made
     ElseIf (currentselected > -1) Then
          
          'Make undo
          CreateUndo "flip linedefs"
          
          'Flip vertices
          FlipLinedefVertices currentselected
          
          'Flip sidedefs (because they flipped with the linedef's vertices)
          FlipLinedefSidedefs currentselected
     End If
     
     'Remove highlight
     RemoveHighlight True
     
     'Redraw map
     RedrawMap False
     
     'Map changed
     mapchanged = True
     mapnodeschanged = True
End Sub

Private Sub itmLinesFlipSidedefs_Click()
     Dim Indices As Variant
     Dim i As Long
     
     'Switch back if in move mode
     If (mode = EM_MOVE) Then itmEditMode_Click CInt(PreviousMode)
     
     'Cancel if in drawing operation
     CancelCurrentOperation
     
     'Check if a selection is made
     If (numselected > 0) Then
          
          'Make undo
          CreateUndo "flip sidedefs"
          
          'Go for all selected linedefs
          Indices = selected.Items
          For i = LBound(Indices) To UBound(Indices)
               
               'Flip sidedefs
               FlipLinedefSidedefs Indices(i)
          Next i
          
     'Otherwise, check if a highlight is made
     ElseIf (currentselected > -1) Then
          
          'Make undo
          CreateUndo "flip sidedefs"
          
          'Flip sidedefs
          FlipLinedefSidedefs currentselected
     End If
     
     'Remove highlight
     RemoveHighlight True
     
     'Map changed
     mapchanged = True
     mapnodeschanged = True
End Sub

Private Sub itmLinesPaste_Click()
     Dim Indices As Variant
     Dim i As Long
     
     'Switch back if in move mode
     If (mode = EM_MOVE) Then itmEditMode_Click CInt(PreviousMode)
     
     'Check if not in normal mode
     If (submode <> ESM_NONE) Then
          
          'Show message, cant right now
          MsgBox "Cannot perform Paste Properties while in an operation." & vbLf & "Please finish your operation first!", vbExclamation
          
          'Leave here
          Exit Sub
     End If
     
     'Make undo
     CreateUndo "paste linedef properties", , , True
     
     'Check if a selection is made
     If (numselected > 0) Then
          
          'Go for all selected linedefs
          Indices = selected.Items
          For i = LBound(Indices) To UBound(Indices)
               
               'Paste properties
               With linedefs(Indices(i))
                    .arg0 = CopiedLinedef.arg0
                    .arg1 = CopiedLinedef.arg1
                    .arg2 = CopiedLinedef.arg2
                    .arg3 = CopiedLinedef.arg3
                    .arg4 = CopiedLinedef.arg4
                    .effect = CopiedLinedef.effect
                    .Flags = (((CopiedLinedef.Flags And Not LDF_IMPASSIBLE) Or (.Flags And LDF_IMPASSIBLE)) And Not LDF_TWOSIDED) Or (.Flags And LDF_TWOSIDED)
                    .Tag = CopiedLinedef.Tag
               End With
               
               'Paste sidedef1 properties
               If (linedefs(Indices(i)).s1 > -1) And (CopiedLinedef.s1 > -1) Then
                    
                    With sidedefs(linedefs(Indices(i)).s1)
                         .Lower = CopiedSidedef1.Lower
                         .Middle = CopiedSidedef1.Middle
                         .tx = CopiedSidedef1.tx
                         .ty = CopiedSidedef1.ty
                         .Upper = CopiedSidedef1.Upper
                    End With
               End If
               
               'Paste sidedef2 properties
               If (linedefs(Indices(i)).s2 > -1) And (CopiedLinedef.s2 > -1) Then
                    
                    With sidedefs(linedefs(Indices(i)).s2)
                         .Lower = CopiedSidedef2.Lower
                         .Middle = CopiedSidedef2.Middle
                         .tx = CopiedSidedef2.tx
                         .ty = CopiedSidedef2.ty
                         .Upper = CopiedSidedef2.Upper
                    End With
               End If
          Next i
          
     'Otherwise, check if a highlight is made
     ElseIf (currentselected > -1) Then
          
          'Paste properties
          With linedefs(currentselected)
               .arg0 = CopiedLinedef.arg0
               .arg1 = CopiedLinedef.arg1
               .arg2 = CopiedLinedef.arg2
               .arg3 = CopiedLinedef.arg3
               .arg4 = CopiedLinedef.arg4
               .effect = CopiedLinedef.effect
               .Flags = CopiedLinedef.Flags
               .Tag = CopiedLinedef.Tag
          End With
          
          'Paste sidedef1 properties
          If (linedefs(currentselected).s1 > -1) And (CopiedLinedef.s1 > -1) Then
               
               With sidedefs(linedefs(currentselected).s1)
                    .Lower = CopiedSidedef1.Lower
                    .Middle = CopiedSidedef1.Middle
                    .tx = CopiedSidedef1.tx
                    .ty = CopiedSidedef1.ty
                    .Upper = CopiedSidedef1.Upper
               End With
          End If
          
          'Paste sidedef2 properties
          If (linedefs(currentselected).s2 > -1) And (CopiedLinedef.s2 > -1) Then
               
               With sidedefs(linedefs(currentselected).s2)
                    .Lower = CopiedSidedef2.Lower
                    .Middle = CopiedSidedef2.Middle
                    .tx = CopiedSidedef2.tx
                    .ty = CopiedSidedef2.ty
                    .Upper = CopiedSidedef2.Upper
               End With
          End If
     End If
     
     'Remove highlight
     RemoveHighlight True
     
     'Redraw map
     RedrawMap False
     
     'Map changed
     mapchanged = True
     mapnodeschanged = True
End Sub

Private Sub itmLinesSelect_Click(Index As Integer)
     Dim Indices As Variant
     Dim i As Long
     
     'Switch back if in move mode
     If (mode = EM_MOVE) Then itmEditMode_Click CInt(PreviousMode)
     
     'Cancel if in drawing operation
     CancelCurrentOperation
     
     'Check if a selection is made
     If (numselected > 0) Then
          
          'Go for all selected linedefs
          Indices = selected.Items
          For i = LBound(Indices) To UBound(Indices)
               
               'Check what to do
               Select Case Index
                    
                    'Select only 1 sided linedefs
                    Case 0:
                         
                         'Check if not 1-sided
                         If (linedefs(Indices(i)).s2 <> -1) And _
                            (linedefs(Indices(i)).s1 <> -1) Then
                              
                              'Remove selection
                              linedefs(Indices(i)).selected = 0
                              
                              'Remove index from selected objects
                              selected.Remove CStr(Indices(i))
                         End If
                         
                    'Select only 2 sided linedefs
                    Case 1:
                         
                         'Check if not 2-sided
                         If (linedefs(Indices(i)).s2 = -1) Or _
                            (linedefs(Indices(i)).s1 = -1) Then
                              
                              'Remove selection
                              linedefs(Indices(i)).selected = 0
                              
                              'Remove index from selected objects
                              selected.Remove CStr(Indices(i))
                         End If
               End Select
          Next i
          
          'Update number of selected items
          numselected = selected.Count
          
          'Redraw map
          RedrawMap
     End If
End Sub

Private Sub itmLinesSnapToGrid_Click()
     Dim Indices As Variant
     Dim i As Long
     
     'Switch back if in move mode
     If (mode = EM_MOVE) Then itmEditMode_Click CInt(PreviousMode)
     
     'Cancel if in drawing operation
     CancelCurrentOperation
     
     'Check if no selection is made, but a higlight
     If (numselected = 0) And (currentselected > -1) Then
          
          'Undo thise selecting after edit
          DeselectAfterEdit = True
          
          'Select current line
          SelectCurrentLine
     Else
          
          'Dont deselect after edit
          DeselectAfterEdit = False
     End If
     
     'Check if we have a selection
     If (numselected > 0) Then
          
          'Make Undo
          CreateUndo "snap to grid"
          
          'Go for all selected vertices
          Indices = SelectVerticesFromSelection.Items
          For i = LBound(Indices) To UBound(Indices)
               
               'Snap this vertex to grid now
               vertexes(Indices(i)).x = SnappedToGridX(vertexes(Indices(i)).x)
               vertexes(Indices(i)).y = SnappedToGridY(vertexes(Indices(i)).y)
          Next i
          
          'Map changed
          mapchanged = True
          mapnodeschanged = True
          
          'Deselect if we should
          If DeselectAfterEdit Then RemoveSelection False
          
          'Redraw map
          RedrawMap
     End If
End Sub

Private Sub itmLinesSplit_Click()
     Dim Deselect As Boolean
     
     'Switch back if in move mode
     If (mode = EM_MOVE) Then itmEditMode_Click CInt(PreviousMode)
     
     'Cancel if in drawing operation
     CancelCurrentOperation
     
     'Check if no selection is made
     If (numselected = 0) Then
          
          'If no highlight exists, leave
          If (currentselected = -1) Then Exit Sub
          
          'Make selection from highlight
          SelectCurrentLine
          
          'Remove selection after edit
          Deselect = True
     End If
     
     'Make undo
     CreateUndo "split"
     
     'Split, by curving with one vertex and zero angle and distance
     CurveLines 1, 0, 90, False
     
     ReselectLinedefs
     
     'Check if we should deselect
     If Deselect Then
          
          'Remove selection
          RemoveSelection False
     End If
     
     'Redraw
     RedrawMap
End Sub

Private Sub itmPrefabInsert_Click()
     Dim PasteMode As ENUM_PREFABINCLUDEMODE
     Dim PasteX As Long, PasteY As Long
     Dim PasteFile As String
     
     'Leave when map edit is disabled
     If (picMap.Enabled = False) Then Exit Sub
     
     'Remove highlight
     If (mapfile <> "") Then RemoveHighlight True
     
     'Switch back if in move mode
     If (mode = EM_MOVE) Then itmEditMode_Click CInt(PreviousMode)
     
     'Check if not in normal mode
     If (submode <> ESM_NONE) Then
          
          'Show message, cant right now
          MsgBox "Cannot perform Prefab Insert while in an operation." & vbLf & "Please finish your operation first!", vbExclamation
          
          'Leave here
          Exit Sub
     End If
     
     'Browse for file
     picMap.Enabled = False
     If (Trim$(Config("prefabfolder")) <> "") Then
          PasteFile = OpenFile(Me.hWnd, "Insert Prefab file", "Doom Builder Prefab Files   *.dbp|*.dbp", Config("prefabfolder") & Dir(Config("prefabfolder") & "*"), cdlOFNExplorer Or cdlOFNFileMustExist Or cdlOFNHideReadOnly Or cdlOFNLongNames)
     Else
          PasteFile = OpenFile(Me.hWnd, "Insert Prefab file", "Doom Builder Prefab Files   *.dbp|*.dbp", "", cdlOFNExplorer Or cdlOFNFileMustExist Or cdlOFNHideReadOnly Or cdlOFNLongNames)
     End If
     DoEvents
     picMap.Enabled = True
     
     'Check if not cancelled
     If (Trim$(PasteFile) <> "") Then
          
          'Make undo
          CreateUndo "prefab insert"
          
          'Check if we can paste at mouse location
          If MouseInside Then
               
               'Mouse position
               PasteX = LastX
               PasteY = LastY
          Else
               
               'Center of screen
               PasteX = picMap.ScaleLeft + picMap.ScaleWidth / 2
               PasteY = picMap.ScaleTop + picMap.ScaleHeight / 2
          End If
          
          'Determine paste mode by mode
          Select Case mode
               Case EM_VERTICES: PasteMode = PIM_VERTICES   'Paste vertices only
               Case EM_LINES: PasteMode = PIM_STRUCTURE
               Case EM_SECTORS: PasteMode = PIM_STRUCTURE
               Case EM_THINGS: PasteMode = PIM_THINGS
          End Select
          
          'Paste and check if anything was pasted
          If (InsertPrefab(PasteFile, PasteX, PasteY, PasteMode) > 0) Then
               
               'If in lines mode
               If (mode = EM_LINES) Then
                    
                    'Select lines from vertices
                    SelectLinedefsFromVertices
                    
               'If in sectors mode
               ElseIf (mode = EM_SECTORS) Then
                    
                    'Select sectors from vertices
                    SelectLinedefsFromVertices
                    SelectSectorsFromLinedefs
               End If
               
               'Grab it
               GrabX = PasteX
               GrabY = PasteY
               
               'Start pasting
               StartPasteOperation PasteX, PasteY
               
               'Save last pasted
               LastPrefab = PasteFile
          Else
               
               'Nothing to paste
               MsgBox "This prefab does not have anything that can be inserted in the current mode.", vbInformation
          End If
          
          'Redraw map
          RedrawMap
     End If
End Sub

Private Sub itmPrefabPrevious_Click()
     Dim PasteMode As ENUM_PREFABINCLUDEMODE
     Dim PasteX As Long, PasteY As Long
     
     'Leave when map edit is disabled
     If (picMap.Enabled = False) Then Exit Sub
     
     'Remove highlight
     If (mapfile <> "") Then RemoveHighlight True
     
     'Switch back if in move mode
     If (mode = EM_MOVE) Then itmEditMode_Click CInt(PreviousMode)
     
     'Check if not in normal mode
     If (submode <> ESM_NONE) Then
          
          'Show message, cant right now
          MsgBox "Cannot perform Prefab Insert while in an operation." & vbLf & "Please finish your operation first!", vbExclamation
          
          'Leave here
          Exit Sub
     End If
     
     'Make undo
     CreateUndo "prefab insert"
     
     'Check if we can paste at mouse location
     If MouseInside Then
          
          'Mouse position
          PasteX = LastX
          PasteY = LastY
     Else
          
          'Center of screen
          PasteX = picMap.ScaleLeft + picMap.ScaleWidth / 2
          PasteY = picMap.ScaleTop + picMap.ScaleHeight / 2
     End If
     
     'Determine paste mode by mode
     Select Case mode
          Case EM_VERTICES: PasteMode = PIM_VERTICES   'Paste vertices only
          Case EM_LINES: PasteMode = PIM_STRUCTURE
          Case EM_SECTORS: PasteMode = PIM_STRUCTURE
          Case EM_THINGS: PasteMode = PIM_THINGS
     End Select
     
     'Paste and check if anything was pasted
     If (InsertPrefab(LastPrefab, PasteX, PasteY, PasteMode) > 0) Then
          
          'If in lines mode
          If (mode = EM_LINES) Then
               
               'Select lines from vertices
               SelectLinedefsFromVertices
               
          'If in sectors mode
          ElseIf (mode = EM_SECTORS) Then
               
               'Select sectors from vertices
               SelectLinedefsFromVertices
               SelectSectorsFromLinedefs
          End If
          
          'Grab it
          GrabX = PasteX
          GrabY = PasteY
          
          'Start pasting
          StartPasteOperation PasteX, PasteY
     Else
          
          'Nothing to paste
          MsgBox "This prefab does not have anything that can be inserted in the current mode.", vbInformation
     End If
     
     'Redraw map
     RedrawMap
End Sub

Private Sub itmPrefabQuick_Click(Index As Integer)
     Dim PasteMode As ENUM_PREFABINCLUDEMODE
     Dim PasteX As Long, PasteY As Long
     
     'Leave when map edit is disabled
     If (picMap.Enabled = False) Then Exit Sub
     
     'Remove highlight
     If (mapfile <> "") Then RemoveHighlight True
     
     'Switch back if in move mode
     If (mode = EM_MOVE) Then itmEditMode_Click CInt(PreviousMode)
     
     'Check if not in normal mode
     If (submode <> ESM_NONE) Then
          
          'Show message, cant right now
          MsgBox "Cannot perform Prefab Insert while in an operation." & vbLf & "Please finish your operation first!", vbExclamation
          
          'Leave here
          Exit Sub
     End If
     
     'Make undo
     CreateUndo "prefab insert"
     
     'Check if we can paste at mouse location
     If MouseInside Then
          
          'Mouse position
          PasteX = LastX
          PasteY = LastY
     Else
          
          'Center of screen
          PasteX = picMap.ScaleLeft + picMap.ScaleWidth / 2
          PasteY = picMap.ScaleTop + picMap.ScaleHeight / 2
     End If
     
     'Determine paste mode by mode
     Select Case mode
          Case EM_VERTICES: PasteMode = PIM_VERTICES   'Paste vertices only
          Case EM_LINES: PasteMode = PIM_STRUCTURE
          Case EM_SECTORS: PasteMode = PIM_STRUCTURE
          Case EM_THINGS: PasteMode = PIM_THINGS
     End Select
     
     'Paste and check if anything was pasted
     If (InsertPrefab(Config("quickprefab" & Index + 1), PasteX, PasteY, PasteMode) > 0) Then
          
          'If in lines mode
          If (mode = EM_LINES) Then
               
               'Select lines from vertices
               SelectLinedefsFromVertices
               
          'If in sectors mode
          ElseIf (mode = EM_SECTORS) Then
               
               'Select sectors from vertices
               SelectLinedefsFromVertices
               SelectSectorsFromLinedefs
          End If
          
          'Grab it
          GrabX = PasteX
          GrabY = PasteY
          
          'Start pasting
          StartPasteOperation PasteX, PasteY
          
          'Save last pasted
          LastPrefab = Config("quickprefab" & Index + 1)
     Else
          
          'Nothing to paste
          MsgBox "This prefab does not have anything that can be inserted in the current mode.", vbInformation
     End If
     
     'Redraw map
     RedrawMap
End Sub

Private Sub itmPrefabSaveSel_Click()
     Dim result As String
     Dim FilterIndex As Long
     
     'Switch back if in move mode
     If (mode = EM_MOVE) Then itmEditMode_Click CInt(PreviousMode)
     
     'Browse for new file
     If (Trim$(Config("prefabfolder")) <> "") Then
          result = SaveFile(Me.hWnd, "Save Prefab file", "Doom Builder Prefab Files   *.dbp|*.dbp|All Files|*.*", Config("prefabfolder") & Dir(Config("prefabfolder") & "*"), cdlOFNExplorer Or cdlOFNHideReadOnly Or cdlOFNLongNames Or cdlOFNPathMustExist Or cdlOFNOverwritePrompt, FilterIndex)
     Else
          result = SaveFile(Me.hWnd, "Save Prefab file", "Doom Builder Prefab Files   *.dbp|*.dbp|All Files|*.*", "", cdlOFNExplorer Or cdlOFNHideReadOnly Or cdlOFNLongNames Or cdlOFNPathMustExist Or cdlOFNOverwritePrompt, FilterIndex)
     End If
     
     'Check if not cancelled
     If result <> "" Then
          
          'Add extension
          If (LCase$(right$(result, 4)) <> ".dbp") And (FilterIndex = 1) Then result = result & ".dbp"
          
          'Remove the file if already exists
          If (Dir(result) <> "") Then Kill result
          
          'Save the selection as prefab
          SavePrefabSelection result
     End If
End Sub

Private Sub itmScriptEdit_Click(Index As Integer)
     Dim LumpName As String
     
     'Unload the script editor when its already loaded
     If (ScriptEditor) Then Unload frmScript
     
     'Get lump display name
     If (Trim$(itmScriptEdit(Index).Tag) = "~") Then LumpName = maplumpname Else LumpName = itmScriptEdit(Index).Tag
     
     'Leave when map edit is disabled
     If (picMap.Enabled = False) Then Exit Sub
     
     'Ensure the splash dialog is gone
     Unload frmSplash: Set frmSplash = Nothing
     
     'Cancel if in drawing operation
     CancelCurrentOperation
     
     'Load the Script dialog
     Load frmScript
     
     'Set the lump name
     frmScript.lblLumpname.Caption = itmScriptEdit(Index).Tag
     frmScript.Caption = "Doom Builder Map Script - " & LumpName
     
     'Load script
     frmScript.LoadLumpScript
     
     'Show the dialog
     frmScript.Show 0, Me
End Sub

Private Sub itmScriptsMerge_Click()

     MergeMAPnnDIntoMAINCFG

End Sub

Private Sub itmSectorsCascadeTags_Click()

     Dim i As Long
     Dim iSectors As Variant
     Dim V1 As Single, V2 As Single
     
     'Switch back if in move mode
     If (mode = EM_MOVE) Then itmEditMode_Click CInt(PreviousMode)
     
     'Cancel if in drawing operation
     CancelCurrentOperation
     
     'Need at least 2 selected sectors
     If (numselected >= 2) Then
          
          'Make undo
          CreateUndo "cascade tags"
          
          'Get sector indices
          iSectors = selected.Items
          
          'Get first and last values
          V1 = sectors(iSectors(LBound(iSectors))).Tag
          V2 = V1 + selected.Count - 1
          
          'Go for all selected sectors
          For i = LBound(iSectors) To UBound(iSectors)
               
               'Apply the new value
               sectors(iSectors(i)).Tag = V1 + i - LBound(iSectors)
          Next i
     End If
     
     'Redraw map
     RedrawMap False
     
     'Show highlight
     ShowHighlight LastX, LastY
     
     'Map changed
     mapchanged = True
     mapnodeschanged = True

End Sub

Private Sub itmSectorsConvertToStairs_Click()

     picMap.Enabled = False
     Load frmStairs
     If frmStairs.Tag = 0 Then frmStairs.Show vbModal, Me Else Unload frmStairs
     picMap.Enabled = True

End Sub

Private Sub itmSectorsCopy_Click()
     
     'Switch back if in move mode
     If (mode = EM_MOVE) Then itmEditMode_Click CInt(PreviousMode)
     
     'Check if not in normal mode
     If (submode <> ESM_NONE) Then
          
          'Show message, cant right now
          MsgBox "Cannot perform Copy Properties while in an operation." & vbLf & "Please finish your operation first!", vbExclamation
          
          'Leave here
          Exit Sub
     End If
     
     'Check if a selection is made
     If (numselected > 0) Then
          
          'Copy the first selected sector
          CopiedSector = sectors(selected.Items(0))
          
     ElseIf (currentselected > -1) Then
          
          'Copy the highlighted sector
          CopiedSector = sectors(currentselected)
          
     Else
          
          'Nothing selected or highlighted
          Exit Sub
     End If
End Sub

Private Sub itmSectorsDecBrightness_Click()
     Dim Indices As Variant
     Dim i As Long
     
     'Switch back if in move mode
     If (mode = EM_MOVE) Then itmEditMode_Click CInt(PreviousMode)
     
     'Cancel if in drawing operation
     CancelCurrentOperation
     
     'Check if a selection is made
     If (numselected > 0) Then
          
          'Make undo
          CreateUndo "decrease brightness", , , True
          
          'Go for all selected sectors
          Indices = selected.Items
          For i = LBound(Indices) To UBound(Indices)
               
               'Decrease brightness
               sectors(Indices(i)).Brightness = sectors(Indices(i)).Brightness - 16
               If sectors(Indices(i)).Brightness < 0 Then sectors(Indices(i)).Brightness = 0
          Next i
          
     'Otherwise, check if a highlight is made
     ElseIf (currentselected > -1) Then
          
          'Make undo
          CreateUndo "decrease brightness", , , True
          
          'Decrease height
          sectors(currentselected).Brightness = sectors(currentselected).Brightness - 16
          If sectors(currentselected).Brightness < 0 Then sectors(currentselected).Brightness = 0
     End If
     
     'Reselect
     ChangeSectorsHighlight LastX, LastY, True
     
     'Map changed
     mapchanged = True
     mapnodeschanged = True
End Sub

Private Sub itmSectorsFindIdenticalSectorSet_Click()
     
     Dim s As Long, r As Long, RModded As Long, SModded As Long
     Dim OneColl As Dictionary
     Dim c As Dictionary
     Dim v As Variant
     Dim AutoMerge As Boolean
     Static LastS As Long
     
     CancelCurrentOperation
     
     RemoveSelection False
     
     AutoMerge = (CurrentShiftMask And vbShiftMask) <> 0 And _
                    (CurrentShiftMask And vbCtrlMask) <> 0
     
     If AutoMerge Then
     
          CreateUndo "identical sector merge"
          
          With frmStatus
               .Show 0, frmMain
               .SetFocus
               .Refresh
          End With
          
          DisplayStatus "Merging identical sectors, please wait..."
          
     End If
     
     Screen.MousePointer = vbHourglass

     
     For s = 0 To numsectors - 1
          
          Set OneColl = New Dictionary
          
          SModded = (s + LastS) Mod (numsectors)
          
          ' We use modular arithmetic to remember our position from last time.
          For r = s To numsectors - 1
               RModded = (r + LastS) Mod (numsectors)
               If SectorsAreIdentical(RModded, SModded) Then
                    If Not AutoMerge Or (sectors(RModded).special <> 976 And sectors(RModded).special <> 977) Then
                         OneColl.Add CStr(RModded), RModded
                    End If
               End If
          Next r
          
          If OneColl.Count > 1 Then
               
               If AutoMerge Then
                    Set selected = OneColl
          
                    'Remove shared lindefs
                    RemoveSelectedSharedLinedefs
                    
                    'Join selected sectors together
                    JoinSelectedSectors
               Else
                    For Each v In OneColl.Items
                         currentselected = v
                         SelectCurrentSector
                    Next v
                    
                    ' Remember our position.
                    LastS = SModded + 1
                    Exit For
               End If

          End If
     
     Next s
     
     If AutoMerge Then
          
          Unload frmStatus
          Set frmStatus = Nothing
          
          mapchanged = True
          mapnodeschanged = True
          UpdateStatusBar
          
          RemoveSelection True
          
     End If
     
     RedrawMap True
     
     Screen.MousePointer = vbNormal

End Sub

Private Sub itmSectorsFOF_Click()

     'Switch back if in move mode
     If (mode = EM_MOVE) Then itmEditMode_Click CInt(PreviousMode)
     
     'Cancel if in drawing operation
     CancelCurrentOperation
     
     'Need at least 1 selected sector
     If (numselected >= 1) Then
          
          ' Show form
          FOFDialogueBySelection
          
     End If
     
     'Redraw map
     RedrawMap False
     
     'Show highlight
     ShowHighlight LastX, LastY
     
     'Map changed
     mapchanged = True
     mapnodeschanged = True

End Sub

Private Sub itmSectorsGradientBrightness_Click()
     Dim i As Long, p As Single
     Dim iSectors As Variant
     Dim v As Single, V1 As Single, V2 As Single
     
     'Switch back if in move mode
     If (mode = EM_MOVE) Then itmEditMode_Click CInt(PreviousMode)
     
     'Cancel if in drawing operation
     CancelCurrentOperation
     
     'Need at least 3 selected sectors
     If (numselected >= 3) Then
          
          'Make undo
          CreateUndo "gradient brightness"
          
          'Get sector indices
          iSectors = selected.Items
          
          'Get first and last values
          V1 = sectors(iSectors(LBound(iSectors))).Brightness
          V2 = sectors(iSectors(UBound(iSectors))).Brightness
          
          'Go for all selected sectors
          For i = LBound(iSectors) To UBound(iSectors)
               
               'Calculate interpolation
               p = i / UBound(iSectors)
               
               'Calculate new value
               v = V1 * (1 - p) + V2 * p
               
               'Apply the new value
               sectors(iSectors(i)).Brightness = v
          Next i
     End If
     
     'Redraw map
     RedrawMap False
     
     'Show highlight
     ShowHighlight LastX, LastY
     
     'Map changed
     mapchanged = True
     mapnodeschanged = True
End Sub

Private Sub itmSectorsGradientCeilings_Click()
     Dim i As Long, p As Single
     Dim iSectors As Variant
     Dim v As Single, V1 As Single, V2 As Single
     
     'Switch back if in move mode
     If (mode = EM_MOVE) Then itmEditMode_Click CInt(PreviousMode)
     
     'Cancel if in drawing operation
     CancelCurrentOperation
     
     'Need at least 3 selected sectors
     If (numselected >= 3) Then
          
          'Make undo
          CreateUndo "gradient ceilings"
          
          'Get sector indices
          iSectors = selected.Items
          
          'Get first and last values
          V1 = sectors(iSectors(LBound(iSectors))).hceiling
          V2 = sectors(iSectors(UBound(iSectors))).hceiling
          
          'Go for all selected sectors
          For i = LBound(iSectors) To UBound(iSectors)
               
               'Calculate interpolation
               p = i / UBound(iSectors)
               
               'Calculate new value
               v = V1 * (1 - p) + V2 * p
               
               'Apply the new value
               sectors(iSectors(i)).hceiling = v
          Next i
     End If
     
     'Redraw map
     RedrawMap False
     
     'Show highlight
     ShowHighlight LastX, LastY
     
     'Map changed
     mapchanged = True
     mapnodeschanged = True
End Sub


Private Sub itmSectorsGradientFloors_Click()
     Dim i As Long, p As Single
     Dim iSectors As Variant
     Dim v As Single, V1 As Single, V2 As Single
     
     'Switch back if in move mode
     If (mode = EM_MOVE) Then itmEditMode_Click CInt(PreviousMode)
     
     'Cancel if in drawing operation
     CancelCurrentOperation
     
     'Need at least 3 selected sectors
     If (numselected >= 3) Then
          
          'Make undo
          CreateUndo "gradient floors"
          
          'Get sector indices
          iSectors = selected.Items
          
          'Get first and last values
          V1 = sectors(iSectors(LBound(iSectors))).HFloor
          V2 = sectors(iSectors(UBound(iSectors))).HFloor
          
          'Go for all selected sectors
          For i = LBound(iSectors) To UBound(iSectors)
               
               'Calculate interpolation
               p = i / UBound(iSectors)
               
               'Calculate new value
               v = V1 * (1 - p) + V2 * p
               
               'Apply the new value
               sectors(iSectors(i)).HFloor = v
          Next i
     End If
     
     'Redraw map
     RedrawMap False
     
     'Show highlight
     ShowHighlight LastX, LastY
     
     'Map changed
     mapchanged = True
     mapnodeschanged = True
End Sub


Private Sub itmSectorsIncBrightness_Click()
     Dim Indices As Variant
     Dim i As Long
     
     'Switch back if in move mode
     If (mode = EM_MOVE) Then itmEditMode_Click CInt(PreviousMode)
     
     'Cancel if in drawing operation
     CancelCurrentOperation
     
     'Check if a selection is made
     If (numselected > 0) Then
          
          'Make undo
          CreateUndo "increase brightness", , , True
          
          'Go for all selected sectors
          Indices = selected.Items
          For i = LBound(Indices) To UBound(Indices)
               
               'Increase brightness
               sectors(Indices(i)).Brightness = sectors(Indices(i)).Brightness + 16
               If sectors(Indices(i)).Brightness > 255 Then sectors(Indices(i)).Brightness = 255
          Next i
          
     'Otherwise, check if a highlight is made
     ElseIf (currentselected > -1) Then
          
          'Make undo
          CreateUndo "increase brightness", , , True
          
          'Increase height
          sectors(currentselected).Brightness = sectors(currentselected).Brightness + 16
          If sectors(currentselected).Brightness > 255 Then sectors(currentselected).Brightness = 255
     End If
     
     'Reselect
     ChangeSectorsHighlight LastX, LastY, True
     
     'Map changed
     mapchanged = True
     mapnodeschanged = True
End Sub

Private Sub itmSectorsJoin_Click()
     
     'Switch back if in move mode
     If (mode = EM_MOVE) Then itmEditMode_Click CInt(PreviousMode)
     
     'Cancel if in drawing operation
     CancelCurrentOperation
     
     'Need at least 2 selected sectors
     If (numselected >= 2) Then
          
          'Make undo
          CreateUndo "join sectors"
          
          'Join selected sector together
          JoinSelectedSectors
          
          'DEBUG
          'DEBUG_FindUnusedSectors
     End If
     
     'Remove highlight
     RemoveHighlight True
     
     'Reselect
     ReselectSectors
     
     'Redraw map
     RedrawMap False
     
     'Map changed
     mapchanged = True
     mapnodeschanged = True
End Sub

Private Sub itmSectorsLowerCeiling_Click()
     Dim Indices As Variant
     Dim i As Long
     
     'Switch back if in move mode
     If (mode = EM_MOVE) Then itmEditMode_Click CInt(PreviousMode)
     
     'Cancel if in drawing operation
     CancelCurrentOperation
     
     'Check if a selection is made
     If (numselected > 0) Then
          
          'Make undo
          CreateUndo "lower ceiling", , , True
          
          'Go for all selected sectors
          Indices = selected.Items
          For i = LBound(Indices) To UBound(Indices)
               
               'Decrease height
               sectors(Indices(i)).hceiling = sectors(Indices(i)).hceiling - 8
          Next i
          
     'Otherwise, check if a highlight is made
     ElseIf (currentselected > -1) Then
          
          'Make undo
          CreateUndo "lower ceiling", , , True
          
          'Decrease height
          sectors(currentselected).hceiling = sectors(currentselected).hceiling - 8
     End If
     
     'Reselect
     ChangeSectorsHighlight LastX, LastY, True
     
     'Map changed
     mapchanged = True
     mapnodeschanged = True
End Sub

Private Sub itmSectorsLowerFloor_Click()
     Dim Indices As Variant
     Dim i As Long
     
     'Switch back if in move mode
     If (mode = EM_MOVE) Then itmEditMode_Click CInt(PreviousMode)
     
     'Cancel if in drawing operation
     CancelCurrentOperation
     
     'Check if a selection is made
     If (numselected > 0) Then
          
          'Make undo
          CreateUndo "lower floor", , , True
          
          'Go for all selected sectors
          Indices = selected.Items
          For i = LBound(Indices) To UBound(Indices)
               
               'Decrease height
               sectors(Indices(i)).HFloor = sectors(Indices(i)).HFloor - 8
          Next i
          
     'Otherwise, check if a highlight is made
     ElseIf (currentselected > -1) Then
          
          'Make undo
          CreateUndo "lower floor", , , True
          
          'Decrease height
          sectors(currentselected).HFloor = sectors(currentselected).HFloor - 8
     End If
     
     'Reselect
     ChangeSectorsHighlight LastX, LastY, True
     
     'Map changed
     mapchanged = True
     mapnodeschanged = True
End Sub

Private Sub itmSectorsMerge_Click()
     
     'Switch back if in move mode
     If (mode = EM_MOVE) Then itmEditMode_Click CInt(PreviousMode)
     
     'Cancel if in drawing operation
     CancelCurrentOperation
     
     'Need at least 2 selected sectors
     If (numselected >= 2) Then
          
          'Make undo
          CreateUndo "merge sectors"
          
          'Remove shared lindefs
          RemoveSelectedSharedLinedefs
          
          'Join selected sector together
          JoinSelectedSectors
          
          'DEBUG
          'DEBUG_FindUnusedSectors
     End If
     
     'Remove highlight
     RemoveHighlight True
     
     'Reselect
     ReselectSectors
     
     'Redraw map
     RedrawMap False
     
     'Map changed
     mapchanged = True
     mapnodeschanged = True
End Sub

Private Sub itmSectorsPaste_Click()
     Dim Indices As Variant
     Dim i As Long
     
     'Switch back if in move mode
     If (mode = EM_MOVE) Then itmEditMode_Click CInt(PreviousMode)
     
     'Check if not in normal mode
     If (submode <> ESM_NONE) Then
          
          'Show message, cant right now
          MsgBox "Cannot perform Paste Properties while in an operation." & vbLf & "Please finish your operation first!", vbExclamation
          
          'Leave here
          Exit Sub
     End If
     
     'Make undo
     CreateUndo "paste sector properties", , , True
     
     'Check if a selection is made
     If (numselected > 0) Then
          
          'Go for all selected linedefs
          Indices = selected.Items
          For i = LBound(Indices) To UBound(Indices)
               
               'Paste properties
               With sectors(Indices(i))
                    .Brightness = CopiedSector.Brightness
                    .hceiling = CopiedSector.hceiling
                    .HFloor = CopiedSector.HFloor
                    .special = CopiedSector.special
                    .Tag = CopiedSector.Tag
                    .tceiling = CopiedSector.tceiling
                    .TFloor = CopiedSector.TFloor
                    .special = CopiedSector.special
               End With
          Next i
          
     'Otherwise, check if a highlight is made
     ElseIf (currentselected > -1) Then
          
          'Paste properties
          With sectors(currentselected)
               .Brightness = CopiedSector.Brightness
               .hceiling = CopiedSector.hceiling
               .HFloor = CopiedSector.HFloor
               .special = CopiedSector.special
               .Tag = CopiedSector.Tag
               .tceiling = CopiedSector.tceiling
               .TFloor = CopiedSector.TFloor
               .special = CopiedSector.special
          End With
     End If
     
     'Remove highlight
     RemoveHighlight True
     
     'Redraw map
     RedrawMap False
     
     'Map changed
     mapchanged = True
     mapnodeschanged = True
End Sub

Private Sub itmSectorsRaiseCeiling_Click()
     Dim Indices As Variant
     Dim i As Long
     
     'Switch back if in move mode
     If (mode = EM_MOVE) Then itmEditMode_Click CInt(PreviousMode)
     
     'Cancel if in drawing operation
     CancelCurrentOperation
     
     'Check if a selection is made
     If (numselected > 0) Then
          
          'Make undo
          CreateUndo "raise ceiling", , , True
          
          'Go for all selected sectors
          Indices = selected.Items
          For i = LBound(Indices) To UBound(Indices)
               
               'Increase height
               sectors(Indices(i)).hceiling = sectors(Indices(i)).hceiling + 8
          Next i
          
     'Otherwise, check if a highlight is made
     ElseIf (currentselected > -1) Then
          
          'Make undo
          CreateUndo "raise ceiling", , , True
          
          'Increase height
          sectors(currentselected).hceiling = sectors(currentselected).hceiling + 8
     End If
     
     'Reselect
     ChangeSectorsHighlight LastX, LastY, True
     
     'Map changed
     mapchanged = True
     mapnodeschanged = True
End Sub

Private Sub itmSectorsRaiseFloor_Click()
     Dim Indices As Variant
     Dim i As Long
     
     'Switch back if in move mode
     If (mode = EM_MOVE) Then itmEditMode_Click CInt(PreviousMode)
     
     'Cancel if in drawing operation
     CancelCurrentOperation
     
     'Check if a selection is made
     If (numselected > 0) Then
          
          'Make undo
          CreateUndo "raise floor", , , True
          
          'Go for all selected sectors
          Indices = selected.Items
          For i = LBound(Indices) To UBound(Indices)
               
               'Increase height
               sectors(Indices(i)).HFloor = sectors(Indices(i)).HFloor + 8
          Next i
          
     'Otherwise, check if a highlight is made
     ElseIf (currentselected > -1) Then
          
          'Make undo
          CreateUndo "raise floor", , , True
          
          'Increase height
          sectors(currentselected).HFloor = sectors(currentselected).HFloor + 8
     End If
     
     'Reselect
     ChangeSectorsHighlight LastX, LastY, True
     
     'Map changed
     mapchanged = True
     mapnodeschanged = True
End Sub

Private Sub itmSectorsSnapToGrid_Click()
     Dim Indices As Variant
     Dim i As Long
     
     'Switch back if in move mode
     If (mode = EM_MOVE) Then itmEditMode_Click CInt(PreviousMode)
     
     'Cancel if in drawing operation
     CancelCurrentOperation
     
     'Check if no selection is made, but a higlight
     If (numselected = 0) And (currentselected > -1) Then
          
          'Undo thise selecting after edit
          DeselectAfterEdit = True
          
          'Select current sector
          SelectCurrentSector
     Else
          
          'Dont deselect after edit
          DeselectAfterEdit = False
     End If
     
     'Check if we have a selection
     If (numselected > 0) Then
          
          'Make Undo
          CreateUndo "snap to grid"
          
          'Go for all selected vertices
          Indices = SelectVerticesFromSelection.Items
          For i = LBound(Indices) To UBound(Indices)
               
               'Snap this vertex to grid now
               vertexes(Indices(i)).x = SnappedToGridX(vertexes(Indices(i)).x)
               vertexes(Indices(i)).y = SnappedToGridY(vertexes(Indices(i)).y)
          Next i
          
          'Map changed
          mapchanged = True
          mapnodeschanged = True
          
          'Deselect if we should
          If DeselectAfterEdit Then RemoveSelection False
          
          'Redraw map
          RedrawMap
     End If
End Sub

Private Sub itmThingsCopy_Click()
     
     'Switch back if in move mode
     If (mode = EM_MOVE) Then itmEditMode_Click CInt(PreviousMode)
     
     'Check if not in normal mode
     If (submode <> ESM_NONE) Then
          
          'Show message, cant right now
          MsgBox "Cannot perform Copy Properties while in an operation." & vbLf & "Please finish your operation first!", vbExclamation
          
          'Leave here
          Exit Sub
     End If
     
     'Check if a selection is made
     If (numselected > 0) Then
          
          'Copy the first selected thing
          CopiedThing = things(selected.Items(0))
          
     ElseIf (currentselected > -1) Then
          
          'Copy the highlighted thing
          CopiedThing = things(currentselected)
          
     Else
          
          'Nothing selected or highlighted
          Exit Sub
     End If
End Sub

Private Sub itmThingsFilter_Click()
     
     'Switch back if in move mode
     If (mode = EM_MOVE) Then itmEditMode_Click CInt(PreviousMode)
     
     'Cancel if in drawing operation
     CancelCurrentOperation
     
     'Show filter dialog
     Load frmThingFilter
     frmThingFilter.Show 1, Me
End Sub

Private Sub itmThingsMouseRotate_Click()

     If selected.Count <> 1 Or submode <> ESM_NONE Then Exit Sub
     submode = ESM_ROTATING
     RedrawMap

End Sub

Private Sub itmThingsPaste_Click()
     Dim Indices As Variant
     Dim i As Long
     
     'Switch back if in move mode
     If (mode = EM_MOVE) Then itmEditMode_Click CInt(PreviousMode)
     
     'Check if not in normal mode
     If (submode <> ESM_NONE) Then
          
          'Show message, cant right now
          MsgBox "Cannot perform Paste Properties while in an operation." & vbLf & "Please finish your operation first!", vbExclamation
          
          'Leave here
          Exit Sub
     End If
     
     'Make undo
     CreateUndo "paste thing properties", , , True
     
     'Check if a selection is made
     If (numselected > 0) Then
          
          'Go for all selected linedefs
          Indices = selected.Items
          For i = LBound(Indices) To UBound(Indices)
               
               'Paste properties
               With things(Indices(i))
                    .angle = CopiedThing.angle
                    .arg0 = CopiedThing.arg0
                    .arg1 = CopiedThing.arg1
                    .arg2 = CopiedThing.arg2
                    .arg3 = CopiedThing.arg3
                    .arg4 = CopiedThing.arg4
                    .Color = CopiedThing.Color
                    .effect = CopiedThing.effect
                    .Flags = CopiedThing.Flags
                    .image = CopiedThing.image
                    .Tag = CopiedThing.Tag
                    .thing = CopiedThing.thing
                    .Z = CopiedThing.Z
               End With
          Next i
          
     'Otherwise, check if a highlight is made
     ElseIf (currentselected > -1) Then
          
          'Paste properties
          With things(currentselected)
               .angle = CopiedThing.angle
               .arg0 = CopiedThing.arg0
               .arg1 = CopiedThing.arg1
               .arg2 = CopiedThing.arg2
               .arg3 = CopiedThing.arg3
               .arg4 = CopiedThing.arg4
               .Color = CopiedThing.Color
               .effect = CopiedThing.effect
               .Flags = CopiedThing.Flags
               .image = CopiedThing.image
               .Tag = CopiedThing.Tag
               .thing = CopiedThing.thing
               .Z = CopiedThing.Z
          End With
     End If
     
     'Remove highlight
     RemoveHighlight True
     
     'Redraw map
     RedrawMap False
     
     'Map changed
     mapchanged = True
End Sub

Private Sub itmThingsRotateCW_Click()
     Dim Indices As Variant
     Dim i As Long
     
     'Switch back if in move mode
     If (mode = EM_MOVE) Then itmEditMode_Click CInt(PreviousMode)
     
     'Cancel if in drawing operation
     CancelCurrentOperation
     
     'Check if a selection is made
     If (numselected > 0) Then
          
          'Make undo
          CreateUndo "rotate thing", , , True
          
          'Go for all selected things
          Indices = selected.Items
          For i = LBound(Indices) To UBound(Indices)
               
               'Increase angle
               things(Indices(i)).angle = things(Indices(i)).angle - 45
               If (things(Indices(i)).angle < 0) Then things(Indices(i)).angle = things(Indices(i)).angle + 360
               
               'Update thing
               UpdateThingImageColor Indices(i)
          Next i
          
     'Otherwise, check if a highlight is made
     ElseIf (currentselected > -1) Then
          
          'Make undo
          CreateUndo "rotate thing", UGRP_THINGANGLECHANGE, currentselected, True
          
          'Increase angle
          things(currentselected).angle = things(currentselected).angle - 45
          If (things(currentselected).angle < 0) Then things(currentselected).angle = things(currentselected).angle + 360
          
          'Update thing
          UpdateThingImageColor currentselected
     End If
     
     'Reselect
     ChangeThingsHighlight LastX, LastY, True
     
     'Map changed
     mapchanged = True
End Sub

Private Sub itmThingsRotateCCW_Click()
     Dim Indices As Variant
     Dim i As Long
     
     'Switch back if in move mode
     If (mode = EM_MOVE) Then itmEditMode_Click CInt(PreviousMode)
     
     'Cancel if in drawing operation
     CancelCurrentOperation
     
     'Check if a selection is made
     If (numselected > 0) Then
          
          'Make undo
          CreateUndo "rotate thing", , , True
          
          'Go for all selected things
          Indices = selected.Items
          For i = LBound(Indices) To UBound(Indices)
               
               'Increase angle
               things(Indices(i)).angle = things(Indices(i)).angle + 45
               If (things(Indices(i)).angle >= 360) Then things(Indices(i)).angle = things(Indices(i)).angle - 360
               
               'Update thing
               UpdateThingImageColor Indices(i)
          Next i
          
     'Otherwise, check if a highlight is made
     ElseIf (currentselected > -1) Then
          
          'Make undo
          CreateUndo "rotate thing", UGRP_THINGANGLECHANGE, currentselected, True
          
          'Increase angle
          things(currentselected).angle = things(currentselected).angle + 45
          If (things(currentselected).angle >= 360) Then things(currentselected).angle = things(currentselected).angle - 360
          
          'Update thing
          UpdateThingImageColor currentselected
     End If
     
     'Reselect
     ChangeThingsHighlight LastX, LastY, True
     
     'Map changed
     mapchanged = True
End Sub

Private Sub itmThingsSnapToGrid_Click()
     Dim Indices As Variant
     Dim i As Long
     
     'Switch back if in move mode
     If (mode = EM_MOVE) Then itmEditMode_Click CInt(PreviousMode)
     
     'Cancel if in drawing operation
     CancelCurrentOperation
     
     'Check if no selection is made, but a higlight
     If (numselected = 0) And (currentselected > -1) Then
          
          'Undo thise selecting after edit
          DeselectAfterEdit = True
          
          'Select current thing
          SelectCurrentThing
     Else
          
          'Youll only understand this if you are 1337 H4X0R!!! MWhahahah!!!
          DeselectAfterEdit = False
     End If
     
     'Check if we have a selection
     If (numselected > 0) Then
          
          'Make Undo
          CreateUndo "snap to grid"
          
          'Go for all selected vertices
          Indices = selected.Items
          For i = LBound(Indices) To UBound(Indices)
               
               'Snap this vertex to grid now
               things(Indices(i)).x = SnappedToGridX(things(Indices(i)).x)
               things(Indices(i)).y = SnappedToGridY(things(Indices(i)).y)
          Next i
          
          'Map changed
          mapchanged = True
          mapnodeschanged = True
          
          'Deselect if we should
          If DeselectAfterEdit Then RemoveSelection False
          
          'Redraw map
          RedrawMap
     End If
End Sub

Private Sub itmToolbarCustomise_Click()

     itmToolsCustomiseTB_Click

End Sub

Private Sub itmToolsClearTextures_Click()
     Dim ld As Long
     
     'Cancel if in drawing operation
     CancelCurrentOperation
     
     'Switch back if in move mode
     If (mode = EM_MOVE) Then itmEditMode_Click CInt(PreviousMode)
     
     'Make undo
     CreateUndo "clear unused textures"
     
     'Check if a selection is made
     If (numselected > 0) Then
          
          'Go for all linedefs
          For ld = 0 To (numlinedefs - 1)
               
               'Check if selected or none selected at all
               If (linedefs(ld).selected <> 0) Or (mode <> EM_LINES) Then
                    
                    'Check if linedef has a front sidedef
                    If (linedefs(ld).s1 > -1) Then
                         
                         'Remove upper texture if not required
                         If Not RequiresS1Upper(ld) Then sidedefs(linedefs(ld).s1).Upper = "-"
                         
                         'Remove middle texture if not required
                         'If Not RequiresS1Middle(ld) Then sidedefs(linedefs(ld).s1).Middle = "-")
                         
                         'Remove lower texture if not required
                         If Not RequiresS1Lower(ld) Then sidedefs(linedefs(ld).s1).Lower = "-"
                    End If
                    
                    'Check if linedef has a back sidedef
                    If (linedefs(ld).s2 > -1) Then
                         
                         'Remove upper texture if not required
                         If Not RequiresS2Upper(ld) Then sidedefs(linedefs(ld).s2).Upper = "-"
                         
                         'Remove middle texture if not required
                         'If Not RequiresS2Middle(ld) Then sidedefs(linedefs(ld).s2).Middle = "-")
                         
                         'Remove lower texture if not required
                         If Not RequiresS2Lower(ld) Then sidedefs(linedefs(ld).s2).Lower = "-"
                    End If
               End If
          Next ld
     Else
          
          'Please make a selection!
          'This may also remove textures from your changing sectors.
          MsgBox "Please make a selection so that you dont accedentially remove textures from sectors that may need them after being triggered.", vbExclamation
     End If
     
     'Map changed
     mapchanged = True
     mapnodeschanged = True
End Sub

Public Sub itmToolsConfiguration_Click()
     
     'Configure
     ShowConfiguration 1
End Sub

Private Sub itmToolsCustomiseTB_Click()
     
     tlbToolbar.Customize

End Sub

Private Sub itmToolsEffectsZoomTube_Click()

     frmZoomTubes.Show False, Me

End Sub

Private Sub itmToolsFindErrors_Click()
     
     'Switch back if in move mode
     If (mode = EM_MOVE) Then itmEditMode_Click CInt(PreviousMode)
     
     'Cancel if in drawing operation
     CancelCurrentOperation
     
     'Clear selection
     RemoveSelection True
     
     'Load dialog
     Load frmErrorCheck
     
     'Show dialog
     frmErrorCheck.Show 1, Me
End Sub

Private Sub itmToolsFixTextures_Click()
     Dim ld As Long
     Dim DefaultTexture As Dictionary
     
     'Cancel if in drawing operation
     CancelCurrentOperation
     
     'Switch back if in move mode
     If (mode = EM_MOVE) Then itmEditMode_Click CInt(PreviousMode)
     
     'Make undo
     CreateUndo "fix missing textures"
     
     'Ensure valid textures are used to build with
     CorrectDefaultTextures
     
     If (Val(Config("storeeditinginfo"))) And WadSettings.Exists("defaulttexture") Then
          Set DefaultTexture = WadSettings("defaulttexture")
     Else
          Set DefaultTexture = Config("defaulttexture")
     End If
     
     'Go for all linedefs
     For ld = 0 To (numlinedefs - 1)
          
          'Check if selected or none selected at all
          If (linedefs(ld).selected <> 0) Or (numselected = 0) Or (mode <> EM_LINES) Then
               
               'Check if linedef has a front sidedef
               If (linedefs(ld).s1 > -1) Then
                    
                    'Ensure upper texture if required
                    If RequiresS1Upper(ld) And Not IsTextureName(sidedefs(linedefs(ld).s1).Upper) Then sidedefs(linedefs(ld).s1).Upper = DefaultTexture("upper")
                    
                    'Ensure middle texture if required
                    If RequiresS1Middle(ld) And Not IsTextureName(sidedefs(linedefs(ld).s1).Middle) Then sidedefs(linedefs(ld).s1).Middle = DefaultTexture("middle")
                    
                    'Ensure lower texture if required
                    If RequiresS1Lower(ld) And Not IsTextureName(sidedefs(linedefs(ld).s1).Lower) Then sidedefs(linedefs(ld).s1).Lower = DefaultTexture("lower")
               End If
               
               'Check if linedef has a back sidedef
               If (linedefs(ld).s2 > -1) Then
                    
                    'Ensure upper texture if required
                    If RequiresS2Upper(ld) And Not IsTextureName(sidedefs(linedefs(ld).s2).Upper) Then sidedefs(linedefs(ld).s2).Upper = DefaultTexture("upper")
                    
                    'Ensure middle texture if required
                    If RequiresS2Middle(ld) And Not IsTextureName(sidedefs(linedefs(ld).s2).Middle) Then sidedefs(linedefs(ld).s2).Middle = DefaultTexture("middle")
                    
                    'Ensure lower texture if required
                    If RequiresS2Lower(ld) And Not IsTextureName(sidedefs(linedefs(ld).s2).Lower) Then sidedefs(linedefs(ld).s2).Lower = DefaultTexture("lower")
               End If
          End If
     Next ld
     
     'Map changed
     mapchanged = True
     mapnodeschanged = True
End Sub

Private Sub itmToolsFixZeroLinedefs_Click()
     Dim ld As Long
     Dim Count As Long
     
     'Cancel if in drawing operation
     CancelCurrentOperation
     
     'Switch back if in move mode
     If (mode = EM_MOVE) Then itmEditMode_Click CInt(PreviousMode)
     
     'Make undo
     CreateUndo "fix zero-length linedefs"
     
     'Go for all linedefs
     ld = numlinedefs - 1
     Do While (ld >= 0)
          
          'Check if linedef refers to same vertices
          If (linedefs(ld).V1 = linedefs(ld).V2) Then
               
               'Simply remove the linedef
               RemoveLinedef ld, True, True, True
               Count = Count + 1
          Else
               
               'Check if both vertices are at same location
               If (CLng(vertexes(linedefs(ld).V1).x) = CLng(vertexes(linedefs(ld).V2).x)) And _
                  (CLng(vertexes(linedefs(ld).V1).y) = CLng(vertexes(linedefs(ld).V2).y)) Then
                    
                    'Stitch the vertices
                    StitchVertices linedefs(ld).V1, linedefs(ld).V2
                    Count = Count + 1
               End If
          End If
          
          'Next linedef
          ld = ld - 1
     Loop
     
     'Redraw map
     RedrawMap False
     
     'Report
     If (Count > 0) Then
          
          'Show result
          MsgBox Count & " Zero-Length Linedefs have been solved.", vbInformation
          
          'Map changed
          mapchanged = True
          mapnodeschanged = True
     Else
          
          'Nothing found
          MsgBox "No Zero-Length Linedefs found.", vbInformation
     End If
End Sub


Private Sub itmToolsReloadResources_Click()

     'Show status dialog
     frmStatus.Show 0, frmMain
     frmMain.SetFocus
     frmMain.Refresh
     
     'Close additional wads
     IWAD.CloseFile
     AddWAD.CloseFile
     
     'Open additional wads
     OpenIWADFile
     OpenADDWADFile
     
     'Precache resources
     MapLoadResources
     
     'Unload status dialog
     Unload frmStatus: Set frmSplash = Nothing
     
End Sub

Private Sub itmToolsTagBrowser_Click()

     frmTags.Show vbModeless, Me

End Sub

Private Sub itmVerticesClearUnused_Click()
     
     'Switch back if in move mode
     If (mode = EM_MOVE) Then itmEditMode_Click CInt(PreviousMode)
     
     'Cancel if in drawing operation
     CancelCurrentOperation
     
     'Make undo
     CreateUndo "remove unused vertices"
     
     'Rmeove unused vertices
     RemoveUnusedVertices
     
     'Remove selection
     RemoveSelection False
     
     'Redraw map
     RedrawMap
End Sub

Private Sub itmVerticesSnapToGrid_Click()
     Dim Indices As Variant
     Dim i As Long
     
     'Switch back if in move mode
     If (mode = EM_MOVE) Then itmEditMode_Click CInt(PreviousMode)
     
     'Cancel if in drawing operation
     CancelCurrentOperation
     
     'Check if no selection is made, but a higlight
     If (numselected = 0) And (currentselected > -1) Then
          
          'Undo thise selecting after edit
          DeselectAfterEdit = True
          
          'Select current vertex
          SelectCurrentVertex
     Else
          
          'Youll only understand this if you are 1337 H4X0R!!! MWhahahah!!!
          DeselectAfterEdit = False
     End If
     
     'Check if we have a selection
     If (numselected > 0) Then
          
          'Make Undo
          CreateUndo "snap to grid"
          
          'Go for all selected vertices
          Indices = selected.Items
          For i = LBound(Indices) To UBound(Indices)
               
               'Snap this vertex to grid now
               vertexes(Indices(i)).x = SnappedToGridX(vertexes(Indices(i)).x)
               vertexes(Indices(i)).y = SnappedToGridY(vertexes(Indices(i)).y)
          Next i
          
          'Map changed
          mapchanged = True
          mapnodeschanged = True
          
          'Deselect if we should
          If DeselectAfterEdit Then RemoveSelection False
          
          'Redraw map
          RedrawMap
     End If
End Sub

Private Sub KeypressGeneral(ByVal ShortcutCode As Long, ByRef DoUpdateStatusBar As Boolean, ByRef DoRedrawMap As Boolean)
     Dim Xdiff As Long, Ydiff As Long
     Dim NewZ As Single
     Dim Keybinds As Variant
     Dim AllowF7Message As Boolean
     Dim i As Long
     
     'Check what shortcut is pressed in ANY mode
     Select Case ShortcutCode
          
          Case Config("shortcuts")("zoomin")
               
               'Increase zoom
               NewZ = ViewZoom * (1 + Config("zoomspeed") / 1000)
               If NewZ > 100 Then NewZ = 100
               If (Val(Config("zoommouse")) <> 0) And (MouseInside = True) Then
                    Xdiff = ((ScreenWidth / NewZ) - (ScreenWidth / ViewZoom)) * ((LastX - picMap.ScaleLeft) / picMap.ScaleWidth)
                    Ydiff = ((ScreenHeight / NewZ) - (ScreenHeight / ViewZoom)) * ((LastY - picMap.ScaleTop) / picMap.ScaleHeight)
               Else
                    Xdiff = ((ScreenWidth / NewZ) - (ScreenWidth / ViewZoom)) / 2
                    Ydiff = ((ScreenHeight / NewZ) - (ScreenHeight / ViewZoom)) / 2
               End If
               ChangeView ViewLeft - Xdiff, ViewTop - Ydiff, NewZ
               DoRedrawMap = True
               DoUpdateStatusBar = True
               
          Case Config("shortcuts")("zoomout")
               
               'Decrease zoom
               NewZ = ViewZoom * (1 - Config("zoomspeed") / 1000)
               If NewZ < 0.05 Then NewZ = 0.05
               If (Val(Config("zoommouse")) <> 0) And (MouseInside = True) Then
                    Xdiff = ((ScreenWidth / NewZ) - (ScreenWidth / ViewZoom)) * ((LastX - picMap.ScaleLeft) / picMap.ScaleWidth)
                    Ydiff = ((ScreenHeight / NewZ) - (ScreenHeight / ViewZoom)) * ((LastY - picMap.ScaleTop) / picMap.ScaleHeight)
               Else
                    Xdiff = ((ScreenWidth / NewZ) - (ScreenWidth / ViewZoom)) / 2
                    Ydiff = ((ScreenHeight / NewZ) - (ScreenHeight / ViewZoom)) / 2
               End If
               ChangeView ViewLeft - Xdiff, ViewTop - Ydiff, NewZ
               DoRedrawMap = True
               DoUpdateStatusBar = True
               
          Case Config("shortcuts")("scrollleft")
               
               'Move left
               LastX = LastX - Config("scrollpixels") / ViewZoom
               ChangeView ViewLeft - Config("scrollpixels") / ViewZoom, ViewTop, ViewZoom
               DoRedrawMap = True
               DoUpdateStatusBar = True
               
               'Update cursor position in statusbar
               stbStatus.Panels("mousex").Text = "X " & CLng(LastX)
               stbStatus.Panels("mousey").Text = "Y " & -CLng(LastY)
               
          Case Config("shortcuts")("scrollright")
               
               'Move right
               LastX = LastX + Config("scrollpixels") / ViewZoom
               ChangeView ViewLeft + Config("scrollpixels") / ViewZoom, ViewTop, ViewZoom
               DoRedrawMap = True
               DoUpdateStatusBar = True
               
               'Update cursor position in statusbar
               stbStatus.Panels("mousex").Text = "X " & CLng(LastX)
               stbStatus.Panels("mousey").Text = "Y " & -CLng(LastY)
               
          Case Config("shortcuts")("scrollup")
               
               'Move up
               LastY = LastY - Config("scrollpixels") / ViewZoom
               ChangeView ViewLeft, ViewTop - Config("scrollpixels") / ViewZoom, ViewZoom
               DoRedrawMap = True
               DoUpdateStatusBar = True
               
               'Update cursor position in statusbar
               stbStatus.Panels("mousex").Text = "X " & CLng(LastX)
               stbStatus.Panels("mousey").Text = "Y " & -CLng(LastY)
               
          Case Config("shortcuts")("scrolldown")
               
               'Move down
               LastY = LastY + Config("scrollpixels") / ViewZoom
               ChangeView ViewLeft, ViewTop + Config("scrollpixels") / ViewZoom, ViewZoom
               DoRedrawMap = True
               DoUpdateStatusBar = True
               
               'Update cursor position in statusbar
               stbStatus.Panels("mousex").Text = "X " & CLng(LastX)
               stbStatus.Panels("mousey").Text = "Y " & -CLng(LastY)
               
          Case Config("shortcuts")("gridinc")
               
               'Decrease grid
               gridsizex = gridsizex / 2
               gridsizey = gridsizey / 2
               If (gridsizex < 1) Then gridsizex = 1
               If (gridsizey < 1) Then gridsizey = 1
               DoRedrawMap = True
               DoUpdateStatusBar = True
               
          Case Config("shortcuts")("griddec")
               
               'Increase grid
               gridsizex = gridsizex * 2
               gridsizey = gridsizey * 2
               If (gridsizex > 1024) Then gridsizex = 1024
               If (gridsizey > 1024) Then gridsizey = 1024
               DoRedrawMap = True
               DoUpdateStatusBar = True
               
          Case Config("shortcuts")("deselectall")
               
               'Only in normal mode
               If (submode = ESM_NONE) Then
                    
                    'Deselect and remove highlight
                    RemoveHighlight True
                    RemoveSelection True
               End If
               
          Case Config("shortcuts")("cancel")
               
               'Cancel if in drawing operation
               CancelCurrentOperation
               
          Case Config("shortcuts")("drawsector2")
               
               'Cancel if in drawing operation
               CancelCurrentOperation
               
               'Switch to lines mode if needed
               If (mode <> EM_LINES) And (mode <> EM_SECTORS) Then itmEditMode_Click EM_LINES
               
               'Start drawing
               StartDrawOperation
               
          Case Config("shortcuts")("reversedrawing")
               
               'Check if in drawing operation
               If (submode = ESM_DRAWING) Then RevertDrawingOperation
               
          Case Config("shortcuts")("place3dstart")
               
               'Check if mouse cursor is on the map
               If MouseInside Then
                    
                    'Place 3D Start mode Thing
                    Place3DModeStart LastX, LastY
                    DoUpdateStatusBar = True
                    DoRedrawMap = True
               End If
               
          Case Config("shortcuts")("editquickmove")
               
               'Toggle move mode
               If (mode <> EM_MOVE) Then
                    
                    'Switch to move mode now
                    'itmEditMode_Click EM_MOVE
                    
                    'Change mousecursor
                    Set picMap.MouseIcon = imgCursor(2).Picture
                    picMap.MousePointer = vbCustom
                    
                    'And drag now
                    submode = ESM_MOVING
               End If
               
               
          Case Config("shortcuts")("scroll")
          
               CancelCurrentOperation
               ForceScroll = True
               
               
          Case Config("shortcuts")("togglebar")
               
               'Toggle the Info Bar
               InfoBarToggle
               
               
          Case Config("shortcuts")("findtagtarget")
          
               ' Find my brother.
               FindTagTarget
               
               
          'Lesson 1: Dont post flames against Doom Builder :P
          Case vbKeyF7
               
               'Have we hold F7 a while?
               If (F7Count = 100) Then
                    
                    'Presume we can display the message
                    AllowF7Message = True
                    
                    'Is there really no key assigned to F7?
                    Keybinds = Config("shortcuts").Items
                    For i = LBound(Keybinds) To UBound(Keybinds)
                         
                         'We dont want to interrupt any real features
                         If (Keybinds(i) = vbKeyF7) Then AllowF7Message = False
                    Next i
                    
                    'Bla
                    If (AllowF7Message) Then MsgBox "Join objects?", vbYesNo Or vbQuestion, "Not SRB2 Doom Builder"
                    
                    'Reset the count
                    F7Count = 0
               Else
                    
                    'Count the keypress
                    F7Count = F7Count + 1
               End If
     End Select
End Sub

Private Sub KeyreleaseGeneral(ByVal ShortcutCode As Long, ByRef DoUpdateStatusBar As Boolean, ByRef DoRedrawMap As Boolean)
     
     'Check what shortcut is released in ANY mode
     Select Case ShortcutCode
          
          Case Config("shortcuts")("editquickmove")
               
               'Return from move mode
               If (mode <> EM_MOVE) Then
                    
                    'No longer moving
                    Set picMap.MouseIcon = imgCursor(1).Picture
                    picMap.MousePointer = vbNormal
                    submode = ESM_NONE
               End If
               
          Case Config("shortcuts")("scroll")
          
               ForceScroll = False
               
     End Select
End Sub


Private Sub KeypressLines(ByVal ShortcutCode As Long)
     
     'Check what key is pressed
     Select Case ShortcutCode
          
          Case Config("shortcuts")("drawsector")
               
               'Only in normal mode
               If (submode = ESM_NONE) Then
                    
                    'Start drawing
                    StartDrawOperation
               End If
          
          Case Config("shortcuts")("createsector")
               
               'Only in normal mode
               If (submode = ESM_NONE) Then
                    
                    'Insert vertex
                    If MouseInside Then CreateSectorHere LastX, LastY
               End If
     End Select
End Sub

Private Sub KeypressMenus(ByVal ShortcutCode As Long)
     
     'Check what shortcut is pressed for menus
     Select Case ShortcutCode
          Case Config("shortcuts")("filenew"): mnuFile_Click: If itmFile(0).Enabled Then itmFileNew_Click
          Case Config("shortcuts")("fileopen"): mnuFile_Click: If itmFile(1).Enabled Then itmFileOpenMap_Click
          Case Config("shortcuts")("fileswitchmap"): mnuFile_Click: If itmFile(2).Enabled Then itmFileSwitchMap_Click
          Case Config("shortcuts")("fileclose"): mnuFile_Click: If itmFile(3).Enabled Then itmFileCloseMap_Click
          Case Config("shortcuts")("filesave"): mnuFile_Click: If itmFile(5).Enabled Then itmFileSaveMap_Click
          Case Config("shortcuts")("filesaveas"): mnuFile_Click: If itmFile(6).Enabled Then itmFileSaveMapAs_Click
          Case Config("shortcuts")("filesaveinto"): mnuFile_Click: If itmFile(7).Enabled Then itmFileSaveMapInto_Click
          Case Config("shortcuts")("filebuildnodes"): mnuFile_Click: If itmFile(12).Enabled Then itmFileBuild_Click
          Case Config("shortcuts")("filetest"): mnuFile_Click: If itmFile(13).Enabled Then itmFileTest_Click False
          Case Config("shortcuts")("filetest2"): mnuFile_Click: If itmFile(13).Enabled Then itmFileTest_Click True
          Case Config("shortcuts")("fileexport"): mnuFile_Click: If itmFile(9).Enabled Then itmFileExportMap_Click
          Case Config("shortcuts")("fileexportpicture"): mnuFile_Click: If itmFile(10).Enabled Then itmFileExportPicture_Click
          
          Case Config("shortcuts")("editundo"): mnuEdit_Click: If itmEditUndo.Enabled Then itmEditUndo_Click
          Case Config("shortcuts")("editredo"): mnuEdit_Click: If itmEditRedo.Enabled Then itmEditRedo_Click
          Case Config("shortcuts")("editmove"): mnuEdit_Click: If itmEditMode(0).Enabled Then itmEditMode_Click 0
          Case Config("shortcuts")("editvertices"): mnuEdit_Click: If itmEditMode(1).Enabled Then itmEditMode_Click 1
          Case Config("shortcuts")("editlines"): mnuEdit_Click: If itmEditMode(2).Enabled Then itmEditMode_Click 2
          Case Config("shortcuts")("editsectors"): mnuEdit_Click: If itmEditMode(3).Enabled Then itmEditMode_Click 3
          Case Config("shortcuts")("editthings"): mnuEdit_Click: If itmEditMode(4).Enabled Then itmEditMode_Click 4
          Case Config("shortcuts")("edit3d"): mnuEdit_Click: If itmEditMode(5).Enabled Then itmEditMode_Click 5
          Case Config("shortcuts")("editcut"): mnuEdit_Click: If itmEdit(1).Enabled Then itmEditCut_Click
          Case Config("shortcuts")("editcopy"): mnuEdit_Click: If itmEdit(2).Enabled Then itmEditCopy_Click
          Case Config("shortcuts")("editpaste"): mnuEdit_Click: If itmEdit(3).Enabled Then itmEditPaste_Click
          Case Config("shortcuts")("editdelete"): mnuEdit_Click: If itmEdit(4).Enabled Then itmEditDelete_Click
          Case Config("shortcuts")("editfind"): mnuEdit_Click: If itmEdit(6).Enabled Then itmEditFind_Click
          Case Config("shortcuts")("editreplace"): mnuEdit_Click: If itmEdit(7).Enabled Then itmEditReplace_Click
          Case Config("shortcuts")("editoptions"): mnuEdit_Click: If itmEdit(23).Enabled Then itmEditMapOptions_Click
          Case Config("shortcuts")("editfliph"): mnuEdit_Click: If itmEdit(9).Enabled Then itmEditFlipH_Click
          Case Config("shortcuts")("editflipv"): mnuEdit_Click: If itmEdit(10).Enabled Then itmEditFlipV_Click
          Case Config("shortcuts")("editrotate"): mnuEdit_Click: If itmEdit(11).Enabled Then itmEditRotate_Click
          Case Config("shortcuts")("editresize"): mnuEdit_Click: If itmEdit(12).Enabled Then itmEditResize_Click
          Case Config("shortcuts")("editcenterview"): mnuEdit_Click: If itmEdit(17).Enabled Then itmEditCenterView_Click
          Case Config("shortcuts")("editselectall"): mnuEdit_Click: If itmEdit(18).Enabled Then itmEditSelectAll_Click
          Case Config("shortcuts")("editselectnone"): mnuEdit_Click: If itmEdit(20).Enabled Then itmEditSelectNone_Click
          Case Config("shortcuts")("editselectinvert"): mnuEdit_Click: If itmEdit(21).Enabled Then itmEditSelectInvert_Click
          Case Config("shortcuts")("togglesnap"): mnuEdit_Click: If itmEdit(14).Enabled Then itmEditSnapToGrid_Click
          Case Config("shortcuts")("togglesnapvertex"): mnuEdit_Click: If itmEdit(15).Enabled Then itmEditSnapToVertices_Click
          Case Config("shortcuts")("togglestitch"): mnuEdit_Click: If itmEdit(16).Enabled Then itmEditStitch_Click
          
          Case Config("shortcuts")("clearvertices"): If mnuVertices.visible Then mnuVertices_Click: If itmVerticesClearUnused.Enabled Then itmVerticesClearUnused_Click
          Case Config("shortcuts")("stitchvertices"): If mnuVertices.visible Then mnuVertices_Click: If itmVerticesStitch.Enabled Then itmVerticesStitch_Click
          
          Case Config("shortcuts")("linesautoalign"): If mnuLines.visible Then mnuLines_Click: If itmLinesAlign.Enabled Then itmLinesAlign_Click
          Case Config("shortcuts")("select1sided"): If mnuLines.visible Then mnuLines_Click: If itmLinesSelect(0).Enabled Then itmLinesSelect_Click 0
          Case Config("shortcuts")("select2sided"): If mnuLines.visible Then mnuLines_Click: If itmLinesSelect(1).Enabled Then itmLinesSelect_Click 1
          Case Config("shortcuts")("fliplinedefs"): If mnuLines.visible Then mnuLines_Click: If itmLinesFlipLinedefs.Enabled Then itmLinesFlipLinedefs_Click
          Case Config("shortcuts")("splitlinedefs"): If mnuLines.visible Then mnuLines_Click: If itmLinesSplit.Enabled Then itmLinesSplit_Click
          Case Config("shortcuts")("flipsidedefs"): If mnuLines.visible Then mnuLines_Click: If itmLinesFlipSidedefs.Enabled Then itmLinesFlipSidedefs_Click
          Case Config("shortcuts")("curvelines"): If mnuLines.visible Then mnuLines_Click: If itmLinesCurve.Enabled Then itmLinesCurve_Click
          
          Case Config("shortcuts")("cascadetags")
               If mnuLines.visible Then
                    mnuLines_Click
                    If itmLinesCascadeTags.Enabled Then itmLinesCascadeTags_Click
               ElseIf mnuSectors.visible Then
                    mnuSectors_Click
                    If itmSectorsCascadeTags.visible Then itmSectorsCascadeTags_Click
               End If
          
          Case Config("shortcuts")("joinsector"): If mnuSectors.visible Then mnuSectors_Click: If itmSectorsJoin.Enabled Then itmSectorsJoin_Click
          Case Config("shortcuts")("mergesector"): If mnuSectors.visible Then mnuSectors_Click: If itmSectorsMerge.Enabled Then itmSectorsMerge_Click
          Case Config("shortcuts")("raisefloor"): If mnuSectors.visible Then mnuSectors_Click: If itmSectorsRaiseFloor.Enabled Then itmSectorsRaiseFloor_Click
          Case Config("shortcuts")("lowerfloor"): If mnuSectors.visible Then mnuSectors_Click: If itmSectorsLowerFloor.Enabled Then itmSectorsLowerFloor_Click
          Case Config("shortcuts")("raiseceil"): If mnuSectors.visible Then mnuSectors_Click: If itmSectorsRaiseCeiling.Enabled Then itmSectorsRaiseCeiling_Click
          Case Config("shortcuts")("lowerceil"): If mnuSectors.visible Then mnuSectors_Click: If itmSectorsLowerCeiling.Enabled Then itmSectorsLowerCeiling_Click
          Case Config("shortcuts")("brightinc"): If mnuSectors.visible Then mnuSectors_Click: If itmSectorsIncBrightness.Enabled Then itmSectorsIncBrightness_Click
          Case Config("shortcuts")("brightdec"): If mnuSectors.visible Then mnuSectors_Click: If itmSectorsDecBrightness.Enabled Then itmSectorsDecBrightness_Click
          Case Config("shortcuts")("gradientbrightness"): If mnuSectors.visible Then mnuSectors_Click: If itmSectorsGradientBrightness.Enabled Then itmSectorsGradientBrightness_Click
          Case Config("shortcuts")("gradientfloors"): If mnuSectors.visible Then mnuSectors_Click: If itmSectorsGradientFloors.Enabled Then itmSectorsGradientFloors_Click
          Case Config("shortcuts")("gradientceilings"): If mnuSectors.visible Then mnuSectors_Click: If itmSectorsGradientCeilings.Enabled Then itmSectorsGradientCeilings_Click
          Case Config("shortcuts")("fofsetup"): If mnuSectors.visible Then mnuSectors_Click: If itmSectorsFOF.Enabled Then itmSectorsFOF_Click
          Case Config("shortcuts")("stairs"): If mnuSectors.visible Then mnuSectors_Click: If itmSectorsConvertToStairs.Enabled Then itmSectorsConvertToStairs_Click
          Case Config("shortcuts")("sectorsident"): If mnuSectors.visible Then mnuSectors_Click: If itmSectorsFindIdenticalSectorSet.Enabled Then itmSectorsFindIdenticalSectorSet_Click
          
          Case Config("shortcuts")("thingsfilter"): If mnuThings.visible Then mnuThings_Click: If itmThingsFilter.Enabled Then itmThingsFilter_Click
          Case Config("shortcuts")("thingrotatecw"): If mnuThings.visible Then mnuThings_Click: If itmThingsRotateCW.Enabled Then itmThingsRotateCW_Click
          Case Config("shortcuts")("thingrotateccw"): If mnuThings.visible Then mnuThings_Click: If itmThingsRotateCCW.Enabled Then itmThingsRotateCCW_Click
          Case Config("shortcuts")("thingsmouserotate"): If mnuThings.visible Then mnuThings_Click: If itmThingsMouseRotate.Enabled Then itmThingsMouseRotate_Click
               
          Case Config("shortcuts")("errorcheck"): If mnuTools.visible Then mnuTools_Click: If itmToolsFindErrors.Enabled Then itmToolsFindErrors_Click
          Case Config("shortcuts")("removetextures"): If mnuTools.visible Then mnuTools_Click: If itmToolsClearTextures.Enabled Then itmToolsClearTextures_Click
          Case Config("shortcuts")("toolstagbrowser"): If mnuTools.visible Then mnuTools_Click: If itmToolsTagBrowser.Enabled Then itmToolsTagBrowser_Click
          Case Config("shortcuts")("fixtextures"): If mnuTools.visible Then mnuTools_Click: If itmToolsFixTextures.Enabled Then itmToolsFixTextures_Click
          Case Config("shortcuts")("fileconfig"): mnuTools_Click: If itmToolsConfiguration.Enabled Then itmToolsConfiguration_Click
          Case Config("shortcuts")("fixzerolengthlines"): mnuTools_Click: If itmToolsFixZeroLinedefs.Enabled Then itmToolsFixZeroLinedefs_Click
          Case Config("shortcuts")("reloadresources"): mnuTools_Click: If itmToolsReloadResources.Enabled Then itmToolsReloadResources_Click
          
          Case Config("shortcuts")("prefabinsert"): If mnuPrefabs.visible Then mnuPrefabs_Click: If itmPrefabInsert.Enabled Then itmPrefabInsert_Click
          Case Config("shortcuts")("prefabinsertlast"): If mnuPrefabs.visible Then mnuPrefabs_Click: If itmPrefabPrevious.Enabled Then itmPrefabPrevious_Click
          Case Config("shortcuts")("prefabinsert1"): If mnuPrefabs.visible Then mnuPrefabs_Click: If itmPrefabQuick(0).Enabled Then itmPrefabQuick_Click 0
          Case Config("shortcuts")("prefabinsert2"): If mnuPrefabs.visible Then mnuPrefabs_Click: If itmPrefabQuick(1).Enabled Then itmPrefabQuick_Click 1
          Case Config("shortcuts")("prefabinsert3"): If mnuPrefabs.visible Then mnuPrefabs_Click: If itmPrefabQuick(2).Enabled Then itmPrefabQuick_Click 2
          Case Config("shortcuts")("prefabinsert4"): If mnuPrefabs.visible Then mnuPrefabs_Click: If itmPrefabQuick(3).Enabled Then itmPrefabQuick_Click 3
          Case Config("shortcuts")("prefabinsert5"): If mnuPrefabs.visible Then mnuPrefabs_Click: If itmPrefabQuick(4).Enabled Then itmPrefabQuick_Click 4
          
          Case Config("shortcuts")("helpwebsite"): itmHelpWebsite_Click
          Case Config("shortcuts")("helpfaq"): itmHelpFAQ_Click
          Case Config("shortcuts")("helpabout"): itmHelpAbout_Click
          
          
          Case Config("shortcuts")("snaptogrid")
               If mnuVertices.visible Then
                    mnuVertices_Click
                    If itmVerticesSnapToGrid.Enabled Then itmVerticesSnapToGrid_Click
               ElseIf mnuLines.visible Then
                    mnuLines_Click
                    If itmLinesSnapToGrid.Enabled Then itmLinesSnapToGrid_Click
               ElseIf mnuSectors.visible Then
                    mnuSectors_Click
                    If itmSectorsSnapToGrid.Enabled Then itmSectorsSnapToGrid_Click
               ElseIf mnuThings.visible Then
                    mnuThings_Click
                    If itmThingsSnapToGrid.Enabled Then itmThingsSnapToGrid_Click
               End If
               
               
          Case Config("shortcuts")("switchmode")
               
               'Check what mode to switch to
               Select Case mode
                    
                    Case EM_MOVE
                         mnuEdit_Click
                         If itmEditMode(EM_VERTICES).Enabled Then itmEditMode_Click EM_VERTICES
                    
                    Case EM_VERTICES
                         mnuEdit_Click
                         If itmEditMode(EM_LINES).Enabled Then itmEditMode_Click EM_LINES
                    
                    Case EM_LINES
                         mnuEdit_Click
                         If itmEditMode(EM_SECTORS).Enabled Then itmEditMode_Click EM_SECTORS
                    
                    Case EM_SECTORS
                         mnuEdit_Click
                         If itmEditMode(EM_THINGS).Enabled Then itmEditMode_Click EM_THINGS
                    
                    Case EM_THINGS
                         mnuEdit_Click
                         If itmEditMode(EM_VERTICES).Enabled Then itmEditMode_Click EM_VERTICES
               End Select
          
          
          Case Config("shortcuts")("copyprops")
               If mnuLines.visible Then
                    mnuLines_Click
                    If itmLinesCopy.Enabled Then itmLinesCopy_Click
               ElseIf mnuSectors.visible Then
                    mnuSectors_Click
                    If itmSectorsCopy.Enabled Then itmSectorsCopy_Click
               ElseIf mnuThings.visible Then
                    mnuThings_Click
                    If itmThingsCopy.Enabled Then itmThingsCopy_Click
               End If
          
          
          Case Config("shortcuts")("pasteprops")
               If mnuLines.visible Then
                    mnuLines_Click
                    If itmLinesPaste.Enabled Then itmLinesPaste_Click
               ElseIf mnuSectors.visible Then
                    mnuSectors_Click
                    If itmSectorsPaste.Enabled Then itmSectorsPaste_Click
               ElseIf mnuThings.visible Then
                    mnuThings_Click
                    If itmThingsPaste.Enabled Then itmThingsPaste_Click
               End If
               
     End Select
     
End Sub

Private Sub KeypressSectors(ByVal ShortcutCode As Long)
     
     'Check what key is pressed
     Select Case ShortcutCode
          
          Case Config("shortcuts")("drawsector")
               
               'Only in normal mode
               If (submode = ESM_NONE) Then
                    
                    'Start drawing
                    StartDrawOperation
               End If
          
          Case Config("shortcuts")("createsector")
               
               'Only in normal mode
               If (submode = ESM_NONE) Then
                    
                    'Insert vertex
                    If MouseInside Then CreateSectorHere LastX, LastY
               End If
               
               
     End Select
End Sub

Private Sub KeypressThings(ByVal ShortcutCode As Long)
     
     ' Can't check in the select, in case someone assigns Esc to Insert Thing.
     If submode = ESM_ROTATING And ShortcutCode = vbKeyEscape Then
          submode = ESM_NONE
          RedrawMap
     End If
     
     'Check what key is pressed
     Select Case ShortcutCode
          
          Case Config("shortcuts")("insertthing")
               
               'Only in normal mode
               If (submode = ESM_NONE) Then
                    
                    'Insert vertex
                    If MouseInside Then InsertThingHere LastX, LastY
               End If
               
     End Select
End Sub

Private Sub KeypressVertexes(ByVal ShortcutCode As Long)
     
     'Check what key is pressed
     Select Case ShortcutCode
          
          Case Config("shortcuts")("insertvertex")
               
               'Only in normal mode
               If (submode = ESM_NONE) Then
                    
                    'Insert vertex
                    If MouseInside Then
                         InsertVertexHere LastX, LastY
                         picMap.Refresh
                    End If
               End If
               
     End Select
End Sub

Private Sub itmVerticesStitch_Click()
     Dim Indices As Variant
     Dim i As Long
     Dim firstv As Long
     
     'Switch back if in move mode
     If (mode = EM_MOVE) Then itmEditMode_Click CInt(PreviousMode)
     
     'Cancel if in drawing operation
     CancelCurrentOperation
     
     'Check if a selection is made
     If (numselected > 1) Then
          
          'Make undo
          CreateUndo "stitch vertices"
          
          'Stitch em all
          StitchSelectedVertices
          
          'Remove highlight
          RemoveHighlight True
          
          'Redraw map
          RedrawMap False
          
          'Map changed
          mapchanged = True
          mapnodeschanged = True
     End If
End Sub

Public Sub mnuEdit_Click()
     
     'Enable/Disable paste
     itmEdit(3).Enabled = (PasteAvailable And (mapfile <> ""))
     
     'Enable/Disable copy, cut and delete
     itmEdit(2).Enabled = (((numselected > 0) Or (currentselected > -1)) And (mapfile <> ""))
     itmEdit(1).Enabled = (((numselected > 0) Or (currentselected > -1)) And (mapfile <> ""))
     itmEdit(4).Enabled = (((numselected > 0) Or (currentselected > -1)) And (mapfile <> ""))
End Sub

Public Sub mnuFile_Click()
     
     'Remove highlight
     If (mapfile <> "") Then RemoveHighlight True
End Sub

Private Sub mnuHelp_Click()
     'Food
End Sub

Private Sub mnuLines_Click()
     'Restaurant
End Sub

Private Sub mnuPrefabs_Click()
     Dim i As Long
     
     'Check if a previous prefab can be inserted
     If (Trim$(LastPrefab) <> "") Then
          
          'Check if the file can be found
          If (Dir(LastPrefab) <> "") Then
               
               'Enable last prefab
               itmPrefabPrevious.Enabled = True
          Else
               
               'Cant find the file
               itmPrefabPrevious.Enabled = False
          End If
     Else
          
          'No last prefab
          itmPrefabPrevious.Enabled = False
     End If
     
     'Check if a selection is made
     If (numselected > 0) Then
          
          'Enable save selection
          itmPrefabSaveSel.Enabled = True
     Else
          
          'Nothing selected
          itmPrefabSaveSel.Enabled = False
     End If
     
     'Go for all items
     For i = 0 To 4
          
          'Check if a prefab is configured
          If (Trim$(Config("quickprefab" & i + 1)) <> "") And _
             (Dir(Trim$(Config("quickprefab" & i + 1))) <> "") Then
               
               'Enable this item
               itmPrefabQuick(i).Enabled = True
               itmPrefabQuick(i).Caption = MenuNameForShortcut("&" & i + 1 & " - " & Dir(Trim$(Config("quickprefab" & i + 1))), "prefabinsert" & i + 1)
          Else
               
               'Disable item
               itmPrefabQuick(i).Enabled = False
               itmPrefabQuick(i).Caption = MenuNameForShortcut("&" & i + 1 & " - Unused", "prefabinsert" & i + 1)
          End If
     Next i
End Sub

Private Sub mnuScripts_Click()
     'There
End Sub

Private Sub mnuSectors_Click()
     'were
End Sub

Private Sub mnuThings_Click()
     'some
End Sub

Public Sub mnuTools_Click()
     'nasty
End Sub

Private Sub mnuVertices_Click()
     'words here.
End Sub

Public Sub MoveSelectOperation(ByVal x As Single, ByVal y As Single)
     
     'Undraw selection rect
     Render_RectSwitched GrabX, -GrabY, LastSelX, -LastSelY, PAL_NORMAL, 2
     
     'Draw selection rect
     Render_RectSwitched GrabX, -GrabY, x, -y, PAL_MULTISELECTION, 2
     
     'Show changes
     picMap.Refresh
     
     'Keep X and Y
     LastSelX = x
     LastSelY = y
End Sub



Private Sub picCeiling_Click()
     imgCeiling_Click
End Sub

Private Sub picFloor_Click()
     imgFloor_Click
End Sub

Private Sub picMap_DblClick()
     Dim StillDeselectAfterEdit As Boolean
     
     'Check if in 3D Mode
     If (mode = EM_3D) Then
          
          'Redo last mousebutton
          picMap_MouseDown LastMouseButton, LastMouseShift, LastMouseX, LastMouseY
          
     'When not in 3D Mode
     Else
          'Check for selection
          If (selected.Count <= 1) Then StillDeselectAfterEdit = True
          
          'Doubleclick does the same as normal right-click, EDIT
          picMap_MouseDown vbRightButton, 0, LastX, LastY
          
          'Remove selection after editing
          DeselectAfterEdit = StillDeselectAfterEdit
          
          'Do the rightclick
          picMap_MouseUp vbRightButton, 0, LastX, LastY
     End If
End Sub

Private Sub picMap_MouseDown(Button As Integer, Shift As Integer, x As Single, y As Single)
     
     Dim ShortcutCode As Long, thingx As Long, thingy As Long
     Dim angle As Double
     
     EatMouseUp = False
     
     'Keep last position
     tmrMouseOutside.Enabled = True
     MouseInside = True
     
     'Disable autoscroll
     ForceScroll = False
     ChangeAutoscroll True
     
     'Leave immediately if no map is loaded
     If (mapfile = "") Then Exit Sub
     
     'Also leave when a timeout for this event is still active
     'If (tmrMouseTimeout.Enabled = True) Then Exit Sub
     
     'Check if in 3D Mode
     If (mode = EM_3D) Then
          
          'Keep last button and coords
          LastMouseButton = Button
          LastMouseShift = Shift
          LastMouseX = (CSng(x) / CSng(ScreenWidth)) * CSng(VideoParams.BackBufferWidth)
          LastMouseY = (CSng(y) / CSng(ScreenHeight)) * CSng(VideoParams.BackBufferHeight)
          
          'Make the shortcut code from keycode and shift
          Select Case Button
               Case vbLeftButton: ShortcutCode = MOUSE_BUTTON_0 Or (Shift * (2 ^ 16))
               Case vbMiddleButton: ShortcutCode = MOUSE_BUTTON_2 Or (Shift * (2 ^ 16))
               Case vbRightButton: ShortcutCode = MOUSE_BUTTON_1 Or (Shift * (2 ^ 16))
          End Select
          
          'Check how we should process data
          If TextureSelecting Then
               
               'Perform the action associated with the key
               KeydownTextureSelect ShortcutCode
               
          ElseIf DrawingSector Then
          
          
               KeydownDrawSector ShortcutCode
               
          Else
               
               'Perform the action associated with the key
               Keydown3D ShortcutCode
          End If
          
     'When not in 3D Mode
     Else
          
          'Check in what operation we are
          Select Case submode
               
               'No operation
               Case ESM_NONE
                    
                    'Dont start selecting when moving
                    StartSelection = False
                    
                    'Check if an object is highlighted
                    If (currentselected > -1) Then
                         
                         'Check if left button is used (SELECT)
                         If (Button = vbLeftButton) Then
                              
                              'Check if we should remove current selection first
                              If (((Shift And vbCtrlMask) = 0) And ((Shift And vbShiftMask) = 0) And (Config("additiveselect") = vbUnchecked)) Then
                                   RemoveSelection True
                              End If
                              
                              'Select/Deselect highlighted object for the mode we are in
                              Select Case mode
                                   Case EM_VERTICES: SelectCurrentVertex
                                   Case EM_LINES: SelectCurrentLine
                                   Case EM_SECTORS: SelectCurrentSector
                                   Case EM_THINGS: SelectCurrentThing
                              End Select
                              
                              'Ready for editing
                              NoEditing = False
                              
                         'Check if right button is used (EDIT)
                         ElseIf (Button = vbRightButton) And ((Shift And vbCtrlMask) = 0) And ((Shift And vbShiftMask) = 0) Then
                              
                              'Check if clicking outside selection
                              If (selected.Exists(CStr(currentselected)) = False) Then
                                   
                                   'Remove old selection
                                   RemoveSelection True
                                   
                                   'Select/Deselect highlighted object for the mode we are in
                                   Select Case mode
                                        Case EM_VERTICES: SelectCurrentVertex
                                        Case EM_LINES: SelectCurrentLine
                                        Case EM_SECTORS: SelectCurrentSector
                                        Case EM_THINGS: SelectCurrentThing
                                   End Select
                                   
                                   'We'll remove this selection after editing
                                   DeselectAfterEdit = True
                              Else
                                   
                                   'The selection was made manually, keep it
                                   DeselectAfterEdit = False
                              End If
                              
                              'Remove highlight
                              RemoveHighlight
                              
                              'Keep the grabbing postion
                              'When the mouse moves the drag mode will start
                              GrabX = x
                              GrabY = y
                              
                              ' These ones just keep the mouse position: no
                              ' adjustment for offsets. Handy for scrolling.
                              DragStartPxX = (x - ViewLeft) * ViewZoom
                              DragStartPxY = (y - ViewTop) * ViewZoom
                              
                              'If not dragging, the editing dialog will be shown on mouse release
                              NoEditing = False
                         End If
                    Else
                         
                         'Keep the grabbing postion
                         'When the mouse moves the drag mode will start
                         GrabX = x
                         GrabY = y
                         
                         'Check if left button is used
                         If (Button = vbLeftButton) Then
                              
                              'Remove highlight
                              RemoveHighlight
                              
                              'Check mode
                              If (mode = EM_MOVE) Then
                                   
                                   'Change mousecursor
                                   Set picMap.MouseIcon = imgCursor(2).Picture
                                   submode = ESM_MOVING
                              Else
                                   
                                   'Start selecting when moving
                                   StartSelection = True
                              End If
                         ElseIf (Button = vbRightButton) Then
                              
                              'Check if no selection made
                              If (numselected = 0) Then
                                   
                                   'Check what to insert
                                   Select Case mode
                                        Case EM_VERTICES
                                             
                                             'Insert vertex
                                             InsertVertexHere x, y
                                             
                                             'Highlight the vertex
                                             ShowHighlight x, y
                                             
                                             'Select the vertex
                                             SelectCurrentVertex
                                             
                                             'We'll remove this selection after editing
                                             DeselectAfterEdit = True
                                             
                                             'Do not edit the vertex on mouse release
                                             NoEditing = True
                                             
                                        Case EM_THINGS
                                             
                                             'Insert thing
                                             InsertThingHere x, y
                                             
                                             'Highlight the thing
                                             ShowHighlight x, y
                                             
                                             'Check if we should select the thing for dragging
                                             If (Config("newthingdialog") = vbUnchecked) Then
                                                  
                                                  'Select the thing
                                                  SelectCurrentThing
                                                  
                                                  'We'll remove this selection after editing
                                                  DeselectAfterEdit = True
                                             End If
                                             
                                             'Do not edit the thing on mouse release
                                             NoEditing = True
                                   End Select
                              End If
                         End If
                    End If
                    
               'Paste operation
               Case ESM_PASTING
                    
                    'End of dragging operation
                    EndDragOperation
                    
                    'Remove selection
                    RemoveSelection True
                    RemoveHighlight True
                    
                    'Do a mousemove to update the highlight
                    picMap_MouseMove vbNormal, Shift, x, y
                    
               'Drawing operation
               Case ESM_DRAWING
                    
                    ClearOldDrawingLine
                    
                    
                    'Only draw with left mousebutton
                    If (Button = vbLeftButton) Then
                         
                         'Draw here - DCK mode if Alt was depressed for first vertex.
                         DrawVertexHere x, y, , , (ShiftMaskAtDrawStart And vbAltMask)
                    End If
                    
                    'Show changes
                    picMap.Refresh
                    
               ' "Rubber-band" rotation thing.
               Case ESM_ROTATING
                    
                    thingx = things(selected.Items(0)).x
                    thingy = things(selected.Items(0)).y
                    
                    If x <> thingx Then
                         angle = (180 / pi) * Atn((-y - thingy) / (x - thingx))
                    Else
                         angle = 90
                    End If
                    
                    If angle < 0 Then angle = angle + 180        ' Q2 or Q4.
                    If -y < thingy Then angle = angle + 180      ' Q3 or Q4.
                    
                    things(selected.Items(0)).angle = CLng(angle)
                    
                    submode = ESM_NONE
                    mapchanged = True
                    UpdateThingImageColor selected.Items(0)
                    RedrawMap
                    
          End Select
          
          'Keep last position
          LastX = x
          LastY = y
          
          'Set a timeout for the next mousemove event
          tmrMouseTimeout.Enabled = True
     End If
     
End Sub

Private Sub picMap_MouseMove(Button As Integer, Shift As Integer, x As Single, y As Single)
     
     Dim distance As Long
     
     'Keep last position
     tmrMouseOutside.Enabled = True
     MouseInside = True
     
     'Leave immediately if no map is loaded
     If (mapfile = "") Then Exit Sub
     
     'Check if in 3D Mode
     If (mode = EM_3D) Then
          
          'Keep last coords
          LastMouseX = (CSng(x) / CSng(ScreenWidth)) * CSng(VideoParams.BackBufferWidth)
          LastMouseY = (CSng(y) / CSng(ScreenHeight)) * CSng(VideoParams.BackBufferHeight)
          
     'When not in 3D Mode
     Else
          
          'Also leave when a timeout for this event is still active
          If (tmrMouseTimeout.Enabled = True) Then Exit Sub
          
          'Update cursor position in statusbar
          stbStatus.Panels("mousex").Text = "X " & CLng(x)
          stbStatus.Panels("mousey").Text = "Y " & -CLng(y)
          
          'Check in what operation we are
          Select Case submode
               
               'No operation
               Case ESM_NONE
                    
                    'Check if no mousebutton is held
                    If (Button = vbNormal) Then
                         
                         'Show highlight
                         ShowHighlight x, y
                         
                         If ForceScroll Then
                              tmrAutoScroll.Enabled = True
                         End If
                         
                    'Check if right mousebutton is hold and a selection is made
                    ElseIf ((numselected > 0) And (Button = vbRightButton)) Then
                         
                         'Check if moved enough pixels to start drag
                         If ((Abs(x * ViewZoom - GrabX * ViewZoom) >= Config("dragpixels")) Or _
                             (Abs(y * ViewZoom - GrabY * ViewZoom) >= Config("dragpixels"))) Then
                              
                              'Start drag operation, but not if pressing shift in thing mode
                              If (Shift And vbShiftMask) <> vbShiftMask Or mode <> EM_THINGS Then
                                   StartDragOperation x, y
                              End If
                         End If
                         
                    'Check if left mouse button is hold and no highlight
                    ElseIf ((Button = vbLeftButton) And (currentselected = -1) And StartSelection) Then
                         
                         'Remove highlight
                         RemoveHighlight True
                         
                         'Start multiselect mode
                         StartSelectOperation x, y
                    End If
                    
                    If (Button = vbRightButton) And (Shift And 1 = 1) And (mode = EM_THINGS) Then
                         ' Get thing-painting.
                         NearestThing SnappedToGridX(x), SnappedToGridY(y), things(0), numthings, distance, filterthings, filtersettings
                         
                         ' Don't create multiple things in the same place.
                         If distance <> 0 Then
                         
                              InsertThingHere SnappedToGridX(x), SnappedToGridY(y)
                              
                         End If
                         
                         ShowHighlight x, y
                    End If
               
               'Drag operation
               Case ESM_DRAGGING
                    
                    'Dragging from right mousebutton
                    If (Button = vbRightButton) Then
                         
                         'Draw selection
                         DragSelection Shift, x, y
                    End If
                    
                    'Check if we should autoscroll
                    If Config("autoscroll") Then tmrAutoScroll.Enabled = True
                    
               'Paste operation
               Case ESM_PASTING
                    
                    'Draw selection
                    DragSelection Shift, x, y
                    
                    'Check if we should autoscroll
                    If Config("autoscroll") Then tmrAutoScroll.Enabled = True
                    
               'Select operation
               Case ESM_SELECTING
                    
                    'Selection from left mousebutton
                    If (Button = vbLeftButton) Then
                         
                         'Draw selection
                         MoveSelectOperation x, y
                    End If
                    
                    'Check if we should autoscroll
                    If Config("autoscroll") Then tmrAutoScroll.Enabled = True
                    
               'Drawing operation
               Case ESM_DRAWING
                    
                    'Check if no button is hold
                    If (Button = vbNormal) Then
                         
                         ClearOldDrawingLine
                         RenderNewDrawingLine x, y
                         
                         'Show changes
                         picMap.Refresh
                    End If
                    
                    'Check if we should autoscroll
                    If Config("autoscroll") Then tmrAutoScroll.Enabled = True
                    
               'Moving map
               Case ESM_MOVING
                    
                    'Check if not the right button hold
                    If (Button <> vbRightButton) Then
                         
                         'Drag the map
                         ViewLeft = ViewLeft - (x - LastX)
                         ViewTop = ViewTop - (y - LastY)
                         x = x - (x - LastX)
                         y = y - (y - LastY)
                         
                         'Change viewport
                         ChangeView ViewLeft, ViewTop, ViewZoom
                         
                         'Redraw map
                         RedrawMap
                    End If
                    
               ' We do rotating AFTER setting LastX, LastY!
                    
          End Select
          
          'Keep last position
          LastX = x
          LastY = y
          
          If submode = ESM_ROTATING Then
               ' Rotating - draw the line.
               RedrawMap
          End If
          
          'Set autoscroll
          ChangeAutoscroll False
          
          'Set a timeout for the next mousemove event
          tmrMouseTimeout.Enabled = True
     End If
End Sub

Private Sub picMap_MouseUp(Button As Integer, Shift As Integer, x As Single, y As Single)
     Dim ShortcutCode As Long
     
     ' Stupid double-click in dialogue work-around.
     If EatMouseUp Then
          EatMouseUp = False
          Exit Sub
     End If
     
     'Keep last position
     tmrMouseOutside.Enabled = True
     MouseInside = True
     
     'Leave immediately if no map is loaded
     If (mapfile = "") Then Exit Sub
     
     'Also leave when a timeout for this event is still active
     'If (tmrMouseTimeout.Enabled = True) Then Exit Sub
     
     'Check if in 3D Mode
     If (mode = EM_3D) Then
          
          'Keep last coords
          LastMouseX = (CSng(x) / CSng(ScreenWidth)) * CSng(VideoParams.BackBufferWidth)
          LastMouseY = (CSng(y) / CSng(ScreenHeight)) * CSng(VideoParams.BackBufferHeight)
          
          'Make the shortcut code from keycode and shift
          Select Case Button
               Case vbLeftButton: ShortcutCode = MOUSE_BUTTON_0 Or (Shift * (2 ^ 16))
               Case vbMiddleButton: ShortcutCode = MOUSE_BUTTON_2 Or (Shift * (2 ^ 16))
               Case vbRightButton: ShortcutCode = MOUSE_BUTTON_1 Or (Shift * (2 ^ 16))
          End Select
          
          'Check how we should process data
          If Not TextureSelecting Then
               
               'Perform the action associated with the key
               Keyrelease3D ShortcutCode
          End If
          
     'When not in 3D Mode
     Else
          
          'Check in what operation we are
          Select Case submode
               
               'No operation
               Case ESM_NONE
                    
                    'Show highlight
                    ShowHighlight x, y
                    
                    ' If nothing highlighted:
                    If (currentselected = -1) Then
                         
                         'Deselect if preferred
                         If (Config("nothingdeselects") = vbChecked) Then RemoveSelection True
                    End If
                    
                    'Check if no object was highlighted, or if we're holding a modifer.
                    If (currentselected = -1) Or ((Shift And vbShiftMask) <> 0) Or ((Shift And vbCtrlMask) <> 0) Then
                         
                         'Check if right mousebutton used
                         If (Button = vbRightButton) Then
                              
                              'Check if no selection made
                              
                                   
                              'Check what to insert
                              Select Case mode
                              Case EM_LINES
                                   If ((Shift And vbShiftMask) = 0) And _
                                      ((Shift And vbCtrlMask) = 0) Then
                                        
                                        If (numselected = 0) Then
                                             'Start drawing
                                             StartDrawOperation
                                             DrawVertexHere x, y
                                        End If
                                   Else
                                        
                                        'Insert sector
                                        CreateSectorHere x, y
                                   End If
                                   
                              Case EM_SECTORS
                                   If ((Shift And vbShiftMask) = 0) And _
                                      ((Shift And vbCtrlMask) = 0) Then
                                        
                                        If (numselected = 0) Then
                                             'Start drawing
                                             StartDrawOperation
                                             DrawVertexHere x, y
                                        End If
                                   Else
                                        
                                        'Insert sector
                                        CreateSectorHere x, y
                                   End If
                              End Select
                              
                         End If
                    
                    'Check if a selection is made and we will edit it
                    ElseIf ((numselected > 0) And (Button = vbRightButton)) Then
                         
                         'Check if right button is used and allowed to edit
                         If (Button = vbRightButton) Then
                              
                              'Check if allowed to edit
                              If (NoEditing = False) Then
                                   
                                   'Remove any highlight
                                   RemoveHighlight True
                                   
                                   'Check what is gonna be edited
                                   Select Case mode
                                        Case EM_VERTICES:   'Just drag it bitch!
                                        Case EM_LINES: frmLinedef.Show 1, Me
                                        Case EM_SECTORS: frmSector.Show 1, Me
                                        Case EM_THINGS: frmThing.Show 1, Me
                                   End Select
                                   
                                   'Check if we should deselect
                                   If (DeselectAfterEdit) Then
                                        
                                        'Select/Deselect highlighted object for the mode we are in
                                        Select Case mode
                                             Case EM_VERTICES: RemoveVertexSelection
                                             Case EM_LINES: RemoveLinesSelection
                                             Case EM_SECTORS: RemoveSectorsSelection
                                             Case EM_THINGS: RemoveThingsSelection
                                        End Select
                                   End If
                                   
                                   'Redraw the map
                                   RedrawMap True
                              Else
                                   
                                   'Remove selection and highlight
                                   RemoveSelection False
                                   RemoveHighlight True
                                   
                                   'Show highlight
                                   ShowHighlight x, y
                              End If
                         End If
                    Else
                         
                         'Show highlight
                         ShowHighlight x, y
                    End If
                    
               'Drag operation
               Case ESM_DRAGGING
                    
                    'Dragging from right mousebutton
                    If (Button = vbRightButton) Then
                         
                         'End of dragging operation
                         EndDragOperation
                         
                         'Do a mousemove to update the highlight
                         picMap_MouseMove vbNormal, Shift, x, y
                    End If
                    
               'Select operation
               Case ESM_SELECTING
                    
                    'Selection from left mousebutton
                    If (Button = vbLeftButton) Then
                         
                         'End of selecting operation
                         EndSelectOperation Shift, x, y
                         
                         'No more select
                         StartSelection = False
                         
                         'Do a mousemove to update the highlight
                         picMap_MouseMove vbNormal, Shift, x, y
                    End If
               
               'Drawing operation
               Case ESM_DRAWING
                    
                    'Check if right mousebutton used (END DRAWING)
                    If (Button = vbRightButton) Then
                         
                         'End drawing now
                         EndDrawOperation False
                         
                    'Otherwise check if left mousebutton used
                    ElseIf (Button = vbLeftButton) Then
                         
                         'Check if allowed to change
                         If picMap.Enabled Then
                              
                              RenderNewDrawingLine x, y
                              
                              'Show changes
                              picMap.Refresh
                         End If
                    End If
                    
               'Moving map
               Case ESM_MOVING
                    
                    'No longer moving
                    Set picMap.MouseIcon = imgCursor(1).Picture
                    submode = ESM_NONE
                    
          End Select
          
          'Keep last position
          LastX = x
          LastY = y
          
          'Set a timeout for the next mousemove event
          tmrMouseTimeout.Enabled = True
     End If
End Sub

Private Sub Place3DModeStart(ByVal x As Long, ByVal y As Long)
     Dim t_found As Boolean
     Dim t As Long
     
     'Check if the position thing is within bounds
     If (PositionThing >= 0) And (PositionThing < numthings) Then
          
          'Check if the position thing is correct
          If (things(PositionThing).thing = mapconfig("start3dmode")) Then t_found = True
     End If
     
     'If no thing could be found, find a new one
     If (t_found = False) Then
          
          'Go for all things to find another positioning thing
          For t = 0 To (numthings - 1)
               
               'Check if this is a 3D start position
               If (things(t).thing = mapconfig("start3dmode")) Then
                    
                    'Use this
                    PositionThing = t
                    
                    'Found one
                    t_found = True
                    Exit For
               End If
          Next t
     End If
     
     'Check if a position thing could be found
     If t_found Then
          
          'Move the thing
          things(PositionThing).x = x
          things(PositionThing).y = -y
     Else
          
          'Create 3D Start thing
          PositionThing = CreateThing
          
          'Set tis properties
          With things(PositionThing)
               .angle = 0
               .arg0 = 0
               .arg1 = 0
               .arg2 = 0
               .arg3 = 0
               .arg4 = 0
               .effect = 0
               .Flags = 0
               .selected = 0
               .Tag = 0
               .thing = mapconfig("start3dmode")
               .x = x
               .y = -y
               .Z = 0
          End With
          
          'Update image
          UpdateThingImageColor PositionThing
          UpdateThingSize PositionThing
          UpdateThingCategory PositionThing
     End If
     
     'Apply position
     ApplyPositionFromThing PositionThing
End Sub

Public Sub RemoveHighlight(Optional ByVal RemovePanels As Boolean = True)
     Dim ld As Long, ldfound As Long
     
     'Check the viewing mode
     Select Case mode
          
          Case EM_VERTICES
               
               'Render the last current selected to normal
               If (currentselected > -1) Then Render_AllVertices vertexes(0), currentselected, currentselected, vertexsize
               
               'Check if we should remove the info
               If (RemovePanels) Then HideVertexInfo
               
               
          Case EM_LINES
               
               'Check if a previous linedef was selected
               If (currentselected > -1) Then
                    
                    'Render the last selected linedef to normal (also vertices, those have been overdrawn)
                    Render_AllLinedefs vertexes(0), linedefs(0), VarPtr(sectors(0)), VarPtr(sidedefs(0)), currentselected, currentselected, submode, indicatorsize
                    If (Config("mode1vertices")) Then
                         Render_AllVertices vertexes(0), linedefs(currentselected).V1, linedefs(currentselected).V1, vertexsize
                         Render_AllVertices vertexes(0), linedefs(currentselected).V2, linedefs(currentselected).V2, vertexsize
                    End If
                    
                    'Render the last tagged sectors if line had a tag
                    If (linedefs(currentselected).Tag > 0) Then Render_TaggedSectors vertexes(0), linedefs(0), VarPtr(sidedefs(0)), VarPtr(sectors(0)), numsectors, numlinedefs, linedefs(currentselected).Tag, 0, indicatorsize, Config("mode1vertices"), vertexsize
               End If
               
               'Check if we should remove the info
               ' Not if we're using the new tex-change thing.
               'If (RemovePanels) Then HideLinedefInfo
               
               ' Calling SSLI here again (as well as in ChangeLinedefsHighlight) is a
               ' little messy, but probably the best we can manage.
               If (numselected = 0) Then HideLinedefInfo Else ShowSelectedLinedefInfo
               
               
          Case EM_SECTORS
               
               'Check if a previous sector was selected
               If (currentselected > -1) Then
                    
                    'Go for all linedefs
                    For ld = 0 To (numlinedefs - 1)
                         
                         'Check if one of the sidedefs belong to this sector
                         ldfound = 0
                         If (linedefs(ld).s1 > -1) Then If (sidedefs(linedefs(ld).s1).sector = currentselected) Then ldfound = 1
                         If (linedefs(ld).s2 > -1) Then If (sidedefs(linedefs(ld).s2).sector = currentselected) Then ldfound = 1
                         
                         If (ldfound) Then
                              
                              'Render this linedef to normal (also vertices, those have been overdrawn)
                              Render_AllLinedefs vertexes(0), linedefs(0), VarPtr(sectors(0)), VarPtr(sidedefs(0)), ld, ld, submode, indicatorsize
                              If (Config("mode2vertices")) Then
                                   Render_AllVertices vertexes(0), linedefs(ld).V1, linedefs(ld).V1, vertexsize
                                   Render_AllVertices vertexes(0), linedefs(ld).V2, linedefs(ld).V2, vertexsize
                              End If
                         End If
                    Next ld
                    
                    'Render the last tagged linedefs if sector had a tag
                    If (sectors(currentselected).Tag > 0) Then Render_TaggedLinedefs vertexes(0), linedefs(0), VarPtr(sectors(0)), VarPtr(sidedefs(0)), numlinedefs, sectors(currentselected).Tag, 1, 0, indicatorsize, 0, vertexsize
               End If
               
               'Check if we should remove the info
               ' Not if we're using the new tex-change thing.
               'If (RemovePanels) Then HideSectorInfo
               
               ' Calling SSSI here again (as well as in ChangeSectorsHighlight) is a
               ' little messy, but probably the best we can manage.
               If (numselected = 0) Then HideSectorInfo Else ShowSelectedSectorInfo
               
          Case EM_THINGS
               
               'Render the last current selected to normal
               If (currentselected > -1) Then
                    Render_AllThings things(0), currentselected, currentselected, ThingBitmapData(0), frmMain.picThings(thingsize).width, frmMain.picThings(thingsize).Height, Val(Config("allthingsrects")), ViewZoom, filterthings, filtersettings
                    If Config("thingrects") Then Render_BoxSwitched things(currentselected).x, things(currentselected).y, GetThingWidth(things(currentselected).thing) * ViewZoom, PAL_NORMAL, (Config("thingrects") - 1), PAL_NORMAL
               End If
               
               'Check if we should remove the info
               If (RemovePanels) Then HideThingInfo
               
               
          Case EM_3D
               
               'Check if we should remove the info
               If (RemovePanels) Then
                    HideLinedefInfo
                    HideSectorInfo
               End If
               
     End Select
     
     'Show map changes
     picMap.Refresh
     
     'No more highlight
     picMap.ToolTipText = ""
     currentselected = -1
End Sub

Private Sub RenderDrawingLine(ByVal Pal1 As ENUM_PALETTES, ByVal Pal2 As ENUM_PALETTES, ByVal x As Long, ByVal y As Long, Optional ByVal x1 As Long = -40000, Optional ByVal y1 As Long = -40000, Optional ByVal Simple As Boolean = False)
     ' Dim x1 As Long, y1 As Long - Now passed in, optionally.
     Dim x2 As Long, y2 As Long
     Dim lx As Long, ly As Long
     Dim Length As Long
     Dim ShouldSnap45 As Boolean
     Dim UnrenderVertex1 As Boolean
     Dim origx As Long, origy As Long
     
     If Not Simple Then
          DrawingSnap x, y, CurrentShiftMask
          
          ' We do x1 and y1 ourselves.
          If ((Not ShouldSnap45) And (snapmode Xor ((CurrentShiftMask And vbShiftMask) = vbShiftMask))) Or _
             (ShouldSnap45 And ((CurrentShiftMask And vbShiftMask) = vbShiftMask)) Then
               x1 = SnappedToGridX(x1)
               y1 = SnappedToGridY(y1)
          End If
     End If
     
     'Check if a vertex is already drawn
     If (numselected > 0) Then
          
          'Get line coordinates
          ' We remove the origin vertex only when we use its co-ords.
          UnrenderVertex1 = False
          If x1 < -32767 Then
               x1 = vertexes(selected.Items(selected.Count - 1)).x
               UnrenderVertex1 = True
          End If
          
          If y1 < -32767 Then
               y1 = vertexes(selected.Items(selected.Count - 1)).y
               UnrenderVertex1 = True
          Else
               y1 = -y1
          End If
          
          x2 = x
          y2 = -y
          
          'Line distances
          lx = x2 - x1
          ly = y2 - y1
          
          'Length of line
          Length = Int(Sqr(lx * lx + ly * ly))
          
          'Draw line switched
          Render_LinedefLineSwitched x1, y1, x2, y2, Pal1, indicatorsize
          
          'Draw previous vertex normal
          If UnrenderVertex1 Then Render_BoxSwitched x1, y1, vertexsize, PAL_NORMAL, 1, PAL_NORMAL
          
          'Draw line length number
          Render_NumberSwitched Length, x1 + lx * 0.5, y1 + ly * 0.5, NumbersBitmapData(0), frmMain.picNumbers.width, frmMain.picNumbers.Height, frmMain.picNumbers.width / 10, frmMain.picNumbers.Height, Pal1, Pal2
     End If
     
     'Draw target vertex switched
     Render_BoxSwitched x, -y, vertexsize, Pal1, 1, Pal1
End Sub

Private Sub SetupNewLinedefs(ByVal sc1 As Long, ByVal sc2 As Long, ByVal CopyFromSidedef As Long)
     Dim ld As Long
     Dim i As Long
     Dim Indices As Variant
     
     'Go for all selected linedefs
     Indices = selected.Items
     For i = 0 To (numselected - 1)
          
          'Get linedef
          ld = Indices(i)
          
          'Check if we should add Sidedef 1
          If (sc1 > -1) Then
               
               'Create sidedef
               linedefs(ld).s1 = CreateSidedef
               
               'Set sidedef properties
               With sidedefs(linedefs(ld).s1)
                    
                    'Reference to linedef
                    .linedef = ld
                    
                    'Reference to sector
                    .sector = sc1
                    
                    'Standard offsets
                    .tx = 0
                    .ty = 0
                    
                    'Check if lower can be copied
                    If (CopyFromSidedef > -1) Then
                         
                         'Check if source has a lower
                         If (IsTextureName(sidedefs(CopyFromSidedef).Lower)) Then
                              
                              'Copy from source
                              .Lower = sidedefs(CopyFromSidedef).Lower
                         Else
                              
                              'Check if source has a middle
                              If (IsTextureName(sidedefs(CopyFromSidedef).Middle)) Then
                                   
                                   'Copy from source middle
                                   .Lower = sidedefs(CopyFromSidedef).Middle
                              Else
                                   
                                   'Make default
                                   .Lower = "-"
                              End If
                         End If
                    Else
                         
                         'Make default
                         .Lower = "-"
                    End If
                    
                    'Check if a middle texture is needed
                    If (sc2 = -1) Then
                         
                         'Check if middle can be copied
                         If (CopyFromSidedef > -1) Then
                              
                              'Check if source has a middle
                              If (IsTextureName(sidedefs(CopyFromSidedef).Middle)) Then
                                   
                                   'Copy from source
                                   .Middle = sidedefs(CopyFromSidedef).Middle
                              Else
                                   
                                   'Make default
                                   .Middle = "-"
                              End If
                         Else
                              
                              'Make default
                              .Middle = "-"
                         End If
                    Else
                         
                         'No middle texture
                         .Middle = "-"
                    End If
                    
                    'Check if upper can be copied
                    If (CopyFromSidedef > -1) Then
                         
                         'Check if source has a upper
                         If (IsTextureName(sidedefs(CopyFromSidedef).Upper)) Then
                              
                              'Copy from source
                              .Upper = sidedefs(CopyFromSidedef).Upper
                         Else
                              
                              'Check if source has a middle
                              If (IsTextureName(sidedefs(CopyFromSidedef).Middle)) Then
                                   
                                   'Copy from source middle
                                   .Upper = sidedefs(CopyFromSidedef).Middle
                              Else
                                   
                                   'Make default
                                   .Upper = "-"
                              End If
                         End If
                    Else
                         
                         'Make default
                         .Upper = "-"
                    End If
               End With
          End If
          
          'Check if we should add Sidedef 2
          If (sc2 > -1) Then
               
               'Create sidedef
               linedefs(ld).s2 = CreateSidedef
               
               'Set sidedef properties
               With sidedefs(linedefs(ld).s2)
                    
                    'Reference to linedef
                    .linedef = ld
                    
                    'Reference to sector
                    .sector = sc2
                    
                    'Standard offsets
                    .tx = 0
                    .ty = 0
                    
                    'Check if lower can be copied
                    If (CopyFromSidedef > -1) Then
                         
                         'Check if source has a lower
                         If (IsTextureName(sidedefs(CopyFromSidedef).Lower)) Then
                              
                              'Copy from source
                              .Lower = sidedefs(CopyFromSidedef).Lower
                         Else
                              
                              'Check if source has a middle
                              If (IsTextureName(sidedefs(CopyFromSidedef).Middle)) Then
                                   
                                   'Copy from source middle
                                   .Lower = sidedefs(CopyFromSidedef).Middle
                              Else
                                   
                                   'Make default
                                   .Lower = "-"
                              End If
                         End If
                    Else
                         
                         'Make default
                         .Lower = "-"
                    End If
                    
                    'Check if a middle texture is needed
                    If (sc1 = -1) Then
                         
                         'Check if middle can be copied
                         If (CopyFromSidedef > -1) Then
                              
                              'Check if source has a middle
                              If (IsTextureName(sidedefs(CopyFromSidedef).Middle)) Then
                                   
                                   'Copy from source
                                   .Middle = sidedefs(CopyFromSidedef).Middle
                              Else
                                   
                                   'Make default
                                   .Middle = "-"
                              End If
                         Else
                              
                              'Make default
                              .Middle = "-"
                         End If
                    Else
                         
                         'No middle texture
                         .Middle = "-"
                    End If
                    
                    'Check if upper can be copied
                    If (CopyFromSidedef > -1) Then
                         
                         'Check if source has a upper
                         If (IsTextureName(sidedefs(CopyFromSidedef).Upper)) Then
                              
                              'Copy from source
                              .Upper = sidedefs(CopyFromSidedef).Upper
                         Else
                              
                              'Check if source has a middle
                              If (IsTextureName(sidedefs(CopyFromSidedef).Middle)) Then
                                   
                                   'Copy from source middle
                                   .Upper = sidedefs(CopyFromSidedef).Middle
                              Else
                                   
                                   'Make default
                                   .Upper = "-"
                              End If
                         End If
                    Else
                         
                         'Make default
                         .Upper = "-"
                    End If
               End With
               
               'Line is double sided
               linedefs(ld).Flags = linedefs(ld).Flags Or LDF_TWOSIDED
               linedefs(ld).Flags = linedefs(ld).Flags And Not LDF_IMPASSIBLE
          Else
               
               'Line is single sided
               linedefs(ld).Flags = linedefs(ld).Flags And Not LDF_TWOSIDED
               linedefs(ld).Flags = linedefs(ld).Flags Or LDF_IMPASSIBLE
          End If
     Next i
End Sub

Public Sub ShowConfiguration(ByVal showtab As Long)
     Dim OldIWAD As String
     Dim OldMixResources As Long
     
     'Leave when map edit is disabled
     If (picMap.Enabled = False) Then Exit Sub
     
     'Ensure the splash dialog is gone
     Unload frmSplash: Set frmSplash = Nothing
     
     'Cancel if in drawing operation
     CancelCurrentOperation
     
     'Keep the old settings
     OldIWAD = GetCurrentIWADFile
     OldMixResources = Val(Config("mixresources"))
     
     'Load dialog
     Load frmOptions
     
     'Select first tab
     frmOptions.tbsOptions.Tabs(showtab).selected = True
     
     'Show dialog
     frmOptions.Show 1, Me
     
     'Create rendering palette
     CreateRendererPalette
     
     'Only re-initialize renderer when a map is loaded
     If (mapfile <> "") Then
          
          'Change mousepointer
          Screen.MousePointer = vbHourglass
          
          'Show status dialog
          frmStatus.Show 0, frmMain
          frmStatus.Refresh
          frmMain.SetFocus
          frmMain.Refresh
          
          'Load the error log
          ErrorLog_Load
          
          'Reload textures and flat when needed
          If (OldIWAD <> GetCurrentIWADFile) Or (OldMixResources <> Val(Config("mixresources"))) Then
               
               'Close previous IWAD
               IWAD.CloseFile
               
               'Open associated IWAD
               OpenIWADFile
               
               'Precache resources
               MapLoadResources
          End If
          
          'Initialize the map screen
          InitializeMapRenderer frmMain.picMap
          
          'Unload status dialog
          Unload frmStatus: Set frmSplash = Nothing
          
          'Reset mousepointer
          Screen.MousePointer = vbDefault
          
          'Show the errors and warnings dialog
          ErrorLog_DisplayAndFlush
          
          'Set the viewport
          ChangeView ViewLeft, ViewTop, ViewZoom
     End If
     
     'Apply configuration on Interface
     ApplyInterfaceConfiguration
     
     ' Recalc FOF tags (its enabled state might have changed).
     UpdateFOFTagCache
     If IsLoaded(frmTags) Then frmTags.RefreshTagList
     
     'Update the controls
     Form_Resize
     
     'Update shortcuts
     UpdateMenuShortcuts
End Sub

Public Sub ShowHighlight(ByVal x As Long, ByVal y As Long, Optional ByVal ForceUpdate As Boolean = False)
     
     'Cant show the highlight when no mouse inside
     If (MouseInside = False) Then Exit Sub
     
     'Highlight object for the mode we are in
     Select Case mode
          Case EM_VERTICES: ChangeVertexHighlight x, y, ForceUpdate
          Case EM_LINES: ChangeLinesHighlight x, y, ForceUpdate
          Case EM_SECTORS: ChangeSectorsHighlight x, y, ForceUpdate
          Case EM_THINGS: ChangeThingsHighlight x, y, ForceUpdate
     End Select
End Sub

Private Sub StartDragOperation(ByVal x As Single, ByVal y As Single)
     Dim distance As Long
     
     'Make undo
     Select Case mode
          Case EM_THINGS: CreateUndo "thing drag"
          Case EM_VERTICES: CreateUndo "vertex drag"
          Case EM_LINES: CreateUndo "linedef drag"
          Case EM_SECTORS: CreateUndo "sector drag"
     End Select
     
     'never edit during or after drag
     NoEditing = True
     
     'Drag operation only drags vertices and things
     'so select vertices when dragging lines or sectors
     If ((mode = EM_LINES) Or (mode = EM_SECTORS)) Then
          
          'Make dragging selection
          Set dragselected = SelectVerticesFromSelection
          dragnumselected = dragselected.Count
     Else
          
          'Dragging same as selection
          Set dragselected = selected
          dragnumselected = numselected
     End If
     
     'Grab the nearest object
     If (mode = EM_THINGS) Then
          
          'Grab the nearest thing
          grabobject = NearestSelectedThing(x, y, things(0), numthings, distance)
          
          'Calculate offset between mouse and grab object
          If grabobject > -1 Then      ' Really shouldn't happen. TODO: Explore.
               GrabX = GrabX - things(grabobject).x
               GrabY = -GrabY - things(grabobject).y
          End If
     Else
          
          'Grab the nearest vertex
          grabobject = NearestSelectedVertex(x, y, vertexes(0), numvertexes, distance)
          
          'Calculate offset between mouse and grab object
          If grabobject > -1 Then      ' Really shouldn't happen. TODO: Explore.
               GrabX = GrabX - vertexes(grabobject).x
               GrabY = -GrabY - vertexes(grabobject).y
          End If
     End If
     
     'Find and keep lines that will be changed
     If (mode <> EM_THINGS) Then FindChangingLines False, True
     
     'Start drag operation
     currentselected = -1
     submode = ESM_DRAGGING
     
     'Redraw map
     RedrawMap
End Sub

Public Sub StartDrawOperation(Optional ByVal RefreshMap As Boolean = True)
     
     'Make undo
     CreateUndo "draw"
     
     'Deselect all
     RemoveSelection False
     RemoveHighlight True
     
     'Start draw operation
     currentselected = -1
     submode = ESM_DRAWING
     
     'No changed lines yet
     ReDim changedlines(0)
     numchangedlines = 0
     ReDim DrawingCoords(0)
     NumDrawingCoords = 0
     
     ' Save shift mask.
     ShiftMaskAtDrawStart = CurrentShiftMask
     
     'Map has changed
     mapchanged = True
     mapnodeschanged = True
     
     'Redraw map
     If (RefreshMap) Then RedrawMap
End Sub

Private Sub StartPasteOperation(ByVal x As Single, ByVal y As Single)
     Dim distance As Long
     
     'Set hourglass mousepointer
     Screen.MousePointer = vbArrowHourglass
     
     'Drag operation only drags vertices and things
     'so select vertices when dragging lines or sectors
     If ((mode = EM_LINES) Or (mode = EM_SECTORS)) Then
          
          'Make dragging selection
          Set dragselected = SelectVerticesFromSelection
          dragnumselected = dragselected.Count
     Else
          
          'Dragging same as selection
          Set dragselected = selected
          dragnumselected = numselected
     End If
     
     'Grab the nearest object
     If (mode = EM_THINGS) Then
          
          'Grab the nearest thing
          grabobject = NearestSelectedThing(x, y, things(0), numthings, distance)
          
          'Check if anything
          If (grabobject > -1) Then
               
               'Calculate offset between mouse and grab object
               GrabX = GrabX - things(grabobject).x
               GrabY = -GrabY - things(grabobject).y
          End If
     Else
          
          'Grab the nearest vertex
          grabobject = NearestSelectedVertex(x, y, vertexes(0), numvertexes, distance)
          
          'Check if anything
          If (grabobject > -1) Then
               
               'Calculate offset between mouse and grab object
               GrabX = GrabX - vertexes(grabobject).x
               GrabY = -GrabY - vertexes(grabobject).y
          End If
     End If
     
     'Set normal mousepointer
     Screen.MousePointer = vbDefault
     
     'Start paste operation
     currentselected = -1
     submode = ESM_PASTING
End Sub

Public Sub StartSelectOperation(ByVal x As Single, ByVal y As Single)
     
     'Start drag operation
     currentselected = -1
     submode = ESM_SELECTING
     
     'Redraw map
     RedrawMap
     
     'Draw initial rect
     Render_RectSwitched GrabX, -GrabY, x, -y, PAL_MULTISELECTION, 2
     
     'Show changes
     picMap.Refresh
     
     'Keep X and Y
     LastSelX = x
     LastSelY = y
End Sub

Private Sub picMap_Paint()
     On Error Resume Next
     
     'Check if running 3D Mode
     If (Running3D) And (Me.visible = True) Then
          
          'Check if windowed
          If (Val(Config("windowedvideo")) <> 0) And (Running3D = True) Then
               
               'Run a single frame to refresh
               RunSingleFrame False, True
          End If
     End If
End Sub


Private Sub picS1Lower_Click()
     imgS1Lower_Click
End Sub

Private Sub picS1Middle_Click()
     imgS1Middle_Click
End Sub

Private Sub picS1Upper_Click()
     imgS1Upper_Click
End Sub

Private Sub picS2Lower_Click()
     imgS2Lower_Click
End Sub

Private Sub picS2Middle_Click()
     imgS2Middle_Click
End Sub

Private Sub picS2Upper_Click()
     imgS2Upper_Click
End Sub

Private Sub picSCeiling_Click()
     imgSCeiling_Click
End Sub

Private Sub picSFloor_Click()
     imgSFloor_Click
End Sub

Private Sub picSS1Lower_Click()
     imgSS1Lower_Click
End Sub

Private Sub picSS1Middle_Click()
     imgSS1Middle_Click
End Sub

Private Sub picSS1Upper_Click()
     imgSS1Upper_Click
End Sub

Private Sub picSS2Lower_Click()
     imgSS2Lower_Click
End Sub

Private Sub picSS2Middle_Click()
     imgSS2Middle_Click
End Sub

Private Sub picSS2Upper_Click()
     imgSS2Upper_Click
End Sub

Private Sub stbStatus_PanelClick(ByVal Panel As MSComctlLib.Panel)
     
     'Leave immediately if no map is loaded
     If (mapfile = "") Then Exit Sub
     
     'Leave when map edit is disabled
     If (picMap.Enabled = False) Then Exit Sub
     
     'Unable to click these while in 3D mode
     If (mode = EM_3D) Then Exit Sub
     
     'Check what panel is clicked
     Select Case Panel.Key
          
          'Zoom
          Case "viewzoom": frmZoom.Show 1, Me
          
          'Grid
          Case "gridsize": frmGrid.Show 1, Me
          
          'Snap
          Case "snapmode": itmEditSnapToGrid_Click
          
          'Stitch
          Case "stitchmode": itmEditStitch_Click
          
     End Select
End Sub

Private Sub tlbToolbar_ButtonClick(ByVal Button As MSComctlLib.Button)
     
     'Unable to click these while in 3D mode
     If (mode = EM_3D) Then Exit Sub
     
     'Check what button is clicked
     Select Case Button.Key
          Case "FileNewMap": itmFileNew_Click
          Case "FileOpenMap": itmFileOpenMap_Click
          Case "FileSaveMap": itmFileSaveMap_Click
          Case "ModeMove": itmEditMode_Click 0
          Case "ModeVertices": itmEditMode_Click 1
          Case "ModeLines": itmEditMode_Click 2
          Case "ModeSectors": itmEditMode_Click 3
          Case "ModeThings": itmEditMode_Click 4
          Case "Mode3D": itmEditMode_Click 5
          Case "FileTest": itmFileTest_Click False
          Case "FileBuild": itmFileBuild_Click
          Case "EditUndo": itmEditUndo_Click
          Case "EditRedo": itmEditRedo_Click
          Case "EditGrid": frmGrid.Show 1, Me
          Case "EditSnap": itmEditSnapToGrid_Click
          Case "EditStitch": itmEditStitch_Click
          Case "EditFlipH": itmEditFlipH_Click
          Case "EditFlipV": itmEditFlipV_Click
          Case "EditRotate": itmEditRotate_Click
          Case "EditResize": itmEditResize_Click
          Case "EditCenterView": itmEditCenterView_Click
          Case "PrefabsInsert": If mnuPrefabs.visible Then mnuPrefabs_Click: If itmPrefabInsert.Enabled Then itmPrefabInsert_Click
          Case "PrefabsInsertPrevious": If mnuPrefabs.visible Then mnuPrefabs_Click: If itmPrefabPrevious.Enabled Then itmPrefabPrevious_Click
          Case "LinesFlip": itmLinesFlipLinedefs_Click
          Case "LinesCurve": itmLinesCurve_Click
          Case "LinesSplit": itmLinesSplit_Click
          Case "SectorsJoin": itmSectorsJoin_Click
          Case "SectorsMerge": itmSectorsMerge_Click
          Case "SectorsGradientBrightness": itmSectorsGradientBrightness_Click
          Case "SectorsGradientFloors": itmSectorsGradientFloors_Click
          Case "SectorsGradientCeilings": itmSectorsGradientCeilings_Click
          Case "GradientTags": If mode = EM_LINES Then itmLinesCascadeTags_Click Else itmSectorsCascadeTags_Click
          Case "SectorsSetupFOF": itmSectorsFOF_Click
          Case "ThingsFilter": itmThingsFilter_Click
     End Select
End Sub


Private Sub tlbToolbar_MouseUp(Button As Integer, Shift As Integer, x As Single, y As Single)

     If Button And vbRightButton Then
          PopupMenu mnuToolbar, vbPopupMenuRightButton
     End If

End Sub

Private Sub tmr3DRedraw_Timer()
     'On Error GoTo Leave3DMode
     Dim ErrNumber As Long
     Dim ErrDesc As String
     
     'After this fame input will be processed again
     IgnoreInput = False
     
     'Check if still in 3D Mode
     If (Running3D) Then
          
          'Check if editing allowed
          If (picMap.Enabled) Then
               
               'Check if not in texture selection
               If (TextureSelecting = False) Then
                    
                    'Do single frame in 3D Mode
                    RunSingleLoop
               End If
          End If
     End If
     
     'Done
     Exit Sub
     
     
Leave3DMode:
     
     'Keep error
     ErrNumber = Err.number
     ErrDesc = Err.Description
     
     'Stop 3D Mode now
     Stop3DMode
     
     'Display error if not device lost error
     If (ErrNumber <> -2005530520) Then MsgBox "Error " & ErrNumber & " in 3D Mode: " & ErrDesc, vbCritical
End Sub

Private Sub tmrAutoSave_Timer()

     If mode <> EM_3D And submode = ESM_NONE And itmFile(4).visible And itmFile(4).Enabled And Config("autosave") > 0 And picMap.Enabled Then
     
          ASMinutes = ASMinutes + 1          ' We only count if we're able to save.
          If ASMinutes Mod Config("autosave") = 0 And mapchanged Then MapSave mapfile & ".autosave-" & maplumpname, SM_SAVEINTO, False, True
     
     End If

End Sub

Private Sub tmrAutoScroll_Timer()
     
     'Disable timer
     tmrAutoScroll.Enabled = False
     
     'Check if we should stop autoscroling
     If (Not ForceScroll) Then
          If (Config("autoscroll") = vbUnchecked) Then Exit Sub
          If (mode = EM_MOVE) Or (submode = ESM_NONE) Or (submode = ESM_MOVING) Then Exit Sub
     End If
     
     'Restart timer
     tmrAutoScroll.Enabled = True
     
     'Apply scrolling
     ViewLeft = ViewLeft + AutoScrollX
     ViewTop = ViewTop + AutoScrollY
     
     'Change viewport
     ChangeView ViewLeft, ViewTop, ViewZoom
     
     'Redraw map
     RedrawMap
End Sub

Private Sub tmrMouseOutside_Timer()
     Dim ViewRect As RECT
     Dim pnt As POINT
     
     'Leave immediately if no map is loaded
     If (mapfile = "") Then Exit Sub
     
     'Get the screen coordinates of the map box
     ClientToScreen frmMain.picMap.hWnd, pnt
     
     'Make the rect of the map on screen
     With ViewRect
          .left = pnt.x
          .top = pnt.y
          .right = pnt.x + (frmMain.picMap.width - 4)
          .bottom = pnt.y + (frmMain.picMap.Height - 4)
     End With
     
     'Get the mouse coordinates
     GetCursorPos pnt
     
     'Check if cursor is outside viewport
     If ((pnt.x < ViewRect.left) Or _
        (pnt.x > ViewRect.right) Or _
        (pnt.y < ViewRect.top) Or _
        (pnt.y > ViewRect.bottom)) Then
          
          'Update cursor position in statusbar
          stbStatus.Panels("mousex").Text = ""
          stbStatus.Panels("mousey").Text = ""
          
          'Remove drawline when in drawing mode
          If (submode = ESM_DRAWING) Then
               
               'Remove drawing line
               ClearOldDrawingLine
               
               'Show changes
               picMap.Refresh
          End If
          
          'Remove highlighting
          If (currentselected > -1) Then RemoveHighlight
          
          'No scrolling
          ChangeAutoscroll True
          
          'Erase last position
          MouseInside = False
          
          'Disable timer
          tmrMouseOutside.Enabled = False
     End If
End Sub

Private Sub tmrMouseTimeout_Timer()
     
     'Disable timer
     tmrMouseTimeout.Enabled = False
End Sub

Private Sub UpdateMenuShortcuts()
     
     'Set all shortcut tips on the menu items
     itmFile(0).Caption = MenuNameForShortcut(itmFile(0).Caption, "filenew")
     itmFile(1).Caption = MenuNameForShortcut(itmFile(1).Caption, "fileopen")
     itmFile(2).Caption = MenuNameForShortcut(itmFile(2).Caption, "fileswitchmap")
     itmFile(4).Caption = MenuNameForShortcut(itmFile(4).Caption, "fileclose")
     itmFile(5).Caption = MenuNameForShortcut(itmFile(5).Caption, "filesave")
     itmFile(6).Caption = MenuNameForShortcut(itmFile(6).Caption, "filesaveas")
     itmFile(7).Caption = MenuNameForShortcut(itmFile(7).Caption, "filesaveinto")
     itmFile(12).Caption = MenuNameForShortcut(itmFile(12).Caption, "filebuildnodes")
     itmFile(13).Caption = MenuNameForShortcut(itmFile(13).Caption, "filetest")
     itmFile(9).Caption = MenuNameForShortcut(itmFile(9).Caption, "fileexport")
     itmFile(10).Caption = MenuNameForShortcut(itmFile(10).Caption, "fileexportpicture")
     
     itmEditUndo.Caption = MenuNameForShortcut(itmEditUndo.Caption, "editundo")
     itmEditRedo.Caption = MenuNameForShortcut(itmEditRedo.Caption, "editredo")
     itmEdit(1).Caption = MenuNameForShortcut(itmEdit(1).Caption, "editcut")
     itmEdit(2).Caption = MenuNameForShortcut(itmEdit(2).Caption, "editcopy")
     itmEdit(3).Caption = MenuNameForShortcut(itmEdit(3).Caption, "editpaste")
     itmEdit(4).Caption = MenuNameForShortcut(itmEdit(4).Caption, "editdelete")
     itmEdit(6).Caption = MenuNameForShortcut(itmEdit(6).Caption, "editfind")
     itmEdit(7).Caption = MenuNameForShortcut(itmEdit(7).Caption, "editreplace")
     itmEditMode(0).Caption = MenuNameForShortcut(itmEditMode(0).Caption, "editmove")
     itmEditMode(1).Caption = MenuNameForShortcut(itmEditMode(1).Caption, "editvertices")
     itmEditMode(2).Caption = MenuNameForShortcut(itmEditMode(2).Caption, "editlines")
     itmEditMode(3).Caption = MenuNameForShortcut(itmEditMode(3).Caption, "editsectors")
     itmEditMode(4).Caption = MenuNameForShortcut(itmEditMode(4).Caption, "editthings")
     itmEditMode(5).Caption = MenuNameForShortcut(itmEditMode(5).Caption, "edit3d")
     itmEdit(23).Caption = MenuNameForShortcut(itmEdit(23).Caption, "editoptions")
     itmEdit(9).Caption = MenuNameForShortcut(itmEdit(9).Caption, "editfliph")
     itmEdit(10).Caption = MenuNameForShortcut(itmEdit(10).Caption, "editflipv")
     itmEdit(11).Caption = MenuNameForShortcut(itmEdit(11).Caption, "editrotate")
     itmEdit(12).Caption = MenuNameForShortcut(itmEdit(12).Caption, "editresize")
     itmEdit(17).Caption = MenuNameForShortcut(itmEdit(17).Caption, "editcenterview")
     itmEdit(19).Caption = MenuNameForShortcut(itmEdit(19).Caption, "editselectall")
     itmEdit(20).Caption = MenuNameForShortcut(itmEdit(20).Caption, "editselectnone")
     itmEdit(21).Caption = MenuNameForShortcut(itmEdit(21).Caption, "editselectinvert")
     
     itmVerticesSnapToGrid.Caption = MenuNameForShortcut(itmVerticesSnapToGrid.Caption, "snaptogrid")
     itmVerticesClearUnused.Caption = MenuNameForShortcut(itmVerticesClearUnused.Caption, "clearvertices")
     itmVerticesStitch.Caption = MenuNameForShortcut(itmVerticesStitch.Caption, "stitchvertices")
     
     itmLinesSnapToGrid.Caption = MenuNameForShortcut(itmLinesSnapToGrid.Caption, "snaptogrid")
     itmLinesAlign.Caption = MenuNameForShortcut(itmLinesAlign.Caption, "linesautoalign")
     itmLinesSelect(0).Caption = MenuNameForShortcut(itmLinesSelect(0).Caption, "select1sided")
     itmLinesSelect(1).Caption = MenuNameForShortcut(itmLinesSelect(1).Caption, "select2sided")
     itmLinesFlipLinedefs.Caption = MenuNameForShortcut(itmLinesFlipLinedefs.Caption, "fliplinedefs")
     itmLinesSplit.Caption = MenuNameForShortcut(itmLinesSplit.Caption, "splitlinedefs")
     itmLinesFlipSidedefs.Caption = MenuNameForShortcut(itmLinesFlipSidedefs.Caption, "flipsidedefs")
     itmLinesCurve.Caption = MenuNameForShortcut(itmLinesCurve.Caption, "curvelines")
     itmLinesCopy.Caption = MenuNameForShortcut(itmLinesCopy.Caption, "copyprops")
     itmLinesPaste.Caption = MenuNameForShortcut(itmLinesPaste.Caption, "pasteprops")
     itmLinesCascadeTags.Caption = MenuNameForShortcut(itmLinesCascadeTags.Caption, "cascadetags")
     
     itmSectorsSnapToGrid.Caption = MenuNameForShortcut(itmSectorsSnapToGrid.Caption, "snaptogrid")
     itmSectorsJoin.Caption = MenuNameForShortcut(itmSectorsJoin.Caption, "joinsector")
     itmSectorsMerge.Caption = MenuNameForShortcut(itmSectorsMerge.Caption, "mergesector")
     itmSectorsRaiseFloor.Caption = MenuNameForShortcut(itmSectorsRaiseFloor.Caption, "raisefloor")
     itmSectorsLowerFloor.Caption = MenuNameForShortcut(itmSectorsLowerFloor.Caption, "lowerfloor")
     itmSectorsRaiseCeiling.Caption = MenuNameForShortcut(itmSectorsRaiseCeiling.Caption, "raiseceil")
     itmSectorsLowerCeiling.Caption = MenuNameForShortcut(itmSectorsLowerCeiling.Caption, "lowerceil")
     itmSectorsIncBrightness.Caption = MenuNameForShortcut(itmSectorsIncBrightness.Caption, "brightinc")
     itmSectorsDecBrightness.Caption = MenuNameForShortcut(itmSectorsDecBrightness.Caption, "brightdec")
     itmSectorsCopy.Caption = MenuNameForShortcut(itmSectorsCopy.Caption, "copyprops")
     itmSectorsPaste.Caption = MenuNameForShortcut(itmSectorsPaste.Caption, "pasteprops")
     itmSectorsGradientBrightness.Caption = MenuNameForShortcut(itmSectorsGradientBrightness.Caption, "gradientbrightness")
     itmSectorsGradientFloors.Caption = MenuNameForShortcut(itmSectorsGradientFloors.Caption, "gradientceilings")
     itmSectorsGradientCeilings.Caption = MenuNameForShortcut(itmSectorsGradientCeilings.Caption, "gradientfloors")
     itmSectorsCascadeTags.Caption = MenuNameForShortcut(itmSectorsCascadeTags.Caption, "cascadetags")
     itmSectorsFOF.Caption = MenuNameForShortcut(itmSectorsFOF.Caption, "fofsetup")
     itmSectorsConvertToStairs.Caption = MenuNameForShortcut(itmSectorsConvertToStairs.Caption, "stairs")
     itmSectorsFindIdenticalSectorSet.Caption = MenuNameForShortcut(itmSectorsFindIdenticalSectorSet.Caption, "sectorsident")
     
     itmThingsSnapToGrid.Caption = MenuNameForShortcut(itmThingsSnapToGrid.Caption, "snaptogrid")
     itmThingsCopy.Caption = MenuNameForShortcut(itmThingsCopy.Caption, "copyprops")
     itmThingsPaste.Caption = MenuNameForShortcut(itmThingsPaste.Caption, "pasteprops")
     itmThingsFilter.Caption = MenuNameForShortcut(itmThingsFilter.Caption, "thingsfilter")
     itmThingsMouseRotate.Caption = MenuNameForShortcut(itmThingsMouseRotate.Caption, "thingsmouserotate")
     
     itmToolsFindErrors.Caption = MenuNameForShortcut(itmToolsFindErrors.Caption, "errorcheck")
     itmToolsClearTextures.Caption = MenuNameForShortcut(itmToolsClearTextures.Caption, "removetextures")
     itmToolsTagBrowser.Caption = MenuNameForShortcut(itmToolsTagBrowser.Caption, "toolstagbrowser")
     itmToolsFixTextures.Caption = MenuNameForShortcut(itmToolsFixTextures.Caption, "fixtextures")
     itmToolsConfiguration.Caption = MenuNameForShortcut(itmToolsConfiguration.Caption, "fileconfig")
     itmToolsFixZeroLinedefs.Caption = MenuNameForShortcut(itmToolsFixZeroLinedefs.Caption, "fixzerolengthlines")
     itmToolsReloadResources.Caption = MenuNameForShortcut(itmToolsReloadResources.Caption, "reloadresources")
     
     itmPrefabInsert.Caption = MenuNameForShortcut(itmPrefabInsert.Caption, "prefabinsert")
     itmPrefabPrevious.Caption = MenuNameForShortcut(itmPrefabPrevious.Caption, "prefabinsertlast")
     itmPrefabQuick(0).Caption = MenuNameForShortcut(itmPrefabQuick(0).Caption, "prefabinsert1")
     itmPrefabQuick(1).Caption = MenuNameForShortcut(itmPrefabQuick(1).Caption, "prefabinsert2")
     itmPrefabQuick(2).Caption = MenuNameForShortcut(itmPrefabQuick(2).Caption, "prefabinsert3")
     itmPrefabQuick(3).Caption = MenuNameForShortcut(itmPrefabQuick(3).Caption, "prefabinsert4")
     itmPrefabQuick(4).Caption = MenuNameForShortcut(itmPrefabQuick(4).Caption, "prefabinsert5")
     
     itmHelpWebsite.Caption = MenuNameForShortcut(itmHelpWebsite.Caption, "helpwebsite")
     itmHelpFAQ.Caption = MenuNameForShortcut(itmHelpFAQ.Caption, "helpfaq")
     itmHelpAbout.Caption = MenuNameForShortcut(itmHelpAbout.Caption, "helpabout")
End Sub


Private Sub tmrStupidToolbarBugWorkaround_Timer()

     ' We have to wait a while after Form_Load before we can do this.
     RestoreToolbarLayout
     tmrStupidToolbarBugWorkaround.Enabled = False

End Sub

Private Sub tmrTerminate_Timer()
     
     'Terminate program
     Terminate
End Sub


Private Function MakeNiceActionText(ByVal effect As Long)

     'Check if the linedef type can be found
     If effect = 0 Then
          MakeNiceActionText = ""
     ElseIf (mapconfig("linedeftypes").Exists(CStr(effect))) Then
          MakeNiceActionText = effect & " - " & Trim$(mapconfig("linedeftypes")(CStr(effect))("title"))
     Else
          'Check if generalized type
          If IsGenLinedefEffect(effect) Then
               MakeNiceActionText = effect & " - " & GetGenLinedefCategory(effect)("title") & "*"
          Else
               MakeNiceActionText = effect & " - Unknown"
          End If
     End If

End Function


Private Function MakeNiceSecTypeText(ByVal SecType As Long)

     If mapconfig("seceffectnybble") Then
          ' 1.1 allows multiple effects.
          Dim i As Long, iCountDisplayedTypes As Long
          Dim bNybble As Byte
          
          iCountDisplayedTypes = 0
          MakeNiceSecTypeText = CStr(SecType) & " - "
          
          For i = 0 To 3
               bNybble = (SecType \ (&H10 ^ i)) And &HF
               
               ' Okay, not very elegant. Always go the first time; otherwise, only if nonzero.
               If i = 0 Or bNybble > 0 Then
                    If mapconfig("sectortypes")("nybble" & CStr(i)).Exists(CStr(bNybble)) Then
                         If iCountDisplayedTypes > 0 Then MakeNiceSecTypeText = MakeNiceSecTypeText & "; "
                         iCountDisplayedTypes = iCountDisplayedTypes + 1
                         MakeNiceSecTypeText = MakeNiceSecTypeText & Trim$(mapconfig("sectortypes")("nybble" & CStr(i))(CStr(bNybble)))
                    End If
               End If
          Next i
          
          If iCountDisplayedTypes = 0 Then MakeNiceSecTypeText = MakeNiceSecTypeText & "Unknown"
     Else
          ' Simple 1.09 way.
          'Check if the sector effect can be found
          If (Trim$(mapconfig("sectortypes").Exists(CStr(SecType)))) Then
               MakeNiceSecTypeText = SecType & " - " & Trim$(mapconfig("sectortypes")(CStr(SecType)))
          Else
               MakeNiceSecTypeText = SecType & " - Unknown"
          End If
     End If

End Function

Private Sub ClearOldDrawingLine()
     ' If not in DCK mode:
     If Not CBool(ShiftMaskAtDrawStart And vbAltMask) Then
          'Remove previous drawn line
          RenderDrawingLine PAL_NORMAL, PAL_NORMAL, LastX, LastY
          
     Else
          ' We're in DCK mode, so we're drawing boxes.
          'Remove previous drawn lines
          Dim SnappedX As Long, SnappedY As Long
          SnappedX = LastX
          SnappedY = LastY
          DrawingSnap SnappedX, SnappedY, ShiftMaskAtDrawStart
          
          RenderDrawingLine PAL_NORMAL, PAL_NORMAL, vertexes(selected.Items(0)).x, SnappedY, vertexes(selected.Items(0)).x, -vertexes(selected.Items(0)).y, True
          RenderDrawingLine PAL_NORMAL, PAL_NORMAL, SnappedX, SnappedY, vertexes(selected.Items(0)).x, SnappedY, True
          RenderDrawingLine PAL_NORMAL, PAL_NORMAL, SnappedX, -vertexes(selected.Items(0)).y, SnappedX, SnappedY, True
          RenderDrawingLine PAL_NORMAL, PAL_NORMAL, vertexes(selected.Items(0)).x, -vertexes(selected.Items(0)).y, SnappedX, -vertexes(selected.Items(0)).y, True

     End If
End Sub

Private Sub RenderNewDrawingLine(ByVal x As Long, ByVal y As Long)

     ' If not in DCK mode:
     If Not CBool(ShiftMaskAtDrawStart And vbAltMask) Then
          'Render line being drawn
          RenderDrawingLine PAL_MULTISELECTION, PAL_BACKGROUND, x, y
     Else
          ' We're in DCK mode, so we're drawing boxes.
          'Remove previous drawn lines
          Dim SnappedX As Long, SnappedY As Long
          SnappedX = x
          SnappedY = y
          DrawingSnap SnappedX, SnappedY, ShiftMaskAtDrawStart
          
          RenderDrawingLine PAL_MULTISELECTION, PAL_BACKGROUND, vertexes(selected.Items(0)).x, SnappedY, vertexes(selected.Items(0)).x, -vertexes(selected.Items(0)).y, True
          RenderDrawingLine PAL_MULTISELECTION, PAL_BACKGROUND, SnappedX, SnappedY, vertexes(selected.Items(0)).x, SnappedY, True
          RenderDrawingLine PAL_MULTISELECTION, PAL_BACKGROUND, SnappedX, -vertexes(selected.Items(0)).y, SnappedX, SnappedY, True
          RenderDrawingLine PAL_MULTISELECTION, PAL_BACKGROUND, vertexes(selected.Items(0)).x, -vertexes(selected.Items(0)).y, SnappedX, -vertexes(selected.Items(0)).y, True
     End If

End Sub
