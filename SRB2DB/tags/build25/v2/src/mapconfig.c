#include <windows.h>
#include <tchar.h>
#include <stdio.h>

#include "general.h"
#include "config.h"
#include "options.h"
#include "mapconfig.h"
#include "resource.h"


/* Macros. */
#define MAPCFGDIR		TEXT("map/")
#define MAPCFGPATTERN	TEXT("*.cfg")

#define MAPCFG_SIGNATURE	"SRB2 Builder Game Configuration"

#define ARROW_DEFAULT	0
#define CIRCLE_DEFAULT	0
#define ERROR_DEFAULT	0
#define HANGS_DEFAULT	0
#define HEIGHT_DEFAULT	64
#define WIDTH_DEFAULT	64



/* We store all map configs as subsections of the following, since it's
 * convenient.
 */
static CONFIG *g_lpcfgMapConfigs;


/* Static prototypes. */
static BOOL AddThingsByCategory(CONFIG *lpcfgCategorySS, void *lpv);
static BOOL InheritThingDetailsAndAddToFlatSection(CONFIG *lpcfgThingSS, void *lpv);
static BOOL AddSingleMapConfigToComboBox(CONFIG *lpcfg, void *lpvWindow);


typedef struct _ATBCDATA
{
	CONFIG *lpcfgFlatThings, *lpcfgThingFlags;
} ATBCDATA;

typedef struct _THINGDETAILS
{
	int		iWidth, iArrow, iError, iHeight, iHangs, iCircle;
	LPSTR	szDeafText, szMultiText;
} THINGDETAILS;

typedef struct _ITDAATFSDATA
{
	THINGDETAILS	td;
	LPCSTR			szCategoryName;
	CONFIG			*lpcfgFlatThings;
} ITDAATFSDATA;



/* LoadMapConfigs
 *   Loads all map configuration files from the appropriate directory.
 *
 * Parameters:
 *   None.
 *
 * Return value: int
 *   Number of configs loaded.
 */
int LoadMapConfigs(void)
{
	HANDLE hFind;
	WIN32_FIND_DATA fd;
	int iConfigs = 0;	/* Keep track of the number of map configs loaded. */
	LPTSTR szOldDir;
	DWORD cbOldDir;
	TCHAR c;

	/* Save wd -- we change it, since Find[First|Next]File return relative
	 * paths.
	 */
	cbOldDir = GetCurrentDirectory(1, &c);
	szOldDir = ProcHeapAlloc(cbOldDir);
	GetCurrentDirectory(cbOldDir, szOldDir);

	/* Change to the map config directory. */
	SetCurrentDirectory(OPTDIR MAPCFGDIR);

	/* Initialise the container. */
	g_lpcfgMapConfigs = ConfigCreate();

	/* Find the first file we're interested in. */
	hFind = FindFirstFile(MAPCFGPATTERN, &fd);

	/* Did we find at least one file? */
	if(hFind != INVALID_HANDLE_VALUE)
	{
		CONFIG *lpcfg;
		
		/* Repeat for each map config file. */
		do
		{
			/* Load the file. */
			lpcfg = LoadConfig(fd.cFileName);

			/* Was the file a syntactically-valid config? */
			if(lpcfg)
			{
				/* Check whether the type signature is correct. */
				int iLen;
				LPSTR szType;
				CONFIG *lpcfgThingCats;
				ATBCDATA atbcdata;

				iLen = ConfigGetStringLength(lpcfg, "type");

				if(iLen <= 0)
				{
					ConfigDestroy(lpcfg);
					continue;
				}

				szType = ProcHeapAlloc((iLen + 1) * sizeof(char));
				ConfigGetStringA(lpcfg, "type", szType, iLen + 1);

				if(lstrcmpiA(szType, MAPCFG_SIGNATURE))
				{
					ConfigDestroy(lpcfg);
					continue;
				}



				/* Things need some special attention. We store two copies of
				 * each: one in its category, and one in a flat subsection.
				 * Also, we fill in parental values.
				 */

				/* Get the section of things, organised by category. */
				lpcfgThingCats = ConfigGetSubsection(lpcfg, "thingtypes");

				if(!lpcfgThingCats)
				{
					ConfigDestroy(lpcfg);
					continue;
				}

				/* Create a flat section of things, so we don't have to look
				 * through all the categories for each thing.
				 */
				atbcdata.lpcfgFlatThings = ConfigAddSubsection(lpcfg, "__things");
				atbcdata.lpcfgThingFlags = ConfigGetSubsection(lpcfg, "thingflags");

				ConfigIterate(lpcfgThingCats, AddThingsByCategory, &atbcdata);


				/* If we get here, we have a valid map config, at least
				 * as far as the checks we made go.
				 */


				/* Add the map config to the global config tree! Use the file
				 * title as the name.
				 */
#ifdef _UNICODE
				{
					LPSTR szMapCfgID = ProcHeapAlloc((lstrlen(fd.cFileName) + 1) * sizeof(TCHAR));
					wsprintfA(szMapCfgID, "%hs", fd.cFileName);
					ConfigSetSubsection(g_lpcfgMapConfigs, szMapCfgID, lpcfg);
					ProcHeapFree(szMapCfgID);
				}
#else
				ConfigSetSubsection(g_lpcfgMapConfigs, fd.cFileName, lpcfg);
#endif
				iConfigs++;

				ProcHeapFree(szType);

			}	/* if(lpcfg) */

		} while(FindNextFile(hFind, &fd));

		FindClose(hFind);
	}

	/* Change back to our old wd. */
	SetCurrentDirectory(szOldDir);
	ProcHeapFree(szOldDir);

	/* Return the number of map configs. */
	return iConfigs;
}




/* AddThingsByCategory
 *   Given a tree of things from the same category, sets their default
 *   properties as specified by the category, and then also adds a copy of each
 *   to a flat section.
 *
 * Parameters:
 *   CONFIG*	lpcfgCategorySS		The category's SUBSECTION CONTAINER!
 *   void*		lpv					(ATBCDATA*) Flat things ss ROOT, thing
 *									flags.
 *
 * Return value: BOOL
 *   TRUE if iteration should continue; FALSE otherwise.
 *
 * Remarks:
 *   Used as a callback by ConfigIterate.
 */
static BOOL AddThingsByCategory(CONFIG *lpcfgCategorySS, void *lpv)
{
	CONFIG *lpcfgCategoryRoot = lpcfgCategorySS->lpcfgSubsection;
	CONFIG *lpcfgFlatThings = ((ATBCDATA*)lpv)->lpcfgFlatThings;
	CONFIG *lpcfgThingFlags = ((ATBCDATA*)lpv)->lpcfgThingFlags;
	ITDAATFSDATA itdaatfsdata;
	WORD cbBuffer;

	/* Something other than a subsection in things section?? */
	if(lpcfgCategorySS->entrytype != CET_SUBSECTION) return TRUE;

	/* Get all the properties specified by the category. These are used as
	 * defaults when things in this category don't specify these values
	 * themselves.
	 */
	
	itdaatfsdata.td.iArrow	= ConfigNodeExists(lpcfgCategoryRoot, "arrow")	? ConfigGetInteger(lpcfgCategoryRoot, "arrow")	: ARROW_DEFAULT;
	itdaatfsdata.td.iCircle	= ConfigNodeExists(lpcfgCategoryRoot, "circle")	? ConfigGetInteger(lpcfgCategoryRoot, "circle")	: CIRCLE_DEFAULT;
	itdaatfsdata.td.iError	= ConfigNodeExists(lpcfgCategoryRoot, "error")	? ConfigGetInteger(lpcfgCategoryRoot, "error")	: ERROR_DEFAULT;
	itdaatfsdata.td.iHangs	= ConfigNodeExists(lpcfgCategoryRoot, "hangs")	? ConfigGetInteger(lpcfgCategoryRoot, "hangs")	: HANGS_DEFAULT;
	itdaatfsdata.td.iHeight	= ConfigNodeExists(lpcfgCategoryRoot, "height")	? ConfigGetInteger(lpcfgCategoryRoot, "height")	: HEIGHT_DEFAULT;
	itdaatfsdata.td.iWidth	= ConfigNodeExists(lpcfgCategoryRoot, "width")	? ConfigGetInteger(lpcfgCategoryRoot, "width")	: WIDTH_DEFAULT;

	/* Default string values. */

	if(ConfigNodeExists(lpcfgCategoryRoot, "deaftext"))
	{
		cbBuffer = ConfigGetStringLength(lpcfgCategoryRoot, "deaftext") + 1;
		itdaatfsdata.td.szDeafText = ProcHeapAlloc(cbBuffer);
		ConfigGetStringA(lpcfgCategoryRoot, "deaftext", itdaatfsdata.td.szDeafText, cbBuffer);
	}
	else
	{
		cbBuffer = ConfigGetStringLength(lpcfgThingFlags, "8") + 1;
		itdaatfsdata.td.szDeafText = ProcHeapAlloc(cbBuffer);
		ConfigGetStringA(lpcfgThingFlags, "8", itdaatfsdata.td.szDeafText, cbBuffer);
	}

	if(ConfigNodeExists(lpcfgCategoryRoot, "multitext"))
	{
		cbBuffer = ConfigGetStringLength(lpcfgCategoryRoot, "multitext") + 1;
		itdaatfsdata.td.szMultiText = ProcHeapAlloc(cbBuffer);
		ConfigGetStringA(lpcfgCategoryRoot, "multitext", itdaatfsdata.td.szMultiText, cbBuffer);
	}
	else
	{
		cbBuffer = ConfigGetStringLength(lpcfgThingFlags, "16") + 1;
		itdaatfsdata.td.szMultiText = ProcHeapAlloc(cbBuffer);
		ConfigGetStringA(lpcfgThingFlags, "16", itdaatfsdata.td.szMultiText, cbBuffer);
	}

	/* Now we iterate over every thing in the category, setting its missing
	 * fields and also adding a copy to the flat section.
	 */
	itdaatfsdata.szCategoryName = lpcfgCategorySS->szName;
	itdaatfsdata.lpcfgFlatThings = lpcfgFlatThings;
	ConfigIterate(lpcfgCategoryRoot, InheritThingDetailsAndAddToFlatSection, &itdaatfsdata);

	ProcHeapFree(itdaatfsdata.td.szDeafText);
	ProcHeapFree(itdaatfsdata.td.szMultiText);

	/* Keep going. */
	return TRUE;
}



/* InheritThingDetailsAndAddToFlatSection
 *   Sets a thing's default values where they're missing, and adds a copy of the
 *   thing to a flat section of things.
 *
 * Parameters:
 *   CONFIG*	lpcfgCategorySS		The thing's subsection *container* OR a
 *									string config entry!
 *   void*		lpv					Pointers to the defaults and the flat
 *									section.
 *
 * Return value: BOOL
 *   TRUE if iteration should continue; FALSE otherwise.
 *
 * Remarks:
 *   Used as a callback by ConfigIterate.
 */
static BOOL InheritThingDetailsAndAddToFlatSection(CONFIG *lpcfgThingSS, void *lpv)
{
	THINGDETAILS td = ((ITDAATFSDATA*)lpv)->td;
	CONFIG *lpcfgFlatThings = ((ITDAATFSDATA*)lpv)->lpcfgFlatThings;
	CONFIG *lpcfgThingRoot;
	LPSTR szTitle, szName;
	WORD cbTitle, cbName;


	/* If the string's not numeric, then it must be one of the defaults, in
	 * which case we're not interested.
	 */
	if(!IsStringIntA(lpcfgThingSS->szName)) return TRUE;


	/* Determine whether this is a fully-fledged subsection, a simple string, or
	 * some garbage.
	 */
	switch(lpcfgThingSS->entrytype)
	{
	case CET_SUBSECTION:
		lpcfgThingRoot = lpcfgThingSS->lpcfgSubsection;
		break;
	case CET_STRING:
		/* Build a subsection from the string. The string value is the 'title'
		 * field.
		 */
		cbTitle = lstrlenA(lpcfgThingSS->sz) + 1;
		szTitle = ProcHeapAlloc(cbTitle);
		cbName = lstrlenA(lpcfgThingSS->szName) + 1;
		szName = ProcHeapAlloc(cbName);

		CopyMemory(szTitle, lpcfgThingSS->sz, cbTitle);
		
		/* lpcfgThingSS isn't the category root, strictly, but it still fits the
		 * definition.
		 */
		lpcfgThingRoot = ConfigAddSubsection(lpcfgThingSS, szName);
		ConfigSetString(lpcfgThingRoot, "title", szTitle);

		ProcHeapFree(szName);
		ProcHeapFree(szTitle);

	default:
		/* Neither a subsection nor a string. (??) */
		return TRUE;
	}


	/* Set the missing fields. */
	if(!ConfigNodeExists(lpcfgThingRoot, "arrow")) ConfigSetInteger(lpcfgThingRoot, "arrow", td.iArrow);
	if(!ConfigNodeExists(lpcfgThingRoot, "circle")) ConfigSetInteger(lpcfgThingRoot, "circle", td.iCircle);
	if(!ConfigNodeExists(lpcfgThingRoot, "error")) ConfigSetInteger(lpcfgThingRoot, "error", td.iError);
	if(!ConfigNodeExists(lpcfgThingRoot, "hangs")) ConfigSetInteger(lpcfgThingRoot, "hangs", td.iHangs);
	if(!ConfigNodeExists(lpcfgThingRoot, "height")) ConfigSetInteger(lpcfgThingRoot, "height", td.iHeight);
	if(!ConfigNodeExists(lpcfgThingRoot, "width")) ConfigSetInteger(lpcfgThingRoot, "width", td.iWidth);
	if(!ConfigNodeExists(lpcfgThingRoot, "deaftext")) ConfigSetString(lpcfgThingRoot, "deaftext", td.szDeafText);
	if(!ConfigNodeExists(lpcfgThingRoot, "multitext")) ConfigSetString(lpcfgThingRoot, "multitext", td.szMultiText);

	/* Set the category name for cross-referencing from the flat section. */
	ConfigSetString(lpcfgThingRoot, "category", ((ITDAATFSDATA*)lpv)->szCategoryName);

	/* Add a *copy* of the thing to the flat section. */
	ConfigSetSubsection(lpcfgFlatThings, lpcfgThingSS->szName, ConfigDuplicate(lpcfgThingRoot));

	/* Keep going. */
	return TRUE;
}



/* UnloadMapConfigs
 *   Frees all the map configs loaded at startup.
 *
 * Parameters:
 *   None.
 *
 * Return value: None.
 */
void UnloadMapConfigs(void)
{
	ConfigDestroy(g_lpcfgMapConfigs);
}



/* AddMapConfigsToComboBox, AddSingleMapConfigToComboBox
 *   Adds all loaded configs to a combo box.
 *
 * Parameters:
 *   HWND	hwndCombo	Combo box handle.
 *
 * Return value: None.
 *
 * Remarks:
 *   The item data fields are set to pointers to the structures.
 */
void AddMapConfigsToComboBox(HWND hwndCombo)
{
	ConfigIterate(g_lpcfgMapConfigs, AddSingleMapConfigToComboBox, (void*)hwndCombo);
}


static BOOL AddSingleMapConfigToComboBox(CONFIG *lpcfg, void *lpvWindow)
{
	HWND hwndCombo = (HWND)lpvWindow;
	LPTSTR szBuffer;
	int iIndex;
	int cchBuffer;

	/* Get title. */
	cchBuffer = ConfigGetStringLength(lpcfg->lpcfgSubsection, "game") + 1;
	szBuffer = ProcHeapAlloc(cchBuffer * sizeof(TCHAR));
	ConfigGetString(lpcfg->lpcfgSubsection, "game", szBuffer, cchBuffer);

	/* Add entry and store pointer to config. */
	iIndex = SendMessage(hwndCombo, CB_ADDSTRING, 0, (LPARAM)szBuffer);
	SendMessage(hwndCombo, CB_SETITEMDATA, iIndex, (LPARAM)lpcfg);

	ProcHeapFree(szBuffer);

	/* Keep iterating. */
	return TRUE;
}



/* GetThingConfigInfo
 *   Gets config information about a particular type of thing.
 *
 * Parameters:
 *   CONFIG*			lpcfgThings		Root of flat things subsection.
 *   unsigned short		unType			Thing ID.
 *
 * Return value: CONFIG*
 *   Pointer to subsection for the specified thing-type, or NULL if none exists.
 */
CONFIG* GetThingConfigInfo(CONFIG *lpcfgThings, unsigned short unType)
{
	char szTypeEntry[6];

	/* Format the effect number for the config entry. */
	wsprintfA(szTypeEntry, "%u", unType);

	/* Is there an entry for this effect in the config? */
	if(ConfigNodeExists(lpcfgThings, szTypeEntry))
		return ConfigGetSubsection(lpcfgThings, szTypeEntry);
	else return NULL;
}






/* GetThingTypeDisplayText
 *   Builds the display string for a thing's type.
 *
 * Parameters:
 *   unsigned short		unType		Type specifier.
 *   CONFIG*			lpcfgThings	Config tree containing thing information.
 *   LPTSTR				szBuffer	Buffer to store string in.
 *   unsigned short		cchBuffer	Length of buffer, including terminator.
 *
 * Return value: None.
 */
void GetThingTypeDisplayText(unsigned short unType, CONFIG *lpcfgThings, LPTSTR szBuffer, unsigned short cchBuffer)
{
	LPTSTR szTypeText;
	CONFIG *lpcfgThingProps;

	/* We certainly don't need to be any longer than the buffer we return in. */
	szTypeText = ProcHeapAlloc(cchBuffer * sizeof(TCHAR));

	/* Is there an entry for this effect in the config? */
	if(lpcfgThingProps = GetThingConfigInfo(lpcfgThings, unType))
		ConfigGetString(lpcfgThingProps, "title", szTypeText, cchBuffer);
	else LoadString(g_hInstance, IDS_UNKNOWNTHING, szTypeText, cchBuffer);

	_sntprintf(szBuffer, cchBuffer - 1, TEXT("%u - %s"), unType, szTypeText);
	szBuffer[cchBuffer - 1] = '\0';

	ProcHeapFree(szTypeText);
}


/* GetThingDirectionDisplayText
 *   Builds the display string for a thing's direction.
 *
 * Parameters:
 *   unsigned short		nDirection	Direction as per wad-spec (i.e. degrees in
 *									the positive direction from the x-axis).
 *   LPTSTR				szBuffer	Buffer to store string in.
 *   unsigned short		cbBuffer	Length of buffer, including terminator.
 *
 * Return value: None.
 */
void GetThingDirectionDisplayText(short nDirection, LPTSTR szBuffer, unsigned short cbBuffer)
{
	TCHAR szDirString[16];
	UINT uiDirectionID;

	if(nDirection < 23 || nDirection > 337) uiDirectionID = IDS_EAST;
	else if(nDirection < 68) uiDirectionID = IDS_NORTHEAST;
	else if(nDirection < 113) uiDirectionID = IDS_NORTH;
	else if(nDirection < 158) uiDirectionID = IDS_NORTHWEST;
	else if(nDirection < 203) uiDirectionID = IDS_WEST;
	else if(nDirection < 248) uiDirectionID = IDS_SOUTHWEST;
	else if(nDirection < 293) uiDirectionID = IDS_SOUTH;
	else uiDirectionID = IDS_SOUTHEAST;

	LoadString(g_hInstance, uiDirectionID, szDirString, sizeof(szDirString) / sizeof(TCHAR));

	_sntprintf(szBuffer, cbBuffer - 1, TEXT("%s (%d)"), szDirString, nDirection);
	szBuffer[cbBuffer - 1] = '\0';
}



/* GetEffectDisplayText
 *   Builds the display string for a sector's effect.
 *
 * Parameters:
 *   unsigned short		unEffect	Effect specifier.
 *   CONFIG*			lpcfg		Config containing effect descriptions.
 *   LPTSTR				szBuffer	Buffer to store string in.
 *   unsigned short		cchBuffer	Length of buffer, including terminator.
 *
 * Return value: None.
 *
 * Remarks:
 *   This works for sectors and old-format linedefs.
 */
void GetEffectDisplayText(unsigned short unEffect, CONFIG *lpcfg, LPTSTR szBuffer, unsigned short cchBuffer)
{
	LPTSTR szEffectText;
	char szEffectEntry[12];

	/* We certainly don't need to be any longer than the buffer we return in. */
	szEffectText = ProcHeapAlloc(cchBuffer * sizeof(TCHAR));

	/* Format the effect number for the config entry. */
	wsprintfA(szEffectEntry, "%u", unEffect);

	/* Is there an entry for this effect in the config? */
	if(ConfigNodeExists(lpcfg, szEffectEntry))
		ConfigGetString(lpcfg, szEffectEntry, szEffectText, cchBuffer);
	else LoadString(g_hInstance, IDS_UNKNOWNEFFECT, szEffectText, cchBuffer);

	_sntprintf(szBuffer, cchBuffer - 1, TEXT("%u - %s"), unEffect, szEffectText);
	szBuffer[cchBuffer - 1] = '\0';

	ProcHeapFree(szEffectText);
}

