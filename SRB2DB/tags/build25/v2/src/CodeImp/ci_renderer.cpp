/*
'    Doom Builder
'    Copyright (c) 2003 Pascal vd Heiden, www.codeimp.com
'    This program is released under GNU General Public License
'
'    This program is distributed in the hope that it will be useful,
'    but WITHOUT ANY WARRANTY; without even the implied warranty of
'    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
'    GNU General Public License for more details.
*/


// Definitions
#define WIN32_LEAN_AND_MEAN

// Includes
#include <windows.h>
#include <objbase.h>
#include <stdio.h>

// Glue between DB's renderer and SRB2B.
#include "../renderer.h"
#include "../maptypes.h"

#include "ci_const.h"
#include "ci_math.h"
#include "ci_renderer.h"

// turn off some stupid warnings
#pragma warning (disable: 4244)					// conversion from 'type1' to 'type2', possible loss of data



// sng: Returns 1 for positive, 0 for 0, -1 for negative
//----------------------------------------------------------------------------
int inline sgn(int s) { if(s) return s / abs(s); else return 0; }



// Render_LinedefLine: Renders a linedef line
//----------------------------------------------------------------------------
void APIENTRY Render_LinedefLine(int x1, int y1, int x2, int y2, byte c, int sl) { Render_LinedefLineF(x1, y1, x2, y2, c, sl); }
void __fastcall Render_LinedefLineF(int x1, int y1, int x2, int y2, byte c, int sl)
{
	float ix2;
	float iy2;
	
	// Render linedef line
	Render_LineF(x1, y1, x2, y2, c);
	
	// Render indicator?
	if(sl)
	{
		// Middle of indicator line
		float lx = float(x2 - x1) * 0.5;
		float ly = float(-y2 + y1) * 0.5;
		
		// Indicator line begin coordinates
		float ix1 = x1 + lx;
		float iy1 = -y1 + ly;
		
		// Normalize slope and calculate coordinates
		float len = sqrt(lx * lx + ly * ly);
		if(len)
		{
			iy2 = iy1 + (lx / len) * sl;
			ix2 = ix1 - (ly / len) * sl;
		}
		else
		{
			ix2 = ix1;
			iy2 = iy1;
		}
		
		// Render indicator line
		Render_LineF(ix1, -iy1, ix2, -iy2, c);
	}
}



// Render_LineLength: Renders a single line's length, in its middle.
//----------------------------------------------------------------------------
void APIENTRY Render_LineLength(int x1, int y1, int x2, int y2, byte* bitmap, int width, int height, int charwidth, int charheight, byte color) { Render_LineLengthF(x1, y1, x2, y2, bitmap, width, height, charwidth, charheight, color); }
void __fastcall Render_LineLengthF(int x1, int y1, int x2, int y2, byte* bitmap, int width, int height, int charwidth, int charheight, byte color)
{
	int c, cx, cy;
	int lx, ly;
	float len;
	char str_number[16];

	// Line distances
	lx = x2 - x1;
	ly = y2 - y1;
	
	// Length of line
	len = sqrt((float)(lx * lx + ly * ly));
	
	// Round the length
	len = floor(len + 0.5);
	
	// Make a string from the length number
	sprintf(str_number, "%i", (int)len);
	
	// Half line distances
	lx = (x2 - x1) >> 1;
	ly = (y2 - y1) >> 1;
	
	// Start offset for numbers
	cx = x1 + (lx - ((charwidth * strlen(str_number)) >> 1));
	cy = y1 + (ly - (charheight >> 1));
	
	// Go for each character
	for(c = 0; str_number[c] != 0; c++)
	{
		// Draw character bitmap
		Render_ScaledBitmapF(bitmap, width, height, (str_number[c] - 48) * charwidth, 0,
						     charwidth, charheight, cx, cy, color, CLR_BACKGROUND);
		
		// Change the offset for next character
		cx += (charwidth - 1);
	}
}
