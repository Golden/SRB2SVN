#ifndef __SRB2B_CI_DATA_PROTO__
#define __SRB2B_CI_DATA_PROTO__

#ifdef __cplusplus
extern "C" {
#endif

int __fastcall NearestLinedef(int x, int y, MAPVERTEX* vertices, MAPLINEDEF* linedefs, int numlinedefs, int *lpdist);
int NearestVertex(int x, int y, MAPVERTEX* vertices, int numvertices, int *lpdist);
int NearestThing(int x, int y, MAPTHING* things, int numthings, int *lpdist, int filterthings, THINGFILTERS* filter);

int __fastcall ThingFiltered(MAPTHING* thing, int filterthings, THINGFILTERS* filter);
void __fastcall ReallocIntP(int** lplpintarray, int oldlength, int newlength);
int FurthestSectorVertexF(MAPVERTEX* vertices, MAPLINEDEF* linedefs, MAPSIDEDEF* sidedefs, int numlinedefs, int line, int sector);
int IntersectSector(int x, int y, MAPVERTEX* vertices, MAPLINEDEF* linedefs, MAPSIDEDEF* sidedefs, int numlinedefs, int unselectedonly);
int IntersectSectorFloat(float x, float y, MAPVERTEX* vertices, MAPLINEDEF* linedefs, MAPSIDEDEF* sidedefs, int numlinedefs, int unselectedonly);

void ResetSelections(MAPTHING* things, int numthings, MAPLINEDEF* linedefs, int numlinedefs, MAPVERTEX* vertices, int numvertices, MAPSECTOR* sectors, int numsectors);

#ifdef __cplusplus
}
#endif

#endif
