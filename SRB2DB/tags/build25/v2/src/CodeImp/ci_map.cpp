/*
'    Doom Builder
'    Copyright (c) 2003 Pascal vd Heiden, www.codeimp.com
'    This program is released under GNU General Public License
'
'    This program is distributed in the hope that it will be useful,
'    but WITHOUT ANY WARRANTY; without even the implied warranty of
'    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
'    GNU General Public License for more details.
*/


// Definitions
#define WIN32_LEAN_AND_MEAN

// Includes
#include <windows.h>
//#include <objbase.h>
//#include <string>

#include <gl\gl.h>
#include <gl\glu.h>

// Glue between DB's renderer and SRB2B.
#include "../maptypes.h"
#include "../renderer.h"

#include "ci_math.h"
#include "ci_const.h"
#include "ci_renderer.h"
#include "ci_data_proto.h"


// turn off some stupid warnings
#pragma warning (disable: 4244)					// conversion from 'type1' to 'type2', possible loss of data


// Static prototypes.
static inline int DetermineLinedefColour(MAPLINEDEF *ld, MAPSECTOR *sectors, MAPSIDEDEF *sidedefs);


// Render_AllLinedefs: Renders all linedefs according to their properties
//----------------------------------------------------------------------------
extern "C" void Render_AllLinedefs(MAPVERTEX* vertices, MAPLINEDEF* linedefs, MAPSECTOR* sectors, MAPSIDEDEF *sidedefs, int startindex, int endindex, int indicatorlength)
{
	int sc, ld; //, l;
	MAPLINEDEF* linedef = NULL;
	
	// Go for all linedefs
	for(ld = startindex; ld <= endindex; ld++)
	{
		// Get a pointer to the linedef
		linedef = linedefs + ld;
		
		// Determine linedef colour.
		if(linedef->highlight) sc = CLR_LINEHIGHLIGHT;
		else if(linedef->selected) sc = CLR_LINESELECTED;
		else sc = DetermineLinedefColour(linedef, sectors, sidedefs);
		
		// Render the linedef
		Render_LinedefLineF(vertices[linedef->v1].x, vertices[linedef->v1].y, vertices[linedef->v2].x, vertices[linedef->v2].y, sc, indicatorlength);
	}
}


// Render_TaggedLinedefs: Renders all linedefs with the given tag
//----------------------------------------------------------------------------
void APIENTRY Render_TaggedLinedefs(MAPVERTEX* vertices, MAPLINEDEF* linedefs, MAPSECTOR* sectors, MAPSIDEDEF *sidedefs, int numlinedefs, int argtag, int argmark, BYTE c, int indicatorlength, int rendervertices, int vertexsize)
{
	int ld, sc;
	MAPLINEDEF* ldp;
	
	// Go for all linedefs
	for(ld = 0; ld < numlinedefs; ld++)
	{
		// Does this linedef have an action at all?
		if(linedefs[ld].effect)
		{
			// Get a pointer to the linedef
			ldp = &linedefs[ld];
			
			// Check if any of the arguments match the tag
			if(ldp->tag == argtag)
			{
				// Check if we should use given color
				if(c)
				{
					sc = c;
				}
				else
				{
					// Check if selected
					if(ldp->selected)
					{
						// Selection color
						sc = CLR_LINESELECTED;
					}
					else
					{
						sc = DetermineLinedefColour(ldp, sectors, sidedefs);
					}
				}
				
				// Render linedef and vertices
				Render_LinedefLineF(vertices[ldp->v1].x, vertices[ldp->v1].y, vertices[ldp->v2].x, vertices[ldp->v2].y, sc, indicatorlength);
				if(rendervertices)
				{
					Render_SquareF(vertices[ldp->v1].x, vertices[ldp->v1].y, vertexsize, CLR_VERTEX);
					Render_SquareF(vertices[ldp->v2].x, vertices[ldp->v2].y, vertexsize, CLR_VERTEX);
				}
			}
		}
	}
}


// Render_AllImpassableLinedefs: Renders all linedefs marked impassable
//----------------------------------------------------------------------------
void APIENTRY Render_AllImpassableLinedefs(MAPVERTEX* vertices, MAPLINEDEF* linedefs, int startindex, int endindex, int indicatorlength)
{
	int sc, ld; //, l;
	MAPLINEDEF* linedef = NULL;
	
	// Go for all linedefs
	for(ld = startindex; ld <= endindex; ld++)
	{
		// Get a pointer to the linedef
		linedef = linedefs + ld;
		
		// Check if selected
		if(linedef->selected)
		{
			// Selection color
			sc = CLR_LINESELECTED;
		}
		else
		{
			// Determine impassable
			if((linedef->flags & LDF_IMPASSABLE) || ((linedef->flags & LDF_TWOSIDED) == 0))
			{
				// Determine linedef color
				if(linedef->effect != 0) sc = CLR_LINESPECIAL; else sc = CLR_LINE;
			}
			else
			{
				// Dont render
				sc = CLR_BACKGROUND;
			}
		}
		
		// Render the linedef
		if(sc != CLR_BACKGROUND) Render_LinedefLineF(vertices[linedef->v1].x, vertices[linedef->v1].y, vertices[linedef->v2].x, vertices[linedef->v2].y, sc, indicatorlength);
	}
}


// Render_TaggedSectors: Renders all sectors with the given tag
//----------------------------------------------------------------------------
void APIENTRY Render_TaggedSectors(MAPVERTEX* vertices, MAPLINEDEF* linedefs, MAPSIDEDEF* sidedefs, MAPSECTOR* sectors, int numsectors, int numlinedefs, int sectortag, BYTE c, int indicatorlength, int rendervertices, int vertexsize)
{
	int s, sc, ld, found;
	MAPLINEDEF* ldp = NULL;
	
	// Go for all sectors
	for(s = 0; s < numsectors; s++)
	{
		// Check if this sector tag matches
		if(sectors[s].tag == sectortag)
		{
			// Go for all linedefs
			for(ld = 0; ld < numlinedefs; ld++)
			{
				// Get a pointer to the linedef
				ldp = &linedefs[ld];
				
				// Check if any of the sidedefs belong to this sector
				found = 0;
				if(ldp->s1 > -1) if(sidedefs[ldp->s1].sector == s) found = 1;
				if(ldp->s2 > -1) if(sidedefs[ldp->s2].sector == s) found = 1;
				
				// Render if we should render this linedef
				if(found)
				{
					// Check if we should use given color
					if(c)
					{
						sc = c;
					}
					else
					{
						// Check if selected
						if(ldp->selected)
						{
							// Selection color
							sc = CLR_LINESELECTED;
						}
						else
						{
							sc = DetermineLinedefColour(ldp, sectors, sidedefs);
						}
					}
					
					// Render linedef and vertices
					Render_LinedefLineF(vertices[ldp->v1].x, vertices[ldp->v1].y, vertices[ldp->v2].x, vertices[ldp->v2].y, sc, indicatorlength);
					if(rendervertices)
					{
						Render_SquareF(vertices[ldp->v1].x, vertices[ldp->v1].y, vertexsize, CLR_VERTEX);
						Render_SquareF(vertices[ldp->v2].x, vertices[ldp->v2].y, vertexsize, CLR_VERTEX);
					}
				}
			}
		}
	}
}





// Render_AllVertices: Renders all vertices according to their properties
//----------------------------------------------------------------------------
extern "C" void Render_AllVertices(MAPVERTEX* vertices, int startindex, int endindex, float vertexsize)
{
	// Go for all vertices
	for(int v = startindex; v <= endindex; v++)
	{
		// Check if this vertex is highlighted
		if(vertices[v].highlight)
		{
			// Draw vertex with highlight color
			Render_SquareF(vertices[v].x, vertices[v].y, vertexsize, CLR_VERTEXHIGHLIGHT);
		}
		// Check if this vertex is selected
		else if(vertices[v].selected)
		{
			// Draw vertex with selection color
			Render_SquareF(vertices[v].x, vertices[v].y, vertexsize, CLR_VERTEXSELECTED);
		}
		else
		{
			// Draw vertex with normal color
			Render_SquareF(vertices[v].x, vertices[v].y, vertexsize, CLR_VERTEX);
		}
	}
}


// Render_AllThings: Renders all things according to their properties
//----------------------------------------------------------------------------
extern "C" void Render_AllThings(MAPTHING* things, int startindex, int endindex, float imagesize, float fZoom, GLuint uiThingTex, int outlines, int filterthings, THINGFILTERS* filter)
{
	// Check if outlines must be rendered
	if(outlines)
	{
		// Go for all things
		for(int th = startindex; th <= endindex; th++)
		{
			// Check if this thing is highlighted
			if(things[th].highlight)
			{
				// Render the outline with highlight color
				Render_BoxF(things[th].x, things[th].y, things[th].size, CLR_THINGHIGHLIGHT);
			}
			// Check if this thing is selected
			else if(things[th].selected)
			{
				// Render the outline with selection color
				Render_BoxF(things[th].x, things[th].y, things[th].size, CLR_THINGSELECTED);
			}
			else
			{
				// Check if any of the thing flags match any of the filter flags
				if(ThingFiltered(&things[th], filterthings, filter))
				{
					// Render the outline with normal color
					Render_BoxF(things[th].x, things[th].y, things[th].size, things[th].color);
				}
			}

			/*if(things[th].circleradius)
			{
				// Check if any of the thing flags match any of the filter flags
				if(ThingFiltered(&things[th], filterthings, filter))
				{
					// Render the outline with normal color
					Render_CircleF(things[th].x, things[th].y, floor(things[th].circleradius * outlinezoom + 0.5), things[th].color);
				}
			}*/
		}
	}
	
	// Go for all things
	for(int th = startindex; th <= endindex; th++)
	{
		// Check if this thing is highlighted
		if(things[th].highlight)
		{
			// Render the thing with highlight color
			Render_ThingF(things[th].x, things[th].y, things[th].angle, CLR_THINGHIGHLIGHT, imagesize, fZoom, uiThingTex);
		}
		// Check if this thing is selected
		else if(things[th].selected)
		{
			// Render the thing with selection color
			Render_ThingF(things[th].x, things[th].y, things[th].angle, CLR_THINGSELECTED, imagesize, fZoom, uiThingTex);
		}
		else
		{
			// Check if any of the thing flags match any of the filter flags
			if(ThingFiltered(&things[th], filterthings, filter))
			{
				// Render the thing with normal color
				Render_ThingF(things[th].x, things[th].y, things[th].angle, things[th].color, imagesize, fZoom, uiThingTex);
			}
		}
	}
}


// Render_AllThingsDarkened: Renders all things according to their properties, but darker
//----------------------------------------------------------------------------
void APIENTRY Render_AllThingsDarkened(MAPTHING* things, int startindex, int endindex, BYTE* thingbitmaps, int bitmapswidth, int imagesize, int filterthings, THINGFILTERS* filter)
{
	// Go for all things
	for(int th = startindex; th <= endindex; th++)
	{
		// Check if any of the thing flags match any of the filter flags
		if(ThingFiltered(&things[th], filterthings, filter))
		{
			// Render the thing with darker color
			Render_BitmapF(thingbitmaps, bitmapswidth, imagesize, things[th].image * imagesize, 0, imagesize, imagesize, things[th].x, things[th].y, things[th].color + 16, CLR_BACKGROUND);
		}
	}
}



// Render_ChangingLengths: Renders all numbers for changing lines
//----------------------------------------------------------------------------
void APIENTRY Render_ChangingLengths(MAPVERTEX* vertices, MAPLINEDEF* linedefs, int* changelines, int numchangelines, BYTE* bitmap, int width, int height, int charwidth, int charheight)
{
	int x1, y1, x2, y2;
	int l, ld;
	
	// Go for all changing lines
	for(l = 0; l < numchangelines; l++)
	{
		// Get lindef index
		ld = changelines[l];
		
		// Get the line coordinates
		x1 = vertices[linedefs[ld].v1].x;
		y1 = vertices[linedefs[ld].v1].y;
		x2 = vertices[linedefs[ld].v2].x;
		y2 = vertices[linedefs[ld].v2].y;
		
		Render_LineLengthF(x1, y1, x2, y2, bitmap, width, height, charwidth, charheight, CLR_LINEHIGHLIGHT);
	}
}


// DetermineLinedefColour: Determines the colour in which to draw a linedef.
//---------------------------------------------------------------------------
static inline int DetermineLinedefColour(MAPLINEDEF *ld, MAPSECTOR *sectors, MAPSIDEDEF *sidedefs)
{
	// Any sector containing a FOF gets the FOF colour.
	if((ld->s1 >= 0 && (sectors[sidedefs[ld->s1].sector].editflags & SED_HASFOF)) || (ld->s2 >= 0 && (sectors[sidedefs[ld->s2].sector].editflags & SED_HASFOF)))
		return CLR_FOFSECTOR;
	// Determine impassable (unless using the SRB2 impassable trick, which
	// falls through to the else clause).
	else if((ld->flags & LDF_IMPASSABLE) || ((ld->flags & LDF_TWOSIDED) == 0))
	{
		// Determine linedef color
		if(ld->effect != 0)
		{
			// Impassable line with effect
			return CLR_LINESPECIAL;
		}
		else
		{
			// Impassable line
			return CLR_LINE;
		}
	}
	else
	{
		// Determine linedef color
		if(ld->effect != 0)
		{
			// Double line with effect
			return CLR_LINESPECIALDOUBLE;
		}
		else if(ld->flags & LDF_BLOCKSOUND)
		{
			// Double line blocks sound
			return CLR_LINEBLOCKSOUND;
		}
		else if((ld->flags & LDF_TWOSIDED) && ld->s1 >= 0 && ld->s2 >= 0 &&
				 sidedefs[ld->s1].sector >= 0 && sidedefs[ld->s2].sector >= 0)
		{
			MAPSECTOR *front = &sectors[sidedefs[ld->s1].sector];
			MAPSECTOR *back = &sectors[sidedefs[ld->s2].sector];

			// If either side has same floor and ceiling, draw impassable.
			// Otherwise, if both sides have same floor, draw zero-height.
			// Otherwise, draw two-sided.
			return ((front->hceiling == front->hfloor) || (back->hceiling == back->hfloor)) ? CLR_LINE :
				((/*(front->hceiling == back->hceiling) &&*/ (front->hfloor == back->hfloor)) ? CLR_ZEROHEIGHTLINE : CLR_LINEDOUBLE);
		}
		else
		{
			// Double line
			return CLR_LINEDOUBLE;
		}
	}
}