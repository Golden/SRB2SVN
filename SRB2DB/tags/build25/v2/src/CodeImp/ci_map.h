#ifndef __SRB2B_CI_MAP__
#define __SRB2B_CI_MAP__


#ifdef __cplusplus
extern "C" {
#endif

#include <windows.h>
#include <gl\gl.h>
#include <gl\glu.h>

#include "../maptypes.h"

void Render_AllLinedefs(MAPVERTEX* vertices, MAPLINEDEF* linedefs, MAPSECTOR* sectors, MAPSIDEDEF *sidedefs, int startindex, int endindex, int indicatorlength);
void Render_AllVertices(MAPVERTEX* vertices, int startindex, int endindex, float vertexsize);
void Render_AllThings(MAPTHING* things, int startindex, int endindex, float imagesize, float fZoom, GLuint uiThingTex, int outlines, int filterthings, THINGFILTERS* filter);
void Rereference_Vertices(MAPLINEDEF* linedefs, int numlinedefs, int oldref, int newref);

#ifdef __cplusplus
}
#endif


#endif