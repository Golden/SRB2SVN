#ifndef __SRB2B_MAPCONFIG__
#define __SRB2B_MAPCONFIG__

#include <windows.h>

#include "config.h"

int LoadMapConfigs(void);
void UnloadMapConfigs(void);
void AddMapConfigsToComboBox(HWND hwndCombo);
CONFIG* GetThingConfigInfo(CONFIG *lpcfgThings, unsigned short unType);
void GetThingTypeDisplayText(unsigned short unType, CONFIG *lpcfgThings, LPTSTR szBuffer, unsigned short cchBuffer);
void GetThingDirectionDisplayText(short nDirection, LPTSTR szBuffer, unsigned short cbBuffer);
void GetEffectDisplayText(unsigned short unEffect, CONFIG *lpcfgSectors, LPTSTR szBuffer, unsigned short cchBuffer);

#endif
