#include <windows.h>

#include "general.h"
#include "maptypes.h"
#include "openwads.h"
#include "resource.h"

#include "win/mdiframe.h"
#include "win/mapwin.h"

/* Used when enumerating over a certain wad's windows. */
typedef struct _WADWINENUM
{
	WNDENUMPROC	lpEnumProc;
	LPARAM		lParam;
	int			iID;
} WADWINENUM;


static BOOL CALLBACK WinWadCallbackChain(HWND hwnd, LPARAM lParam);
static int AddWad(WAD *lpwad);
static int CountWadWindows(int iWadID);
static OPENWAD* GetOWFromPath(LPCTSTR szPath);


/* Linked list of all open wads. */
OPENWAD g_owOpenWadsHdr, *g_lpowEnd;



/* LoadWad
 *   Loads a wad from disc, if not already open.
 *
 * Parameters:
 *   LPCTSTR		szPath		Path to file to load.
 *
 * Return value: int
 *   ID of wad on success; negative on error.
 *
 * Notes:
 *   If the wad library runs out of memory, we return an error code.
 */
int LoadWad(LPCTSTR szPath)
{
	WAD *lpwad;
	OPENWAD *lpow;

	/* Is the file already open? */
	lpow = GetOWFromPath(szPath);
	if(lpow)
	{
		/* If so, increment the reference count and return the ID. */
		lpow->uiReferenceCount++;
		return lpow->iID;
	}
	
	/* Not open yet: actually load the wad now. */
	lpwad = OpenWAD(szPath);

	if(!lpwad)
	{
		/* Failed to open the wad. Bad file/sharing violation? */
		return -1;
	}

	return AddWad(lpwad);
}


/* GetWadIDFromPath
 *   Returns the open-wad structure for a given path.
 *
 * Parameters:
 *   LPCTSTR		szPath		Path to check for.
 *
 * Return value: int
 *   Address of open-wad struct on success; NULL if not found.
 *
 * Notes:
 *   Handy for checking whether a certain file is loaded.
 */
static OPENWAD* GetOWFromPath(LPCTSTR szPath)
{
	OPENWAD *lpowRover;

	lpowRover = g_owOpenWadsHdr.lpowNext;
	while(lpowRover && !PathsReferToSameFile(lpowRover->lpwad->filename, szPath)) lpowRover = lpowRover->lpowNext;

	return lpowRover;
}


/* GetWadPath
 *   Gets the path to the wad file for an open wad.
 *
 * Parameters:
 *   int		iWad		ID of wad.
 *   LPTSTR		szPath		Buffer to store path in.
 *   WORD		cbBuffer	Length of buffer, including terminator.
 *
 * Return value: int
 *   Total number of bytes required to store full path. Negative on error.
 *
 * Remarks:
 *   Calling with cbBuffer == 0 will simply return the required length.
 */
int GetWadPath(int iWad, LPTSTR szPath, WORD cbBuffer)
{
	WAD *lpwad = GetWad(iWad);

	if(cbBuffer > 0)
		lstrcpyn(szPath, lpwad->filename, cbBuffer);

	return lstrlen(lpwad->filename) + 1;
}


/* AddWad
 *   Adds a wad structure to the list of open wads.
 *
 * Parameters:
 *   WAD	*lpwad	Wad to add.
 *
 * Return value: int
 *   ID of wad on success; negative on error.
 */
static int AddWad(WAD *lpwad)
{
	static int iNextWadID = 0;
	OPENWAD *lpowNew;

	if(!lpwad) return -1;

	lpowNew = ProcHeapAlloc(sizeof(OPENWAD));
	
	lpowNew->lpwad = lpwad;
	lpowNew->iID = iNextWadID++;
	lpowNew->lpowNext = NULL;
	lpowNew->uiReferenceCount = 1;		/* Initial reference count is unity. */
	g_lpowEnd->lpowNext = lpowNew;
	g_lpowEnd = lpowNew;

	return lpowNew->iID;
}

/* NewWad
 *   Creates a new wad and opens it for editing.
 *
 * Parameters:
 *   None
 *
 * Return value: int
 *   ID of wad on success; negative on error.
 *
 * Notes:
 *   If the wad library runs out of memory, we return an error code.
 */
int NewWad(void)
{
	WAD *lpwad = CreateWAD();

	if(!lpwad)
	{
		/* Out of memory. Abort. */
		DIE(IDS_MEMORY);
		return -1;
	}

	return AddWad(lpwad);
}


/* GetWad
 *   Gets a pointer to an open wad given its ID.
 *
 * Parameters:
 *   int		iWad		ID of wad to find.
 *
 * Return value: WAD*
 *   Pointer to wad structure, or NULL if not found.
 */
WAD* GetWad(int iWad)
{
	OPENWAD *lpowRover;

	lpowRover = g_owOpenWadsHdr.lpowNext;
	while(lpowRover && lpowRover->iID != iWad) lpowRover = lpowRover->lpowNext;

	return lpowRover ? lpowRover->lpwad : NULL;
}



/* IncrementWadReferenceCount
 *   Increments the reference count for an open wad.
 *
 * Parameters:
 *   int		iWad		ID of wad.
 *
 * Return value: None.
 *
 * Remarks:
 *   Sometimes we want two references, and there's really no point in calling
 *   LoadWad twice. We might not even have the path handy by the time we decide
 *   we want another reference.
 */
void IncrementWadReferenceCount(int iWad)
{
	OPENWAD *lpowRover;

	lpowRover = g_owOpenWadsHdr.lpowNext;
	while(lpowRover && lpowRover->iID != iWad) lpowRover = lpowRover->lpowNext;

	if(lpowRover) lpowRover->uiReferenceCount++;
}


/* InitOpenWadsList
 *   Called at app startup to set up some pointers.
 *
 * Parameters:
 *   None.
 *
 * Return value:
 *   None.
 */
void InitOpenWadsList(void)
{
	g_owOpenWadsHdr.lpowNext = NULL;
	g_owOpenWadsHdr.lpwad = NULL;
	g_owOpenWadsHdr.uiReferenceCount = (unsigned)-1;
	g_owOpenWadsHdr.iID = -1;
	g_lpowEnd = &g_owOpenWadsHdr;
}


/* ReleaseWad
 *   Releases a reference to an open wad, and frees it if this was the last.
 *
 * Parameters:
 *   int	iID		ID of wad to release.
 *
 * Return value: int
 *   Zero on success; negative on error.
 */
int ReleaseWad(int iID)
{
	OPENWAD *lpow = &g_owOpenWadsHdr;
	OPENWAD *lpowPrev = &g_owOpenWadsHdr;

	while(lpow = lpow->lpowNext)
	{
		/* Do we have a match? */
		if(lpow->iID == iID)
		{
			/* Decrement the reference count. */
			lpow->uiReferenceCount--;

			/* Have all references been released? */
			if(lpow->uiReferenceCount == 0)
			{
				/* Close the hole in the list. */
				lpowPrev->lpowNext = lpow->lpowNext;
				if(!lpowPrev->lpowNext) g_lpowEnd = lpowPrev;

				/* Close the wad itself. */
				FreeWAD(lpow->lpwad);

				/* Free the memory used by our structure. */
				ProcHeapFree(lpow);
			}

			/* Done! */
			return 0;
		}
	}

	/* Not found. */
	return -1;
}



/* EnumChildWindowsByWad
 *   Calls a function for each child window belonging to a specified wad.
 *
 * Parameters:
 *   int			iID			ID of wad.
 *   WNDENUMPROC	lpEnumFunc	Function to call.
 *   LPARAM			lParam		Extra parameter to pass to enum function.
 *
 * Return value:
 *   None
 */
void EnumChildWindowsByWad(int iID, WNDENUMPROC lpEnumFunc, LPARAM lParam)
{
	WADWINENUM wwe;

	wwe.lParam = lParam;
	wwe.lpEnumProc = lpEnumFunc;
	wwe.iID = iID;

	EnumChildWindows(g_hwndClient, WinWadCallbackChain, (LPARAM)&wwe);
}

/* WinWadCallbackChain
 *   Does EnumChildWindowsByWad's dirty work for it.
 *
 * Parameters:
 *   HWND	hwnd	Window to check.
 *   LPARAM	lParam	Pointer to data to use for real enum function.
 *
 * Return value:
 *   TRUE if enumeration should continue; FALSE otherwise.
 */
static BOOL CALLBACK WinWadCallbackChain(HWND hwnd, LPARAM lParam)
{
	WADWINENUM *lpwwe = (WADWINENUM*)lParam;

	/* Belongs to us? */
	if(MapWinBelongsToWad(hwnd, lpwwe->iID))
	{
		/* If so, call the real enum procedure. */
		return lpwwe->lpEnumProc(hwnd, lpwwe->lParam);
	}
	else return TRUE;
}

static int CountWadWindows(int iWadID)
{
	int i = 0;
	EnumChildWindowsByWad(iWadID, IncrementInteger, (LPARAM)&i);
	return i;
}
