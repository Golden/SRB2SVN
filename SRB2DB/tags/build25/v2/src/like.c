#include <windows.h>

BOOL LikeA(LPCSTR sz, LPCSTR szPattern)
{
	/* Loop through the pattern string. */
	while(*szPattern)
	{
		switch(*szPattern)
		{
		case '?':
			/* Any character, but must exist. */
			if(!(*sz++)) return FALSE;
			break;

		case '#':
			/* Must be a digit. */
			if(*sz < '0' || *sz > '9') return FALSE;
			*sz++;
			break;

		case '[':
			{
				BOOL bNegate = FALSE;

				/* Class group. */
				szPattern++;		/* Skip opening bracket. */
				if(*szPattern == '!')	/* Negation? */
				{
					bNegate = TRUE;
					szPattern++;
				}

				while(*szPattern && *szPattern != ']')	/* Loop through class. */
				{
					if(szPattern[1] == '-')
					{
						if(!szPattern[2]) return FALSE;		/* Malformed. */
						if(*sz >= *szPattern && *sz <= szPattern[2])
						{
							/* Match in class. */
							if(bNegate) return FALSE;
							break;
						}

						/* Skip past part of char interval that's left after one
						 * advance.
						 */
						szPattern += 2;
					}
					/* Simple char. */
					else if(*szPattern == *sz)
					{
						if(bNegate) return FALSE;
						break;
					}

					/* Skip to next part of class. */
					szPattern++;
				}

				/* Did we get through the class and not find a match? */
				if(!bNegate && (!(*szPattern) || *szPattern == ']'))
					return FALSE;

				sz++;		/* Advance test string. */

				/* Read to end of class in pattern. */
				while(*szPattern && *szPattern != ']') szPattern++;
				if(!(*szPattern)) return FALSE;	/* Malformed. */
			}

			break;

		case '*':
			/* Firstly, get rid of all consecutive *s at this point. We're
			 * testing the tail of the pattern against all possible tails of the
			 * test string.
			 */
			while(*szPattern == '*') szPattern++;

			/* Test each tail until we find a match. */
			while(*sz && !LikeA(sz, szPattern)) sz++;

			/* We ALWAYS RETURN now! The recursive call took us to the end of
			 * the string. This return value is slightly sneaky. If the equality
			 * fails, *sz must be \0, since LikeA would've been tested and
			 * failed otherwise, causing another loop iteration. This indicates
			 * that we reached the end without finding a suitable tail. If,
			 * however, they're *both* \0, then we matched right to the end of
			 * the string with our *, and so we succeed.
			 */
			return *sz == *szPattern;

		default:
			/* Simple character. */
			if(*sz++ != *szPattern) return FALSE;
		}

		szPattern++;
	}

	/* Finished the pattern. */

	/* Only succeed if we've reached the end of the test string, too. */
	return !(*sz);
}
