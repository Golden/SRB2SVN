#include <windows.h>
#include <commdlg.h>

#include "general.h"

int CommDlgOpen(HWND hwnd, LPTSTR szBuffer, int cbBuffer, LPCTSTR szTitle, LPCTSTR szFilter, LPCTSTR szDefExt, LPCTSTR szInitFilename, int iFlags)
{
	OPENFILENAME ofn;

	ofn.Flags = iFlags;
	ofn.hwndOwner = hwnd;
	ofn.lpstrFilter = szFilter;
	ofn.nFilterIndex = 1;
	ofn.lpstrDefExt = szDefExt;
	ofn.lpstrFile = szBuffer;
	ofn.nMaxFile = cbBuffer;
	ofn.lpfnHook = NULL;
	ofn.lpstrCustomFilter = NULL;
	ofn.lpstrFileTitle = NULL;
	ofn.lpstrInitialDir = NULL;
	ofn.lpTemplateName = NULL;
	ofn.lpstrTitle = szTitle;
	ofn.lStructSize = sizeof(ofn);

	return GetOpenFileName(&ofn);;
}
