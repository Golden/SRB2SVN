#include <windows.h>
#include <commctrl.h>

#include "general.h"
#include "resource.h"
#include "renderer.h"
#include "openwads.h"
#include "options.h"
#include "keyboard.h"
#include "mapconfig.h"

#include "win/mdiframe.h"
#include "win/mapwin.h"

#include "../DockWnd/DockWnd.h"



/* WinMain
 *   Entry point.
 *
 * Parameters:
 *   HINSTANCE	hInstance			Instance handle for application.
 *   HINSTANCE  hPrevInstance		Zero.
 *   LPSTR		szCmdLine			String of all arguments passed on command
 *									line.
 *   int		iCmdShow			Constant determining initial window state.
 *
 * Return value: int
 *   Exit code.
 */

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR szCmdLine, int iCmdShow)
{
	MSG msg;
	INITCOMMONCONTROLSEX iccx;
	OSVERSIONINFO ovi;

	/* Initialise globals. */
	g_hInstance = hInstance;
	g_hProcHeap = GetProcessHeap();

	/* Prepare for concurrency. */
	InitializeCriticalSection(&g_cs);


	/* Check OS version. */
	ovi.dwOSVersionInfoSize = sizeof(ovi);
	GetVersionEx(&ovi);
	g_bWinNT = (ovi.dwPlatformId == VER_PLATFORM_WIN32_NT);

	/* XP and on required for Common Controls 6. */
	g_bHasCC6 = g_bWinNT && (ovi.dwMajorVersion > 5 || (ovi.dwMajorVersion == 5 && ovi.dwMinorVersion >= 1));

	
	/* Prepare some internal structures. */
	InitOpenWadsList();

	/* Register window classes. */
	RegisterMapWindowClass();

	/* Load the config file. */
	if(!LoadMainConfigurationFile())
	{
		/* Failed to open config file? */
		DIE(IDS_ERROR_LOADCONFIG);
		return 1;
	}

	/* Load map configurations. */
	LoadMapConfigs();

	/* Create the palette we use for drawing maps. */
	InitialiseRenderer();

	/* Initialise the Common Controls. */
	iccx.dwSize = sizeof(iccx);
	iccx.dwICC = ICC_BAR_CLASSES;
	InitCommonControlsEx(&iccx);

	/* Set up the docking library. */
	DockingInitialize(hInstance);

	/* Try to create the main window. */
	if(CreateMainWindow(iCmdShow) != 0) DIE(IDS_CREATEWINDOW);

	/* Was there a filename passed on the command line? */
	/* TODO: Make this slightly less stupid. */
	if(*szCmdLine) OpenWadForEditing(szCmdLine);

	/* Message loop. Exits on a WM_QUIT. */
	while(GetMessage(&msg, NULL, 0, 0))
	{
		if(!TranslateMDISysAccel(g_hwndClient, &msg) &&
		   !TranslateAccelerator(g_hwndMain, g_hAccel, &msg))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
	}

	/* Begin cleaning up. */
	DockingUnInitialize();
	ShutdownRenderer();
	UnloadMapConfigs();
	UnloadMainConfigurationFile();

	DestroyCustomAccelerator();

	DeleteCriticalSection(&g_cs);

	/* Exit code. */
	return msg.wParam;
}