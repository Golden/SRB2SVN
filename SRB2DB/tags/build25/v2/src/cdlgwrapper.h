#ifndef __SRB2B_CDLGWRAPPER__
#define __SRB2B_CDLGWRAPPER__

int CommDlgOpen(HWND hwnd, LPTSTR szBuffer, int cbBuffer, LPCTSTR szTitle, LPCTSTR szFilter, LPCTSTR szDefExt, LPCTSTR szInitFilename, int iFlags);

#endif