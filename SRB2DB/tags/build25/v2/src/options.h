#ifndef __SRB2B_OPTIONS__
#define __SRB2B_OPTIONS__

#include "config.h"

/* Types. */


/* Every time you update this table, alter the corresponding menu code map! */
typedef enum _SHORTCUTCODES
{
	SCK_EDITQUICKMOVE,
	SCK_ZOOMIN,
	SCK_ZOOMOUT,
	SCK_CENTREVIEW,
	SCK_EDITMOVE,
	SCK_EDITLINES,
	SCK_EDITSECTORS,
	SCK_EDITVERTICES,
	SCK_EDITTHINGS,
	SCK_EDIT3D,
	SCK_MAX
} SHORTCUTCODES;
/* Every time you update this table, alter the corresponding menu code map! */


typedef struct _RENDEREROPTIONS
{
	int		iIndicatorSize;
	int		iVertexSize;
	BOOL	bVerticesInLinesMode;
	BOOL	bVerticesInSectorsMode;
} RENDEREROPTIONS;

/* extern globals. */
extern CONFIG *g_lpcfgMain;
extern int g_iShortcutCodes[SCK_MAX];
extern RENDEREROPTIONS g_rendopts;

/* Prototypes. */
BOOL LoadMainConfigurationFile(void);
void UnloadMainConfigurationFile(void);

/* Macros. */
#define OPTDIR TEXT("conf/")

#endif
