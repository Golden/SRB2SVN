#include <windows.h>
#include <search.h>
#include <stdio.h>
#include <tchar.h>

#include "general.h"
#include "texture.h"
#include "openwads.h"

#include "../Wad/Wad.h"


/* Macros. */
#define COLUMN_END				0xFF
#define TNL_BUFSIZEINCREMENT	64


/* Types. */
typedef struct _ADDTOTEXCACHEDATA
{
	TEX_FORMAT	tf;
	WAD			*lpwad;
	TEXCACHE	*lptcHdr;
	RGBQUAD		*lprgbq;
} ADDTOTEXCACHEDATA;


typedef struct _TEXTURE_PATCH_DESCRIPTOR
{
	short		xOffset, yOffset;
	short		nPatchNum;
	short		nStepDir, nColourmap;
} TEXTURE_PATCH_DESCRIPTOR;


typedef struct _TEXTURE_ENTRY
{
	char		sTexName[8];
	short		nReserved1, nReserved2;
	short		cx, cy;
	short		nReserved3, nReserved4;
	short		nNumPatches;

	/* There are variably many of these. Declared thus for convenience. */
	TEXTURE_PATCH_DESCRIPTOR	tpd[1];
} TEXTURE_ENTRY;


typedef struct _DOOMPICTURE_HEADER
{
	short	cx, cy;
	short	xOffset, yOffset;

	/* There are variably many of these. Declared thus for convenience. */
	int		iDataOffsets[1];
} DOOMPICTURE_HEADER;


typedef struct _DOOMPICTURE_POST
{
	BYTE	byRowStart;
	BYTE	byNumPixels;
	BYTE	byReserved;

	/* There are variably many of these. Declared thus for convenience. */
	BYTE	byPixels[1];
} DOOMPICTURE_POST;


typedef struct _PNAMES
{
	int			iNumPatches;

	/* There are variably many of these. Declared thus for convenience. */
	char		lpsPatchName[1][8];
} PNAMES;


static void EnumTextures(WAD *lpwad, TEX_FORMAT tf, void (__stdcall *f)(const char *szTexName, void *lpvParam), void *lpvParam);
static void DoomPictureToDIB(BYTE *lpbyDIB, DOOMPICTURE_HEADER *lpdp, int cbImage, short cxDIB, short cyDIB, short xOffset, short yOffset);
static int CompareTextureNames(LPCTSTR sz1, LPCTSTR sz2);
static int CompareTextureNameHeads(LPCTSTR sz1, LPCTSTR sz2);


/* LoadTexture
 *   Loads a texture from a wad.
 *
 * Parameters:
 *   WAD*		lpwad				Wad from which to load the texture.
 *   LPCSTR		szTexName			Name of texture's lump.
 *   TEX_FORMAT	tf					Specifies whether flat or texture.
 *   RGBQUAD*	lprgbq				256-colour palette.
 *
 * Return value: TEXTURE*
 *   Pointer to newly-allocated texture.
 *
 * Remarks:
 *   The caller must call DestroyTexture to free the memory!
 */
TEXTURE* LoadTextureW(WAD *lpwad, LPCWSTR sTexName, TEX_FORMAT tf, RGBQUAD *lprgbq)
{
	WCHAR szTexNameW[TEXNAME_BUFFER_LENGTH];
	char szTexNameA[TEXNAME_BUFFER_LENGTH];

	/* Terminate. */
	CopyMemory(szTexNameW, sTexName, sizeof(WCHAR) * (TEXNAME_BUFFER_LENGTH - 1));
	szTexNameW[TEXNAME_BUFFER_LENGTH - 1] = L'\0';

	wsprintfA(szTexNameA, "%ls", szTexNameW);
	return LoadTextureA(lpwad, szTexNameA, tf, lprgbq);
}

TEXTURE* LoadTextureA(WAD *lpwad, LPCSTR sTexName, TEX_FORMAT tf, RGBQUAD *lprgbq)
{
	TEXTURE *lptex = NULL;
	BYTE *lpbyBits = NULL;

	switch(tf)
	{
	case TF_FLAT:
		{
			int	cb;

			/* TODO: Read the section from the config (?). */
			cb = GetLumpLength(lpwad, sTexName, "F_START");

			if(cb <= 0) break;

			lpbyBits = ProcHeapAlloc(cb);
			GetLump(lpwad, sTexName, "F_START", lpbyBits, cb);

			lptex = ProcHeapAlloc(sizeof(TEXTURE));

			/* There's probably a nice, integer way of doing this. */
			lptex->cx = lptex->cy = (int)sqrt(cb);

			lptex->xOffset = lptex->yOffset = 0;
		}

		/* Pointer to texture will be returned. */
		break;

	case TF_TEXTURE:
		{
			BYTE *lpbyTexture1, *lpbyTexture2;
			TEXTURE_ENTRY *lpte;
			PNAMES *lppnames;
			DOOMPICTURE_HEADER *lpdpPatch;
			int cbTexture1, cbTexture2, cbPnames, cbPatch;
			int cbTextureBitmap;
			int iTexCount, i;

			/* At least one of TEXTURE1 and TEXTURE2 must exist. */
			cbTexture1 = GetLumpLength(lpwad, "TEXTURE1", NULL);
			cbTexture2 = GetLumpLength(lpwad, "TEXTURE2", NULL);

			/* Make sure we have at least one of them. */
			if(max(cbTexture1, 0) + max(cbTexture2, 0) == 0) return NULL;

			/* PNAMES must exist, too. */
			cbPnames = GetLumpLength(lpwad, "PNAMES", NULL);
			if(cbPnames <= 0) break;

			/* Allocate room for and load each of them. */
			if(cbTexture1 > 0)
			{
				lpbyTexture1 = ProcHeapAlloc(cbTexture1);
				GetLump(lpwad, "TEXTURE1", NULL, lpbyTexture1, cbTexture1);
			} else lpbyTexture1 = NULL;

			if(cbTexture2 > 0)
			{
				lpbyTexture2 = ProcHeapAlloc(cbTexture2);
				GetLump(lpwad, "TEXTURE2", NULL, lpbyTexture2, cbTexture2);
			} else lpbyTexture2 = NULL;

			lppnames = ProcHeapAlloc(cbPnames);
			GetLump(lpwad, "PNAMES", NULL, (BYTE*)lppnames, cbPnames);


			/* Find the specified texture in either TEXTURE1 or TEXTURE2. */

			/* Assume we don't find it. */
			lpte = NULL;

			if(lpbyTexture1)
			{
				iTexCount = *((int*)lpbyTexture1);

				for(i=0; i<iTexCount; i++)
				{
					/* TEXTUREn has a table of offsets into itself, after a long
					 * indicating the no. entries.
					 */
					if(strncmp(&lpbyTexture1[((int*)lpbyTexture1)[1 + i]], sTexName, 8) == 0)
					{
						/* Found it! */
						lpte = (TEXTURE_ENTRY*)&lpbyTexture1[((int*)lpbyTexture1)[1 + i]];
						break;
					}
				}
			}

			/* Try TEXTURE2 if TEXTURE1 failed. */
			if(!lpte && lpbyTexture2)
			{
				iTexCount = *((int*)lpbyTexture2);

				for(i=0; i<iTexCount; i++)
				{
					/* TEXTUREn has a table of offsets into itself, after a long
					 * indicating the no. entries.
					 */
					if(strncmp(&lpbyTexture2[((int*)lpbyTexture2)[1 + i]], sTexName, 8) == 0)
					{
						/* Found it! */
						lpte = (TEXTURE_ENTRY*)&lpbyTexture2[((int*)lpbyTexture2)[1 + i]];
						break;
					}
				}
			}

			/* No match? Or bad texture descriptor? */
			if(!lpte || lpte->cx <= 0 || lpte->cy <= 0)
			{
				if(lpbyTexture1) ProcHeapFree(lpbyTexture1);
				if(lpbyTexture2) ProcHeapFree(lpbyTexture2);
				ProcHeapFree(lppnames);

				break;
			}

			/* From now on, we won't fail, even if we can't find a patch or it's
			 * bad. We just won't draw it.
			 */

			/* Allocate memory for the texture. */
			lptex = ProcHeapAlloc(sizeof(TEXTURE));

			/* DIBs must have widths of a multiple of 4. */
			lptex->cx = ((lpte->cx - 1) & ~3) + 4;
			lptex->cy = lpte->cy;
			cbTextureBitmap = lptex->cx * lptex->cy;
			lptex->xOffset = lptex->yOffset = 0;

			lpbyBits = ProcHeapAlloc(cbTextureBitmap);

			/* Clear the texture bitmap. */
			ZeroMemory(lpbyBits, cbTextureBitmap);

			/* Loop for each patch in the texture. */
			for(i=0; i < lpte->nNumPatches; i++)
			{
				/* Make sure patch is in range. And yes, there may be long many
				 * patches, but a texture can only reference short many.
				 */
				if(lpte->tpd[i].nPatchNum >= lppnames->iNumPatches) continue;

				cbPatch = GetLumpLength(lpwad, lppnames->lpsPatchName[lpte->tpd[i].nPatchNum], "P_START");

				/* Patch not found? */
				if(cbPatch <= 0) continue;

				lpdpPatch = ProcHeapAlloc(cbPatch);

				/* Load patch. */
				GetLump(lpwad, lppnames->lpsPatchName[lpte->tpd[i].nPatchNum], "P_START", (BYTE*)lpdpPatch, cbPatch);

				/* Blit! */
				DoomPictureToDIB(lpbyBits, lpdpPatch, cbPatch, lptex->cx, lptex->cy, lpte->tpd[i].xOffset, lpte->tpd[i].yOffset);

				/* Free patch. */
				ProcHeapFree(lpdpPatch);
			}

			/* Hooray! Finished blitting. Free memory and return. */

			if(lpbyTexture1) ProcHeapFree(lpbyTexture1);
			if(lpbyTexture2) ProcHeapFree(lpbyTexture2);
			ProcHeapFree(lppnames);
		}

		/* Pointer to texture will be returned. */
		break;

	case TF_IMAGE:
		{
			int cbImage, cbTextureBitmap;
			DOOMPICTURE_HEADER *lpdp;
			
			cbImage = GetLumpLength(lpwad, sTexName, NULL);

			/* Patch not found? */
			if(cbImage <= 0) return NULL;

			lpdp = ProcHeapAlloc(cbImage);

			/* Load image. */
			GetLump(lpwad, sTexName, NULL, (BYTE*)lpdp, cbImage);

			/* Allocate memory for the texture. */
			lptex = ProcHeapAlloc(sizeof(TEXTURE));

			/* DIBs must have widths of a multiple of 4. */
			lptex->cx = ((lpdp->cx - 1) & ~3) + 4;
			lptex->cy = lpdp->cy;
			cbTextureBitmap = lptex->cx * lptex->cy;
			lptex->xOffset = lptex->yOffset = 0;

			lpbyBits = ProcHeapAlloc(cbTextureBitmap);

			/* Clear the texture bitmap. */
			ZeroMemory(lpbyBits, cbTextureBitmap);

			/* Blit! */
			DoomPictureToDIB(lpbyBits, lpdp, cbImage, lptex->cx, lptex->cy, 0, 0);

			/* Free Doom-format image. */
			ProcHeapFree(lpdp);
		}

		/* Pointer to texture will be returned. */
		break;
	}

	if(lptex)
	{
		/* Create a DDB from the bits. */

		HDC hdc;
		BITMAPINFO *lpbmiDI;
		BITMAPINFOHEADER bmih;
		BITMAP bm;

		/* The BITMAPINFO structure requires a tail of 256-1 palette
		 * entries.
		 */
		lpbmiDI = ProcHeapAlloc(sizeof(BITMAPINFO) + 255 * sizeof(RGBQUAD));

		lpbmiDI->bmiHeader.biBitCount = 8;		/* 8bpp. */
		lpbmiDI->bmiHeader.biClrImportant = 0;	/* All are important. */
		lpbmiDI->bmiHeader.biClrUsed = 256;
		lpbmiDI->bmiHeader.biCompression = BI_RGB;
		lpbmiDI->bmiHeader.biPlanes = 1;
		lpbmiDI->bmiHeader.biSize = sizeof(BITMAPINFOHEADER);
		lpbmiDI->bmiHeader.biSizeImage = 0;
		lpbmiDI->bmiHeader.biWidth = lptex->cx;
		lpbmiDI->bmiHeader.biHeight = -lptex->cy;

		bmih.biBitCount = 32;		/* 32bpp. */
		bmih.biClrImportant = 0;	/* All are important. */
		bmih.biClrUsed = 0;
		bmih.biCompression = BI_RGB;
		bmih.biPlanes = 1;
		bmih.biSize = sizeof(BITMAPINFOHEADER);
		bmih.biSizeImage = 0;
		bmih.biWidth = lptex->cx;
		bmih.biHeight = lptex->cy;
		
		/* Copy the palette data. */
		CopyMemory(lpbmiDI->bmiColors, lprgbq, (CB_PLAYPAL / 3) * sizeof(RGBQUAD));

		hdc = GetDC(NULL);
		lptex->hbitmap = CreateDIBitmap(hdc, &bmih, CBM_INIT, lpbyBits, lpbmiDI, DIB_RGB_COLORS);
		GetObject(lptex->hbitmap, sizeof(bm), &bm);
		ReleaseDC(NULL, hdc);

		ProcHeapFree(lpbmiDI);
		ProcHeapFree(lpbyBits);
	}

	return lptex;
}



/* DestroyTexture
 *   Frees a texture.
 *
 * Parameters:
 *   TEXTURE*	lptex	Texture to free.
 *
 * Return value: None.
 *
 * Remarks:
 *   The texture would normally have been allocated by LoadTexture.
 */
void DestroyTexture(TEXTURE *lptex)
{
	DeleteObject(lptex->hbitmap);
	ProcHeapFree(lptex);
}


/* AddToTextureCache
 *   Adds a texture to a texture cache.
 *
 * Parameters:
 *   TEXCACHE*	lptcHdr		Header of texture cache.
 *   LPSTR		szLumpName	Name of texture.
 *   TEXTURE*	lptex		Texture to add.
 *
 * Return value: None.
 *
 * Remarks:
 *   No copying is done -- the pointer itself is used in the cache.
 */
void AddToTextureCache(TEXCACHE *lptcHdr, LPCSTR szLumpName, TEXTURE *lptex)
{
	TEXCACHE *lptcNew = ProcHeapAlloc(sizeof(TEXCACHE));

	/* Fill data. */
	lptcNew->lptexture = lptex;
	lstrcpyA(lptcNew->szLumpName, szLumpName);

	/* Add to the beginning of the list. This must be done atomically, or Bad
	 * Things may happen.
	 */
	EnterCriticalSection(&g_cs);
		lptcNew->lptcNext = lptcHdr->lptcNext;
		lptcHdr->lptcNext = lptcNew;
	LeaveCriticalSection(&g_cs);
}


/* EnumTextures
 *   Enumerates the names of all textures or flats in a wad.
 *
 * Parameters:
 *   WAD*			lpwad		Wad to enumerate lumps from.
 *   TEX_FORMAT		tf			Enumerate either flats or textures.
 *   BOOL (__stdcall *)(const char*, void*)	f
 *								Callback function.
 *   void*			lpvParam	Parameter to callback.
 *
 * Return value: None.
 */
static void EnumTextures(WAD *lpwad, TEX_FORMAT tf, void (__stdcall *f)(const char *szTexName, void *lpvParam), void *lpvParam)
{
	switch(tf)
	{
	case TF_FLAT:
		EnumLumpNamesBySection(lpwad, f, lpvParam, "F_START", "F_END");
		break;

	case TF_TEXTURE:
		{
			char szLumpName[9];
			int cbTexture1, cbTexture2;
			int iTexCount;
			int i;

			cbTexture1 = GetLumpLength(lpwad, "TEXTURE1", NULL);
			cbTexture2 = GetLumpLength(lpwad, "TEXTURE2", NULL);

			/* Since lstrcpynA won't always terminate it. */
			szLumpName[8] = '\0';
			
			if(cbTexture1 > 0)
			{
				BYTE *lpbyTexture1 = ProcHeapAlloc(cbTexture1);

				GetLump(lpwad, "TEXTURE1", NULL, lpbyTexture1, cbTexture1);

				iTexCount = *((int*)lpbyTexture1);

				for(i=0; i<iTexCount; i++)
				{
					/* TEXTUREn has a table of offsets into itself, after a long
					 * indicating the no. entries.
					 */
					lstrcpynA(szLumpName, &lpbyTexture1[((int*)lpbyTexture1)[1 + i]], 8);
					f(szLumpName, lpvParam);
				}

				ProcHeapFree(lpbyTexture1);
			}

			if(cbTexture2 > 0)
			{
				BYTE *lpbyTexture2 = ProcHeapAlloc(cbTexture2);

				GetLump(lpwad, "TEXTURE2", NULL, lpbyTexture2, cbTexture2);

				iTexCount = *((int*)lpbyTexture2);

				for(i=0; i<iTexCount; i++)
				{
					/* TEXTUREn has a table of offsets into itself, after a long
					 * indicating the no. entries.
					 */
					lstrcpynA(szLumpName, &lpbyTexture2[((int*)lpbyTexture2)[1 + i]], 8);
					f(szLumpName, lpvParam);
				}

				ProcHeapFree(lpbyTexture2);
			}
		}

		break;
	}
}


/* GetTextureFromCache
 *   Searches a cache for a particular texture.
 *
 * Parameters:
 *   TEXCACHE*	lptcHdr		Cache header node.
 *   LPTSTR		szTexName	Name of texture to search for.
 *
 * Return value: TEXTURE*
 *   Pointer to texture, or NULL if not found.
 *
 * Remarks:
 *   The cache texture itself is returned, so the caller must not destroy it.
 *   This represents a disparity with LoadTexture, which can sometimes make the
 *   calling code messy (only freeing if it wasn't in the cache), but the
 *   inefficiency of copying the texture isn't worthwhile.
 */
TEXTURE* GetTextureFromCacheW(TEXCACHE *lptcHdr, LPCWSTR szTexName)
{
	char szTexNameA[9];
	wsprintfA(szTexNameA, "%ls", szTexName);
	return GetTextureFromCacheA(lptcHdr, szTexNameA);
}

TEXTURE* GetTextureFromCacheA(TEXCACHE *lptcHdr, LPCSTR szTexName)
{
	TEXCACHE *lptcRover = lptcHdr;
	TEXCACHE *lptcPrev = lptcHdr;

	while(lptcRover = lptcRover->lptcNext)
	{
		if(lstrcmpA(lptcRover->szLumpName, szTexName) == 0)
		{
			/* Move match to front of list. */
			lptcPrev->lptcNext = lptcRover->lptcNext;
			lptcRover->lptcNext = lptcHdr->lptcNext;
			lptcHdr->lptcNext = lptcRover;

			return lptcRover->lptexture;
		}

		lptcPrev = lptcPrev->lptcNext;
	}

	return NULL;
}



/* PurgeTextureCache
 *   Frees all memory used by a texture cache.
 *
 * Parameters:
 *   TEXCACHE*	lptcHdr		Cache header node.
 *
 * Return value: None.
 */
void PurgeTextureCache(TEXCACHE *lptcHdr)
{
	/* Skip header. */
	TEXCACHE *lptcNext = lptcHdr->lptcNext;

	/* The caller expects this. */
	lptcHdr->lptcNext = NULL;

	while(lptcHdr = lptcNext)
	{
		lptcNext = lptcHdr->lptcNext;
		DestroyTexture(lptcHdr->lptexture);
		ProcHeapFree(lptcHdr);
	}
}



/* DoomPictureToDIB
 *   Converts a column-major Doom picture into a top-down DIB.
 *
 * Parameters:
 *   BYTE*			lpbyDIB				Buffer to store DIB in.
 *   DOOMPICTURE*	*lpdp				Doom picture structure.
 *   int			cbImage				Length of buffer pointed to by lpdp.
 *   short			cxDIB, cyDIB		Dimensions of buffer.
 *   short			xOffset, yOffset	Offset within DIB at which to blit.
 *
 * Return value: None.
 */
static void DoomPictureToDIB(BYTE *lpbyDIB, DOOMPICTURE_HEADER *lpdp, int cbImage, short cxDIB, short cyDIB, short xOffset, short yOffset)
{
	/* Part of offset into the data common to the whole patch. */
	int iPatchOffset = yOffset * cxDIB + xOffset;
	int cbTextureBitmap = cxDIB * cyDIB;
	int i;

	/* Blit patch to texture. Loop for each column. */
	int iColLimit = min(lpdp->cx, cxDIB - xOffset);

	for(i = max(0, -xOffset); i < iColLimit; i++)
	{
		/* Part of offset into the data common to this column. */
		int iColOffset = iPatchOffset + i;

		/* Find this column's first post. */
		DOOMPICTURE_POST *lppost = (DOOMPICTURE_POST*)&((BYTE*)lpdp)[lpdp->iDataOffsets[i]];

		/* Loop until we get to the column-end magic number. */
		while(lppost->byRowStart != COLUMN_END)
		{
			int iPostOffset = iColOffset + lppost->byRowStart * cxDIB;
			int j, iRowLimit;

			/* Clipping. */
			iRowLimit = min(lppost->byNumPixels, cyDIB - yOffset);
			for(j = max(0, -yOffset); j < iRowLimit; j++)
			{
				/* Skies are very strange. They seem to have another post
				 * following the normal one, which purports to have full height,
				 * but only actually lasts a byte before we see what looks like
				 * a COLUMN_END marker (even though it's in the middle of a
				 * post) whereafter you can read synchronously again. The offset
				 * directory also resyncs there. So, as a workaround, just check
				 * some buffer boundaries.
				 */
				if(iPostOffset + j * cxDIB >= cbTextureBitmap) continue;
				
				/* TODO: If this proves to be right, move this one into
				 * iRowLimit.
				 */
				if(lpdp->iDataOffsets[i] + j >= cbImage) break;

				lpbyDIB[iPostOffset + j * cxDIB] = lppost->byPixels[j];
			}

			/* Move to next post. Skip the unused byte at end. */
			lppost = (DOOMPICTURE_POST*)&lppost->byPixels[lppost->byNumPixels + 1];
		}
	}
}


/* CreateTextureNameList
 *   Creates a new texture name list structure.
 *
 * Parameters:
 *   None.
 *
 * Return value: TEXTURENAMELIST*
 *   Pointer to new structure.
 *
 * Remarks:
 *   The caller should call DestroyTextureNameList to free the memory.
 */
TEXTURENAMELIST* CreateTextureNameList(void)
{
	TEXTURENAMELIST *lptnl = ProcHeapAlloc(sizeof(TEXTURENAMELIST));

	lptnl->iEntries = 0;
	lptnl->cstrBuffer = TNL_BUFSIZEINCREMENT;
	lptnl->szNames = ProcHeapAlloc(lptnl->cstrBuffer * sizeof(TCHAR) * TEXNAME_BUFFER_LENGTH);

	return lptnl;
}


/* DestroyTextureNameList
 *   Destroys a texture name list structure.
 *
 * Parameters:
 *   TEXTURENAMELIST*	lptnl	List to destroy.
 *
 * Return value: None.
 */
void DestroyTextureNameList(TEXTURENAMELIST *lptnl)
{
	ProcHeapFree(lptnl->szNames);
	ProcHeapFree(lptnl);
}


/* AddTextureNameToList
 *   Adds a texture name to a texture name list.
 *
 * Parameters:
 *   const char*	sTexName		Name of texture. Not NULL-terminated.
 *   void*			lpvParam		(TEXTURENAMELIST*) List to add to.
 *
 * Return value: None.
 *
 * Remarks:
 *   The list is extended if necessary. This is intended to be used as a
 *   callback by EnumTextures.
 */
void __stdcall AddTextureNameToList(const char *sTexName, void *lpvParam)
{
	TEXTURENAMELIST *lptnl = (TEXTURENAMELIST*)lpvParam;
	CHAR szTexNameA[TEXNAME_BUFFER_LENGTH];
	TCHAR szTexNameT[TEXNAME_BUFFER_LENGTH];

	if(lptnl->iEntries == lptnl->cstrBuffer)
	{
		/* No more room in buffer; make it bigger. */
		lptnl->cstrBuffer += TNL_BUFSIZEINCREMENT;
		lptnl->szNames = ProcHeapReAlloc(lptnl->szNames, lptnl->cstrBuffer * sizeof(TCHAR) * TEXNAME_BUFFER_LENGTH);
	}

	/* Terminate the string. */
	CopyMemory(szTexNameA, sTexName, TEXNAME_BUFFER_LENGTH - 1);
	szTexNameA[TEXNAME_BUFFER_LENGTH - 1] = '\0';

	/* Convert string from ANSI if necessary. */
	wsprintf(szTexNameT, TEXT("%hs"), szTexNameA);
	
	/* Add to list. */
	lstrcpy(&lptnl->szNames[TEXNAME_BUFFER_LENGTH * lptnl->iEntries++], szTexNameT);
}


/* AddAllTextureNamesToList
 *   Adds all texture names from a wad to a texture name list.
 *
 * Parameters:
 *   TEXNAMELIST*	lptnl	List to add to.
 *   WAD*			lpwad	Wad from which to add textures.
 *   TEX_FORMAT		tf		Flats or textures?
 *
 * Return value: None.
 */
void AddAllTextureNamesToList(TEXTURENAMELIST *lptnl, WAD *lpwad, TEX_FORMAT tf)
{
	EnumTextures(lpwad, tf, AddTextureNameToList, lptnl);
}


/* SortTextureNameList, CompareTextureNames
 *   Sorts a list of texture names.
 *
 * Parameters:
 *   TEXTURENAMELIST*	lptnl	List to sort.
 *
 * Return value: None.
 */
void SortTextureNameList(TEXTURENAMELIST *lptnl)
{
	qsort(lptnl->szNames, lptnl->iEntries, TEXNAME_BUFFER_LENGTH * sizeof(TCHAR), CompareTextureNames);
}

static int CompareTextureNames(LPCTSTR sz1, LPCTSTR sz2)
{
	return lstrcmp(sz1, sz2);
}

LPCTSTR FindFirstTexNameMatch(LPCTSTR szTexName, TEXTURENAMELIST *lptnl)
{
	return _lfind(szTexName, lptnl->szNames, &lptnl->iEntries, TEXNAME_BUFFER_LENGTH * sizeof(TCHAR), CompareTextureNameHeads);
}

static int CompareTextureNameHeads(LPCTSTR sz1, LPCTSTR sz2)
{
	int iLen = min(lstrlen(sz1), lstrlen(sz2));
	return _tcsncmp(sz1, sz2, iLen);
}



void StretchTextureToDC(TEXTURE *lptex, HDC hdcTarget, int cx, int cy)
{
	int xStretch, yStretch, cxStretch, cyStretch;
	HDC hdcSource;

	/* Just lose the intermediate pixels. */
	SetStretchBltMode(hdcTarget, COLORONCOLOR);

	if(lptex->cx == cx && lptex->cy == cy)
	{
		/* Preview control and texture are in same ratio. */

		xStretch = 0;
		yStretch = 0;
		cxStretch = cx;
		cyStretch = cy;
	}
	else
	{
		HBRUSH hbrushOld;

		/* Preview control and texture are in different ratios.
		 * Centre the latter in the former.
		 */

		if(lptex->cx * cy > lptex->cy * cx)
		{
			/* Texture is wider than it is high, with respect to the
			 * ratio of the preview control.
			 */

			cxStretch = cx;
			cyStretch = (lptex->cy * cy) / lptex->cx;
			xStretch = 0;
			yStretch = (cy - cyStretch) >> 1;
		}
		else
		{
			/* Texture is higher than it is wide, with respect to the
			 * ratio of the preview control.
			 */

			cyStretch = cy;
			cxStretch = (lptex->cx * cx) / lptex->cy;
			yStretch = 0;
			xStretch = (cx - cxStretch) >> 1;
		}

		/* Clear the bitmap first, since we don't fill the whole thing. */
		hbrushOld = SelectObject(hdcTarget, GetStockObject(BLACK_BRUSH));
		Rectangle(hdcTarget, 0, 0, cx, cy);
		SelectObject(hdcTarget, hbrushOld);
	}


	hdcSource = CreateCompatibleDC(NULL);
	SelectObject(hdcSource, lptex->hbitmap);
	StretchBlt(	hdcTarget,
				xStretch, yStretch, cxStretch, cyStretch,
				hdcSource, 0, 0, lptex->cx, lptex->cy,
				SRCCOPY);

	DeleteDC(hdcSource);
}
