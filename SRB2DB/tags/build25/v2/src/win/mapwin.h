#ifndef __SRB2B_MAPWIN__
#define __SRB2B_MAPWIN__

#include <windows.h>
#include <gl\gl.h>
#include <gl\glu.h>

#include "../maptypes.h"
#include "../config.h"
#include "../selection.h"
#include "../texture.h"

#include "../CodeImp/ci_const.h"

/* Types. */

typedef struct _SELECTION
{
	SELECTION_LIST *lpsellistSectors, *lpsellistLinedefs, *lpsellistVertices, *lpsellistThings;
} SELECTION;


/* Function prototypes. */
HWND CreateMapWindow(MAP *lpmap, LPSTR szLumpName, CONFIG *lpcfgMap, int iWadID, int iIWadID);
int RegisterMapWindowClass(void);
BOOL MapWinBelongsToWad(HWND hwnd, int iWadID);
BOOL GetTextureForMap(HWND hwnd, LPTSTR szTexName, TEXTURE **lplptex, TEX_FORMAT tf);

/* Macros. */
#define MAPWINCLASS TEXT("SRB2BuilderMap")

#endif
