#ifndef __SRB2B_EDITDLG__
#define __SRB2B_EDITDLG__

#include <windows.h>

#include "../map.h"
#include "../selection.h"
#include "../config.h"

#include "mapwin.h"


/* Types. */
typedef struct _MAPPPDATA
{
	HWND				hwndMap;
	MAP					*lpmap;
	SELECTION			*lpselection;
	CONFIG				*lpcfgMap;
	TEXTURENAMELIST		*lptnlFlats, *lptnlTextures;
} MAPPPDATA;

enum ENUM_MAPPPFLAGS
{
	MPPF_SECTOR,
	MPPF_LINEDEF,
	MPPF_SIDEDEF,
	MPPF_THING,
	MPPF_VERTEX
};


/* Prototypes. */
void ShowMapObjectProperties(DWORD dwPageFlags, LPCTSTR szCaption, MAPPPDATA *lpmapppdata);


#endif