#ifndef __SRB2B_TEXBROWSER__
#define __SRB2B_TEXBROWSER__

#include <windows.h>

#include "../general.h"
#include "../texture.h"

BOOL SelectTexture(HWND hwndParent, HWND hwndMap, TEX_FORMAT tf, TEXTURENAMELIST *lptnlAll, TEXTURENAMELIST *lptnlUsed, LPTSTR szTexName);

#endif
