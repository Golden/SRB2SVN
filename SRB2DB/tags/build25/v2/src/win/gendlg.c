#include <windows.h>
#include <tchar.h>
#include <stdio.h>

#include "../general.h"


/* TransDlgProc
 *   Window procedure for mouse-transparent dialogue boxes.
 *
 * Parameters:
 *   HWND		hwnd		Window handle for message.
 *   UINT		uiMessage	ID for the message.
 *   WPARAM		wParam		Message-specific.
 *   LPARAM		lParam		Message-specific.
 *
 * Return value: LRESULT
 *   Message-specific.
 *
 * Remarks:
 *   Subclass the dialogue with this. Necessary since dialogue procs have no
 *   means to process WM_NCHITTEST usefully.
 */
LRESULT CALLBACK TransDlgProc(HWND hwndDlg, UINT uiMsg, WPARAM wParam, LPARAM lParam)
{
	switch(uiMsg)
	{
	case WM_NCHITTEST:
		{
			/* Get default HT result. */
			LRESULT lrHT = DefDlgProc(hwndDlg, uiMsg, wParam, lParam);

			/* If we're in the client are, fall through to the parent. */
			return (lrHT == HTCLIENT) ? HTTRANSPARENT : lrHT;
		}
	}

	/* Default behaviour. */
	return DefDlgProc(hwndDlg, uiMsg, wParam, lParam);
}



/* ListBoxSearchByItemData
 *   Searches a list box on its members' item data.
 *
 * Parameters:
 *   HWND		hwndListBox	Handle to list box.
 *   int		iItemData	Value to search for.
 *   BOOL		bSelect		Also select the match.
 *
 * Return value: int
 *   Index of first match, or negative if no match found.
 *
 * Remarks:
 *   Search ends after the first match.
 */
int ListBoxSearchByItemData(HWND hwndListBox, int iItemData, BOOL bSelect)
{
	int i, iCount;

	/* Find limit for looping through box. */
	iCount = SendMessage(hwndListBox, LB_GETCOUNT, 0, 0);

	for(i = 0; i < iCount; i++)
	{
		if(iItemData == SendMessage(hwndListBox, LB_GETITEMDATA, i, 0))
		{
			if(bSelect) SendMessage(hwndListBox, LB_SETCURSEL, i, 0);
			return i;
		}
	}

	/* No match. */
	if(bSelect) SendMessage(hwndListBox, LB_SETCURSEL, -1, 0);
	return -1;
}


/* BoundEditBox
 *   Bounds an edit-box's text between two ints.
 *
 * Parameters:
 *   HWND		hwndEditBox	Handle to edit box.
 *   int		iMin, iMax	Bounding range.
 *   BOOL		bPreserve	Leave empty boxes be?
 *
 * Return value: BOOL
 *   TRUE if box text was changed; FALSE otherwise.
 */
BOOL BoundEditBox(HWND hwndEditBox, int iMin, int iMax, BOOL bPreserveEmpty)
{
	int cch, i, j;
	LPSTR sz, szEnd;

	cch = GetWindowTextLength(hwndEditBox) + 1;
	sz = ProcHeapAlloc(cch * sizeof(TCHAR));
	GetWindowText(hwndEditBox, sz, cch);

	/* As a special case, preserve empty boxes. */
	if(bPreserveEmpty && *sz == '\0')
	{
		ProcHeapFree(sz);
		return FALSE;
	}

	i = _tcstol(sz, &szEnd, 10);
	j = min(max(i, iMin), iMax);
	wsprintf(sz, "%d", j);
	SetWindowText(hwndEditBox, sz);

	ProcHeapFree(sz);

	return *szEnd || i != j;
}
