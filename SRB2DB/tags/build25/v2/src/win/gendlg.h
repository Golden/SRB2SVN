#ifndef __SRB2B_GENDLG__
#define __SRB2B_GENDLG__

LRESULT CALLBACK TransDlgProc(HWND hwndDlg, UINT uiMsg, WPARAM wParam, LPARAM lParam);
int ListBoxSearchByItemData(HWND hwndListBox, int iItemData, BOOL bSelect);
BOOL BoundEditBox(HWND hwndEditBox, int iMin, int iMax, BOOL bPreserveEmpty);

#endif
