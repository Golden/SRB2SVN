#include <windows.h>
#include <commctrl.h>

#include <tchar.h>
#include <stdio.h>

#include "../general.h"
#include "../map.h"
#include "../selection.h"
#include "../config.h"
#include "../options.h"
#include "../editing.h"
#include "../texture.h"
#include "../resource.h"

#include "editdlg.h"
#include "mapwin.h"
#include "mdiframe.h"
#include "infobar.h"
#include "gendlg.h"
#include "texbrowser.h"


/* Types. */
typedef struct _TPPDATA
{
	WNDPROC	wndprocStatic;
	USHORT	unDlgItemID;
} TPPDATA;

/* Static prototypes. */
static BOOL CALLBACK SectorsPropPageProc(HWND hwndPropPage, UINT uiMsg, WPARAM wParam, LPARAM lParam);
static void InitSectorsPropPageFields(HWND hwndPropPage, SECTORDISPLAYINFO *lpsdi, DWORD dwCheckFlags);
static BOOL AddSectorTypeToList(CONFIG *lpcfg, void *lpvWindow);
static LRESULT CALLBACK TexPreviewProc(HWND hwnd, UINT uiMsg, WPARAM wParam, LPARAM lParam);


/* ShowMapObjectProperties
 *   Shows the Properties dialogue for the selection.
 *
 * Parameters:
 *   DWORD		dwPageFlags		Flags determining which pages to show.
 *   LPCTSTR	szCaption		Window caption, sans " Properties".
 *   MAPPPDATA*	lpmapppdata		Selection info and map data.
 *
 * Return value: None.
 *   Message-specific.
 *
 * Remarks:
 *   All the processing is done by the property sheet pages. This function just
 *   shows the dialogue box.
 */
void ShowMapObjectProperties(DWORD dwPageFlags, LPCTSTR szCaption, MAPPPDATA *lpmapppdata)
{
	PROPSHEETHEADER		psh;
	PROPSHEETPAGE		psp[5];
	int					iNumPages, i;


	/* Check which pages were requested, and fill in the necessary info. */

	iNumPages = 0;

	if(dwPageFlags | MPPF_SECTOR)
	{
		psp[iNumPages].pfnDlgProc = SectorsPropPageProc;
		/* TODO: 1.1 */
		psp[iNumPages].pszTemplate = MAKEINTRESOURCE(IDD_PP_SECTORS_109);

		iNumPages++;
	}

	/* Fill in fields common to all pages. */
	for(i = 0; i < iNumPages; i++)
	{
		psp[i].dwSize = sizeof(PROPSHEETPAGE);
		psp[i].dwFlags = /*PSP_HASHELP */ 0;
		psp[i].lParam = (LONG)lpmapppdata;
		psp[i].hInstance = g_hInstance;
	}
	
	/* Properties affecting the whole dialogue. */
	psh.dwSize = sizeof(psh);
	psh.dwFlags = PSH_HASHELP | PSH_NOAPPLYNOW | PSH_PROPTITLE | PSH_PROPSHEETPAGE;
	psh.hwndParent = g_hwndMain;
	psh.nPages = iNumPages;
	psh.nStartPage = 0;
	psh.ppsp = psp;
	psh.pszCaption = szCaption;

	/* Go! */
	PropertySheet(&psh);
	i = GetLastError();
}



/* SectorsPropPageProc
 *   Dialogue proc for sectors property page.
 *
 * Parameters:
 *   HWND	hwndPropPage	Property page window handle.
 *   UINT	uiMsg			Message identifier.
 *   WPARAM wParam			Message-specific.
 *   LPARAM lParam			Message-specific.
 *
 * Return value: BOOL
 *   TRUE if the message was processed; FALSE otherwise.
 */
static BOOL CALLBACK SectorsPropPageProc(HWND hwndPropPage, UINT uiMsg, WPARAM wParam, LPARAM lParam)
{
	static MAPPPDATA *s_lpmapppdata;
	static BOOL s_bInitComplete = FALSE;
	static HBITMAP s_hbmCeil, s_hbmFloor;

	/* Not hugely elegant, but the alternative is subclassing... */
	static TCHAR s_szPrevTFloor[TEXNAME_BUFFER_LENGTH], s_szPrevTCeil[TEXNAME_BUFFER_LENGTH];

	switch(uiMsg)
	{
	case WM_INITDIALOG:
		{
			SECTORDISPLAYINFO sdi;
			DWORD dwCheckFlags;
			CONFIG *lpcfgSecTypes;
			UDACCEL udaccelHeight[2], udaccelOther[1];
			int i;
			short nMaxAdjCeil, nMinAdjCeil, nMaxAdjFloor, nMinAdjFloor;
			HDC hdcDlg;

			/* Get the map data. */
			s_lpmapppdata = (MAPPPDATA*)((PROPSHEETPAGE*)lParam)->lParam;

			/* Set the texture previews' bitmaps. */
			hdcDlg = GetDC(hwndPropPage);
			s_hbmCeil = CreateCompatibleBitmap(hdcDlg, CX_TEXPREVIEW, CY_TEXPREVIEW);
			s_hbmFloor = CreateCompatibleBitmap(hdcDlg, CX_TEXPREVIEW, CY_TEXPREVIEW);
			ReleaseDC(hwndPropPage, hdcDlg);
			SendDlgItemMessage(hwndPropPage, IDC_TEX_CEIL, STM_SETIMAGE, IMAGE_BITMAP, (LPARAM)s_hbmCeil);
			SendDlgItemMessage(hwndPropPage, IDC_TEX_FLOOR, STM_SETIMAGE, IMAGE_BITMAP, (LPARAM)s_hbmFloor);

			/* Subclass the texture preview controls. */
			{
				HWND hwndTexCeil = GetDlgItem(hwndPropPage, IDC_TEX_CEIL);
				HWND hwndTexFloor = GetDlgItem(hwndPropPage, IDC_TEX_FLOOR);

				/* Allocate memory for the preview windows. It's freed when
				 * they're destroyed.
				 */
				TPPDATA *lptppdCeil = ProcHeapAlloc(sizeof(TPPDATA));
				TPPDATA *lptppdFloor = ProcHeapAlloc(sizeof(TPPDATA));

				lptppdCeil->wndprocStatic = (WNDPROC)GetWindowLong(hwndTexCeil, GWL_WNDPROC);
				lptppdCeil->unDlgItemID = IDC_TEX_CEIL;
				lptppdFloor->wndprocStatic = (WNDPROC)GetWindowLong(hwndTexFloor, GWL_WNDPROC);
				lptppdFloor->unDlgItemID = IDC_TEX_FLOOR;

				SetWindowLong(hwndTexCeil, GWL_USERDATA, (LONG)lptppdCeil);
				SetWindowLong(hwndTexFloor, GWL_USERDATA, (LONG)lptppdFloor);

				SetWindowLong(hwndTexCeil, GWL_WNDPROC, (LONG)TexPreviewProc);
				SetWindowLong(hwndTexFloor, GWL_WNDPROC, (LONG)TexPreviewProc);
			}


			/* Sector types from map configuration. */
			lpcfgSecTypes = ConfigGetSubsection(s_lpmapppdata->lpcfgMap, "sectortypes");

			/* Find info common to all selected sectors. */
			dwCheckFlags = CheckSectors(
							s_lpmapppdata->lpmap,
							s_lpmapppdata->lpselection->lpsellistSectors,
							lpcfgSecTypes,
							&sdi);

			/* Fill the list of sector effects. */
			ConfigIterate(lpcfgSecTypes, AddSectorTypeToList, GetDlgItem(hwndPropPage, IDC_LIST_EFFECTS));

			/* Set ranges and increments on the spinners. */

			udaccelHeight[0].nInc = 8;
			udaccelHeight[0].nSec = 0;
			udaccelHeight[1].nInc = 64;
			udaccelHeight[1].nSec = 2;

			udaccelOther[0].nInc = 1;
			udaccelOther[0].nSec = 2;

			SendDlgItemMessage(hwndPropPage, IDC_SPIN_TAG, UDM_SETRANGE32, 0, 65535);
			SendDlgItemMessage(hwndPropPage, IDC_SPIN_TAG, UDM_SETACCEL, 1, (LPARAM)udaccelOther);

			SendDlgItemMessage(hwndPropPage, IDC_SPIN_HCEIL, UDM_SETRANGE32, -32768, 32767);
			SendDlgItemMessage(hwndPropPage, IDC_SPIN_HCEIL, UDM_SETACCEL, 2, (LPARAM)udaccelHeight);

			SendDlgItemMessage(hwndPropPage, IDC_SPIN_HFLOOR, UDM_SETRANGE32, -32768, 32767);
			SendDlgItemMessage(hwndPropPage, IDC_SPIN_HFLOOR, UDM_SETACCEL, 2, (LPARAM)udaccelHeight);

			/* Add some brightnesses to the combo. */
			for(i=16; i>=0; i--)
			{
				TCHAR szNum[4];
				wsprintf(szNum, TEXT("%d"), min(16*i, 255));
				SendDlgItemMessage(hwndPropPage, IDC_COMBO_LIGHT, CB_ADDSTRING, FALSE, (LPARAM)szNum);
			}
			

			/* Set up the brightness slider. */
			SendDlgItemMessage(hwndPropPage, IDC_SLIDER_LIGHT, TBM_SETBUDDY, FALSE, (LPARAM)GetDlgItem(hwndPropPage, IDC_COMBO_LIGHT));
			SendDlgItemMessage(hwndPropPage, IDC_SLIDER_LIGHT, TBM_SETTICFREQ, 16, 0);
			SendDlgItemMessage(hwndPropPage, IDC_SLIDER_LIGHT, TBM_SETRANGE, FALSE, MAKELONG(0, 255));

			/* Set fields of the property page. */
			InitSectorsPropPageFields(hwndPropPage, &sdi, dwCheckFlags);

			/* Fill the adjacent sector info. */
			if(GetAdjacentSectorHeightRange(s_lpmapppdata->lpmap, &nMaxAdjCeil, &nMinAdjCeil, &nMaxAdjFloor, &nMinAdjFloor))
			{
				/* We had some adjacent sectors. */
				TCHAR szCeilRange[17], szFloorRange[17];

				if(nMinAdjCeil != nMaxAdjCeil)
					_sntprintf(szCeilRange, sizeof(szCeilRange) / sizeof(TCHAR), TEXT("[%d, %d]"), nMinAdjCeil, nMaxAdjCeil);
				else
					_sntprintf(szCeilRange, sizeof(szCeilRange) / sizeof(TCHAR), TEXT("%d"), nMinAdjCeil);

				if(nMinAdjFloor != nMaxAdjFloor)
					_sntprintf(szFloorRange, sizeof(szFloorRange) / sizeof(TCHAR), TEXT("[%d, %d]"), nMinAdjFloor, nMaxAdjFloor);
				else
					_sntprintf(szFloorRange, sizeof(szFloorRange) / sizeof(TCHAR), TEXT("%d"), nMinAdjFloor);

				SetDlgItemText(hwndPropPage, IDC_STATIC_CEILRANGE, szCeilRange);
				SetDlgItemText(hwndPropPage, IDC_STATIC_FLRRANGE, szFloorRange);
			}
			else
			{
				/* No adjacent sectors. */
				SetDlgItemText(hwndPropPage, IDC_STATIC_CEILRANGE, TEXT("-"));
				SetDlgItemText(hwndPropPage, IDC_STATIC_FLRRANGE, TEXT("-"));
			}
		}

		s_bInitComplete = TRUE;

		/* Let the system set the focus. */
		return TRUE;

	case WM_COMMAND:

		switch(LOWORD(wParam))
		{
		case IDC_BUTTON_NEXTTAG:
			SetDlgItemInt(hwndPropPage, IDC_EDIT_TAG, NextUnusedTag(s_lpmapppdata->lpmap), FALSE);
			return TRUE;

		case IDC_COMBO_LIGHT:
			switch(HIWORD(wParam))
			{
			case CBN_SELCHANGE:
				{
					/* Update the slider to reflect the combo box. */

					int iIndex, cchBuffer;
					LPTSTR sz;
					
					iIndex = SendDlgItemMessage(hwndPropPage, IDC_COMBO_LIGHT, CB_GETCURSEL, 0, 0);
					cchBuffer = SendDlgItemMessage(hwndPropPage, IDC_COMBO_LIGHT, CB_GETLBTEXTLEN, iIndex, 0) + 1;
					sz = ProcHeapAlloc(cchBuffer * sizeof(TCHAR));

					SendDlgItemMessage(hwndPropPage, IDC_COMBO_LIGHT, CB_GETLBTEXT, iIndex, (LPARAM)sz);
					SendDlgItemMessage(hwndPropPage, IDC_SLIDER_LIGHT, TBM_SETPOS, TRUE, _tcstol(sz, NULL, 10));

					ProcHeapFree(sz);
				}

				return TRUE;

			case CBN_EDITUPDATE:
				/* Validate. */
				BoundEditBox(GetDlgItem(hwndPropPage, LOWORD(wParam)), 0, 255, TRUE);
		
				/* Update the slider to reflect the combo box. */
				SendDlgItemMessage(hwndPropPage, IDC_SLIDER_LIGHT, TBM_SETPOS, TRUE, GetDlgItemInt(hwndPropPage, IDC_COMBO_LIGHT, NULL, FALSE));
				return TRUE;
			}

			break;

		case IDC_TEX_CEIL:
		case IDC_TEX_FLOOR:

			switch(HIWORD(wParam))
			{
			case BN_CLICKED:
				{
					TCHAR szTexName[TEXNAME_BUFFER_LENGTH];
					UINT uiEditBoxID = (LOWORD(wParam) == IDC_TEX_FLOOR) ? IDC_EDIT_TFLOOR : IDC_EDIT_TCEIL;

					/* Get the current flat so it's selected initially. */
					GetDlgItemText(hwndPropPage, uiEditBoxID, szTexName, TEXNAME_BUFFER_LENGTH);

					/* Show the texture browser. */
					if(SelectTexture(hwndPropPage, s_lpmapppdata->hwndMap, TF_FLAT, s_lpmapppdata->lptnlFlats, NULL, szTexName))
					{
						/* User didn't cancel. Update the edit-box. */
						SetDlgItemText(hwndPropPage, uiEditBoxID, szTexName);
					}
				}

				return 0;
			}

			break;

		case IDC_LIST_EFFECT:

			switch(HIWORD(wParam))
			{
			case LBN_SELCHANGE:
				{
					int iIndex = SendDlgItemMessage(hwndPropPage, IDC_LIST_EFFECT, LB_GETCURSEL, 0, 0);
					
					if(iIndex >= 0)
					{
						int iEffect = SendDlgItemMessage(hwndPropPage, IDC_LIST_EFFECT, LB_GETITEMDATA, iIndex, 0);
						SetDlgItemInt(hwndPropPage, IDC_EDIT_EFFECT, iEffect, FALSE);
					}
				}

				return TRUE;
			}

			break;

		case IDC_EDIT_EFFECT:
			switch(HIWORD(wParam))
			{
			case EN_CHANGE:
				{
					BOOL bTranslated;
					int iEffect = GetDlgItemInt(hwndPropPage, IDC_EDIT_EFFECT, &bTranslated, FALSE);

					/* Valid number? */
					if(bTranslated)
					{
						/* Select corresponding list-box item. */
						ListBoxSearchByItemData(GetDlgItem(hwndPropPage, IDC_LIST_EFFECT), iEffect, TRUE);
					}
				}

				return TRUE;

			case EN_KILLFOCUS:
				/* Validation. */
				BoundEditBox(GetDlgItem(hwndPropPage, EN_KILLFOCUS), -32768, 32767, TRUE);

				return TRUE;
			}

			break;

		case IDC_EDIT_TFLOOR:
		case IDC_EDIT_TCEIL:
			switch(HIWORD(wParam))
			{
			case EN_CHANGE:
				{
					int iTextLength;
					LPTSTR szEditText;
					LPTSTR szPrev;
					TEXTURE *lptex;
					BOOL bNeedFree;

					iTextLength = GetWindowTextLength((HWND)lParam);
					szEditText = ProcHeapAlloc((iTextLength + 1) * sizeof(TCHAR));
					GetWindowText((HWND)lParam, szEditText, iTextLength + 1);

					/* Get pointer to previous string (i.e. before edit). */
					szPrev = (LOWORD(wParam) == IDC_EDIT_TFLOOR) ? s_szPrevTFloor : s_szPrevTCeil;

					/* Texture name changed. Do autocomplete. */
					if(s_bInitComplete && ConfigGetInteger(g_lpcfgMain, "autocompletetex"))
					{
						DWORD dwStartSel, dwEndSel;
						BOOL bRootDifferent;

						/* Don't autocomplete if box is empty or full, if the caret
						 * isn't at the end, if any text is selected or if the text
						 * is a truncated form of what it was before.
						 */
						bRootDifferent = lstrlen(szEditText) > lstrlen(szPrev) || _tcsncmp(szPrev, szEditText, min(lstrlen(szEditText), lstrlen(szPrev)));
						SendMessage((HWND)lParam, EM_GETSEL, (WPARAM)&dwStartSel, (LPARAM)&dwEndSel);
						if(iTextLength > 0
							&& iTextLength < TEXNAME_BUFFER_LENGTH
							&& dwEndSel == (unsigned int)iTextLength
							&& dwStartSel == dwEndSel
							&& bRootDifferent)
						{
							LPCTSTR szCompletedTexName;

							szCompletedTexName = FindFirstTexNameMatch(szEditText, s_lpmapppdata->lptnlFlats);

							/* Found a match? */
							if(szCompletedTexName)
							{
								/* Set text and select the portion that changed. */
								SetWindowText((HWND)lParam, szCompletedTexName);
								SendMessage((HWND)lParam, EM_SETSEL, iTextLength, lstrlen(szCompletedTexName) + 1);
							}
						}
					}

					/* Update the preview image. */
					bNeedFree = GetTextureForMap(s_lpmapppdata->hwndMap, szEditText, &lptex, TF_FLAT);
					if(lptex)
					{
						HBITMAP hbmPreview;
						HDC hdcPreview;

						hbmPreview = (LOWORD(wParam) == IDC_EDIT_TCEIL) ? s_hbmCeil : s_hbmFloor;
						hdcPreview = CreateCompatibleDC(NULL);
						SelectObject(hdcPreview, hbmPreview);

						StretchTextureToDC(lptex, hdcPreview, CX_TEXPREVIEW, CY_TEXPREVIEW);

						DeleteDC(hdcPreview);

						InvalidateRect(GetDlgItem(hwndPropPage, (LOWORD(wParam) == IDC_EDIT_TCEIL) ? IDC_TEX_CEIL : IDC_TEX_FLOOR), NULL, FALSE);

						/* Free memory if necessary. */
						if(bNeedFree) DestroyTexture(lptex);
					}
					

					/* Update remembered texture string. */
					lstrcpyn(szPrev, szEditText, TEXNAME_BUFFER_LENGTH);
					ProcHeapFree(szEditText);
				}

				return TRUE;
			}

			break;

		case IDC_EDIT_HCEIL:
		case IDC_EDIT_HFLOOR:

		case IDC_EDIT_TAG:
			switch(HIWORD(wParam))
			{
			case EN_KILLFOCUS:
				/* Validation. */
				BoundEditBox(GetDlgItem(hwndPropPage, LOWORD(wParam)), -32768, 32767, TRUE);

				return TRUE;
			}
		}

		/* Didn't process the message. */
		break;


	case WM_NOTIFY:
		
		switch(((LPNMHDR)lParam)->code)
		{
		case PSN_APPLY:
			{
				/* User OK-ed. Set the properties! */

				SECTORDISPLAYINFO sdi;
				DWORD dwPropertyFlags = 0;
				TCHAR szBuffer[TEXNAME_BUFFER_LENGTH];

				/* We go through each field, checking whether it's non-empty.
				 * If so, copy its value and set the corresponding flag.
				 */

				/* Floor texture. */
				GetDlgItemText(hwndPropPage, IDC_EDIT_TFLOOR, szBuffer, sizeof(szBuffer)/sizeof(TCHAR));
				if(*szBuffer)
				{
					lstrcpy(sdi.szFloor, szBuffer);
					dwPropertyFlags |= SDIF_FLOORTEX;
				}

				/* Ceiling texture. */
				GetDlgItemText(hwndPropPage, IDC_EDIT_TCEIL, szBuffer, sizeof(szBuffer)/sizeof(TCHAR));
				if(*szBuffer)
				{
					lstrcpy(sdi.szCeiling, szBuffer);
					dwPropertyFlags |= SDIF_CEILINGTEX;
				}

				/* Floor height. */
				GetDlgItemText(hwndPropPage, IDC_EDIT_HFLOOR, szBuffer, sizeof(szBuffer)/sizeof(TCHAR));
				if(*szBuffer)
				{
					sdi.nFloor = GetDlgItemInt(hwndPropPage, IDC_EDIT_HFLOOR, NULL, TRUE);
					dwPropertyFlags |= SDIF_FLOOR;
				}

				/* Ceiling height. */
				GetDlgItemText(hwndPropPage, IDC_EDIT_HCEIL, szBuffer, sizeof(szBuffer)/sizeof(TCHAR));
				if(*szBuffer)
				{
					sdi.nCeiling = GetDlgItemInt(hwndPropPage, IDC_EDIT_HCEIL, NULL, TRUE);
					dwPropertyFlags |= SDIF_CEILING;
				}

				/* Brightness. */
				GetDlgItemText(hwndPropPage, IDC_COMBO_LIGHT, szBuffer, sizeof(szBuffer)/sizeof(TCHAR));
				if(*szBuffer)
				{
					sdi.ucBrightness = GetDlgItemInt(hwndPropPage, IDC_COMBO_LIGHT, NULL, FALSE);
					dwPropertyFlags |= SDIF_BRIGHTNESS;
				}

				/* Effect. */
				GetDlgItemText(hwndPropPage, IDC_EDIT_EFFECT, szBuffer, sizeof(szBuffer)/sizeof(TCHAR));
				if(*szBuffer)
				{
					sdi.unEffect = GetDlgItemInt(hwndPropPage, IDC_EDIT_EFFECT, NULL, FALSE);
					dwPropertyFlags |= SDIF_EFFECT;
				}

				/* Tag. */
				GetDlgItemText(hwndPropPage, IDC_EDIT_TAG, szBuffer, sizeof(szBuffer)/sizeof(TCHAR));
				if(*szBuffer)
				{
					sdi.unTag = GetDlgItemInt(hwndPropPage, IDC_EDIT_TAG, NULL, FALSE);
					dwPropertyFlags |= SDIF_TAG;
				}

				/* TODO: Comments. */

				/* We've got all the properties now. Apply them. */
				ApplySectorPropertiesToSelection(s_lpmapppdata->lpmap, s_lpmapppdata->lpselection->lpsellistSectors, &sdi, dwPropertyFlags);

				/* Finished! */
			}

			SetWindowLong(hwndPropPage, DWL_MSGRESULT, PSNRET_NOERROR);
			return TRUE;
		}
		

		/* Didn't process the message. */
		break;

	case WM_HSCROLL:
		/* We've only got the one slider. */
		
		SetDlgItemInt(hwndPropPage, IDC_COMBO_LIGHT, SendDlgItemMessage(hwndPropPage, IDC_SLIDER_LIGHT, TBM_GETPOS, 0, 0), FALSE);
		return TRUE;

	case WM_DESTROY:

		/* Destroy the preview bitmaps. */
		DeleteObject(s_hbmCeil);
		DeleteObject(s_hbmFloor);

		return TRUE;
	}

	/* Didn't process message. */
	return FALSE;
}


/* InitSectorsPropPageFields
 *   Fills fields of the sector property page.
 *
 * Parameters:
 *   HWND				hwndPropPage	Property page window handle.
 *   SECTORDISPLAYINFO*	lpsdi			Data to fill in.
 *   DWORD				dwCheckFlags	Flags specifying which fields are valid.
 *
 * Return value: None.
 */
static void InitSectorsPropPageFields(HWND hwndPropPage, SECTORDISPLAYINFO *lpsdi, DWORD dwCheckFlags)
{
	if(dwCheckFlags & SDIF_EFFECT)		SetDlgItemInt(hwndPropPage, IDC_EDIT_EFFECT, lpsdi->unEffect, FALSE);
	if(dwCheckFlags & SDIF_CEILING)		SetDlgItemInt(hwndPropPage, IDC_EDIT_HCEIL, lpsdi->nCeiling, TRUE);
	if(dwCheckFlags & SDIF_FLOOR)		SetDlgItemInt(hwndPropPage, IDC_EDIT_HFLOOR, lpsdi->nFloor, TRUE);
	if(dwCheckFlags & SDIF_TAG)			SetDlgItemInt(hwndPropPage, IDC_EDIT_TAG, lpsdi->unTag, FALSE);
	if(dwCheckFlags & SDIF_BRIGHTNESS)
	{
		SetDlgItemInt(hwndPropPage, IDC_COMBO_LIGHT, lpsdi->ucBrightness, FALSE);
		SendDlgItemMessage(hwndPropPage, IDC_SLIDER_LIGHT, TBM_SETPOS, TRUE, lpsdi->ucBrightness);
	}

	if(dwCheckFlags & SDIF_CEILINGTEX)	SetDlgItemText(hwndPropPage, IDC_EDIT_TCEIL, lpsdi->szCeiling);
	if(dwCheckFlags & SDIF_FLOORTEX)	SetDlgItemText(hwndPropPage, IDC_EDIT_TFLOOR, lpsdi->szFloor);
}


/* AddSectorTypeToList
 *   Adds a sector effect to a list-box.
 *
 * Parameters:
 *   CONFIG		*lpcfg		Config node containing effect info.
 *   void*		lpvWindow	(HWND) Window handle of list-box.
 *
 * Return value: BOOL
 *   Always TRUE.
 *
 * Remarks:
 *   Used as a callback function by ConfigIterate.
 */
static BOOL AddSectorTypeToList(CONFIG *lpcfg, void *lpvWindow)
{
	HWND hwndList = (HWND)lpvWindow;
	int iIndex, iEffect;

	/* Add entry and store effect value. */
	iIndex = SendMessage(hwndList, LB_ADDSTRING, 0, (LPARAM)lpcfg->sz);
	iEffect = _tcstol(lpcfg->szName, NULL, 10);
	SendMessage(hwndList, LB_SETITEMDATA, iIndex, iEffect);

	/* Keep iterating. */
	return TRUE;
}


static LRESULT CALLBACK TexPreviewProc(HWND hwnd, UINT uiMsg, WPARAM wParam, LPARAM lParam)
{
	TPPDATA *lptppd = (TPPDATA*)GetWindowLong(hwnd, GWL_USERDATA);

	switch(uiMsg)
	{
	case WM_SETCURSOR:
		{
			HCURSOR hcursorHand = LoadCursor(NULL, IDC_HAND);
		
			/* Non-NT Windows don't support IDC_HAND. */
			if(hcursorHand) SetCursor(hcursorHand);
		}

		return 0;

	case WM_DESTROY:
		/* Restore default window class -- WM_DESTROY is apparently not the last
		 * message.
		 */
		SetWindowLong(hwnd, GWL_WNDPROC, (LONG)lptppd->wndprocStatic);

		/* Free subclassing data. */
		ProcHeapFree(lptppd);
		return 0;
	}

	/* The original wndproc is stored as our userdata. */
	return CallWindowProc(lptppd->wndprocStatic, hwnd, uiMsg, wParam, lParam);
}
