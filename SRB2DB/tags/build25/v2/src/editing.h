#ifndef __SRB2B_EDITING__
#define __SRB2B_EDITING__

#include "map.h"
#include "config.h"
#include "renderer.h"
#include "selection.h"
#include "win/infobar.h"


/* Types. */
typedef struct _DRAW_OPERATION
{
	int				*lpiNewVertices;
	int				*lpiNewLines;

	int				iNewVertexCount;
	int				iNewLineCount;

	unsigned int	ciVertexBuffer;
	unsigned int	ciLineBuffer;
} DRAW_OPERATION;


enum ENUM_VERTEX_EDIT_FLAGS
{
	VEF_NEW = 1
};

enum ENUM_LINEDEF_EDIT_FLAGS
{
	LEF_NEW = 1
};

enum ENUM_SNAP_FLAGS
{
	SF_RECTANGLE	= 1,
	SF_45			= 2
};

enum ENUM_LINE_SIDE
{
	LS_FRONT, LS_BACK
};


/* Prototypes. */
int RemoveUnusedVertices(MAP *lpmap);
void GetLinedefDisplayInfo(MAP *lpmap, CONFIG *lpcfgLinedefs, int iIndex, LINEDEFDISPLAYINFO *lplddi);
void GetSectorDisplayInfo(MAP *lpmap, CONFIG *lpcfgSectors, int iIndex, SECTORDISPLAYINFO *lpsdi);
void GetThingDisplayInfo(MAP *lpmap, CONFIG *lpcfgThings, int iIndex, THINGDISPLAYINFO *lptdi);
void GetVertexDisplayInfo(MAP *lpmap, int iIndex, VERTEXDISPLAYINFO *lpvdi);
DWORD CheckLines(MAP *lpmap, SELECTION_LIST *lpsellist, CONFIG *lpcfgLinedefTypes, LINEDEFDISPLAYINFO *lpsdi);
DWORD CheckSectors(MAP *lpmap, SELECTION_LIST *lpsellist, CONFIG *lpcfgSecTypes, SECTORDISPLAYINFO *lpsdi);
DWORD CheckThings(MAP *lpmap, SELECTION_LIST *lpsellist, CONFIG *lpcfgFlatThings, THINGDISPLAYINFO *lptdi);
DWORD CheckVertices(MAP *lpmap, SELECTION_LIST *lpsellist, VERTEXDISPLAYINFO *lptdi);
unsigned short NextUnusedTag(MAP *lpmap);
BOOL GetAdjacentSectorHeightRange(MAP *lpmap, short *lpnMaxCeil, short *lpnMinCeil, short *lpnMaxFloor, short *lpnMinFloor);
void GetMapRect(MAP *lpmap, RECT *lprc);
void ResetMapView(MAP *lpmap, MAPVIEW *lpmapview);
void ApplySectorPropertiesToSelection(MAP *lpmap, SELECTION_LIST *lpsellist, SECTORDISPLAYINFO *lpsdi, DWORD dwFlags);
void ApplySectorProperties(MAP *lpmap, int iIndex, SECTORDISPLAYINFO *lpsdi, DWORD dwFlags);
int AddVertex(MAP *lpmap, short x, short y);
BOOL RemoveVertex(MAP *lpmap, int iIndex);
int AddLinedef(MAP *lpmap, int iVertexStart, int iVertexEnd);
int AddSector(MAP *lpmap, int iParent);
int AddSidedef(MAP *lpmap, int iSector);
void BeginDrawOperation(DRAW_OPERATION *lpdrawop);
void EndDrawOperation(MAP *lpmap, DRAW_OPERATION *lpdrawop);
BOOL DrawToNewVertex(MAP *lpmap, DRAW_OPERATION *lpdrawop, short x, short y);
void FlipLinedef(MAP *lpmap, int iLinedef);
int GetVertexFromPosition(MAP *lpmap, short x, short y);
int FindLinedefBetweenVertices(MAP *lpmap, int iVertexA, int iVertexB);
int FindLinedefBetweenVerticesDirected(MAP *lpmap, int iVertex1, int iVertex2);
void Snap(short *lpx, short *lpy, unsigned short cxSnap, unsigned short cySnap);
BOOL VerticesReachable(MAP *lpmap, int iVertex1, int iVertex2);

#endif
