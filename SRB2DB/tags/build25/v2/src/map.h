#ifndef __SRB2B_MAP__
#define __SRB2B_MAP__

#include "maptypes.h"

/* Function prototypes. */
MAP *AllocateMapStructure(void);
void DestroyMapStructure(MAP *lpmap);
int MapLoad(MAP *lpmap, int iWad, LPCSTR szLumpname);
int MapLoadForPreview(MAP *lpmap, int iWad, LPCSTR szLumpname);


__inline BOOL LinedefBelongsToSector(MAP *lpmap, int iLinedefIndex, int iSectorIndex)
{
	int s1 = lpmap->linedefs[iLinedefIndex].s1, s2 = lpmap->linedefs[iLinedefIndex].s2;
	if(s1 >= 0 && lpmap->sidedefs[s1].sector == iSectorIndex) return TRUE;
	if(s2 >= 0 && lpmap->sidedefs[s2].sector == iSectorIndex) return TRUE;
	return FALSE;
}


#endif