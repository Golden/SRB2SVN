#ifndef __SRB2B_OPENWADS__
#define __SRB2B_OPENWADS__

#include "../wad/Wad.h"

/* Types. */
typedef struct _OPENWAD OPENWAD;

struct _OPENWAD
{
	WAD				*lpwad;
	unsigned int	uiReferenceCount;
	int				iID;
	OPENWAD	*lpowNext;
};


/* Function prototypes. */
int LoadWad(LPCTSTR szPath);
void EnumChildWindowsByWad(int iID, WNDENUMPROC lpEnumFunc, LPARAM lParam);
int ReleaseWad(int iID);
void InitOpenWadsList(void);
int NewWad(void);
WAD* GetWad(int iWad);
void IncrementWadReferenceCount(int iWad);
int GetWadPath(int iWad, LPTSTR szPath, WORD cbBuffer);

#endif	/* __SRB2B_OPENWADS__ */