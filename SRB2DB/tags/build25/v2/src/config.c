/* Config files are ANSI! */

#include <windows.h>
#include <stdio.h>
#include <tchar.h>

#include "general.h"
#include "config.h"
#include "resource.h"

/* Macros. */
#define NODEROOT "node-root"

/* Static prototypes. */
static CONFIG* CreateConfigNode(LPCSTR szName);
static void DestroyConfigNode(CONFIG *lpcfg);
static CONFIG* ParseConfigSection(char **lpszFile);
static BOOL ParseAndAddConfigStatement(CONFIG *lpcfgRoot, char **lpszFile);
static BOOL ParseConfigStringValue(char **lpsz);
static CONFIG_ENTRY_TYPE ParseConfigNumericValue(char **lpsz, int *lpi, float *lpf);
static BOOL ReadPastString(char **lpsz, char *szToken);
static BOOL ReadUntilAnyChar(char **lpsz, char *szCharSet);
static BOOL ReadPastAnyChar(char **lpsz, char *szCharSet);
static BOOL CharBelongsToSet(char c, char *szCharSet);
static BOOL EatCommentsAndWhitespace(char **lpsz);
static BOOL AddConfigNode(CONFIG *lpcfgRoot, CONFIG *lpcfgNew);
static void DestroyConfigNodeData(CONFIG *lpcfg);
static CONFIG* FindConfigNode(CONFIG *lpcfgRoot, LPCSTR szName);


/* LoadConfig
 *   Loads a Doom Builder-esque config file from disc.
 *
 * Parameters:
 *   LPCTSTR	szFilename		File to load.
 *
 * Return value:
 *   Pointer to new config structure, or NULL on error.
 *
 * Notes:
 *   The app should never manipulate the returned structure directly. It should
 *   only ever be passed to functions in this file.
 */
CONFIG* LoadConfig(LPCTSTR szFilename)
{
	CONFIG *lpcfg;
	HANDLE hFile = CreateFile(szFilename, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING, FILE_FLAG_SEQUENTIAL_SCAN, NULL);
	char *szFile, *szFileRover;
	DWORD dwFileLength;
	DWORD dwBytesRead;

	/* File not found? */
	if(hFile == INVALID_HANDLE_VALUE) return NULL;

	/* Get file length. */
	dwFileLength = GetFileSize(hFile, NULL);
	if(!dwFileLength)		/* Zero-length file. */
	{
		CloseHandle(hFile);
		return NULL;
	}

	/* Load the whole lot into memory. */
	szFile = ProcHeapAlloc(dwFileLength + 1);
	ReadFile(hFile, szFile, dwFileLength, &dwBytesRead, NULL);
	CloseHandle(hFile);

	if(dwBytesRead != dwFileLength)		/* Failed during read. */
		return NULL;
	szFile[dwFileLength] = '\0';		/* Terminate string. */

	/* We've loaded the data; get cracking with parsing it. */

	/* Treat the whole thing as a section. */
	szFileRover = szFile;
	lpcfg = ParseConfigSection(&szFileRover);

	ProcHeapFree(szFile);

	/* If we failed, this is NULL, which is our error return anyway. */
	return lpcfg;
}


/* ParseConfigSection
 *   Generates a config structure for a section of a config file.
 *
 * Parameters:
 *   char	**lpszFile		Address of buffer containing section.
 *
 * Return value:
 *   Pointer to new config structure, or NULL on error.
 *
 * Notes:
 *   A section is bounded either by {} or by the beginning and end of the
 *   buffer. The function advances the pointer that's passed so the caller can
 *   resume parsing where we left off.
 */
static CONFIG* ParseConfigSection(char **lpszFile)
{
	CONFIG *lpcfg;
	BOOL bEndSection = FALSE, bError = FALSE;

	/* In the event that we actually have an entry called NODEROOT, the root
	 * will, in fact, be used to store it. Having it begin with 'n' also makes
	 * it nice for balancing the tree.
	 */
	lpcfg = ConfigCreate();

	while(!bEndSection && !bError)
	{
		EatCommentsAndWhitespace(lpszFile);

		switch(**lpszFile)
		{
		/* Finished! As a special case, don't advance at '\0': this stops
		 * badly-formed configs from crashing.
		 */
		case '}':
			(*lpszFile)++;
		case '\0':
			bEndSection = TRUE;
			break;

		/* Skip over whitespace and extraneous semicolons. */
		case ' ': case '\n': case '\r': case '\t': case ';':
			(*lpszFile)++;
			break;

		/* Comments are now processed elsewhere. */

		/* Nothing special; must be an identifier. */
		default:
			
			/* Delegate the task of creating and adding the node. */
			if(!ParseAndAddConfigStatement(lpcfg, lpszFile))	/* Bad statement. */
				bError = TRUE;

		}	/* switch(*(*lpszFile)) */
	}		/* while(!bEndSection && !bError) */

	/* If something went wrong somewhere, give up entirely on the section. */
	if(bError)
	{
		ConfigDestroy(lpcfg);
		return NULL;
	}

	return lpcfg;
}


/* ParseAndAddConfigStatement
 *   Parses a statement and adds the required node.
 *
 * Parameters:
 *   CONFIG	*lpcfgRoot		Root of tree that the statement pertains to.
 *   char	**lpszFile		Address of buffer containing section.
 *
 * Return value: BOOL
 *   TRUE on success; FALSE on error.
 *
 * Notes:
 *   'Statement' includes subsections.
 */
static BOOL ParseAndAddConfigStatement(CONFIG *lpcfgRoot, char **lpszFile)
{
	char *szID, *szEndID;
	char cOperator;
	
	szID = *lpszFile;

	/* We're at the beginning of an identifier, which ends when we reach
	 * whitespace or an operator. Move past the end.
	 */
	while(!CharBelongsToSet(**lpszFile, " \n\r\t={;")) (*lpszFile)++;

	/* Zero-length identifier. */
	if(*lpszFile == szID) return FALSE;

	/* Eat whitespace between identifier and operator. */
	szEndID = *lpszFile;
	EatCommentsAndWhitespace(lpszFile);
	cOperator = *(*lpszFile)++;

	*szEndID = '\0';	/* Terminate identifier. */

	

	/* Determine whether it's a subsection, assignment, or atomic entry. */
	if(cOperator == '{')		/* Subsection. */
	{
		CONFIG *lpcfgSubsectContainer;

		lpcfgSubsectContainer = CreateConfigNode(szID);
		lpcfgSubsectContainer->entrytype = CET_SUBSECTION;
		lpcfgSubsectContainer->lpcfgSubsection = ParseConfigSection(lpszFile);

		if(!lpcfgSubsectContainer->lpcfgSubsection)
		{
			/* Clean up -- no actual data to free, just node metadata. */
			lpcfgSubsectContainer->entrytype = CET_NULL;
			DestroyConfigNode(lpcfgSubsectContainer);
			return FALSE;
		}

		/* Section is okay. Try to add it. */
		if(!AddConfigNode(lpcfgRoot, lpcfgSubsectContainer))
		{
			/* The section already existed. The new section was put in its old
			 * node. Free our node.
			 */
			DestroyConfigNode(lpcfgSubsectContainer);
		}
	}
	else if(cOperator == ';')	/* Null entry. */
	{
		/* We're almost there now. Add atomic entry to the config. */
		ConfigSetAtom(lpcfgRoot, szID);
	}
	else if(cOperator == '=')		/* Assignment. */
	{
		/* Move to the beginning of the value to be assigned. */
		EatCommentsAndWhitespace(lpszFile);

		/* String? */
		if(**lpszFile == '"')
		{
			char *szValue;

			/* Skip opening quote. */
			(*lpszFile)++;
			szValue = *lpszFile;

			/* Make sure string is valid. */
			if(!ParseConfigStringValue(lpszFile)) return FALSE;

			/* We're almost there now. Set the string in the config. */
			ConfigSetString(lpcfgRoot, szID, szValue);
		}
		else	/* Number. */
		{
			CONFIG_ENTRY_TYPE cet;
			int i;
			float f;

			/* Determine the type of number. */
			cet = ParseConfigNumericValue(lpszFile, &i, &f);

			/* Syntax error? */
			if(cet == CET_NULL) return FALSE;

			/* Which type are we dealing with? */
			switch(cet)
			{
			case CET_INT:
				/* We're almost there now. Set the int in the config. */
				ConfigSetInteger(lpcfgRoot, szID, i);
				break;

			case CET_FLOAT:
				/* We're almost there now. Set the float in the config. */
				ConfigSetFloat(lpcfgRoot, szID, f);
			}
		}
	}
	else return FALSE;		/* Not assignment, subsect or atom. */

	/* Success! */
	return TRUE;
}


/* ParseConfigStringValue
 *   Parses an escaped, quoted string for assignment.
 *
 * Parameters:
 *   char	**lpsz		Address of pointer to first ch. of string (after quote).
 *
 * Return value: BOOL
 *   TRUE on success; FALSE on error.
 *
 * Notes:
 *   The pointer's advanced beyond the semicolon.
 */
static BOOL ParseConfigStringValue(char **lpsz)
{
	char *szStart;
	int iReducedChars;

	szStart = *lpsz;

	/* We have a little work to do. We'll do it in two passes. First, make sure
	 * the string is closed, and terminate it at the endquote.
	 */
	while(**lpsz && **lpsz != '"')
	{
		/* Skip over escape characters. */
		if(**lpsz == '\\')
		{
			(*lpsz)++;
			if(isdigit(**lpsz)) *lpsz += 2;	/* Skip 3-digit code. */
		}

		/* Next char. */
		(*lpsz)++;
	}

	if(!(**lpsz)) return FALSE;	/* Unterminated. */

	/* Replace the endquote with \0, and move on. */
	*(*lpsz)++ = '\0';

	/* Eat whitespace. */
	EatCommentsAndWhitespace(lpsz);

	/* Must be semicolon now. */
	if(*(*lpsz)++ != ';') return FALSE;

	/* Now we tidy up the string, replacing escaped things. */
	iReducedChars = 0;
	while(szStart[iReducedChars])
	{
		if(szStart[iReducedChars] == '\\')
		{
			/* Move to char after backspace. */
			iReducedChars++;

			/* \nnn pattern? */
			if(isdigit(szStart[iReducedChars]))
			{
				char szNum[4];

				/* Copy the digits in. */
				szNum[0] = szStart[iReducedChars];
				szNum[1] = szStart[iReducedChars+1];
				szNum[2] = szStart[iReducedChars+2];
				szNum[3] = '\0';

				/* Must all be digits. */
				if(!isdigit(szNum[1]) || !isdigit(szNum[2])) return FALSE;

				/* Get the character and then skip over. */
				*szStart = atoi(szNum);
				iReducedChars += 2;
			}
			else	/* Single escape char. */
			{
				switch(szStart[iReducedChars])
				{
				case 'n':
					*szStart = 0x10;	/* Really a linefeed, not CRLF. */
					break;
				case 'r':
					*szStart = '\r';
					break;
				case 't':
					*szStart = '\t';
					break;

				/* Unrecognised (including " and \), so just output the character. */
				default:
					*szStart = szStart[iReducedChars];
				}
			}
		}
		/* Just copy, and even then, only if necessary. */
		else if(iReducedChars > 0) *szStart = szStart[iReducedChars];

		szStart++;
	}

	/* Since we copied backwards, we may still have garbage left at the end of
	 * the buffer. Terminate the string.
	 */
	*szStart = '\0';

	return TRUE;
}


/* ParseConfigNumericValue
 *   Parses a numeric value (float or int) for assignment.
 *
 * Parameters:
 *   char	**lpsz		Address of pointer to first ch. of string (after quote).
 *   int	*lpi		Pointer to int to set if type is int.
 *   float	*lpf		Pointer to float to set if type is float.
 *
 * Return value: CONFIG_ENTRY_TYPE
 *   CET_FLOAT if num was a float, CET_INT if it was an int, and CET_NULL on
 *   error.
 *
 * Notes:
 *   The pointer's advanced beyond the semicolon.
 */
static CONFIG_ENTRY_TYPE ParseConfigNumericValue(char **lpsz, int *lpi, float *lpf)
{
	char *szStart = *lpsz;
	CONFIG_ENTRY_TYPE cet;

	/* First, validate and determine type. */

	/* We're allowed one leading minus sign. */
	if(**lpsz == '-') (*lpsz)++;

	/* Pass over initial digits. */
	while(isdigit(**lpsz)) (*lpsz)++;

	/* Float? */
	if(**lpsz == '.') cet = CET_FLOAT;
	else cet = CET_INT;

	/* Eat whitespace. */
	EatCommentsAndWhitespace(lpsz);

	/* Must be at semicolon now. */
	if(**lpsz != ';') return CET_NULL;

	/* Terminate for reading string. */
	*(*lpsz)++ = '\0';

	if(cet == CET_FLOAT)
		*lpf = (float)atof(szStart);
	else
		*lpi = atoi(szStart);

	return cet;
}



/* ReadPastString, ReadUntilAnyChar, ReadPastAnyChar, CharBelongsToSet
 *   String-parsing utilities.
 *
 * Parameters:
 *   Varying, but intuitively comprehensible.
 *
 * Return value: BOOL
 *   TRUE if found; FALSE otherwise.
 */
static BOOL ReadPastString(char **lpsz, char *szToken)
{
	BOOL bFound = TRUE;		/* Assume that we find the token. */
	int iTokenLength = lstrlenA(szToken);

	/* If we're at the end, signal that we didn't find it. Otherwise, keep going
	 * until we do find it, or reach the end.
	 */
	while((**lpsz || (bFound = FALSE)) && strncmp(*lpsz, szToken, iTokenLength)) (*lpsz)++;
	
	if(bFound) *lpsz += lstrlenA(szToken);
	return bFound;
}

static BOOL ReadUntilAnyChar(char **lpsz, char *szCharSet)
{
	BOOL bFound = TRUE;		/* Assume that we find the token. */

	/* If we're at the end, signal that we didn't match. Otherwise, keep going
	 * until we do match, or reach the end.
	 */
	while((**lpsz || (bFound = FALSE)) && !CharBelongsToSet(**lpsz, szCharSet)) (*lpsz)++;
	
	return bFound;
}

static BOOL ReadPastAnyChar(char **lpsz, char *szCharSet)
{
	BOOL bFound = TRUE;		/* Assume that we find the token. */

	/* If we're at the end, signal that we didn't match. Otherwise, keep going
	 * until we do match, or reach the end.
	 */
	while((**lpsz || (bFound = FALSE)) && CharBelongsToSet(**lpsz, szCharSet)) (*lpsz)++;
	
	return bFound;
}

static BOOL CharBelongsToSet(char c, char *szCharSet)
{
	BOOL bMatch = TRUE;
	/* If we're at the end, signal that we didn't match. Otherwise, keep going
	 * until we do match, or reach the end.
	 */
	while((*szCharSet || (bMatch = FALSE)) && *szCharSet++ != c);
	return bMatch;
}



/* EatCommentsAndWhitespace
 *   Reads past any comments and whitespace.
 *
 * Parameters:
 *   char**		lpsz	Pointer to pointer to beginning of string to parse.
 *
 * Return value: BOOL
 *   FALSE if an unterminated block comment occurred; TRUE otherwise.
 */
static BOOL EatCommentsAndWhitespace(char **lpsz)
{
	ReadPastAnyChar(lpsz, " \n\r\t");

	/* We're at the end of leading whitespace. Are we at a comment? */
	if(**lpsz == '/')
	{
		/* Block comment. */
		if((*lpsz)[1] == '*')
		{
			(*lpsz) += 2;	/* Skip initial "/*". */
			if(!ReadPastString(lpsz, "*/")) return FALSE;

			/* Passed the comment -- eat s'more! */
			return EatCommentsAndWhitespace(lpsz);
		}
		/* Line comment. */
		else if((*lpsz)[1] == '/')
		{
			(*lpsz) += 2;
			/* Pass next newline. */
			ReadUntilAnyChar(lpsz, "\n\r");
			ReadPastAnyChar(lpsz, "\n\r");
			
			/* Passed the comment -- eat s'more! */
			return EatCommentsAndWhitespace(lpsz);
		}
	}

	/* No problems. */
	return TRUE;
}


/* ConfigSetString, ConfigSetFloat, ConfigSetInteger, ConfigSetAtom
 *   Sets the value of a config entry, adding it if it doesn't exist.
 *
 * Parameters:
 *   CONFIG*	lpcfgRoot	Root of config tree.
 *   LPCSTR		szName		Name of element to set.
 *   LPCSTR		szValue		\
 *   float		f			-	Values to set.
 *   int		i			/
 *
 * Return value:
 *   None.
 */
void ConfigSetString(CONFIG *lpcfgRoot, LPCSTR szName, LPCSTR szValue)
{
	CONFIG *lpcfgNew = CreateConfigNode(szName);
	int iStrLen = lstrlenA(szValue) + 1;

	lpcfgNew->entrytype = CET_STRING;
	lpcfgNew->sz = ProcHeapAlloc(iStrLen);
	CopyMemory(lpcfgNew->sz, szValue, iStrLen);

	if(!AddConfigNode(lpcfgRoot, lpcfgNew))
	{
		/* Node of that name already existed; free the new node. */
		DestroyConfigNode(lpcfgNew);
	}
}

void ConfigSetFloat(CONFIG *lpcfgRoot, LPCSTR szName, float f)
{
	CONFIG *lpcfgNew = CreateConfigNode(szName);

	lpcfgNew->entrytype = CET_FLOAT;
	lpcfgNew->f = f;

	if(!AddConfigNode(lpcfgRoot, lpcfgNew))
	{
		/* Node of that name already existed; free the new node. */
		DestroyConfigNode(lpcfgNew);
	}
}

void ConfigSetInteger(CONFIG *lpcfgRoot, LPCSTR szName, int i)
{
	CONFIG *lpcfgNew = CreateConfigNode(szName);

	lpcfgNew->entrytype = CET_INT;
	lpcfgNew->i = i;

	if(!AddConfigNode(lpcfgRoot, lpcfgNew))
	{
		/* Node of that name already existed; free the new node. */
		DestroyConfigNode(lpcfgNew);
	}
}

void ConfigSetAtom(CONFIG *lpcfgRoot, LPCSTR szName)
{
	CONFIG *lpcfgNew = CreateConfigNode(szName);

	lpcfgNew->entrytype = CET_NULL;

	if(!AddConfigNode(lpcfgRoot, lpcfgNew))
	{
		/* Node of that name already existed; free the new node. */
		DestroyConfigNode(lpcfgNew);
	}
}



/* ConfigSetSubsection
 *   Adds a config to another config as a subsection.
 *
 * Parameters:
 *   CONFIG		*lpcfgRoot	Root of config tree.
 *   char		*szName		Name of subsection.
 *   CONFIG		*lpcfgSS	Subsection tree.
 *
 * Return value: None.
 */
void ConfigSetSubsection(CONFIG *lpcfgRoot, LPCSTR szName, CONFIG *lpcfgSS)
{
	CONFIG *lpcfgSubsectContainer = CreateConfigNode(szName);

	/* This is for the container node, remember. */
	lpcfgSubsectContainer->entrytype = CET_SUBSECTION;

	/* Store it once in the container node. */
	lpcfgSubsectContainer->lpcfgSubsection = lpcfgSS;

	/* Add new subsection, and check whether a node of that name already
	 * existed.
	 */
	if(!AddConfigNode(lpcfgRoot, lpcfgSubsectContainer))
	{
		/* The section already existed. The new section was put in its old
		 * node. Free our node.
		 */
		DestroyConfigNode(lpcfgSubsectContainer);
	}
}


/* ConfigAddSubsection
 *   Creates a new subsection and adds it to the tree.
 *
 * Parameters:
 *   CONFIG		*lpcfgRoot	Root of config tree.
 *   char		*szName		Name of subsection to create.
 *
 * Return value:
 *   Pointer to the root (not the container!) of the new subsection.
 */
CONFIG* ConfigAddSubsection(CONFIG *lpcfgRoot, LPCSTR szName)
{
	CONFIG *lpcfgSubsectContainer = CreateConfigNode(szName);
	CONFIG *lpcfgNewSubsection;

	/* This is for the container node, remember. */
	lpcfgSubsectContainer->entrytype = CET_SUBSECTION;

	/* Store it once in the container node, and once for returning (since we
	 * might have to destroy the container).
	 */
	lpcfgNewSubsection = lpcfgSubsectContainer->lpcfgSubsection = ConfigCreate();

	/* Add new subsection, and check whether a node of that name already
	 * existed.
	 */
	if(!AddConfigNode(lpcfgRoot, lpcfgSubsectContainer))
	{
		/* The section already existed. The new section was put in its old
		 * node. Free our node.
		 */
		DestroyConfigNode(lpcfgSubsectContainer);
	}

	return lpcfgNewSubsection;
}


/* CreateConfigNode
 *   Creates a node in a config tree.
 *
 * Parameters:
 *   LPCSTR	szName		Name of node.
 *
 * Return value: CONFIG*
 *   Pointer to new config node.
 *
 * Notes:
 *   The new node has a null type.
 */
static CONFIG* CreateConfigNode(LPCSTR szName)
{
	CONFIG *lpcfg = ProcHeapAlloc(sizeof(CONFIG));

	lpcfg->szName = ProcHeapAlloc(lstrlenA(szName) + 1);
	lstrcpy(lpcfg->szName, szName);
	lpcfg->entrytype = CET_NULL;
	lpcfg->lpcfgLeft = lpcfg->lpcfgRight = NULL;

	return lpcfg;
}


/* AddConfigNode
 *   Adds a node to a config tree.
 *
 * Parameters:
 *   CONFIG		*lpcfgRoot	Root of tree to add to.
 *   CONFIG		*lpcfgNew	Node to add.
 *
 * Return value: BOOL
 *   FALSE if a node with the same name as the new one already existed; TRUE
 *   otherwise.
 *
 * Notes:
 *   If a node with the same name as the new one already exists, the *existing*
 *   node is altered, so the app should free the node it passed in, assuming it
 *   doesn't need it any more (since it's not actually part of the tree). Also,
 *   in these circumstances, the passed-in node may be changed.
 */
static BOOL AddConfigNode(CONFIG *lpcfgRoot, CONFIG *lpcfgNew)
{
	int iStrCmpResult;

	/* The conditions for loop termination are sufficiently diverse that this is
	 * probably the neatest way of doing it. It's just binary tree node
	 * insertion, anyway.
	 */
	while(TRUE)
	{
		iStrCmpResult = stricmp(lpcfgNew->szName, lpcfgRoot->szName);

		if(!iStrCmpResult)
		{
			/* A node with this name already exists, so alter that one. */
			DestroyConfigNodeData(lpcfgRoot);

			/* We reuse the memory from the new node. */
			switch(lpcfgNew->entrytype)
			{
			case CET_INT:
				lpcfgRoot->i = lpcfgNew->i;
				break;

			case CET_FLOAT:
				lpcfgRoot->f = lpcfgNew->f;
				break;

			case CET_STRING:
				lpcfgRoot->sz = lpcfgNew->sz;
				break;

			case CET_SUBSECTION:
				lpcfgRoot->lpcfgSubsection = lpcfgNew->lpcfgSubsection;
				break;
			}

			lpcfgRoot->entrytype = lpcfgNew->entrytype;

			/* Set the 'new' node to a null node, since we're using its data. */
			lpcfgNew->entrytype = CET_NULL;

			return FALSE;		/* Node has been added by alteration. */
		}
		else if(iStrCmpResult < 0)
		{
			/* New node belongs on left. */
			if(lpcfgRoot->lpcfgLeft)
			{
				/* Repeat everything for the left branch. */
				lpcfgRoot = lpcfgRoot->lpcfgLeft;
			}
			else
			{
				/* We're empty on the left, so just add ourselves here. */
				lpcfgRoot->lpcfgLeft = lpcfgNew;
				return TRUE;		/* New node added. */
			}
		}
		else	/* iStrCmpResult > 0 */
		{
			/* New node belongs on right. */
			if(lpcfgRoot->lpcfgRight)
			{
				/* Repeat everything for the right branch. */
				lpcfgRoot = lpcfgRoot->lpcfgRight;
			}
			else
			{
				/* We're empty on the right, so just add ourselves here. */
				lpcfgRoot->lpcfgRight = lpcfgNew;
				return TRUE;		/* New node added. */
			}
		}
	}
}


/* ConfigDestroy
 *   Frees a config node tree.
 *
 * Parameters:
 *   CONFIG		*lpcfg	Root node of tree to free.
 *
 * Return value: None.
 *
 * Notes:
 *   This just recurses over the tree, calling DestroyConfigNode for each node.
 */
void ConfigDestroy(CONFIG *lpcfg)
{
	if(lpcfg->lpcfgLeft) ConfigDestroy(lpcfg->lpcfgLeft);
	if(lpcfg->lpcfgRight) ConfigDestroy(lpcfg->lpcfgRight);
	DestroyConfigNode(lpcfg);
}


/* DestroyConfigNode
 *   Frees a config node and its data.
 *
 * Parameters:
 *   CONFIG		*lpcfg	Node to free.
 *
 * Return value: None.
 *
 * Notes:
 *   This doesn't free any children. Use DestroyConfig for that.
 */
static void DestroyConfigNode(CONFIG *lpcfg)
{
	/* Clean up the contents of the node. */
	DestroyConfigNodeData(lpcfg);

	/* Free the name storage. */
	ProcHeapFree(lpcfg->szName);

	/* Free the node itself. */
	ProcHeapFree(lpcfg);
}

/* DestroyConfigNodeData
 *   Frees a config node's data.
 *
 * Parameters:
 *   CONFIG		*lpcfg	Node whose data is to be freed.
 *
 * Return value: None.
 *
 * Notes:
 *   If this is a subsection, that entire tree is freed.
 */
static void DestroyConfigNodeData(CONFIG *lpcfg)
{
	/* If the memory for this node's data was allocated dynamically, free it. */
	switch(lpcfg->entrytype)
	{
	case CET_STRING:
		ProcHeapFree(lpcfg->sz);
		break;
	case CET_SUBSECTION:
		ConfigDestroy(lpcfg->lpcfgSubsection);
		break;
	}
}


/* FindConfigNode
 *   Searches a tree for a given node.
 *
 * Parameters:
 *   CONFIG*	lpcfgRoot	Root of tree to search.
 *   LPCSTR		szName		Name of node to find.
 *
 * Return value: CONFIG*
 *   Address of matching node, or NULL if not found.
 */
static CONFIG* FindConfigNode(CONFIG *lpcfgRoot, LPCSTR szName)
{
	int iStrCmpResult;

	/* Stop when the branch where the node *would* be is empty. */
	while(lpcfgRoot)
	{
		iStrCmpResult = stricmp(szName, lpcfgRoot->szName);

		if(!iStrCmpResult)
		{
			/* Found it! */
			return lpcfgRoot;
		}
		else if(iStrCmpResult < 0)
		{
			/* Node belongs on left. */
			lpcfgRoot = lpcfgRoot->lpcfgLeft;
		}
		else	/* iStrCmpResult > 0 */
		{
			/* Node belongs on right. */
			lpcfgRoot = lpcfgRoot->lpcfgRight;
		}
	}

	return NULL;
}

/* ConfigNodeExists
 *   Searches a tree for a given node.
 *
 * Parameters:
 *   CONFIG*	lpcfgRoot	Root of tree to search.
 *   LPCSTR		szName		Name of node to find.
 *
 * Return value: BOOL
 *   TRUE if node found, or FALSE if not.
 */
BOOL ConfigNodeExists(CONFIG *lpcfgRoot, LPCSTR szName)
{
	return FindConfigNode(lpcfgRoot, szName) != NULL;
}


/* ConfigGetInteger
 *   Gets an integer from a config structure.
 *
 * Parameters:
 *   CONFIG*	lpcfgRoot	Root of config tree.
 *   LPCSTR		szName		Name of node whose value is to be retrieved.
 *
 * Return value: int
 *   Value of node, or zero if not found.
 *
 * Notes:
 *   Floats are casted to ints. Non-numbers have an integer value of zero.
 */
int ConfigGetInteger(CONFIG *lpcfgRoot, LPCSTR szName)
{
	CONFIG *lpcfg = FindConfigNode(lpcfgRoot, szName);

	/* Not found. */
	if(!lpcfg)
		return 0;

	switch(lpcfg->entrytype)
	{
	case CET_INT:
		return lpcfg->i;
	case CET_FLOAT:
		return (int)lpcfg->f;
	default:
		return 0;
	}
}


/* ConfigGetFloat
 *   Gets a float from a config structure.
 *
 * Parameters:
 *   CONFIG*	lpcfgRoot	Root of config tree.
 *   LPCSTR		szName		Name of node whose value is to be retrieved.
 *
 * Return value: float
 *   Value of node, or zero if not found.
 *
 * Notes:
 *   Ints are casted to floats. Non-numbers have an integer value of zero.
 */
float ConfigGetFloat(CONFIG *lpcfgRoot, LPCSTR szName)
{
	CONFIG *lpcfg = FindConfigNode(lpcfgRoot, szName);

	/* Not found. */
	if(!lpcfg)
		return 0;

	switch(lpcfg->entrytype)
	{
	case CET_FLOAT:
		return lpcfg->f;
	case CET_INT:
		return (float)lpcfg->i;
	default:
		return 0;
	}
}


/* ConfigGetStringLength
 *   Gets the length of a string from within a config structure.
 *
 * Parameters:
 *   CONFIG*	lpcfgRoot	Root of config tree.
 *   LPCSTR		szName		Name of node whose string length is to be retrieved.
 *
 * Return value: int
 *   Length of string, or zero if not found.
 *
 * Notes:
 *   Non-strings are considered to have a length of zero.
 */
int ConfigGetStringLength(CONFIG *lpcfgRoot, LPCSTR szName)
{
	CONFIG *lpcfg = FindConfigNode(lpcfgRoot, szName);

	/* Not found. */
	if(!lpcfg)
		return 0;

	switch(lpcfg->entrytype)
	{
	case CET_STRING:
		return lstrlenA(lpcfg->sz);
	default:
		return 0;
	}
}


/* ConfigGetString
 *   Gets a string from within a config structure.
 *
 * Parameters:
 *   CONFIG			*lpcfgRoot	Root of config tree.
 *   LPCSTR			szName		Name of node whose string is to be retrieved.
 *   char			*szBuffer	Buffer in which to store string data.
 *   unsigned int	cchBuffer	Length of buffer, including room for termiator.
 *
 * Return value: BOOL
 *   TRUE if node exists and sufficient room in buffer; FALSE otherwise.
 *
 * Notes:
 *   Non-strings put an empty string in the buffer. If the buffer is too small,
 *   as much will be copied in as possible.
 */

BOOL ConfigGetStringA(CONFIG *lpcfgRoot, LPCSTR szName, LPSTR szBuffer, unsigned int cchBuffer)
{
	CONFIG *lpcfg;
	
	/* Buffer must be at least one char long. */
	if(cchBuffer == 0) return FALSE;

	lpcfg = FindConfigNode(lpcfgRoot, szName);

	/* Not found. */
	if(!lpcfg)
		return FALSE;

	switch(lpcfg->entrytype)
	{
	case CET_STRING:
		
		lstrcpynA(szBuffer, lpcfg->sz, cchBuffer);
		
		/* lstrcpynA doesn't terminate if buffer full. */
		szBuffer[cchBuffer-1] = '\0';

		return (unsigned int)lstrlenA(lpcfg->sz) < cchBuffer;

	default:
		*szBuffer = '\0';
		return TRUE;	/* Even though it's not a string, still succeed. */
	}
}


BOOL ConfigGetStringW(CONFIG *lpcfgRoot, LPCSTR szName, LPWSTR szBuffer, unsigned int cchBuffer)
{
	CONFIG *lpcfg;
	
	/* Buffer must be at least one char long. */
	if(cchBuffer == 0) return FALSE;

	lpcfg = FindConfigNode(lpcfgRoot, szName);

	/* Not found. */
	if(!lpcfg)
		return FALSE;

	switch(lpcfg->entrytype)
	{
	case CET_STRING:
		
		_snwprintf(szBuffer, cchBuffer - 1, L"%S", lpcfg->sz);
		
		/* _snwprintf doesn't terminate if buffer full. */
		szBuffer[cchBuffer-1] = L'\0';

		return (unsigned int)lstrlenA(lpcfg->sz) < cchBuffer;

	default:
		*szBuffer = L'\0';
		return TRUE;	/* Even though it's not a string, still succeed. */
	}
}


/* ConfigGetSubsection
 *   Gets a subsection from within a config structure.
 *
 * Parameters:
 *   CONFIG*	lpcfgRoot	Root of config tree.
 *   LPCSTR		szName		Name of node whose subsect. is to be retrieved.
 *
 * Return value: CONFIG*
 *   Pointer to root (not container!) of subsection, or NULL on error.
 */
CONFIG* ConfigGetSubsection(CONFIG *lpcfgRoot, LPCSTR szName)
{
	CONFIG *lpcfg;

	lpcfg = FindConfigNode(lpcfgRoot, szName);

	/* Not found, or not a subsection. */
	if(!lpcfg || lpcfg->entrytype != CET_SUBSECTION)
		return NULL;

	return lpcfg->lpcfgSubsection;
}



/* ConfigCreate
 *   Creates a new root node.
 *
 * Parameters: None.
 *
 * Return value: CONFIG*
 *   Pointer to new root.
 *
 * Remarks:
 *   This is what the rest of the world should use to create a blank config. If
 *   they want to load a config from disc, they should use LoadConfig instead.
 */
CONFIG *ConfigCreate(void)
{
	return CreateConfigNode(NODEROOT);
}



/* ConfigDuplicate
 *   Duplicates a config node and its descendants.
 *
 * Parameters:
 *   CONFIG*	lpcfg	Config to duplicate.
 *
 * Return value: CONFIG*
 *   Pointer to duplicate.
 *
 * Remarks:
 *   Map configs, in particular, want two copies of thing data in different
 *   places. We can't just have two pointers to the same location, or we'd
 *   explode when we tried to free the config.
 *     The values of the undefined data in the original config is undefined in
 *   the new, i.e. there's no guarantee that the fluff is copied, whatever it
 *   may be.
 */
CONFIG* ConfigDuplicate(CONFIG *lpcfg)
{
	CONFIG *lpcfgNew;

	/* Makes the recursion slightly neater. */
	if(!lpcfg) return NULL;
	
	/* New node. This duplicates the name, of course. */
	lpcfgNew = CreateConfigNode(lpcfg->szName);

	/* Duplicate the contents and descendants. */
	lpcfgNew->entrytype = lpcfg->entrytype;
	lpcfgNew->lpcfgLeft = ConfigDuplicate(lpcfg->lpcfgLeft);
	lpcfgNew->lpcfgRight = ConfigDuplicate(lpcfg->lpcfgRight);

	/* If the value has memory associated with it, duplicate that, too. */
	switch(lpcfg->entrytype)
	{
	case CET_STRING:
		lpcfgNew->sz = ProcHeapAlloc(lstrlenA(lpcfg->sz) + 1);
		CopyMemory(lpcfgNew->sz, lpcfg->sz, lstrlenA(lpcfg->sz) + 1);
		break;
	case CET_SUBSECTION:
		lpcfgNew->lpcfgSubsection = ConfigDuplicate(lpcfg->lpcfgSubsection);
		break;
	case CET_INT:
		lpcfgNew->i = lpcfg->i;
		break;
	case CET_FLOAT:
		lpcfgNew->f = lpcfg->f;
		break;
	}

	return lpcfgNew;
}



/* ConfigIterate
 *  Calls a function for each item in a config tree.
 *
 * Parameters:
 *   CONFIG*					lpcfgRoot		Pointer to root of config.
 *   BOOL (*)(CONFIG*, void*)	lpfnCallback	Function to call for each item.
 *   void*						lpvParam		Extra parameter to callback.
 *
 * Return value: BOOL
 *   FALSE if iteration was terminated by the callback function; TRUE otherwise.
 */
BOOL ConfigIterate(CONFIG *lpcfgRoot, BOOL (*lpfnCallback)(CONFIG*, void*), void *lpvParam)
{
	while(lpcfgRoot)
	{
		/* Loop to the left and recur to the right. More efficient than just
		 * recurring down both sides.
		 */

		if(lpcfgRoot->lpcfgLeft)
			if(!lpfnCallback(lpcfgRoot->lpcfgLeft, lpvParam)) return FALSE;
		if(lpcfgRoot->lpcfgRight)
		{
			if(!lpfnCallback(lpcfgRoot->lpcfgRight, lpvParam)) return FALSE;
			ConfigIterate(lpcfgRoot->lpcfgRight, lpfnCallback, lpvParam);
		}

		lpcfgRoot = lpcfgRoot->lpcfgLeft;
	}

	return TRUE;
}
