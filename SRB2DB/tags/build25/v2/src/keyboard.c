#include <windows.h>

#include "keyboard.h"
#include "options.h"
#include "resource.h"


#define NOCOMMAND 0xFFFF

/* Shift masks used for shortcut keys. */
#define SC_SHIFT	0x10000
#define SC_CTRL		0x20000
#define SC_ALT		0x40000
#define SC_KEY		0xFFFF


/* Globals. */
HACCEL g_hAccel = NULL;

/* Mapping table of shortcut keys to menu commands. */
/* NOCOMMAND indicates that no menu command is associated with the key. */
static USHORT g_unShortcutsToMenu[SCK_MAX] =
{
	NOCOMMAND,				/* SCK_EDITQUICKMOVE */
	NOCOMMAND,				/* SCK_ZOOMIN */
	NOCOMMAND,				/* SCK_ZOOMOUT */
	IDM_VIEW_CENTRE,		/* SCK_CENTREVIEW */
	IDM_VIEW_MODE_MOVE,		/* SCK_EDIT_MOVE */
	IDM_VIEW_MODE_LINES,	/* SCK_EDITLINES */
	IDM_VIEW_MODE_SECTORS,	/* SCK_EDITSECTORS */
	IDM_VIEW_MODE_VERTICES,	/* SCK_EDITVERTICES */
	IDM_VIEW_MODE_THINGS,	/* SCK_EDITTHINGS */
	IDM_VIEW_3D,			/* SCK_EDIT3D */
};


/* MakeShiftedKeyCode
 *   Maps a VK code to a character code with shift states.
 *
 * Parameters:
 *   int		iVirtKey	Virtual key code.
 *
 * Return value: int
 *   DB-esque key-code containing character code and shift states.
 */
int MakeShiftedKeyCode(int iVirtKey)
{
	/* TODO: Does VB actually use VK codes, or chars? If the latter, be
	 * careful: you're processing mousewheel events too!
	 */
	return iVirtKey
		+ ((GetKeyState(VK_SHIFT) & 0x8000) ? 0x10000 : 0)
		+ ((GetKeyState(VK_CONTROL) & 0x8000) ? 0x20000 : 0)
		+ ((GetKeyState(VK_MENU) & 0x8000) ? 0x40000 : 0);
}


/* UpdateAcceleratorFromOptions
 *   Copies shortcut keys into accelerator table.
 *
 * Parameters:
 *   None.
 *
 * Return value: None.
 *
 * Notes:
 *   Call this after first loading the config file, and then every time the user
 *   changes the shortcut keys.
 */
void UpdateAcceleratorFromOptions(void)
{
	ACCEL accel[SCK_MAX];
	int i, iAccelCount;

	/* We recreate the table here, so destroy it if it exists already. */
	if(g_hAccel) DestroyCustomAccelerator();

	/* First entry is dummy until something overwrites it. */
	accel[0].cmd = (unsigned short)-1;
	accel[0].fVirt = 0;
	accel[0].key = 0;

	/* Loop through all shortcut keys, copying into accel struct. */
	iAccelCount = 0;
	for(i=0; i<SCK_MAX; i++)
	{
		if(g_unShortcutsToMenu[i] != NOCOMMAND && g_iShortcutCodes[i] != 0)
		{
			accel[iAccelCount].cmd = g_unShortcutsToMenu[i];

			/* Build shift-mask. */
			accel[iAccelCount].fVirt = 0;
			if(g_iShortcutCodes[i] & SC_SHIFT) accel[iAccelCount].fVirt |= FSHIFT;
			if(g_iShortcutCodes[i] & SC_CTRL) accel[iAccelCount].fVirt |= FCONTROL;
			if(g_iShortcutCodes[i] & SC_ALT) accel[iAccelCount].fVirt |= FALT;

			/* TODO: Work out whether this is okay: */
			accel[iAccelCount].fVirt |= FVIRTKEY;
			
			accel[iAccelCount].key = g_iShortcutCodes[i] & SC_KEY;

			iAccelCount++;
		}
	}

	g_hAccel = CreateAcceleratorTable(accel, max(iAccelCount, 1));
}