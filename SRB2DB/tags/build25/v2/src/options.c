#include <windows.h>

#include "general.h"
#include "config.h"
#include "options.h"
#include "keyboard.h"
#include "resource.h"

/* Macros. */
#define OPTFILE_SIGNATURE "SRB2 Builder Configuration"
#define OPTFILENAME TEXT("builder.cfg")


/* Globals. */
CONFIG *g_lpcfgMain = NULL;
int g_iShortcutCodes[SCK_MAX];
RENDEREROPTIONS g_rendopts;


/* Static prototypes. */
static void LoadShortcutKeys(void);
static void SetShortcutKeys(void);
static void LoadRendererOptions(void);
static void SetRendererOptions(void);


/* LoadMainConfigurationFile
 *   Loads the main settings file and reads the settings into their appropriate
 *   places.
 *
 * Parameters:
 *   None.
 *
 * Return value: BOOL
 *   TRUE on success; FALSE on failure.
 */
BOOL LoadMainConfigurationFile(void)
{
	char *szType;
	int iLen;

	g_lpcfgMain = LoadConfig(OPTDIR OPTFILENAME);
	if(!g_lpcfgMain) return FALSE;

	iLen = ConfigGetStringLength(g_lpcfgMain, "type");

	if(iLen <= 0)
	{
		UnloadMainConfigurationFile();
		return FALSE;
	}

	szType = ProcHeapAlloc((iLen + 1) * sizeof(char));
	ConfigGetStringA(g_lpcfgMain, "type", szType, iLen + 1);

	if(lstrcmpiA(szType, OPTFILE_SIGNATURE))
	{
		UnloadMainConfigurationFile();
		ProcHeapFree(szType);
		return FALSE;
	}

	ProcHeapFree(szType);

	/* Copies from tree into other storage, for efficiency. */
	LoadShortcutKeys();
	LoadRendererOptions();

	return TRUE;
}


/* UnloadMainConfigurationFile
 *   Cleans up the settings structures.
 *
 * Parameters:
 *   None.
 *
 * Return value: None.
 */
void UnloadMainConfigurationFile(void)
{
	if(g_lpcfgMain)
	{
		/* Copy settings back into the tree. */
		SetShortcutKeys();
		SetRendererOptions();

		/* WriteConfig(OPTDIR OPTFILENAME); */

		ConfigDestroy(g_lpcfgMain);
	}
}


/* LoadShortcutKeys, SetShortcutKeys
 *   Copies shortcut key settings between the tree and the array.
 *
 * Parameters:
 *   None.
 *
 * Return value: None.
 */
static void LoadShortcutKeys(void)
{
	CONFIG *lpcfgShortcuts = ConfigGetSubsection(g_lpcfgMain, "shortcuts");

	if(!lpcfgShortcuts)
	{
		/* Shortcuts section is missing for some reason. Just set everything to
		 * zero (no key assigned).
		 */
		int i;

		for(i=0; i<SCK_MAX; i++) g_iShortcutCodes[i] = 0;

		return;
	}

	g_iShortcutCodes[SCK_EDITQUICKMOVE] = ConfigGetInteger(lpcfgShortcuts, "editquickmove");
	g_iShortcutCodes[SCK_ZOOMIN] = ConfigGetInteger(lpcfgShortcuts, "zoomin");
	g_iShortcutCodes[SCK_ZOOMOUT] = ConfigGetInteger(lpcfgShortcuts, "zoomout");
	g_iShortcutCodes[SCK_CENTREVIEW] = ConfigGetInteger(lpcfgShortcuts, "editcenterview");
	g_iShortcutCodes[SCK_EDITMOVE] = ConfigGetInteger(lpcfgShortcuts, "editmove");
	g_iShortcutCodes[SCK_EDITLINES] = ConfigGetInteger(lpcfgShortcuts, "editlines");
	g_iShortcutCodes[SCK_EDITSECTORS] = ConfigGetInteger(lpcfgShortcuts, "editsectors");
	g_iShortcutCodes[SCK_EDITVERTICES] = ConfigGetInteger(lpcfgShortcuts, "editvertices");
	g_iShortcutCodes[SCK_EDITTHINGS] = ConfigGetInteger(lpcfgShortcuts, "editthings");
	g_iShortcutCodes[SCK_EDIT3D] = ConfigGetInteger(lpcfgShortcuts, "edit3d");

	/* Create the accelerator table. */
	UpdateAcceleratorFromOptions();
}

static void SetShortcutKeys(void)
{
	CONFIG *lpcfgShortcuts;

	if(!ConfigNodeExists(g_lpcfgMain, "shortcuts"))
	{
		/* Shortcuts ss doesn't exist; create it. */
		lpcfgShortcuts = ConfigAddSubsection(g_lpcfgMain, "shortcuts");
	}
	else lpcfgShortcuts = ConfigGetSubsection(g_lpcfgMain, "shortcuts");

	ConfigSetInteger(lpcfgShortcuts, "editquickmove", g_iShortcutCodes[SCK_EDITQUICKMOVE]);
	ConfigSetInteger(lpcfgShortcuts, "zoomin", g_iShortcutCodes[SCK_ZOOMIN]);
	ConfigSetInteger(lpcfgShortcuts, "zoomout", g_iShortcutCodes[SCK_ZOOMOUT]);
	ConfigSetInteger(lpcfgShortcuts, "editmove", g_iShortcutCodes[SCK_EDITMOVE]);
	ConfigSetInteger(lpcfgShortcuts, "editlines", g_iShortcutCodes[SCK_EDITLINES]);
	ConfigSetInteger(lpcfgShortcuts, "editsectors", g_iShortcutCodes[SCK_EDITSECTORS]);
	ConfigSetInteger(lpcfgShortcuts, "editvertices", g_iShortcutCodes[SCK_EDITVERTICES]);
	ConfigSetInteger(lpcfgShortcuts, "editthings", g_iShortcutCodes[SCK_EDITTHINGS]);
	ConfigSetInteger(lpcfgShortcuts, "edit3d", g_iShortcutCodes[SCK_EDIT3D]);
}


/* LoadRendererOptions, SetRendererOptions
 *   Copies renderer settings between the tree and the structure.
 *
 * Parameters:
 *   None.
 *
 * Return value: None.
 */
static void LoadRendererOptions(void)
{
	g_rendopts.iIndicatorSize = ConfigGetInteger(g_lpcfgMain, "indicatorsize");
	g_rendopts.iVertexSize = ConfigGetInteger(g_lpcfgMain, "vertexsize");
	g_rendopts.bVerticesInLinesMode = ConfigGetInteger(g_lpcfgMain, "mode1vertices");
	g_rendopts.bVerticesInSectorsMode = ConfigGetInteger(g_lpcfgMain, "mode2vertices");
}

static void SetRendererOptions(void)
{
	ConfigSetInteger(g_lpcfgMain, "indicatorsize", g_rendopts.iIndicatorSize);
	ConfigSetInteger(g_lpcfgMain, "vertexsize", g_rendopts.iVertexSize);
	ConfigSetInteger(g_lpcfgMain, "mode1vertices", g_rendopts.bVerticesInLinesMode);
	ConfigSetInteger(g_lpcfgMain, "mode2vertices", g_rendopts.bVerticesInSectorsMode);
}
