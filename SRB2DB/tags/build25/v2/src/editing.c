#include <windows.h>
#include <stdlib.h>
#include <search.h>

#include "general.h"
#include "map.h"
#include "renderer.h"
#include "mapconfig.h"
#include "selection.h"
#include "config.h"
#include "editing.h"
#include "resource.h"

#include "win/infobar.h"

#include "CodeImp/ci_map.h"
#include "CodeImp/ci_math.h"
#include "CodeImp/ci_data_proto.h"



/* Types. */
typedef struct _LDLOOKUP
{
	unsigned int uiBufSize, uiCount;
	int *lpiIndices;
} LDLOOKUP;

enum ENUM_LDLOOKUP_TYPES
{
	LDLT_VXTOVX, LDLT_VXTOLD
};


/* Macros. */
#define LDLOOKUP_INITBUFSIZE 8

/* Number of new vertices/lines to store per buffer increment. */
#define DRAWOP_BUFFER_COUNT_INCREMENT 64

/* Static prototypes. */
static BOOL __fastcall VerticesReachableByLookup(LDLOOKUP *lpldlookup, BOOL *lpbVisited, int iVertex1, int iVertex2);




/* Info used to find intersection of multiple objects' properties. */
typedef struct _CHECKINFO
{
	MAP		*lpmap;
	CONFIG	*lpcfg;
	BOOL	bFirstTime;
	void	*lpdi;		/* [LINEDEF/SECTOR/THING/VERTEX]DISPLAYINFO. */
	DWORD	dwDisplayFlags;
} CHECKINFO;


/* Static prototypes. */
static BOOL CheckLineCallback(int iIndex, CHECKINFO *lpci);
static BOOL CheckSectorCallback(int iIndex, CHECKINFO *lpci);
static BOOL CheckThingCallback(int iIndex, CHECKINFO *lpci);
static BOOL CheckVerticesCallback(int iIndex, CHECKINFO *lpci);
static void AddVertexToDrawList(MAP *lpmap, DRAW_OPERATION *lpdrawop, int iVertex);
static void AddLinedefToDrawList(MAP *lpmap, DRAW_OPERATION *lpdrawop, int iLinedef);
static void GetLineSideSpot(MAP *lpmap, int iLinedef, int iSideOfLine, float fDistance, float *lpx, float *lpy);
static LDLOOKUP *BuildLDLookupTable(MAP *lpmap, int iLookupType);
static void DestroyLDLookupTable(MAP *lpmap, LDLOOKUP *lpldlookup);
static void __fastcall PropagateNewSector(MAP *lpmap, LDLOOKUP *lpldlookup, int iLinedef, int iLinedefSide, int iNewSector);
static int FindNearestAdjacentLinedef(MAP *lpmap, LDLOOKUP *lpldlookup, int iLinedef, int iLindefSide, int iWhichVertex);
static double AdjacentLinedefAngleFromFront(MAP *lpmap, int iLinedef1, int iLinedef2);
static double LinedefAngleV1(MAP *lpmap, int iLinedef);
static double LinedefAngleV2(MAP *lpmap, int iLinedef);
static void SetLinedefSidedef(MAP *lpmap, int iLinedef, int iSidedef, int iLinedefSide);


/* RemoveUnusedVertices
 *   Removes all unused vertices from a map.
 *
 * Parameters:
 *   MAP*			lpmap		Map.
 *
 * Return value: int
 *   Number of vertices removed.
 */
int RemoveUnusedVertices(MAP *lpmap)
{
	int i, iVerticesRemoved = 0;
	BOOL *lpbyUsedVertices;

	/* Trivial case. */
	if(lpmap->iVertices <= 0) return 0;

	/* Allocate storage for determining which vertices are used. */
	lpbyUsedVertices = ProcHeapAlloc(lpmap->iVertices * sizeof(BOOL));
	ZeroMemory(lpbyUsedVertices, lpmap->iVertices * sizeof(BOOL));

	/* Loop through all linedefs, marking each vertex as used. */
	for(i = 0; i < lpmap->iLinedefs; i++)
	{
		lpbyUsedVertices[lpmap->linedefs[i].v1] = TRUE;
		lpbyUsedVertices[lpmap->linedefs[i].v2] = TRUE;
	}

	/* Loop through all vertices, removing the unused ones. */
	for(i = lpmap->iVertices; i >= 0; i--)
	{
		if(!lpbyUsedVertices[i])
		{
			iVerticesRemoved++;
			RemoveVertex(lpmap, i);
		}
	}

	return iVerticesRemoved;
}







/* GetLinedefDisplayInfo
 *   Builds info-bar details for one linedef.
 *
 * Parameters:
 *   MAP*					lpmap			Map.
 *   CONFIG*				lpcfgLinedefs	Linedef effect descriptions.
 *   int					iIndex			Index of linedef.
 *   LINEDEFDISPLAYINFO*	lplddi			Pointer to return buffer.
 *
 * Return value: None.
 */
void GetLinedefDisplayInfo(MAP *lpmap, CONFIG *lpcfgLinedefs, int iIndex, LINEDEFDISPLAYINFO *lplddi)
{
	MAPLINEDEF			*lpld = &lpmap->linedefs[iIndex];
	MAPVERTEX			*lpv1 = &lpmap->vertices[lpld->v1];
	MAPVERTEX			*lpv2 = &lpmap->vertices[lpld->v2];
	MAPSIDEDEF			*lpsd1 = lpld->s1 >= 0 ? &lpmap->sidedefs[lpld->s1] : NULL;
	MAPSIDEDEF			*lpsd2 = lpld->s2 >= 0 ? &lpmap->sidedefs[lpld->s2] : NULL;
	MAPSECTOR			*lps1 = lpsd1 && lpsd1->sector >= 0 && lpsd1->sector < lpmap->iSectors ? &lpmap->sectors[lpsd1->sector] : NULL;
	MAPSECTOR			*lps2 = lpsd2 && lpsd2->sector >= 0 && lpsd2->sector < lpmap->iSectors ? &lpmap->sectors[lpsd2->sector] : NULL;

	lplddi->iIndex = iIndex;
	lplddi->cxVector = lpv2->x - lpv1->x;
	lplddi->cyVector = lpv2->y - lpv1->y;
	lplddi->unTag = lpld->tag;

	/* Ellipses are handled by the control's styles. */
	GetEffectDisplayText(lpld->effect, lpcfgLinedefs, lplddi->szEffect, sizeof(lplddi->szEffect) / sizeof(TCHAR));
	lplddi->unEffect = lpld->effect;
	
	if(lplddi->bHasFrontSec = (lps1 != NULL))
	{
		lplddi->unFrontSector = lpsd1->sector;
		lplddi->iFrontHeight = lps1->hceiling - lps1->hfloor;
	}

	if(lplddi->bHasBackSec = (lps2 != NULL))
	{
		lplddi->unBackSector = lpsd2->sector;
		lplddi->iBackHeight = lps2->hceiling - lps2->hfloor;
	}

	if(lplddi->bHasFront = (lpsd1 != NULL))
	{
		lplddi->nFrontX = lpsd1->tx;
		lplddi->nFrontY = lpsd1->ty;

		wsprintf(lplddi->szFrontUpper, TEXT("%.8hs"), lpsd1->upper);
		wsprintf(lplddi->szFrontMiddle, TEXT("%.8hs"), lpsd1->middle);
		wsprintf(lplddi->szFrontLower, TEXT("%.8hs"), lpsd1->lower);

		lplddi->szFrontUpper[8] = lplddi->szFrontMiddle[8] = lplddi->szFrontLower[8] = '\0';
	}

	if(lplddi->bHasBack = (lpsd2 != NULL))
	{
		lplddi->nBackX = lpsd2->tx;
		lplddi->nBackY = lpsd2->ty;

		wsprintf(lplddi->szBackUpper, TEXT("%.8hs"), lpsd2->upper);
		wsprintf(lplddi->szBackMiddle, TEXT("%.8hs"), lpsd2->middle);
		wsprintf(lplddi->szBackLower, TEXT("%.8hs"), lpsd2->lower);

		lplddi->szBackUpper[8] = lplddi->szBackMiddle[8] = lplddi->szBackLower[8] = '\0';
	}
}



/* GetSectorDisplayInfo
 *   Builds info-bar details for one sector.
 *
 * Parameters:
 *   MAP*				lpmap			Map.
 *   CONFIG*			lpcfgSectors	Sector effects from map config.
 *   int				iIndex			Index of sector.
 *   SECTORDISPLAYINFO*	lpsdi			Pointer to return buffer.
 *
 * Return value: None.
 */
void GetSectorDisplayInfo(MAP *lpmap, CONFIG *lpcfgSectors, int iIndex, SECTORDISPLAYINFO *lpsdi)
{
	MAPSECTOR *lpsec = &lpmap->sectors[iIndex];

	/* Ellipses are handled by the control's styles. */
	GetEffectDisplayText(lpsec->special, lpcfgSectors, lpsdi->szEffect, sizeof(lpsdi->szEffect) / sizeof(TCHAR));
	lpsdi->unEffect = lpsec->special;

	lpsdi->iIndex = iIndex;
	lpsdi->ucBrightness = (unsigned char)lpsec->brightness;
	lpsdi->nCeiling = lpsec->hceiling;
	lpsdi->nFloor = lpsec->hfloor;
	lpsdi->unTag = lpsec->tag;

	wsprintf(lpsdi->szFloor, TEXT("%.8hs"), lpsec->tfloor);
	wsprintf(lpsdi->szCeiling, TEXT("%.8hs"), lpsec->tceiling);
	lpsdi->szFloor[8] = lpsdi->szFloor[8] = '\0';
}



/* GetThingDisplayInfo
 *   Builds info-bar details for one thing.
 *
 * Parameters:
 *   MAP*				lpmap		Map.
 *   CONFIG*			lpcfgThings	Thing properties (flat) from map config.
 *   int				iIndex		Index of thing.
 *   THINGDISPLAYINFO*	lptdi		Pointer to buffer in which to store data.
 *
 * Return value: None.
 */
void GetThingDisplayInfo(MAP *lpmap, CONFIG *lpcfgThings, int iIndex, THINGDISPLAYINFO *lptdi)
{
	MAPTHING *lpthing = &lpmap->things[iIndex];
	CONFIG *lpcfgThingProperties;
				
	GetThingTypeDisplayText(lpthing->thing, lpcfgThings, lptdi->szType, sizeof(lptdi->szType) / sizeof(TCHAR));
	lptdi->unType = lpthing->thing;

	lptdi->iIndex = iIndex;
	lptdi->x = lpthing->x;
	lptdi->y = lpthing->y;
	lptdi->z = lpthing->flag >> 4;		/* TODO: Use correct factor. */
	lptdi->unFlags = lpthing->flag;
	GetThingDirectionDisplayText(lpthing->angle, lptdi->szDirection, sizeof(lptdi->szDirection) / sizeof(TCHAR));
	lptdi->unDirection = lpthing->angle;

	if(lpcfgThingProperties = GetThingConfigInfo(lpcfgThings, (unsigned short)lpthing->thing))
		ConfigGetString(lpcfgThingProperties, "sprite", lptdi->szSprite, sizeof(lptdi->szSprite) / sizeof(TCHAR));
	else lptdi->szSprite[0] = '\0';
}



/* GetVertexDisplayInfo
 *   Builds info-bar details for one vertex.
 *
 * Parameters:
 *   MAP*						lpmap	Map.
 *   int						iIndex	Index of vertex.
 *   VERTEXDISPLAYINFO*	lpvdi	Pointer to buffer in which to store data.
 *
 * Return value: None.
 */
void GetVertexDisplayInfo(MAP *lpmap, int iIndex, VERTEXDISPLAYINFO *lpvdi)
{
	MAPVERTEX *lpvx = &lpmap->vertices[iIndex];

	lpvdi->iIndex = iIndex;
	lpvdi->x = lpvx->x;
	lpvdi->y = lpvx->y;
}



/* CheckLineCallback
 *   Sets linedef display information fields and determines which of those match
 *   with the existing data.
 *
 * Parameters:
 *   int		iIndex	Index of linedef.
 *   CHECKINFO*	lpci	Existing aggregate data, validity flags, field
 *						specifying whether this is the first iteration.
 *
 * Return value: BOOL
 *   TRUE if iteration should continue; FALSE otherwise.
 *
 * Remarks:
 *   This is used as a callback by IterateOverSelection. It is handy for
 *   building the set of intersecting properties for selected linedefs.
 */
static BOOL CheckLineCallback(int iIndex, CHECKINFO *lpci)
{
	LINEDEFDISPLAYINFO lddi;
	LINEDEFDISPLAYINFO *lplddiAggregate = (LINEDEFDISPLAYINFO*)lpci->lpdi;

	/* Get all the info for the specified linedef. */
	GetLinedefDisplayInfo(lpci->lpmap, lpci->lpcfg, iIndex, &lddi);

	/* We now have different behaviour depending on whether this is the first
	 * iteration.
	 */
	if(lpci->bFirstTime)
	{
		/* If this is the first time, everything's okay, so copy it all. */
		*lplddiAggregate = lddi;
		lpci->dwDisplayFlags = LDDIF_ALL;
		lpci->bFirstTime = FALSE;
	}
	else
	{
		/* We've iterated at least once already, so find what matches. We work
		 * subtractively.
		 */
		if(lddi.iIndex != lplddiAggregate->iIndex) lpci->dwDisplayFlags &= ~LDDIF_INDEX;
		if(lddi.unEffect != lplddiAggregate->unEffect) lpci->dwDisplayFlags &= ~LDDIF_EFFECT;
		if(lddi.cxVector != lplddiAggregate->cxVector || lddi.cyVector != lplddiAggregate->cyVector) lpci->dwDisplayFlags &= ~LDDIF_VECTOR;
		if(lddi.cxVector * lddi.cxVector + lddi.cyVector * lddi.cyVector !=
			lplddiAggregate->cxVector * lplddiAggregate->cxVector + lplddiAggregate->cyVector * lplddiAggregate->cyVector)
			lpci->dwDisplayFlags &= ~LDDIF_LENGTH;
		if(lddi.unTag != lplddiAggregate->unTag) lpci->dwDisplayFlags &= ~LDDIF_TAG;

		/* Now we have to be a bit careful. We have to negotiate the Boolean
		 * fields with consideration to their meaning. */
		if(lddi.bHasFrontSec != lplddiAggregate->bHasFrontSec) lpci->dwDisplayFlags &= ~(LDDIF_FRONTHEIGHT | LDDIF_FRONTSEC);
		else if(lddi.bHasFrontSec)
		{
			if(lddi.unFrontSector != lplddiAggregate->unFrontSector) lpci->dwDisplayFlags &= ~LDDIF_FRONTSEC;
			if(lddi.iFrontHeight != lplddiAggregate->iFrontHeight) lpci->dwDisplayFlags &= ~LDDIF_FRONTHEIGHT;
		}

		if(lddi.bHasBackSec != lplddiAggregate->bHasBackSec) lpci->dwDisplayFlags &= ~(LDDIF_BACKHEIGHT | LDDIF_BACKSEC);
		else if(lddi.bHasBackSec)
		{
			if(lddi.unBackSector != lplddiAggregate->unBackSector) lpci->dwDisplayFlags &= ~LDDIF_BACKSEC;
			if(lddi.iFrontHeight != lplddiAggregate->iFrontHeight) lpci->dwDisplayFlags &= ~LDDIF_BACKHEIGHT;
		}

		if(lddi.bHasFront != lplddiAggregate->bHasFront) lpci->dwDisplayFlags &= ~(LDDIF_FRONTX | LDDIF_FRONTY);
		else if(lddi.bHasFront)
		{
			if(lddi.nFrontX != lplddiAggregate->nFrontX) lpci->dwDisplayFlags &= ~LDDIF_FRONTX;
			if(lddi.nFrontY != lplddiAggregate->nFrontY) lpci->dwDisplayFlags &= ~LDDIF_FRONTY;


			/* Texture comparisons now. We make the additional initial check as to
			 * whether we care about them anyway, since the string comparisons are
			 * quite expensive.
			 */

			if((lpci->dwDisplayFlags & LDDIF_FRONTUPPER) && lstrcmp(lddi.szFrontUpper, lplddiAggregate->szFrontUpper))
				 lpci->dwDisplayFlags &= ~LDDIF_FRONTUPPER;

			if((lpci->dwDisplayFlags & LDDIF_FRONTMIDDLE) && lstrcmp(lddi.szFrontMiddle, lplddiAggregate->szFrontMiddle))
				 lpci->dwDisplayFlags &= ~LDDIF_FRONTMIDDLE;

			if((lpci->dwDisplayFlags & LDDIF_FRONTLOWER) && lstrcmp(lddi.szFrontLower, lplddiAggregate->szFrontLower))
				 lpci->dwDisplayFlags &= ~LDDIF_FRONTLOWER;
		}

		if(lddi.bHasBack != lplddiAggregate->bHasBack) lpci->dwDisplayFlags &= ~(LDDIF_BACKX | LDDIF_BACKY);
		else if(lddi.bHasBack)
		{
			if(lddi.nBackX != lplddiAggregate->nBackX) lpci->dwDisplayFlags &= ~LDDIF_BACKX;
			if(lddi.nBackY != lplddiAggregate->nBackY) lpci->dwDisplayFlags &= ~LDDIF_BACKY;

			/* Texture comparisons now. We make the additional initial check as to
			 * whether we care about them anyway, since the string comparisons are
			 * quite expensive.
			 */

			if((lpci->dwDisplayFlags & LDDIF_BACKUPPER) && lstrcmp(lddi.szBackUpper, lplddiAggregate->szBackUpper))
				 lpci->dwDisplayFlags &= ~LDDIF_BACKUPPER;

			if((lpci->dwDisplayFlags & LDDIF_BACKMIDDLE) && lstrcmp(lddi.szBackMiddle, lplddiAggregate->szBackMiddle))
				 lpci->dwDisplayFlags &= ~LDDIF_BACKMIDDLE;

			if((lpci->dwDisplayFlags & LDDIF_BACKLOWER) && lstrcmp(lddi.szBackLower, lplddiAggregate->szBackLower))
				 lpci->dwDisplayFlags &= ~LDDIF_BACKLOWER;
		}
	}

	/* Keep going, unless we already know we have nothing in common. */
	return (lpci->dwDisplayFlags != 0);
}


DWORD CheckLines(MAP *lpmap, SELECTION_LIST *lpsellist, CONFIG *lpcfgLinedefTypes, LINEDEFDISPLAYINFO *lplddi)
{
	CHECKINFO checkinfo;
	int i;

	/* Set the first-iteration field. */
	checkinfo.bFirstTime = TRUE;
	checkinfo.lpdi = (void*)lplddi;
	checkinfo.lpmap = lpmap;
	checkinfo.lpcfg = lpcfgLinedefTypes;

	for(i = 0; i < lpsellist->iDataCount; i++)
		CheckLineCallback(lpsellist->lpiIndices[i], &checkinfo);

	return checkinfo.dwDisplayFlags;
}


DWORD CheckSectors(MAP *lpmap, SELECTION_LIST *lpsellist, CONFIG *lpcfgSecTypes, SECTORDISPLAYINFO *lpsdi)
{
	CHECKINFO checkinfo;
	int i;

	/* Set the first-iteration field. */
	checkinfo.bFirstTime = TRUE;
	checkinfo.lpdi = (void*)lpsdi;
	checkinfo.lpmap = lpmap;
	checkinfo.lpcfg = lpcfgSecTypes;

	for(i = 0; i < lpsellist->iDataCount; i++)
		CheckSectorCallback(lpsellist->lpiIndices[i], &checkinfo);

	return checkinfo.dwDisplayFlags;
}


DWORD CheckThings(MAP *lpmap, SELECTION_LIST *lpsellist, CONFIG *lpcfgFlatThings, THINGDISPLAYINFO *lptdi)
{
	CHECKINFO checkinfo;
	int i;

	/* Set the first-iteration field. */
	checkinfo.bFirstTime = TRUE;
	checkinfo.lpdi = (void*)lptdi;
	checkinfo.lpmap = lpmap;
	checkinfo.lpcfg = lpcfgFlatThings;

	for(i = 0; i < lpsellist->iDataCount; i++)
		CheckThingCallback(lpsellist->lpiIndices[i], &checkinfo);

	return checkinfo.dwDisplayFlags;
}


DWORD CheckVertices(MAP *lpmap, SELECTION_LIST *lpsellist, VERTEXDISPLAYINFO *lpvdi)
{
	CHECKINFO checkinfo;
	int i;

	/* Set the first-iteration field. */
	checkinfo.bFirstTime = TRUE;
	checkinfo.lpdi = (void*)lpvdi;
	checkinfo.lpmap = lpmap;

	for(i = 0; i < lpsellist->iDataCount; i++)
		CheckVerticesCallback(lpsellist->lpiIndices[i], &checkinfo);

	return checkinfo.dwDisplayFlags;
}


/* CheckSectorCallback
 *   Sets sector display information fields and determines which of those match
 *   with the existing data.
 *
 * Parameters:
 *   int		iIndex	Index of sector.
 *   CHECKINFO*	lpci	Existing aggregate data, validity flags, field
 *						specifying whether this is the first iteration.
 *
 * Return value: BOOL
 *   TRUE if iteration should continue; FALSE otherwise.
 *
 * Remarks:
 *   This is used as a callback by IterateOverSelection. It is handy for
 *   building the set of intersecting properties for selected sectors.
 */
static BOOL CheckSectorCallback(int iIndex, CHECKINFO *lpci)
{
	SECTORDISPLAYINFO sdi;
	SECTORDISPLAYINFO *lpsdiAggregate = (SECTORDISPLAYINFO*)lpci->lpdi;

	/* Get all the info for the specified sector. */
	GetSectorDisplayInfo(lpci->lpmap, lpci->lpcfg, iIndex, &sdi);

	/* We now have different behaviour depending on whether this is the first
	 * iteration.
	 */
	if(lpci->bFirstTime)
	{
		/* If this is the first time, everything's okay, so copy it all. */
		*lpsdiAggregate = sdi;
		lpci->dwDisplayFlags = SDIF_ALL;
		lpci->bFirstTime = FALSE;
	}
	else
	{
		/* We've iterated at least once already, so find what matches. We work
		 * subtractively.
		 */
		if(sdi.iIndex != lpsdiAggregate->iIndex) lpci->dwDisplayFlags &= ~SDIF_INDEX;
		if(sdi.nCeiling != lpsdiAggregate->nCeiling) lpci->dwDisplayFlags &= ~SDIF_CEILING;
		if(sdi.nFloor != lpsdiAggregate->nFloor) lpci->dwDisplayFlags &= ~SDIF_FLOOR;
		if(sdi.nCeiling - sdi.nFloor != lpsdiAggregate->nCeiling - lpsdiAggregate->nFloor)
			lpci->dwDisplayFlags &= ~SDIF_HEIGHT;
		if(sdi.unEffect != lpsdiAggregate->unEffect) lpci->dwDisplayFlags &= ~SDIF_EFFECT;
		if(sdi.ucBrightness != lpsdiAggregate->ucBrightness) lpci->dwDisplayFlags &= ~SDIF_BRIGHTNESS;
		if(sdi.unTag != lpsdiAggregate->unTag) lpci->dwDisplayFlags &= ~SDIF_TAG;


		/* Texture comparisons now. We make the additional initial check as to
		 * whether we care about them anyway, since the string comparisons are
		 * quite expensive.
		 */

		if((lpci->dwDisplayFlags & SDIF_CEILINGTEX) && lstrcmp(sdi.szCeiling, lpsdiAggregate->szCeiling))
			 lpci->dwDisplayFlags &= ~SDIF_CEILINGTEX;

		if((lpci->dwDisplayFlags & SDIF_FLOORTEX) && lstrcmp(sdi.szFloor, lpsdiAggregate->szFloor))
			 lpci->dwDisplayFlags &= ~SDIF_FLOORTEX;
	}

	/* Keep going, unless we already know we have nothing in common. */
	return (lpci->dwDisplayFlags != 0);
}



/* CheckThingCallback
 *   Sets thing display information fields and determines which of those match
 *   with the existing data.
 *
 * Parameters:
 *   int		iIndex	Index of sector.
 *   CHECKINFO*	lpci	Existing aggregate data, validity flags, field
 *						specifying whether this is the first iteration.
 *
 * Return value: BOOL
 *   TRUE if iteration should continue; FALSE otherwise.
 *
 * Remarks:
 *   This is used as a callback by IterateOverSelection. It is handy for
 *   building the set of intersecting properties for selected things.
 */
static BOOL CheckThingCallback(int iIndex, CHECKINFO *lpci)
{
	THINGDISPLAYINFO tdi;
	THINGDISPLAYINFO *lptdiAggregate = (THINGDISPLAYINFO*)lpci->lpdi;

	/* Get all the info for the specified sector. */
	GetThingDisplayInfo(lpci->lpmap, lpci->lpcfg, iIndex, &tdi);

	/* We now have different behaviour depending on whether this is the first
	 * iteration.
	 */
	if(lpci->bFirstTime)
	{
		/* If this is the first time, everything's okay, so copy it all. */
		*lptdiAggregate = tdi;
		lpci->dwDisplayFlags = TDIF_ALL;
		lpci->bFirstTime = FALSE;
	}
	else
	{
		/* We've iterated at least once already, so find what matches. We work
		 * subtractively.
		 */
		if(tdi.iIndex != lptdiAggregate->iIndex) lpci->dwDisplayFlags &= ~TDIF_INDEX;
		if(tdi.unDirection != lptdiAggregate->unDirection) lpci->dwDisplayFlags &= ~TDIF_DIRECTION;
		if(tdi.unType != lptdiAggregate->unType) lpci->dwDisplayFlags &= ~TDIF_TYPE;
		if(tdi.unFlags != lptdiAggregate->unFlags) lpci->dwDisplayFlags &= ~TDIF_FLAGS;
		if(tdi.z != lptdiAggregate->z) lpci->dwDisplayFlags &= ~TDIF_Z;
		if(tdi.x != lptdiAggregate->x || tdi.y != lptdiAggregate->y) lpci->dwDisplayFlags &= ~TDIF_COORDS;


		/* Sprite comparisons now. We make the additional initial check as to
		 * whether we care about them anyway, since the string comparisons are
		 * quite expensive.
		 */

		if((lpci->dwDisplayFlags & TDIF_SPRITE) && lstrcmp(tdi.szSprite, lptdiAggregate->szSprite))
			 lpci->dwDisplayFlags &= ~TDIF_SPRITE;
	}

	/* Keep going, unless we already know we have nothing in common. */
	return (lpci->dwDisplayFlags != 0);
}

/* CheckVerticesCallback
 *   Sets vertex display information fields and determines which of those match
 *   with the existing data.
 *
 * Parameters:
 *   int		iIndex	Index of sector.
 *   CHECKINFO*	lpci	Existing aggregate data, validity flags, field
 *						specifying whether this is the first iteration.
 *
 * Return value: BOOL
 *   TRUE if iteration should continue; FALSE otherwise.
 *
 * Remarks:
 *   This is used as a callback by IterateOverSelection. It is handy for
 *   building the set of intersecting properties for selected vertices.
 */
static BOOL CheckVerticesCallback(int iIndex, CHECKINFO *lpci)
{
	VERTEXDISPLAYINFO vdi;
	VERTEXDISPLAYINFO *lpvdiAggregate = (VERTEXDISPLAYINFO*)lpci->lpdi;

	/* Get all the info for the specified sector. */
	GetVertexDisplayInfo(lpci->lpmap, iIndex, &vdi);

	/* We now have different behaviour depending on whether this is the first
	 * iteration.
	 */
	if(lpci->bFirstTime)
	{
		/* If this is the first time, everything's okay, so copy it all. */
		*lpvdiAggregate = vdi;
		lpci->dwDisplayFlags = VDIF_ALL;
		lpci->bFirstTime = FALSE;
	}
	else
	{
		/* We've iterated at least once already, so find what matches. We work
		 * subtractively.
		 */
		if(vdi.iIndex != lpvdiAggregate->iIndex) lpci->dwDisplayFlags &= ~VDIF_INDEX;
		if(vdi.x != lpvdiAggregate->x || vdi.y != lpvdiAggregate->y) lpci->dwDisplayFlags &= ~VDIF_COORDS;
	}

	/* Keep going, unless we already know we have nothing in common. */
	return (lpci->dwDisplayFlags != 0);
}



/* NextUnusedTag
 *   Finds the first tag value that is unused in a map.
 *
 * Parameters:
 *   MAP*	lpmap	Pointer to map data.
 *
 * Return value: unsigned short
 *   Value of tag.
 */
unsigned short NextUnusedTag(MAP *lpmap)
{
	/* Fastest way I can think of doing it is sorting all used tags and finding
	 * the first hole.
	 */

	int cn = lpmap->iLinedefs + lpmap->iSectors;		/* Count of tags. */
	unsigned short *lpunTags;
	int i;
	short unNextTag;

	/* Empty map? */
	if(cn == 0) return 0;

	/* Allocate memory. */
	lpunTags = ProcHeapAlloc(cn * sizeof(unsigned short));

	/* Loop through sectors and linedefs, adding their tags to the array. */
	for(i=0; i < lpmap->iSectors; i++)
		lpunTags[i] = lpmap->sectors[i].tag;

	for(; i < cn; i++)
		lpunTags[i] = lpmap->linedefs[i - lpmap->iSectors].tag;

	/* Sort the array. */
	qsort(lpunTags, cn, sizeof(unsigned short), QsortUShortComparison);
	
	/* Find the first hole in the list. Consecutive tags may be equal or differ
	 * by 1 without creating a hole. Stop if we get to the end with no hole.
	 */
	i = 0;
	while(i < cn - 1 && lpunTags[i+1] - lpunTags[i] <= 1) i++;

	/* Return the first integer that fits in the hole, or past the end if no
	 * hole.
	 */
	unNextTag = lpunTags[i] + 1;

	ProcHeapFree(lpunTags);

	return unNextTag;
}


/* GetAdjacentSectorHeightRange
 *   Finds the range of ceiling and floor heights of sectors adjacent to the
 *   selection.
 *
 * Parameters:
 *   MAP*	lpmap			Pointer to map data.
 *   short*	lpnMaxCeil etc.	Buffers to return range in.
 *
 * Return value: BOOL
 *   FALSE if there were no sectors adjacent to the selection, or if there was
 *   no selection; TRUE otherwise.
 */
BOOL GetAdjacentSectorHeightRange(MAP *lpmap, short *lpnMaxCeil, short *lpnMinCeil, short *lpnMaxFloor, short *lpnMinFloor)
{
	short nMinFloor =  32767, nMinCeil =  32767;
	short nMaxFloor = -32768, nMaxCeil = -32768;
	int i;
	BOOL bConsideredAnySectors = FALSE;

	/* All the adjacent sectors will contain at least one selected linedef. Loop
	 * through all linedefs and look at the relevant selected ones.
	 */
	for(i = 0; i < lpmap->iLinedefs; i++)
	{
		/* Linedef is selected? */
		if(lpmap->linedefs[i].selected)
		{
			/* If a sector bounded by this ld is unselected, it's adjacent. */
			if(lpmap->linedefs[i].s1 >= 0)
			{
				MAPSECTOR *lpsec = &lpmap->sectors[lpmap->sidedefs[lpmap->linedefs[i].s1].sector];

				if(!lpsec->selected)
				{
					/* Check whether we need to update the range for this sector. */
					if(lpsec->hfloor < nMinFloor) nMinFloor = lpsec->hfloor;
					if(lpsec->hfloor > nMaxFloor) nMaxFloor = lpsec->hfloor;
					if(lpsec->hceiling < nMinCeil) nMinCeil = lpsec->hceiling;
					if(lpsec->hceiling > nMaxCeil) nMaxCeil = lpsec->hceiling;

					bConsideredAnySectors = TRUE;
				}
			}

			if(lpmap->linedefs[i].s2 >= 0)
			{
				MAPSECTOR *lpsec = &lpmap->sectors[lpmap->sidedefs[lpmap->linedefs[i].s2].sector];

				if(!lpsec->selected)
				{
					/* Check whether we need to update the range for this sector. */
					if(lpsec->hfloor < nMinFloor) nMinFloor = lpsec->hfloor;
					if(lpsec->hfloor > nMaxFloor) nMaxFloor = lpsec->hfloor;
					if(lpsec->hceiling < nMinCeil) nMinCeil = lpsec->hceiling;
					if(lpsec->hceiling > nMaxCeil) nMaxCeil = lpsec->hceiling;

					bConsideredAnySectors = TRUE;
				}
			}
		}
	}

	*lpnMaxCeil = nMaxCeil;
	*lpnMinCeil = nMinCeil;
	*lpnMaxFloor = nMaxFloor;
	*lpnMinFloor = nMinFloor;

	return bConsideredAnySectors;
}


/* GetMapRect
 *   Obtains a bounding rectangle for a map in map co-ordinates.
 *
 * Parameters:
 *   MAP*	lpmap	Pointer to map data.
 *   RECT*	lprc	Rectangle structure to return into.
 *
 * Return value: None.
 */
void GetMapRect(MAP *lpmap, RECT *lprc)
{
	int i;

	/* Sanity check: empty map? */
	if(lpmap->iVertices == 0 && lpmap->iThings == 0)
	{
		/* Return zero rectangle. */
		lprc->bottom = lprc->left = lprc->right = lprc->top = 0;
		return;
	}


	/* Only need to consider vertices and things. */

	/* Set initial rectangle to be a point at the position of the first vertex,
	 * or the first thing if there are no vertices.
	 */
	if(lpmap->iVertices > 0)
	{
		lprc->left = lprc->right = lpmap->vertices[0].x;
		lprc->bottom = lprc->top = lpmap->vertices[0].y;
	}
	else
	{
		lprc->left = lprc->right = lpmap->things[0].x;
		lprc->bottom = lprc->top = lpmap->things[0].y;
	}

	/* Loop through all things and vertices, extending the rectangle as
	 * necessary.
	 */
	for(i = 0; i < lpmap->iVertices; i++)
	{
		if(lpmap->vertices[i].x < lprc->left) lprc->left = lpmap->vertices[i].x;
		if(lpmap->vertices[i].x > lprc->right) lprc->right = lpmap->vertices[i].x;
		if(lpmap->vertices[i].y < lprc->bottom) lprc->bottom = lpmap->vertices[i].y;
		if(lpmap->vertices[i].y > lprc->top) lprc->top = lpmap->vertices[i].y;
	}

	for(i = 0; i < lpmap->iThings; i++)
	{
		if(lpmap->things[i].x < lprc->left) lprc->left = lpmap->things[i].x;
		if(lpmap->things[i].x > lprc->right) lprc->right = lpmap->things[i].x;
		if(lpmap->things[i].y < lprc->bottom) lprc->bottom = lpmap->things[i].y;
		if(lpmap->things[i].y > lprc->top) lprc->top = lpmap->things[i].y;
	}
}


/* ResetMapView
 *   Adjusts the viewport to show the entire map.
 *
 * Parameters:
 *   MAP*		lpmap		Pointer to map data.
 *   MAPVIEW*	lpmapview	Pointer to viewport data.
 *
 * Return value: None.
 */
void ResetMapView(MAP *lpmap, MAPVIEW *lpmapview)
{
	RECT rcMap;

	GetMapRect(lpmap, &rcMap);

	/* Make sure the view is at least 499 wide. This also adjusts the height,
	 * since viewport ratio is maintained. Borders are added later.
	 */
	if(rcMap.right - rcMap.left < 500)
	{
		int dx = (500 - (rcMap.right - rcMap.left)) >> 1;
		rcMap.left -= dx;
		rcMap.right += dx;
	}

	/* Zoom to the rectangle with 10% borders. */
	ZoomToRect(lpmapview, &rcMap, 10);
}



/* ApplySectorPropertiesToSelection
 *   Applies the specified properties to multiple sectors.
 *
 * Parameters:
 *   MAP*				lpmap		Pointer to map data.
 *   SELECTION_LIST*	lpsellist	Selection specifying the sectors to modify.
 *   SECTORDISPLAYINFO*	lpsdi		Structure containing properties to apply.
 *   DWORD				dwFlags		Flags specifying which fields of sdi are
 *									valid.
 *
 * Return value: None.
 */
void ApplySectorPropertiesToSelection(MAP *lpmap, SELECTION_LIST *lpsellist, SECTORDISPLAYINFO *lpsdi, DWORD dwFlags)
{
	int i;

	for(i = 0; i < lpsellist->iDataCount; i++)
		ApplySectorProperties(lpmap, lpsellist->lpiIndices[i], lpsdi, dwFlags);
}


/* ApplySectorProperties
 *   Applies the specified properties to a sector.
 *
 * Parameters:
 *   MAP*				lpmap		Pointer to map data.
 *   int				iIndex		Index of sector to modify.
 *   SECTORDISPLAYINFO*	lpsdi		Structure containing properties to apply.
 *   DWORD				dwFlags		Flags specifying which fields of sdi are
 *									valid.
 *
 * Return value: None.
 */
void ApplySectorProperties(MAP *lpmap, int iIndex, SECTORDISPLAYINFO *lpsdi, DWORD dwFlags)
{
	MAPSECTOR *lpsec = &lpmap->sectors[iIndex];
	/* Check whether each field is valid, and apply it if so. */

	if(dwFlags & SDIF_EFFECT) lpsec->special = lpsdi->unEffect;
	if(dwFlags & SDIF_CEILING) lpsec->hceiling = lpsdi->nCeiling;
	if(dwFlags & SDIF_FLOOR) lpsec->hfloor = lpsdi->nFloor;
	if(dwFlags & SDIF_TAG) lpsec->tag = lpsdi->unTag;
	if(dwFlags & SDIF_BRIGHTNESS) lpsec->brightness = lpsdi->ucBrightness;
	if(dwFlags & SDIF_CEILINGTEX)
	{
#ifdef _UNICODE
			wsprintfA(lpsec->tceiling, "%.8ls", lpsdi->szCeiling);
#else
			CopyMemory(lpsec->tceiling, lpsdi->szCeiling, sizeof(lpsec->tceiling));
#endif
	}
	if(dwFlags & SDIF_FLOORTEX)
	{
#ifdef _UNICODE
			wsprintfA(lpsec->tfloor, "%.8ls", lpsdi->szFloor);
#else
			CopyMemory(lpsec->tfloor, lpsdi->szFloor, sizeof(lpsec->tfloor));
#endif
	}
}




/* AddVertex
 *   Adds a new vertex to a map.
 *
 * Parameters:
 *   MAP*	lpmap		Pointer to map data.
 *   short	x, y		Co-ordinates of new vertex.
 *
 * Return value: int
 *   Index of new vertex.
 *
 * Remarks:
 *   No snapping is done here. That's the caller's problem.
 */
int AddVertex(MAP *lpmap, short x, short y)
{
	MAPVERTEX *lpvertexNew;

	/* TODO: Realloc space if necessary? */
	if(lpmap->iVertices >= 65536) return -1;

	/* Get pointer to new vertex. */
	lpvertexNew = &lpmap->vertices[lpmap->iVertices];

	/* Set position of new vertex. */
	lpvertexNew->x = x;
	lpvertexNew->y = y;

	/* Initialise other fields. */
	lpvertexNew->selected = 0;

	return lpmap->iVertices++;
}




/* RemoveVertex
 *   Removes a vertex from a map.
 *
 * Parameters:
 *   MAP*			lpmap		Map.
 *   int			iIndex		Index of vertex to remove.
 *
 * Return value: BOOL
 *   TRUE on success; FALSE on error.
 *
 * Notes:
 *   Lines referring to the vertex are not removed.
 */
BOOL RemoveVertex(MAP *lpmap, int iIndex)
{
	int iLastVertex;

	/* Sanity check. */
	if(iIndex < 0 || iIndex >= lpmap->iVertices) return FALSE;

	/* Make sure we still have vertices left. */
	if(lpmap->iVertices > 1)
	{
		/* Calculate the last vertex index. */
		iLastVertex = lpmap->iVertices - 1;
  
		/* TODO: Remove from selection if appropriate. */
  
		/* Replace the vertex with the final one. */
		lpmap->vertices[iIndex] = lpmap->vertices[iLastVertex];
  
		/* Re-reference linedefs to the moved vertex. */
		Rereference_Vertices(lpmap->linedefs, lpmap->iLinedefs, iLastVertex, iIndex);
	}

	/* Decrease number of vertices. */
	lpmap->iVertices--;

	/* Success! */
	return TRUE;
}



/* AddLinedef
 *   Adds a new linedef to a map.
 *
 * Parameters:
 *   MAP*	lpmap						Pointer to map data.
 *   int	iVertexStart, iVertexEnd	Vertices for linedef.
 *
 * Return value: int
 *   Index of new linedef.
 */
int AddLinedef(MAP *lpmap, int iVertexStart, int iVertexEnd)
{
	MAPLINEDEF *lplinedefNew;

	/* TODO: Realloc space if necessary? */
	if(lpmap->iLinedefs >= 65536) return -1;

	/* Get pointer to new linedef. */
	lplinedefNew = &lpmap->linedefs[lpmap->iLinedefs];

	/* Zero all fields to begin with. */
	ZeroMemory(lplinedefNew, sizeof(MAPLINEDEF));

	/* Set vertices for new linedef. */
	lplinedefNew->v1 = iVertexStart;
	lplinedefNew->v2 = iVertexEnd;

	/* Initialise other fields. */
	lplinedefNew->s1 = lplinedefNew->s2 = -1;

	return lpmap->iLinedefs++;
}


/* AddSector
 *   Adds a new sector to a map.
 *
 * Parameters:
 *   MAP*	lpmap		Pointer to map data.
 *   int	iParent		Index of parent sector from which to inherit properties.
 *						May be set to -1 for no inheritance.
 *
 * Return value: int
 *   Index of new sector.
 */
int AddSector(MAP *lpmap, int iParent)
{
	MAPSECTOR *lpsecNew;

	/* TODO: Realloc space if necessary? */
	if(lpmap->iSectors >= 65536) return -1;

	/* Get pointer to new sector. */
	lpsecNew = &lpmap->sectors[lpmap->iSectors];

	/* Inherit properties from parent. */
	if(iParent >= 0)
	{
		*lpsecNew = lpmap->sectors[iParent];
	}
	else
	{
		/* TODO: No inheritance, so set defaults. */

		/* Zero all fields to begin with. */
		ZeroMemory(lpsecNew, sizeof(MAPSECTOR));
	}

	return lpmap->iSectors++;
}


/* AddSidedef
 *   Adds a new sidedef to a map.
 *
 * Parameters:
 *   MAP*	lpmap		Pointer to map data.
 *   int	iSector		Sector to reference.
 *
 * Return value: int
 *   Index of new sidedef.
 */
int AddSidedef(MAP *lpmap, int iSector)
{
	MAPSIDEDEF *lpsdNew;

	/* TODO: Realloc space if necessary? */
	if(lpmap->iSidedefs >= 65536) return -1;

	/* Get pointer to new sector. */
	lpsdNew = &lpmap->sidedefs[lpmap->iSidedefs];

	/* Zero all fields to begin with. */
	ZeroMemory(lpsdNew, sizeof(MAPSIDEDEF));
	
	/* Set some sensible defaults. TODO. */
	lpsdNew->lower[0] = '-';
	lpsdNew->middle[0] = '-';
	lpsdNew->upper[0] = '-';
	lpsdNew->sector = iSector;

	return lpmap->iSidedefs++;
}


/* BeginDrawOperation
 *   Intialises a draw-buffer structure.
 *
 * Parameters:
 *   DRAW_OPERATION*	lpdrawop	Pointer to draw-op structure.
 *
 * Return value: None.
 *
 * Remarks:
 *   Call EndDrawOperation to free memory when you're finished.
 */
void BeginDrawOperation(DRAW_OPERATION *lpdrawop)
{
	/* Allocate buffers for new vertices and lines. */
	lpdrawop->lpiNewLines = ProcHeapAlloc(DRAWOP_BUFFER_COUNT_INCREMENT * sizeof(int));
	lpdrawop->lpiNewVertices = ProcHeapAlloc(DRAWOP_BUFFER_COUNT_INCREMENT * sizeof(int));

	/* Store buffer sizes. */
	lpdrawop->ciLineBuffer = lpdrawop->ciVertexBuffer = DRAWOP_BUFFER_COUNT_INCREMENT;

	/* They're empty to start with. */
	lpdrawop->iNewLineCount = lpdrawop->iNewVertexCount = 0;
}



/* EndDrawOperation
 *   Frees memory used in a drawing operation and unmarks 'new' flags from new
 *   lines and vertices.
 *
 * Parameters:
 *   MAP*				lpmap		Pointer to map data.
 *   DRAW_OPERATION*	lpdrawop	Pointer to draw-op structure.
 *
 * Return value: None.
 */
void EndDrawOperation(MAP *lpmap, DRAW_OPERATION *lpdrawop)
{
	int i;

	/* Unmark the new vertices. */
	for(i=0; i < lpdrawop->iNewVertexCount; i++)
		lpmap->vertices[lpdrawop->lpiNewVertices[i]].editflags &= ~VEF_NEW;

	/* Unmark the new linedefs. */
	for(i=0; i < lpdrawop->iNewLineCount; i++)
		lpmap->linedefs[lpdrawop->lpiNewLines[i]].editflags &= ~LEF_NEW;

	ProcHeapFree(lpdrawop->lpiNewLines);
	ProcHeapFree(lpdrawop->lpiNewVertices);
}



/* DrawToNewVertex
 *   Draws another vertex and adds a line between it and the previous vertex.
 *
 * Parameters:
 *   MAP*				lpmap		Pointer to map structure.
 *   DRAW_OPERATION*	lpdrawop	Pointer to draw-op structure.
 *   short				x, y		Co-ordinates of new vertex.
 *
 * Return value: BOOL
 *   TRUE if drawing should continue; FALSE otherwise.
 *
 * Remarks:
 *   If a vertex already exists at (x, y), it is used instead. All the sneaky
 *   drawing stuff is/will be performed here: sector creation etc. If we return
 *   FALSE, the caller should call EndDrawOperation to clean up.
 */
BOOL DrawToNewVertex(MAP *lpmap, DRAW_OPERATION *lpdrawop, short x, short y)
{
	int iVertex = GetVertexFromPosition(lpmap, x, y);
	int iVertexPrev;
	int iLinedef;
	BOOL bExistingVertex;
	BOOL bKeepGoing = TRUE;

	/* Get index of previous vertex, or -1 if we're first. */
	iVertexPrev = (lpdrawop->iNewVertexCount > 0) ? lpdrawop->lpiNewVertices[lpdrawop->iNewVertexCount-1] : -1;

	/* No vertex at that position yet? */
	bExistingVertex = (iVertex >= 0);
	if(!bExistingVertex)
	{
		/* Create a new vertex there. */
		iVertex = AddVertex(lpmap, x, y);
	}

	/* If that wasn't the first vertex, add a linedef to it from the previous
	 * vertex.
	 */
	if(iVertexPrev >= 0)
	{
		/* Is there already a linedef between these vertices (in either direction)?
		 */
		iLinedef = FindLinedefBetweenVertices(lpmap, iVertexPrev, iVertex);
		if(iLinedef >= 0)
		{
			/* There was already a linedef there. If it's going the wrong way,
			 * flip it.
			 */
			if(lpmap->linedefs[iLinedef].v1 == iVertex)
				FlipLinedef(lpmap, iLinedef);
		}
		else
		{
			float x, y;
			int iParentSector;

			/* No previous linedef, so this might require a new sector. Must
			 * check this before adding the new linedef.
			 */
			BOOL bNewSector = bExistingVertex && VerticesReachable(lpmap, iVertex, iVertexPrev);

			/* Create a linedef from the previous vertex to the new one. */
			iLinedef = AddLinedef(lpmap, iVertexPrev, iVertex);

			/* This'll be done again later for the general case, but that's no
			 * loss, and we need it marked before that.
			 */
			lpmap->linedefs[iLinedef].editflags |= LEF_NEW;

			/* Find what sector we're within. */
			GetLineSideSpot(lpmap, iLinedef, LS_FRONT, 0, &x, &y);
			iParentSector = IntersectSectorFloat(x, y, lpmap->vertices, lpmap->linedefs, lpmap->sidedefs, lpmap->iLinedefs, TRUE);

			if(iParentSector >= 0)
			{
				/* Initially set all sidedefs to refer to parent sector. */

				/* Two new sidedefs. */
				int iSD1 = AddSidedef(lpmap, iParentSector);
				int iSD2 = AddSidedef(lpmap, iParentSector);

				/* They're within the parent sector. */
				lpmap->sidedefs[iSD1].sector = iParentSector;
				lpmap->sidedefs[iSD2].sector = iParentSector;

				/* Set them. */
				SetLinedefSidedef(lpmap, iLinedef, iSD1, LS_FRONT);
				SetLinedefSidedef(lpmap, iLinedef, iSD2, LS_BACK);
			}
			
			/* If there's no parent sector, we don't create any sidedefs. If the
			 * user closes the sector, front sds will be created then; if not,
			 * the line will just be left without sidedefs.
			 */

			if(bNewSector)
			{
				int iNewSector;
				LDLOOKUP *lpldlookup;

				/* Create a new sector. */
				iNewSector = AddSector(lpmap, iParentSector);

				/* Build vertex-to-ld lookup table. */
				lpldlookup = BuildLDLookupTable(lpmap, LDLT_VXTOLD);

				/* Set the sidedefs to point to the new sector. */
				PropagateNewSector(lpmap, lpldlookup, iLinedef, LS_FRONT, iNewSector);

				/* Clean up. */
				DestroyLDLookupTable(lpmap, lpldlookup);

				/* Knux: This [MessageBox call] is only temporaaaaa-i-aaary... */
				MessageBox(NULL, TEXT("Created a new sector."), g_szAppName, MB_ICONASTERISK);
			}
		}

		/* Add the linedef to the list of new ones. We do this even if it's there
		 * already. This also marks it new.
		 */
		AddLinedefToDrawList(lpmap, lpdrawop, iLinedef);

		/* Did we return to one of the vertices we've just drawn? If so, we stop
		 * drawing.
		 */
		if(lpmap->vertices[iVertex].editflags & VEF_NEW)
		{
			/* Tell the caller to stop drawing. */
			bKeepGoing = FALSE;
		}
	}

	/* Add the vertex to the list of new ones. We do this even if it's there
	 * already. This also marks it new.
	 */
	AddVertexToDrawList(lpmap, lpdrawop, iVertex);

	/* Indicate whether the drawing operation should continue. */
	return bKeepGoing;
}



/* AddVertexToDrawList, AddLinedefToDrawList
 *   Adds a vertex or linedef to the draw-list, and marks it as new.
 *
 * Parameters:
 *   MAP*				lpmap				Pointer to map structure.
 *   DRAW_OPERATION*	lpdrawop			Pointer to draw-op structure.
 *   int				iVertex/iLinedef	Index of vertex/linedef to add.
 *
 * Return value: None.
 *
 * Remarks:
 *   Largely a copy-paste job, I'm afraid.
 */
static void AddVertexToDrawList(MAP *lpmap, DRAW_OPERATION *lpdrawop, int iVertex)
{
	/* Not enough room in the buffer? */
	if(lpdrawop->ciVertexBuffer == (unsigned int)lpdrawop->iNewVertexCount)
	{
		/* Increase the size of the buffer. */
		lpdrawop->ciVertexBuffer += DRAWOP_BUFFER_COUNT_INCREMENT;
		lpdrawop->lpiNewVertices = ProcHeapReAlloc(lpdrawop->lpiNewVertices, lpdrawop->ciVertexBuffer * sizeof(int));
	}

	/* Add the vertex to the buffer and increment the count. */
	lpdrawop->lpiNewVertices[lpdrawop->iNewVertexCount++] = iVertex;

	/* Mark the vertex as new. */
	lpmap->vertices[iVertex].editflags |= VEF_NEW;
}

static void AddLinedefToDrawList(MAP *lpmap, DRAW_OPERATION *lpdrawop, int iLinedef)
{
	/* Not enough room in the buffer? */
	if(lpdrawop->ciLineBuffer == (unsigned int)lpdrawop->iNewLineCount)
	{
		/* Increase the size of the buffer. */
		lpdrawop->ciLineBuffer += DRAWOP_BUFFER_COUNT_INCREMENT;
		lpdrawop->lpiNewLines = ProcHeapReAlloc(lpdrawop->lpiNewLines, lpdrawop->ciLineBuffer * sizeof(int));
	}

	/* Add the linedef to the buffer and increment the count. */
	lpdrawop->lpiNewLines[lpdrawop->iNewLineCount++] = iLinedef;

	/* Mark the linedef as new. */
	lpmap->linedefs[iLinedef].editflags |= LEF_NEW;
}



/* FlipLinedef
 *   Flips a linedef in the intuitive way.
 *
 * Parameters:
 *   MAP*	lpmap		Pointer to map structure.
 *   int	iLinedef	Index of linedef to flip.
 *
 * Return value: None.
 */
void FlipLinedef(MAP *lpmap, int iLinedef)
{
	int iVertexIntermediate, iSidedefIntermediate;
	MAPLINEDEF *lplinedef = &lpmap->linedefs[iLinedef];

	/* Interchange vertices. */
	iVertexIntermediate = lplinedef->v1;
	lplinedef->v1 = lplinedef->v2;
	lplinedef->v2 = iVertexIntermediate;

	/* Interchange sidedefs. This means that sidedefs don't actually move, in
	 * the sense of their drawing positions.
	 */
	iSidedefIntermediate = lplinedef->s1;
	lplinedef->s1 = lplinedef->s2;
	lplinedef->s2 = iSidedefIntermediate;
}


/* GetVertexFromPosition
 *   Returns the index of the vertex at the specified co-ordinates.
 *
 * Parameters:
 *   MAP*	lpmap	Pointer to map structure.
 *   short	x, y	Co-ordinates of desired vertex.
 *
 * Return value: int
 *   Index of vertex if found; negative otherwise.
 */
int GetVertexFromPosition(MAP *lpmap, short x, short y)
{
	int iDist;
	int iVertex = NearestVertex(x, y, lpmap->vertices, lpmap->iVertices, &iDist);

	/* Did we find a vertex, and if so, is it exactly where we want it? */
	if(iVertex >= 0 && lpmap->vertices[iVertex].x == x && lpmap->vertices[iVertex].y == y)
		return iVertex;

	/* No vertex at specified position. */
	return -1;
}


/* FindLinedefBetweenVertices
 *   Searches for a linedef joining two vertices in either direction.
 *
 * Parameters:
 *   MAP*	lpmap				Pointer to map structure.
 *   int	iVertexA, iVertexB	Indices of vertices the linedef must join.
 *
 * Return value: int
 *   Index of linedef if found; negative otherwise.
 *
 * Remarks:
 *   See also FindLinedefBetweenVerticesDirected, which cares which way the line
 *   goes.
 */
int FindLinedefBetweenVertices(MAP *lpmap, int iVertexA, int iVertexB)
{
	int iLinedef;

	/* Look for a vertex from A to B first. Return index if found. */
	iLinedef = FindLinedefBetweenVerticesDirected(lpmap, iVertexA, iVertexB);
	if(iLinedef >= 0) return iLinedef;

	/* None from A to B; look from B to A. */
	return FindLinedefBetweenVerticesDirected(lpmap, iVertexB, iVertexA);
}


/* FindLinedefBetweenVerticesDirected
 *   Searches for a linedef beginning at one vertex and ending at another.
 *
 * Parameters:
 *   MAP*	lpmap				Pointer to map structure.
 *   int	iVertexA, iVertexB	Indices of vertices the linedef must join.
 *
 * Return value: int
 *   Index of linedef if found; negative otherwise.
 *
 * Remarks:
 *   See also FindLinedefBetweenVertices, which doesn't care which way the line
 *   goes.
 */
int FindLinedefBetweenVerticesDirected(MAP *lpmap, int iVertex1, int iVertex2)
{
	int i;

	for(i = 0; i < lpmap->iLinedefs; i++)
	{
		/* Does the linedef connect the correct vertices in the desired dirn? */
		if(lpmap->linedefs[i].v1 == iVertex1 && lpmap->linedefs[i].v2 == iVertex2)
			return i;
	}

	/* Not found. */
	return -1;
}



/* Snap
 *   Snaps a point to a rectangular grid.
 *
 * Parameters:
 *   short				*lpx, *lpy		Pointers to co-ordinates of point to
 *										snap.
 *   unsigned short		cxGrid, cyGrid	Grid dimensions.
 *
 * Return value: None.
 */
void Snap(short *lpx, short *lpy, unsigned short cxSnap, unsigned short cySnap)
{
	if(*lpx >= 0)
		*lpx = cxSnap * ((*lpx + (cxSnap >> 1)) / cxSnap);
	else
		*lpx = cxSnap * ((*lpx - (cxSnap >> 1)) / cxSnap);

	if(*lpy >= 0)
		*lpy = cySnap * ((*lpy + (cySnap >> 1)) / cySnap);
	else
		*lpy = cySnap * ((*lpy - (cySnap >> 1)) / cySnap);
}




/* VerticesReachable
 *   Determines whether a path exists between two vertices.
 *
 * Parameters:
 *   MAP*	lpmap				Pointer to map data.
 *   int	iVertex1, iVertex2	Indices of vertices.
 *
 * Return value: TRUE if path exists; FALSE if not.
 *
 * Remarks:
 *   Just creates some data structures and calls VerticesReachableByLookup,
 *   where the reall work is done.
 */
BOOL VerticesReachable(MAP *lpmap, int iVertex1, int iVertex2)
{
	/* We do a recursive depth-first search from iVertex1 for iVertex2. We need
	 * to build a lookup table of edges first, since it would be s-l-o-w
	 * otherwise. We also need to keep track of which vertices we've visited.
	 */
	
	LDLOOKUP *lpldlookup;
	BOOL *lpbVisited = ProcHeapAlloc(lpmap->iVertices * sizeof(BOOL));
	BOOL bReachable;

	/* We haven't visited any vertices to start with. */
	ZeroMemory(lpbVisited, lpmap->iVertices * sizeof(BOOL));

	/* Build a vx-to-vx lookup table. */
	lpldlookup = BuildLDLookupTable(lpmap, LDLT_VXTOVX);

	/* Now that we've got the lookup table, we can do the actual search. */
	bReachable = VerticesReachableByLookup(lpldlookup, lpbVisited, iVertex1, iVertex2);

	/* Search is finished. Clean up. */
	DestroyLDLookupTable(lpmap, lpldlookup);

	ProcHeapFree(lpbVisited);

	return bReachable;
}



/* VerticesReachable
 *   Determines whether a path exists between two vertices.
 *
 * Parameters:
 *   MAP*	lpmap				Pointer to map data.
 *   int	iVertex1, iVertex2	Indices of vertices.
 *
 * Return value: BOOL
 *   TRUE if path exists; FALSE if not.
 *
 * Remarks:
 *   Just creates some data structures and calls VerticesReachableByLookup,
 *   where the reall work is done.
 */
static BOOL __fastcall VerticesReachableByLookup(LDLOOKUP *lpldlookup, BOOL *lpbVisited, int iVertex1, int iVertex2)
{
	int i;

	/* Termination case: we're reachable from ourselves. */
	if(iVertex1 == iVertex2) return TRUE;

	/* We've visited ourselves. */
	lpbVisited[iVertex1] = TRUE;

	/* Is it reachable from any of our as-yet-unvisited neighbours? */
	for(i = 0; (unsigned int)i < lpldlookup[iVertex1].uiCount; i++)
		if(!lpbVisited[lpldlookup[iVertex1].lpiIndices[i]] && VerticesReachableByLookup(lpldlookup, lpbVisited, lpldlookup[iVertex1].lpiIndices[i], iVertex2))
			return TRUE;

	/* Not reachable from any of our neighbours, so it isn't reachable from
	 * here.
	 */
	return FALSE;
}


/* GetLineSideSpot
 *   Finds a point a given distance normally from the centre of a linedef.
 *
 * Parameters:
 *   MAP*	lpmap				Pointer to map data.
 *   int	iLinedef			Index of linedef.
 *   int	iSideOfLine			LS_FRONT or LS_BACK: from which side the
 *								distance is to be measured.
 *   float	fDistance			Distance.
 *   float*	lpx, lpy			Point is returned here.
 *
 * Return value: None.
 */
static void GetLineSideSpot(MAP *lpmap, int iLinedef, int iSideOfLine, float fDistance, float *lpx, float *lpy)
{
	POINT ptCentre, ptNormal;
	MAPLINEDEF *lpld = &lpmap->linedefs[iLinedef];
	MAPVERTEX *lpv1 = &lpmap->vertices[lpld->v1], *lpv2 = &lpmap->vertices[lpld->v2];
	float fNormalLength;

	/* Get the point of the centre of the line. */
	ptCentre.x = (lpv2->x + lpv1->x) / 2;
	ptCentre.y = (lpv2->y + lpv1->y) / 2;

	/* Find the normal. */
	ptNormal.x = lpv1->y - lpv2->y;
	ptNormal.y = lpv2->x - lpv1->x;
	if(iSideOfLine == LS_BACK)
	{
		ptNormal.x = -ptNormal.x;
		ptNormal.y = -ptNormal.y;
	}
	fNormalLength = (float)sqrt(ptNormal.x * ptNormal.x + ptNormal.y * ptNormal.y);

	/* Find the desired point. */
	*lpx = ptCentre.x + (ptNormal.x * fDistance) / fNormalLength;
	*lpy = ptCentre.y + (ptNormal.y * fDistance) / fNormalLength;
}



/* BuildLDLookupTable
 *   Builds a lookup table for adjacent linedefs or vertices given a vertex.
 *
 * Parameters:
 *   MAP*	lpmap			Pointer to map data.
 *   int	iLookupType		LDLT_VXTOVX or LDLT_VXTOLD
 *
 * Return value: LDLOOKUP*
 *   Pointer to new lookup table.
 *
 * Remarks:
 *   The caller should call DestroyLDLookupTable to free the allocated memory.
 */
static LDLOOKUP *BuildLDLookupTable(MAP *lpmap, int iLookupType)
{
	LDLOOKUP *lpldlookup = ProcHeapAlloc(lpmap->iVertices * sizeof(LDLOOKUP));
	int i;

	/* Allocate an initial buffer for each vertex, to contain the other vertices
	 * joined to each directly.
	 */
	for(i = 0; i < lpmap->iVertices; i++)
	{
		lpldlookup[i].uiCount = 0;
		lpldlookup[i].uiBufSize = LDLOOKUP_INITBUFSIZE;
		lpldlookup[i].lpiIndices = ProcHeapAlloc(LDLOOKUP_INITBUFSIZE * sizeof(int));
	}

	/* Now fill the table. */
	for(i = 0; i < lpmap->iLinedefs; i++)
	{
		int iVerticesToAdd[2];
		int iValues[2];
		int j;

		/* Are we doing vx-to-vx, or vx-to-ld? */
		switch(iLookupType)
		{
		case LDLT_VXTOVX:
			/* Add the entry. */
			iVerticesToAdd[0] = lpmap->linedefs[i].v1; iValues[0] = lpmap->linedefs[i].v2;
			iVerticesToAdd[1] = lpmap->linedefs[i].v2; iValues[1] = lpmap->linedefs[i].v1;
			break;

		case LDLT_VXTOLD:
			iVerticesToAdd[0] = lpmap->linedefs[i].v1; iValues[0] = i;
			iVerticesToAdd[1] = lpmap->linedefs[i].v2; iValues[1] = i;
			break;
		}

		for(j = 0; j < sizeof(iVerticesToAdd) / sizeof(int); j++)
		{
			/* If there's nothing to add, skip. */
			if(iVerticesToAdd[j] < 0) continue;

			/* Do we need to allocate more space? */
			if(lpldlookup[iVerticesToAdd[j]].uiCount == lpldlookup[iVerticesToAdd[j]].uiBufSize)
			{
				/* Double the buffer length. */
				lpldlookup[iVerticesToAdd[j]].uiBufSize <<= 1;

				/* Reallocate. */
				lpldlookup[iVerticesToAdd[j]].lpiIndices = ProcHeapReAlloc(lpldlookup[iVerticesToAdd[j]].lpiIndices, lpldlookup[iVerticesToAdd[j]].uiBufSize * sizeof(int));
			}

			lpldlookup[iVerticesToAdd[j]].lpiIndices[lpldlookup[iVerticesToAdd[j]].uiCount++] = iValues[j];
		}
	}

	return lpldlookup;
}


/* DestroyLDLookupTable
 *   Frees memory used by a lookup table.
 *
 * Parameters:
 *   MAP*		lpmap		Pointer to map used to generate table.
 *   LDLOOKUP*	lpldlookup	Pointer to table to free.
 *
 * Return value: None.
 *
 * Remarks:
 *   No vertices may be added to or removed from the map between creating an
 *   destroying the table.
 */
static void DestroyLDLookupTable(MAP *lpmap, LDLOOKUP *lpldlookup)
{
	int i;

	for(i = 0; i < lpmap->iVertices; i++)
		ProcHeapFree(lpldlookup[i].lpiIndices);

	ProcHeapFree(lpldlookup);
}



/* PropagateNewSector
 *   Moves around the most convex polygon from a given linedef, setting sidedefs
 *   to refer to a particular sector.
 *
 * Parameters:
 *   MAP*		lpmap		Pointer to map data.
 *   LDLOOKUP*	lpldlookup	Pointer to table linking vertices to linedefs.
 *   int		iLinedef	Linedef to start from.
 *   int		iLindefSide	LS_FRONT or LS_BACK, as appropriate.
 *   int		iNewSector	Index of sector to propagate to sidedefs.
 *
 * Return value: None.
 *
 * Remarks:
 *   Behaviour is undefined in all but the case where a cycle has just been
 *   created by the addition of a new linedef.
 */
static void __fastcall PropagateNewSector(MAP *lpmap, LDLOOKUP *lpldlookup, int iLinedef, int iLinedefSide, int iNewSector)
{
	MAPLINEDEF *lpld = &lpmap->linedefs[iLinedef];
	int iSidedef = (iLinedefSide == LS_FRONT ? lpld->s1 : lpld->s2);
	MAPSIDEDEF *lpsd = (iSidedef >= 0 ? &lpmap->sidedefs[iSidedef] : NULL);
	int iNextLinedef;

	/* Termination case: if this side already refers to the new sector, we're
	 * finished.
	 */
	if(lpsd && lpsd->sector == iNewSector) return;

	/* No sidedef yet? Then make one, and add it to the line. */
	if(!lpsd)
	{
		iSidedef = AddSidedef(lpmap, iNewSector);
		lpsd = &lpmap->sidedefs[iSidedef];
		SetLinedefSidedef(lpmap, iLinedef, iSidedef, iLinedefSide);
	}
	
	/* Set the sidedef to reference the new sector. */
	lpsd->sector = iNewSector;

	/* Find the linedef adjacent to ourselves that makes the smallest angle with
	 * the appropriate side.
	 */
	iNextLinedef = FindNearestAdjacentLinedef(lpmap, lpldlookup, iLinedef, iLinedefSide, (iLinedefSide == LS_FRONT) ? 1 : 0);

	/* Is it orientated the same way we are? */
	if(lpld->v1 == lpmap->linedefs[iNextLinedef].v2 || lpld->v2 == lpmap->linedefs[iNextLinedef].v1)
		/* Recurse along the same side. */
		PropagateNewSector(lpmap, lpldlookup, iNextLinedef, iLinedefSide, iNewSector);
	else
		/* Recurse along the other side. */
		PropagateNewSector(lpmap, lpldlookup, iNextLinedef, (iLinedefSide == LS_FRONT) ? LS_BACK : LS_FRONT, iNewSector);
}



/* FindNearestAdjacentLinedef
 *   Finds the linedef forming the smallest angle with a linedef from a
 *   specified side.
 *
 * Parameters:
 *   MAP*		lpmap			Pointer to map data.
 *   LDLOOKUP*	lpldlookup		Pointer to table linking vertices to linedefs.
 *   int		iLinedef		Linedef to start from.
 *   int		iLindefSide		LS_FRONT or LS_BACK, as appropriate.
 *   int		iWhichVertex	0 for v1, 1 for v2.
 *
 * Return value: int
 *   Index of nearest linedef.
 */
static int FindNearestAdjacentLinedef(MAP *lpmap, LDLOOKUP *lpldlookup, int iLinedef, int iLindefSide, int iWhichVertex)
{
	MAPLINEDEF *lpld = &lpmap->linedefs[iLinedef];
	int iVertex = iWhichVertex == 0 ? lpld->v1 : lpld->v2;
	int i, iNearestLinedef;
	double dNearestAngle;

	/* Set something outwith the bounds. */
	dNearestAngle = 3 * acos(-1);

	/* Enumerate all the linedefs connected to the specified vertex. */
	for(i = 0; (unsigned int)i < lpldlookup[iVertex].uiCount; i++)
	{
		double dAng;

		/* Skip ourselves. */
		if(lpldlookup[iVertex].lpiIndices[i] == iLinedef) continue;

		dAng = AdjacentLinedefAngleFromFront(lpmap, iLinedef, lpldlookup[iVertex].lpiIndices[i]);

		if(dAng < dNearestAngle)
		{
			dNearestAngle = dAng;
			iNearestLinedef = lpldlookup[iVertex].lpiIndices[i];
		}
	}

	return iNearestLinedef;
}



/* AdjacentLinedefAngleFromFront
 *   Finds the angle from the front of one linedef to another ld adjacent to it.
 *
 * Parameters:
 *   MAP*	lpmap		Pointer to map data.
 *   int	iLinedef1	Index of ld to measure from.
 *   int	iLinedef2	Index of ld to measure to.
 *
 * Return value: double
 *   Angle between them in radians, in [0, 2pi).
 */
static double AdjacentLinedefAngleFromFront(MAP *lpmap, int iLinedef1, int iLinedef2)
{
	MAPLINEDEF *lpld1 = &lpmap->linedefs[iLinedef1];
	MAPLINEDEF *lpld2 = &lpmap->linedefs[iLinedef2];
	double dAng1, dAng2;
	double dAng;

	/* Find which vertices they have in common. */
	if(lpld1->v1 == lpld2->v1)
	{
		dAng1 = LinedefAngleV1(lpmap, iLinedef1);
		dAng2 = LinedefAngleV1(lpmap, iLinedef2);
	}
	else if(lpld1->v1 == lpld2->v2)
	{
		dAng1 = LinedefAngleV1(lpmap, iLinedef1);
		dAng2 = LinedefAngleV2(lpmap, iLinedef2);
	}
	else if(lpld1->v2 == lpld2->v1)
	{
		dAng1 = LinedefAngleV2(lpmap, iLinedef1);
		dAng2 = LinedefAngleV1(lpmap, iLinedef2);
	}
	else if(lpld1->v2 == lpld2->v2)
	{
		dAng1 = LinedefAngleV2(lpmap, iLinedef1);
		dAng2 = LinedefAngleV2(lpmap, iLinedef2);
	}

	dAng = dAng2 - dAng1;
	while(dAng < 0) dAng += 2 * PI;
	while(dAng >= 2 * PI) dAng -= 2 * PI;

	return dAng;
}

static double LinedefAngleV1(MAP *lpmap, int iLinedef)
{
	return atan2(lpmap->vertices[lpmap->linedefs[iLinedef].v2].y - lpmap->vertices[lpmap->linedefs[iLinedef].v1].y, lpmap->vertices[lpmap->linedefs[iLinedef].v2].x - lpmap->vertices[lpmap->linedefs[iLinedef].v1].x);
}

static double LinedefAngleV2(MAP *lpmap, int iLinedef)
{
	return atan2(lpmap->vertices[lpmap->linedefs[iLinedef].v1].y - lpmap->vertices[lpmap->linedefs[iLinedef].v2].y, lpmap->vertices[lpmap->linedefs[iLinedef].v1].x - lpmap->vertices[lpmap->linedefs[iLinedef].v2].x);
}


/* SetLinedefSidedef
 *   Sets a one of a linedef's sidedefs, and sets flags accordingly.
 *
 * Parameters:
 *   MAP*	lpmap			Pointer to map data.
 *   int	iLinedef		Index of linedef whose sidedef is to be changed.
 *   int	iSidedef		Index of sidedef.
 *   int	iLinedefSide	LS_FRONT or LS_BACK.
 *
 * Return value: None.
 */
static void SetLinedefSidedef(MAP *lpmap, int iLinedef, int iSidedef, int iLinedefSide)
{
	MAPLINEDEF *lpld = &lpmap->linedefs[iLinedef];

	if(iLinedefSide == LS_FRONT)
		lpld->s1 = iSidedef;
	else
	{
		lpld->s2 = iSidedef;

		/* If this was negative, we're no longer double-sided; otherwise, we
		 * are.
		 */
		if(iSidedef < 0)
		{
			lpld->flags &= ~LDF_TWOSIDED;
			lpld->flags |= LDF_IMPASSABLE;
		}
		else
		{
			lpld->flags |= LDF_TWOSIDED;
			lpld->flags &= ~LDF_IMPASSABLE;
		}
	}
}
