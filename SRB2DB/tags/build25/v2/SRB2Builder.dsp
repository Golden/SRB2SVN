# Microsoft Developer Studio Project File - Name="SRB2Builder" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 6.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Console Application" 0x0103

CFG=SRB2Builder - Win32 Debug
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "SRB2Builder.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "SRB2Builder.mak" CFG="SRB2Builder - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "SRB2Builder - Win32 Release" (based on "Win32 (x86) Console Application")
!MESSAGE "SRB2Builder - Win32 Debug" (based on "Win32 (x86) Console Application")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 0
# PROP Scc_ProjName ""
# PROP Scc_LocalPath ""
CPP=cl.exe
RSC=rc.exe

!IF  "$(CFG)" == "SRB2Builder - Win32 Release"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "Release"
# PROP Intermediate_Dir "Release"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_CONSOLE" /D "_MBCS" /YX /FD /c
# ADD CPP /nologo /MT /W3 /GX /O2 /D "NDEBUG" /D "WIN32" /D "_WINDOWS" /D "_MBCS" /D _WIN32_WINDOWS=0x4100 /D _WIN32_IE=0x0600 /YX /FD /c
# ADD BASE RSC /l 0x809 /d "NDEBUG"
# ADD RSC /l 0x809 /d "NDEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:console /machine:I386
# ADD LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib comctl32.lib OpenGL32.lib GLu32.lib GLaux.lib wad/Release/Wad.lib DockWnd/Release/DockWnd.lib /nologo /subsystem:windows /machine:I386
# SUBTRACT LINK32 /pdb:none

!ELSEIF  "$(CFG)" == "SRB2Builder - Win32 Debug"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Debug"
# PROP BASE Intermediate_Dir "Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "Debug"
# PROP Intermediate_Dir "Debug"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_CONSOLE" /D "_MBCS" /YX /FD /GZ /c
# ADD CPP /nologo /MTd /W3 /WX /Gm /GX /ZI /Od /D "_DEBUG" /D "WIN32" /D "_WINDOWS" /D "_MBCS" /D _WIN32_WINDOWS=0x0501 /D _WIN32_WINNT=0x0501 /D _WIN32_IE=0x0600 /YX /FD /GZ /c
# ADD BASE RSC /l 0x809 /d "_DEBUG"
# ADD RSC /l 0x809 /d "_DEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:console /debug /machine:I386 /pdbtype:sept
# ADD LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib comctl32.lib OpenGL32.lib GLu32.lib GLaux.lib wad/Debug/Wad.lib DockWnd/Debug/DockWnd.lib /nologo /subsystem:windows /profile /debug /machine:I386

!ENDIF 

# Begin Target

# Name "SRB2Builder - Win32 Release"
# Name "SRB2Builder - Win32 Debug"
# Begin Group "Modules"

# PROP Default_Filter "cpp;c;cxx;rc;def;r;odl;idl;hpj;bat"
# Begin Group "Module Headers"

# PROP Default_Filter "h"
# Begin Source File

SOURCE=.\src\cdlgwrapper.h
# End Source File
# Begin Source File

SOURCE=.\src\config.h
# End Source File
# Begin Source File

SOURCE=.\src\editing.h
# End Source File
# Begin Source File

SOURCE=.\src\general.h
# End Source File
# Begin Source File

SOURCE=.\src\keyboard.h
# End Source File
# Begin Source File

SOURCE=.\src\like.h
# End Source File
# Begin Source File

SOURCE=.\src\map.h
# End Source File
# Begin Source File

SOURCE=.\src\mapconfig.h
# End Source File
# Begin Source File

SOURCE=.\src\maptypes.h
# End Source File
# Begin Source File

SOURCE=.\src\openwads.h
# End Source File
# Begin Source File

SOURCE=.\src\options.h
# End Source File
# Begin Source File

SOURCE=.\src\renderer.h
# End Source File
# Begin Source File

SOURCE=.\src\resource.h
# End Source File
# Begin Source File

SOURCE=.\src\selection.h
# End Source File
# Begin Source File

SOURCE=.\src\texture.h
# End Source File
# End Group
# Begin Source File

SOURCE=.\src\cdlgwrapper.c
# End Source File
# Begin Source File

SOURCE=.\src\config.c
# End Source File
# Begin Source File

SOURCE=.\src\editing.c
# End Source File
# Begin Source File

SOURCE=.\src\entry.c
# End Source File
# Begin Source File

SOURCE=.\src\general.c
# End Source File
# Begin Source File

SOURCE=.\src\keyboard.c
# End Source File
# Begin Source File

SOURCE=.\src\like.c
# End Source File
# Begin Source File

SOURCE=.\src\map.c
# End Source File
# Begin Source File

SOURCE=.\src\mapconfig.c
# End Source File
# Begin Source File

SOURCE=.\src\openwads.c
# End Source File
# Begin Source File

SOURCE=.\src\options.c
# End Source File
# Begin Source File

SOURCE=.\src\renderer.c
# End Source File
# Begin Source File

SOURCE=.\src\selection.c
# End Source File
# Begin Source File

SOURCE=.\src\texture.c
# End Source File
# End Group
# Begin Group "Resource Files"

# PROP Default_Filter "ico;cur;bmp;dlg;rc2;rct;bin;rgs;gif;jpg;jpeg;jpe"
# Begin Source File

SOURCE=.\res\Grab1.cur
# End Source File
# Begin Source File

SOURCE=.\src\Grab1.cur
# End Source File
# Begin Source File

SOURCE=.\res\Grab2.cur
# End Source File
# Begin Source File

SOURCE=.\src\Grab2.cur
# End Source File
# Begin Source File

SOURCE=.\res\NoTexture.bmp
# End Source File
# Begin Source File

SOURCE=.\res\SRB2Builder.exe.manifest
# End Source File
# Begin Source File

SOURCE=.\src\SRB2Builder.rc
# End Source File
# Begin Source File

SOURCE=.\res\ThingCircle.bmp
# End Source File
# Begin Source File

SOURCE=.\res\UnknownFlat.bmp
# End Source File
# Begin Source File

SOURCE=.\res\UnknownImage.bmp
# End Source File
# End Group
# Begin Group "Windows"

# PROP Default_Filter ""
# Begin Source File

SOURCE=.\src\win\editdlg.c
# End Source File
# Begin Source File

SOURCE=.\src\win\editdlg.h
# End Source File
# Begin Source File

SOURCE=.\src\win\gendlg.c
# End Source File
# Begin Source File

SOURCE=.\src\win\gendlg.h
# End Source File
# Begin Source File

SOURCE=.\src\win\infobar.c
# End Source File
# Begin Source File

SOURCE=.\src\win\infobar.h
# End Source File
# Begin Source File

SOURCE=.\src\win\mapselect.c
# End Source File
# Begin Source File

SOURCE=.\src\win\mapselect.h
# End Source File
# Begin Source File

SOURCE=.\src\win\mapwin.c
# End Source File
# Begin Source File

SOURCE=.\src\win\mapwin.h
# End Source File
# Begin Source File

SOURCE=.\src\win\mdiframe.c
# End Source File
# Begin Source File

SOURCE=.\src\win\mdiframe.h
# End Source File
# Begin Source File

SOURCE=.\src\win\texbrowser.c
# End Source File
# Begin Source File

SOURCE=.\src\win\texbrowser.h
# End Source File
# Begin Source File

SOURCE=.\src\win\wadlist.c
# End Source File
# Begin Source File

SOURCE=.\src\win\wadlist.h
# End Source File
# End Group
# Begin Group "CodeImp"

# PROP Default_Filter ""
# Begin Source File

SOURCE=.\src\CodeImp\ci_const.h
# End Source File
# Begin Source File

SOURCE=.\src\CodeImp\ci_data.c
# End Source File
# Begin Source File

SOURCE=.\src\CodeImp\ci_data_proto.h
# End Source File
# Begin Source File

SOURCE=.\src\CodeImp\ci_map.cpp
# End Source File
# Begin Source File

SOURCE=.\src\CodeImp\ci_map.h
# End Source File
# Begin Source File

SOURCE=.\src\CodeImp\ci_math.h
# End Source File
# Begin Source File

SOURCE=.\src\CodeImp\ci_poly.h
# End Source File
# Begin Source File

SOURCE=.\src\CodeImp\ci_renderer.cpp
# End Source File
# Begin Source File

SOURCE=.\src\CodeImp\ci_renderer.h
# End Source File
# Begin Source File

SOURCE=.\src\CodeImp\ci_renderer_proto.h
# End Source File
# End Group
# End Target
# End Project
