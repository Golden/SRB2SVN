/******************************************************************************\
 
 SRB2 Builder
 Configuration file for SRB2 version 1.09.4.
 
 Contributors:
  - Kristos
  - Shadow Hog
  - ST218
  - Foxboy
  - JJames19119
  - SRB2-Playah
  - Oogaland
  
\******************************************************************************/

// This is required to prevent accedential use of a different configuration
type = "SRB2 Builder Game Configuration";

// This is the title to show for this game
game = "Sonic Robo Blast 2 v1.09.4";

// Thing number for start position in 3D Mode
start3dmode = 32000;

// Thing number for control sector position hint.
ctrlsechint = 32001;

// Zoom tube waypoint, for the wizard.
zoomtubewaypoint = 18;

// Load textures/flats by default from this file
texturesfile = "";

// Default lump name for new map
defaultlumpname = "MAP01";

// Default flags for first new thing
defaulthingflags = 7;


/*
TEXTURES AND FLAT SOURCES-------------------------------------------------------
This tells Doom Builder where to find the information for textures
and flats in the IWAD file, Addition WAD file and Map WAD file.

Start and end lumps must be given in a structure (of which the
key name doesnt matter) and any textures or flats in between them
are loaded in either the textures category or flats category.

For textures: PNAMES, TEXTURE1 and TEXTURE2 are loaded by default.
*/

// Texture sources
textures
{
}

// Flat sources
flats
{
	standard1
	{
		start = "F_START";
		end = "F_END";
	}
	
	standard2
	{
		start = "FF_START";
		end = "FF_END";
	}
	
	standard3
	{
		start = "FF_START";
		end = "F_END";
	}
	
	standard4
	{
		start = "F_START";
		end = "FF_END";
	}
}

/*
TEXTURES AND FLATS FILTERING----------------------------------------------------
This allows you to filter textures and flats so that you only see the
textures/flats listed that you prefer to use.

The key name doesnt matter here, only the values. You can use
the following wildcards in values to specify ranges:

? = Any character
* = Zero or more characters
# = Any numeric digit
[abc...] = Any of these characters that are between brackets
[!abc..] = Not any of these characters that are between brackets
*/

// List these textures...
texturesfilter
{
	all_textures = "*";
}

// But do not list these textures...
notexturesfilter
{
	none = "";
}

// List these flats...
flatsfilter
{
	all_flats = "*";
}

// But do not list these flats...
noflatsfilter
{
	none = "";
}

/*
PSEUDO-FLATS AND TEXTURES-------------------------------------------------------
Names of flats and textures that don't really exist, but shouldn't be identified
as invalid. Pattern-matching is performed.
*/
pseudoflats
{
	F_SKY1;
}

pseudotextures
{
	[#]*;
}

/*
GAME DETECT PATTERN-------------------------------------------------------------
Used to guess the game for which a WAD file is made.

1 = One of these lumps must exist
2 = None of these lumps must exist
3 = All of these lumps must exist

Pattern-matching is now performed on lump names. See above for syntax.
*/
gamedetect
{
	EXTENDED = 2;
	BEHAVIOR = 2;
	E#M# = 2;
	MAP?? = 1;
}

/*
MAP LUMP NAMES------------------------------------------------------------------
Map lumps are loaded with the map as long as they are right after each other.
When the editor meets a lump which is not defined in this list (or defined as 0)
it will stop loading right there.

The order of items defines the order in which lumps will be written to WAD file
on save. The value (flags) of items determines what the editor should do with
it. You should never mess with value 4, because it may result in incorrect map
builds.

1 = Lump required
2 = Lump which must be respected
4 = Lump generated by node builder
8 = Lump allowed to be empty after nodebuilding
16 = Lump allowed to be missing after nodebuilding
4096 = Lump which can be edited as text
8192 = Lump which can be edited as DEHACKED
12288 = Lump which can be edited as MAPINFO
16384 = WAD-global.
*/

maplumpnames
{
	THINGS = 13;
	LINEDEFS = 5;
	SIDEDEFS = 5;
	VERTEXES = 5;
	SEGS = 4;
	SSECTORS = 4;
	NODES = 4;
	SECTORS = 5;
	REJECT = 4;
	BLOCKMAP = 20;
	MAINCFG = 24576;
}


// DEFAULT SECTOR BRIGHTNESS LEVELS---------------------------------------------
sectorbrightness
{
	255;
	240;
	224;
	208;
	192;
	176;
	160;
	144;
	128;
	112;
	96;
	80;
	64;
	48;
	32;
	16;
	0;
}

// SECTOR TYPES-----------------------------------------------------------------
sectortypes
{
	0 = "Normal";
	1 = "Light Blink Randomly";
	2 = "Light Blink Every Half Second";
	3 = "Light Blink Every Second";
	4 = "Spikes";
	5 = "Death Pit (No Camera Mod)";
	6 = "Space Countdown";
	7 = "Damage (Fire)";
	8 = "Light Pulse Smoothly";
	9 = "Special Stage Damage";
	10 = "Instant Kill";
	11 = "Damage (Plain)";
	12 = "Light Blink Every Half Second Synch";
	13 = "Light Blink Every Second Synch";
	14 = "Bouncy FOF";
	16 = "Death Pit (Camera Modification)";
	17 = "Light Flicker";
	18 = "Damage (Electric)";
	33 = "Special Stage Goal";
	256 = "Ice/Sludge";
	512 = "Wind/Current";
	519 = "Damage (Fire) and Current";
	666 = "Egg Trap Capsule";
	690 = "Button for Door 700";
	691 = "Button for Door 702";
	692 = "Button for Door 704";
	693 = "Button for Door 706";
	694 = "Button for Door 708";
	695 = "Button for Door 710";
	696 = "Button for Door 712";
	697 = "Button for Door 714";
	698 = "Button for Door 716";
	699 = "Button for Door 718";
	700 = "Button for Door 720";
	701 = "Button for Door 722";
	702 = "Button for Door 724";
	703 = "Button for Door 726";
	704 = "Button for Door 728";
	705 = "Button for Door 730";
	706 = "Button for Door 732";
	707 = "Button for Door 734";
	708 = "Button for Door 736";
	709 = "Button for Door 738";
	710 = "Button 21 (THZ2)";
	711 = "Button for Door 743 (Fast)";
	768 = "Ice/Sludge and Wind/Current";
	967 = "Trigger Linedef Executor (Emerald Check)";
	968 = "Trigger Linedef Executor (NiGHTS mare)";
	969 = "Super Sonic Transform";
	970 = "Check for pushable specials on FOFs";
	971 = "Trigger Linedef Executor (Pushable Objects)";
	972 = "Trigger Linedef Executor (Anywhere in Sector, All Players)";
	973 = "Trigger Linedef Executor (Floor touch, All players)";
	974 = "Trigger Linedef Executor (Anywhere in Sector)";
	975 = "Trigger Linedef Executor (Touch Sector Floor)";
	976 = "Speed Pad (No Spin)";
	977 = "Speed Pad (Spin)";
	978 = "Ring Drainer (Floor touch)";
	979 = "Spinner";
	980 = "Ring Drainer (No Floor Touch)";
	981 = "Raise Ceiling to Highest";
	982 = "Exit Sector";
	983 = "Damage (Water)";
	984 = "Damage (Water) with Current";
	985 = "Conveyor Belt";
	986 = "THZ2 Slime Raise";
	987 = "No Tag Zone";
	988 = "CTF Red Team Base";
	989 = "CTF Blue Team Base";
	990 = "Special Stage Time";
	991 = "Custom Gravity";
	992 = "Ramp Sector";
	993 = "Starpost Activator";
	994 = "Circuit: Finish Line";
	995 = "Return Dropped Flag";
	996 = "Non-Ramp Sector";
	997 = "Fan Sector";
	998 = "Zoom Tube Start";
	999 = "Zoom Tube End";
	1500 ="Bustable Block Sprite parameter(ROIA)";
	1501 ="Bustable Block Sprite parameter(ROIB)";
	1502 ="Bustable Block Sprite parameter(ROIC)";
	1503 ="Bustable Block Sprite parameter(ROID)";
	1504 ="Bustable Block Sprite parameter(ROIE)";
	1505 ="Bustable Block Sprite parameter(ROIF)";
	1506 ="Bustable Block Sprite parameter(ROIG)";
	1507 ="Bustable Block Sprite parameter(ROIH)";
	1508 ="Bustable Block Sprite parameter(ROII)";
	1509 ="Bustable Block Sprite parameter(ROIJ)";
	1510 ="Bustable Block Sprite parameter(ROIK)";
	1511 ="Bustable Block Sprite parameter(ROIL)";
	1512 ="Bustable Block Sprite parameter(ROIM)";
	1513 ="Bustable Block Sprite parameter(ROIN)";
	1514 ="Bustable Block Sprite parameter(ROIO)";
	1515 ="Bustable Block Sprite parameter(ROIP)";
}

// LINEDEF FLAGS----------------------------------------------------------------
linedefflags
{
	1 = "Impassable";
	2 = "Block Monster";
	4 = "Double Sided";
	8 = "Upper Unpegged";
	16 = "Lower Unpegged";
	32 = "No Easy";
	64 = "No Climb";
	128 = "No Normal";
	256 = "No Hard";
	512 = "Passuse";
	2048 = "No Sonic "; 
	4096 = "No Tails"; 
	8192 = "No Knuckles";
	16384 = "Bouncy Wall";
}

// LINEDEF ACTIVATIONS
linedefactivations
{
}

// FOF FLAGS
fofs
{
	25 = 207;
	33 = 239;
	34 = 131279;
	35 = 786639;
	36 = 524495;
	37 = 655567;
	41 = 2097359;
	42 = 917711;
	44 = 3247;
	45 = 20381;
	48 = 18333;
	51 = 203;
	52 = 3469;
	55 = 4194319;
	57 = 35;
	58 = 33;
	59 = 16794319;
	62 = 18317;
	67 = 16421;
	68 = 231;
	74 = 3993;
	75 = 1945;
	76 = 37748751;
	77 = 16779979;
	78 = 71303183;
	79 = 17318607;
	80 = 17580751;
	81 = 16796367;
	82 = 17320655;
	83 = 17582799;
	84 = 71305231;
	86 = 37750799;
}

// LINEDEF TYPES----------------------------------------------------------------
linedeftypes
{
	0 = "Normal";
	1 = "FOF: Crumbling (No respawn), Floating, Bobbing";
	2 = "Continuous Mover: Floor/Ceiling";
	3 = "Continuous Mover: Floor";
	4 = "Continuous Mover: Ceiling";
	5 = "Lighting: Set Ceiling";
	6 = "Continuous Mover: 2-Speed Floor/Ceiling";
	7 = "Continuous Mover: 2-Speed Floor";
	8 = "Continuous Mover: 2-Speed Ceiling";
	9 = "Linedef Executor: Trigger Once (Race only)";
	10 = "Linedef Executor: Trigger Repeatedly (CTF - Red Team)";
	11 = "Linedef Executor: Trigger Each Time (CTF - Red Team)";
	12 = "Linedef Executor: Trigger Repeatedly (CTF - Blue Team)";
	13 = "Linedef Executor: Trigger Each Time (CTF - Blue Team)";
	14 = "Deprecated: Old Water";
	15 = "Linedef Executor: Trigger Once (No More Enemies)";
	16 = "Lighting: Colormap";
	18 = "Zoom Tube Parameters";
	19 = "Linedef Executor: Trigger Continuous (Character Ability)";
	20 = "Linedef Executor: Trigger Each Time (Character Ability)";
	21 = "Linedef Executor: Trigger Once (Character Ability)";
	24 = "Deprecated: Instant Ceiling Raise";
	25 = "FOF: Shadowcasting (CTL sector lighting used below)";
	26 = "Deprecated: Instant Floor Drop";
	33 = "FOF: No Shadow (CTL sector lighting used above)";
	34 = "FOF: Floating, Bobbing";
	35 = "FOF: Falls";
	36 = "FOF: Falls, Reappears 15 Sec.";
	37 = "FOF: Falls, Reappears 15 Sec. and Floats";
	38 = "FOF: Bobbing";
	39 = "FOF: Falls, Floats, Bobs";
	40 = "FOF: Falls, Bobs";
	41 = "FOF: Mario [?] Block";
	42 = "FOF: Falls, Floats";
	43 = "Crusher: Crusher 1";
	44 = "FOF: Translucent";
	45 = "Water: Translucent Block";
	46 = "Lighting: Fog Block";
	47 = "Lighting: Light Block (From ceiling to bottom of level)";
	48 = "Water: Opaque Block";
	49 = "Lighting: Light Block (From ceiling to floor)";
	50 = "Crusher: Crusher 2";
	51 = "FOF: No Sides";
	52 = "FOF: Translucent, Intangible";
	53 = "FOF: Laser Block";
	54 = "FOF: Thwomp Block";
	55 = "FOF: Bustable";
	56 = "FOF: QuickSand";
	57 = "FOF: Invisible";
	58 = "FOF: Invisible, Intangible (3D Area)";
	59 = "FOF: Platform (Intangible From Below)";
	60 = "Lighting: Pulsating";
	61 = "Lighting: Flickering";
	62 = "FOF: Intangible, Opaque";
	63 = "Camera Scanner";
	64 = "Per-Sector Gravity";
	65 = "Movement/Scrolling: Speed Pad";
	66 = "Flat Alignment";
	67 = "FOF: Intangible, Sides Only";
	68 = "FOF: Adjustable Bobbing (Air)";
	69 = "FOF: Solid, Sides Only";
	71 = "Custom Exit";
	72 = "FOF: Reverse Adjustable Bobbing (Air)";
	73 = "Disable Linedef (Reserved for Future Use)";
	74 = "Water: Translucent Block, No Sides";
	75 = "Water: Opaque Block, No Sides";
	76 = "FOF: Shatter Block";
	77 = "FOF: Platform, Translucent, No Sides";
	78 = "FOF: Spin Bust Block";
	79 = "FOF: Crumbling Block (Respawn)";
	80 = "FOF: Crumbling Block (No Respawn)";
	81 = "FOF: Platform, Translucent";
	82 = "FOF: Platform, Crumbling (Respawn), Translucent";
	83 = "FOF: Platform, Crumbling (No Respawn), Translucent";
	84 = "FOF: Spin Bust Block, Translucent";
	85 = "Movement/Scrolling: Scroll Wall First Side Opposite Direction";
	86 = "FOF: Shatter Block, Translucent";
	87 = "FOF: Custom";
	88 = "Continuous Mover: Falling Sector";
	89 = "FOF: Rising, Platform, Solid, Opaque, Shadowcasting";
	90 = "FOF: Rising, Platform, Opaque, Solid, No Shadow";
	91 = "FOF: Rising, Platform, Solid, Translucent";
	92 = "FOF: Rising, Platform, Opaque";
	93 = "FOF: Rising, Platform, Translucent";
	94 = "FOF: Rising, Platform, Solid, Invisible";
	95 = "Linedef Executor: Trigger Repeatedly (Ring Count)";
	96 = "Linedef Executor: Trigger Repeatedly";
	97 = "Linedef Executor: Trigger Each Time";
	98 = "Linedef Executor: Trigger Once";
	99 = "Linedef Executor: Trigger Once (Ring Count)";
	100 = "Movement/Scrolling: Scroll Wall First Side Left";
	101 = "Linedef Executor: Set Floor Y and Pic";
	102 = "Linedef Executor: Set Ceiling Y and Pic";
	103 = "Linedef Executor: Set Sector Light Level";
	104 = "Linedef Executor: Teleport Player";
	105 = "Linedef Executor: Change Music";
	106 = "Linedef Executor: Move Sector Floor";
	107 = "Linedef Executor: Move Sector Ceiling";
	108 = "Linedef Executor: Lower Floor by Line";
	109 = "Linedef Executor: Raise Floor by Line";
	110 = "Linedef Executor: Lower Ceiling by Line";
	111 = "Linedef Executor: Raise Ceiling by Line";
	112 = "Linedef Executor: Change Calling Sector's Tag";
	113 = "Linedef Executor: Run Script";
	114 = "Linedef Executor: Change Front Sector's Tag";
	115 = "Linedef Executor: Play SFX";
	116 = "Linedef Executor: Stop Plane Movement";
	117 = "Linedef Executor: Fade Light Level";
	118 = "Linedef Executor: Stop Lighting Effect";
	119 = "Linedef Executor: Start Adjustable Fire Flicker";
	120 = "Linedef Executor: Start Adjustable Glowing Light";
	121 = "Linedef Executor: Cut-Away View";
	122 = "Linedef Executor: Stop Object";
	123 = "Linedef Executor: Change Sky";
	124 = "Linedef Executor: Change Weather";
	125 = "Linedef Executor: Change Object State";
	126 = "Linedef Executor: Award Points";
	127 = "Linedef Executor: Start Platform Movement";
	200 = "Movement/Scrolling: Scroll (Disp) Ceiling w/ Objects";
	201 = "Movement/Scrolling: Scroll (Disp) Ceiling Objects";
	202 = "Movement/Scrolling: Scroll Ceiling w/ Objects";
	203 = "Movement/Scrolling: Scroll Ceiling Objects";
	204 = "Movement/Scrolling: Scroll (Acc) Ceiling w/ Objects";
	205 = "Movement/Scrolling: Scroll (Acc) Ceiling Objects";
	213 = "Lighting: Set Floor";
	214 = "Movement/Scrolling: Scroll (Acc) Sector Ceiling";
	215 = "Movement/Scrolling: Scroll (Acc) Sector Floor";
	216 = "Movement/Scrolling: Scroll (Acc) Floor Objects";
	217 = "Movement/Scrolling: Scroll (Acc) Floor w/ Objects";
	218 = "Movement/Scrolling: Scroll (Acc) Wall by Linedef";
	223 = "Friction";
	224 = "Wind/Current: Wind";
	225 = "Wind/Current: Current";
	226 = "Boom Push/Pull";
	227 = "Wind/Current: Current Up";
	228 = "Wind/Current: Current Down";
	229 = "Wind/Current: Wind Up";
	230 = "Wind/Current: Wind Down";
	232 = "FOF: Activate Floating Platform";
	233 = "FOF: Activate Floating Platform (Adj Spd)";
	242 = "Fake Floor";
	245 = "Movement/Scrolling: Scroll (Disp) Sector Ceiling";
	246 = "Movement/Scrolling: Scroll (Disp) Sector Floor";
	247 = "Movement/Scrolling: Scroll (Disp) Floor Objects";
	248 = "Movement/Scrolling: Scroll (Disp) Floor w/ Objects";
	249 = "Movement/Scrolling: Scroll (Disp) Wall by Linedef";
	250 = "Movement/Scrolling: Scroll Ceiling";
	251 = "Movement/Scrolling: Scroll Floor";
	252 = "Movement/Scrolling: Scroll Floor Objects";
	253 = "Movement/Scrolling: Scroll Floor w/ Objects";
	254 = "Movement/Scrolling: Scroll Wall by Linedef";
	255 = "Movement/Scrolling: Scroll Texture by Offsets";
}

// THING FLAGS------------------------------------------------------------------
thingflags
{
	1 = "Easy";
	2 = "Medium";
	4 = "Hard";
	8 = "Deaf";
	16 = "Multiplayer";
}

// THING TYPES------------------------------------------------------------------
// Color values: 1-Blue 2-Green 3-Cyan 4-Red 5-Magenta 6-Brown 7-Gray
// 8-Dark_Gray 9-Light_Blue 10-Light_Green 11-Light_Cyan 12-Light_Red 13-Pink
// 14-Yellow 15-White
thingtypes
{
	editor
	{
		color = 15;	// White
		arrow = 1;
		title = "Editor Things";
		width = 16;
		
		32000
		{
			title = "3D Mode start";
		}

		32001
		{
			title = "Control Sector Position Hint";
			error = -1;
		}
	}

	players //------------------------------------------------------------------
	{
		color = 1;	// Blue
		arrow = 1;
		title = "Player Starts";
		width = 32;
		sort = 2;
		zfactor = 32;
		deaftext = "Spawn from ceiling";
		
		1
		{
			title = "Player 1 Start";
			sprite = "SUPTD0";
			height = 96;
		}
		2
		{
			title = "Player 2 Start";
			sprite = "SUPTD0";
			height = 96;
		}
		3
		{
			title = "Player 3 Start";
			sprite = "SUPTD0";
			height = 96;
		}
		4
		{
			title = "Player 4 Start";
			sprite = "SUPTD0";
			height = 96;
		}
		4001
		{
			title = "Player 5 Start";
			sprite = "SUPTD0";
			height = 96;
		}
		4002
		{
			title = "Player 6 Start";
			sprite = "SUPTD0";
			height = 96;
		}
		4003
		{
			title = "Player 7 Start";
			sprite = "SUPTD0";
			height = 96;
		}
		4004
		{
			title = "Player 8 Start";
			sprite = "SUPTD0";
			height = 96;
		}
		87
		{
			title = "Red Team Start (CTF)";
			sprite = "SUPTD0";
			height = 96;
		}
		89
		{
			title = "Blue Team Start (CTF)";
			sprite = "SUPTD0";
			height = 96;
		}
		11
		{
			title = "Deathmatch Start";
			sprite = "SUPTD0";
			height = 96;
		}
	}

	enemies //------------------------------------------------------------------
	{
		color = 4;	// Red
		arrow = 1;
		title = "Enemies";
		width = 40;
		sort = 2;

		3004
		{ 
			title = "Blue Crawla";
			sprite = "POSSA1";
			height = 64;
		}
		9
		{
			title = "Red Crawla";
			sprite = "SPOSA1";
			height = 64;
		}
		21
		{
			title = "Crawla Commander";
			sprite = "CCOMA1";
			height = 64;
		}
		5005
		{
			title = "Gold Buzz";
			sprite = "BUZZA1";
			height = 64;
		}
		5006
		{
			title = "Red Buzz";
			sprite = "RBUZA1";
			height = 64;
		}
		3005
		{
			title = "Jettysyn Bomber";
			sprite = "JETBB1";
			height = 64;
		}
		22
		{
			title = "Jettysyn Gunner";
			sprite = "JETGC1";
			height = 64;
		}
		58
		{
			title = "Stupid Dumb Unnamed Robo-fish";
			sprite = "FISHA0";
			height = 32;
		}
		56
		{
			title = "Skim";
			sprite = "SKIMA0";
			height = 64;
		}
		71
		{
			title = "Deton";
			sprite = "DETNA1";
			height = 64;
		}
		2004
		{
			title = "Turret";
			sprite = "TRETA1";
			height = 32;
		}
		42
		{
			title = "Pop Up Turret";
			sprite = "TURRD1";
			height = 96;
		}
		16
		{
			title = "Egg Mobile (Boss A)";
			width = 80;
			sprite = "EGGMA1";
			height = 96;
			zfactor = 32;
			deaftext = "Has spikeballs";
			multitext = "End level on death";
		}
		2008
		{
			title = "Egg Slimer (Boss B)";
			width = 80;
			sprite = "EGGNA1";
			height = 96;
			zfactor = 32;
		}
		17
		{
			title = "Boss Flypoint";
			width = 20;
			sprite = "EGGNG1";
			height = 96;
		}
		8
		{
			title = "Chaos Enemy Spawn Point (1.08 only)";
			width = 20;
			sprite = "TFOGI0";
			height = 64;
		}
	}

	NiGHTS //-------------------------------------------------------------------
	{
		color = 11;	// Light Cyan
		arrow = 0;
		title = "NiGHTS items";
		width = 20;
		sort = 2;
		height = 32;

		
		72
		{
			title = "Axis";
			circle = 1;
		}

		61 = "Axis Transfer";
		46 = "Axis Transfer Line";
		60
		{
			title = "NiGHTS Super Sonic";
			sprite = "NDRNA1";
			height = 64;
		}
		37
		{
			title = "Wing Logo";
			sprite = "NWNGA0";
			height = 64;
		}
		57 = "Hoop";
		47 = "Ring Circle";
		2007 = "Big Ring Circle";
		2048 = "Wing Logo Circle";
		40 = "NiGHTS Egg Capsule";
		2010 = "Big Wing Logo Circle";
		2046 = "Ring N Wing Circle";
		2047 = "Big Ring N Wing Circle";
		3007 = "Super loop";
		3008 = "Drill refill";
		3009 = "Helper";
	}

	unused
	{
		color = 7;		// Grey
		arrow = 0;
		title = "Unused items";
		width = 20;
		sort = 2;

		82 = "Axis Transfer Condition";
		85 = "Axis Transfer Condition 2";
	}

	monitors //-----------------------------------------------------------------
	{
		color = 7;	// Gray?
		arrow = 0;
		title = "Monitors";
		width = 20;
		sort = 2;
		deaftext = "Respawn randomly";

		41
		{
			title = "Extra Life";
			sprite = "PRUPA0";
			height = 64;
		}
		2022
		{
			title = "Invincibility";
			sprite = "PINVA0";
			height = 64;
		}
		25
		{
			title = "Super Sneakers";
			sprite = "SHTVA0";
			height = 64;
		}
		2028
		{
			title = "Liquid Shield";
			sprite = "BLTVA0";
			height = 64;
		}
		35
		{
			title = "Whirlwind shield";
			sprite = "WHTVA0";
			height = 64;
		}
		48
		{
			title = "Lightning Shield";
			sprite = "YLTVA0";
			height = 64;
		}
		2002
		{
			title = "Fire Shield";
			sprite = "RDTVA0";
			height = 64;
		}
		2018
		{
			title = "Armageddon Shield";
			sprite = "BKTVA0";
			height = 64;
		}
		2011
		{ 
			title = "Super Rings";
			sprite = "SRBXA0";
			height = 64;
		}
		2012
		{
			title = "Silver Ring";
			sprite = "GRBXA0";
			height = 64;
		}
		78
		{
			title = "Teleporter";
			sprite = "MIXUA0";
			height = 64;
		}
		2005
		{
			title = "Robotnik";
			sprite = "EGGBA0";
			height = 64;
		}
		3000
		{
			title = "Random Monitor";
			sprite = "QUESA0";
			height = 64;
		}
	}

	weapons //------------------------------------------------------------------
	{
		color = 14;	// Yellow
		arrow = 0;
		title = "Rings and Weapons";
		width = 20;
		sort = 2;

		// deafheight isn't actually used yet.
		
		2014
		{
			title= "Ring";
			sprite = "BON1A0";
			height = 32;
			deafheight = 32;
			deaftext = "Float";
		}
		84
		{
			title = "5 Vertical Rings for Yellow Spring";
			sprite = "BON1A0";
			height = 64;
		}
		44 
		{
			title = "5 Vertical Rings for Red Spring";
			sprite = "BON1A0";
			height = 64;
		}
		76 
		{
			title = "5 Diagonal Rings for Yellow Spring";
			sprite = "BON1A0";
			height = 64;
			arrow = 1;
		}
		77 
		{
			title = "10 Diagonal Rings for Red Spring";
			sprite = "BON1A0";
			height = 64;
			arrow = 1;
		}
		69
		{
			title= "Homing Ring";
			sprite = "HOMNIND";
			height = 32;
			deafheight = 32;
			deaftext = "Float";
		}
		3003
		{
			title = "Rail Ring";
			sprite = "RAILIND";
			height = 32;
			deafheight = 32;
			deaftext = "Float";
		}
		26
		{
			title = "Auto Ring";
			sprite = "AUTOIND";
			height = 32;
			deafheight = 32;
			deaftext = "Float";
		}
		54
		{
			title = "Bomb Ring";
			sprite = "BOMBIND";
			height = 32;
			deafheight = 32;
			deaftext = "Float";
		}
		80
		{
			title = "Infinity Ring";
			sprite = "INFNIND";
			height = 32;
			deafheight = 32;
			deaftext = "Float";
		}
	}

	collectables //-------------------------------------------------------------
	{
		color = 10;	// Light Green
		arrow = 0;
		title = "Other Collectables";
		width = 20;
		sort = 2;
		
		31
		{
			title = "Red Flag (CTF)";
			sprite = "RFLGA0";
			height = 96;
		}
		34
		{
			title = "Blue Flag (CTF)";
			sprite = "BFLGA0";
			height = 96;
		}
		2013
		{
			title = "Special Stage Token";
			sprite = "TOKEA0";
			height = 64;
		}
		64
		{	title = "Emerald Hunt Location A";
			sprite = "EMERA0";
			height = 32;
		}
		3002
		{	title = "Emerald Hunt Location B";
			sprite = "EMERA0";
			height = 32;
		}
		3001
		{
			title = "Emerald Hunt Location C";
			sprite = "EMERA0";
			height = 32;
		}
		420
		{ 
			title = "Emerald 1 (Green)";
			sprite = "cemga0";
			height = 32;
		}
		421
		{ 
			title = "Emerald 2 (Orange)";
			sprite = "cemoa0";
			height = 32;
		}
		422
		{ 
			title = "Emerald 3 (Pink)";
			sprite = "cempa0";
			height = 32;
		}
		423
		{ 
			title = "Emerald 4 (Blue)";
			sprite = "cemba0";
			height = 32;
		}
		424
		{ 
			title = "Emerald 5 (Red)";
			sprite = "cemra0";
			height = 32;
		}
		425
		{ 
			title = "Emerald 6 (Light Blue)";
			sprite = "cemla0";
			height = 32;
		}
		426
		{ 
			title = "Emerald 7 (Grey)";
			sprite = "cemya0";
			height = 32;
		}
		427
		{
			title = "Emerald 8 (Master)";
			sprite = "cemka0";
			height = 32;
		}
	}

	springs //------------------------------------------------------------------
	{
		color = 12;	// Light Red
		arrow = 1;
		title = "Springs and Other Vertical Boosters";
		width = 20;
		sort = 2;
		
		28
		{
			title = "Yellow Spring (384)";
			sprite = "SPRYA0";
			height = 32;
			arrow = 0;
		}
		65
		{
			title = "Yellow Spring Down";
			sprite = "SUDYA0";
			height = 32;
			hangs = 1;
			arrow = 0;
		}
		2015 
		{
			title = "Yellow Spring Diagonal Up";
			sprite = "YSPRD2";
			height = 32;
		}
		20
		{
			title = "Yellow Spring Diagonal Down";
			sprite = "YSUDE2";
			height = 32;
			hangs = 1;
		}
		79
		{
			title = "Red Spring (1024)";
			sprite = "SPRRA0";
			height = 32;
			arrow = 0;
		}
		66
		{
			title = "Red Spring Down";
			sprite = "SUDRA0";
			height = 32;
			hangs = 1;
			arrow = 0;
		}
		38
		{
			title = "Red Spring Diagonal Up";
			sprite = "RSPRD2";
			height = 32;
		}
		39
		{
			title = "Red Spring Diagonal Down";
			sprite = "RSUDE2";
			height = 32;
			hangs = 1;
		}
		30
		{
			title = "Gas Jet";
			sprite = "STEMD0";
			height = 64;
			arrow = 0;
		}
		32
		{
			title = "Fan";
			sprite = "FANSA0";
			height = 32;
			arrow = 0;
		}
		5004
		{
			title = "Blue Spring";
			sprite = "SPRBA0";
			height = 32;
			arrow = 0;
		} 
	}

	hazards //------------------------------------------------------------------
	{
		color = 5;	// Magenta
		arrow = 0;
		title = "Hazards";
		width = 20;
		sort = 2;
		
		68
		{
			title = "Spike";
			sprite = "USPKA0";
			height = 96;
		}
		67
		{
			title = "Hanging Spike";
			sprite = "DSPKA0";
			height = 96;
			hangs = 1;
		}
		23
		{
			title = "Spike ball";
			sprite = "SPIKA0";
			height = 64;
		}
		24
		{
			title = "Torch";
			sprite = "FLAMA0";
			height = 96;
		}
		51
		{
			title = "Laser";
			sprite = "LASRA0";
			height = 32;
		}

	}

	others //-------------------------------------------------------------------
	{
		color = 6;	// Brown
		arrow = 0;
		title = "Other Useful Things";
		width = 20;
		sort = 2;
		
		33
		{
			title = "Air Bubbles"; 
			sprite = "BUBMA0";
			height = 32;
		}
		3006
		{
			title = "Starpost";
			sprite = "STPTA0";
			height = 96;
			arrow = 1;
		}
		2049
		{
			title = "Capsule Center";
			sprite = "SQRLA1";
			height = 32;
		}
		86
		{
			title = "End Sign";
			sprite = "SIGNF0";
			height = 64;
		}
		5001 = "Push";
		5002 = "Pull";
		5003
		{
			title = "Teleport Destination";
			arrow = 1;
			height = 32;
		}
		5007
		{
			title = "Alt View (Cut-Away)";
			arrow = 1;
			height = 32;
		}
		18
		{
			title = "Zoom Tube Waypoint";
			sprite = "MIXUB0";
			height = 32;
		}
	}

	scenery //------------------------------------------------------------------
	{
		color = 2;	// Green
		arrow = 0;
		title = "Scenery";
		width = 20;
		sort = 2;

		36
		{
			title = "Orange Flower";
			sprite = "FWR1A0";
			height = 64;
		}
		70
		{ 
			title = "Sun-Flower";
			sprite = "FWR2A0";
			height = 128;
		}
		73
		{
			title = "Purple Budding Flower";
			sprite = "FWR3A0";
			height = 32;
		}
		75
		{
			title = "Small Tree";
			sprite = "BUS2A0";
			height = 64;
		}
		74
		{
			title = "Small Tree w/ Fruit";
			sprite = "BUS1A0";
			height = 64;
		}
		2035
		{
			title = "Polluted Flower";
			sprite = "THZPA0";
			height = 32;
		}
		2001
		{
			title = "Dead Flower";
			sprite = "FWR4A0";
			height = 64;
		}
		81
		{
			title = "Gargoyle";
			sprite = "GARGA1";
			height = 64;
			deaftext = "Non-pushable";
		}
		49
		{
			title = "Chain";
			sprite = "CHANA0";
			height = 128;
			hangs = 1;
		}
		5
		{
			title = "Christmas Pole";
			sprite = "XMS1A0";
			height = 96;
		}
		13
		{
			title = "Candy Cane";
			sprite = "XMS2A0";
			height = 96;
		}
		6
		{
			title = "Snow Man";
			sprite = "XMS3A0";
			height = 96;
			deaftext = "Non-pushable";
		}
	}

	sounds //-------------------------------------------------------------------
	{
		color = 3;	// Cyan
		arrow = 0;
		title = "Sounds and Lights";
		width = 16;
		height = 32;
		sort = 2;
		
		14  = "Random Ambience";
		2019 = "Water sound 3B (Small)";
		2025 = "Water Sound 4A (Extra Large)";
		2026 = "Water Sound 1A (Large)";
		2023 = "Water Sound 2A (Medium)";
		83 = "Water Sound 3A (Small)";
		27 = "Water Sound 2 (Huge)";
		2024 = "Water Sound 1B (Large)";
		2045 = "Water Sound 2B (Medium)";

		2006
		{
			title = "Alarm";
			sprite = "ALRMA0";
			height = 32;
			hangs = 1;
		}
		2003 = "Light Source";
	}

	 mario //-------------------------------------------------------------------
	 {
		color = 9;
		arrow = 0;
		title = "Mario themed items";

		10005
		{
		       title = "Coin";
		       sprite = "COINB0";
		       height = 64;
		}
		50
		{
			title= "Fire Flower";
			sprite = "FFWRB0";
			height = 64;
		}
		10002
		{
			title = "Mario Tree";
			sprite = "MUS1A0";
			height = 96;
		}
		10003
		{
			title = "Mario Tree (Tall)";
			sprite = "MUS2A0";
			height = 128;
		}
		10004
		{
			title = "Toad";
			sprite = "TOADA0";
			height = 96;
		}
		12
		{
			title = "Mario Bridge Axe";
			sprite = "MAXEA0";
			height = 64;
		}
		10
		{
			title = "Koopa Shell";
			sprite = "SHLLA0";
			height = 64;
		}
		29
		{
			title = "Leaping Fireball (Puma)";
			sprite = "PUMAA0";
			height = 96;
		}
		10000
		{
			title = "Goomba";
			sprite = "GOOMB0";
			height = 64;
			arrow = 1;
		}
		10001
		{
			title = "Blue Underground Goomba";
			sprite = "BGOMA0";
			height = 64;
			arrow = 1;
		}
		19
		{
			title = "Bowser";
			width = 80;
			sprite = "KOOPA0";
			height = 96;
			arrow = 1;
		}
	}
}
