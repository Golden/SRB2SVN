int extract(WAD *wad);
void hex_dump(unsigned char *buf, int len);
int delete_lump(WAD *wad);
int write_wad(WAD *wad);
int create_lump(WAD *wad);
int set_lump(WAD *wad);
