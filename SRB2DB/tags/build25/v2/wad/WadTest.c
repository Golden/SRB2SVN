#include <stdio.h>
#include <string.h>
#include <stdarg.h>
#include <io.h>
#include <windows.h>

#include "wad.h"
#include "WadTest.h"

static void debug_printf(const char *fmt, ...);
static void WINAPI display_callback(const char *str);

int main(int argc, char *argv[])
{
	WAD *wad;
	int quit = FALSE;
	char buf[256];
	char *cmd;
	int len;
	int ret;

	if(argc >= 2)
	{
		/* Load the wad. */
		wad = OpenWAD(argv[1]);
		if(!wad)
		{
			fprintf(stderr, "Error opening %s.\n\n", argv[1]);
			return 1;
		}
	}
	else
	{
		/* New wad. */
		wad = CreateWAD();
		if(!wad)
		{
			fprintf(stderr, "Error creating WAD.\n\n");
			return 2;
		}
	}

	/* No funny stuff. */
	buf[255] = '\0';

	while(!quit)
	{
		printf("> ");
		fgets(buf, 255, stdin);

		cmd = strtok(buf, " \n\r\t");
		len = strlen(cmd);

		if(strncmp(cmd, "create", len) == 0)
		{
			ret = create_lump(wad);
			if(ret < 0) fprintf(stderr, "Error creating lump.\n");

			debug_printf("create_lump returned %d.\n", ret);
		}
		else if(strncmp(cmd, "delete", len) == 0)
		{
			ret = delete_lump(wad);
			if(ret >= 0) printf("Deleted lump size: %d bytes.\n", ret);
			else fprintf(stderr, "Error deleting lump.\n");

			debug_printf("delete_lump returned %d.\n", ret);
		}
		else if(strncmp(cmd, "extract", len) == 0)
		{
			ret = extract(wad);
			if(ret >= 0) printf("%d bytes.\n", ret);
			else fprintf(stderr, "Error extracting lump.\n");

			debug_printf("extract returned %d.\n", ret);
		}
		else if(strncmp(cmd, "list", len) == 0)
		{
			ret = EnumLumpNames(wad, display_callback);
			if(ret >= 0) printf("%d lumps.\n", ret);
			else fprintf(stderr, "Error reading lump names.\n");

			debug_printf("list_lumps returned %d.\n", ret);
		}
		else if(strncmp(cmd, "quit", len) == 0)
		{
			quit = TRUE;
			continue;
		}
		else if(strncmp(cmd, "set", len) == 0)
		{
			ret = set_lump(wad);
			if(ret >= 0) printf("%d bytes.\n", ret);
			else fprintf(stderr, "Error setting lump data.\n");

			debug_printf("set_lump returned %d.\n", ret);
		}
		else if(strncmp(cmd, "version", len) == 0)
		{
			printf("%s %s\n", __DATE__, __TIME__);
		}
		else if(strncmp(cmd, "write", len) == 0)
		{
			ret = write_wad(wad);
			if(ret < 0) fprintf(stderr, "Error writing WAD.\n");

			debug_printf("write_wad returned %d.\n", ret);
		}
		else fprintf(stderr, "Bad command: %s.\n", cmd);
	}

	FreeWAD(wad);

	return 0;
}


/* Gets lump data and displays the hex or outputs to disc.
 * Returns the length, or negative on error.
 */

int extract(WAD *wad)
{
	char *param1, *param2, *section, *lumpname;
	int len;
	unsigned char *buf;

	param1 = strtok(NULL, " \t\r\n");
	param2 = strtok(NULL, " \t\r\n");

	if(!param1) return -6;

	/* If there's no second sub-param, the first was the lumpname. */
	if(strchr(param1, ':'))
	{
		section = strtok(param1, ":");
		lumpname = strtok(NULL, "");
	}
	else
	{
		lumpname = param1;
		section = NULL;
	}

	len = GetLumpLength(wad, lumpname, section);
	if(len < 0) return -1;
	else if(len == 0) return 0;

	buf = (unsigned char*)malloc(len);
	if(!buf) return -2;

	if(GetLump(wad, lumpname, section, buf, len) < 0)
	{
		free(buf);
		return -3;
	}

	/* Write to disc? */
	if(param2)
	{
		FILE *f = fopen(param2, "wb");
		if(!f)
		{
			free(buf);
			return -4;
		}

		if(fwrite(buf, len, 1, f) != 1)
		{
			fclose(f);
			free(buf);
			return -5;
		}

		fclose(f);
	}
	else
		hex_dump(buf, len);


	free(buf);

	return len;
}

/* Displays len bytes of data in buf, in two eight-byte columns of
 * hex digits, with ASCII.
 */
void hex_dump(unsigned char *buf, int len)
{
	int i, leftinline;

	if(!buf) return;

	while(len > 0)
	{
		leftinline = min(len, 16);
		/* Hex. */
		for(i = 0; i < leftinline; i++)
		{
			printf("%02X ", buf[i]);
			if(i==7) printf("   ");
		}

		/* Align to the ASCII column. */
		for(i = 0; i < 3*(17 - leftinline + ((leftinline <= 8) ? 1 : 0)); i++)
			putchar(' ');

		/* ASCII. */
		for(i = 0; i < leftinline; i++)
			putchar((buf[i] >= 0x20 && buf[i] <= 0x7E) ? buf[i] : '.');

		putchar('\n');

		buf += 16;
		len -= 16;
	}
}


int delete_lump(WAD *wad)
{
	char *param;
	int ret;

	param = strtok(NULL, " \t\r\n");

	if(!param) return -1;

	ret = DeleteLump(wad, param, NULL);
	if(ret < 0) return -2;
	return ret;
}


int create_lump(WAD *wad)
{
	char *param1, *param2, *section, *lumpname;
	int ret;

	param1 = strtok(NULL, " \t\r\n");
	param2 = strtok(NULL, " \t\r\n");

	if(!param1) return -1;

	/* If there's no second sub-param, the first was the lumpname. */
	if(strchr(param1, ':'))
	{
		section = strtok(param1, ":");
		lumpname = strtok(NULL, "");
	}
	else
	{
		lumpname = param1;
		section = NULL;
	}

	ret = CreateLump(wad, lumpname, section, param2);
	if(!ret) return -2;
	return 0;
}

int set_lump(WAD *wad)
{
	char *param1, *param2, *section, *lumpname;
	int ret;
	unsigned char *buffer;
	FILE *f;
	int len;

	param1 = strtok(NULL, " \t\r\n");
	param2 = strtok(NULL, " \t\r\n");

	if(!param1 || !param2) return -1;

	/* If there's no second sub-param, the first was the lumpname. */
	if(strchr(param1, ':'))
	{
		section = strtok(param1, ":");
		lumpname = strtok(NULL, "");
	}
	else
	{
		lumpname = param1;
		section = NULL;
	}

	f = fopen(param2, "rb");
	if(!f) return -2;

	len = filelength(fileno(f));
	buffer = (unsigned char*)malloc(len);
	if(!buffer)
	{
		fclose(f);
		return -3;
	}

	if(fread(buffer, len, 1, f) != 1)
	{
		free(buffer);
		fclose(f);
		return -4;
	}

	ret = SetLump(wad, lumpname, section, buffer, len);
	if(!ret) return -5;

	free(buffer);
	fclose(f);

	return len;
}


int write_wad(WAD *wad)
{
	char *param;

	param = strtok(NULL, " \t\r\n");

	if(!WriteWAD(wad, param)) return -1;
	return 0;
}



/* Static functions. */





static void WINAPI display_callback(const char *str)
{
	puts(str);
}


static void debug_printf(const char *fmt, ...)
{
#ifdef _DEBUG
	va_list val;

	va_start(val, fmt);
	vfprintf(stderr, fmt, val);
	va_end(val);
#endif
}

