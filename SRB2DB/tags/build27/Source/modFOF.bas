Attribute VB_Name = "modFOF"
'
'    Doom Builder
'    Copyright (c) 2003 Pascal vd Heiden, www.codeimp.com
'    This program is released under GNU General Public License
'
'    This program is distributed in the hope that it will be useful,
'    but WITHOUT ANY WARRANTY; without even the implied warranty of
'    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
'    GNU General Public License for more details.
'

' This module by Oogaland

' FOF Stuff.


'Do not allow any undeclared variables
Option Explicit

'Case sensitive comparisions
Option Compare Binary


Public Const FF_EXISTS = &H1&                     ' Always set, to check for validity.
Public Const FF_SOLID = &H2&                      ' Clips things.
Public Const FF_RENDERSIDES = &H4&                ' Renders the sides.
Public Const FF_RENDERPLANES = &H8&               ' Renders the floor/ceiling.
Public Const FF_RENDERALL = &HC&                  ' Renders everything.
Public Const FF_SWIMMABLE = &H10&                 ' Is a water block.
Public Const FF_NOSHADE = &H20&                   ' Messes with the lighting?
Public Const FF_CUTSOLIDS = &H40&                 ' Cuts out hidden solid pixels.
Public Const FF_CUTEXTRA = &H80&                  ' Cuts out hidden translucent pixels.
Public Const FF_CUTLEVEL = &HC0&                  ' Cuts out all hidden pixels.
Public Const FF_CUTSPRITES = &H100&               ' Final step in making 3D water.
Public Const FF_BOTHPLANES = &H200&               ' Renders both planes all the time.
Public Const FF_EXTRA = &H400&                    ' Gets cut by ::FF_CUTEXTRA.
Public Const FF_TRANSLUCENT = &H800&              ' See through!
Public Const FF_FOG = &H1000&                     ' Fog "brush."
Public Const FF_INVERTPLANES = &H2000&            ' Reverse the plane visibility rules.
Public Const FF_ALLSIDES = &H4000&                ' Render inside and outside sides.
Public Const FF_INVERTSIDES = &H8000&             ' Only render inside sides.
Public Const FF_DOUBLESHADOW = &H10000            ' Make two lightlist entries to reset light?
Public Const FF_FLOATBOB = &H20000                ' Floats on water and bobs if you step on it.
Public Const FF_NORETURN = &H40000                ' Used with ::FF_CRUMBLE. Will not return to its original position after falling.
Public Const FF_CRUMBLE = &H80000                 ' Falls 2 seconds after being stepped on, and randomly brings all touching crumbling 3dfloors down with it, providing their master sectors share the same tag (allows crumble platforms above or below, to also exist).
Public Const FF_SHATTERBOTTOM = &H100000          ' Used with ::FF_BUSTUP. Like FF_SHATTER, but only breaks from the bottom. Good for springing up through rubble.
Public Const FF_MARIO = &H200000                  ' Acts like a question block when hit from underneath. Goodie spawned at top is determined by master sector.
Public Const FF_BUSTUP = &H400000                 ' You can spin through/punch this block and it will crumble!
Public Const FF_QUICKSAND = &H800000              ' Quicksand!
Public Const FF_PLATFORM = &H1000000              ' You can jump up through this to the top.
Public Const FF_SHATTER = &H2000000               ' Used with ::FF_BUSTUP. Thinks everyone's Knuckles.
Public Const FF_SPINBUST = &H4000000              ' Used with ::FF_BUSTUP. Jump or fall onto it while curled in a ball.
Public Const FF_KNUXONLY = &H8000000              ' Not real! Used internally by DB. Transformed into NOCLIMB.

' These will be set manually if custom, but handled by NOCLIMB otherwise.
Public Const FF_RUNTIME_FLAGS = FF_SHATTERBOTTOM Or FF_KNUXONLY


Public Const LF_NOCLIMB = &H40&

Public Const LE_COLOURMAP = 16
Public Const LE_CUSTOM = 87

Public Const LE_W_TRANS = 45
Public Const LE_W_OPAQUE = 48
Public Const LE_W_TRANSNOSIDES = 74
Public Const LE_W_OPAQUENOSIDES = 75

Public Const SS_FIREHURT = 7
Public Const SS_NONELEMHURT = 11
Public Const SS_SLIMEHURT = 983

' These must match the combo box list indices.
Public Const FC_NONE = -1
Public Const FC_BLOCK = 0
Public Const FC_WATER = 1
Public Const FC_MAX = 1



' Used when we're not using selection
Public ForceLD As Long
Public ParentSector As Long



'Public Function SearchFOFList(ByVal effect As Long) As Long
'
'     Dim i As Long
'
'     For i = LBound(FOFTypes, 1) To UBound(FOFTypes, 1)
'
'          If effect = FOFTypes(i, 0) Then
'               SearchFOFList = i
'               Exit Function
'          End If
'
'     Next i
'
'     SearchFOFList = -1
'
'End Function


Public Function FindFOFLinedefs(ByVal ctrlTag As Long, ByRef matches As Long, ByRef Flags As Long, ByRef FOFClass As Long) As Long

     Dim i As Long, j As Long
     
     matches = 0
     FindFOFLinedefs = -1
     FOFClass = FC_NONE
     
     For i = 0 To numlinedefs - 1
     
          If linedefs(i).Tag = ctrlTag And linedefs(i).effect <> 0 Then
          
               j = GetFOFFlagsAndClassByLDIndex(i, Flags, FOFClass)
               
               If j > 0 Then
                    FindFOFLinedefs = i
                    matches = matches + j
               End If
               
          End If
     
     Next i

End Function



Public Function GetFOFFlagsAndClassByLDIndex(ByVal ldindex As Long, ByRef Flags As Long, ByRef FOFClass As Long) As Long

     Dim effect As Long, Index As Long
     
     effect = linedefs(ldindex).effect
     
     GetFOFFlagsAndClassByLDIndex = 0

     If effect = LE_CUSTOM Then
               
          Flags = Val("&H" & sidedefs(linedefs(ldindex).s2).Upper)
          GetFOFFlagsAndClassByLDIndex = GetFOFFlagsAndClassByLDIndex + 1
          FOFClass = FC_BLOCK
          
     ElseIf mapconfig("fofs").Exists(CStr(effect)) Then
     
          Flags = mapconfig("fofs")(CStr(effect))
          GetFOFFlagsAndClassByLDIndex = GetFOFFlagsAndClassByLDIndex + 1
          
          If (Flags And FF_SWIMMABLE) = FF_SWIMMABLE Then
               FOFClass = FC_WATER
          Else
               FOFClass = FC_BLOCK
          End If
          
     End If

End Function



Public Function IsFOFLinedef(ByVal i As Long) As Boolean
     
     IsFOFLinedef = False
          
     If linedefs(i).effect = LE_CUSTOM _
        Or mapconfig("fofs").Exists(CStr(linedefs(i).effect)) Then

          IsFOFLinedef = True
          
     End If

End Function


Public Function ShouldDrawFOFSides(fofld As Long) As Boolean

     If linedefs(fofld).effect = LE_CUSTOM Then
          ShouldDrawFOFSides = Val("&H" & sidedefs(linedefs(fofld).s2).Upper) And FF_RENDERSIDES
     Else
          If mapconfig("fofs").Exists(CStr(linedefs(fofld).effect)) Then
               ShouldDrawFOFSides = mapconfig("fofs")(CStr(linedefs(fofld).effect)) And FF_RENDERSIDES
          Else
               ShouldDrawFOFSides = False
          End If
     End If

End Function



Public Function ShouldDrawFOFPlanes(fofld As Long) As Boolean

     If linedefs(fofld).effect = LE_CUSTOM Then
          ShouldDrawFOFPlanes = Val("&H" & sidedefs(linedefs(fofld).s2).Upper) And FF_RENDERPLANES
     Else
          If mapconfig("fofs").Exists(CStr(linedefs(fofld).effect)) Then
               ShouldDrawFOFPlanes = mapconfig("fofs")(CStr(linedefs(fofld).effect)) And FF_RENDERPLANES
          Else
               ShouldDrawFOFPlanes = False
          End If
     End If

End Function



Public Sub FOFDialogueBySelection()

     ForceLD = -1
     ParentSector = -1
     frmFOF.Show 1, frmMain

End Sub

Public Sub FOFDialogueByFOFLDAndSector(ByVal fofld As Long, ByVal sector As Long)

     ForceLD = fofld
     ParentSector = sector
     frmFOF.Show 1, frmMain

End Sub


Public Function CreateControlSector(Optional ByVal radius As Long = 16) As Long

     Static x As Long, y As Long        ' Not reset on loading another map, mind.
     Dim i As Long
     Dim rc As RECT
     Dim DX As Long, dy As Long
     
     ' Assume we move 3 * radius px to the left each time.
     DX = -3 * radius
     dy = 0
     
     ' Search for a position hinter, and use it if we find one.
     For i = LBound(things) To UBound(things)
          If things(i).thing = mapconfig("ctrlsechint") Then
               x = things(i).x
               y = -things(i).y
               
               DX = 3 * radius * Cos(things(i).angle * pi / 180)
               dy = -3 * radius * sIn(things(i).angle * pi / 180)
               Exit For
          End If
     Next i
     
     rc.bottom = -(y - (radius + 1))
     rc.top = -(y + (radius + 1))
     rc.left = x - (radius + 1)
     rc.right = x + (radius + 1)
    
     ' Keep moving left until we're outside a sector.
     ' If this ends up being too slow, try hacking in a short-circuit for the
     ' huge conditional. Or rewrite it in C++, or something.
     Do While CheckCSecSpace(x, y, radius) Or ExistVerticesInRect(rc)
              
          x = x + DX
          y = y + dy
          rc.bottom = -(y - (radius + 1))
          rc.top = -(y + (radius + 1))
          rc.left = x - (radius + 1)
          rc.right = x + (radius + 1)
          
     Loop
    
     ' Make the sector.
     CreateControlSector = CreateSectorHereParams(x, y, 4, radius, False, False)

End Function

Private Function CheckCSecSpace(ByVal x As Long, ByVal y As Long, ByVal radius As Long)

     Dim i As Long
     
     For i = 0 To radius * 2 + 4
     
          If IntersectSector(x - (radius + 2) + i, y - (radius + 2) + i, vertexes(0), linedefs(0), VarPtr(sidedefs(0)), numlinedefs, 0) >= 0 Then
               CheckCSecSpace = True
               Exit Function
          End If
          
          If IntersectSector(x - (radius + 2) + i, y + (radius + 2) - i, vertexes(0), linedefs(0), VarPtr(sidedefs(0)), numlinedefs, 0) >= 0 Then
               CheckCSecSpace = True
               Exit Function
          End If
          
     Next i
     
     CheckCSecSpace = False

End Function
