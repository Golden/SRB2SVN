SRB2 Builder 0.1 - README.TXT
http://homepages.inf.ed.ac.uk/s0569864/srb2builder/
oogaland_NO@SPAM_gmail.com
---------------------------------------------------

THIS IS AN ALPHA BUILD! Expect crashes. Also, only the most basic features have
been implemented: it's no replacement for existing map editors yet, but it *is*
possible to create a complete map with it.


System Requirements
-------------------

Not tested yet, but it should run on Windows 98 and ME, in addition to NT 4.0,
2000, XP and Vista. I'd be interested to hear about people's results with the
latter. It doesn't need much RAM, so that shouldn't be a problem. CPU? Don't
know; give it a try. This build was compiled without optimisations, to make it
easier to debug, but that does slow it down a bit.


Setting up SRB2 Builder
-----------------------

The options interface hasn't been implemented yet, so you'll need to set things
in the configuration file by hand. The most important one is the IWAD: open
conf/builder.cfg and find the entry for srb2-1094.cfg and set it to the path to
SRB2.SRB. Use double-backslashes for path separation, as the backslash is the
escape character.


What is, and what isn't, in this version
----------------------------------------

The basic editing features have been implemented. It's possible to draw sectors,
set textures, copy and paste, set heights and so on. To test these is the
purpose of this release. More advanced features such as FOF editing will come
later. 3D mode is a long way off yet.

Two fairly important missing features are automatic nodebuilding and map
testing. These will need to be done by hand for the time being, but with adept
use of the command line, these actions can be repeated very quickly.

Shortcut keys are implemented, but are not yet indicated in the menus. By and
large they're the same as for Doom Builder.

For more details of what's in this build, with particular emphasis on
differences from Doom Builder, please see the website:
http://homepages.inf.ed.ac.uk/s0569864/srb2builder/


Compiling
---------

A Visual C++ 6 workspace, a Dev-C++ 4.9.9.2 project and a Makefile for GNU make
with GCC (MinGW) are provided. A reasonably up-to-date version of the Win32
Platform SDK is required; in particular, the version that ships with MSVC6 is
too old.


Contributors
------------

SRB2 Builder was written by Gregor Dick. It owes a large part of its design, and
at the time of writing somewhat under a tenth of its code, to Pascal vd Heiden's
Doom Builder. It also incorporates a window-docking library by James Brown and
Jeff Glatt, and a bug-reporting library by Maksim Pyatkovskiy. Thanks are due
also to Alam Arias for writing a Makefile and fixing GCC warnings.


Licence
-------

SRB2 Builder is licensed under version 2 of the GNU General Public License. See
the file named COPYING for details.
