#ifndef __SRB2B_CLIPUTIL__
#define __SRB2B_CLIPUTIL__

int CopyBufferToClipboard(void *lpvBuffer, DWORD cbBuffer, UINT uiFormat);
void* GetBufferFromClipboard(UINT uiFormat);

#endif
