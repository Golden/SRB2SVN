#ifndef __SRB2B_TEXTURE__
#define __SRB2B_TEXTURE__

#include <windows.h>

#include "wad.h"


/* Types. */

typedef struct _TEXTURE
{
	HBITMAP			hbitmap;
	unsigned short	cx, cy;
	short			xOffset, yOffset;
} TEXTURE;


typedef enum _TEX_FORMAT
{
	TF_TEXTURE,
	TF_FLAT,
	TF_IMAGE
} TEX_FORMAT;


typedef struct _TEXCACHE
{
	char				szLumpName[9];
	TEXTURE				*lptexture;

	struct _TEXCACHE	*lptcNext;
} TEXCACHE;


typedef struct _TEXTURENAMELIST
{
	LPTSTR	szNames;
	int		iEntries;
	int		cstrBuffer;		/* Number of strings the buffer can hold. */
} TEXTURENAMELIST;


/* Prototypes. */
TEXTURE* LoadTextureW(WAD *lpwad, LPCWSTR sTexName, TEX_FORMAT tf, RGBQUAD *lprgbq);
TEXTURE* LoadTextureA(WAD *lpwad, LPCSTR sTexName, TEX_FORMAT tf, RGBQUAD *lprgbq);
void DestroyTexture(TEXTURE *lptex);
TEXTURE* GetTextureFromCache(TEXCACHE *lptcHdr, LPCSTR szTexName);
void PurgeTextureCache(TEXCACHE *lptcHdr);
TEXTURENAMELIST* CreateTextureNameList(void);
void DestroyTextureNameList(TEXTURENAMELIST *lptnl);
void AddAllTextureNamesToList(TEXTURENAMELIST *lptnl, WAD *lpwad, TEX_FORMAT tf);
void SortTextureNameList(TEXTURENAMELIST *lptnl);
LPCTSTR FindFirstTexNameMatch(LPCTSTR szTexName, TEXTURENAMELIST *lptnl);
void StretchTextureToDC(TEXTURE *lptex, HDC hdcTarget, int cx, int cy);
void AddToTextureCache(TEXCACHE *lptcHdr, LPCSTR szLumpName, TEXTURE *lptex);

#ifdef _UNICODE
BOOL IsPseudoTextureW(LPCWSTR sTexName);
BOOL IsPseudoFlatW(LPCWSTR sTexName);
BOOL IsBlankTextureW(LPCWSTR sTexName);
#endif

BOOL IsPseudoTextureA(LPCSTR sTexName);
BOOL IsPseudoFlatA(LPCSTR sTexName);
BOOL IsBlankTextureA(LPCSTR sTexName);

BOOL IsBonaFideTexture(LPCTSTR sTexName);
BOOL IsBonaFideTextureA(LPCSTR sTexName);

#ifdef _UNICODE
#define IsPseudoTexture IsPseudoTextureW
#define IsPseudoFlat IsPseudoFlatW
#define IsBlankTexture IsBlankTextureW
#else
#define IsPseudoTexture IsPseudoTextureA
#define IsPseudoFlat IsPseudoFlatA
#define IsBlankTexture IsBlankTextureA
#endif


/* Macros. */

#ifdef _UNICODE

#define LoadTexture LoadTextureW
#define GetTextureForMap GetTextureForMapW

#else

#define LoadTexture LoadTextureA
#define GetTextureForMap GetTextureForMapA

#endif

/* Size of PLAYPAL lump. */
#define CB_PLAYPAL 768

#define TEXNAME_BUFFER_LENGTH		9
#define TEXNAME_WAD_BUFFER_LENGTH	8

#define CX_TEXPREVIEW 64
#define CY_TEXPREVIEW 64

#endif
