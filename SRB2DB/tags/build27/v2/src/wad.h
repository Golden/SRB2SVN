#ifndef __SRB2B_WAD__
#define __SRB2B_WAD__

#include <windows.h>


typedef struct _WAD WAD;


#define CCH_LUMPNAME			8


WAD *CreateWad(void);
void FreeWad(WAD *lpwad);
WAD *OpenWad(LPCTSTR szFileName);
long CreateLump(WAD *lpwad, LPCSTR sLumpName, long iLumpIndex);
BOOL SetLump(WAD *lpwad, long iLumpIndex, const BYTE *lpbyData, long cb);
long GetLumpIndex(WAD *lpwad, long iFirstLump, long iLastLump, LPCSTR sLumpName);
long GetLumpCount(WAD *lpwad);
BOOL GetLumpName(WAD *lpwad, long iLumpIndex, LPSTR sLumpName);
long GetLumpLength(WAD *lpwad, long iLumpIndex);
long GetLump(WAD *lpwad, long iLumpIndex, BYTE *lpbyBuffer, long cbBuffer);
BOOL DeleteMultipleLumps(WAD *lpwad, long iFirstLump, long iLastLump);
BOOL WriteWad(WAD *lpwad, LPCTSTR szFileName);
WAD* DuplicateWadToMemory(WAD *lpwad);


#endif
