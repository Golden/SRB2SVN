#ifndef __SRB2B_CI_MAP__
#define __SRB2B_CI_MAP__


#ifdef __cplusplus
extern "C" {
#endif

#include <windows.h>
#include <GL/gl.h>
#include <GL/glu.h>

#include "../maptypes.h"

enum ENUM_THINGRENDERFLAGS
{
	TRF_DIMMED = 1
};

void Render_AllLinedefs(MAP *lpmap, int iIndicatorLength, float fZoom);
void Render_AllVertices(MAP *lpmap, float vertexsize);
void Render_AllThings(MAP *lpmap, float fImageSize, float fZoom, GLuint uiThingTex, BOOL bOutlines, BOOL bFilterThings, THINGFILTERS* lpthingfilters, BYTE byFlags);
void Rereference_Vertices(MAPLINEDEF* linedefs, int numlinedefs, int oldref, int newref);
void Render_TaggedLinedefs(MAPVERTEX* vertices, MAPLINEDEF* linedefs, MAPSECTOR* sectors, MAPSIDEDEF *sidedefs, int numlinedefs, int argtag, int argmark, BYTE c, int indicatorlength, int rendervertices, int vertexsize);
void Render_AllImpassableLinedefs(MAPVERTEX* vertices, MAPLINEDEF* linedefs, int startindex, int endindex, int indicatorlength);
void Render_TaggedSectors(MAPVERTEX* vertices, MAPLINEDEF* linedefs, MAPSIDEDEF* sidedefs, MAPSECTOR* sectors, int numsectors, int numlinedefs, int sectortag, BYTE c, int indicatorlength, int rendervertices, int vertexsize);
void Render_AllThingsDarkened(MAPTHING* things, int startindex, int endindex, BYTE* thingbitmaps, int bitmapswidth, int imagesize, int filterthings, THINGFILTERS* filter);
void Render_ChangingLengths(MAPVERTEX* vertices, MAPLINEDEF* linedefs, int* changelines, int numchangelines, BYTE* bitmap, int width, int height, int charwidth, int charheight);

#ifdef __cplusplus
}
#endif


#endif

