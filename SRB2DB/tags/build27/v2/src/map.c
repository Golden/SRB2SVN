#include <windows.h>

#include "general.h"
#include "maptypes.h"
#include "openwads.h"
#include "editing.h"
#include "wad.h"
#include "like.h"

#include "CodeImp/ci_map.h"


/* AllocateMapStructure
 *   Allocates memory for map structures.
 *
 * Parameters:
 *   None
 *
 * Return value: MAP*
 *   Pointer to the memory allocated.
 *
 * Notes:
 *   The system raises an exception if we run out of memory, so we don't need to
 *   check the return values. Call DestroyMapStructure to free the memory.
 */
MAP *AllocateMapStructure(void)
{
	MAP *lpmap;

	lpmap = ProcHeapAlloc(sizeof(MAP));
	lpmap->sectors = ProcHeapAlloc(65536 * sizeof(MAPSECTOR));
	lpmap->linedefs = ProcHeapAlloc(65536 * sizeof(MAPLINEDEF));
	lpmap->vertices = ProcHeapAlloc(65536 * sizeof(MAPVERTEX));
	lpmap->sidedefs = ProcHeapAlloc(65536 * sizeof(MAPSIDEDEF));
	lpmap->things = ProcHeapAlloc(65536 * sizeof(MAPTHING));
	lpmap->iLinedefs = lpmap->iSectors = lpmap->iSidedefs = lpmap->iVertices = lpmap->iThings = 0;

	return lpmap;
}


/* DestroyMapStructure
 *   Frees memory associated with a map.
 *
 * Parameters:
 *   MAP*		Pointer to map structure to free.
 *
 * Return value:
 *   None
 *
 * Notes:
 *   Frees memory pointed to by the members, as well as the struct's own memory.
 */
void DestroyMapStructure(MAP *lpmap)
{
	ProcHeapFree(lpmap->sectors);
	ProcHeapFree(lpmap->linedefs);
	ProcHeapFree(lpmap->vertices);
	ProcHeapFree(lpmap->sidedefs);
	ProcHeapFree(lpmap->things);
	ProcHeapFree(lpmap);
}


/* MapLoad
 *   Loads a map from a wad into a map structure in memory.
 *
 * Parameters:
 *   MAP*			lpmap		Structure to load map into.
 *   int			iWad		ID of wad.
 *   LPCSTR			szLumpname	Name of map marker lump.
 *
 * Return value: int
 *   Non-negative on success; negative on error.
 */
int MapLoad(MAP *lpmap, int iWad, LPCSTR szLumpname)
{
	WAD *lpwad = GetWad(iWad);
	int i, cbBuf;
	BYTE *lpbBuffer;
	long iMapLump;
	long iLinedefs, iSectors, iThings, iSidedefs, iVertices;

	/* Bad wad ID. */
	if(!lpwad) return -1;

	/* Make sure map exists. */
	if((iMapLump = GetLumpIndex(lpwad, 0, -1, szLumpname)) < 0)
		return -2;

	/* Get lump indices. */
	iLinedefs = GetLumpIndex(lpwad, iMapLump, -1, "LINEDEFS");
	iSectors = GetLumpIndex(lpwad, iMapLump, -1, "SECTORS");
	iThings = GetLumpIndex(lpwad, iMapLump, -1, "THINGS");
	iSidedefs = GetLumpIndex(lpwad, iMapLump, -1, "SIDEDEFS");
	iVertices = GetLumpIndex(lpwad, iMapLump, -1, "VERTEXES");

	/* Get sizes of each lump. Scaled to counts later. */
	lpmap->iLinedefs = GetLumpLength(lpwad, iLinedefs);
	lpmap->iSectors = GetLumpLength(lpwad, iSectors);
	lpmap->iThings = GetLumpLength(lpwad, iThings);
	lpmap->iSidedefs = GetLumpLength(lpwad, iSidedefs);
	lpmap->iVertices = GetLumpLength(lpwad, iVertices);

	/* Missing lumps? */
	if(lpmap->iLinedefs < 0 || lpmap->iSectors < 0 || lpmap->iSidedefs < 0 || lpmap->iVertices < 0 || lpmap->iThings < 0)
		return -3;

	/* Find maximum length for our buffer. */
	cbBuf = lpmap->iLinedefs;
	if(cbBuf < lpmap->iSectors)		cbBuf = lpmap->iSectors;
	if(cbBuf < lpmap->iThings)		cbBuf = lpmap->iThings;
	if(cbBuf < lpmap->iSidedefs)	cbBuf = lpmap->iSidedefs;
	if(cbBuf < lpmap->iVertices)	cbBuf = lpmap->iVertices;

	/* Allocate memory for buffer. */
	lpbBuffer = ProcHeapAlloc(cbBuf);


	/* Load linedefs. ------------------------------------------------------- */
	GetLump(lpwad, iLinedefs, lpbBuffer, lpmap->iLinedefs);

	/* Scale the byte count to record numbers. */
	lpmap->iLinedefs /= LINEDEFRECORDSIZE;

	/* We have data in our structure that doesn't come from the file. */
	ZeroMemory(lpmap->linedefs, lpmap->iLinedefs * sizeof(MAPLINEDEF));

	for(i=0; i < lpmap->iLinedefs; i++)
		CopyMemory(&lpmap->linedefs[i], lpbBuffer + i * LINEDEFRECORDSIZE, LINEDEFRECORDSIZE);


	/* Load sectors. -------------------------------------------------------- */
	GetLump(lpwad, iSectors, lpbBuffer, lpmap->iSectors);

	/* Scale the byte count to record numbers. */
	lpmap->iSectors /= SECTORRECORDSIZE;

	/* We have data in our structure that doesn't come from the file. */
	ZeroMemory(lpmap->sectors, lpmap->iSectors * sizeof(MAPSECTOR));

	for(i=0; i < lpmap->iSectors; i++)
		CopyMemory(&lpmap->sectors[i], lpbBuffer + i * SECTORRECORDSIZE, SECTORRECORDSIZE);


	/* Load things. --------------------------------------------------------- */
	GetLump(lpwad, iThings, lpbBuffer, lpmap->iThings);

	/* Scale the byte count to record numbers. */
	lpmap->iThings /= THINGRECORDSIZE;

	/* We have data in our structure that doesn't come from the file. */
	ZeroMemory(lpmap->things, lpmap->iThings * sizeof(MAPTHING));

	for(i=0; i < lpmap->iThings; i++)
		CopyMemory(&lpmap->things[i], lpbBuffer + i * THINGRECORDSIZE, THINGRECORDSIZE);


	/* Load sidedefs. ------------------------------------------------------- */
	GetLump(lpwad, iSidedefs, lpbBuffer, lpmap->iSidedefs);

	/* Scale the byte count to record numbers. */
	lpmap->iSidedefs /= SIDEDEFRECORDSIZE;

	/* We have data in our structure that doesn't come from the file. */
	ZeroMemory(lpmap->sidedefs, lpmap->iSidedefs * sizeof(MAPSIDEDEF));

	for(i=0; i < lpmap->iSidedefs; i++)
		CopyMemory(&lpmap->sidedefs[i], lpbBuffer + i * SIDEDEFRECORDSIZE, SIDEDEFRECORDSIZE);


	/* Load vertices. ------------------------------------------------------- */
	GetLump(lpwad, iVertices, lpbBuffer, lpmap->iVertices);

	/* Scale the byte count to record numbers. */
	lpmap->iVertices /= VERTEXRECORDSIZE;

	/* Copy the data. */
	for(i=0; i < lpmap->iVertices; i++)
		CopyMemory(&lpmap->vertices[i], lpbBuffer + i * VERTEXRECORDSIZE, VERTEXRECORDSIZE);

	/* Finished with our buffer now. */
	ProcHeapFree(lpbBuffer);


	/* The map's loaded now, but we still have to set some fields in the
	 * structures that we use for our own purposes.
	 */

	/* Set all (used) sidedefs' linedef references. */
	for(i = 0; i < lpmap->iLinedefs; i++)
	{
		if(lpmap->linedefs[i].s1 >= 0) lpmap->sidedefs[lpmap->linedefs[i].s1].linedef = i;
		if(lpmap->linedefs[i].s2 >= 0) lpmap->sidedefs[lpmap->linedefs[i].s2].linedef = i;
	}

	/* TODO: Validate map. As it is, invalid maps cause crashes. */

	/* Nodebuilding adds vertices that we don't want. */
	RemoveUnusedVertices(lpmap, 0);
	DeleteZeroLengthLinedefs(lpmap);

	/* Success! */
	return 0;
}



/* MapLoadForPreview
 *   Loads only linedefs and vertices, and doesn't do any cleaning.
 *
 * Parameters:
 *   MAP*			lpmap		Structure to load map into.
 *   int			iWad		ID of wad.
 *   LPCSTR			szLumpname	Name of map marker lump.
 *
 * Return value: int
 *   Non-negative on success; negative on error.
 *
 * Remarks:
 *   Quicker than MapLoad, so useful for the preview in the map selection
 *   dialogue. All the other fields will be invalid, though.
 */
int MapLoadForPreview(MAP *lpmap, int iWad, LPCSTR szLumpname)
{
	WAD *lpwad = GetWad(iWad);
	int i, cbBuf;
	BYTE *lpbBuffer;
	long iMapLump;
	long iLinedefs, iSectors, iThings, iSidedefs, iVertices;

	/* Bad wad ID. */
	if(!lpwad) return -1;

	/* Make sure map exists. */
	if((iMapLump = GetLumpIndex(lpwad, 0, -1, szLumpname)) < 0)
		return -2;

	/* Get lump indices. */
	iLinedefs = GetLumpIndex(lpwad, iMapLump, -1, "LINEDEFS");
	iSectors = GetLumpIndex(lpwad, iMapLump, -1, "SECTORS");
	iThings = GetLumpIndex(lpwad, iMapLump, -1, "THINGS");
	iSidedefs = GetLumpIndex(lpwad, iMapLump, -1, "SIDEDEFS");
	iVertices = GetLumpIndex(lpwad, iMapLump, -1, "VERTEXES");

	/* Get sizes of each lump. Scaled to counts later. */
	lpmap->iLinedefs = GetLumpLength(lpwad, iLinedefs);
	lpmap->iSectors = GetLumpLength(lpwad, iSectors);
	lpmap->iThings = GetLumpLength(lpwad, iThings);
	lpmap->iSidedefs = GetLumpLength(lpwad, iSidedefs);
	lpmap->iVertices = GetLumpLength(lpwad, iVertices);

	/* Missing lumps? */
	if(lpmap->iLinedefs < 0 || lpmap->iSectors < 0 || lpmap->iSidedefs < 0 || lpmap->iVertices < 0 || lpmap->iThings < 0)
		return -3;

	/* Find maximum length for our buffer. */
	cbBuf = lpmap->iLinedefs;
	if(cbBuf < lpmap->iVertices)	cbBuf = lpmap->iVertices;

	/* Allocate memory for buffer. */
	lpbBuffer = ProcHeapAlloc(cbBuf);


	/* Load linedefs. ------------------------------------------------------- */
	GetLump(lpwad, iLinedefs, lpbBuffer, lpmap->iLinedefs);

	/* Scale the byte count to record numbers. */
	lpmap->iLinedefs /= LINEDEFRECORDSIZE;

	for(i=0; i < lpmap->iLinedefs; i++)
		CopyMemory(&lpmap->linedefs[i], lpbBuffer + i * LINEDEFRECORDSIZE, LINEDEFRECORDSIZE);


	/* Load vertices. ------------------------------------------------------- */
	GetLump(lpwad, iVertices, lpbBuffer, lpmap->iVertices);

	/* Scale the byte count to record numbers. */
	lpmap->iVertices /= VERTEXRECORDSIZE;

	/* Copy the data. */
	for(i=0; i < lpmap->iVertices; i++)
		CopyMemory(&lpmap->vertices[i], lpbBuffer + i * VERTEXRECORDSIZE, VERTEXRECORDSIZE);



	/* Finished with our buffer now. */
	ProcHeapFree(lpbBuffer);

	/* TODO: Validate map. As it is, invalid maps cause crashes. */

	/* Success! */
	return 0;
}




/* UpdateMap
 *   Copies a map to a wad in memory.
 *
 * Parameters:
 *   MAP*		lpmap		Structure to save.
 *   int		iWad		ID of wad.
 *   LPCSTR		szLumpname	Name of map marker lump.
 *   CONFIG*	lpcfgMap	Map configuration, to determine which lumps are
 *							required.
 *
 * Return value: None.
 */
void UpdateMap(MAP *lpmap, int iWad, LPCSTR szLumpname, CONFIG *lpcfgMap)
{
	WAD *lpwad = GetWad(iWad);
	UpdateMapToWadStructure(lpmap, lpwad, szLumpname, lpcfgMap);
}


/* UpdateMapToWadStructure
 *   Copies a map to a wad in memory.
 *
 * Parameters:
 *   MAP*		lpmap		Structure to save.
 *   WAD*		lpwad		Wad structure.
 *   LPCSTR		szLumpname	Name of map marker lump.
 *   CONFIG*	lpcfgMap	Map configuration, to determine which lumps are
 *							required.
 *
 * Return value: None.
 */
void UpdateMapToWadStructure(MAP *lpmap, WAD *lpwad, LPCSTR szLumpname, CONFIG *lpcfgMap)
{
	BYTE *lpbyBuffer = NULL;
	unsigned int cbBuffer;
	int i;
	long iMapLump;
	long iSectors, iLinedefs, iThings, iVertices, iSidedefs;

	/* No map with this name yet? */
	iMapLump = GetLumpIndex(lpwad, 0, -1, szLumpname);
	if(iMapLump < 0)
	{
		/* Create the marker lump at the end of the wad. */
		iMapLump = CreateLump(lpwad, szLumpname, -1);
	}
	else
	{
		/* This map already exists. Get rid of all its lumps in preparation for
		 * recreating them. This solves the problem of maps whose lumps were in
		 * the wrong order -- wonder who could've let them get like that...?
		 */

		long iLastMapLump = iMapLump;
		long iTotalLumps = GetLumpCount(lpwad);
		CONFIG *lpcfgMapLumps = ConfigGetSubsection(lpcfgMap, "maplumpnames");
		char szLumpName[CCH_LUMPNAME + 1];

		/* Make sure the lumpname buffer is NULL-terminated. */
		szLumpName[CCH_LUMPNAME] = '\0';

		/* Find the last lump that belongs to us. */
		while(iLastMapLump < iTotalLumps - 1 &&
			GetLumpName(lpwad, iLastMapLump + 1, szLumpName) &&
			ConfigNodeExists(lpcfgMapLumps, szLumpName))
			iLastMapLump++;

		/* Delete them! */
		if(iLastMapLump > iMapLump)
			DeleteMultipleLumps(lpwad, iMapLump + 1, iLastMapLump);
	}

	/* Create all the map lumps. TODO: Read this from the config, somehow. */
	iThings = CreateLump(lpwad, "THINGS", iMapLump + 1);
	iLinedefs = CreateLump(lpwad, "LINEDEFS", iMapLump + 2);
	iSidedefs = CreateLump(lpwad, "SIDEDEFS", iMapLump + 3);
	iVertices = CreateLump(lpwad, "VERTEXES", iMapLump + 4);
	CreateLump(lpwad, "SEGS", iMapLump + 5);
	CreateLump(lpwad, "SSECTORS", iMapLump + 6);
	CreateLump(lpwad, "NODES", iMapLump + 7);
	iSectors = CreateLump(lpwad, "SECTORS", iMapLump + 8);
	CreateLump(lpwad, "REJECT", iMapLump + 9);
	CreateLump(lpwad, "BLOCKMAP", iMapLump + 10);

	/* Loop through each type of item, building the appropriate structures for
	 * the wad.
	 */


	/* Sectors. ***************************************************************/
	cbBuffer = lpmap->iSectors * SECTORRECORDSIZE;

	if(cbBuffer > 0)
	{
		lpbyBuffer = ProcHeapAlloc(cbBuffer);
		for(i = 0; i < lpmap->iSectors; i++)
			CopyMemory(lpbyBuffer + i * SECTORRECORDSIZE, &lpmap->sectors[i], SECTORRECORDSIZE);
	}

	SetLump(lpwad, iSectors, lpbyBuffer, cbBuffer);

	if(cbBuffer > 0) ProcHeapFree(lpbyBuffer);


	/* Vertices. **************************************************************/
	cbBuffer = lpmap->iVertices * VERTEXRECORDSIZE;

	if(cbBuffer > 0)
	{
		lpbyBuffer = ProcHeapAlloc(cbBuffer);
		for(i = 0; i < lpmap->iVertices; i++)
			CopyMemory(lpbyBuffer + i * VERTEXRECORDSIZE, &lpmap->vertices[i], VERTEXRECORDSIZE);
	}

	/* If the lump exists, get its index; otherwise, create it. */
	SetLump(lpwad, iVertices, lpbyBuffer, cbBuffer);

	if(cbBuffer > 0) ProcHeapFree(lpbyBuffer);


	/* Sidedefs. **************************************************************/
	cbBuffer = lpmap->iSidedefs * SIDEDEFRECORDSIZE;

	if(cbBuffer > 0)
	{
		lpbyBuffer = ProcHeapAlloc(cbBuffer);
		for(i = 0; i < lpmap->iSidedefs; i++)
			CopyMemory(lpbyBuffer + i * SIDEDEFRECORDSIZE, &lpmap->sidedefs[i], SIDEDEFRECORDSIZE);
	}

	/* If the lump exists, get its index; otherwise, create it. */
	SetLump(lpwad, iSidedefs, lpbyBuffer, cbBuffer);

	if(cbBuffer > 0) ProcHeapFree(lpbyBuffer);


	/* Linedefs. **************************************************************/
	cbBuffer = lpmap->iLinedefs * LINEDEFRECORDSIZE;

	if(cbBuffer > 0)
	{
		lpbyBuffer = ProcHeapAlloc(cbBuffer);
		for(i = 0; i < lpmap->iLinedefs; i++)
			CopyMemory(lpbyBuffer + i * LINEDEFRECORDSIZE, &lpmap->linedefs[i], LINEDEFRECORDSIZE);
	}

	/* If the lump exists, get its index; otherwise, create it. */
	SetLump(lpwad, iLinedefs, lpbyBuffer, cbBuffer);

	if(cbBuffer > 0) ProcHeapFree(lpbyBuffer);


	/* Things. ****************************************************************/
	cbBuffer = lpmap->iThings * THINGRECORDSIZE;

	if(cbBuffer > 0)
	{
		lpbyBuffer = ProcHeapAlloc(cbBuffer);
		for(i = 0; i < lpmap->iThings; i++)
			CopyMemory(lpbyBuffer + i * THINGRECORDSIZE, &lpmap->things[i], THINGRECORDSIZE);
	}

	/* If the lump exists, get its index; otherwise, create it. */
	SetLump(lpwad, iThings, lpbyBuffer, cbBuffer);

	if(cbBuffer > 0) ProcHeapFree(lpbyBuffer);
}


/* IterateMapLumpNames
 *   Calls a function with the name of each map lump in a wad.
 *
 * Parameters:
 *   int						iWad		Index of wad.
 *   void (*)(LPCTSTR, void*)	fnCallback	Callback function.
 *   void*						lpvParam	Parameter for the callback function.
 *
 * Return value: None.
 */
void IterateMapLumpNames(int iWad, void (*fnCallback)(LPCTSTR, void*), void *lpvParam)
{
	WAD *lpwad = GetWad(iWad);
	int iLumpCount = GetLumpCount(lpwad);
	int i;

	for(i = 0; i < iLumpCount; i++)
	{
		char szLumpName[CCH_LUMPNAME + 1];

		/* Get the (ANSI) lumpname and terminate it. */
		GetLumpName(lpwad, i, szLumpName);
		szLumpName[CCH_LUMPNAME] = '\0';

		/* If the lumpname makes it look like a map, add it to the list. */
		if(LikeA(szLumpName, "MAP##") || LikeA(szLumpName, "MAP[A-Z][0-9A-Z]") || LikeA(szLumpName, "E#M#"))
		{
			TCHAR szLumpNameT[CCH_LUMPNAME + 1];

			/* De-ANSIfy if necessary. */
			wsprintf(szLumpNameT, TEXT("%hs"), szLumpName);

			/* Do whatever it is we want to do with the name. */
			fnCallback(szLumpNameT, lpvParam);
		}
	}
}
