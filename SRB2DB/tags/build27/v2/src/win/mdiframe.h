#ifndef __SRB2B_MDIFRAME__
#define __SRB2B_MDIFRAME__

/* Function prototypes. */
int CreateMainWindow(int iCmdShow);
void DestroyMainWindow(void);
BOOL CALLBACK CloseEnumProc(HWND hwnd, LPARAM lParam);
BOOL OpenWadForEditing(LPCTSTR szFilename);
void StatusBarNoWindow(void);

/* Types. */
enum CHILD_IDS
{
	IDC_STATUSBAR,
	IDC_TOOLBAR,
};


/* Status bar panels. */
enum _ENUM_SBPANELS
{
	SBP_MODE = 0,
	SBP_SECTORS,
	SBP_LINEDEFS,
	SBP_SIDEDEFS,
	SBP_VERTICES,
	SBP_THINGS,
	SBP_GRID,
	SBP_ZOOM,
	SBP_COORD,
	SBP_MAPCONFIG,
	SBP_LAST
};

/* Corresponding flags. */
#define SBPF_MODE		(1 << SBP_MODE)
#define SBPF_SECTORS	(1 << SBP_SECTORS)
#define SBPF_LINEDEFS	(1 << SBP_LINEDEFS)
#define SBPF_SIDEDEFS	(1 << SBP_SIDEDEFS)
#define SBPF_VERTICES	(1 << SBP_VERTICES)
#define SBPF_THINGS		(1 << SBP_THINGS)
#define SBPF_GRID		(1 << SBP_GRID)
#define SBPF_ZOOM		(1 << SBP_ZOOM)
#define SBPF_COORD		(1 << SBP_COORD)
#define SBPF_MAPCONFIG	(1 << SBP_MAPCONFIG)


/* Globals. */
extern HWND g_hwndMain, g_hwndClient;
extern HMENU g_hmenuNodoc, g_hmenuMap;
extern HMENU g_hmenuNodocWin, g_hmenuMapWin;
extern HWND g_hwndStatusBar;

#endif
