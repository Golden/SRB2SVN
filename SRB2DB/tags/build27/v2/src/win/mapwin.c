#include <windows.h>
#include <commctrl.h>
#include <stdio.h>
#include <tchar.h>
#include <math.h>
#include <process.h>

#include "../general.h"
#include "../../res/resource.h"
#include "../maptypes.h"
#include "../map.h"
#include "../editing.h"
#include "../config.h"
#include "../openwads.h"
#include "../options.h"
#include "../keyboard.h"
#include "../renderer.h"
#include "../selection.h"
#include "../mapconfig.h"
#include "../texture.h"
#include "../undo.h"
#include "../prefab.h"
#include "../cdlgwrapper.h"
#include "../find.h"
#include "../preset.h"
#include "../wadopts.h"
#include "../testing.h"

#include "mdiframe.h"
#include "mapwin.h"
#include "infobar.h"
#include "editdlg.h"
#include "finddlg.h"
#include "gendlg.h"
#include "mapoptions.h"
#include "texbrowser.h"

#include "../CodeImp/ci_const.h"
#include "../CodeImp/ci_data_proto.h"
#include "../CodeImp/ci_map.h"
#include "../CodeImp/ci_math.h"

/* Macros. */
#define XMOUSE_WIN_TO_MAP(n, lpmwd) ((int)floor((n)/(lpmwd)->mapview.fZoom + lpmwd->mapview.xLeft + 0.5))
#define YMOUSE_WIN_TO_MAP(n, lpmwd) ((int)floor((-n)/(lpmwd)->mapview.fZoom + lpmwd->mapview.yTop + 0.5))

/* In WadAuthor-selection-mode, there's a threshold for picking between linedefs
 * and sectors. It's measured in pixels. Similarly vertices.
 * Must be <= HOVER_THRESHOLD, or funny things start happening.
 */
#define LINEDEF_THRESHOLD	16
#define VERTEX_THRESHOLD	4

/* We set a maximum distance for hovering near objects. Even if that vertex
 * half-way across the map is in fact nearest, it feels weird when it lights up.
 * Measured in pixels.
 */
#define HOVER_THRESHOLD		16


/* Indices of caching events in window data. */
#define HCACHE_FLAT			0
#define HCACHE_TEX			1


/* Initial size of selection hash buckets. */
#define INIT_SELLIST_SIZE	10

/* ID constants. */
#define IDT_ANIM_TIMER 1

/* Menu positions. */
#define EDITMENUPOS 1
#define VIEWMENUPOS 2

/* Size of buffer for undo/redo strings. */
#define UNDOCAPTIONBUFSIZE 128

#define SAVECHANGES_BUFSIZE 1024

/* Custom messages. */
#define WM_UPDATEWAD			WM_APP
#define WM_UPDATETITLE			(WM_APP+1)
#define WM_TEXPREVIEWCLICKED	(WM_APP+2)

/* Number of characters (inc. terminator) for "Untitled" string. */
#define UNTITLED_BUFLEN 32

/* Region and increment for edge-scrolling. */
#define EDGESCROLL_BORDER 32
#define EDGESCROLL_INCREMENT 1

/* Initial size for array of identical sector indices. */
#define IDENT_SEC_ARRAY_INIT_SIZE 8

/* Temporary file prefix when testing maps. */
#define TEST_TEMP_PREFIX TEXT("twd")

/* Characters in string to display number of merged sector sets. */
#define CCH_MERGECOUNT_FORMAT 128


typedef struct _HIGHLIGHTOBJ
{
	ENUM_EDITMODE	emType;
	int			iIndex;
} HIGHLIGHTOBJ;


typedef struct _MAPWINDATA
{
	HWND				hwnd;		/* Just for convenience. */
	int					iWadID, iIWadID, iAdditionalWadID;
	WAD					*lpwadIWad;	/* For efficiency. */
	MAP					*lpmap;
	char				szLumpName[9];
	ENUM_EDITMODE		editmode, prevmode;
	ENUM_EDITSUBMODE	submode;
	HDC					hdc;
	HCURSOR				hCursor;
	short				xMouseLast, yMouseLast;
	MAPVIEW				mapview;
	BOOL				bWantRedraw;
	HIGHLIGHTOBJ		highlightobj;
	HIGHLIGHTOBJ		hobjDrag;
	SELECTION			selection, selectionAux;
	CONFIG				*lpcfgMap;
	TEXCACHE			tcFlatsHdr, tcTexturesHdr;
	TEXTURENAMELIST		*lptnlTextures, *lptnlFlats;
	RGBQUAD				rgbqPalette[CB_PLAYPAL / 3];
	DRAW_OPERATION		drawop;
	BYTE				bySnapFlags, bySnapToggleMask;
	short				xDragLast, yDragLast;
	BOOL				bDeselectAfterEdit, bDeselectOnMouseUp;
	UNDOSTACK			*lpundostackUndo, *lpundostackRedo;
	BOOL				bMapChanged;
	LOOPLIST			*lplooplistRS;
	BOOL				bStitchMode;
	MAPTHING			thingLast;
	HIMAGELIST			himlCacheFlats, himlCacheTextures;
	CONFIG				*lpcfgWadOpt, *lpcfgWadOptMap;
	POINTS				ptsPopupMenuLast, ptsSelectionStart;
	RECT				rcScrollBounds;
	BYTE				byScrollKeyFlags;
	CONFIG				*lpcfgMainGame;
	int					iNextSectorSetIndex;
} MAPWINDATA;


/* The order of these corresponds to their order in the menu bar. */
typedef enum _ENUM_POPUPMENUS
{
	POPUP_LINES,
	POPUP_SECTORS,
	POPUP_VERTICES,
	POPUP_THINGS,
	POPUP_COMMON,
	POPUP_NOSEL,
	POPUP_COUNT
} ENUM_POPUPMENUS;

enum ENUM_SCROLL_KEY_FLAGS
{
	SKF_UP = 1,
	SKF_DOWN = 2,
	SKF_LEFT = 4,
	SKF_RIGHT = 8
};

#define SKF_ALL (SKF_UP | SKF_DOWN | SKF_LEFT | SKF_RIGHT)


static HMENU g_hmenuPopups;


/* Static function declarations. */
static LRESULT CALLBACK MapWndProc(HWND hwnd, UINT uiMessage, WPARAM wParam, LPARAM lParam);
static void DestroyMapWindowData(HWND hwnd);
static BOOL MapKeyDown(MAPWINDATA *lpmwd, int iKeyCode);
static BOOL MapKeyUp(MAPWINDATA *lpmwd, int iKeyCode);
static void RedrawMapWindow(MAPWINDATA *lpmwd);
static void ChangeMode(MAPWINDATA *lpmwd, USHORT unModeMenuID);
static void StatusBarMapWindow(void);
static void UpdateStatusBar(MAPWINDATA *lpmwd, DWORD dwFlags);
static __inline BOOL UpdateHighlight(MAPWINDATA *lpmwd, int xMouse, int yMouse);
static BOOL CalculateHighlight(MAPWINDATA *lpmwd, int xMouse, int yMouse);
static void GetNearestObject(MAPWINDATA *lpmwd, int xMap, int yMap, ENUM_EDITMODE mode, HIGHLIGHTOBJ *lphighlightobj);
static void ClearHighlight(MAPWINDATA *lpmwd);
static void SetHighlight(MAPWINDATA *lpmwd, HIGHLIGHTOBJ *lphightlightobj);
static LRESULT MouseMove(MAPWINDATA *lpmwd, WORD xMouse, WORD yMouse);
static LRESULT LButtonDown(MAPWINDATA *lpmwd, WORD xMouse, WORD yMouse, WORD wFlags);
static LRESULT LButtonDblClk(MAPWINDATA *lpmwd, WORD xMouse, WORD yMouse, WORD wFlags);
static LRESULT LButtonUp(MAPWINDATA *lpmwd, WORD xMouse, WORD yMouse, WORD wFlags);
static LRESULT RButtonUp(MAPWINDATA *lpmwd, WORD xMouse, WORD yMouse, WORD wFlags);
static LRESULT RButtonDown(MAPWINDATA *lpmwd, WORD xMouse, WORD yMouse, WORD wFlags);
static BOOL ObjectSelected(MAPWINDATA *lpmwd, HIGHLIGHTOBJ *lphighlightobj);
static void AddObjectToSelection(MAPWINDATA *lpmwd, HIGHLIGHTOBJ *lphighlightobj);
static BOOL RemoveObjectFromSelection(MAPWINDATA *lpmwd, HIGHLIGHTOBJ *lphighlightobj);
static void UpdateMapWindowTitle(MAPWINDATA *lpmwd);
static void BuildSectorTexturePreviews(MAPWINDATA *lpmwd, SECTORDISPLAYINFO *lpsdi, DWORD dwDisplayFlags);
static void DestroySectorTexturePreviews(SECTORDISPLAYINFO *lpsdi, DWORD dwDisplayFlags);
static void BuildThingSpritePreview(MAPWINDATA *lpmwd, THINGDISPLAYINFO *lptdi, DWORD dwDisplayFlags);
static void DestroyThingSpritePreview(THINGDISPLAYINFO *lptdi, DWORD dwDisplayFlags);
static void BuildSidedefTexturePreviews(MAPWINDATA *lpmwd, LINEDEFDISPLAYINFO *lplddi, DWORD dwDisplayFlags);
static void DestroySidedefTexturePreviews(LINEDEFDISPLAYINFO *lplddi, DWORD dwDisplayFlags);
static void MapWinUpdateInfoBar(MAPWINDATA *lpmwd);
static void ShowLinesSelectionInfo(MAPWINDATA *lpmwd);
static void ShowSectorSelectionInfo(MAPWINDATA *lpmwd);
static void ShowThingsSelectionInfo(MAPWINDATA *lpmwd);
static void ShowVerticesSelectionInfo(MAPWINDATA *lpmwd);
static __inline BOOL ShowSelectionProperties(MAPWINDATA *lpmwd, BOOL bCreateUndo);
static BOOL ShowTypedSelectionProperties(MAPWINDATA *lpmwd, BOOL bCreateUndo, ENUM_EDITMODE editmode);
static void SelectVerticesFromLines(MAPWINDATA *lpmwd);
static void SelectLinesFromVertices(MAPWINDATA *lpmwd);
static void SelectSectorsFromLines(MAPWINDATA *lpmwd);
static void DeselectAll(MAPWINDATA *lpmwd);
static void DeselectAllBruteForce(MAPWINDATA *lpmwd);
static void DeselectAllLines(MAPWINDATA *lpmwd);
static void DeselectAllSectors(MAPWINDATA *lpmwd);
static void DeselectAllThings(MAPWINDATA *lpmwd);
static void DeselectAllVertices(MAPWINDATA *lpmwd);
static void DeselectLinesFromPartialSectors(MAPWINDATA *lpmwd);
static void DoInsertOperation(MAPWINDATA *lpmwd, short xMap, short yMap);
static __inline int SelectionCount(MAPWINDATA *lpmwd);
static void CentreAuxSelectionAtPoint(MAPWINDATA *lpmwd, short xCentre, short yCentre);
static void FindAuxSelectionBoundaries(MAPWINDATA *lpmwd, short *lpxMin, short *lpxMax, short *lpyMin, short *lpyMax);
static int InsertThing(MAPWINDATA *lpmwd, unsigned short xMap, unsigned short yMap);
static BOOL CALLBACK SendUpdateWadMessage(HWND hwnd, LPARAM lParam);
static BOOL CALLBACK SendUpdateTitleMessage(HWND hwnd, LPARAM lParam);
static void EndDragOperation(MAPWINDATA *lpmwd);
static void PostDragCleanup(MAPWINDATA *lpmwd);
static void RebuildSelection(MAPWINDATA *lpmwd);
static void CreateUndo(MAPWINDATA *lpmwd, int iStringIndex);
static void ClearRedoStack(MAPWINDATA *lpmwd);
static void PerformUndo(MAPWINDATA *lpmwd);
static void PerformRedo(MAPWINDATA *lpmwd);
static void WithdrawUndo(MAPWINDATA *lpmwd);
static void WithdrawRedo(MAPWINDATA *lpmwd);
static void FlipSelectionHorizontally(MAPWINDATA *lpmwd);
static void FlipSelectionVertically(MAPWINDATA *lpmwd);
static void CreateAuxiliarySelection(MAPWINDATA *lpmwd);
static void CleanAuxiliarySelection(MAPWINDATA *lpmwd);
static void MapWindowSave(MAPWINDATA *lpmwd);
static void MapWindowSaveAs(MAPWINDATA *lpmwd);
static void SnapAuxVerticesToGrid(MAPWINDATA *lpmwd);
static void SnapSelectedThingsToGrid(MAPWINDATA *lpmwd);
static void StartPan(MAPWINDATA *lpmwd);
static void EndPan(MAPWINDATA *lpmwd);
static void DeleteSelection(MAPWINDATA *lpmwd);
static void InsertPolygonalSector(MAPWINDATA *lpmwd, short xCentre, short yCentre);
static void CancelCurrentOperation(MAPWINDATA *lpmwd);
static void MakeUndoRedoText(LPTSTR szCaption, DWORD cchCaption, UNDOSTACK *lpundostack, WORD wPrefixID);
static BOOL HighlightObjSelected(MAP *lpmap, HIGHLIGHTOBJ *lphobj);
static void RotateSelection(MAPWINDATA *lpmwd, int iAngle, char cRotType, BOOL bAdjustThingAngles, BOOL bSnap);
static void ResizeSelection(MAPWINDATA *lpmwd, unsigned short unFactor, char cOrigType, BOOL bSnap);
static void GetAuxFixedPoint(MAPWINDATA *lpmwd, char cType, short *lpx, short *lpy);
static void RotateSelectedThings(MAPWINDATA *lpmwd, int iAngle);
static void SelectAll(MAPWINDATA *lpmwd);
static void InvertSelection(MAPWINDATA *lpmwd);
static BOOL CreatePresetFromSelection(MAPWINDATA *lpmwd, PRESET *lppreset);
static BOOL ApplyPresetToSelection(MAPWINDATA *lpmwd, PRESET *lppreset);
static void EditMapProperties(MAPWINDATA *lpmwd);
static void RotateSelectedThingsToPoint(MAPWINDATA *lpmwd, short x, short y);
static void InitTextureCaches(MAPWINDATA *lpmwd);
static void FreeTextureCaches(MAPWINDATA *lpmwd);
static BOOL IsSelectionHomogeneous(MAPWINDATA *lpmwd);
static void BeginPreDrag(MAPWINDATA *lpmwd);
static void BeginInsertOperation(MAPWINDATA *lpmwd);
static BOOL UpdateLassoedSelection(MAPWINDATA *lpmwd, unsigned short xMouse, unsigned short yMouse);
static void EndSelectOperation(MAPWINDATA *lpmwd);
static BOOL EdgeScrolling(MAPWINDATA *lpmwd);
static void InitEdgeScrollBoundaries(MAPWINDATA *lpmwd, unsigned short xMouse, unsigned short yMouse);
static void EditorInsertThing(MAPWINDATA *lpmwd, short xMap, short yMap);


/* RegisterMapWindowClass
 *   Register's the map window class.
 *
 * Parameters:
 *   None
 *
 * Return value: int
 *   Zero on success; nonzero on error.
 *
 * Notes:
 *   Only needs to be called at startup.
 */
int RegisterMapWindowClass(void)
{
	WNDCLASSEX wndclassex;
	const TCHAR szClassName[] = MAPWINCLASS;

	/* Set up and register the window class. */

	wndclassex.cbClsExtra = 0;
	wndclassex.cbSize = sizeof(wndclassex);
	wndclassex.cbWndExtra = sizeof(struct MAP*);
	wndclassex.hbrBackground = (HBRUSH)GetStockObject(WHITE_BRUSH);
	wndclassex.hCursor = NULL;
	wndclassex.hIcon = LoadIcon(NULL, IDI_APPLICATION);

	/* For the small icon, ask the system what size it should be. */
	wndclassex.hIconSm = LoadImage(NULL, IDI_APPLICATION, IMAGE_ICON, GetSystemMetrics(SM_CXSMICON), GetSystemMetrics(SM_CYSMICON), LR_DEFAULTCOLOR);

	wndclassex.hInstance = g_hInstance;
	wndclassex.lpfnWndProc = MapWndProc;
	wndclassex.lpszClassName = szClassName;
	wndclassex.lpszMenuName = NULL;
	wndclassex.style = CS_VREDRAW | CS_HREDRAW | CS_OWNDC | CS_DBLCLKS;

	/* The only reason this should fail is if we try to register a Unicode class
	 * on 9x.
	 */
	if(!RegisterClassEx(&wndclassex)) return 1;
	return 0;
}


/* CreateMapWindow
 *   Creates a map editor window.
 *
 * Parameters:
 *   MAP*		lpmap			Pointer to map data for window.
 *   LPSTR		szLumpName		Name of map-lump.
 *   CONFIG*	lpcfgMap		Map configuration.
 *   CONFIG*	lpcfgWadOpts	Wad options.
 *   int		iWadID			ID of wad structure this window belongs to.
 *   int		iIWadID			ID of IWAD for the selected config.
 *
 * Return value: HWND
 *   Window handle, or NULL on error.
 */
HWND CreateMapWindow(MAP *lpmap, LPSTR szLumpName, CONFIG *lpcfgMap, CONFIG *lpcfgWadOpts, int iWadID, int iIWadID)
{
	const TCHAR szClassName[] = MAPWINCLASS;
	MAPWINDATA *lpmwd;
	long iPlayPal;

	/* Allocate storage for this window's data. */
	lpmwd = ProcHeapAlloc(sizeof(MAPWINDATA));

	lpmwd->iWadID = iWadID;
	lpmwd->iIWadID = iIWadID;
	lpmwd->iAdditionalWadID = -1;	/* TODO. */

	/* Map is unchanged initially. */
	lpmwd->bMapChanged = FALSE;

	/* We use this rather a lot, so cache it. */
	lpmwd->lpwadIWad = iIWadID >= 0 ? GetWad(iIWadID) : NULL;

	lpmwd->lpmap = lpmap;

	/* Get lumpname. Note that this is always ANSI. */
	lstrcpynA(lpmwd->szLumpName, szLumpName, 9);

	/* Store map config and options for this map config. */
	lpmwd->lpcfgMap = lpcfgMap;
	lpmwd->lpcfgMainGame = GetOptionsForGame(lpcfgMap);

	/* Store wad options and get section for this map. */
	lpmwd->lpcfgWadOpt = lpcfgWadOpts;
	lpmwd->lpcfgWadOptMap = GetMapOptionsFromWadOpt(lpcfgWadOpts, lpmwd->szLumpName);

	lpmwd->submode = ESM_NONE;
	lpmwd->editmode = EM_ANY;
	lpmwd->prevmode = EM_ANY;

	lpmwd->mapview.cxDrawSurface = lpmwd->mapview.cyDrawSurface = -1;

	/* We don't set the view position until we've created the window, since we
	 * need to know its dimensions.
	 */

	lpmwd->mapview.cxGrid = lpmwd->mapview.cyGrid = ConfigGetInteger(g_lpcfgMain, "defaultgrid");
	lpmwd->mapview.iVxSnapDist = ConfigGetInteger(g_lpcfgMain, "vertexsnapdistance");
	lpmwd->mapview.bShowGrid = ConfigGetInteger(g_lpcfgMain, "gridshow");
	lpmwd->mapview.bShow64Grid = ConfigGetInteger(g_lpcfgMain, "grid64show");
	lpmwd->mapview.bShowAxes = ConfigGetInteger(g_lpcfgMain, "drawaxes");
	lpmwd->mapview.xGridOffset = lpmwd->mapview.yGridOffset = 0;

	lpmwd->bStitchMode = ConfigGetInteger(g_lpcfgMain, "defaultstitch");

	/* No object highlighted initially. */
	lpmwd->highlightobj.emType = EM_MOVE;

	lpmwd->hCursor = LoadCursor(NULL, IDC_ARROW);

	/* Mouse is assumed to be outside window. */
	lpmwd->xMouseLast = lpmwd->yMouseLast = -1;

	/* Scrolling key flags. */
	lpmwd->byScrollKeyFlags = 0;

	/* Snapping flags. */
	lpmwd->bySnapFlags = ConfigGetInteger(g_lpcfgMain, "defaultsnap");
	lpmwd->bySnapToggleMask = 0;

	/* Set default values for last thing. */
	lpmwd->thingLast.angle = 0;
	lpmwd->thingLast.flag = ConfigGetInteger(lpcfgMap, "defaultthingflags");
	lpmwd->thingLast.thing = ConfigGetInteger(lpcfgMap, "defaultthing");

	/* For finding identical sector sets. */
	lpmwd->iNextSectorSetIndex = 0;


	/* Load palette. */
	if(lpmwd->iIWadID >= 0 &&
		(iPlayPal = GetLumpIndex(lpmwd->lpwadIWad, 0, -1, "PLAYPAL")) &&
		GetLumpLength(lpmwd->lpwadIWad, iPlayPal) >= CB_PLAYPAL)
	{
		BYTE	lpbyPlaypal[CB_PLAYPAL];
		int		i;

		/* Get the palette from the IWAD. */
		GetLump(lpmwd->lpwadIWad, iPlayPal, lpbyPlaypal, CB_PLAYPAL);

		/* Convert it to an array of RGBQUADs for the API. */
		for(i = 0; i < CB_PLAYPAL / 3; i++)
		{
			lpmwd->rgbqPalette[i].rgbRed = lpbyPlaypal[3*i];
			lpmwd->rgbqPalette[i].rgbGreen = lpbyPlaypal[3*i + 1];
			lpmwd->rgbqPalette[i].rgbBlue = lpbyPlaypal[3*i + 2];
		}
	}
	else
	{
		/* No palette? Zero it. */
		FillMemory(lpmwd->rgbqPalette, sizeof(lpmwd->rgbqPalette), 0);
	}

	/* Initialise all things' colour flags. */
	SetAllThingPropertiesFromType(lpmap, ConfigGetSubsection(lpcfgMap, FLAT_THING_SECTION));

	/* Initialise the texture/flat caches. Empty initially, but the lists of
	 * names need generated.
	 */
	InitTextureCaches(lpmwd);

	lpmwd->hwnd = CreateMDIWindow(szClassName, TEXT("Map"), 0, CW_USEDEFAULT, CW_USEDEFAULT, CW_USEDEFAULT, CW_USEDEFAULT, g_hwndClient, g_hInstance, (LPARAM)lpmwd);
	UpdateMapWindowTitle(lpmwd);

	/* Now that we've got the window, we can get its DC and prepare it for
	 * OpenGL rendering.
	 */
	lpmwd->hdc = GetDC(lpmwd->hwnd);
	if(!InitGL(lpmwd->hdc))
	{
		/* Whoops, couldn't initialise renderer. */
		DIE(IDS_ERROR_RENDERER);
	}

	/* Set the view now that we know the dimensions. */
	ResetMapView(lpmap, &lpmwd->mapview);

	/* Initialise selection structures. */
	lpmwd->selection.lpsellistLinedefs = AllocateSelectionList(INIT_SELLIST_SIZE);
	lpmwd->selection.lpsellistVertices = AllocateSelectionList(INIT_SELLIST_SIZE);
	lpmwd->selection.lpsellistSectors = AllocateSelectionList(INIT_SELLIST_SIZE);
	lpmwd->selection.lpsellistThings = AllocateSelectionList(INIT_SELLIST_SIZE);

	/* No undo structures to start with. */
	lpmwd->lpundostackUndo = NULL;
	lpmwd->lpundostackRedo = NULL;

	RendererMakeDCCurrent(lpmwd->hdc);
	lpmwd->mapview.uiThingTex = LoadThingCircleTexture();
	ReSizeGLScene(lpmwd->mapview.cxDrawSurface, lpmwd->mapview.cyDrawSurface);

	/* Need to be set up in order to do this. */
	ChangeMode(lpmwd, IDM_VIEW_MODE_ANY);	/* Sets menus etc. */

	return lpmwd->hwnd;
}



/* MapWndProc
 *   Map editor child window procedure. Called by the message loop.
 *
 * Parameters:
 *   HWND		hwnd		Window handle for message.
 *   UINT		uiMessage	ID for the message.
 *   WPARAM		wParam		Message-specific.
 *   LPARAM		lParam		Message-specific.
 *
 * Return value: LRESULT
 *   Message-specific.
 */

static LRESULT CALLBACK MapWndProc(HWND hwnd, UINT uiMessage, WPARAM wParam, LPARAM lParam)
{
	MAPWINDATA *lpmwd;

	switch(uiMessage)
	{
	case WM_CREATE:
		{
			/* Retrieve the window-specific structure, and store it. */
			lpmwd = (MAPWINDATA*)((MDICREATESTRUCT*)((CREATESTRUCT*)lParam)->lpCreateParams)->lParam;
			SetWindowLong(hwnd, GWL_USERDATA, (LONG)lpmwd);

			/* Create an animation timer for this window. */
			SetTimer(hwnd, IDT_ANIM_TIMER, 10, NULL);
		}

		return 0;

	case WM_MDIACTIVATE:
		/* Are we now active? */
		if(lParam == (LPARAM)hwnd)
		{
			lpmwd = (MAPWINDATA*)GetWindowLong(hwnd, GWL_USERDATA);

			/* Set our menu and status bar.. */
			SendMessage(g_hwndClient, WM_MDISETMENU, (WPARAM)g_hmenuMap, (LPARAM)g_hmenuMapWin);
			StatusBarMapWindow();
			UpdateStatusBar(lpmwd, 0xFFFFFFFF);		/* Update all cells. */
		}
		else	/* We're not active any more. */
		{
			/* Set the 'no active window' menu and status bar. */
			SendMessage(g_hwndClient, WM_MDISETMENU, (WPARAM)g_hmenuNodoc, (LPARAM)g_hmenuNodocWin);
			StatusBarNoWindow();
		}

		DrawMenuBar(g_hwndMain);

		return 0;

	case WM_KILLFOCUS:
		/* Don't keep scrolling if we lose the focus while holding a key. */
		lpmwd = (MAPWINDATA*)GetWindowLong(hwnd, GWL_USERDATA);
		lpmwd->byScrollKeyFlags = 0;
		return 0;

	case WM_UPDATEWAD:
		/* Update the wad structure with the changes made to the map. Necessary
		 * immediately prior to saving, and may be necessary if another map
		 * window triggers a Save As, which is why it's done in response to a
		 * window message.
		 */
		lpmwd = (MAPWINDATA*)GetWindowLong(hwnd, GWL_USERDATA);
		UpdateMap(lpmwd->lpmap, lpmwd->iWadID, lpmwd->szLumpName, lpmwd->lpcfgMap);
		return 0;

	case WM_UPDATETITLE:
		/* Update the windows title with lumpname and filename. Necessary
		 * immediately after a Save As, and is necessary when another map window
		 * triggers such, which is why it's done in response to a window
		 * message.
		 */
		lpmwd = (MAPWINDATA*)GetWindowLong(hwnd, GWL_USERDATA);
		UpdateMapWindowTitle(lpmwd);
		return 0;

	case WM_TEXPREVIEWCLICKED:
		{
			ENUM_EDITMODE emType = EM_ANY;
			SECTORDISPLAYINFO sdi;
			LINEDEFDISPLAYINFO lddi;
			LPTSTR szTexName = NULL;
			DWORD dwPropertyFlags = 0;

			lpmwd = (MAPWINDATA*)GetWindowLong(hwnd, GWL_USERDATA);

			switch(wParam)
			{
			case TP_FRONTUPPER:
				emType = EM_LINES;
				szTexName = lddi.szFrontUpper;
				dwPropertyFlags = LDDIF_FRONTUPPER;
				break;

			case TP_FRONTMIDDLE:
				emType = EM_LINES;
				szTexName = lddi.szFrontMiddle;
				dwPropertyFlags = LDDIF_FRONTMIDDLE;
				break;

			case TP_FRONTLOWER:
				emType = EM_LINES;
				szTexName = lddi.szFrontLower;
				dwPropertyFlags = LDDIF_FRONTLOWER;
				break;

			case TP_BACKUPPER:
				emType = EM_LINES;
				szTexName = lddi.szBackUpper;
				dwPropertyFlags = LDDIF_BACKUPPER;
				break;

			case TP_BACKMIDDLE:
				emType = EM_LINES;
				szTexName = lddi.szBackMiddle;
				dwPropertyFlags = LDDIF_BACKMIDDLE;
				break;

			case TP_BACKLOWER:
				emType = EM_LINES;
				szTexName = lddi.szBackLower;
				dwPropertyFlags = LDDIF_BACKLOWER;
				break;

			case TP_CEILING:
				emType = EM_SECTORS;
				szTexName = sdi.szCeiling;
				dwPropertyFlags = SDIF_CEILINGTEX;
				break;

			case TP_FLOOR:
				emType = EM_SECTORS;
				szTexName = sdi.szFloor;
				dwPropertyFlags = SDIF_FLOORTEX;
				break;

			case TP_SPRITE:
				emType = EM_THINGS;
				break;
			}

			switch(emType)
			{
			case EM_SECTORS:
				if(lpmwd->selection.lpsellistSectors->iDataCount > 0)
				{
					DWORD dwCheckFlags;

					/* Find info common to all selected sectors. */
					dwCheckFlags = CheckSectors(lpmwd->lpmap, lpmwd->selection.lpsellistSectors, ConfigGetSubsection(lpmwd->lpcfgMap, "sectortypes"), &sdi);

					/* If all the textures didn't match, we don't want to select
					 * anything initially in the browser.
					 */
					if(!(dwCheckFlags & dwPropertyFlags))
						*szTexName = TEXT('\0');

					if(SelectTexture(g_hwndMain, lpmwd->hwnd, TF_FLAT, &lpmwd->himlCacheFlats, lpmwd->lptnlFlats, NULL, szTexName))
					{
						CreateUndo(lpmwd, IDS_UNDO_FLAT);
						ClearRedoStack(lpmwd);
						lpmwd->bMapChanged = TRUE;

						ApplySectorPropertiesToSelection(lpmwd->lpmap, lpmwd->selection.lpsellistSectors, &sdi, dwPropertyFlags);
					}
				}
				else MessageBeep(MB_OK);

				break;

			case EM_LINES:
				if(lpmwd->selection.lpsellistLinedefs->iDataCount > 0)
				{
					DWORD dwCheckFlags;

					/* Find info common to all selected lines. */
					dwCheckFlags = CheckLines(lpmwd->lpmap, lpmwd->selection.lpsellistLinedefs, ConfigGetSubsection(lpmwd->lpcfgMap, "__ldtypesflat"), &lddi);

					/* If all the textures didn't match, we don't want to select
					 * anything initially in the browser.
					 */
					if(!(dwCheckFlags & dwPropertyFlags))
						*szTexName = TEXT('\0');

					if(SelectTexture(g_hwndMain, lpmwd->hwnd, TF_TEXTURE, &lpmwd->himlCacheTextures, lpmwd->lptnlTextures, NULL, szTexName))
					{
						CreateUndo(lpmwd, IDS_UNDO_TEXTURE);
						ClearRedoStack(lpmwd);
						lpmwd->bMapChanged = TRUE;

						ApplyLinePropertiesToSelection(lpmwd->lpmap, lpmwd->selection.lpsellistLinedefs, &lddi, dwPropertyFlags);
					}
				}
				else MessageBeep(MB_OK);

				break;

			case EM_THINGS:
				/* This does all the work for us: undos and everything. */
				if(lpmwd->selection.lpsellistThings->iDataCount > 0)
					ShowTypedSelectionProperties(lpmwd, TRUE, EM_THINGS);
				else MessageBeep(MB_OK);

				break;

			default:
				break;
			}

			MapWinUpdateInfoBar(lpmwd);
		}

		return 0;


	case WM_INITMENUPOPUP:
		/* Check items as necessary before the menu's displayed. */

		/* Make sure it's not the system menu. */
		if(!HIWORD(lParam))
		{
			lpmwd = (MAPWINDATA*)GetWindowLong(hwnd, GWL_USERDATA);

			/* Switch on menu position. We subtract one when maximised due to
			 * the MDI system menu. TODO: Replace this with an ID check. We can
			 * assign IDs to pop-up menus at runtime, which'll allow us to
			 * process sub-menus, e.g. gradients.
			 */
			switch(LOWORD(lParam) - (IsZoomed(lpmwd->hwnd) ? 1 : 0))
			{
			case EDITMENUPOS:
				{
					MENUITEMINFO mii;
					TCHAR szCaption[UNDOCAPTIONBUFSIZE];
					/* Check some options as appropriate. */
					CheckMenuItem((HMENU)wParam, IDM_EDIT_SNAPTOGRID, lpmwd->bySnapFlags & SF_RECTANGLE ? MF_CHECKED : MF_UNCHECKED);
					CheckMenuItem((HMENU)wParam, IDM_EDIT_SNAPTOVERTICES, lpmwd->bySnapFlags & SF_VERTICES ? MF_CHECKED : MF_UNCHECKED);
					CheckMenuItem((HMENU)wParam, IDM_EDIT_AUTOSTITCH, lpmwd->bStitchMode & SF_RECTANGLE ? MF_CHECKED : MF_UNCHECKED);

					/* Set undo/redo strings. */

					mii.cbSize = sizeof(mii);
					mii.fMask = MIIM_TYPE;
					mii.fType = MFT_STRING;
					mii.dwTypeData = szCaption;

					/* Concatenate undo prefix and description, and set string. */
					MakeUndoText(hwnd, szCaption, sizeof(szCaption) / sizeof(TCHAR));
					SetMenuItemInfo((HMENU)wParam, IDM_EDIT_UNDO, FALSE, &mii);

					/* Concatenate redo prefix and description, and set string. */
					MakeRedoText(hwnd, szCaption, sizeof(szCaption) / sizeof(TCHAR));
					SetMenuItemInfo((HMENU)wParam, IDM_EDIT_REDO, FALSE, &mii);


					/* Enable/disable undo and redo. */
					EnableMenuItem((HMENU)wParam, IDM_EDIT_UNDO, lpmwd->lpundostackUndo ? MF_ENABLED : MF_GRAYED);
					EnableMenuItem((HMENU)wParam, IDM_EDIT_REDO, lpmwd->lpundostackRedo ? MF_ENABLED : MF_GRAYED);
				}

				return 0;
			}
		}

		/* Didn't do anything. Fall through. */
		break;

	/* Menu commands. */
	case WM_COMMAND:

		lpmwd = (MAPWINDATA*)GetWindowLong(hwnd, GWL_USERDATA);

		switch(LOWORD(wParam))
		{
		/* File menu. *********************************************************/

		/* Save. */
		case IDM_FILE_SAVE:
			CancelCurrentOperation(lpmwd);
			MapWindowSave(lpmwd);
			return 0;

		/* Save As. */
		case IDM_FILE_SAVEAS:
			CancelCurrentOperation(lpmwd);
			MapWindowSaveAs(lpmwd);
			return 0;

		/* Test Map. */
		case IDM_FILE_TEST:
		case ID_QUICKTEST:
			/* Make sure there's not already a test in progress. */
			if(!TestInProgress())
			{
				/* This displays the dialogue and applies the settings in the
				 * config structure, if OKed. If we're quick-testing, don't
				 * bother with the dialogue.
				 */
				if(LOWORD(wParam) == ID_QUICKTEST ||
					TestDlg(g_hwndMain, GetTestingOptions(lpmwd->lpcfgMainGame)))
				{
					/* The structure will be freed when testing finishes. */
					int cchBinary;

					/* Make sure there's a binary set. */
					cchBinary = ConfigGetStringLength(lpmwd->lpcfgMainGame, "binary") + 1;
					if(cchBinary > 1)
					{
						TESTTHREADDATA *lpttdata = ProcHeapAlloc(sizeof(TESTTHREADDATA));
						CONFIG *lpcfgTest = GetTestingOptions(lpmwd->lpcfgMainGame);
						LPTSTR szBinary = ProcHeapAlloc(cchBinary * sizeof(TCHAR));
						LPTSTR szAddWadFile = NULL;
						WAD *lpwadTemp;

						/* Get the temporary file directory. */
						UINT cchTempPath = GetTempPath(0, NULL);
						LPTSTR szTempPath = ProcHeapAlloc((cchTempPath + 1) * sizeof(TCHAR));
						GetTempPath(cchTempPath, szTempPath);

						/* Create a temporary file. */
						GetTempFileName(szTempPath, TEST_TEMP_PREFIX, 0, lpttdata->szFilename);

						ProcHeapFree(szTempPath);

						/* Save a copy of the wad to this temporary file. This
						 * doesn't interfere with the real wad, including the
						 * metadata we store.
						 */
						lpwadTemp = DuplicateWadToMemory(GetWad(lpmwd->iWadID));
						UpdateMapToWadStructure(lpmwd->lpmap, lpwadTemp, lpmwd->szLumpName, lpmwd->lpcfgMap);
						WriteWad(lpwadTemp, lpttdata->szFilename);
						FreeWad(lpwadTemp);

						/* Get the name of the binary. */
						ConfigGetString(lpmwd->lpcfgMainGame, "binary", szBinary, cchBinary);

						/* If we're using an external wad file, get its name. */
						if(lpmwd->iAdditionalWadID >= 0)
						{
							int cchAddWadFile = GetWadFilename(lpmwd->iAdditionalWadID, NULL, 0);
							szAddWadFile = ProcHeapAlloc(cchAddWadFile * sizeof(TCHAR));
							GetWadFilename(lpmwd->iAdditionalWadID, szAddWadFile, cchAddWadFile);
						}

						/* Start testing. */
						lpttdata->hProcess = TestMapWithSRB2(lpttdata->szFilename, lpmwd->szLumpName, szBinary, szAddWadFile, lpcfgTest, lpmwd->lpcfgWadOptMap);

						ProcHeapFree(szBinary);
						if(szAddWadFile) ProcHeapFree(szAddWadFile);

						/* Did we succeed? */
						if(lpttdata->hProcess)
						{
							/* Create a new thread that sits and waits for
							 * testing to finish, and then tidies up.
							 */
							_beginthread(TestThreadProc, 0, lpttdata);
						}
						else MessageBoxFromStringTable(g_hwndMain, IDS_ERROR_TEST, MB_ICONERROR);
					}
					else MessageBoxFromStringTable(g_hwndMain, IDS_NOBINARY, MB_ICONEXCLAMATION);
				}
			}
			else MessageBoxFromStringTable(g_hwndMain, IDS_TESTINPROGRESS, MB_ICONEXCLAMATION);

			return 0;


		/* Edit menu. *********************************************************/

		/* Copy. */
		case IDM_EDIT_CUT:
			if(lpmwd->submode == ESM_NONE && SelectionCount(lpmwd) > 0)
			{
				PREFAB *lpprefab;

				lpprefab = CreatePrefabFromSelection(lpmwd->lpmap);
				if(CopyPrefabToClipboard(lpprefab) != 0)
					MessageBoxFromStringTable(g_hwndMain, IDS_ERROR_CLIPBOARD, MB_ICONEXCLAMATION);

				FreePrefab(lpprefab);

				CreateUndo(lpmwd, IDS_UNDO_CUT);
				ClearRedoStack(lpmwd);
				DeleteSelection(lpmwd);

				lpmwd->bMapChanged = TRUE;
				lpmwd->bWantRedraw = TRUE;
				MapWinUpdateInfoBar(lpmwd);
				UpdateStatusBar(lpmwd, SBPF_LINEDEFS | SBPF_SIDEDEFS | SBPF_VERTICES | SBPF_SECTORS | SBPF_THINGS);
			}
			else MessageBeep(MB_OK);

			return 0;

		/* Copy. */
		case IDM_EDIT_COPY:
			if(lpmwd->submode == ESM_NONE && SelectionCount(lpmwd) > 0)
			{
				PREFAB *lpprefab;

				lpprefab = CreatePrefabFromSelection(lpmwd->lpmap);
				if(CopyPrefabToClipboard(lpprefab) != 0)
					MessageBoxFromStringTable(g_hwndMain, IDS_ERROR_CLIPBOARD, MB_ICONEXCLAMATION);

				FreePrefab(lpprefab);
			}
			else MessageBeep(MB_OK);

			return 0;

		/* Paste. */
		case IDM_EDIT_PASTE:
			/* If the user presses the accelerator, he circumvents the disabling
			 * of menu items and toolbar buttons, so we need to check the
			 * clipboard again.
			 */
			if(ClipboardContainsPrefab())
			{
				PREFAB *lpprefab;

				CancelCurrentOperation(lpmwd);
				DeselectAll(lpmwd);

				if((lpprefab = PastePrefabFromClipboard()))
				{
					POINT ptMouse;
					GetCursorPos(&ptMouse);
					ScreenToClient(lpmwd->hwnd, &ptMouse);

					/* Remember where we started out. Can't use MouseLast
					 * because of menus.
					 */
					InitEdgeScrollBoundaries(lpmwd, (unsigned short)ptMouse.x, (unsigned short)ptMouse.y);


					/* Don't clear the redo stack yet. */
					CreateUndo(lpmwd, IDS_UNDO_PASTE);

					InsertPrefab(lpmwd->lpmap, lpprefab);
					RebuildSelection(lpmwd);
					CreateAuxiliarySelection(lpmwd);

					/* Find loops. We don't do the extra check of any linedefs we
					 * loop, though, since we don't really exist yet and so aren't
					 * affecting any other lines.
					 */
					lpmwd->lplooplistRS = LabelRSLinesLoops(lpmwd->lpmap);

					/* TODO: What if the mouse is outside the map? */
					lpmwd->xDragLast = XMOUSE_WIN_TO_MAP(lpmwd->xMouseLast, lpmwd);
					lpmwd->yDragLast = YMOUSE_WIN_TO_MAP(lpmwd->yMouseLast, lpmwd);

					/* Centre the prefab on the mouse position. */
					CentreAuxSelectionAtPoint(lpmwd, lpmwd->xDragLast, lpmwd->yDragLast);

					lpmwd->submode = ESM_PASTING;
					lpmwd->bWantRedraw = TRUE;

					FreePrefab(lpprefab);
				}
				else
				{
					/* Paste failed. */
					MessageBoxFromStringTable(g_hwndMain, IDS_ERROR_CLIPBOARD, MB_ICONEXCLAMATION);
				}
			}

			return 0;

		/* Delete. */
		case IDM_EDIT_DELETE:

			if(lpmwd->submode != ESM_NONE)
				MessageBeep(MB_OK);
			else
			{
				CreateUndo(lpmwd, IDS_UNDO_DELETE);
				ClearRedoStack(lpmwd);
				DeleteSelection(lpmwd);

				lpmwd->bMapChanged = TRUE;
				lpmwd->bWantRedraw = TRUE;
				MapWinUpdateInfoBar(lpmwd);
				UpdateStatusBar(lpmwd, SBPF_LINEDEFS | SBPF_SIDEDEFS | SBPF_VERTICES | SBPF_SECTORS | SBPF_THINGS);
			}

			return 0;

		/* Undo. */
		case IDM_EDIT_UNDO:

			if(lpmwd->submode != ESM_NONE)
				CancelCurrentOperation(lpmwd);
			/* Make sure we've got something to undo. */
			else if(lpmwd->lpundostackUndo)
			{
				PerformUndo(lpmwd);
				lpmwd->bWantRedraw = TRUE;

				/* The selection is now garbage, both because of nonsense in the
				 * stored selection fields and that numbering might have
				 * changed. Same goes for highlighting.
				 */
				DeselectAllBruteForce(lpmwd);
				ClearHighlight(lpmwd);

				/* Update highlight and info bar. */
				UpdateHighlight(lpmwd, lpmwd->xMouseLast, lpmwd->yMouseLast);

				/* Update all cells. */
				UpdateStatusBar(lpmwd, 0xFFFFFFFF);
			}

			return 0;

		/* Redo. */
		case IDM_EDIT_REDO:

			if(lpmwd->submode != ESM_NONE)
				CancelCurrentOperation(lpmwd);

			/* In contrast to undoing, it makes sense to try redoing even if we
			 * were in an operation.
			 */

			/* Make sure we've got something to redo. */
			if(lpmwd->lpundostackRedo)
			{
				PerformRedo(lpmwd);
				lpmwd->bWantRedraw = TRUE;

				/* The selection is now garbage, both because of nonsense in the
				 * stored selection fields and that numbering might have
				 * changed. Same goes for highlighting.
				 */
				DeselectAllBruteForce(lpmwd);
				ClearHighlight(lpmwd);

				/* Update highlight and info bar. */
				UpdateHighlight(lpmwd, lpmwd->xMouseLast, lpmwd->yMouseLast);

				/* Update all cells. */
				UpdateStatusBar(lpmwd, 0xFFFFFFFF);
			}

			return 0;

		/* Copy selection properties. */
		case IDM_EDIT_COPYSELECTIONPROPERTIES:
			{
				PRESET preset;

				if(lpmwd->submode == ESM_NONE &&
					(lpmwd->selection.lpsellistLinedefs->iDataCount > 0 ||
					lpmwd->selection.lpsellistSectors->iDataCount > 0 ||
					lpmwd->selection.lpsellistThings->iDataCount > 0))
				{
					if(CreatePresetFromSelection(lpmwd, &preset))
						CopyPresetToClipboard(&preset);
				}
				else
					MessageBeep(MB_OK);
			}

			return 0;

		/* Paste selection properties. */
		case IDM_EDIT_PASTESELECTIONPROPERTIES:
			{
				if(lpmwd->submode == ESM_NONE &&
					ClipboardContainsPreset() &&
					(lpmwd->selection.lpsellistLinedefs->iDataCount > 0 ||
					lpmwd->selection.lpsellistSectors->iDataCount > 0 ||
					lpmwd->selection.lpsellistThings->iDataCount > 0))
				{
					PRESET *lppreset;
					if((lppreset = PastePresetFromClipboard()))
					{
						/* Don't clear redo stack yet -- we might cancel. */
						CreateUndo(lpmwd, IDS_UNDO_PASTESELPROP);

						if(ApplyPresetToSelection(lpmwd, lppreset))
						{
							/* Made changes. */
							ClearRedoStack(lpmwd);

							lpmwd->bMapChanged = TRUE;
							/* Adjacent heights might have changed, so redraw. */
							lpmwd->bWantRedraw = TRUE;

							MapWinUpdateInfoBar(lpmwd);
							UpdateStatusBar(lpmwd, SBPF_LINEDEFS | SBPF_SIDEDEFS | SBPF_VERTICES | SBPF_SECTORS | SBPF_THINGS);
						}
						else WithdrawUndo(lpmwd);

						ProcHeapFree(lppreset);
					}
				}
				else
					MessageBeep(MB_OK);
			}

			return 0;

		/* Snap to grid. */
		case IDM_EDIT_SNAPTOGRID:

			lpmwd->bySnapFlags ^= SF_RECTANGLE;

			/* If we're drawing or dragging, this could alter the position of
			 * the object under the mouse. Redraw. TODO: This doesn't actually
			 * achieve anything...
			 */
			lpmwd->bWantRedraw = TRUE;

			return 0;

		/* Snap to vertices. */
		case IDM_EDIT_SNAPTOVERTICES:

			lpmwd->bySnapFlags ^= SF_VERTICES;

			/* If we're drawing or dragging, this could alter the position of
			 * the object under the mouse. Redraw. TODO: This doesn't actually
			 * achieve anything...
			 */
			lpmwd->bWantRedraw = TRUE;

			return 0;

		/* Autostitch. */
		case IDM_EDIT_AUTOSTITCH:

			lpmwd->bStitchMode = !lpmwd->bStitchMode;

			/* TODO: If you start visibly snapping to vertices while drawing,
			 * set the redraw flag.
			 */

			return 0;

		/* Flip horizontally. */
		case IDM_EDIT_TRANSFORMSELECTION_FLIPHORIZONTALLY:

			/* If we're pasting or dragging, we already have an aux. selection.
			 */
			if(lpmwd->submode != ESM_PASTING && lpmwd->submode != ESM_DRAGGING)
			{
				CancelCurrentOperation(lpmwd);
				CreateAuxiliarySelection(lpmwd);
			}

			if(lpmwd->selectionAux.lpsellistVertices->iDataCount + lpmwd->selectionAux.lpsellistThings->iDataCount >= 1)
			{
				if(lpmwd->submode != ESM_PASTING && lpmwd->submode != ESM_DRAGGING)
				{
					CreateUndo(lpmwd, IDS_UNDO_FLIPHORIZ);
					ClearRedoStack(lpmwd);
				}

				FlipSelectionHorizontally(lpmwd);

				lpmwd->bWantRedraw = TRUE;
			}
			else MessageBeep(MB_OK);

			if(lpmwd->submode != ESM_PASTING && lpmwd->submode != ESM_DRAGGING)
			{
				lpmwd->bMapChanged = TRUE;
				CleanAuxiliarySelection(lpmwd);
			}

			return 0;

		/* Flip vertically. */
		case IDM_EDIT_TRANSFORMSELECTION_FLIPVERTICALLY:

			/* If we're pasting or dragging, we already have an aux. selection.
			 */
			if(lpmwd->submode != ESM_PASTING && lpmwd->submode != ESM_DRAGGING)
			{
				CancelCurrentOperation(lpmwd);
				CreateAuxiliarySelection(lpmwd);
			}

			if(lpmwd->selectionAux.lpsellistVertices->iDataCount + lpmwd->selectionAux.lpsellistThings->iDataCount >= 1)
			{
				if(lpmwd->submode != ESM_PASTING && lpmwd->submode != ESM_DRAGGING)
				{
					CreateUndo(lpmwd, IDS_UNDO_FLIPVERT);
					ClearRedoStack(lpmwd);
				}

				FlipSelectionVertically(lpmwd);

				lpmwd->bWantRedraw = TRUE;
			}
			else MessageBeep(MB_OK);

			if(lpmwd->submode != ESM_PASTING && lpmwd->submode != ESM_DRAGGING)
			{
				lpmwd->bMapChanged = TRUE;
				CleanAuxiliarySelection(lpmwd);
			}

			return 0;


		case IDM_EDIT_SELECT_ALL:
			CancelCurrentOperation(lpmwd);
			SelectAll(lpmwd);
			lpmwd->bWantRedraw = TRUE;
			return 0;

		case IDM_EDIT_SELECT_NONE:
			CancelCurrentOperation(lpmwd);
			DeselectAll(lpmwd);
			lpmwd->bWantRedraw = TRUE;
			return 0;

		case IDM_EDIT_SELECT_INVERTSELECTION:
			CancelCurrentOperation(lpmwd);
			InvertSelection(lpmwd);
			lpmwd->bWantRedraw = TRUE;
			return 0;


		case IDM_EDIT_FIND:

			CancelCurrentOperation(lpmwd);
			ShowFindDlg(hwnd, ConfigGetInteger(lpmwd->lpcfgMap, "seceffectnybble"));
			return 0;

		case IDM_EDIT_REPLACE:

			CancelCurrentOperation(lpmwd);

			/* Make an undo, but don't clear the redo stack yet. */
			CreateUndo(lpmwd, IDS_UNDO_REPLACE);

			if(ShowReplaceDlg(hwnd, ConfigGetInteger(lpmwd->lpcfgMap, "seceffectnybble")))
			{
				/* If we didn't cancel, we keep the undo and set the changed flag. */
				ClearRedoStack(lpmwd);
				lpmwd->bMapChanged = TRUE;
			}
			/* If we cancelled, get rid of that undo. */
			else WithdrawUndo(lpmwd);

			return 0;


		/* Rotate. */
		case IDM_EDIT_TRANSFORMSELECTION_ROTATE:
			{
				/* If we're pasting or dragging, we already have an aux. selection.
				 */
				if(lpmwd->submode != ESM_PASTING && lpmwd->submode != ESM_DRAGGING)
				{
					CancelCurrentOperation(lpmwd);
					CreateUndo(lpmwd, IDS_UNDO_ROTATE);

					CreateAuxiliarySelection(lpmwd);
				}

				if(lpmwd->selectionAux.lpsellistVertices->iDataCount + lpmwd->selectionAux.lpsellistThings->iDataCount >= 1)
				{
					ROTATEDLGDATA rotdd;

					/* Set initial parameters for the dialogue. */
					rotdd.bThingsSelected = (lpmwd->selectionAux.lpsellistThings->iDataCount > 0);
					rotdd.bVerticesSelected = (lpmwd->selectionAux.lpsellistVertices->iDataCount > 0);

					/* Show dialogue. */
					if(RotateDlg(g_hwndMain, &rotdd))
					{
						/* If the user didn't cancel... */

						RotateSelection(lpmwd, rotdd.iAngle, rotdd.cCentreType, rotdd.bAlterThingAngles, rotdd.bSnap);
						lpmwd->bWantRedraw = TRUE;

						if(lpmwd->submode != ESM_PASTING && lpmwd->submode != ESM_DRAGGING)
						{
							lpmwd->bMapChanged = TRUE;
							ClearRedoStack(lpmwd);
							UpdateStatusBar(lpmwd, SBPF_LINEDEFS | SBPF_SIDEDEFS | SBPF_VERTICES | SBPF_SECTORS);
							RebuildSelection(lpmwd);
						}
					}
					else WithdrawUndo(lpmwd);
				}
				else MessageBeep(MB_OK);

				if(lpmwd->submode != ESM_PASTING && lpmwd->submode != ESM_DRAGGING)
					CleanAuxiliarySelection(lpmwd);
			}

			return 0;

		/* Resize. */
		case IDM_EDIT_TRANSFORMSELECTION_RESIZE:
			{
				/* If we're pasting or dragging, we already have an aux. selection.
				 */
				if(lpmwd->submode != ESM_PASTING && lpmwd->submode != ESM_DRAGGING)
				{
					CancelCurrentOperation(lpmwd);
					CreateUndo(lpmwd, IDS_UNDO_RESIZE);

					CreateAuxiliarySelection(lpmwd);
				}

				if(lpmwd->selectionAux.lpsellistVertices->iDataCount + lpmwd->selectionAux.lpsellistThings->iDataCount >= 1)
				{
					RESIZEDLGDATA rszdd;

					/* Set initial parameters for the dialogue. */
					rszdd.bThingsSelected = (lpmwd->selectionAux.lpsellistThings->iDataCount > 0);
					rszdd.bVerticesSelected = (lpmwd->selectionAux.lpsellistVertices->iDataCount > 0);

					/* Show dialogue. */
					if(ResizeDlg(g_hwndMain, &rszdd))
					{
						/* If the user didn't cancel... */

						ResizeSelection(lpmwd, rszdd.unFactor, rszdd.cCentreType, rszdd.bSnap);
						lpmwd->bWantRedraw = TRUE;

						if(lpmwd->submode != ESM_PASTING && lpmwd->submode != ESM_DRAGGING)
						{
							lpmwd->bMapChanged = TRUE;
							ClearRedoStack(lpmwd);
							UpdateStatusBar(lpmwd, SBPF_LINEDEFS | SBPF_SIDEDEFS | SBPF_VERTICES | SBPF_SECTORS);
							RebuildSelection(lpmwd);
						}
					}
					else WithdrawUndo(lpmwd);
				}
				else MessageBeep(MB_OK);

				if(lpmwd->submode != ESM_PASTING && lpmwd->submode != ESM_DRAGGING)
					CleanAuxiliarySelection(lpmwd);
			}

			return 0;


		case IDM_EDIT_TRANSFORM_SNAP:

			if(lpmwd->submode == ESM_NONE && SelectionCount(lpmwd) > 0)
			{
				CreateUndo(lpmwd, IDS_UNDO_SNAP);
				ClearRedoStack(lpmwd);
				lpmwd->bMapChanged = TRUE;

				/* Vertices first. Select them from lines. */
				CreateAuxiliarySelection(lpmwd);
				SnapAuxVerticesToGrid(lpmwd);
				CleanAuxiliarySelection(lpmwd);

				/* Now things. */
				SnapSelectedThingsToGrid(lpmwd);

				lpmwd->bWantRedraw = TRUE;
				MapWinUpdateInfoBar(lpmwd);
				UpdateStatusBar(lpmwd, SBPF_LINEDEFS | SBPF_SIDEDEFS | SBPF_VERTICES);
			}
			else MessageBeep(MB_OK);

			return 0;


		case IDM_EDIT_MAPOPTIONS:
			EditMapProperties(lpmwd);
			return 0;


		/* View menu. *********************************************************/

		/* Mode change (other than 3D). */
		case IDM_VIEW_MODE_MOVE:
		case IDM_VIEW_MODE_ANY:
		case IDM_VIEW_MODE_LINES:
		case IDM_VIEW_MODE_SECTORS:
		case IDM_VIEW_MODE_VERTICES:
		case IDM_VIEW_MODE_THINGS:
			CancelCurrentOperation(lpmwd);
			ChangeMode(lpmwd, LOWORD(wParam));
			UpdateStatusBar(lpmwd, SBPF_MODE);
			lpmwd->bWantRedraw = TRUE;
			return 0;

		case IDM_VIEW_3D:
			return 0;

		case IDM_VIEW_CENTRE:
			ResetMapView(lpmwd->lpmap, &lpmwd->mapview);
			lpmwd->bWantRedraw = TRUE;
			return 0;

		case IDM_VIEW_GRID_SETUP:
			GridProperties(lpmwd->hwnd);
			return 0;

		case IDM_VIEW_GRID_INCREASE:
			/* Ensure both to maintain ratio. */
			if(lpmwd->mapview.cxGrid < 32768 && lpmwd->mapview.cyGrid < 32768)
			{
				lpmwd->mapview.cxGrid <<= 1;
				lpmwd->mapview.cyGrid <<= 1;
			}

			lpmwd->bWantRedraw = TRUE;
			UpdateStatusBar(lpmwd, SBPF_GRID);

			return 0;

		case IDM_VIEW_GRID_DECREASE:
			/* Ensure both to maintain ratio. */
			if(lpmwd->mapview.cxGrid > 1 && lpmwd->mapview.cyGrid > 1)
			{
				lpmwd->mapview.cxGrid >>= 1;
				lpmwd->mapview.cyGrid >>= 1;
			}

			lpmwd->bWantRedraw = TRUE;
			UpdateStatusBar(lpmwd, SBPF_GRID);

			return 0;

		/* Lines menu. ********************************************************/
		case IDM_LINES_AUTOALIGNTEXTURES:
			{
				ALIGNDLGDATA aligndd;
				int iLinedef = 0;

				/* Fill in members to set whether controls are enabled. */
				if(lpmwd->highlightobj.emType == EM_LINES)
				{
					iLinedef = lpmwd->highlightobj.iIndex;
					aligndd.bFromThisLine = TRUE;
				}
				else if(lpmwd->selection.lpsellistLinedefs->iDataCount == 1)
				{
					iLinedef = lpmwd->selection.lpsellistLinedefs->lpiIndices[0];
					aligndd.bFromThisLine = TRUE;
				}
				else aligndd.bFromThisLine = FALSE;

				aligndd.bInSelection = (lpmwd->selection.lpsellistLinedefs->iDataCount > 0);

				/* Present the dialogue, and ensure the user doesn't cancel. */
				if(AlignDlg(g_hwndMain, &aligndd))
				{
					if(lpmwd->submode != ESM_DRAGGING && lpmwd->submode != ESM_PASTING)
					{
						CancelCurrentOperation(lpmwd);
						CreateUndo(lpmwd, IDS_UNDO_AUTOALIGN);
						ClearRedoStack(lpmwd);
						lpmwd->bMapChanged = TRUE;
					}

					if(aligndd.bFromThisLine)
						AutoalignTexturesFromLinedef(lpmwd->lpmap, hwnd, iLinedef, aligndd.byTexFlags, aligndd.bInSelection);
					else
						AutoalignAllTextures(lpmwd->lpmap, hwnd, aligndd.byTexFlags, aligndd.bInSelection);

					MapWinUpdateInfoBar(lpmwd);
				}
			}

			return 0;

		case IDM_LINES_FIXMISSINGTEXTURES:
			{
				MISSTEXDLGDATA mtdd;
				CONFIG *lpcfgDefTex = ConfigGetSubsection(lpmwd->lpcfgWadOptMap, OPT_DEFAULTTEX);

				/* Do we have a selection? */
				mtdd.bInSelection = (lpmwd->selection.lpsellistLinedefs->iDataCount > 0);

				/* Set some things that the dialogue needs to know about the map
				 * window.
				 */
				mtdd.lphimlCache = &lpmwd->himlCacheTextures;
				mtdd.lptnlAllTextures = lpmwd->lptnlTextures;
				mtdd.hwndMap = hwnd;

				/* Get the default textures to use for filling in. */

				/* In case someone's messed with the config file. */
				*mtdd.szUpper = *mtdd.szMiddle = *mtdd.szLower = TEXT('\0');

				ConfigGetString(lpcfgDefTex, "upper", mtdd.szUpper, sizeof(mtdd.szUpper)/sizeof(TCHAR));
				ConfigGetString(lpcfgDefTex, "middle", mtdd.szMiddle, sizeof(mtdd.szMiddle)/sizeof(TCHAR));
				ConfigGetString(lpcfgDefTex, "lower", mtdd.szLower, sizeof(mtdd.szLower)/sizeof(TCHAR));

				/* Present the dialogue, and ensure the user doesn't cancel. */
				if(MissingTexDlg(g_hwndMain, &mtdd))
				{
					if(lpmwd->submode != ESM_DRAGGING && lpmwd->submode != ESM_PASTING)
					{
						CancelCurrentOperation(lpmwd);
						CreateUndo(lpmwd, IDS_UNDO_MISSTEX);
						ClearRedoStack(lpmwd);
						lpmwd->bMapChanged = TRUE;
					}

					/* Go! */
					FixMissingTextures(lpmwd->lpmap, mtdd.szUpper, mtdd.szMiddle, mtdd.szLower, mtdd.bInSelection);

					MapWinUpdateInfoBar(lpmwd);
				}
			}

			return 0;

		case IDM_LINES_FLIPLINEDEFS:

			if(lpmwd->selection.lpsellistLinedefs->iDataCount > 0)
			{
				if(lpmwd->submode != ESM_DRAGGING && lpmwd->submode != ESM_PASTING)
				{
					CancelCurrentOperation(lpmwd);
					CreateUndo(lpmwd, IDS_UNDO_FLIPLINEDEFS);
					ClearRedoStack(lpmwd);
					lpmwd->bMapChanged = TRUE;
				}

				FlipSelectedLinedefs(lpmwd->lpmap, lpmwd->selection.lpsellistLinedefs);
				lpmwd->bWantRedraw = TRUE;

				MapWinUpdateInfoBar(lpmwd);
			}
			else MessageBeep(MB_OK);

			return 0;

		case IDM_LINES_FLIPSIDEDEFS:

			if(lpmwd->selection.lpsellistLinedefs->iDataCount > 0)
			{
				if(lpmwd->submode != ESM_DRAGGING && lpmwd->submode != ESM_PASTING)
				{
					CancelCurrentOperation(lpmwd);
					CreateUndo(lpmwd, IDS_UNDO_FLIPSIDEDEFS);
					ClearRedoStack(lpmwd);
					lpmwd->bMapChanged = TRUE;
				}

				ExchangeSelectedSidedefs(lpmwd->lpmap, lpmwd->selection.lpsellistLinedefs);
				lpmwd->bWantRedraw = TRUE;

				MapWinUpdateInfoBar(lpmwd);
			}
			else MessageBeep(MB_OK);

			return 0;

		case IDM_LINES_SPLITLINEDEFS:

			if(lpmwd->selection.lpsellistLinedefs->iDataCount > 0)
			{
				if(lpmwd->submode != ESM_DRAGGING && lpmwd->submode != ESM_PASTING)
				{
					CancelCurrentOperation(lpmwd);
					CreateUndo(lpmwd, IDS_UNDO_SPLITLINEDEFS);
					ClearRedoStack(lpmwd);
					lpmwd->bMapChanged = TRUE;
				}

				BisectSelectedLinedefs(lpmwd->lpmap, lpmwd->selection.lpsellistLinedefs);
				/* TODO: Move the selection rebuilding from the fn to here. */
				lpmwd->bWantRedraw = TRUE;

				MapWinUpdateInfoBar(lpmwd);
				UpdateStatusBar(lpmwd, SBPF_LINEDEFS | SBPF_SIDEDEFS | SBPF_VERTICES);
			}
			else MessageBeep(MB_OK);

			return 0;

		/* Sectors menu. ******************************************************/
		case IDM_SECTORS_JOIN:

			if(lpmwd->submode == ESM_NONE && lpmwd->selection.lpsellistSectors->iDataCount > 1)
			{
				CreateUndo(lpmwd, IDS_UNDO_JOINSECTORS);
				ClearRedoStack(lpmwd);
				lpmwd->bMapChanged = TRUE;

				JoinSelectedSectors(lpmwd->lpmap, lpmwd->selection.lpsellistSectors, FALSE);
				RebuildSelection(lpmwd);
				lpmwd->bWantRedraw = TRUE;

				MapWinUpdateInfoBar(lpmwd);
				UpdateStatusBar(lpmwd, SBPF_SECTORS);
			}
			else MessageBeep(MB_OK);

			return 0;

		case IDM_SECTORS_MERGE:

			if(lpmwd->submode == ESM_NONE && lpmwd->selection.lpsellistSectors->iDataCount > 1)
			{
				CreateUndo(lpmwd, IDS_UNDO_MERGESECTORS);
				ClearRedoStack(lpmwd);
				lpmwd->bMapChanged = TRUE;

				JoinSelectedSectors(lpmwd->lpmap, lpmwd->selection.lpsellistSectors, TRUE);
				RebuildSelection(lpmwd);
				lpmwd->bWantRedraw = TRUE;

				MapWinUpdateInfoBar(lpmwd);
				UpdateStatusBar(lpmwd, SBPF_LINEDEFS | SBPF_SIDEDEFS | SBPF_VERTICES | SBPF_SECTORS);
			}
			else MessageBeep(MB_OK);

			return 0;

		case IDM_SECTORS_FLOORS_INCREASE:

			if(lpmwd->selection.lpsellistSectors->iDataCount > 0)
			{
				if(lpmwd->submode != ESM_DRAGGING && lpmwd->submode != ESM_PASTING)
				{
					CancelCurrentOperation(lpmwd);
					CreateUndo(lpmwd, IDS_UNDO_INCREASEFLOOR);
					ClearRedoStack(lpmwd);
					lpmwd->bMapChanged = TRUE;
				}

				ApplyRelativeFloorHeightSelection(lpmwd->lpmap, lpmwd->selection.lpsellistSectors, 8);

				/* Might change line colours. */
				lpmwd->bWantRedraw = TRUE;

				MapWinUpdateInfoBar(lpmwd);
			}
			else MessageBeep(MB_OK);

			return 0;

		case IDM_SECTORS_FLOORS_DECREASE:

			if(lpmwd->selection.lpsellistSectors->iDataCount > 0)
			{
				if(lpmwd->submode != ESM_DRAGGING && lpmwd->submode != ESM_PASTING)
				{
					CancelCurrentOperation(lpmwd);
					CreateUndo(lpmwd, IDS_UNDO_DECREASEFLOOR);
					ClearRedoStack(lpmwd);
					lpmwd->bMapChanged = TRUE;
				}

				ApplyRelativeFloorHeightSelection(lpmwd->lpmap, lpmwd->selection.lpsellistSectors, -8);

				/* Might change line colours. */
				lpmwd->bWantRedraw = TRUE;

				MapWinUpdateInfoBar(lpmwd);
			}
			else MessageBeep(MB_OK);

			return 0;

		case IDM_SECTORS_FLOORS_GRADIENT:

			if(lpmwd->selection.lpsellistSectors->iDataCount > 0)
			{
				if(lpmwd->submode != ESM_DRAGGING && lpmwd->submode != ESM_PASTING)
				{
					CancelCurrentOperation(lpmwd);
					CreateUndo(lpmwd, IDS_UNDO_GRADIENTFLOOR);
					ClearRedoStack(lpmwd);
					lpmwd->bMapChanged = TRUE;
				}

				GradientSelectedFloors(lpmwd->lpmap, lpmwd->selection.lpsellistSectors);

				/* Might change line colours. */
				lpmwd->bWantRedraw = TRUE;
			}
			else MessageBeep(MB_OK);

			return 0;

		case IDM_SECTORS_CEILINGS_INCREASE:

			if(lpmwd->selection.lpsellistSectors->iDataCount > 0)
			{
				if(lpmwd->submode != ESM_DRAGGING && lpmwd->submode != ESM_PASTING)
				{
					CancelCurrentOperation(lpmwd);
					CreateUndo(lpmwd, IDS_UNDO_INCREASECEIL);
					ClearRedoStack(lpmwd);
					lpmwd->bMapChanged = TRUE;
				}

				ApplyRelativeCeilingHeightSelection(lpmwd->lpmap, lpmwd->selection.lpsellistSectors, 8);

				/* Might change line colours. */
				lpmwd->bWantRedraw = TRUE;

				MapWinUpdateInfoBar(lpmwd);
			}
			else MessageBeep(MB_OK);

			return 0;

		case IDM_SECTORS_CEILINGS_DECREASE:

			if(lpmwd->selection.lpsellistSectors->iDataCount > 0)
			{
				if(lpmwd->submode != ESM_DRAGGING && lpmwd->submode != ESM_PASTING)
				{
					CancelCurrentOperation(lpmwd);
					CreateUndo(lpmwd, IDS_UNDO_DECREASECEIL);
					ClearRedoStack(lpmwd);
					lpmwd->bMapChanged = TRUE;
				}

				ApplyRelativeCeilingHeightSelection(lpmwd->lpmap, lpmwd->selection.lpsellistSectors, -8);

				/* Might change line colours. */
				lpmwd->bWantRedraw = TRUE;

				MapWinUpdateInfoBar(lpmwd);
			}
			else MessageBeep(MB_OK);

			return 0;

		case IDM_SECTORS_CEILINGS_GRADIENT:

			if(lpmwd->selection.lpsellistSectors->iDataCount > 1)
			{
				if(lpmwd->submode != ESM_DRAGGING && lpmwd->submode != ESM_PASTING)
				{
					CancelCurrentOperation(lpmwd);
					CreateUndo(lpmwd, IDS_UNDO_GRADIENTCEIL);
					ClearRedoStack(lpmwd);
					lpmwd->bMapChanged = TRUE;
				}

				GradientSelectedCeilings(lpmwd->lpmap, lpmwd->selection.lpsellistSectors);

				/* Might change line colours. */
				lpmwd->bWantRedraw = TRUE;
			}
			else MessageBeep(MB_OK);

			return 0;

		case IDM_SECTORS_LIGHT_INCREASE:

			if(lpmwd->selection.lpsellistSectors->iDataCount > 0)
			{
				if(lpmwd->submode != ESM_DRAGGING && lpmwd->submode != ESM_PASTING)
				{
					CancelCurrentOperation(lpmwd);
					CreateUndo(lpmwd, IDS_UNDO_INCREASELIGHT);
					ClearRedoStack(lpmwd);
					lpmwd->bMapChanged = TRUE;
				}

				ApplyRelativeBrightnessSelection(lpmwd->lpmap, lpmwd->selection.lpsellistSectors, 16);

				MapWinUpdateInfoBar(lpmwd);
			}
			else MessageBeep(MB_OK);

			return 0;

		case IDM_SECTORS_LIGHT_DECREASE:

			if(lpmwd->selection.lpsellistSectors->iDataCount > 0)
			{
				if(lpmwd->submode != ESM_DRAGGING && lpmwd->submode != ESM_PASTING)
				{
					CancelCurrentOperation(lpmwd);
					CreateUndo(lpmwd, IDS_UNDO_DECREASELIGHT);
					ClearRedoStack(lpmwd);
					lpmwd->bMapChanged = TRUE;
				}

				ApplyRelativeBrightnessSelection(lpmwd->lpmap, lpmwd->selection.lpsellistSectors, -16);

				MapWinUpdateInfoBar(lpmwd);
			}
			else MessageBeep(MB_OK);

			return 0;

		case IDM_SECTORS_LIGHT_GRADIENT:

			if(lpmwd->selection.lpsellistSectors->iDataCount > 1)
			{
				if(lpmwd->submode != ESM_DRAGGING && lpmwd->submode != ESM_PASTING)
				{
					CancelCurrentOperation(lpmwd);
					CreateUndo(lpmwd, IDS_UNDO_GRADIENTLIGHT);
					ClearRedoStack(lpmwd);
					lpmwd->bMapChanged = TRUE;
				}

				GradientSelectedBrightnesses(lpmwd->lpmap, lpmwd->selection.lpsellistSectors);

				/* Might change line colours. */
				lpmwd->bWantRedraw = TRUE;
			}
			else MessageBeep(MB_OK);

			return 0;

		case IDM_SECTORS_IDENT:
			if(lpmwd->editmode == EM_ANY || lpmwd->editmode == EM_SECTORS)
			{
				int i, iRet;
				HIGHLIGHTOBJ hobj;
				DYNAMICINTARRAY diarraySet;

				CancelCurrentOperation(lpmwd);

				/* Make sure in range. */
				if(lpmwd->iNextSectorSetIndex < 0)
					lpmwd->iNextSectorSetIndex = 0;
				else if(lpmwd->iNextSectorSetIndex >= lpmwd->lpmap->iSectors)
					lpmwd->iNextSectorSetIndex = lpmwd->lpmap->iSectors - 1;

				/* Initialise the array for the set. */
				InitialiseDynamicIntArray(&diarraySet, IDENT_SEC_ARRAY_INIT_SIZE);				

				/* Try to find a set of sectors. */
				iRet = FindIdenticalSectorSet(lpmwd->lpmap, lpmwd->iNextSectorSetIndex, &diarraySet);

				if(iRet >= 0)
				{
					/* Found a set. Select it. */

					DeselectAllLines(lpmwd);
					DeselectAllSectors(lpmwd);

					/* Select the set. */
					hobj.emType = EM_SECTORS;
					for(i = 0; (unsigned int)i < diarraySet.uiCount; i++)
					{
						hobj.iIndex = diarraySet.lpiIndices[i];
						AddObjectToSelection(lpmwd, &hobj);	
					}

					lpmwd->bWantRedraw = TRUE;
					MapWinUpdateInfoBar(lpmwd);
				}
				else MessageBoxFromStringTable(g_hwndMain, IDS_NOIDENTSECTORS, MB_ICONINFORMATION);

				/* Doesn't matter if this is out of range: we correct beforehand
				 * next time.
				 */
				lpmwd->iNextSectorSetIndex = 1 + iRet;
				
				/* Clean up. */
				FreeDynamicIntArray(&diarraySet);
			}
			else MessageBeep(MB_OK);

			return 0;

		case IDM_SECTORS_MERGEALL:
			if(IDYES == MessageBoxFromStringTable(g_hwndMain, IDS_WARNING_MERGEALL, MB_ICONEXCLAMATION | MB_YESNO))
			{
				int iMerged;

				/* Don't clear the redo stack yet -- if we don't find any
				 * identical sets, we withdraw the undo.
				 */
				CancelCurrentOperation(lpmwd);
				CreateUndo(lpmwd, IDS_UNDO_MERGEALL);

				/* Go! */
				iMerged = MergeAllIdenticalSectors(lpmwd->lpmap);

				if(iMerged > 0)
				{
					/* Merged some sectors. Display a message box saying how
					 * many sets.
					 */
					TCHAR szFormat[CCH_MERGECOUNT_FORMAT];
					TCHAR szMessage[CCH_MERGECOUNT_FORMAT];

					/* Get the format string. */
					LoadString(g_hInstance, IDS_MERGECOUNT_FORMAT, szFormat, sizeof(szFormat)/sizeof(TCHAR));

					/* Fill in the number of sectors, and make sure the string
					 * is terminated.
					 */
					_sntprintf(szMessage, sizeof(szMessage)/sizeof(TCHAR) - 1, szFormat, iMerged);
					szMessage[sizeof(szMessage)/sizeof(TCHAR) - 1] = TEXT('\0');

					/* Show the message. */
					MessageBox(g_hwndMain, szMessage, g_szAppName, MB_ICONINFORMATION);

					/* We can't really recover the selection, since the line/sec
					 * association is lost.
					 */
					DeselectAllBruteForce(lpmwd);

					/* Finish off. */
					ClearRedoStack(lpmwd);
					UpdateStatusBar(lpmwd, SBPF_LINEDEFS | SBPF_SIDEDEFS | SBPF_SECTORS);
					lpmwd->bMapChanged = TRUE;
				}
				else
				{
					/* Didn't merge anything. */
					MessageBoxFromStringTable(g_hwndMain, IDS_NOIDENTSECTORS, MB_ICONINFORMATION);
					WithdrawUndo(lpmwd);
				}

				/* We cleared the selection, so redraw regardless of whether we
				 * merged.
				 */
				lpmwd->bWantRedraw = TRUE;
				MapWinUpdateInfoBar(lpmwd);
			}

			return 0;


		/* Vertices menu. *******************************************************/
		case IDM_VERTICES_STITCH:

			if(lpmwd->selection.lpsellistVertices->iDataCount > 1)
			{
				if(lpmwd->submode != ESM_DRAGGING && lpmwd->submode != ESM_PASTING)
				{
					CancelCurrentOperation(lpmwd);
					CreateUndo(lpmwd, IDS_UNDO_STITCH);
					ClearRedoStack(lpmwd);
					lpmwd->bMapChanged = TRUE;
				}

				StitchMultipleVertices(lpmwd->lpmap,
					&lpmwd->selection.lpsellistVertices->lpiIndices[1],
					lpmwd->selection.lpsellistVertices->iDataCount - 1,
					lpmwd->selection.lpsellistVertices->lpiIndices[0]);

				/* All sorts have been deleted. */
				RebuildSelection(lpmwd);
				MapWinUpdateInfoBar(lpmwd);
				UpdateStatusBar(lpmwd, SBPF_VERTICES | SBPF_LINEDEFS | SBPF_SECTORS);

				lpmwd->bWantRedraw = TRUE;
			}
			else MessageBeep(MB_OK);

			return 0;

		case IDM_VERTICES_DELETEUNUSED:
			if(lpmwd->submode == ESM_NONE)
			{
				CreateUndo(lpmwd, IDS_UNDO_DELUNUSEDVERTICES);
				ClearRedoStack(lpmwd);
				lpmwd->bMapChanged = TRUE;

				RemoveUnusedVertices(lpmwd->lpmap, 0);
				lpmwd->bWantRedraw = TRUE;
			}
			else MessageBeep(MB_OK);

			return 0;


		/* Things menu. *******************************************************/
		case IDM_THINGS_ROTATE_ACW:
		case IDM_THINGS_ROTATE_CW:

			if(lpmwd->selection.lpsellistThings->iDataCount > 0)
			{
				if(lpmwd->submode != ESM_DRAGGING && lpmwd->submode != ESM_PASTING)
				{
					CancelCurrentOperation(lpmwd);
					CreateUndo(lpmwd, IDS_UNDO_ROTATETHINGS);
					ClearRedoStack(lpmwd);
					lpmwd->bMapChanged = TRUE;
				}

				RotateSelectedThings(lpmwd, (LOWORD(wParam) == IDM_THINGS_ROTATE_ACW) ? 45 : -45);

				MapWinUpdateInfoBar(lpmwd);
				lpmwd->bWantRedraw = TRUE;
			}
			else MessageBeep(MB_OK);

			return 0;

		case IDM_THINGS_ROTATE_MOUSE:

			if(lpmwd->selection.lpsellistThings->iDataCount > 0 &&
				lpmwd->submode != ESM_DRAGGING && lpmwd->submode != ESM_PASTING)
			{
				POINT ptMouse;
				GetCursorPos(&ptMouse);
				ScreenToClient(lpmwd->hwnd, &ptMouse);

				CancelCurrentOperation(lpmwd);
				UpdateHighlight(lpmwd, -1, -1);

				/* Remember where we started out. Can't use MouseLast because of
				 * menus.
				 */
				InitEdgeScrollBoundaries(lpmwd, (unsigned short)ptMouse.x, (unsigned short)ptMouse.y);

				lpmwd->submode = ESM_ROTATING;
				lpmwd->bWantRedraw = TRUE;
			}
			else MessageBeep(MB_OK);

			return 0;

		case IDM_THINGS_HEIGHT_INCREASE:

			if(lpmwd->selection.lpsellistThings->iDataCount > 0)
			{
				if(lpmwd->submode != ESM_DRAGGING && lpmwd->submode != ESM_PASTING)
				{
					CancelCurrentOperation(lpmwd);
					CreateUndo(lpmwd, IDS_UNDO_INCREASETHINGZ);
					ClearRedoStack(lpmwd);
					lpmwd->bMapChanged = TRUE;
				}

				ApplyRelativeThingZSelection(lpmwd->lpmap, lpmwd->selection.lpsellistThings, ConfigGetSubsection(lpmwd->lpcfgMap, FLAT_THING_SECTION), 8);

				MapWinUpdateInfoBar(lpmwd);
			}
			else MessageBeep(MB_OK);

			return 0;

		case IDM_THINGS_HEIGHT_DECREASE:

			if(lpmwd->selection.lpsellistThings->iDataCount > 0)
			{
				if(lpmwd->submode != ESM_DRAGGING && lpmwd->submode != ESM_PASTING)
				{
					CancelCurrentOperation(lpmwd);
					CreateUndo(lpmwd, IDS_UNDO_DECREASETHINGZ);
					ClearRedoStack(lpmwd);
					lpmwd->bMapChanged = TRUE;
				}

				ApplyRelativeThingZSelection(lpmwd->lpmap, lpmwd->selection.lpsellistThings, ConfigGetSubsection(lpmwd->lpcfgMap, FLAT_THING_SECTION), -8);

				MapWinUpdateInfoBar(lpmwd);
			}
			else MessageBeep(MB_OK);

			return 0;

		case IDM_THINGS_HEIGHT_GRADIENT:

			if(lpmwd->selection.lpsellistThings->iDataCount > 1)
			{
				if(lpmwd->submode != ESM_DRAGGING && lpmwd->submode != ESM_PASTING)
				{
					CancelCurrentOperation(lpmwd);
					CreateUndo(lpmwd, IDS_UNDO_GRADIENTTHINGZ);
					ClearRedoStack(lpmwd);
					lpmwd->bMapChanged = TRUE;
				}

				GradientSelectedThingZ(lpmwd->lpmap, lpmwd->selection.lpsellistThings, ConfigGetSubsection(lpmwd->lpcfgMap, FLAT_THING_SECTION));

				MapWinUpdateInfoBar(lpmwd);
			}
			else MessageBeep(MB_OK);

			return 0;


		case IDM_POPUP_POLY:
			InsertPolygonalSector(lpmwd, XMOUSE_WIN_TO_MAP((short)lpmwd->ptsPopupMenuLast.x, lpmwd), YMOUSE_WIN_TO_MAP((short)lpmwd->ptsPopupMenuLast.y, lpmwd));
			lpmwd->bWantRedraw = TRUE;
			return 0;

		case IDM_POPUP_DRAW:
			{
				short x, y;

				/* Translate to map co-ordinates. */
				x = XMOUSE_WIN_TO_MAP((short)lpmwd->ptsPopupMenuLast.x, lpmwd);
				y = YMOUSE_WIN_TO_MAP((short)lpmwd->ptsPopupMenuLast.y, lpmwd);

				/* Do snapping on the point if necessary. */
				RequiredSnapping(lpmwd->lpmap, &x, &y, &lpmwd->mapview, lpmwd->bySnapFlags ^ lpmwd->bySnapToggleMask, lpmwd->submode, -1);

				lpmwd->submode = ESM_INSERTING;
				BeginDrawOperation(&lpmwd->drawop);

				/* Remember where we started out. */
				InitEdgeScrollBoundaries(lpmwd, lpmwd->ptsPopupMenuLast.x, lpmwd->ptsPopupMenuLast.y);

				/* Undo is handled here. We're not in vertices mode, so everything
				 * is hunky-dory.
				 */
				DoInsertOperation(lpmwd, x, y);
			}
			return 0;

		case IDM_POPUP_VERTEX:
			{
				short x, y;

				/* Translate to map co-ordinates. */
				x = XMOUSE_WIN_TO_MAP((short)lpmwd->ptsPopupMenuLast.x, lpmwd);
				y = YMOUSE_WIN_TO_MAP((short)lpmwd->ptsPopupMenuLast.y, lpmwd);

				/* Do snapping on the point if necessary. */
				RequiredSnapping(lpmwd->lpmap, &x, &y, &lpmwd->mapview, lpmwd->bySnapFlags ^ lpmwd->bySnapToggleMask, lpmwd->submode, -1);

				CreateUndo(lpmwd, IDS_UNDO_VERTEXINSERT);
				ClearRedoStack(lpmwd);
				lpmwd->bMapChanged = TRUE;

				/* Simulate a draw in vertex mode. */
				BeginDrawOperation(&lpmwd->drawop);
				DrawToNewVertex(lpmwd->lpmap, &lpmwd->drawop, x, y, lpmwd->bStitchMode);
				EndDrawOperation(lpmwd->lpmap, &lpmwd->drawop);

				/* Finish off. */
				lpmwd->bWantRedraw = TRUE;

				UpdateStatusBar(lpmwd, SBPF_VERTICES | SBPF_LINEDEFS | SBPF_SIDEDEFS);
			}

			return 0;

		case IDM_POPUP_THING:
			{
				short x, y;

				/* Translate to map co-ordinates. */
				x = XMOUSE_WIN_TO_MAP((short)lpmwd->ptsPopupMenuLast.x, lpmwd);
				y = YMOUSE_WIN_TO_MAP((short)lpmwd->ptsPopupMenuLast.y, lpmwd);

				/* Do snapping on the point if necessary. */
				RequiredSnapping(lpmwd->lpmap, &x, &y, &lpmwd->mapview, lpmwd->bySnapFlags ^ lpmwd->bySnapToggleMask, lpmwd->submode, -1);

				/* Undoing and status bar updating are handled here. */
				EditorInsertThing(lpmwd, x, y);
				lpmwd->bWantRedraw = TRUE;
			}

			return 0;

		case IDM_POPUP_PROPERTIES:
			ShowSelectionProperties(lpmwd, TRUE);
			return 0;

		case IDM_POPUP_SPLITLINEHERE:
			if(lpmwd->selection.lpsellistLinedefs->iDataCount == 1)
			{
				short x, y;

				/* Translate to map co-ordinates. */
				x = XMOUSE_WIN_TO_MAP((short)lpmwd->ptsPopupMenuLast.x, lpmwd);
				y = YMOUSE_WIN_TO_MAP((short)lpmwd->ptsPopupMenuLast.y, lpmwd);

				/* Do snapping on the point if necessary. */
				RequiredSnapping(lpmwd->lpmap, &x, &y, &lpmwd->mapview, lpmwd->bySnapFlags ^ lpmwd->bySnapToggleMask, lpmwd->submode, -1);

				CreateUndo(lpmwd, IDS_UNDO_SPLITLINEDEFS);
				ClearRedoStack(lpmwd);
				lpmwd->bMapChanged = TRUE;

				/* Split the line. */
				SplitLinedefAtPoint(lpmwd->lpmap, lpmwd->selection.lpsellistLinedefs->lpiIndices[0], x, y);
				lpmwd->bWantRedraw = TRUE;

				UpdateStatusBar(lpmwd, SBPF_VERTICES | SBPF_LINEDEFS | SBPF_SIDEDEFS);
			}
			else MessageBeep(MB_OK);

			return 0;			
		}

		/* No-one else needs to see it any more. */
		return 0;


	case WM_MOUSEWHEEL:
		/* We pass wheel events as keypresses so they can act as shortcuts. */
		lpmwd = (MAPWINDATA*)GetWindowLong(hwnd, GWL_USERDATA);
		MapKeyDown(lpmwd, MakeShiftedKeyCode((short)HIWORD(wParam) > 0 ? MOUSE_SCROLL_UP : MOUSE_SCROLL_DOWN));
		return 0;

	case WM_KEYDOWN:
		/* Most of our keypresses are handled using accelerators, but there are
		 * some exceptions.
		 */
		lpmwd = (MAPWINDATA*)GetWindowLong(hwnd, GWL_USERDATA);
		MapKeyDown(lpmwd, MakeShiftedKeyCode(wParam));
		return 0;

	case WM_KEYUP:
		lpmwd = (MAPWINDATA*)GetWindowLong(hwnd, GWL_USERDATA);
		MapKeyUp(lpmwd, MakeShiftedKeyCode(wParam));
		return 0;

	case WM_MOUSEMOVE:
		return MouseMove((MAPWINDATA*)GetWindowLong(hwnd, GWL_USERDATA), LOWORD(lParam), HIWORD(lParam));

	case WM_LBUTTONDOWN:
		return LButtonDown((MAPWINDATA*)GetWindowLong(hwnd, GWL_USERDATA), LOWORD(lParam), HIWORD(lParam), (WORD)wParam);

	case WM_LBUTTONDBLCLK:
		return LButtonDblClk((MAPWINDATA*)GetWindowLong(hwnd, GWL_USERDATA), LOWORD(lParam), HIWORD(lParam), (WORD)wParam);

	case WM_LBUTTONUP:
		return LButtonUp((MAPWINDATA*)GetWindowLong(hwnd, GWL_USERDATA), LOWORD(lParam), HIWORD(lParam), (WORD)wParam);

	case WM_RBUTTONUP:
		return RButtonUp((MAPWINDATA*)GetWindowLong(hwnd, GWL_USERDATA), LOWORD(lParam), HIWORD(lParam), (WORD)wParam);

	case WM_RBUTTONDOWN:
		return RButtonDown((MAPWINDATA*)GetWindowLong(hwnd, GWL_USERDATA), LOWORD(lParam), HIWORD(lParam), (WORD)wParam);

	case WM_MOUSELEAVE:
		/* Generated by TrackMouseEvent. */

		lpmwd = (MAPWINDATA*)GetWindowLong(hwnd, GWL_USERDATA);
		lpmwd->xMouseLast = lpmwd->yMouseLast = -1;

		/* Clear the highlight if necessary. */
		UpdateHighlight(lpmwd, -1, -1);

		return 0;

	case WM_EXITMENULOOP:
		{
			/* A menu just closed, so to avoid this being interpreted as the
			 * mouse having moved directly from its former position to its new
			 * one, pretend it's been here all along.
			 */

			POINT pt;

			GetCursorPos(&pt);
			ScreenToClient(hwnd, &pt);
			lpmwd = (MAPWINDATA*)GetWindowLong(hwnd, GWL_USERDATA);
			lpmwd->xMouseLast = (short)pt.x;
			lpmwd->yMouseLast = (short)pt.y;

			/* Menus can break the highlight. */
			UpdateHighlight(lpmwd, lpmwd->xMouseLast, lpmwd->yMouseLast);
		}

		return 0;

	case WM_SIZE:

		if(wParam != SIZE_MINIMIZED)
		{
			lpmwd = (MAPWINDATA*)GetWindowLong(hwnd, GWL_USERDATA);

			/* Store the width and the height for later use. */
			lpmwd->mapview.cxDrawSurface = (LOWORD(lParam) + 3) & ~3;
			lpmwd->mapview.cyDrawSurface = HIWORD(lParam);

			RendererMakeDCCurrent(lpmwd->hdc);
			ReSizeGLScene(lpmwd->mapview.cxDrawSurface, lpmwd->mapview.cyDrawSurface);
		}

		/* DefMDIChildProc needs to process this message too. */
		break;

	case WM_ERASEBKGND:
		return 1;	/* Stop flickering. */

	case WM_TIMER:

		/* Is this the redraw timer? */
		if(wParam == IDT_ANIM_TIMER)
		{
			lpmwd = (MAPWINDATA*)GetWindowLong(hwnd, GWL_USERDATA);

			/* Edge scrolling. */
			switch(lpmwd->submode)
			{
			case ESM_DRAWING:
			case ESM_INSERTING:
			case ESM_ROTATING:
			case ESM_SELECTING:
			case ESM_DRAGGING:
			case ESM_PASTING:
				if(EdgeScrolling(lpmwd))
				{
					POINT ptMouse;

					/* Simulate a mouse move. */
					GetCursorPos(&ptMouse);
					ScreenToClient(lpmwd->hwnd, &ptMouse);
					MouseMove(lpmwd, (short)ptMouse.x, (short)ptMouse.y);

					/* Refresh. */
					lpmwd->bWantRedraw = TRUE;
				}

				break;

			default:
				break;
			}

			/* Handle scrolling keys. */
			if(lpmwd->byScrollKeyFlags & SKF_ALL)
			{
				int iScrollSpeed = ConfigGetInteger(g_lpcfgMain, "scrollspeed");

				if(lpmwd->byScrollKeyFlags & SKF_UP)
					lpmwd->mapview.yTop += iScrollSpeed / lpmwd->mapview.fZoom;
				if(lpmwd->byScrollKeyFlags & SKF_DOWN)
					lpmwd->mapview.yTop -= iScrollSpeed / lpmwd->mapview.fZoom;
				if(lpmwd->byScrollKeyFlags & SKF_LEFT)
					lpmwd->mapview.xLeft -= iScrollSpeed / lpmwd->mapview.fZoom;
				if(lpmwd->byScrollKeyFlags & SKF_RIGHT)
					lpmwd->mapview.xLeft += iScrollSpeed / lpmwd->mapview.fZoom;

				lpmwd->bWantRedraw = TRUE;
			}

			if(lpmwd->bWantRedraw)
			{
				/* Force a repaint. */
				InvalidateRect(hwnd, NULL, FALSE);
				lpmwd->bWantRedraw = FALSE;
			}
		}

		return 0;

	case WM_PAINT:
		{
			PAINTSTRUCT ps;

			lpmwd = (MAPWINDATA*)GetWindowLong(hwnd, GWL_USERDATA);

			BeginPaint(hwnd, &ps);
			RedrawMapWindow(lpmwd);
			EndPaint(hwnd, &ps);
		}

		return 0;

	case WM_CLOSE:

		lpmwd = (MAPWINDATA*)GetWindowLong(hwnd, GWL_USERDATA);

		/* If we're unsaved, display the customary message box. */
		if(lpmwd->bMapChanged)
		{
			TCHAR szSaveChanges[SAVECHANGES_BUFSIZE];
			int iHeadLength;
			int iMBRet;

			/* Cheap trick: use the window title to report the filename and
			 * lumpname in the message box.
			 */
			GetWindowText(hwnd, szSaveChanges, sizeof(szSaveChanges) / sizeof(TCHAR));
			iHeadLength = lstrlen(szSaveChanges);
			LoadString(g_hInstance, IDS_SAVECHANGES, szSaveChanges + iHeadLength, sizeof(szSaveChanges) / sizeof(TCHAR) - iHeadLength);

			iMBRet = MessageBox(g_hwndMain, szSaveChanges, g_szAppName, MB_ICONEXCLAMATION | MB_YESNOCANCEL);

			switch(iMBRet)
			{
			case IDYES:
				/* User selected to save. */
				MapWindowSave(lpmwd);
				break;
			case IDCANCEL:
				/* Don't close the window. */
				return 0;
			}
		}

		break;		/* Allow DefMDIChildProc to destroy us. */

	case WM_DESTROY:

		lpmwd = (MAPWINDATA*)GetWindowLong(hwnd, GWL_USERDATA);

		/* Stop the animation timer. */
		KillTimer(hwnd, IDT_ANIM_TIMER);

		/* Free data. */
		DestroyMapWindowData(hwnd);

		return 0;
	}

	return DefMDIChildProc(hwnd, uiMessage, wParam, lParam);
}


/* DestroyMapWindowData
 *   Cleans up a map window's data.
 *
 * Parameters:
 *   HWND		hwnd		Window handle.
 *
 * Return value:
 *   None.
 */
static void DestroyMapWindowData(HWND hwnd)
{
	MAPWINDATA *lpmwd = (MAPWINDATA*)GetWindowLong(hwnd, GWL_USERDATA);

	/* Deselect current rendering context -- it might be us! */
	wglMakeCurrent(NULL, NULL);

	/* Release the window's DC. */
	if(lpmwd->hdc) ReleaseDC(hwnd, lpmwd->hdc);

	/* Release the wad references. */
	ReleaseWad(lpmwd->iWadID);
	if(lpmwd->iIWadID >= 0) ReleaseWad(lpmwd->iIWadID);
	if(lpmwd->iAdditionalWadID >= 0) ReleaseWad(lpmwd->iAdditionalWadID);

	/* Clean up undo memory. */
	if(lpmwd->lpundostackUndo) FreeUndoStack(lpmwd->lpundostackUndo);
	if(lpmwd->lpundostackRedo) FreeUndoStack(lpmwd->lpundostackRedo);

	/* Destroy selection structures. */
	DestroySelectionList(lpmwd->selection.lpsellistLinedefs);
	DestroySelectionList(lpmwd->selection.lpsellistSectors);
	DestroySelectionList(lpmwd->selection.lpsellistVertices);
	DestroySelectionList(lpmwd->selection.lpsellistThings);

	/* Tidy up tex/flat caches and name-lists. */
	FreeTextureCaches(lpmwd);

	/* Destroy map structure. */
	DestroyMapStructure(lpmwd->lpmap);

	/* Destroy window structure. */
	HeapFree(g_hProcHeap, 0, lpmwd);
}

/* MapWinBelongsToWad
 *   Determines whether a map window belongs to a particular wad.
 *
 * Parameters:
 *   HWND		hwnd		Window handle.
 *   int		iWadID		ID of wad.
 *
 * Return value: BOOL
 *   TRUE if window belongs to wad; FALSE otherwise.
 */
BOOL MapWinBelongsToWad(HWND hwnd, int iWadID)
{
	return ((MAPWINDATA*)GetWindowLong(hwnd, GWL_USERDATA))->iWadID == iWadID;
}


/* MapKeyDown
 *   Performs certain keypress operations for a map window.
 *
 * Parameters:
 *   MAPWINDATA *lpmwd		Window data.
 *   int		iKeyCode	Key-code containing VK and shift states.
 *
 * Return value: BOOL
 *   TRUE if something done in response to keypress; FALSE otherwise.
 *
 * Notes:
 *   Most keypresses are handled by accelerators, but certain ones -- e.g. those
 *   that have separate up/down responses -- are handled separately.
 */
static BOOL MapKeyDown(MAPWINDATA *lpmwd, int iKeyCode)
{
	if(iKeyCode == g_iShortcutCodes[SCK_EDITQUICKMOVE])
	{
		if(lpmwd->editmode != EM_MOVE)
		{
			CancelCurrentOperation(lpmwd);
			StartPan(lpmwd);

			return TRUE;
		}
	}
	else if(iKeyCode == g_iShortcutCodes[SCK_ZOOMIN] || iKeyCode == g_iShortcutCodes[SCK_ZOOMOUT])
	{
		float fNewZoom;
		float cxDelta, cyDelta;
		BOOL bMouseInside;
		POINT ptCursor;

		/* Determine whether the mouse is over our map. */
		GetCursorPos(&ptCursor);
		bMouseInside = (SendMessage(lpmwd->hwnd, WM_NCHITTEST, 0, MAKELPARAM(ptCursor.x, ptCursor.y)) == HTCLIENT);

		/* Zoom in or out, according to request. */
		fNewZoom = lpmwd->mapview.fZoom * (1 + (((iKeyCode == g_iShortcutCodes[SCK_ZOOMIN]) ? 1 : -1) * ConfigGetInteger(g_lpcfgMain, "zoomspeed") / 1000.0f));

		/* Bounds. */
		if(fNewZoom > 100) fNewZoom = 100;
		else if(fNewZoom < 0.01f) fNewZoom = 0.01f;

		if(ConfigGetInteger(g_lpcfgMain, "zoommouse") && bMouseInside)
		{
			/* Fracunit x-origin difference =
			 *		(new fracunit width - old fracunit width) *
			 *		mouse pos as proportion of width.
			 *
			 * Similarly height.
			 */
			cxDelta = (((lpmwd->mapview.cxDrawSurface / fNewZoom) - (lpmwd->mapview.cxDrawSurface / lpmwd->mapview.fZoom)) * ((float)lpmwd->xMouseLast / lpmwd->mapview.cxDrawSurface));
			cyDelta = (((lpmwd->mapview.cyDrawSurface / fNewZoom) - (lpmwd->mapview.cyDrawSurface / lpmwd->mapview.fZoom)) * ((float)lpmwd->yMouseLast / lpmwd->mapview.cyDrawSurface));
		}
		else
		{
			cxDelta = (((lpmwd->mapview.cxDrawSurface / fNewZoom) - (lpmwd->mapview.cxDrawSurface / lpmwd->mapview.fZoom)) / 2);
			cyDelta = (((lpmwd->mapview.cyDrawSurface / fNewZoom) - (lpmwd->mapview.cyDrawSurface / lpmwd->mapview.fZoom)) / 2);
		}

		lpmwd->mapview.xLeft -= cxDelta;
		lpmwd->mapview.yTop += cyDelta;
		SetZoom(&lpmwd->mapview, fNewZoom);

		if(lpmwd->editmode == ESM_NONE)
			UpdateHighlight(lpmwd, lpmwd->xMouseLast, lpmwd->yMouseLast);

		/* Redraw map next frame. */
		lpmwd->bWantRedraw = TRUE;

		UpdateStatusBar(lpmwd, SBPF_ZOOM);
	}
	else if(iKeyCode == g_iShortcutCodes[SCK_DRAWSECTOR])
	{
		/* TODO: This should maybe have a menu item. If so, it should be handled
		 * as a WM_COMMAND.
		 */

		/* Begin insertion, of a type proper to the mode. */
		switch(lpmwd->editmode)
		{
		case EM_ANY:
		case EM_LINES:
		case EM_SECTORS:
		case EM_VERTICES:
			/* This is a prelude to ESM_DRAWING. Also, note that the map
			 * hasn't changed yet!
			 */
			CancelCurrentOperation(lpmwd);
			DeselectAll(lpmwd);
			UpdateHighlight(lpmwd, -1, -1);

			/* Remember where we started out. */
			InitEdgeScrollBoundaries(lpmwd, lpmwd->xMouseLast, lpmwd->yMouseLast);

			lpmwd->submode = ESM_INSERTING;

			BeginDrawOperation(&lpmwd->drawop);

			break;

		default:
            break;
		}

		lpmwd->bWantRedraw = TRUE;
	}
	else if(iKeyCode == g_iShortcutCodes[SCK_CANCEL])
		CancelCurrentOperation(lpmwd);
	else if(iKeyCode == g_iShortcutCodes[SCK_SCROLL_UP])
		lpmwd->byScrollKeyFlags |= SKF_UP;
	else if(iKeyCode == g_iShortcutCodes[SCK_SCROLL_DOWN])
		lpmwd->byScrollKeyFlags |= SKF_DOWN;
	else if(iKeyCode == g_iShortcutCodes[SCK_SCROLL_LEFT])
		lpmwd->byScrollKeyFlags |= SKF_LEFT;
	else if(iKeyCode == g_iShortcutCodes[SCK_SCROLL_RIGHT])
		lpmwd->byScrollKeyFlags |= SKF_RIGHT;

	return FALSE;
}



/* MapKeyUp
 *   Performs certain keyrelease operations for a map window.
 *
 * Parameters:
 *   MAPWINDATA*	lpmwd		Window data.
 *   int			iKeyCode	Key-code containing VK and shift states.
 *
 * Return value: BOOL
 *   TRUE if something done in response to keyrelease; FALSE otherwise.
 *
 * Notes:
 *   Most keypresses are handled by accelerators, but certain ones -- e.g. those
 *   that have separate up/down responses -- are handled separately.
 */
static BOOL MapKeyUp(MAPWINDATA *lpmwd, int iKeyCode)
{
	if(iKeyCode == g_iShortcutCodes[SCK_EDITQUICKMOVE])
	{
		if(lpmwd->editmode != EM_MOVE)
		{
			EndPan(lpmwd);

			return TRUE;
		}
	}
	else if(iKeyCode == g_iShortcutCodes[SCK_SCROLL_UP])
		lpmwd->byScrollKeyFlags &= ~SKF_UP;
	else if(iKeyCode == g_iShortcutCodes[SCK_SCROLL_DOWN])
		lpmwd->byScrollKeyFlags &= ~SKF_DOWN;
	else if(iKeyCode == g_iShortcutCodes[SCK_SCROLL_LEFT])
		lpmwd->byScrollKeyFlags &= ~SKF_LEFT;
	else if(iKeyCode == g_iShortcutCodes[SCK_SCROLL_RIGHT])
		lpmwd->byScrollKeyFlags &= ~SKF_RIGHT;

	return FALSE;
}


/* RedrawMapWindow
 *   Performs certain keypress operations for a map window.
 *
 * Parameters:
 *   MAPWINDATA*	lpmwd		Window data.
 *
 * Return value: None.
 */
static void RedrawMapWindow(MAPWINDATA *lpmwd)
{
	DRAWOP_RENDERINFO dori;

	/* If we're in a drawing operation, set up the extra info. */
	if(lpmwd->submode == ESM_DRAWING)
	{
		/* We must have a previous vertex in ESM_DRAWING. */
		int iVertexPrev = lpmwd->drawop.lpiNewVertices[lpmwd->drawop.iNewVertexCount-1];

		dori.ptSrc.x = lpmwd->lpmap->vertices[iVertexPrev].x;
		dori.ptSrc.y = lpmwd->lpmap->vertices[iVertexPrev].y;
	}

	if(lpmwd->submode == ESM_DRAWING || lpmwd->submode == ESM_INSERTING)
	{
		short x, y;

		/* We draw the floating vertex at the mouse position. The drawing line
		 * will also be drawn *to* here if necessary.
		 */
		x = XMOUSE_WIN_TO_MAP(lpmwd->xMouseLast, lpmwd);
		y = YMOUSE_WIN_TO_MAP(lpmwd->yMouseLast, lpmwd);

		/* Do snapping on the point if necessary. */
		RequiredSnapping(lpmwd->lpmap, &x, &y, &lpmwd->mapview, lpmwd->bySnapFlags ^ lpmwd->bySnapToggleMask, lpmwd->submode,
			(lpmwd->submode == ESM_DRAWING && lpmwd->drawop.iNewVertexCount > 0) ? lpmwd->drawop.lpiNewVertices[lpmwd->drawop.iNewVertexCount - 1] : -1);

		/* We needed these intermediates since Snap expects shorts, while POINTs
		 * have ints.
		 */
		dori.ptDest.x = x;
		dori.ptDest.y = y;
	}
	else if(lpmwd->submode == ESM_ROTATING)
	{
		dori.ptDest.x = XMOUSE_WIN_TO_MAP(lpmwd->xMouseLast, lpmwd);
		dori.ptDest.y = YMOUSE_WIN_TO_MAP(lpmwd->yMouseLast, lpmwd);
	}
	else if(lpmwd->submode == ESM_SELECTING)
	{
		dori.ptSrc.x = XMOUSE_WIN_TO_MAP(lpmwd->ptsSelectionStart.x, lpmwd);
		dori.ptSrc.y = YMOUSE_WIN_TO_MAP(lpmwd->ptsSelectionStart.y, lpmwd);
		dori.ptDest.x = XMOUSE_WIN_TO_MAP(lpmwd->xMouseLast, lpmwd);
		dori.ptDest.y = YMOUSE_WIN_TO_MAP(lpmwd->yMouseLast, lpmwd);
	}

	RendererMakeDCCurrent(lpmwd->hdc);

	/* Draw onto backbuffer. */
	RedrawMap(lpmwd->lpmap, lpmwd->editmode, lpmwd->submode, &lpmwd->mapview, &dori, &lpmwd->selection);

	glFlush();
	SwapBuffers(lpmwd->hdc);
}


static void ChangeMode(MAPWINDATA *lpmwd, USHORT unModeMenuID)
{
	/* TODO: Do all the menu stuff on WM_INITMENUPOPUP. */

	static USHORT s_unModeToMenu[7] =
	{
		IDM_VIEW_MODE_MOVE, IDM_VIEW_MODE_ANY,
		IDM_VIEW_MODE_VERTICES, IDM_VIEW_MODE_LINES,
		IDM_VIEW_MODE_SECTORS, IDM_VIEW_MODE_THINGS,
		IDM_VIEW_3D
	};

	HMENU hmenuView;
	POINT pt;


	/* Get handle to View menu. */
	hmenuView = GetSubMenu(g_hmenuMap, VIEWMENUPOS);


	/* Uncheck old modes if we're not about to move. */
	if(unModeMenuID != IDM_VIEW_MODE_MOVE)
	{
		CheckMenuItem(hmenuView, s_unModeToMenu[lpmwd->editmode], MF_BYCOMMAND | MF_UNCHECKED);
		CheckMenuItem(hmenuView, s_unModeToMenu[lpmwd->prevmode], MF_BYCOMMAND | MF_UNCHECKED);
	}


	if(unModeMenuID == IDM_VIEW_MODE_MOVE && lpmwd->editmode == EM_MOVE)
	{
		/* Restore previous mode. */
		ChangeMode(lpmwd, s_unModeToMenu[lpmwd->prevmode]);
		return;
	}
	else
	{
		lpmwd->prevmode = lpmwd->editmode;
	}

	/* Do things necessary on leaving old mode. */
	switch(lpmwd->editmode)
	{
	case EM_MOVE:
		lpmwd->hCursor = LoadCursor(NULL, IDC_ARROW);
		break;
	case EM_ANY:
		break;
	case EM_LINES:
		break;
	case EM_SECTORS:
		break;
	case EM_VERTICES:
		break;
	case EM_THINGS:
		break;
	default:
        break;
	}


	switch(unModeMenuID)
	{
	case IDM_VIEW_MODE_MOVE:
		lpmwd->editmode = EM_MOVE;
		lpmwd->hCursor = LoadCursor(g_hInstance, MAKEINTRESOURCE(IDC_GRABOPEN));
		break;

	case IDM_VIEW_MODE_ANY:
		lpmwd->editmode = EM_ANY;
		break;

	case IDM_VIEW_MODE_LINES:
		SelectLinesFromVertices(lpmwd);
		SelectSectorsFromLines(lpmwd);
		DeselectAllThings(lpmwd);
		DeselectAllVertices(lpmwd);
		lpmwd->editmode = EM_LINES;
		break;

	case IDM_VIEW_MODE_SECTORS:
		SelectLinesFromVertices(lpmwd);
		SelectSectorsFromLines(lpmwd);
		DeselectAllThings(lpmwd);
		DeselectAllVertices(lpmwd);
		DeselectLinesFromPartialSectors(lpmwd);
		lpmwd->editmode = EM_SECTORS;
		break;

	case IDM_VIEW_MODE_THINGS:
		DeselectAllLines(lpmwd);
		DeselectAllSectors(lpmwd);
		DeselectAllVertices(lpmwd);
		lpmwd->editmode = EM_THINGS;
		break;

	case IDM_VIEW_MODE_VERTICES:
		SelectVerticesFromLines(lpmwd);
		DeselectAllLines(lpmwd);
		DeselectAllSectors(lpmwd);
		DeselectAllThings(lpmwd);
		lpmwd->editmode = EM_VERTICES;
		break;
	}

	/* Check new mode. */
	CheckMenuItem(hmenuView, unModeMenuID, MF_BYCOMMAND | MF_CHECKED);

	/* Update highlight if mouse is in window. */
	if(lpmwd->xMouseLast >= 0 && lpmwd->yMouseLast >= 0)
		UpdateHighlight(lpmwd, lpmwd->xMouseLast, lpmwd->yMouseLast);

	/* Set up info bar. */
	SetInfoBarMode(lpmwd->editmode);
	MapWinUpdateInfoBar(lpmwd);

	/* Update cursor if we're in the client area. */
	pt.x = lpmwd->xMouseLast;
	pt.y = lpmwd->yMouseLast;
	ClientToScreen(lpmwd->hwnd, &pt);
	if(SendMessage(lpmwd->hwnd, WM_NCHITTEST, 0, MAKELPARAM(pt.x, pt.y)) == HTCLIENT)
		SetCursor(lpmwd->hCursor);
}



/* StatusBarMapWindow
 *   Sets the status bar to correspond to the active map window.
 *
 * Parameters:
 *   None.
 *
 * Return value: None.
 */
static void StatusBarMapWindow(void)
{
	INT lpiWidths[SBP_LAST] = {96, 84, 84, 84, 84, 84, 84, 84, 96, -1};
	int i;

	/* Calculate RHS from widths. Loop from second to second-to-last: The first
	 * and last are okay.
	 */
	for(i = 1; i < SBP_LAST - 1; i++) lpiWidths[i] += lpiWidths[i-1];

	SendMessage(g_hwndStatusBar, SB_SETPARTS, SBP_LAST, (LPARAM)lpiWidths);

	/* Not simple mode. */
	SendMessage(g_hwndStatusBar, SB_SIMPLE, FALSE, 0);
}


/* UpdateStatusBar
 *   Updates the status bar's panels' text.
 *
 * Parameters:
 *   MAPWINDATA*	lpmwd	Pointer to data for active window.
 *   DWORD			dwFlags	Flags specifiying which panels to update.
 *
 * Return value: None.
 *
 * Remarks:
 *   Each bit of the flag corresponds to the equally-numbered constant specified
 *   in _ENUM_SBPANELS.
 */
static void UpdateStatusBar(MAPWINDATA *lpmwd, DWORD dwFlags)
{
	TCHAR szBuffer[256], szFromStringTable[32];
	MAP *lpmap = lpmwd->lpmap;

	/* Check for each field, and update if asked. */
	if(dwFlags & SBPF_MODE)
	{
		DWORD dwStringID = 0;

		switch(lpmwd->editmode)
		{
			case EM_ANY:		dwStringID = IDS_SB_ANYMODE;		break;
			case EM_SECTORS:	dwStringID = IDS_SB_SECTORSMODE;	break;
			case EM_LINES:		dwStringID = IDS_SB_LINESMODE;		break;
			case EM_VERTICES:	dwStringID = IDS_SB_VERTICESMODE;	break;
			case EM_THINGS:		dwStringID = IDS_SB_THINGSMODE;		break;
			default:												break;
		}

		LoadString(g_hInstance, dwStringID, szFromStringTable, sizeof(szFromStringTable) / sizeof(TCHAR));
		_sntprintf(szBuffer, sizeof(szBuffer)/sizeof(TCHAR) - 1, TEXT("\t%s"), szFromStringTable);
		szBuffer[255] = '\0';
		SendMessage(g_hwndStatusBar, SB_SETTEXT, SBP_MODE, (LPARAM)szBuffer);
	}

	if(dwFlags & SBPF_SECTORS)
	{
		LoadString(g_hInstance, IDS_SB_SECTORS, szFromStringTable, sizeof(szFromStringTable) / sizeof(TCHAR));
		_sntprintf(szBuffer, sizeof(szBuffer)/sizeof(TCHAR) - 1, TEXT("\t%d %s"), lpmap->iSectors, szFromStringTable);
		szBuffer[255] = '\0';
		SendMessage(g_hwndStatusBar, SB_SETTEXT, SBP_SECTORS, (LPARAM)szBuffer);
	}

	if(dwFlags & SBPF_LINEDEFS)
	{
		LoadString(g_hInstance, IDS_SB_LINEDEFS, szFromStringTable, sizeof(szFromStringTable) / sizeof(TCHAR));
		_sntprintf(szBuffer, sizeof(szBuffer)/sizeof(TCHAR) - 1, TEXT("\t%d %s"), lpmap->iLinedefs, szFromStringTable);
		szBuffer[255] = '\0';
		SendMessage(g_hwndStatusBar, SB_SETTEXT, SBP_LINEDEFS, (LPARAM)szBuffer);
	}

	if(dwFlags & SBPF_SIDEDEFS)
	{
		LoadString(g_hInstance, IDS_SB_SIDEDEFS, szFromStringTable, sizeof(szFromStringTable) / sizeof(TCHAR));
		_sntprintf(szBuffer, sizeof(szBuffer)/sizeof(TCHAR) - 1, TEXT("\t%d %s"), lpmap->iSidedefs, szFromStringTable);
		szBuffer[255] = '\0';
		SendMessage(g_hwndStatusBar, SB_SETTEXT, SBP_SIDEDEFS, (LPARAM)szBuffer);
	}

	if(dwFlags & SBPF_VERTICES)
	{
		LoadString(g_hInstance, IDS_SB_VERTICES, szFromStringTable, sizeof(szFromStringTable) / sizeof(TCHAR));
		_sntprintf(szBuffer, sizeof(szBuffer)/sizeof(TCHAR) - 1, TEXT("\t%d %s"), lpmap->iVertices, szFromStringTable);
		szBuffer[255] = '\0';
		SendMessage(g_hwndStatusBar, SB_SETTEXT, SBP_VERTICES, (LPARAM)szBuffer);
	}

	if(dwFlags & SBPF_THINGS)
	{
		LoadString(g_hInstance, IDS_SB_THINGS, szFromStringTable, sizeof(szFromStringTable) / sizeof(TCHAR));
		_sntprintf(szBuffer, sizeof(szBuffer)/sizeof(TCHAR) - 1, TEXT("\t%d %s"), lpmap->iThings, szFromStringTable);
		szBuffer[255] = '\0';
		SendMessage(g_hwndStatusBar, SB_SETTEXT, SBP_THINGS, (LPARAM)szBuffer);
	}

	if(dwFlags & SBPF_COORD)
	{
		_sntprintf(szBuffer, sizeof(szBuffer)/sizeof(TCHAR) - 1, TEXT("\t(%d, %d)"), XMOUSE_WIN_TO_MAP(lpmwd->xMouseLast, lpmwd), YMOUSE_WIN_TO_MAP(lpmwd->yMouseLast, lpmwd));
		szBuffer[255] = '\0';
		SendMessage(g_hwndStatusBar, SB_SETTEXT, SBP_COORD, (LPARAM)szBuffer);
	}

	if(dwFlags & SBPF_GRID)
	{
		LoadString(g_hInstance, IDS_SB_GRID, szFromStringTable, sizeof(szFromStringTable) / sizeof(TCHAR));

		/* If the grid is square, don't duplicate the dimension. */
		if(lpmwd->mapview.cxGrid == lpmwd->mapview.cyGrid)
			_sntprintf(szBuffer, sizeof(szBuffer)/sizeof(TCHAR) - 1, TEXT("\t%s: %d"), szFromStringTable, lpmwd->mapview.cxGrid);
		else
			_sntprintf(szBuffer, sizeof(szBuffer)/sizeof(TCHAR) - 1, TEXT("\t%s: (%d, %d)"), szFromStringTable, lpmwd->mapview.cxGrid, lpmwd->mapview.cyGrid);

		szBuffer[255] = '\0';

		SendMessage(g_hwndStatusBar, SB_SETTEXT, SBP_GRID, (LPARAM)szBuffer);
	}

	if(dwFlags & SBPF_ZOOM)
	{
		LoadString(g_hInstance, IDS_SB_ZOOM, szFromStringTable, sizeof(szFromStringTable) / sizeof(TCHAR));

		/* If the grid is square, don't duplicate the dimension. */
		_sntprintf(szBuffer, sizeof(szBuffer)/sizeof(TCHAR) - 1, TEXT("\t%s: %d%%"), szFromStringTable, ROUND(lpmwd->mapview.fZoom * 100));

		szBuffer[255] = '\0';

		SendMessage(g_hwndStatusBar, SB_SETTEXT, SBP_ZOOM, (LPARAM)szBuffer);
	}

	if(dwFlags & SBPF_MAPCONFIG)
	{
		ConfigGetString(lpmwd->lpcfgMap, "game", szBuffer, sizeof(szBuffer) / sizeof(TCHAR));
		SendMessage(g_hwndStatusBar, SB_SETTEXT, SBP_MAPCONFIG, (LPARAM)szBuffer);
	}
}


/* UpdateHighlight, CalculateHighlight
 *   Updates highlighted object for a window, given mouse co-ordinates.
 *
 * Parameters:
 *   MAPWINDATA*	lpmwd			Pointer to data for window.
 *   int			xMouse, yMouse	Mouse co-ordinates.
 *
 * Return value: BOOL
 *   True if highlighted object was changed; FALSE otherwise.
 *
 * Remarks:
 *   If any of the mouse co-ordinates are negative, clear the highlight.
 *   CalculateHighlight doesn't update the info-bar.
 */
static __inline BOOL UpdateHighlight(MAPWINDATA *lpmwd, int xMouse, int yMouse)
{
	BOOL bRet = CalculateHighlight(lpmwd, xMouse, yMouse);

	if(bRet)
		MapWinUpdateInfoBar(lpmwd);

	return bRet;
}

static BOOL CalculateHighlight(MAPWINDATA *lpmwd, int xMouse, int yMouse)
{
	HIGHLIGHTOBJ highlightobj;

	if(xMouse >= 0 && yMouse >= 0)
	{
		int xMap, yMap;

		xMap = XMOUSE_WIN_TO_MAP(xMouse, lpmwd);
		yMap = YMOUSE_WIN_TO_MAP(yMouse, lpmwd);

		/* Find the nearest object for the mode. */
		GetNearestObject(lpmwd, xMap, yMap, lpmwd->editmode, &highlightobj);
	}
	else
	{
		/* Just clear the highlight. */
		highlightobj.emType = EM_MOVE;
	}

	/* Has the highlight changed? */
	if(highlightobj.emType != lpmwd->highlightobj.emType || (highlightobj.emType != EM_MOVE && highlightobj.iIndex != lpmwd->highlightobj.iIndex))
	{
		/* If so, redraw with new highlight. */
		ClearHighlight(lpmwd);
		SetHighlight(lpmwd, &highlightobj);
		lpmwd->bWantRedraw = TRUE;

		/* Highlight changed. */
		return TRUE;
	}

	/* Highlight didn't change. */
	return FALSE;
}


/* GetNearestObject
 *   Gets index and type of object nearest to a point on the map, according to
 *   mode.
 *
 * Parameters:
 *   MAPWINDATA*	lpmwd			Pointer to data for window.
 *   int			xMap, yMap		Map co-ordinates to find obj nearest to.
 *   ENUM_EDITMODE	mode			Editing mode, specifying types of obj.
 *   HIGHLIGHTOBJ*	lphighlightobj	Nearest object is returned here.
 *
 * Return value: None.
 *
 * Remarks:
 *   It's possible that nothing will be found, in which case the emType field of
 *   lphighlightobj will be set to EM_MOVE.
 */
static void GetNearestObject(MAPWINDATA *lpmwd, int xMap, int yMap, ENUM_EDITMODE mode, HIGHLIGHTOBJ *lphighlightobj)
{
	MAP *lpmap = lpmwd->lpmap;

	BOOL bWantLinedef = (mode == EM_LINES) || (mode == EM_ANY);
	BOOL bWantSector = (mode == EM_SECTORS) || (mode == EM_ANY);
	BOOL bWantThing = (mode == EM_THINGS) || (mode == EM_ANY);
	BOOL bWantVertex = (mode == EM_VERTICES) || (mode == EM_ANY);

	int iDistLinedef, iDistThing, iDistVertex;
	int iIndexLinedef = 0, iIndexSector = 0, iIndexThing = 0, iIndexVertex = 0;

	int iMinDist = INT_MAX;


	/* Begin by assuming we don't find anything. */
	lphighlightobj->emType = EM_MOVE;


	/* Find nearest objects. If we don't find any at each stage, indicate that
	 * we're no longer looking for objects of that type.
	 */

	if((bWantLinedef || bWantSector)
		&& -1 == (iIndexLinedef = NearestLinedef(xMap, yMap, lpmap->vertices, lpmap->linedefs, lpmap->iLinedefs, &iDistLinedef)))
	{
		bWantLinedef = bWantSector = FALSE;
	}

	if(bWantThing && -1 == (iIndexThing = NearestThing(xMap, yMap, lpmap->things, lpmap->iThings, &iDistThing, FALSE, NULL)))
		bWantThing = FALSE;

	if(bWantVertex && -1 == (iIndexVertex = NearestVertex(lpmap, xMap, yMap, &iDistVertex)))
		bWantVertex = FALSE;


	/* For sectors, we also check that we're actually *in* a sector. If we are,
	 * get its index.
	 */
	if(bWantSector)
	{
		iIndexSector = IntersectSector(xMap, yMap, lpmap->vertices, lpmap->linedefs, lpmap->sidedefs, lpmap->iLinedefs, NULL, NULL);
		if(iIndexSector < 0) bWantSector = FALSE;
	}


	/* If we're within VERTEX_THRESHOLD of a vertex, it takes precedence over
	 * linedefs and sectors.
	 */
	if(bWantVertex && iDistVertex * lpmwd->mapview.fZoom <= VERTEX_THRESHOLD)
		bWantLinedef = bWantSector = FALSE;

	/* Turn off one of these. */
	if(bWantLinedef && bWantSector)
	{
		/* Are we near enough to the linedef to choose it in preference to the
		 * sector?
		 */
		if(iDistLinedef * lpmwd->mapview.fZoom <= LINEDEF_THRESHOLD)
			bWantSector = FALSE;		/* Pick linedef. */
		else bWantLinedef = FALSE;		/* Pick sector. */
	}

	if(bWantLinedef && iDistLinedef < iMinDist)
	{
		iMinDist = iDistLinedef;
		lphighlightobj->emType = EM_LINES;
		lphighlightobj->iIndex = iIndexLinedef;
	}

	if(bWantThing && iDistThing < iMinDist)
	{
		iMinDist = iDistThing;
		lphighlightobj->emType = EM_THINGS;
		lphighlightobj->iIndex = iIndexThing;
	}

	if(bWantVertex && iDistVertex < iMinDist)
	{
		iMinDist = iDistVertex;
		lphighlightobj->emType = EM_VERTICES;
		lphighlightobj->iIndex = iIndexVertex;
	}

	/* We've checked everything except sectors now. These must respect the hover
	 * threshold.
	 */
	if(iMinDist * lpmwd->mapview.fZoom > HOVER_THRESHOLD)
		lphighlightobj->emType = EM_MOVE;

	/* With sectors, pick one if it's nearest *or* if we haven't found anything
	 * else yet. This assumes we're interested at all, of course.
	 */
	if(bWantSector && (iDistLinedef < iMinDist || lphighlightobj->emType == EM_MOVE))
	{
		iMinDist = iDistLinedef;
		lphighlightobj->emType = EM_SECTORS;
		lphighlightobj->iIndex = iIndexSector;
	}
}


/* ClearHighlight
 *   Sets a window's highlighted object to nothing.
 *
 * Parameters:
 *   MAPWINDATA*	lpmwd			Pointer to data for window.
 *
 * Return value: None.
 */
static void ClearHighlight(MAPWINDATA *lpmwd)
{
	int i, iLinedefs;
	MAP *lpmap = lpmwd->lpmap;
	int iIndex = lpmwd->highlightobj.iIndex;

	/* Clear the map-data highlight flag. */
	switch(lpmwd->highlightobj.emType)
	{
	case EM_SECTORS:

		/* For sectors, we actually highlight the linedefs. */
		iLinedefs = lpmwd->lpmap->iLinedefs;
		for(i=0; i<iLinedefs; i++)
		{
			if(LinedefBelongsToSector(lpmap, i, iIndex))
				lpmap->linedefs[i].highlight = 0;
		}

		break;

	case EM_LINES:
		lpmap->linedefs[iIndex].highlight = 0;
		break;
	case EM_VERTICES:
		lpmap->vertices[iIndex].highlight = 0;
		break;
	case EM_THINGS:
		lpmap->things[iIndex].highlight = 0;
		break;
	default:
        break;
	}

	/* Move mode indicates no higlight. There's *kind* of a logic to it... */
	lpmwd->highlightobj.emType = EM_MOVE;
}


/* SetHighlight
 *   Sets a window's highlighted object.
 *
 * Parameters:
 *   MAPWINDATA*	lpmwd			Pointer to data for window.
 *   HIGHLIGHTOBJ*	lphighlightobj	Pointer to object data.
 *
 * Return value: None.
 */
static void SetHighlight(MAPWINDATA *lpmwd, HIGHLIGHTOBJ *lphightlightobj)
{
	int i, iLinedefs;
	MAP *lpmap = lpmwd->lpmap;
	int iIndex;

	/* Store the details in the window data. */
	lpmwd->highlightobj = *lphightlightobj;
	iIndex = lphightlightobj->iIndex;

	/* Set the highlight flag in the map data. */
	switch(lphightlightobj->emType)
	{
	case EM_SECTORS:

		/* For sectors, we actually highlight the linedefs. */
		iLinedefs = lpmwd->lpmap->iLinedefs;
		for(i=0; i<iLinedefs; i++)
		{
			if(LinedefBelongsToSector(lpmap, i, iIndex))
				lpmap->linedefs[i].highlight = 1;
		}

		break;

	case EM_LINES:
		lpmap->linedefs[iIndex].highlight = 1;
		break;
	case EM_VERTICES:
		lpmap->vertices[iIndex].highlight = 1;
		break;
	case EM_THINGS:
		lpmap->things[iIndex].highlight = 1;
		break;
	default:
        break;
	}
}


/* MouseMove
 *   Perform map window's WM_MOUSEMOVE processing.
 *
 * Parameters:
 *   MAPWINDATA*	lpmwd			Pointer to data for window.
 *   WORD			xMouse, yMouse	Mouse co-ordinates.
 *
 * Return value: LRESULT
 *   Value window procedure should return.
 */
static LRESULT MouseMove(MAPWINDATA *lpmwd, WORD xMouse, WORD yMouse)
{
	/* Not all WM_MOUSEMOVE messages are genuine. */
	if(lpmwd->xMouseLast == xMouse && lpmwd->yMouseLast == yMouse)
		return 0;

	/* Set the selected cursor. This is fast if it's already selected. */
	SetCursor(lpmwd->hCursor);

	/* Depending on the submode, we process mouse moves differently. */
	switch(lpmwd->submode)
	{
	case ESM_NONE:
		/* Update the highlight (only redraws if necessary). Also updates the
		 * info-bar.
		 */
		UpdateHighlight(lpmwd, xMouse, yMouse);

		break;

	case ESM_MOVING:
		if(lpmwd->xMouseLast >= 0 && lpmwd->yMouseLast >= 0)
		{
			/* Pan the map. */
			lpmwd->mapview.xLeft += (lpmwd->xMouseLast - xMouse) / lpmwd->mapview.fZoom;
			lpmwd->mapview.yTop -= (lpmwd->yMouseLast - yMouse) / lpmwd->mapview.fZoom;

			lpmwd->bWantRedraw = TRUE;
		}

		break;

	case ESM_PREDRAG:
		if(lpmwd->hobjDrag.emType != EM_MOVE)
		{
			/* TODO: Move to its own function? */

			/* Create an undo marker, but don't clear the redo stack, since the
			 * user might cancel.
			 */
			CreateUndo(lpmwd, IDS_UNDO_DRAG);

			/* Convert to a selection of things and vertices in an extra
			 * structure, so as not to interfere with the real selection.
			 */
			CreateAuxiliarySelection(lpmwd);

			/* Mark all selected lines as dragged. */
			LabelSelectedLinesRS(lpmwd->lpmap);

			/* Find loops. */
			lpmwd->lplooplistRS = LabelRSLinesLoops(lpmwd->lpmap);

			/* Any lines inside any of our loops in their initial positions also
			 * need to be marked RS.
			 */
			LabelLoopedLinesRS(lpmwd->lpmap, lpmwd->lplooplistRS);

			/* Label any lines whose lengths might change. */
			LabelChangingLines(lpmwd->lpmap, lpmwd->selectionAux.lpsellistVertices);

			/* Enter the dragging state. */
			lpmwd->submode = ESM_DRAGGING;

			/* TODO: Capture mouse? */
		}
		else
		{
			/* We weren't pointing at anything when we clicked, so start
			 * lassoing.
			 */
			lpmwd->submode = ESM_SELECTING;
			lpmwd->bWantRedraw = TRUE;

			/* Don't fall through! */
			break;
		}

		/* Fall through if we were pointing at something. */

	case ESM_PASTING:
	case ESM_DRAGGING:
		{
			short xDragNew = XMOUSE_WIN_TO_MAP(xMouse, lpmwd);
			short yDragNew = YMOUSE_WIN_TO_MAP(yMouse, lpmwd);
			int i;

			/* Do the snapping, first of all. */
			short xUnsnapped = 0, yUnsnapped = 0;
			short xSnapped = 0, ySnapped = 0;

			/* Find the unsnapped opsition for the first selected item. */
			if(lpmwd->selectionAux.lpsellistVertices->iDataCount > 0)
			{
				xUnsnapped = lpmwd->lpmap->vertices[lpmwd->selectionAux.lpsellistVertices->lpiIndices[0]].x + xDragNew - lpmwd->xDragLast;
				yUnsnapped = lpmwd->lpmap->vertices[lpmwd->selectionAux.lpsellistVertices->lpiIndices[0]].y + yDragNew - lpmwd->yDragLast;
			}
			else if(lpmwd->selectionAux.lpsellistThings->iDataCount > 0)
			{
				xUnsnapped = lpmwd->lpmap->things[lpmwd->selectionAux.lpsellistThings->lpiIndices[0]].x + xDragNew - lpmwd->xDragLast;
				yUnsnapped = lpmwd->lpmap->things[lpmwd->selectionAux.lpsellistThings->lpiIndices[0]].y + yDragNew - lpmwd->yDragLast;
			}

			/* Calculate the corresponding snapped position. */
			xSnapped = xUnsnapped;
			ySnapped = yUnsnapped;
			RequiredSnapping(lpmwd->lpmap, &xSnapped, &ySnapped, &lpmwd->mapview, lpmwd->bySnapFlags ^ lpmwd->bySnapToggleMask, lpmwd->submode, -1);

			/* Find the difference between the two. */
			xDragNew += xSnapped - xUnsnapped;
			yDragNew += ySnapped - yUnsnapped;

			/* If we moved far enough to require moving stuff (i.e. far enough
			 * so that snapping doesn't keep us in place), update positions.
			 */
			if(xDragNew - lpmwd->xDragLast != 0 || yDragNew - lpmwd->yDragLast != 0)
			{
				/* Displace all selected vertices and things by the appropriate
				 * amount.
				 */
				for(i = 0; i < lpmwd->selectionAux.lpsellistVertices->iDataCount; i++)
				{
					lpmwd->lpmap->vertices[lpmwd->selectionAux.lpsellistVertices->lpiIndices[i]].x += xDragNew - lpmwd->xDragLast;
					lpmwd->lpmap->vertices[lpmwd->selectionAux.lpsellistVertices->lpiIndices[i]].y += yDragNew - lpmwd->yDragLast;
				}

				for(i = 0; i < lpmwd->selectionAux.lpsellistThings->iDataCount; i++)
				{
					lpmwd->lpmap->things[lpmwd->selectionAux.lpsellistThings->lpiIndices[i]].x += xDragNew - lpmwd->xDragLast;
					lpmwd->lpmap->things[lpmwd->selectionAux.lpsellistThings->lpiIndices[i]].y += yDragNew - lpmwd->yDragLast;
				}

				/* Update stored co-ordinates. */
				lpmwd->xDragLast = xDragNew;
				lpmwd->yDragLast = yDragNew;

				lpmwd->bWantRedraw = TRUE;
			}
		}

		break;

	case ESM_SELECTING:
		
		/* Adjust selection. Update info-bar if necessary. */
		if(UpdateLassoedSelection(lpmwd, xMouse, yMouse))
			MapWinUpdateInfoBar(lpmwd);

		lpmwd->bWantRedraw = TRUE;
		
		break;

	case ESM_DRAWING:
	case ESM_INSERTING:
	case ESM_ROTATING:
		/* Always redraw in these cases. */
		lpmwd->bWantRedraw = TRUE;
		break;

	default:
        break;
	}

	/* Was the mouse not previously inside the window? */
	if(lpmwd->xMouseLast < 0 || lpmwd->yMouseLast < 0)
	{
		/* If so, track the pointer until it leaves. */
		TRACKMOUSEEVENT tme;

		tme.cbSize = sizeof(tme);
		tme.dwFlags = TME_LEAVE;
		tme.hwndTrack = lpmwd->hwnd;

		TrackMouseEvent(&tme);
	}

	/* Save the cursor position. */
	lpmwd->xMouseLast = xMouse;
	lpmwd->yMouseLast = yMouse;

	UpdateStatusBar(lpmwd, SBPF_COORD);

	/* Make window proc return 0. */
	return 0;
}


/* LButtonDown
 *   Perform map window's WM_LBUTTONDOWN processing.
 *
 * Parameters:
 *   MAPWINDATA*	lpmwd			Pointer to data for window.
 *   WORD			xMouse, yMouse	Mouse co-ordinates.
 *   WORD			wFlags			Flags corresponding to WM_LBUTTONDOWN's
 *									wParam.
 *
 * Return value: LRESULT
 *   Value window procedure should return.
 */
static LRESULT LButtonDown(MAPWINDATA *lpmwd, WORD xMouse, WORD yMouse, WORD wFlags)
{
	UNREFERENCED_PARAMETER(xMouse);
	UNREFERENCED_PARAMETER(yMouse);
	UNREFERENCED_PARAMETER(wFlags);

	switch(lpmwd->submode)
	{
	case ESM_NONE:
		/* Normal processing if we're not doing anything special. */

		if(lpmwd->editmode == EM_MOVE)
		{
			StartPan(lpmwd);
		}
		else
		{
			/* Remember where we started out. */
			InitEdgeScrollBoundaries(lpmwd, xMouse, yMouse);

			/* Select/deselect an object if we're pointing at one. */
			if(lpmwd->highlightobj.emType != EM_MOVE)
			{
				/* Logical XOR for additive selection. */
				BOOL bAdditive = ((wFlags & MK_CONTROL) || (wFlags & MK_SHIFT)) != !!ConfigGetInteger(g_lpcfgMain, "additiveselect");

				/* If object isn't selected, add it to selection; signal to
				 * remove it when we mouseup (without moving), otherwise.
				 */
				if(!ObjectSelected(lpmwd, &lpmwd->highlightobj))
				{
					if(!bAdditive)
						DeselectAll(lpmwd);

					AddObjectToSelection(lpmwd, &lpmwd->highlightobj);
					lpmwd->bDeselectAfterEdit = TRUE;
					lpmwd->bDeselectOnMouseUp = FALSE;
				}
				else
				{
					lpmwd->bDeselectAfterEdit = FALSE;
					lpmwd->bDeselectOnMouseUp = bAdditive;
				}
			}
			else
			{
				/* Remember where we started out. */
				lpmwd->ptsSelectionStart.x = (short)xMouse;
				lpmwd->ptsSelectionStart.y = (short)yMouse;
			}
			
			/* If we're not pointing at anything, this acts as a prelude to
			 * lassoing.
			 */
			BeginPreDrag(lpmwd);
		}

		break;

	case ESM_MOVING:
		/* Stop panning. */
		EndPan(lpmwd);
		break;

	case ESM_PASTING:

		/* Always deselect following a prefab insertion. */
		lpmwd->bDeselectAfterEdit = TRUE;

		/* This also clears the redo stack. */
		EndDragOperation(lpmwd);

		break;

	case ESM_ROTATING:
		CreateUndo(lpmwd, IDS_UNDO_ROTATETHINGS);
		ClearRedoStack(lpmwd);

		/* Make all the selected things face the mouse. */
		RotateSelectedThingsToPoint(lpmwd, XMOUSE_WIN_TO_MAP(lpmwd->xMouseLast, lpmwd), YMOUSE_WIN_TO_MAP(lpmwd->yMouseLast, lpmwd));

		/* Back to normal. */
		lpmwd->submode = ESM_NONE;
		
		lpmwd->bMapChanged = TRUE;
		lpmwd->bWantRedraw = TRUE;
		MapWinUpdateInfoBar(lpmwd);

		break;

	default:
		break;
	}

	return 0;
}


/* LButtonDblClk
 *   Perform map window's WM_LBUTTONDBLCLK processing.
 *
 * Parameters:
 *   MAPWINDATA*	lpmwd			Pointer to data for window.
 *   WORD			xMouse, yMouse	Mouse co-ordinates.
 *   WORD			wFlags			Flags corresponding to WM_LBUTTONDOWN's
 *									wParam.
 *
 * Return value: LRESULT
 *   Value window procedure should return.
 */
static LRESULT LButtonDblClk(MAPWINDATA *lpmwd, WORD xMouse, WORD yMouse, WORD wFlags)
{
	UNREFERENCED_PARAMETER(xMouse);
	UNREFERENCED_PARAMETER(yMouse);
	UNREFERENCED_PARAMETER(wFlags);

	switch(lpmwd->submode)
	{
	case ESM_NONE:
		/* Normal processing if we're not doing anything special. */

		if(lpmwd->editmode != EM_MOVE)
		{
			/* Open properties if we're pointing at anything. */
			if(lpmwd->highlightobj.emType != EM_MOVE)
				ShowSelectionProperties(lpmwd, TRUE);
			/* If we're not pointing, start inserting. */
			else BeginInsertOperation(lpmwd);
		}

		break;

	case ESM_DRAWING:
		/* Just stop drawing. */
		EndDrawOperation(lpmwd->lpmap, &lpmwd->drawop);

		ClearRedoStack(lpmwd);
		lpmwd->bMapChanged = TRUE;

		UpdateStatusBar(lpmwd, SBPF_LINEDEFS | SBPF_SIDEDEFS | SBPF_VERTICES | SBPF_SECTORS);
		UpdateHighlight(lpmwd, lpmwd->xMouseLast, lpmwd->yMouseLast);
		MapWinUpdateInfoBar(lpmwd);

		lpmwd->submode = ESM_NONE;
		lpmwd->bWantRedraw = TRUE;
		break;

	default:
		break;
	}
	return 0;
}


/* LButtonUp
 *   Perform map window's WM_LBUTTONUP processing.
 *
 * Parameters:
 *   MAPWINDATA*	lpmwd			Pointer to data for window.
 *   WORD			xMouse, yMouse	Mouse co-ordinates.
 *   WORD			wFlags			Flags corresponding to WM_LBUTTONUP's
 *									wParam.
 *
 * Return value: LRESULT
 *   Value window procedure should return.
 */
static LRESULT LButtonUp(MAPWINDATA *lpmwd, WORD xMouse, WORD yMouse, WORD wFlags)
{
	UNREFERENCED_PARAMETER(wFlags);
	switch(lpmwd->submode)
	{
	case ESM_MOVING:
		/* Stop panning. */
		EndPan(lpmwd);
		break;

	case ESM_INSERTING:
		{
			short x = (short)XMOUSE_WIN_TO_MAP(xMouse, lpmwd);
			short y = (short)YMOUSE_WIN_TO_MAP(yMouse, lpmwd);

			RequiredSnapping(lpmwd->lpmap, &x, &y, &lpmwd->mapview, lpmwd->bySnapFlags ^ lpmwd->bySnapToggleMask, lpmwd->submode, -1);

			/* Undo is handled in here. */
			DoInsertOperation(lpmwd, x, y);
			UpdateHighlight(lpmwd, lpmwd->xMouseLast, lpmwd->yMouseLast);
			lpmwd->bWantRedraw = TRUE;
		}

		break;

	case ESM_DRAWING:
		{
			short x = (short)XMOUSE_WIN_TO_MAP(xMouse, lpmwd);
			short y = (short)YMOUSE_WIN_TO_MAP(yMouse, lpmwd);

			RequiredSnapping(lpmwd->lpmap, &x, &y, &lpmwd->mapview, lpmwd->bySnapFlags ^ lpmwd->bySnapToggleMask, lpmwd->submode,
				(lpmwd->submode == ESM_DRAWING && lpmwd->drawop.iNewVertexCount > 0) ? lpmwd->drawop.lpiNewVertices[lpmwd->drawop.iNewVertexCount - 1] : -1);

			/* Draw the next vertex. */
			if(!DrawToNewVertex(lpmwd->lpmap, &lpmwd->drawop, x, y, lpmwd->bStitchMode))
			{
				/* Drawing this vertex finishes the drawing operation. */
				EndDrawOperation(lpmwd->lpmap, &lpmwd->drawop);
				lpmwd->submode = ESM_NONE;

				ClearRedoStack(lpmwd);
				lpmwd->bMapChanged = TRUE;

				UpdateStatusBar(lpmwd, SBPF_LINEDEFS | SBPF_SIDEDEFS | SBPF_VERTICES | SBPF_SECTORS);
			}

			lpmwd->bWantRedraw = TRUE;
		}

		break;

	case ESM_PREDRAG:

		/* We left-clicked an object without moving. It was selected at
		 * mousedown if necessary; now we deselect it if we have to. If nothing
		 * was selected anyway (right-clicked in empty space with context menus
		 * on), then no harm is done.
		 */
		if(lpmwd->hobjDrag.emType != EM_MOVE)
		{
			if(lpmwd->bDeselectOnMouseUp)
				RemoveObjectFromSelection(lpmwd, &lpmwd->hobjDrag);
		}		
		/* Not pointing at anything. */
		else if(ConfigGetInteger(g_lpcfgMain, "nothingdeselects"))
		{
			DeselectAll(lpmwd);
			MapWinUpdateInfoBar(lpmwd);
			lpmwd->bWantRedraw = TRUE;
		}

		lpmwd->submode = ESM_NONE;

		/* Update the highlight since we lost it in the pre-drag op. */
		UpdateHighlight(lpmwd, xMouse, yMouse);

		break;

	case ESM_DRAGGING:

		/* This also clears the redo stack. */
		EndDragOperation(lpmwd);

		break;

	case ESM_SELECTING:
		EndSelectOperation(lpmwd);
		lpmwd->bWantRedraw = TRUE;
		break;

	default:
        break;
	}

	return 0;
}



/* RButtonUp
 *   Perform map window's WM_RBUTTONUP processing.
 *
 * Parameters:
 *   MAPWINDATA*	lpmwd			Pointer to data for window.
 *   WORD			xMouse, yMouse	Mouse co-ordinates.
 *   WORD			wFlags			Flags corresponding to WM_RBUTTONUP's
 *									wParam.
 *
 * Return value: LRESULT
 *   Value window procedure should return.
 */
static LRESULT RButtonUp(MAPWINDATA *lpmwd, WORD xMouse, WORD yMouse, WORD wFlags)
{
	UNREFERENCED_PARAMETER(wFlags);
	switch(lpmwd->submode)
	{
	case ESM_INSERTING:
		{
			short x = (short)XMOUSE_WIN_TO_MAP(xMouse, lpmwd);
			short y = (short)YMOUSE_WIN_TO_MAP(yMouse, lpmwd);

			RequiredSnapping(lpmwd->lpmap, &x, &y, &lpmwd->mapview, lpmwd->bySnapFlags ^ lpmwd->bySnapToggleMask, lpmwd->submode, -1);

			/* Undo is handled in here. */
			DoInsertOperation(lpmwd, x, y);
			UpdateHighlight(lpmwd, lpmwd->xMouseLast, lpmwd->yMouseLast);
			lpmwd->bWantRedraw = TRUE;
		}

		break;

	case ESM_SELECTING:
		EndSelectOperation(lpmwd);
		/* Fall through. */

	case ESM_PREDRAG:
		/* If we get a mouse-up while still in this state, then the mouse didn't
		 * move (otherwise we'd've moved into ESM_DRAGGING). Hence, we should
		 * show the selection's properties.
		 */

		/* Restore normal behaviour, and do so *before* the rest of the
		 * processing, since some of it requires this.
		 */
		lpmwd->submode = ESM_NONE;

		/* Selection properties. Display a context menu if the user likes
		 * that sort of thing; otherwise go straight to properties dialogue.
		 */
		if(ConfigGetInteger(g_lpcfgMain, "contextmenus"))
		{
			/* Context menus. These depend on the mode *and* the nature of
			 * the selected objects, if indeed there are any.
			 */

			ENUM_POPUPMENUS popupmenu = POPUP_NOSEL;
			WORD wID;
			TRACKMOUSEEVENT tme;
			HMENU hmenu;
			POINT ptMouse = {xMouse, yMouse};

			lpmwd->ptsPopupMenuLast.x = (short)ptMouse.x;
			lpmwd->ptsPopupMenuLast.y = (short)ptMouse.y;

			/* TODO: Needed for animation? Check on ME. The flags aren't
			 * defined for WINVER < 0x0500, though...
			 */
			/* Some locales (?) have right-aligned menus. */
			/*UINT uiExtraFlags = GetSystemMetrics(SM_MENUDROPALIGNMENT) ? (TPM_RIGHTALIGN | TPM_HORNEGANIMATION) : (TPM_LEFTALIGN | TPM_HORPOSANIMATION);*/

			if(SelectionCount(lpmwd) > 0)
			{
				/* In 'any' mode, need to check whether we have several types of
				 * object selected.
				 */
				switch(lpmwd->editmode)
				{
				case EM_ANY:
					if(IsSelectionHomogeneous(lpmwd))
					{
						/* Do sectors before lines, since when sectors are
						 * selected, their lines are, too.
						 */
						if(lpmwd->selection.lpsellistSectors->iDataCount > 0)
							popupmenu = POPUP_SECTORS;
						else if(lpmwd->selection.lpsellistLinedefs->iDataCount > 0)
							popupmenu = POPUP_LINES;
						else if(lpmwd->selection.lpsellistThings->iDataCount > 0)
							popupmenu = POPUP_THINGS;
						else if(lpmwd->selection.lpsellistVertices->iDataCount > 0)
							popupmenu = POPUP_VERTICES;
					}
					else popupmenu = POPUP_COMMON;

					break;

				case EM_SECTORS:
					popupmenu = POPUP_SECTORS;
					break;

				case EM_LINES:
					popupmenu = POPUP_LINES;
					break;

				case EM_THINGS:
					popupmenu = POPUP_THINGS;
					break;

				case EM_VERTICES:
					popupmenu = POPUP_VERTICES;
					break;

				default:
					break;
				}
			}
			/* If there's nothing selected, we leave popupmenu as
			 * POPUP_NOSEL.
			 */

			/* Preprocess the menu, enabling and disabling appropriate items. */
			hmenu = GetSubMenu(g_hmenuPopups, popupmenu);
			switch(popupmenu)
			{
			case POPUP_NOSEL:
				EnableMenuItem(hmenu, IDM_POPUP_POLY, MF_BYCOMMAND | ((lpmwd->editmode == EM_ANY || lpmwd->editmode == EM_SECTORS || lpmwd->editmode == EM_LINES) ? MF_ENABLED : MF_GRAYED));
				EnableMenuItem(hmenu, IDM_POPUP_DRAW, MF_BYCOMMAND | ((lpmwd->editmode == EM_ANY || lpmwd->editmode == EM_SECTORS || lpmwd->editmode == EM_LINES) ? MF_ENABLED : MF_GRAYED));
				EnableMenuItem(hmenu, IDM_POPUP_VERTEX, MF_BYCOMMAND | ((lpmwd->editmode == EM_ANY || lpmwd->editmode == EM_VERTICES) ? MF_ENABLED : MF_GRAYED));
				EnableMenuItem(hmenu, IDM_POPUP_THING, MF_BYCOMMAND | ((lpmwd->editmode == EM_ANY || lpmwd->editmode == EM_THINGS) ? MF_ENABLED : MF_GRAYED));
				break;

			default:
				break;
			}

			/* Cancel mouse-tracking -- we get spurious WM_MOUSELEAVE
			 * messages otherwise.
			 */
			tme.cbSize = sizeof(tme);
			tme.dwFlags = TME_CANCEL | TME_LEAVE;
			tme.hwndTrack = lpmwd->hwnd;
			TrackMouseEvent(&tme);


			/* Show the menu! We get the return code and process sending
			 * WM_COMMAND message ourselves, though, since TPM posts it
			 * rather than sends, which is no use to us, as we deselect
			 * after calling TPM and hence before the message would be
			 * processed.
			 */
			ClientToScreen(lpmwd->hwnd, &ptMouse);
			wID = TrackPopupMenu(hmenu,
				TPM_NONOTIFY | TPM_RETURNCMD | TPM_TOPALIGN | TPM_RIGHTBUTTON /*| TPM_VERPOSANIMATION | uiExtraFlags*/,
				ptMouse.x, ptMouse.y,
				0, lpmwd->hwnd, NULL);

			if(wID)
				SendMessage(lpmwd->hwnd, WM_COMMAND, MAKELONG(wID, 0), (LPARAM)NULL);

			/* Fake the mouse position, so we don't jump. */
			GetCursorPos(&ptMouse);
			ScreenToClient(lpmwd->hwnd, &ptMouse);
			lpmwd->xMouseLast = (short)ptMouse.x;
			lpmwd->yMouseLast = (short)ptMouse.y;
			UpdateHighlight(lpmwd, lpmwd->xMouseLast, lpmwd->yMouseLast);

			/* Reinstate mouse tracking. */
			tme.dwFlags = TME_LEAVE;
			TrackMouseEvent(&tme);
		}
		else if(SelectionCount(lpmwd) > 0)
		{
			/* No context menus. Go straight to the properties. Undo is all
			 * handled in here.
			 */
			ShowSelectionProperties(lpmwd, TRUE);
		}

		/* Were the objects selected before we clicked on them? If not, deselect
		 * them.
		 */
		if(lpmwd->bDeselectAfterEdit) DeselectAll(lpmwd);

		lpmwd->bWantRedraw = TRUE;

		break;

	case ESM_DRAGGING:

		/* This also clears the redo stack. */
		EndDragOperation(lpmwd);

		break;

	/* Nothing happens in ESM_NONE: We're never pointing at anything, since that
	 * would have triggered a move to ESM_PREDRAG at the mouse-down stage.
	 */

	default:
	    break;
	}

	return 0;
}


/* RButtonDown
 *   Perform map window's WM_RBUTTONDOWN processing.
 *
 * Parameters:
 *   MAPWINDATA*	lpmwd			Pointer to data for window.
 *   WORD			xMouse, yMouse	Mouse co-ordinates.
 *   WORD			wFlags			Flags corresponding to WM_RBUTTONDOWN's
 *									wParam.
 *
 * Return value: LRESULT
 *   Value window procedure should return.
 */
static LRESULT RButtonDown(MAPWINDATA *lpmwd, WORD xMouse, WORD yMouse, WORD wFlags)
{
	UNREFERENCED_PARAMETER(xMouse);
	UNREFERENCED_PARAMETER(yMouse);

	switch(lpmwd->submode)
	{
	case ESM_NONE:
		/* If we're holding Ctrl, always insert a sector. */
		if(wFlags & MK_CONTROL)
		{
			short x = (short)XMOUSE_WIN_TO_MAP(lpmwd->xMouseLast, lpmwd);
			short y = (short)YMOUSE_WIN_TO_MAP(lpmwd->yMouseLast, lpmwd);

			RequiredSnapping(lpmwd->lpmap, &x, &y, &lpmwd->mapview, lpmwd->bySnapFlags ^ lpmwd->bySnapToggleMask, lpmwd->submode, -1);

			DeselectAll(lpmwd);
			InsertPolygonalSector(lpmwd, x, y);
			lpmwd->bWantRedraw = TRUE;
		}
		else
		{
			InitEdgeScrollBoundaries(lpmwd, xMouse, yMouse);

			/* Not holding Ctrl. Check whether we're not pointing at
			 * anything.
			 */
			if(lpmwd->highlightobj.emType == EM_MOVE)
			{
				if(ConfigGetInteger(g_lpcfgMain, "contextmenus"))
				{
					/* Remember where we started out. */
					lpmwd->ptsSelectionStart.x = (short)xMouse;
					lpmwd->ptsSelectionStart.y = (short)yMouse;
					lpmwd->bDeselectAfterEdit = FALSE;

					DeselectAll(lpmwd);
					BeginPreDrag(lpmwd);
				}
				else
				{
					/* Begin insertion, of a type proper to the mode. */
					BeginInsertOperation(lpmwd);
				}
			}
			else
			{
				/* We are pointing at something, so enter into the I'm-either-going-
				 * to-drag-you-or-alter-your-properties state, after altering the
				 * selection as necessary.
				 */

				/* If the object was already selected, great; otherwise clear the
				 * existing selection and then select the new object.
				 */
				if(!ObjectSelected(lpmwd, &lpmwd->highlightobj))
				{
					DeselectAll(lpmwd);
					AddObjectToSelection(lpmwd, &lpmwd->highlightobj);
					lpmwd->bDeselectAfterEdit = TRUE;
				}
				else
					lpmwd->bDeselectAfterEdit = FALSE;

				BeginPreDrag(lpmwd);
			}
		}

		break;

	case ESM_DRAWING:
		/* Just stop drawing. */
		EndDrawOperation(lpmwd->lpmap, &lpmwd->drawop);

		ClearRedoStack(lpmwd);
		lpmwd->bMapChanged = TRUE;

		UpdateStatusBar(lpmwd, SBPF_LINEDEFS | SBPF_SIDEDEFS | SBPF_VERTICES | SBPF_SECTORS);
		UpdateHighlight(lpmwd, lpmwd->xMouseLast, lpmwd->yMouseLast);
		MapWinUpdateInfoBar(lpmwd);

		lpmwd->submode = ESM_NONE;
		lpmwd->bWantRedraw = TRUE;
		break;

	default:
        break;
	}

	return 0;
}


/* ObjectSelected
 *   Determines whether a certain object is selected.
 *
 * Parameters:
 *   MAPWINDATA*	lpmwd			Pointer to data for window.
 *   HIGHLIGHTOBJ*	lphighlightobj	Type and index of object.
 *
 * Return value: BOOL
 *   True if the object is selected; FALSE otherwise.
 */
static BOOL ObjectSelected(MAPWINDATA *lpmwd, HIGHLIGHTOBJ *lphighlightobj)
{
	switch(lphighlightobj->emType)
	{
	case EM_SECTORS:
		return ExistsInSelectionList(lpmwd->selection.lpsellistSectors, lphighlightobj->iIndex);
	case EM_LINES:
		return ExistsInSelectionList(lpmwd->selection.lpsellistLinedefs, lphighlightobj->iIndex);
	case EM_VERTICES:
		return ExistsInSelectionList(lpmwd->selection.lpsellistVertices, lphighlightobj->iIndex);
	case EM_THINGS:
		return ExistsInSelectionList(lpmwd->selection.lpsellistThings, lphighlightobj->iIndex);
	default:
        break;
	}

	return FALSE;
}



/* AddObjectToSelection
 *   Selects an object.
 *
 * Parameters:
 *   MAPWINDATA*	lpmwd			Pointer to data for window.
 *   HIGHLIGHTOBJ*	lphighlightobj	Type and index of object.
 *
 * Return value: None.
 *
 * Remarks:
 *   The selection fields in the map data are also updated.
 */
static void AddObjectToSelection(MAPWINDATA *lpmwd, HIGHLIGHTOBJ *lphighlightobj)
{
	MAP *lpmap = lpmwd->lpmap;

	switch(lphighlightobj->emType)
	{
	case EM_SECTORS:
		{
			int i;

			AddToSelectionList(lpmwd->selection.lpsellistSectors, lphighlightobj->iIndex);
			lpmap->sectors[lphighlightobj->iIndex].selected = SLF_SELECTED;

			/* Select and mark linedefs for this sector, but don't trigger any
			 * of the effects that would occur if the user selected them
			 * manually.
			 */
			for(i = 0; i < lpmap->iLinedefs; i++)
			{
				if((lpmap->linedefs[i].s1 >= 0 && lpmap->sidedefs[lpmap->linedefs[i].s1].sector == lphighlightobj->iIndex) ||
					(lpmap->linedefs[i].s2 >= 0 && lpmap->sidedefs[lpmap->linedefs[i].s2].sector == lphighlightobj->iIndex))
				{
					AddToSelectionList(lpmwd->selection.lpsellistLinedefs, i);
					lpmap->linedefs[i].selected = SLF_SELECTED;
				}
			}
		}

		break;

	case EM_LINES:

		{
			int iFrontSec, iBackSec;
			int i;
			BOOL bAddFront, bAddBack;

			if(!lpmap->linedefs[lphighlightobj->iIndex].selected)
			{
				AddToSelectionList(lpmwd->selection.lpsellistLinedefs, lphighlightobj->iIndex);
				lpmap->linedefs[lphighlightobj->iIndex].selected = SLF_SELECTED;
			}

			/* Did adding this linedef cause a complete sector to be selected?
			 * This takes a bit of working out, but if it did, add it. We need
			 * to check both front and back sectors. Cf. SelectSectorsFromLines,
			 * which rebuilds the whole lot; this is more efficient, though.
			 */
			iFrontSec = lpmap->linedefs[lphighlightobj->iIndex].s1 >= 0 ? lpmap->sidedefs[lpmap->linedefs[lphighlightobj->iIndex].s1].sector : -1;
			iBackSec = lpmap->linedefs[lphighlightobj->iIndex].s2 >= 0 ? lpmap->sidedefs[lpmap->linedefs[lphighlightobj->iIndex].s2].sector : -1;

			/* Assume that we add them, if they exist. */
			bAddFront = (iFrontSec >= 0);
			bAddBack = (iBackSec >= 0);

			/* Look through all sidedefs attached to linedefs; if we find an
			 * unselected sd referencing one of our sectors, we don't add it.
			 */
			for(i = 0; (bAddFront || bAddBack) && i < lpmap->iLinedefs; i++)
			{
				MAPLINEDEF *lpld = &lpmap->linedefs[i];
				if(!lpld->selected)
				{
					if(lpld->s1 >= 0)
					{
						int iSector = lpmap->sidedefs[lpld->s1].sector;
						if(iSector == iFrontSec) bAddFront = FALSE;
						if(iSector == iBackSec) bAddBack = FALSE;
					}

					if(lpld->s2 >= 0)
					{
						int iSector = lpmap->sidedefs[lpld->s2].sector;
						if(iSector == iFrontSec) bAddFront = FALSE;
						if(iSector == iBackSec) bAddBack = FALSE;
					}
				}
			}

			/* Add the sectors if we should. */
			if(bAddFront)
			{
				AddToSelectionList(lpmwd->selection.lpsellistSectors, iFrontSec);
				lpmap->sectors[iFrontSec].selected = SLF_SELECTED;
			}

			if(bAddBack)
			{
				AddToSelectionList(lpmwd->selection.lpsellistSectors, iBackSec);
				lpmap->sectors[iBackSec].selected = SLF_SELECTED;
			}
		}

		break;

	case EM_VERTICES:

		AddToSelectionList(lpmwd->selection.lpsellistVertices, lphighlightobj->iIndex);
		lpmap->vertices[lphighlightobj->iIndex].selected = SLF_SELECTED;
		break;

	case EM_THINGS:

		AddToSelectionList(lpmwd->selection.lpsellistThings, lphighlightobj->iIndex);
		lpmap->things[lphighlightobj->iIndex].selected = SLF_SELECTED;
		break;

	default:
        break;
	}
}



/* RemoveObjectFromSelection
 *   Deselects an object.
 *
 * Parameters:
 *   MAPWINDATA*	lpmwd			Pointer to data for window.
 *   HIGHLIGHTOBJ*	lphighlightobj	Type and index of object.
 *
 * Return value: BOOL
 *   TRUE if object was indeed previously selected; FALSE otherwise.
 *
 * Remarks:
 *   The selection fields in the map data are also updated.
 */
static BOOL RemoveObjectFromSelection(MAPWINDATA *lpmwd, HIGHLIGHTOBJ *lphighlightobj)
{
	BOOL bRet = FALSE;
	MAP *lpmap = lpmwd->lpmap;
	int i;

	switch(lphighlightobj->emType)
	{
	case EM_SECTORS:

		bRet = RemoveFromSelectionList(lpmwd->selection.lpsellistSectors, lphighlightobj->iIndex);
		lpmap->sectors[lphighlightobj->iIndex].selected = 0;

		/* Unmark linedefs for this sector. */
		for(i = 0; i < lpmap->iLinedefs; i++)
		{
			/* This is not particularly transparent. We deselect the linedef iff
			 * it belongs to us and the sector on its opposite side is
			 * unselected.
			 */
			if(((lpmap->linedefs[i].s1 >= 0 && lpmap->sidedefs[lpmap->linedefs[i].s1].sector == lphighlightobj->iIndex) &&
				!(lpmap->linedefs[i].s2 >= 0 && ExistsInSelectionList(lpmwd->selection.lpsellistSectors, lpmap->sidedefs[lpmap->linedefs[i].s2].sector)))
			|| ((lpmap->linedefs[i].s2 >= 0 && lpmap->sidedefs[lpmap->linedefs[i].s2].sector == lphighlightobj->iIndex) &&
				!(lpmap->linedefs[i].s1 >= 0 && ExistsInSelectionList(lpmwd->selection.lpsellistSectors, lpmap->sidedefs[lpmap->linedefs[i].s1].sector))))
			{
				RemoveFromSelectionList(lpmwd->selection.lpsellistLinedefs, i);
				lpmap->linedefs[i].selected = 0;
			}
		}

		break;

	case EM_LINES:
		RemoveFromSelectionList(lpmwd->selection.lpsellistLinedefs, lphighlightobj->iIndex);
		lpmap->linedefs[lphighlightobj->iIndex].selected = 0;

		/* Remove any sectors that might have become unselected. We don't
		 * actually check whether they *were* selected: it wouldn't be any
		 * quicker.
		 */
		if(lpmap->linedefs[lphighlightobj->iIndex].s1 >= 0)
		{
			int iSector = lpmap->sidedefs[lpmap->linedefs[lphighlightobj->iIndex].s1].sector;
			if(iSector >= 0) RemoveFromSelectionList(lpmwd->selection.lpsellistSectors, iSector);
		}

		if(lpmap->linedefs[lphighlightobj->iIndex].s2 >= 0)
		{
			int iSector = lpmap->sidedefs[lpmap->linedefs[lphighlightobj->iIndex].s2].sector;
			if(iSector >= 0) RemoveFromSelectionList(lpmwd->selection.lpsellistSectors, iSector);
		}

		break;
	case EM_VERTICES:
		RemoveFromSelectionList(lpmwd->selection.lpsellistVertices, lphighlightobj->iIndex);
		lpmap->vertices[lphighlightobj->iIndex].selected = 0;
		break;
	case EM_THINGS:
		RemoveFromSelectionList(lpmwd->selection.lpsellistThings, lphighlightobj->iIndex);
		lpmap->things[lphighlightobj->iIndex].selected = 0;
		break;
	default:
        break;
	}

	return bRet;
}


/* SelectVerticesFromLines
 *   Selects those vertices belonging to selected lines.
 *
 * Parameters:
 *   MAPWINDATA*	lpmwd	Pointer to map window data.
 *
 * Return value: None.
 *
 * Remarks:
 *   Doesn't deselect any vertices that are already selected.
 */
static void SelectVerticesFromLines(MAPWINDATA *lpmwd)
{
	int i;

	for(i = 0; i < lpmwd->lpmap->iLinedefs; i++)
	{
		MAPLINEDEF *lpld = &lpmwd->lpmap->linedefs[i];

		if(lpld->selected)
		{
			AddToSelectionList(lpmwd->selection.lpsellistVertices, lpld->v1);
			lpmwd->lpmap->vertices[lpld->v1].selected = SLF_SELECTED;
			AddToSelectionList(lpmwd->selection.lpsellistVertices, lpld->v2);
			lpmwd->lpmap->vertices[lpld->v2].selected = SLF_SELECTED;
		}
	}
}


/* SelectLinesFromVertices
 *   Selects those lines whose vertices are both selected.
 *
 * Parameters:
 *   MAPWINDATA*	lpmwd	Pointer to map window data.
 *
 * Return value: None.
 *
 * Remarks:
 *   Doesn't deselect any lines that are already selected. Doesn't trigger
 *   selection of any sectors that should become selected.
 */
static void SelectLinesFromVertices(MAPWINDATA *lpmwd)
{
	int i;

	for(i = 0; i < lpmwd->lpmap->iLinedefs; i++)
	{
		MAPLINEDEF *lpld = &lpmwd->lpmap->linedefs[i];

		if(lpmwd->lpmap->vertices[lpld->v1].selected && lpmwd->lpmap->vertices[lpld->v2].selected)
		{
			AddToSelectionList(lpmwd->selection.lpsellistLinedefs, i);
			lpld->selected = SLF_SELECTED;
		}
	}
}


/* SelectSectorsFromLines
 *   Selects those sectors whose linedefs are all selected.
 *
 * Parameters:
 *   MAPWINDATA*	lpmwd	Pointer to map window data.
 *
 * Return value: None.
 *
 * Remarks:
 *   Doesn't deselect any sectors that are already selected.
 */
static void SelectSectorsFromLines(MAPWINDATA *lpmwd)
{
	MAP *lpmap = lpmwd->lpmap;
	int i;
	char *lpcSelectSector;

	/* Sanity check. */
	if(lpmap->iSectors == 0) return;

	/* Begin by indicating that it's possible to select all of the sectors. */
	lpcSelectSector = (char*)ProcHeapAlloc(lpmap->iSectors * sizeof(char));
	ZeroMemory(lpcSelectSector, lpmap->iSectors * sizeof(char));

	/* Consider all linedefs. */
	for(i = 0; i < lpmap->iLinedefs; i++)
	{
		MAPLINEDEF *lpld = &lpmap->linedefs[i];
		int iFrontSec = lpld->s1 >= 0 ? lpmwd->lpmap->sidedefs[lpld->s1].sector : -1;
		int iBackSec = lpld->s2 >= 0 ? lpmwd->lpmap->sidedefs[lpld->s2].sector : -1;

		if(!lpld->selected)
		{
			/* We definitely don't want to select any sectors belonging to this
			 * line.
			 */
			if(iFrontSec >= 0) lpcSelectSector[iFrontSec] = -1;
			if(iBackSec >= 0) lpcSelectSector[iBackSec] = -1;
		}
		else
		{
			/* SLF_LASSOING is 'stronger' in some sense than SLF_SELECTED, i.e.
			 * in that one lassoed linedef makes the whole sector lassoed and
			 * not really selected. If the sector was *already* selected,
			 * however, it won't be interfered with when we come to the
			 * appropriate stage.
			 */
			char cSelLevel = (lpld->selected & SLF_LASSOING) ? SLF_LASSOING : SLF_SELECTED;

			/* Unless we already know we can never select this sector, assume
			 * that we can select it.
			 */
			if(iFrontSec >= 0 && lpcSelectSector[iFrontSec] >= 0 && lpcSelectSector[iFrontSec] < cSelLevel)
				lpcSelectSector[iFrontSec] = cSelLevel;

			if(iBackSec >= 0 && lpcSelectSector[iBackSec] >= 0 && lpcSelectSector[iBackSec] < cSelLevel)
				lpcSelectSector[iBackSec] = cSelLevel;
		}
	}

	/* Select all the sectors that we ought to. */
	for(i = 0; i < lpmap->iSectors; i++)
	{
		if(lpcSelectSector[i] > 0 && !lpmap->sectors[i].selected)
		{
			AddToSelectionList(lpmwd->selection.lpsellistSectors, i);
			lpmap->sectors[i].selected |= lpcSelectSector[i];
		}
	}

	ProcHeapFree(lpcSelectSector);
}


/* DeselectAll
 *   Deselects all map objects.
 *
 * Parameters:
 *   MAPWINDATA*	lpmwd	Pointer to map window data.
 *
 * Return value: None.
 */
static void DeselectAll(MAPWINDATA *lpmwd)
{
	if(lpmwd->editmode == EM_ANY || lpmwd->editmode == EM_SECTORS || lpmwd->editmode == EM_LINES)
	{
		DeselectAllLines(lpmwd);
		DeselectAllSectors(lpmwd);
	}

	if(lpmwd->editmode == EM_ANY || lpmwd->editmode == EM_THINGS)
		DeselectAllThings(lpmwd);

	if(lpmwd->editmode == EM_ANY || lpmwd->editmode == EM_VERTICES)
		DeselectAllVertices(lpmwd);
}


/* DeselectAllBruteForce
 *   Deselects everything without trying to be clever. Useful for when the
 *   selection can be thrown out of sync, e.g. after an undo.
 *
 * Parameters:
 *   MAPWINDATA*	lpmwd	Pointer to map window data.
 *
 * Return value: None.
 */
static void DeselectAllBruteForce(MAPWINDATA *lpmwd)
{
	ClearSelectionList(lpmwd->selection.lpsellistSectors);
	ClearSelectionList(lpmwd->selection.lpsellistLinedefs);
	ClearSelectionList(lpmwd->selection.lpsellistThings);
	ClearSelectionList(lpmwd->selection.lpsellistVertices);

	ResetSelections(NULL, 0, NULL, 0, NULL, 0, lpmwd->lpmap->sectors, lpmwd->lpmap->iSectors);
	ResetSelections(NULL, 0, lpmwd->lpmap->linedefs, lpmwd->lpmap->iLinedefs, NULL, 0, NULL, 0);
	ResetSelections(lpmwd->lpmap->things, lpmwd->lpmap->iThings, NULL, 0, NULL, 0, NULL, 0);
	ResetSelections(NULL, 0, NULL, 0, lpmwd->lpmap->vertices, lpmwd->lpmap->iVertices, NULL, 0);
}


/* DeselectAllLines etc.
 *   Deselects all map objects of a specified type. Doesn't do any associated
 *   deselections, e.g. between lines and sectors.
 *
 * Parameters:
 *   MAPWINDATA*	lpmwd	Pointer to map window data.
 *
 * Return value: None.
 */
static void DeselectAllLines(MAPWINDATA *lpmwd)
{
	ClearSelectionList(lpmwd->selection.lpsellistSectors);
	ResetSelections(NULL, 0, NULL, 0, NULL, 0, lpmwd->lpmap->sectors, lpmwd->lpmap->iSectors);
}

static void DeselectAllSectors(MAPWINDATA *lpmwd)
{
	ClearSelectionList(lpmwd->selection.lpsellistLinedefs);
	ResetSelections(NULL, 0, lpmwd->lpmap->linedefs, lpmwd->lpmap->iLinedefs, NULL, 0, NULL, 0);
}

static void DeselectAllThings(MAPWINDATA *lpmwd)
{
	ClearSelectionList(lpmwd->selection.lpsellistThings);
	ResetSelections(lpmwd->lpmap->things, lpmwd->lpmap->iThings, NULL, 0, NULL, 0, NULL, 0);
}

static void DeselectAllVertices(MAPWINDATA *lpmwd)
{
	ClearSelectionList(lpmwd->selection.lpsellistVertices);
	ResetSelections(NULL, 0, NULL, 0, lpmwd->lpmap->vertices, lpmwd->lpmap->iVertices, NULL, 0);
}


/* DeselectLinesFromPartialSectors
 *   Deselects any lines that do not belong to a selected sector.
 *
 * Parameters:
 *   MAPWINDATA*	lpmwd	Pointer to map window data.
 *
 * Return value: None.
 */
static void DeselectLinesFromPartialSectors(MAPWINDATA *lpmwd)
{
	int i;

	for(i = 0; i < lpmwd->lpmap->iLinedefs; i++)
	{
		MAPLINEDEF *lpld = &lpmwd->lpmap->linedefs[i];

		if(lpld->selected)
		{
			int iFrontSec = lpld->s1 >= 0 ? lpmwd->lpmap->sidedefs[lpld->s1].sector : -1;
			int iBackSec = lpld->s2 >= 0 ? lpmwd->lpmap->sidedefs[lpld->s2].sector : -1;

			if((iFrontSec < 0 || !lpmwd->lpmap->sectors[iFrontSec].selected) &&
				(iBackSec < 0 || !lpmwd->lpmap->sectors[iBackSec].selected))
			{
				RemoveFromSelectionList(lpmwd->selection.lpsellistLinedefs, i);
				lpld->selected = 0;
			}
		}
	}
}

/* UpdateMapWindowTitle
 *   Updates a map window's title to include path and lumpname.
 *
 * Parameters:
 *   MAPWINDATA*	lpmwd	Pointer to map window data.
 *
 * Return value: None.
 */
static void UpdateMapWindowTitle(MAPWINDATA *lpmwd)
{
	LPTSTR	szTitle;
	TCHAR	szTail[12];		/* With lumpname corrected for ANSI/UNICODE-ness. */

	/* Allocate buffer for full path, and get it. */
	if(!WadIsNewByID(lpmwd->iWadID))
	{
		LPTSTR	szPath;
		WORD	cb;
		cb = GetWadFilename(lpmwd->iWadID, NULL, 0);
		szPath = (LPTSTR)ProcHeapAlloc(cb * sizeof(TCHAR));
		GetWadFilename(lpmwd->iWadID, szPath, cb);

		/* Allocate buffer for title (including tail), and get file title. */
		cb = GetFileTitle(szPath, NULL, 0);
		szTitle = (LPTSTR)ProcHeapAlloc((cb + strlen(lpmwd->szLumpName) + 3) * sizeof(TCHAR));
		GetFileTitle(szPath, szTitle, cb);
		ProcHeapFree(szPath);
	}
	else
	{
		szTitle = (LPTSTR)ProcHeapAlloc((UNTITLED_BUFLEN + strlen(lpmwd->szLumpName) + 3) * sizeof(TCHAR));
		LoadString(g_hInstance, IDS_UNTITLED, szTitle, UNTITLED_BUFLEN);
	}

	/* Get tail, including corrected lumpname. */
	wsprintf(szTail, TEXT(" - %hs"), lpmwd->szLumpName);
	lstrcat(szTitle, szTail);

	SetWindowText(lpmwd->hwnd, szTitle);

	ProcHeapFree(szTitle);
}


static void BuildSectorTexturePreviews(MAPWINDATA *lpmwd, SECTORDISPLAYINFO *lpsdi, DWORD dwDisplayFlags)
{
	/* Begin by assuming we need to destroy no textures. */
	lpsdi->dwDestroyTexFlags = 0;

	if(dwDisplayFlags & SDIF_CEILINGTEX)
	{
		if(GetTextureForMap(lpmwd->hwnd, lpsdi->szCeiling, &lpsdi->lptexCeiling, TF_FLAT))
			lpsdi->dwDestroyTexFlags |= SDIF_CEILINGTEX;
	}
	else
		lpsdi->lptexCeiling = NULL;

	if(dwDisplayFlags & SDIF_FLOORTEX)
	{
		if(GetTextureForMap(lpmwd->hwnd, lpsdi->szFloor, &lpsdi->lptexFloor, TF_FLAT))
			lpsdi->dwDestroyTexFlags |= SDIF_FLOORTEX;
	}
	else
		lpsdi->lptexFloor = NULL;
}


static void DestroySectorTexturePreviews(SECTORDISPLAYINFO *lpsdi, DWORD dwDisplayFlags)
{
	if(lpsdi->lptexCeiling && (dwDisplayFlags & SDIF_CEILINGTEX) && (lpsdi->dwDestroyTexFlags & SDIF_CEILINGTEX))
		DestroyTexture(lpsdi->lptexCeiling);

	if(lpsdi->lptexFloor && (dwDisplayFlags & SDIF_FLOORTEX) && (lpsdi->dwDestroyTexFlags & SDIF_FLOORTEX))
		DestroyTexture(lpsdi->lptexFloor);
}



static void BuildThingSpritePreview(MAPWINDATA *lpmwd, THINGDISPLAYINFO *lptdi, DWORD dwDisplayFlags)
{
	/* Begin by assuming we need to destroy no textures. */
	lptdi->dwDestroyTexFlags = 0;

	if(dwDisplayFlags & TDIF_SPRITE)
	{
		if(GetTextureForMap(lpmwd->hwnd, lptdi->szSprite, &lptdi->lptexSprite, TF_IMAGE))
			lptdi->dwDestroyTexFlags |= TDIF_SPRITE;
	}
	else
		lptdi->lptexSprite = NULL;
}


static void DestroyThingSpritePreview(THINGDISPLAYINFO *lptdi, DWORD dwDisplayFlags)
{
	if(lptdi->lptexSprite && (dwDisplayFlags & TDIF_SPRITE) && (lptdi->dwDestroyTexFlags & TDIF_SPRITE))
		DestroyTexture(lptdi->lptexSprite);
}



static void BuildSidedefTexturePreviews(MAPWINDATA *lpmwd, LINEDEFDISPLAYINFO *lplddi, DWORD dwDisplayFlags)
{
	/* Begin by assuming we need to destroy no textures. */
	lplddi->dwDestroyTexFlags = 0;

	/* Same for the pseudo- and blank textures. */
	lplddi->dwPseudoTexFlags = 0;
	lplddi->dwBlankTexFlags = 0;

	if(dwDisplayFlags & LDDIF_FRONTSD)
	{
		if(dwDisplayFlags & LDDIF_FRONTUPPER)
		{
			if(IsPseudoTexture(lplddi->szFrontUpper))
			{
				lplddi->lptexFrontUpper = NULL;
				lplddi->dwPseudoTexFlags |= LDDIF_FRONTUPPER;
			}
			else if(IsBlankTexture(lplddi->szFrontUpper))
			{
				lplddi->lptexFrontUpper = NULL;
				lplddi->dwBlankTexFlags |= LDDIF_FRONTUPPER;
			}
			else if(GetTextureForMap(lpmwd->hwnd, lplddi->szFrontUpper, &lplddi->lptexFrontUpper, TF_TEXTURE))
				lplddi->dwDestroyTexFlags |= LDDIF_FRONTUPPER;
		}
		else
			lplddi->lptexFrontUpper = NULL;

		if(dwDisplayFlags & LDDIF_FRONTMIDDLE)
		{
			if(IsPseudoTexture(lplddi->szFrontMiddle))
			{
				lplddi->lptexFrontMiddle = NULL;
				lplddi->dwPseudoTexFlags |= LDDIF_FRONTMIDDLE;
			}
			else if(IsBlankTexture(lplddi->szFrontMiddle))
			{
				lplddi->lptexFrontMiddle = NULL;
				lplddi->dwBlankTexFlags |= LDDIF_FRONTMIDDLE;
			}
			else if(GetTextureForMap(lpmwd->hwnd, lplddi->szFrontMiddle, &lplddi->lptexFrontMiddle, TF_TEXTURE))
				lplddi->dwDestroyTexFlags |= LDDIF_FRONTMIDDLE;
		}
		else
			lplddi->lptexFrontMiddle = NULL;

		if(dwDisplayFlags & LDDIF_FRONTLOWER)
		{
			if(IsPseudoTexture(lplddi->szFrontLower))
			{
				lplddi->lptexFrontLower = NULL;
				lplddi->dwPseudoTexFlags |= LDDIF_FRONTLOWER;
			}
			else if(IsBlankTexture(lplddi->szFrontLower))
			{
				lplddi->lptexFrontLower = NULL;
				lplddi->dwBlankTexFlags |= LDDIF_FRONTLOWER;
			}
			else if(GetTextureForMap(lpmwd->hwnd, lplddi->szFrontLower, &lplddi->lptexFrontLower, TF_TEXTURE))
				lplddi->dwDestroyTexFlags |= LDDIF_FRONTLOWER;
		}
		else
			lplddi->lptexFrontLower = NULL;
	}
	else lplddi->lptexFrontUpper = lplddi->lptexFrontMiddle = lplddi->lptexFrontLower = NULL;


	if(dwDisplayFlags & LDDIF_BACKSD)
	{
		if(dwDisplayFlags & LDDIF_BACKUPPER)
		{
			if(IsPseudoTexture(lplddi->szBackUpper))
			{
				lplddi->lptexBackUpper = NULL;
				lplddi->dwPseudoTexFlags |= LDDIF_BACKUPPER;
			}
			else if(IsBlankTexture(lplddi->szBackUpper))
			{
				lplddi->lptexBackUpper = NULL;
				lplddi->dwBlankTexFlags |= LDDIF_BACKUPPER;
			}
			else if(GetTextureForMap(lpmwd->hwnd, lplddi->szBackUpper, &lplddi->lptexBackUpper, TF_TEXTURE))
				lplddi->dwDestroyTexFlags |= LDDIF_BACKUPPER;
		}
		else
			lplddi->lptexBackUpper = NULL;

		if(dwDisplayFlags & LDDIF_BACKMIDDLE)
		{
			if(IsPseudoTexture(lplddi->szBackMiddle))
			{
				lplddi->lptexBackMiddle = NULL;
				lplddi->dwPseudoTexFlags |= LDDIF_BACKMIDDLE;
			}
			else if(IsBlankTexture(lplddi->szBackMiddle))
			{
				lplddi->lptexBackMiddle = NULL;
				lplddi->dwBlankTexFlags |= LDDIF_BACKMIDDLE;
			}
			else if(GetTextureForMap(lpmwd->hwnd, lplddi->szBackMiddle, &lplddi->lptexBackMiddle, TF_TEXTURE))
				lplddi->dwDestroyTexFlags |= LDDIF_BACKMIDDLE;
		}
		else
			lplddi->lptexBackMiddle = NULL;

		if(dwDisplayFlags & LDDIF_BACKLOWER)
		{
			if(IsPseudoTexture(lplddi->szBackLower))
			{
				lplddi->lptexBackLower = NULL;
				lplddi->dwPseudoTexFlags |= LDDIF_BACKLOWER;
			}
			else if(IsBlankTexture(lplddi->szBackLower))
			{
				lplddi->lptexBackLower = NULL;
				lplddi->dwBlankTexFlags |= LDDIF_BACKLOWER;
			}
			else if(GetTextureForMap(lpmwd->hwnd, lplddi->szBackLower, &lplddi->lptexBackLower, TF_TEXTURE))
				lplddi->dwDestroyTexFlags |= LDDIF_BACKLOWER;
		}
		else
			lplddi->lptexBackLower = NULL;
	}
	else lplddi->lptexBackUpper = lplddi->lptexBackMiddle = lplddi->lptexBackLower = NULL;
}


static void DestroySidedefTexturePreviews(LINEDEFDISPLAYINFO *lplddi, DWORD dwDisplayFlags)
{
	if(lplddi->lptexFrontUpper && (dwDisplayFlags & LDDIF_FRONTUPPER) && (lplddi->dwDestroyTexFlags & LDDIF_FRONTUPPER))
		DestroyTexture(lplddi->lptexFrontUpper);

	if(lplddi->lptexFrontMiddle && (dwDisplayFlags & LDDIF_FRONTMIDDLE) && (lplddi->dwDestroyTexFlags & LDDIF_FRONTMIDDLE))
		DestroyTexture(lplddi->lptexFrontMiddle);

	if(lplddi->lptexFrontLower && (dwDisplayFlags & LDDIF_FRONTLOWER) && (lplddi->dwDestroyTexFlags & LDDIF_FRONTLOWER))
		DestroyTexture(lplddi->lptexFrontLower);


	if(lplddi->lptexBackUpper && (dwDisplayFlags & LDDIF_BACKUPPER) && (lplddi->dwDestroyTexFlags & LDDIF_BACKUPPER))
		DestroyTexture(lplddi->lptexBackUpper);

	if(lplddi->lptexBackMiddle && (dwDisplayFlags & LDDIF_BACKMIDDLE) && (lplddi->dwDestroyTexFlags & LDDIF_BACKMIDDLE))
		DestroyTexture(lplddi->lptexBackMiddle);

	if(lplddi->lptexBackLower && (dwDisplayFlags & LDDIF_BACKLOWER) && (lplddi->dwDestroyTexFlags & LDDIF_BACKLOWER))
		DestroyTexture(lplddi->lptexBackLower);
}





/* GetTextureForMap
 *   Loads a texture from cache if possible, and then from a wad if not.
 *
 * Parameters:
 *   HWND			hwnd		Map window handle.
 *   LPCTSTR		szTexName	NUL-terminated lumpname of texture.
 *   TEXTURE**		lplptex		Buffer in which to return texture. NULL if not
 *								found.
 *   TEX_FORMAT		tf			Specifies whether texture or flat.
 *
 * Return value: BOOL
 *   TRUE if memory was allocated for the texture; FALSE if not.
 *
 * Remarks:
 *   The caller must call DestroyTexture if we return TRUE.
 */
BOOL GetTextureForMapW(HWND hwnd, LPCWSTR szTexNameW, TEXTURE **lplptex, TEX_FORMAT tf)
{
	CHAR szTexNameA[TEXNAME_BUFFER_LENGTH];
	WideCharToMultiByte(CP_ACP, 0, szTexNameW, -1, szTexNameA, sizeof(szTexNameA), NULL, NULL);
	return GetTextureForMapA(hwnd, szTexNameA, lplptex, tf);
}

BOOL GetTextureForMapA(HWND hwnd, LPCSTR szTexName, TEXTURE **lplptex, TEX_FORMAT tf)
{
	MAPWINDATA *lpmwd = (MAPWINDATA*)GetWindowLong(hwnd, GWL_USERDATA);
	TEXCACHE *lptcHdr;

	/* Search the appropriate cache first. The WaitForSingleObject call
	 * determines whether we can write to the cache.
	 */
	switch(tf)
	{
	case TF_FLAT:
		lptcHdr = &lpmwd->tcFlatsHdr;
		*lplptex = GetTextureFromCache(lptcHdr, szTexName);
		break;
	case TF_TEXTURE:
		lptcHdr = &lpmwd->tcTexturesHdr;
		*lplptex = GetTextureFromCache(lptcHdr, szTexName);
		break;
	default:
		/* We don't cache TF_IMAGEs. */
		lptcHdr = NULL;
		break;
	}

	if(lptcHdr)
		*lplptex = GetTextureFromCache(lptcHdr, szTexName);
	else
		*lplptex = NULL;

	/* Found in cache? */
	if(*lplptex)
	{
		/* We're finished, and we didn't allocate any memory. */
		return FALSE;
	}

	/* Fall back to wad-files, and add to the cache once found. */
	if(lpmwd->iAdditionalWadID >= 0)
	{
		*lplptex = LoadTexture(GetWad(lpmwd->iAdditionalWadID), szTexName, tf, lpmwd->rgbqPalette);
		if(lptcHdr && *lplptex) AddToTextureCache(lptcHdr, szTexName, *lplptex);
	}

	if(!(*lplptex) && lpmwd->iIWadID >= 0)
	{
		*lplptex = LoadTexture(lpmwd->lpwadIWad, szTexName, tf, lpmwd->rgbqPalette);
		if(lptcHdr && *lplptex) AddToTextureCache(lptcHdr, szTexName, *lplptex);
	}

	/* If the texture is non-null and an image, the caller needs to free it. */
	return *lplptex && tf == TF_IMAGE;
}


/* MapWinUpdateInfoBar
 *   Updates the info-bar to reflect selection or highlight.
 *
 * Parameters:
 *   MAPWINDATA*	lpmwd	Map window data.
 *
 * Return value: None.
 */
static void MapWinUpdateInfoBar(MAPWINDATA *lpmwd)
{
	/* Set the palette if necessary. */
	if(TexPreviewPaletteSetWindow() != lpmwd->hwnd)
	{
		InitAllTexPreviews(lpmwd->hwnd, lpmwd->rgbqPalette);
	}

	/* Assume we don't override the LHS. */
	InfoBarEnableDetailsPanels();

	switch(lpmwd->highlightobj.emType)
	{
	case EM_LINES:
		{
			LINEDEFDISPLAYINFO	lddi;
			CONFIG *lpcfgLinedefs = ConfigGetSubsection(lpmwd->lpcfgMap, "__ldtypesflat");

			ResetFlatPreviews();
			ResetSpritePreview();

			GetLinedefDisplayInfo(lpmwd->lpmap, lpcfgLinedefs, lpmwd->highlightobj.iIndex, &lddi);
			BuildSidedefTexturePreviews(lpmwd, &lddi, LDDIF_ALL);
			ShowLinesInfo(&lddi, LDDIF_ALL, RequiredTextures(lpmwd->lpmap, lpmwd->highlightobj.iIndex));
			DestroySidedefTexturePreviews(&lddi, LDDIF_ALL);
		}

		break;

	case EM_SECTORS:
		{
			SECTORDISPLAYINFO	sdi;
			CONFIG *lpcfgSectors = ConfigGetSubsection(lpmwd->lpcfgMap, "sectortypes");

			ResetSidedefPreviews();
			ResetSpritePreview();

			GetSectorDisplayInfo(lpmwd->lpmap, lpcfgSectors, lpmwd->highlightobj.iIndex, &sdi);
			BuildSectorTexturePreviews(lpmwd, &sdi, SDIF_ALL);
			ShowSectorInfo(&sdi, SDIF_ALL);
			DestroySectorTexturePreviews(&sdi, SDIF_ALL);
		}

		break;

	case EM_THINGS:
		{
			THINGDISPLAYINFO	tdi;
			CONFIG *lpcfgThings = ConfigGetSubsection(lpmwd->lpcfgMap, FLAT_THING_SECTION);

			ResetSidedefPreviews();
			ResetFlatPreviews();

			GetThingDisplayInfo(lpmwd->lpmap, lpcfgThings, lpmwd->highlightobj.iIndex, &tdi);
			BuildThingSpritePreview(lpmwd, &tdi, TDIF_ALL);
			ShowThingInfo(&tdi, TDIF_ALL);
			DestroyThingSpritePreview(&tdi, TDIF_ALL);
		}

		break;

	case EM_VERTICES:
		{
			VERTEXDISPLAYINFO	vdi;

			ResetSidedefPreviews();
			ResetFlatPreviews();
			ResetSpritePreview();

			GetVertexDisplayInfo(lpmwd->lpmap, lpmwd->highlightobj.iIndex, &vdi);
			ShowVertexInfo(&vdi, VDIF_ALL);
		}

		break;

	/* Not pointing at anything. */
	case EM_MOVE:
		{
			switch(lpmwd->editmode)
			{
			case EM_ANY:
				{
					/* Begin by clearing all previews. Doesn't flicker, since we
					 * only get here when the info-bar *needs* updating.
					 */
					ResetSidedefPreviews();
					ResetFlatPreviews();
					ResetSpritePreview();

					/* If we've got a mixed selection or no selection, hide the
					 * left-hand panels.
					 */
					if((lpmwd->selection.lpsellistLinedefs->iDataCount > 0 ? 1 : 0) +
						(lpmwd->selection.lpsellistSectors->iDataCount > 0 ? 1 : 0) +
						(lpmwd->selection.lpsellistThings->iDataCount > 0 ? 1 : 0) +
						(lpmwd->selection.lpsellistVertices->iDataCount > 0 ? 1 : 0) != 1)
					{
						/* Disable them. */
						InfoBarDisableDetailsPanels();

						/* Hide them. */
						InfoBarShowPanels(IBPF_ALLINFO, FALSE);
					}

					if(lpmwd->selection.lpsellistLinedefs->iDataCount > 0) ShowLinesSelectionInfo(lpmwd);
					if(lpmwd->selection.lpsellistSectors->iDataCount > 0) ShowSectorSelectionInfo(lpmwd);
					if(lpmwd->selection.lpsellistThings->iDataCount > 0) ShowThingsSelectionInfo(lpmwd);
					if(lpmwd->selection.lpsellistVertices->iDataCount > 0) ShowVerticesSelectionInfo(lpmwd);
				}

				break;

			case EM_LINES:
				if(lpmwd->selection.lpsellistLinedefs->iDataCount > 0) ShowLinesSelectionInfo(lpmwd);
				else InfoBarShowPanels(IBPF_ALL, FALSE);

				break;

			case EM_SECTORS:
				if(lpmwd->selection.lpsellistSectors->iDataCount > 0) ShowSectorSelectionInfo(lpmwd);
				else InfoBarShowPanels(IBPF_ALL, FALSE);

				break;

			case EM_THINGS:
				if(lpmwd->selection.lpsellistThings->iDataCount > 0) ShowThingsSelectionInfo(lpmwd);
				else InfoBarShowPanels(IBPF_ALL, FALSE);

				break;

			case EM_VERTICES:
				if(lpmwd->selection.lpsellistVertices->iDataCount > 0) ShowVerticesSelectionInfo(lpmwd);
				else InfoBarShowPanels(IBPF_ALL, FALSE);

				break;

			default:

				InfoBarShowPanels(IBPF_ALL, FALSE);
			}
		}

		break;

	default:
		break;
	}
}


/* ShowsLinesSelectionInfo, ShowsSectorSelectionInfo, ShowsThingSelectionInfo,
 * ShowsVertexSelectionInfo
 *   Updates the info-bar to reflect selections.
 *
 * Parameters:
 *   MAPWINDATA*	lpmwd		Map window data.
 *
 * Return value: None.
 */
static void ShowLinesSelectionInfo(MAPWINDATA *lpmwd)
{
	LINEDEFDISPLAYINFO lddi;
	DWORD dwDisplayFlags;
	BYTE byTexRequirementFlags;

	dwDisplayFlags = CheckLines(lpmwd->lpmap, lpmwd->selection.lpsellistLinedefs, ConfigGetSubsection(lpmwd->lpcfgMap, "__ldtypesflat"), &lddi);
	byTexRequirementFlags = CheckTextureFlags(lpmwd->lpmap, lpmwd->selection.lpsellistLinedefs);

	BuildSidedefTexturePreviews(lpmwd, &lddi, dwDisplayFlags);
	ShowLinesInfo(&lddi, dwDisplayFlags, byTexRequirementFlags);
	DestroySidedefTexturePreviews(&lddi, dwDisplayFlags);
}

static void ShowSectorSelectionInfo(MAPWINDATA *lpmwd)
{
	SECTORDISPLAYINFO sdi;
	DWORD dwDisplayFlags;

	dwDisplayFlags = CheckSectors(lpmwd->lpmap, lpmwd->selection.lpsellistSectors, ConfigGetSubsection(lpmwd->lpcfgMap, "sectortypes"), &sdi);

	BuildSectorTexturePreviews(lpmwd, &sdi, dwDisplayFlags);
	ShowSectorInfo(&sdi, dwDisplayFlags);
	DestroySectorTexturePreviews(&sdi, dwDisplayFlags);
}

static void ShowThingsSelectionInfo(MAPWINDATA *lpmwd)
{
	THINGDISPLAYINFO tdi;
	DWORD dwDisplayFlags;

	dwDisplayFlags = CheckThings(lpmwd->lpmap, lpmwd->selection.lpsellistThings, ConfigGetSubsection(lpmwd->lpcfgMap, FLAT_THING_SECTION), &tdi);

	BuildThingSpritePreview(lpmwd, &tdi, dwDisplayFlags);
	ShowThingInfo(&tdi, dwDisplayFlags);
	DestroyThingSpritePreview(&tdi, dwDisplayFlags);
}

static void ShowVerticesSelectionInfo(MAPWINDATA *lpmwd)
{
	VERTEXDISPLAYINFO vdi;
	DWORD dwDisplayFlags;

	dwDisplayFlags = CheckVertices(lpmwd->lpmap, lpmwd->selection.lpsellistVertices, &vdi);

	ShowVertexInfo(&vdi, dwDisplayFlags);
}


/* ShowsSelectionProperties, ShowTypedSelectionProperties
 *   Updates the info-bar to reflect selections.
 *
 * Parameters:
 *   MAPWINDATA*	lpmwd			Map window data.
 *   BOOL			bCreateUndo		Whether to create an undo.
 *   EDITOMODE		editmode		Types of properties to show.
 *
 * Return value: None.
 *
 * Remarks:
 *   The caller should ensure that something is in fact selected.
 *   ShowSelectionProperties just calls ShowTypedSelectionProperties with the
 *   current editing mode.
 */
static __inline BOOL ShowSelectionProperties(MAPWINDATA *lpmwd, BOOL bCreateUndo)
{
	return ShowTypedSelectionProperties(lpmwd, bCreateUndo, lpmwd->editmode);
}

static BOOL ShowTypedSelectionProperties(MAPWINDATA *lpmwd, BOOL bCreateUndo, ENUM_EDITMODE editmode)
{
	DWORD dwFlags = 0;
	DWORD dwStartPage = 0;
	MAPPPDATA mapppdata;
	TCHAR szFromTable[64], szCaption[64];
	UINT uiStringID = 0;
	int iObject;
	BOOL bRet;


	/* Build the flags to determine which property pages to show, and which
	 * one to start on.
	 */

	/* Assume we don't need to show obj num in caption. */
	iObject = -1;

	switch(editmode)
	{
	case EM_SECTORS:
		dwFlags = MPPF_SECTOR | MPPF_LINE;
		dwStartPage = MPPF_SECTOR;

		if(lpmwd->selection.lpsellistSectors->iDataCount == 1)
		{
			uiStringID = IDS_INFOBAR_SCAP;
			iObject = lpmwd->selection.lpsellistSectors->lpiIndices[0];
		}
		else
		{
			uiStringID = IDS_MAPOBJECT;
		}

		break;

	case EM_LINES:
		dwFlags = MPPF_LINE;
		dwStartPage = MPPF_LINE;

		/* If we've got any sectors selected, show that page, too. */
		if(lpmwd->selection.lpsellistSectors->iDataCount > 0) dwFlags |= MPPF_SECTOR;

		if(lpmwd->selection.lpsellistLinedefs->iDataCount == 1)
		{
			uiStringID = IDS_INFOBAR_LDCAP;
			iObject = lpmwd->selection.lpsellistLinedefs->lpiIndices[0];
		}
		else
		{
			uiStringID = IDS_MAPOBJECT;
		}

		break;

	case EM_THINGS:
		dwFlags = MPPF_THING;
		dwStartPage = MPPF_THING;

		if(lpmwd->selection.lpsellistThings->iDataCount == 1)
		{
			uiStringID = IDS_INFOBAR_TCAP;
			iObject = lpmwd->selection.lpsellistThings->lpiIndices[0];
		}
		else
		{
			uiStringID = IDS_MAPOBJECT;
		}

		break;

	case EM_VERTICES:
		/* The caller should make sure only one vertex is selected. */
		dwFlags = MPPF_VERTEX;
		dwStartPage = MPPF_VERTEX;
		uiStringID = IDS_INFOBAR_SCAP;
		iObject = lpmwd->selection.lpsellistSectors->lpiIndices[0];

		break;

	case EM_ANY:
		/* This is slightly more involved. We have to check what sort of things
		 * we have selected.
		 */
		if(lpmwd->selection.lpsellistLinedefs->iDataCount > 0)	dwFlags |= MPPF_LINE;
		if(lpmwd->selection.lpsellistSectors->iDataCount > 0)	dwFlags |= MPPF_SECTOR;
		if(lpmwd->selection.lpsellistThings->iDataCount > 0)	dwFlags |= MPPF_THING;

		/* Setting the properties of multiple vertices makes no sense at all. */
		if(lpmwd->selection.lpsellistVertices->iDataCount == 1)	dwFlags |= MPPF_VERTEX;

		/* We determine the start page by the type of the object that was
		 * clicked. TODO: Does this work with context menus?
		 */
		switch(lpmwd->hobjDrag.emType)
		{
			case EM_SECTORS:	dwStartPage = MPPF_SECTOR;	break;
			case EM_LINES:		dwStartPage = MPPF_LINE;	break;
			case EM_VERTICES:	dwStartPage = MPPF_VERTEX;	break;
			case EM_THINGS:		dwStartPage = MPPF_THING;	break;
			default:			dwStartPage = (DWORD)-1;	/* Don't care. */
		}

		uiStringID = IDS_MAPOBJECT;

		break;

	default:
		break;
	}

	/* Caption. Fill in using determined parameters. */
	LoadString(g_hInstance, uiStringID, szFromTable, sizeof(szFromTable) / sizeof(TCHAR));
	if(iObject >= 0)
		_sntprintf(szCaption, sizeof(szCaption) / sizeof(TCHAR) - 1, szFromTable, iObject);
	else
		lstrcpyn(szCaption, szFromTable, sizeof(szCaption) / sizeof(TCHAR));

	/* Terminate string in the event that _sntprintf filled it entirely. */
	szCaption[sizeof(szCaption) / sizeof(TCHAR) - 1] = '\0';

	mapppdata.hwndMap = lpmwd->hwnd;
	mapppdata.lpmap = lpmwd->lpmap;
	mapppdata.lpselection = &lpmwd->selection;
	mapppdata.lpcfgMap = lpmwd->lpcfgMap;
	mapppdata.lptnlFlats = lpmwd->lptnlFlats;
	mapppdata.lptnlTextures = lpmwd->lptnlTextures;
	mapppdata.lphimlCacheFlats = &lpmwd->himlCacheFlats;
	mapppdata.lphimlCacheTextures = &lpmwd->himlCacheTextures;

	/* Make an undo. */
	if(bCreateUndo)
		CreateUndo(lpmwd, IDS_UNDO_PROPERTIES);

	/* Show the dialogue. If the user cancelled, withdraw the undo if necessary.
	 */
	bRet = ShowMapObjectProperties(dwFlags, dwStartPage, szCaption, &mapppdata);
	if(bCreateUndo)
	{
		if(!bRet)
			WithdrawUndo(lpmwd);
		else
		{
			ClearRedoStack(lpmwd);
			lpmwd->bMapChanged = TRUE;
		}
	}

	return bRet;
}



/* DoInsertOperation
 *   Inserts an object.
 *
 * Parameters:
 *   MAPWINDATA*	lpmwd		Map window data.
 *   short			xMap, yMap	Map co-ordinates for insertion.
 *
 * Return value: None.
 *
 * Remarks:
 *   This may be done in response to either a left- or right-button release. The
 *   selection is cleared, but it should be clear beforehand anyway.
 */
static void DoInsertOperation(MAPWINDATA *lpmwd, short xMap, short yMap)
{
	DeselectAll(lpmwd);

	switch(lpmwd->editmode)
	{
	case EM_ANY:
	case EM_LINES:
	case EM_SECTORS:
	case EM_VERTICES:

		/* User may cancel, so don't clear redo. */
		CreateUndo(lpmwd, (lpmwd->editmode == EM_VERTICES) ? IDS_UNDO_VERTEXINSERT : IDS_UNDO_LINEDEFDRAW);

		/* Draw the first vertex. */
		DrawToNewVertex(lpmwd->lpmap, &lpmwd->drawop, xMap, yMap, lpmwd->bStitchMode);

		/* Switch to drawing submode (this is overriden in vertex mode). */
		lpmwd->submode = ESM_DRAWING;

		/* If we're in vertex mode, that was also the last. */
		if(lpmwd->editmode == EM_VERTICES)
		{
			/* Drawing this vertex finishes the drawing operation. */
			EndDrawOperation(lpmwd->lpmap, &lpmwd->drawop);
			lpmwd->submode = ESM_NONE;

			ClearRedoStack(lpmwd);
			lpmwd->bMapChanged = TRUE;

			UpdateStatusBar(lpmwd, SBPF_LINEDEFS | SBPF_SIDEDEFS | SBPF_VERTICES);
		}

		break;

	case EM_THINGS:
		EditorInsertThing(lpmwd, xMap, yMap);
		lpmwd->submode = ESM_NONE;

		break;

	default:
		break;
	}
}


/* EndDragOperation
 *   Called when the user releases the mouse when dragging something. Sorts
 *   the map out, cleans up and restores normal behaviour.
 *
 * Parameters:
 *   MAPWINDATA*	lpmwd		Map window data.
 *
 * Return value: None.
 */
static void EndDragOperation(MAPWINDATA *lpmwd)
{
	/* TODO: Release mouse? */
	/* TODO: Add vertices for crossing lines. */

	/* Stitch any vertices within range. */
	if(lpmwd->bStitchMode)
	{
		/* Do this before we change linedef indices. */
		LabelRSLinesEnclosure(lpmwd->lpmap, lpmwd->lplooplistRS);

		/* Stitch and split. */
		StitchDraggedVertices(lpmwd->lpmap, lpmwd->selectionAux.lpsellistVertices);

		/* We need to do this now, so that we can restore our dragged loops
		 * before checking if we've been dropped around some other lines.
		 */
		CorrectDraggedSectorReferences(lpmwd->lpmap);
	}
	/* If we're not stitching, just do the minimum to keep the map correct. */
	else DeleteZeroLengthLinedefs(lpmwd->lpmap);

	/* The above messed with indices. */
	RebuildSelection(lpmwd);


	/* The auxiliary selection is now invalid! */


	/* Update the loop list and flags to include the newly-RS-marked lines. */
	DestroyLoopList(lpmwd->lplooplistRS);
	lpmwd->lplooplistRS = LabelRSLinesLoops(lpmwd->lpmap);

	/* Any lines inside any of our loops in their final positions also need to
	 * be marked RS.
	 */
	LabelLoopedLinesRS(lpmwd->lpmap, lpmwd->lplooplistRS);

	/* Label whether each side should keep its sector reference. */
	LabelRSLinesEnclosure(lpmwd->lpmap, lpmwd->lplooplistRS);

	/* Set the sector references on sides that weren't enclosed. */
	CorrectDraggedSectorReferences(lpmwd->lpmap);

	/* Clean up. */
	PostDragCleanup(lpmwd);

	/* Restore normal behaviour. */
	lpmwd->submode = ESM_NONE;
	UpdateHighlight(lpmwd, -1, -1);
	MapWinUpdateInfoBar(lpmwd);
	UpdateStatusBar(lpmwd, SBPF_LINEDEFS | SBPF_SIDEDEFS | SBPF_VERTICES | SBPF_SECTORS);
	lpmwd->bWantRedraw = TRUE;

	/* Too late to cancel now! */
	ClearRedoStack(lpmwd);
	lpmwd->bMapChanged = TRUE;
}


/* PostDragCleanup
 *   Cleans up memory and flags involved in a dragging operation.
 *
 * Parameters:
 *   MAPWINDATA*	lpmwd		Map window data.
 *
 * Return value: None.
 */
static void PostDragCleanup(MAPWINDATA *lpmwd)
{
	CleanAuxiliarySelection(lpmwd);

	ClearDraggingFlags(lpmwd->lpmap);

	/* Were the objects selected before we clicked on them? If not, deselect
	 * them. We use brute force since, when pasting, we don't know that the
	 * types of the objects we're dragging correspond to the mode.
	 */
	if(lpmwd->bDeselectAfterEdit)
		DeselectAllBruteForce(lpmwd);

	/* Free memory. */
	DestroyLoopList(lpmwd->lplooplistRS);
}


/* RebuildSelection
 *   Recreates the selection lists for a map window based on the selection flags
 *   in the map objects.
 *
 * Parameters:
 *   MAPWINDATA*	lpmwd		Map window data.
 *
 * Return value: None.
 *
 * Remarks:
 *   Useful following a map editing operation that changes indices.
 */
static void RebuildSelection(MAPWINDATA *lpmwd)
{
	int i;

	SELECTION_LIST *lpsellistLinedefs = lpmwd->selection.lpsellistLinedefs;
	SELECTION_LIST *lpsellistSectors = lpmwd->selection.lpsellistSectors;
	SELECTION_LIST *lpsellistThings = lpmwd->selection.lpsellistThings;
	SELECTION_LIST *lpsellistVertices = lpmwd->selection.lpsellistVertices;

	/* Begin by clearing everything. */
	ClearSelectionList(lpsellistLinedefs);
	ClearSelectionList(lpsellistSectors);
	ClearSelectionList(lpsellistThings);
	ClearSelectionList(lpsellistVertices);

	/* Reselect each type in turn. */

	for(i = 0; i < lpmwd->lpmap->iLinedefs; i++)
		if(lpmwd->lpmap->linedefs[i].selected)
			AddToSelectionList(lpsellistLinedefs, i);

	for(i = 0; i < lpmwd->lpmap->iSectors; i++)
		if(lpmwd->lpmap->sectors[i].selected)
			AddToSelectionList(lpsellistSectors, i);

	for(i = 0; i < lpmwd->lpmap->iThings; i++)
		if(lpmwd->lpmap->things[i].selected)
			AddToSelectionList(lpsellistThings, i);

	for(i = 0; i < lpmwd->lpmap->iVertices; i++)
		if(lpmwd->lpmap->vertices[i].selected)
			AddToSelectionList(lpsellistVertices, i);
}


/* CreateUndo
 *   Saves an undo snapshot.
 *
 * Parameters:
 *   MAPWINDATA*	lpmwd			Map window data.
 *   int			iStringIndex	ID of resource string describing the action.
 *
 * Return value: None.
 *
 * Remarks:
 *   The redo stack is not cleared, since the app may want to withdraw the undo
 *   if the user cancels, in which case the redo stack must be left intact.
 */
static void CreateUndo(MAPWINDATA *lpmwd, int iStringIndex)
{
	/* Create the new undo frame. If we don't already have a stack, make one. */
	if(lpmwd->lpundostackUndo)
		PushNewUndoFrame(lpmwd->lpundostackUndo, lpmwd->lpmap, iStringIndex);
	else
		lpmwd->lpundostackUndo = CreateUndoStack(lpmwd->lpmap, iStringIndex);
}


/* ClearRedoStack
 *   Clears the redo stack.
 *
 * Parameters:
 *   MAPWINDATA*	lpmwd			Map window data.
 *
 * Return value: None.
 *
 * Remarks:
 *   Call this once you're sure you want to leave that undo you just created.
 */
static void ClearRedoStack(MAPWINDATA *lpmwd)
{
	/* Clear the redo stack. */
	if(lpmwd->lpundostackRedo)
	{
		FreeUndoStack(lpmwd->lpundostackRedo);
		lpmwd->lpundostackRedo = NULL;
	}
}


/* PerformUndo
 *   Undoes the last action for an open map.
 *
 * Parameters:
 *   MAPWINDATA*	lpmwd			Map window data.
 *
 * Return value: None.
 *
 * Remarks:
 *   The item is removed from the undo stack once it's undone. The redo stack is
 *   modified accordingly.
 */
static void PerformUndo(MAPWINDATA *lpmwd)
{
	int iUndoStringIndex = GetUndoTopStringIndex(lpmwd->lpundostackUndo);

	/* Create a new redo frame, making a new stack if necessary. */
	if(lpmwd->lpundostackRedo)
		PushNewUndoFrame(lpmwd->lpundostackRedo, lpmwd->lpmap, iUndoStringIndex);
	else
		lpmwd->lpundostackRedo = CreateUndoStack(lpmwd->lpmap, iUndoStringIndex);

	PerformTopUndo(lpmwd->lpundostackUndo, lpmwd->lpmap);
	WithdrawUndo(lpmwd);
}


/* PerformRedo
 *   Redoes the last action for an open map.
 *
 * Parameters:
 *   MAPWINDATA*	lpmwd			Map window data.
 *
 * Return value: None.
 *
 * Remarks:
 *   The item is removed from the redo stack once it's redone. The undo stack is
 *   modified accordingly. This is a copy-paste hack from PerformUndo.
 */
static void PerformRedo(MAPWINDATA *lpmwd)
{
	int iUndoStringIndex = GetUndoTopStringIndex(lpmwd->lpundostackRedo);

	/* Create a new undo frame, making a new stack if necessary. */
	if(lpmwd->lpundostackUndo)
		PushNewUndoFrame(lpmwd->lpundostackUndo, lpmwd->lpmap, iUndoStringIndex);
	else
		lpmwd->lpundostackUndo = CreateUndoStack(lpmwd->lpmap, iUndoStringIndex);

	PerformTopUndo(lpmwd->lpundostackRedo, lpmwd->lpmap);
	WithdrawRedo(lpmwd);
}


/* WithdrawUndo
 *   Removes an item from the undo stack for a window.
 *
 * Parameters:
 *   MAPWINDATA*	lpmwd			Map window data.
 *
 * Return value: None.
 */
static void WithdrawUndo(MAPWINDATA *lpmwd)
{
	if(UndoStackHasOnlyOneFrame(lpmwd->lpundostackUndo))
	{
		FreeUndoStack(lpmwd->lpundostackUndo);
		lpmwd->lpundostackUndo = NULL;
	}
	else
	{
		PopUndoFrame(lpmwd->lpundostackUndo);
	}
}


/* WithdrawRedo
 *   Removes an item from the redo stack for a window.
 *
 * Parameters:
 *   MAPWINDATA*	lpmwd			Map window data.
 *
 * Return value: None.
 */
static void WithdrawRedo(MAPWINDATA *lpmwd)
{
	if(UndoStackHasOnlyOneFrame(lpmwd->lpundostackRedo))
	{
		FreeUndoStack(lpmwd->lpundostackRedo);
		lpmwd->lpundostackRedo = NULL;
	}
	else
	{
		PopUndoFrame(lpmwd->lpundostackRedo);
	}
}


/* FlipSelectionHorizontally
 *   Flips the aux. selection horizontally in the intuitive manner.
 *
 * Parameters:
 *   MAPWINDATA*	lpmwd			Map window data.
 *
 * Return value: None.
 *
 * Remarks:
 *   The auxiliary selection must be set before calling this.
 */
static void FlipSelectionHorizontally(MAPWINDATA *lpmwd)
{
	short xMin, xMax;
	int i;
	short xAxis;

	/* Firstly, calculate the boundaries. */
	FindAuxSelectionBoundaries(lpmwd, &xMin, &xMax, NULL, NULL);

	/* Now, find an axis that bisects the region. */
	xAxis = ((int)xMax + (int)xMin) / 2;

	/* Now, flip vertices and things about the axis. */
	for(i = 0; i < lpmwd->selectionAux.lpsellistVertices->iDataCount; i++)
		FlipVertexAboutVerticalAxis(lpmwd->lpmap, lpmwd->selectionAux.lpsellistVertices->lpiIndices[i], xAxis);

	for(i = 0; i < lpmwd->selectionAux.lpsellistThings->iDataCount; i++)
		FlipThingAboutVerticalAxis(lpmwd->lpmap, lpmwd->selectionAux.lpsellistThings->lpiIndices[i], xAxis);

	/* Flip selected lines. */
	FlipSelectedLinedefs(lpmwd->lpmap, lpmwd->selection.lpsellistLinedefs);
	ExchangeSelectedSidedefs(lpmwd->lpmap, lpmwd->selection.lpsellistLinedefs);
}


/* FlipSelectionVertically
 *   Flips the aux. selection vertically in the intuitive manner.
 *
 * Parameters:
 *   MAPWINDATA*	lpmwd			Map window data.
 *
 * Return value: None.
 *
 * Remarks:
 *   The auxiliary selection must be set before calling this.
 */
static void FlipSelectionVertically(MAPWINDATA *lpmwd)
{
	short yMin, yMax;
	int i;
	short yAxis;

	/* Firstly, calculate the boundaries. */
	FindAuxSelectionBoundaries(lpmwd, NULL, NULL, &yMin, &yMax);

	/* Now, find an axis that bisects the region. */
	yAxis = ((int)yMax + (int)yMin) / 2;

	/* Now, flip vertices and things about the axis. */
	for(i = 0; i < lpmwd->selectionAux.lpsellistVertices->iDataCount; i++)
		FlipVertexAboutHorizontalAxis(lpmwd->lpmap, lpmwd->selectionAux.lpsellistVertices->lpiIndices[i], yAxis);

	for(i = 0; i < lpmwd->selectionAux.lpsellistThings->iDataCount; i++)
		FlipThingAboutHorizontalAxis(lpmwd->lpmap, lpmwd->selectionAux.lpsellistThings->lpiIndices[i], yAxis);

	/* Flip selected lines. */
	FlipSelectedLinedefs(lpmwd->lpmap, lpmwd->selection.lpsellistLinedefs);
	ExchangeSelectedSidedefs(lpmwd->lpmap, lpmwd->selection.lpsellistLinedefs);
}


/* CreateAuxiliarySelection
 *   Creates an auxiliary selection structure of vertices and things from the
 *   real selection.
 *
 * Parameters:
 *   MAPWINDATA*	lpmwd			Map window data.
 *
 * Return value: None.
 *
 * Remarks:
 *   Call CleanAuxiliarySelection to free memory one you're finished.
 */
static void CreateAuxiliarySelection(MAPWINDATA *lpmwd)
{
	int i;

	/* Build our dragging selection list. */
	lpmwd->selectionAux.lpsellistVertices = AllocateSelectionList(INIT_SELLIST_SIZE);
	lpmwd->selectionAux.lpsellistThings = AllocateSelectionList(INIT_SELLIST_SIZE);

	for(i = 0; i < lpmwd->selection.lpsellistThings->iDataCount; i++)
		AddToSelectionList(lpmwd->selectionAux.lpsellistThings, lpmwd->selection.lpsellistThings->lpiIndices[i]);

	/* Add the vertices that are already selected first. */
	for(i = 0; i < lpmwd->selection.lpsellistVertices->iDataCount; i++)
		AddToSelectionList(lpmwd->selectionAux.lpsellistVertices, lpmwd->selection.lpsellistVertices->lpiIndices[i]);
	/* Now add vertices for selected linedefs. This also covers the
	 * sector case, since they force selection of linedefs.
	 */
	for(i = 0; i < lpmwd->selection.lpsellistLinedefs->iDataCount; i++)
	{
		MAPLINEDEF *lpld = &lpmwd->lpmap->linedefs[lpmwd->selection.lpsellistLinedefs->lpiIndices[i]];

		/* AddToSelectionList eats duplicates, so this is okay. */
		AddToSelectionList(lpmwd->selectionAux.lpsellistVertices, lpld->v1);
		AddToSelectionList(lpmwd->selectionAux.lpsellistVertices, lpld->v2);
	}

	/* This saves headaches later. */
	SortSelectionList(lpmwd->selectionAux.lpsellistVertices);
}


/* CleanAuxiliarySelection
 *   Frees an aux. selection.
 *
 * Parameters:
 *   MAPWINDATA*	lpmwd			Map window data.
 *
 * Return value: None.
 */
static void CleanAuxiliarySelection(MAPWINDATA *lpmwd)
{
	DestroySelectionList(lpmwd->selectionAux.lpsellistThings);
	DestroySelectionList(lpmwd->selectionAux.lpsellistVertices);
}


/* SelectionCount
 *   Returns the total number of selected objects.
 *
 * Parameters:
 *   MAPWINDATA*	lpmwd			Map window data.
 *
 * Return value: int
 *   Total number of selected objects.
 */
static __inline int SelectionCount(MAPWINDATA *lpmwd)
{
	return lpmwd->selection.lpsellistLinedefs->iDataCount +
		lpmwd->selection.lpsellistSectors->iDataCount +
		lpmwd->selection.lpsellistVertices->iDataCount +
		lpmwd->selection.lpsellistThings->iDataCount;
}


/* CentreAuxSelectionAtPoint
 *   Translates the auxiliary selection such that it's centred at a point.
 *
 * Parameters:
 *   MAPWINDATA*	lpmwd				Map window data.
 *   short			xCentre, yCentre	Point at which to centre selection.
 *
 * Return value: None.
 */
static void CentreAuxSelectionAtPoint(MAPWINDATA *lpmwd, short xCentre, short yCentre)
{
	short xMin, xMax, yMin, yMax;
	short xBoxCentre, yBoxCentre;
	int i;

	FindAuxSelectionBoundaries(lpmwd, &xMin, &xMax, &yMin, &yMax);

	/* Find the centre of the bounding box. */
	xBoxCentre = ((int)xMin + (int)xMax) / 2;
	yBoxCentre = ((int)yMin + (int)yMax) / 2;

	/* Translate the selection. */
	for(i = 0; i < lpmwd->selectionAux.lpsellistVertices->iDataCount; i++)
	{
		lpmwd->lpmap->vertices[lpmwd->selectionAux.lpsellistVertices->lpiIndices[i]].x += xCentre - xBoxCentre;
		lpmwd->lpmap->vertices[lpmwd->selectionAux.lpsellistVertices->lpiIndices[i]].y += yCentre - yBoxCentre;
	}

	for(i = 0; i < lpmwd->selectionAux.lpsellistThings->iDataCount; i++)
	{
		lpmwd->lpmap->things[lpmwd->selectionAux.lpsellistThings->lpiIndices[i]].x += xCentre - xBoxCentre;
		lpmwd->lpmap->things[lpmwd->selectionAux.lpsellistThings->lpiIndices[i]].y += yCentre - yBoxCentre;
	}
}


/* FindAuxSelectionBoundaries
 *   Finds the tightest bounding box for the auxiliarly selection.
 *
 * Parameters:
 *   MAPWINDATA*	lpmwd				Map window data.
 *   short			*lpxMin, *lpxMax, 	Used to return co-ordinates of
 *					*lpyMin, *lpyMax	the bounding box. May be NULL.
 *
 * Return value: None.
 */
static void FindAuxSelectionBoundaries(MAPWINDATA *lpmwd, short *lpxMin, short *lpxMax, short *lpyMin, short *lpyMax)
{
	int i;

	if(lpxMin) *lpxMin = 32767;
	if(lpxMax) *lpxMax = -32768;
	if(lpyMin) *lpyMin = 32767;
	if(lpyMax) *lpyMax = -32768;

	for(i = 0; i < lpmwd->selectionAux.lpsellistVertices->iDataCount; i++)
	{
		if(lpxMin) *lpxMin = min(*lpxMin, lpmwd->lpmap->vertices[lpmwd->selectionAux.lpsellistVertices->lpiIndices[i]].x);
		if(lpxMax) *lpxMax = max(*lpxMax, lpmwd->lpmap->vertices[lpmwd->selectionAux.lpsellistVertices->lpiIndices[i]].x);
		if(lpyMin) *lpyMin = min(*lpyMin, lpmwd->lpmap->vertices[lpmwd->selectionAux.lpsellistVertices->lpiIndices[i]].y);
		if(lpyMax) *lpyMax = max(*lpyMax, lpmwd->lpmap->vertices[lpmwd->selectionAux.lpsellistVertices->lpiIndices[i]].y);
	}

	for(i = 0; i < lpmwd->selectionAux.lpsellistThings->iDataCount; i++)
	{

		if(lpxMin) *lpxMin = min(*lpxMin, lpmwd->lpmap->things[lpmwd->selectionAux.lpsellistThings->lpiIndices[i]].x);
		if(lpxMax) *lpxMax = max(*lpxMax, lpmwd->lpmap->things[lpmwd->selectionAux.lpsellistThings->lpiIndices[i]].x);
		if(lpyMin) *lpyMin = min(*lpyMin, lpmwd->lpmap->things[lpmwd->selectionAux.lpsellistThings->lpiIndices[i]].y);
		if(lpyMax) *lpyMax = max(*lpyMax, lpmwd->lpmap->things[lpmwd->selectionAux.lpsellistThings->lpiIndices[i]].y);
	}
}


/* InsertThing
 *   Adds a new thing to a map, and initialises it with the properties of the
 *   thing last edited.
 *
 * Parameters:
 *   MAPWINDATA*		lpmwd		Map window data.
 *   unsigned short		xMap, yMap	Co-ordinates of new thing.
 *
 * Return value: int
 *   Index of new thing, or negative on error.
 */
static int InsertThing(MAPWINDATA *lpmwd, unsigned short xMap, unsigned short yMap)
{
	int iThing = AddThing(lpmwd->lpmap, xMap, yMap);
	MAPTHING *lpthing;

	/* Out of things...? */
	if(iThing < 0) return -1;

	lpthing = &lpmwd->lpmap->things[iThing];

	/* Set some initial properties. */
	lpthing->thing = lpmwd->thingLast.thing;
	lpthing->flag = lpmwd->thingLast.flag;
	lpthing->angle = lpmwd->thingLast.angle;

	SetThingPropertiesFromType(lpmwd->lpmap, iThing, ConfigGetSubsection(lpmwd->lpcfgMap, FLAT_THING_SECTION));

	return iThing;
}


/* SendUpdateWadMessage
 *   Sends a WM_UPDATEWAD message to a window.
 *
 * Parameters:
 *   HWND		hwnd	Window to send message to.
 *   LPARAM		lParam	Unused.
 *
 * Return value: BOOL
 *   Always TRUE.
 *
 * Remarks:
 *   Used as a callback by EnumChildWindowsByWad when doing a Save As.
 */
static BOOL CALLBACK SendUpdateWadMessage(HWND hwnd, LPARAM lParam)
{
	UNREFERENCED_PARAMETER(lParam);

	SendMessage(hwnd, WM_UPDATEWAD, 0, 0);

	return TRUE;
}


/* SendUpdateTitleMessage
 *   Sends a WM_UPDATETITLE message to a window.
 *
 * Parameters:
 *   HWND		hwnd	Window to send message to.
 *   LPARAM		lParam	Unused.
 *
 * Return value: BOOL
 *   Always TRUE.
 *
 * Remarks:
 *   Used as a callback by EnumChildWindowsByWad when doing a Save As.
 */
static BOOL CALLBACK SendUpdateTitleMessage(HWND hwnd, LPARAM lParam)
{
	UNREFERENCED_PARAMETER(lParam);

	SendMessage(hwnd, WM_UPDATETITLE, 0, 0);

	return TRUE;
}


/* MapWindowSaveAs
 *   Displays the Save As dialogue and saves the wad.
 *
 * Parameters:
 *   MAPWINDATA*	lpmwd	Map window data.
 *
 * Return value: None.
 */
static void MapWindowSaveAs(MAPWINDATA *lpmwd)
{
	TCHAR szFileName[MAX_PATH];
	LPTSTR szInitFileName = NULL;
	WORD cchInitFileName;
	TCHAR szFilter[128];

	/* Load the filter from the string table. */
	LoadAndFormatFilterString(IDS_WADFILEFILTER, szFilter, sizeof(szFilter)/sizeof(TCHAR));

	cchInitFileName = WadIsNewByID(lpmwd->iWadID) ? 0 : GetWadFilename(lpmwd->iWadID, NULL, 0);
	if(cchInitFileName > 0)
	{
		szInitFileName = ProcHeapAlloc(cchInitFileName * sizeof(TCHAR));
		GetWadFilename(lpmwd->iWadID, szInitFileName, cchInitFileName);
	}

	/* Make sure the user doesn't cancel. */
	if(CommDlgSave(lpmwd->hwnd, szFileName, sizeof(szFileName)/sizeof(TCHAR), NULL, szFilter, TEXT("wad"), szInitFileName, OFN_EXPLORER | OFN_OVERWRITEPROMPT))
	{
		/* Update the wad structure for all open maps for this wad. */
		EnumChildWindowsByWad(lpmwd->iWadID, SendUpdateWadMessage, 0);

		/* TODO: Nicer errors. */
		if(SaveWadAs(lpmwd->iWadID, szFileName))
			MessageBoxFromStringTable(g_hwndMain, IDS_SAVEERROR, MB_ICONERROR);
		else
		{
			lpmwd->bMapChanged = FALSE;

			/* Add to the MRU. */
			MRUAdd(szFileName);

			/* Save the wad's options. */
			if(WriteWadOptions(szFileName, lpmwd->lpcfgWadOpt))
				MessageBoxFromStringTable(g_hwndMain, IDS_ERROR_WRITEWADOPT, MB_ICONERROR);
		}

		/* Update the title for all open maps for this wad. */
		EnumChildWindowsByWad(lpmwd->iWadID, SendUpdateTitleMessage, 0);
	}

	if(szInitFileName) ProcHeapFree(szInitFileName);
}


/* MapWindowSave
 *   Saves the current map for the specified map window.
 *
 * Parameters:
 *   MAPWINDATA*	lpmwd	Map window data.
 *
 * Return value: None.
 */
static void MapWindowSave(MAPWINDATA *lpmwd)
{
	/* Do a Save As if necessary. */
	if(WadIsNewByID(lpmwd->iWadID))
		MapWindowSaveAs(lpmwd);
	else
	{
		/* TODO: Nicer error messages? */
		SendUpdateWadMessage(lpmwd->hwnd, 0);
		if(SaveWad(lpmwd->iWadID))
			MessageBoxFromStringTable(g_hwndMain, IDS_SAVEERROR, MB_ICONERROR);
		else
		{
			int cb;
			LPTSTR szFilename;

			lpmwd->bMapChanged = FALSE;

			/* Allocate buffer for full path, and get it. */
			cb = GetWadFilename(lpmwd->iWadID, NULL, 0);
			szFilename = (LPTSTR)ProcHeapAlloc(cb * sizeof(TCHAR));
			GetWadFilename(lpmwd->iWadID, szFilename, cb);
		
			/* Save the wad's options. */
			if(WriteWadOptions(szFilename, lpmwd->lpcfgWadOpt))
				MessageBoxFromStringTable(g_hwndMain, IDS_ERROR_WRITEWADOPT, MB_ICONERROR);

			ProcHeapFree(szFilename);
		}
	}
}


/* SnapAuxVerticesToGrid, SnapSelectedThingsToGrid
 *   Snaps selected vertices/things to the grid.
 *
 * Parameters:
 *   MAPWINDATA*	lpmwd			Map window data.
 *
 * Return value: None.
 */
static void SnapAuxVerticesToGrid(MAPWINDATA *lpmwd)
{
	int i;
	for(i = 0; i < lpmwd->selectionAux.lpsellistVertices->iDataCount; i++)
	{
		MAPVERTEX *lpvx = &lpmwd->lpmap->vertices[lpmwd->selectionAux.lpsellistVertices->lpiIndices[i]];
		Snap(&lpvx->x, &lpvx->y, lpmwd->mapview.cxGrid, lpmwd->mapview.cyGrid, lpmwd->mapview.xGridOffset, lpmwd->mapview.yGridOffset);
	}

	/* Stitch if desired. */
	if(lpmwd->bStitchMode)
	{
		StitchDraggedVertices(lpmwd->lpmap, lpmwd->selectionAux.lpsellistVertices);
		CorrectDraggedSectorReferences(lpmwd->lpmap);
		ClearDraggingFlags(lpmwd->lpmap);
		RebuildSelection(lpmwd);
	}
}

static void SnapSelectedThingsToGrid(MAPWINDATA *lpmwd)
{
	int i;
	for(i = 0; i < lpmwd->selection.lpsellistThings->iDataCount; i++)
	{
		MAPTHING *lpthing = &lpmwd->lpmap->things[lpmwd->selection.lpsellistThings->lpiIndices[i]];
		Snap(&lpthing->x, &lpthing->y, lpmwd->mapview.cxGrid, lpmwd->mapview.cyGrid, lpmwd->mapview.xGridOffset, lpmwd->mapview.yGridOffset);
	}
}


/* StartPan
 *   Ends a panning operation, either quick-move or EM_MOVE.
 *
 * Parameters:
 *   MAPWINDATA*	lpmwd			Map window data.
 *
 * Return value: None.
 */
static void StartPan(MAPWINDATA *lpmwd)
{
	POINT ptCursor;

	/* End the operation if we're in one. */
	CancelCurrentOperation(lpmwd);

	lpmwd->submode = ESM_MOVING;

	/* Store the cursor. */
	lpmwd->hCursor = LoadCursor(g_hInstance, MAKEINTRESOURCE(IDC_GRABCLOSED));

	/* Show the cursor if we're in the client area. */
	GetCursorPos(&ptCursor);
	if(SendMessage(lpmwd->hwnd, WM_NCHITTEST, 0, MAKELPARAM(ptCursor.x, ptCursor.y)) == HTCLIENT)
		SetCursor(lpmwd->hCursor);
}


/* EndPan
 *   Ends a panning operation, either quick-move or EM_MOVE.
 *
 * Parameters:
 *   MAPWINDATA*	lpmwd			Map window data.
 *
 * Return value: None.
 */
static void EndPan(MAPWINDATA *lpmwd)
{
	POINT pt;

	lpmwd->submode = ESM_NONE;

	/* Store the cursor. */
	lpmwd->hCursor = lpmwd->editmode == EM_MOVE ? LoadCursor(g_hInstance, MAKEINTRESOURCE(IDC_GRABOPEN)) : LoadCursor(NULL, IDC_ARROW);

	/* Show the cursor if we're in the client area. */
	pt.x = lpmwd->xMouseLast;
	pt.y = lpmwd->yMouseLast;
	ClientToScreen(lpmwd->hwnd, &pt);
	if(SendMessage(lpmwd->hwnd, WM_NCHITTEST, 0, MAKELPARAM(pt.x, pt.y)) == HTCLIENT)
		SetCursor(lpmwd->hCursor);
}


/* DeleteSelection
 *   Deletes the selection.
 *
 * Parameters:
 *   MAPWINDATA*	lpmwd			Map window data.
 *
 * Return value: None.
 *
 * Remarks:
 *   Clobbers the aux. selection and the VEF_LABELLED flag.
 */
static void DeleteSelection(MAPWINDATA *lpmwd)
{
	int i;

	/* Label the vertices that we should delete if they become unused. */
	CreateAuxiliarySelection(lpmwd);
	for(i = 0; i < lpmwd->selectionAux.lpsellistVertices->iDataCount; i++)
		lpmwd->lpmap->vertices[lpmwd->selectionAux.lpsellistVertices->lpiIndices[i]].editflags |= VEF_LABELLED;
	CleanAuxiliarySelection(lpmwd);

	SortSelectionList(lpmwd->selection.lpsellistLinedefs);
	for(i = lpmwd->selection.lpsellistLinedefs->iDataCount - 1; i >= 0; i--)
		DeleteLinedef(lpmwd->lpmap, lpmwd->selection.lpsellistLinedefs->lpiIndices[i]);

	SortSelectionList(lpmwd->selection.lpsellistVertices);
	for(i = lpmwd->selection.lpsellistVertices->iDataCount - 1; i >= 0; i--)
		DeleteVertex(lpmwd->lpmap, lpmwd->selection.lpsellistVertices->lpiIndices[i]);

	SortSelectionList(lpmwd->selection.lpsellistSectors);
	for(i = lpmwd->selection.lpsellistSectors->iDataCount - 1; i >= 0; i--)
		DeleteSector(lpmwd->lpmap, lpmwd->selection.lpsellistSectors->lpiIndices[i]);

	SortSelectionList(lpmwd->selection.lpsellistThings);
	for(i = lpmwd->selection.lpsellistThings->iDataCount - 1; i >= 0; i--)
		DeleteThing(lpmwd->lpmap, lpmwd->selection.lpsellistThings->lpiIndices[i]);

	RemoveUnusedVertices(lpmwd->lpmap, VEF_LABELLED);


	/* Clear labels. */
	for(i = 0; i < lpmwd->lpmap->iVertices; i++)
		lpmwd->lpmap->vertices[i].editflags &= ~VEF_LABELLED;

	RebuildSelection(lpmwd);
}


/* InsertPolygonalSector
 *   Displays the Insert Sector dialogue, and creates the sector unless the
 *   user cancels.
 *
 * Parameters:
 *   MAPWINDATA*	lpmwd				Map window data.
 *   short			xCentre, yCentre	Centre of new sector.
 *
 * Return value: None.
 *
 * Remarks:
 *   This sets the changed and redraw flags, handles undo, and all the stuff
 *   that's normally done in the window proc. It also clears the selection.
 */
static void InsertPolygonalSector(MAPWINDATA *lpmwd, short xCentre, short yCentre)
{
	INSERTSECTORDLGDATA isdd;
	int iOldSectorCount = lpmwd->lpmap->iSectors;

	DeselectAll(lpmwd);

	/* Show the Insert Sector dialogue. */
	isdd.bSnap = (lpmwd->bySnapFlags ^ lpmwd->bySnapToggleMask) & SF_RECTANGLE;
	isdd.nRadius = lpmwd->mapview.cxGrid;
	if(InsertSectorDlg(lpmwd->hwnd, &isdd))
	{
		int i;
		float fRadius;
		BOOL bKeepSector = TRUE;

		/* If the user didn't cancel, go ahead and make the sector. */

		CreateUndo(lpmwd, IDS_UNDO_INSERTSECTOR);

		BeginDrawOperation(&lpmwd->drawop);

		/* Calculate the real radius. */
		fRadius = isdd.cRadiusType == RT_TOEDGES ? isdd.nRadius / (float)cos(PI / isdd.nEdges) : isdd.nRadius;

		/* Loop round the vertices, making the first one *twice* - this
		 * completes the sector.
		 */
		for(i = 0; i <= isdd.nEdges; i++)
		{
			short x, y;

			x = max(-32768, min(32767, Round(xCentre + fRadius * cos((2 * i + 1) * PI / isdd.nEdges))));
			y = max(-32768, min(32767, Round(yCentre - fRadius * sin((2 * i + 1) * PI / isdd.nEdges))));

			/* This is a real snap-to-grid, not a snap as we draw, which would
			 * require the various flags.
			 */
			if(isdd.bSnap)
				Snap(&x, &y, lpmwd->mapview.cxGrid, lpmwd->mapview.cyGrid, lpmwd->mapview.xGridOffset, lpmwd->mapview.yGridOffset);

			DrawToNewVertex(lpmwd->lpmap, &lpmwd->drawop, x, y, FALSE);
		}

		EndDrawOperation(lpmwd->lpmap, &lpmwd->drawop);

		/* Does the user want a dialogue? */
		if(ConfigGetInteger(g_lpcfgMain, "newsectordialogue"))
		{
			HIGHLIGHTOBJ hobj;

			hobj.emType = EM_SECTORS;

			/* Select all new sectors. */
			for(hobj.iIndex = iOldSectorCount; hobj.iIndex < lpmwd->lpmap->iSectors; hobj.iIndex++)
				AddObjectToSelection(lpmwd, &hobj);

			/* Show the dialogue and then clear the selection. */
			bKeepSector = ShowTypedSelectionProperties(lpmwd, FALSE, EM_SECTORS);
			DeselectAll(lpmwd);

			/* Cancelled the dialogue, so revert to previous state. */
			if(!bKeepSector)
				PerformUndo(lpmwd);
		}

		/* We really did insert a sector. */
		if(bKeepSector)
		{
			/* Too late to cancel now. */
			ClearRedoStack(lpmwd);
			lpmwd->bMapChanged = TRUE;
			lpmwd->bWantRedraw = TRUE;
			UpdateStatusBar(lpmwd, SBPF_LINEDEFS | SBPF_SIDEDEFS | SBPF_VERTICES | SBPF_SECTORS);
		}
	}
}


/* CancelCurrentOperation
 *   Cancels the current editing operation (e.g. dragging), reverts any changes
 *   made so far, and restores normality.
 *
 * Parameters:
 *   MAPWINDATA*	lpmwd				Map window data.
 *
 * Return value: None.
 */
static void CancelCurrentOperation(MAPWINDATA *lpmwd)
{
	BOOL bPreserveSelection = FALSE;

	switch(lpmwd->submode)
	{
	case ESM_NONE:
		/* Don't want to do anything, including the post-switch stuff. */
		return;

	case ESM_DRAGGING:
	case ESM_PASTING:

		PostDragCleanup(lpmwd);

		/* Undo, but don't add it to the redo stack. */
		PerformTopUndo(lpmwd->lpundostackUndo, lpmwd->lpmap);
		WithdrawUndo(lpmwd);

		break;

	case ESM_DRAWING:

		EndDrawOperation(lpmwd->lpmap, &lpmwd->drawop);

		/* Undo, but don't add it to the redo stack. */
		PerformTopUndo(lpmwd->lpundostackUndo, lpmwd->lpmap);
		WithdrawUndo(lpmwd);

		break;

	case ESM_INSERTING:
		EndDrawOperation(lpmwd->lpmap, &lpmwd->drawop);
		break;

	case ESM_MOVING:
		EndPan(lpmwd);
		/* Fall through. */

	case ESM_SELECTING:
	case ESM_ROTATING:
		bPreserveSelection = TRUE;
		break;

	default:
		break;
	}

	lpmwd->bWantRedraw = TRUE;
	lpmwd->submode = ESM_NONE;

	if(!bPreserveSelection)
		DeselectAllBruteForce(lpmwd);
}


/* MakeUndoText, MakeRedoText
 *   Builds a string to display in the menus or tooltips for undo or redo.
 *
 * Parameters:
 *   HWND			hwndMap			Handle to map window.
 *   LPTSTR			szCaption		Buffer in which to store string.
 *   DWORD			cchCaption		Size of szCaption, including room for
 *									terminator.
 *
 * Return value: None.
 */
void MakeUndoText(HWND hwndMap, LPTSTR szCaption, DWORD cchCaption)
{
	MakeUndoRedoText(szCaption, cchCaption, ((MAPWINDATA*)GetWindowLong(hwndMap, GWL_USERDATA))->lpundostackUndo, IDS_UNDOPREFIX);
}

void MakeRedoText(HWND hwndMap, LPTSTR szCaption, DWORD cchCaption)
{
	MakeUndoRedoText(szCaption, cchCaption, ((MAPWINDATA*)GetWindowLong(hwndMap, GWL_USERDATA))->lpundostackRedo, IDS_REDOPREFIX);
}


/* MakeUndoRedoText
 *   Builds a string to display in the menus or tooltips for undo or redo.
 *   Called by MakeUndoText and MakeRedoText.
 *
 * Parameters:
 *   LPTSTR			szCaption		Buffer in which to store string.
 *   DWORD			cchCaption		Size of szCaption, including room for
 *									terminator.
 *   UNDOSTACK*		lpundostack		Undo stack whose top element is to be used
 *									to determine the string.
 *   WORD			wPrefixID		IDS_UNDOPREFIX or IDS_REDOPREFIX.
 *
 * Return value: None.
 */
static void MakeUndoRedoText(LPTSTR szCaption, DWORD cchCaption, UNDOSTACK *lpundostack, WORD wPrefixID)
{
	DWORD dwPrefixLength;

	/* Concatenate undo prefix and description, and set string. */
	LoadString(g_hInstance, wPrefixID, szCaption, cchCaption);
	dwPrefixLength = (DWORD)lstrlen(szCaption);

	if(lpundostack)
	{
		/* Get the string for the top undo frame. */
		LoadString(g_hInstance, GetUndoTopStringIndex(lpundostack), &szCaption[dwPrefixLength], cchCaption - dwPrefixLength);
	}
	else
	{
		/* Empty stack. Clobber the space at the end of the string. */
		szCaption[dwPrefixLength - 1] = TEXT('\0');
	}
}

/* DeselectAllByHandle
 *   Clears the selection.
 *
 * Parameters:
 *   HWND	hwndMap		Map window handle.
 *
 * Return value: None.
 *
 * Remarks:
 *   This works by window handle, and can be called by the outside world.
 *   Useful for the Find dialogue, for instance.
 */
void DeselectAllByHandle(HWND hwndMap)
{
	MAPWINDATA *lpmwd = (MAPWINDATA*)GetWindowLong(hwndMap, GWL_USERDATA);

	DeselectAll(lpmwd);
}


/* FindReplaceInt
 *   Does a Find and/or Replace for matching objects, using an integer search
 *   value.
 *
 * Parameters:
 *   HWND		hwndMap			Handle to map window.
 *   WORD		wType			Type of condition: see ENUM_FIND_TYPES.
 *   WORD		wCompare		Comparative operator: see ENUM_FIND_CONDITIONS.
 *   int		iValue			Value to match.
 *   int		iReplace		Value to replace with, if appropriate.
 *   BOOL		bInSelection	Whether only to search within the selection.
 *   BYTE		byFlags			Set FFL_SELECT to select matches and FFL_REPLACE
 *								to replace them.
 *
 * Return value: TRUE
 *   TRUE if it matches; FALSE otherwise.
 */
int FindReplaceInt(HWND hwndMap, WORD wFindType, WORD wCompare, int iValue, int iReplace, BOOL bInSelection, BYTE byFlags)
{
	int iMatches = 0;
	MAPWINDATA *lpmwd = (MAPWINDATA*)GetWindowLong(hwndMap, GWL_USERDATA);
	HIGHLIGHTOBJ hobj;
	CONFIG *lpcfgFlatThings = ConfigGetSubsection(lpmwd->lpcfgMap, FLAT_THING_SECTION);
	int iObjectCount;

	/* Determine the number of the correct type of objects. */
	switch(wFindType)
	{
	case FT_VX:
		hobj.emType = EM_VERTICES;
		iObjectCount = lpmwd->lpmap->iVertices;
		break;

	case FT_LINE: case FT_LINEFLAGS: case FT_LINETAG: case FT_SIDEDEF:
		hobj.emType = EM_LINES;
		iObjectCount = lpmwd->lpmap->iLinedefs;
		break;

	case FT_SEC: case FT_SECCEIL: case FT_SECFLR: case FT_SECTAG:
	case FT_SECLIGHT: case FT_SECHEIGHT: case FT_SECEFFECT: case FT_SECEFFECTA:
	case FT_SECEFFECTB: case FT_SECEFFECTC: case FT_SECEFFECTD:
		hobj.emType = EM_SECTORS;
		iObjectCount = lpmwd->lpmap->iSectors;
		break;

	case FT_THING: case FT_THINGANGLE: case FT_THINGFLAGS: case FT_THINGZOFF:
	case FT_THINGZABS: case FT_THINGTYPE:
		hobj.emType = EM_THINGS;
		iObjectCount = lpmwd->lpmap->iThings;
		break;

	default:
		return 0;
	}

	if((byFlags & FFL_SELECT) && lpmwd->editmode != hobj.emType && lpmwd->editmode != EM_ANY)
		ChangeMode(lpmwd, IDM_VIEW_MODE_ANY);

	for(hobj.iIndex = 0; hobj.iIndex < iObjectCount; hobj.iIndex++)
	{
		/* If we're bothered about whether it's selected, make sure it
		 * is.
		 */
		if(!bInSelection || HighlightObjSelected(lpmwd->lpmap, &hobj))
		{
			if(MapObjectSatisfiesFindConditionInt(lpmwd->lpmap, wFindType, wCompare, hobj.iIndex, iValue, lpcfgFlatThings))
			{
				/* Select if that's what we want. */
				if(byFlags & FFL_SELECT)
					AddObjectToSelection(lpmwd, &hobj);

				/* Replace if desired. */
				if(byFlags & FFL_REPLACE)
					ReplaceInt(lpmwd->lpmap, wFindType, hobj.iIndex, iReplace, lpcfgFlatThings);

				iMatches++;
			}
			else if(bInSelection && (byFlags & FFL_SELECT))
				RemoveObjectFromSelection(lpmwd, &hobj);
		}
	}

	lpmwd->bWantRedraw = TRUE;
	MapWinUpdateInfoBar(lpmwd);

	return iMatches;
}


/* FindReplaceStr
 *   Does a Find and/or Replace for  matching objects, using a string search
 *   value.
 *
 * Parameters:
 *   HWND		hwndMap			Handle to map window.
 *   WORD		wType			Type of condition: see ENUM_FIND_TYPES.
 *   WORD		wCompare		Comparative operator: see ENUM_FIND_CONDITIONS.
 *   LPSTR		szString		String to match.
 *   LPSTR		szReplace		String to replace with, if appropriate.
 *   BOOL		bInSelection	Whether only to search within the selection.
 *   BYTE		byFlags			Set FFL_SELECT to select matches and FFL_REPLACE
 *								to replace them.
 *
 * Return value: BOOL
 *   TRUE if it matches; FALSE otherwise.
 */
int FindReplaceStr(HWND hwndMap, WORD wFindType, WORD wCompare, LPSTR szString, LPSTR szReplace, BOOL bInSelection, BYTE byFlags)
{
	int iMatches = 0;
	MAPWINDATA *lpmwd = (MAPWINDATA*)GetWindowLong(hwndMap, GWL_USERDATA);
	HIGHLIGHTOBJ hobj;
	int iObjectCount;

	/* Determine the number of the correct type of objects. */
	switch(wFindType)
	{
	case FT_LINETEX:
		hobj.emType = EM_LINES;
		iObjectCount = lpmwd->lpmap->iLinedefs;
		break;

	case FT_SECCEILFLAT:
	case FT_SECFLRFLAT:
		hobj.emType = EM_SECTORS;
		iObjectCount = lpmwd->lpmap->iSectors;
		break;

	default:
		return 0;
	}

	/* Set the mode if necessary. */
	if(lpmwd->editmode != hobj.emType && lpmwd->editmode != EM_ANY)
		ChangeMode(lpmwd, IDM_VIEW_MODE_ANY);

	for(hobj.iIndex = 0; hobj.iIndex < iObjectCount; hobj.iIndex++)
	{
		/* If we're bothered about whether it's selected, make sure it
		 * is.
		 */
		if(!bInSelection || HighlightObjSelected(lpmwd->lpmap, &hobj))
		{
			BYTE byMatchFlags = MapObjectSatisfiesFindConditionStr(lpmwd->lpmap, wFindType, wCompare, hobj.iIndex, szString);

			if(byMatchFlags)
			{
				/* Select if that's what we want. */
				if(byFlags & FFL_SELECT)
					AddObjectToSelection(lpmwd, &hobj);

				/* Replace if desired. */
				if(byFlags & FFL_REPLACE)
					ReplaceStr(lpmwd->lpmap, wFindType, hobj.iIndex, szReplace, byMatchFlags);

				iMatches++;
			}
			else if(bInSelection && (byFlags & FFL_SELECT))
				RemoveObjectFromSelection(lpmwd, &hobj);
		}
	}

	lpmwd->bWantRedraw = TRUE;
	MapWinUpdateInfoBar(lpmwd);

	return iMatches;
}


/* HighlightObjSelected
 *   Determines whether a HIGHLIGHTOBJ is selected.
 *
 * Parameters:
 *   MAP*			lpmap	Map data.
 *   HIGHLIGHTOBJ*	lphobj	Object description.
 *
 * Return value: BOOL
 *   TRUE if it selected; FALSE otherwise.
 */
static BOOL HighlightObjSelected(MAP *lpmap, HIGHLIGHTOBJ *lphobj)
{
	switch(lphobj->emType)
	{
	case EM_SECTORS:	return lpmap->sectors[lphobj->iIndex].selected;
	case EM_LINES:		return lpmap->linedefs[lphobj->iIndex].selected;
	case EM_VERTICES:	return lpmap->vertices[lphobj->iIndex].selected;
	case EM_THINGS:		return lpmap->things[lphobj->iIndex].selected;
	default:			return FALSE;
	}
}


/* RotateSelection
 *   Rotates the aux. selection about a point.
 *
 * Parameters:
 *   MAPWINDATA*	lpmwd				Map window data.
 *   int			iAngle				Angle through which to rotate.
 *   char			cRotType			Origin type: CT_BOUNDINGBOX, CT_THING or
 *										CT_VERTEX.
 *   BOOL			bAdjustThingAngles	Whether to adjust things' directions.
 *   BOOL			bSnap				Whether to snap after rotation.
 *
 * Return value: None.
 *
 * Remarks:
 *   The auxiliary selection must be set before calling this.
 */
static void RotateSelection(MAPWINDATA *lpmwd, int iAngle, char cRotType, BOOL bAdjustThingAngles, BOOL bSnap)
{
	int i;
	short x, y;

	/* Get the appropriate fixed point. */
	GetAuxFixedPoint(lpmwd, cRotType, &x, &y);

	/* Now, rotate vertices and things about the point. */
	for(i = 0; i < lpmwd->selectionAux.lpsellistVertices->iDataCount; i++)
		RotateVertexAboutPoint(lpmwd->lpmap, lpmwd->selectionAux.lpsellistVertices->lpiIndices[i], x, y, iAngle);

	for(i = 0; i < lpmwd->selectionAux.lpsellistThings->iDataCount; i++)
	{
		RotateThingAboutPoint(lpmwd->lpmap, lpmwd->selectionAux.lpsellistThings->lpiIndices[i], x, y, iAngle);
		if(bAdjustThingAngles) RotateThingDirection(lpmwd->lpmap, lpmwd->selectionAux.lpsellistThings->lpiIndices[i], iAngle);
	}

	/* Snap if the user asked for it in the dialogue. */
	if(bSnap)
	{
		SnapAuxVerticesToGrid(lpmwd);
		SnapSelectedThingsToGrid(lpmwd);
	}

	/* Stitch any within stitching distance. */
	if(lpmwd->bStitchMode && lpmwd->submode != ESM_PASTING && lpmwd->submode != ESM_DRAGGING)
		StitchDraggedVertices(lpmwd->lpmap, lpmwd->selectionAux.lpsellistVertices);
}


/* ResizeSelection
 *   Resizes the aux. selection with respect to a fixed point.
 *
 * Parameters:
 *   MAPWINDATA*	lpmwd				Map window data.
 *   unsigned short	unFactor			Percentage scale factor.
 *   char			cOrigType			Origin type: CT_BOUNDINGBOX, CT_THING or
 *										CT_VERTEX.
 *   BOOL			bSnap				Whether to snap after rotation.
 *
 * Return value: None.
 *
 * Remarks:
 *   The auxiliary selection must be set before calling this.
 */
static void ResizeSelection(MAPWINDATA *lpmwd, unsigned short unFactor, char cOrigType, BOOL bSnap)
{
	int i;
	short x, y;

	/* Get the appropriate fixed point. */
	GetAuxFixedPoint(lpmwd, cOrigType, &x, &y);

	/* Now, dilate vertices and things wrt the point. */
	for(i = 0; i < lpmwd->selectionAux.lpsellistVertices->iDataCount; i++)
		DilateVertexWrtFixedPoint(lpmwd->lpmap, lpmwd->selectionAux.lpsellistVertices->lpiIndices[i], x, y, unFactor);

	for(i = 0; i < lpmwd->selectionAux.lpsellistThings->iDataCount; i++)
		DilateThingWrtFixedPoint(lpmwd->lpmap, lpmwd->selectionAux.lpsellistThings->lpiIndices[i], x, y, unFactor);

	/* Snap if the user asked for it in the dialogue. */
	if(bSnap)
	{
		SnapAuxVerticesToGrid(lpmwd);
		SnapSelectedThingsToGrid(lpmwd);
	}

	/* Stitch any within stitching distance. */
	if(lpmwd->bStitchMode && lpmwd->submode != ESM_PASTING && lpmwd->submode != ESM_DRAGGING)
		StitchDraggedVertices(lpmwd->lpmap, lpmwd->selectionAux.lpsellistVertices);
}


/* GetAuxFixedPoint
 *   Gets the fixed point of the aux. selection, based either on the bounding
 *   box, the first selected vertex or the first selected thing.
 *
 * Parameters:
 *   MAPWINDATA*	lpmwd		Map window data.
 *   char			cRotType	Origin type: CT_BOUNDINGBOX, CT_THING or
 *								CT_VERTEX.
 *   short			*lpx, *lpy	Used to return co-ordinates.
 *
 * Return value: None.
 */
static void GetAuxFixedPoint(MAPWINDATA *lpmwd, char cType, short *lpx, short *lpy)
{
	short xMin, xMax;
	short yMin, yMax;

	switch(cType)
	{
	case CT_BOUNDINGBOX:
		/* Firstly, calculate the boundaries. */
		FindAuxSelectionBoundaries(lpmwd, &xMin, &xMax, &yMin, &yMax);

		/* Find the centre of the bounding box. */
		*lpx = ((int)xMax + (int)xMin) / 2;
		*lpy = ((int)yMax + (int)yMin) / 2;

		break;

	case CT_THING:
		/* Use first selected thing as fixed point. */
		*lpx = lpmwd->lpmap->things[lpmwd->selectionAux.lpsellistThings->lpiIndices[0]].x;
		*lpy = lpmwd->lpmap->things[lpmwd->selectionAux.lpsellistThings->lpiIndices[0]].y;

		break;

	case CT_VERTEX:
		/* Use first selected vertex as fixed point. */
		*lpx = lpmwd->lpmap->vertices[lpmwd->selectionAux.lpsellistVertices->lpiIndices[0]].x;
		*lpy = lpmwd->lpmap->vertices[lpmwd->selectionAux.lpsellistVertices->lpiIndices[0]].y;

		break;
	}
}


/* RotateSelection
 *   Rotates the selected things on the spot.
 *
 * Parameters:
 *   MAPWINDATA*	lpmwd				Map window data.
 *   int			iAngle				Angle through which to rotate.
 *
 * Return value: None.
 */
static void RotateSelectedThings(MAPWINDATA *lpmwd, int iAngle)
{
	int i;

	for(i = 0; i < lpmwd->selection.lpsellistThings->iDataCount; i++)
		RotateThingDirection(lpmwd->lpmap, lpmwd->selection.lpsellistThings->lpiIndices[i], iAngle);
}


/* SelectAll
 *   Selects all map objects according to the current edit mode.
 *
 * Parameters:
 *   MAPWINDATA*	lpmwd	Map window data.
 *
 * Return value: None.
 */
static void SelectAll(MAPWINDATA *lpmwd)
{
	int i;

	/* Select linedefs and sectors. */
	if(lpmwd->editmode == EM_ANY || lpmwd->editmode == EM_LINES || lpmwd->editmode == EM_SECTORS)
	{
		/* Linedefs. */
		for(i = 0; i < lpmwd->lpmap->iLinedefs; i++)
		{
			lpmwd->lpmap->linedefs[i].selected = SLF_SELECTED;
			AddToSelectionList(lpmwd->selection.lpsellistLinedefs, i);
		}

		/* Sectors. */
		for(i = 0; i < lpmwd->lpmap->iSectors; i++)
		{
			lpmwd->lpmap->sectors[i].selected = SLF_SELECTED;
			AddToSelectionList(lpmwd->selection.lpsellistSectors, i);
		}
	}

	/* Select vertices. */
	if(lpmwd->editmode == EM_ANY || lpmwd->editmode == EM_VERTICES)
		for(i = 0; i < lpmwd->lpmap->iVertices; i++)
		{
			lpmwd->lpmap->vertices[i].selected = SLF_SELECTED;
			AddToSelectionList(lpmwd->selection.lpsellistVertices, i);
		}

	/* Select things. */
	if(lpmwd->editmode == EM_ANY || lpmwd->editmode == EM_THINGS)
		for(i = 0; i < lpmwd->lpmap->iThings; i++)
		{
			lpmwd->lpmap->things[i].selected = SLF_SELECTED;
			AddToSelectionList(lpmwd->selection.lpsellistThings, i);
		}
}


/* InvertSelection
 *   Inverts the selection of map objects according to the current edit mode.
 *
 * Parameters:
 *   MAPWINDATA*	lpmwd	Map window data.
 *
 * Return value: None.
 */
static void InvertSelection(MAPWINDATA *lpmwd)
{
	int i;

	/* Linedefs and sectors. */
	if(lpmwd->editmode == EM_ANY || lpmwd->editmode == EM_LINES || lpmwd->editmode == EM_SECTORS)
	{
		/* Toggle selected lines. */
		for(i = 0; i < lpmwd->lpmap->iLinedefs; i++)
			lpmwd->lpmap->linedefs[i].selected = (lpmwd->lpmap->linedefs[i].selected & SLF_SELECTED) ? 0 : SLF_SELECTED;

		/* Deselect all sectors. */
		for(i = 0; i < lpmwd->lpmap->iLinedefs; i++)
			lpmwd->lpmap->sectors[i].selected = 0;

		/* Reselect the appropriate sectors. This does a little bit of
		 * unnecessary work in adding the sectors' indices to the selection
		 * list, since they'll be cleared again when we rebuild the selection,
		 * but it's no big loss.
		 */
		SelectSectorsFromLines(lpmwd);
	}

	/* Vertices. */
	if(lpmwd->editmode == EM_ANY || lpmwd->editmode == EM_VERTICES)
		for(i = 0; i < lpmwd->lpmap->iVertices; i++)
			lpmwd->lpmap->vertices[i].selected = (lpmwd->lpmap->vertices[i].selected & SLF_SELECTED) ? 0 : 1;

	/* Things. */
	if(lpmwd->editmode == EM_ANY || lpmwd->editmode == EM_THINGS)
		for(i = 0; i < lpmwd->lpmap->iThings; i++)
			lpmwd->lpmap->things[i].selected = (lpmwd->lpmap->things[i].selected & SLF_SELECTED) ? 0 : 1;

	/* Set the selection lists from the flags. */
	RebuildSelection(lpmwd);
}


/* CreatePresetFromSelection
 *   Reads the common properties, by types, of selected objects into a preset
 *   structure.
 *
 * Parameters:
 *   MAPWINDATA*	lpmwd		Map window data.
 *   PRESET*		lppreset	Structure in which to return the properties.
 *
 * Return value: BOOL
 *   TRUE if there was anything to copy; FALSE otherwise.
 */
static BOOL CreatePresetFromSelection(MAPWINDATA *lpmwd, PRESET *lppreset)
{
	/* Clear the flags specifying which object types are included in the preset.
	 * We'll add to this as necessary.
	 */
	lppreset->byObjectTypeFlags = 0;

	/* Get sector properties if applicable. */
	if((lpmwd->editmode == EM_SECTORS || lpmwd->editmode == EM_ANY) && lpmwd->selection.lpsellistSectors->iDataCount > 0)
	{
		lppreset->byObjectTypeFlags |= POF_SECTOR;
		lppreset->dwSectorFlags = CreateSectorPresetFromSelection(lpmwd->lpmap, lpmwd->selection.lpsellistSectors, &lppreset->sec);
	}

	/* Get line properties if applicable. */
	if((lpmwd->editmode == EM_LINES || lpmwd->editmode == EM_ANY) && lpmwd->selection.lpsellistLinedefs->iDataCount > 0)
	{
		lppreset->byObjectTypeFlags |= POF_LINE;
		lppreset->dwLineFlags = CreateLinePresetFromSelection(lpmwd->lpmap, lpmwd->selection.lpsellistLinedefs, &lppreset->ld, &lppreset->sdFront, &lppreset->sdBack);
	}

	/* Get thing properties if applicable. */
	if((lpmwd->editmode == EM_THINGS || lpmwd->editmode == EM_ANY) && lpmwd->selection.lpsellistThings->iDataCount > 0)
	{
		lppreset->byObjectTypeFlags |= POF_THING;
		lppreset->dwThingFlags = CreateThingPresetFromSelection(lpmwd->lpmap, lpmwd->selection.lpsellistThings, &lppreset->thing);
	}

	/* Return whether there was anything for us to work with. */
	return (lppreset->byObjectTypeFlags != 0);
}


/* CreatePresetFromSelection
 *   Applies a preset to the selection, prompting the user which types of
 *   property to apply if there's ambiguity.
 *
 * Parameters:
 *   MAPWINDATA*	lpmwd		Map window data.
 *   PRESET*		lppreset	Preset.
 *
 * Return value: BOOL
 *   TRUE if changes were made; FALSE otherwise.
 */
static BOOL ApplyPresetToSelection(MAPWINDATA *lpmwd, PRESET *lppreset)
{
	/* Assume we made no changes. */
	BOOL bMapChanged = FALSE;

	/* Assume we apply everything we can (mode notwithstanding). */
	BYTE byObjectTypeFlags = lppreset->byObjectTypeFlags;

	/* If we're in Any Object mode and have more than one type of preset to
	 * apply, prompt the user.
	 */
	if(lpmwd->editmode == EM_ANY)
	{
		int iApplicableTypes = 0;

		if(lpmwd->selection.lpsellistSectors->iDataCount > 0 && byObjectTypeFlags & POF_SECTOR) iApplicableTypes++;
		if(lpmwd->selection.lpsellistLinedefs->iDataCount > 0 && byObjectTypeFlags & POF_LINE) iApplicableTypes++;
		if(lpmwd->selection.lpsellistThings->iDataCount > 0 && byObjectTypeFlags & POF_THING) iApplicableTypes++;

		if(iApplicableTypes > 1)
		{
			/* If the user cancels, SPT returns zero, which is equivalent to
			 * clearing all the checkboxes.
			 */
			byObjectTypeFlags &= SelectPresetTypes(g_hwndMain, byObjectTypeFlags);
		}

		/* byObjectTypeFlags might be zero now, but we don't need to check
		 * specially. The following conditionals will all fail, and we'll end up
		 * returning FALSE, which is what we want.
		 */
	}

	/* Apply sector properties if we have any, and we're in an appropriate mode
	 * and have some sectors selected.
	 */
	if((lpmwd->editmode == EM_SECTORS || lpmwd->editmode == EM_ANY) &&
		lpmwd->selection.lpsellistSectors->iDataCount > 0 &&
		byObjectTypeFlags & POF_SECTOR)
	{
		ApplySectorPresetToSelection(lpmwd->lpmap, lpmwd->selection.lpsellistSectors, &lppreset->sec, lppreset->dwSectorFlags);
		bMapChanged = TRUE;
	}

	/* Apply line properties if we have any, and we're in an appropriate mode
	 * and have some linedefs selected.
	 */
	if((lpmwd->editmode == EM_LINES || lpmwd->editmode == EM_ANY) &&
		lpmwd->selection.lpsellistSectors->iDataCount > 0 &&
		byObjectTypeFlags & POF_SECTOR)
	{
		ApplySectorPresetToSelection(lpmwd->lpmap, lpmwd->selection.lpsellistSectors, &lppreset->sec, lppreset->dwSectorFlags);
		bMapChanged = TRUE;
	}

	return bMapChanged;
}


/* GridProperties
 *   Allows the user to set the grid's properties.
 *
 * Parameters:
 *   HWND	hwndMap		Map window handle.
 *
 * Return value: None.
 *
 * Remarks:
 *   This handles redrawing etc. if necessary. The frame can also call us, which
 *   is why we use HWND and not MAPWINDATA.
 */
void GridProperties(HWND hwndMap)
{
	MAPWINDATA *lpmwd = (MAPWINDATA*)GetWindowLong(hwndMap, GWL_USERDATA);
	GRIDDLGDATA griddd;

	/* Get current values. */
	griddd.bShowAxes = lpmwd->mapview.bShowAxes;
	griddd.bShowGrid = lpmwd->mapview.bShowGrid;
	griddd.bShow64Grid = lpmwd->mapview.bShow64Grid;
	griddd.cx = lpmwd->mapview.cxGrid;
	griddd.cy = lpmwd->mapview.cyGrid;
	griddd.xOffset = lpmwd->mapview.xGridOffset;
	griddd.yOffset = lpmwd->mapview.yGridOffset;

	/* Show the dialogue. */
	if(ShowGridDlg(g_hwndMain, &griddd))
	{
		/* If the user confirmed, update the settings and redraw. */
		lpmwd->mapview.bShowAxes = griddd.bShowAxes;
		lpmwd->mapview.bShow64Grid = griddd.bShow64Grid;
		lpmwd->mapview.cxGrid = griddd.cx;
		lpmwd->mapview.cyGrid = griddd.cy;
		lpmwd->mapview.xGridOffset = griddd.xOffset;
		lpmwd->mapview.yGridOffset = griddd.yOffset;

		lpmwd->bWantRedraw = TRUE;
		UpdateStatusBar(lpmwd, SBPF_GRID);
	}
}


/* EditMapProperties
 *   Displays the Map Properties dialogue, and updates the map properties if the
 *   user changes them.
 *
 * Parameters:
 *   MAPWINDATA*	lpmwd		Map window data.
 *
 * Return value: None.
 */
static void EditMapProperties(MAPWINDATA *lpmwd)
{
	MAPOPTPPDATA mapoptppdata;

	/* Set initial values for use in the dialogue. */
	lstrcpyA(mapoptppdata.szLumpName, lpmwd->szLumpName);
	mapoptppdata.lpcfgMap = lpmwd->lpcfgMap;
	mapoptppdata.cGametype = ConfigGetInteger(lpmwd->lpcfgWadOptMap, "gametype");

	/* Get filename of additional wad if we have one. */
	if(lpmwd->iAdditionalWadID >= 0)
		GetWadFilename(lpmwd->iAdditionalWadID, mapoptppdata.szAddWadName, sizeof(mapoptppdata.szAddWadName) / sizeof(TCHAR));
	else
		*mapoptppdata.szAddWadName = TEXT('\0');

	/* Show the dialogue. */
	ShowMapProperties(&mapoptppdata);

	/* Make sure the user didn't cancel. */
	if(mapoptppdata.bOKed)
	{
		/* Update the properties. */
		lstrcpyA(lpmwd->szLumpName, mapoptppdata.szLumpName);
		ConfigSetInteger(lpmwd->lpcfgWadOptMap, "gametype", mapoptppdata.cGametype);

		/* Free a reference to the old additional wad. */
		if(lpmwd->iAdditionalWadID >= 0)
		{
			ReleaseWad(lpmwd->iAdditionalWadID);
			lpmwd->iAdditionalWadID = -1;
		}

		/* If an additional wad was specified, open it. */
		if(*mapoptppdata.szAddWadName)
		{
			lpmwd->iAdditionalWadID = LoadWad(mapoptppdata.szAddWadName);
			if(lpmwd->iAdditionalWadID < 0)
				MessageBoxFromStringTable(g_hwndMain, IDS_BADWAD, MB_ICONERROR);
		}

		/* Map configuration. */
		if(mapoptppdata.lpcfgMap)
		{
			lpmwd->lpcfgMap = mapoptppdata.lpcfgMap;
			lpmwd->lpcfgMainGame = GetOptionsForGame(mapoptppdata.lpcfgMap);

			/* Need to open new IWAD. */
			if(lpmwd->iIWadID >= 0)
				ReleaseWad(lpmwd->iIWadID);
			lpmwd->iIWadID = GetIWadForConfig(mapoptppdata.lpcfgMap);
			lpmwd->lpwadIWad = lpmwd->iIWadID >= 0 ? GetWad(lpmwd->iIWadID) : NULL;

			/* Remember in the options which config was specified. */
			StoreMapCfgForWad(lpmwd->lpcfgWadOpt, lpmwd->lpcfgMap);
		}

		/* Reset texture caches. */
		FreeTextureCaches(lpmwd);
		InitTextureCaches(lpmwd);

		/* Lumpname might have changed, so update title. */
		UpdateMapWindowTitle(lpmwd);
	}
}


/* RotateSelectedThingsToPoint
 *   Rotates the selected things to face a point.
 *
 * Parameters:
 *   MAPWINDATA*	lpmwd	Map window data.
 *   short			x, y	Point to face, in map co-ordinates.
 *
 * Return value: None.
 */
static void RotateSelectedThingsToPoint(MAPWINDATA *lpmwd, short x, short y)
{
	int i;

	for(i = 0; i < lpmwd->selection.lpsellistThings->iDataCount; i++)
		RotateThingToPoint(lpmwd->lpmap, lpmwd->selection.lpsellistThings->lpiIndices[i], x, y);
}


/* InitTextureCaches
 *   Initialises texture and flat caches, including name-lists.
 *
 * Parameters:
 *   MAPWINDATA*	lpmwd	Map window data.
 *
 * Return value: None.
 */
static void InitTextureCaches(MAPWINDATA *lpmwd)
{
	/* Get a list of all texture names. */
	lpmwd->lptnlFlats = CreateTextureNameList();
	lpmwd->lptnlTextures = CreateTextureNameList();
	if(lpmwd->iIWadID >= 0)
	{
		AddAllTextureNamesToList(lpmwd->lptnlFlats, lpmwd->lpwadIWad, TF_FLAT);
		AddAllTextureNamesToList(lpmwd->lptnlTextures, lpmwd->lpwadIWad, TF_TEXTURE);
	}
	if(lpmwd->iAdditionalWadID >= 0)
	{
		WAD *lpwadAdd = GetWad(lpmwd->iAdditionalWadID);

		AddAllTextureNamesToList(lpmwd->lptnlFlats, lpwadAdd, TF_FLAT);
		AddAllTextureNamesToList(lpmwd->lptnlTextures, lpwadAdd, TF_TEXTURE);
	}
	/* TODO: Also add textures from file itself? */

	SortTextureNameList(lpmwd->lptnlFlats);
	SortTextureNameList(lpmwd->lptnlTextures);

	/* Texture caching: both the image list and our own bitmaps. */
	/* TODO: Use the image list for both? */
	lpmwd->tcFlatsHdr.lptcNext = NULL;
	lpmwd->tcTexturesHdr.lptcNext = NULL;
	lpmwd->himlCacheFlats = NULL;
	lpmwd->himlCacheTextures = NULL;
}


/* FreeTextureCaches
 *   Frees texture and flat caches, including name-lists.
 *
 * Parameters:
 *   MAPWINDATA*	lpmwd	Map window data.
 *
 * Return value: None.
 */
static void FreeTextureCaches(MAPWINDATA *lpmwd)
{
	/* Clean the texture caches. */
	PurgeTextureCache(&lpmwd->tcFlatsHdr);
	PurgeTextureCache(&lpmwd->tcTexturesHdr);
	if(lpmwd->himlCacheFlats) ImageList_Destroy(lpmwd->himlCacheFlats);
	if(lpmwd->himlCacheTextures) ImageList_Destroy(lpmwd->himlCacheTextures);

	/* Free the list of texture names. */
	DestroyTextureNameList(lpmwd->lptnlFlats);
	DestroyTextureNameList(lpmwd->lptnlTextures);
}


/* TexPreviewClicked
 *   Called by the infobar when a texture preview is clicked. Passes a
 *   corresponding window message to the window.
 *
 * Parameters:
 *   HWND	hwndMap		Map window handle.
 *   char	cNotifyID	ID specifying which preview was clicked.
 *
 * Return value: None.
 */
void TexPreviewClicked(HWND hwndMap, char cNotifyID)
{
	SendMessage(hwndMap, WM_TEXPREVIEWCLICKED, cNotifyID, 0);
}


/* LoadPopupMenus, DestroyPopupMenus
 *   Respectively loads the popup menus from the resource, and frees them.
 *
 * Parameters: None.
 *
 * Return value: None.
 *
 * Remarks:
 *   The common items are in each menu in the resource. This involves some
 *   duplication of menu items, but appending menus is fiddly.
 */
void LoadPopupMenus(void)
{
	g_hmenuPopups = LoadMenu(g_hInstance, MAKEINTRESOURCE(IDM_POPUP));
}

void DestroyPopupMenus(void)
{
	DestroyMenu(g_hmenuPopups);
}


/* IsSelectionHomogeneous
 *   Determines whether the selection consists of only one type of object.
 *
 * Parameters:
 *   MAPWINDATA*	lpmwd	Map window data.
 *
 * Return value: BOOL
 *   TRUE if the selection is homogeneous; FALSE if not.
 *
 * Remarks:
 *   Lines and sectors are considered to be of the same type. An empty selection
 *   is considered homogeneous.
 */
static BOOL IsSelectionHomogeneous(MAPWINDATA *lpmwd)
{
	char c = 0;

	if(lpmwd->selection.lpsellistSectors->iDataCount > 0 || lpmwd->selection.lpsellistLinedefs->iDataCount > 0) c++;
	if(lpmwd->selection.lpsellistVertices->iDataCount > 0) c++;
	if(lpmwd->selection.lpsellistThings->iDataCount > 0) c++;

	return (c < 2);
}


/* BeginPreDrag
 *   Switches to the pre-dragging state that occurs upon a mousedown over an
 *   object.
 *
 * Parameters:
 *   MAPWINDATA*	lpmwd	Map window data.
 *
 * Return value: None.
 */
static void BeginPreDrag(MAPWINDATA *lpmwd)
{
	/* Clear the highlight, but save what we're pointing at first. */
	lpmwd->hobjDrag = lpmwd->highlightobj;
	UpdateHighlight(lpmwd, -1, -1);

	/* Store the current map co-ordinates of the mouse. These are reset
	 * with every mouse move.
	 */
	lpmwd->xDragLast = XMOUSE_WIN_TO_MAP(lpmwd->xMouseLast, lpmwd);
	lpmwd->yDragLast = YMOUSE_WIN_TO_MAP(lpmwd->yMouseLast, lpmwd);

	/* Remember where we started out. */
	InitEdgeScrollBoundaries(lpmwd, lpmwd->xMouseLast, lpmwd->yMouseLast);

	/* Set the state and redraw. */
	lpmwd->submode = ESM_PREDRAG;
	lpmwd->bWantRedraw = TRUE;
}


/* BeginInsertOperation
 *   Starts a line-drawing, vertex insertion or thing insertion operation,
 *   depending on the mode.
 *
 * Parameters:
 *   MAPWINDATA*	lpmwd	Map window data.
 *
 * Return value: None.
 */
static void BeginInsertOperation(MAPWINDATA *lpmwd)
{
	POINT ptMouse;
	GetCursorPos(&ptMouse);
	ScreenToClient(lpmwd->hwnd, &ptMouse);

	/* Remember where we started out. Can't use MouseLast because of menus. */
	InitEdgeScrollBoundaries(lpmwd, (unsigned short)ptMouse.x, (unsigned short)ptMouse.y);

	switch(lpmwd->editmode)
	{
	case EM_ANY:
	case EM_LINES:
	case EM_SECTORS:
	case EM_VERTICES:
		/* This is a prelude to ESM_DRAWING. Also, note that the
		 * map hasn't changed yet!
		 */
		DeselectAll(lpmwd);
		lpmwd->submode = ESM_INSERTING;

		BeginDrawOperation(&lpmwd->drawop);

		break;

	case EM_THINGS:
		DeselectAll(lpmwd);
		lpmwd->submode = ESM_INSERTING;
		break;

	default:
		break;
	}

	lpmwd->bWantRedraw = TRUE;
}


/* UpdateLassoedSelection
 *   Updates the selection state during a lasso operation in response to a
 *   mouse move.
 *
 * Parameters:
 *   MAPWINDATA*		lpmwd			Map window data.
 *   unsigned short		xMouse, yMouse	Mouse co-ordinates.
 *
 * Return value: BOOL
 *   TRUE if selection changed; FALSE if not.
 */
static BOOL UpdateLassoedSelection(MAPWINDATA *lpmwd, unsigned short xMouse, unsigned short yMouse)
{
	/* Some co-ordinates. */
	short xMapOrg = XMOUSE_WIN_TO_MAP(lpmwd->ptsSelectionStart.x, lpmwd);
	short yMapOrg = YMOUSE_WIN_TO_MAP(lpmwd->ptsSelectionStart.y, lpmwd);
	short xMapNew = XMOUSE_WIN_TO_MAP(xMouse, lpmwd);
	short yMapNew = YMOUSE_WIN_TO_MAP(yMouse, lpmwd);
	RECT rcNew = {min(xMapOrg, xMapNew), min(yMapOrg, yMapNew), max(xMapOrg, xMapNew), max(yMapOrg, yMapNew)};

	/* Determine whether we're interested in each type of object. */
	BOOL bWantThings = lpmwd->editmode == EM_ANY || lpmwd->editmode == EM_THINGS;
	BOOL bWantVertices = lpmwd->editmode == EM_ANY || lpmwd->editmode == EM_VERTICES;
	BOOL bWantLines = lpmwd->editmode == EM_ANY || lpmwd->editmode == EM_SECTORS || lpmwd->editmode == EM_LINES;

	/* Even if we're not selecting vertices, we need to find whether they're in
	 * the rectangle.
	 */
	BOOL *lpbVerticesInRect = ProcHeapAlloc(lpmwd->lpmap->iVertices * sizeof(BOOL));

	BOOL bSelChanged = FALSE;
	int i;

	/* Things are easy. */
	if(bWantThings)
	{
		for(i = 0; i < lpmwd->lpmap->iThings; i++)
		{
			MAPTHING *lpthing = &lpmwd->lpmap->things[i];
			POINT ptThing = {lpthing->x, lpthing->y};
			BOOL bInRect = PtInRect(&rcNew, ptThing);

			/* If it's really selected, we have nothing to do. */
			if(!(lpthing->selected & SLF_SELECTED))
			{
				if(lpthing->selected & SLF_LASSOING)
				{
					/* Deselect if necessary. */
					if(!bInRect)
					{
						lpthing->selected &= ~SLF_LASSOING;
						RemoveFromSelectionList(lpmwd->selection.lpsellistThings, i);
						bSelChanged = TRUE;
					}
				}
				/* Select if necessary. */
				else if(bInRect)
				{
					lpthing->selected |= SLF_LASSOING;
					AddToSelectionList(lpmwd->selection.lpsellistThings, i);
					bSelChanged = TRUE;
				}
			}
		}
	}

	/* Check whether each vertex is in the rectangle, also doing selection/
	 * deselection if we're interested in vertices.
	 */
	for(i = 0; i < lpmwd->lpmap->iVertices; i++)
	{
		MAPVERTEX *lpvertex = &lpmwd->lpmap->vertices[i];
		POINT ptVertex = {lpvertex->x, lpvertex->y};
		lpbVerticesInRect[i] = PtInRect(&rcNew, ptVertex);

		if(bWantVertices)
		{
			/* If it's really selected, we have nothing to do. */
			if(!(lpvertex->selected & SLF_SELECTED))
			{
				if(lpvertex->selected & SLF_LASSOING)
				{
					/* Deselect if necessary. */
					if(!lpbVerticesInRect[i])
					{
						lpvertex->selected &= ~SLF_LASSOING;
						RemoveFromSelectionList(lpmwd->selection.lpsellistVertices, i);
						bSelChanged = TRUE;
					}
				}
				/* Select if necessary. */
				else if(lpbVerticesInRect[i])
				{
					lpvertex->selected |= SLF_LASSOING;
					AddToSelectionList(lpmwd->selection.lpsellistVertices, i);
					bSelChanged = TRUE;
				}
			}
		}
	}


	/* Now lines. We use vertex states to determine whether these should be
	 * selected.
	 */
	if(bWantLines)
	{
		for(i = 0; i < lpmwd->lpmap->iLinedefs; i++)
		{
			MAPLINEDEF *lpld = &lpmwd->lpmap->linedefs[i];

			/* If it's really selected, we have nothing to do. */
			if(!(lpld->selected & SLF_SELECTED))
			{
				if(lpld->selected & SLF_LASSOING)
				{
					/* Deselect if necessary. */
					if(!lpbVerticesInRect[lpld->v1] || !lpbVerticesInRect[lpld->v2])
					{
						lpld->selected &= ~SLF_LASSOING;
						RemoveFromSelectionList(lpmwd->selection.lpsellistLinedefs, i);
						bSelChanged = TRUE;
					}
				}
				/* Select if necessary. */
				else if(lpbVerticesInRect[lpld->v1] && lpbVerticesInRect[lpld->v2])
				{
					lpld->selected |= SLF_LASSOING;
					AddToSelectionList(lpmwd->selection.lpsellistLinedefs, i);
					bSelChanged = TRUE;
				}
			}
		}
	}

	/* Finished with vertex states now. */
	ProcHeapFree(lpbVerticesInRect);


	/* Update the sector selection state. This doesn't interfere with the real
	 * selection. Begin by deselecting sectors we selected before.
	 */
	for(i = 0; i < lpmwd->lpmap->iSectors; i++)
	{
		MAPSECTOR *lpsec = &lpmwd->lpmap->sectors[i];

		if(lpsec->selected & SLF_LASSOING)
		{
			lpsec->selected &= ~SLF_LASSOING;
			RemoveFromSelectionList(lpmwd->selection.lpsellistSectors, i);
		}
	}

	/* And reselect ones we ought to. */
	SelectSectorsFromLines(lpmwd);

	/* Don't want stray lines in sectors mode. */
	if(lpmwd->editmode == EM_SECTORS)
		DeselectLinesFromPartialSectors(lpmwd);

	return bSelChanged;
}


/* EndSelectOperation
 *   Returns editing mode to normal and makes lassoed objects properly selected.
 *
 * Parameters:
 *   MAPWINDATA*	lpmwd	Map window data.
 *
 * Return value: None.
 */
static void EndSelectOperation(MAPWINDATA *lpmwd)
{
	int i;

	/* Back to normal. */
	lpmwd->submode = ESM_NONE;

	/* Lassoed objects graduate to fully-fledged selected objects. */
	
	for(i = 0; i < lpmwd->lpmap->iLinedefs; i++)
	{
		MAPLINEDEF *lpld = &lpmwd->lpmap->linedefs[i];
		if(lpld->selected & SLF_LASSOING)
		{
			lpld->selected &= ~SLF_LASSOING;
			lpld->selected |= SLF_SELECTED;
		}
	}

	for(i = 0; i < lpmwd->lpmap->iSectors; i++)
	{
		MAPSECTOR *lpsec = &lpmwd->lpmap->sectors[i];
		if(lpsec->selected & SLF_LASSOING)
		{
			lpsec->selected &= ~SLF_LASSOING;
			lpsec->selected |= SLF_SELECTED;
		}
	}

	for(i = 0; i < lpmwd->lpmap->iVertices; i++)
	{
		MAPVERTEX *lpvx = &lpmwd->lpmap->vertices[i];
		if(lpvx->selected & SLF_LASSOING)
		{
			lpvx->selected &= ~SLF_LASSOING;
			lpvx->selected |= SLF_SELECTED;
		}
	}

	for(i = 0; i < lpmwd->lpmap->iThings; i++)
	{
		MAPTHING *lpthing = &lpmwd->lpmap->things[i];
		if(lpthing->selected & SLF_LASSOING)
		{
			lpthing->selected &= ~SLF_LASSOING;
			lpthing->selected |= SLF_SELECTED;
		}
	}
}


/* EdgeScrolling
 *   Scrolls the view if the mouse is sufficiently close to the edge of the
 *   screen.
 *
 * Parameters:
 *   MAPWINDATA*	lpmwd	Map window data.
 *
 * Return value: BOOL
 *   TRUE if we scrolled, FALSE if we didn't.
 */
static BOOL EdgeScrolling(MAPWINDATA *lpmwd)
{
	RECT rcClient;
	BOOL bScrolled = FALSE;

	GetClientRect(lpmwd->hwnd, &rcClient);

	/* Make sure we're in the window, first of all. */
	/* TODO: If we're captured...? */
	if(lpmwd->xMouseLast >= 0 && lpmwd->yMouseLast >= 0)
	{
		int iIncrement;

		/* Update the scroll boundaries. */
		lpmwd->rcScrollBounds.left = min(max(lpmwd->rcScrollBounds.left, lpmwd->xMouseLast), EDGESCROLL_BORDER);
		lpmwd->rcScrollBounds.right = max(min(lpmwd->rcScrollBounds.right, lpmwd->xMouseLast), rcClient.right - EDGESCROLL_BORDER - 1);
		lpmwd->rcScrollBounds.top = min(max(lpmwd->rcScrollBounds.top, lpmwd->yMouseLast), EDGESCROLL_BORDER);
		lpmwd->rcScrollBounds.bottom = max(min(lpmwd->rcScrollBounds.bottom, lpmwd->yMouseLast), rcClient.bottom - EDGESCROLL_BORDER - 1);

		/* Left. */
		if(lpmwd->xMouseLast < lpmwd->rcScrollBounds.left)
		{
			iIncrement = EDGESCROLL_INCREMENT * (Log2Floor(lpmwd->rcScrollBounds.left - lpmwd->xMouseLast) + 1);

			/* Move boundaries and points relative to the client area. */
			lpmwd->mapview.xLeft -= iIncrement / lpmwd->mapview.fZoom;
			lpmwd->ptsSelectionStart.x += iIncrement;
			lpmwd->xMouseLast += iIncrement;

			bScrolled = TRUE;
		}

		/* Right. */
		if(lpmwd->xMouseLast > lpmwd->rcScrollBounds.right)
		{
			iIncrement = EDGESCROLL_INCREMENT * (Log2Floor(lpmwd->xMouseLast - lpmwd->rcScrollBounds.right) + 1);

			/* Move boundaries and points relative to the client area. */
			lpmwd->mapview.xLeft += iIncrement / lpmwd->mapview.fZoom;
			lpmwd->ptsSelectionStart.x -= iIncrement;
			lpmwd->xMouseLast -= iIncrement;

			bScrolled = TRUE;
		}

		/* Top. */
		if(lpmwd->yMouseLast < lpmwd->rcScrollBounds.top)
		{
			iIncrement = EDGESCROLL_INCREMENT * (Log2Floor(lpmwd->rcScrollBounds.top - lpmwd->yMouseLast) + 1);

			/* Move boundaries and points relative to the client area. */
			lpmwd->mapview.yTop += iIncrement / lpmwd->mapview.fZoom;
			lpmwd->ptsSelectionStart.y += iIncrement;
			lpmwd->yMouseLast += iIncrement;

			bScrolled = TRUE;
		}

		/* Bottom. */
		if(lpmwd->yMouseLast > lpmwd->rcScrollBounds.bottom)
		{
			iIncrement = EDGESCROLL_INCREMENT * (Log2Floor(lpmwd->yMouseLast - lpmwd->rcScrollBounds.bottom) + 1);

			/* Move boundaries and points relative to the client area. */
			lpmwd->mapview.yTop -= iIncrement / lpmwd->mapview.fZoom;
			lpmwd->ptsSelectionStart.y -= iIncrement;
			lpmwd->yMouseLast -= iIncrement;

			bScrolled = TRUE;
		}
	}

	return bScrolled;
}


/* InitEdgeScrollBoundaries
 *   Initialises the regions in which edge scrolling occurs. They are altered
 *   during the process of scrolling.
 *
 * Parameters:
 *   MAPWINDATA*		lpmwd			Map window data.
 *   unsigned short		xMouse, yMouse	Mouse co-ordinates in the client area.
 *
 * Return value: BOOL
 *   TRUE if we scrolled, FALSE if we didn't.
 */
static void InitEdgeScrollBoundaries(MAPWINDATA *lpmwd, unsigned short xMouse, unsigned short yMouse)
{
	RECT rcClient;
	GetClientRect(lpmwd->hwnd, &rcClient);

	lpmwd->rcScrollBounds.left = min(xMouse + 1, EDGESCROLL_BORDER);
	lpmwd->rcScrollBounds.right = max(xMouse, rcClient.right - EDGESCROLL_BORDER - 1);
	lpmwd->rcScrollBounds.top = min(yMouse + 1, EDGESCROLL_BORDER);
	lpmwd->rcScrollBounds.bottom = max(yMouse, rcClient.bottom - EDGESCROLL_BORDER - 1);
}


/* EditorInsertThing
 *   Performs the necessary processing in response to a user's request to insert
 *   a thing.
 *
 * Parameters:
 *   MAPWINDATA*		lpmwd			Map window data.
 *   unsigned short		xMap, yMap		Map co-ordinates at which to insert.
 *
 * Return value: None.
 *
 * Remarks:
 *   Undoing is handled here. The user may cancel if dialogues are enabled.
 */
static void EditorInsertThing(MAPWINDATA *lpmwd, short xMap, short yMap)
{
	HIGHLIGHTOBJ hobj;
	BOOL bKeepThing = TRUE;

	/* Create an undo snapshot. */
	CreateUndo(lpmwd, IDS_UNDO_INSERTTHING);

	hobj.emType = EM_THINGS;
	hobj.iIndex = InsertThing(lpmwd, xMap, yMap);

	if(ConfigGetInteger(g_lpcfgMain, "newthingdialogue"))
	{
		/* Select the thing. */
		AddObjectToSelection(lpmwd, &hobj);

		/* Show the dialogue and then clear the selection. */
		bKeepThing = ShowTypedSelectionProperties(lpmwd, FALSE, EM_THINGS);
		DeselectAll(lpmwd);

		/* Cancelled the dialogue, so revert to previous state. */
		if(!bKeepThing)
			PerformUndo(lpmwd);
	}

	/* We really did insert a thing. */
	if(bKeepThing)
	{
		/* Too late to cancel now. */
		ClearRedoStack(lpmwd);
		lpmwd->bMapChanged = TRUE;
	}

	UpdateStatusBar(lpmwd, SBPF_THINGS);
}
