#include <windows.h>
#include <commctrl.h>
#include <stdio.h>
#include <tchar.h>

#include "../general.h"
#include "../texture.h"
#include "../../res/resource.h"
#include "texbrowser.h"
#include "mapwin.h"

/* TODO: Move this somewhere else. */
#if (_WIN32_WINNT < 0x0501) || defined(__MINGW32__)
typedef struct _LVTILEINFO
{
	UINT cbSize;
	int iItem;
	UINT cColumns;
	PUINT puColumns;
} LVTILEINFO;

/* This is needed with MS SDK, too, if _WIN32_WINNT is small. */
#ifndef LVM_SETTILEINFO
#define LVM_SETTILEINFO (LVM_FIRST + 164)
#endif

#endif


/* Types. */
typedef struct _TEXBROWSERDATA
{
	HWND			hwndMap;
	TEX_FORMAT		tf;
	LPTSTR			szTexName;
	TEXTURENAMELIST *lptnlAll, *lptnlUsed;
	HIMAGELIST		*lphimlCache;
} TEXBROWSERDATA;

/* Static prototypes. */
static BOOL CALLBACK TexBrowserProc(HWND hwndDlg, UINT uiMsg, WPARAM wParam, LPARAM lParam);
static void BuildTextureImageList(HWND hwndList, HWND hwndMap, TEXTURENAMELIST *lptnl, TEX_FORMAT tf, HIMAGELIST himl);
static void FillTextureList(HWND hwndList, HWND hwndMap, TEXTURENAMELIST *lptnl);


/* SelectTexture
 *   Displays the texture browser and allows the user to select a texture.
 *
 * Parameters:
 *   HWND				hwndParent		Parent window for dialogue.
 *   HWND				hwndMap			Map window handle.
 *   TEX_FORMAT			tf				Texture format.
 *   HIMAGELIST*		lphimlCache		Cached image list.
 *   TEXTURENAMELIST*	lptnlAll		*Sorted* list of all texture names.
 *   TEXTURENAMELIST*	lptnlUsed		*Sorted* list of used texture names.
 *   LPTSTR				szTexName		Name of texture to select initially, and
 *										buffer used to return selection. Must be
 *										at least TEXNAME_BUFFER_LENGTH long.
 *
 * Return value: BOOL
 *   FALSE if user cancelled; TRUE otherwise.
 */
BOOL SelectTexture(HWND hwndParent, HWND hwndMap, TEX_FORMAT tf, HIMAGELIST* lphimlCache, TEXTURENAMELIST *lptnlAll, TEXTURENAMELIST *lptnlUsed, LPTSTR szTexName)
{
	TEXBROWSERDATA tbd = {hwndMap, tf, szTexName, lptnlAll, lptnlUsed, lphimlCache};

	return (BOOL)DialogBoxParam(g_hInstance, MAKEINTRESOURCE(IDD_TEXBROWSER), hwndParent, TexBrowserProc, (LPARAM)&tbd);
}


/* TexBrowserProc
 *   Dialogue procedure for the texture browser.
 *
 * Parameters:
 *   HWND	hwndDlg		Handle to dialogue window.
 *   HWND	uiMsg		Window message ID.
 *   WPARAM	wParam		Message-specific.
 *   LPARAM lParam		Message-specific.
 *
 * Return value: BOOL
 *   TRUE if message was processed; FALSE otherwise.
 */
static BOOL CALLBACK TexBrowserProc(HWND hwndDlg, UINT uiMsg, WPARAM wParam, LPARAM lParam)
{
	static TEXBROWSERDATA *s_lptbd;
	static POINT s_ptListPadding;
	static POINT s_ptOKFromBR, s_ptCancelFromBR, s_ptHelpFromBR;

	switch(uiMsg)
	{
	case WM_INITDIALOG:
		{
			RECT rcDlg, rcDlgClient;
			RECT rcChild;
			POINT ptClientBR;
			LVCOLUMN lvcol;
			HWND hwndList = GetDlgItem(hwndDlg, IDC_LIST);
			LVFINDINFO lvfi;
			LVITEM lvi;

			/* Store parameter. */
			s_lptbd = (TEXBROWSERDATA*)lParam;

			/* Not only is the tile view not supported in old versions of the
			 * Common Controls, the LVM_SETVIEW message isn't, either.
			 *
			 * Icon is actually better. Allow user to change? Maybe only on XP?
			 * if(g_bHasCC6)
			 *	SendMessage(hwndList, LVM_SETVIEW, LV_VIEW_TILE, 0);
			 */

			lvcol.mask = LVCF_SUBITEM;
			lvcol.iSubItem = 0;
			ListView_InsertColumn(hwndList, 1, &lvcol);

			lvcol.mask = LVCF_SUBITEM;
			lvcol.iSubItem = 1;
			ListView_InsertColumn(hwndList, 2, &lvcol);

			/* Spacing includes icon dimensions. */
			ListView_SetIconSpacing(hwndList, 80, 96);

			/* If we don't already have an image list, make one. */
			if(!*s_lptbd->lphimlCache)
			{
				/* Create image-list. */
				*s_lptbd->lphimlCache = ImageList_Create(64, 64, ILC_COLOR24, s_lptbd->lptnlAll->iEntries, 0);

				BuildTextureImageList(hwndList, s_lptbd->hwndMap, s_lptbd->lptnlAll, s_lptbd->tf, *s_lptbd->lphimlCache);
			}

			/* Set image list. */
			ListView_SetImageList(hwndList, *s_lptbd->lphimlCache, LVSIL_NORMAL);

			/* Fill the texture list with the textures. */
			FillTextureList(hwndList, s_lptbd->hwndMap, s_lptbd->lptnlAll);

			GetWindowRect(hwndDlg, &rcDlg);
			GetClientRect(hwndDlg, &rcDlgClient);

			/* Get list-view padding. */
			GetWindowRect(hwndList, &rcChild);
			MapWindowPoints(NULL, hwndDlg, (LPPOINT)(LPVOID)&rcChild, 2);
			s_ptListPadding.x = rcDlgClient.right - rcChild.right + rcChild.left;
			s_ptListPadding.y = rcDlgClient.bottom - rcChild.bottom + rcChild.top;

			/* Bottom-right of client in screen co-ords. */
			ptClientBR.x = rcDlgClient.right;
			ptClientBR.y = rcDlgClient.bottom;
			ClientToScreen(hwndDlg, &ptClientBR);

			/* Get positions of the buttons relative to the bottom-right. */
			GetWindowRect(GetDlgItem(hwndDlg, IDOK), &rcChild);
			s_ptOKFromBR.x = ptClientBR.x - rcChild.left;
			s_ptOKFromBR.y = ptClientBR.y - rcChild.top;

			GetWindowRect(GetDlgItem(hwndDlg, IDCANCEL), &rcChild);
			s_ptCancelFromBR.x = ptClientBR.x - rcChild.left;
			s_ptCancelFromBR.y = ptClientBR.y - rcChild.top;

			GetWindowRect(GetDlgItem(hwndDlg, IDC_BUTTON_HELP), &rcChild);
			s_ptHelpFromBR.x = ptClientBR.x - rcChild.left;
			s_ptHelpFromBR.y = ptClientBR.y - rcChild.top;

			/* Select the specified texture. */
			lvfi.flags = LVFI_STRING;
			lvfi.psz = s_lptbd->szTexName;
			lvi.iItem = ListView_FindItem(hwndList, -1, &lvfi);

			if(lvi.iItem >= 0)
			{
				lvi.mask = LVIF_STATE;
				lvi.iSubItem = 0;
				lvi.state = lvi.stateMask = LVIS_SELECTED;
				ListView_SetItem(hwndList, &lvi);
				ListView_EnsureVisible(hwndList, lvi.iItem, FALSE);
			}

			SetFocus(hwndList);
		}

		return FALSE;	/* We set the focus. */

	case WM_SIZE:
		{
			HDWP hdwp = BeginDeferWindowPos(4);
			RECT rcClient;
			int cx, cy;

			GetClientRect(hwndDlg, &rcClient);
			cx = rcClient.right;
			cy = rcClient.bottom;

			DeferWindowPos(hdwp, GetDlgItem(hwndDlg, IDC_LIST), NULL, -1, -1, cx - s_ptListPadding.x, cy - s_ptListPadding.y, SWP_NOMOVE | SWP_NOZORDER);
			DeferWindowPos(hdwp, GetDlgItem(hwndDlg, IDOK), NULL, cx - s_ptOKFromBR.x, cy - s_ptOKFromBR.y, -1, -1, SWP_NOSIZE | SWP_NOZORDER);
			DeferWindowPos(hdwp, GetDlgItem(hwndDlg, IDCANCEL), NULL, cx - s_ptCancelFromBR.x, cy - s_ptCancelFromBR.y, -1, -1, SWP_NOSIZE | SWP_NOZORDER);
			DeferWindowPos(hdwp, GetDlgItem(hwndDlg, IDC_BUTTON_HELP), NULL, cx - s_ptHelpFromBR.x, cy - s_ptHelpFromBR.y, -1, -1, SWP_NOSIZE | SWP_NOZORDER);

			EndDeferWindowPos(hdwp);
		}

		return TRUE;

	case WM_COMMAND:
		switch(LOWORD(wParam))
		{
		case IDOK:
			{
				HWND hwndList = GetDlgItem(hwndDlg, IDC_LIST);

				ListView_GetItemText(hwndList, ListView_GetNextItem(hwndList, -1, LVIS_SELECTED), 0, s_lptbd->szTexName, TEXNAME_BUFFER_LENGTH);
				EndDialog(hwndDlg, TRUE);
			}
			return TRUE;

		case IDCANCEL:
			EndDialog(hwndDlg, FALSE);
			return TRUE;
		}

		break;

	case WM_NOTIFY:
		switch(wParam)
		{
		/* Texture list-view. */
		case IDC_LIST:
			{
				NMLISTVIEW *lpnmlv = (NMLISTVIEW*)lParam;
				switch(lpnmlv->hdr.code)
				{
				case LVN_ITEMCHANGED:
					/* Deselection always precedes selection, so this is
					 * okay.
					 */
					EnableWindow(GetDlgItem(hwndDlg, IDOK), lpnmlv->uNewState & LVIS_SELECTED);
					return TRUE;

				case LVN_ITEMACTIVATE:
					SendMessage(hwndDlg, WM_COMMAND, MAKELONG(IDOK, BN_CLICKED), (LPARAM)GetDlgItem(hwndDlg, IDOK));
					return TRUE;
				}
			}

			break;
		}

		break;
	}

	/* Didn't process message. */
	return FALSE;
}



static void BuildTextureImageList(HWND hwndList, HWND hwndMap, TEXTURENAMELIST *lptnl, TEX_FORMAT tf, HIMAGELIST himl)
{
	HBITMAP hbm;
	HDC hdc, hdcList;
	int i;

	ImageList_RemoveAll(himl);

	hdcList = GetDC(hwndList);
	hbm = CreateCompatibleBitmap(hdcList, 64, 64);
	hdc = CreateCompatibleDC(NULL);

	ImageList_SetImageCount(himl, lptnl->iEntries);

	for(i = 0; i < lptnl->iEntries; i++)
	{
		TEXTURE *lptex;
		HBITMAP hbmOld;
		BOOL bNeedFree;

		/* Skip duplicate entries. */
		if(i > 0 && lstrcmp(&lptnl->szNames[(i-1)*TEXNAME_BUFFER_LENGTH], &lptnl->szNames[i*TEXNAME_BUFFER_LENGTH]) == 0) continue;

		hbmOld = SelectObject(hdc, hbm);

		bNeedFree = GetTextureForMap(hwndMap, &lptnl->szNames[i * TEXNAME_BUFFER_LENGTH], &lptex, tf);
		StretchTextureToDC(lptex, hdc, 64, 64);
		if(bNeedFree) DestroyTexture(lptex);

		SelectObject(hdc, hbmOld);
		ImageList_Replace(himl, i, hbm, NULL);
	}

	DeleteDC(hdc);
	DeleteObject(hbm);
	ReleaseDC(hwndList, hdcList);
}



static void FillTextureList(HWND hwndList, HWND hwndMap, TEXTURENAMELIST *lptnl)
{
	int i;

	UNREFERENCED_PARAMETER(hwndMap);

	for(i = 0; i < lptnl->iEntries; i++)
	{
		LVITEM lvitem;
		TCHAR szDimensions[32];
		int iNewItem;
		unsigned int uiCol = 1;

		/* Skip duplicate entries. */
		if(i > 0 && lstrcmp(&lptnl->szNames[(i-1)*TEXNAME_BUFFER_LENGTH], &lptnl->szNames[i*TEXNAME_BUFFER_LENGTH]) == 0) continue;

		/* Add the image. */
		lvitem.mask = LVIF_TEXT | LVIF_IMAGE;

		/* Image list order is same as texture order. */
		lvitem.iImage = i;

		lvitem.cchTextMax = TEXNAME_BUFFER_LENGTH;
		lvitem.pszText = &lptnl->szNames[i * TEXNAME_BUFFER_LENGTH];
		lvitem.iItem = 0;
		lvitem.iSubItem = 0;
		iNewItem = ListView_InsertItem(hwndList, &lvitem);

		/* TODO: Show the dimensions as a subitem. This was implemented, but it
		 * broke when you stated caching image lists. Fix it. Maybe by recording
		 * dimensions in BuildTextureImageList?
		 */
		_sntprintf(szDimensions, sizeof(szDimensions) / sizeof(TCHAR), TEXT("TODO"));
		ListView_SetItemText(hwndList, iNewItem, 1, szDimensions);
		if(g_bHasCC6)
		{
			LVTILEINFO lvti;
			lvti.cbSize = sizeof(lvti);
			lvti.iItem = iNewItem;
			lvti.cColumns = 1;
			lvti.puColumns = &uiCol;
			SendMessage(hwndList, LVM_SETTILEINFO, 0, (LPARAM)&lvti);
		}
	}
}

