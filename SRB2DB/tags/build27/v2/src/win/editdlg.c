#include <windows.h>
#include <commctrl.h>

#include <tchar.h>
#include <stdio.h>

#include "../general.h"
#include "../map.h"
#include "../selection.h"
#include "../config.h"
#include "../options.h"
#include "../editing.h"
#include "../texture.h"
#include "../mapconfig.h"
#include "../preset.h"
#include "../../res/resource.h"

#include "editdlg.h"
#include "mapwin.h"
#include "mdiframe.h"
#include "infobar.h"
#include "gendlg.h"
#include "texbrowser.h"


#if _WIN32_WINNT < 0x0500
#define IDC_HAND MAKEINTRESOURCE(32649)
#endif

#define EDGESMIN 3
#define EDGESMAX 32767
#define EDGESINIT 4
#define RADIUSMIN 0
#define RADIUSMAX 32767

#define ANGLEMIN ((signed short)0x8000)
#define ANGLEMAX ((signed short)0x7FFF)

#define RESIZEFACTORMIN 10
#define RESIZEINIT 100
#define RESIZEFACTORMAX 1000


/* Types. */
typedef struct _TYPETREEDATA
{
	HWND		hwndTree;
	CONFIG		*lpcfgTypeToNode;
	HTREEITEM	htreeitemParent;
} TYPETREEDATA;

typedef struct _AFTLDATA
{
	HWND	hwndListView;
	WORD	wFlags;
	WORD	wFlagMask;
} AFTLDATA;

typedef struct _SECTOR_RELATIVE
{
	int iFloor, iCeil, iLight;
} SECTOR_RELATIVE;

typedef struct _LINEDEF_RELATIVE
{
	int xFront, yFront, xBack, yBack;
} LINEDEF_RELATIVE;

typedef struct _THING_RELATIVE
{
	int x, y, z, iAngle;
} THING_RELATIVE;


/* Static prototypes. */
static BOOL CALLBACK SectorsPropPageProc(HWND hwndPropPage, UINT uiMsg, WPARAM wParam, LPARAM lParam);
static BOOL CALLBACK LinesPropPageProc(HWND hwndPropPage, UINT uiMsg, WPARAM wParam, LPARAM lParam);
static void InitSectorsPropPageFields(HWND hwndPropPage, SECTORDISPLAYINFO *lpsdi, DWORD dwCheckFlags);
static void InitLinesPropPageFields(HWND hwndPropPage, LINEDEFDISPLAYINFO *lplddi, DWORD dwCheckFlags);
static void InitThingsPropPageFields(HWND hwndPropPage, THINGDISPLAYINFO *lpsdi, DWORD dwCheckFlags);
static BOOL AddSectorTypeToList(CONFIG *lpcfg, void *lpvWindow);
static BOOL AddFlagToList(CONFIG *lpcfg, void *lpvWindow);
static BOOL AddCategoryToTree(CONFIG *lpcfgCategory, void *lpv);
static BOOL AddTypeToTree(CONFIG *lpcfgType, void *lpv);
static void SetTexturePreviewImage(HWND hwndMap, HWND hwndCtrl, LPCTSTR szTexName, TEX_FORMAT tf, HBITMAP hbmPreview, BOOL bRequired);
static void GetFlagsFromListView(HWND hwndListView, WORD *lpwFlagValues, WORD *lpwFlagMask);
static BOOL CALLBACK ThingsPropPageProc(HWND hwndPropPage, UINT uiMsg, WPARAM wParam, LPARAM lParam);
static BOOL CALLBACK InsertSectorDlgProc(HWND hwndDlg, UINT uiMsg, WPARAM wParam, LPARAM lParam);
static BOOL CALLBACK RotateDlgProc(HWND hwndDlg, UINT uiMsg, WPARAM wParam, LPARAM lParam);
static BOOL CALLBACK ResizeDlgProc(HWND hwndDlg, UINT uiMsg, WPARAM wParam, LPARAM lParam);
static BOOL CALLBACK SelectPresetTypesDlgProc(HWND hwndDlg, UINT uiMsg, WPARAM wParam, LPARAM lParam);
static BOOL CALLBACK AlignDlgProc(HWND hwndDlg, UINT uiMsg, WPARAM wParam, LPARAM lParam);
static BOOL CALLBACK MissingTexProc(HWND hwndDlg, UINT uiMsg, WPARAM wParam, LPARAM lParam);


/* ShowMapObjectProperties
 *   Shows the Properties dialogue for the selection.
 *
 * Parameters:
 *   DWORD		dwPageFlags		Flags determining which pages to show.
 *   DWORD		dwStartPage		Flag indicating which page to show first. -1 if
 *								the caller doesn't care.
 *   LPCTSTR	szCaption		Window caption, sans " Properties".
 *   MAPPPDATA*	lpmapppdata		Selection info and map data.
 *
 * Return value: BOOL
 *   TRUE if the user OKed to exit the dialogue; FALSE if cancelled.
 *
 * Remarks:
 *   All the processing is done by the property sheet pages. This function just
 *   shows the dialogue box.
 */
BOOL ShowMapObjectProperties(DWORD dwPageFlags, DWORD dwStartPage, LPCTSTR szCaption, MAPPPDATA *lpmapppdata)
{
	PROPSHEETHEADER		psh;
	PROPSHEETPAGE		psp[5];
	int					iNumPages, i;

	/* Show the first page initially, unless we were told otherwise. */
	short				nStartPage = 0;


	/* Check which pages were requested, and fill in the necessary info. */

	iNumPages = 0;

	if(dwPageFlags & MPPF_SECTOR)
	{
		psp[iNumPages].pfnDlgProc = SectorsPropPageProc;
		/* TODO: 1.1 */
		psp[iNumPages].pszTemplate = MAKEINTRESOURCE(IDD_PP_SECTORS_109);

		/* Is this the page to show first? */
		if(dwStartPage == MPPF_SECTOR) nStartPage = iNumPages;

		iNumPages++;
	}

	if(dwPageFlags & MPPF_LINE)
	{
		psp[iNumPages].pfnDlgProc = LinesPropPageProc;
		psp[iNumPages].pszTemplate = MAKEINTRESOURCE(IDD_PP_LINES);

		/* Is this the page to show first? */
		if(dwStartPage == MPPF_LINE) nStartPage = iNumPages;

		iNumPages++;
	}

	if(dwPageFlags & MPPF_THING)
	{
		psp[iNumPages].pfnDlgProc = ThingsPropPageProc;
		psp[iNumPages].pszTemplate = MAKEINTRESOURCE(IDD_PP_THINGS);

		/* Is this the page to show first? */
		if(dwStartPage == MPPF_THING) nStartPage = iNumPages;

		iNumPages++;
	}


	/* Fill in fields common to all pages. */
	for(i = 0; i < iNumPages; i++)
	{
		psp[i].dwSize = sizeof(PROPSHEETPAGE);
		psp[i].dwFlags = /*PSP_HASHELP */ 0;
		psp[i].lParam = (LONG)lpmapppdata;
		psp[i].hInstance = g_hInstance;
	}

	/* Properties affecting the whole dialogue. */
	psh.dwSize = sizeof(psh);
	psh.dwFlags = PSH_HASHELP | PSH_NOAPPLYNOW | PSH_PROPTITLE | PSH_PROPSHEETPAGE;
	psh.hwndParent = g_hwndMain;
	psh.nPages = iNumPages;
	psh.nStartPage = nStartPage;
	psh.ppsp = psp;
	psh.pszCaption = szCaption;

	/* Assume we cancelled. */
	lpmapppdata->bOKed = FALSE;

	/* Go! */
	PropertySheet(&psh);

	/* Did we close by clicking OK? */
	return lpmapppdata->bOKed;
}



/* SectorsPropPageProc
 *   Dialogue proc for sectors property page.
 *
 * Parameters:
 *   HWND	hwndPropPage	Property page window handle.
 *   UINT	uiMsg			Message identifier.
 *   WPARAM wParam			Message-specific.
 *   LPARAM lParam			Message-specific.
 *
 * Return value: BOOL
 *   TRUE if the message was processed; FALSE otherwise.
 */
static BOOL CALLBACK SectorsPropPageProc(HWND hwndPropPage, UINT uiMsg, WPARAM wParam, LPARAM lParam)
{
	static MAPPPDATA *s_lpmapppdata;
	static BOOL s_bInitComplete;
	static HBITMAP s_hbmCeil, s_hbmFloor;

	/* Not hugely elegant, but the alternative is subclassing... */
	static TCHAR s_szPrevTFloor[TEXNAME_BUFFER_LENGTH], s_szPrevTCeil[TEXNAME_BUFFER_LENGTH];

	switch(uiMsg)
	{
	case WM_INITDIALOG:
		{
			SECTORDISPLAYINFO sdi;
			DWORD dwCheckFlags;
			CONFIG *lpcfgSecTypes;
			UDACCEL udaccelHeight[2], udaccelOther[1];
			int i;
			short nMaxAdjCeil, nMinAdjCeil, nMaxAdjFloor, nMinAdjFloor;
			HDC hdcDlg;

			/* Stop interactions between controls until we're ready. */
			s_bInitComplete = FALSE;

			/* Get the map data. */
			s_lpmapppdata = (MAPPPDATA*)((PROPSHEETPAGE*)lParam)->lParam;

			/* Set the texture previews' bitmaps. */
			hdcDlg = GetDC(hwndPropPage);
			s_hbmCeil = CreateCompatibleBitmap(hdcDlg, CX_TEXPREVIEW, CY_TEXPREVIEW);
			s_hbmFloor = CreateCompatibleBitmap(hdcDlg, CX_TEXPREVIEW, CY_TEXPREVIEW);
			ReleaseDC(hwndPropPage, hdcDlg);
			SendDlgItemMessage(hwndPropPage, IDC_TEX_CEIL, STM_SETIMAGE, IMAGE_BITMAP, (LPARAM)s_hbmCeil);
			SendDlgItemMessage(hwndPropPage, IDC_TEX_FLOOR, STM_SETIMAGE, IMAGE_BITMAP, (LPARAM)s_hbmFloor);

			/* Subclass the texture preview controls. */
			{
				HWND hwndTexCeil = GetDlgItem(hwndPropPage, IDC_TEX_CEIL);
				HWND hwndTexFloor = GetDlgItem(hwndPropPage, IDC_TEX_FLOOR);

				/* Allocate memory for the preview windows. It's freed when
				 * they're destroyed.
				 */
				TPPDATA *lptppdCeil = ProcHeapAlloc(sizeof(TPPDATA));
				TPPDATA *lptppdFloor = ProcHeapAlloc(sizeof(TPPDATA));

				lptppdCeil->wndprocStatic = (WNDPROC)GetWindowLong(hwndTexCeil, GWL_WNDPROC);
				lptppdFloor->wndprocStatic = (WNDPROC)GetWindowLong(hwndTexFloor, GWL_WNDPROC);

				SetWindowLong(hwndTexCeil, GWL_USERDATA, (LONG)lptppdCeil);
				SetWindowLong(hwndTexFloor, GWL_USERDATA, (LONG)lptppdFloor);

				SetWindowLong(hwndTexCeil, GWL_WNDPROC, (LONG)TexPreviewProc);
				SetWindowLong(hwndTexFloor, GWL_WNDPROC, (LONG)TexPreviewProc);
			}


			/* Sector types from map configuration. */
			lpcfgSecTypes = ConfigGetSubsection(s_lpmapppdata->lpcfgMap, "sectortypes");

			/* Find info common to all selected sectors. */
			dwCheckFlags = CheckSectors(
							s_lpmapppdata->lpmap,
							s_lpmapppdata->lpselection->lpsellistSectors,
							lpcfgSecTypes,
							&sdi);

			/* Fill the list of sector effects. */
			ConfigIterate(lpcfgSecTypes, AddSectorTypeToList, GetDlgItem(hwndPropPage, IDC_LIST_EFFECT));

			/* Set ranges and increments on the spinners. */

			udaccelHeight[0].nInc = 8;
			udaccelHeight[0].nSec = 0;
			udaccelHeight[1].nInc = 64;
			udaccelHeight[1].nSec = 2;

			udaccelOther[0].nInc = 1;
			udaccelOther[0].nSec = 0;

			SendDlgItemMessage(hwndPropPage, IDC_SPIN_TAG, UDM_SETRANGE32, 0, 65535);
			SendDlgItemMessage(hwndPropPage, IDC_SPIN_TAG, UDM_SETACCEL, 1, (LPARAM)udaccelOther);

			SendDlgItemMessage(hwndPropPage, IDC_SPIN_HCEIL, UDM_SETRANGE32, -32768, 32767);
			SendDlgItemMessage(hwndPropPage, IDC_SPIN_HCEIL, UDM_SETACCEL, 2, (LPARAM)udaccelHeight);

			SendDlgItemMessage(hwndPropPage, IDC_SPIN_HFLOOR, UDM_SETRANGE32, -32768, 32767);
			SendDlgItemMessage(hwndPropPage, IDC_SPIN_HFLOOR, UDM_SETACCEL, 2, (LPARAM)udaccelHeight);

			/* Add some brightnesses to the combo. */
			for(i=16; i>=0; i--)
			{
				TCHAR szNum[4];
				wsprintf(szNum, TEXT("%d"), min(16*i, 255));
				SendDlgItemMessage(hwndPropPage, IDC_COMBO_LIGHT, CB_ADDSTRING, FALSE, (LPARAM)szNum);
			}


			/* Set up the brightness slider. */
			SendDlgItemMessage(hwndPropPage, IDC_SLIDER_LIGHT, TBM_SETBUDDY, FALSE, (LPARAM)GetDlgItem(hwndPropPage, IDC_COMBO_LIGHT));
			SendDlgItemMessage(hwndPropPage, IDC_SLIDER_LIGHT, TBM_SETTICFREQ, 16, 0);
			SendDlgItemMessage(hwndPropPage, IDC_SLIDER_LIGHT, TBM_SETRANGE, FALSE, MAKELONG(0, 255));

			/* Set fields of the property page. */
			InitSectorsPropPageFields(hwndPropPage, &sdi, dwCheckFlags);

			/* Fill the adjacent sector info. */
			if(GetAdjacentSectorHeightRange(s_lpmapppdata->lpmap, &nMaxAdjCeil, &nMinAdjCeil, &nMaxAdjFloor, &nMinAdjFloor))
			{
				/* We had some adjacent sectors. */
				TCHAR szCeilRange[17], szFloorRange[17];

				if(nMinAdjCeil != nMaxAdjCeil)
					_sntprintf(szCeilRange, sizeof(szCeilRange) / sizeof(TCHAR), TEXT("[%d, %d]"), nMinAdjCeil, nMaxAdjCeil);
				else
					_sntprintf(szCeilRange, sizeof(szCeilRange) / sizeof(TCHAR), TEXT("%d"), nMinAdjCeil);

				if(nMinAdjFloor != nMaxAdjFloor)
					_sntprintf(szFloorRange, sizeof(szFloorRange) / sizeof(TCHAR), TEXT("[%d, %d]"), nMinAdjFloor, nMaxAdjFloor);
				else
					_sntprintf(szFloorRange, sizeof(szFloorRange) / sizeof(TCHAR), TEXT("%d"), nMinAdjFloor);

				SetDlgItemText(hwndPropPage, IDC_STATIC_CEILRANGE, szCeilRange);
				SetDlgItemText(hwndPropPage, IDC_STATIC_FLRRANGE, szFloorRange);
			}
			else
			{
				/* No adjacent sectors. */
				SetDlgItemText(hwndPropPage, IDC_STATIC_CEILRANGE, TEXT("-"));
				SetDlgItemText(hwndPropPage, IDC_STATIC_FLRRANGE, TEXT("-"));
			}
		}

		s_bInitComplete = TRUE;

		/* Let the system set the focus. */
		return TRUE;

	case WM_COMMAND:

		switch(LOWORD(wParam))
		{
		case IDC_BUTTON_NEXTTAG:
			SetDlgItemInt(hwndPropPage, IDC_EDIT_TAG, NextUnusedTag(s_lpmapppdata->lpmap), FALSE);
			return TRUE;

		case IDC_COMBO_LIGHT:
			switch(HIWORD(wParam))
			{
			case CBN_SELCHANGE:
				{
					/* Update the slider to reflect the combo box. */
					int iIndex, cchBuffer;
					LPTSTR sz;

					iIndex = SendDlgItemMessage(hwndPropPage, IDC_COMBO_LIGHT, CB_GETCURSEL, 0, 0);
					cchBuffer = SendDlgItemMessage(hwndPropPage, IDC_COMBO_LIGHT, CB_GETLBTEXTLEN, iIndex, 0) + 1;
					sz = ProcHeapAlloc(cchBuffer * sizeof(TCHAR));

					SendDlgItemMessage(hwndPropPage, IDC_COMBO_LIGHT, CB_GETLBTEXT, iIndex, (LPARAM)sz);
					SendDlgItemMessage(hwndPropPage, IDC_SLIDER_LIGHT, TBM_SETPOS, TRUE, _tcstol(sz, NULL, 10));

					ProcHeapFree(sz);
				}

				return TRUE;

			case CBN_EDITUPDATE:

				/* Update the slider to reflect the combo box. */
				SendDlgItemMessage(hwndPropPage, IDC_SLIDER_LIGHT, TBM_SETPOS, TRUE, min(255, GetDlgItemInt(hwndPropPage, IDC_COMBO_LIGHT, NULL, FALSE)));

				return TRUE;

			case CBN_KILLFOCUS:

				/* Validate. */
				BoundEditBox((HWND)lParam, 0, 255, TRUE, TRUE);

				return TRUE;
			}

			break;

		case IDC_TEX_CEIL:
		case IDC_TEX_FLOOR:

			switch(HIWORD(wParam))
			{
			case BN_CLICKED:
				{
					TCHAR szTexName[TEXNAME_BUFFER_LENGTH];
					UINT uiEditBoxID = (LOWORD(wParam) == IDC_TEX_FLOOR) ? IDC_EDIT_TFLOOR : IDC_EDIT_TCEIL;

					/* Get the current flat so it's selected initially. */
					GetDlgItemText(hwndPropPage, uiEditBoxID, szTexName, TEXNAME_BUFFER_LENGTH);

					/* Show the texture browser. */
					if(SelectTexture(hwndPropPage, s_lpmapppdata->hwndMap, TF_FLAT, s_lpmapppdata->lphimlCacheFlats, s_lpmapppdata->lptnlFlats, NULL, szTexName))
					{
						/* User didn't cancel. Update the edit-box. */
						SetDlgItemText(hwndPropPage, uiEditBoxID, szTexName);
					}
				}

				return TRUE;
			}

			break;

		case IDC_LIST_EFFECT:

			switch(HIWORD(wParam))
			{
			case LBN_SELCHANGE:
				{
					int iIndex = SendDlgItemMessage(hwndPropPage, IDC_LIST_EFFECT, LB_GETCURSEL, 0, 0);

					if(iIndex >= 0)
					{
						int iEffect = SendDlgItemMessage(hwndPropPage, IDC_LIST_EFFECT, LB_GETITEMDATA, iIndex, 0);
						SetDlgItemInt(hwndPropPage, IDC_EDIT_EFFECT, iEffect, FALSE);
					}
				}

				return TRUE;
			}

			break;

		case IDC_EDIT_EFFECT:
			switch(HIWORD(wParam))
			{
			case EN_CHANGE:
				{
					BOOL bTranslated;
					int iEffect = GetDlgItemInt(hwndPropPage, IDC_EDIT_EFFECT, &bTranslated, FALSE);

					/* Valid number? */
					if(bTranslated)
					{
						/* Select corresponding list-box item. */
						ListBoxSearchByItemData(GetDlgItem(hwndPropPage, IDC_LIST_EFFECT), iEffect, TRUE);
					}
				}

				return TRUE;

			case EN_KILLFOCUS:
				/* Validation. */
				BoundEditBox(GetDlgItem(hwndPropPage, IDC_EDIT_EFFECT), -32768, 32767, TRUE, FALSE);

				return TRUE;
			}

			break;

		case IDC_EDIT_TFLOOR:
		case IDC_EDIT_TCEIL:
			switch(HIWORD(wParam))
			{
			case EN_CHANGE:
				{
					int iTextLength;
					LPTSTR szEditText;
					LPTSTR szPrev;

					iTextLength = GetWindowTextLength((HWND)lParam);
					szEditText = ProcHeapAlloc((iTextLength + 1) * sizeof(TCHAR));
					GetWindowText((HWND)lParam, szEditText, iTextLength + 1);

					/* Get pointer to previous string (i.e. before edit). */
					szPrev = (LOWORD(wParam) == IDC_EDIT_TFLOOR) ? s_szPrevTFloor : s_szPrevTCeil;

					/* Texture name changed. Do autocomplete. */
					if(s_bInitComplete && ConfigGetInteger(g_lpcfgMain, "autocompletetex"))
					{
						DWORD dwStartSel, dwEndSel;
						BOOL bRootDifferent;

						/* Don't autocomplete if box is empty or full, if the caret
						 * isn't at the end, if any text is selected or if the text
						 * is a truncated form of what it was before.
						 */
						bRootDifferent = lstrlen(szEditText) > lstrlen(szPrev) || _tcsncmp(szPrev, szEditText, min(lstrlen(szEditText), lstrlen(szPrev)));
						SendMessage((HWND)lParam, EM_GETSEL, (WPARAM)&dwStartSel, (LPARAM)&dwEndSel);
						if(iTextLength > 0
							&& iTextLength < TEXNAME_BUFFER_LENGTH
							&& dwEndSel == (unsigned int)iTextLength
							&& dwStartSel == dwEndSel
							&& bRootDifferent)
						{
							LPCTSTR szCompletedTexName;

							szCompletedTexName = FindFirstTexNameMatch(szEditText, s_lpmapppdata->lptnlFlats);

							/* Found a match? */
							if(szCompletedTexName)
							{
								/* Set text and select the portion that changed. */
								SetWindowText((HWND)lParam, szCompletedTexName);
								SendMessage((HWND)lParam, EM_SETSEL, iTextLength, lstrlen(szCompletedTexName) + 1);
							}
						}
					}

					/* Update the preview image. */
					SetTexturePreviewImage
						(s_lpmapppdata->hwndMap,
						GetDlgItem(hwndPropPage, (LOWORD(wParam) == IDC_EDIT_TCEIL) ? IDC_TEX_CEIL : IDC_TEX_FLOOR),
						szEditText,
						TF_FLAT,
						(LOWORD(wParam) == IDC_EDIT_TCEIL) ? s_hbmCeil : s_hbmFloor,
						TRUE);


					/* Update remembered texture string. */
					lstrcpyn(szPrev, szEditText, TEXNAME_BUFFER_LENGTH);
					ProcHeapFree(szEditText);
				}

				return TRUE;
			}

			break;

		case IDC_EDIT_HCEIL:
		case IDC_EDIT_HFLOOR:
			switch(HIWORD(wParam))
			{
			case EN_KILLFOCUS:
				/* Validation. */
				BoundEditBox(GetDlgItem(hwndPropPage, LOWORD(wParam)), -32768, 32767, TRUE, TRUE);

				return TRUE;
			}

		case IDC_EDIT_TAG:
			switch(HIWORD(wParam))
			{
			case EN_KILLFOCUS:
				/* Validation. */
				BoundEditBox(GetDlgItem(hwndPropPage, LOWORD(wParam)), -32768, 32767, TRUE, FALSE);

				return TRUE;
			}
		}

		/* Didn't process the message. */
		break;


	case WM_NOTIFY:

		switch(((LPNMHDR)lParam)->code)
		{
		case PSN_APPLY:
			{
				/* User OK-ed. Set the properties! */

				SECTORDISPLAYINFO sdi;
				SECTOR_RELATIVE secrelative;
				DWORD dwPropertyFlags = 0, dwRelativeFlags = 0;
				TCHAR szBuffer[TEXNAME_BUFFER_LENGTH];

				/* We go through each field, checking whether it's non-empty.
				 * If so, copy its value and set the corresponding flag.
				 */

				/* Floor texture. */
				GetDlgItemText(hwndPropPage, IDC_EDIT_TFLOOR, szBuffer, sizeof(szBuffer)/sizeof(TCHAR));
				if(*szBuffer)
				{
					lstrcpy(sdi.szFloor, szBuffer);
					dwPropertyFlags |= SDIF_FLOORTEX;
				}

				/* Ceiling texture. */
				GetDlgItemText(hwndPropPage, IDC_EDIT_TCEIL, szBuffer, sizeof(szBuffer)/sizeof(TCHAR));
				if(*szBuffer)
				{
					lstrcpy(sdi.szCeiling, szBuffer);
					dwPropertyFlags |= SDIF_CEILINGTEX;
				}

				/* Floor height. */
				GetDlgItemText(hwndPropPage, IDC_EDIT_HFLOOR, szBuffer, sizeof(szBuffer)/sizeof(TCHAR));
				if(*szBuffer)
				{
					if(STRING_RELATIVE(szBuffer))
					{
						secrelative.iFloor = _tcstol(&szBuffer[1], NULL, 10);
						dwRelativeFlags |= SDIF_FLOOR;
					}
					else
					{
						sdi.nFloor = GetDlgItemInt(hwndPropPage, IDC_EDIT_HFLOOR, NULL, TRUE);
						dwPropertyFlags |= SDIF_FLOOR;
					}
				}

				/* Ceiling height. */
				GetDlgItemText(hwndPropPage, IDC_EDIT_HCEIL, szBuffer, sizeof(szBuffer)/sizeof(TCHAR));
				if(*szBuffer)
				{
					if(STRING_RELATIVE(szBuffer))
					{
						secrelative.iCeil = _tcstol(&szBuffer[1], NULL, 10);
						dwRelativeFlags |= SDIF_CEILING;
					}
					else
					{
						sdi.nCeiling = GetDlgItemInt(hwndPropPage, IDC_EDIT_HCEIL, NULL, TRUE);
						dwPropertyFlags |= SDIF_CEILING;
					}
				}

				/* Brightness. */
				GetDlgItemText(hwndPropPage, IDC_COMBO_LIGHT, szBuffer, sizeof(szBuffer)/sizeof(TCHAR));
				if(*szBuffer)
				{
					if(STRING_RELATIVE(szBuffer))
					{
						secrelative.iLight = _tcstol(&szBuffer[1], NULL, 10);
						dwRelativeFlags |= SDIF_BRIGHTNESS;
					}
					else
					{
						sdi.ucBrightness = GetDlgItemInt(hwndPropPage, IDC_COMBO_LIGHT, NULL, FALSE);
						dwPropertyFlags |= SDIF_BRIGHTNESS;
					}
				}

				/* Effect. */
				GetDlgItemText(hwndPropPage, IDC_EDIT_EFFECT, szBuffer, sizeof(szBuffer)/sizeof(TCHAR));
				if(*szBuffer)
				{
					sdi.unEffect = GetDlgItemInt(hwndPropPage, IDC_EDIT_EFFECT, NULL, FALSE);
					dwPropertyFlags |= SDIF_EFFECT;
				}

				/* Tag. */
				GetDlgItemText(hwndPropPage, IDC_EDIT_TAG, szBuffer, sizeof(szBuffer)/sizeof(TCHAR));
				if(*szBuffer)
				{
					sdi.unTag = GetDlgItemInt(hwndPropPage, IDC_EDIT_TAG, NULL, FALSE);
					dwPropertyFlags |= SDIF_TAG;
				}

				/* TODO: Comments. */

				/* We're making changes. */
				s_lpmapppdata->bOKed = TRUE;

				/* Apply the relative properties. */
				if(dwRelativeFlags & SDIF_FLOOR)
					ApplyRelativeFloorHeightSelection(s_lpmapppdata->lpmap, s_lpmapppdata->lpselection->lpsellistSectors, secrelative.iFloor);

				if(dwRelativeFlags & SDIF_CEILING)
					ApplyRelativeCeilingHeightSelection(s_lpmapppdata->lpmap, s_lpmapppdata->lpselection->lpsellistSectors, secrelative.iCeil);

				if(dwRelativeFlags & SDIF_BRIGHTNESS)
					ApplyRelativeBrightnessSelection(s_lpmapppdata->lpmap, s_lpmapppdata->lpselection->lpsellistSectors, (short)secrelative.iLight);

				/* We've got all the properties now. Apply them. */
				ApplySectorPropertiesToSelection(s_lpmapppdata->lpmap, s_lpmapppdata->lpselection->lpsellistSectors, &sdi, dwPropertyFlags);

				/* Finished! */
			}

			SetWindowLong(hwndPropPage, DWL_MSGRESULT, PSNRET_NOERROR);
			return TRUE;
		}


		/* Didn't process the message. */
		break;

	case WM_HSCROLL:
		/* We've only got the one slider. */

		SetDlgItemInt(hwndPropPage, IDC_COMBO_LIGHT, SendDlgItemMessage(hwndPropPage, IDC_SLIDER_LIGHT, TBM_GETPOS, 0, 0), FALSE);
		return TRUE;

	case WM_DESTROY:

		/* Destroy the preview bitmaps. */
		DeleteObject(s_hbmCeil);
		DeleteObject(s_hbmFloor);

		return TRUE;
	}

	/* Didn't process message. */
	return FALSE;
}


/* LinesPropPageProc
 *   Dialogue proc for lines property page.
 *
 * Parameters:
 *   HWND	hwndPropPage	Property page window handle.
 *   UINT	uiMsg			Message identifier.
 *   WPARAM wParam			Message-specific.
 *   LPARAM lParam			Message-specific.
 *
 * Return value: BOOL
 *   TRUE if the message was processed; FALSE otherwise.
 */
static BOOL CALLBACK LinesPropPageProc(HWND hwndPropPage, UINT uiMsg, WPARAM wParam, LPARAM lParam)
{
	static MAPPPDATA *s_lpmapppdata;
	static CONFIG *s_lpcfgTypeToNode;
	static HBITMAP s_hbmTexPreviews[6];
	static BOOL s_bHasFrontSec, s_bHasBackSec;
	static BYTE s_byTexRequirementFlags;

	switch(uiMsg)
	{
	case WM_INITDIALOG:
		{
			CONFIG *lpcfgLinedefFlags;
			CONFIG *lpcfgLDEffects;
			TYPETREEDATA ttd;
			LVCOLUMN lvcol;
			UDACCEL udaccelOffsets[2], udaccelTag[1];
			HWND hwndListFlags = GetDlgItem(hwndPropPage, IDC_LIST_FLAGS);
			DWORD dwCheckFlags;
			LINEDEFDISPLAYINFO lddi;
			int i;
			HDC hdcDlg;
			AFTLDATA aftldata;

			const int iOffsetSpinIDs[] = {IDC_SPIN_FRONT_OFFX, IDC_SPIN_FRONT_OFFY, IDC_SPIN_BACK_OFFX, IDC_SPIN_BACK_OFFY};
			const int iTexPreviewIDs[] = {	IDC_TEX_FRONT_UPPER, IDC_TEX_FRONT_MIDDLE, IDC_TEX_FRONT_LOWER,
											IDC_TEX_BACK_UPPER, IDC_TEX_BACK_MIDDLE, IDC_TEX_BACK_LOWER};

			/* Get the map data. */
			s_lpmapppdata = (MAPPPDATA*)((PROPSHEETPAGE*)lParam)->lParam;

			/* Initialise the flag list. */
			lvcol.mask = LVCF_SUBITEM;
			lvcol.iSubItem = 0;
			ListView_InsertColumn(hwndListFlags, 0, &lvcol);
			Init3StateListView(hwndListFlags);

			/* Get the possible linedef flag values. */
			lpcfgLinedefFlags = ConfigGetSubsection(s_lpmapppdata->lpcfgMap, "linedefflags");

			/* Add them to the list, with their initial values. */
			aftldata.hwndListView = hwndListFlags;
			CheckLineFlags(s_lpmapppdata->lpmap, s_lpmapppdata->lpselection->lpsellistLinedefs, &aftldata.wFlags, &aftldata.wFlagMask);
			ConfigIterate(lpcfgLinedefFlags, AddFlagToList, &aftldata);

			/* Make the column big enough. */
			ListView_SetColumnWidth(hwndListFlags, 0, LVSCW_AUTOSIZE);

			/* Create an empty effect-to-node lookup table. */
			s_lpcfgTypeToNode = ConfigCreate();

			/* Populate the effect tree and corresponding lookup table. */
			lpcfgLDEffects = ConfigGetSubsection(s_lpmapppdata->lpcfgMap, "linedeftypes");
			ttd.hwndTree = GetDlgItem(hwndPropPage, IDC_TREE_EFFECT);
			ttd.lpcfgTypeToNode = s_lpcfgTypeToNode;
			ConfigIterate(lpcfgLDEffects, AddCategoryToTree, &ttd);

			/* Subclass the texture preview controls and create their preview
			 * bitmaps.
			 */
			hdcDlg = GetDC(hwndPropPage);
			for(i = 0; i < 6; i++)
			{
				HWND hwndTex = GetDlgItem(hwndPropPage, iTexPreviewIDs[i]);

				/* Allocate memory for the preview window. It's freed when the
				 * window's destroyed.
				 */
				TPPDATA *lptppd = ProcHeapAlloc(sizeof(TPPDATA));

				lptppd->wndprocStatic = (WNDPROC)GetWindowLong(hwndTex, GWL_WNDPROC);

				SetWindowLong(hwndTex, GWL_USERDATA, (LONG)lptppd);
				SetWindowLong(hwndTex, GWL_WNDPROC, (LONG)TexPreviewProc);

				/* Create preview bitmap. */
				s_hbmTexPreviews[i] = CreateCompatibleBitmap(hdcDlg, CX_TEXPREVIEW, CY_TEXPREVIEW);
				SendDlgItemMessage(hwndPropPage, iTexPreviewIDs[i], STM_SETIMAGE, IMAGE_BITMAP, (LPARAM)s_hbmTexPreviews[i]);
			}

			ReleaseDC(hwndPropPage, hdcDlg);

			/* Set up the spinner controls. */

			udaccelTag[0].nInc = 1;
			udaccelTag[0].nSec = 0;

			udaccelOffsets[0].nInc = 1;
			udaccelOffsets[0].nSec = 0;
			udaccelOffsets[1].nInc = 8;
			udaccelOffsets[1].nSec = 2;

			SendDlgItemMessage(hwndPropPage, IDC_SPIN_TAG, UDM_SETRANGE32, 0, 65535);
			SendDlgItemMessage(hwndPropPage, IDC_SPIN_TAG, UDM_SETACCEL, 1, (LPARAM)udaccelTag);

			/* This sets all the offset spinners' ranges and speeds. */
			for(i = 0; i < 4; i++)
			{
				SendDlgItemMessage(hwndPropPage, iOffsetSpinIDs[i], UDM_SETRANGE32, -32768, 32767);
				SendDlgItemMessage(hwndPropPage, iOffsetSpinIDs[i], UDM_SETACCEL, 2, (LPARAM)udaccelOffsets);
			}


			/* Initialise the controls to the values from the selection. */

			dwCheckFlags = CheckLines(
							s_lpmapppdata->lpmap,
							s_lpmapppdata->lpselection->lpsellistLinedefs,
							lpcfgLDEffects,
							&lddi);

			/* These will be recalculated every time the tab is activated, since
			 * the user might have made changes on the Sectors tab.
			 */
			s_byTexRequirementFlags = CheckTextureFlags(s_lpmapppdata->lpmap, s_lpmapppdata->lpselection->lpsellistLinedefs);

			s_bHasFrontSec = (dwCheckFlags & LDDIF_HASFRONT) && lddi.bHasFront;
			s_bHasBackSec = (dwCheckFlags & LDDIF_HASBACK) && lddi.bHasBack;

			InitLinesPropPageFields(hwndPropPage, &lddi, dwCheckFlags);
		}

		/* Let the system set the focus. */
		return TRUE;

	case WM_COMMAND:

		switch(LOWORD(wParam))
		{
		case IDC_BUTTON_LINE_NEXTTAG:
			SetDlgItemInt(hwndPropPage, IDC_EDIT_TAG, NextUnusedTag(s_lpmapppdata->lpmap), FALSE);
			return TRUE;

		case IDC_EDIT_EFFECT:
			switch(HIWORD(wParam))
			{
			case EN_CHANGE:
				{
					BOOL bTranslated;
					int iEffect = GetDlgItemInt(hwndPropPage, IDC_EDIT_EFFECT, &bTranslated, FALSE);

					/* Valid number? */
					if(bTranslated && iEffect >= 0)
					{
						/* Select corresponding tree item. */
						char szValue[12];
						wsprintfA(szValue, "%d", iEffect);

						if(ConfigNodeExists(s_lpcfgTypeToNode, szValue))
							TreeView_SelectItem(GetDlgItem(hwndPropPage, IDC_TREE_EFFECT), (HTREEITEM)ConfigGetInteger(s_lpcfgTypeToNode, szValue));
						else
							TreeView_SelectItem(GetDlgItem(hwndPropPage, IDC_TREE_EFFECT), NULL);
					}
				}

				return TRUE;

			case EN_KILLFOCUS:
				/* Validation. */
				BoundEditBox(GetDlgItem(hwndPropPage, IDC_EDIT_EFFECT), 0, 65535, TRUE, FALSE);

				return TRUE;
			}

			break;

		case IDC_EDIT_TAG:
		case IDC_EDIT_FRONT_SECTOR:
		case IDC_EDIT_BACK_SECTOR:
			if(HIWORD(wParam) == EN_KILLFOCUS)
			{
				/* Validation. */
				BoundEditBox((HWND)lParam, 0, 65535, TRUE, FALSE);
				return TRUE;
			}

			break;

		case IDC_EDIT_FRONT_OFFX:
		case IDC_EDIT_FRONT_OFFY:
		case IDC_EDIT_BACK_OFFX:
		case IDC_EDIT_BACK_OFFY:
			if(HIWORD(wParam) == EN_KILLFOCUS)
			{
				/* Validation. */
				BoundEditBox((HWND)lParam, -32768, 32767, TRUE, TRUE);
				return TRUE;
			}

			break;

		case IDC_TEX_FRONT_UPPER:
		case IDC_TEX_FRONT_MIDDLE:
		case IDC_TEX_FRONT_LOWER:
		case IDC_TEX_BACK_UPPER:
		case IDC_TEX_BACK_MIDDLE:
		case IDC_TEX_BACK_LOWER:

			switch(HIWORD(wParam))
			{
			case BN_CLICKED:
				{
					TCHAR szTexName[TEXNAME_BUFFER_LENGTH];
					UINT uiEditBoxID = 0;

					switch(LOWORD(wParam))
					{
						case IDC_TEX_FRONT_UPPER:	uiEditBoxID = IDC_EDIT_FRONT_UPPER;		break;
						case IDC_TEX_FRONT_MIDDLE:	uiEditBoxID = IDC_EDIT_FRONT_MIDDLE;	break;
						case IDC_TEX_FRONT_LOWER:	uiEditBoxID = IDC_EDIT_FRONT_LOWER;		break;
						case IDC_TEX_BACK_UPPER:	uiEditBoxID = IDC_EDIT_BACK_UPPER;		break;
						case IDC_TEX_BACK_MIDDLE:	uiEditBoxID = IDC_EDIT_BACK_MIDDLE;		break;
						case IDC_TEX_BACK_LOWER:	uiEditBoxID = IDC_EDIT_BACK_LOWER;		break;
					}

					/* Get the current flat so it's selected initially. */
					GetDlgItemText(hwndPropPage, uiEditBoxID, szTexName, TEXNAME_BUFFER_LENGTH);

					/* Show the texture browser. */
					if(SelectTexture(hwndPropPage, s_lpmapppdata->hwndMap, TF_TEXTURE, s_lpmapppdata->lphimlCacheTextures, s_lpmapppdata->lptnlTextures, NULL, szTexName))
					{
						/* User didn't cancel. Update the edit-box. */
						SetDlgItemText(hwndPropPage, uiEditBoxID, szTexName);
					}
				}

				return TRUE;
			}

			break;

		case IDC_EDIT_FRONT_UPPER:
		case IDC_EDIT_FRONT_MIDDLE:
		case IDC_EDIT_FRONT_LOWER:
		case IDC_EDIT_BACK_UPPER:
		case IDC_EDIT_BACK_MIDDLE:
		case IDC_EDIT_BACK_LOWER:

			switch(HIWORD(wParam))
			{
			case EN_CHANGE:
				{
					UINT uiPreviewID = 0;
					HWND hwndPreview = NULL;
					HBITMAP hbmPreview = NULL;
					int iTextLength;
					LPTSTR szEditText;
					BOOL bTexRequired = FALSE;

					/* Get preview control handle and bitmap corresponding to
					 * the edit box.
					 */
					switch(LOWORD(wParam))
					{
					case IDC_EDIT_FRONT_UPPER:
						uiPreviewID = IDC_TEX_FRONT_UPPER;
						hbmPreview = s_hbmTexPreviews[0];
						bTexRequired = s_byTexRequirementFlags & LDTF_FRONTUPPER;
						break;
					case IDC_EDIT_FRONT_MIDDLE:
						uiPreviewID = IDC_TEX_FRONT_MIDDLE;
						hbmPreview = s_hbmTexPreviews[1];
						bTexRequired = s_byTexRequirementFlags & LDTF_FRONTMIDDLE;
						break;
					case IDC_EDIT_FRONT_LOWER:
						uiPreviewID = IDC_TEX_FRONT_LOWER;
						hbmPreview = s_hbmTexPreviews[2];
						bTexRequired = s_byTexRequirementFlags & LDTF_FRONTLOWER;
						break;
					case IDC_EDIT_BACK_UPPER:
						uiPreviewID = IDC_TEX_BACK_UPPER;
						hbmPreview = s_hbmTexPreviews[3];
						bTexRequired = s_byTexRequirementFlags & LDTF_BACKUPPER;
						break;
					case IDC_EDIT_BACK_MIDDLE:
						uiPreviewID = IDC_TEX_BACK_MIDDLE;
						hbmPreview = s_hbmTexPreviews[4];
						bTexRequired = s_byTexRequirementFlags & LDTF_BACKMIDDLE;
						break;
					case IDC_EDIT_BACK_LOWER:
						uiPreviewID = IDC_TEX_BACK_LOWER;
						hbmPreview = s_hbmTexPreviews[5];
						bTexRequired = s_byTexRequirementFlags & LDTF_BACKLOWER;
						break;
					}

					hwndPreview = GetDlgItem(hwndPropPage, uiPreviewID);


					/* Get the texture name. */
					iTextLength = GetWindowTextLength((HWND)lParam);
					szEditText = ProcHeapAlloc((iTextLength + 1) * sizeof(TCHAR));
					GetWindowText((HWND)lParam, szEditText, iTextLength + 1);

					/* Update the texture preview image. */
					SetTexturePreviewImage
						(s_lpmapppdata->hwndMap,
						 hwndPreview,
						 szEditText,
						 TF_TEXTURE,
						 hbmPreview,
						 bTexRequired);

					/* Free the buffer used to store the texture name. */
					ProcHeapFree(szEditText);
				}

				return TRUE;
			}

			break;
		}

		/* Didn't process message. */
		break;

	case WM_NOTIFY:
		{
			LPNMHDR lpnmhdr = (LPNMHDR)lParam;

			/* Special case: notifications from the property sheet don't set an
			 * ID.
			 */
			switch(lpnmhdr->code)
			{
			case PSN_APPLY:
				{
					/* User OK-ed. Set the properties! */

					LINEDEFDISPLAYINFO lddi;
					LINEDEF_RELATIVE ldrelative;
					DWORD dwPropertyFlags = 0, dwRelativeFlags = 0;
					TCHAR szBuffer[TEXNAME_BUFFER_LENGTH];
					WORD wFlags, wFlagMask;

					/* We go through each field, checking whether it's non-empty.
					 * If so, copy its value and set the corresponding flag.
					 */

					/* Effect. */
					GetDlgItemText(hwndPropPage, IDC_EDIT_EFFECT, szBuffer, sizeof(szBuffer)/sizeof(TCHAR));
					if(*szBuffer)
					{
						lddi.unEffect = GetDlgItemInt(hwndPropPage, IDC_EDIT_EFFECT, NULL, FALSE);
						dwPropertyFlags |= LDDIF_EFFECT;
					}

					/* Tag. */
					GetDlgItemText(hwndPropPage, IDC_EDIT_TAG, szBuffer, sizeof(szBuffer)/sizeof(TCHAR));
					if(*szBuffer)
					{
						lddi.unTag = GetDlgItemInt(hwndPropPage, IDC_EDIT_TAG, NULL, FALSE);
						dwPropertyFlags |= LDDIF_TAG;
					}

					/* Flags. */
					GetFlagsFromListView(GetDlgItem(hwndPropPage, IDC_LIST_FLAGS), &wFlags, &wFlagMask);

					/* Front sector. */
					if(s_bHasFrontSec)
					{
						/* X Offset. */
						GetDlgItemText(hwndPropPage, IDC_EDIT_FRONT_OFFX, szBuffer, sizeof(szBuffer)/sizeof(TCHAR));
						if(*szBuffer)
						{
							if(STRING_RELATIVE(szBuffer))
							{
								ldrelative.xFront = _tcstol(&szBuffer[1], NULL, 10);
								dwRelativeFlags |= LDDIF_FRONTX;
							}
							else
							{
								lddi.nFrontX = GetDlgItemInt(hwndPropPage, IDC_EDIT_FRONT_OFFX, NULL, TRUE);
								dwPropertyFlags |= LDDIF_FRONTX;
							}
						}

						/* Y Offset. */
						GetDlgItemText(hwndPropPage, IDC_EDIT_FRONT_OFFY, szBuffer, sizeof(szBuffer)/sizeof(TCHAR));
						if(*szBuffer)
						{
							if(STRING_RELATIVE(szBuffer))
							{
								ldrelative.yFront = _tcstol(&szBuffer[1], NULL, 10);
								dwRelativeFlags |= LDDIF_FRONTY;
							}
							else
							{
								lddi.nFrontY = GetDlgItemInt(hwndPropPage, IDC_EDIT_FRONT_OFFY, NULL, TRUE);
								dwPropertyFlags |= LDDIF_FRONTY;
							}
						}

						/* Sector. */
						GetDlgItemText(hwndPropPage, IDC_EDIT_FRONT_SECTOR, szBuffer, sizeof(szBuffer)/sizeof(TCHAR));
						if(*szBuffer)
						{
							lddi.unFrontSector = GetDlgItemInt(hwndPropPage, IDC_EDIT_FRONT_SECTOR, NULL, FALSE);
							dwPropertyFlags |= LDDIF_FRONTSEC;
						}

						/* Upper texture. */
						GetDlgItemText(hwndPropPage, IDC_EDIT_FRONT_UPPER, szBuffer, sizeof(szBuffer)/sizeof(TCHAR));
						if(*szBuffer)
						{
							lstrcpy(lddi.szFrontUpper, szBuffer);
							dwPropertyFlags |= LDDIF_FRONTUPPER;
						}

						/* Middle texture. */
						GetDlgItemText(hwndPropPage, IDC_EDIT_FRONT_MIDDLE, szBuffer, sizeof(szBuffer)/sizeof(TCHAR));
						if(*szBuffer)
						{
							lstrcpy(lddi.szFrontMiddle, szBuffer);
							dwPropertyFlags |= LDDIF_FRONTMIDDLE;
						}

						/* Lower texture. */
						GetDlgItemText(hwndPropPage, IDC_EDIT_FRONT_LOWER, szBuffer, sizeof(szBuffer)/sizeof(TCHAR));
						if(*szBuffer)
						{
							lstrcpy(lddi.szFrontLower, szBuffer);
							dwPropertyFlags |= LDDIF_FRONTLOWER;
						}
					}

					/* Back sector. */
					if(s_bHasBackSec)
					{
						/* X Offset. */
						GetDlgItemText(hwndPropPage, IDC_EDIT_BACK_OFFX, szBuffer, sizeof(szBuffer)/sizeof(TCHAR));
						if(*szBuffer)
						{
							if(STRING_RELATIVE(szBuffer))
							{
								ldrelative.xBack = _tcstol(&szBuffer[1], NULL, 10);
								dwRelativeFlags |= LDDIF_BACKX;
							}
							else
							{
								lddi.nBackX = GetDlgItemInt(hwndPropPage, IDC_EDIT_BACK_OFFX, NULL, TRUE);
								dwPropertyFlags |= LDDIF_BACKX;
							}
						}

						/* Y Offset. */
						GetDlgItemText(hwndPropPage, IDC_EDIT_BACK_OFFY, szBuffer, sizeof(szBuffer)/sizeof(TCHAR));
						if(*szBuffer)
						{
							if(STRING_RELATIVE(szBuffer))
							{
								ldrelative.yBack = _tcstol(&szBuffer[1], NULL, 10);
								dwRelativeFlags |= LDDIF_BACKY;
							}
							else
							{
								lddi.nBackY = GetDlgItemInt(hwndPropPage, IDC_EDIT_BACK_OFFY, NULL, TRUE);
								dwPropertyFlags |= LDDIF_BACKY;
							}
						}

						/* Sector. */
						GetDlgItemText(hwndPropPage, IDC_EDIT_BACK_SECTOR, szBuffer, sizeof(szBuffer)/sizeof(TCHAR));
						if(*szBuffer)
						{
							lddi.unBackSector = GetDlgItemInt(hwndPropPage, IDC_EDIT_BACK_SECTOR, NULL, FALSE);
							dwPropertyFlags |= LDDIF_BACKSEC;
						}

						/* Upper texture. */
						GetDlgItemText(hwndPropPage, IDC_EDIT_BACK_UPPER, szBuffer, sizeof(szBuffer)/sizeof(TCHAR));
						if(*szBuffer)
						{
							lstrcpy(lddi.szBackUpper, szBuffer);
							dwPropertyFlags |= LDDIF_BACKUPPER;
						}

						/* Middle texture. */
						GetDlgItemText(hwndPropPage, IDC_EDIT_BACK_MIDDLE, szBuffer, sizeof(szBuffer)/sizeof(TCHAR));
						if(*szBuffer)
						{
							lstrcpy(lddi.szBackMiddle, szBuffer);
							dwPropertyFlags |= LDDIF_BACKMIDDLE;
						}

						/* Lower texture. */
						GetDlgItemText(hwndPropPage, IDC_EDIT_BACK_LOWER, szBuffer, sizeof(szBuffer)/sizeof(TCHAR));
						if(*szBuffer)
						{
							lstrcpy(lddi.szBackLower, szBuffer);
							dwPropertyFlags |= LDDIF_BACKLOWER;
						}
					}

					/* We're making changes. */
					s_lpmapppdata->bOKed = TRUE;

					/* Apply the relative properties. */
					if(dwRelativeFlags & LDDIF_FRONTX)
						ApplyRelativeFrontSidedefXSelection(s_lpmapppdata->lpmap, s_lpmapppdata->lpselection->lpsellistLinedefs, ldrelative.xFront);

					if(dwRelativeFlags & LDDIF_FRONTY)
						ApplyRelativeFrontSidedefYSelection(s_lpmapppdata->lpmap, s_lpmapppdata->lpselection->lpsellistLinedefs, ldrelative.yFront);

					if(dwRelativeFlags & LDDIF_BACKX)
						ApplyRelativeBackSidedefXSelection(s_lpmapppdata->lpmap, s_lpmapppdata->lpselection->lpsellistLinedefs, ldrelative.xBack);

					if(dwRelativeFlags & LDDIF_BACKY)
						ApplyRelativeBackSidedefYSelection(s_lpmapppdata->lpmap, s_lpmapppdata->lpselection->lpsellistLinedefs, ldrelative.yBack);

					/* We've got all the properties now. Apply them. */
					ApplyLinePropertiesToSelection(s_lpmapppdata->lpmap, s_lpmapppdata->lpselection->lpsellistLinedefs, &lddi, dwPropertyFlags);
					SetLineFlags(s_lpmapppdata->lpmap, s_lpmapppdata->lpselection->lpsellistLinedefs, wFlags, wFlagMask);
				}

				return TRUE;
			}

			/* Still WM_NOTIFY... */

			switch(lpnmhdr->idFrom)
			{
			case IDC_TREE_EFFECT:
				{
					LPNMTREEVIEW lpnmtreeview = (LPNMTREEVIEW)lParam;

					switch(lpnmhdr->code)
					{
					case TVN_SELCHANGED:
						/* Does the selected node correspond to an effect, and
						 * was it the direct result of user input (and not
						 * because the edit control changed)? If so, set the
						 * edit control.
						 */
						if((lpnmtreeview->action == TVC_BYKEYBOARD || lpnmtreeview->action == TVC_BYMOUSE) && lpnmtreeview->itemNew.lParam >= 0)
							SetDlgItemInt(hwndPropPage, IDC_EDIT_EFFECT, lpnmtreeview->itemNew.lParam, FALSE);

						return TRUE;
					}
				}

				break;

			case IDC_LIST_FLAGS:
				{
					switch(lpnmhdr->code)
					{
					case NM_DBLCLK:
					case NM_CLICK:
						/* Do checkbox processing. */
						ListView3StateClick((LPNMLISTVIEW)lParam);
						return TRUE;

					case LVN_KEYDOWN:
						/* Do checkbox processing. */
						ListView3StateKeyDown((LPNMLVKEYDOWN)lParam);
						return TRUE;
					}
				}

				break;
			}
		}

		/* Didn't process the message. */
		break;

	case WM_DESTROY:
		{
			int i;

			/* Clean up. */

			/* Free effect no. -> node lookup. */
			ConfigDestroy(s_lpcfgTypeToNode);

			/* Destroy preview bitmaps. */
			for(i = 0; i < 6; i++) DeleteObject(s_hbmTexPreviews[i]);
		}

		return TRUE;
	}

	/* Didn't process message. */
	return FALSE;
}


/* ThingsPropPageProc
 *   Dialogue proc for things property page.
 *
 * Parameters:
 *   HWND	hwndPropPage	Property page window handle.
 *   UINT	uiMsg			Message identifier.
 *   WPARAM wParam			Message-specific.
 *   LPARAM lParam			Message-specific.
 *
 * Return value: BOOL
 *   TRUE if the message was processed; FALSE otherwise.
 */
static BOOL CALLBACK ThingsPropPageProc(HWND hwndPropPage, UINT uiMsg, WPARAM wParam, LPARAM lParam)
{
	static MAPPPDATA *s_lpmapppdata;
	static CONFIG *s_lpcfgTypeToNode;
	static HBITMAP s_hbmSprite;
	static BOOL s_bInitComplete;

	switch(uiMsg)
	{
	case WM_INITDIALOG:
		{
			LVCOLUMNA lvcol;
			HWND hwndListFlags = GetDlgItem(hwndPropPage, IDC_LIST_FLAGS);
			CONFIG *lpcfgThingFlags;
			AFTLDATA aftldata;
			UDACCEL udaccel[2];
			TYPETREEDATA ttd;
			CONFIG *lpcfgThingEffects;
			HDC hdcDlg;
			DWORD dwCheckFlags;
			THINGDISPLAYINFO tdi;

			/* Stop interactions between controls until we're ready. */
			s_bInitComplete = FALSE;

			/* Get the map data. */
			s_lpmapppdata = (MAPPPDATA*)((PROPSHEETPAGE*)lParam)->lParam;

			/* Create the sprite bitmap. */
			hdcDlg = GetDC(hwndPropPage);
			s_hbmSprite = CreateCompatibleBitmap(hdcDlg, CX_TEXPREVIEW, CY_TEXPREVIEW);
			SendDlgItemMessage(hwndPropPage, IDC_TEX_SPRITE, STM_SETIMAGE, IMAGE_BITMAP, (LPARAM)s_hbmSprite);
			ReleaseDC(hwndPropPage, hdcDlg);

			/* Initialise the flag list. */
			lvcol.mask = LVCF_SUBITEM;
			lvcol.iSubItem = 0;
			ListView_InsertColumn(hwndListFlags, 0, &lvcol);
			Init3StateListView(hwndListFlags);

			/* Get the possible thing flag values. */
			lpcfgThingFlags = ConfigGetSubsection(s_lpmapppdata->lpcfgMap, "thingflags");

			/* Add them to the list, with their initial values. */
			aftldata.hwndListView = hwndListFlags;
			CheckThingFlags(s_lpmapppdata->lpmap, s_lpmapppdata->lpselection->lpsellistThings, &aftldata.wFlags, &aftldata.wFlagMask);
			ConfigIterate(lpcfgThingFlags, AddFlagToList, &aftldata);

			/* Sort by flag value. */
			ListView_SortItems(hwndListFlags, ListViewIntegerComparison, 0);

			/* Make the column big enough. */
			ListView_SetColumnWidth(hwndListFlags, 0, LVSCW_AUTOSIZE);

			/* Set up the up-down acceleration structure. */
			udaccel[0].nInc = 1;
			udaccel[0].nSec = 0;
			udaccel[1].nInc = 8;
			udaccel[1].nSec = 2;

			SendDlgItemMessage(hwndPropPage, IDC_SPIN_ANGLE, UDM_SETRANGE32, -32768, 32767);
			SendDlgItemMessage(hwndPropPage, IDC_SPIN_ANGLE, UDM_SETACCEL, 2, (LPARAM)udaccel);

			SendDlgItemMessage(hwndPropPage, IDC_SPIN_X, UDM_SETRANGE32, -32768, 32767);
			SendDlgItemMessage(hwndPropPage, IDC_SPIN_X, UDM_SETACCEL, 2, (LPARAM)udaccel);

			SendDlgItemMessage(hwndPropPage, IDC_SPIN_Y, UDM_SETRANGE32, -32768, 32767);
			SendDlgItemMessage(hwndPropPage, IDC_SPIN_Y, UDM_SETACCEL, 2, (LPARAM)udaccel);

			/* Z's range will change depending on the Z-factor. But we set a
			 * default.
			 */
			SendDlgItemMessage(hwndPropPage, IDC_SPIN_Z, UDM_SETRANGE32, 0, 4094);
			SendDlgItemMessage(hwndPropPage, IDC_SPIN_Z, UDM_SETACCEL, 2, (LPARAM)udaccel);


			/* Create an empty effect-to-node lookup table. */
			s_lpcfgTypeToNode = ConfigCreate();

			/* Populate the thing-type tree and corresponding lookup table. */
			lpcfgThingEffects = ConfigGetSubsection(s_lpmapppdata->lpcfgMap, "thingtypes");
			ttd.hwndTree = GetDlgItem(hwndPropPage, IDC_TREE_THING);
			ttd.lpcfgTypeToNode = s_lpcfgTypeToNode;
			ConfigIterate(lpcfgThingEffects, AddCategoryToTree, &ttd);


			/* Set the angles associated with the radio buttons. */
			SetWindowLong(GetDlgItem(hwndPropPage, IDC_RADIO_E), GWL_USERDATA, 0);
			SetWindowLong(GetDlgItem(hwndPropPage, IDC_RADIO_NE), GWL_USERDATA, 45);
			SetWindowLong(GetDlgItem(hwndPropPage, IDC_RADIO_N), GWL_USERDATA, 90);
			SetWindowLong(GetDlgItem(hwndPropPage, IDC_RADIO_NW), GWL_USERDATA, 135);
			SetWindowLong(GetDlgItem(hwndPropPage, IDC_RADIO_W), GWL_USERDATA, 180);
			SetWindowLong(GetDlgItem(hwndPropPage, IDC_RADIO_SW), GWL_USERDATA, 225);
			SetWindowLong(GetDlgItem(hwndPropPage, IDC_RADIO_S), GWL_USERDATA, 270);
			SetWindowLong(GetDlgItem(hwndPropPage, IDC_RADIO_SE), GWL_USERDATA, 315);


			/* Find out which fields match in the selection. */
			dwCheckFlags = CheckThings(
				s_lpmapppdata->lpmap,
				s_lpmapppdata->lpselection->lpsellistThings,
				ConfigGetSubsection(s_lpmapppdata->lpcfgMap, FLAT_THING_SECTION),
				&tdi);

			/* Now set them. */
			InitThingsPropPageFields(hwndPropPage, &tdi, dwCheckFlags);

			s_bInitComplete = TRUE;
		}

		/* Let the system set the focus. */
		return TRUE;


	case WM_COMMAND:

		switch(LOWORD(wParam))
		{
		case IDC_EDIT_THING:
			switch(HIWORD(wParam))
			{
			case EN_CHANGE:
				{
					BOOL bTranslated;
					unsigned short unType = (unsigned short)GetDlgItemInt(hwndPropPage, IDC_EDIT_THING, &bTranslated, FALSE);

					/* Valid number? */
					if(bTranslated)
					{
						/* Select corresponding tree item. */
						char szValue[12];
						wsprintfA(szValue, "%d", unType);

						if(ConfigNodeExists(s_lpcfgTypeToNode, szValue))
						{
							CONFIG *lpcfgThing;

							TreeView_SelectItem(GetDlgItem(hwndPropPage, IDC_TREE_THING), (HTREEITEM)ConfigGetInteger(s_lpcfgTypeToNode, szValue));

							lpcfgThing = GetThingConfigInfo(ConfigGetSubsection(s_lpmapppdata->lpcfgMap, FLAT_THING_SECTION), unType);
							if(lpcfgThing)
							{
								/* TODO: Check whether sprite specified at all. */
								TCHAR szSprite[TEXNAME_BUFFER_LENGTH];

								ConfigGetString(lpcfgThing, "sprite", szSprite, TEXNAME_BUFFER_LENGTH);

								SetTexturePreviewImage(
									s_lpmapppdata->hwndMap,
									GetDlgItem(hwndPropPage, IDC_TEX_SPRITE),
									szSprite,
									TF_IMAGE,
									s_hbmSprite,
									TRUE);
							}
						}
						else
							TreeView_SelectItem(GetDlgItem(hwndPropPage, IDC_TREE_THING), NULL);
					}
				}

				return TRUE;

			case EN_KILLFOCUS:
				/* Validation. */
				BoundEditBox((HWND)lParam, 0, 65535, TRUE, FALSE);

				return TRUE;
			}

			break;

		case IDC_EDIT_FLAGS:
			switch(HIWORD(wParam))
			{
			case EN_CHANGE:
				/* Clear the Z if the flags have been set. */
				if(s_bInitComplete && GetWindowTextLength((HWND)lParam) > 0)
					SetDlgItemText(hwndPropPage, IDC_EDIT_Z, TEXT(""));

				return TRUE;

			case EN_KILLFOCUS:
				/* Validation. */
				BoundEditBox((HWND)lParam, 0, 65535, TRUE, FALSE);

				return TRUE;
			}

			break;

		case IDC_EDIT_Z:
			switch(HIWORD(wParam))
			{
			case EN_CHANGE:
				/* Clear the flags if the Z has been set. */
				if(s_bInitComplete && GetWindowTextLength((HWND)lParam) > 0)
					SetDlgItemText(hwndPropPage, IDC_EDIT_FLAGS, TEXT(""));

				return TRUE;

			case EN_KILLFOCUS:
				/* Validation. TODO: zFactor. */
				BoundEditBox((HWND)lParam, 0, 4094, TRUE, TRUE);

				return TRUE;
			}

			break;

		case IDC_RADIO_E:
		case IDC_RADIO_NE:
		case IDC_RADIO_N:
		case IDC_RADIO_NW:
		case IDC_RADIO_W:
		case IDC_RADIO_SW:
		case IDC_RADIO_S:
		case IDC_RADIO_SE:
			/* If the radio button was clicked and is now set, set the angle. */
			if(HIWORD(wParam) == BN_CLICKED && SendMessage((HWND)lParam, BM_GETCHECK, 0, 0) == BST_CHECKED)
			{
				SetDlgItemInt(hwndPropPage, IDC_EDIT_ANGLE, GetWindowLong((HWND)lParam, GWL_USERDATA), FALSE);
				return TRUE;
			}

			break;

		case IDC_EDIT_ANGLE:
			switch(HIWORD(wParam))
			{
			case EN_CHANGE:
				{
					const int iRadioIDs[] = {IDC_RADIO_E, IDC_RADIO_NE, IDC_RADIO_N, IDC_RADIO_NW,
												IDC_RADIO_W, IDC_RADIO_SW, IDC_RADIO_S, IDC_RADIO_SE};
					size_t i;
					BOOL bValidNumber;
					int iAngle = GetDlgItemInt(hwndPropPage, LOWORD(wParam), &bValidNumber, TRUE);

					/* For each of the radio buttons, select it if we entered a
					 * valid angle and it matches that for this button, otherwise
					 * deselect it.
					 */
					for(i = 0; i < sizeof(iRadioIDs) / sizeof(int); i++)
						SendDlgItemMessage(hwndPropPage, iRadioIDs[i], BM_SETCHECK, bValidNumber && GetWindowLong(GetDlgItem(hwndPropPage, iRadioIDs[i]), GWL_USERDATA) == iAngle ? BST_CHECKED : BST_UNCHECKED, 0);
				}

				return TRUE;

			case EN_KILLFOCUS:
				/* Validation. */
				BoundEditBox((HWND)lParam, -32768, 32767, TRUE, TRUE);
				return TRUE;
			}

			break;

		case IDC_EDIT_X:
		case IDC_EDIT_Y:
			if(HIWORD(wParam) == EN_KILLFOCUS)
			{
				BoundEditBox((HWND)lParam, -32768, 32767, TRUE, TRUE);
				return TRUE;
			}

			break;
		}

		break;


	case WM_NOTIFY:
		{
			LPNMHDR lpnmhdr = (LPNMHDR)lParam;

			/* Special case: notifications from the property sheet don't set an
			 * ID.
			 */
			if(lpnmhdr->code == (unsigned)PSN_APPLY)
			{
				/* User OK-ed. Set the properties! */

				CONFIG *lpcfgFlatThings = ConfigGetSubsection(s_lpmapppdata->lpcfgMap, FLAT_THING_SECTION);
				THINGDISPLAYINFO tdi;
				THING_RELATIVE thingrelative;
				DWORD dwPropertyFlags = 0, dwRelativeFlags = 0;
				TCHAR szBuffer[7];
				WORD wFlags, wFlagMask;

				/* We go through each field, checking whether it's non-empty.
				 * If so, copy its value and set the corresponding flag.
				 */

				/* Thing type. */
				GetDlgItemText(hwndPropPage, IDC_EDIT_THING, szBuffer, sizeof(szBuffer)/sizeof(TCHAR));
				if(*szBuffer)
				{
					tdi.unType = GetDlgItemInt(hwndPropPage, IDC_EDIT_THING, NULL, FALSE);
					dwPropertyFlags |= TDIF_TYPE;
				}

				/* Angle. */
				GetDlgItemText(hwndPropPage, IDC_EDIT_ANGLE, szBuffer, sizeof(szBuffer)/sizeof(TCHAR));
				if(*szBuffer)
				{
					if(STRING_RELATIVE(szBuffer))
					{
						thingrelative.iAngle = _tcstol(&szBuffer[1], NULL, 10);
						dwRelativeFlags |= TDIF_DIRECTION;
					}
					else
					{
						tdi.nDirection = GetDlgItemInt(hwndPropPage, IDC_EDIT_ANGLE, NULL, TRUE);
						dwPropertyFlags |= TDIF_DIRECTION;
					}
				}

				/* X. */
				GetDlgItemText(hwndPropPage, IDC_EDIT_X, szBuffer, sizeof(szBuffer)/sizeof(TCHAR));
				if(*szBuffer)
				{
					if(STRING_RELATIVE(szBuffer))
					{
						thingrelative.x = _tcstol(&szBuffer[1], NULL, 10);
						dwRelativeFlags |= TDIF_X;
					}
					else
					{
						tdi.x = GetDlgItemInt(hwndPropPage, IDC_EDIT_X, NULL, TRUE);
						dwPropertyFlags |= TDIF_X;
					}
				}

				/* Y. */
				GetDlgItemText(hwndPropPage, IDC_EDIT_Y, szBuffer, sizeof(szBuffer)/sizeof(TCHAR));
				if(*szBuffer)
				{
					if(STRING_RELATIVE(szBuffer))
					{
						thingrelative.y = _tcstol(&szBuffer[1], NULL, 10);
						dwRelativeFlags |= TDIF_Y;
					}
					else
					{
						tdi.y = GetDlgItemInt(hwndPropPage, IDC_EDIT_Y, NULL, TRUE);
						dwPropertyFlags |= TDIF_Y;
					}
				}

				/* Flags. */
				GetDlgItemText(hwndPropPage, IDC_EDIT_FLAGS, szBuffer, sizeof(szBuffer)/sizeof(TCHAR));
				if(*szBuffer)
				{
					/* Value for flags is specified, so just use that. */
					wFlags = GetDlgItemInt(hwndPropPage, IDC_EDIT_FLAGS, NULL, FALSE);
					dwPropertyFlags |= TDIF_FLAGS;

					/* All flags are valid. */
					wFlagMask = (WORD)-1;
				}
				else
				{
					/* No direct value specified from flags, so construct it
					 * from the checkboxes.
					 */
					GetFlagsFromListView(GetDlgItem(hwndPropPage, IDC_LIST_FLAGS), &wFlags, &wFlagMask);
				}


				/* Apply the relative properties. */
				if(dwRelativeFlags & TDIF_DIRECTION)
					ApplyRelativeAngleSelection(s_lpmapppdata->lpmap, s_lpmapppdata->lpselection->lpsellistThings, thingrelative.iAngle);

				if(dwRelativeFlags & TDIF_X)
					ApplyRelativeThingXSelection(s_lpmapppdata->lpmap, s_lpmapppdata->lpselection->lpsellistThings, thingrelative.x);

				if(dwRelativeFlags & TDIF_Y)
					ApplyRelativeThingYSelection(s_lpmapppdata->lpmap, s_lpmapppdata->lpselection->lpsellistThings, thingrelative.y);

				/* We've got all the properties now. Apply them. */
				ApplyThingPropertiesToSelection(s_lpmapppdata->lpmap, s_lpmapppdata->lpselection->lpsellistThings, &tdi, dwPropertyFlags, lpcfgFlatThings);
				SetThingFlags(s_lpmapppdata->lpmap, s_lpmapppdata->lpselection->lpsellistThings, wFlags, wFlagMask);

				/* Z. This must be applied after flags! */
				GetDlgItemText(hwndPropPage, IDC_EDIT_Z, szBuffer, sizeof(szBuffer)/sizeof(TCHAR));
				if(*szBuffer)
				{
					if(STRING_RELATIVE(szBuffer))
					{
						thingrelative.z = _tcstol(&szBuffer[1], NULL, 10);
						ApplyRelativeThingZSelection(s_lpmapppdata->lpmap, s_lpmapppdata->lpselection->lpsellistThings, lpcfgFlatThings, thingrelative.z);
					}
					else
					{
						SetThingSelectionZ(
							s_lpmapppdata->lpmap,
							s_lpmapppdata->lpselection->lpsellistThings,
							lpcfgFlatThings,
							(WORD)GetDlgItemInt(hwndPropPage, IDC_EDIT_Z, NULL, FALSE),
							IsDlgButtonChecked(hwndPropPage, IDC_CHECK_ABSZ) != BST_UNCHECKED);
					}
				}

				/* We've made changes. */
				s_lpmapppdata->bOKed = TRUE;

				return TRUE;
			}

			/* Still WM_NOTIFY... */

			switch(lpnmhdr->idFrom)
			{
			case IDC_TREE_THING:
				{
					LPNMTREEVIEW lpnmtreeview = (LPNMTREEVIEW)lParam;

					switch(lpnmhdr->code)
					{
					case TVN_SELCHANGED:
						/* Does the selected node correspond to an effect, and
						 * was it the direct result of user input (and not
						 * because the edit control changed)? If so, set the
						 * edit control.
						 */
						if((lpnmtreeview->action == TVC_BYKEYBOARD || lpnmtreeview->action == TVC_BYMOUSE) && lpnmtreeview->itemNew.lParam >= 0)
							SetDlgItemInt(hwndPropPage, IDC_EDIT_THING, lpnmtreeview->itemNew.lParam, FALSE);

						return TRUE;
					}
				}

				break;

			case IDC_LIST_FLAGS:
				{
					LPNMLISTVIEW lpnmlistview = ((LPNMLISTVIEW)lParam);

					switch(lpnmhdr->code)
					{
					case NM_DBLCLK:
					case NM_CLICK:
						/* Do checkbox processing. */
						ListView3StateClick(lpnmlistview);
						return TRUE;

					case LVN_KEYDOWN:
						/* Do checkbox processing. */
						ListView3StateKeyDown((LPNMLVKEYDOWN)lParam);
						return TRUE;

					case LVN_ITEMCHANGED:
						/* If user changed the flags, clear flags and Z edit
						 * boxes.
						 */
						if(s_bInitComplete && (lpnmlistview->uChanged & LVIF_STATE) && ((lpnmlistview->uNewState ^ lpnmlistview->uOldState) & LVIS_STATEIMAGEMASK))
						{
							SetDlgItemText(hwndPropPage, IDC_EDIT_FLAGS, TEXT(""));
							SetDlgItemText(hwndPropPage, IDC_EDIT_Z, TEXT(""));
						}

						return TRUE;
					}
				}

				break;
			}
		}

		break;

	case WM_DESTROY:

		/* Free effect no. -> node lookup. */
		ConfigDestroy(s_lpcfgTypeToNode);

		/* Free sprite bitmap. */
		DeleteObject(s_hbmSprite);

		return TRUE;
	}

	/* Didn't process message. */
	return FALSE;
}


/* InitSectorsPropPageFields
 *   Fills fields of the sector property page.
 *
 * Parameters:
 *   HWND				hwndPropPage	Property page window handle.
 *   SECTORDISPLAYINFO*	lpsdi			Data to fill in.
 *   DWORD				dwCheckFlags	Flags specifying which fields are valid.
 *
 * Return value: None.
 */
static void InitSectorsPropPageFields(HWND hwndPropPage, SECTORDISPLAYINFO *lpsdi, DWORD dwCheckFlags)
{
	/* Some are set to zero initially by their up-down controls, so we have to
	 * blank those if that's what we want. With the others, they're blank to
	 * start with.
	 */

	if(dwCheckFlags & SDIF_EFFECT)		SetDlgItemInt(hwndPropPage, IDC_EDIT_EFFECT, lpsdi->unEffect, FALSE);

	if(dwCheckFlags & SDIF_CEILING) SetDlgItemInt(hwndPropPage, IDC_EDIT_HCEIL, lpsdi->nCeiling, TRUE);
	else SetDlgItemText(hwndPropPage, IDC_EDIT_HCEIL, TEXT(""));

	if(dwCheckFlags & SDIF_FLOOR) SetDlgItemInt(hwndPropPage, IDC_EDIT_HFLOOR, lpsdi->nFloor, TRUE);
	else SetDlgItemText(hwndPropPage, IDC_EDIT_HFLOOR, TEXT(""));

	if(dwCheckFlags & SDIF_TAG) SetDlgItemInt(hwndPropPage, IDC_EDIT_TAG, lpsdi->unTag, FALSE);
	else SetDlgItemText(hwndPropPage, IDC_EDIT_TAG, TEXT(""));

	if(dwCheckFlags & SDIF_BRIGHTNESS)
	{
		SetDlgItemInt(hwndPropPage, IDC_COMBO_LIGHT, lpsdi->ucBrightness, FALSE);
		SendDlgItemMessage(hwndPropPage, IDC_SLIDER_LIGHT, TBM_SETPOS, TRUE, lpsdi->ucBrightness);
	}

	if(dwCheckFlags & SDIF_CEILINGTEX)	SetDlgItemText(hwndPropPage, IDC_EDIT_TCEIL, lpsdi->szCeiling);
	if(dwCheckFlags & SDIF_FLOORTEX)	SetDlgItemText(hwndPropPage, IDC_EDIT_TFLOOR, lpsdi->szFloor);
}


/* InitLinesPropPageFields
 *   Fills fields of the lines property page.
 *
 * Parameters:
 *   HWND					hwndPropPage	Property page window handle.
 *   LINEDEFDISPLAYINFO*	lplddi			Data to fill in.
 *   DWORD					dwCheckFlags	Flags specifying which fields are
 *											valid.
 *
 * Return value: None.
 */
static void InitLinesPropPageFields(HWND hwndPropPage, LINEDEFDISPLAYINFO *lplddi, DWORD dwCheckFlags)
{
	/* Some are set to zero initially by their up-down controls, so we have to
	 * blank those if that's what we want. With the others, they're blank to
	 * start with.
	 */

	if(dwCheckFlags & LDDIF_EFFECT)		SetDlgItemInt(hwndPropPage, IDC_EDIT_EFFECT, lplddi->unEffect, FALSE);

	if(dwCheckFlags & LDDIF_TAG) SetDlgItemInt(hwndPropPage, IDC_EDIT_TAG, lplddi->unTag, FALSE);
	else SetDlgItemText(hwndPropPage, IDC_EDIT_TAG, TEXT(""));

	if((dwCheckFlags & LDDIF_HASFRONT) && lplddi->bHasFront)
	{
		if(dwCheckFlags & LDDIF_FRONTSEC)	SetDlgItemInt(hwndPropPage, IDC_EDIT_FRONT_SECTOR, lplddi->unFrontSector, FALSE);

		if(dwCheckFlags & LDDIF_FRONTX) SetDlgItemInt(hwndPropPage, IDC_EDIT_FRONT_OFFX, lplddi->nFrontX, TRUE);
		else SetDlgItemText(hwndPropPage, IDC_EDIT_FRONT_OFFX, TEXT(""));

		if(dwCheckFlags & LDDIF_FRONTY) SetDlgItemInt(hwndPropPage, IDC_EDIT_FRONT_OFFY, lplddi->nFrontY, TRUE);
		else SetDlgItemText(hwndPropPage, IDC_EDIT_FRONT_OFFY, TEXT(""));

		if(dwCheckFlags & LDDIF_FRONTUPPER)	SetDlgItemText(hwndPropPage, IDC_EDIT_FRONT_UPPER, lplddi->szFrontUpper);
		if(dwCheckFlags & LDDIF_FRONTMIDDLE)	SetDlgItemText(hwndPropPage, IDC_EDIT_FRONT_MIDDLE, lplddi->szFrontMiddle);
		if(dwCheckFlags & LDDIF_FRONTLOWER)	SetDlgItemText(hwndPropPage, IDC_EDIT_FRONT_LOWER, lplddi->szFrontLower);
	}
	else
	{
		/* No front sector, so disable the front controls. */
		EnableWindow(GetDlgItem(hwndPropPage, IDC_GROUP_FRONT), FALSE);
		EnableWindow(GetDlgItem(hwndPropPage, IDC_STATIC_FRONT_OFFSET), FALSE);
		EnableWindow(GetDlgItem(hwndPropPage, IDC_STATIC_FRONT_SECTOR), FALSE);
		EnableWindow(GetDlgItem(hwndPropPage, IDC_STATIC_FRONT_UPPER), FALSE);
		EnableWindow(GetDlgItem(hwndPropPage, IDC_STATIC_FRONT_MIDDLE), FALSE);
		EnableWindow(GetDlgItem(hwndPropPage, IDC_STATIC_FRONT_LOWER), FALSE);
		EnableWindow(GetDlgItem(hwndPropPage, IDC_TEX_FRONT_UPPER), FALSE);
		EnableWindow(GetDlgItem(hwndPropPage, IDC_TEX_FRONT_MIDDLE), FALSE);
		EnableWindow(GetDlgItem(hwndPropPage, IDC_TEX_FRONT_LOWER), FALSE);
		EnableWindow(GetDlgItem(hwndPropPage, IDC_EDIT_FRONT_UPPER), FALSE);
		EnableWindow(GetDlgItem(hwndPropPage, IDC_EDIT_FRONT_MIDDLE), FALSE);
		EnableWindow(GetDlgItem(hwndPropPage, IDC_EDIT_FRONT_LOWER), FALSE);
		EnableWindow(GetDlgItem(hwndPropPage, IDC_EDIT_FRONT_OFFX), FALSE);
		EnableWindow(GetDlgItem(hwndPropPage, IDC_EDIT_FRONT_OFFY), FALSE);
		EnableWindow(GetDlgItem(hwndPropPage, IDC_SPIN_FRONT_OFFX), FALSE);
		EnableWindow(GetDlgItem(hwndPropPage, IDC_SPIN_FRONT_OFFY), FALSE);
		EnableWindow(GetDlgItem(hwndPropPage, IDC_EDIT_FRONT_SECTOR), FALSE);
		EnableWindow(GetDlgItem(hwndPropPage, IDC_BUTTON_FRONT_VISOFF), FALSE);
		EnableWindow(GetDlgItem(hwndPropPage, IDC_CHECK_FRONT_SAVEDEFAULT), FALSE);
	}

	if((dwCheckFlags & LDDIF_HASBACK) && lplddi->bHasBack)
	{
		if(dwCheckFlags & LDDIF_BACKSEC)	SetDlgItemInt(hwndPropPage, IDC_EDIT_BACK_SECTOR, lplddi->unBackSector, FALSE);

		if(dwCheckFlags & LDDIF_BACKX)		SetDlgItemInt(hwndPropPage, IDC_EDIT_BACK_OFFX, lplddi->nBackX, TRUE);
		else SetDlgItemText(hwndPropPage, IDC_EDIT_BACK_OFFX, TEXT(""));

		if(dwCheckFlags & LDDIF_BACKY)		SetDlgItemInt(hwndPropPage, IDC_EDIT_BACK_OFFY, lplddi->nBackY, TRUE);
		else SetDlgItemText(hwndPropPage, IDC_EDIT_BACK_OFFY, TEXT(""));

		if(dwCheckFlags & LDDIF_BACKUPPER)	SetDlgItemText(hwndPropPage, IDC_EDIT_BACK_UPPER, lplddi->szBackUpper);
		if(dwCheckFlags & LDDIF_BACKMIDDLE)	SetDlgItemText(hwndPropPage, IDC_EDIT_BACK_MIDDLE, lplddi->szBackMiddle);
		if(dwCheckFlags & LDDIF_BACKLOWER)	SetDlgItemText(hwndPropPage, IDC_EDIT_BACK_LOWER, lplddi->szBackLower);
	}
	else
	{
		/* No back sector, so disable the back controls. */
		EnableWindow(GetDlgItem(hwndPropPage, IDC_GROUP_BACK), FALSE);
		EnableWindow(GetDlgItem(hwndPropPage, IDC_STATIC_BACK_OFFSET), FALSE);
		EnableWindow(GetDlgItem(hwndPropPage, IDC_STATIC_BACK_SECTOR), FALSE);
		EnableWindow(GetDlgItem(hwndPropPage, IDC_STATIC_BACK_UPPER), FALSE);
		EnableWindow(GetDlgItem(hwndPropPage, IDC_STATIC_BACK_MIDDLE), FALSE);
		EnableWindow(GetDlgItem(hwndPropPage, IDC_STATIC_BACK_LOWER), FALSE);
		EnableWindow(GetDlgItem(hwndPropPage, IDC_TEX_BACK_UPPER), FALSE);
		EnableWindow(GetDlgItem(hwndPropPage, IDC_TEX_BACK_MIDDLE), FALSE);
		EnableWindow(GetDlgItem(hwndPropPage, IDC_TEX_BACK_LOWER), FALSE);
		EnableWindow(GetDlgItem(hwndPropPage, IDC_EDIT_BACK_UPPER), FALSE);
		EnableWindow(GetDlgItem(hwndPropPage, IDC_EDIT_BACK_MIDDLE), FALSE);
		EnableWindow(GetDlgItem(hwndPropPage, IDC_EDIT_BACK_LOWER), FALSE);
		EnableWindow(GetDlgItem(hwndPropPage, IDC_EDIT_BACK_OFFX), FALSE);
		EnableWindow(GetDlgItem(hwndPropPage, IDC_EDIT_BACK_OFFY), FALSE);
		EnableWindow(GetDlgItem(hwndPropPage, IDC_SPIN_BACK_OFFX), FALSE);
		EnableWindow(GetDlgItem(hwndPropPage, IDC_SPIN_BACK_OFFY), FALSE);
		EnableWindow(GetDlgItem(hwndPropPage, IDC_EDIT_BACK_SECTOR), FALSE);
		EnableWindow(GetDlgItem(hwndPropPage, IDC_BUTTON_BACK_VISOFF), FALSE);
		EnableWindow(GetDlgItem(hwndPropPage, IDC_CHECK_BACK_SAVEDEFAULT), FALSE);
	}
}


/* InitThingsPropPageFields
 *   Fills fields of the things property page.
 *
 * Parameters:
 *   HWND				hwndPropPage	Property page window handle.
 *   THINGDISPLAYINFO*	lptdi			Data to fill in.
 *   DWORD				dwCheckFlags	Flags specifying which fields are valid.
 *
 * Return value: None.
 */
static void InitThingsPropPageFields(HWND hwndPropPage, THINGDISPLAYINFO *lptdi, DWORD dwCheckFlags)
{
	/* Some are set to zero initially by their up-down controls, so we have to
	 * blank those if that's what we want. With the others, they're blank to
	 * start with.
	 */

	if(dwCheckFlags & TDIF_TYPE) SetDlgItemInt(hwndPropPage, IDC_EDIT_THING, lptdi->unType, FALSE);

	if(dwCheckFlags & TDIF_X) SetDlgItemInt(hwndPropPage, IDC_EDIT_X, lptdi->x, TRUE);
	else SetDlgItemText(hwndPropPage, IDC_EDIT_X, TEXT(""));

	if(dwCheckFlags & TDIF_Y) SetDlgItemInt(hwndPropPage, IDC_EDIT_Y, lptdi->y, TRUE);
	else SetDlgItemText(hwndPropPage, IDC_EDIT_Y, TEXT(""));

	if(dwCheckFlags & TDIF_Z) SetDlgItemInt(hwndPropPage, IDC_EDIT_Z, lptdi->z, FALSE);
	else SetDlgItemText(hwndPropPage, IDC_EDIT_Z, TEXT(""));

	if(dwCheckFlags & TDIF_FLAGS) SetDlgItemInt(hwndPropPage, IDC_EDIT_FLAGS, lptdi->unFlags, FALSE);

	if(dwCheckFlags & TDIF_DIRECTION) SetDlgItemInt(hwndPropPage, IDC_EDIT_ANGLE, lptdi->nDirection, TRUE);
	else SetDlgItemText(hwndPropPage, IDC_EDIT_ANGLE, TEXT(""));
}


/* AddSectorTypeToList
 *   Adds a sector effect to a list-box.
 *
 * Parameters:
 *   CONFIG		*lpcfg		Config node containing effect info.
 *   void*		lpvWindow	(HWND) Window handle of list-box.
 *
 * Return value: BOOL
 *   Always TRUE.
 *
 * Remarks:
 *   Used as a callback function by ConfigIterate.
 */
static BOOL AddSectorTypeToList(CONFIG *lpcfg, void *lpvWindow)
{
	HWND hwndList = (HWND)lpvWindow;
	int iIndex, iEffect;

	/* Add entry and store effect value. */
	iIndex = SendMessage(hwndList, LB_ADDSTRING, 0, (LPARAM)lpcfg->sz);
	iEffect = _tcstol(lpcfg->szName, NULL, 10);
	SendMessage(hwndList, LB_SETITEMDATA, iIndex, iEffect);

	/* Keep iterating. */
	return TRUE;
}


/* AddFlagToList
 *   Adds a linedef/thing flag to a list-view control.
 *
 * Parameters:
 *   CONFIG		*lpcfg		Config node containing flag info.
 *   void*		lpvData		(AFTLDATA*) Data including window handle of list
 *							and line selection flag values for initialisation.
 *
 * Return value: BOOL
 *   Always TRUE.
 *
 * Remarks:
 *   Used as a callback function by ConfigIterate.
 */
static BOOL AddFlagToList(CONFIG *lpcfg, void *lpvData)
{
	AFTLDATA *lpaftldata = (AFTLDATA*)lpvData;
	LVITEM lvitem;
	int iIndex, iState;

	/* Add entry and store effect value. */
	lvitem.mask = LVIF_TEXT | LVIF_PARAM;
	lvitem.iItem = lvitem.iSubItem = 0;
	lvitem.pszText = lpcfg->sz;
	lvitem.lParam = _tcstol(lpcfg->szName, NULL, 10);
	iIndex = ListView_InsertItem(lpaftldata->hwndListView, &lvitem);

	/* Determine the initial state. */
	if(!(lpaftldata->wFlagMask & lvitem.lParam))
		iState = LV3_INDETERMINATE;
	else if(lpaftldata->wFlags & lvitem.lParam)
		iState = LV3_CHECKED;
	else iState = LV3_UNCHECKED;

	ListView3StateSetItemState(lpaftldata->hwndListView, iIndex, iState);

	/* Keep iterating. */
	return TRUE;
}


LRESULT CALLBACK TexPreviewProc(HWND hwnd, UINT uiMsg, WPARAM wParam, LPARAM lParam)
{
	TPPDATA *lptppd = (TPPDATA*)GetWindowLong(hwnd, GWL_USERDATA);

	switch(uiMsg)
	{
	case WM_SETCURSOR:
		{
			HCURSOR hcursorHand = LoadCursor(NULL, IDC_HAND);

			/* Non-NT Windows don't support IDC_HAND. */
			if(hcursorHand) SetCursor(hcursorHand);
		}

		return 0;

	case WM_DESTROY:
		/* Restore default window class -- WM_DESTROY is apparently not the last
		 * message.
		 */
		SetWindowLong(hwnd, GWL_WNDPROC, (LONG)lptppd->wndprocStatic);

		/* Free subclassing data. */
		ProcHeapFree(lptppd);
		return 0;
	}

	/* The original wndproc is stored as our userdata. */
	return CallWindowProc(lptppd->wndprocStatic, hwnd, uiMsg, wParam, lParam);
}



/* AddCategoryToTree
 *   Creates a top-level node in a tree for a linedef/thing category, and adds
 *   all the associated types as children.
 *
 * Parameters:
 *   CONFIG*	lpcfgType	A linedef/thing type category subsection container.
 *   void*		lpv			(TYPETREEDATA*) Specifies handle of control and
 *							config used to create the effect-to-node lookup;
 *							parent field is ignored.
 *
 * Return value: BOOL
 *   TRUE if iteration should continue; FALSE otherwise.
 *
 * Remarks:
 *   Used as a callback by ConfigIterate.
 */
static BOOL AddCategoryToTree(CONFIG *lpcfgCategory, void *lpv)
{
	TYPETREEDATA *lpttd = (TYPETREEDATA*)lpv;
	CONFIG *lpcfgValues = ConfigGetSubsection(lpcfgCategory->lpcfgSubsection, "values");
	int cchCategoryTitle = ConfigGetStringLength(lpcfgCategory->lpcfgSubsection, "title");

	/* Sanity check. */
	if(lpcfgValues && cchCategoryTitle > 0)
	{
		TVINSERTSTRUCT tvis;

		/* Get the string for the parent node. */
		tvis.item.pszText = ProcHeapAlloc((cchCategoryTitle + 1) * sizeof(TCHAR));
		ConfigGetString(lpcfgCategory->lpcfgSubsection, "title", tvis.item.pszText, cchCategoryTitle + 1);

		/* Set up some properties for the new node. */
		tvis.hParent = TVI_ROOT;
		tvis.hInsertAfter = TVI_SORT;
		tvis.item.mask = TVIF_TEXT | TVIF_PARAM;
		tvis.item.lParam = -1;		/* Doesn't correspond to an effect. */

		/* Create the parent node. */
		lpttd->htreeitemParent = TreeView_InsertItem(lpttd->hwndTree, &tvis);

		/* Finished with the title now. */
		ProcHeapFree(tvis.item.pszText);

		/* Add all the children. */
		ConfigIterate(lpcfgValues, AddTypeToTree, lpttd);
	}

	/* Keep going. */
	return TRUE;
}


/* AddTypeToTree
 *   Adds a linedef effect type as a child of a tree node.
 *
 * Parameters:
 *   CONFIG*	lpcfgType		A linedef/thing type entry.
 *   void*		lpv				(TYPETREEDATA*) Specifies handle of control,
 *								config used to create the effect-to-node lookup
 *								and parent node.
 *
 * Return value: BOOL
 *   TRUE if iteration should continue; FALSE otherwise.
 *
 * Remarks:
 *   Used as a callback by ConfigIterate.
 */
static BOOL AddTypeToTree(CONFIG *lpcfgType, void *lpv)
{
	TYPETREEDATA *lpttd = (TYPETREEDATA*)lpv;
	TVINSERTSTRUCT tvis;
	int cchTitle;
	HTREEITEM htreeitemNew;

	/* Get the display text. */
	switch(lpcfgType->entrytype)
	{
	case CET_STRING:
		/* Get the string for the node: *yes*, we already have it, but this will
		 * return it in the correct format, rather than always ANSI.
		 */
		cchTitle = ConfigGetStringLength(lpcfgType, lpcfgType->szName);
		tvis.item.pszText = ProcHeapAlloc((cchTitle + 1) * sizeof(TCHAR));
		ConfigGetString(lpcfgType, lpcfgType->szName, tvis.item.pszText, cchTitle + 1);
		break;

	case CET_SUBSECTION:
		cchTitle = ConfigGetStringLength(lpcfgType->lpcfgSubsection, "title");
		tvis.item.pszText = ProcHeapAlloc((cchTitle + 1) * sizeof(TCHAR));
		ConfigGetString(lpcfgType->lpcfgSubsection, "title", tvis.item.pszText, cchTitle + 1);
		break;

	default:
		break;
	}

	/* Set up some properties for the new node. */
	tvis.hParent = lpttd->htreeitemParent;
	tvis.hInsertAfter = TVI_SORT;
	tvis.item.mask = TVIF_TEXT | TVIF_PARAM;
	tvis.item.lParam = atoi(lpcfgType->szName);

	/* Create the node. */
	htreeitemNew = TreeView_InsertItem(lpttd->hwndTree, &tvis);

	/* Add the node to the lookup table. */
	ConfigSetInteger(lpttd->lpcfgTypeToNode, lpcfgType->szName, (int)htreeitemNew);

	/* Clean up. */
	ProcHeapFree(tvis.item.pszText);

	return TRUE;
}


/* SetTexturePreviewImage
 *   Sets the image of a static control to that of the specified texture.
 *
 * Parameters:
 *   HWND		hwndMap		Handle to map window.
 *   HWND		hwndCtrl	Handle to static control.
 *   LPCTSTR	szTexName	Name of texture.
 *   TEX_FORMAT	tf			Texture format.
 *   HBITMAP	hbmPreview	Bitmap object used by the control.
 *   BOOL		bRequired	Whether the texture is required. Only used for
 *							TF_TEXTURE.
 *
 * Return value: None.
 */
static void SetTexturePreviewImage(HWND hwndMap, HWND hwndCtrl, LPCTSTR szTexName, TEX_FORMAT tf, HBITMAP hbmPreview, BOOL bRequired)
{
	TEXTURE *lptex;
	BOOL bNeedFree = GetTextureForMap(hwndMap, szTexName, &lptex, tf);
	HDC hdcPreview;

	hdcPreview = CreateCompatibleDC(NULL);
	SelectObject(hdcPreview, hbmPreview);

	if(lptex)
	{
		StretchTextureToDC(lptex, hdcPreview, CX_TEXPREVIEW, CY_TEXPREVIEW);

		/* Free memory if necessary. */
		if(bNeedFree) DestroyTexture(lptex);
	}
	else
	{
		HBITMAP hbmSource;
		HDC		hdcSource;
		INT		iBitmapID;

		if(tf == TF_TEXTURE)
		{
			/* If it's a pseudo-texture, draw the blank image. */
			if(IsPseudoTexture(szTexName))
				iBitmapID = IDB_NOTEXTURE;

			/* If it's the blank texture, draw the blank image *unless* the
			 * texture is required, in which case use 'Missing Texture'.
			 */
			else if(IsBlankTexture(szTexName))
				iBitmapID = bRequired ? IDB_MISSINGTEXTURE : IDB_NOTEXTURE;

			else iBitmapID = IDB_UNKNOWNTEXTURE;
		}
		/* Handle pseudo-flats, and unknown flats. */
		else if(tf == TF_FLAT)
			iBitmapID = IsPseudoFlat(szTexName) ? IDB_NOTEXTURE : IDB_UNKNOWNFLAT;
		/* Finally, unknown images. */
		else iBitmapID = IDB_UNKNOWNIMAGE;

		hbmSource = LoadBitmap(g_hInstance, MAKEINTRESOURCE(iBitmapID));
		hdcSource = CreateCompatibleDC(NULL);
		SelectObject(hdcSource, hbmSource);

		/* We don't stretch resource bitmaps. */
		BitBlt(hdcPreview, 0, 0, CX_TEXPREVIEW, CY_TEXPREVIEW, hdcSource, 0, 0, SRCCOPY);

		DeleteDC(hdcSource);
		DeleteObject(hbmSource);
	}

	DeleteDC(hdcPreview);

	InvalidateRect(hwndCtrl, NULL, FALSE);
}


/* GetFlagsFromListView
 *   Obtains the values of flags from a 3-state checkbox list view, and also a
 *   mask indicating which of these flags are valid.
 *
 * Parameters:
 *   HWND	hwndListView	Handle to list control.
 *   WORD	*lpwFlagValues	Used to return values of shared flags.
 *   WORD	*lpwFlagMask	Used to return which of the flags in the
 *							above are valid.
 *
 * Return value: None.
 */
static void GetFlagsFromListView(HWND hwndListView, WORD *lpwFlagValues, WORD *lpwFlagMask)
{
	int iCountItems, i;

	/* Zero the flags and mask to begin with. */
	*lpwFlagMask = *lpwFlagValues = 0;

	/* How many flags in our list? */
	iCountItems = ListView_GetItemCount(hwndListView);

	/* Repeat for each flag. */
	for(i = 0; i < iCountItems; i++)
	{
		int iState = ListView3StateGetItemState(hwndListView, i);

		/* If the checkbox is on or off (but not mixed), we set the flag if
		 * necessary, and indicate in the mask that the bit is valid.
		 */
		if(iState != LV3_INDETERMINATE)
		{
			LVITEM lvitem;

			/* Get the flag value. */
			lvitem.iItem = i;
			lvitem.iSubItem = 0;
			lvitem.mask = LVIF_PARAM;
			ListView_GetItem(hwndListView, &lvitem);

			/* This bit is valid, so set the bit in the mask. */
			*lpwFlagMask |= (WORD)lvitem.lParam;

			/* If the checkbox is ticked, we also set the bit in the flags
			 * themselves. No need to reset the bit if the box is unticked,
			 * since we zeroed everything at the start.
			 */
			if(iState == LV3_CHECKED) *lpwFlagValues |= (WORD)lvitem.lParam;
		}
	}
}


/* InsertSectorDlg
 *   Dialogue proc for Insert Sector dialouge box.
 *
 * Parameters:
 *   HWND					hwndParent	Parent for dialogue.
 *   INSERTSECTORDLGDATA*	lpisdd		Pointer to structure in which to return
 *										the options specified by the user.
 *
 * Return value: BOOL
 *   FALSE if cancelled; TRUE otherwise.
 */
BOOL InsertSectorDlg(HWND hwndParent, INSERTSECTORDLGDATA *lpisdd)
{
	return DialogBoxParam(g_hInstance, MAKEINTRESOURCE(IDD_INSERTSECTOR), hwndParent, InsertSectorDlgProc, (LPARAM)lpisdd);
}



/* InsertSectorDlgProc
 *   Dialogue proc for Insert Sector dialouge box.
 *
 * Parameters:
 *   HWND	hwndDlg		Dialogue window handle.
 *   UINT	uiMsg		Message identifier.
 *   WPARAM wParam		Message-specific.
 *   LPARAM lParam		Message-specific.
 *
 * Return value: BOOL
 *   TRUE if the message was processed; FALSE otherwise.
 */
static BOOL CALLBACK InsertSectorDlgProc(HWND hwndDlg, UINT uiMsg, WPARAM wParam, LPARAM lParam)
{
	static INSERTSECTORDLGDATA *s_lpisdd;

	switch(uiMsg)
	{
	case WM_INITDIALOG:
		{
			UDACCEL udaccelEdges[1], udaccelRadius[2];

			/* Get the structure to fill. Its radius and snapping fields are
			 * used to initialise the dialogue, but not the others.
			 */
			s_lpisdd = (INSERTSECTORDLGDATA*)lParam;

			/* Set up the spinner controls. */

			udaccelRadius[0].nInc = 8;
			udaccelRadius[0].nSec = 0;
			udaccelRadius[1].nInc = 64;
			udaccelRadius[1].nSec = 2;

			udaccelEdges[0].nInc = 1;
			udaccelEdges[0].nSec = 0;

			SendDlgItemMessage(hwndDlg, IDC_SPIN_EDGES, UDM_SETRANGE32, EDGESMIN, EDGESMAX);
			SendDlgItemMessage(hwndDlg, IDC_SPIN_EDGES, UDM_SETACCEL, 1, (LPARAM)udaccelEdges);

			SendDlgItemMessage(hwndDlg, IDC_SPIN_RADIUS, UDM_SETRANGE32, RADIUSMIN, RADIUSMAX);
			SendDlgItemMessage(hwndDlg, IDC_SPIN_RADIUS, UDM_SETACCEL, 2, (LPARAM)udaccelRadius);

			/* Initialise edges and radius editboxes. */
			SetDlgItemInt(hwndDlg, IDC_EDIT_EDGES, EDGESINIT, FALSE);
			SetDlgItemInt(hwndDlg, IDC_EDIT_RADIUS, s_lpisdd->nRadius, FALSE);


			/* Use the structure's snapping parameter to set the initial state
			 * of the checkbox.
			 */
			CheckDlgButton(hwndDlg, IDC_CHECK_SNAP, s_lpisdd->bSnap);

			/* Assume radius measured to edges. Good for squares. */
			CheckRadioButton(hwndDlg, IDC_RADIO_EDGES, IDC_RADIO_VERTICES, IDC_RADIO_EDGES);
		}

		/* Let the system set the focus. */
		return TRUE;

	case WM_COMMAND:
		switch(LOWORD(wParam))
		{
		case IDOK:
			/* Set the structure fields. */
			s_lpisdd->nEdges = GetDlgItemInt(hwndDlg, IDC_EDIT_EDGES, NULL, FALSE);
			s_lpisdd->nRadius = GetDlgItemInt(hwndDlg, IDC_EDIT_RADIUS, NULL, FALSE);
			s_lpisdd->bSnap = IsDlgButtonChecked(hwndDlg, IDC_CHECK_SNAP) != BST_UNCHECKED;
			s_lpisdd->cRadiusType = (IsDlgButtonChecked(hwndDlg, IDC_RADIO_EDGES) != BST_UNCHECKED) ? RT_TOEDGES : RT_TOVERTICES;

			/* Signal that we OKed. */
			EndDialog(hwndDlg, TRUE);

			return TRUE;

		case IDCANCEL:
			/* Signal that we cancelled. */
			EndDialog(hwndDlg, FALSE);

			return TRUE;

		case IDC_EDIT_EDGES:
			if(HIWORD(wParam) == EN_KILLFOCUS) BoundEditBox((HWND)lParam, EDGESMIN, EDGESMAX, FALSE, FALSE);
			return TRUE;

		case IDC_EDIT_RADIUS:
			if(HIWORD(wParam) == EN_KILLFOCUS) BoundEditBox((HWND)lParam, RADIUSMIN, RADIUSMAX, FALSE, FALSE);
			return TRUE;
		}

		break;
	}

	/* Didn't process message. */
	return FALSE;
}


/* RotateDlg
 *   Displays the Rotate dialouge box.
 *
 * Parameters:
 *   HWND				hwndParent	Parent for dialogue.
 *   ROTATEDLGDATA*		lprotdd		Pointer to structure in which to return the
 *									options specified by the user, and used to
 *									determine whether the rotate-about-selected-
 *									thing/vertex options should be enabled.
 *
 * Return value: BOOL
 *   FALSE if cancelled; TRUE otherwise.
 */
BOOL RotateDlg(HWND hwndParent, ROTATEDLGDATA *lprotdd)
{
	return DialogBoxParam(g_hInstance, MAKEINTRESOURCE(IDD_ROTATE), hwndParent, RotateDlgProc, (LPARAM)lprotdd);
}


/* RotateDlgProc
 *   Dialogue proc for Rotate dialouge box.
 *
 * Parameters:
 *   HWND	hwndDlg		Dialogue window handle.
 *   UINT	uiMsg		Message identifier.
 *   WPARAM wParam		Message-specific.
 *   LPARAM lParam		Message-specific.
 *
 * Return value: BOOL
 *   TRUE if the message was processed; FALSE otherwise.
 */
static BOOL CALLBACK RotateDlgProc(HWND hwndDlg, UINT uiMsg, WPARAM wParam, LPARAM lParam)
{
	static ROTATEDLGDATA *s_lprotdd;

	switch(uiMsg)
	{
	case WM_INITDIALOG:
		{
			UDACCEL udaccel[2];

			/* Get the structure in which we return parameters, and from which
			 * we determine whether to enable some radio buttons.
			 */
			s_lprotdd = (ROTATEDLGDATA*)lParam;

			/* Set up the spinner control. */

			udaccel[0].nInc = 1;
			udaccel[0].nSec = 0;
			udaccel[1].nInc = 15;
			udaccel[1].nSec = 2;

			SendDlgItemMessage(hwndDlg, IDC_SPIN_ANGLE, UDM_SETRANGE32, ANGLEMIN, ANGLEMAX);
			SendDlgItemMessage(hwndDlg, IDC_SPIN_ANGLE, UDM_SETACCEL, sizeof(udaccel)/sizeof(UDACCEL), (LPARAM)udaccel);


			/* Disable the thing/vertex radio buttons and checkboxes if
			 * necessary.
			 */
			EnableWindow(GetDlgItem(hwndDlg, IDC_RADIO_FIRSTVERTEX), s_lprotdd->bVerticesSelected);
			EnableWindow(GetDlgItem(hwndDlg, IDC_RADIO_FIRSTTHING), s_lprotdd->bThingsSelected);
			EnableWindow(GetDlgItem(hwndDlg, IDC_CHECK_THINGANGLE), s_lprotdd->bThingsSelected);

			/* Assume bounding box and thing-angle alteration. */
			CheckRadioButton(hwndDlg, IDC_RADIO_CENTREBB, IDC_RADIO_CENTREBB, IDC_RADIO_CENTREBB);
			CheckDlgButton(hwndDlg, IDC_CHECK_THINGANGLE, BST_CHECKED);
		}

		/* Let the system set the focus. */
		return TRUE;

	case WM_COMMAND:
		switch(LOWORD(wParam))
		{
		case IDOK:
			/* Set the structure fields. */
			s_lprotdd->iAngle = GetDlgItemInt(hwndDlg, IDC_EDIT_ANGLE, NULL, TRUE);
			s_lprotdd->bSnap = IsDlgButtonChecked(hwndDlg, IDC_CHECK_SNAP) != BST_UNCHECKED;
			s_lprotdd->bAlterThingAngles = IsDlgButtonChecked(hwndDlg, IDC_CHECK_THINGANGLE) != BST_UNCHECKED;

			if(IsDlgButtonChecked(hwndDlg, IDC_RADIO_CENTREBB) != BST_UNCHECKED)
				s_lprotdd->cCentreType = CT_BOUNDINGBOX;
			else if(IsDlgButtonChecked(hwndDlg, IDC_RADIO_FIRSTVERTEX) != BST_UNCHECKED)
				s_lprotdd->cCentreType = CT_VERTEX;
			else
				s_lprotdd->cCentreType = CT_THING;

			/* Signal that we OKed. */
			EndDialog(hwndDlg, TRUE);

			return TRUE;

		case IDCANCEL:
			/* Signal that we cancelled. */
			EndDialog(hwndDlg, FALSE);

			return TRUE;

		case IDC_EDIT_ANGLE:
			if(HIWORD(wParam) == EN_KILLFOCUS) BoundEditBox((HWND)lParam, ANGLEMIN, ANGLEMAX, FALSE, FALSE);
			return TRUE;
		}

		break;
	}

	/* Didn't process message. */
	return FALSE;
}


/* ResizeDlg
 *   Displays the Resize dialogue box.
 *
 * Parameters:
 *   HWND				hwndParent	Parent for dialogue.
 *   RESIZEDLGDATA*		lprszdd		Pointer to structure in which to return the
 *									options specified by the user, and used to
 *									determine which fixed-point options should
 *									be enabled.
 *
 * Return value: BOOL
 *   FALSE if cancelled; TRUE otherwise.
 */
BOOL ResizeDlg(HWND hwndParent, RESIZEDLGDATA *lprszdd)
{
	return DialogBoxParam(g_hInstance, MAKEINTRESOURCE(IDD_RESIZE), hwndParent, ResizeDlgProc, (LPARAM)lprszdd);
}


/* ResizeDlgProc
 *   Dialogue proc for Resize dialouge box.
 *
 * Parameters:
 *   HWND	hwndDlg		Dialogue window handle.
 *   UINT	uiMsg		Message identifier.
 *   WPARAM wParam		Message-specific.
 *   LPARAM lParam		Message-specific.
 *
 * Return value: BOOL
 *   TRUE if the message was processed; FALSE otherwise.
 */
static BOOL CALLBACK ResizeDlgProc(HWND hwndDlg, UINT uiMsg, WPARAM wParam, LPARAM lParam)
{
	static RESIZEDLGDATA *s_lprszdd;

	switch(uiMsg)
	{
	case WM_INITDIALOG:
		{
			UDACCEL udaccel[2];

			/* Get the structure in which we return parameters, and from which
			 * we determine whether to enable some radio buttons.
			 */
			s_lprszdd = (RESIZEDLGDATA*)lParam;

			/* Set up the spinner control. */

			udaccel[0].nInc = 1;
			udaccel[0].nSec = 0;
			udaccel[1].nInc = 25;
			udaccel[1].nSec = 2;

			SendDlgItemMessage(hwndDlg, IDC_SPIN_FACTOR, UDM_SETRANGE32, RESIZEFACTORMIN, RESIZEFACTORMAX);
			SendDlgItemMessage(hwndDlg, IDC_SPIN_FACTOR, UDM_SETACCEL, sizeof(udaccel)/sizeof(UDACCEL), (LPARAM)udaccel);


			/* Disable the thing/vertex radio buttons and checkboxes if
			 * necessary.
			 */
			EnableWindow(GetDlgItem(hwndDlg, IDC_RADIO_FIRSTVERTEX), s_lprszdd->bVerticesSelected);
			EnableWindow(GetDlgItem(hwndDlg, IDC_RADIO_FIRSTTHING), s_lprszdd->bThingsSelected);

			/* Assume bounding box. */
			CheckRadioButton(hwndDlg, IDC_RADIO_CENTREBB, IDC_RADIO_FIRSTTHING, IDC_RADIO_CENTREBB);

			/* Set initial factor. */
			SetDlgItemInt(hwndDlg, IDC_EDIT_FACTOR, RESIZEINIT, FALSE);
		}

		/* Let the system set the focus. */
		return TRUE;

	case WM_COMMAND:
		switch(LOWORD(wParam))
		{
		case IDOK:
			/* Set the structure fields. */
			s_lprszdd->unFactor = GetDlgItemInt(hwndDlg, IDC_EDIT_FACTOR, NULL, FALSE);
			s_lprszdd->bSnap = IsDlgButtonChecked(hwndDlg, IDC_CHECK_SNAP) != BST_UNCHECKED;

			if(IsDlgButtonChecked(hwndDlg, IDC_RADIO_CENTREBB) != BST_UNCHECKED)
				s_lprszdd->cCentreType = CT_BOUNDINGBOX;
			else if(IsDlgButtonChecked(hwndDlg, IDC_RADIO_FIRSTVERTEX) != BST_UNCHECKED)
				s_lprszdd->cCentreType = CT_VERTEX;
			else
				s_lprszdd->cCentreType = CT_THING;

			/* Signal that we OKed. */
			EndDialog(hwndDlg, TRUE);

			return TRUE;

		case IDCANCEL:
			/* Signal that we cancelled. */
			EndDialog(hwndDlg, FALSE);

			return TRUE;

		case IDC_EDIT_FACTOR:
			if(HIWORD(wParam) == EN_KILLFOCUS) BoundEditBox((HWND)lParam, RESIZEFACTORMIN, RESIZEFACTORMAX, FALSE, FALSE);
			return TRUE;
		}

		break;
	}

	/* Didn't process message. */
	return FALSE;
}


/* SelectPresetTypes
 *   Displays the Resize dialogue box.
 *
 * Parameters:
 *   HWND	hwndParent			Parent for dialogue.
 *   BYTE	byObjectTypeFlags	Flags specifying which checkboxes to enable. See
 *								ENUM_PRESET_OBJECT_FLAGS for values.
 *
 * Return value: BYTE
 *   Flags specifying which types were selected, or zero if cancelled.
 */
BYTE SelectPresetTypes(HWND hwndParent, BYTE byObjectFlags)
{
	return (BYTE)DialogBoxParam(g_hInstance, MAKEINTRESOURCE(IDD_PASTEMULTIPROP), hwndParent, SelectPresetTypesDlgProc, (LPARAM)byObjectFlags);
}

/* SelectPresetTypesDlgProc
 *   Dialogue proc for "Apply Properties" dialouge box.
 *
 * Parameters:
 *   HWND	hwndDlg		Dialogue window handle.
 *   UINT	uiMsg		Message identifier.
 *   WPARAM wParam		Message-specific.
 *   LPARAM lParam		Message-specific.
 *
 * Return value: BOOL
 *   TRUE if the message was processed; FALSE otherwise.
 */
static BOOL CALLBACK SelectPresetTypesDlgProc(HWND hwndDlg, UINT uiMsg, WPARAM wParam, LPARAM lParam)
{
	switch(uiMsg)
	{
	case WM_INITDIALOG:
		{
			/* Disable non-applicable checkboxes. */
			if(!(lParam & POF_SECTOR)) EnableWindow(GetDlgItem(hwndDlg, IDC_CHECK_SECTORS), FALSE);
			else CheckDlgButton(hwndDlg, IDC_CHECK_SECTORS, BST_CHECKED);

			if(!(lParam & POF_LINE)) EnableWindow(GetDlgItem(hwndDlg, IDC_CHECK_LINES), FALSE);
			else CheckDlgButton(hwndDlg, IDC_CHECK_LINES, BST_CHECKED);

			if(!(lParam & POF_THING)) EnableWindow(GetDlgItem(hwndDlg, IDC_CHECK_THINGS), FALSE);
			else CheckDlgButton(hwndDlg, IDC_CHECK_THINGS, BST_CHECKED);			
		}

		/* Let the system set the focus. */
		return TRUE;

	case WM_COMMAND:
		switch(LOWORD(wParam))
		{
		case IDOK:
			{
				BYTE byObjectFlags = 0;

				/* Set flags corresponding to selected checkboxes. */
				if(IsDlgButtonChecked(hwndDlg, IDC_CHECK_SECTORS) == BST_CHECKED) byObjectFlags |= POF_SECTOR;
				if(IsDlgButtonChecked(hwndDlg, IDC_CHECK_LINES) == BST_CHECKED) byObjectFlags |= POF_LINE;
				if(IsDlgButtonChecked(hwndDlg, IDC_CHECK_THINGS) == BST_CHECKED) byObjectFlags |= POF_THING;

				/* Signal that we OKed. */
				EndDialog(hwndDlg, byObjectFlags);
			}

			return TRUE;

		case IDCANCEL:
			/* Signal that we cancelled - equivalent to clearing all the checkboxes. */
			EndDialog(hwndDlg, 0);
			return TRUE;
		}

		break;
	}

	/* Didn't process message. */
	return FALSE;
}


/* AlignDlg
 *   Displays the "Align Textures" dialogue box.
 *
 * Parameters:
 *   HWND				hwndParent	Parent for dialogue.
 *   ALIGNDLGDATA*		lpaligndd	Pointer to structure in which to return the
 *									options specified by the user, and which
 *									specifies some controls' initial states.
 *
 * Return value: BOOL
 *   FALSE if cancelled; TRUE otherwise.
 */
BOOL AlignDlg(HWND hwndParent, ALIGNDLGDATA *lpaligndd)
{
	return DialogBoxParam(g_hInstance, MAKEINTRESOURCE(IDD_ALIGN), hwndParent, AlignDlgProc, (LPARAM)lpaligndd);
}


/* AlignDlgProc
 *   Dialogue proc for "Align Textures" dialouge box.
 *
 * Parameters:
 *   HWND	hwndDlg		Dialogue window handle.
 *   UINT	uiMsg		Message identifier.
 *   WPARAM wParam		Message-specific.
 *   LPARAM lParam		Message-specific.
 *
 * Return value: BOOL
 *   TRUE if the message was processed; FALSE otherwise.
 */
static BOOL CALLBACK AlignDlgProc(HWND hwndDlg, UINT uiMsg, WPARAM wParam, LPARAM lParam)
{
	static ALIGNDLGDATA *s_lpaligndd;

	switch(uiMsg)
	{
	case WM_INITDIALOG:
		{
			/* Get the structure in which we return parameters, and from which
			 * we determine whether to enable and select some radio buttons and
			 * checkboxes.
			 */
			s_lpaligndd = (ALIGNDLGDATA*)lParam;

			/* Disable the thing/vertex radio buttons and checkboxes if
			 * necessary.
			 */
			EnableWindow(GetDlgItem(hwndDlg, IDC_RADIO_THISLINE), s_lpaligndd->bFromThisLine);
			EnableWindow(GetDlgItem(hwndDlg, IDC_CHECK_INSELECTION), s_lpaligndd->bInSelection);

			/* Assume aligning from any linedefs. */
			CheckRadioButton(hwndDlg, IDC_RADIO_ANYLINE, IDC_RADIO_THISLINE, IDC_RADIO_ANYLINE);

			/* All the texture checkboxes are selected by default. */
			CheckDlgButton(hwndDlg, IDC_CHECK_FRONTUPPER, BST_CHECKED);
			CheckDlgButton(hwndDlg, IDC_CHECK_FRONTMIDDLE, BST_CHECKED);
			CheckDlgButton(hwndDlg, IDC_CHECK_FRONTLOWER, BST_CHECKED);
			CheckDlgButton(hwndDlg, IDC_CHECK_BACKUPPER, BST_CHECKED);
			CheckDlgButton(hwndDlg, IDC_CHECK_BACKMIDDLE, BST_CHECKED);
			CheckDlgButton(hwndDlg, IDC_CHECK_BACKLOWER, BST_CHECKED);			

			/* If we have a selection, assume we search only within it. */
			if(s_lpaligndd->bInSelection)
				CheckDlgButton(hwndDlg, IDC_CHECK_INSELECTION, BST_CHECKED);
		}

		/* Let the system set the focus. */
		return TRUE;

	case WM_COMMAND:
		switch(LOWORD(wParam))
		{
		case IDOK:
			/* Set the structure fields. */
			s_lpaligndd->bInSelection = IsDlgButtonChecked(hwndDlg, IDC_CHECK_INSELECTION) != BST_UNCHECKED;
			s_lpaligndd->bFromThisLine = IsDlgButtonChecked(hwndDlg, IDC_RADIO_THISLINE) != BST_UNCHECKED;

			/* Texture flags. */
			s_lpaligndd->byTexFlags = 0;

			if(IsDlgButtonChecked(hwndDlg, IDC_CHECK_FRONTUPPER) != BST_UNCHECKED)
				s_lpaligndd->byTexFlags |= LDTF_FRONTUPPER;

			if(IsDlgButtonChecked(hwndDlg, IDC_CHECK_FRONTMIDDLE) != BST_UNCHECKED)
				s_lpaligndd->byTexFlags |= LDTF_FRONTMIDDLE;

			if(IsDlgButtonChecked(hwndDlg, IDC_CHECK_FRONTLOWER) != BST_UNCHECKED)
				s_lpaligndd->byTexFlags |= LDTF_FRONTLOWER;

			if(IsDlgButtonChecked(hwndDlg, IDC_CHECK_BACKUPPER) != BST_UNCHECKED)
				s_lpaligndd->byTexFlags |= LDTF_BACKUPPER;

			if(IsDlgButtonChecked(hwndDlg, IDC_CHECK_BACKMIDDLE) != BST_UNCHECKED)
				s_lpaligndd->byTexFlags |= LDTF_BACKMIDDLE;

			if(IsDlgButtonChecked(hwndDlg, IDC_CHECK_BACKLOWER) != BST_UNCHECKED)
				s_lpaligndd->byTexFlags |= LDTF_BACKLOWER;

			/* Signal that we OKed. */
			EndDialog(hwndDlg, TRUE);

			return TRUE;

		case IDCANCEL:
			/* Signal that we cancelled. */
			EndDialog(hwndDlg, FALSE);

			return TRUE;
		}

		break;
	}

	/* Didn't process message. */
	return FALSE;
}


/* MissingTexDlg
 *   Displays the "Fix Missing Textures" dialogue box.
 *
 * Parameters:
 *   HWND				hwndParent	Parent for dialogue.
 *   MISSTEXDLGDATA*	lpmtdd		Pointer to structure in which to return the
 *									options specified by the user, and which
 *									specifies some controls' initial states.
 *
 * Return value: BOOL
 *   FALSE if cancelled; TRUE otherwise.
 */
BOOL MissingTexDlg(HWND hwndParent, MISSTEXDLGDATA *lpmtdd)
{
	return DialogBoxParam(g_hInstance, MAKEINTRESOURCE(IDD_MISSTEX), hwndParent, MissingTexProc, (LPARAM)lpmtdd);
}


/* MissingTexProc
 *   Dialogue proc for "Fix Missing Textures" dialouge box.
 *
 * Parameters:
 *   HWND	hwndDlg		Dialogue window handle.
 *   UINT	uiMsg		Message identifier.
 *   WPARAM wParam		Message-specific.
 *   LPARAM lParam		Message-specific.
 *
 * Return value: BOOL
 *   TRUE if the message was processed; FALSE otherwise.
 */
static BOOL CALLBACK MissingTexProc(HWND hwndDlg, UINT uiMsg, WPARAM wParam, LPARAM lParam)
{
	static MISSTEXDLGDATA *s_lpmtdd;

	switch(uiMsg)
	{
	case WM_INITDIALOG:
		{
			/* Get the structure in which we return parameters, and from which
			 * we determine whether to enable and select some radio buttons and
			 * checkboxes.
			 */
			s_lpmtdd = (MISSTEXDLGDATA*)lParam;

			/* Disable the thing/vertex radio buttons and checkboxes if
			 * necessary.
			 */
			EnableWindow(GetDlgItem(hwndDlg, IDC_CHECK_INSELECTION), s_lpmtdd->bInSelection);

			/* If we have a selection, assume we search only within it. */
			if(s_lpmtdd->bInSelection)
				CheckDlgButton(hwndDlg, IDC_CHECK_INSELECTION, BST_CHECKED);

			/* Get the default textures. */
			SetDlgItemText(hwndDlg, IDC_EDIT_UPPER, s_lpmtdd->szUpper);		
			SetDlgItemText(hwndDlg, IDC_EDIT_MIDDLE, s_lpmtdd->szMiddle);
			SetDlgItemText(hwndDlg, IDC_EDIT_LOWER, s_lpmtdd->szLower);
		}

		/* Let the system set the focus. */
		return TRUE;

	case WM_COMMAND:
		switch(LOWORD(wParam))
		{
		case IDOK:
			/* Set the structure fields. */
			s_lpmtdd->bInSelection = IsDlgButtonChecked(hwndDlg, IDC_CHECK_INSELECTION) != BST_UNCHECKED;

			/* Texture names. */
			GetDlgItemText(hwndDlg, IDC_EDIT_UPPER, s_lpmtdd->szUpper, sizeof(s_lpmtdd->szUpper) / sizeof(TCHAR));
			GetDlgItemText(hwndDlg, IDC_EDIT_MIDDLE, s_lpmtdd->szMiddle, sizeof(s_lpmtdd->szMiddle) / sizeof(TCHAR));
			GetDlgItemText(hwndDlg, IDC_EDIT_LOWER, s_lpmtdd->szLower, sizeof(s_lpmtdd->szLower) / sizeof(TCHAR));

			/* Signal that we OKed. */
			EndDialog(hwndDlg, TRUE);

			return TRUE;

		case IDCANCEL:
			/* Signal that we cancelled. */
			EndDialog(hwndDlg, FALSE);

			return TRUE;

		case IDC_BUTTON_UPPER:
		case IDC_BUTTON_MIDDLE:
		case IDC_BUTTON_LOWER:
			{
				int iEditID;
				TCHAR szTexname[TEXNAME_BUFFER_LENGTH];

				if(LOWORD(wParam) == IDC_BUTTON_UPPER) iEditID = IDC_EDIT_UPPER;
				else if(LOWORD(wParam) == IDC_BUTTON_MIDDLE) iEditID = IDC_EDIT_MIDDLE;
				else iEditID = IDC_EDIT_LOWER;

				/* Get the initial texture name. */
				GetDlgItemText(hwndDlg, iEditID, szTexname, sizeof(szTexname)/sizeof(TCHAR));

				/* Show the dialogue, and check whether we OKed or cancelled. */
				if(SelectTexture(hwndDlg, s_lpmtdd->hwndMap, TF_TEXTURE, s_lpmtdd->lphimlCache, s_lpmtdd->lptnlAllTextures, NULL, szTexname))
					SetDlgItemText(hwndDlg, iEditID, szTexname);
			}

			return TRUE;
		}

		break;
	}

	/* Didn't process message. */
	return FALSE;
}
