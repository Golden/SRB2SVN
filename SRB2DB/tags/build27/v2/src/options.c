#include <windows.h>

#include "general.h"
#include "config.h"
#include "options.h"
#include "keyboard.h"
#include "editing.h"
#include "mapconfig.h"
#include "testing.h"

#include "../res/resource.h"

/* Macros. */
#define OPTFILE_SIGNATURE "SRB2 Builder Configuration"
#define OPTFILENAME TEXT("builder.cfg")

/* Default values that are used multiple times. */
#define OPT_DEFAULT_DEFTEXTURE	"GFZROCK"


/* Globals. */
CONFIG *g_lpcfgMain = NULL;
int g_iShortcutCodes[SCK_MAX];
RENDEREROPTIONS g_rendopts;


/* Static prototypes. */
static void LoadShortcutKeys(void);
static void SetShortcutKeys(void);
static void LoadRendererOptions(void);
static void SetRendererOptions(void);
static void SetMissingOptions(void);
static void SetDefaultPalette(void);
static void SetDefaultTextures(void);
static void SetDefaultSector(void);


/* LoadMainConfigurationFile
 *   Loads the main settings file and reads the settings into their appropriate
 *   places.
 *
 * Parameters:
 *   None.
 *
 * Return value: BOOL
 *   TRUE on success; FALSE on failure.
 */
BOOL LoadMainConfigurationFile(void)
{
	char *szType;
	int iLen;

	g_lpcfgMain = ConfigLoad(OPTDIR OPTFILENAME);
	if(!g_lpcfgMain)
	{
		/* Make a new config file. */
		g_lpcfgMain = ConfigCreate();
		ConfigSetStringA(g_lpcfgMain, "type", OPTFILE_SIGNATURE);

		/* Defaults are set later. */
	}
	else
	{
		iLen = ConfigGetStringLength(g_lpcfgMain, "type");

		if(iLen <= 0)
		{
			UnloadMainConfigurationFile();
			return FALSE;
		}

		szType = ProcHeapAlloc((iLen + 1) * sizeof(char));
		ConfigGetStringA(g_lpcfgMain, "type", szType, iLen + 1);

		if(lstrcmpiA(szType, OPTFILE_SIGNATURE))
		{
			UnloadMainConfigurationFile();
			ProcHeapFree(szType);
			return FALSE;
		}

		ProcHeapFree(szType);
	}

	/* Set any missing options. */
	SetMissingOptions();

	/* Copies from tree into other storage, for efficiency. */
	LoadShortcutKeys();
	LoadRendererOptions();

	return TRUE;
}


/* UnloadMainConfigurationFile
 *   Cleans up the settings structures.
 *
 * Parameters:
 *   None.
 *
 * Return value: None.
 */
void UnloadMainConfigurationFile(void)
{
	if(g_lpcfgMain)
	{
		/* Copy settings back into the tree. */
		SetShortcutKeys();
		SetRendererOptions();

		/* Save. */
		if(ConfigWrite(g_lpcfgMain, OPTDIR OPTFILENAME))
			MessageBoxFromStringTable(NULL, IDS_ERROR_WRITECONFIG, MB_ICONERROR);

		ConfigDestroy(g_lpcfgMain);
	}
}


/* LoadShortcutKeys, SetShortcutKeys
 *   Copies shortcut key settings between the tree and the array.
 *
 * Parameters:
 *   None.
 *
 * Return value: None.
 */
static void LoadShortcutKeys(void)
{
	CONFIG *lpcfgShortcuts = ConfigGetSubsection(g_lpcfgMain, OPT_SHORTCUTS);

	g_iShortcutCodes[SCK_EDITQUICKMOVE] = ConfigGetInteger(lpcfgShortcuts, "editquickmove");
	g_iShortcutCodes[SCK_ZOOMIN] = ConfigGetInteger(lpcfgShortcuts, "zoomin");
	g_iShortcutCodes[SCK_ZOOMOUT] = ConfigGetInteger(lpcfgShortcuts, "zoomout");
	g_iShortcutCodes[SCK_CENTREVIEW] = ConfigGetInteger(lpcfgShortcuts, "editcenterview");
	g_iShortcutCodes[SCK_EDITMOVE] = ConfigGetInteger(lpcfgShortcuts, "editmove");
	g_iShortcutCodes[SCK_EDITANY] = ConfigGetInteger(lpcfgShortcuts, "editany");
	g_iShortcutCodes[SCK_EDITLINES] = ConfigGetInteger(lpcfgShortcuts, "editlines");
	g_iShortcutCodes[SCK_EDITSECTORS] = ConfigGetInteger(lpcfgShortcuts, "editsectors");
	g_iShortcutCodes[SCK_EDITVERTICES] = ConfigGetInteger(lpcfgShortcuts, "editvertices");
	g_iShortcutCodes[SCK_EDITTHINGS] = ConfigGetInteger(lpcfgShortcuts, "editthings");
	g_iShortcutCodes[SCK_EDIT3D] = ConfigGetInteger(lpcfgShortcuts, "edit3d");
	g_iShortcutCodes[SCK_EDITSNAPTOGRID] = ConfigGetInteger(lpcfgShortcuts, "togglesnap");
	g_iShortcutCodes[SCK_FLIPLINEDEFS] = ConfigGetInteger(lpcfgShortcuts, "fliplinedefs");
	g_iShortcutCodes[SCK_FLIPSIDEDEFS] = ConfigGetInteger(lpcfgShortcuts, "flipsidedefs");
	g_iShortcutCodes[SCK_SPLITLINEDEFS] = ConfigGetInteger(lpcfgShortcuts, "splitlinedefs");
	g_iShortcutCodes[SCK_JOINSECTORS] = ConfigGetInteger(lpcfgShortcuts, "joinsector");
	g_iShortcutCodes[SCK_MERGESECTORS] = ConfigGetInteger(lpcfgShortcuts, "mergesector");
	g_iShortcutCodes[SCK_UNDO] = ConfigGetInteger(lpcfgShortcuts, "editundo");
	g_iShortcutCodes[SCK_REDO] = ConfigGetInteger(lpcfgShortcuts, "editredo");
	g_iShortcutCodes[SCK_FLIPHORIZ] = ConfigGetInteger(lpcfgShortcuts, "editfliph");
	g_iShortcutCodes[SCK_FLIPVERT] = ConfigGetInteger(lpcfgShortcuts, "editflipv");
	g_iShortcutCodes[SCK_COPY] = ConfigGetInteger(lpcfgShortcuts, "editcopy");
	g_iShortcutCodes[SCK_PASTE] = ConfigGetInteger(lpcfgShortcuts, "editpaste");
	g_iShortcutCodes[SCK_SAVEAS] = ConfigGetInteger(lpcfgShortcuts, "filesaveas");
	g_iShortcutCodes[SCK_INCFLOOR] = ConfigGetInteger(lpcfgShortcuts, "incfloor");
	g_iShortcutCodes[SCK_DECFLOOR] = ConfigGetInteger(lpcfgShortcuts, "decfloor");
	g_iShortcutCodes[SCK_INCCEIL] = ConfigGetInteger(lpcfgShortcuts, "incceil");
	g_iShortcutCodes[SCK_DECCEIL] = ConfigGetInteger(lpcfgShortcuts, "decceil");
	g_iShortcutCodes[SCK_INCLIGHT] = ConfigGetInteger(lpcfgShortcuts, "inclight");
	g_iShortcutCodes[SCK_DECLIGHT] = ConfigGetInteger(lpcfgShortcuts, "declight");
	g_iShortcutCodes[SCK_INCTHINGZ] = ConfigGetInteger(lpcfgShortcuts, "incthingz");
	g_iShortcutCodes[SCK_DECTHINGZ] = ConfigGetInteger(lpcfgShortcuts, "decthingz");
	g_iShortcutCodes[SCK_SNAPSELECTION] = ConfigGetInteger(lpcfgShortcuts, "snapselection");
	g_iShortcutCodes[SCK_GRADIENTFLOORS] = ConfigGetInteger(lpcfgShortcuts, "gradientfloors");
	g_iShortcutCodes[SCK_GRADIENTCEILINGS] = ConfigGetInteger(lpcfgShortcuts, "gradientceilings");
	g_iShortcutCodes[SCK_GRADIENTBRIGHTNESS] = ConfigGetInteger(lpcfgShortcuts, "gradientbrightness");
	g_iShortcutCodes[SCK_DRAWSECTOR] = ConfigGetInteger(lpcfgShortcuts, "drawsector");
	g_iShortcutCodes[SCK_EDITCUT] = ConfigGetInteger(lpcfgShortcuts, "editcut");
	g_iShortcutCodes[SCK_EDITDELETE] = ConfigGetInteger(lpcfgShortcuts, "editdelete");
	g_iShortcutCodes[SCK_CANCEL] = ConfigGetInteger(lpcfgShortcuts, "cancel");
	g_iShortcutCodes[SCK_GRADIENTTHINGZ] = ConfigGetInteger(lpcfgShortcuts, "gradientthingz");
	g_iShortcutCodes[SCK_ROTATE] = ConfigGetInteger(lpcfgShortcuts, "editrotate");
	g_iShortcutCodes[SCK_RESIZE] = ConfigGetInteger(lpcfgShortcuts, "editresize");
	g_iShortcutCodes[SCK_THINGROTACW] = ConfigGetInteger(lpcfgShortcuts, "thingrotateacw");
	g_iShortcutCodes[SCK_THINGROTCW] = ConfigGetInteger(lpcfgShortcuts, "thingrotatecw");
	g_iShortcutCodes[SCK_SELECTALL] = ConfigGetInteger(lpcfgShortcuts, "editselectall");
	g_iShortcutCodes[SCK_SELECTNONE] = ConfigGetInteger(lpcfgShortcuts, "editselectnone");
	g_iShortcutCodes[SCK_INVERTSELECTION] = ConfigGetInteger(lpcfgShortcuts, "editselectinvert");
	g_iShortcutCodes[SCK_COPYPROPS] = ConfigGetInteger(lpcfgShortcuts, "copyprops");
	g_iShortcutCodes[SCK_PASTEPROPS] = ConfigGetInteger(lpcfgShortcuts, "pasteprops");
	g_iShortcutCodes[SCK_GRIDINC] = ConfigGetInteger(lpcfgShortcuts, "gridinc");
	g_iShortcutCodes[SCK_GRIDDEC] = ConfigGetInteger(lpcfgShortcuts, "griddec");
	g_iShortcutCodes[SCK_FIND] = ConfigGetInteger(lpcfgShortcuts, "editfind");
	g_iShortcutCodes[SCK_REPLACE] = ConfigGetInteger(lpcfgShortcuts, "editreplace");
	g_iShortcutCodes[SCK_NEW] = ConfigGetInteger(lpcfgShortcuts, "filenew");
	g_iShortcutCodes[SCK_OPEN] = ConfigGetInteger(lpcfgShortcuts, "fileopen");
	g_iShortcutCodes[SCK_SAVE] = ConfigGetInteger(lpcfgShortcuts, "filesave");
	g_iShortcutCodes[SCK_MAPOPTIONS] = ConfigGetInteger(lpcfgShortcuts, "editoptions");
	g_iShortcutCodes[SCK_MOUSEROTATE] = ConfigGetInteger(lpcfgShortcuts, "thingsmouserotate");
	g_iShortcutCodes[SCK_STITCHVERTICES] = ConfigGetInteger(lpcfgShortcuts, "stitchvertices");
	g_iShortcutCodes[SCK_SCROLL_UP] = ConfigGetInteger(lpcfgShortcuts, "scrollup");
	g_iShortcutCodes[SCK_SCROLL_DOWN] = ConfigGetInteger(lpcfgShortcuts, "scrolldown");
	g_iShortcutCodes[SCK_SCROLL_LEFT] = ConfigGetInteger(lpcfgShortcuts, "scrollleft");
	g_iShortcutCodes[SCK_SCROLL_RIGHT] = ConfigGetInteger(lpcfgShortcuts, "scrollright");
	g_iShortcutCodes[SCK_TEST] = ConfigGetInteger(lpcfgShortcuts, "filetest");
	g_iShortcutCodes[SCK_QUICKTEST] = ConfigGetInteger(lpcfgShortcuts, "quicktest");
	g_iShortcutCodes[SCK_IDENTSECTORS] = ConfigGetInteger(lpcfgShortcuts, "sectorsident");
	/* _SCK_ */

	/* Create the accelerator table. */
	UpdateAcceleratorFromOptions();
}

static void SetShortcutKeys(void)
{
	CONFIG *lpcfgShortcuts = ConfigGetSubsection(g_lpcfgMain, OPT_SHORTCUTS);

	ConfigSetInteger(lpcfgShortcuts, "editquickmove", g_iShortcutCodes[SCK_EDITQUICKMOVE]);
	ConfigSetInteger(lpcfgShortcuts, "zoomin", g_iShortcutCodes[SCK_ZOOMIN]);
	ConfigSetInteger(lpcfgShortcuts, "zoomout", g_iShortcutCodes[SCK_ZOOMOUT]);
	ConfigSetInteger(lpcfgShortcuts, "editmove", g_iShortcutCodes[SCK_EDITMOVE]);
	ConfigSetInteger(lpcfgShortcuts, "editlines", g_iShortcutCodes[SCK_EDITLINES]);
	ConfigSetInteger(lpcfgShortcuts, "editsectors", g_iShortcutCodes[SCK_EDITSECTORS]);
	ConfigSetInteger(lpcfgShortcuts, "editvertices", g_iShortcutCodes[SCK_EDITVERTICES]);
	ConfigSetInteger(lpcfgShortcuts, "editthings", g_iShortcutCodes[SCK_EDITTHINGS]);
	ConfigSetInteger(lpcfgShortcuts, "edit3d", g_iShortcutCodes[SCK_EDIT3D]);
	ConfigSetInteger(lpcfgShortcuts, "togglesnap", g_iShortcutCodes[SCK_EDITSNAPTOGRID]);
	ConfigSetInteger(lpcfgShortcuts, "fliplinedefs", g_iShortcutCodes[SCK_FLIPLINEDEFS]);
	ConfigSetInteger(lpcfgShortcuts, "flipsidedefs", g_iShortcutCodes[SCK_FLIPSIDEDEFS]);
	ConfigSetInteger(lpcfgShortcuts, "splitlinedefs", g_iShortcutCodes[SCK_SPLITLINEDEFS]);
	ConfigSetInteger(lpcfgShortcuts, "joinsector", g_iShortcutCodes[SCK_JOINSECTORS]);
	ConfigSetInteger(lpcfgShortcuts, "mergesector", g_iShortcutCodes[SCK_MERGESECTORS]);
	ConfigSetInteger(lpcfgShortcuts, "editundo", g_iShortcutCodes[SCK_UNDO]);
	ConfigSetInteger(lpcfgShortcuts, "editredo", g_iShortcutCodes[SCK_REDO]);
	ConfigSetInteger(lpcfgShortcuts, "editfliph", g_iShortcutCodes[SCK_FLIPHORIZ]);
	ConfigSetInteger(lpcfgShortcuts, "editflipv", g_iShortcutCodes[SCK_FLIPVERT]);
	ConfigSetInteger(lpcfgShortcuts, "editcopy", g_iShortcutCodes[SCK_COPY]);
	ConfigSetInteger(lpcfgShortcuts, "editpaste", g_iShortcutCodes[SCK_PASTE]);
	ConfigSetInteger(lpcfgShortcuts, "filesaveas", g_iShortcutCodes[SCK_SAVEAS]);
	ConfigSetInteger(lpcfgShortcuts, "incfloor", g_iShortcutCodes[SCK_INCFLOOR]);
	ConfigSetInteger(lpcfgShortcuts, "decfloor", g_iShortcutCodes[SCK_DECFLOOR]);
	ConfigSetInteger(lpcfgShortcuts, "incceil", g_iShortcutCodes[SCK_INCCEIL]);
	ConfigSetInteger(lpcfgShortcuts, "decceil", g_iShortcutCodes[SCK_DECCEIL]);
	ConfigSetInteger(lpcfgShortcuts, "inclight", g_iShortcutCodes[SCK_INCLIGHT]);
	ConfigSetInteger(lpcfgShortcuts, "declight", g_iShortcutCodes[SCK_DECLIGHT]);
	ConfigSetInteger(lpcfgShortcuts, "incthingz", g_iShortcutCodes[SCK_INCTHINGZ]);
	ConfigSetInteger(lpcfgShortcuts, "decthingz", g_iShortcutCodes[SCK_DECTHINGZ]);
	ConfigSetInteger(lpcfgShortcuts, "snapselection", g_iShortcutCodes[SCK_SNAPSELECTION]);
	ConfigSetInteger(lpcfgShortcuts, "gradientfloors", g_iShortcutCodes[SCK_GRADIENTFLOORS]);
	ConfigSetInteger(lpcfgShortcuts, "gradientceilings", g_iShortcutCodes[SCK_GRADIENTCEILINGS]);
	ConfigSetInteger(lpcfgShortcuts, "gradientbrightness", g_iShortcutCodes[SCK_GRADIENTBRIGHTNESS]);
	ConfigSetInteger(lpcfgShortcuts, "drawsector", g_iShortcutCodes[SCK_DRAWSECTOR]);
	ConfigSetInteger(lpcfgShortcuts, "editcut", g_iShortcutCodes[SCK_EDITCUT]);
	ConfigSetInteger(lpcfgShortcuts, "editdelete", g_iShortcutCodes[SCK_EDITDELETE]);
	ConfigSetInteger(lpcfgShortcuts, "cancel", g_iShortcutCodes[SCK_CANCEL]);
	ConfigSetInteger(lpcfgShortcuts, "gradientthingz", g_iShortcutCodes[SCK_GRADIENTTHINGZ]);
	ConfigSetInteger(lpcfgShortcuts, "editrotate", g_iShortcutCodes[SCK_ROTATE]);
	ConfigSetInteger(lpcfgShortcuts, "editresize", g_iShortcutCodes[SCK_RESIZE]);
	ConfigSetInteger(lpcfgShortcuts, "thingrotateacw", g_iShortcutCodes[SCK_THINGROTACW]);
	ConfigSetInteger(lpcfgShortcuts, "thingrotatecw", g_iShortcutCodes[SCK_THINGROTCW]);
	ConfigSetInteger(lpcfgShortcuts, "editselectall", g_iShortcutCodes[SCK_SELECTALL]);
	ConfigSetInteger(lpcfgShortcuts, "editselectnone", g_iShortcutCodes[SCK_SELECTNONE]);
	ConfigSetInteger(lpcfgShortcuts, "editselectinvert", g_iShortcutCodes[SCK_INVERTSELECTION]);
	ConfigSetInteger(lpcfgShortcuts, "copyprops", g_iShortcutCodes[SCK_COPYPROPS]);
	ConfigSetInteger(lpcfgShortcuts, "pasteprops", g_iShortcutCodes[SCK_PASTEPROPS]);
	ConfigSetInteger(lpcfgShortcuts, "gridinc", g_iShortcutCodes[SCK_GRIDINC]);
	ConfigSetInteger(lpcfgShortcuts, "griddec", g_iShortcutCodes[SCK_GRIDDEC]);
	ConfigSetInteger(lpcfgShortcuts, "editfind", g_iShortcutCodes[SCK_FIND]);
	ConfigSetInteger(lpcfgShortcuts, "editreplace", g_iShortcutCodes[SCK_REPLACE]);
	ConfigSetInteger(lpcfgShortcuts, "filenew", g_iShortcutCodes[SCK_NEW]);
	ConfigSetInteger(lpcfgShortcuts, "fileopen", g_iShortcutCodes[SCK_OPEN]);
	ConfigSetInteger(lpcfgShortcuts, "filesave", g_iShortcutCodes[SCK_SAVE]);
	ConfigSetInteger(lpcfgShortcuts, "editoptions", g_iShortcutCodes[SCK_MAPOPTIONS]);
	ConfigSetInteger(lpcfgShortcuts, "thingsmouserotate", g_iShortcutCodes[SCK_MOUSEROTATE]);
	ConfigSetInteger(lpcfgShortcuts, "stitchvertices", g_iShortcutCodes[SCK_STITCHVERTICES]);
	ConfigSetInteger(lpcfgShortcuts, "scrollup", g_iShortcutCodes[SCK_SCROLL_UP]);
	ConfigSetInteger(lpcfgShortcuts, "scrolldown", g_iShortcutCodes[SCK_SCROLL_DOWN]);
	ConfigSetInteger(lpcfgShortcuts, "scrollleft", g_iShortcutCodes[SCK_SCROLL_LEFT]);
	ConfigSetInteger(lpcfgShortcuts, "scrollright", g_iShortcutCodes[SCK_SCROLL_RIGHT]);
	ConfigSetInteger(lpcfgShortcuts, "filetest", g_iShortcutCodes[SCK_TEST]);
	ConfigSetInteger(lpcfgShortcuts, "quicktest", g_iShortcutCodes[SCK_QUICKTEST]);
	ConfigSetInteger(lpcfgShortcuts, "sectorsident", g_iShortcutCodes[SCK_IDENTSECTORS]);
	/* _SCK_ */
}


/* LoadRendererOptions, SetRendererOptions
 *   Copies renderer settings between the tree and the structure.
 *
 * Parameters:
 *   None.
 *
 * Return value: None.
 */
static void LoadRendererOptions(void)
{
	g_rendopts.iIndicatorSize = ConfigGetInteger(g_lpcfgMain, "indicatorsize");
	g_rendopts.iVertexSize = ConfigGetInteger(g_lpcfgMain, "vertexsize");
	g_rendopts.bVerticesInLinesMode = ConfigGetInteger(g_lpcfgMain, "linemodevertices");
	g_rendopts.bVerticesInSectorsMode = ConfigGetInteger(g_lpcfgMain, "secmodevertices");
}

static void SetRendererOptions(void)
{
	ConfigSetInteger(g_lpcfgMain, "indicatorsize", g_rendopts.iIndicatorSize);
	ConfigSetInteger(g_lpcfgMain, "vertexsize", g_rendopts.iVertexSize);
	ConfigSetInteger(g_lpcfgMain, "linemodevertices", g_rendopts.bVerticesInLinesMode);
	ConfigSetInteger(g_lpcfgMain, "secmodevertices", g_rendopts.bVerticesInSectorsMode);
}


/* MRUAdd
 *   Adds a file to the top of the MRU list.
 *
 * Parameters:
 *   LPCTSTR	szFilename	Filename to add.
 *
 * Return value: None.
 *
 * Remarks:
 *   The filename need not be the full path: it is expanded by the function. If
 *   the file is already in the list, it is moved to first place, and not
 *   duplicated.
 */
void MRUAdd(LPCTSTR szFilename)
{
	LPTSTR szFullFilename;
	LPSTR szFullFilenameA;
	UINT cchFullFilename;
	LPSTR szIndices[MRUENTRIES] = {"1", "2", "3", "4", "5", "6", "7", "8", "9", "10"};
	CONFIG *lpcfgRecent = ConfigGetSubsection(g_lpcfgMain, OPT_RECENT);
	int i, iExistingPos;

	cchFullFilename = GetFullPathName(szFilename, 0, TEXT(""), NULL);
	szFullFilename = ProcHeapAlloc(cchFullFilename * sizeof(TCHAR));
	GetFullPathName(szFilename, cchFullFilename, szFullFilename, NULL);

	/* Config files are ANSI ATM. TODO: Once this changes, update this. */
	szFullFilenameA = ProcHeapAlloc(cchFullFilename * sizeof(TCHAR));

#ifdef _UNICODE
	WideCharToMultiByte(CP_ACP, 0, szFullFilename, -1, szFullFilenameA, cchFullFilename, NULL, NULL);
#else
	lstrcpyA(szFullFilenameA, szFullFilename);
#endif

	/* Check whether we're already in the list, and at which point. Note that we
	 * don't need to check the last position, as it being there is equivalent to
	 * it not being there at all.
	 */
	for(iExistingPos = 0; iExistingPos < (int)(sizeof(szIndices) / sizeof(LPSTR) - 1) && ConfigNodeExists(lpcfgRecent, szIndices[iExistingPos]); iExistingPos++)
	{
		/* Get the filename to move. */
		UINT cchExistingFilename = ConfigGetStringLength(lpcfgRecent, szIndices[iExistingPos]) + 1;
		LPTSTR szExistingFilename = ProcHeapAlloc(cchExistingFilename * sizeof(TCHAR));
		ConfigGetString(lpcfgRecent, szIndices[iExistingPos], szExistingFilename, cchExistingFilename);

		/* Does this match with the file we're adding? */
		if(PathsReferToSameFile(szFullFilename, szExistingFilename))
		{
			/* The file was already in the MRU. */
			ProcHeapFree(szExistingFilename);
			break;
		}

		ProcHeapFree(szExistingFilename);
	}

	/* Move all the items down one. */
	for(i = iExistingPos; i > 0; i--)
	{
		/* Get the filename to move. */
		UINT cchExistingFilename = ConfigGetStringLength(lpcfgRecent, szIndices[i - 1]) + 1;
		LPSTR szExistingFilenameA = ProcHeapAlloc(cchExistingFilename);
		ConfigGetStringA(lpcfgRecent, szIndices[i - 1], szExistingFilenameA, cchExistingFilename);

		/* Move it down. */
		ConfigSetStringA(lpcfgRecent, szIndices[i], szExistingFilenameA);

		ProcHeapFree(szExistingFilenameA);
	}

	/* Add the new item. */
	ConfigSetStringA(lpcfgRecent, szIndices[0], szFullFilenameA);

	ProcHeapFree(szFullFilename);
	ProcHeapFree(szFullFilenameA);
}


/* SetMissingOptions
 *   Sets missing items in the config file to sensible defaults.
 *
 * Parameters: None.
 *
 * Return value: None.
 */
static void SetMissingOptions(void)
{
	/* Make sure we have a recent files section. */
	if(!ConfigGetSubsection(g_lpcfgMain, OPT_RECENT))
		MRUClear();

	if(!ConfigGetSubsection(g_lpcfgMain, OPT_PALETTE)) ConfigSetSubsection(g_lpcfgMain, OPT_PALETTE, ConfigCreate());
	if(!ConfigGetSubsection(g_lpcfgMain, OPT_SHORTCUTS)) ConfigSetSubsection(g_lpcfgMain, OPT_SHORTCUTS, ConfigCreate());
	if(!ConfigGetSubsection(g_lpcfgMain, OPT_GAMECONFIGS)) ConfigSetSubsection(g_lpcfgMain, OPT_GAMECONFIGS, ConfigCreate());
	if(!ConfigGetSubsection(g_lpcfgMain, OPT_DEFAULTTEX)) ConfigSetSubsection(g_lpcfgMain, OPT_DEFAULTTEX, ConfigCreate());
	if(!ConfigGetSubsection(g_lpcfgMain, OPT_DEFAULTSEC)) ConfigSetSubsection(g_lpcfgMain, OPT_DEFAULTSEC, ConfigCreate());
	if(!ConfigGetSubsection(g_lpcfgMain, OPT_WINDOWPOS)) ConfigSetSubsection(g_lpcfgMain, OPT_WINDOWPOS, ConfigCreate());

	SetDefaultPalette();
	SetDefaultShortcuts();
	SetDefaultTextures();
	SetDefaultSector();

	if(!ConfigNodeExists(g_lpcfgMain, "vertexsize")) ConfigSetInteger(g_lpcfgMain, "vertexsize", 4);
	if(!ConfigNodeExists(g_lpcfgMain, "vertexsnapdistance")) ConfigSetInteger(g_lpcfgMain, "vertexsnapdistance", 8);
	if(!ConfigNodeExists(g_lpcfgMain, "zoommouse")) ConfigSetInteger(g_lpcfgMain, "zoommouse", TRUE);
	if(!ConfigNodeExists(g_lpcfgMain, "zoomspeed")) ConfigSetInteger(g_lpcfgMain, "zoomspeed", 200);
	if(!ConfigNodeExists(g_lpcfgMain, "nothingdeselects")) ConfigSetInteger(g_lpcfgMain, "nothingdeselects", TRUE);
	if(!ConfigNodeExists(g_lpcfgMain, "defaultsnap")) ConfigSetInteger(g_lpcfgMain, "defaultsnap", SF_RECTANGLE | SF_VERTICES);
	if(!ConfigNodeExists(g_lpcfgMain, "detailsany")) ConfigSetInteger(g_lpcfgMain, "detailsany", TRUE);
	if(!ConfigNodeExists(g_lpcfgMain, "drawaxes")) ConfigSetInteger(g_lpcfgMain, "drawaxes", TRUE);
	if(!ConfigNodeExists(g_lpcfgMain, "gridshow")) ConfigSetInteger(g_lpcfgMain, "gridshow", TRUE);
	if(!ConfigNodeExists(g_lpcfgMain, "grid64show")) ConfigSetInteger(g_lpcfgMain, "grid64show", TRUE);
	if(!ConfigNodeExists(g_lpcfgMain, "indicatorsize")) ConfigSetInteger(g_lpcfgMain, "indicatorsize", 5);
	if(!ConfigNodeExists(g_lpcfgMain, "linesplitdistance")) ConfigSetInteger(g_lpcfgMain, "linesplitdistance", 2);
	if(!ConfigNodeExists(g_lpcfgMain, "defaultstitch")) ConfigSetInteger(g_lpcfgMain, "defaultstitch", TRUE);
	if(!ConfigNodeExists(g_lpcfgMain, "autostitchdistance")) ConfigSetInteger(g_lpcfgMain, "autostitchdistance", 2);
	if(!ConfigNodeExists(g_lpcfgMain, "contextmenus")) ConfigSetInteger(g_lpcfgMain, "contextmenus", TRUE);
	if(!ConfigNodeExists(g_lpcfgMain, "defaultgrid")) ConfigSetInteger(g_lpcfgMain, "defaultgrid", 32);
	if(!ConfigNodeExists(g_lpcfgMain, "scrollspeed")) ConfigSetInteger(g_lpcfgMain, "scrollspeed", 9);
}


/* SetDefaultPalette
 *   Sets default palette colours.
 *
 * Parameters: None.
 *
 * Return value: None.
 */
static void SetDefaultPalette(void)
{
	CONFIG *lpcfgPal = ConfigGetSubsection(g_lpcfgMain, OPT_PALETTE);

	if(!ConfigNodeExists(lpcfgPal, "CLR_BACKGROUND"))
		ConfigSetInteger(lpcfgPal, "CLR_BACKGROUND",		RGB(0x00, 0x00, 0x00));

	if(!ConfigNodeExists(lpcfgPal, "CLR_VERTEX"))
		ConfigSetInteger(lpcfgPal, "CLR_VERTEX",			RGB(0x00, 0x99, 0xFF));

	if(!ConfigNodeExists(lpcfgPal, "CLR_VERTEXSELECTED"))
		ConfigSetInteger(lpcfgPal, "CLR_VERTEXSELECTED",	RGB(0xFF, 0x33, 0x11));

	if(!ConfigNodeExists(lpcfgPal, "CLR_VERTEXHIGHLIGHT"))
		ConfigSetInteger(lpcfgPal, "CLR_VERTEXHIGHLIGHT",	RGB(0xFF, 0x99, 0x00));

	if(!ConfigNodeExists(lpcfgPal, "CLR_LINE"))
		ConfigSetInteger(lpcfgPal, "CLR_LINE",				RGB(0xFF, 0xFF, 0xFF));

	if(!ConfigNodeExists(lpcfgPal, "CLR_LINEDOUBLE"))
		ConfigSetInteger(lpcfgPal, "CLR_LINEDOUBLE",		RGB(0xAA, 0xAA, 0xAA));

	if(!ConfigNodeExists(lpcfgPal, "CLR_LINESPECIAL"))
		ConfigSetInteger(lpcfgPal, "CLR_LINESPECIAL",		RGB(0xAA, 0xFF, 0xAA));

	if(!ConfigNodeExists(lpcfgPal, "CLR_LINESPECIALDOUBLE"))
		ConfigSetInteger(lpcfgPal, "CLR_LINESPECIALDOUBLE",	RGB(0x80, 0xAA, 0x80));

	if(!ConfigNodeExists(lpcfgPal, "CLR_LINESELECTED"))
		ConfigSetInteger(lpcfgPal, "CLR_LINESELECTED",		RGB(0xFF, 0x00, 0x00));

	if(!ConfigNodeExists(lpcfgPal, "CLR_LINEHIGHLIGHT"))
		ConfigSetInteger(lpcfgPal, "CLR_LINEHIGHLIGHT",		RGB(0xFF, 0x99, 0x00));

	if(!ConfigNodeExists(lpcfgPal, "CLR_LINEDRAG"))
		ConfigSetInteger(lpcfgPal, "CLR_LINEDRAG",			RGB(0xAA, 0x99, 0x33));

	if(!ConfigNodeExists(lpcfgPal, "CLR_SECTORTAG"))
		ConfigSetInteger(lpcfgPal, "CLR_SECTORTAG",			RGB(0xFF, 0xFF, 0x80));

	if(!ConfigNodeExists(lpcfgPal, "CLR_THINGUNKNOWN"))
		ConfigSetInteger(lpcfgPal, "CLR_THINGUNKNOWN",		RGB(0x80, 0x80, 0x80));

	if(!ConfigNodeExists(lpcfgPal, "CLR_THINGSELECTED"))
		ConfigSetInteger(lpcfgPal, "CLR_THINGSELECTED",		RGB(0xFF, 0x00, 0x00));

	if(!ConfigNodeExists(lpcfgPal, "CLR_THINGHIGHLIGHT"))
		ConfigSetInteger(lpcfgPal, "CLR_THINGHIGHLIGHT",	RGB(0xFF, 0x99, 0x00));

	if(!ConfigNodeExists(lpcfgPal, "CLR_GRID"))
		ConfigSetInteger(lpcfgPal, "CLR_GRID",				RGB(0x00, 0x00, 0x36));

	if(!ConfigNodeExists(lpcfgPal, "CLR_GRID64"))
		ConfigSetInteger(lpcfgPal, "CLR_GRID64",			RGB(0x00, 0x00, 0x5E));

	if(!ConfigNodeExists(lpcfgPal, "CLR_LINEBLOCKSOUND"))
		ConfigSetInteger(lpcfgPal, "CLR_LINEBLOCKSOUND",	RGB(0x69, 0x67, 0x98));

	if(!ConfigNodeExists(lpcfgPal, "CLR_MAPBOUNDARY"))
		ConfigSetInteger(lpcfgPal, "CLR_MAPBOUNDARY",		RGB(0xFF, 0x00, 0x00));

	if(!ConfigNodeExists(lpcfgPal, "CLR_AXES"))
		ConfigSetInteger(lpcfgPal, "CLR_AXES",				RGB(0x56, 0x00, 0x73));

	if(!ConfigNodeExists(lpcfgPal, "CLR_ZEROHEIGHTLINE"))
		ConfigSetInteger(lpcfgPal, "CLR_ZEROHEIGHTLINE",	RGB(0x55, 0x55, 0x55));

	if(!ConfigNodeExists(lpcfgPal, "CLR_FOFSECTOR"))
		ConfigSetInteger(lpcfgPal, "CLR_FOFSECTOR",			RGB(0xAA, 0xFF, 0xAA));
}


/* SetDefaultTextures
 *   Sets default default default (!) textures.
 *
 * Parameters: None.
 *
 * Return value: None.
 */
static void SetDefaultTextures(void)
{
	CONFIG *lpcfgDefTextures = ConfigGetSubsection(g_lpcfgMain, OPT_DEFAULTTEX);

	if(!ConfigNodeExists(lpcfgDefTextures, "upper")) ConfigSetStringA(lpcfgDefTextures, "upper", OPT_DEFAULT_DEFTEXTURE);
	if(!ConfigNodeExists(lpcfgDefTextures, "middle")) ConfigSetStringA(lpcfgDefTextures, "middle", OPT_DEFAULT_DEFTEXTURE);
	if(!ConfigNodeExists(lpcfgDefTextures, "lower")) ConfigSetStringA(lpcfgDefTextures, "lower", OPT_DEFAULT_DEFTEXTURE);
}


/* SetDefaultSector
 *   Sets default default default (!) sector properties.
 *
 * Parameters: None.
 *
 * Return value: None.
 */
static void SetDefaultSector(void)
{
	CONFIG *lpcfgDefSec = ConfigGetSubsection(g_lpcfgMain, OPT_DEFAULTSEC);

	if(!ConfigNodeExists(lpcfgDefSec, "tfloor")) ConfigSetStringA(lpcfgDefSec, "tfloor", "FLOOR0_6");
	if(!ConfigNodeExists(lpcfgDefSec, "tceiling")) ConfigSetStringA(lpcfgDefSec, "tceiling", "FLOOR0_3");

	if(!ConfigNodeExists(lpcfgDefSec, "hfloor")) ConfigSetInteger(lpcfgDefSec, "hfloor", 128);
	if(!ConfigNodeExists(lpcfgDefSec, "hceiling")) ConfigSetInteger(lpcfgDefSec, "hfloor", 512);
	if(!ConfigNodeExists(lpcfgDefSec, "brightness")) ConfigSetInteger(lpcfgDefSec, "hfloor", 255);
}


/* GetOptionsForGame
 *   Gets the subsection of the main options for the specified game config.
 *
 * Parameters:
 *   CONFIG*	lpcfgMap	Game configuration.
 *
 * Return value: CONFIG*
 *   Subsection of the main options for lpcfgMap.
 */
CONFIG* GetOptionsForGame(CONFIG *lpcfgMap)
{
	UINT cchGameID = ConfigGetStringLength(lpcfgMap, MAPCFG_ID) + 1;
	LPSTR szGameID = ProcHeapAlloc(cchGameID);
	CONFIG *lpcfgGameOptions;

	/* Get the ID for this game. */
	ConfigGetStringA(lpcfgMap, MAPCFG_ID, szGameID, cchGameID);

	/* Get the options if they exist, or create them if they don't. */
	lpcfgGameOptions = ConfigGetOrAddSubsection(
		ConfigGetSubsection(g_lpcfgMain, OPT_GAMECONFIGS),
		szGameID);
	ProcHeapFree(szGameID);

	return lpcfgGameOptions;
}


/* GetTestingOptions
 *   Gets the testing options for a particular game configuration.
 *
 * Parameters:
 *   CONFIG*	lpcfgMainGame	Subsection of the main options for the game.
 *
 * Return value: CONFIG*
 *   Testing options.
 */
CONFIG* GetTestingOptions(CONFIG *lpcfgMainGame)
{
	CONFIG *lpcfgTesting = ConfigGetOrAddSubsection(lpcfgMainGame, "testing");

	/* Set some defaults if necessary. */
	if(!ConfigNodeExists(lpcfgTesting, TESTCFG_SFX)) ConfigSetInteger(lpcfgTesting, TESTCFG_SFX, TRUE);
	if(!ConfigNodeExists(lpcfgTesting, TESTCFG_MUSIC)) ConfigSetInteger(lpcfgTesting, TESTCFG_MUSIC, TRUE);
	if(!ConfigNodeExists(lpcfgTesting, TESTCFG_DIFFICULTY)) ConfigSetInteger(lpcfgTesting, TESTCFG_DIFFICULTY, DIFF_NORMAL);
	if(!ConfigNodeExists(lpcfgTesting, TESTCFG_GAMETYPE)) ConfigSetInteger(lpcfgTesting, TESTCFG_GAMETYPE, GT_DEFAULT);
	if(!ConfigNodeExists(lpcfgTesting, TESTCFG_SKIN)) ConfigSetStringA(lpcfgTesting, TESTCFG_SKIN, "Sonic");

	return lpcfgTesting;
}
