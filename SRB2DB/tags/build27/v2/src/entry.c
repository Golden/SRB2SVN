#include <windows.h>
#include <commctrl.h>

#include "general.h"
#include "../res/resource.h"
#include "renderer.h"
#include "openwads.h"
#include "options.h"
#include "keyboard.h"
#include "mapconfig.h"
#include "prefab.h"
#include "preset.h"

#include "win/mdiframe.h"
#include "win/mapwin.h"

#include "../DockWnd/DockWnd.h"

#ifdef BUGTRAP

#include "../BugTrap/BugTrap.h"
#include "../BugTrap/BugTrap_FnTypes.h"

static HMODULE g_hmodBugTrap;

static void InstallExceptionHandler(void);
static void RemoveExceptionHandler(void);

#endif


/* WinMain
 *   Entry point.
 *
 * Parameters:
 *   HINSTANCE	hInstance			Instance handle for application.
 *   HINSTANCE  hPrevInstance		Zero.
 *   LPSTR		szCmdLine			String of all arguments passed on command
 *									line.
 *   int		iCmdShow			Constant determining initial window state.
 *
 * Return value: int
 *   Exit code.
 */

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR szCmdLine, int iCmdShow)
{
	MSG msg;
	OSVERSIONINFO ovi;
	INITCOMMONCONTROLSEX iccx;

	UNREFERENCED_PARAMETER(hPrevInstance);

	/* Initialise globals. */
	g_hInstance = hInstance;
	g_hProcHeap = GetProcessHeap();

#ifdef BUGTRAP
	/* Install the exception handler. */
	InstallExceptionHandler();
#endif

	/* Prepare for concurrency. */
	InitializeCriticalSection(&g_cs);

	/* Check OS version. */
	ovi.dwOSVersionInfoSize = sizeof(ovi);
	GetVersionEx(&ovi);
	g_bWinNT = (ovi.dwPlatformId == VER_PLATFORM_WIN32_NT);

	/* XP and on required for Common Controls 6. */
	g_bHasCC6 = g_bWinNT && (ovi.dwMajorVersion > 5 || (ovi.dwMajorVersion == 5 && ovi.dwMinorVersion >= 1));


	/* Prepare some internal structures. */
	InitOpenWadsList();

	/* Register window classes. */
	RegisterMapWindowClass();

	/* Load the config file. */
	if(!LoadMainConfigurationFile())
	{
		/* Failed to open config file? */
		DIE(IDS_ERROR_LOADCONFIG);
		return 1;
	}

	/* Load map configurations. */
	LoadMapConfigs();

	/* Register our custom clipboard data types. */
	RegisterPrefabClipboardFormat();
	RegisterPresetClipboardFormat();

	/* Create the palette we use for drawing maps. */
	InitialiseRenderer();

	/* Initialise the Common Controls. */
	iccx.dwSize = sizeof(iccx);
	iccx.dwICC = ICC_BAR_CLASSES;
	InitCommonControlsEx(&iccx);

	/* Set up the docking library. */
	DockingInitialize(hInstance);

	/* Try to create the main window. */
	if(CreateMainWindow(iCmdShow) != 0) DIE(IDS_CREATEWINDOW);

	/* Was there a filename passed on the command line? */
	/* TODO: Make this slightly less stupid. */
	if(*szCmdLine) OpenWadForEditing(szCmdLine);

	/* Message loop. Exits on a WM_QUIT. */
	while(GetMessage(&msg, NULL, 0, 0))
	{
		if(!TranslateMDISysAccel(g_hwndClient, &msg) &&
		   !TranslateAccelerator(g_hwndMain, g_hAccel, &msg))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
	}

	/* Begin cleaning up. */
	DockingUnInitialize();
	ShutdownRenderer();
	UnloadMapConfigs();
	UnloadMainConfigurationFile();

	DestroyCustomAccelerator();

	DeleteCriticalSection(&g_cs);

#ifdef BUGTRAP
	/* Remove the exception handler and unload the library. */
	RemoveExceptionHandler();
#endif

	/* Exit code. */
	return msg.wParam;
}


#ifdef BUGTRAP

/* InstallExceptionHandler
 *   Sets up the BugTrap exception handler.
 *
 * Parameters:
 *   None.
 *
 * Return value: None.
 */
static void InstallExceptionHandler(void)
{
	BT_SETSUPPORTEMAIL lpfnBT_SetSupportEMail;
	BT_SETFLAGS lpfnBT_SetFlags;
	//BT_SETSUPPORTURL lpfnBT_SetSupportURL;
	//BT_READVERSIONINFO lpfnBT_ReadVersionInfo;
	BT_SETAPPNAME lpfnBT_SetAppName;
	BT_SETAPPVERSION lpfnBT_SetAppVersion;

	/* Loading the library installs the exception handler. */
#ifdef _UNICODE
	g_hmodBugTrap = LoadLibrary(L"BugTrapU.dll");
#else
	g_hmodBugTrap = LoadLibrary("BugTrap.dll");
#endif

	/* Get the functions. */
	lpfnBT_SetSupportEMail = (BT_SETSUPPORTEMAIL)GetProcAddress(g_hmodBugTrap, "BT_SetSupportEMail");
	lpfnBT_SetFlags = (BT_SETFLAGS)GetProcAddress(g_hmodBugTrap, "BT_SetFlags");
	//lpfnBT_SetSupportURL = (BT_SETSUPPORTURL)GetProcAddress(g_hmodBugTrap, "BT_SetSupportURL");
	//lpfnBT_ReadVersionInfo = (BT_READVERSIONINFO)GetProcAddress(g_hmodBugTrap, "BT_ReadVersionInfo");
	lpfnBT_SetAppName = (BT_SETAPPNAME)GetProcAddress(g_hmodBugTrap, "BT_SetAppName");
	lpfnBT_SetAppVersion = (BT_SETAPPVERSION)GetProcAddress(g_hmodBugTrap, "BT_SetAppVersion");

	if(g_hmodBugTrap)
	{
		lpfnBT_SetAppName(g_szAppName);
		lpfnBT_SetAppVersion(TEXT("0.1.0.0"));
		lpfnBT_SetSupportEMail(TEXT("oogaland@gm" /* No spam, please. */ "ail.com"));
		lpfnBT_SetFlags(BTF_DETAILEDMODE | BTF_EDITMAIL | BTF_ATTACHREPORT);

		/* TODO: This stuff should work, but doesn't. Bug in BugTrap? */
		//lpfnBT_SetSupportURL(TEXT("http://homepages.inf.ed.ac.uk/s0569864/srb2builder/"));
		//lpfnBT_ReadVersionInfo(NULL);
	}
}

/* RemoveExceptionHandler
 *   Removes the BugTrap exception handler.
 *
 * Parameters:
 *   None.
 *
 * Return value: None.
 */
static void RemoveExceptionHandler(void)
{
	if(g_hmodBugTrap) FreeLibrary(g_hmodBugTrap);
}

#endif
