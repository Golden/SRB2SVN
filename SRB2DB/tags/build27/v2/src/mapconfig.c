#include <windows.h>
#include <tchar.h>
#include <stdio.h>

#include "general.h"
#include "config.h"
#include "options.h"
#include "mapconfig.h"
#include "map.h"
#include "openwads.h"
#include "../res/resource.h"

#include "win/mdiframe.h"


/* Macros. */
#define MAPCFGDIR		TEXT("map/")
#define MAPCFGPATTERN	TEXT("*.cfg")

#define MAPCFG_SIGNATURE	"SRB2 Builder Game Configuration"

#define ARROW_DEFAULT	0
#define CIRCLE_DEFAULT	0
#define ERROR_DEFAULT	0
#define HANGS_DEFAULT	0
#define HEIGHT_DEFAULT	64
#define WIDTH_DEFAULT	64
#define COLOUR_DEFAULT	0x808080
#define ZFACTOR_DEFAULT	16



/* We store all map configs as subsections of the following, since it's
 * convenient.
 */
static CONFIG *g_lpcfgMapConfigs;


/* Static prototypes. */
static BOOL AddThingsByCategory(CONFIG *lpcfgCategorySS, void *lpv);
static BOOL InheritThingDetailsAndAddToFlatSection(CONFIG *lpcfgThingSS, void *lpv);
static BOOL LDTypeCategoryIterator(CONFIG *lpcfgLDCategory, void *lpv);
static BOOL AddLineTypeToFlatList(CONFIG *lpcfgLDType, void *lpv);
static BOOL AddSingleMapConfigToComboBox(CONFIG *lpcfg, void *lpvWindow);


typedef struct _ATBCDATA
{
	CONFIG *lpcfgFlatThings, *lpcfgThingFlags;
} ATBCDATA;

typedef struct _ALTTFLDATA
{
	CONFIG *lpcfgFlatTypes;
	char *szPrefix;
} ALTTFLDATA;

typedef struct _THINGDETAILS
{
	int		iWidth, iArrow, iError, iHeight, iHangs, iCircle, iColour, iZFactor;
	LPSTR	szDeafText, szMultiText;
} THINGDETAILS;

typedef struct _ITDAATFSDATA
{
	THINGDETAILS	td;
	LPCSTR			szCategoryName;
	CONFIG			*lpcfgFlatThings;
} ITDAATFSDATA;



/* LoadMapConfigs
 *   Loads all map configuration files from the appropriate directory.
 *
 * Parameters:
 *   None.
 *
 * Return value: int
 *   Number of configs loaded.
 */
int LoadMapConfigs(void)
{
	HANDLE hFind;
	WIN32_FIND_DATA fd;
	int iConfigs = 0;	/* Keep track of the number of map configs loaded. */
	LPTSTR szOldDir;
	DWORD cbOldDir;
	TCHAR c;

	/* Save wd -- we change it, since Find[First|Next]File return relative
	 * paths.
	 */
	cbOldDir = GetCurrentDirectory(1, &c);
	szOldDir = ProcHeapAlloc(cbOldDir);
	GetCurrentDirectory(cbOldDir, szOldDir);

	/* Change to the map config directory. */
	SetCurrentDirectory(OPTDIR MAPCFGDIR);

	/* Initialise the container. */
	g_lpcfgMapConfigs = ConfigCreate();

	/* Find the first file we're interested in. */
	hFind = FindFirstFile(MAPCFGPATTERN, &fd);

	/* Did we find at least one file? */
	if(hFind != INVALID_HANDLE_VALUE)
	{
		CONFIG *lpcfg;

		/* Repeat for each map config file. */
		do
		{
			/* Load the file. */
			lpcfg = ConfigLoad(fd.cFileName);

			/* Was the file a syntactically-valid config? */
			if(lpcfg)
			{
				/* Check whether the type signature is correct. */
				int iLen;
				LPSTR szType;
				CONFIG *lpcfgThingCats, *lpcfgLineTypes;
				ATBCDATA atbcdata;
				int cchGameID;

				iLen = ConfigGetStringLength(lpcfg, "type");

				if(iLen <= 0)
				{
					ConfigDestroy(lpcfg);
					continue;
				}

				szType = ProcHeapAlloc((iLen + 1) * sizeof(char));
				ConfigGetStringA(lpcfg, "type", szType, iLen + 1);

				if(lstrcmpiA(szType, MAPCFG_SIGNATURE))
				{
					ConfigDestroy(lpcfg);
					ProcHeapFree(szType);
					continue;
				}

				ProcHeapFree(szType);



				/* Things and line types need some special attention. We store
				 * two copies of each: one in its category, and one in a flat
				 * subsection. Also, we fill in parental values for things.
				 */

				/* Get the sections of things and line types, organised by
				 * category.
				 */
				lpcfgThingCats = ConfigGetSubsection(lpcfg, "thingtypes");
				lpcfgLineTypes = ConfigGetSubsection(lpcfg, "linedeftypes");

				if(!lpcfgThingCats || !lpcfgLineTypes)
				{
					ConfigDestroy(lpcfg);
					continue;
				}

				/* Create a flat section of things, so we don't have to look
				 * through all the categories for each thing.
				 */
				atbcdata.lpcfgFlatThings = ConfigAddSubsection(lpcfg, FLAT_THING_SECTION);
				atbcdata.lpcfgThingFlags = ConfigGetSubsection(lpcfg, "thingflags");

				ConfigIterate(lpcfgThingCats, AddThingsByCategory, &atbcdata);

				/* Do the same for line types. */
				ConfigIterate(lpcfgLineTypes, LDTypeCategoryIterator, ConfigAddSubsection(lpcfg, "__ldtypesflat"));


				/* Add the map config to the global config tree! Use the ID as
				 * the name.
				 */

				cchGameID = ConfigGetStringLength(lpcfg, MAPCFG_ID) + 1;

				if(cchGameID > 1)
				{
					LPSTR szMapCfgID = ProcHeapAlloc(cchGameID);
					LPSTR szInCfg = szMapCfgID;
					ConfigGetStringA(lpcfg, MAPCFG_ID, szMapCfgID, cchGameID);

					/* Replace illegal characters in the ID. */
					while((szInCfg = strpbrk(szInCfg, " \t\r\n={};"))) *szInCfg = '_';

					ConfigSetSubsection(g_lpcfgMapConfigs, szMapCfgID, lpcfg);
					ProcHeapFree(szMapCfgID);

					iConfigs++;
				}

			}	/* if(lpcfg) */

		} while(FindNextFile(hFind, &fd));

		FindClose(hFind);
	}

	/* Change back to our old wd. */
	SetCurrentDirectory(szOldDir);
	ProcHeapFree(szOldDir);

	/* Return the number of map configs. */
	return iConfigs;
}




/* AddThingsByCategory
 *   Given a tree of things from the same category, sets their default
 *   properties as specified by the category, and then also adds a copy of each
 *   to a flat section.
 *
 * Parameters:
 *   CONFIG*	lpcfgCategorySS		The category's SUBSECTION CONTAINER!
 *   void*		lpv					(ATBCDATA*) Flat things ss ROOT, thing
 *									flags.
 *
 * Return value: BOOL
 *   TRUE if iteration should continue; FALSE otherwise.
 *
 * Remarks:
 *   Used as a callback by ConfigIterate.
 */
static BOOL AddThingsByCategory(CONFIG *lpcfgCategorySS, void *lpv)
{
	CONFIG *lpcfgCategoryRoot = lpcfgCategorySS->lpcfgSubsection;
	CONFIG *lpcfgFlatThings = ((ATBCDATA*)lpv)->lpcfgFlatThings;
	CONFIG *lpcfgThingFlags = ((ATBCDATA*)lpv)->lpcfgThingFlags;
	CONFIG *lpcfgValues;
	ITDAATFSDATA itdaatfsdata;
	WORD cbBuffer;

	/* Something other than a subsection in things section?? */
	if(lpcfgCategorySS->entrytype != CET_SUBSECTION) return TRUE;

	/* Get all the properties specified by the category. These are used as
	 * defaults when things in this category don't specify these values
	 * themselves.
	 */

	itdaatfsdata.td.iArrow		= ConfigNodeExists(lpcfgCategoryRoot, "arrow")		? ConfigGetInteger(lpcfgCategoryRoot, "arrow")		: ARROW_DEFAULT;
	itdaatfsdata.td.iCircle		= ConfigNodeExists(lpcfgCategoryRoot, "circle")		? ConfigGetInteger(lpcfgCategoryRoot, "circle")		: CIRCLE_DEFAULT;
	itdaatfsdata.td.iColour		= ConfigNodeExists(lpcfgCategoryRoot, "color")		? ConfigGetInteger(lpcfgCategoryRoot, "color")		: (signed)COLOUR_DEFAULT;
	itdaatfsdata.td.iError		= ConfigNodeExists(lpcfgCategoryRoot, "error")		? ConfigGetInteger(lpcfgCategoryRoot, "error")		: ERROR_DEFAULT;
	itdaatfsdata.td.iHangs		= ConfigNodeExists(lpcfgCategoryRoot, "hangs")		? ConfigGetInteger(lpcfgCategoryRoot, "hangs")		: HANGS_DEFAULT;
	itdaatfsdata.td.iHeight		= ConfigNodeExists(lpcfgCategoryRoot, "height")		? ConfigGetInteger(lpcfgCategoryRoot, "height")		: HEIGHT_DEFAULT;
	itdaatfsdata.td.iWidth		= ConfigNodeExists(lpcfgCategoryRoot, "width")		? ConfigGetInteger(lpcfgCategoryRoot, "width")		: WIDTH_DEFAULT;
	itdaatfsdata.td.iZFactor	= ConfigNodeExists(lpcfgCategoryRoot, "zfactor")	? ConfigGetInteger(lpcfgCategoryRoot, "zfactor")	: ZFACTOR_DEFAULT;

	/* Default string values. */

	if(ConfigNodeExists(lpcfgCategoryRoot, "deaftext"))
	{
		cbBuffer = ConfigGetStringLength(lpcfgCategoryRoot, "deaftext") + 1;
		itdaatfsdata.td.szDeafText = ProcHeapAlloc(cbBuffer);
		ConfigGetStringA(lpcfgCategoryRoot, "deaftext", itdaatfsdata.td.szDeafText, cbBuffer);
	}
	else
	{
		cbBuffer = ConfigGetStringLength(lpcfgThingFlags, "8") + 1;
		itdaatfsdata.td.szDeafText = ProcHeapAlloc(cbBuffer);
		ConfigGetStringA(lpcfgThingFlags, "8", itdaatfsdata.td.szDeafText, cbBuffer);
	}

	if(ConfigNodeExists(lpcfgCategoryRoot, "multitext"))
	{
		cbBuffer = ConfigGetStringLength(lpcfgCategoryRoot, "multitext") + 1;
		itdaatfsdata.td.szMultiText = ProcHeapAlloc(cbBuffer);
		ConfigGetStringA(lpcfgCategoryRoot, "multitext", itdaatfsdata.td.szMultiText, cbBuffer);
	}
	else
	{
		cbBuffer = ConfigGetStringLength(lpcfgThingFlags, "16") + 1;
		itdaatfsdata.td.szMultiText = ProcHeapAlloc(cbBuffer);
		ConfigGetStringA(lpcfgThingFlags, "16", itdaatfsdata.td.szMultiText, cbBuffer);
	}

	/* Now we iterate over every thing in the category, setting its missing
	 * fields and also adding a copy to the flat section.
	 */
	itdaatfsdata.szCategoryName = lpcfgCategorySS->szName;
	itdaatfsdata.lpcfgFlatThings = lpcfgFlatThings;
	lpcfgValues = ConfigGetSubsection(lpcfgCategoryRoot, "values");
	if(lpcfgValues)
		ConfigIterate(lpcfgValues, InheritThingDetailsAndAddToFlatSection, &itdaatfsdata);

	ProcHeapFree(itdaatfsdata.td.szDeafText);
	ProcHeapFree(itdaatfsdata.td.szMultiText);

	/* Keep going. */
	return TRUE;
}



/* InheritThingDetailsAndAddToFlatSection
 *   Sets a thing's default values where they're missing, and adds a copy of the
 *   thing to a flat section of things.
 *
 * Parameters:
 *   CONFIG*	lpcfgCategorySS		The thing's subsection *container* OR a
 *									string config entry!
 *   void*		lpv					Pointers to the defaults and the flat
 *									section.
 *
 * Return value: BOOL
 *   TRUE if iteration should continue; FALSE otherwise.
 *
 * Remarks:
 *   Used as a callback by ConfigIterate.
 */
static BOOL InheritThingDetailsAndAddToFlatSection(CONFIG *lpcfgThingSS, void *lpv)
{
	THINGDETAILS td = ((ITDAATFSDATA*)lpv)->td;
	CONFIG *lpcfgFlatThings = ((ITDAATFSDATA*)lpv)->lpcfgFlatThings;
	CONFIG *lpcfgThingRoot;
	LPSTR szTitle, szName;
	WORD cbTitle, cbName;


	/* Determine whether this is a fully-fledged subsection, a simple string, or
	 * some garbage.
	 */
	switch(lpcfgThingSS->entrytype)
	{
	case CET_SUBSECTION:
		lpcfgThingRoot = lpcfgThingSS->lpcfgSubsection;
		break;
	case CET_STRING:
		/* Build a subsection from the string. The string value is the 'title'
		 * field.
		 */
		cbTitle = lstrlenA(lpcfgThingSS->sz) + 1;
		szTitle = ProcHeapAlloc(cbTitle);
		cbName = lstrlenA(lpcfgThingSS->szName) + 1;
		szName = ProcHeapAlloc(cbName);

		CopyMemory(szTitle, lpcfgThingSS->sz, cbTitle);
		CopyMemory(szName, lpcfgThingSS->szName, cbName);

		/* lpcfgThingSS isn't the category root, strictly, but it still fits the
		 * definition.
		 */
		lpcfgThingRoot = ConfigAddSubsection(lpcfgThingSS, szName);
		ConfigSetStringA(lpcfgThingRoot, "title", szTitle);

		ProcHeapFree(szName);
		ProcHeapFree(szTitle);

	default:
		/* Neither a subsection nor a string. (??) */
		return TRUE;
	}


	/* Set the missing fields. */
	if(!ConfigNodeExists(lpcfgThingRoot, "arrow")) ConfigSetInteger(lpcfgThingRoot, "arrow", td.iArrow);
	if(!ConfigNodeExists(lpcfgThingRoot, "circle")) ConfigSetInteger(lpcfgThingRoot, "circle", td.iCircle);
	if(!ConfigNodeExists(lpcfgThingRoot, "color")) ConfigSetInteger(lpcfgThingRoot, "color", td.iColour);
	if(!ConfigNodeExists(lpcfgThingRoot, "error")) ConfigSetInteger(lpcfgThingRoot, "error", td.iError);
	if(!ConfigNodeExists(lpcfgThingRoot, "hangs")) ConfigSetInteger(lpcfgThingRoot, "hangs", td.iHangs);
	if(!ConfigNodeExists(lpcfgThingRoot, "height")) ConfigSetInteger(lpcfgThingRoot, "height", td.iHeight);
	if(!ConfigNodeExists(lpcfgThingRoot, "width")) ConfigSetInteger(lpcfgThingRoot, "width", td.iWidth);
	if(!ConfigNodeExists(lpcfgThingRoot, "zfactor")) ConfigSetInteger(lpcfgThingRoot, "zfactor", td.iZFactor);
	if(!ConfigNodeExists(lpcfgThingRoot, "deaftext")) ConfigSetStringA(lpcfgThingRoot, "deaftext", td.szDeafText);
	if(!ConfigNodeExists(lpcfgThingRoot, "multitext")) ConfigSetStringA(lpcfgThingRoot, "multitext", td.szMultiText);

	/* Set the category name for cross-referencing from the flat section. */
	ConfigSetStringA(lpcfgThingRoot, "category", ((ITDAATFSDATA*)lpv)->szCategoryName);

	/* Add a *copy* of the thing to the flat section. */
	ConfigSetSubsection(lpcfgFlatThings, lpcfgThingSS->szName, ConfigDuplicate(lpcfgThingRoot));

	/* Keep going. */
	return TRUE;
}


/* LDTypeCategoryIterator
 *   Adds all linedef types in a category to the flat list of linedef types.
 *
 * Parameters:
 *   CONFIG*	lpcfgLDType		A linedef type category subsection.
 *   void*		lpv				Pointer to the flat section.
 *
 * Return value: BOOL
 *   TRUE if iteration should continue; FALSE otherwise.
 *
 * Remarks:
 *   Used as a callback by ConfigIterate.
 */
static BOOL LDTypeCategoryIterator(CONFIG *lpcfgLDCategory, void *lpv)
{
	/* We should have a line-type category. It's only its values that we're
	 * interested in. Iterate over those.
	 */
	CONFIG *lpcfgValues = ConfigGetSubsection(lpcfgLDCategory->lpcfgSubsection, "values");
	int cchPrefix = ConfigGetStringLength(lpcfgLDCategory->lpcfgSubsection, "title");

	if(lpcfgValues && cchPrefix > 0)
	{
		ALTTFLDATA alttfldata;

		alttfldata.lpcfgFlatTypes = (CONFIG*)lpv;
		alttfldata.szPrefix = ProcHeapAlloc((cchPrefix + 1) * sizeof(char));
		ConfigGetStringA(lpcfgLDCategory->lpcfgSubsection, "title", alttfldata.szPrefix, cchPrefix + 1);

		ConfigIterate(lpcfgValues, AddLineTypeToFlatList, &alttfldata);

		ProcHeapFree(alttfldata.szPrefix);
	}

	/* Keep going. */
	return TRUE;
}


/* AddLineTypeToFlatList
 *   Adds a linedef type to the flat list of linedef types.
 *
 * Parameters:
 *   CONFIG*	lpcfgLDType		A string entry whose name is the linedef effect
 *								value.
 *   void*		lpv				Pointer to ALTTFLDATA with the flat section and
 *								prefix for the category.
 *
 * Return value: BOOL
 *   TRUE if iteration should continue; FALSE otherwise.
 *
 * Remarks:
 *   Used as a callback by ConfigIterate.
 */
static BOOL AddLineTypeToFlatList(CONFIG *lpcfgLDType, void *lpv)
{
	ALTTFLDATA *lpalttfldata = (ALTTFLDATA*)lpv;

	/* Build the display string from the prefix and the description. */
	char *szDisplayString = ProcHeapAlloc(lstrlenA(lpcfgLDType->sz) + lstrlenA(lpalttfldata->szPrefix) + 3);
	wsprintfA(szDisplayString, "%s: %s", lpalttfldata->szPrefix, lpcfgLDType->sz);

	/* Add the linedef type to the flat section. */
	ConfigSetStringA(lpalttfldata->lpcfgFlatTypes, lpcfgLDType->szName, szDisplayString);

	ProcHeapFree(szDisplayString);

	/* Keep going. */
	return TRUE;
}



/* UnloadMapConfigs
 *   Frees all the map configs loaded at startup.
 *
 * Parameters:
 *   None.
 *
 * Return value: None.
 */
void UnloadMapConfigs(void)
{
	ConfigDestroy(g_lpcfgMapConfigs);
}



/* AddMapConfigsToComboBox, AddSingleMapConfigToComboBox
 *   Adds all loaded configs to a combo box.
 *
 * Parameters:
 *   HWND	hwndCombo	Combo box handle.
 *
 * Return value: None.
 *
 * Remarks:
 *   The item data fields are set to pointers to the structures.
 */
void AddMapConfigsToComboBox(HWND hwndCombo)
{
	ConfigIterate(g_lpcfgMapConfigs, AddSingleMapConfigToComboBox, (void*)hwndCombo);
}


static BOOL AddSingleMapConfigToComboBox(CONFIG *lpcfg, void *lpvWindow)
{
	HWND hwndCombo = (HWND)lpvWindow;
	LPTSTR szBuffer;
	int iIndex;
	int cchBuffer;

	/* Get title. */
	cchBuffer = ConfigGetStringLength(lpcfg->lpcfgSubsection, MAPCFG_GAME) + 1;
	szBuffer = ProcHeapAlloc(cchBuffer * sizeof(TCHAR));
	ConfigGetString(lpcfg->lpcfgSubsection, MAPCFG_GAME, szBuffer, cchBuffer);

	/* Add entry and store pointer to config. */
	iIndex = SendMessage(hwndCombo, CB_ADDSTRING, 0, (LPARAM)szBuffer);
	SendMessage(hwndCombo, CB_SETITEMDATA, iIndex, (LPARAM)lpcfg->lpcfgSubsection);

	ProcHeapFree(szBuffer);

	/* Keep iterating. */
	return TRUE;
}



/* GetThingConfigInfo
 *   Gets config information about a particular type of thing.
 *
 * Parameters:
 *   CONFIG*			lpcfgThings		Root of flat things subsection.
 *   unsigned short		unType			Thing ID.
 *
 * Return value: CONFIG*
 *   Pointer to subsection for the specified thing-type, or NULL if none exists.
 */
CONFIG* GetThingConfigInfo(CONFIG *lpcfgThings, unsigned short unType)
{
	char szTypeEntry[6];

	/* Format the effect number for the config entry. */
	wsprintfA(szTypeEntry, "%u", unType);

	/* Is there an entry for this effect in the config? */
	if(ConfigNodeExists(lpcfgThings, szTypeEntry))
		return ConfigGetSubsection(lpcfgThings, szTypeEntry);
	else return NULL;
}






/* GetThingTypeDisplayText
 *   Builds the display string for a thing's type.
 *
 * Parameters:
 *   unsigned short		unType		Type specifier.
 *   CONFIG*			lpcfgThings	Config tree containing thing information.
 *   LPTSTR				szBuffer	Buffer to store string in.
 *   unsigned short		cchBuffer	Length of buffer, including terminator.
 *
 * Return value: None.
 */
void GetThingTypeDisplayText(unsigned short unType, CONFIG *lpcfgThings, LPTSTR szBuffer, unsigned short cchBuffer)
{
	LPTSTR szTypeText;
	CONFIG *lpcfgThingProps;

	/* We certainly don't need to be any longer than the buffer we return in. */
	szTypeText = ProcHeapAlloc(cchBuffer * sizeof(TCHAR));

	/* Is there an entry for this effect in the config? */
	if((lpcfgThingProps = GetThingConfigInfo(lpcfgThings, unType)))
		ConfigGetString(lpcfgThingProps, "title", szTypeText, cchBuffer);
	else LoadString(g_hInstance, IDS_UNKNOWNTHING, szTypeText, cchBuffer);

	_sntprintf(szBuffer, cchBuffer - 1, TEXT("%u - %s"), unType, szTypeText);
	szBuffer[cchBuffer - 1] = '\0';

	ProcHeapFree(szTypeText);
}


/* GetThingDirectionDisplayText
 *   Builds the display string for a thing's direction.
 *
 * Parameters:
 *   unsigned short		nDirection	Direction as per wad-spec (i.e. degrees in
 *									the positive direction from the x-axis).
 *   LPTSTR				szBuffer	Buffer to store string in.
 *   unsigned short		cbBuffer	Length of buffer, including terminator.
 *
 * Return value: None.
 */
void GetThingDirectionDisplayText(short nDirection, LPTSTR szBuffer, unsigned short cbBuffer)
{
	TCHAR szDirString[16];
	UINT uiDirectionID;

	if(nDirection < 23 || nDirection > 337) uiDirectionID = IDS_EAST;
	else if(nDirection < 68) uiDirectionID = IDS_NORTHEAST;
	else if(nDirection < 113) uiDirectionID = IDS_NORTH;
	else if(nDirection < 158) uiDirectionID = IDS_NORTHWEST;
	else if(nDirection < 203) uiDirectionID = IDS_WEST;
	else if(nDirection < 248) uiDirectionID = IDS_SOUTHWEST;
	else if(nDirection < 293) uiDirectionID = IDS_SOUTH;
	else uiDirectionID = IDS_SOUTHEAST;

	LoadString(g_hInstance, uiDirectionID, szDirString, sizeof(szDirString) / sizeof(TCHAR));

	_sntprintf(szBuffer, cbBuffer - 1, TEXT("%s (%d)"), szDirString, nDirection);
	szBuffer[cbBuffer - 1] = '\0';
}



/* GetEffectDisplayText
 *   Builds the display string for a sector's effect.
 *
 * Parameters:
 *   unsigned short		unEffect	Effect specifier.
 *   CONFIG*			lpcfg		Config containing effect descriptions.
 *   LPTSTR				szBuffer	Buffer to store string in.
 *   unsigned short		cchBuffer	Length of buffer, including terminator.
 *
 * Return value: None.
 *
 * Remarks:
 *   This works for sectors and old-format linedefs.
 */
void GetEffectDisplayText(unsigned short unEffect, CONFIG *lpcfg, LPTSTR szBuffer, unsigned short cchBuffer)
{
	LPTSTR szEffectText;
	char szEffectEntry[12];

	/* We certainly don't need to be any longer than the buffer we return in. */
	szEffectText = ProcHeapAlloc(cchBuffer * sizeof(TCHAR));

	/* Format the effect number for the config entry. */
	wsprintfA(szEffectEntry, "%u", unEffect);

	/* Is there an entry for this effect in the config? */
	if(ConfigNodeExists(lpcfg, szEffectEntry))
		ConfigGetString(lpcfg, szEffectEntry, szEffectText, cchBuffer);
	else LoadString(g_hInstance, IDS_UNKNOWNEFFECT, szEffectText, cchBuffer);

	_sntprintf(szBuffer, cchBuffer - 1, TEXT("%u - %s"), unEffect, szEffectText);
	szBuffer[cchBuffer - 1] = '\0';

	ProcHeapFree(szEffectText);
}



/* SetAllThingPropertiesFromType, SetThingPropertiesFromType
 *   Sets some fields of (a) thing(s), based on those specified for the thing
 *   type.
 *
 * Parameters:
 *   MAP*		lpmap				Pointer to map data.
 *   int		iThing				Index of thing.
 *   CONFIG*	lpcfgFlatThings		Flat thing config structure.
 *
 * Return value: None.
 */
void SetAllThingPropertiesFromType(MAP *lpmap, CONFIG *lpcfgFlatThings)
{
	int i;

	for(i = 0; i < lpmap->iThings; i++)
		SetThingPropertiesFromType(lpmap, i, lpcfgFlatThings);
}

void SetThingPropertiesFromType(MAP *lpmap, int iThing, CONFIG *lpcfgFlatThings)
{
	char szTypeEntry[6];

	/* Format the effect number for the config entry. */
	wsprintfA(szTypeEntry, "%u", lpmap->things[iThing].thing);

	/* Is there an entry for this effect in the config? */
	if(ConfigNodeExists(lpcfgFlatThings, szTypeEntry))
	{
		CONFIG *lpcfgThing = ConfigGetSubsection(lpcfgFlatThings, szTypeEntry);
		lpmap->things[iThing].arrow = ConfigGetInteger(lpcfgThing, "arrow");
		lpmap->things[iThing].color = ConfigGetInteger(lpcfgThing, "color");
	}
}


/* GetZFactor
 *   Retrieves the z-factor for a thing type.
 *
 * Parameters:
 *   CONFIG*			lpcfgFlatThings		Flat thing config structure.
 *   unsigned short		unType				Type of thing.
 *
 * Return value: WORD
 *   Z-factor.
 */
WORD GetZFactor(CONFIG *lpcfgFlatThings, unsigned short unType)
{
	CONFIG *lpcfgThing = GetThingConfigInfo(lpcfgFlatThings, unType);

	/* No entry in the config for this type of thing? */
	if(!lpcfgThing) return ZFACTOR_DEFAULT;

	return (WORD)ConfigGetInteger(lpcfgThing, "zfactor");
}


/* GetIWadForConfig
 *   Gets the index of the IWAD for a map config, loading it first if necessary.
 *
 * Parameters:
 *   CONFIG*	lpcfgMap	Map configuration. *Not* the container.
 *
 * Return value: int
 *   Index of IWAD.
 */
int GetIWadForConfig(CONFIG *lpcfgMap)
{
	int iIWad = -1;
	int cchGameID;

	cchGameID = ConfigGetStringLength(lpcfgMap, MAPCFG_ID);
	if(cchGameID > 0)
	{
		CONFIG *lpcfgGame;
		int cchIWad;
		LPSTR szGameID = ProcHeapAlloc(cchGameID + 1);

		/* Get the ID. */
		ConfigGetStringA(lpcfgMap, MAPCFG_ID, szGameID, cchGameID + 1);

		/* Game-config-specific options. */
		lpcfgGame = ConfigGetSubsection(ConfigGetSubsection(g_lpcfgMain, OPT_GAMECONFIGS), szGameID);

		ProcHeapFree(szGameID);

		/* Make sure we have an entry for this config at all, and if we do, also
		 * an IWAD entry.
		 */
		if(lpcfgGame && (cchIWad = ConfigGetStringLength(lpcfgGame, "iwad")) > 0)
		{
			LPTSTR szIWad;

			szIWad = ProcHeapAlloc((cchIWad + 1) * sizeof(TCHAR));
			ConfigGetString(lpcfgGame, "iwad", szIWad, cchIWad + 1);

			iIWad = LoadWad(szIWad);

			ProcHeapFree(szIWad);
		}
	}

	if(iIWad < 0)
	{
		/* TODO: Prompt to select IWAD. */
		MessageBox(g_hwndMain, "No IWAD specified.", g_szAppName, MB_ICONASTERISK);
	}

	return iIWad;
}

/* GetDefaultMapConfig
 *   Returns the default map config.
 *
 * Parameters:
 *   None.
 *
 * Return value: CONFIG*
 *   Default map config.
 */
CONFIG *GetDefaultMapConfig(void)
{
	/* TODO: Actually implement this correctly rather than just taking the first
	 * one we find.
	 */
	return g_lpcfgMapConfigs->lpcfgLeft ?
		g_lpcfgMapConfigs->lpcfgLeft->lpcfgSubsection :
		g_lpcfgMapConfigs->lpcfgRight->lpcfgSubsection;
}
