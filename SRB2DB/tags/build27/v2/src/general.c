#include <windows.h>
#include <tchar.h>
#include <stdio.h>

#include "general.h"
#include "../res/resource.h"

#include "win/mdiframe.h"

/* Globals. */
TCHAR g_szAppName[] = TEXT("SRB2 Builder");
HINSTANCE g_hInstance;
HANDLE g_hProcHeap;
CRITICAL_SECTION g_cs;
BOOL g_bWinNT, g_bHasCC6;


/* Static function prototypes. */
static void QuitImmediately(int iExitCode);


/* Die
 *   Displays a message box with an error message, and then quits.
 *
 * Parameters:
 *   LPCTSTR	szFile			Filename where error occurred.
 *   int		iLine			Line number where error occurred.
 *   int		iMsgResource	Resource ID of error message.
 *
 * Return value:
 *   None, but note that it *does* return.
 *
 * Notes:
 *   Use the DIE macro to avoid the line/file macros.
 */
void Die(LPCTSTR szFile, int iLine, int iMsgResource)
{
	TCHAR szBuffer[640], szFormat[128], szErrorMsg[512];

	LoadString(g_hInstance, IDS_DIEFORMAT, szFormat, 128);
	LoadString(g_hInstance, iMsgResource, szErrorMsg, 128);

	/* szFormat should have a %d, an %s and an %s, in that order. */
	_sntprintf(szBuffer, 640, szFormat, iLine, szFile, szErrorMsg);
	MessageBox(NULL, szBuffer, g_szAppName, MB_ICONERROR);

	/* Right, that's enough. Bye! */
	QuitImmediately(1);
}


/* QuitImmediately
 *   Drops everything and quits.
 *
 * Parameters:
 *   int		iExitCode			Errorlevel.
 *
 * Return value:
 *   None, but note that it *does* return.
 */
static void QuitImmediately(int iExitCode)
{
	DestroyMainWindow();
	PostQuitMessage(iExitCode);
}


/* IncrementInteger
 *   Does exactly what it says on the tin.
 *
 * Parameters:
 *   HWND		hwndUnused	Unused.
 *   LPARAM		lParam		Pointer to integer to increment.
 *
 * Return value:
 *   Always TRUE.
 *
 * Notes:
 *   Useful for enumeration functions that count things.
 */
BOOL CALLBACK IncrementInteger(HWND hwnd, LPARAM lParam)
{
	UNREFERENCED_PARAMETER(hwnd);
	(*(int*)lParam)++;
	return TRUE;
}


/* EqualPaths
 *   Determines whether two paths are equivalent.
 *
 * Parameters:
 *   LPCTSTR	szPath1		First path.
 *   LPCTSTR	szPath2		Second path.
 *
 * Return value:
 *   TRUE if paths are equivalent; FALSE if not.
 *
 * Notes:
 *   Ignorant of links. See also PathsReferToSameFile.
 */
BOOL EqualPaths(LPCTSTR szPath1, LPCTSTR szPath2)
{
	int cb1, cb2;
	LPTSTR szFullPath1, szFullPath2;
	BOOL bEqual;

	/* First, make the buffers big enough to store the full pathname. */
	cb1 = GetFullPathName(szPath1, 0, TEXT(""), NULL);
	cb2 = GetFullPathName(szPath2, 0, TEXT(""), NULL);
	szFullPath1 = ProcHeapAlloc((cb1 + 1)*sizeof(TCHAR));
	szFullPath2 = ProcHeapAlloc((cb2 + 1)*sizeof(TCHAR));

	/* Generate comparable paths. */
	GetFullPathName(szPath1, cb1, szFullPath1, NULL);
	GetFullPathName(szPath2, cb1, szFullPath2, NULL);
	GetShortPathName(szFullPath1, szFullPath1, cb1);
	GetShortPathName(szFullPath2, szFullPath2, cb2);

	bEqual = !lstrcmpi(szFullPath1, szFullPath2);

	/* Clean up. */
	ProcHeapFree(szFullPath1);
	ProcHeapFree(szFullPath2);

	return bEqual;
}


/* PathsReferToSameFile
 *   Determines whether two paths refer to the same file.
 *
 * Parameters:
 *   LPCTSTR	szPath1		First path.
 *   LPCTSTR	szPath2		Second path.
 *
 * Return value: BOOL
 *   TRUE if paths refer to same file; FALSE if not.
 *
 * Notes:
 *   Unlike EqualPaths, PathsReferToSameFile takes hard links into account.
 */
BOOL PathsReferToSameFile(LPCTSTR szPath1, LPCTSTR szPath2)
{
	HANDLE hFile1, hFile2;
	BOOL bEqual;

	/* Trivial case: paths are equivalent. */
	if(EqualPaths(szPath1, szPath2)) return TRUE;

	/* To check whether they're both links, we have to open them and compare
	 * their IDs.
	 */
	hFile1 = CreateFile(szPath1, 0, FILE_SHARE_READ | FILE_SHARE_WRITE, NULL, OPEN_EXISTING, 0, NULL);

	/* If we couldn't open the first one, then they're different (since the
	 * paths are different.
	 */
	if(!hFile1) return FALSE;

	hFile2 = CreateFile(szPath2, 0, FILE_SHARE_READ | FILE_SHARE_WRITE, NULL, OPEN_EXISTING, 0, NULL);

	/* Assume not equal. */
	bEqual = FALSE;
	if(hFile2)
	{
		BY_HANDLE_FILE_INFORMATION byhfi1, byhfi2;

		GetFileInformationByHandle(hFile1, &byhfi1);
		GetFileInformationByHandle(hFile2, &byhfi2);

		/* Files are equal iff they have the same ID on the same volume. */
		if(byhfi1.dwVolumeSerialNumber == byhfi2.dwVolumeSerialNumber &&
			byhfi1.nFileIndexLow == byhfi2.nFileIndexLow &&
			byhfi1.nFileIndexHigh == byhfi2.nFileIndexHigh)
		{
			bEqual = TRUE;
		}

		CloseHandle(hFile2);
	}

	CloseHandle(hFile1);
	return bEqual;
}



/* IsStringIntA
 *   Determines whether a string is a valid integer.
 *
 * Parameters:
 *   LPCSTR		sz		String to check.
 *
 * Return value: BOOL
 *   TRUE if string is integer; FALSE if not.
 *
 * Notes:
 *   Range is not checked.
 */
BOOL IsStringIntA(LPCSTR sz)
{
	if(*sz == '-') sz++;
	while(*sz >= '0' && *sz <= '9') sz++;
	return !(*sz);
}



/* MessageBoxFromStringTable
 *   Displays a message box using a string from the table.
 *
 * Parameters:
 *   HWND	hwnd				Parent window.
 *   WORD	wResourceString		ID of string.
 *   UINT	uiType				As for MessageBox.
 *
 * Return value: int
 *   As for MessageBox.
 */
int MessageBoxFromStringTable(HWND hwnd, WORD wResourceString, UINT uiType)
{
	MSGBOXPARAMS mbp;

	mbp.cbSize = sizeof(mbp);
	mbp.dwContextHelpId = 0;
	mbp.dwLanguageId = MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT);
	mbp.dwStyle = uiType;
	mbp.hInstance = g_hInstance;
	mbp.hwndOwner = hwnd;
	mbp.lpfnMsgBoxCallback = NULL;
	mbp.lpszCaption = g_szAppName;
	mbp.lpszIcon = NULL;
	mbp.lpszText = MAKEINTRESOURCE(wResourceString);

	return MessageBoxIndirect(&mbp);
}



/* Qsort[*]Comparison
 *   qsort callback function for comparing array entries. Various types.
 *
 * Parameters:
 *   const void*	lpv1, lpv2		Addresses of values to compare.
 *
 * Return value: int
 *   0 if *lpv1 == *lpv2; < 0 if *lpv1 < *lpv2; > 0 otherwise. Operators here
 *   are understood to refer to the objects referenced by the array
 */
int QsortIntegerComparison(const void *lpv1, const void *lpv2)
{
	int i1 = *((int*)lpv1), i2 = *((int*)lpv2);

	if(i1 < i2) return -1;
	if(i1 > i2) return 1;
	return 0;
}

int QsortUShortComparison(const void *lpv1, const void *lpv2)
{
	unsigned short un1 = *((unsigned short*)lpv1), un2 = *((unsigned short*)lpv2);

	if(un1 < un2) return -1;
	if(un1 > un2) return 1;
	return 0;
}


/* ListViewIntegerComparison
 *   Callback function for comparing LPARAMs. Useful for sorting ListView items.
 *
 * Parameters:
 *   LPARAM		lParam1, lParam2	Values to compare.
 *   LPARAM		lParamSort			An extra parameter. Unused.
 *
 * Return value: int
 *   0 if lParam1 == lParam2; < 0 if lParam1 < lParam2; > 0 otherwise.
 */
int CALLBACK ListViewIntegerComparison(LPARAM lParam1, LPARAM lParam2, LPARAM lParamSort)
{
	UNREFERENCED_PARAMETER(lParamSort);
	return lParam1 - lParam2;
}


/* InitialiseDynamicIntArray
 *   Initialises a dynamic integer array, allocating memory for an in initial
 *   buffer.
 *
 * Parameters:
 *   DYNAMICINTARRAY*	lpdiarray		Pointer to array structure.
 *   unsigned int		ciInitialSize	Number of ints for which memory must be
 *										allocated initially.
 *
 * Return value: None.
 *
 * Remarks:
 *   Call FreeDynamicIntArray to free the memory.
 */
void InitialiseDynamicIntArray(DYNAMICINTARRAY *lpdiarray, unsigned int ciInitialSize)
{
	lpdiarray->uiCount = 0;
	lpdiarray->uiBufSize = ciInitialSize;
	lpdiarray->lpiIndices = ProcHeapAlloc(ciInitialSize * sizeof(int));
}


/* FreeDynamicIntArray
 *   Frees the memory used by a dynamic integer array.
 *
 * Parameters:
 *   DYNAMICINTARRAY*	lpdiarray		Pointer to array structure.
 *
 * Return value: None.
 *
 * Remarks:
 *   Does not free the array structure itself: only the integer buffer.
 */
void FreeDynamicIntArray(DYNAMICINTARRAY *lpdiarray)
{
	ProcHeapFree(lpdiarray->lpiIndices);
}

/* AddToDynamicIntArray
 *   Adds the value to the end of a dynamic array of integers, allocating more
 *   memory if necessary.
 *
 * Parameters:
 *   DYNAMICINTARRAY*	lpdiarray	Pointer to array structure.
 *   int				iValue		Value to add.
 *
 * Return value: None.
 */
void AddToDynamicIntArray(DYNAMICINTARRAY *lpdiarray, int iValue)
{
	/* Do we need to allocate more space? */
	if(lpdiarray->uiCount == lpdiarray->uiBufSize)
	{
		/* Double the buffer length. */
		lpdiarray->uiBufSize <<= 1;

		/* Reallocate. */
		lpdiarray->lpiIndices = ProcHeapReAlloc(lpdiarray->lpiIndices, lpdiarray->uiBufSize * sizeof(int));
	}

	lpdiarray->lpiIndices[lpdiarray->uiCount++] = iValue;
}


/* SortDynamicIntArray
 *  Sorts a dynamic integer array in ascending order.
 *
 * Parameters:
 *   DYNAMICINTARRAY*	lpdiarray	Pointer to array structure.
 *
 * Return value: None.
 */
void SortDynamicIntArray(DYNAMICINTARRAY *lpdiarray)
{
	qsort(lpdiarray->lpiIndices, lpdiarray->uiCount, sizeof(int), QsortIntegerComparison);
}



/* LoadAndFormatFilterString
 *   Loads a string from the string table and replaces tabs with NULs. This gets
 *   around a limitation of the resource compiler.
 *
 * Parameters:
 *   USHORT		unStringID		ID of string resource.
 *   LPTSTR		szFilter		Buffer in which to return string.
 *   UINT		cchFilter		Size of buffer in characters, including space
 *								for terminators (both of them!).
 *
 * Return value: None.
 */
void LoadAndFormatFilterString(USHORT unStringID, LPTSTR szFilter, UINT cchFilter)
{
	TCHAR *szInFilter = szFilter;

	/* Get filter string from string table. Need TWO NULs, remember!
	 */
	LoadString(g_hInstance, unStringID, szFilter, cchFilter-1);
	szFilter[cchFilter - 1] = TEXT('\0');

	/* Replace tabs with NULs. The resource compiler doesn't like
	 * NULs embedded in strings, so we have to do it.
	 */
	do
	{
		if(*szInFilter == TEXT('\t'))
			*szInFilter = TEXT('\0');
	}
	while(*(++szInFilter));
}


/* Log2Floor
 *   Returns the integer part of the logarithm in base 2 of an integer.
 *
 * Parameters:
 *   unsigned int	ui	Number whose logarithm is to be taken.
 *
 * Return value: char
 *   Floor of the logarithm, or -1 if ui == 0.
 */
char Log2Floor(unsigned int ui)
{
	char c = 0;
	if(!ui) return -1;
	while(ui >>= 1) c++;
	return c;
}
