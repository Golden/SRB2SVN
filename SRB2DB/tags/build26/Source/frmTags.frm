VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form frmTags 
   BorderStyle     =   5  'Sizable ToolWindow
   Caption         =   "Tag browser"
   ClientHeight    =   2235
   ClientLeft      =   60
   ClientTop       =   300
   ClientWidth     =   2535
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2235
   ScaleWidth      =   2535
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Begin MSComctlLib.ListView lstObjects 
      Height          =   1695
      Left            =   0
      TabIndex        =   1
      Top             =   360
      Width           =   2535
      _ExtentX        =   4471
      _ExtentY        =   2990
      View            =   3
      LabelEdit       =   1
      Sorted          =   -1  'True
      LabelWrap       =   -1  'True
      HideSelection   =   0   'False
      FullRowSelect   =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      NumItems        =   2
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Text            =   "Tag"
         Object.Width           =   1270
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Text            =   "Objects"
         Object.Width           =   2540
      EndProperty
   End
   Begin VB.ComboBox cmbType 
      Height          =   315
      ItemData        =   "frmTags.frx":0000
      Left            =   0
      List            =   "frmTags.frx":000A
      Style           =   2  'Dropdown List
      TabIndex        =   2
      Top             =   0
      Width           =   2535
   End
   Begin VB.TextBox txtFocusDummy 
      Height          =   285
      Left            =   840
      TabIndex        =   0
      Text            =   "Text1"
      Top             =   0
      Width           =   615
   End
   Begin VB.Menu mnuList 
      Caption         =   "List Context Menu"
      Visible         =   0   'False
      Begin VB.Menu itmListSelect 
         Caption         =   "&Select and zoom"
      End
      Begin VB.Menu itmListProperties 
         Caption         =   "&Properties"
      End
   End
End
Attribute VB_Name = "frmTags"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit


Private Tags As Dictionary
Private LastButton As MouseButtonConstants


Private Sub cmbType_Click()
     RefreshTagList
     
     Select Case cmbType.ListIndex
     Case 0
          lstObjects.ColumnHeaders(2).Text = "Sectors"
     Case 1
          lstObjects.ColumnHeaders(2).Text = "Linedefs"
     End Select
     
End Sub


Private Sub cmbType_GotFocus()
     ' If the user really wants to set the focus here, then let them.
     'txtFocusDummy.SetFocus
End Sub

Private Sub Form_Activate()
     txtFocusDummy.SetFocus
End Sub

Private Sub Form_Load()

     cmbType.ListIndex = 0
     RefreshTagList

End Sub



Private Sub Form_Resize()

     lstObjects.height = ScaleHeight - lstObjects.top - 8
     lstObjects.width = ScaleWidth - lstObjects.left - 8
     cmbType.width = ScaleWidth - cmbType.left - 8

End Sub


Public Sub RefreshTagList()

     Dim i As Long, t As Long, n As Long
     Dim TagColl As Dictionary
     Dim Indices As Variant
     Dim v As Variant
     Dim li As ListItem
     
     Set Tags = New Dictionary
     
     
     Select Case cmbType.ListIndex
     Case 0
          n = numsectors
     Case 1
          n = numlinedefs
     End Select
     
     For i = 0 To n - 1
     
          Select Case cmbType.ListIndex
          Case 0
               t = sectors(i).Tag
          Case 1
               t = linedefs(i).Tag
          End Select
     
          ' Take zero as 'no tag'.
          If t <> 0 Then
          
               If Not Tags.Exists(t) Then Tags.Add t, New Dictionary
               Set TagColl = Tags(t)
               
               TagColl.Add CStr(i), i
               
          End If
          
     Next i
     
     ' Populate the ListView.
     lstObjects.ListItems.Clear
     Indices = Tags.Keys()
     For Each v In Indices
     
          ' Pad with spaces to get numerical sorting...
          Set li = lstObjects.ListItems.Add(, "X" & CStr(v), Format$(CStr(v), "@@@@@"))
          li.SubItems(1) = SelectionToString(Tags(v), True)
     
     Next v

End Sub

Private Sub itmListProperties_Click()

     Dim OldSel As Dictionary
     Dim OldNumSel As Long
     
     ' Keep the old selection dictionary.
     Set OldSel = selected
     OldNumSel = numselected
     
     Set selected = Tags(Val(lstObjects.SelectedItem.Text))
     numselected = selected.Count
     
     ' Show properties dialogue.
     Select Case cmbType.ListIndex
     Case 0
          frmSector.Show vbModal, frmMain
     Case 1
          frmLinedef.Show vbModal, frmMain
     End Select
     
     ' Restore selection dictionary.
     Set selected = OldSel
     numselected = OldNumSel

End Sub

Private Sub itmListSelect_Click()
     SelAndJumpItem
End Sub

Private Sub lstObjects_DblClick()

     If LastButton = vbLeftButton And Not (lstObjects.SelectedItem Is Nothing) Then
          SelAndJumpItem
     End If

End Sub

Private Sub SelAndJumpItem()

     Dim SelType As ENUM_EDITMODE

     Select Case cmbType.ListIndex
     Case 0
          SelType = EM_SECTORS
     Case 1
          SelType = EM_LINES
     End Select
     
     SelectAndJump Tags(Val(lstObjects.SelectedItem.Text)), SelType

End Sub


Private Sub lstObjects_GotFocus()
     txtFocusDummy.SetFocus
End Sub

Private Sub lstObjects_ItemClick(ByVal Item As MSComctlLib.ListItem)

     If LastButton = vbRightButton Then
          PopupMenu mnuList
     End If

End Sub

Private Sub lstObjects_MouseDown(Button As Integer, Shift As Integer, x As Single, y As Single)

     Set lstObjects.SelectedItem = lstObjects.HitTest(x, y)
     LastButton = Button

End Sub


Private Sub txtFocusDummy_KeyDown(KeyCode As Integer, Shift As Integer)
     frmMain.Form_KeyDown KeyCode, Shift
End Sub

Private Sub txtFocusDummy_KeyUp(KeyCode As Integer, Shift As Integer)
     frmMain.Form_KeyUp KeyCode, Shift
End Sub



