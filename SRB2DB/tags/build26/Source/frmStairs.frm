VERSION 5.00
Begin VB.Form frmStairs 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Convert Sector to Stairs"
   ClientHeight    =   4185
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   4710
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmStairs.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4185
   ScaleWidth      =   4710
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton cmdCancel 
      Cancel          =   -1  'True
      Caption         =   "Cancel"
      Height          =   375
      Left            =   2520
      TabIndex        =   18
      Top             =   3720
      Width           =   1575
   End
   Begin VB.CommandButton cmdOK 
      Caption         =   "OK"
      Default         =   -1  'True
      Height          =   375
      Left            =   600
      TabIndex        =   17
      Top             =   3720
      Width           =   1575
   End
   Begin VB.Frame Frame4 
      Caption         =   "Fa�ade Texture"
      Height          =   1815
      Left            =   120
      TabIndex        =   6
      Top             =   1800
      Width           =   1695
      Begin VB.PictureBox picTFacade 
         BackColor       =   &H8000000C&
         CausesValidation=   0   'False
         ClipControls    =   0   'False
         HasDC           =   0   'False
         Height          =   1020
         Left            =   360
         ScaleHeight     =   64
         ScaleMode       =   3  'Pixel
         ScaleWidth      =   64
         TabIndex        =   15
         TabStop         =   0   'False
         ToolTipText     =   "Ceiling Texture"
         Top             =   240
         Width           =   1020
         Begin VB.Image imgTFacade 
            Height          =   960
            Left            =   0
            Stretch         =   -1  'True
            ToolTipText     =   "Ceiling Texture"
            Top             =   0
            Width           =   960
         End
      End
      Begin VB.TextBox txtTFacade 
         Height          =   315
         Left            =   360
         MaxLength       =   8
         TabIndex        =   14
         Text            =   "-"
         Top             =   1320
         Width           =   1020
      End
   End
   Begin VB.Frame Frame3 
      Caption         =   "Steps"
      Height          =   2775
      Left            =   1920
      TabIndex        =   5
      Top             =   840
      Width           =   2655
      Begin VB.OptionButton optHeight 
         Caption         =   "Height"
         Height          =   255
         Left            =   480
         TabIndex        =   13
         Top             =   2040
         Width           =   975
      End
      Begin VB.OptionButton optWidth 
         Caption         =   "Width"
         Height          =   255
         Left            =   480
         TabIndex        =   12
         Top             =   1800
         Value           =   -1  'True
         Width           =   1095
      End
      Begin DoomBuilder.ctlValueBox ctlValTotalHeight 
         Height          =   375
         Left            =   1560
         TabIndex        =   10
         Top             =   840
         Width           =   855
         _ExtentX        =   1508
         _ExtentY        =   661
         Max             =   9999
         Value           =   "256"
      End
      Begin DoomBuilder.ctlValueBox ctlValNumSteps 
         Height          =   375
         Left            =   1560
         TabIndex        =   8
         Top             =   420
         Width           =   855
         _ExtentX        =   1508
         _ExtentY        =   661
         Max             =   9999
         Min             =   2
         Value           =   "8"
      End
      Begin VB.Label Label3 
         Caption         =   "Distribute by:"
         Height          =   255
         Left            =   240
         TabIndex        =   11
         Top             =   1440
         Width           =   1215
      End
      Begin VB.Label Label2 
         Caption         =   "Total height:"
         Height          =   255
         Left            =   240
         TabIndex        =   9
         Top             =   900
         Width           =   1095
      End
      Begin VB.Label Label1 
         Caption         =   "Number:"
         Height          =   255
         Left            =   240
         TabIndex        =   7
         Top             =   480
         Width           =   1095
      End
   End
   Begin VB.Frame Frame2 
      Caption         =   "Curve"
      Height          =   855
      Left            =   120
      TabIndex        =   3
      Top             =   840
      Width           =   1695
      Begin VB.OptionButton optElliptical 
         Caption         =   "Elliptical"
         Height          =   255
         Left            =   240
         TabIndex        =   16
         Top             =   480
         Width           =   975
      End
      Begin VB.OptionButton optLinear 
         Caption         =   "Linear"
         Height          =   255
         Left            =   240
         TabIndex        =   4
         Top             =   240
         Value           =   -1  'True
         Width           =   855
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "Adjust"
      Height          =   615
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   4455
      Begin VB.CheckBox chkCeil 
         Caption         =   "Ceilings"
         Height          =   255
         Left            =   1920
         TabIndex        =   2
         Top             =   240
         Width           =   1455
      End
      Begin VB.CheckBox chkFloor 
         Caption         =   "Floors"
         Height          =   255
         Left            =   360
         TabIndex        =   1
         Top             =   240
         Value           =   1  'Checked
         Width           =   1455
      End
   End
End
Attribute VB_Name = "frmStairs"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private minx As Long, MinY As Long, MaxX As Long, MaxY As Long
Private sector As Long

Private Sub chkCeil_Click()
     CheckOKButton
End Sub

Private Sub chkFloor_Click()
     CheckOKButton
End Sub

Private Sub cmdCancel_Click()
     Unload Me
End Sub

Private Sub cmdOK_Click()

     Dim Abscissae() As Long, Heights() As Long ', AllHeights() As Long
     Dim i As Long, j As Long
     Dim remaining As Long
     Dim SaveSnap As Boolean
     Dim SaveStitch As Long
     Dim NewSectors As New Dictionary
     
     CreateUndo "stairs"

     ReDim Abscissae(0 To Val(ctlValNumSteps.Value) - 1)
     
     ' Throughout, we use an extra step.
     
     ' Width calculations.
     remaining = MaxX - minx
     For i = 0 To UBound(Abscissae)
     
          If optLinear.Value Or optWidth.Value Then
               Abscissae(i) = remaining / (UBound(Abscissae) + 1 - i)
               If i > 0 Then Abscissae(i) = Abscissae(i) + Abscissae(i - 1)
               remaining = MaxX - minx - Abscissae(i)
          Else
               Do While EllipseQ4YFromX(MaxX - minx - remaining, MaxX - minx, Val(ctlValTotalHeight.Value)) < ((i + 1) * Val(ctlValTotalHeight.Value) / Val(ctlValNumSteps.Value))
                    remaining = remaining - 1
               Loop
                    
               Abscissae(i) = MaxX - minx - remaining
          End If
          
     Next i
     
     ' We might have consecutive steps with the same abscissa, in which case
     ' we must recalculate the number of steps.
     
     j = 1     ' Keeps track of the steps we use.
     For i = 1 To UBound(Abscissae)
          If Abscissae(i) > Abscissae(i - 1) Then
               Abscissae(j) = Abscissae(i)
               j = j + 1
          End If
     Next i
     
     ReDim Preserve Abscissae(0 To j - 1)
     ctlValNumSteps.Value = j
     
     
     
     ' Heights, now.
     
     ReDim Heights(0 To Val(ctlValNumSteps.Value))
     
     ' Step 0 is NOT A STEP.
     Heights(0) = 0
     
     ' Top step has max height.
     Heights(Val(ctlValNumSteps.Value)) = Val(ctlValTotalHeight.Value)
     
     ' Individual height calculations.
     If optLinear.Value Or optHeight.Value Then
     
          ' Just gradient heights linearly.
          remaining = Heights(UBound(Heights))
          For i = 1 To UBound(Heights) - 1
               Heights(i) = Heights(i - 1) + remaining / (UBound(Heights) - i + 1)
               remaining = Heights(UBound(Heights)) - Heights(i)
          Next i
     Else

          ' Elliptical heights.
          For i = 1 To UBound(Heights) - 1
               Heights(i) = EllipseQ4YFromX(Abscissae(i), MaxX - minx, Val(ctlValTotalHeight.Value))
          Next i
          
     End If
     
     
     ' Make the sectors!
     
     ' The left-most step is the original sector. Its heights are set
     ' at the END, so everything else is relative to its original
     ' heights.
     
     ' Save current snap mode and stitch distance.
     SaveSnap = snapmode
     snapmode = False
     SaveStitch = Config("autostitchdistance")
     Config("autostitchdistance") = 0
          
     For i = 1 To UBound(Abscissae)
          
          frmMain.StartDrawOperation False
               
          ' Four vertices.
          frmMain.DrawVertexHere minx + Abscissae(i - 1), -MinY, False
          frmMain.DrawVertexHere minx + Abscissae(i - 1), -MaxY, False
          
          ' Split sector.
          frmMain.EndDrawOperation False
          
          WithdrawUndo
          
          If chkFloor.Value = vbChecked Then sectors(frmMain.NewSec).HFloor = sectors(sector).HFloor + Heights(i + 1)
          If chkCeil.Value = vbChecked Then sectors(frmMain.NewSec).hceiling = sectors(sector).hceiling - Heights(i + 1)
          
          ' We'll need this later.
          NewSectors.Add frmMain.NewSec, 0
          
     Next i
     
     If chkFloor.Value = vbChecked Then sectors(sector).HFloor = sectors(sector).HFloor + Heights(1)
     If chkCeil.Value = vbChecked Then sectors(sector).hceiling = sectors(sector).hceiling - Heights(1)
     
     ' We want to fix the original sector's textures, too.
     NewSectors.Add sector, 0
     
     
     
     
     
     ' KLUDGE * KLUDGE * KLUDGE
     ' I noticed a nasty bug, and since I promised I'd commit within an hour...
     ' I'll replace this ASAP.
     ' The right-most step's sidedefs' sectors referred to the left-most step.
     ' And there's (possibly) still a problem if you create exactly two steps.
     ' KLUDGE * KLUDGE * KLUDGE
     
     'For i = 0 To numlinedefs - 1
     '     If linedefs(i).s1 >= 0 And linedefs(i).s2 >= 0 Then
     '          If sidedefs(linedefs(i).s1).sector = sector And _
     '             vertexes(linedefs(i).v1).x > minx + Abscissae(1) Then
     '               j = sidedefs(linedefs(i).s1).sector
     '               sidedefs(linedefs(i).s1).sector = sidedefs(linedefs(i).s2).sector
     '               sidedefs(linedefs(i).s2).sector = j
     '          End If
     '     End If
     'Next i
     
     ' End kludge.
     
     
     
     
     ' Restore snap mode and stitch distance.
     snapmode = SaveSnap
     Config("autostitchdistance") = SaveStitch
     
     
     ' Texture-fixing time.
     If IsTextureName(txtTFacade.Text) Then
          For i = 0 To numsidedefs - 1
          
               ' Does this sd belong to one of our sectors?
               If NewSectors.Exists(sidedefs(i).sector) Then
                    
                    ' Other sd please.
                    j = linedefs(sidedefs(i).linedef).s1
                    If j = i Then j = linedefs(sidedefs(i).linedef).s2
                    
                    If chkFloor.Value = vbChecked Then
                         sidedefs(i).Lower = txtTFacade.Text
                         If j >= 0 Then sidedefs(j).Lower = txtTFacade.Text
                    End If
                    
                    If chkCeil.Value = vbChecked Then
                         sidedefs(i).Upper = txtTFacade.Text
                         If j >= 0 Then sidedefs(j).Upper = txtTFacade.Text
                    End If
                    
               End If
          
          Next i
     End If

     Unload Me

End Sub

Private Sub Form_Load()

     Dim i As Long, ldcount As Long
     Dim ldIsOurs As Boolean

     If selected.Count <> 1 Then
          
          MsgBox "You must select exactly one sector to perform this operation.", vbExclamation
          
          Tag = 1
          Exit Sub
     End If
     
     sector = selected.Items(0)
     
     ' We need a rectangular sector with all its linedefs parallel to an axis.
     ldcount = 0
     minx = 32766
     MinY = 32766
     MaxX = -32767
     MaxY = -32767
     
     For i = 0 To numlinedefs - 1
     
          ldIsOurs = False
          
          If linedefs(i).s1 >= 0 Then
               If sidedefs(linedefs(i).s1).sector = sector Then ldIsOurs = True
          End If
          
          If linedefs(i).s2 >= 0 Then
               If sidedefs(linedefs(i).s2).sector = sector Then ldIsOurs = True
          End If
          
          If ldIsOurs Then
               ldcount = ldcount + 1
               
               If vertexes(linedefs(i).V1).x <> vertexes(linedefs(i).V2).x _
                    And vertexes(linedefs(i).V1).y <> vertexes(linedefs(i).V2).y Then
                    
                    MsgBox "All linedefs must be parallel to an axis.", vbExclamation
                    
                    Tag = 1
                    Exit Sub
               End If
               
               If vertexes(linedefs(i).V1).x < minx Then minx = vertexes(linedefs(i).V1).x
               If vertexes(linedefs(i).V1).y < MinY Then MinY = vertexes(linedefs(i).V1).y
               If vertexes(linedefs(i).V2).x < minx Then minx = vertexes(linedefs(i).V2).x
               If vertexes(linedefs(i).V2).y < MinY Then MinY = vertexes(linedefs(i).V2).y
               
               If vertexes(linedefs(i).V1).x > MaxX Then MaxX = vertexes(linedefs(i).V1).x
               If vertexes(linedefs(i).V1).y > MaxY Then MaxY = vertexes(linedefs(i).V1).y
               If vertexes(linedefs(i).V2).x > MaxX Then MaxX = vertexes(linedefs(i).V2).x
               If vertexes(linedefs(i).V2).y > MaxY Then MaxY = vertexes(linedefs(i).V2).y
          
          End If
          
     Next i
     
     If ldcount <> 4 Then
     
          MsgBox "The selected sector must have exactly four linedefs.", vbExclamation
          
          Tag = 1
          Exit Sub
          
     End If
     
     If MaxX - minx <= 1 Then
     
          MsgBox "The selected sector is too narrow.", vbExclamation
          
          Tag = 1
          Exit Sub
          
     End If
     
     ctlValNumSteps.Max = MaxX - minx
     
     If (Val(Config("storeeditinginfo"))) And WadSettings.Exists("defaulttexture") Then
          txtTFacade.Text = WadSettings("defaulttexture")("lower")
     Else
          txtTFacade.Text = Config("defaulttexture")("lower")
     End If
     
     Tag = 0

End Sub


Private Function EllipseQ4YFromX(ByVal x As Double, ByVal a As Double, ByVal b As Double) As Double

     EllipseQ4YFromX = b - Sqr(b * b * (1# - x * x / (a * a)))

End Function

Private Sub imgTFacade_Click()

     txtTFacade.Text = SelectTexture(txtTFacade.Text, Me)

End Sub

Private Sub txtTFacade_Change()

     GetScaledTexturePicture txtTFacade.Text, imgTFacade

End Sub

Private Sub txtTFacade_KeyUp(KeyCode As Integer, Shift As Integer)

     If Val(Config("autocompletetypetex")) Then CompleteTextureName KeyCode, Shift, txtTFacade

End Sub

Private Sub txtTFacade_Validate(Cancel As Boolean)

     If Val(Config("autocompletetex")) Then txtTFacade.Text = GetNearestTextureName(txtTFacade.Text)

End Sub


Private Sub CheckOKButton()

     If chkFloor.Value <> vbChecked And chkCeil.Value <> vbChecked Then
          cmdOK.Enabled = False
     Else
          cmdOK.Enabled = True
     End If

End Sub
