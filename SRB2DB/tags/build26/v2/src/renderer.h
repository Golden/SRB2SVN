#ifndef __SRB2B_RENDERER__
#define __SRB2B_RENDERER__

#ifdef __cplusplus
extern "C" {
#endif

#include <windows.h>
#include <GL/gl.h>
#include <GL/glu.h>

#include "map.h"
#include "CodeImp/ci_const.h"

/* Types. */
typedef struct _MAPVIEW
{
	short	cxDrawSurface, cyDrawSurface;
	float	xLeft, yTop;
	float	fZoom;
	int		iIndicatorSize;
	float	fVertexSize;
	float	fThingSize;
	unsigned short	cxGrid, cyGrid;
	short	xGridOffset, yGridOffset;
	BOOL	bShowGrid;
	GLuint	uiThingTex;
} MAPVIEW;

typedef struct _DRAWOP_RENDERINFO
{
	POINT	ptSrc, ptDest;
} DRAWOP_RENDERINFO;


/* Constants. */
#define PALETTE_16COLORS_OFFSET 32
#define PALETTE_16COLORSDIMMED_OFFSET 48

/* extern globals. */

/* These are used by CodeImp's renderer. */
extern int g_cxClip;
extern int g_cyClip;
extern BYTE *g_lpbyDraw;

/* These globals are ours. */
extern RGBQUAD g_rgbqPalette[];

/* Prototypes. */
void InitialiseRenderer(void);
void ShutdownRenderer(void);
void LoadUserPalette(void);
void RedrawMap(MAP *lpmap, ENUM_EDITMODE editmode, ENUM_EDITSUBMODE submode, MAPVIEW *lpmapview, DRAWOP_RENDERINFO *lpdori);
void SetZoom(MAPVIEW *lpmapview, float fZoom);
GLvoid InitGL(GLvoid);
GLuint LoadThingCircleTexture(GLvoid);
GLvoid ReSizeGLScene(GLsizei cx, GLsizei cy);
void __fastcall Render_LineF(float x1, float y1, float x2, float y2, BYTE byColour);
void __fastcall Render_SquareF(float xCentre, float yCentre, float fHalfSize, BYTE byColour);
void __fastcall Render_ThingPalettedF(float xCentre, float yCentre, int iAngle, BYTE byPalColour, float fSize, float fZoom, GLuint uiThingTex);
void __fastcall Render_ThingF(float xCentre, float yCentre, int iAngle, int iColour, float fSize, float fZoom, GLuint uiThingTex);

void __fastcall Render_BoxF(int xCentre, int yCentre, int iRadius, BYTE byColour);
void __fastcall Render_CircleF(float xCentre, float yCentre, float fRadius, int iColour, GLuint uiThingTex);
void __fastcall Render_BitmapF(BYTE* bitmap, int width, int height, int sx, int sy, int sw, int sh, int tx, int ty, BYTE c1, BYTE c2);
void __fastcall Render_ScaledBitmapF(BYTE* bitmap, int width, int height, int sx, int sy, int sw, int sh, int tx, int ty, BYTE c1, BYTE c2);

void ZoomToRect(MAPVIEW *lpmapview, RECT *lprc, int iBorderPercentage);
void CentreViewAt(MAPVIEW *lpmapview, int x, int y);

#ifdef __cplusplus
}
#endif

#endif
