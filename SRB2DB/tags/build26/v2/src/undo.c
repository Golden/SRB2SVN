#include <windows.h>

#include "general.h"
#include "map.h"
#include "undo.h"


/* Types. */

typedef struct _UNDOMEM
{
	void	*lpv;
	struct _UNDOMEM *lpundomemNext;
} UNDOMEM;

typedef struct _UNDOITEMLIST
{
	unsigned int	uiFirstIndex, uiLastIndex;
	void			*lpvBase;

	struct _UNDOITEMLIST *lpuilNext;
} UNDOITEMLIST;

typedef struct _UNDOFRAME
{
	int		iSectors, iLinedefs, iSidedefs, iVertices, iThings;

	int		iStringIndex;

	UNDOITEMLIST	*lpuilSectors, *lpuilLinedefs, *lpuilSidedefs;
	UNDOITEMLIST	*lpuilVertices, *lpuilThings;

	struct _UNDOFRAME	*lpundoframeNext;
	struct _UNDOFRAME	*lpundoframePrev;
} UNDOFRAME;

struct _UNDOSTACK
{
	UNDOMEM		undomemHdr, *lpundomemLast;
	UNDOFRAME	undoframeBase, *lpundoframeLast;
};


/* Static prototypes. */

static UNDOITEMLIST* CreateUndoListNodeFromData(UNDOSTACK *lpundostack, unsigned int uiFirstIndex, unsigned int uiLastIndex, unsigned int cbRecord, void *lpvBase);
static void FreeUndoFrameItems(UNDOFRAME *lpundoframe);
static void FreeUndoItemList(UNDOITEMLIST *lpuil);
static UNDOITEMLIST* CreateUndoItemList(UNDOSTACK *lpundostack, UNDOITEMLIST *lpuilLastFrameList, void *lpvData, UINT uiRecords, UINT cbRecord, UINT cbCompare);
static void ApplyUndoItemList(UNDOITEMLIST *lpuil, BYTE *lpbyData, UINT cbRecord);


/* CreateUndoStack
 *   Creates a new undo stack for a map.
 *
 * Parameters:
 *   MAP*			lpmap		Map.
 *
 * Return value: UNDOSTACK*
 *   Pointer to new undo stack.
 */
UNDOSTACK* CreateUndoStack(MAP *lpmap, int iStringIndex)
{
	UNDOSTACK *lpundostack = (UNDOSTACK*)ProcHeapAlloc(sizeof(UNDOSTACK));

	/* Set up the last pointers. */
	lpundostack->lpundoframeLast = &lpundostack->undoframeBase;
	lpundostack->lpundomemLast = &lpundostack->undomemHdr;

	/* No memory allocated initially. */
	lpundostack->undomemHdr.lpundomemNext = NULL;

	/* The base frame is the only one to start with, and is the entire map. */
	lpundostack->undoframeBase.lpundoframeNext = NULL;
	lpundostack->undoframeBase.lpundoframePrev = NULL;

	/* Set the description. */
	lpundostack->undoframeBase.iStringIndex = iStringIndex;

	/* Store the counts. */
	lpundostack->undoframeBase.iSectors = lpmap->iSectors;
	lpundostack->undoframeBase.iLinedefs = lpmap->iLinedefs;
	lpundostack->undoframeBase.iSidedefs = lpmap->iSidedefs;
	lpundostack->undoframeBase.iVertices = lpmap->iVertices;
	lpundostack->undoframeBase.iThings = lpmap->iThings;

	/* Copy the data into the base frame. */
	lpundostack->undoframeBase.lpuilSectors = lpmap->iSectors > 0 ? CreateUndoListNodeFromData(lpundostack, 0, lpmap->iSectors - 1, sizeof(MAPSECTOR), lpmap->sectors) : NULL;
	lpundostack->undoframeBase.lpuilLinedefs = lpmap->iLinedefs > 0 ? CreateUndoListNodeFromData(lpundostack, 0, lpmap->iLinedefs - 1, sizeof(MAPLINEDEF), lpmap->linedefs) : NULL;
	lpundostack->undoframeBase.lpuilSidedefs = lpmap->iSidedefs > 0 ? CreateUndoListNodeFromData(lpundostack, 0, lpmap->iSidedefs - 1, sizeof(MAPSIDEDEF), lpmap->sidedefs) : NULL;
	lpundostack->undoframeBase.lpuilVertices = lpmap->iVertices > 0 ? CreateUndoListNodeFromData(lpundostack, 0, lpmap->iVertices - 1, sizeof(MAPVERTEX), lpmap->vertices) : NULL;
	lpundostack->undoframeBase.lpuilThings = lpmap->iThings > 0 ? CreateUndoListNodeFromData(lpundostack, 0, lpmap->iThings - 1, sizeof(MAPTHING), lpmap->things) : NULL;

	/* Done! Return the new stack. */
	return lpundostack;
}


/* CreateUndoListNodeFromData
 *   Creates a new undo items list node.
 *
 * Parameters:
 *   UNDOSTACK*		lpundostack		Undo stack with which to associate the
 *									allocated memory.
 *   unsigned int	uiFirstIndex	Index of first item to copy.
 *   unsigned int	uiLastIndex		Index of last item to copy.
 *   unsigned int	cbRecord		Size of each item, in bytes.
 *   void*			lpvBase			Buffer containing the data to copy.
 *
 * Return value: UNDOITEMLIST*
 *   Pointer to new undo item list node.
 *
 * Remarks:
 *   The node is not added to any list; the pointer to the stack passed in is
 *   just so that the memory allocated for the data can be associated with the
 *   stack.
 */
static UNDOITEMLIST* CreateUndoListNodeFromData(UNDOSTACK *lpundostack, unsigned int uiFirstIndex, unsigned int uiLastIndex, unsigned int cbRecord, void *lpvBase)
{
	UNDOITEMLIST *lpuil;		/* The new node. */
	unsigned int cbData = (uiLastIndex - uiFirstIndex + 1) * cbRecord;

	/* Add a node to the list of allocated memory. */
	lpundostack->lpundomemLast->lpundomemNext = (UNDOMEM*)ProcHeapAlloc(sizeof(UNDOMEM));
	lpundostack->lpundomemLast = lpundostack->lpundomemLast->lpundomemNext;
	lpundostack->lpundomemLast->lpundomemNext = NULL;

	/* Allocate memory for storage, and copy the data. */
	lpundostack->lpundomemLast->lpv = ProcHeapAlloc(cbData);
	CopyMemory(lpundostack->lpundomemLast->lpv, (BYTE*)lpvBase + uiFirstIndex * cbRecord, cbData);

	/* Allocate a new list node and set the pointer to the data. */
	lpuil = (UNDOITEMLIST*)ProcHeapAlloc(sizeof(UNDOITEMLIST));
	lpuil->lpuilNext = NULL;
	lpuil->uiFirstIndex = uiFirstIndex;
	lpuil->uiLastIndex = uiLastIndex;
	lpuil->lpvBase = lpundostack->lpundomemLast->lpv;

	return lpuil;
}


/* FreeUndoStack
 *   Destroys an undo stack and all associated memory.
 *
 * Parameters:
 *   UNDOSTACK*		lpundostack		Stack to free.
 *
 * Return value: None.
 *
 * Remarks:
 *   The stack structure itself is freed.
 */
void FreeUndoStack(UNDOSTACK *lpundostack)
{
	UNDOMEM *lpundomem = lpundostack->undomemHdr.lpundomemNext;
	UNDOMEM *lpundomemNext;
	UNDOFRAME *lpundoframe, *lpundoframeNext;

	/* Free all the memory we allocated for map data. */
	while(lpundomem)
	{
		ProcHeapFree(lpundomem->lpv);
		lpundomemNext = lpundomem->lpundomemNext;
		ProcHeapFree(lpundomem);
		lpundomem = lpundomemNext;
	}

	/* Free the memory used by the base frame, but not the frame itself. */
	FreeUndoFrameItems(&lpundostack->undoframeBase);

	/* Free all the remaining frames and their data. */
	lpundoframe = lpundostack->undoframeBase.lpundoframeNext;
	while(lpundoframe)
	{
		FreeUndoFrameItems(lpundoframe);
		lpundoframeNext = lpundoframe->lpundoframeNext;
		ProcHeapFree(lpundoframe);
		lpundoframe = lpundoframeNext;
	}

	/* Free the stack itself. */
	ProcHeapFree(lpundostack);
}


/* FreeUndoFrameItems
 *   Frees all item lists associated with an undo frame.
 *
 * Parameters:
 *   UNDOFRAME*		lpundoframe		Frame whose items are to be freed.
 *
 * Return value: None.
 *
 * Remarks:
 *   The memory used in the item lists for the map data is not freed, as many
 *   frames may point to the same buffer.
 */
static void FreeUndoFrameItems(UNDOFRAME *lpundoframe)
{
	/* Free the lists for each map type. */
	FreeUndoItemList(lpundoframe->lpuilSectors);
	FreeUndoItemList(lpundoframe->lpuilLinedefs);
	FreeUndoItemList(lpundoframe->lpuilSidedefs);
	FreeUndoItemList(lpundoframe->lpuilVertices);
	FreeUndoItemList(lpundoframe->lpuilThings);
}


/* FreeUndoItemList
 *   Frees an undo item list.
 *
 * Parameters:
 *   UNDOITEMLIST*	lpuil	List to be freed.
 *
 * Return value: None.
 *
 * Remarks:
 *   The memory used in the item lists for the map data is not freed, as many
 *   frames may point to the same buffer.
 */
static void FreeUndoItemList(UNDOITEMLIST *lpuil)
{
	UNDOITEMLIST *lpuilNext;

	while(lpuil)
	{
		lpuilNext = lpuil->lpuilNext;
		ProcHeapFree(lpuil);
		lpuil = lpuilNext;
	}
}


/* PushNewUndoFrame
 *   Creates a new undo frame from a map, and adds it to an undo stack.
 *
 * Parameters:
 *   UNDOSTACK*	lpundostack		Undo stack to add to.
 *   MAP*		lpmap			Pointer to map.
 *   int		iStringIndex	ID of resource string to display in menus.
 *
 * Return value: None.
 */
void PushNewUndoFrame(UNDOSTACK *lpundostack, MAP *lpmap, int iStringIndex)
{
	UNDOFRAME *lpundoframe = (UNDOFRAME*)ProcHeapAlloc(sizeof(UNDOFRAME));

	/* Store the numbers of each type of object. */
	lpundoframe->iSectors = lpmap->iSectors;
	lpundoframe->iLinedefs = lpmap->iLinedefs;
	lpundoframe->iSidedefs = lpmap->iSidedefs;
	lpundoframe->iVertices = lpmap->iVertices;
	lpundoframe->iThings = lpmap->iThings;

	/* Store the string index. */
	lpundoframe->iStringIndex = iStringIndex;

	/* Build the item lists for each type of object. */
	lpundoframe->lpuilSectors = CreateUndoItemList(lpundostack, lpundostack->lpundoframeLast->lpuilSectors, lpmap->sectors, lpmap->iSectors, sizeof(MAPSECTOR), SECTORRECORDSIZE);
	lpundoframe->lpuilLinedefs = CreateUndoItemList(lpundostack, lpundostack->lpundoframeLast->lpuilLinedefs, lpmap->linedefs, lpmap->iLinedefs, sizeof(MAPLINEDEF), LINEDEFRECORDSIZE);
	lpundoframe->lpuilSidedefs = CreateUndoItemList(lpundostack, lpundostack->lpundoframeLast->lpuilSidedefs, lpmap->sidedefs, lpmap->iSidedefs, sizeof(MAPSIDEDEF), SIDEDEFRECORDSIZE);
	lpundoframe->lpuilVertices = CreateUndoItemList(lpundostack, lpundostack->lpundoframeLast->lpuilVertices, lpmap->vertices, lpmap->iVertices, sizeof(MAPVERTEX), VERTEXRECORDSIZE);
	lpundoframe->lpuilThings = CreateUndoItemList(lpundostack, lpundostack->lpundoframeLast->lpuilThings, lpmap->things, lpmap->iThings, sizeof(MAPTHING), THINGRECORDSIZE);

	/* Add the new frame to the list. */
	lpundoframe->lpundoframePrev = lpundostack->lpundoframeLast;
	lpundoframe->lpundoframeNext = NULL;
	lpundostack->lpundoframeLast->lpundoframeNext = lpundoframe;
	lpundostack->lpundoframeLast = lpundoframe;
}



/* CreateUndoItemList
 *   Creates a new undo item list from a block of data wrt a previous list.
 *
 * Parameters:
 *   UNDOSTACK*		lpundostack			Undo stack to associate the allocated
 *										memory with.
 *   UNDOITEMLIST*	lpuilLastFrameList	Corresponding list in the previous frame.
 *   void*			lpvData				Data for the items in the new list.
 *   UINT			uiRecords			Number of objects in the current state.
 *   UINT			cbRecord			Size of each object.
 *   UNIT			cbCompare			The number of bytes at the start of each
 *										object that matter for equality.
 *
 * Return value: UNDOITEMLIST*
 *   Pointer to new item list.
 */
static UNDOITEMLIST* CreateUndoItemList(UNDOSTACK *lpundostack, UNDOITEMLIST *lpuilLastFrameList, void *lpvData, UINT uiRecords, UINT cbRecord, UINT cbCompare)
{
	BOOL bCurrentMatchState;
	BOOL bStartNewNode = TRUE;
	unsigned int ui, uiLow;

	UNDOITEMLIST *lpuilNew = NULL;
	UNDOITEMLIST *lpuilCurrent;
	UNDOITEMLIST **lplpuilNext = &lpuilNew;

	ui = uiLow = 0;

	while(lpuilLastFrameList && ui < uiRecords)
	{
		BOOL bNextMatchState;
		BOOL bDifferentMatchStates = FALSE;

		if(bStartNewNode)
		{
			/* Start a new node. */
			uiLow = ui;

			/* Assume we don't need a new node next time. */
			bStartNewNode = FALSE;
		}

		/* Do we match? */
		bCurrentMatchState = (memcmp((BYTE*)lpvData + cbRecord * ui, (BYTE*)lpuilLastFrameList->lpvBase + cbRecord * (ui - lpuilLastFrameList->uiFirstIndex), cbCompare) == 0);

		/* Find the next match state, if such a thing exists. */
		if(ui < uiRecords - 1 && ui < lpuilLastFrameList->uiLastIndex)
		{
			bNextMatchState = (memcmp((BYTE*)lpvData + cbRecord * (ui + 1), (BYTE*)lpuilLastFrameList->lpvBase + cbRecord * (ui + 1 - lpuilLastFrameList->uiFirstIndex), cbCompare) == 0);
			bDifferentMatchStates = (bNextMatchState && !bCurrentMatchState) || (!bNextMatchState && bCurrentMatchState);
		}

		/* Need to finish this node? */
		if(ui == uiRecords - 1 || lpuilLastFrameList->uiLastIndex == ui || bDifferentMatchStates)
		{
			/* This is the last element for this node, so start a new one next
			 * time.
			 */
			bStartNewNode = TRUE;

			/* Set the data pointer. How we do this depends on whether it's data
			 * that's the same as the previous frame.
			 */
			if(bCurrentMatchState)
			{
				/* We're same as the frame below, so just calculate an offset
				 * from the base.
				 */
				lpuilCurrent = *lplpuilNext = (UNDOITEMLIST*)ProcHeapAlloc(sizeof(UNDOITEMLIST));
				lplpuilNext = &lpuilCurrent->lpuilNext;
				lpuilCurrent->lpuilNext = NULL;
				lpuilCurrent->uiFirstIndex = uiLow;
				lpuilCurrent->uiLastIndex = ui;
				lpuilCurrent->lpvBase = (BYTE*)lpuilLastFrameList->lpvBase + (uiLow - lpuilLastFrameList->uiFirstIndex) * cbRecord;
			}
			else
			{
				/* This node is for objects that have changed since the last
				 * frame, so make a copy of them.
				 */
				lpuilCurrent = *lplpuilNext = CreateUndoListNodeFromData(lpundostack, uiLow, ui, cbRecord, lpvData);
				lplpuilNext = &lpuilCurrent->lpuilNext;
			}

			/* Additionally advance to the next section if we've reached the
			 * end of this one.
			 */
			if(lpuilLastFrameList->uiLastIndex == ui)
				lpuilLastFrameList = lpuilLastFrameList->lpuilNext;
		}

		/* Next object. */
		ui++;
	}

	/* NB: bCurrentMatchState is undefined if we've run out of records. */

	/* If there are still any objects remaining, they can't correspond to any
	 * from the previous frame (since in this case all the frames have gone),
	 * so copy them.
	 */
	if(ui < uiRecords)
		lpuilCurrent = *lplpuilNext = CreateUndoListNodeFromData(lpundostack, ui, uiRecords - 1, cbRecord, lpvData);

	/* Done! Return the beginning of the list. */
	return lpuilNew;
}


/* GetUndoTopStringIndex
 *   Retrieves resource string index associated with frame at the top of an undo
 *   stack.
 *
 * Parameters:
 *   UNDOSTACK*		lpundostack		Undo stack.
 *
 * Return value: int
 *   Index of string.
 */
int GetUndoTopStringIndex(UNDOSTACK *lpundostack)
{
	return lpundostack->lpundoframeLast->iStringIndex;
}


/* UndoStackHasOnlyOneFrame
 *   Determines whether an undo stack has only one frame.
 *
 * Parameters:
 *   UNDOSTACK*		lpundostack		Undo stack.
 *
 * Return value: BOOL
 *   TRUE if the stack has only one frame; FALSE otherwise.
 */
BOOL UndoStackHasOnlyOneFrame(UNDOSTACK *lpundostack)
{
	return !lpundostack->lpundoframeLast->lpundoframePrev;
}


/* PopUndoFrame
 *   Removes a frame from the top of an undo stack.
 *
 * Parameters:
 *   UNDOSTACK*		lpundostack		Undo stack.
 *
 * Return value: None.
 *
 * Remarks:
 *   The last frame must not be popped! Call UndoStackHasOnlyOneFrame first.
 */
void PopUndoFrame(UNDOSTACK *lpundostack)
{
	UNDOFRAME *lpundoframePrev = lpundostack->lpundoframeLast->lpundoframePrev;

	FreeUndoFrameItems(lpundostack->lpundoframeLast);
	ProcHeapFree(lpundostack->lpundoframeLast);

	/* This is why popping the last frame is not allowed... */
	lpundoframePrev->lpundoframeNext = NULL;
	lpundostack->lpundoframeLast = lpundoframePrev;
}


/* PerformTopUndo
 *   Applies the top undo frame of a stack to a map.
 *
 * Parameters:
 *   UNDOSTACK*		lpundostack		Undo stack.
 *   MAP*			lpmap			Map.
 *
 * Return value: None.
 *
 * Remarks:
 *   The frame is NOT popped after it's been applied. This is because when the
 *   last frame has been undone, the entire stack structure must be destroyed.
 */
void PerformTopUndo(UNDOSTACK *lpundostack, MAP *lpmap)
{
	lpmap->iLinedefs = lpundostack->lpundoframeLast->iLinedefs;
	lpmap->iSectors = lpundostack->lpundoframeLast->iSectors;
	lpmap->iSidedefs = lpundostack->lpundoframeLast->iSidedefs;
	lpmap->iVertices = lpundostack->lpundoframeLast->iVertices;
	lpmap->iThings = lpundostack->lpundoframeLast->iThings;
	
	ApplyUndoItemList(lpundostack->lpundoframeLast->lpuilLinedefs, (BYTE*)lpmap->linedefs, sizeof(MAPLINEDEF));
	ApplyUndoItemList(lpundostack->lpundoframeLast->lpuilSectors, (BYTE*)lpmap->sectors, sizeof(MAPSECTOR));
	ApplyUndoItemList(lpundostack->lpundoframeLast->lpuilSidedefs, (BYTE*)lpmap->sidedefs, sizeof(MAPSIDEDEF));
	ApplyUndoItemList(lpundostack->lpundoframeLast->lpuilVertices, (BYTE*)lpmap->vertices, sizeof(MAPVERTEX));
	ApplyUndoItemList(lpundostack->lpundoframeLast->lpuilThings, (BYTE*)lpmap->things, sizeof(MAPTHING));
}


/* ApplyUndoItemList
 *   Applies data in an undo item list to a buffer.
 *
 * Parameters:
 *   UNDOITEMLIST*	lpuil		Undo item list.
 *   BYTE*			lpbyData	Map data buffer.
 *   UINT			cbRecord	Size of each object.
 *
 * Return value: None.
 *
 * Remarks:
 *   The frame is NOT popped after it's been applied. This is because when the
 *   last frame has been undone, the entire stack structure must be destroyed.
 */
static void ApplyUndoItemList(UNDOITEMLIST *lpuil, BYTE *lpbyData, UINT cbRecord)
{
	while(lpuil)
	{
		CopyMemory(lpbyData + lpuil->uiFirstIndex * cbRecord, lpuil->lpvBase, (lpuil->uiLastIndex - lpuil->uiFirstIndex + 1) * cbRecord);
		lpuil = lpuil->lpuilNext;
	}
}
