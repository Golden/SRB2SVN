#ifndef __SRB2B_SELECTION__
#define __SRB2B_SELECTION__

/* Types. */
typedef struct _SELECTION_LIST
{
	int ciBufferLength;
	int iDataCount;
	int *lpiIndices;
} SELECTION_LIST;


/* Function prototypes. */
SELECTION_LIST* AllocateSelectionList(int iInitListSize);
void DestroySelectionList(SELECTION_LIST *lpsellist);
void AddToSelectionList(SELECTION_LIST *lpsellist, int iValue);
BOOL RemoveFromSelectionList(SELECTION_LIST *lpsellist, int iValue);
BOOL ExistsInSelectionList(SELECTION_LIST *lpsellist, int iValue);

/* Inline functions. */

/* ClearSelectionList
 *   Removes all items from a selection list.
 *
 * Parameters:
 *   SELECTION_LIST*	lpsellist	Pointer to selection list to empty.
 *
 * Return value: None.
 */
static __inline void ClearSelectionList(SELECTION_LIST *lpsellist)
{
	lpsellist->iDataCount = 0;
}


#endif
