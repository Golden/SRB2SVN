#ifndef __SRB2B_EDITING__
#define __SRB2B_EDITING__

#include "map.h"
#include "config.h"
#include "renderer.h"
#include "selection.h"
#include "win/infobar.h"


/* Types. */
typedef struct _DRAW_OPERATION
{
	int				*lpiNewVertices;
	int				*lpiNewLines;

	int				iNewVertexCount;
	int				iNewLineCount;

	unsigned int	ciVertexBuffer;
	unsigned int	ciLineBuffer;
} DRAW_OPERATION;

typedef struct _LOOPLIST LOOPLIST;


enum ENUM_VERTEX_EDIT_FLAGS
{
	VEF_NEW = 1
};

enum ENUM_LINEDEF_EDIT_FLAGS
{
	LEF_NEW = 1,
	LEF_INVALIDSIDEDEFS = 2,	/* For new lds which haven't been set yet. */
	LEF_LOOPFRONT = 4,
	LEF_LOOPBACK = 8,
	LEF_ENCLOSEDFRONT = 16,
	LEF_ENCLOSEDBACK = 32,
	LEF_RECALCSECTOR = 64,
	LEF_LABELLED = 0x8000	/* A generic label. */
};

enum ENUM_SNAP_FLAGS
{
	SF_RECTANGLE	= 1,
	SF_45			= 2
};

enum ENUM_LINE_SIDE
{
	LS_FRONT, LS_BACK
};


/* Prototypes. */
int RemoveUnusedVertices(MAP *lpmap);
void GetLinedefDisplayInfo(MAP *lpmap, CONFIG *lpcfgLinedefFlat, int iIndex, LINEDEFDISPLAYINFO *lplddi);
void GetSectorDisplayInfo(MAP *lpmap, CONFIG *lpcfgSectors, int iIndex, SECTORDISPLAYINFO *lpsdi);
void GetThingDisplayInfo(MAP *lpmap, CONFIG *lpcfgThings, int iIndex, THINGDISPLAYINFO *lptdi);
void GetVertexDisplayInfo(MAP *lpmap, int iIndex, VERTEXDISPLAYINFO *lpvdi);
DWORD CheckLines(MAP *lpmap, SELECTION_LIST *lpsellist, CONFIG *lpcfgLinedefTypesFlat, LINEDEFDISPLAYINFO *lpsdi);
DWORD CheckSectors(MAP *lpmap, SELECTION_LIST *lpsellist, CONFIG *lpcfgSecTypes, SECTORDISPLAYINFO *lpsdi);
DWORD CheckThings(MAP *lpmap, SELECTION_LIST *lpsellist, CONFIG *lpcfgFlatThings, THINGDISPLAYINFO *lptdi);
DWORD CheckVertices(MAP *lpmap, SELECTION_LIST *lpsellist, VERTEXDISPLAYINFO *lptdi);
void CheckLineFlags(MAP *lpmap, SELECTION_LIST *lpsellist, WORD *lpwFlagValues, WORD *lpwFlagMask);
void SetLineFlags(MAP *lpmap, SELECTION_LIST *lpsellist, WORD wFlagValues, WORD wFlagMask);
void CheckThingFlags(MAP *lpmap, SELECTION_LIST *lpsellist, WORD *lpwFlagValues, WORD *lpwFlagMask);
void SetThingFlags(MAP *lpmap, SELECTION_LIST *lpsellist, WORD wFlagValues, WORD wFlagMask);
unsigned short NextUnusedTag(MAP *lpmap);
BOOL GetAdjacentSectorHeightRange(MAP *lpmap, short *lpnMaxCeil, short *lpnMinCeil, short *lpnMaxFloor, short *lpnMinFloor);
void GetMapRect(MAP *lpmap, RECT *lprc);
void ResetMapView(MAP *lpmap, MAPVIEW *lpmapview);
void ApplySectorPropertiesToSelection(MAP *lpmap, SELECTION_LIST *lpsellist, SECTORDISPLAYINFO *lpsdi, DWORD dwFlags);
void ApplySectorProperties(MAP *lpmap, int iIndex, SECTORDISPLAYINFO *lpsdi, DWORD dwFlags);
void ApplyLinePropertiesToSelection(MAP *lpmap, SELECTION_LIST *lpsellist, LINEDEFDISPLAYINFO *lplddi, DWORD dwFlags);
void ApplyLineProperties(MAP *lpmap, int iIndex, LINEDEFDISPLAYINFO *lplddi, DWORD dwFlags);
void ApplyThingPropertiesToSelection(MAP *lpmap, SELECTION_LIST *lpsellist, THINGDISPLAYINFO *lptdi, DWORD dwFlags);
void ApplyThingProperties(MAP *lpmap, int iIndex, THINGDISPLAYINFO *lptdi, DWORD dwFlags);
int AddVertex(MAP *lpmap, short x, short y);
BOOL RemoveVertex(MAP *lpmap, int iIndex);
int AddLinedef(MAP *lpmap, int iVertexStart, int iVertexEnd);
int AddSector(MAP *lpmap, int iParent);
int AddSidedef(MAP *lpmap, int iSector);
void BeginDrawOperation(DRAW_OPERATION *lpdrawop);
void EndDrawOperation(MAP *lpmap, DRAW_OPERATION *lpdrawop);
BOOL DrawToNewVertex(MAP *lpmap, DRAW_OPERATION *lpdrawop, short x, short y);
void FlipLinedef(MAP *lpmap, int iLinedef);
void FlipSelectedLinedefs(MAP *lpmap, SELECTION_LIST* lpsellist);
void ExchangeSelectedSidedefs(MAP *lpmap, SELECTION_LIST* lpsellist);
void BisectSelectedLinedefs(MAP *lpmap, SELECTION_LIST* lpsellist);
int GetVertexFromPosition(MAP *lpmap, short x, short y);
int FindLinedefBetweenVertices(MAP *lpmap, int iVertexA, int iVertexB);
int FindLinedefBetweenVerticesDirected(MAP *lpmap, int iVertex1, int iVertex2);
void Snap(short *lpx, short *lpy, unsigned short cxSnap, unsigned short cySnap);
BOOL VerticesReachable(MAP *lpmap, int iVertex1, int iVertex2);
void DeleteSector(MAP *lpmap, int iSector);
void DeleteLinedef(MAP *lpmap, int iLinedef);
void DeleteSidedef(MAP *lpmap, int iSidedef);
void JoinSelectedSectors(MAP *lpmap, SELECTION_LIST *lpsellist, BOOL bMerge);
void LabelSelectedLinesRS(MAP *lpmap);
void LabelRSLinesEnclosure(MAP *lpmap, LOOPLIST *lplooplist);
LOOPLIST* LabelRSLinesLoops(MAP *lpmap);
void LabelLoopedLinesRS(MAP *lpmap, LOOPLIST *lplooplist);
void DestroyLoopList(LOOPLIST *lplooplist);
void ClearDraggingFlags(MAP *lpmap);
void CorrectDraggedSectorReferences(MAP *lpmap);
void FlipVertexAboutVerticalAxis(MAP *lpmap, int iVertex, short xAxis);
void FlipThingAboutVerticalAxis(MAP *lpmap, int iThing, short xAxis);
void FlipVertexAboutHorizontalAxis(MAP *lpmap, int iVertex, short yAxis);
void FlipThingAboutHorizontalAxis(MAP *lpmap, int iThing, short yAxis);
void SetThingSelectionZ(MAP *lpmap, SELECTION_LIST *lpsellist, CONFIG *lpcfgFlatThings, WORD z);

#endif
