#ifndef __SRB2B_MAPTYPES__
#define __SRB2B_MAPTYPES__

#define MAX_SSECTORS_VERTICES		200

/* Sizes of entries in the respective lumps. */
#define LINEDEFRECORDSIZE	14
#define SECTORRECORDSIZE	26
#define THINGRECORDSIZE		10
#define SIDEDEFRECORDSIZE	30
#define VERTEXRECORDSIZE	4

typedef struct _FPOINT
{
	float x;
	float y;
} FPOINT;

typedef struct _FPOINT3D
{
	float x;
	float y;
	float z;
} FPOINT3D;

typedef struct _SRECT
{
	float left;
	float right;
	float top;
	float bottom;
} SRECT;

typedef struct _CLIPPER
{
	float x;
	float y;
	float dx;
	float dy;
} CLIPPER;

typedef struct _THINGFILTERS
{
	int filtermode;
	int category;
	int flags;
} THINGFILTERS;



/* The first parts of these structures is exactly the lump structure. */
typedef struct _MAPTHING
{
	short x;
	short y;
	short angle;
	short thing;
	short flag;
	
	// Optimisation variables
	int		z;
	short	category;
	int		color;
	int		image;
	int		size;
	int		height;
	BOOL	hangs;
	BYTE	selected;
	int		sector;
	short	circleradius;
	BYTE	highlight;
	BOOL	arrow;
} MAPTHING;

typedef struct _MAPLINEDEF
{
	short v1;
	short v2;
	short flags;
	short effect;
	short tag;
	short s1;
	short s2;
	
	// Optimisation variables
	BYTE	selected;
	BYTE	highlight;
	WORD	editflags;
} MAPLINEDEF;

typedef struct _MAPSIDEDEF
{
	short	tx;
	short	ty;
	char	upper[8];
	char	lower[8];
	char	middle[8];
	short	sector;
	
	// Optimisation variables
	short	linedef;
	short	middletop;
	short	middlebottom;
} MAPSIDEDEF;

typedef struct _MAPVERTEX
{
	short	x;
	short	y;
	
	// Optimisation variables
	BYTE	selected;
	BYTE	highlight;
	BYTE	editflags;
} MAPVERTEX;

typedef struct _MAPSECTOR
{
	short	hfloor;
	short	hceiling;
	char	tfloor[8];
	char	tceiling[8];
	short	brightness;
	short	special;
	short	tag;
	
	// Optimisation variables
	BYTE	selected;
	BYTE	highlight;
	int		editflags;
} MAPSECTOR;

typedef struct _MAPSPLIT
{
	int x;
	int y;
	int dx;
	int dy;
} MAPSPLIT;

typedef struct _MAPSEG
{
	short v1;
	short v2;
	short angle;
	short linedef;
	short side;
	short offset;
} MAPSEG;

typedef struct _MAPSSECTOR
{
	short startseg;
	short numsegs;
	
	// Optimization variables
	int sector;
	int numvertices;
	FPOINT vertices[MAX_SSECTORS_VERTICES];
} MAPSSECTOR;

typedef struct _MAPNODE
{
	int x;
	int y;
	int dx;
	int dy;
	
	int rtop;
	int rbottom;
	int rleft;
	int rright;
	
	int ltop;
	int lbottom;
	int lleft;
	int lright;
	
	int right;
	int left;
} MAPNODE;

typedef struct _MAP
{
	MAPSECTOR	*sectors;
	MAPLINEDEF	*linedefs;
	MAPVERTEX	*vertices;
	MAPSIDEDEF	*sidedefs;
	MAPTHING	*things;
	int			iSectors, iLinedefs;
	int			iVertices, iSidedefs;
	int			iThings;
} MAP;

#endif
