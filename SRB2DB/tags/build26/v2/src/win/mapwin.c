#include <windows.h>
#include <commctrl.h>
#include <stdio.h>
#include <tchar.h>
#include <math.h>

#include "../general.h"
#include "../../res/resource.h"
#include "../maptypes.h"
#include "../map.h"
#include "../editing.h"
#include "../config.h"
#include "../openwads.h"
#include "../options.h"
#include "../keyboard.h"
#include "../renderer.h"
#include "../selection.h"
#include "../mapconfig.h"
#include "../texture.h"
#include "../undo.h"

#include "mdiframe.h"
#include "mapwin.h"
#include "infobar.h"
#include "editdlg.h"

#include "../CodeImp/ci_const.h"
#include "../CodeImp/ci_data_proto.h"
#include "../CodeImp/ci_map.h"

/* Macros. */
#define XMOUSE_WIN_TO_MAP(n, lpmwd) ((int)floor((n)/(lpmwd)->mapview.fZoom + lpmwd->mapview.xLeft + 0.5))
#define YMOUSE_WIN_TO_MAP(n, lpmwd) ((int)floor((-n)/(lpmwd)->mapview.fZoom + lpmwd->mapview.yTop + 0.5))

/* The wheel is treated like a keypress. */
#define MOUSE_SCROLL_UP 4008
#define MOUSE_SCROLL_DOWN 4009

/* In WadAuthor-selection-mode, there's a threshold for picking between linedefs
 * and sectors. It's measured in pixels. Similarly vertices.
 * Must be <= HOVER_THRESHOLD, or funny things start happening.
 */
#define LINEDEF_THRESHOLD	16
#define VERTEX_THRESHOLD	4

/* We set a maximum distance for hovering near objects. Even if that vertex
 * half-way across the map is in fact nearest, it feels weird when it lights up.
 * Measured in pixels.
 */
#define HOVER_THRESHOLD		16


/* Indices of caching events in window data. */
#define HCACHE_FLAT			0
#define HCACHE_TEX			1


/* Initial size of selection hash buckets. */
#define INIT_SELLIST_SIZE	10

/* ID constants. */
#define IDT_ANIM_TIMER 1

/* Menu positions. */
#define EDITMENUPOS 1
#define VIEWMENUPOS 2

/* Size of buffer for undo/redo strings. */
#define UNDOCAPTIONBUFSIZE 128


#ifdef _UNICODE
#define IsPseudoTexture IsPseudoTextureW
#else
#define IsPseudoTexture IsPseudoTextureA
#endif


typedef struct _HIGHLIGHTOBJ
{
	ENUM_EDITMODE	emType;
	int			iIndex;
} HIGHLIGHTOBJ;


typedef struct _MAPWINDATA
{
	HWND				hwnd;		/* Just for convenience. */
	int					iWadID, iIWadID, iAdditionalWadID;
	WAD					*lpwadIWad;	/* For efficiency. */
	MAP					*lpmap;
	char				szLumpName[9];
	ENUM_EDITMODE		editmode, prevmode;
	ENUM_EDITSUBMODE	submode;
	HDC					hdc;
	HGLRC				hrc;
	HCURSOR				hCursor;
	short				xMouseLast, yMouseLast;
	MAPVIEW				mapview;
	BOOL				bWantRedraw;
	HIGHLIGHTOBJ		highlightobj;
	HIGHLIGHTOBJ		hobjDrag;
	SELECTION			selection, selectionAux;
	CONFIG				*lpcfgMap;
	TEXCACHE			tcFlatsHdr, tcTexturesHdr;
	TEXTURENAMELIST		*lptnlTextures, *lptnlFlats;
	RGBQUAD				rgbqPalette[CB_PLAYPAL / 3];
	DRAW_OPERATION		drawop;
	BYTE				bySnapFlags, bySnapToggleMask;
	short				xDragLast, yDragLast;
	BOOL				bDeselectAfterEdit;
	UNDOSTACK			*lpundostackUndo, *lpundostackRedo;
	BOOL				bMapChanged;
	LOOPLIST			*lplooplistRS;
} MAPWINDATA;

/* Status bar panels. */
enum _ENUM_SBPANELS
{
	SBP_MODE = 0,
	SBP_SECTORS,
	SBP_LINEDEFS,
	SBP_SIDEDEFS,
	SBP_VERTICES,
	SBP_THINGS,
	SBP_GRID,
	SBP_ZOOM,
	SBP_COORD,
	SBP_MAPCONFIG,
	SBP_LAST
};



/* Static function declarations. */
static LRESULT CALLBACK MapWndProc(HWND hwnd, UINT uiMessage, WPARAM wParam, LPARAM lParam);
static void DestroyMapWindowData(HWND hwnd);
static BOOL MapKeyDown(MAPWINDATA *lpmwd, int iKeyCode);
static BOOL MapKeyUp(MAPWINDATA *lpmwd, int iKeyCode);
static void RedrawMapWindow(MAPWINDATA *lpmwd);
static void ChangeMode(MAPWINDATA *lpmwd, USHORT unModeMenuID);
static void StatusBarMapWindow(void);
static void UpdateStatusBar(MAPWINDATA *lpmwd, DWORD dwFlags);
static BOOL UpdateHighlight(MAPWINDATA *lpmwd, int xMouse, int yMouse);
static void GetNearestObject(MAPWINDATA *lpmwd, int xMap, int yMap, ENUM_EDITMODE mode, HIGHLIGHTOBJ *lphighlightobj);
static void ClearHighlight(MAPWINDATA *lpmwd);
static void SetHighlight(MAPWINDATA *lpmwd, HIGHLIGHTOBJ *lphightlightobj);
static LRESULT MouseMove(MAPWINDATA *lpmwd, WORD xMouse, WORD yMouse);
static LRESULT LButtonDown(MAPWINDATA *lpmwd, WORD xMouse, WORD yMouse, WORD wFlags);
static LRESULT LButtonUp(MAPWINDATA *lpmwd, WORD xMouse, WORD yMouse, WORD wFlags);
static LRESULT RButtonUp(MAPWINDATA *lpmwd, WORD xMouse, WORD yMouse, WORD wFlags);
static LRESULT RButtonDown(MAPWINDATA *lpmwd, WORD xMouse, WORD yMouse, WORD wFlags);
static BOOL ObjectSelected(MAPWINDATA *lpmwd, HIGHLIGHTOBJ *lphighlightobj);
static void AddObjectToSelection(MAPWINDATA *lpmwd, HIGHLIGHTOBJ *lphighlightobj);
static BOOL RemoveObjectFromSelection(MAPWINDATA *lpmwd, HIGHLIGHTOBJ *lphighlightobj);
static void UpdateMapWindowTitle(MAPWINDATA *lpmwd);
static void BuildSectorTexturePreviews(MAPWINDATA *lpmwd, SECTORDISPLAYINFO *lpsdi, DWORD dwDisplayFlags);
static void DestroySectorTexturePreviews(SECTORDISPLAYINFO *lpsdi, DWORD dwDisplayFlags);
static void BuildThingSpritePreview(MAPWINDATA *lpmwd, THINGDISPLAYINFO *lptdi, DWORD dwDisplayFlags);
static void DestroyThingSpritePreview(THINGDISPLAYINFO *lptdi, DWORD dwDisplayFlags);
static void BuildSidedefTexturePreviews(MAPWINDATA *lpmwd, LINEDEFDISPLAYINFO *lplddi, DWORD dwDisplayFlags);
static void DestroySidedefTexturePreviews(LINEDEFDISPLAYINFO *lplddi, DWORD dwDisplayFlags);

#ifdef _UNICODE
static BOOL IsPseudoTextureW(LPCWSTR szTexName);
#endif

static BOOL IsPseudoTextureA(LPCSTR szTexName);
static void MapWinUpdateInfoBar(MAPWINDATA *lpmwd);
static void ShowLinesSelectionInfo(MAPWINDATA *lpmwd);
static void ShowSectorSelectionInfo(MAPWINDATA *lpmwd);
static void ShowThingsSelectionInfo(MAPWINDATA *lpmwd);
static void ShowVerticesSelectionInfo(MAPWINDATA *lpmwd);
static void ShowSelectionProperties(MAPWINDATA *lpmwd);
static void SelectVerticesFromLines(MAPWINDATA *lpmwd);
static void SelectLinesFromVertices(MAPWINDATA *lpmwd);
static void SelectSectorsFromLines(MAPWINDATA *lpmwd);
static void DeselectAll(MAPWINDATA *lpmwd);
static void DeselectAllBruteForce(MAPWINDATA *lpmwd);
static void DeselectAllLines(MAPWINDATA *lpmwd);
static void DeselectAllSectors(MAPWINDATA *lpmwd);
static void DeselectAllThings(MAPWINDATA *lpmwd);
static void DeselectAllVertices(MAPWINDATA *lpmwd);
static void DeselectLinesFromPartialSectors(MAPWINDATA *lpmwd);
static void DoInsertOperation(MAPWINDATA *lpmwd, short xMap, short yMap);
static void EndDragOperation(MAPWINDATA *lpmwd);
static void RebuildSelection(MAPWINDATA *lpmwd);
static void CreateUndo(MAPWINDATA *lpmwd, int iStringIndex);
static void ClearRedoStack(MAPWINDATA *lpmwd);
static void PerformUndo(MAPWINDATA *lpmwd);
static void PerformRedo(MAPWINDATA *lpmwd);
static void WithdrawUndo(MAPWINDATA *lpmwd);
static void WithdrawRedo(MAPWINDATA *lpmwd);
static void FlipSelectionHorizontally(MAPWINDATA *lpmwd);
static void FlipSelectionVertically(MAPWINDATA *lpmwd);
static void CreateAuxiliarySelection(MAPWINDATA *lpmwd);
static void CleanAuxiliarySelection(MAPWINDATA *lpmwd);


/* RegisterMapWindowClass
 *   Register's the map window class.
 *
 * Parameters:
 *   None
 *
 * Return value: int
 *   Zero on success; nonzero on error.
 *
 * Notes:
 *   Only needs to be called at startup.
 */
int RegisterMapWindowClass(void)
{
	WNDCLASSEX wndclassex;
	const TCHAR szClassName[] = MAPWINCLASS;
	
	/* Set up and register the window class. */

	wndclassex.cbClsExtra = 0;
	wndclassex.cbSize = sizeof(wndclassex);
	wndclassex.cbWndExtra = sizeof(struct MAP*);
	wndclassex.hbrBackground = (HBRUSH)GetStockObject(WHITE_BRUSH);
	wndclassex.hCursor = NULL;
	wndclassex.hIcon = LoadIcon(NULL, IDI_APPLICATION);

	/* For the small icon, ask the system what size it should be. */
	wndclassex.hIconSm = LoadImage(NULL, IDI_APPLICATION, IMAGE_ICON, GetSystemMetrics(SM_CXSMICON), GetSystemMetrics(SM_CYSMICON), LR_DEFAULTCOLOR);

	wndclassex.hInstance = g_hInstance;
	wndclassex.lpfnWndProc = MapWndProc;
	wndclassex.lpszClassName = szClassName;
	wndclassex.lpszMenuName = NULL;
	wndclassex.style = CS_VREDRAW | CS_HREDRAW | CS_OWNDC;

	/* The only reason this should fail is if we try to register a Unicode class
	 * on 9x.
	 */
	if(!RegisterClassEx(&wndclassex)) return 1;
	return 0;
}


/* CreateMapWindow
 *   Creates a map editor window.
 *
 * Parameters:
 *   MAP*	lpmap		Pointer to map data for window.
 *   LPSTR	szLumpName	Name of map-lump.
 *   CONFIG *lpcfgMap	Map configuration.
 *   int	iWadID		ID of wad structure this window belongs to.
 *   int	iIWadID		ID of IWAD for the selected config.
 *
 * Return value: HWND
 *   Window handle, or NULL on error.
 */
HWND CreateMapWindow(MAP *lpmap, LPSTR szLumpName, CONFIG *lpcfgMap, int iWadID, int iIWadID)
{
	const TCHAR szClassName[] = MAPWINCLASS;
	MAPWINDATA *lpmwd;
	PIXELFORMATDESCRIPTOR pfd;

	/* Allocate storage for this window's data. */
	lpmwd = ProcHeapAlloc(sizeof(MAPWINDATA));

	lpmwd->iWadID = iWadID;
	lpmwd->iIWadID = iIWadID;
	lpmwd->iAdditionalWadID = -1;	/* TODO. */

	/* Map is unchanged initially. */
	lpmwd->bMapChanged = FALSE;
	
	/* We use this rather a lot, so cache it. */
	lpmwd->lpwadIWad = iIWadID >= 0 ? GetWad(iIWadID) : NULL;

	lpmwd->lpmap = lpmap;

	/* Get lumpname. Note that this is always ANSI. */
	lstrcpynA(lpmwd->szLumpName, szLumpName, 9);

	/* Store map config. */
	lpmwd->lpcfgMap = lpcfgMap;

	lpmwd->submode = ESM_NONE;
	lpmwd->editmode = EM_ANY;
	lpmwd->prevmode = EM_ANY;
	
	lpmwd->mapview.cxDrawSurface = lpmwd->mapview.cyDrawSurface = -1;
	
	/* We don't set the view position until we've created the window, since we
	 * need to know its dimensions.
	 */

	lpmwd->mapview.cxGrid = lpmwd->mapview.cyGrid = ConfigGetInteger(g_lpcfgMain, "defaultgrid");
	lpmwd->mapview.bShowGrid = ConfigGetInteger(g_lpcfgMain, "gridshow");
	lpmwd->mapview.xGridOffset = lpmwd->mapview.yGridOffset = 0;

	/* No object highlighted initially. */
	lpmwd->highlightobj.emType = EM_MOVE;

	lpmwd->hCursor = LoadCursor(NULL, IDC_ARROW);
	
	/* Mouse is assumed to be outside window. */
	lpmwd->xMouseLast = lpmwd->yMouseLast = -1;

	/* Snap to grid. */
	lpmwd->bySnapFlags = SF_RECTANGLE;
	lpmwd->bySnapToggleMask = 0;


	/* Load palette. */
	if(lpmwd->iIWadID >= 0 && GetLumpLength(lpmwd->lpwadIWad, "PLAYPAL", NULL) >= CB_PLAYPAL)
	{
		BYTE	lpbyPlaypal[CB_PLAYPAL];
		int		i;

		/* Get the palette from the IWAD. */
		GetLump(lpmwd->lpwadIWad, "PLAYPAL", NULL, lpbyPlaypal, CB_PLAYPAL);

		/* Convert it to an array of RGBQUADs for the API. */
		for(i = 0; i < CB_PLAYPAL / 3; i++)
		{
			lpmwd->rgbqPalette[i].rgbRed = lpbyPlaypal[3*i];
			lpmwd->rgbqPalette[i].rgbGreen = lpbyPlaypal[3*i + 1];
			lpmwd->rgbqPalette[i].rgbBlue = lpbyPlaypal[3*i + 2];
		}
	}
	else
	{
		/* No palette? Zero it. */
		FillMemory(lpmwd->rgbqPalette, sizeof(lpmwd->rgbqPalette), 0);
	}

	/* Initialise all things' colour flags. */
	SetAllThingPropertiesFromType(lpmap, ConfigGetSubsection(lpcfgMap, FLAT_THING_SECTION));

	/* Get a list of all texture names. */
	lpmwd->lptnlFlats = CreateTextureNameList();
	lpmwd->lptnlTextures = CreateTextureNameList();
	if(lpmwd->iWadID >= 0)
	{
		AddAllTextureNamesToList(lpmwd->lptnlFlats, lpmwd->lpwadIWad, TF_FLAT);
		AddAllTextureNamesToList(lpmwd->lptnlTextures, lpmwd->lpwadIWad, TF_TEXTURE);
	}
	if(lpmwd->iAdditionalWadID >= 0)
	{
		WAD *lpwadAdd = GetWad(lpmwd->iAdditionalWadID);

		AddAllTextureNamesToList(lpmwd->lptnlFlats, lpwadAdd, TF_FLAT);
		AddAllTextureNamesToList(lpmwd->lptnlTextures, lpwadAdd, TF_TEXTURE);
	}
	/* TODO: Also add textures from file itself? */

	SortTextureNameList(lpmwd->lptnlFlats);
	SortTextureNameList(lpmwd->lptnlTextures);

	/* Texture caching. */
	lpmwd->tcFlatsHdr.lptcNext = NULL;
	lpmwd->tcTexturesHdr.lptcNext = NULL;

	lpmwd->hwnd = CreateMDIWindow(szClassName, TEXT("Map"), 0, CW_USEDEFAULT, CW_USEDEFAULT, CW_USEDEFAULT, CW_USEDEFAULT, g_hwndClient, g_hInstance, (LPARAM)lpmwd);
	UpdateMapWindowTitle(lpmwd);

	/* Now that we've got the window, we can get its DC. */
	lpmwd->hdc = GetDC(lpmwd->hwnd);

	/* Set the view now that we know the dimensions. */
	ResetMapView(lpmap, &lpmwd->mapview);

	/* Initialise selection structures. */
	lpmwd->selection.lpsellistLinedefs = AllocateSelectionList(INIT_SELLIST_SIZE);
	lpmwd->selection.lpsellistVertices = AllocateSelectionList(INIT_SELLIST_SIZE);
	lpmwd->selection.lpsellistSectors = AllocateSelectionList(INIT_SELLIST_SIZE);
	lpmwd->selection.lpsellistThings = AllocateSelectionList(INIT_SELLIST_SIZE);

	/* No undo structures to start with. */
	lpmwd->lpundostackUndo = NULL;
	lpmwd->lpundostackRedo = NULL;

	/* Set the pixel format. */
	ZeroMemory(&pfd, sizeof(pfd));
	pfd.nSize = sizeof(pfd);
    pfd.nVersion = 1;
    pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
    pfd.iPixelType = PFD_TYPE_RGBA;
    pfd.cColorBits = 32;
    pfd.cDepthBits = 16;
	SetPixelFormat(lpmwd->hdc, ChoosePixelFormat(lpmwd->hdc, &pfd), &pfd);

	/* Get a resource context and make it current. */
	lpmwd->hrc = wglCreateContext(lpmwd->hdc);
	wglMakeCurrent(lpmwd->hdc, lpmwd->hrc);
	InitGL();
	lpmwd->mapview.uiThingTex = LoadThingCircleTexture();
	ReSizeGLScene(lpmwd->mapview.cxDrawSurface, lpmwd->mapview.cyDrawSurface);

	/* Need to be set up in order to do this. */
	ChangeMode(lpmwd, IDM_VIEW_MODE_ANY);	/* Sets menus etc. */

	return lpmwd->hwnd;
}



/* MapWndProc
 *   Map editor child window procedure. Called by the message loop.
 *
 * Parameters:
 *   HWND		hwnd		Window handle for message.
 *   UINT		uiMessage	ID for the message.
 *   WPARAM		wParam		Message-specific.
 *   LPARAM		lParam		Message-specific.
 *
 * Return value: LRESULT
 *   Message-specific.
 */

static LRESULT CALLBACK MapWndProc(HWND hwnd, UINT uiMessage, WPARAM wParam, LPARAM lParam)
{
	MAPWINDATA *lpmwd;

	switch(uiMessage)
	{
	case WM_CREATE:
		{
			/* Retrieve the window-specific structure, and store it. */
			lpmwd = (MAPWINDATA*)((MDICREATESTRUCT*)((CREATESTRUCT*)lParam)->lpCreateParams)->lParam;
			SetWindowLong(hwnd, GWL_USERDATA, (LONG)lpmwd);

			/* Create an animation timer for this window. */
			SetTimer(hwnd, IDT_ANIM_TIMER, 16, NULL);
		}

		return 0;

	case WM_MDIACTIVATE:
		/* Are we now active? */
		if(lParam == (LPARAM)hwnd)
		{
			lpmwd = (MAPWINDATA*)GetWindowLong(hwnd, GWL_USERDATA);

			/* Set our menu and status bar.. */
			SendMessage(g_hwndClient, WM_MDISETMENU, (WPARAM)g_hmenuMap, (LPARAM)g_hmenuMapWin);
			StatusBarMapWindow();
			UpdateStatusBar(lpmwd, 0xFFFFFFFF);		/* Update all cells. */
		}
		else	/* We're not active any more. */
		{
			/* Set the 'no active window' menu and status bar. */
			SendMessage(g_hwndClient, WM_MDISETMENU, (WPARAM)g_hmenuNodoc, (LPARAM)g_hmenuNodocWin);
			StatusBarNoWindow();
		}

		DrawMenuBar(g_hwndMain);

		return 0;


	case WM_INITMENUPOPUP:
		/* Check items as necessary before the menu's displayed. */

		/* Make sure it's not the system menu. */
		if(!HIWORD(lParam))
		{
			lpmwd = (MAPWINDATA*)GetWindowLong(hwnd, GWL_USERDATA);

			/* Switch on menu position. We subtract one when maximised due to
			 * the MDI system menu.
			 */
			switch(LOWORD(lParam) - (IsZoomed(lpmwd->hwnd) ? 1 : 0))
			{
			case EDITMENUPOS:
				{
					MENUITEMINFO mii;
					TCHAR szCaption[UNDOCAPTIONBUFSIZE];
					int iPrefixLength;

					/* Check 'snap to grid' option as appropriate. */
					CheckMenuItem((HMENU)wParam, IDM_EDIT_SNAPTOGRID, lpmwd->bySnapFlags & SF_RECTANGLE ? MF_CHECKED : MF_UNCHECKED);

					/* Set undo/redo strings. */

					mii.cbSize = sizeof(mii);
					mii.fMask = MIIM_TYPE;
					mii.fType = MFT_STRING;
					mii.dwTypeData = szCaption;

					/* Concatenate undo prefix and description, and set string. */
					LoadString(g_hInstance, IDS_UNDOPREFIX, szCaption, sizeof(szCaption) / sizeof(TCHAR));
					if(lpmwd->lpundostackUndo)
					{
						iPrefixLength = lstrlen(szCaption);
						LoadString(g_hInstance, GetUndoTopStringIndex(lpmwd->lpundostackUndo), &szCaption[iPrefixLength], sizeof(szCaption) / sizeof(TCHAR) - iPrefixLength);
					}
					SetMenuItemInfo((HMENU)wParam, IDM_EDIT_UNDO, FALSE, &mii);

					/* Concatenate redo prefix and description, and set string. */
					LoadString(g_hInstance, IDS_REDOPREFIX, szCaption, sizeof(szCaption) / sizeof(TCHAR));
					if(lpmwd->lpundostackRedo)
					{
						iPrefixLength = lstrlen(szCaption);
						LoadString(g_hInstance, GetUndoTopStringIndex(lpmwd->lpundostackRedo), &szCaption[iPrefixLength], sizeof(szCaption) / sizeof(TCHAR) - iPrefixLength);
					}
					SetMenuItemInfo((HMENU)wParam, IDM_EDIT_REDO, FALSE, &mii);


					/* Enable/disable undo and redo. */
					EnableMenuItem((HMENU)wParam, IDM_EDIT_UNDO, lpmwd->lpundostackUndo ? MF_ENABLED : MF_GRAYED);
					EnableMenuItem((HMENU)wParam, IDM_EDIT_REDO, lpmwd->lpundostackRedo ? MF_ENABLED : MF_GRAYED);
				}

				return 0;
			}
		}

		/* Didn't do anything. Fall through. */
		break;

	/* Menu commands. */
	case WM_COMMAND:

		lpmwd = (MAPWINDATA*)GetWindowLong(hwnd, GWL_USERDATA);

		switch(LOWORD(wParam))
		{
		/* File menu. *********************************************************/
		case IDM_FILE_SAVE:

			/* TODO: Nicer error messages? */
			if(MapSave(lpmwd->lpmap, lpmwd->iWadID, lpmwd->szLumpName))
				MessageBoxFromStringTable(g_hwndMain, IDS_SAVEERROR, MB_ICONERROR);
			return 0;


		/* Edit menu. *********************************************************/

		/* Undo. */
		case IDM_EDIT_UNDO:

			/* Make sure we've got something to undo. */
			if(lpmwd->lpundostackUndo)
			{
				PerformUndo(lpmwd);
				lpmwd->bWantRedraw = TRUE;

				/* The selection is now garbage, both because of nonsense in the
				 * stored selection fields and that numbering might have
				 * changed. Same goes for highlighting.
				 */
				DeselectAllBruteForce(lpmwd);
				UpdateHighlight(lpmwd, -1, -1);

				/* Update highlight and info bar. */
				UpdateHighlight(lpmwd, lpmwd->xMouseLast, lpmwd->yMouseLast);
				MapWinUpdateInfoBar(lpmwd);

				/* Update all cells. */
				UpdateStatusBar(lpmwd, 0xFFFFFFFF);
			}

			return 0;

		/* Redo. */
		case IDM_EDIT_REDO:

			/* Make sure we've got something to redo. */
			if(lpmwd->lpundostackRedo)
			{
				PerformRedo(lpmwd);
				lpmwd->bWantRedraw = TRUE;

				/* The selection is now garbage, both because of nonsense in the
				 * stored selection fields and that numbering might have
				 * changed. Same goes for highlighting.
				 */
				DeselectAllBruteForce(lpmwd);
				UpdateHighlight(lpmwd, -1, -1);

				/* Update highlight and info bar. */
				UpdateHighlight(lpmwd, lpmwd->xMouseLast, lpmwd->yMouseLast);
				MapWinUpdateInfoBar(lpmwd);

				/* Update all cells. */
				UpdateStatusBar(lpmwd, 0xFFFFFFFF);
			}

			return 0;

		/* Snap to grid. */
		case IDM_EDIT_SNAPTOGRID:

			lpmwd->bySnapFlags ^= SF_RECTANGLE;

			/* If we're drawing or dragging, this could alter the position of
			 * the object under the mouse. Redraw.
			 */
			lpmwd->bWantRedraw = TRUE;

			return 0;

		/* Flip horizontally. */
		case IDM_EDIT_TRANSFORMSELECTION_FLIPHORIZONTALLY:

			CreateAuxiliarySelection(lpmwd);

			if(lpmwd->selectionAux.lpsellistVertices->iDataCount + lpmwd->selectionAux.lpsellistThings->iDataCount >= 1)
			{
				CreateUndo(lpmwd, IDS_UNDO_FLIPHORIZ);
				ClearRedoStack(lpmwd);

				FlipSelectionHorizontally(lpmwd);

				lpmwd->bMapChanged = TRUE;
				lpmwd->bWantRedraw = TRUE;
			}

			CleanAuxiliarySelection(lpmwd);

			return 0;

		/* Flip vertically. */
		case IDM_EDIT_TRANSFORMSELECTION_FLIPVERTICALLY:

			CreateAuxiliarySelection(lpmwd);

			if(lpmwd->selectionAux.lpsellistVertices->iDataCount + lpmwd->selectionAux.lpsellistThings->iDataCount >= 1)
			{
				CreateUndo(lpmwd, IDS_UNDO_FLIPVERT);
				ClearRedoStack(lpmwd);

				FlipSelectionVertically(lpmwd);

				lpmwd->bMapChanged = TRUE;
				lpmwd->bWantRedraw = TRUE;
			}

			CleanAuxiliarySelection(lpmwd);

			return 0;



		/* View menu. *********************************************************/

		/* Mode change (other than 3D). */
		case IDM_VIEW_MODE_MOVE:
		case IDM_VIEW_MODE_ANY:
		case IDM_VIEW_MODE_LINES:
		case IDM_VIEW_MODE_SECTORS:
		case IDM_VIEW_MODE_VERTICES:
		case IDM_VIEW_MODE_THINGS:
			ChangeMode(lpmwd, LOWORD(wParam));
			UpdateStatusBar(lpmwd, 1 << SBP_MODE);
			lpmwd->bWantRedraw = TRUE;
			return 0;

		case IDM_VIEW_3D:
			return 0;

		case IDM_VIEW_CENTRE:
			ResetMapView(lpmwd->lpmap, &lpmwd->mapview);
			lpmwd->bWantRedraw = TRUE;
			return 0;

		/* Lines menu. ********************************************************/
		case IDM_LINES_FLIPLINEDEFS:
			
			CreateUndo(lpmwd, IDS_UNDO_FLIPLINEDEFS);
			ClearRedoStack(lpmwd);
			lpmwd->bMapChanged = TRUE;

			FlipSelectedLinedefs(lpmwd->lpmap, lpmwd->selection.lpsellistLinedefs);
			lpmwd->bWantRedraw = TRUE;

			return 0;

		case IDM_LINES_FLIPSIDEDEFS:

			CreateUndo(lpmwd, IDS_UNDO_FLIPSIDEDEFS);
			ClearRedoStack(lpmwd);
			lpmwd->bMapChanged = TRUE;

			ExchangeSelectedSidedefs(lpmwd->lpmap, lpmwd->selection.lpsellistLinedefs);
			lpmwd->bWantRedraw = TRUE;
			return 0;

		case IDM_LINES_SPLITLINEDEFS:

			CreateUndo(lpmwd, IDS_UNDO_SPLITLINEDEFS);
			ClearRedoStack(lpmwd);
			lpmwd->bMapChanged = TRUE;

			BisectSelectedLinedefs(lpmwd->lpmap, lpmwd->selection.lpsellistLinedefs);
			/* TODO: Move the selection rebuilding from the fn to here. */
			lpmwd->bWantRedraw = TRUE;
			return 0;

		/* Sectors menu. ******************************************************/
		case IDM_SECTORS_JOIN:

			CreateUndo(lpmwd, IDS_UNDO_JOINSECTORS);
			ClearRedoStack(lpmwd);
			lpmwd->bMapChanged = TRUE;

			JoinSelectedSectors(lpmwd->lpmap, lpmwd->selection.lpsellistSectors, FALSE);
			RebuildSelection(lpmwd);
			lpmwd->bWantRedraw = TRUE;
			return 0;

		case IDM_SECTORS_MERGE:

			CreateUndo(lpmwd, IDS_UNDO_MERGESECTORS);
			ClearRedoStack(lpmwd);
			lpmwd->bMapChanged = TRUE;

			JoinSelectedSectors(lpmwd->lpmap, lpmwd->selection.lpsellistSectors, TRUE);
			RebuildSelection(lpmwd);
			lpmwd->bWantRedraw = TRUE;
			return 0;
		}

		/* No-one else needs to see it any more. */
		return 0;


	case WM_MOUSEWHEEL:
		/* We pass wheel events as keypresses so they can act as shortcuts. */
		lpmwd = (MAPWINDATA*)GetWindowLong(hwnd, GWL_USERDATA);
		MapKeyDown(lpmwd, MakeShiftedKeyCode((short)HIWORD(wParam) > 0 ? MOUSE_SCROLL_UP : MOUSE_SCROLL_DOWN));
		return 0;

	case WM_KEYDOWN:
		/* Most of our keypresses are handled using accelerators, but there are
		 * some exceptions.
		 */
		lpmwd = (MAPWINDATA*)GetWindowLong(hwnd, GWL_USERDATA);
		MapKeyDown(lpmwd, MakeShiftedKeyCode(wParam));
		return 0;

	case WM_KEYUP:
		lpmwd = (MAPWINDATA*)GetWindowLong(hwnd, GWL_USERDATA);
		MapKeyUp(lpmwd, MakeShiftedKeyCode(wParam));
		return 0;

	case WM_MOUSEMOVE:
		return MouseMove((MAPWINDATA*)GetWindowLong(hwnd, GWL_USERDATA), LOWORD(lParam), HIWORD(lParam));

	case WM_LBUTTONDOWN:
		return LButtonDown((MAPWINDATA*)GetWindowLong(hwnd, GWL_USERDATA), LOWORD(lParam), HIWORD(lParam), (WORD)wParam);

	case WM_LBUTTONUP:
		return LButtonUp((MAPWINDATA*)GetWindowLong(hwnd, GWL_USERDATA), LOWORD(lParam), HIWORD(lParam), (WORD)wParam);

	case WM_RBUTTONUP:
		return RButtonUp((MAPWINDATA*)GetWindowLong(hwnd, GWL_USERDATA), LOWORD(lParam), HIWORD(lParam), (WORD)wParam);

	case WM_RBUTTONDOWN:
		return RButtonDown((MAPWINDATA*)GetWindowLong(hwnd, GWL_USERDATA), LOWORD(lParam), HIWORD(lParam), (WORD)wParam);

	case WM_MOUSELEAVE:
		/* Generated by TrackMouseEvent. */

		lpmwd = (MAPWINDATA*)GetWindowLong(hwnd, GWL_USERDATA);
		lpmwd->xMouseLast = lpmwd->yMouseLast = -1;

		/* Clear the highlight if necessary. */
		if(UpdateHighlight(lpmwd, -1, -1))
			MapWinUpdateInfoBar(lpmwd);

		return 0;

	case WM_SIZE:

		if(wParam != SIZE_MINIMIZED)
		{
			lpmwd = (MAPWINDATA*)GetWindowLong(hwnd, GWL_USERDATA);

			/* Store the width and the height for later use. */
			lpmwd->mapview.cxDrawSurface = (LOWORD(lParam) + 3) & ~3;
			lpmwd->mapview.cyDrawSurface = HIWORD(lParam);

			wglMakeCurrent(lpmwd->hdc, lpmwd->hrc);
			ReSizeGLScene(lpmwd->mapview.cxDrawSurface, lpmwd->mapview.cyDrawSurface);
		}

		/* DefMDIChildProc needs to process this message too. */
		break;

	case WM_ERASEBKGND:
		return 1;	/* Stop flickering. */

	case WM_TIMER:

		/* Is this the redraw timer? */
		if(wParam == IDT_ANIM_TIMER)
		{
			lpmwd = (MAPWINDATA*)GetWindowLong(hwnd, GWL_USERDATA);
			if(lpmwd->bWantRedraw)
			{
				/* Force a repaint. */
				InvalidateRect(hwnd, NULL, FALSE);
				lpmwd->bWantRedraw = FALSE;
			}
		}

		return 0;

	case WM_PAINT:
		{
			PAINTSTRUCT ps;

			lpmwd = (MAPWINDATA*)GetWindowLong(hwnd, GWL_USERDATA);

			BeginPaint(hwnd, &ps);
			RedrawMapWindow(lpmwd);
			EndPaint(hwnd, &ps);
		}

		return 0;


	case WM_CLOSE:

		/* TODO: Allow cancel if map unsaved. */
		break;		/* Allow DefMDIChildProc to destroy us. */

	case WM_DESTROY:

		lpmwd = (MAPWINDATA*)GetWindowLong(hwnd, GWL_USERDATA);

		PurgeTextureCache(&lpmwd->tcFlatsHdr);
		PurgeTextureCache(&lpmwd->tcTexturesHdr);

		/* Free the list of texture names. */
		DestroyTextureNameList(lpmwd->lptnlFlats);
		DestroyTextureNameList(lpmwd->lptnlTextures);

		/* Stop the animation timer. */
		KillTimer(hwnd, IDT_ANIM_TIMER);

		/* Release the wad references. */
		ReleaseWad(lpmwd->iWadID);
		if(lpmwd->iIWadID) ReleaseWad(lpmwd->iIWadID);

		/* Free data. */
		DestroyMapWindowData(hwnd);

		return 0;
	}

	return DefMDIChildProc(hwnd, uiMessage, wParam, lParam);
}


/* DestroyMapWindowData
 *   Cleans up a map window's data.
 *
 * Parameters:
 *   HWND		hwnd		Window handle.
 *
 * Return value:
 *   None.
 */
static void DestroyMapWindowData(HWND hwnd)
{
	MAPWINDATA *lpmwd = (MAPWINDATA*)GetWindowLong(hwnd, GWL_USERDATA);

	/* Renderer clean-up. */
	if(lpmwd->hrc)
	{
		/* Try to release DC and RC. */
		wglMakeCurrent(NULL,NULL);

		/* Try to delete the RC. */
		wglDeleteContext(lpmwd->hrc);
	}

	if(lpmwd->hdc) ReleaseDC(hwnd, lpmwd->hdc);

	/* Clean up undo memory. */
	if(lpmwd->lpundostackUndo) FreeUndoStack(lpmwd->lpundostackUndo);
	if(lpmwd->lpundostackRedo) FreeUndoStack(lpmwd->lpundostackRedo);

	/* Destroy selection structures. */
	DestroySelectionList(lpmwd->selection.lpsellistLinedefs);
	DestroySelectionList(lpmwd->selection.lpsellistSectors);
	DestroySelectionList(lpmwd->selection.lpsellistVertices);
	DestroySelectionList(lpmwd->selection.lpsellistThings);

	/* Destroy map structure. */
	DestroyMapStructure(lpmwd->lpmap);

	/* Destroy window structure. */
	HeapFree(g_hProcHeap, 0, lpmwd);
}

/* MapWinBelongsToWad
 *   Determines whether a map window belongs to a particular wad.
 *
 * Parameters:
 *   HWND		hwnd		Window handle.
 *   int		iWadID		ID of wad.
 *
 * Return value: BOOL
 *   TRUE if window belongs to wad; FALSE otherwise.
 */
BOOL MapWinBelongsToWad(HWND hwnd, int iWadID)
{
	return ((MAPWINDATA*)GetWindowLong(hwnd, GWL_USERDATA))->iWadID == iWadID;
}


/* MapKeyDown
 *   Performs certain keypress operations for a map window.
 *
 * Parameters:
 *   MAPWINDATA *lpmwd		Window data.
 *   int		iKeyCode	Key-code containing VK and shift states.
 *
 * Return value: BOOL
 *   TRUE if something done in response to keypress; FALSE otherwise.
 *
 * Notes:
 *   Most keypresses are handled by accelerators, but certain ones -- e.g. those
 *   that have separate up/down responses -- are handled separately.
 */
static BOOL MapKeyDown(MAPWINDATA *lpmwd, int iKeyCode)
{
	if(iKeyCode == g_iShortcutCodes[SCK_EDITQUICKMOVE])
	{
		if(lpmwd->editmode != EM_MOVE)
		{
			POINT ptCursor;

			/* End the draw operation if we're in one. */
			if(lpmwd->submode == ESM_DRAWING)
				EndDrawOperation(lpmwd->lpmap, &lpmwd->drawop);

			lpmwd->submode = ESM_MOVING;
			
			/* Store the cursor. */
			lpmwd->hCursor = LoadCursor(g_hInstance, MAKEINTRESOURCE(IDC_GRABOPEN));

			/* Show the cursor if we're in the client area. */
			GetCursorPos(&ptCursor);
			if(SendMessage(lpmwd->hwnd, WM_NCHITTEST, 0, MAKELPARAM(ptCursor.x, ptCursor.y)) == HTCLIENT)
				SetCursor(lpmwd->hCursor);

			return TRUE;
		}
	}
	else if(iKeyCode == g_iShortcutCodes[SCK_ZOOMIN] || iKeyCode == g_iShortcutCodes[SCK_ZOOMOUT])
	{
		float fNewZoom;
		float cxDelta, cyDelta;
		BOOL bMouseInside;
		POINT ptCursor;
		LRESULT lresult;

		/* Determine whether the mouse is over our map. */
		GetCursorPos(&ptCursor);
		bMouseInside = ((lresult=SendMessage(lpmwd->hwnd, WM_NCHITTEST, 0, MAKELPARAM(ptCursor.x, ptCursor.y))) == HTCLIENT);

		/* Zoom in or out, according to request. */
		fNewZoom = lpmwd->mapview.fZoom * (1 + (((iKeyCode == g_iShortcutCodes[SCK_ZOOMIN]) ? 1 : -1) * ConfigGetInteger(g_lpcfgMain, "zoomspeed") / 1000.0f));

		/* Bounds. */
		if(fNewZoom > 100) fNewZoom = 100;
		else if(fNewZoom < 0.01f) fNewZoom = 0.01f;

		if(ConfigGetInteger(g_lpcfgMain, "zoommouse") && bMouseInside)
		{
			/* Fracunit x-origin difference = 
			 *		(new fracunit width - old fracunit width) *
			 *		mouse pos as proportion of width.
			 *
			 * Similarly height.
			 */
			cxDelta = (((lpmwd->mapview.cxDrawSurface / fNewZoom) - (lpmwd->mapview.cxDrawSurface / lpmwd->mapview.fZoom)) * ((float)lpmwd->xMouseLast / lpmwd->mapview.cxDrawSurface));
			cyDelta = (((lpmwd->mapview.cyDrawSurface / fNewZoom) - (lpmwd->mapview.cyDrawSurface / lpmwd->mapview.fZoom)) * ((float)lpmwd->yMouseLast / lpmwd->mapview.cyDrawSurface));
		}
		else
		{
			cxDelta = (((lpmwd->mapview.cxDrawSurface / fNewZoom) - (lpmwd->mapview.cxDrawSurface / lpmwd->mapview.fZoom)) / 2);
			cyDelta = (((lpmwd->mapview.cyDrawSurface / fNewZoom) - (lpmwd->mapview.cyDrawSurface / lpmwd->mapview.fZoom)) / 2);
		}

		lpmwd->mapview.xLeft -= cxDelta;
		lpmwd->mapview.yTop += cyDelta;
		SetZoom(&lpmwd->mapview, fNewZoom);

		/* Redraw map next frame. */
		lpmwd->bWantRedraw = TRUE;

		UpdateStatusBar(lpmwd, 1 << SBP_ZOOM);
	}

	return FALSE;
}



/* MapKeyUp
 *   Performs certain keyrelease operations for a map window.
 *
 * Parameters:
 *   MAPWINDATA*	lpmwd		Window data.
 *   int			iKeyCode	Key-code containing VK and shift states.
 *
 * Return value: BOOL
 *   TRUE if something done in response to keyrelease; FALSE otherwise.
 *
 * Notes:
 *   Most keypresses are handled by accelerators, but certain ones -- e.g. those
 *   that have separate up/down responses -- are handled separately.
 */
static BOOL MapKeyUp(MAPWINDATA *lpmwd, int iKeyCode)
{
	if(iKeyCode == g_iShortcutCodes[SCK_EDITQUICKMOVE])
	{
		if(lpmwd->editmode != EM_MOVE)
		{
			POINT pt;

			lpmwd->submode = ESM_NONE;
			
			/* Store the cursor. */
			lpmwd->hCursor = LoadCursor(NULL, IDC_ARROW);

			/* Show the cursor if we're in the client area. */
			pt.x = lpmwd->xMouseLast;
			pt.y = lpmwd->yMouseLast;
			ClientToScreen(lpmwd->hwnd, &pt);
			if(SendMessage(lpmwd->hwnd, WM_NCHITTEST, pt.x, pt.y) == HTCLIENT)
				SetCursor(lpmwd->hCursor);

			return TRUE;
		}
	}

	return FALSE;
}


/* RedrawMapWindow
 *   Performs certain keypress operations for a map window.
 *
 * Parameters:
 *   MAPWINDATA*	lpmwd		Window data.
 *
 * Return value: None.
 */
static void RedrawMapWindow(MAPWINDATA *lpmwd)
{
	DRAWOP_RENDERINFO dori;

	/* If we're in a drawing operation, set up the extra info. */
	if(lpmwd->submode == ESM_DRAWING)
	{
		/* We must have a previous vertex in ESM_DRAWING. */
		int iVertexPrev = lpmwd->drawop.lpiNewVertices[lpmwd->drawop.iNewVertexCount-1];

		dori.ptSrc.x = lpmwd->lpmap->vertices[iVertexPrev].x;
		dori.ptSrc.y = lpmwd->lpmap->vertices[iVertexPrev].y;
	}

	if(lpmwd->submode == ESM_DRAWING || lpmwd->submode == ESM_INSERTING)
	{
		short x, y;

		/* We draw the floating vertex at the mouse position. The drawing line
		 * will also be drawn *to* here if necessary.
		 */
		x = XMOUSE_WIN_TO_MAP(lpmwd->xMouseLast, lpmwd);
		y = YMOUSE_WIN_TO_MAP(lpmwd->yMouseLast, lpmwd);

		/* Do snapping on the point if necessary. */
		if((lpmwd->bySnapFlags ^ lpmwd->bySnapToggleMask) & SF_RECTANGLE)
			Snap(&x, &y, lpmwd->mapview.cxGrid, lpmwd->mapview.cyGrid);

		/* We needed these intermediates since Snap expects shorts, while POINTs
		 * have ints.
		 */
		dori.ptDest.x = x;
		dori.ptDest.y = y;
	}

	wglMakeCurrent(lpmwd->hdc, lpmwd->hrc);
	
	/* Draw onto backbuffer. */
	RedrawMap(lpmwd->lpmap, lpmwd->editmode, lpmwd->submode, &lpmwd->mapview, &dori);
	
	glFlush();
	SwapBuffers(lpmwd->hdc);
}


static void ChangeMode(MAPWINDATA *lpmwd, USHORT unModeMenuID)
{
	/* TODO: Do all the menu stuff on WM_INITMENUPOPUP. */

	static USHORT s_unModeToMenu[7] =
	{
		IDM_VIEW_MODE_MOVE, IDM_VIEW_MODE_ANY,
		IDM_VIEW_MODE_VERTICES, IDM_VIEW_MODE_LINES,
		IDM_VIEW_MODE_SECTORS, IDM_VIEW_MODE_THINGS,
		IDM_VIEW_3D
	};

	HMENU hmenuView;
	

	/* Get handle to View menu. */
	hmenuView = GetSubMenu(g_hmenuMap, VIEWMENUPOS);


	/* Uncheck old modes if we're not about to move. */
	if(unModeMenuID != IDM_VIEW_MODE_MOVE)
	{
		CheckMenuItem(hmenuView, s_unModeToMenu[lpmwd->editmode], MF_BYCOMMAND | MF_UNCHECKED);
		CheckMenuItem(hmenuView, s_unModeToMenu[lpmwd->prevmode], MF_BYCOMMAND | MF_UNCHECKED);
	}


	if(unModeMenuID == IDM_VIEW_MODE_MOVE && lpmwd->editmode == EM_MOVE)
	{
		/* Restore previous mode. */
		ChangeMode(lpmwd, s_unModeToMenu[lpmwd->prevmode]);
		return;
	}
	else
	{
		lpmwd->prevmode = lpmwd->editmode;
	}
	
	/* Do things necessary on leaving old mode. */
	switch(lpmwd->editmode)
	{
	case EM_MOVE:
		break;
	case EM_ANY:
		break;
	case EM_LINES:
		break;
	case EM_SECTORS:
		break;
	case EM_VERTICES:
		break;
	case EM_THINGS:
		break;
	default:
        break;
	}


	switch(unModeMenuID)
	{
	case IDM_VIEW_MODE_MOVE:
		lpmwd->editmode = EM_MOVE;
		break;

	case IDM_VIEW_MODE_ANY:
		lpmwd->editmode = EM_ANY;
		break;

	case IDM_VIEW_MODE_LINES:
		SelectLinesFromVertices(lpmwd);
		SelectSectorsFromLines(lpmwd);
		DeselectAllThings(lpmwd);
		DeselectAllVertices(lpmwd);
		lpmwd->editmode = EM_LINES;
		break;

	case IDM_VIEW_MODE_SECTORS:
		SelectLinesFromVertices(lpmwd);
		SelectSectorsFromLines(lpmwd);
		DeselectAllThings(lpmwd);
		DeselectAllVertices(lpmwd);
		DeselectLinesFromPartialSectors(lpmwd);
		lpmwd->editmode = EM_SECTORS;
		break;

	case IDM_VIEW_MODE_THINGS:
		DeselectAllLines(lpmwd);
		DeselectAllSectors(lpmwd);
		DeselectAllVertices(lpmwd);
		lpmwd->editmode = EM_THINGS;
		break;

	case IDM_VIEW_MODE_VERTICES:
		SelectVerticesFromLines(lpmwd);
		DeselectAllLines(lpmwd);
		DeselectAllSectors(lpmwd);
		DeselectAllThings(lpmwd);
		lpmwd->editmode = EM_VERTICES;
		break;
	}

	/* Check new mode. */
	CheckMenuItem(hmenuView, unModeMenuID, MF_BYCOMMAND | MF_CHECKED);

	/* Update highlight if mouse is in window. */
	if(lpmwd->xMouseLast >= 0 && lpmwd->yMouseLast >= 0)
		UpdateHighlight(lpmwd, lpmwd->xMouseLast, lpmwd->yMouseLast);

	/* Set up info bar. */
	SetInfoBarMode(lpmwd->editmode);
	MapWinUpdateInfoBar(lpmwd);
}



/* StatusBarMapWindow
 *   Sets the status bar to correspond to the active map window.
 *
 * Parameters:
 *   None.
 *
 * Return value: None.
 */
static void StatusBarMapWindow(void)
{
	INT lpiWidths[SBP_LAST] = {96, 84, 84, 84, 84, 84, 84, 84, 96, -1};
	int i;

	/* Calculate RHS from widths. Loop from second to second-to-last: The first
	 * and last are okay.
	 */
	for(i = 1; i < SBP_LAST - 1; i++) lpiWidths[i] += lpiWidths[i-1];

	SendMessage(g_hwndStatusBar, SB_SETPARTS, SBP_LAST, (LPARAM)lpiWidths);

	/* Not simple mode. */
	SendMessage(g_hwndStatusBar, SB_SIMPLE, FALSE, 0);
}


/* UpdateStatusBar
 *   Updates the status bar's panels' text.
 *
 * Parameters:
 *   MAPWINDATA*	lpmwd	Pointer to data for active window.
 *   DWORD			dwFlags	Flags specifiying which panels to update.
 *
 * Return value: None.
 *
 * Remarks:
 *   Each bit of the flag corresponds to the equally-numbered constant specified
 *   in _ENUM_SBPANELS.
 */
static void UpdateStatusBar(MAPWINDATA *lpmwd, DWORD dwFlags)
{
	TCHAR szBuffer[256], szFromStringTable[32];
	MAP *lpmap = lpmwd->lpmap;

	/* Check for each field, and update if asked. */
	if(dwFlags & (1 << SBP_MODE))
	{
		DWORD dwStringID = 0;

		switch(lpmwd->editmode)
		{
			case EM_ANY:		dwStringID = IDS_SB_ANYMODE;		break;
			case EM_SECTORS:	dwStringID = IDS_SB_SECTORSMODE;	break;
			case EM_LINES:		dwStringID = IDS_SB_LINESMODE;		break;
			case EM_VERTICES:	dwStringID = IDS_SB_VERTICESMODE;	break;
			case EM_THINGS:		dwStringID = IDS_SB_THINGSMODE;		break;
			default:												break;
		}

		LoadString(g_hInstance, dwStringID, szFromStringTable, sizeof(szFromStringTable) / sizeof(TCHAR));
		_sntprintf(szBuffer, sizeof(szBuffer)/sizeof(TCHAR) - 1, TEXT("\t%s"), szFromStringTable);
		szBuffer[255] = '\0';
		SendMessage(g_hwndStatusBar, SB_SETTEXT, SBP_MODE, (LPARAM)szBuffer);
	}

	if(dwFlags & (1 << SBP_SECTORS))
	{
		LoadString(g_hInstance, IDS_SB_SECTORS, szFromStringTable, sizeof(szFromStringTable) / sizeof(TCHAR));
		_sntprintf(szBuffer, sizeof(szBuffer)/sizeof(TCHAR) - 1, TEXT("\t%d %s"), lpmap->iSectors, szFromStringTable);
		szBuffer[255] = '\0';
		SendMessage(g_hwndStatusBar, SB_SETTEXT, SBP_SECTORS, (LPARAM)szBuffer);
	}

	if(dwFlags & (1 << SBP_LINEDEFS))
	{
		LoadString(g_hInstance, IDS_SB_LINEDEFS, szFromStringTable, sizeof(szFromStringTable) / sizeof(TCHAR));
		_sntprintf(szBuffer, sizeof(szBuffer)/sizeof(TCHAR) - 1, TEXT("\t%d %s"), lpmap->iLinedefs, szFromStringTable);
		szBuffer[255] = '\0';
		SendMessage(g_hwndStatusBar, SB_SETTEXT, SBP_LINEDEFS, (LPARAM)szBuffer);
	}

	if(dwFlags & (1 << SBP_SIDEDEFS))
	{
		LoadString(g_hInstance, IDS_SB_SIDEDEFS, szFromStringTable, sizeof(szFromStringTable) / sizeof(TCHAR));
		_sntprintf(szBuffer, sizeof(szBuffer)/sizeof(TCHAR) - 1, TEXT("\t%d %s"), lpmap->iSidedefs, szFromStringTable);
		szBuffer[255] = '\0';
		SendMessage(g_hwndStatusBar, SB_SETTEXT, SBP_SIDEDEFS, (LPARAM)szBuffer);
	}

	if(dwFlags & (1 << SBP_VERTICES))
	{
		LoadString(g_hInstance, IDS_SB_VERTICES, szFromStringTable, sizeof(szFromStringTable) / sizeof(TCHAR));
		_sntprintf(szBuffer, sizeof(szBuffer)/sizeof(TCHAR) - 1, TEXT("\t%d %s"), lpmap->iVertices, szFromStringTable);
		szBuffer[255] = '\0';
		SendMessage(g_hwndStatusBar, SB_SETTEXT, SBP_VERTICES, (LPARAM)szBuffer);
	}

	if(dwFlags & (1 << SBP_THINGS))
	{
		LoadString(g_hInstance, IDS_SB_THINGS, szFromStringTable, sizeof(szFromStringTable) / sizeof(TCHAR));
		_sntprintf(szBuffer, sizeof(szBuffer)/sizeof(TCHAR) - 1, TEXT("\t%d %s"), lpmap->iThings, szFromStringTable);
		szBuffer[255] = '\0';
		SendMessage(g_hwndStatusBar, SB_SETTEXT, SBP_THINGS, (LPARAM)szBuffer);
	}

	if(dwFlags & (1 << SBP_COORD))
	{
		_sntprintf(szBuffer, sizeof(szBuffer)/sizeof(TCHAR) - 1, TEXT("\t(%d, %d)"), XMOUSE_WIN_TO_MAP(lpmwd->xMouseLast, lpmwd), YMOUSE_WIN_TO_MAP(lpmwd->yMouseLast, lpmwd));
		szBuffer[255] = '\0';
		SendMessage(g_hwndStatusBar, SB_SETTEXT, SBP_COORD, (LPARAM)szBuffer);
	}

	if(dwFlags & (1 << SBP_GRID))
	{
		LoadString(g_hInstance, IDS_SB_GRID, szFromStringTable, sizeof(szFromStringTable) / sizeof(TCHAR));

		/* If the grid is square, don't duplicate the dimension. */
		if(lpmwd->mapview.cxGrid == lpmwd->mapview.cyGrid)
			_sntprintf(szBuffer, sizeof(szBuffer)/sizeof(TCHAR) - 1, TEXT("\t%s: %d"), szFromStringTable, lpmwd->mapview.cxGrid);
		else
			_sntprintf(szBuffer, sizeof(szBuffer)/sizeof(TCHAR) - 1, TEXT("\t%s: (%d, %d)"), szFromStringTable, lpmwd->mapview.cxGrid, lpmwd->mapview.cyGrid);

		szBuffer[255] = '\0';

		SendMessage(g_hwndStatusBar, SB_SETTEXT, SBP_GRID, (LPARAM)szBuffer);
	}

	if(dwFlags & (1 << SBP_ZOOM))
	{
		LoadString(g_hInstance, IDS_SB_ZOOM, szFromStringTable, sizeof(szFromStringTable) / sizeof(TCHAR));

		/* If the grid is square, don't duplicate the dimension. */
		_sntprintf(szBuffer, sizeof(szBuffer)/sizeof(TCHAR) - 1, TEXT("\t%s: %d%%"), szFromStringTable, ROUND(lpmwd->mapview.fZoom * 100));

		szBuffer[255] = '\0';

		SendMessage(g_hwndStatusBar, SB_SETTEXT, SBP_ZOOM, (LPARAM)szBuffer);
	}
}


/* UpdateHighlight, ClearHighlight
 *   Updates highlighted object for a window, given mouse co-ordinates.
 *
 * Parameters:
 *   MAPWINDATA*	lpmwd			Pointer to data for window.
 *   int			xMouse, yMouse	Mouse co-ordinates.
 *
 * Return value: BOOL
 *   True if highlighted object was changed; FALSE otherwise.
 *
 * Remarks:
 *   If any of the mouse co-ordinates are negative, clear the highlight.
 *   ClearHighlight calls UpdateHighlight with xMouse, yMouse == -1.
 */
static BOOL UpdateHighlight(MAPWINDATA *lpmwd, int xMouse, int yMouse)
{
	HIGHLIGHTOBJ highlightobj;

	if(xMouse >= 0 && yMouse >= 0)
	{
		int xMap, yMap;

		xMap = XMOUSE_WIN_TO_MAP(xMouse, lpmwd);
		yMap = YMOUSE_WIN_TO_MAP(yMouse, lpmwd);

		/* Find the nearest object for the mode. */
		GetNearestObject(lpmwd, xMap, yMap, lpmwd->editmode, &highlightobj);
	}
	else
	{
		/* Just clear the highlight. */
		highlightobj.emType = EM_MOVE;
	}

	/* Has the highlight changed? */
	if(highlightobj.emType != lpmwd->highlightobj.emType || (highlightobj.emType != EM_MOVE && highlightobj.iIndex != lpmwd->highlightobj.iIndex))
	{
		/* If so, redraw with new highlight. */
		ClearHighlight(lpmwd);
		SetHighlight(lpmwd, &highlightobj);
		lpmwd->bWantRedraw = TRUE;

		/* Highlight changed. */
		return TRUE;
	}

	/* Highlight didn't change. */
	return FALSE;
}


/* GetNearestObject
 *   Gets index and type of object nearest to a point on the map, according to
 *   mode.
 *
 * Parameters:
 *   MAPWINDATA*	lpmwd			Pointer to data for window.
 *   int			xMap, yMap		Map co-ordinates to find obj nearest to.
 *   ENUM_EDITMODE	mode			Editing mode, specifying types of obj.
 *   HIGHLIGHTOBJ*	lphighlightobj	Nearest object is returned here.
 *
 * Return value: None.
 *
 * Remarks:
 *   It's possible that nothing will be found, in which case the emType field of
 *   lphighlightobj will be set to EM_MOVE.
 */
static void GetNearestObject(MAPWINDATA *lpmwd, int xMap, int yMap, ENUM_EDITMODE mode, HIGHLIGHTOBJ *lphighlightobj)
{
	MAP *lpmap = lpmwd->lpmap;

	BOOL bWantLinedef = (mode == EM_LINES) || (mode == EM_ANY);
	BOOL bWantSector = (mode == EM_SECTORS) || (mode == EM_ANY);
	BOOL bWantThing = (mode == EM_THINGS) || (mode == EM_ANY);
	BOOL bWantVertex = (mode == EM_VERTICES) || (mode == EM_ANY);

	int iDistLinedef, iDistThing, iDistVertex;
	int iIndexLinedef = 0, iIndexSector = 0, iIndexThing = 0, iIndexVertex = 0;

	int iMinDist = INT_MAX;


	/* Begin by assuming we don't find anything. */
	lphighlightobj->emType = EM_MOVE;


	/* Find nearest objects. If we don't find any at each stage, indicate that
	 * we're no longer looking for objects of that type.
	 */

	if((bWantLinedef || bWantSector)
		&& -1 == (iIndexLinedef = NearestLinedef(xMap, yMap, lpmap->vertices, lpmap->linedefs, lpmap->iLinedefs, &iDistLinedef)))
	{
		bWantLinedef = bWantSector = FALSE;
	}

	if(bWantThing && -1 == (iIndexThing = NearestThing(xMap, yMap, lpmap->things, lpmap->iThings, &iDistThing, FALSE, NULL)))
		bWantThing = FALSE;

	if(bWantVertex && -1 == (iIndexVertex = NearestVertex(lpmap, xMap, yMap, &iDistVertex)))
		bWantVertex = FALSE;


	/* For sectors, we also check that we're actually *in* a sector. If we are,
	 * get its index.
	 */
	if(bWantSector)
	{
		iIndexSector = IntersectSector(xMap, yMap, lpmap->vertices, lpmap->linedefs, lpmap->sidedefs, lpmap->iLinedefs, NULL, NULL);
		if(iIndexSector < 0) bWantSector = FALSE;
	}


	/* If we're within VERTEX_THRESHOLD of a vertex, it takes precedence over
	 * linedefs and sectors.
	 */
	if(bWantVertex && iDistVertex * lpmwd->mapview.fZoom <= VERTEX_THRESHOLD)
		bWantLinedef = bWantSector = FALSE;

	/* Turn off one of these. */
	if(bWantLinedef && bWantSector)
	{
		/* Are we near enough to the linedef to choose it in preference to the
		 * sector?
		 */
		if(iDistLinedef * lpmwd->mapview.fZoom <= LINEDEF_THRESHOLD)
			bWantSector = FALSE;		/* Pick linedef. */
		else bWantLinedef = FALSE;		/* Pick sector. */
	}

	if(bWantLinedef && iDistLinedef < iMinDist)
	{
		iMinDist = iDistLinedef;
		lphighlightobj->emType = EM_LINES;
		lphighlightobj->iIndex = iIndexLinedef;
	}

	if(bWantThing && iDistThing < iMinDist)
	{
		iMinDist = iDistThing;
		lphighlightobj->emType = EM_THINGS;
		lphighlightobj->iIndex = iIndexThing;
	}

	if(bWantVertex && iDistVertex < iMinDist)
	{
		iMinDist = iDistVertex;
		lphighlightobj->emType = EM_VERTICES;
		lphighlightobj->iIndex = iIndexVertex;
	}

	/* We've checked everything except sectors now. These must respect the hover
	 * threshold.
	 */
	if(iMinDist * lpmwd->mapview.fZoom > HOVER_THRESHOLD)
		lphighlightobj->emType = EM_MOVE;

	/* With sectors, pick one if it's nearest *or* if we haven't found anything
	 * else yet. This assumes we're interested at all, of course.
	 */
	if(bWantSector && (iDistLinedef < iMinDist || lphighlightobj->emType == EM_MOVE))
	{
		iMinDist = iDistLinedef;
		lphighlightobj->emType = EM_SECTORS;
		lphighlightobj->iIndex = iIndexSector;
	}
}


/* ClearHighlight
 *   Sets a window's highlighted object to nothing.
 *
 * Parameters:
 *   MAPWINDATA*	lpmwd			Pointer to data for window.
 *
 * Return value: None.
 */
static void ClearHighlight(MAPWINDATA *lpmwd)
{
	int i, iLinedefs;
	MAP *lpmap = lpmwd->lpmap;
	int iIndex = lpmwd->highlightobj.iIndex;

	/* Clear the map-data highlight flag. */
	switch(lpmwd->highlightobj.emType)
	{
	case EM_SECTORS:

		/* For sectors, we actually highlight the linedefs. */
		iLinedefs = lpmwd->lpmap->iLinedefs;
		for(i=0; i<iLinedefs; i++)
		{
			if(LinedefBelongsToSector(lpmap, i, iIndex))
				lpmap->linedefs[i].highlight = 0;
		}

		break;

	case EM_LINES:
		lpmap->linedefs[iIndex].highlight = 0;
		break;
	case EM_VERTICES:
		lpmap->vertices[iIndex].highlight = 0;
		break;
	case EM_THINGS:
		lpmap->things[iIndex].highlight = 0;
		break;
	default:
        break;
	}

	/* Move mode indicates no higlight. There's *kind* of a logic to it... */
	lpmwd->highlightobj.emType = EM_MOVE;
}


/* SetHighlight
 *   Sets a window's highlighted object.
 *
 * Parameters:
 *   MAPWINDATA*	lpmwd			Pointer to data for window.
 *   HIGHLIGHTOBJ*	lphighlightobj	Pointer to object data.
 *
 * Return value: None.
 */
static void SetHighlight(MAPWINDATA *lpmwd, HIGHLIGHTOBJ *lphightlightobj)
{
	int i, iLinedefs;
	MAP *lpmap = lpmwd->lpmap;
	int iIndex;

	/* Store the details in the window data. */
	lpmwd->highlightobj = *lphightlightobj;
	iIndex = lphightlightobj->iIndex;

	/* Set the highlight flag in the map data. */
	switch(lphightlightobj->emType)
	{
	case EM_SECTORS:
		
		/* For sectors, we actually highlight the linedefs. */
		iLinedefs = lpmwd->lpmap->iLinedefs;
		for(i=0; i<iLinedefs; i++)
		{
			if(LinedefBelongsToSector(lpmap, i, iIndex))
				lpmap->linedefs[i].highlight = 1;
		}

		break;

	case EM_LINES:
		lpmap->linedefs[iIndex].highlight = 1;
		break;
	case EM_VERTICES:
		lpmap->vertices[iIndex].highlight = 1;
		break;
	case EM_THINGS:
		lpmap->things[iIndex].highlight = 1;
		break;
	default:
        break;
	}
}


/* MouseMove
 *   Perform map window's WM_MOUSEMOVE processing.
 *
 * Parameters:
 *   MAPWINDATA*	lpmwd			Pointer to data for window.
 *   WORD			xMouse, yMouse	Mouse co-ordinates.
 *
 * Return value: LRESULT
 *   Value window procedure should return.
 */
static LRESULT MouseMove(MAPWINDATA *lpmwd, WORD xMouse, WORD yMouse)
{
	/* Set the selected cursor. This is fast if it's already selected. */
	SetCursor(lpmwd->hCursor);

	/* Depending on the submode, we process mouse moves differently. */
	switch(lpmwd->submode)
	{
	case ESM_NONE:
		/* Update the highlight (only redraws if necessary). */
		if(UpdateHighlight(lpmwd, xMouse, yMouse))
		{
			/* If the highlight changed, update the info-bar. */
			MapWinUpdateInfoBar(lpmwd);
		}

		break;

	case ESM_MOVING:
		/* Pan the map. */
		lpmwd->mapview.xLeft += (lpmwd->xMouseLast - xMouse) / lpmwd->mapview.fZoom;
		lpmwd->mapview.yTop -= (lpmwd->yMouseLast - yMouse) / lpmwd->mapview.fZoom;

		lpmwd->bWantRedraw = TRUE;

		break;

	case ESM_PREDRAG:
		{
			/* TODO: Move to its own function? */

			/* Create an undo marker, but don't clear the redo stack, since the
			 * user might cancel.
			 */
			CreateUndo(lpmwd, IDS_UNDO_DRAG);

			/* Convert to a selection of things and vertices in an extra
			 * structure, so as not to interfere with the real selection.
			 */
			CreateAuxiliarySelection(lpmwd);

			/* Mark all selected lines as dragged. */
			LabelSelectedLinesRS(lpmwd->lpmap);

			/* Find loops. */
			lpmwd->lplooplistRS = LabelRSLinesLoops(lpmwd->lpmap);

			/* Any lines inside any of our loops in their initial positions also
			 * need to be marked RS.
			 */
			LabelLoopedLinesRS(lpmwd->lpmap, lpmwd->lplooplistRS);

			/* Enter the dragging state. */
			lpmwd->submode = ESM_DRAGGING;

			/* TODO: Capture mouse? */
		}

		/* Fall through. */

	case ESM_DRAGGING:
		{
			short xDragNew = XMOUSE_WIN_TO_MAP(xMouse, lpmwd);
			short yDragNew = YMOUSE_WIN_TO_MAP(yMouse, lpmwd);
			int i;

			/* Displace all selected vertices and things by the appropriate
			 * amount.
			 */
			for(i = 0; i < lpmwd->selectionAux.lpsellistVertices->iDataCount; i++)
			{
				lpmwd->lpmap->vertices[lpmwd->selectionAux.lpsellistVertices->lpiIndices[i]].x += xDragNew - lpmwd->xDragLast;
				lpmwd->lpmap->vertices[lpmwd->selectionAux.lpsellistVertices->lpiIndices[i]].y += yDragNew - lpmwd->yDragLast;
			}

			for(i = 0; i < lpmwd->selectionAux.lpsellistThings->iDataCount; i++)
			{
				lpmwd->lpmap->things[lpmwd->selectionAux.lpsellistThings->lpiIndices[i]].x += xDragNew - lpmwd->xDragLast;
				lpmwd->lpmap->things[lpmwd->selectionAux.lpsellistThings->lpiIndices[i]].y += yDragNew - lpmwd->yDragLast;
			}

			/* Update stored co-ordinates. */
			lpmwd->xDragLast = xDragNew;
			lpmwd->yDragLast = yDragNew;

			lpmwd->bWantRedraw = TRUE;
		}

		break;

	case ESM_DRAWING:
	case ESM_INSERTING:
		/* Always redraw when drawing. */
		lpmwd->bWantRedraw = TRUE;
		break;		

	default:
        break;
	}

	/* Was the mouse not previously inside the window? */
	if(lpmwd->xMouseLast < 0 || lpmwd->yMouseLast < 0)
	{
		/* If so, track the pointer until it leaves. */
		TRACKMOUSEEVENT tme;

		tme.cbSize = sizeof(tme);
		tme.dwFlags = TME_LEAVE;
		tme.hwndTrack = lpmwd->hwnd;
		
		_TrackMouseEvent(&tme);
	}

	/* Save the cursor position. */
	lpmwd->xMouseLast = xMouse;
	lpmwd->yMouseLast = yMouse;

	UpdateStatusBar(lpmwd, 1 << SBP_COORD);

	/* Make window proc return 0. */
	return 0;
}


/* LButtonDown
 *   Perform map window's WM_LBUTTONDOWN processing.
 *
 * Parameters:
 *   MAPWINDATA*	lpmwd			Pointer to data for window.
 *   WORD			xMouse, yMouse	Mouse co-ordinates.
 *   WORD			wFlags			Flags corresponding to WM_LBUTTONDOWN's
 *									wParam.
 *
 * Return value: LRESULT
 *   Value window procedure should return.
 */
static LRESULT LButtonDown(MAPWINDATA *lpmwd, WORD xMouse, WORD yMouse, WORD wFlags)
{
	UNREFERENCED_PARAMETER(xMouse);
	UNREFERENCED_PARAMETER(yMouse);
	UNREFERENCED_PARAMETER(wFlags);
	switch(lpmwd->submode)
	{
	case ESM_NONE:
		/* Normal processing if we're not doing anything special. */

		/* Select/deselect an object if we're pointing at one. */
		if(lpmwd->highlightobj.emType != EM_MOVE)
		{
			/* If object isn't selected, add it to selection; remove it,
			 * otherwise.
			 */
			if(!ObjectSelected(lpmwd, &lpmwd->highlightobj))
				AddObjectToSelection(lpmwd, &lpmwd->highlightobj);
			else
				RemoveObjectFromSelection(lpmwd, &lpmwd->highlightobj);

			lpmwd->bWantRedraw = TRUE;
		}
		/* Not pointing at anything. */
		else if(ConfigGetInteger(g_lpcfgMain, "nothingdeselects"))
		{
			DeselectAll(lpmwd);
			lpmwd->bWantRedraw = TRUE;
		}

		break;

	default:
		break;
	}

	return 0;
}


/* LButtonUp
 *   Perform map window's WM_LBUTTONUP processing.
 *
 * Parameters:
 *   MAPWINDATA*	lpmwd			Pointer to data for window.
 *   WORD			xMouse, yMouse	Mouse co-ordinates.
 *   WORD			wFlags			Flags corresponding to WM_LBUTTONDOWN's
 *									wParam.
 *
 * Return value: LRESULT
 *   Value window procedure should return.
 */
static LRESULT LButtonUp(MAPWINDATA *lpmwd, WORD xMouse, WORD yMouse, WORD wFlags)
{
	UNREFERENCED_PARAMETER(wFlags);
	switch(lpmwd->submode)
	{
	case ESM_INSERTING:
		{
			short x = (short)XMOUSE_WIN_TO_MAP(xMouse, lpmwd);
			short y = (short)YMOUSE_WIN_TO_MAP(yMouse, lpmwd);

			if((lpmwd->bySnapFlags ^ lpmwd->bySnapToggleMask) & SF_RECTANGLE)
				Snap(&x, &y, lpmwd->mapview.cxGrid, lpmwd->mapview.cyGrid);

			/* Undo is handled in here. */
			DoInsertOperation(lpmwd, x, y);
			lpmwd->bWantRedraw = TRUE;
		}

		break;

	case ESM_DRAWING:
		{
			short x = (short)XMOUSE_WIN_TO_MAP(xMouse, lpmwd);
			short y = (short)YMOUSE_WIN_TO_MAP(yMouse, lpmwd);

			if((lpmwd->bySnapFlags ^ lpmwd->bySnapToggleMask) & SF_RECTANGLE)
				Snap(&x, &y, lpmwd->mapview.cxGrid, lpmwd->mapview.cyGrid);

			/* Draw the next vertex. */
			if(!DrawToNewVertex(lpmwd->lpmap, &lpmwd->drawop, x, y))
			{
				/* Drawing this vertex finishes the drawing operation. */
				EndDrawOperation(lpmwd->lpmap, &lpmwd->drawop);
				lpmwd->submode = ESM_NONE;

				ClearRedoStack(lpmwd);
				lpmwd->bMapChanged = TRUE;
			}

			lpmwd->bWantRedraw = TRUE;
		}

		break;
	
	default:
        break;
	}

	return 0;
}



/* RButtonUp
 *   Perform map window's WM_RBUTTONUP processing.
 *
 * Parameters:
 *   MAPWINDATA*	lpmwd			Pointer to data for window.
 *   WORD			xMouse, yMouse	Mouse co-ordinates.
 *   WORD			wFlags			Flags corresponding to WM_RBUTTONUP's
 *									wParam.
 *
 * Return value: LRESULT
 *   Value window procedure should return.
 */
static LRESULT RButtonUp(MAPWINDATA *lpmwd, WORD xMouse, WORD yMouse, WORD wFlags)
{
	UNREFERENCED_PARAMETER(wFlags);
	switch(lpmwd->submode)
	{
	case ESM_MOVING:
		/* Stop panning. */
		lpmwd->submode = ESM_NONE;
		/* TODO: Restore cursor? */
		break;

	case ESM_INSERTING:		
		{
			short x = (short)XMOUSE_WIN_TO_MAP(xMouse, lpmwd);
			short y = (short)YMOUSE_WIN_TO_MAP(yMouse, lpmwd);

			if((lpmwd->bySnapFlags ^ lpmwd->bySnapToggleMask) & SF_RECTANGLE)
				Snap(&x, &y, lpmwd->mapview.cxGrid, lpmwd->mapview.cyGrid);

			/* Undo is handled in here. */
			DoInsertOperation(lpmwd, x, y);
			lpmwd->bWantRedraw = TRUE;
		}

		break;

	case ESM_PREDRAG:
		/* If we get a mouse-up while still in this state, then the mouse didn't
		 * move (otherwise we'd've moved into ESM_DRAGGING). Hence, we should
		 * show the selection's properties.
		 */

		if(lpmwd->selection.lpsellistLinedefs->iDataCount > 0 ||
			lpmwd->selection.lpsellistSectors->iDataCount > 0 ||
			lpmwd->selection.lpsellistThings->iDataCount > 0 ||
			lpmwd->selection.lpsellistVertices->iDataCount > 0)
		{
			/* Selection properties. Display a context menu if the user likes
			 * that sort of thing; otherwise go straight to properties dialogue.
			 */
			if(ConfigGetInteger(g_lpcfgMain, "contextmenus"))
			{
				/* TODO: Context-menus. These depend on the mode *and* the
				 * nature of the selected objects, if indeed there are any.
				 */
			}
			else
			{
				/* Undo is all handled in here. */
				ShowSelectionProperties(lpmwd);
			}
		}

		/* Were the objects selected before we clicked on them? If not, deselect
		 * them.
		 */
		if(lpmwd->bDeselectAfterEdit) DeselectAll(lpmwd);

		/* Restore normal behaviour. */
		lpmwd->submode = ESM_NONE;
		lpmwd->bWantRedraw = TRUE;

		break;

	case ESM_DRAGGING:

		/* This also clears the redo stack. */
		EndDragOperation(lpmwd);

		break;
		
	/* Nothing happens in ESM_NONE: We're never pointing at anything, since that
	 * would have triggered a move to ESM_PREDRAG at the mouse-down stage.
	 */
	
	default:
	    break;
	}

	return 0;
}


/* RButtonDown
 *   Perform map window's WM_RBUTTONDOWN processing.
 *
 * Parameters:
 *   MAPWINDATA*	lpmwd			Pointer to data for window.
 *   WORD			xMouse, yMouse	Mouse co-ordinates.
 *   WORD			wFlags			Flags corresponding to WM_RBUTTONUP's
 *									wParam.
 *
 * Return value: LRESULT
 *   Value window procedure should return.
 */
static LRESULT RButtonDown(MAPWINDATA *lpmwd, WORD xMouse, WORD yMouse, WORD wFlags)
{
	UNREFERENCED_PARAMETER(xMouse);
	UNREFERENCED_PARAMETER(yMouse);
	UNREFERENCED_PARAMETER(wFlags);
	switch(lpmwd->submode)
	{
	case ESM_NONE:
		/* Not pointing at anything? */
		if(lpmwd->highlightobj.emType == EM_MOVE)
		{
			/* Begin insertion, of a type proper to the mode. */
			switch(lpmwd->editmode)
			{
			case EM_ANY:
			case EM_LINES:
			case EM_SECTORS:
			case EM_VERTICES:
				/* This is a prelude to ESM_DRAWING. Also, note that the map
				 * hasn't changed yet!
				 */
				lpmwd->submode = ESM_INSERTING;
				BeginDrawOperation(&lpmwd->drawop);

				break;
				
			default:
                break;
			}

			lpmwd->bWantRedraw = TRUE;
		}
		else
		{
			/* We are pointing at something, so enter into the I'm-either-going-
			 * to-drag-you-or-alter-your-properties state, after altering the
			 * selection as necessary.
			 */

			/* If the object was already selected, great; otherwise clear the
			 * existing selection and then select the new object.
			 */
			if(!ObjectSelected(lpmwd, &lpmwd->highlightobj))
			{
				DeselectAll(lpmwd);				
				AddObjectToSelection(lpmwd, &lpmwd->highlightobj);
				lpmwd->bDeselectAfterEdit = TRUE;
			}
			else
				lpmwd->bDeselectAfterEdit = FALSE;

			/* Clear the highlight, but save what we're pointing at first. */
			lpmwd->hobjDrag = lpmwd->highlightobj;
			if(UpdateHighlight(lpmwd, -1, -1))
				MapWinUpdateInfoBar(lpmwd);

			/* Store the current map co-ordinates of the mouse. These are reset
			 * with every mouse move.
			 */
			lpmwd->xDragLast = XMOUSE_WIN_TO_MAP(lpmwd->xMouseLast, lpmwd);
			lpmwd->yDragLast = YMOUSE_WIN_TO_MAP(lpmwd->yMouseLast, lpmwd);

			/* Set the state and redraw. */
			lpmwd->submode = ESM_PREDRAG;
			lpmwd->bWantRedraw = TRUE;
		}

		break;
	
	default:
        break;
	}

	return 0;
}


/* ObjectSelected
 *   Determines whether a certain object is selected.
 *
 * Parameters:
 *   MAPWINDATA*	lpmwd			Pointer to data for window.
 *   HIGHLIGHTOBJ*	lphighlightobj	Type and index of object.
 *
 * Return value: BOOL
 *   True if the object is selected; FALSE otherwise.
 */
static BOOL ObjectSelected(MAPWINDATA *lpmwd, HIGHLIGHTOBJ *lphighlightobj)
{
	switch(lphighlightobj->emType)
	{
	case EM_SECTORS:
		return ExistsInSelectionList(lpmwd->selection.lpsellistSectors, lphighlightobj->iIndex);
	case EM_LINES:
		return ExistsInSelectionList(lpmwd->selection.lpsellistLinedefs, lphighlightobj->iIndex);
	case EM_VERTICES:
		return ExistsInSelectionList(lpmwd->selection.lpsellistVertices, lphighlightobj->iIndex);
	case EM_THINGS:
		return ExistsInSelectionList(lpmwd->selection.lpsellistThings, lphighlightobj->iIndex);	
	default:
        break;
	}

	return FALSE;
}



/* AddObjectToSelection
 *   Selects an object.
 *
 * Parameters:
 *   MAPWINDATA*	lpmwd			Pointer to data for window.
 *   HIGHLIGHTOBJ*	lphighlightobj	Type and index of object.
 *
 * Return value: None.
 *
 * Remarks:
 *   The selection fields in the map data are also updated.
 */
static void AddObjectToSelection(MAPWINDATA *lpmwd, HIGHLIGHTOBJ *lphighlightobj)
{
	MAP *lpmap = lpmwd->lpmap;

	switch(lphighlightobj->emType)
	{
	case EM_SECTORS:
		{
			int i;

			AddToSelectionList(lpmwd->selection.lpsellistSectors, lphighlightobj->iIndex);
			lpmap->sectors[lphighlightobj->iIndex].selected = 1;

			/* Select and mark linedefs for this sector, but don't trigger any
			 * of the effects that would occur if the user selected them
			 * manually.
			 */
			for(i = 0; i < lpmap->iLinedefs; i++)
			{
				if((lpmap->linedefs[i].s1 >= 0 && lpmap->sidedefs[lpmap->linedefs[i].s1].sector == lphighlightobj->iIndex) ||
					(lpmap->linedefs[i].s2 >= 0 && lpmap->sidedefs[lpmap->linedefs[i].s2].sector == lphighlightobj->iIndex))
				{
					AddToSelectionList(lpmwd->selection.lpsellistLinedefs, i);
					lpmap->linedefs[i].selected = 1;
				}
			}
		}

		break;

	case EM_LINES:

		{
			int iFrontSec, iBackSec;
			int i;
			BOOL bAddFront, bAddBack;
			
			if(!lpmap->linedefs[lphighlightobj->iIndex].selected)
			{
				AddToSelectionList(lpmwd->selection.lpsellistLinedefs, lphighlightobj->iIndex);
				lpmap->linedefs[lphighlightobj->iIndex].selected = 1;
			}

			/* Did adding this linedef cause a complete sector to be selected?
			 * This takes a bit of working out, but if it did, add it. We need
			 * to check both front and back sectors. Cf. SelectSectorsFromLines,
			 * which rebuilds the whole lot; this is more efficient, though.
			 */
			iFrontSec = lpmap->linedefs[lphighlightobj->iIndex].s1 >= 0 ? lpmap->sidedefs[lpmap->linedefs[lphighlightobj->iIndex].s1].sector : -1;
			iBackSec = lpmap->linedefs[lphighlightobj->iIndex].s2 >= 0 ? lpmap->sidedefs[lpmap->linedefs[lphighlightobj->iIndex].s2].sector : -1;

			/* Assume that we add them, if they exist. */
			bAddFront = (iFrontSec >= 0);
			bAddBack = (iBackSec >= 0);

			/* Look through all sidedefs attached to linedefs; if we find an
			 * unselected sd referencing one of our sectors, we don't add it.
			 */
			for(i = 0; (bAddFront || bAddBack) && i < lpmap->iLinedefs; i++)
			{
				MAPLINEDEF *lpld = &lpmap->linedefs[i];
				if(!lpld->selected)
				{
					if(lpld->s1 >= 0)
					{
						int iSector = lpmap->sidedefs[lpld->s1].sector;
						if(iSector == iFrontSec) bAddFront = FALSE;
						if(iSector == iBackSec) bAddBack = FALSE;
					}

					if(lpld->s2 >= 0)
					{
						int iSector = lpmap->sidedefs[lpld->s2].sector;
						if(iSector == iFrontSec) bAddFront = FALSE;
						if(iSector == iBackSec) bAddBack = FALSE;
					}
				}
			}

			/* Add the sectors if we should. */
			if(bAddFront)
			{
				AddToSelectionList(lpmwd->selection.lpsellistSectors, iFrontSec);
				lpmap->sectors[iFrontSec].selected = 1;
			}

			if(bAddBack)
			{
				AddToSelectionList(lpmwd->selection.lpsellistSectors, iBackSec);
				lpmap->sectors[iBackSec].selected = 1;
			}
		}

		break;

	case EM_VERTICES:

		AddToSelectionList(lpmwd->selection.lpsellistVertices, lphighlightobj->iIndex);
		lpmap->vertices[lphighlightobj->iIndex].selected = 1;
		break;

	case EM_THINGS:

		AddToSelectionList(lpmwd->selection.lpsellistThings, lphighlightobj->iIndex);
		lpmap->things[lphighlightobj->iIndex].selected = 1;
		break;
		
	default:
        break;
	}
}



/* RemoveObjectFromSelection
 *   Deselects an object.
 *
 * Parameters:
 *   MAPWINDATA*	lpmwd			Pointer to data for window.
 *   HIGHLIGHTOBJ*	lphighlightobj	Type and index of object.
 *
 * Return value: BOOL
 *   TRUE if object was indeed previously selected; FALSE otherwise.
 *
 * Remarks:
 *   The selection fields in the map data are also updated.
 */
static BOOL RemoveObjectFromSelection(MAPWINDATA *lpmwd, HIGHLIGHTOBJ *lphighlightobj)
{
	BOOL bRet = FALSE;
	MAP *lpmap = lpmwd->lpmap;
	int i;

	switch(lphighlightobj->emType)
	{
	case EM_SECTORS:

		bRet = RemoveFromSelectionList(lpmwd->selection.lpsellistSectors, lphighlightobj->iIndex);
		lpmap->sectors[lphighlightobj->iIndex].selected = 0;

		/* Unmark linedefs for this sector. */
		for(i = 0; i < lpmap->iLinedefs; i++)
		{
			/* This is not particularly transparent. We deselect the linedef iff
			 * it belongs to us and the sector on its opposite side is
			 * unselected.
			 */
			if(((lpmap->linedefs[i].s1 >= 0 && lpmap->sidedefs[lpmap->linedefs[i].s1].sector == lphighlightobj->iIndex) &&
				!(lpmap->linedefs[i].s2 >= 0 && ExistsInSelectionList(lpmwd->selection.lpsellistSectors, lpmap->sidedefs[lpmap->linedefs[i].s2].sector)))
			|| ((lpmap->linedefs[i].s2 >= 0 && lpmap->sidedefs[lpmap->linedefs[i].s2].sector == lphighlightobj->iIndex) &&
				!(lpmap->linedefs[i].s1 >= 0 && ExistsInSelectionList(lpmwd->selection.lpsellistSectors, lpmap->sidedefs[lpmap->linedefs[i].s1].sector))))
			{
				RemoveFromSelectionList(lpmwd->selection.lpsellistLinedefs, i);
				lpmap->linedefs[i].selected = 0;
			}
		}

		break;

	case EM_LINES:
		RemoveFromSelectionList(lpmwd->selection.lpsellistLinedefs, lphighlightobj->iIndex);
		lpmap->linedefs[lphighlightobj->iIndex].selected = 0;

		/* Remove any sectors that might have become unselected. We don't
		 * actually check whether they *were* selected: it wouldn't be any
		 * quicker.
		 */
		if(lpmap->linedefs[lphighlightobj->iIndex].s1 >= 0)
		{
			int iSector = lpmap->sidedefs[lpmap->linedefs[lphighlightobj->iIndex].s1].sector;
			if(iSector >= 0) RemoveFromSelectionList(lpmwd->selection.lpsellistSectors, iSector);
		}

		if(lpmap->linedefs[lphighlightobj->iIndex].s2 >= 0)
		{
			int iSector = lpmap->sidedefs[lpmap->linedefs[lphighlightobj->iIndex].s2].sector;
			if(iSector >= 0) RemoveFromSelectionList(lpmwd->selection.lpsellistSectors, iSector);
		}

		break;
	case EM_VERTICES:
		RemoveFromSelectionList(lpmwd->selection.lpsellistVertices, lphighlightobj->iIndex);
		lpmap->vertices[lphighlightobj->iIndex].selected = 0;
		break;
	case EM_THINGS:
		RemoveFromSelectionList(lpmwd->selection.lpsellistThings, lphighlightobj->iIndex);
		lpmap->things[lphighlightobj->iIndex].selected = 0;
		break;
	default:
        break;
	}

	return bRet;
}


/* SelectVerticesFromLines
 *   Selects those vertices belonging to selected lines.
 *
 * Parameters:
 *   MAPWINDATA*	lpmwd	Pointer to map window data.
 *
 * Return value: None.
 *
 * Remarks:
 *   Doesn't deselect any vertices that are already selected.
 */
static void SelectVerticesFromLines(MAPWINDATA *lpmwd)
{
	int i;

	for(i = 0; i < lpmwd->lpmap->iLinedefs; i++)
	{
		MAPLINEDEF *lpld = &lpmwd->lpmap->linedefs[i];
		
		if(lpld->selected)
		{
			AddToSelectionList(lpmwd->selection.lpsellistVertices, lpld->v1);
			lpmwd->lpmap->vertices[lpld->v1].selected = 1;
			AddToSelectionList(lpmwd->selection.lpsellistVertices, lpld->v2);
			lpmwd->lpmap->vertices[lpld->v2].selected = 1;
		}
	}
}


/* SelectLinesFromVertices
 *   Selects those lines whose vertices are both selected.
 *
 * Parameters:
 *   MAPWINDATA*	lpmwd	Pointer to map window data.
 *
 * Return value: None.
 *
 * Remarks:
 *   Doesn't deselect any lines that are already selected. Doesn't trigger
 *   selection of any sectors that should become selected.
 */
static void SelectLinesFromVertices(MAPWINDATA *lpmwd)
{
	int i;

	for(i = 0; i < lpmwd->lpmap->iLinedefs; i++)
	{
		MAPLINEDEF *lpld = &lpmwd->lpmap->linedefs[i];
		
		if(lpmwd->lpmap->vertices[lpld->v1].selected && lpmwd->lpmap->vertices[lpld->v2].selected)
		{
			AddToSelectionList(lpmwd->selection.lpsellistLinedefs, i);
			lpld->selected = 1;
		}
	}
}


/* SelectSectorsFromLines
 *   Selects those sectors whose linedefs are all selected.
 *
 * Parameters:
 *   MAPWINDATA*	lpmwd	Pointer to map window data.
 *
 * Return value: None.
 *
 * Remarks:
 *   Doesn't deselect any sectors that are already selected.
 */
static void SelectSectorsFromLines(MAPWINDATA *lpmwd)
{
	MAP *lpmap = lpmwd->lpmap;
	int i;
	BOOL *lpbSelectSector;

	/* Sanity check. */
	if(lpmap->iSectors == 0) return;

	/* Begin by assuming that we're going to select all the sectors. */
	lpbSelectSector = (BOOL*)ProcHeapAlloc(lpmap->iSectors * sizeof(BOOL));
	FillMemory(lpbSelectSector, lpmap->iSectors * sizeof(BOOL), TRUE);

	/* Consider all unselected linedefs. */
	for(i = 0; i < lpmap->iLinedefs; i++)
	{
		MAPLINEDEF *lpld = &lpmap->linedefs[i];
		if(!lpld->selected)
		{
			/* We don't want to select any sectors belonging to this line. */
			int iFrontSec = lpld->s1 >= 0 ? lpmwd->lpmap->sidedefs[lpld->s1].sector : -1;	
			int iBackSec = lpld->s2 >= 0 ? lpmwd->lpmap->sidedefs[lpld->s2].sector : -1;

			if(iFrontSec >= 0) lpbSelectSector[iFrontSec] = FALSE;
			if(iBackSec >= 0) lpbSelectSector[iBackSec] = FALSE;
		}
	}

	/* Select all the sectors that we ought to. */
	for(i = 0; i < lpmap->iSectors; i++)
	{
		if(lpbSelectSector[i] && !lpmap->sectors[i].selected)
		{
			AddToSelectionList(lpmwd->selection.lpsellistSectors, i);
			lpmap->sectors[i].selected = 1;
		}
	}

	ProcHeapFree(lpbSelectSector);
}


/* DeselectAll
 *   Deselects all map objects.
 *
 * Parameters:
 *   MAPWINDATA*	lpmwd	Pointer to map window data.
 *
 * Return value: None.
 */
static void DeselectAll(MAPWINDATA *lpmwd)
{
	if(lpmwd->editmode == EM_ANY || lpmwd->editmode == EM_SECTORS || lpmwd->editmode == EM_LINES)
	{
		DeselectAllLines(lpmwd);
		DeselectAllSectors(lpmwd);
	}

	if(lpmwd->editmode == EM_ANY || lpmwd->editmode == EM_THINGS)
		DeselectAllThings(lpmwd);

	if(lpmwd->editmode == EM_ANY || lpmwd->editmode == EM_VERTICES)
		DeselectAllVertices(lpmwd);
}


/* DeselectAllBruteForce
 *   Deselects everything without trying to be clever. Useful for when the
 *   selection can be thrown out of sync, e.g. after an undo.
 *
 * Parameters:
 *   MAPWINDATA*	lpmwd	Pointer to map window data.
 *
 * Return value: None.
 */
static void DeselectAllBruteForce(MAPWINDATA *lpmwd)
{
	ClearSelectionList(lpmwd->selection.lpsellistSectors);
	ClearSelectionList(lpmwd->selection.lpsellistLinedefs);
	ClearSelectionList(lpmwd->selection.lpsellistThings);
	ClearSelectionList(lpmwd->selection.lpsellistVertices);

	ResetSelections(NULL, 0, NULL, 0, NULL, 0, lpmwd->lpmap->sectors, lpmwd->lpmap->iSectors);
	ResetSelections(NULL, 0, lpmwd->lpmap->linedefs, lpmwd->lpmap->iLinedefs, NULL, 0, NULL, 0);
	ResetSelections(lpmwd->lpmap->things, lpmwd->lpmap->iThings, NULL, 0, NULL, 0, NULL, 0);
	ResetSelections(NULL, 0, NULL, 0, lpmwd->lpmap->vertices, lpmwd->lpmap->iVertices, NULL, 0);
}


/* DeselectAllLines etc.
 *   Deselects all map objects of a specified type. Doesn't do any associated
 *   deselections, e.g. between lines and sectors.
 *
 * Parameters:
 *   MAPWINDATA*	lpmwd	Pointer to map window data.
 *
 * Return value: None.
 */
static void DeselectAllLines(MAPWINDATA *lpmwd)
{
	ClearSelectionList(lpmwd->selection.lpsellistSectors);
	ResetSelections(NULL, 0, NULL, 0, NULL, 0, lpmwd->lpmap->sectors, lpmwd->lpmap->iSectors);
}

static void DeselectAllSectors(MAPWINDATA *lpmwd)
{
	ClearSelectionList(lpmwd->selection.lpsellistLinedefs);
	ResetSelections(NULL, 0, lpmwd->lpmap->linedefs, lpmwd->lpmap->iLinedefs, NULL, 0, NULL, 0);
}

static void DeselectAllThings(MAPWINDATA *lpmwd)
{
	ClearSelectionList(lpmwd->selection.lpsellistThings);
	ResetSelections(lpmwd->lpmap->things, lpmwd->lpmap->iThings, NULL, 0, NULL, 0, NULL, 0);
}

static void DeselectAllVertices(MAPWINDATA *lpmwd)
{
	ClearSelectionList(lpmwd->selection.lpsellistVertices);
	ResetSelections(NULL, 0, NULL, 0, lpmwd->lpmap->vertices, lpmwd->lpmap->iVertices, NULL, 0);
}


/* DeselectLinesFromPartialSectors
 *   Deselects any lines that do not belong to a selected sector.
 *
 * Parameters:
 *   MAPWINDATA*	lpmwd	Pointer to map window data.
 *
 * Return value: None.
 */
static void DeselectLinesFromPartialSectors(MAPWINDATA *lpmwd)
{
	int i;

	for(i = 0; i < lpmwd->lpmap->iLinedefs; i++)
	{
		MAPLINEDEF *lpld = &lpmwd->lpmap->linedefs[i];
		
		if(lpld->selected)
		{
			int iFrontSec = lpld->s1 >= 0 ? lpmwd->lpmap->sidedefs[lpld->s1].sector : -1;	
			int iBackSec = lpld->s2 >= 0 ? lpmwd->lpmap->sidedefs[lpld->s2].sector : -1;

			if((iFrontSec < 0 || !lpmwd->lpmap->sectors[iFrontSec].selected) &&
				(iBackSec < 0 || !lpmwd->lpmap->sectors[iBackSec].selected)) 
			{
				RemoveFromSelectionList(lpmwd->selection.lpsellistLinedefs, i);
				lpld->selected = 0;
			}
		}
	}
}

/* UpdateMapWindowTitle
 *   Updates a map window's title to include path and lumpname.
 *
 * Parameters:
 *   MAPWINDATA*	lpmwd	Pointer to map window data.
 *
 * Return value: None.
 */
static void UpdateMapWindowTitle(MAPWINDATA *lpmwd)
{
	LPTSTR	szPath, szTitle;
	WORD	cb;
	TCHAR	szTail[12];		/* With lumpname corrected for ANSI/UNICODE-ness. */

	/* Allocate buffer for full path, and get it. */
	cb = GetWadPath(lpmwd->iWadID, NULL, 0);
	szPath = (LPTSTR)ProcHeapAlloc(cb * sizeof(TCHAR));
	GetWadPath(lpmwd->iWadID, szPath, cb);

	/* Allocate buffer for title (including tail), and get file title. */
	cb = GetFileTitle(szPath, NULL, 0);
	szTitle = (LPTSTR)ProcHeapAlloc((cb + strlen(lpmwd->szLumpName) + 3) * sizeof(TCHAR));
	GetFileTitle(szPath, szTitle, cb);
	ProcHeapFree(szPath);

	/* Get tail, including corrected lumpname. */
	wsprintf(szTail, TEXT(" - %hs"), lpmwd->szLumpName);
	lstrcat(szTitle, szTail);

	SetWindowText(lpmwd->hwnd, szTitle);

	ProcHeapFree(szTitle);
}


static void BuildSectorTexturePreviews(MAPWINDATA *lpmwd, SECTORDISPLAYINFO *lpsdi, DWORD dwDisplayFlags)
{
	/* Begin by assuming we need to destroy no textures. */
	lpsdi->dwDestroyTexFlags = 0;

	if(dwDisplayFlags & SDIF_CEILINGTEX)
	{
		if(GetTextureForMap(lpmwd->hwnd, lpsdi->szCeiling, &lpsdi->lptexCeiling, TF_FLAT))
			lpsdi->dwDestroyTexFlags |= SDIF_CEILINGTEX;
	}
	else
		lpsdi->lptexCeiling = NULL;

	if(dwDisplayFlags & SDIF_FLOORTEX)
	{
		if(GetTextureForMap(lpmwd->hwnd, lpsdi->szFloor, &lpsdi->lptexFloor, TF_FLAT))
			lpsdi->dwDestroyTexFlags |= SDIF_FLOORTEX;
	}
	else
		lpsdi->lptexFloor = NULL;
}


static void DestroySectorTexturePreviews(SECTORDISPLAYINFO *lpsdi, DWORD dwDisplayFlags)
{
	if(lpsdi->lptexCeiling && (dwDisplayFlags & SDIF_CEILINGTEX) && (lpsdi->dwDestroyTexFlags & SDIF_CEILINGTEX))
		DestroyTexture(lpsdi->lptexCeiling);

	if(lpsdi->lptexFloor && (dwDisplayFlags & SDIF_FLOORTEX) && (lpsdi->dwDestroyTexFlags & SDIF_FLOORTEX))
		DestroyTexture(lpsdi->lptexFloor);
}



static void BuildThingSpritePreview(MAPWINDATA *lpmwd, THINGDISPLAYINFO *lptdi, DWORD dwDisplayFlags)
{
	/* Begin by assuming we need to destroy no textures. */
	lptdi->dwDestroyTexFlags = 0;

	if(dwDisplayFlags & TDIF_SPRITE)
	{
		if(GetTextureForMap(lpmwd->hwnd, lptdi->szSprite, &lptdi->lptexSprite, TF_IMAGE))
			lptdi->dwDestroyTexFlags |= TDIF_SPRITE;
	}
	else
		lptdi->lptexSprite = NULL;
}


static void DestroyThingSpritePreview(THINGDISPLAYINFO *lptdi, DWORD dwDisplayFlags)
{
	if(lptdi->lptexSprite && (dwDisplayFlags & TDIF_SPRITE) && (lptdi->dwDestroyTexFlags & TDIF_SPRITE))
		DestroyTexture(lptdi->lptexSprite);
}



static void BuildSidedefTexturePreviews(MAPWINDATA *lpmwd, LINEDEFDISPLAYINFO *lplddi, DWORD dwDisplayFlags)
{
	/* Begin by assuming we need to destroy no textures. */
	lplddi->dwDestroyTexFlags = 0;

	/* Same for the psedo textures. */
	lplddi->dwPseudoTexFlags = 0;

	if(dwDisplayFlags & LDDIF_FRONTSD)
	{
		if(dwDisplayFlags & LDDIF_FRONTUPPER)
		{
			if(IsPseudoTexture(lplddi->szFrontUpper))
			{
				lplddi->lptexFrontUpper = NULL;
				lplddi->dwPseudoTexFlags |= LDDIF_FRONTUPPER;
			}
			else if(GetTextureForMap(lpmwd->hwnd, lplddi->szFrontUpper, &lplddi->lptexFrontUpper, TF_TEXTURE))
				lplddi->dwDestroyTexFlags |= LDDIF_FRONTUPPER;
		}
		else
			lplddi->lptexFrontUpper = NULL;

		if(dwDisplayFlags & LDDIF_FRONTMIDDLE)
		{
			if(IsPseudoTexture(lplddi->szFrontMiddle))
			{
				lplddi->lptexFrontMiddle = NULL;
				lplddi->dwPseudoTexFlags |= LDDIF_FRONTMIDDLE;
			}
			else if(GetTextureForMap(lpmwd->hwnd, lplddi->szFrontMiddle, &lplddi->lptexFrontMiddle, TF_TEXTURE))
				lplddi->dwDestroyTexFlags |= LDDIF_FRONTMIDDLE;
		}
		else
			lplddi->lptexFrontMiddle = NULL;

		if(dwDisplayFlags & LDDIF_FRONTLOWER)
		{
			if(IsPseudoTexture(lplddi->szFrontLower))
			{
				lplddi->lptexFrontLower = NULL;
				lplddi->dwPseudoTexFlags |= LDDIF_FRONTLOWER;
			}
			else if(GetTextureForMap(lpmwd->hwnd, lplddi->szFrontLower, &lplddi->lptexFrontLower, TF_TEXTURE))
				lplddi->dwDestroyTexFlags |= LDDIF_FRONTLOWER;
		}
		else
			lplddi->lptexFrontLower = NULL;
	}
	else lplddi->lptexFrontUpper = lplddi->lptexFrontMiddle = lplddi->lptexFrontLower = NULL;


	if(dwDisplayFlags & LDDIF_BACKSD)
	{
		if(dwDisplayFlags & LDDIF_BACKUPPER)
		{
			if(IsPseudoTexture(lplddi->szBackUpper))
			{
				lplddi->lptexBackUpper = NULL;
				lplddi->dwPseudoTexFlags |= LDDIF_BACKUPPER;
			}
			else if(GetTextureForMap(lpmwd->hwnd, lplddi->szBackUpper, &lplddi->lptexBackUpper, TF_TEXTURE))
				lplddi->dwDestroyTexFlags |= LDDIF_BACKUPPER;
		}
		else
			lplddi->lptexBackUpper = NULL;

		if(dwDisplayFlags & LDDIF_BACKMIDDLE)
		{
			if(IsPseudoTexture(lplddi->szBackMiddle))
			{
				lplddi->lptexBackMiddle = NULL;
				lplddi->dwPseudoTexFlags |= LDDIF_BACKMIDDLE;
			}
			else if(GetTextureForMap(lpmwd->hwnd, lplddi->szBackMiddle, &lplddi->lptexBackMiddle, TF_TEXTURE))
				lplddi->dwDestroyTexFlags |= LDDIF_BACKMIDDLE;
		}
		else
			lplddi->lptexBackMiddle = NULL;

		if(dwDisplayFlags & LDDIF_BACKLOWER)
		{
			if(IsPseudoTexture(lplddi->szBackLower))
			{
				lplddi->lptexBackLower = NULL;
				lplddi->dwPseudoTexFlags |= LDDIF_BACKLOWER;
			}
			else if(GetTextureForMap(lpmwd->hwnd, lplddi->szBackLower, &lplddi->lptexBackLower, TF_TEXTURE))
				lplddi->dwDestroyTexFlags |= LDDIF_BACKLOWER;
		}
		else
			lplddi->lptexBackLower = NULL;
	}
	else lplddi->lptexBackUpper = lplddi->lptexBackMiddle = lplddi->lptexBackLower = NULL;
}


static void DestroySidedefTexturePreviews(LINEDEFDISPLAYINFO *lplddi, DWORD dwDisplayFlags)
{
	if(lplddi->lptexFrontUpper && (dwDisplayFlags & LDDIF_FRONTUPPER) && (lplddi->dwDestroyTexFlags & LDDIF_FRONTUPPER))
		DestroyTexture(lplddi->lptexFrontUpper);

	if(lplddi->lptexFrontMiddle && (dwDisplayFlags & LDDIF_FRONTMIDDLE) && (lplddi->dwDestroyTexFlags & LDDIF_FRONTMIDDLE))
		DestroyTexture(lplddi->lptexFrontMiddle);

	if(lplddi->lptexFrontLower && (dwDisplayFlags & LDDIF_FRONTLOWER) && (lplddi->dwDestroyTexFlags & LDDIF_FRONTLOWER))
		DestroyTexture(lplddi->lptexFrontLower);


	if(lplddi->lptexBackUpper && (dwDisplayFlags & LDDIF_BACKUPPER) && (lplddi->dwDestroyTexFlags & LDDIF_BACKUPPER))
		DestroyTexture(lplddi->lptexBackUpper);

	if(lplddi->lptexBackMiddle && (dwDisplayFlags & LDDIF_BACKMIDDLE) && (lplddi->dwDestroyTexFlags & LDDIF_BACKMIDDLE))
		DestroyTexture(lplddi->lptexBackMiddle);

	if(lplddi->lptexBackLower && (dwDisplayFlags & LDDIF_BACKLOWER) && (lplddi->dwDestroyTexFlags & LDDIF_BACKLOWER))
		DestroyTexture(lplddi->lptexBackLower);
}





/* GetTextureForMap
 *   Loads a texture from cache if possible, and then from a wad if not.
 *
 * Parameters:
 *   MAPWINDATA*	lpmwd		Map window data.
 *   LPTSTR			szTexName	Lumpname of texture.
 *   TEXTURE**		lplptex		Buffer in which to return texture. NULL if not
 *								found.
 *   TEX_FORMAT		tf			Specifies whether texture or flat.
 *
 * Return value: BOOL
 *   TRUE if memory was allocated for the texture; FALSE if not.
 *
 * Remarks:
 *   The caller must call DestroyTexture if we return TRUE.
 */
BOOL GetTextureForMap(HWND hwnd, LPCTSTR szTexName, TEXTURE **lplptex, TEX_FORMAT tf)
{
	MAPWINDATA *lpmwd = (MAPWINDATA*)GetWindowLong(hwnd, GWL_USERDATA);
	TEXCACHE *lptcHdr;

	/* Search the appropriate cache first. The WaitForSingleObject call
	 * determines whether we can write to the cache.
	 */
	switch(tf)
	{
	case TF_FLAT:
		lptcHdr = &lpmwd->tcFlatsHdr;
		*lplptex = GetTextureFromCache(lptcHdr, szTexName);
		break;
	case TF_TEXTURE:
		lptcHdr = &lpmwd->tcTexturesHdr;
		*lplptex = GetTextureFromCache(lptcHdr, szTexName);
		break;
	default:
		/* We don't cache TF_IMAGEs. */
		lptcHdr = NULL;
		break;
	}

	if(lptcHdr)
		*lplptex = GetTextureFromCache(lptcHdr, szTexName);
	else
		*lplptex = NULL;

	/* Found in cache? */
	if(*lplptex)
	{
		/* We're finished, and we didn't allocate any memory. */
		return FALSE;
	}

	/* Fall back to wad-files, and add to the cache once found. */
	if(lpmwd->iAdditionalWadID >= 0)
	{
		*lplptex = LoadTexture(GetWad(lpmwd->iAdditionalWadID), szTexName, tf, lpmwd->rgbqPalette);
		if(lptcHdr && *lplptex) AddToTextureCache(lptcHdr, szTexName, *lplptex);
	}

	if(!(*lplptex) && lpmwd->iIWadID >= 0)
	{
		*lplptex = LoadTexture(lpmwd->lpwadIWad, szTexName, tf, lpmwd->rgbqPalette);
		if(lptcHdr && *lplptex) AddToTextureCache(lptcHdr, szTexName, *lplptex);
	}

	/* If the texture is non-null and an image, the caller needs to free it. */
	return *lplptex && tf == TF_IMAGE;
}



/* IsPseudoTexture
 *   Determines whether a texture that does not actually exist should
 *   nonetheless not be reported as missing.
 *
 * Parameters:
 *   LPCTSTR		szTexName	Name of texture.
 *
 * Return value: BOOL
 *   TRUE if pseudo-texture; FALSE if not.
 *
 * Remarks:
 *   Useful for "-" and things like F_SKY1 and colourmaps.
 */

#ifdef _UNICODE
static BOOL IsPseudoTextureW(LPCWSTR szTexName)
{
	char szTexNameA[9];
	wsprintfA(szTexNameA, "%ls", szTexName);
	return IsPseudoTextureA(szTexNameA);
}
#endif

static BOOL IsPseudoTextureA(LPCSTR szTexName)
{
	if(lstrcmpA(szTexName, "-") == 0) return TRUE;

	/* TODO: Check for pseudo-textures from config. */

	return FALSE;
}



/* MapWinUpdateInfoBar
 *   Updates the info-bar to reflect selection or highlight.
 *
 * Parameters:
 *   MAPWINDATA*	lpmwd	Map window data.
 *
 * Return value: None.
 */
static void MapWinUpdateInfoBar(MAPWINDATA *lpmwd)
{
	/* Set the palette if necessary. */
	if(TexPreviewPaletteSetWindow() != lpmwd->hwnd)
	{
		InitAllTexPreviews(lpmwd->hwnd, lpmwd->rgbqPalette);
	}

	switch(lpmwd->highlightobj.emType)
	{
	case EM_LINES:
		{
			LINEDEFDISPLAYINFO	lddi;
			CONFIG *lpcfgLinedefs = ConfigGetSubsection(lpmwd->lpcfgMap, "__ldtypesflat");

			ResetFlatPreviews();
			ResetSpritePreview();

			GetLinedefDisplayInfo(lpmwd->lpmap, lpcfgLinedefs, lpmwd->highlightobj.iIndex, &lddi);
			BuildSidedefTexturePreviews(lpmwd, &lddi, LDDIF_ALL);
			ShowLinesInfo(&lddi, LDDIF_ALL);
			DestroySidedefTexturePreviews(&lddi, LDDIF_ALL);
		}

		break;

	case EM_SECTORS:
		{
			SECTORDISPLAYINFO	sdi;
			CONFIG *lpcfgSectors = ConfigGetSubsection(lpmwd->lpcfgMap, "sectortypes");

			ResetSidedefPreviews();
			ResetSpritePreview();

			GetSectorDisplayInfo(lpmwd->lpmap, lpcfgSectors, lpmwd->highlightobj.iIndex, &sdi);
			BuildSectorTexturePreviews(lpmwd, &sdi, SDIF_ALL);
			ShowSectorInfo(&sdi, SDIF_ALL);
			DestroySectorTexturePreviews(&sdi, SDIF_ALL);
		}

		break;

	case EM_THINGS:
		{
			THINGDISPLAYINFO	tdi;
			CONFIG *lpcfgThings = ConfigGetSubsection(lpmwd->lpcfgMap, FLAT_THING_SECTION);

			ResetSidedefPreviews();
			ResetFlatPreviews();

			GetThingDisplayInfo(lpmwd->lpmap, lpcfgThings, lpmwd->highlightobj.iIndex, &tdi);
			BuildThingSpritePreview(lpmwd, &tdi, TDIF_ALL);
			ShowThingInfo(&tdi, TDIF_ALL);
			DestroyThingSpritePreview(&tdi, TDIF_ALL);
		}

		break;

	case EM_VERTICES:
		{
			VERTEXDISPLAYINFO	vdi;

			ResetSidedefPreviews();
			ResetFlatPreviews();
			ResetSpritePreview();

			GetVertexDisplayInfo(lpmwd->lpmap, lpmwd->highlightobj.iIndex, &vdi);
			ShowVertexInfo(&vdi, VDIF_ALL);
		}

		break;

	/* Not pointing at anything. */
	case EM_MOVE:
		{
			switch(lpmwd->editmode)
			{
			case EM_ANY:
				
				/* Begin by clearing all previews. Doesn't flicker, since we
				 * only get here when the info-bar *needs* updating.
				 */
				ResetSidedefPreviews();
				ResetFlatPreviews();
				ResetSpritePreview();

				if(lpmwd->selection.lpsellistLinedefs->iDataCount > 0) ShowLinesSelectionInfo(lpmwd);
				if(lpmwd->selection.lpsellistSectors->iDataCount > 0) ShowSectorSelectionInfo(lpmwd);
				if(lpmwd->selection.lpsellistThings->iDataCount > 0) ShowThingsSelectionInfo(lpmwd);
				if(lpmwd->selection.lpsellistVertices->iDataCount > 0) ShowVerticesSelectionInfo(lpmwd);

				break;

			case EM_LINES:
				if(lpmwd->selection.lpsellistLinedefs->iDataCount > 0) ShowLinesSelectionInfo(lpmwd);
				else InfoBarShowPanels(IBPF_ALL, FALSE);

				break;

			case EM_SECTORS:
				if(lpmwd->selection.lpsellistSectors->iDataCount > 0) ShowSectorSelectionInfo(lpmwd);
				else InfoBarShowPanels(IBPF_ALL, FALSE);

				break;

			case EM_THINGS:
				if(lpmwd->selection.lpsellistThings->iDataCount > 0) ShowThingsSelectionInfo(lpmwd);
				else InfoBarShowPanels(IBPF_ALL, FALSE);

				break;

			case EM_VERTICES:
				if(lpmwd->selection.lpsellistVertices->iDataCount > 0) ShowVerticesSelectionInfo(lpmwd);
				else InfoBarShowPanels(IBPF_ALL, FALSE);

				break;

			default:
				
				InfoBarShowPanels(IBPF_ALL, FALSE);
			}
		}
		
		break;
		
	default:
		break;
	}
}


/* ShowsLinesSelectionInfo, ShowsSectorSelectionInfo, ShowsThingSelectionInfo,
 * ShowsVertexSelectionInfo
 *   Updates the info-bar to reflect selections.
 *
 * Parameters:
 *   MAPWINDATA*	lpmwd	Map window data.
 *
 * Return value: None.
 */
static void ShowLinesSelectionInfo(MAPWINDATA *lpmwd)
{
	LINEDEFDISPLAYINFO lddi;
	DWORD dwDisplayFlags;

	dwDisplayFlags = CheckLines(lpmwd->lpmap, lpmwd->selection.lpsellistLinedefs, ConfigGetSubsection(lpmwd->lpcfgMap, "__ldtypesflat"), &lddi);

	BuildSidedefTexturePreviews(lpmwd, &lddi, dwDisplayFlags);
	ShowLinesInfo(&lddi, dwDisplayFlags);
	DestroySidedefTexturePreviews(&lddi, dwDisplayFlags);
}

static void ShowSectorSelectionInfo(MAPWINDATA *lpmwd)
{
	SECTORDISPLAYINFO sdi;
	DWORD dwDisplayFlags;

	dwDisplayFlags = CheckSectors(lpmwd->lpmap, lpmwd->selection.lpsellistSectors, ConfigGetSubsection(lpmwd->lpcfgMap, "sectortypes"), &sdi);

	BuildSectorTexturePreviews(lpmwd, &sdi, dwDisplayFlags);
	ShowSectorInfo(&sdi, dwDisplayFlags);
	DestroySectorTexturePreviews(&sdi, dwDisplayFlags);
}

static void ShowThingsSelectionInfo(MAPWINDATA *lpmwd)
{
	THINGDISPLAYINFO tdi;
	DWORD dwDisplayFlags;

	dwDisplayFlags = CheckThings(lpmwd->lpmap, lpmwd->selection.lpsellistThings, ConfigGetSubsection(lpmwd->lpcfgMap, FLAT_THING_SECTION), &tdi);

	BuildThingSpritePreview(lpmwd, &tdi, dwDisplayFlags);
	ShowThingInfo(&tdi, dwDisplayFlags);
	DestroyThingSpritePreview(&tdi, dwDisplayFlags);
}

static void ShowVerticesSelectionInfo(MAPWINDATA *lpmwd)
{
	VERTEXDISPLAYINFO vdi;
	DWORD dwDisplayFlags;

	dwDisplayFlags = CheckVertices(lpmwd->lpmap, lpmwd->selection.lpsellistVertices, &vdi);

	ShowVertexInfo(&vdi, dwDisplayFlags);
}


/* ShowsSelectionProperties
 *   Updates the info-bar to reflect selections.
 *
 * Parameters:
 *   MAPWINDATA*	lpmwd	Map window data.
 *
 * Return value: None.
 *
 * Remarks:
 *   The caller should ensure that something is in fact selected.
 */
static void ShowSelectionProperties(MAPWINDATA *lpmwd)
{
	DWORD dwFlags = 0;
	DWORD dwStartPage = 0;
	MAPPPDATA mapppdata;
	TCHAR szFromTable[64], szCaption[64];
	UINT uiStringID = 0;
	int iObject;
	

	/* Build the flags to determine which property pages to show, and which
	 * one to start on.
	 */

	/* Assume we don't need to show obj num in caption. */
	iObject = -1;

	switch(lpmwd->editmode)
	{
	case EM_SECTORS:
		dwFlags = MPPF_SECTOR | MPPF_LINE;
		dwStartPage = MPPF_SECTOR;

		if(lpmwd->selection.lpsellistSectors->iDataCount == 1)
		{
			uiStringID = IDS_INFOBAR_SCAP;
			iObject = lpmwd->selection.lpsellistSectors->lpiIndices[0];
		}
		else
		{
			uiStringID = IDS_MAPOBJECT;
		}

		break;

	case EM_LINES:
		dwFlags = MPPF_LINE;
		dwStartPage = MPPF_LINE;

		/* If we've got any sectors selected, show that page, too. */
		if(lpmwd->selection.lpsellistSectors->iDataCount > 0) dwFlags |= MPPF_SECTOR;

		if(lpmwd->selection.lpsellistLinedefs->iDataCount == 1)
		{
			uiStringID = IDS_INFOBAR_LDCAP;
			iObject = lpmwd->selection.lpsellistLinedefs->lpiIndices[0];
		}
		else
		{
			uiStringID = IDS_MAPOBJECT;
		}

		break;

	case EM_THINGS:
		dwFlags = MPPF_THING;
		dwStartPage = MPPF_THING;

		if(lpmwd->selection.lpsellistThings->iDataCount == 1)
		{
			uiStringID = IDS_INFOBAR_TCAP;
			iObject = lpmwd->selection.lpsellistThings->lpiIndices[0];
		}
		else
		{
			uiStringID = IDS_MAPOBJECT;
		}

		break;

	case EM_VERTICES:
		/* The caller should make sure only one vertex is selected. */
		dwFlags = MPPF_VERTEX;
		dwStartPage = MPPF_VERTEX;
		uiStringID = IDS_INFOBAR_SCAP;
		iObject = lpmwd->selection.lpsellistSectors->lpiIndices[0];

		break;

	case EM_ANY:
		/* This is slightly more involved. We have to check what sort of things
		 * we have selected.
		 */
		if(lpmwd->selection.lpsellistLinedefs->iDataCount > 0)	dwFlags |= MPPF_LINE;
		if(lpmwd->selection.lpsellistSectors->iDataCount > 0)	dwFlags |= MPPF_SECTOR;
		if(lpmwd->selection.lpsellistThings->iDataCount > 0)	dwFlags |= MPPF_THING;

		/* Setting the properties of multiple vertices makes no sense at all. */
		if(lpmwd->selection.lpsellistVertices->iDataCount == 1)	dwFlags |= MPPF_VERTEX;

		/* We determine the start page by the type of the object that was
		 * clicked. TODO: Does this work with context menus?
		 */
		switch(lpmwd->hobjDrag.emType)
		{
			case EM_SECTORS:	dwStartPage = MPPF_SECTOR;	break;
			case EM_LINES:		dwStartPage = MPPF_LINE;	break;
			case EM_VERTICES:	dwStartPage = MPPF_VERTEX;	break;
			case EM_THINGS:		dwStartPage = MPPF_THING;	break;
			default:			dwStartPage = (DWORD)-1;	/* Don't care. */
		}

		uiStringID = IDS_MAPOBJECT;

		break;
		
	default:
		break;
	}

	/* Caption. Fill in using determined parameters. */
	LoadString(g_hInstance, uiStringID, szFromTable, sizeof(szFromTable) / sizeof(TCHAR));
	if(iObject >= 0)
		_sntprintf(szCaption, sizeof(szCaption) / sizeof(TCHAR) - 1, szFromTable, iObject);
	else
		lstrcpyn(szCaption, szFromTable, sizeof(szCaption) / sizeof(TCHAR));

	/* Terminate string in the event that _sntprintf filled it entirely. */
	szCaption[sizeof(szCaption) / sizeof(TCHAR) - 1] = '\0';

	mapppdata.hwndMap = lpmwd->hwnd;
	mapppdata.lpmap = lpmwd->lpmap;
	mapppdata.lpselection = &lpmwd->selection;
	mapppdata.lpcfgMap = lpmwd->lpcfgMap;
	mapppdata.lptnlFlats = lpmwd->lptnlFlats;
	mapppdata.lptnlTextures = lpmwd->lptnlTextures;
	
	/* Make an undo. */
	CreateUndo(lpmwd, IDS_UNDO_PROPERTIES);
	
	/* Show the dialogue. If the user cancelled, withdraw the undo. */
	if(!ShowMapObjectProperties(dwFlags, dwStartPage, szCaption, &mapppdata))
		WithdrawUndo(lpmwd);
	else
	{
		ClearRedoStack(lpmwd);
		lpmwd->bMapChanged = TRUE;
	}
}



/* DoInsertOperation
 *   Inserts an object.
 *
 * Parameters:
 *   MAPWINDATA*	lpmwd		Map window data.
 *   short			xMap, yMap	Map co-ordinates for insertion.
 *
 * Return value: None.
 *
 * Remarks:
 *   This may be done in response to either a left- or right-button release.
 */
static void DoInsertOperation(MAPWINDATA *lpmwd, short xMap, short yMap)
{
	switch(lpmwd->editmode)
	{
	case EM_ANY:
	case EM_LINES:
	case EM_SECTORS:
	case EM_VERTICES:

		/* User may cancel, so don't clear redo. */
		CreateUndo(lpmwd, (lpmwd->editmode == EM_VERTICES) ? IDS_UNDO_VERTEXINSERT : IDS_UNDO_LINEDEFDRAW);

		/* Draw the first vertex. */
		DrawToNewVertex(lpmwd->lpmap, &lpmwd->drawop, xMap, yMap);

		/* Switch to drawing submode (this is overriden in vertex mode). */
		lpmwd->submode = ESM_DRAWING;

		/* If we're in vertex mode, that was also the last. */
		if(lpmwd->editmode == EM_VERTICES)
		{
			/* Drawing this vertex finishes the drawing operation. */
			EndDrawOperation(lpmwd->lpmap, &lpmwd->drawop);
			lpmwd->submode = ESM_NONE;

			ClearRedoStack(lpmwd);
			lpmwd->bMapChanged = TRUE;
		}

		break;
		
	default:
		break;
	}
}


/* EndDragOperation
 *   Called when the user releases the mouse when dragging something. Sorts
 *   the map out, cleans up and restores normal behaviour.
 *
 * Parameters:
 *   MAPWINDATA*	lpmwd		Map window data.
 *
 * Return value: None.
 */
static void EndDragOperation(MAPWINDATA *lpmwd)
{
	/* TODO: Release mouse? */
	/* TODO: Add vertices for crossing lines; stitching. */

	/* Any lines inside any of our loops in their final positions also need to
	 * be marked RS.
	 */
	LabelLoopedLinesRS(lpmwd->lpmap, lpmwd->lplooplistRS);

	/* Update the loop list and flags to include the newly-RS-marked lines. */
	DestroyLoopList(lpmwd->lplooplistRS);
	lpmwd->lplooplistRS = LabelRSLinesLoops(lpmwd->lpmap);

	/* Label whether each side should keep its sector reference. */
	LabelRSLinesEnclosure(lpmwd->lpmap, lpmwd->lplooplistRS);

	/* Set the sector references on sides that weren't enclosed. */
	CorrectDraggedSectorReferences(lpmwd->lpmap);

	/* Clear the dragging flags. */
	ClearDraggingFlags(lpmwd->lpmap);

	/* Were the objects selected before we clicked on them? If not, deselect
	 * them.
	 */
	if(lpmwd->bDeselectAfterEdit) DeselectAll(lpmwd);

	/* Free memory. */
	DestroyLoopList(lpmwd->lplooplistRS);
	CleanAuxiliarySelection(lpmwd);

	/* Restore normal behaviour. */
	lpmwd->submode = ESM_NONE;
	lpmwd->bWantRedraw = TRUE;

	/* Too late to cancel now! */
	ClearRedoStack(lpmwd);
	lpmwd->bMapChanged = TRUE;
}


/* RebuildSelection
 *   Recreates the selection lists for a map window based on the selection flags
 *   in the map objects.
 *
 * Parameters:
 *   MAPWINDATA*	lpmwd		Map window data.
 *
 * Return value: None.
 *
 * Remarks:
 *   Useful following a map editing operation that changes indices.
 */
static void RebuildSelection(MAPWINDATA *lpmwd)
{
	int i;

	SELECTION_LIST *lpsellistLinedefs = lpmwd->selection.lpsellistLinedefs;
	SELECTION_LIST *lpsellistSectors = lpmwd->selection.lpsellistSectors;
	SELECTION_LIST *lpsellistThings = lpmwd->selection.lpsellistThings;
	SELECTION_LIST *lpsellistVertices = lpmwd->selection.lpsellistVertices;

	/* Begin by clearing everything. */
	ClearSelectionList(lpsellistLinedefs);
	ClearSelectionList(lpsellistSectors);
	ClearSelectionList(lpsellistThings);
	ClearSelectionList(lpsellistVertices);

	/* Reselect each type in turn. */

	for(i = 0; i < lpmwd->lpmap->iLinedefs; i++)
		if(lpmwd->lpmap->linedefs[i].selected)
			AddToSelectionList(lpsellistLinedefs, i);

	for(i = 0; i < lpmwd->lpmap->iSectors; i++)
		if(lpmwd->lpmap->sectors[i].selected)
			AddToSelectionList(lpsellistSectors, i);

	for(i = 0; i < lpmwd->lpmap->iThings; i++)
		if(lpmwd->lpmap->things[i].selected)
			AddToSelectionList(lpsellistThings, i);

	for(i = 0; i < lpmwd->lpmap->iVertices; i++)
		if(lpmwd->lpmap->vertices[i].selected)
			AddToSelectionList(lpsellistVertices, i);
}


/* CreateUndo
 *   Saves an undo snapshot.
 *
 * Parameters:
 *   MAPWINDATA*	lpmwd			Map window data.
 *   int			iStringIndex	ID of resource string describing the action.
 *
 * Return value: None.
 *
 * Remarks:
 *   The redo stack is not cleared, since the app may want to withdraw the undo
 *   if the user cancels, in which case the redo stack must be left intact.
 */
static void CreateUndo(MAPWINDATA *lpmwd, int iStringIndex)
{
	/* Create the new undo frame. If we don't already have a stack, make one. */
	if(lpmwd->lpundostackUndo)
		PushNewUndoFrame(lpmwd->lpundostackUndo, lpmwd->lpmap, iStringIndex);
	else
		lpmwd->lpundostackUndo = CreateUndoStack(lpmwd->lpmap, iStringIndex);
}


/* ClearRedoStack
 *   Clears the redo stack.
 *
 * Parameters:
 *   MAPWINDATA*	lpmwd			Map window data.
 *
 * Return value: None.
 *
 * Remarks:
 *   Call this once you're sure you want to leave that undo you just created.
 */
static void ClearRedoStack(MAPWINDATA *lpmwd)
{
	/* Clear the redo stack. */
	if(lpmwd->lpundostackRedo)
	{
		FreeUndoStack(lpmwd->lpundostackRedo);
		lpmwd->lpundostackRedo = NULL;
	}
}


/* PerformUndo
 *   Undoes the last action for an open map.
 *
 * Parameters:
 *   MAPWINDATA*	lpmwd			Map window data.
 *
 * Return value: None.
 *
 * Remarks:
 *   The item is removed from the undo stack once it's undone. The redo stack is
 *   modified accordingly.
 */
static void PerformUndo(MAPWINDATA *lpmwd)
{
	int iUndoStringIndex = GetUndoTopStringIndex(lpmwd->lpundostackUndo);

	/* Create a new redo frame, making a new stack if necessary. */
	if(lpmwd->lpundostackRedo)
		PushNewUndoFrame(lpmwd->lpundostackRedo, lpmwd->lpmap, iUndoStringIndex);
	else
		lpmwd->lpundostackRedo = CreateUndoStack(lpmwd->lpmap, iUndoStringIndex);

	PerformTopUndo(lpmwd->lpundostackUndo, lpmwd->lpmap);
	WithdrawUndo(lpmwd);
}


/* PerformRedo
 *   Redoes the last action for an open map.
 *
 * Parameters:
 *   MAPWINDATA*	lpmwd			Map window data.
 *
 * Return value: None.
 *
 * Remarks:
 *   The item is removed from the redo stack once it's redone. The undo stack is
 *   modified accordingly. This is a copy-paste hack from PerformUndo.
 */
static void PerformRedo(MAPWINDATA *lpmwd)
{
	int iUndoStringIndex = GetUndoTopStringIndex(lpmwd->lpundostackRedo);

	/* Create a new undo frame, making a new stack if necessary. */
	if(lpmwd->lpundostackUndo)
		PushNewUndoFrame(lpmwd->lpundostackUndo, lpmwd->lpmap, iUndoStringIndex);
	else
		lpmwd->lpundostackUndo = CreateUndoStack(lpmwd->lpmap, iUndoStringIndex);

	PerformTopUndo(lpmwd->lpundostackRedo, lpmwd->lpmap);
	WithdrawRedo(lpmwd);
}


/* WithdrawUndo
 *   Removes an item from the undo stack for a window.
 *
 * Parameters:
 *   MAPWINDATA*	lpmwd			Map window data.
 *
 * Return value: None.
 */
static void WithdrawUndo(MAPWINDATA *lpmwd)
{
	if(UndoStackHasOnlyOneFrame(lpmwd->lpundostackUndo))
	{
		FreeUndoStack(lpmwd->lpundostackUndo);
		lpmwd->lpundostackUndo = NULL;
	}
	else
	{
		PopUndoFrame(lpmwd->lpundostackUndo);
	}
}


/* WithdrawRedo
 *   Removes an item from the redo stack for a window.
 *
 * Parameters:
 *   MAPWINDATA*	lpmwd			Map window data.
 *
 * Return value: None.
 */
static void WithdrawRedo(MAPWINDATA *lpmwd)
{
	if(UndoStackHasOnlyOneFrame(lpmwd->lpundostackRedo))
	{
		FreeUndoStack(lpmwd->lpundostackRedo);
		lpmwd->lpundostackRedo = NULL;
	}
	else
	{
		PopUndoFrame(lpmwd->lpundostackRedo);
	}
}


/* FlipSelectionHorizontally
 *   Flips the aux. selection horizontally in the intuitive manner.
 *
 * Parameters:
 *   MAPWINDATA*	lpmwd			Map window data.
 *
 * Return value: None.
 *
 * Remarks:
 *   The auxiliary selection must be set before calling this.
 */
static void FlipSelectionHorizontally(MAPWINDATA *lpmwd)
{
	int xMin = 32767, xMax = -32768;
	int i;
	short xAxis;

	/* Firstly, calculate the boundaries. */
	for(i = 0; i < lpmwd->selectionAux.lpsellistVertices->iDataCount; i++)
	{
		xMin = min(xMin, lpmwd->lpmap->vertices[lpmwd->selectionAux.lpsellistVertices->lpiIndices[i]].x);
		xMax = max(xMax, lpmwd->lpmap->vertices[lpmwd->selectionAux.lpsellistVertices->lpiIndices[i]].x);
	}

	for(i = 0; i < lpmwd->selectionAux.lpsellistThings->iDataCount; i++)
	{
		xMin = min(xMin, lpmwd->lpmap->things[lpmwd->selectionAux.lpsellistThings->lpiIndices[i]].x);
		xMax = max(xMax, lpmwd->lpmap->things[lpmwd->selectionAux.lpsellistThings->lpiIndices[i]].x);
	}

	/* Now, find an axis that bisects the region. */
	xAxis = (xMax + xMin) / 2;

	/* Now, flip vertices and things about the axis. */
	for(i = 0; i < lpmwd->selectionAux.lpsellistVertices->iDataCount; i++)
		FlipVertexAboutVerticalAxis(lpmwd->lpmap, lpmwd->selectionAux.lpsellistVertices->lpiIndices[i], xAxis);

	for(i = 0; i < lpmwd->selectionAux.lpsellistThings->iDataCount; i++)
		FlipThingAboutVerticalAxis(lpmwd->lpmap, lpmwd->selectionAux.lpsellistThings->lpiIndices[i], xAxis);

	/* Flip selected lines. */
	FlipSelectedLinedefs(lpmwd->lpmap, lpmwd->selection.lpsellistLinedefs);
	ExchangeSelectedSidedefs(lpmwd->lpmap, lpmwd->selection.lpsellistLinedefs);
}


/* FlipSelectionVertically
 *   Flips the aux. selection vertically in the intuitive manner.
 *
 * Parameters:
 *   MAPWINDATA*	lpmwd			Map window data.
 *
 * Return value: None.
 *
 * Remarks:
 *   The auxiliary selection must be set before calling this.
 */
static void FlipSelectionVertically(MAPWINDATA *lpmwd)
{
	int yMin = 32767, yMax = -32768;
	int i;
	short yAxis;

	/* Firstly, calculate the boundaries. */
	for(i = 0; i < lpmwd->selectionAux.lpsellistVertices->iDataCount; i++)
	{
		yMin = min(yMin, lpmwd->lpmap->vertices[lpmwd->selectionAux.lpsellistVertices->lpiIndices[i]].y);
		yMax = max(yMax, lpmwd->lpmap->vertices[lpmwd->selectionAux.lpsellistVertices->lpiIndices[i]].y);
	}

	for(i = 0; i < lpmwd->selectionAux.lpsellistThings->iDataCount; i++)
	{
		yMin = min(yMin, lpmwd->lpmap->things[lpmwd->selectionAux.lpsellistThings->lpiIndices[i]].y);
		yMax = max(yMax, lpmwd->lpmap->things[lpmwd->selectionAux.lpsellistThings->lpiIndices[i]].y);
	}

	/* Now, find an axis that bisects the region. */
	yAxis = (yMax + yMin) / 2;

	/* Now, flip vertices and things about the axis. */
	for(i = 0; i < lpmwd->selectionAux.lpsellistVertices->iDataCount; i++)
		FlipVertexAboutHorizontalAxis(lpmwd->lpmap, lpmwd->selectionAux.lpsellistVertices->lpiIndices[i], yAxis);

	for(i = 0; i < lpmwd->selectionAux.lpsellistThings->iDataCount; i++)
		FlipThingAboutHorizontalAxis(lpmwd->lpmap, lpmwd->selectionAux.lpsellistThings->lpiIndices[i], yAxis);

	/* Flip selected lines. */
	FlipSelectedLinedefs(lpmwd->lpmap, lpmwd->selection.lpsellistLinedefs);
	ExchangeSelectedSidedefs(lpmwd->lpmap, lpmwd->selection.lpsellistLinedefs);
}


/* CreateAuxiliarySelection
 *   Creates an auxiliary selection structure of vertices and things from the
 *   real selection.
 *
 * Parameters:
 *   MAPWINDATA*	lpmwd			Map window data.
 *
 * Return value: None.
 *
 * Remarks:
 *   Call CleanAuxiliarySelection to free memory one you're finished.
 */
static void CreateAuxiliarySelection(MAPWINDATA *lpmwd)
{
	int i;

	/* Build our dragging selection list. */
	lpmwd->selectionAux.lpsellistVertices = AllocateSelectionList(INIT_SELLIST_SIZE);
	lpmwd->selectionAux.lpsellistThings = AllocateSelectionList(INIT_SELLIST_SIZE);

	for(i = 0; i < lpmwd->selection.lpsellistThings->iDataCount; i++)
		AddToSelectionList(lpmwd->selectionAux.lpsellistThings, lpmwd->selection.lpsellistThings->lpiIndices[i]);

	/* Add the vertices that are already selected first. */
	for(i = 0; i < lpmwd->selection.lpsellistVertices->iDataCount; i++)
		AddToSelectionList(lpmwd->selectionAux.lpsellistVertices, lpmwd->selection.lpsellistVertices->lpiIndices[i]);
	/* Now add vertices for selected linedefs. This also covers the
	 * sector case, since they force selection of linedefs.
	 */
	for(i = 0; i < lpmwd->selection.lpsellistLinedefs->iDataCount; i++)
	{
		MAPLINEDEF *lpld = &lpmwd->lpmap->linedefs[lpmwd->selection.lpsellistLinedefs->lpiIndices[i]];

		/* AddToSelectionList eats duplicates, so this is okay. */
		AddToSelectionList(lpmwd->selectionAux.lpsellistVertices, lpld->v1);
		AddToSelectionList(lpmwd->selectionAux.lpsellistVertices, lpld->v2);
	}
}


/* CreateAuxiliarySelection
 *   Frees an aux. selection.
 *
 * Parameters:
 *   MAPWINDATA*	lpmwd			Map window data.
 *
 * Return value: None.
 */
static void CleanAuxiliarySelection(MAPWINDATA *lpmwd)
{
	DestroySelectionList(lpmwd->selectionAux.lpsellistThings);
	DestroySelectionList(lpmwd->selectionAux.lpsellistVertices);
}
