#include <windows.h>

#include "../general.h"
#include "../../res/resource.h"

#include "wadlist.h"
#include "mdiframe.h"
#include "gendlg.h"


#include "../../DockWnd/DockWnd.h"


static HWND g_hwndWadlist;


static BOOL CALLBACK WadListProc(HWND hwndDlg, UINT uiMsg, WPARAM wParam, LPARAM lParam);


/* CreateWadListWindow
 *   Creates the wad-list tool window.
 *
 * Parameters:
 *   HWND	hwndParent	Parent window (i.e. MDI frame window).
 *
 * Return value: BOOL
 *   TRUE on success; FALSE on error.
 *
 * Remarks:
 *   We have to pass the frame window handle since this is called before the
 *   global var is set.
 */
BOOL CreateWadListWindow(HWND hwndParent)
{
	DOCKINFO *lpdi;
	TCHAR	szCaption[64];

	/* Allocate a docking structure. This'll be freed automatically by the lib.
	 */
	lpdi = DockingAlloc((char)DWS_DOCKED_LEFT);

	/* Destroy the dialogue when the docking container is destroyed. Also, hide
	 * the docking container if the user clicks the Close button, rather than
	 * destroying the window.
	 */
	lpdi->dwStyle |= DWS_NODESTROY | DWS_DESTROYFOCUSWIN;

	/* This window'll be destroyed by the lib, too. */
	LoadString(g_hInstance, IDS_WADLIST_CAPTION, szCaption, sizeof(szCaption) / sizeof(TCHAR));
	DockingCreateFrame(lpdi, hwndParent, szCaption);

	/* Now that we've got a frame, we can create the window that will appear
	 * inside it.
	 */
	g_hwndWadlist = lpdi->hwnd;
	lpdi->focusWindow = CreateDialog(g_hInstance, MAKEINTRESOURCE(IDD_WADLIST), lpdi->hwnd, WadListProc);

	/* Subclass the dialogue so we can process non-client hit-test messages. */
	SetWindowLong(lpdi->focusWindow, GWL_WNDPROC, (LONG)TransDlgProc);

	DockingShowFrame(lpdi);

	return TRUE;
}


/* WadListProc
 *   Dialogue procedure for the wad-list dialogue.
 *
 * Parameters:
 *   HWND	hwndDlg		Handle to dialogue window.
 *   HWND	uiMsg		Window message ID.
 *   WPARAM	wParam		Message-specific.
 *   LPARAM lParam		Message-specific.
 *
 * Return value: BOOL
 *   TRUE if message was processed; FALSE otherwise.
 *
 * Remarks:
 *   This is the procedure for the window *inside* the docking container.
 */
static BOOL CALLBACK WadListProc(HWND hwndDlg, UINT uiMsg, WPARAM wParam, LPARAM lParam)
{
	UNREFERENCED_PARAMETER(wParam);
	switch(uiMsg)
	{
		case WM_INITDIALOG:
		/* Let the system set the focus. */
		return TRUE;

		case WM_SIZE:
		/* Resize the tree window to fit the dialogue. */
		SetWindowPos(GetDlgItem(hwndDlg, IDC_WADLIST_TREE), NULL, 0, 0, LOWORD(lParam) - 9, HIWORD(lParam) - 9, SWP_NOMOVE|SWP_NOZORDER);
		return TRUE;
	}

	return FALSE;
}



/* DestroyWadListWindow
 *   Destroys the wad-list window.
 *
 * Parameters:
 *   None.
 *
 * Return value: None.
 *
 * Remarks:
 *   This destroys the docking container, which in turn destroys the dialogue.
 */
void DestroyWadListWindow(void)
{
	if(IsWindow(g_hwndWadlist)) DestroyWindow(g_hwndWadlist);
}
