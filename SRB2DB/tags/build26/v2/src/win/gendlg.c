#include <windows.h>
#include <commctrl.h>
#include <uxtheme.h>
#include <tmschema.h>
#include <tchar.h>
#include <stdio.h>

#include "../general.h"

#include "mdiframe.h"
#include "gendlg.h"


#ifndef HTHEME
#define HTHEME HANDLE
#endif


/* We have to load UxTheme on demand, so we need some typedefs for its
 * functions.
 */
typedef HTHEME (WINAPI *OPENTHEMEDATAFUNC) (HWND hwnd, LPCWSTR pszClassList);
typedef HRESULT (WINAPI *CLOSETHEMEDATAFUNC) (HTHEME hTheme);
typedef HRESULT (WINAPI *DRAWTHEMEBACKGROUNDFUNC) (HTHEME hTheme, HDC hdc, int iPartId, int iStateId, const RECT *pRect, const RECT *pClipRect);


/* TransDlgProc
 *   Window procedure for mouse-transparent dialogue boxes.
 *
 * Parameters:
 *   HWND		hwnd		Window handle for message.
 *   UINT		uiMessage	ID for the message.
 *   WPARAM		wParam		Message-specific.
 *   LPARAM		lParam		Message-specific.
 *
 * Return value: LRESULT
 *   Message-specific.
 *
 * Remarks:
 *   Subclass the dialogue with this. Necessary since dialogue procs have no
 *   means to process WM_NCHITTEST usefully.
 */
LRESULT CALLBACK TransDlgProc(HWND hwndDlg, UINT uiMsg, WPARAM wParam, LPARAM lParam)
{
	switch(uiMsg)
	{
	case WM_NCHITTEST:
		{
			/* Get default HT result. */
			LRESULT lrHT = DefDlgProc(hwndDlg, uiMsg, wParam, lParam);

			/* If we're in the client are, fall through to the parent. */
			return (lrHT == HTCLIENT) ? HTTRANSPARENT : lrHT;
		}
	}

	/* Default behaviour. */
	return DefDlgProc(hwndDlg, uiMsg, wParam, lParam);
}



/* ListBoxSearchByItemData
 *   Searches a list box on its members' item data.
 *
 * Parameters:
 *   HWND		hwndListBox	Handle to list box.
 *   int		iItemData	Value to search for.
 *   BOOL		bSelect		Also select the match.
 *
 * Return value: int
 *   Index of first match, or negative if no match found.
 *
 * Remarks:
 *   Search ends after the first match.
 */
int ListBoxSearchByItemData(HWND hwndListBox, int iItemData, BOOL bSelect)
{
	int i, iCount;

	/* Find limit for looping through box. */
	iCount = SendMessage(hwndListBox, LB_GETCOUNT, 0, 0);

	for(i = 0; i < iCount; i++)
	{
		if(iItemData == SendMessage(hwndListBox, LB_GETITEMDATA, i, 0))
		{
			if(bSelect) SendMessage(hwndListBox, LB_SETCURSEL, i, 0);
			return i;
		}
	}

	/* No match. */
	if(bSelect) SendMessage(hwndListBox, LB_SETCURSEL, -1, 0);
	return -1;
}


/* BoundEditBox
 *   Bounds an edit-box's text between two ints.
 *
 * Parameters:
 *   HWND		hwndEditBox	Handle to edit box.
 *   int		iMin, iMax	Bounding range.
 *   BOOL		bPreserve	Leave empty boxes be?
 *
 * Return value: BOOL
 *   TRUE if box text was changed; FALSE otherwise.
 */
BOOL BoundEditBox(HWND hwndEditBox, int iMin, int iMax, BOOL bPreserveEmpty)
{
	int cch, i, j;
	LPSTR sz, szEnd;

	cch = GetWindowTextLength(hwndEditBox) + 1;
	sz = ProcHeapAlloc(cch * sizeof(TCHAR));
	GetWindowText(hwndEditBox, sz, cch);

	/* As a special case, preserve empty boxes. */
	if(bPreserveEmpty && *sz == '\0')
	{
		ProcHeapFree(sz);
		return FALSE;
	}

	i = _tcstol(sz, &szEnd, 10);
	j = min(max(i, iMin), iMax);
	wsprintf(sz, "%d", j);
	SetWindowText(hwndEditBox, sz);

	ProcHeapFree(sz);

	return *szEnd || i != j;
}


/* Init3StateListView
 *   Configures a list view control to incorporate 3-state checkboxes.
 *
 * Parameters:
 *   HWND		hwndListView	Handle to list view.
 *
 * Return value: None.
 *
 * Remarks:
 *   The list view should not have the LVS_EX_CHECKBOXES style. To perform the
 *   state logic in response to clicks/keypresses, call ListView3StateClick or
 *   ListView3StateKeyDown, respectively.
 */
void Init3StateListView(HWND hwndListView)
{
	HIMAGELIST himl;
	HDC hdc = CreateCompatibleDC(NULL);
	HBITMAP hbmUnchecked, hbmIndeterminate, hbmChecked;
	int cxSmIcon, cySmIcon;
	RECT rc;
	HGDIOBJ hgdiobjOriginal;

	/* For Visual Styles. */
	HMODULE hmodUxTheme;
	HTHEME hTheme = NULL;


	/* How big should the images be? */
	cxSmIcon = GetSystemMetrics(SM_CXMENUCHECK);
	cySmIcon = GetSystemMetrics(SM_CYMENUCHECK);

	/* Create the empty bitmaps. */
	hbmUnchecked = CreateBitmap(cxSmIcon, cySmIcon, 1, 32, NULL);
	hbmIndeterminate = CreateBitmap(cxSmIcon, cySmIcon, 1, 32, NULL);
	hbmChecked = CreateBitmap(cxSmIcon, cySmIcon, 1, 32, NULL);

	/* Bounding rectangle for the images. */
	rc.top = rc.left = 0;
	rc.right = cxSmIcon;
	rc.bottom = cySmIcon;

	/* Draw onto the bitmaps. How we do this depends on whether there's a visual
	 * style active.
	 */
	if((hmodUxTheme = LoadLibrary(TEXT("uxtheme.dll"))))
	{
		OPENTHEMEDATAFUNC lpfnOpenThemeData = (OPENTHEMEDATAFUNC)GetProcAddress(hmodUxTheme, "OpenThemeData");
		hTheme = lpfnOpenThemeData(hwndListView, L"BUTTON");
	}

	hgdiobjOriginal = SelectObject(hdc, hbmUnchecked);

	if(hTheme)
	{
		DRAWTHEMEBACKGROUNDFUNC lpfnDrawThemeBackground = (DRAWTHEMEBACKGROUNDFUNC)GetProcAddress(hmodUxTheme, "DrawThemeBackground");
		CLOSETHEMEDATAFUNC lpfnCloseThemeData = (CLOSETHEMEDATAFUNC)GetProcAddress(hmodUxTheme, "CloseThemeData");

		lpfnDrawThemeBackground(hTheme, hdc, BP_CHECKBOX, CBS_UNCHECKEDNORMAL, &rc, NULL);
		SelectObject(hdc, hbmIndeterminate);
		lpfnDrawThemeBackground(hTheme, hdc, BP_CHECKBOX, CBS_MIXEDNORMAL, &rc, NULL);
		SelectObject(hdc, hbmChecked);
		lpfnDrawThemeBackground(hTheme, hdc, BP_CHECKBOX, CBS_CHECKEDNORMAL, &rc, NULL);

		lpfnCloseThemeData(hTheme);
	}
	else
	{
		DrawFrameControl(hdc, &rc, DFC_BUTTON, DFCS_BUTTONCHECK );
		SelectObject(hdc, hbmIndeterminate);
		DrawFrameControl(hdc, &rc, DFC_BUTTON, DFCS_BUTTON3STATE | DFCS_CHECKED);
		SelectObject(hdc, hbmChecked);
		DrawFrameControl(hdc, &rc, DFC_BUTTON, DFCS_BUTTONCHECK | DFCS_CHECKED);
	}

	SelectObject(hdc, hgdiobjOriginal);

	/* Create the image list. It'll be destroyed when the list view is
	 * destroyed.
	 */
	himl = ImageList_Create(cxSmIcon, cySmIcon, ILC_COLOR24, 3, 0);

	/* Add the images. */
	ImageList_Add(himl, hbmUnchecked, NULL);
	ImageList_Add(himl, hbmIndeterminate, NULL);
	ImageList_Add(himl, hbmChecked, NULL);

	/* Associate the image list with the control. */
	ListView_SetImageList(hwndListView, himl, LVSIL_STATE);


	/* Clean up. */
	if(hmodUxTheme) FreeLibrary(hmodUxTheme);

	DeleteObject(hbmUnchecked);
	DeleteObject(hbmIndeterminate);
	DeleteObject(hbmChecked);

	DeleteDC(hdc);
}



/* ListView3StateClick
 *   Performs state changes for a 3-state checkbox list view in response to a
 *   mouse click.
 *
 * Parameters:
 *   LPNMLISTVIEW	lpnmlistview	Notification info from NM_CLICK notification
 *									message.
 *
 * Return value: None.
 */
void ListView3StateClick(LPNMLISTVIEW lpnmlistview)
{
	LVHITTESTINFO lvhti;
	int iIndex;

	/* Did we click on a checkbox? */
	lvhti.pt = lpnmlistview->ptAction;
	iIndex = ListView_HitTest(lpnmlistview->hdr.hwndFrom, &lvhti);
	if(lvhti.flags & LVHT_ONITEMSTATEICON)
		ListView3StateToggleItem(lpnmlistview->hdr.hwndFrom, iIndex);
}


/* ListView3StateKeyDown
 *   Performs state changes for a 3-state checkbox list view in response to a
 *   key depression.
 *
 * Parameters:
 *   LPNMLVKEYDOWN	lpnmlvkeydown	Notification info from LVN_KEYDOWN
 *									notification message.
 *
 * Return value: None.
 */
void ListView3StateKeyDown(LPNMLVKEYDOWN lpnmlvkeydown)
{
	int iIndex = ListView_GetSelectionMark(lpnmlvkeydown->hdr.hwndFrom);

	if(iIndex >= 0 && lpnmlvkeydown->wVKey == VK_SPACE)
		ListView3StateToggleItem(lpnmlvkeydown->hdr.hwndFrom, iIndex);
}


/* ListView3StateToggleItem
 *   Deselects an item in a 3-state checkbox list view if it was selected (even
 *   only partially), and selects it otherwise.
 *
 * Parameters:
 *   HWND	hwndListView	Handle to list view control.
 *   int	iIndex			Index of item to toggle.
 *
 * Return value: None.
 */
void ListView3StateToggleItem(HWND hwndListView, int iIndex)
{
	/* If the checkbox is fully or partially selected, deselect it;
	 * otherwise select it.
	 */
	int iState = ListView3StateGetItemState(hwndListView, iIndex);
	iState = (iState != LV3_UNCHECKED) ? LV3_UNCHECKED : LV3_CHECKED;
	ListView3StateSetItemState(hwndListView, iIndex, iState);
}


/* ListView3StateSetItemState
 *   Sets the state of an item in a 3-state checkbox list view.
 *
 * Parameters:
 *   HWND	hwndListView	Handle to list view control.
 *   int	iIndex			Index of item.
 *   int	iState			State (see ENUM_LV_3STATE).
 *
 * Return value: None.
 */
void ListView3StateSetItemState(HWND hwndListView, int iIndex, int iState)
{
	ListView_SetItemState(hwndListView, iIndex, iState << 12, LVIS_STATEIMAGEMASK);
}


/* ListView3StateGetItemState
 *   Gets the state of an item in a 3-state checkbox list view.
 *
 * Parameters:
 *   HWND	hwndListView	Handle to list view control.
 *   int	iIndex			Index of item.
 *
 * Return value: int
 *   State. See ENUM_LV_3STATE for values.
 */
int ListView3StateGetItemState(HWND hwndListView, int iIndex)
{
	return ListView_GetItemState(hwndListView, iIndex, LVIS_STATEIMAGEMASK) >> 12;
}
