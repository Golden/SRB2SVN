#ifndef __SRB2B_MDIFRAME__
#define __SRB2B_MDIFRAME__

/* Function prototypes. */
int CreateMainWindow(int iCmdShow);
void DestroyMainWindow(void);
BOOL CALLBACK CloseEnumProc(HWND hwnd, LPARAM lParam);
BOOL OpenWadForEditing(LPCTSTR szFilename);
void StatusBarNoWindow(void);

/* Globals. */
extern HWND g_hwndMain, g_hwndClient;
extern HMENU g_hmenuNodoc, g_hmenuMap;
extern HMENU g_hmenuNodocWin, g_hmenuMapWin;
extern HWND g_hwndStatusBar;
#endif
