#ifndef __SRB2B_TEXTURE__
#define __SRB2B_TEXTURE__

#include <windows.h>

#include "../wad/Wad.h"


/* Types. */

typedef struct _TEXTURE
{
	HBITMAP			hbitmap;
	unsigned short	cx, cy;
	short			xOffset, yOffset;
} TEXTURE;


typedef enum _TEX_FORMAT
{
	TF_TEXTURE,
	TF_FLAT,
	TF_IMAGE
} TEX_FORMAT;


typedef struct _TEXCACHE
{
	char				szLumpName[9];
	TEXTURE				*lptexture;

	struct _TEXCACHE	*lptcNext;
} TEXCACHE;


typedef struct _TEXTURENAMELIST
{
	LPTSTR	szNames;
	int		iEntries;
	int		cstrBuffer;		/* Number of strings the buffer can hold. */
} TEXTURENAMELIST;


/* Prototypes. */
TEXTURE* LoadTextureW(WAD *lpwad, LPCWSTR sTexName, TEX_FORMAT tf, RGBQUAD *lprgbq);
TEXTURE* LoadTextureA(WAD *lpwad, LPCSTR sTexName, TEX_FORMAT tf, RGBQUAD *lprgbq);
void DestroyTexture(TEXTURE *lptex);
TEXTURE* GetTextureFromCacheW(TEXCACHE *lptcHdr, LPCWSTR szTexName);
TEXTURE* GetTextureFromCacheA(TEXCACHE *lptcHdr, LPCSTR szTexName);
void PurgeTextureCache(TEXCACHE *lptcHdr);
TEXTURENAMELIST* CreateTextureNameList(void);
void DestroyTextureNameList(TEXTURENAMELIST *lptnl);
void AddAllTextureNamesToList(TEXTURENAMELIST *lptnl, WAD *lpwad, TEX_FORMAT tf);
void SortTextureNameList(TEXTURENAMELIST *lptnl);
LPCTSTR FindFirstTexNameMatch(LPCTSTR szTexName, TEXTURENAMELIST *lptnl);
void StretchTextureToDC(TEXTURE *lptex, HDC hdcTarget, int cx, int cy);
void AddToTextureCache(TEXCACHE *lptcHdr, LPCSTR szLumpName, TEXTURE *lptex);

/* Macros. */

#ifdef _UNICODE

#define LoadTexture LoadTextureW
#define GetTextureFromCache GetTextureFromCacheW

#else

#define LoadTexture LoadTextureA
#define GetTextureFromCache GetTextureFromCacheA

#endif

/* Size of PLAYPAL lump. */
#define CB_PLAYPAL 768

#define TEXNAME_BUFFER_LENGTH	9

#define CX_TEXPREVIEW 64
#define CY_TEXPREVIEW 64

#endif
