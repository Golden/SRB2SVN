#ifndef __SRB2B_KEYBOARD__
#define __SRB2B_KEYBOARD__

#define DestroyCustomAccelerator() (DestroyAcceleratorTable(g_hAccel))

extern HACCEL g_hAccel;

int MakeShiftedKeyCode(int iVirtKey);
void UpdateAcceleratorFromOptions(void);

#endif
