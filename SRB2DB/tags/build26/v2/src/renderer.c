#include <windows.h>
#include <math.h>

#include <GL/gl.h>
#include <GL/glu.h>

#include "general.h"
#include "maptypes.h"
#include "../res/resource.h"
#include "editing.h"
#include "config.h"
#include "options.h"
#include "renderer.h"

/* CodeImp's renderer. */
#include "CodeImp/ci_const.h"
#include "CodeImp/ci_renderer_proto.h"
#include "CodeImp/ci_map.h"


/* Used when constructing the palette. */
#define PALETTE_MIX_THINGSCOLOR 0.4
#define PALETTE_MIX_ORIGINALCOLOR 0.4
#define PALETTE_MIX_SELECTIONCOLOR 0.8


/* Globals. These mirror DB's renderer's behaviour. */
int g_cxClip;
int g_cyClip;
BYTE *g_lpbyDraw;

/* Globals of our own. */
RGBQUAD g_rgbqPalette[CLR_MAX];	/* Renderer palette. */


/* Static prototypes. */
static RGBQUAD LongToRGBQUAD(int i);
static void RenderGrid(MAPVIEW *lpmapview, unsigned short cx, unsigned short cy, BYTE byColour);



/* InitialiseRenderer
 *   Sets up the renderer during application initialisation.
 *
 * Parameters:
 *   None.
 *
 * Return value: None
 *
 * Remarks:
 *   Called in the processing of WM_CREATE by the main window.
 */
void InitialiseRenderer(void)
{
	LoadUserPalette();
}


/* ShutdownRenderer
 *   Sets up the renderer during application initialisation.
 *
 * Parameters:
 *   None.
 *
 * Return value: None
 *
 * Remarks:
 *   Called in the processing of WM_CREATE by the main window.
 */
void ShutdownRenderer(void)
{
	
}

/* LoadUserPalette
 *   Loads the renderer palette from the config file and selects it.
 *
 * Parameters:
 *   HPALETTE	hpal		Palette whose colours are to be set.
 *
 * Return value: None
 *
 * Remarks:
 *   Use CreateBasePalette to create the palette with the invariant colours set.
 *   Then call this whenever the user-specified colours need to be set.
 */
void LoadUserPalette(void)
{
	CONFIG *lpcfgPal;
     
	/* Make the palette from configuration. */

	/* First, get the palette section from the config, if it exists. */
	if(ConfigNodeExists(g_lpcfgMain, "palette"))
	{
		lpcfgPal = ConfigGetSubsection(g_lpcfgMain, "palette");
	}
	else
	{
		/* It doesn't exist (someone's been messing with the config file). So,
		 * just make an empty subsection, and draw everything in black...
		 * That'll larn 'im.
		 */
		lpcfgPal = ConfigAddSubsection(g_lpcfgMain, "palette");
	}
	
	/* Read in all the colour from the config, converting them to the required
	 * format.
	 */
	g_rgbqPalette[CLR_BACKGROUND] = LongToRGBQUAD(ConfigGetInteger(lpcfgPal, "CLR_BACKGROUND"));
	g_rgbqPalette[CLR_VERTEX] = LongToRGBQUAD(ConfigGetInteger(lpcfgPal, "CLR_VERTEX"));
	g_rgbqPalette[CLR_VERTEXSELECTED] = LongToRGBQUAD(ConfigGetInteger(lpcfgPal, "CLR_VERTEXSELECTED"));
	g_rgbqPalette[CLR_VERTEXHIGHLIGHT] = LongToRGBQUAD(ConfigGetInteger(lpcfgPal, "CLR_VERTEXHIGHLIGHT"));
	g_rgbqPalette[CLR_LINE] = LongToRGBQUAD(ConfigGetInteger(lpcfgPal, "CLR_LINE"));
	g_rgbqPalette[CLR_LINEDOUBLE] = LongToRGBQUAD(ConfigGetInteger(lpcfgPal, "CLR_LINEDOUBLE"));
	g_rgbqPalette[CLR_LINESPECIAL] = LongToRGBQUAD(ConfigGetInteger(lpcfgPal, "CLR_LINESPECIAL"));
	g_rgbqPalette[CLR_LINESPECIALDOUBLE] = LongToRGBQUAD(ConfigGetInteger(lpcfgPal, "CLR_LINESPECIALDOUBLE"));
	g_rgbqPalette[CLR_LINESELECTED] = LongToRGBQUAD(ConfigGetInteger(lpcfgPal, "CLR_LINESELECTED"));
	g_rgbqPalette[CLR_LINEHIGHLIGHT] = LongToRGBQUAD(ConfigGetInteger(lpcfgPal, "CLR_LINEHIGHLIGHT"));
	g_rgbqPalette[CLR_LINEDRAG] = LongToRGBQUAD(ConfigGetInteger(lpcfgPal, "CLR_LINEDRAG"));
	g_rgbqPalette[CLR_THINGTAG] = LongToRGBQUAD(ConfigGetInteger(lpcfgPal, "CLR_THINGTAG"));
	g_rgbqPalette[CLR_SECTORTAG] = LongToRGBQUAD(ConfigGetInteger(lpcfgPal, "CLR_SECTORTAG"));
	g_rgbqPalette[CLR_THINGUNKNOWN] = LongToRGBQUAD(ConfigGetInteger(lpcfgPal, "CLR_THINGUNKNOWN"));
	g_rgbqPalette[CLR_THINGSELECTED] = LongToRGBQUAD(ConfigGetInteger(lpcfgPal, "CLR_THINGSELECTED"));
	g_rgbqPalette[CLR_THINGHIGHLIGHT] = LongToRGBQUAD(ConfigGetInteger(lpcfgPal, "CLR_THINGHIGHLIGHT"));
	g_rgbqPalette[CLR_MULTISELECT] = LongToRGBQUAD(ConfigGetInteger(lpcfgPal, "CLR_MULTISELECT"));
	g_rgbqPalette[CLR_GRID] = LongToRGBQUAD(ConfigGetInteger(lpcfgPal, "CLR_GRID"));
	g_rgbqPalette[CLR_GRID64] = LongToRGBQUAD(ConfigGetInteger(lpcfgPal, "CLR_GRID64"));
	g_rgbqPalette[CLR_LINEBLOCKSOUND] = LongToRGBQUAD(ConfigGetInteger(lpcfgPal, "CLR_LINEBLOCKSOUND"));
	g_rgbqPalette[CLR_MAPBOUNDARY] = LongToRGBQUAD(ConfigGetInteger(lpcfgPal, "CLR_MAPBOUNDARY"));
	g_rgbqPalette[CLR_AXES] = LongToRGBQUAD(ConfigGetInteger(lpcfgPal, "CLR_AXES"));
	g_rgbqPalette[CLR_ZEROHEIGHTLINE] = LongToRGBQUAD(ConfigGetInteger(lpcfgPal, "CLR_ZEROHEIGHTLINE"));
	g_rgbqPalette[CLR_FOFSECTOR] = LongToRGBQUAD(ConfigGetInteger(lpcfgPal, "CLR_FOFSECTOR"));
	
	g_rgbqPalette[CLR_BLACK] = LongToRGBQUAD(0);
	
	
}


/* LongToRGBQUAD
 *   Converts an RGB integer to an RGBQUAD.
 *
 * Parameters:
 *   int	i	Integer with B in bits 16..23, G in 8..15 and R in 0..7.
 *
 * Return value: RGBQUAD
 *   Equivalent RGBQUAD.
 */
static RGBQUAD LongToRGBQUAD(int i)
{
	DWORD dw = i & 0xFFFFFF;

	/* The compiler takes a bit of persuading to return a DWORD as an RGBQUAD. */
	return *((RGBQUAD*)&dw);
}




/* RedrawMap
 *   Redraws the map to the backbuffer.
 *
 * Parameters:
 *   MAP*				lpmap		Map to redraw.
 *   ENUM_EDITMODE		editmode	Current editing mode.
 *   ENUM_EDITSUBMODE	submode		Current submode.
 *   MAPVIEW*			lpmapview	View settings (dimensions, zoom etc.)
 *   DRAWOP_RENDERINFO*	lpdori		Info for rendering drawing lines/vertices.
 *
 * Return value: None
 */
void RedrawMap(MAP *lpmap, ENUM_EDITMODE editmode, ENUM_EDITSUBMODE submode, MAPVIEW *lpmapview, DRAWOP_RENDERINFO *lpdori)
{
	if(editmode == EM_3D) return;

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glLoadIdentity();
	glScalef(lpmapview->fZoom, -lpmapview->fZoom, 1.0f);
	glTranslatef(-lpmapview->xLeft, -lpmapview->yTop, 0.0f);


	/* Disable antialiasing while we draw the grid. */
	glDisable(GL_LINE_SMOOTH);

	/* User-grid and 64-grid. */
	RenderGrid(lpmapview, lpmapview->cxGrid, lpmapview->cyGrid, CLR_GRID);
	if(lpmapview->cxGrid <= 64 || lpmapview->cyGrid <= 64)
		RenderGrid(lpmapview, 64, 64, CLR_GRID64);


	/* Map boundaries. */
	Render_LineF(-32767, -32767, 32766, -32767, CLR_MAPBOUNDARY);
	Render_LineF(32766, -32767, 32766, 32766, CLR_MAPBOUNDARY);
	Render_LineF(32766, 32766, -32767, 32766, CLR_MAPBOUNDARY);
	Render_LineF(-32767, 32766, -32767, -32767, CLR_MAPBOUNDARY);


	/* Re-enable antialiasing. */
	glEnable(GL_LINE_SMOOTH);


	Render_AllLinedefs(	lpmap->vertices,
						lpmap->linedefs,
						lpmap->sectors,
						lpmap->sidedefs,
						0,							/* Starting ld. */
						lpmap->iLinedefs - 1,
						lpmapview->iIndicatorSize);


	/* Mode-specific stuff, including mode-dependent submode things. */
	switch(editmode)
	{
	case EM_LINES:
		if(g_rendopts.bVerticesInLinesMode) Render_AllVertices(lpmap->vertices, 0, lpmap->iVertices - 1, lpmapview->fVertexSize);
		break;

	case EM_ANY:
		Render_AllVertices(lpmap->vertices, 0, lpmap->iVertices - 1, lpmapview->fVertexSize);
		Render_AllThings(lpmap->things, 0, lpmap->iThings - 1, lpmapview->fThingSize, lpmapview->fZoom, lpmapview->uiThingTex, FALSE, FALSE, NULL);
		break;

	case EM_SECTORS:
	case EM_MOVE:
		if(g_rendopts.bVerticesInSectorsMode) Render_AllVertices(lpmap->vertices, 0, lpmap->iVertices - 1, lpmapview->fVertexSize);
		break;

	case EM_VERTICES:
		Render_AllVertices(lpmap->vertices, 0, lpmap->iVertices - 1, lpmapview->fVertexSize);
		break;

	case EM_THINGS:
		Render_AllThings(lpmap->things, 0, lpmap->iThings - 1, lpmapview->fThingSize, lpmapview->fZoom, lpmapview->uiThingTex, FALSE, FALSE, NULL);
		
	default:
        break;
	}

	/* Submode-specific stuff that doesn't depend on mode. */
	switch(submode)
	{
	case ESM_DRAWING:
		/* Render the drawing line from the last-drawn vertex to the mouse. */
		Render_LinedefLineF(lpdori->ptSrc.x, lpdori->ptSrc.y, lpdori->ptDest.x, lpdori->ptDest.y, CLR_LINESELECTED, TRUE);

		/* Fall through. */
	case ESM_INSERTING:
		/* Draw a vertex at the mouse position. */
		Render_SquareF((float)lpdori->ptDest.x, (float)lpdori->ptDest.y, lpmapview->fVertexSize, CLR_VERTEXHIGHLIGHT);

		break;
		
	default:
        break;
	}
}


/* SetZoom
 *   Alters the zoom for a map view.
 *
 * Parameters:
 *   MAPVIEW*	lpmapview	View settings (dimensions, zoom etc.)
 *   float		fZoom		New zoom level.
 *
 * Return value: None
 *
 * Remarks:
 *   Adjusts other parameters that depend on zoom.
 */
void SetZoom(MAPVIEW *lpmapview, float fZoom)
{
	lpmapview->fZoom = fZoom;
	lpmapview->iIndicatorSize = (int)ceil((1/fZoom + 0.1f) * g_rendopts.iIndicatorSize);
	
	/* TODO: Write a nicer formula for this? */
	lpmapview->fVertexSize = ((float)ceil(g_rendopts.iVertexSize * sqrt(fZoom))) / fZoom;

	lpmapview->fThingSize = (1 + (float)ceil(8 * fZoom)) / fZoom;
    if(lpmapview->fThingSize > 32 / fZoom)
		lpmapview->fThingSize = 32 / fZoom;
}


/* RenderGrid
 *   Renders a rectangular grid.
 *
 * Parameters:
 *   MAPVIEW*			lpmapview	View settings (dimensions, zoom etc.)
 *   unsigned short		cx			Horizontal grid spacing.
 *   unsigned short		cy			Vertical grid spacing.
 *   BYTE				byColour	Colour to draw grid.
 *
 * Return value: None
 */
static void RenderGrid(MAPVIEW *lpmapview, unsigned short cx, unsigned short cy, BYTE byColour)
{
	unsigned short iOffset;
	int i;
	int iGridStart, iGridEnd;

	/* Check whether to render the grid. */
	if(lpmapview->bShowGrid)
	{
		/* Make sure the grid isn't too dense. */
		if(cx * lpmapview->fZoom > 4)
		{               
			/* X offset. */
			iOffset = lpmapview->xGridOffset % cx;

			/* Determine horizontal start and end. */
			iGridStart = ((int)(lpmapview->xLeft - cx - iOffset) / cx) * cx + iOffset;
			iGridEnd = ((int)(lpmapview->xLeft + lpmapview->cxDrawSurface / lpmapview->fZoom + cx - iOffset) / cx) * cx + iOffset;

			/* Vertical lines. */
			for(i = iGridStart; i <= iGridEnd; i += cx)
				Render_LineF((float)i, lpmapview->yTop, (float)i, lpmapview->yTop - lpmapview->cyDrawSurface / lpmapview->fZoom, byColour);
		}

		/* Make sure the grid isn't too dense. */
		if(cy * lpmapview->fZoom > 4)
		{               
			/* X offset. */
			iOffset = lpmapview->yGridOffset % cy;

			/* Determine vertical start and end. */
			iGridStart = ((int)(-lpmapview->yTop - cy - iOffset) / cy) * cy + iOffset;
			iGridEnd = ((int)(-lpmapview->yTop + lpmapview->cyDrawSurface / lpmapview->fZoom + cy - iOffset) / cy) * cy + iOffset;

			/* Horizontal lines. */
			for(i = iGridStart; i <= iGridEnd; i += cy)
				Render_LineF(lpmapview->xLeft, (float)-i, lpmapview->xLeft + lpmapview->cxDrawSurface / lpmapview->fZoom, (float)-i, byColour);
		}
	}
}


/* InitGL
 *   Initialises OpenGL for the current rendering context.
 *
 * Parameters:
 *   None.
 *
 * Return value: None
 *
 * Remarks:
 *   Call once for each child window creation. Uses the contexts specified by
 *   the most recent call to wglMakeCurrent.
 */
GLvoid InitGL(GLvoid)
{
	glShadeModel(GL_SMOOTH);
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);				/* Black Background */
	glClearDepth(1.0f);

	/* Anti-aliasing. */
	glEnable(GL_LINE_SMOOTH);
	glHint(GL_LINE_SMOOTH_HINT, GL_NICEST);

	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
}


/* LoadThingCircleTexture
 *   Loads the texture used to make things look round.
 *
 * Parameters:
 *   None.
 *
 * Return value: GLuint
 *   Texture name.
 *
 * Remarks:
 *   Each child window gets its own copy of the texture.
 */
GLuint LoadThingCircleTexture(GLvoid)
{
	HBITMAP hbm;
	BYTE lpbyBits[64][64][4];
	GLuint uiTexName;
	int i;

	/* Get bitmap. */
	hbm = LoadImage(g_hInstance, MAKEINTRESOURCE(IDB_THINGCIRCLE), IMAGE_BITMAP, 0, 0, 0);


	GetBitmapBits(hbm, 4096*4, lpbyBits);
	DeleteObject(hbm);

	/* Set alpha to equal intensity. TODO: Work out a way of storing an RGBA
	 * bitmap as a resource. Maybe just the raw bits?
	 */
	for(i=0; i < 4096; i++)
	{
		((BYTE*)lpbyBits)[4*i+3] = ((BYTE*)lpbyBits)[4*i];
	}

	/* Make texture. */
	glGenTextures(1, &uiTexName);
	glBindTexture(GL_TEXTURE_2D, uiTexName);
	glTexImage2D(GL_TEXTURE_2D, 0, 4, 64, 64, 0, GL_RGBA, GL_UNSIGNED_BYTE, lpbyBits);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	return uiTexName;
}


/* ReSizeGLScene
 *   Resizes the current rendering surface.
 *
 * Parameters:
 *   GLsizei		cx			Width.
 *   GLsizei		cy			Height.
 *
 * Return value: None
 */
GLvoid ReSizeGLScene(GLsizei cx, GLsizei cy)
{
	/* Avoid divide-by-zero. */
	if(cy == 0) cy = 1;

	/* Reset the viewport. */
	glViewport(0, 0, cx, cy);

	/* Select and reset the projection matrix. */
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	/* Orthographic view. */
	glOrtho(0.0f, cx, cy, 0.0f, -1.0f, 1.0f);

	/* Select and reset the modelview matrix. */
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
}


/* Render_LineF
 *   Renders a line.
 *
 * Parameters:
 *   float		x1, y1, x2, y2		Endpoint co-ordinates.
 *   BYTE		byColour			Colour index.
 *
 * Return value: None
 */
void __fastcall Render_LineF(float x1, float y1, float x2, float y2, BYTE byColour)
{
	glColor3ub(g_rgbqPalette[byColour].rgbRed, g_rgbqPalette[byColour].rgbGreen, g_rgbqPalette[byColour].rgbBlue);
	glBegin(GL_LINES);
		glVertex2f(x1, y1);
		glVertex2f(x2, y2);
	glEnd();
}


/* Render_SquareF
 *   Renders a square.
 *
 * Parameters:
 *   float		xCentre, yCentre	Co-ordinates of centre.
 *   float		fHalfSize			Perpendicular distance from centre to edge.
 *   BYTE		byColour			Colour index.
 *
 * Return value: None
 */
void __fastcall Render_SquareF(float xCentre, float yCentre, float fHalfSize, BYTE byColour)
{
	glColor3ub(g_rgbqPalette[byColour].rgbRed, g_rgbqPalette[byColour].rgbGreen, g_rgbqPalette[byColour].rgbBlue);

	glBegin(GL_TRIANGLE_FAN);
		glVertex2f(xCentre + fHalfSize, yCentre - fHalfSize);
		glVertex2f(xCentre + fHalfSize, yCentre + fHalfSize);
		glVertex2f(xCentre - fHalfSize, yCentre + fHalfSize);
		glVertex2f(xCentre - fHalfSize, yCentre - fHalfSize);
	glEnd();
}


/* Render_ThingPalettedF, Render_ThingF
 *   Renders a thing.
 *
 * Parameters:
 *   float		xCentre, yCentre	Co-ordinates of centre.
 *   int		iAngle				Angle, or negative for a square instead of an
 *									arrow.
 *   [BYTE		byPalColour]		Index of palette colour.
 *   [int		iColour]			RGBQUAD colour.
 *   float		fSize				Radius.
 *   GLuint		uiThingTex			ID of texture used to draw thing.
 *
 * Return value: None
 */
void __fastcall Render_ThingPalettedF(float xCentre, float yCentre, int iAngle, BYTE byPalColour, float fSize, float fZoom, GLuint uiThingTex)
{
	Render_ThingF(xCentre, yCentre, iAngle, *(int*)&g_rgbqPalette[byPalColour], fSize, fZoom, uiThingTex);
}

void __fastcall Render_ThingF(float xCentre, float yCentre, int iAngle, int iColour, float fSize, float fZoom, GLuint uiThingTex)
{
	Render_CircleF(xCentre, yCentre, fSize, iColour, uiThingTex);

	/* Render the arrow/dot only if we're close enough. */
	if(fZoom >= 0.2f)
	{
		if(iAngle >= 0)
		{
			glColor3f(0.0f, 0.0f, 0.0f);

			glPushMatrix();
			glTranslatef(xCentre, yCentre, 0.0f);
			glRotatef((float)iAngle, 0.0f, 0.0f, 1.0f);
			
			glBegin(GL_LINES);

				/* Bar. */
				glVertex2f(-fSize * 0.5f, 0.0f);
				glVertex2f(fSize * 0.5f, 0.0f);

				/* Pointy bit. */
				glVertex2f(fSize * 0.5f, 0.0f);
				glVertex2f(fSize * 0.1f, fSize * 0.4f);

				glVertex2f(fSize * 0.5f, 0.0f);
				glVertex2f(fSize * 0.1f, -fSize * 0.4f);

			glEnd();

			glPopMatrix();
		}
		else
		{
			Render_CircleF(xCentre, yCentre, fSize / 3, 0, uiThingTex);
		}
	}
}


/* These are just here to let the thing link. */
void __fastcall Render_BoxF(int xCentre, int yCentre, int iRadius, byte byColour)
{
	UNREFERENCED_PARAMETER(xCentre);
	UNREFERENCED_PARAMETER(yCentre);
	UNREFERENCED_PARAMETER(iRadius);
	UNREFERENCED_PARAMETER(byColour);
}

void __fastcall Render_CircleF(float xCentre, float yCentre, float fRadius, int iColour, GLuint uiThingTex)
{
	/* We render the circle by texturing a square. */
	glColor3ub((BYTE)((iColour >> 16) & 0xFF), (BYTE)((iColour >> 8) & 0xFF), (BYTE)(iColour & 0xFF));

	glEnable(GL_TEXTURE_2D);

	glBindTexture(GL_TEXTURE_2D, uiThingTex);

	glBegin(GL_TRIANGLE_FAN);
		glTexCoord2f(1.0f, 0.0f); glVertex2f(xCentre + fRadius, yCentre - fRadius);
		glTexCoord2f(1.0f, 1.0f); glVertex2f(xCentre + fRadius, yCentre + fRadius);
		glTexCoord2f(0.0f, 1.0f); glVertex2f(xCentre - fRadius, yCentre + fRadius);
		glTexCoord2f(0.0f, 0.0f); glVertex2f(xCentre - fRadius, yCentre - fRadius);
	glEnd();

	glDisable(GL_TEXTURE_2D);
}

void __fastcall Render_BitmapF(byte* bitmap, int width, int height, int sx, int sy, int sw, int sh, int tx, int ty, BYTE c1, BYTE c2)
{
	UNREFERENCED_PARAMETER(bitmap);
	UNREFERENCED_PARAMETER(width);
	UNREFERENCED_PARAMETER(height);
	UNREFERENCED_PARAMETER(sx);
	UNREFERENCED_PARAMETER(sy);
	UNREFERENCED_PARAMETER(sw);
	UNREFERENCED_PARAMETER(sh);
	UNREFERENCED_PARAMETER(tx);
	UNREFERENCED_PARAMETER(ty);
	UNREFERENCED_PARAMETER(c1);
	UNREFERENCED_PARAMETER(c2);
}
void __fastcall Render_ScaledBitmapF(byte* bitmap, int width, int height, int sx, int sy, int sw, int sh, int tx, int ty, BYTE c1, BYTE c2)
{
	UNREFERENCED_PARAMETER(bitmap);
	UNREFERENCED_PARAMETER(width);
	UNREFERENCED_PARAMETER(height);
	UNREFERENCED_PARAMETER(sx);
	UNREFERENCED_PARAMETER(sy);
	UNREFERENCED_PARAMETER(sw);
	UNREFERENCED_PARAMETER(sh);
	UNREFERENCED_PARAMETER(tx);
	UNREFERENCED_PARAMETER(ty);
	UNREFERENCED_PARAMETER(c1);
	UNREFERENCED_PARAMETER(c2);
}



/* ZoomToRect
 *   Adjusts the viewport s.t. it centres on a given rectangle, and contains the
 *   entire rectangle.
 *
 * Parameters:
 *   MAPVIEW*	lpmapview			Map view structure.
 *   RECT*		lprc				Rectangle to centre on.
 *   int		iBorderPercentage	Percentage border to add. 100 means 50% of
 *									rect's dimension at each side on dominant
 *									axis.
 *
 * Return value: None.
 */
void ZoomToRect(MAPVIEW *lpmapview, RECT *lprc, int iBorderPercentage)
{
	float fZoom;

	/* Which axis is dominant? */
	if(lpmapview->cyDrawSurface * (lprc->right - lprc->left) > lpmapview->cxDrawSurface * (lprc->top - lprc->bottom))
	{
		/* Horizontal axis. */
		fZoom = (float)lpmapview->cxDrawSurface / (float)(lprc->right - lprc->left);
	}
	else
	{
		/* Vertical axis. */
		fZoom = (float)lpmapview->cyDrawSurface / (float)(lprc->top - lprc->bottom);
	}

	/* Add border. */
	fZoom *= 100.0f;
	fZoom /= (float)(100 + iBorderPercentage);

	/* Set new zoom. */
	SetZoom(lpmapview, fZoom);

	/* Centre view on centre of rectangle. */
	CentreViewAt(lpmapview, (lprc->right + lprc->left) / 2, (lprc->top + lprc->bottom) / 2);
}


/* CentreViewAt
 *   Centres the view at a point.
 *
 * Parameters:
 *   MAPVIEW*	lpmapview	Map view structure.
 *   int		x, y		Point to centre on.
 *
 * Return value: None.
 */
void CentreViewAt(MAPVIEW *lpmapview, int x, int y)
{
	lpmapview->xLeft = x - lpmapview->cxDrawSurface / (lpmapview->fZoom * 2.0f);
	lpmapview->yTop = y + lpmapview->cyDrawSurface / (lpmapview->fZoom * 2.0f);
}
