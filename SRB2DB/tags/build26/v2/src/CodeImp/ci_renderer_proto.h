#ifndef __SRB2B_CI_RENDERER_PROTO__
#define __SRB2B_CI_RENDERER_PROTO__

#ifdef __cplusplus
extern "C" {
#endif

#include <windows.h>

void __fastcall Render_LinedefLineF(int x1, int y1, int x2, int y2, BYTE c, int sl);
void __fastcall Render_LineLengthF(int x1, int y1, int x2, int y2, BYTE* bitmap, int width, int height, int charwidth, int charheight, BYTE color);


#ifdef __cplusplus
}
#endif

#endif /* defined __SRB2B_CI_RENDERER_PROTO__ */
