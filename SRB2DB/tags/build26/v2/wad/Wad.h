#ifndef WAD_H
#define WAD_H

#include <stdio.h>
#include <windows.h>

/* Magic numbers used in the header. */
#define PWAD 0x44415750
#define IWAD 0x44415749


/* For BUILDDIR. */
#define LUMP_NEW		0
#define LUMP_FROMFILE	1


/* Logic */

#ifndef FALSE
#define FALSE 0
#endif


#ifndef TRUE
#define TRUE (!FALSE)
#endif



/* Structures. */


typedef struct sWADDIR
{
	int		offset;
	int		length;
	char	name[8];
} WADDIR;


typedef struct sLUMP
{
	char		name[8];
	char		lumptype;

	union
	{
		unsigned char	*lumpdata;
		int				srcoffset;
	};

	int length;

	struct sLUMP	*next;
} LUMP;


typedef struct sWAD
{
	FILE	*file;
	char	*filename;

	/* ALL lumps! */
	LUMP	lumphdr;
} WAD;






/* Function prototypes. */

WAD  * WINAPI CreateWAD(void);
void   WINAPI FreeWAD(WAD *wad);
WAD  * WINAPI OpenWAD(const char *filename);
int    WINAPI WriteWAD(WAD *wad, const char *filename);
int    WINAPI GetLump(WAD *wad, const char *lumpname, const char *section, unsigned char *buffer, int maxlen);
int    WINAPI GetLumpLength(WAD *wad, const char *lumpname, const char *section);
int    WINAPI DeleteLump(WAD *wad, const char *lumpname, const char *section);
int	   WINAPI EnumLumpNamesBySection(WAD *wad, void (WINAPI *f)(const char*, void*), void *lpv, char *startsec, char *endsec);
int    WINAPI SetLump(WAD *wad, const char *lumpname, const char *section, const unsigned char *data, int length);
int    WINAPI CreateLump(WAD *wad, const char *lumpname, const char *section, const char *addafter, char addatend);

#define EnumLumpNames(a,b,c) (EnumLumpNamesBySection((a),(b),(c),NULL,NULL))

#endif
