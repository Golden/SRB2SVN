# NSIS Script for SRB2 Doom Builder.

!include Library.nsh


Var ALREADY_INSTALLED

Page license
Page components
Page directory
Page instfiles
UninstPage uninstConfirm
UninstPage instfiles

Name "SRB2 Doom Builder"
OutFile "builder_srb2-setup-r26.exe"
LicenseData "GNU_GPL.txt"
InstallDir "$PROGRAMFILES\SRB2 Doom Builder"
XPStyle on
SetCompressor /SOLID lzma

Section "SRB2 Doom Builder"

	# Put these files in the installation directory.
	SetOutPath $INSTDIR
	
	# These files will be overwritten if they already exist.
	File "BuilderSRB2.exe"
	File "BuilderSRB2Lib.dll"
	File "GDIPlus.dll"
	
	File "Shortcuts.cfg"
	File "SOC.cfg"
	File "SRB2ex.cfg"
	File "Font.fnt"
	File "*.tga"
	File "*.bmp"
	File "changelog.txt"
	File "GNU_GPL.txt"
	File "readme.txt"
	
	# Only install the following if they don't already exist.
	SetOverwrite off
	File "ColourSchemes.cfg"
	File "FOFPresets.cfg"
	File "Parameters.cfg"
	File /oname=Builder.cfg Builder_Vanilla.cfg
	
	# Check whether we're already installed. Used for sharing count on CodeSense and DX8VB.
	IfFileExists "$INSTDIR\BuilderSRB2.exe" 0 new_installation
		StrCpy $ALREADY_INSTALLED 1
	new_installation:

	!insertmacro InstallLib REGDLL $ALREADY_INSTALLED NOREBOOT_NOTPROTECTED "C:\windows\system32\cmcs21.dll" "$SYSDIR\cmcs21.dll" "$SYSDIR"
	!insertmacro InstallLib REGDLL $ALREADY_INSTALLED NOREBOOT_NOTPROTECTED "C:\windows\system32\dx8vb.dll" "$SYSDIR\dx8vb.dll" "$SYSDIR"

	WriteUninstaller "UninstSRB2DB.exe"
SectionEnd

Section "ZenNode"
	SetOverwrite on
	File "ZenNode.exe"
SectionEnd

Section "Uninstall"
	Delete "$INSTDIR\UninstSRB2DB.exe"
	Delete "$INSTDIR\BuilderSRB2.exe"
	Delete "$INSTDIR\BuilderSRB2Lib.dll"
	Delete "$INSTDIR\GDIPlus.dll"
	Delete "$INSTDIR\Shortcuts.cfg"
	Delete "$INSTDIR\SOC.cfg"
	Delete "$INSTDIR\SRB2ex.cfg"
	Delete "$INSTDIR\Font.fnt"
	Delete "$INSTDIR\*.tga"
	Delete "$INSTDIR\*.bmp"
	Delete "$INSTDIR\changelog.txt"
	Delete "$INSTDIR\GNU_GPL.txt"
	Delete "$INSTDIR\readme.txt"
	Delete "$INSTDIR\ColourSchemes.cfg"
	Delete "$INSTDIR\FOFPresets.cfg"
	Delete "$INSTDIR\Parameters.cfg"
	Delete "$INSTDIR\Builder.cfg"
	RMDir "$INSTDIR"
SectionEnd

