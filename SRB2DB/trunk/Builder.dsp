# Microsoft Developer Studio Project File - Name="Builder" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 6.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Dynamic-Link Library" 0x0102

CFG=Builder - Win32 Debug
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "Builder.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "Builder.mak" CFG="Builder - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "Builder - Win32 Release" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE "Builder - Win32 Debug" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 0
# PROP Scc_ProjName ""
# PROP Scc_LocalPath ""
CPP=cl.exe
MTL=midl.exe
RSC=rc.exe

!IF  "$(CFG)" == "Builder - Win32 Release"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "Source\Release"
# PROP Intermediate_Dir "Source\Release"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MT /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_MBCS" /D "_USRDLL" /D "BUILDER_EXPORTS" /YX /FD /c
# ADD CPP /nologo /W3 /GX /Zd /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_MBCS" /D "_USRDLL" /D "BUILDER_EXPORTS" /FR /YX /FD /c
# ADD BASE MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x409 /d "NDEBUG"
# ADD RSC /l 0x409 /d "NDEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /dll /machine:I386
# ADD LINK32 kernel32.lib user32.lib LIBC.lib oleaut32.lib /nologo /dll /map:"BuilderSRB2Lib.map" /machine:I386 /nodefaultlib /out:"BuilderSRB2Lib.dll" /mapinfo:lines
# SUBTRACT LINK32 /pdb:none

!ELSEIF  "$(CFG)" == "Builder - Win32 Debug"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Debug"
# PROP BASE Intermediate_Dir "Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "Source\Debug"
# PROP Intermediate_Dir "Source\Debug"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MTd /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_MBCS" /D "_USRDLL" /D "BUILDER_EXPORTS" /YX /FD /GZ /c
# ADD CPP /nologo /W3 /Gm /GX /Zi /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_MBCS" /D "_USRDLL" /D "BUILDER_EXPORTS" /FAcs /FR /YX /FD /GZ /c
# ADD BASE MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x409 /d "_DEBUG"
# ADD RSC /l 0x409 /d "_DEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /dll /debug /machine:I386 /pdbtype:sept
# ADD LINK32 kernel32.lib user32.lib LIBC.lib oleaut32.lib /nologo /dll /pdb:"BuilderSRB2LibDebug.pdb" /map /debug /machine:I386 /nodefaultlib /out:"BuilderSRB2LibDebug.dll"
# SUBTRACT LINK32 /pdbtype:<none>

!ENDIF 

# Begin Target

# Name "Builder - Win32 Release"
# Name "Builder - Win32 Debug"
# Begin Group "Source Files"

# PROP Default_Filter "cpp;c;cxx;rc;def;r;odl;idl;hpj;bat"
# Begin Source File

SOURCE=.\Source\bsp.cpp
# End Source File
# Begin Source File

SOURCE=.\Source\clip.cpp
# End Source File
# Begin Source File

SOURCE=.\Source\data.cpp
# End Source File
# Begin Source File

SOURCE=.\Source\errorcheck.cpp
# End Source File
# Begin Source File

SOURCE=.\Source\export.cpp
# End Source File
# Begin Source File

SOURCE=.\Source\main.cpp
# End Source File
# Begin Source File

SOURCE=.\Source\map.cpp
# End Source File
# Begin Source File

SOURCE=.\Source\math.cpp
# End Source File
# Begin Source File

SOURCE=.\Source\pick.cpp
# End Source File
# Begin Source File

SOURCE=.\Source\poly.cpp
# End Source File
# Begin Source File

SOURCE=.\Source\renderer.cpp
# End Source File
# Begin Source File

SOURCE=.\Source\text.cpp
# End Source File
# Begin Source File

SOURCE=.\Source\textures.cpp
# End Source File
# Begin Source File

SOURCE=.\Source\undoredo.cpp
# End Source File
# End Group
# Begin Group "Header Files"

# PROP Default_Filter "h;hpp;hxx;hm;inl"
# Begin Source File

SOURCE=.\Source\clip.h
# End Source File
# Begin Source File

SOURCE=.\Source\const.h
# End Source File
# Begin Source File

SOURCE=.\Source\data.h
# End Source File
# Begin Source File

SOURCE=.\Source\math.h
# End Source File
# Begin Source File

SOURCE=.\Source\poly.h
# End Source File
# Begin Source File

SOURCE=.\Source\renderer.h
# End Source File
# Begin Source File

SOURCE=.\Source\things.h
# End Source File
# End Group
# Begin Group "Definition Files"

# PROP Default_Filter "def"
# Begin Source File

SOURCE=.\Source\Builder.def
# End Source File
# End Group
# End Target
# End Project
