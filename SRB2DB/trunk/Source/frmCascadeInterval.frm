VERSION 5.00
Begin VB.Form frmCascadeInterval 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Cascade Tags"
   ClientHeight    =   1575
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   3255
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmCascadeInterval.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   1575
   ScaleWidth      =   3255
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.CommandButton Command1 
      Cancel          =   -1  'True
      Caption         =   "Cancel"
      Height          =   375
      Left            =   1680
      TabIndex        =   4
      Top             =   1080
      Width           =   1215
   End
   Begin VB.CommandButton cmdOK 
      Caption         =   "OK"
      Default         =   -1  'True
      Height          =   375
      Left            =   360
      TabIndex        =   3
      Top             =   1080
      Width           =   1215
   End
   Begin DoomBuilder.ctlValueBox valIncrement 
      Height          =   375
      Left            =   1680
      TabIndex        =   2
      Top             =   480
      Width           =   975
      _ExtentX        =   1720
      _ExtentY        =   661
      Max             =   9999
      Min             =   1
      Value           =   "1"
   End
   Begin VB.Label Label2 
      Caption         =   "Increment:"
      Height          =   255
      Left            =   480
      TabIndex        =   1
      Top             =   540
      Width           =   975
   End
   Begin VB.Label Label1 
      Caption         =   "Please select an increment for cascading."
      Height          =   255
      Left            =   60
      TabIndex        =   0
      Top             =   60
      Width           =   3135
   End
End
Attribute VB_Name = "frmCascadeInterval"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub cmdOK_Click()
     Tag = Str(Minimum(valIncrement.Max, Maximum(valIncrement.min, valIncrement.Value)))
     Hide
End Sub

Private Sub Command1_Click()
     Tag = "0"
     Hide
End Sub
