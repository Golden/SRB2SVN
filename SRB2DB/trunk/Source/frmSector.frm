VERSION 5.00
Begin VB.Form frmSector 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Edit Sector Selection"
   ClientHeight    =   4575
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   7545
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   HasDC           =   0   'False
   Icon            =   "frmSector.frx":0000
   KeyPreview      =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   NegotiateMenus  =   0   'False
   ScaleHeight     =   305
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   503
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.Frame Frame5 
      Caption         =   " Adjacent Sector Info. "
      Height          =   1335
      Left            =   4920
      TabIndex        =   25
      Top             =   1740
      Width           =   2535
      Begin VB.Label lblFRCap 
         AutoSize        =   -1  'True
         Caption         =   "Floor range:"
         Height          =   210
         Left            =   120
         TabIndex        =   29
         Top             =   240
         Width           =   870
      End
      Begin VB.Label lblCRCap 
         AutoSize        =   -1  'True
         Caption         =   "Ceiling range:"
         Height          =   225
         Left            =   120
         TabIndex        =   28
         Top             =   720
         Width           =   975
      End
      Begin VB.Label lblFloorRange 
         AutoSize        =   -1  'True
         Height          =   195
         Left            =   360
         TabIndex        =   27
         Top             =   480
         Width           =   45
      End
      Begin VB.Label lblCeilRange 
         AutoSize        =   -1  'True
         Height          =   195
         Left            =   360
         TabIndex        =   26
         Top             =   960
         Width           =   45
      End
   End
   Begin VB.Frame Frame4 
      Caption         =   " Editing Aids "
      Height          =   855
      Left            =   75
      TabIndex        =   20
      Top             =   3195
      Width           =   7380
      Begin VB.TextBox txtDescription 
         Height          =   295
         Left            =   1800
         TabIndex        =   22
         Text            =   "Text1"
         Top             =   320
         Width           =   5415
      End
      Begin VB.Label Label7 
         Caption         =   "Sector description:"
         Height          =   255
         Left            =   240
         TabIndex        =   21
         Top             =   360
         Width           =   1575
      End
   End
   Begin VB.CommandButton cmdOK 
      Caption         =   "OK"
      Default         =   -1  'True
      Height          =   345
      Left            =   4200
      TabIndex        =   7
      Top             =   4140
      Width           =   1575
   End
   Begin VB.CommandButton cmdCancel 
      Cancel          =   -1  'True
      Caption         =   "Cancel"
      Height          =   345
      Left            =   5880
      TabIndex        =   8
      Top             =   4140
      Width           =   1575
   End
   Begin VB.Frame Frame3 
      Caption         =   " Appearance "
      Height          =   2055
      Left            =   75
      TabIndex        =   12
      Top             =   1020
      Width           =   4695
      Begin VB.CheckBox chkSetDefault 
         Caption         =   "Set as build defaults"
         Height          =   255
         Left            =   2820
         TabIndex        =   35
         Top             =   1740
         Width           =   1755
      End
      Begin VB.ComboBox cmbBrightness 
         Height          =   315
         ItemData        =   "frmSector.frx":000C
         Left            =   1395
         List            =   "frmSector.frx":000E
         TabIndex        =   23
         Text            =   "0"
         Top             =   1440
         Width           =   825
      End
      Begin VB.PictureBox picTCeiling 
         BackColor       =   &H8000000C&
         CausesValidation=   0   'False
         ClipControls    =   0   'False
         HasDC           =   0   'False
         Height          =   1020
         Left            =   2370
         ScaleHeight     =   64
         ScaleMode       =   3  'Pixel
         ScaleWidth      =   64
         TabIndex        =   16
         TabStop         =   0   'False
         ToolTipText     =   "Ceiling Texture"
         Top             =   270
         Width           =   1020
         Begin VB.Image imgTCeiling 
            Height          =   960
            Left            =   0
            Stretch         =   -1  'True
            ToolTipText     =   "Ceiling Texture"
            Top             =   0
            Width           =   960
         End
      End
      Begin VB.TextBox txtTCeiling 
         Height          =   315
         Left            =   2370
         MaxLength       =   8
         TabIndex        =   5
         Text            =   "-"
         Top             =   1350
         Width           =   1020
      End
      Begin VB.PictureBox picTFloor 
         BackColor       =   &H8000000C&
         CausesValidation=   0   'False
         ClipControls    =   0   'False
         HasDC           =   0   'False
         Height          =   1020
         Left            =   3510
         ScaleHeight     =   64
         ScaleMode       =   3  'Pixel
         ScaleWidth      =   64
         TabIndex        =   13
         TabStop         =   0   'False
         ToolTipText     =   "Floor Texture"
         Top             =   270
         Width           =   1020
         Begin VB.Image imgTFloor 
            Height          =   960
            Left            =   0
            Stretch         =   -1  'True
            ToolTipText     =   "Floor Texture"
            Top             =   0
            Width           =   960
         End
      End
      Begin VB.TextBox txtTFloor 
         Height          =   315
         Left            =   3510
         MaxLength       =   8
         TabIndex        =   6
         Text            =   "-"
         Top             =   1350
         Width           =   1020
      End
      Begin DoomBuilder.ctlValueBox txtHCeiling 
         Height          =   360
         Left            =   1395
         TabIndex        =   3
         Top             =   240
         Width           =   825
         _ExtentX        =   1455
         _ExtentY        =   635
         MaxLength       =   8
         Min             =   -32767
         SmallChange     =   8
         EmptyAllowed    =   -1  'True
         RelativeAllowed =   -1  'True
      End
      Begin DoomBuilder.ctlValueBox txtHFloor 
         Height          =   360
         Left            =   1395
         TabIndex        =   4
         Top             =   690
         Width           =   825
         _ExtentX        =   1455
         _ExtentY        =   635
         MaxLength       =   8
         Min             =   -32767
         SmallChange     =   8
         EmptyAllowed    =   -1  'True
         RelativeAllowed =   -1  'True
      End
      Begin VB.Label Label2 
         Alignment       =   1  'Right Justify
         AutoSize        =   -1  'True
         Caption         =   "Brightness:"
         Height          =   210
         Left            =   330
         TabIndex        =   24
         Top             =   1485
         UseMnemonic     =   0   'False
         Width           =   885
      End
      Begin VB.Label lblHeight 
         AutoSize        =   -1  'True
         Caption         =   "0"
         Height          =   210
         Left            =   1425
         TabIndex        =   19
         Top             =   1140
         Width           =   90
      End
      Begin VB.Label Label6 
         Alignment       =   1  'Right Justify
         AutoSize        =   -1  'True
         Caption         =   "Sector height:"
         Height          =   210
         Left            =   60
         TabIndex        =   18
         Top             =   1140
         Width           =   1125
      End
      Begin VB.Label Label5 
         Alignment       =   1  'Right Justify
         AutoSize        =   -1  'True
         Caption         =   "Floor height:"
         Height          =   210
         Left            =   330
         TabIndex        =   15
         Top             =   765
         Width           =   885
      End
      Begin VB.Label Label4 
         Alignment       =   1  'Right Justify
         AutoSize        =   -1  'True
         Caption         =   "Ceiling height:"
         Height          =   210
         Left            =   165
         TabIndex        =   14
         Top             =   315
         Width           =   1050
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   " Action "
      Height          =   675
      Left            =   4920
      TabIndex        =   10
      Top             =   1020
      Width           =   2535
      Begin VB.CommandButton cmdNextTag 
         Caption         =   "Next Unused"
         Height          =   360
         Left            =   1320
         TabIndex        =   2
         Top             =   180
         Width           =   1095
      End
      Begin DoomBuilder.ctlValueBox txtTag 
         Height          =   360
         Left            =   540
         TabIndex        =   1
         Top             =   180
         Width           =   705
         _ExtentX        =   1244
         _ExtentY        =   635
         MaxLength       =   5
         Min             =   -32768
         EmptyAllowed    =   -1  'True
         RelativeAllowed =   -1  'True
         Unsigned        =   -1  'True
      End
      Begin VB.Label Label3 
         Alignment       =   1  'Right Justify
         AutoSize        =   -1  'True
         Caption         =   "Tag:"
         Height          =   195
         Left            =   120
         TabIndex        =   11
         Top             =   240
         UseMnemonic     =   0   'False
         Width           =   330
      End
   End
   Begin VB.Frame Frame2 
      Caption         =   "Sector Effect"
      Height          =   855
      Left            =   75
      TabIndex        =   9
      Top             =   90
      Width           =   7380
      Begin VB.ComboBox cmbSectorTypes11 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   3
         Left            =   4320
         Sorted          =   -1  'True
         Style           =   2  'Dropdown List
         TabIndex        =   34
         Top             =   480
         Visible         =   0   'False
         Width           =   3015
      End
      Begin VB.ComboBox cmbSectorTypes11 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   2
         Left            =   1200
         Sorted          =   -1  'True
         Style           =   2  'Dropdown List
         TabIndex        =   33
         Top             =   480
         Visible         =   0   'False
         Width           =   3015
      End
      Begin VB.ComboBox cmbSectorTypes11 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   1
         Left            =   4320
         Sorted          =   -1  'True
         Style           =   2  'Dropdown List
         TabIndex        =   32
         Top             =   120
         Visible         =   0   'False
         Width           =   3015
      End
      Begin VB.ComboBox cmbSectorTypes11 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   0
         Left            =   1200
         Sorted          =   -1  'True
         Style           =   2  'Dropdown List
         TabIndex        =   31
         Top             =   120
         Visible         =   0   'False
         Width           =   3015
      End
      Begin VB.ComboBox cmbSectorTypes109 
         Height          =   315
         Left            =   1200
         Sorted          =   -1  'True
         Style           =   2  'Dropdown List
         TabIndex        =   30
         Top             =   300
         Visible         =   0   'False
         Width           =   6015
      End
      Begin DoomBuilder.ctlValueBox txtType 
         Height          =   360
         Left            =   240
         TabIndex        =   0
         Top             =   285
         Width           =   825
         _ExtentX        =   1455
         _ExtentY        =   635
         MaxLength       =   5
         Min             =   -32768
         EmptyAllowed    =   -1  'True
         Unsigned        =   -1  'True
      End
   End
   Begin VB.Label lblMakeUndo 
      Height          =   210
      Left            =   360
      TabIndex        =   17
      Top             =   4095
      UseMnemonic     =   0   'False
      Visible         =   0   'False
      Width           =   900
   End
End
Attribute VB_Name = "frmSector"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'
'    Doom Builder
'    Copyright (c) 2003 Pascal vd Heiden, www.codeimp.com
'    This program is released under GNU General Public License
'
'    This program is distributed in the hope that it will be useful,
'    but WITHOUT ANY WARRANTY; without even the implied warranty of
'    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
'    GNU General Public License for more details.
'


'Do not allow any undeclared variables
Option Explicit

'Case sensitive comparisions
Option Compare Binary

Private DescChanged As Boolean
Private DisableListUpdate As Boolean, DisableBoxUpdate As Boolean

Public Function CheckSectorBrightness() As String
     Dim i As Long
     Dim Indices As Variant
     
     'Get selection indices
     Indices = selected.Items
     
     'Set result to first sector's brightness
     CheckSectorBrightness = sectors(Indices(LBound(Indices))).Brightness
     
     'Go for all selected sectors
     For i = LBound(Indices) To UBound(Indices)
          
          'Check if the brightness is different
          If (sectors(Indices(i)).Brightness <> CheckSectorBrightness) Then
               CheckSectorBrightness = ""
               Exit Function
          End If
     Next i
End Function

Public Function CheckSectorHCeiling() As String
     Dim i As Long
     Dim Indices As Variant
     
     'Get selection indices
     Indices = selected.Items
     
     'Set result to first
     CheckSectorHCeiling = sectors(Indices(LBound(Indices))).hceiling
     
     'Go for all selected sectors
     For i = LBound(Indices) To UBound(Indices)
          
          'Check if different
          If (sectors(Indices(i)).hceiling <> CheckSectorHCeiling) Then
               CheckSectorHCeiling = ""
               Exit Function
          End If
     Next i
End Function

Public Function CheckSectorHFloor() As String
     Dim i As Long
     Dim Indices As Variant
     
     'Get selection indices
     Indices = selected.Items
     
     'Set result to first
     CheckSectorHFloor = sectors(Indices(LBound(Indices))).HFloor
     
     'Go for all selected sectors
     For i = LBound(Indices) To UBound(Indices)
          
          'Check if different
          If (sectors(Indices(i)).HFloor <> CheckSectorHFloor) Then
               CheckSectorHFloor = ""
               Exit Function
          End If
     Next i
End Function

Public Function CheckSectorTag() As String
     Dim i As Long
     Dim Indices As Variant
     
     'Get selection indices
     Indices = selected.Items
     
     'Set result to first sector's tag
     CheckSectorTag = sectors(Indices(LBound(Indices))).Tag
     
     'Go for all selected sectors
     For i = LBound(Indices) To UBound(Indices)
          
          'Check if the tag is different
          If (sectors(Indices(i)).Tag <> CheckSectorTag) Then
               CheckSectorTag = ""
               Exit Function
          End If
     Next i
End Function

Public Function CheckSectorTCeiling() As String
     Dim i As Long
     Dim Indices As Variant
     
     'Get selection indices
     Indices = selected.Items
     
     'Set result to first
     CheckSectorTCeiling = UCase$(sectors(Indices(LBound(Indices))).tceiling)
     
     'Go for all selected sectors
     For i = LBound(Indices) To UBound(Indices)
          
          'Check if different
          If (UCase$(sectors(Indices(i)).tceiling) <> CheckSectorTCeiling) Then
               CheckSectorTCeiling = ""
               Exit Function
          End If
     Next i
End Function

Public Function CheckSectorTFloor() As String
     Dim i As Long
     Dim Indices As Variant
     
     'Get selection indices
     Indices = selected.Items
     
     'Set result to first
     CheckSectorTFloor = UCase$(sectors(Indices(LBound(Indices))).TFloor)
     
     'Go for all selected sectors
     For i = LBound(Indices) To UBound(Indices)
          
          'Check if different
          If (UCase$(sectors(Indices(i)).TFloor) <> CheckSectorTFloor) Then
               CheckSectorTFloor = ""
               Exit Function
          End If
     Next i
End Function

Public Function CheckSectorType() As String
     Dim i As Long
     Dim Indices As Variant
     
     'Get selection indices
     Indices = selected.Items
     
     'Set result to first sector's type
     CheckSectorType = sectors(Indices(LBound(Indices))).special
     
     'Go for all selected sectors
     For i = LBound(Indices) To UBound(Indices)
          
          'Check if the type is different
          If (sectors(Indices(i)).special <> CheckSectorType) Then
               CheckSectorType = ""
               Exit Function
          End If
     Next i
End Function

Private Function CheckSectorDescription() As String
     Dim i As Long
     Dim Indices As Variant
     Dim SectorDescriptions As Dictionary
     
     If WadSettings.Exists(UCase$(maplumpname)) Then
     
          If WadSettings(UCase$(maplumpname)).Exists("sectordesc") Then
          
               ' Get sector description config.
               Set SectorDescriptions = WadSettings(UCase$(maplumpname))("sectordesc")
               
               'Get selection indices
               Indices = selected.Items
               
               'Set result to first sector's desc
               CheckSectorDescription = SectorDescriptions(CStr(Indices(LBound(Indices))))
               
               'Go for all selected sectors
               For i = LBound(Indices) To UBound(Indices)
                    
                    'Check if the desc is different
                    If (SectorDescriptions(CStr(Indices(i))) <> CheckSectorDescription) Then
                         CheckSectorDescription = ""
                         Exit Function
                    End If
               Next i
          Else
               CheckSectorDescription = ""
          End If
     Else
          CheckSectorDescription = ""
     End If
     
End Function

Private Sub cmbBrightness_GotFocus()
     SelectAllText cmbBrightness
End Sub

Private Sub cmbBrightness_KeyPress(KeyAscii As Integer)
     If (KeyAscii <> 8) And _
        ((KeyAscii < 48) Or (KeyAscii > 57)) And _
        (KeyAscii <> 43) And (KeyAscii <> 45) Then KeyAscii = 0
End Sub

Private Sub cmbSectorTypes109_Click()
     If cmbSectorTypes109.ListIndex <> -1 Then
          txtType.Text = CStr(cmbSectorTypes109.ItemData(cmbSectorTypes109.ListIndex))
     End If
End Sub

Private Sub cmbSectorTypes11_Click(Index As Integer)
     
     ' Otherwise one cause the other...
     If DisableBoxUpdate Then Exit Sub
     
     DisableListUpdate = True

     ' If nothing's selected, we've got nothing to do.
     If cmbSectorTypes11(Index).ListIndex >= 0 Then
          txtType.Text = CStr((Val(txtType.Text) And Not (&HF * &H10 ^ Index)) Or (Val(cmbSectorTypes11(Index).ItemData(cmbSectorTypes11(Index).ListIndex)) * (&H10 ^ Index)))
     End If
     
     DisableListUpdate = False
     
End Sub

Private Sub cmdCancel_Click()
     Unload Me
     Set frmSector = Nothing
End Sub

Private Sub cmdNextTag_Click()
     txtTag.Text = NextUnusedTag
End Sub

Private Sub cmdOK_Click()
     Dim Indices As Variant
     Dim i As Long
     Dim s As Long
     Dim SectorDesc As Dictionary
     Dim RecalcTags As Boolean
     
     'Change mousepointer
     Screen.MousePointer = vbHourglass
     
     'Make undo
     If (lblMakeUndo.Caption = "") Then CreateUndo "sector edit"
     
     'Get the selection indices
     Indices = selected.Items
     
     ' Allocate settings if necessary.
     If (txtDescription.Text <> "") Or DescChanged Then
               
          If Not WadSettings.Exists(UCase$(maplumpname)) Then
               WadSettings.Add UCase$(maplumpname), New Dictionary
          End If
          
          If Not WadSettings(UCase$(maplumpname)).Exists("sectordesc") Then
               Set SectorDesc = New Dictionary
               WadSettings(UCase$(maplumpname)).Add "sectordesc", SectorDesc
          Else
               Set SectorDesc = WadSettings(UCase$(maplumpname))("sectordesc")
          End If
          
     End If
     
     'Go for all selected sectors
     For i = LBound(Indices) To UBound(Indices)
          
          'Get the sector index
          s = Indices(i)
          
          'Apply type if a type is specified
          If (txtType.Text <> "") Then sectors(s).special = Val(txtType.Text)
          
          'Apply tag if a tag is specified
          If (txtTag.Text <> "") Then
               sectors(s).Tag = txtTag.RelativeValue(sectors(s).Tag)
               RecalcTags = True
          End If
          
          'Apply brightness if a brightness is specified
          If (cmbBrightness.Text <> "") Then sectors(s).Brightness = RelativeBrightness(sectors(s).Brightness)
          
          'Apply heights if specified
          If (txtHCeiling.Text <> "") Then sectors(s).hceiling = txtHCeiling.RelativeValue(sectors(s).hceiling)
          If (txtHFloor.Text <> "") Then sectors(s).HFloor = txtHFloor.RelativeValue(sectors(s).HFloor)
          
          'Apply textures if specified
          If (txtTCeiling.Text <> "") Then sectors(s).tceiling = txtTCeiling.Text
          If (txtTFloor.Text <> "") Then sectors(s).TFloor = txtTFloor.Text
          
          ' Apply description if specified.
          If (txtDescription.Text <> "") Or DescChanged Then SectorDesc(CStr(s)) = txtDescription.Text
          
     Next i
     
     'Map is modified
     mapnodeschanged = True
     mapchanged = True
     
     'Make build defaults if requested
     If (chkSetDefault.Value = vbChecked) Then
          
          'Set the build defaults
          If (Val(Config("storeeditinginfo"))) Then
          
               If Not WadSettings.Exists("defaultsector") Then WadSettings.Add "defaultsector", New Dictionary
               WadSettings("defaultsector")("brightness") = Val(cmbBrightness.Text)
               WadSettings("defaultsector")("hceiling") = Val(txtHCeiling.Value)
               WadSettings("defaultsector")("hfloor") = Val(txtHFloor.Value)
               WadSettings("defaultsector")("tceiling") = txtTCeiling.Text
               WadSettings("defaultsector")("tfloor") = txtTFloor.Text
          Else
               Config("defaultsector")("brightness") = Val(cmbBrightness.Text)
               Config("defaultsector")("hceiling") = Val(txtHCeiling.Value)
               Config("defaultsector")("hfloor") = Val(txtHFloor.Value)
               Config("defaultsector")("tceiling") = txtTCeiling.Text
               Config("defaultsector")("tfloor") = txtTFloor.Text
          End If
     End If
     
     If RecalcTags Then
          UpdateFOFTagCache
          If IsLoaded(frmTags) Then frmTags.RefreshTagList
     End If
     
     'Reset mousepointer
     Screen.MousePointer = vbNormal
     
     'Leave here
     Unload Me
     Set frmSector = Nothing
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
     
     'Adjust shift mask
     CurrentShiftMask = Shift
End Sub

Private Sub Form_KeyUp(KeyCode As Integer, Shift As Integer)
     
     'Adjust shift mask
     CurrentShiftMask = Shift
End Sub


Private Sub Form_Load()
     Dim i As Long, j As Long
     Dim Levels As Variant
     Dim Keys As Variant
     Dim adj As String
     Dim SecType As Long
     
     'Check if only one selected
     If (numselected = 1) Then Caption = Caption & " - Sector " & selected.Items(0)
     
     'Fill combo with default brightness levels
     Levels = mapconfig("sectorbrightness").Keys
     For i = LBound(Levels) To UBound(Levels)
          cmbBrightness.AddItem Levels(i)
     Next i
     
     ' Fill the sector-type list.
     ' We do this differently for 1.09 and 1.1
     If mapconfig("seceffectnybble") Then
          
          ' 1.1 splits sector effect into nybbles.
          For j = 0 To cmbSectorTypes11.Count - 1
               Keys = mapconfig("sectortypes")("nybble" & CStr(j)).Keys
               For i = LBound(Keys) To UBound(Keys)
                    
                    ' Add the item to list
                    cmbSectorTypes11(j).AddItem mapconfig("sectortypes")("nybble" & CStr(j))(Keys(i))
                    cmbSectorTypes11(j).ItemData(cmbSectorTypes11(j).NewIndex) = Val(Keys(i))
                    
               Next i
               
               cmbSectorTypes11(j).visible = True
          Next j
          
     Else
          ' Simple 1.09 way.
          Keys = mapconfig("sectortypes").Keys
          For i = LBound(Keys) To UBound(Keys)
               
               ' Add the item to list
               cmbSectorTypes109.AddItem mapconfig("sectortypes")(Keys(i))
               cmbSectorTypes109.ItemData(cmbSectorTypes109.NewIndex) = Val(Keys(i))
               
          Next i
          
          cmbSectorTypes109.visible = True
     End If
     
     'Sector type
     txtType.Text = CheckSectorType
     If CheckSectorType <> "" Then
          SetSecTypeFromValue Val(CheckSectorType)
     End If
    
     
     'Sector tag
     txtTag.Text = CheckSectorTag
     
     'Sector Brightness
     cmbBrightness.Text = CheckSectorBrightness
     
     'Floor and Ceiling
     txtHCeiling.Text = CheckSectorHCeiling
     txtHFloor.Text = CheckSectorHFloor
     txtTCeiling.Text = CheckSectorTCeiling
     txtTFloor.Text = CheckSectorTFloor
     txtDescription.Text = CheckSectorDescription
     
     ' Adjacent sector info.
     ' We need to split its output.
     adj = GetCeilRange
     lblCRCap.Caption = left$(adj, InStr(adj, ":"))
     lblCeilRange.Caption = right$(adj, Len(adj) - Len(lblCRCap.Caption) - 1)
     
     adj = GetFloorRange
     lblFRCap.Caption = left$(adj, InStr(adj, ":"))
     lblFloorRange.Caption = right$(adj, Len(adj) - Len(lblFRCap.Caption) - 1)
     
     ' Desc hasn't changed. It's allowed to be blank, so we can't use that in the case
     ' of multiple selected sectors.
     DescChanged = False
     
     'Set the relativescroll property
     'NOTE: This doesnt work nicely
     'txtHCeiling.RelativeScroll = (numselected > 1)
     'txtHFloor.RelativeScroll = (numselected > 1)
End Sub

Public Function RelativeBrightness(ByVal OriginalValue As Long) As Long
     On Local Error Resume Next
     
     'Check if theres anything given
     If (Replace$(Replace$(cmbBrightness.Text, "-", ""), "+", "") <> "") Then
          
          'Check if the value is relative
          If (left$(cmbBrightness.Text, 2) = "--") Or (left$(cmbBrightness.Text, 2) = "++") Then
               
               'Add/Subtract to original
               RelativeBrightness = OriginalValue + Val(Mid$(cmbBrightness.Text, 2))
          Else
               
               'Apply normally
               RelativeBrightness = Val(cmbBrightness.Text)
          End If
     Else
          
          'Keep original value
          RelativeBrightness = OriginalValue
     End If
End Function






Private Sub imgTCeiling_Click()
     txtTCeiling.Text = SelectFlat(txtTCeiling.Text, Me)
End Sub

Private Sub imgTFloor_Click()
     txtTFloor.Text = SelectFlat(txtTFloor.Text, Me)
End Sub



Private Sub txtDescription_Change()

     DescChanged = True

End Sub

Private Sub txtDescription_GotFocus()

     SelectAllText txtDescription

End Sub

Private Sub txtHCeiling_Change()
     
     'Display the height
     If (Trim$(txtHCeiling.Text) <> "") And (Trim$(txtHFloor.Text) <> "") Then
          lblHeight.Caption = Val(txtHCeiling.Value) - Val(txtHFloor.Value)
     Else
          lblHeight.Caption = "-"
     End If
End Sub

Private Sub txtHCeiling_GotFocus()
     SelectAllText txtHCeiling
End Sub


Private Sub txtHFloor_Change()
     
     'Display the height
     If (Trim$(txtHCeiling.Text) <> "") And (Trim$(txtHFloor.Text) <> "") Then
          lblHeight.Caption = Val(txtHCeiling.Value) - Val(txtHFloor.Value)
     Else
          lblHeight.Caption = "-"
     End If
End Sub

Private Sub txtHFloor_GotFocus()
     SelectAllText txtHFloor
End Sub


Private Sub txtTag_GotFocus()
     SelectAllText txtTag
End Sub


Private Sub txtTCeiling_Change()
     
     'Set the flat in the preview box
     GetScaledFlatPicture txtTCeiling.Text, imgTCeiling
End Sub

Private Sub txtTCeiling_GotFocus()
     SelectAllText txtTCeiling
End Sub


Private Sub txtTCeiling_KeyUp(KeyCode As Integer, Shift As Integer)
     
     'Complete texture name
     If Val(Config("autocompletetypetex")) And (txtTCeiling.SelLength = 0) Then CompleteFlatName KeyCode, Shift, txtTCeiling
End Sub

Private Sub txtTCeiling_Validate(Cancel As Boolean)
     
     'Find closest match if preferred
     If Val(Config("autocompletetex")) Then txtTCeiling.Text = GetNearestFlatName(txtTCeiling.Text)
End Sub


Private Sub txtTFloor_Change()
     
     'Set the flat in the preview box
     GetScaledFlatPicture txtTFloor.Text, imgTFloor
End Sub

Private Sub txtTFloor_GotFocus()
     SelectAllText txtTFloor
End Sub


Private Sub txtTFloor_KeyUp(KeyCode As Integer, Shift As Integer)
     
     'Complete texture name
     If Val(Config("autocompletetypetex")) And (txtTFloor.SelLength = 0) Then CompleteFlatName KeyCode, Shift, txtTFloor
End Sub

Private Sub txtTFloor_Validate(Cancel As Boolean)
     
     'Find closest match if preferred
     If Val(Config("autocompletetex")) Then txtTFloor.Text = GetNearestFlatName(txtTFloor.Text)
End Sub


Private Sub txtType_Change()

     Dim i As Long
     
     ' Otherwise one causes the other...
     If DisableListUpdate Then Exit Sub
     
     DisableBoxUpdate = True
     
     If IsNumeric(txtType.Text) Then
          SetSecTypeFromValue Val(txtType.Text)
     Else
          cmbSectorTypes109.ListIndex = -1
          
          For i = 0 To cmbSectorTypes11.Count - 1
               cmbSectorTypes11(i).ListIndex = -1
          Next i
     End If
     
     DisableBoxUpdate = False
     
End Sub

Private Sub txtType_GotFocus()
     SelectAllText txtType
End Sub


Private Sub SetSecTypeFromValue(ByVal effect As Long)
     
     Dim i As Long, j As Long
     Dim MatchFound As Boolean
     
     If mapconfig("seceffectnybble") Then
     
          For j = 0 To cmbSectorTypes11.Count - 1
          
               MatchFound = False
               
               ' Find matching item.
               For i = 0 To cmbSectorTypes11(j).ListCount - 1
                    If cmbSectorTypes11(j).ItemData(i) = ((effect \ &H10 ^ j) And &HF) Then
                         cmbSectorTypes11(j).ListIndex = i
                         MatchFound = True
                         Exit For
                    End If
               Next i
               
               If Not MatchFound Then cmbSectorTypes11(j).ListIndex = -1
               
          Next j
          
     Else
     
          MatchFound = False
          
          ' Find matching item.
          For i = 0 To cmbSectorTypes109.ListCount - 1
               If cmbSectorTypes109.ItemData(i) = effect Then
                    cmbSectorTypes109.ListIndex = i
                    MatchFound = True
                    Exit For
               End If
          Next i
          
          If Not MatchFound Then cmbSectorTypes109.ListIndex = -1
     
     End If
End Sub
