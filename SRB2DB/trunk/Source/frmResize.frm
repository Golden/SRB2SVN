VERSION 5.00
Begin VB.Form frmResize 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Resize Selection"
   ClientHeight    =   2235
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   2775
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   HasDC           =   0   'False
   Icon            =   "frmResize.frx":0000
   KeyPreview      =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   149
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   185
   ShowInTaskbar   =   0   'False
   Begin VB.Frame Frame1 
      Caption         =   "Percentages"
      Height          =   1575
      Left            =   120
      TabIndex        =   2
      Top             =   120
      Width           =   2535
      Begin VB.CheckBox chkAspectCorrect 
         Caption         =   "Aspect-correct"
         Height          =   255
         Left            =   330
         TabIndex        =   7
         Top             =   1200
         Value           =   1  'Checked
         Width           =   2055
      End
      Begin DoomBuilder.ctlValueBox txtScaleH 
         Height          =   375
         Left            =   1245
         TabIndex        =   3
         Top             =   240
         Width           =   1095
         _ExtentX        =   1931
         _ExtentY        =   661
         Max             =   32760
         MaxLength       =   4
         Min             =   -32760
      End
      Begin DoomBuilder.ctlValueBox txtScaleV 
         Height          =   375
         Left            =   1245
         TabIndex        =   5
         Top             =   720
         Width           =   1095
         _ExtentX        =   1931
         _ExtentY        =   661
         Max             =   32760
         MaxLength       =   4
         Min             =   -32760
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         Caption         =   "Vertical:"
         Height          =   195
         Left            =   330
         TabIndex        =   6
         Top             =   795
         Width           =   585
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "Horizontal:"
         Height          =   195
         Left            =   330
         TabIndex        =   4
         Top             =   315
         Width           =   780
      End
   End
   Begin VB.CommandButton cmdCancel 
      Cancel          =   -1  'True
      Caption         =   "Cancel"
      Height          =   345
      Left            =   1500
      TabIndex        =   1
      Top             =   1800
      Width           =   1155
   End
   Begin VB.CommandButton cmdOK 
      Caption         =   "OK"
      Default         =   -1  'True
      Height          =   345
      Left            =   120
      TabIndex        =   0
      Top             =   1800
      Width           =   1155
   End
End
Attribute VB_Name = "frmResize"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'
'    Doom Builder
'    Copyright (c) 2003 Pascal vd Heiden, www.codeimp.com
'    This program is released under GNU General Public License
'
'    This program is distributed in the hope that it will be useful,
'    but WITHOUT ANY WARRANTY; without even the implied warranty of
'    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
'    GNU General Public License for more details.
'


Option Explicit


Private Sub cmdCancel_Click()
     
     'Perform undo
     PerformUndo True
     
     'Remove redo (as if nothing happend)
     WithdrawRedo
     
     'Close
     Unload Me
End Sub

Private Sub cmdOK_Click()
     
     'When used during pasting, remove the undo
     If (submode = ESM_PASTING) Then WithdrawUndo
     
     'Round vertices
     RoundVertices vertexes(0), numvertexes
     
     'Map changed
     mapchanged = True
     If (mode <> EM_THINGS) Then mapnodeschanged = True
     
     'Close
     Unload Me
End Sub


Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
     
     'Adjust shift mask
     CurrentShiftMask = Shift
End Sub

Private Sub Form_KeyUp(KeyCode As Integer, Shift As Integer)
     
     'Adjust shift mask
     CurrentShiftMask = Shift
End Sub


Private Sub Form_Load()
     
     'Move to left top of parent
     left = frmMain.left + 50 * Screen.TwipsPerPixelX
     top = frmMain.top + 100 * Screen.TwipsPerPixelY
     txtScaleH.Text = 100
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
     
     'Check if cancelling
     If (UnloadMode = 0) Then cmdCancel_Click
End Sub


Private Sub txtScaleH_Change()
     
     'Check if we have a value to scale by
     If (Trim$(txtScaleH.Text) <> "") Then
          If chkAspectCorrect.Value = vbChecked And Val(txtScaleH.Text) <> Val(txtScaleV.Text) Then
               ' Update vertical and let *it* trigger the scaling.
               txtScaleV.Text = Val(txtScaleH.Text)
          Else
               DoScaling
          End If
     End If
End Sub

Private Sub txtScaleH_GotFocus()
     SelectAllText txtScaleH
End Sub

Private Sub DoScaling()

     'Perform undo
     PerformUndo True
     
     'Remove redo (as if nothing happend)
     WithdrawRedo
     
     'Make undo
     CreateUndo "resize"
     
     'Rotate with the change difference
     If (mode = EM_THINGS) Then
          ScaleThings Val(txtScaleH.Value), Val(txtScaleV.Value)
     Else
          ScaleVertices Val(txtScaleH.Value), Val(txtScaleV.Value)
     End If
     
     'Redraw map
     RedrawMap

End Sub

Private Sub txtScaleV_Change()

     'Check if we have a value to scale by
     If (Trim$(txtScaleV.Text) <> "") Then
          If chkAspectCorrect.Value = vbChecked And Val(txtScaleV.Text) <> Val(txtScaleH.Text) Then
               ' Update horizontal and let *it* trigger the scaling.
               txtScaleH.Text = Val(txtScaleV.Text)
          Else
               DoScaling
          End If
     End If
End Sub
