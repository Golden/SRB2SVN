typedef struct
{
	u32 score; // Player' score.
	s32 x;
   s32 y;
   s32 radius;
   bool exists;
   bool alive;
   byte arrows[2];
}player_t;

void PlayerThink(void);
void MovePlayer(void);


