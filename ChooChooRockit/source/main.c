// Includes
#include <PA9.h>

// Program Includes
#include "main.h"
#include "player.h"
#include "mobj.h"
#include "defines.h"

extern const unsigned char ship_Sprite[];
extern u16 graffix[];

void AimingThinker(void);
void ConsoleClear(void);
int TouchButtonCheck(type_t type);

mobj_t mobjlist[MAXMOBJS];
player_t player;
byte curslot = 0;
byte objects = 0;
u16 counter = 0;

int main(int argc, char ** argv)
{
	PA_Init();
	PA_InitVBL();
	PA_InitText(1,0); // On the top screen
	PA_InitSound(); // Init sound
	
//	PA_PlaySound(7, bgmusic, (u32)bgmusic_size, 90, 16000); // Start BG music
 	InitPlayer(); // Init the player
	InitGFX();
	InitPalettes();
	InitBackgrounds();

	SpawnMobj(140*UNIT, 70*UNIT, ANGDOWN, 16*UNIT, MT_TRAIN);
	SpawnMobj(204*UNIT, 22*UNIT, 0, 16*UNIT, MT_BLUEROCKIT);
	SpawnMobj(92*UNIT, 134*UNIT, 0, 16*UNIT, MT_VERTIWALL);
	SpawnMobj(92*UNIT, 86*UNIT, 0, 16*UNIT, MT_HORIZWALL);
	SpawnMobj(92*UNIT, 102*UNIT, 0, 16*UNIT, MT_VERTIWALL);

	while(1)
	{
	   u8 i;
	   static int songloop = 0;
	   
	   if(songloop >= 92*TICRATE)
	   {
	//   	PA_PlaySound(7, bgmusic, (u32)bgmusic_size, 90, 16000); // Start BG music
	   	songloop = 0;
	 	}  
			
	   PlayerThink();
		MovePlayer();	
		DebugInfo();
	 	
		for(i = 0; i < MAXMOBJS; i++)
		{		
			if(!mobjlist[i].alive)
				continue;
			
			MoveMobj(&mobjlist[i]);	
			CheckCollisions(&mobjlist[i]);	
			
			if(mobjlist[i].fuse > 0)
				mobjlist[i].fuse--;
				
			if(mobjlist[i].fuse == 0 && mobjlist[i].alive) // Kill mobjs when their fuse is gone.
				KillMobj(&mobjlist[i]);  
		}				
			
		songloop++;
		DrawScore();
		PA_WaitForVBL();
		counter += 1;
		ConsoleClear();
	}
	return 0;
}
void ConsoleClear(void)
{
	byte i;
	for(i=0;i<15;i++)
		PA_OutputText(1, 5, i, "                             ");   
}
void SlowConsoleClear(void)
{
	byte i;
	for(i=16;i<24;i++)
		PA_OutputText(1, 5, i, "                             ");   
}
void DebugInfo(void)
{

   mobj_t* mobj = &mobjlist[4];
	PA_OutputText(1, 5, 2, "x: %d ", (mobj->x>>8));
	PA_OutputText(1, 5, 3, "y: %d ", (mobj->y>>8));
	PA_OutputText(1, 5, 4, "spritenum: %d ", mobj->spritenum);
	PA_OutputText(1, 5, 5, "spritesizex: %d ", mobj->spritesizex);
	PA_OutputText(1, 5, 6, "spritesizey: %d ", mobj->spritesizey);
	
	PA_OutputText(1, 5, 10, "mobj->leftwall: %d ", mobj->leftwall);
	
/*
	PA_OutputText(1, 5, 13, "x: %d ", (player.x>>8));
	PA_OutputText(1, 5, 14, "y: %d ", (player.y>>8));	
	PA_OutputText(1, 5, 16, "Objects: %d", objects);
	PA_OutputText(1, 5, 17, "curslot: %d", curslot);
	PA_OutputText(1, 5, 18, "Arrow 0: %d", player.arrows[0]);
	PA_OutputText(1, 5, 19, "Arrow 1: %d", player.arrows[1]);
	PA_OutputText(1, 5, 20, "Arrow 2: %d", player.arrows[2]);
	PA_OutputText(1, 5, 21, "Score: %d", player.score);
	PA_OutputText(1, 5, 22, "Seconds: %d", counter/TICRATE);
*/
}

int TouchButtonCheck(type_t type)
{
   u16 i;
   bool foundmobj = false;
 	mobj_t* mobj;
	   
   for(i=0;i<MAXMOBJS;i++)
   {
      if(!mobjlist[i].alive)
      	continue;
      if(mobjlist[i].type != type)
      	continue;
      	
      mobj = &mobjlist[i];
      
		if(Stylus.X*UNIT < mobj->x+mobj->spritesizex/2*UNIT && Stylus.X*UNIT > mobj->x-mobj->spritesizex/2*UNIT
			&& Stylus.Y*UNIT < mobj->y+mobj->spritesizey/2*UNIT && Stylus.Y*UNIT > mobj->y-mobj->spritesizey/2*UNIT
			&& Stylus.Newpress)
			 foundmobj = true;
	}
	return foundmobj;	 	   	    
}
// I don't want these in this file, but they NEED to be here. :|
// Initialize the player.
void InitPlayer(void)
{
   if(!player.exists)
		PA_CreateSpriteFromGfx(0, // Screen
										127, // Sprite number..
										graffix[CROSSHAIR], // Image
										OBJ_SIZE_16X16, 1, CROSSHAIR, -255, -255);
		else
			PA_SetSpriteGfx(0, 127, graffix[CROSSHAIR]);
	
	player.x = 140*UNIT; 
	player.y = 70*UNIT;
	player.radius = 16*UNIT;
	player.score = 0; // Player's score. 
	player.alive = true;
}

void KillPlayer(void)
{
   player.x = player.y = -255;
   PA_SetSpriteXY(0, 127, -255, -255);
//   PA_PlaySimpleSound(5, explode);
	player.alive = false;
}
void DrawScore(void)
{ 
/*   char* buf = NULL;
   sprintf(buf, "%i", player.score);
   
	PA_CenterSmartText(0, 5, 17, 37, 35, buf, 1, 4, 0);
	PA_CenterSmartText(0, 5, 54, 37, 70, "222", 1, 4, 0);
	PA_CenterSmartText(0, 5, 89, 37, 108, "333", 1, 4, 0);
	PA_CenterSmartText(0, 5, 128, 37, 141, "444", 1, 4, 0);
*/
}
