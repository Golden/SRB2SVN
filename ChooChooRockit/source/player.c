#include <PA9.h>
#include <math.h>
#include "player.h"
#include "defines.h"
#include "mobj.h"

extern player_t player;
extern mobj_t mobjlist[];

// Misc player stuff goes here, such as animations, cheat codes, and actions.
void PlayerThink(void)
{
   mobj_t* mobj = NULL;
   if(!player.alive)
   	return;
   
   if(Pad.Newpress.A)
   	mobj = SpawnMobj(player.x, player.y, ANGRIGHT, 16*UNIT, MT_RIGHTARROW);
   else if(Pad.Newpress.B)
   	mobj = SpawnMobj(player.x, player.y, ANGDOWN, 16*UNIT, MT_DOWNARROW);
   else if(Pad.Newpress.X)
   	mobj = SpawnMobj(player.x, player.y, ANGUP, 16*UNIT, MT_UPARROW);
   else if(Pad.Newpress.Y)
   	mobj = SpawnMobj(player.x, player.y, ANGLEFT, 16*UNIT, MT_LEFTARROW);
   
	if(mobj)
	{	
	   // Delete first of three arrows
	   if(player.arrows[2])
	   {
			KillMobj(&mobjlist[player.arrows[0]]);	
			player.arrows[0] = player.arrows[1];
			player.arrows[1] = player.arrows[2];
			player.arrows[2] = 0;
		}
		
	   if(!player.arrows[0])
	   	player.arrows[0] = mobj->spritenum;
	   else if(!player.arrows[1])
	  		player.arrows[1] = mobj->spritenum;
		else if(!player.arrows[2])
	   	player.arrows[2] = mobj->spritenum;	
	}			
   
}

// MovePlayer handles player movement. NOTHING ELSE! I don't want it to look like SRB2's P_MovePlayer
void MovePlayer(void)
{   
   if(!player.alive)
   	return;
		
		if(Pad.Newpress.Up)
		{
			player.y -= 16*UNIT;
			if(player.y < BORDERTOP)
				player.y += 16*UNIT;
		}			
		else if(Pad.Newpress.Down)
		{
			player.y += 16*UNIT;
			if(player.y >= BORDERBOTTOM)
				player.y -= 16*UNIT;
		}			
		if(Pad.Newpress.Left)
		{
			player.x -= 16*UNIT;
			if(player.x < BORDERLEFT)
				player.x += 16*UNIT;
		}				
		else if(Pad.Newpress.Right)
		{
			player.x += 16*UNIT;
			if(player.x >= BORDERRIGHT)
				player.x -= 16*UNIT;
		}			
		
		PA_SetSpriteXY(0, 127, (player.x>>8), (player.y>>8)); // Sprite position converted to normal...
}
