#define MAXMOVE 25
#define UNIT 256
// Screen borders
#define BORDERLEFT 44*UNIT 
#define BORDERRIGHT 252*UNIT
#define BORDERTOP 6*UNIT
#define BORDERBOTTOM 150*UNIT
#define MAXMOBJS 126
#define TICRATE 60

// GFX Numbers
#define CROSSHAIR 0
#define CHOOCHOO 1
#define CATPOO 2
#define ROCKIT 3
#define UPARROW 4
#define DOWNARROW 5
#define LEFTARROW 6
#define RIGHTARROW 7
#define TRAIN 8
#define HORIZWALL 9
#define VERTIWALL 10

// Angles
#define ANGUP 0
#define ANGDOWN 1
#define ANGLEFT 2
#define ANGRIGHT 3
