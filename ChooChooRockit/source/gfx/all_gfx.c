//Gfx converted using Mollusk's PAGfx Converter

//This file contains all the .c, for easier inclusion in a project

#ifdef __cplusplus
extern "C" {
#endif

#include "all_gfx.h"


// Sprite files : 
#include "uparrow.c"
#include "catpoo.c"
#include "choochoo.c"
#include "crosshair.c"
#include "downarrow.c"
#include "leftarrow.c"
#include "rightarrow.c"
#include "rockit.c"
#include "train.c"
#include "horizontalwall.c"
#include "verticalwall.c"

// Background files : 
#include "background.c"

// Palette files : 
#include "uparrow.pal.c"
#include "catpoo.pal.c"
#include "choochoo.pal.c"
#include "crosshair.pal.c"
#include "downarrow.pal.c"
#include "leftarrow.pal.c"
#include "rightarrow.pal.c"
#include "rockit.pal.c"
#include "train.pal.c"
#include "horizontalwall.pal.c"
#include "verticalwall.pal.c"
#include "background.pal.c"

// Background Pointers :
PAGfx_struct background = {(void*)background_Map, 768, (void*)background_Tiles, 5696, (void*)background_Pal, (int*)background_Info };


#ifdef __cplusplus
}
#endif

