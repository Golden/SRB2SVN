//Palette created using Mollusk's PAGfxConverter

const unsigned short background_Pal[26] __attribute__ ((aligned (4))) = {
64543, 32768, 59815, 64512, 63154, 64377, 65337, 65535, 63028, 54111, 40794, 52157, 59358, 41210, 51805, 59198, 40780, 52149, 59355, 65145, 64950, 64593, 56430, 44604, 51966, 58239};
