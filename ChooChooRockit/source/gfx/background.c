//Background converted using Mollusk's PAGfx Converter
//This Background uses background_Pal

const int background_Info[3] = {2, 256, 192}; // BgMode, Width, Height

const unsigned short background_Map[768] __attribute__ ((aligned (4))) = {
0, 1, 1, 1, 2, 3, 4, 5, 6, 1029, 4, 5, 6, 1029, 4, 5, 
6, 1029, 4, 5, 6, 1029, 4, 5, 6, 1029, 4, 5, 6, 1029, 4, 1027, 
7, 8, 8, 8, 9, 10, 11, 12, 13, 1036, 11, 12, 13, 1036, 11, 12, 
13, 1036, 11, 12, 13, 1036, 11, 12, 13, 1036, 11, 12, 13, 1036, 11, 1034, 
14, 15, 15, 15, 16, 17, 18, 19, 20, 1043, 18, 19, 20, 1043, 18, 19, 
20, 1043, 18, 19, 20, 1043, 18, 19, 20, 1043, 18, 19, 20, 1043, 18, 1041, 
21, 22, 22, 22, 23, 24, 13, 1036, 11, 12, 13, 1036, 11, 12, 13, 1036, 
11, 12, 13, 1036, 11, 12, 13, 1036, 11, 12, 13, 1036, 11, 12, 13, 1048, 
25, 26, 26, 26, 27, 28, 20, 1043, 18, 19, 20, 1043, 18, 19, 20, 1043, 
18, 19, 20, 1043, 18, 19, 20, 1043, 18, 19, 20, 1043, 18, 19, 20, 1052, 
29, 30, 30, 30, 31, 10, 11, 12, 13, 1036, 11, 12, 13, 1036, 11, 12, 
13, 1036, 11, 12, 13, 1036, 11, 12, 13, 1036, 11, 12, 13, 1036, 11, 1034, 
32, 33, 33, 33, 34, 17, 18, 19, 20, 1043, 18, 19, 20, 1043, 18, 19, 
20, 1043, 18, 19, 20, 1043, 18, 19, 20, 1043, 18, 19, 20, 1043, 18, 1041, 
35, 22, 22, 22, 36, 24, 13, 1036, 11, 12, 13, 1036, 11, 12, 13, 1036, 
11, 12, 13, 1036, 11, 12, 13, 1036, 11, 12, 13, 1036, 11, 12, 13, 1048, 
37, 38, 38, 38, 39, 28, 20, 1043, 18, 19, 20, 1043, 18, 19, 20, 1043, 
18, 19, 20, 1043, 18, 19, 20, 1043, 18, 19, 20, 1043, 18, 19, 20, 1052, 
40, 41, 41, 41, 42, 10, 11, 12, 13, 1036, 11, 12, 13, 1036, 11, 12, 
13, 1036, 11, 12, 13, 1036, 11, 12, 13, 1036, 11, 12, 13, 1036, 11, 1034, 
43, 8, 8, 8, 44, 17, 18, 19, 20, 1043, 18, 19, 20, 1043, 18, 19, 
20, 1043, 18, 19, 20, 1043, 18, 19, 20, 1043, 18, 19, 20, 1043, 18, 1041, 
45, 15, 15, 15, 46, 24, 13, 1036, 11, 12, 13, 1036, 11, 12, 13, 1036, 
11, 12, 13, 1036, 11, 12, 13, 1036, 11, 12, 13, 1036, 11, 12, 13, 1048, 
47, 22, 22, 22, 48, 28, 20, 1043, 18, 19, 20, 1043, 18, 19, 20, 1043, 
18, 19, 20, 1043, 18, 19, 20, 1043, 18, 19, 20, 1043, 18, 19, 20, 1052, 
49, 50, 50, 50, 51, 10, 11, 12, 13, 1036, 11, 12, 13, 1036, 11, 12, 
13, 1036, 11, 12, 13, 1036, 11, 12, 13, 1036, 11, 12, 13, 1036, 11, 1034, 
52, 53, 53, 53, 54, 17, 18, 19, 20, 1043, 18, 19, 20, 1043, 18, 19, 
20, 1043, 18, 19, 20, 1043, 18, 19, 20, 1043, 18, 19, 20, 1043, 18, 1041, 
55, 33, 33, 33, 56, 24, 13, 1036, 11, 12, 13, 1036, 11, 12, 13, 1036, 
11, 12, 13, 1036, 11, 12, 13, 1036, 11, 12, 13, 1036, 11, 12, 13, 1048, 
57, 22, 22, 22, 58, 28, 20, 1043, 18, 19, 20, 1043, 18, 19, 20, 1043, 
18, 19, 20, 1043, 18, 19, 20, 1043, 18, 19, 20, 1043, 18, 19, 20, 1052, 
59, 60, 60, 60, 61, 10, 11, 12, 13, 1036, 11, 12, 13, 1036, 11, 12, 
13, 1036, 11, 12, 13, 1036, 11, 12, 13, 1036, 11, 12, 13, 1036, 11, 1034, 
62, 63, 63, 63, 64, 65, 66, 67, 68, 1091, 66, 67, 68, 1091, 66, 67, 
68, 1091, 66, 67, 68, 1091, 66, 67, 68, 1091, 66, 67, 68, 1091, 66, 1089, 
69, 70, 70, 71, 72, 73, 74, 74, 74, 74, 74, 74, 74, 74, 74, 74, 
74, 74, 74, 74, 74, 74, 74, 74, 74, 74, 74, 74, 74, 74, 74, 75, 
76, 22, 22, 22, 77, 78, 78, 78, 78, 78, 78, 78, 78, 78, 78, 78, 
78, 78, 78, 78, 78, 78, 78, 78, 78, 78, 78, 78, 78, 78, 78, 79, 
80, 22, 22, 81, 77, 78, 78, 78, 78, 78, 78, 78, 78, 78, 78, 78, 
78, 78, 78, 78, 78, 78, 78, 78, 78, 78, 78, 78, 78, 78, 78, 79, 
82, 83, 83, 84, 85, 86, 86, 86, 86, 86, 86, 86, 86, 86, 86, 86, 
86, 86, 86, 86, 86, 86, 86, 86, 86, 86, 86, 86, 86, 86, 86, 87, 
88, 88, 88, 88, 88, 88, 88, 88, 88, 88, 88, 88, 88, 88, 88, 88, 
88, 88, 88, 88, 88, 88, 88, 88, 88, 88, 88, 88, 88, 88, 88, 88
};

const unsigned char background_Tiles[5696] __attribute__ ((aligned (4))) = {
1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 
1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 2, 2, 2, 2, 
1, 1, 2, 2, 4, 4, 4, 4, 1, 1, 2, 4, 4, 5, 5, 5, 
1, 1, 2, 4, 5, 5, 7, 7, 1, 1, 2, 4, 5, 7, 7, 7, 
1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 
1, 1, 1, 1, 1, 1, 1, 1, 2, 2, 2, 2, 2, 2, 2, 2, 
4, 4, 4, 4, 4, 4, 4, 4, 5, 5, 5, 5, 5, 5, 5, 5, 
7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 
1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 
1, 1, 1, 1, 1, 1, 1, 1, 2, 2, 2, 2, 2, 2, 2, 1, 
4, 4, 4, 4, 4, 4, 2, 2, 5, 5, 5, 5, 5, 4, 4, 2, 
7, 7, 7, 7, 5, 5, 4, 2, 7, 7, 7, 7, 7, 5, 4, 2, 
1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 
1, 1, 1, 1, 1, 1, 1, 1, 1, 3, 3, 3, 3, 3, 3, 3, 
1, 3, 3, 3, 3, 3, 3, 3, 1, 3, 3, 6, 6, 6, 6, 6, 
1, 3, 3, 6, 8, 8, 8, 8, 1, 3, 3, 6, 8, 8, 8, 8, 
1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 
1, 1, 1, 1, 1, 1, 1, 1, 3, 3, 3, 3, 3, 3, 3, 3, 
3, 3, 3, 3, 3, 3, 3, 3, 6, 6, 6, 6, 6, 6, 6, 6, 
8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 
1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 
1, 1, 1, 1, 1, 1, 1, 1, 3, 3, 3, 3, 3, 3, 3, 3, 
3, 3, 3, 3, 3, 3, 3, 3, 6, 6, 6, 6, 6, 6, 6, 6, 
8, 8, 8, 8, 9, 9, 9, 9, 8, 8, 8, 8, 9, 9, 9, 9, 
1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 
1, 1, 1, 1, 1, 1, 1, 1, 3, 3, 3, 3, 3, 3, 3, 3, 
3, 3, 3, 3, 3, 3, 3, 3, 6, 6, 6, 6, 6, 6, 6, 6, 
9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 
1, 1, 2, 4, 5, 7, 7, 7, 1, 1, 2, 4, 5, 7, 7, 7, 
1, 1, 2, 4, 5, 7, 7, 7, 1, 1, 2, 4, 5, 7, 7, 7, 
1, 1, 2, 4, 5, 7, 7, 7, 1, 1, 2, 4, 5, 7, 7, 7, 
1, 1, 2, 4, 5, 7, 7, 7, 1, 1, 2, 4, 5, 7, 7, 7, 
7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 
7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 
7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 
7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 
7, 7, 7, 7, 7, 5, 4, 2, 7, 7, 7, 7, 7, 5, 4, 2, 
7, 7, 7, 7, 7, 5, 4, 2, 7, 7, 7, 7, 7, 5, 4, 2, 
7, 7, 7, 7, 7, 5, 4, 2, 7, 7, 7, 7, 7, 5, 4, 2, 
7, 7, 7, 7, 7, 5, 4, 2, 7, 7, 7, 7, 7, 5, 4, 2, 
1, 3, 3, 6, 8, 8, 8, 8, 1, 3, 3, 6, 8, 8, 8, 8, 
1, 3, 3, 6, 8, 8, 8, 8, 1, 3, 3, 6, 8, 8, 8, 8, 
1, 3, 3, 6, 8, 8, 8, 8, 1, 3, 3, 6, 8, 8, 8, 8, 
1, 3, 3, 6, 8, 8, 8, 8, 1, 3, 3, 6, 8, 8, 8, 8, 
8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 
8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 
8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 
8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 
8, 8, 8, 8, 9, 9, 9, 9, 8, 8, 8, 8, 9, 9, 9, 9, 
8, 8, 8, 8, 9, 9, 9, 9, 8, 8, 8, 8, 9, 9, 9, 9, 
8, 8, 8, 8, 9, 9, 9, 9, 8, 8, 8, 8, 9, 9, 9, 9, 
8, 8, 8, 8, 9, 9, 9, 9, 8, 8, 8, 8, 9, 9, 9, 9, 
9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 
9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 
9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 
9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 
1, 1, 2, 4, 5, 7, 7, 7, 1, 1, 2, 4, 5, 1, 1, 1, 
1, 1, 2, 4, 5, 1, 1, 1, 1, 1, 2, 4, 5, 1, 1, 1, 
1, 1, 2, 4, 5, 1, 1, 1, 1, 1, 2, 4, 5, 1, 1, 1, 
1, 1, 2, 4, 5, 1, 1, 1, 1, 1, 2, 4, 5, 1, 1, 1, 
7, 7, 7, 7, 7, 7, 7, 7, 1, 1, 1, 1, 1, 1, 1, 1, 
1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 
1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 
1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 
7, 7, 7, 7, 7, 5, 4, 2, 1, 1, 1, 1, 1, 5, 4, 2, 
1, 1, 1, 1, 1, 5, 4, 2, 1, 1, 1, 1, 1, 5, 4, 2, 
1, 1, 1, 1, 1, 5, 4, 2, 1, 1, 1, 1, 1, 5, 4, 2, 
1, 1, 1, 1, 1, 5, 4, 2, 1, 1, 1, 1, 1, 5, 4, 2, 
1, 3, 3, 6, 8, 8, 8, 8, 1, 3, 3, 6, 8, 8, 8, 8, 
1, 3, 3, 6, 8, 8, 8, 8, 1, 3, 3, 6, 8, 8, 8, 8, 
1, 3, 3, 6, 8, 8, 8, 8, 1, 3, 3, 6, 8, 8, 8, 8, 
1, 3, 3, 6, 9, 9, 9, 9, 1, 3, 3, 6, 9, 9, 9, 9, 
8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 
8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 
8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 
9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 
8, 8, 8, 8, 9, 9, 9, 9, 8, 8, 8, 8, 9, 9, 9, 9, 
8, 8, 8, 8, 9, 9, 9, 9, 8, 8, 8, 8, 9, 9, 9, 9, 
8, 8, 8, 8, 9, 9, 9, 9, 8, 8, 8, 8, 9, 9, 9, 9, 
9, 9, 9, 9, 8, 8, 8, 8, 9, 9, 9, 9, 8, 8, 8, 8, 
9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 
9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 
9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 
8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 
1, 1, 2, 4, 5, 1, 1, 1, 1, 1, 2, 4, 5, 1, 1, 1, 
1, 1, 2, 4, 5, 1, 1, 1, 1, 1, 2, 4, 5, 1, 1, 1, 
1, 1, 2, 4, 5, 1, 1, 1, 1, 1, 2, 4, 5, 1, 1, 1, 
1, 1, 2, 4, 5, 1, 1, 1, 1, 1, 2, 4, 5, 1, 1, 1, 
1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 
1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 
1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 
1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 
1, 1, 1, 1, 1, 5, 4, 2, 1, 1, 1, 1, 1, 5, 4, 2, 
1, 1, 1, 1, 1, 5, 4, 2, 1, 1, 1, 1, 1, 5, 4, 2, 
1, 1, 1, 1, 1, 5, 4, 2, 1, 1, 1, 1, 1, 5, 4, 2, 
1, 1, 1, 1, 1, 5, 4, 2, 1, 1, 1, 1, 1, 5, 4, 2, 
1, 3, 3, 6, 9, 9, 9, 9, 1, 3, 3, 6, 9, 9, 9, 9, 
1, 3, 3, 6, 9, 9, 9, 9, 1, 3, 3, 6, 9, 9, 9, 9, 
1, 3, 3, 6, 9, 9, 9, 9, 1, 3, 3, 6, 9, 9, 9, 9, 
1, 3, 3, 6, 9, 9, 9, 9, 1, 3, 3, 6, 9, 9, 9, 9, 
1, 1, 2, 4, 5, 1, 1, 1, 1, 1, 2, 4, 5, 5, 1, 1, 
1, 1, 2, 4, 4, 5, 5, 5, 1, 1, 1, 2, 2, 2, 2, 2, 
1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 
1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 10, 10, 10, 10, 10, 
1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 
5, 5, 5, 5, 5, 5, 5, 5, 2, 2, 2, 2, 2, 2, 2, 2, 
1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 
1, 1, 1, 1, 1, 1, 1, 1, 10, 10, 10, 10, 10, 10, 10, 10, 
1, 1, 1, 1, 1, 5, 4, 2, 1, 1, 1, 1, 5, 5, 4, 2, 
5, 5, 5, 5, 5, 4, 4, 2, 2, 2, 2, 2, 2, 2, 2, 1, 
1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 
1, 1, 1, 1, 1, 1, 1, 1, 10, 10, 10, 10, 10, 10, 10, 1, 
1, 3, 3, 6, 9, 9, 9, 9, 1, 3, 3, 6, 9, 9, 9, 9, 
1, 3, 3, 6, 9, 9, 9, 9, 1, 3, 3, 6, 9, 9, 9, 9, 
1, 3, 3, 6, 9, 9, 9, 9, 1, 3, 3, 6, 9, 9, 9, 9, 
1, 3, 3, 6, 8, 8, 8, 8, 1, 3, 3, 6, 8, 8, 8, 8, 
1, 1, 10, 10, 11, 11, 11, 11, 1, 1, 10, 11, 11, 12, 12, 12, 
1, 1, 10, 11, 12, 12, 7, 7, 1, 1, 10, 11, 12, 7, 7, 7, 
1, 1, 10, 11, 12, 7, 7, 7, 1, 1, 10, 11, 12, 7, 7, 7, 
1, 1, 10, 11, 12, 7, 7, 7, 1, 1, 10, 11, 12, 7, 7, 7, 
11, 11, 11, 11, 11, 11, 11, 11, 12, 12, 12, 12, 12, 12, 12, 12, 
7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 
7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 
7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 
11, 11, 11, 11, 11, 11, 10, 10, 12, 12, 12, 12, 12, 11, 11, 10, 
7, 7, 7, 7, 12, 12, 11, 10, 7, 7, 7, 7, 7, 12, 11, 10, 
7, 7, 7, 7, 7, 12, 11, 10, 7, 7, 7, 7, 7, 12, 11, 10, 
7, 7, 7, 7, 7, 12, 11, 10, 7, 7, 7, 7, 7, 12, 11, 10, 
1, 1, 10, 11, 12, 7, 7, 7, 1, 1, 10, 11, 12, 7, 7, 7, 
1, 1, 10, 11, 12, 7, 7, 7, 1, 1, 10, 11, 12, 7, 7, 7, 
1, 1, 10, 11, 12, 7, 7, 7, 1, 1, 10, 11, 12, 1, 1, 1, 
1, 1, 10, 11, 12, 1, 1, 1, 1, 1, 10, 11, 12, 1, 1, 1, 
7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 
7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 
7, 7, 7, 7, 7, 7, 7, 7, 1, 1, 1, 1, 1, 1, 1, 1, 
1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 
7, 7, 7, 7, 7, 12, 11, 10, 7, 7, 7, 7, 7, 12, 11, 10, 
7, 7, 7, 7, 7, 12, 11, 10, 7, 7, 7, 7, 7, 12, 11, 10, 
7, 7, 7, 7, 7, 12, 11, 10, 1, 1, 1, 1, 1, 12, 11, 10, 
1, 1, 1, 1, 1, 12, 11, 10, 1, 1, 1, 1, 1, 12, 11, 10, 
1, 1, 10, 11, 12, 1, 1, 1, 1, 1, 10, 11, 12, 1, 1, 1, 
1, 1, 10, 11, 12, 1, 1, 1, 1, 1, 10, 11, 12, 1, 1, 1, 
1, 1, 10, 11, 12, 1, 1, 1, 1, 1, 10, 11, 12, 1, 1, 1, 
1, 1, 10, 11, 12, 1, 1, 1, 1, 1, 10, 11, 12, 1, 1, 1, 
1, 1, 1, 1, 1, 12, 11, 10, 1, 1, 1, 1, 1, 12, 11, 10, 
1, 1, 1, 1, 1, 12, 11, 10, 1, 1, 1, 1, 1, 12, 11, 10, 
1, 1, 1, 1, 1, 12, 11, 10, 1, 1, 1, 1, 1, 12, 11, 10, 
1, 1, 1, 1, 1, 12, 11, 10, 1, 1, 1, 1, 1, 12, 11, 10, 
1, 1, 10, 11, 12, 1, 1, 1, 1, 1, 10, 11, 12, 1, 1, 1, 
1, 1, 10, 11, 12, 1, 1, 1, 1, 1, 10, 11, 12, 1, 1, 1, 
1, 1, 10, 11, 12, 1, 1, 1, 1, 1, 10, 11, 12, 12, 1, 1, 
1, 1, 10, 11, 11, 12, 12, 12, 1, 1, 1, 10, 10, 10, 10, 10, 
1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 
1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 
1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 
12, 12, 12, 12, 12, 12, 12, 12, 10, 10, 10, 10, 10, 10, 10, 10, 
1, 1, 1, 1, 1, 12, 11, 10, 1, 1, 1, 1, 1, 12, 11, 10, 
1, 1, 1, 1, 1, 12, 11, 10, 1, 1, 1, 1, 1, 12, 11, 10, 
1, 1, 1, 1, 1, 12, 11, 10, 1, 1, 1, 1, 12, 12, 11, 10, 
12, 12, 12, 12, 12, 11, 11, 10, 10, 10, 10, 10, 10, 10, 10, 1, 
1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 
1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 13, 13, 13, 13, 13, 
1, 1, 13, 13, 14, 14, 14, 14, 1, 1, 13, 14, 14, 15, 15, 15, 
1, 1, 13, 14, 15, 15, 7, 7, 1, 1, 13, 14, 15, 7, 7, 7, 
1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 
1, 1, 1, 1, 1, 1, 1, 1, 13, 13, 13, 13, 13, 13, 13, 13, 
14, 14, 14, 14, 14, 14, 14, 14, 15, 15, 15, 15, 15, 15, 15, 15, 
7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 
1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 
1, 1, 1, 1, 1, 1, 1, 1, 13, 13, 13, 13, 13, 13, 13, 1, 
14, 14, 14, 14, 14, 14, 13, 13, 15, 15, 15, 15, 15, 14, 14, 13, 
7, 7, 7, 7, 15, 15, 14, 13, 7, 7, 7, 7, 7, 15, 14, 13, 
1, 1, 13, 14, 15, 7, 7, 7, 1, 1, 13, 14, 15, 7, 7, 7, 
1, 1, 13, 14, 15, 7, 7, 7, 1, 1, 13, 14, 15, 7, 7, 7, 
1, 1, 13, 14, 15, 7, 7, 7, 1, 1, 13, 14, 15, 7, 7, 7, 
1, 1, 13, 14, 15, 7, 7, 7, 1, 1, 13, 14, 15, 7, 7, 7, 
7, 7, 7, 7, 7, 15, 14, 13, 7, 7, 7, 7, 7, 15, 14, 13, 
7, 7, 7, 7, 7, 15, 14, 13, 7, 7, 7, 7, 7, 15, 14, 13, 
7, 7, 7, 7, 7, 15, 14, 13, 7, 7, 7, 7, 7, 15, 14, 13, 
7, 7, 7, 7, 7, 15, 14, 13, 7, 7, 7, 7, 7, 15, 14, 13, 
1, 1, 13, 14, 15, 7, 7, 7, 1, 1, 13, 14, 15, 1, 1, 1, 
1, 1, 13, 14, 15, 1, 1, 1, 1, 1, 13, 14, 15, 1, 1, 1, 
1, 1, 13, 14, 15, 1, 1, 1, 1, 1, 13, 14, 15, 1, 1, 1, 
1, 1, 13, 14, 15, 1, 1, 1, 1, 1, 13, 14, 15, 1, 1, 1, 
7, 7, 7, 7, 7, 15, 14, 13, 1, 1, 1, 1, 1, 15, 14, 13, 
1, 1, 1, 1, 1, 15, 14, 13, 1, 1, 1, 1, 1, 15, 14, 13, 
1, 1, 1, 1, 1, 15, 14, 13, 1, 1, 1, 1, 1, 15, 14, 13, 
1, 1, 1, 1, 1, 15, 14, 13, 1, 1, 1, 1, 1, 15, 14, 13, 
1, 1, 13, 14, 15, 1, 1, 1, 1, 1, 13, 14, 15, 1, 1, 1, 
1, 1, 13, 14, 15, 1, 1, 1, 1, 1, 13, 14, 15, 1, 1, 1, 
1, 1, 13, 14, 15, 1, 1, 1, 1, 1, 13, 14, 15, 1, 1, 1, 
1, 1, 13, 14, 15, 1, 1, 1, 1, 1, 13, 14, 15, 1, 1, 1, 
1, 1, 1, 1, 1, 15, 14, 13, 1, 1, 1, 1, 1, 15, 14, 13, 
1, 1, 1, 1, 1, 15, 14, 13, 1, 1, 1, 1, 1, 15, 14, 13, 
1, 1, 1, 1, 1, 15, 14, 13, 1, 1, 1, 1, 1, 15, 14, 13, 
1, 1, 1, 1, 1, 15, 14, 13, 1, 1, 1, 1, 1, 15, 14, 13, 
1, 1, 13, 14, 15, 1, 1, 1, 1, 1, 13, 14, 15, 15, 1, 1, 
1, 1, 13, 14, 14, 15, 15, 15, 1, 1, 1, 13, 13, 13, 13, 13, 
1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 
1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 16, 16, 16, 16, 16, 
1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 
15, 15, 15, 15, 15, 15, 15, 15, 13, 13, 13, 13, 13, 13, 13, 13, 
1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 
1, 1, 1, 1, 1, 1, 1, 1, 16, 16, 16, 16, 16, 16, 16, 16, 
1, 1, 1, 1, 1, 15, 14, 13, 1, 1, 1, 1, 15, 15, 14, 13, 
15, 15, 15, 15, 15, 14, 14, 13, 13, 13, 13, 13, 13, 13, 13, 1, 
1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 
1, 1, 1, 1, 1, 1, 1, 1, 16, 16, 16, 16, 16, 16, 16, 1, 
1, 1, 16, 16, 17, 17, 17, 17, 1, 1, 16, 17, 17, 18, 18, 18, 
1, 1, 16, 17, 18, 18, 7, 7, 1, 1, 16, 17, 18, 7, 7, 7, 
1, 1, 16, 17, 18, 7, 7, 7, 1, 1, 16, 17, 18, 7, 7, 7, 
1, 1, 16, 17, 18, 7, 7, 7, 1, 1, 16, 17, 18, 7, 7, 7, 
17, 17, 17, 17, 17, 17, 17, 17, 18, 18, 18, 18, 18, 18, 18, 18, 
7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 
7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 
7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 
17, 17, 17, 17, 17, 17, 16, 16, 18, 18, 18, 18, 18, 17, 17, 16, 
7, 7, 7, 7, 18, 18, 17, 16, 7, 7, 7, 7, 7, 18, 17, 16, 
7, 7, 7, 7, 7, 18, 17, 16, 7, 7, 7, 7, 7, 18, 17, 16, 
7, 7, 7, 7, 7, 18, 17, 16, 7, 7, 7, 7, 7, 18, 17, 16, 
1, 1, 16, 17, 18, 7, 7, 7, 1, 1, 16, 17, 18, 7, 7, 7, 
1, 1, 16, 17, 18, 7, 7, 7, 1, 1, 16, 17, 18, 7, 7, 7, 
1, 1, 16, 17, 18, 7, 7, 7, 1, 1, 16, 17, 18, 1, 1, 1, 
1, 1, 16, 17, 18, 1, 1, 1, 1, 1, 16, 17, 18, 1, 1, 1, 
7, 7, 7, 7, 7, 18, 17, 16, 7, 7, 7, 7, 7, 18, 17, 16, 
7, 7, 7, 7, 7, 18, 17, 16, 7, 7, 7, 7, 7, 18, 17, 16, 
7, 7, 7, 7, 7, 18, 17, 16, 1, 1, 1, 1, 1, 18, 17, 16, 
1, 1, 1, 1, 1, 18, 17, 16, 1, 1, 1, 1, 1, 18, 17, 16, 
1, 1, 16, 17, 18, 1, 1, 1, 1, 1, 16, 17, 18, 1, 1, 1, 
1, 1, 16, 17, 18, 1, 1, 1, 1, 1, 16, 17, 18, 1, 1, 1, 
1, 1, 16, 17, 18, 1, 1, 1, 1, 1, 16, 17, 18, 1, 1, 1, 
1, 1, 16, 17, 18, 1, 1, 1, 1, 1, 16, 17, 18, 1, 1, 1, 
1, 1, 1, 1, 1, 18, 17, 16, 1, 1, 1, 1, 1, 18, 17, 16, 
1, 1, 1, 1, 1, 18, 17, 16, 1, 1, 1, 1, 1, 18, 17, 16, 
1, 1, 1, 1, 1, 18, 17, 16, 1, 1, 1, 1, 1, 18, 17, 16, 
1, 1, 1, 1, 1, 18, 17, 16, 1, 1, 1, 1, 1, 18, 17, 16, 
1, 1, 16, 17, 18, 1, 1, 1, 1, 1, 16, 17, 18, 1, 1, 1, 
1, 1, 16, 17, 18, 1, 1, 1, 1, 1, 16, 17, 18, 1, 1, 1, 
1, 1, 16, 17, 18, 1, 1, 1, 1, 1, 16, 17, 18, 18, 1, 1, 
1, 1, 16, 17, 17, 18, 18, 18, 1, 1, 1, 16, 16, 16, 16, 16, 
1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 
1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 
1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 
18, 18, 18, 18, 18, 18, 18, 18, 16, 16, 16, 16, 16, 16, 16, 16, 
1, 1, 1, 1, 1, 18, 17, 16, 1, 1, 1, 1, 1, 18, 17, 16, 
1, 1, 1, 1, 1, 18, 17, 16, 1, 1, 1, 1, 1, 18, 17, 16, 
1, 1, 1, 1, 1, 18, 17, 16, 1, 1, 1, 1, 18, 18, 17, 16, 
18, 18, 18, 18, 18, 17, 17, 16, 16, 16, 16, 16, 16, 16, 16, 1, 
1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 
1, 1, 1, 1, 1, 1, 1, 1, 19, 19, 19, 19, 19, 19, 19, 19, 
19, 20, 20, 20, 20, 20, 20, 20, 19, 20, 21, 21, 21, 21, 21, 21, 
19, 20, 21, 22, 22, 22, 22, 22, 19, 20, 21, 22, 22, 22, 22, 22, 
1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 
1, 1, 1, 1, 1, 1, 1, 1, 19, 19, 19, 19, 19, 19, 19, 19, 
20, 20, 20, 20, 20, 20, 20, 20, 21, 21, 21, 21, 21, 21, 21, 21, 
22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 
1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 
1, 1, 1, 1, 1, 1, 1, 1, 19, 19, 19, 19, 19, 19, 19, 1, 
20, 20, 20, 20, 20, 20, 19, 1, 21, 21, 21, 21, 21, 20, 19, 1, 
22, 22, 22, 22, 21, 20, 19, 1, 22, 22, 22, 22, 21, 20, 19, 1, 
1, 3, 3, 6, 8, 8, 8, 8, 1, 3, 3, 6, 8, 8, 8, 8, 
1, 3, 3, 6, 8, 8, 8, 8, 1, 3, 3, 6, 8, 8, 8, 8, 
1, 3, 3, 6, 8, 8, 8, 8, 1, 3, 3, 6, 8, 8, 8, 8, 
1, 3, 3, 6, 6, 6, 6, 6, 1, 3, 3, 3, 3, 3, 3, 3, 
8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 
8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 
8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 
6, 6, 6, 6, 6, 6, 6, 6, 3, 3, 3, 3, 3, 3, 3, 3, 
8, 8, 8, 8, 9, 9, 9, 9, 8, 8, 8, 8, 9, 9, 9, 9, 
8, 8, 8, 8, 9, 9, 9, 9, 8, 8, 8, 8, 9, 9, 9, 9, 
8, 8, 8, 8, 9, 9, 9, 9, 8, 8, 8, 8, 9, 9, 9, 9, 
6, 6, 6, 6, 6, 6, 6, 6, 3, 3, 3, 3, 3, 3, 3, 3, 
9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 
9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 
9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 
6, 6, 6, 6, 6, 6, 6, 6, 3, 3, 3, 3, 3, 3, 3, 3, 
19, 20, 21, 22, 22, 22, 22, 23, 19, 20, 21, 22, 22, 22, 23, 24, 
19, 20, 21, 22, 22, 23, 24, 25, 19, 20, 21, 22, 23, 24, 25, 25, 
19, 20, 21, 23, 24, 25, 25, 1, 19, 20, 21, 23, 24, 25, 1, 1, 
19, 20, 21, 23, 24, 25, 1, 1, 19, 20, 21, 23, 24, 25, 1, 1, 
23, 23, 23, 23, 23, 23, 23, 23, 24, 24, 24, 24, 24, 24, 24, 24, 
25, 25, 25, 25, 25, 25, 25, 25, 1, 1, 1, 1, 1, 1, 1, 1, 
1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 
1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 
23, 23, 23, 23, 23, 23, 23, 22, 24, 24, 24, 24, 24, 24, 24, 23, 
25, 25, 25, 25, 25, 25, 25, 24, 1, 1, 1, 1, 1, 1, 25, 25, 
1, 1, 1, 1, 1, 1, 1, 25, 1, 1, 1, 1, 1, 1, 1, 1, 
1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 
22, 22, 22, 22, 21, 20, 19, 1, 22, 22, 22, 22, 21, 20, 19, 1, 
23, 22, 22, 22, 21, 20, 19, 1, 24, 23, 22, 22, 21, 20, 19, 1, 
25, 24, 23, 22, 21, 20, 19, 19, 25, 24, 23, 22, 21, 20, 20, 20, 
25, 24, 23, 22, 21, 21, 21, 21, 25, 24, 23, 22, 22, 22, 22, 22, 
1, 3, 3, 3, 3, 3, 3, 3, 1, 1, 1, 1, 1, 1, 1, 1, 
1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 
19, 19, 19, 19, 19, 19, 19, 19, 20, 20, 20, 20, 20, 20, 20, 20, 
21, 21, 21, 21, 21, 21, 21, 21, 22, 22, 22, 22, 22, 22, 22, 22, 
3, 3, 3, 3, 3, 3, 3, 3, 1, 1, 1, 1, 1, 1, 1, 1, 
1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 
19, 19, 19, 19, 19, 19, 19, 19, 20, 20, 20, 20, 20, 20, 20, 20, 
21, 21, 21, 21, 21, 21, 21, 21, 22, 22, 22, 22, 22, 22, 22, 22, 
3, 3, 3, 3, 3, 3, 3, 1, 1, 1, 1, 1, 1, 1, 1, 1, 
1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 
19, 19, 19, 19, 19, 19, 19, 19, 20, 20, 20, 20, 20, 20, 20, 19, 
21, 21, 21, 21, 21, 21, 20, 19, 22, 22, 22, 22, 22, 21, 20, 19, 
19, 20, 21, 23, 24, 25, 1, 1, 19, 20, 21, 23, 24, 25, 1, 1, 
19, 20, 21, 23, 24, 25, 1, 1, 19, 20, 21, 23, 24, 25, 1, 1, 
19, 20, 21, 23, 24, 25, 1, 1, 19, 20, 21, 23, 24, 25, 1, 1, 
19, 20, 21, 23, 24, 25, 1, 1, 19, 20, 21, 23, 24, 25, 1, 1, 
25, 24, 23, 22, 22, 22, 22, 22, 25, 24, 23, 22, 22, 22, 22, 22, 
25, 24, 23, 22, 22, 22, 22, 22, 25, 24, 23, 22, 22, 22, 22, 22, 
25, 24, 23, 22, 22, 22, 22, 22, 25, 24, 23, 22, 22, 22, 22, 22, 
25, 24, 23, 22, 22, 22, 22, 22, 25, 24, 23, 22, 22, 22, 22, 22, 
22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 
22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 
22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 
22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 
22, 22, 22, 22, 22, 21, 20, 19, 22, 22, 22, 22, 22, 21, 20, 19, 
22, 22, 22, 22, 22, 21, 20, 19, 22, 22, 22, 22, 22, 21, 20, 19, 
22, 22, 22, 22, 22, 21, 20, 19, 22, 22, 22, 22, 22, 21, 20, 19, 
22, 22, 22, 22, 22, 21, 20, 19, 22, 22, 22, 22, 22, 21, 20, 19, 
19, 20, 21, 23, 24, 25, 1, 1, 19, 20, 21, 23, 24, 25, 1, 1, 
19, 20, 21, 23, 24, 25, 1, 1, 19, 20, 21, 23, 24, 25, 1, 1, 
19, 20, 21, 23, 24, 25, 1, 1, 19, 20, 21, 23, 24, 25, 1, 1, 
19, 20, 21, 23, 24, 25, 1, 1, 19, 20, 21, 23, 24, 25, 25, 1, 
1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 
1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 
1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 
1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 25, 
19, 20, 21, 22, 23, 24, 25, 25, 19, 20, 21, 22, 22, 23, 24, 25, 
19, 20, 21, 22, 22, 22, 23, 24, 19, 20, 21, 22, 22, 22, 22, 23, 
19, 20, 21, 22, 22, 22, 22, 22, 19, 20, 21, 22, 22, 22, 22, 22, 
19, 20, 21, 21, 21, 21, 21, 21, 19, 20, 20, 20, 20, 20, 20, 20, 
1, 1, 1, 1, 1, 1, 1, 1, 25, 25, 25, 25, 25, 25, 25, 25, 
24, 24, 24, 24, 24, 24, 24, 24, 23, 23, 23, 23, 23, 23, 23, 23, 
22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 
21, 21, 21, 21, 21, 21, 21, 21, 20, 20, 20, 20, 20, 20, 20, 20, 
1, 1, 1, 1, 1, 1, 25, 25, 25, 25, 25, 25, 25, 25, 25, 24, 
24, 24, 24, 24, 24, 24, 24, 23, 23, 23, 23, 23, 23, 23, 23, 22, 
22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 
21, 21, 21, 21, 21, 21, 21, 21, 20, 20, 20, 20, 20, 20, 20, 20, 
24, 23, 22, 22, 22, 22, 22, 22, 23, 22, 22, 22, 22, 22, 22, 22, 
22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 
22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 
21, 21, 21, 21, 21, 21, 21, 21, 20, 20, 20, 20, 20, 20, 20, 20, 
22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 
22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 
22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 
21, 21, 21, 21, 21, 21, 21, 21, 20, 20, 20, 20, 20, 20, 20, 20, 
22, 22, 22, 22, 22, 21, 20, 19, 22, 22, 22, 22, 22, 21, 20, 19, 
22, 22, 22, 22, 22, 21, 20, 19, 22, 22, 22, 22, 22, 21, 20, 19, 
22, 22, 22, 22, 22, 21, 20, 19, 22, 22, 22, 22, 22, 21, 20, 19, 
21, 21, 21, 21, 21, 21, 20, 19, 20, 20, 20, 20, 20, 20, 20, 19, 
19, 19, 19, 19, 19, 19, 19, 19, 1, 1, 1, 1, 1, 1, 1, 1, 
1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 
1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 
1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1
};

