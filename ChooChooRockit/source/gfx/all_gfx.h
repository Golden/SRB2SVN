//Gfx converted using Mollusk's PAGfx Converter

//This file contains all the .h, for easier inclusion in a project

#ifndef ALL_GFX_H
#define ALL_GFX_H

#ifndef PAGfx_struct
    typedef struct{
    void *Map;
    int MapSize;
    void *Tiles;
    int TileSize;
    void *Palette;
    int *Info;
} PAGfx_struct;
#endif


// Sprite files : 
extern const unsigned char uparrow_Sprite[256] __attribute__ ((aligned (4))) ;  // Pal : uparrow_Pal
extern const unsigned char catpoo_Sprite[256] __attribute__ ((aligned (4))) ;  // Pal : catpoo_Pal
extern const unsigned char choochoo_Sprite[256] __attribute__ ((aligned (4))) ;  // Pal : choochoo_Pal
extern const unsigned char crosshair_Sprite[256] __attribute__ ((aligned (4))) ;  // Pal : crosshair_Pal
extern const unsigned char downarrow_Sprite[256] __attribute__ ((aligned (4))) ;  // Pal : downarrow_Pal
extern const unsigned char leftarrow_Sprite[256] __attribute__ ((aligned (4))) ;  // Pal : leftarrow_Pal
extern const unsigned char rightarrow_Sprite[256] __attribute__ ((aligned (4))) ;  // Pal : rightarrow_Pal
extern const unsigned char rockit_Sprite[256] __attribute__ ((aligned (4))) ;  // Pal : rockit_Pal
extern const unsigned char train_Sprite[256] __attribute__ ((aligned (4))) ;  // Pal : train_Pal
extern const unsigned char horizontalwall_Sprite[256] __attribute__ ((aligned (4))) ;  // Pal : horizontalwall_Pal
extern const unsigned char verticalwall_Sprite[256] __attribute__ ((aligned (4))) ;  // Pal : verticalwall_Pal

// Background files : 
extern const int background_Info[3]; // BgMode, Width, Height
extern const unsigned short background_Map[768] __attribute__ ((aligned (4))) ;  // Pal : background_Pal
extern const unsigned char background_Tiles[5696] __attribute__ ((aligned (4))) ;  // Pal : background_Pal
extern PAGfx_struct background; // background pointer


// Palette files : 
extern const unsigned short uparrow_Pal[3] __attribute__ ((aligned (4))) ;
extern const unsigned short catpoo_Pal[3] __attribute__ ((aligned (4))) ;
extern const unsigned short choochoo_Pal[5] __attribute__ ((aligned (4))) ;
extern const unsigned short crosshair_Pal[2] __attribute__ ((aligned (4))) ;
extern const unsigned short downarrow_Pal[3] __attribute__ ((aligned (4))) ;
extern const unsigned short leftarrow_Pal[3] __attribute__ ((aligned (4))) ;
extern const unsigned short rightarrow_Pal[3] __attribute__ ((aligned (4))) ;
extern const unsigned short rockit_Pal[13] __attribute__ ((aligned (4))) ;
extern const unsigned short train_Pal[2] __attribute__ ((aligned (4))) ;
extern const unsigned short horizontalwall_Pal[3] __attribute__ ((aligned (4))) ;
extern const unsigned short verticalwall_Pal[3] __attribute__ ((aligned (4))) ;
extern const unsigned short background_Pal[26] __attribute__ ((aligned (4))) ;


#endif

