typedef enum
{
   MT_CHOOCHOO,
   MT_CATPOO,
   MT_REDROCKIT,
   MT_BLUEROCKIT,
   MT_YELLOWROCKIT,
   MT_GREENROCKIT,
   MT_TRAIN,
   MT_UPARROW,
   MT_DOWNARROW,
   MT_LEFTARROW,
   MT_RIGHTARROW,
   MT_HORIZWALL,
   MT_VERTIWALL
}type_t;

typedef struct mobj_s
{
   u16 x;
   u16 y;
   u16 radius;
   u16 speed;
   s16 fuse; // Tic.. tic.. tic..
   byte spritesizex;
   byte spritesizey;
   bool alive; // 0 is dead, 1 is alive
   byte spritenum;
   bool exists; // Mobj and sprite already exist..
   byte angle;
   s16 leftwall;
   s16 rightwall;
   s16 upwall;
   s16 downwall;
   
   type_t type;
}mobj_t;

void MoveMobj(mobj_t* mobj);
mobj_t* SpawnMobj(s32 x, s32 y, byte angle, s32 radius, type_t type);
void CheckCollisions(mobj_t* mobj);
void KillMobj(mobj_t* mobj);
int FindSlot(void);
void InitPalettes(void);
void InitBackgrounds(void);
void SetSpriteToMobjXY(byte screen, mobj_t* mo);
void InitGFX(void);
bool CheckSpaceAvailable(s32 x, s32 y);
void CheckWalls(mobj_t* mobj);
