#include <PA9.h>
#include "main.h"
#include "player.h"
#include "mobj.h"
#include "defines.h"

// PAGfxConverter Include
#include "gfx/all_gfx.c"
#include "gfx/all_gfx.h"

extern byte objects;
extern byte curslot;
extern mobj_t mobjlist[];
extern player_t player;
extern u16 counter;
u16 graffix[10];

// all_gfx.c is included here, so yes, there's a reason for it being in the wrong file..
void InitPalettes(void)
{
	PA_LoadSpritePal(0, CROSSHAIR, (void*)crosshair_Pal);
	PA_LoadSpritePal(0, CHOOCHOO, (void*)choochoo_Pal); 
	PA_LoadSpritePal(0, CATPOO, (void*)catpoo_Pal); 
	PA_LoadSpritePal(0, UPARROW, (void*)uparrow_Pal);
	PA_LoadSpritePal(0, ROCKIT, (void*)rockit_Pal); 
	PA_LoadSpritePal(0, HORIZWALL, (void*)horizontalwall_Pal); 
}
// Also here.. same reason.
void InitBackgrounds(void)
{
  // PA_SetBgPalCol(0, 1, PA_RGB(31, 31, 31)); // Color for text
//   PA_Init8bitBg(0, 2); // BG for text
   
	PA_EasyBgLoad(0, // Screen
   				3, // BG number
   				background); // Name
/*  PA_EnableSpecialFx(0, // Screen
							SFX_ALPHA, // Alpha blending mode
							SFX_BG0, // Nothing
							SFX_BG0 | SFX_BG1 | SFX_BG2 | SFX_OBJ | SFX_BG3); // Everything normal
	PA_SetSFXAlpha(0, // Screen
						15, // Alpha level, 0-15
						5); // What's this one do..?   
*/
}
// And again..
void InitGFX(void)
{
	graffix[CROSSHAIR] = PA_CreateGfx(0, (void*)crosshair_Sprite, OBJ_SIZE_16X16, 1);
	graffix[CHOOCHOO] = PA_CreateGfx(0, (void*)choochoo_Sprite, OBJ_SIZE_16X16, 1);
	graffix[CATPOO] = PA_CreateGfx(0, (void*)catpoo_Sprite, OBJ_SIZE_16X16, 1);
	graffix[ROCKIT] = PA_CreateGfx(0, (void*)rockit_Sprite, OBJ_SIZE_16X16, 1);
	graffix[TRAIN] = PA_CreateGfx(0, (void*)train_Sprite, OBJ_SIZE_16X16, 1);
	graffix[UPARROW] = PA_CreateGfx(0, (void*)uparrow_Sprite, OBJ_SIZE_16X16, 1);
	graffix[DOWNARROW] = PA_CreateGfx(0, (void*)downarrow_Sprite, OBJ_SIZE_16X16, 1);
	graffix[LEFTARROW] = PA_CreateGfx(0, (void*)leftarrow_Sprite, OBJ_SIZE_16X16, 1);
	graffix[RIGHTARROW] = PA_CreateGfx(0, (void*)rightarrow_Sprite, OBJ_SIZE_16X16, 1);
	graffix[HORIZWALL] = PA_CreateGfx(0, (void*)horizontalwall_Sprite, OBJ_SIZE_16X16, 1);
	graffix[VERTIWALL] = PA_CreateGfx(0, (void*)verticalwall_Sprite, OBJ_SIZE_16X16, 1);
}
// Find the next available mobjlist slot and return it.
int FindSlot(void)
{
   byte i = 1;
	for(i=1;i<MAXMOBJS;i++)
	{
	   if(mobjlist[i].alive)
	   	continue;
	   
	   memset(&mobjlist[i], 0, sizeof(mobj_t));
	   mobjlist[i].spritenum = curslot = i;
		break;			
	}	
	return i;
}
// Move a mobj. Called once per tic.
void MoveMobj(mobj_t* mobj)
{
   u8 x, y;
   x = mobj->x>>8;
   y = mobj->y>>8;
   
   if(mobj->type == MT_CHOOCHOO ||
   	mobj->type == MT_CATPOO)
      CheckWalls(mobj);	
			
	// Thinkers
	switch(mobj->type)
	{
		case MT_TRAIN:
		   if(objects <= 115)
		   {
			   if(PA_RandMax(1000) >= 980)
					SpawnMobj(mobj->x, mobj->y, mobj->angle, 16*UNIT, MT_CHOOCHOO);
				if((counter%(10*TICRATE)) == 0
				&& (counter)) // Don't spawn one at the very start of the level..
					SpawnMobj(mobj->x, mobj->y, mobj->angle, 16*UNIT, MT_CATPOO);
			}			
			break;
		default:
		   break;  
	}
	
	// If a mobj doesn't have any of these walls, just default to the borders so weird shit doesn't happen.
	if(!mobj->leftwall)
		mobj->leftwall = -400;
	if(!mobj->rightwall)
		mobj->rightwall = 400;
	if (!mobj->downwall)
		mobj->downwall = 400;
	if (!mobj->upwall)
		mobj->upwall = -400;
	
	// Wall collision
	// Move the mobj back to where it was to get it away from the border.
	if(mobj->x + mobj->spritesizex*UNIT >= mobj->rightwall*UNIT)
	{
	   // Turn left __|
	   if(mobj->y + mobj->spritesizey*UNIT >= mobj->downwall*UNIT)
	   {
			mobj->angle = ANGUP;
			mobj->y -= UNIT;
		}			
		// Turn right ```|
		else
		{
			mobj->angle = ANGDOWN;
			mobj->y += UNIT;
		}			
		
		mobj->x = mobj->rightwall*UNIT-mobj->spritesizex*UNIT;
		mobj->rightwall = 400; 
	}
	else if(mobj->x <= mobj->leftwall*UNIT)
	{
	   // Turn left |```
	   if(mobj->y - mobj->spritesizey*UNIT <= mobj->upwall*UNIT)
	   {
	      mobj->angle = ANGDOWN;
	      mobj->y += UNIT;
	   }   
	   // Turn right |__
	   else		   
	   {
			mobj->angle = ANGUP;	
			mobj->y -= UNIT;
		}			
			
		mobj->x = mobj->leftwall*UNIT;	
		mobj->leftwall = -400;	
	}		
		
	else if(mobj->y < mobj->upwall*UNIT)
	{
	   // Turn left ```|
	   if(mobj->x + mobj->spritesizex*UNIT >= mobj->rightwall*UNIT)
	   {
			mobj->angle = ANGLEFT;	
			mobj->x -= UNIT;
		}			
		// Turn right |```
		else
		{
			mobj->angle = ANGRIGHT;
			mobj->x += UNIT;
		}		
		
		mobj->y = mobj->upwall*UNIT;
		mobj->upwall = -400;
	}		
	else if(mobj->y + mobj->spritesizey*UNIT > mobj->downwall*UNIT)
	{
	   // Turn left |__
	   if(mobj->x - mobj->spritesizex*UNIT <= mobj->leftwall*UNIT)
	   {
			mobj->angle = ANGRIGHT;
			mobj->x += UNIT;
		}			
		// Turn right __|
		else
		{
			mobj->angle = ANGLEFT;
			mobj->x -= UNIT;
		}			
		
		mobj->y = mobj->downwall*UNIT-mobj->spritesizey*UNIT;
		mobj->downwall = 400;
	}

	
	// Border patrol
	if(mobj->x + mobj->spritesizex*UNIT > BORDERRIGHT)
	{
	   // Turn left __|
	   if(mobj->y + mobj->spritesizey*UNIT >= BORDERBOTTOM)
	   {
			mobj->angle = ANGUP;
			mobj->y -= UNIT;
		}			
		// Turn right ```|
		else
		{
			mobj->angle = ANGDOWN;
			mobj->y += UNIT;
		}			
		
		mobj->x = BORDERRIGHT-mobj->spritesizex*UNIT;  
	}
	else if(mobj->x < BORDERLEFT)
	{
	   // Turn left |```
	   if(mobj->y - mobj->spritesizey*UNIT <= BORDERTOP)
	   {
	      mobj->angle = ANGDOWN;
	      mobj->y += UNIT;
	   }   
	   // Turn right |__
	   else		   
	   {
			mobj->angle = ANGUP;	
			mobj->y -= UNIT;
		}			
			
		mobj->x = BORDERLEFT;			
	}			
	else if(mobj->y < BORDERTOP)
	{
	   // Turn left ```|
	   if(mobj->x + mobj->spritesizex*UNIT >= BORDERRIGHT)
	   {
			mobj->angle = ANGLEFT;	
			mobj->x -= UNIT;
		}			
		// Turn right |```
		else
		{
			mobj->angle = ANGRIGHT;
			mobj->x += UNIT;
		}		
		
		mobj->y = BORDERTOP;
	}		
	else if(mobj->y + mobj->spritesizey*UNIT > BORDERBOTTOM)
	{
	   // Turn left |__
	   if(mobj->x - mobj->spritesizex*UNIT <= BORDERLEFT)
	   {
			mobj->angle = ANGRIGHT;
			mobj->x += UNIT;
		}			
		// Turn right __|
		else
		{
			mobj->angle = ANGLEFT;
			mobj->x -= UNIT;
		}			
		
		mobj->y = BORDERBOTTOM-mobj->spritesizey*UNIT;
	}	
		
	// Actual movement.
	if(mobj->speed)
   {
      switch(mobj->angle)
      {
	      case ANGUP:
	         mobj->y -= (mobj->speed);
				break;
			case ANGDOWN:
			   mobj->y += (mobj->speed);
			   break;
			case ANGLEFT:
			   mobj->x -= (mobj->speed);
			   break;
			case ANGRIGHT:
			   mobj->x += (mobj->speed);
			   break;
		}   
		   
	}	
		
	SetSpriteToMobjXY(0, mobj); 
}

mobj_t* SpawnMobj(s32 x, s32 y, byte angle, s32 radius, type_t type)
{
   mobj_t* mobj;

	if ((type == MT_UPARROW
	|| type == MT_DOWNARROW
	|| type == MT_LEFTARROW
	|| type == MT_RIGHTARROW)
	&& (!CheckSpaceAvailable(x,y)))
		return 0;
		
	mobj = &mobjlist[FindSlot()];
	mobj->type = type;
	
	switch(mobj->type)
	{
	   case MT_TRAIN:
	      mobj->spritesizex = mobj->spritesizey = 16;
	      if(!mobj->exists)
				PA_CreateSpriteFromGfx(0, // Screen
											mobj->spritenum, // Sprite number..
											graffix[TRAIN], // Image
											OBJ_SIZE_16X16, 1, CROSSHAIR, -255, -255);
			else
				PA_SetSpriteGfx(0, mobj->spritenum, graffix[TRAIN]);
				
			mobj->fuse = -1;
			mobj->speed = 0;	
		   break;
		case MT_CHOOCHOO:
		   mobj->spritesizex = mobj->spritesizey = 16;
		   if(!mobj->exists)
				PA_CreateSpriteFromGfx(0, // Screen
											mobj->spritenum, // Sprite number..
											graffix[CHOOCHOO], // Image
											OBJ_SIZE_16X16, 1, CHOOCHOO, -255, -255);
			else
				PA_SetSpriteGfx(0, mobj->spritenum, graffix[CHOOCHOO]);
				
			mobj->fuse = -1;
			mobj->speed = 550;
			break;
		case MT_CATPOO:
		   mobj->spritesizex = mobj->spritesizey = 16;
		   if(!mobj->exists)
				PA_CreateSpriteFromGfx(0, // Screen
											mobj->spritenum, // Sprite number..
											graffix[CATPOO], // Image
											OBJ_SIZE_16X16, 1, CATPOO, -255, -255);
			else
				PA_SetSpriteGfx(0, mobj->spritenum, graffix[CATPOO]);	
			mobj->fuse = -1;
			mobj->speed = 350;
			break;
		case MT_REDROCKIT:
		case MT_GREENROCKIT:
		case MT_BLUEROCKIT:
		case MT_YELLOWROCKIT:
		   mobj->spritesizex = mobj->spritesizey = 16;
		   if(!mobj->exists)
				PA_CreateSpriteFromGfx(0, // Screen
											mobj->spritenum, // Sprite number..
											graffix[ROCKIT], // Image
											OBJ_SIZE_16X16, 1, ROCKIT, -255, -255);
			else
				PA_SetSpriteGfx(0, mobj->spritenum, graffix[ROCKIT]);	
			mobj->fuse = -1;
			mobj->speed = 0;
			break;
		case MT_UPARROW:
		   mobj->spritesizex = mobj->spritesizey = 16;
		   if(!mobj->exists)
				PA_CreateSpriteFromGfx(0, // Screen
											mobj->spritenum, // Sprite number..
											graffix[UPARROW], // Image
											OBJ_SIZE_16X16, 1, UPARROW, -255, -255);
			else
				PA_SetSpriteGfx(0, mobj->spritenum, graffix[UPARROW]);
				
			PA_SetSpritePrio(0, mobj->spritenum, 1);	
			mobj->fuse = -1;
			mobj->speed = 0;
		   break;   
		case MT_DOWNARROW:
		   mobj->spritesizex = mobj->spritesizey = 16;
		   if(!mobj->exists)
				PA_CreateSpriteFromGfx(0, // Screen
											mobj->spritenum, // Sprite number..
											graffix[DOWNARROW], // Image
											OBJ_SIZE_16X16, 1, UPARROW, -255, -255);
			else
				PA_SetSpriteGfx(0, mobj->spritenum, graffix[DOWNARROW]);
				
			PA_SetSpritePrio(0, mobj->spritenum, 1);	
			mobj->fuse = -1;
			mobj->speed = 0;
		   break;
		case MT_LEFTARROW:
		   mobj->spritesizex = mobj->spritesizey = 16;
		   if(!mobj->exists)
				PA_CreateSpriteFromGfx(0, // Screen
											mobj->spritenum, // Sprite number..
											graffix[LEFTARROW], // Image
											OBJ_SIZE_16X16, 1, UPARROW, -255, -255);
			else
				PA_SetSpriteGfx(0, mobj->spritenum, graffix[LEFTARROW]);	
				
			PA_SetSpritePrio(0, mobj->spritenum, 1);
			mobj->fuse = -1;
			mobj->speed = 0;
		   break;
		case MT_RIGHTARROW:
		   mobj->spritesizex = mobj->spritesizey = 16;
		   if(!mobj->exists)
				PA_CreateSpriteFromGfx(0, // Screen
											mobj->spritenum, // Sprite number..
											graffix[RIGHTARROW], // Image
											OBJ_SIZE_16X16, 1, UPARROW, -255, -255);
			else
				PA_SetSpriteGfx(0, mobj->spritenum, graffix[RIGHTARROW]);
				
			PA_SetSpritePrio(0, mobj->spritenum, 1);	
			mobj->fuse = -1;
			mobj->speed = 0;
		   break;
		case MT_HORIZWALL:
		   mobj->spritesizex = mobj->spritesizey = 16;
		   if(!mobj->exists)
				PA_CreateSpriteFromGfx(0, // Screen
											mobj->spritenum, // Sprite number..
											graffix[HORIZWALL], // Image
											OBJ_SIZE_16X16, 1, HORIZWALL, -255, -255);
			else
				PA_SetSpriteGfx(0, mobj->spritenum, graffix[HORIZWALL]);	
				
			PA_SetSpritePrio(0, mobj->spritenum, 1);
			mobj->fuse = -1;
			mobj->speed = 0;
		   break;
		case MT_VERTIWALL:
		   mobj->spritesizex = mobj->spritesizey = 16;
		   if(!mobj->exists)
				PA_CreateSpriteFromGfx(0, // Screen
											mobj->spritenum, // Sprite number..
											graffix[VERTIWALL], // Image
											OBJ_SIZE_16X16, 1, HORIZWALL, -255, -255);
			else
				PA_SetSpriteGfx(0, mobj->spritenum, graffix[VERTIWALL]);	
				
			PA_SetSpritePrio(0, mobj->spritenum, 1);
			mobj->fuse = -1;
			mobj->speed = 0;
		   break;
		default:
		   if(!mobj->exists)
		 	  PA_CreateSpriteFromGfx(0, // Screen
											mobj->spritenum, // Sprite number..
											graffix[CATPOO], // Image
											OBJ_SIZE_16X16, 1, CATPOO, -255, -255);
			else
				PA_SetSpriteGfx(0, mobj->spritenum, graffix[CATPOO]);
		   mobj->fuse = -1;
		   break;
	}			
	
	mobj->x = x;
	mobj->y = y;
	mobj->radius = radius;
	mobj->angle = angle;
	
	mobj->alive = true; // IT'S ALIIIIIVE!
	mobj->exists = true;
	
	objects++;
	return mobj;
}

// Kill a mobj, obviously.
void KillMobj(mobj_t* mobj)
{
	mobj->x = 256;
	mobj->y = 192;
	mobj->alive = false;
	PA_SetSpriteXY(0, mobj->spritenum, 256, 192);
	// Stop any animations the mobj may be going through..
	PA_StopSpriteAnim(0, mobj->spritenum);
	// Bullet is small and never falls over the edge of the screen, so change it to that..
//	PA_SetSpriteGfx(0, mobj->spritenum, graffix[BULLET]);
   objects--;
} 
void CheckWalls(mobj_t* mobj)
{
  	u8 i, x, y = 0;
  	mobj_t* wall = NULL;
  	
  	x = mobj->x>>8;
  	y = mobj->y>>8;
	
	for(i=1;i<125;i++)
	{
	   wall = &mobjlist[i];
	   if(!wall->alive)
      	continue;
      	
		if(wall->type != MT_HORIZWALL
		&& wall->type != MT_VERTIWALL)
			continue;
			
		// Left wall
		if(x-16 == wall->x>>8
			&& y == wall->y>>8)
				mobj->leftwall = x-16;			
	//	else
	//		mobj->leftwall = BORDERLEFT>>8;	
		
		// Right wall
		if(x+16 == wall->x>>8
			&& y == wall->y>>8)
			mobj->rightwall = x+16;	
	//	else
	//		mobj->rightwall = BORDERRIGHT>>8;
				
		// Top wall
		if(x == wall->x>>8
			&& y-16 == wall->y>>8)
			mobj->upwall = y-16;
	//	else
	//		mobj->upwall = BORDERTOP>>8;
	
		// Bottom wall
		if(x == wall->x>>8
			&& y+16 == wall->y>>8)
			mobj->downwall = y+16;	
	//	else
	//		mobj->downwall = BORDERBOTTOM>>8;
					
	}		 
}
	  
void CheckCollisions(mobj_t* toucher)
{
   byte i;
   mobj_t* touched;
   for(i=0;i<MAXMOBJS;i++)
   {
   	touched = &mobjlist[i];
   	if(!touched->alive)
      	continue;
      
      // Don't touch yourself -_-;
      if(toucher == touched)
      	continue;
      	
      // Within hit distance?
      if(PA_Distance(toucher->x, toucher->y, touched->x, touched->y) > touched->speed*touched->speed)
    	  continue; 
				
	   switch(toucher->type)
	   {  
		   case MT_UPARROW:
		   case MT_DOWNARROW:
			case MT_LEFTARROW:
			case MT_RIGHTARROW:
			   touched->angle = toucher->angle;
				break;
			case MT_REDROCKIT:
			case MT_BLUEROCKIT:
			case MT_GREENROCKIT:
			case MT_YELLOWROCKIT:
			   KillMobj(touched);
			   if(touched->type == MT_CHOOCHOO)
					player.score++;
				else if(touched->type == MT_CATPOO)
					player.score *= .6;
					
				break;
			case MT_CATPOO:
			   if(touched->type == MT_CHOOCHOO)
					KillMobj(touched);		
			default:	
				break;		
		}	// End switch
	}	// End for loop			
}

// Easier to work with. This way, I don't need to work with bitshifts,
// nor do I need to subtract half of the sprite's size.
void SetSpriteToMobjXY(byte screen, mobj_t* mo)
{
   if(mo->type == MT_HORIZWALL)
   	PA_SetSpriteXY(screen, mo->spritenum, (mo->x>>8), ((mo->y>>8)-8));
   else if(mo->type == MT_VERTIWALL)
   	PA_SetSpriteXY(screen, mo->spritenum, ((mo->x>>8)-8), (mo->y>>8));
   else
		PA_SetSpriteXY(screen, mo->spritenum, (mo->x>>8), (mo->y>>8));
}

// Check to see if you can place an arrow here.
bool CheckSpaceAvailable(s32 x, s32 y)
{
	byte i = 0;
	mobj_t* mobj = NULL;
	for(i=1;i<125;i++)
	{
	   mobj = &mobjlist[i];
	   if(mobj->type != MT_UPARROW
		&& mobj->type != MT_DOWNARROW
		&& mobj->type != MT_LEFTARROW
		&& mobj->type != MT_RIGHTARROW)
			continue; 
			
		if(x == mobj->x
		&& y == mobj->y)
			return false;
	}
	return true;
}
