#!/bin/sh

############################
## NETWORKING INIT SCRIPT ##
############################

## Callum Dickinson <gcfreak_ag20@hotmail.com>
## Sonic Team Junior <http://www.srb2.org>
## http://mb.srb2.org/showthread.php?t=31137
## svn://code.srb2.org/SRB2/branches/WII

## This program is free software; you can redistribute it and/or
## modify it under the terms of the GNU General Public License
## as published by the Free Software Foundation; either version 2
## of the License, or (at your option) any later version.

## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.

## Any lines in this file preceeded by a hash ('#') indicates a comment, and is not read by the interpreter.

## - The below lines are init script parameters - DO NOT MODIFY UNLESS YOU KNOW WHAT YOU ARE DOING!
# INIT_FILESYSTEM

# INIT_STARTSCRIPT
# INIT_STOPSCRIPT

# INIT_STARTPRIORITY 55
# INIT_STOPPRIORITY 40
## -

#--------------------#
. /lib/lsb/init-functions
. /etc/profile
LOGFILE="$LOGDIR/$ME.log"
#--------------------#

ifrun ()
{
	$IFRUN -a -v > /tmp/ifup-run.log 2>&1
	cat /tmp/ifup-run.log >> $LOGDIR/ifup-run.log
# ugly hack must die
#	while [ "`cat /tmp/ifup-run.log | grep "Aborted"`" ] || [ "`cat /tmp/ifup-run.log | grep "Segmentation fault"`" ]; do
#		$IFRUN -a -v > /tmp/ifup-run.log 2>&1
#		cat /tmp/ifup-run.log >> $LOGDIR/ifup-run.log
#	done
}

error_check_and_stop ()
{
	if [ "`cat /tmp/ifup-run.log | grep "No lease"`" ]; then
		echo "dhcp_nolease" > /tmp/networking_fail
		log_action_end_msg 1
	else
		log_action_end_msg 0
	fi
}

rm -rf /tmp/networking_fail

if [ "`cat $SYSCONF 2> /dev/null | grep Networking | sed -ne 's/.*Networking=\([^ ]*\).*/\1/p'`" = "Enabled" ]; then
	[ -x /sbin/ifup ] || exit 0

	spoofprotect_rp_filter() {
	    [ -e /proc/sys/net/ipv4/conf/all/rp_filter ] || return 1
	    RC=0
	    for f in /proc/sys/net/ipv4/conf/*/rp_filter; do
		echo 1 > $f || RC=1
	    done
	    return $RC
	}

	spoofprotect() {
	  spoofprotect_rp_filter
	}

	ip_forward() {
	  echo 1 > /proc/sys/net/ipv4/ip_forward
	}

	syncookies() {
	  echo 1 > /proc/sys/net/ipv4/tcp_syncookies
	}

	doopt() {
	    optname=$1
	    default=$2
	    opt=`grep "^$optname=" /etc/network/options || true`
	    if [ -z "$opt" ]; then
		opt="$optname=$default"
	    fi
	    optval=${opt#$optname=}
	    if [ "$optval" = "yes" ]; then
		eval $optname
	    fi
	}

	process_options() {
	    [ -e /etc/network/options ] || return 0
	    doopt spoofprotect yes
	    doopt syncookies no
	    doopt ip_forward no
	}

	case "$1" in
	start)
		IFRUN="ifup"
		log_action_begin_msg "Starting networking services"
		process_options
		echo "--$IFRUN run on `date`--" >> $LOGDIR/ifup-run.log
		ifrun
		error_check_and_stop
		;;

	stop)
		IFRUN="ifdown"
		log_action_begin_msg "Stopping networking services"
		if sed -n 's/^[^ ]* \([^ ]*\) \([^ ]*\) .*$/\2/p' /proc/mounts | 
			grep -qE '^(nfs[1234]?|smbfs|ncp|ncpfs|coda|cifs)$'; then
		    exit 0
		fi
		echo "--$IFRUN run on `date`--" >> $LOGDIR/ifup-run.log
		ifrun
		log_action_end_msg 0
		;;

	force-reload|restart)
		IFRUN="ifdown"
		log_action_begin_msg "Restarting networking services"
		process_options
		echo "--ifupdown run on `date`--" >> $LOGDIR/ifup-run.log
		echo "--$IFRUN--" >> $LOGDIR/ifup-run.log
		ifrun
		IFRUN="ifup"
		echo "--$IFRUN--" >> $LOGDIR/ifup-run.log
		ifrun
		error_check_and_stop
		;;

	*)
		print "Usage: /etc/init.d/networking {start|stop|restart|force-reload}"
		exit 1
		;;
	esac
fi
exit $log_status

