// Emacs style mode select   -*- C++ -*-
// -----------------------------------------------------------------------------
//                _____ _            _     _           _
//               / ____(_)          | |   | |         | |
//              | |     _ _ __   ___| |__ | | __ _ ___| |_
//              | |    | | '_ \ / _ \ '_ \| |/ _` / __| __|
//              | |____| | | | |  __/ |_) | | (_| \__ \ |_
//               \_____|_|_| |_|\___|_.__/|_|\__,_|___/\__|
//
// -----------------------------------------------------------------------------
// By Cineblast Team
// -----------------------------------------------------------------------------
// Copyright (C) 2009-2010 Cineblast Team (D.B. Mackay)
// -----------------------------------------------------------------------------
// This work 'as-is' we provide - no warranty express or implied.
// We've done our best, to debug and test.
// Liability for damages denied. 
//
// Permission is granted hereby, to copy, share, and modify.
// Use as is fit, for free or for profit.
// These rights, on this notice, rely.
// -----------------------------------------------------------------------------
/// \file UMF_VM.h
/// \brief UMF Virtual Machine definitions
// -----------------------------------------------------------------------------

#ifndef UMF_VM_H_
#define UMF_VM_H_

#include "../doomstat.h"
#include "../doomdef.h"

#include <stdio.h>
#include <stdlib.h>

#define MAX_CYCLES 4000000  // the virtual machine may only run up to 4 million cycles before it's stopped

typedef float FLOAT32;

typedef union 
{
  INT32 int32;
  UINT32 uint32;
  INT16 int16[2];
  UINT16 uint16[2];
  SINT8 int8[4];
  UINT8 uint8[4];
  FLOAT32 float32;
}WORD; // Word G

typdef struct 
{
  INT32 opcode;
  WORD parameter;
}OPCODE;


typedef struct 
{
  SINT8 *block;
  INT32 blocklength;
  INT32 freeblock;

  INT32 codelength; 
  OPCODE *code;
  INT32 datalength;  
  UINT8 *data;

  INT32 datastack;
  INT32 returnstack;

  WORD registers[4]; 
  INT32 DP;
  INT32 PC;
  INT32 RP;

  INT32 cm:1;

  INT32 exectime;

}UMF_VM;

enum 
{
  MAGIC_CODE = 0x12341245,
};

typedef struct 
{
  INT32 magic_code;
  INT32 instructioncount;
  INT32 codeoffset;
  INT32 codesize;
  INT32 dataoffset;
  INT32 datasize;
  INT32 litsize;
  INT32 bsssize;
}VM_HEADER;

#endif
