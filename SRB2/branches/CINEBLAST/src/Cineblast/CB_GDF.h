#ifndef _CB_GDF_
#define _CB_GDF_
#include "../m_fixed.h"
#include "../info.h"
#include "../z_zone.h"
#include "../w_wad.h"

typedef struct
{
	const char* Function;
	actionf_t Action;
} GDF_ActionInfo_t;

INT32 GDF_LoadGDF(void);

// Finding things
statenum_t GDF_FindState(char* state);
mobjtype_t GDF_FindMobjType(char* mobjtype);
spritenum_t GDF_FindSpriteNum(char* sprite);

#define MAX_GDFNAME 32
#define MAX_GDFSPRITEID 5
#define MAX_GDFACTIONNAME 32

typedef struct
{
	char GDF_statename[MAX_GDFNAME];
	char GDF_sprite[MAX_GDFSPRITEID];
	INT32 GDF_frame;
	UINT8 GDF_lit;
	INT32 GDF_duration;
	char GDF_actionname[MAX_GDFACTIONNAME];
	char GDF_nextstate[MAX_GDFNAME];
	INT32 GDF_dehackednum;
} GDFstate_t;

typedef struct
{
	// Base info
	char GDF_thingname[MAX_GDFNAME];
	INT32 GDF_dehackednum;

	// States
	char GDF_spawnstate[MAX_GDFNAME];
	char GDF_seestate[MAX_GDFNAME];
	char GDF_painstate[MAX_GDFNAME];
	char GDF_meleestate[MAX_GDFNAME];
	char GDF_missilestate[MAX_GDFNAME];
	char GDF_deathstate[MAX_GDFNAME];
	char GDF_xdeathstate[MAX_GDFNAME];
	char GDF_raisestate[MAX_GDFNAME];

	// Sounds
	char GDF_seesound[MAX_GDFNAME];
	char GDF_attacksound[MAX_GDFNAME];
	char GDF_painsound[MAX_GDFNAME];
	char GDF_deathsound[MAX_GDFNAME];

	// Engine
	INT32 GDF_doomednum;
	INT32 GDF_spawnhealth;
	INT32 GDF_reactiontime;
	INT32 GDF_painchance;
	fixed_t GDF_radius;
	fixed_t GDF_height;
	UINT32 GDF_flags;
	UINT32 GDF_flags2;
	UINT32 GDF_flags3;

	// Misc
	char GDF_transfx[9];

} GDFthing_t;

#define ___EEQUOTE(a) #a
#define __EEQUOTE(a) ___EEQUOTE(a)
#define EEMT(x) ((Int32)GDF_FindMobjType(MT2EEClass[x]))
#define EEST(x) ((Int32)GDF_FindState(__EEQUOTE(x)))
#define EESPR(x) ((Int32)GDF_FindSpriteNum(__EEQUOTE(x)))
#endif
