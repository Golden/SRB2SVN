/*
 *  CB_GDF.c
 *  SRB2Cineblast
 *
 *  Dynamic mobjinfo and state tables.
 *
 */

#include "CB_GDF.h"
#include "../w_wad.h"
#include "../byteptr.h"
#include "../info.h"

#ifdef GDF
GDF_ActionInfo_t GDF_ActionTable[] =
{
	{"", {&A_Explode}},
	{"", {&A_Pain}},
	{"", {&A_Fall}},
	{"", {&A_MonitorPop}},
	{"", {&A_Look}},
	{"", {&A_Chase}},
	{"", {&A_FaceTarget}},
	{"", {&A_Scream}},
	{"", {&A_BossDeath}},
	{"", {&A_CustomPower}}, // Use this for a custom power
	{"", {&A_GiveWeapon}}, // Gives the player weapon(s)
	{"", {&A_JumpShield}}, // Obtained Jump Shield
	{"", {&A_RingShield}}, // Obtained Ring Shield
	{"", {&A_RingBox}}, // Obtained Ring Box Tails
	{"", {&A_Invincibility}}, // Obtained Invincibility Box
	{"", {&A_SuperSneakers}}, // Obtained Super Sneakers Box
	{"", {&A_BunnyHop}}, // have bunny hop tails
	{"", {&A_BubbleSpawn}}, // Randomly spawn bubbles
	{"", {&A_BubbleRise}}, // Bubbles float to surface
	{"", {&A_BubbleCheck}}, // Don't draw if not underwater
	{"", {&A_ExtraLife}}, // Extra Life
	{"", {&A_BombShield}}, // Obtained Bomb Shield
	{"", {&A_WaterShield}}, // Obtained Water Shield
	{"", {&A_ForceShield}},
	{"", {&A_GravityBox}},
	{"", {&A_ScoreRise}}, // Rise the score logo
	{"", {&A_ParticleSpawn}},
	{"", {&A_AttractChase}}, // Ring Chase
	{"", {&A_DropMine}}, // Drop Mine from Skim or Jetty-Syn Bomber
	{"", {&A_FishJump}}, // Fish Jump
	{"", {&A_ThrownRing}}, // Sparkle trail for red ring
	{"", {&A_GrenadeRing}},
	{"", {&A_SetSolidSteam}},
	{"", {&A_UnsetSolidSteam}},
	{"", {&A_SignPlayer}},
	{"", {&A_JetChase}},
	{"", {&A_JetbThink}}, // Jetty-Syn Bomber Thinker
	{"", {&A_JetgThink}}, // Jetty-Syn Gunner Thinker
	{"", {&A_JetgShoot}}, // Jetty-Syn Shoot Function
	{"", {&A_ShootBullet}}, // JetgShoot without reactiontime setting
	{"", {&A_MinusDigging}},
	{"", {&A_MinusPopup}},
	{"", {&A_MinusCheck}},
	{"", {&A_ChickenCheck}},
	{"", {&A_MouseThink}}, // Mouse Thinker
	{"", {&A_DetonChase}}, // Deton Chaser
	{"", {&A_CapeChase}}, // Fake little Super Sonic cape
	{"", {&A_RotateSpikeBall}}, // Spike ball rotation
	{"", {&A_MaceRotate}},
	{"", {&A_RockSpawn}},
	{"", {&A_SnowBall}}, // Snowball function for Snow Buster
	{"", {&A_CrawlaCommanderThink}}, // Crawla Commander
	{"", {&A_SmokeTrailer}},
	{"", {&A_RingExplode}},
	{"", {&A_OldRingExplode}},
	{"", {&A_MixUp}},
	{"", {&A_RecyclePowers}},
	{"", {&A_BossScream}},
	{"", {&A_Invinciblerize}},
	{"", {&A_DeInvinciblerize}},
	{"", {&A_GoopSplat}},
	{"", {&A_Boss2PogoSFX}},
	{"", {&A_EggmanBox}},
	{"", {&A_TurretFire}},
	{"", {&A_SuperTurretFire}},
	{"", {&A_TurretStop}},
	{"", {&A_JetJawRoam}},
	{"", {&A_JetJawChomp}},
	{"", {&A_PointyThink}},
	{"", {&A_CheckBuddy}},
	{"", {&A_HoodThink}},
	{"", {&A_ArrowCheck}},
	{"", {&A_SnailerThink}},
	{"", {&A_SharpChase}},
	{"", {&A_SharpSpin}},
	{"", {&A_VultureVtol}},
	{"", {&A_VultureCheck}},
	{"", {&A_SkimChase}},
	{"", {&A_SkullAttack}},
	{"", {&A_LobShot}},
	{"", {&A_CannonLook}},
	{"", {&A_FireShot}},
	{"", {&A_SuperFireShot}},
	{"", {&A_BossFireShot}},
	{"", {&A_SparkFollow}},
	{"", {&A_BuzzFly}},
	{"", {&A_GuardChase}},
	{"", {&A_SetReactionTime}},
	{"", {&A_Boss3TakeDamage}},
	{"", {&A_LinedefExecute}},
	{"", {&A_PlaySeeSound}},
	{"", {&A_PlayAttackSound}},
	{"", {&A_PlayActiveSound}},
	{"", {&A_1upThinker}},
	{"", {&A_BossZoom}}, //Usused
	{"", {&A_Boss1Chase}},
	{"", {&A_Boss2Chase}},
	{"", {&A_Boss2Pogo}},
	{"", {&A_BossJetFume}},
	{"", {&A_SpawnObjectAbsolute}},
	{"", {&A_SpawnObjectRelative}},
	{"", {&A_ChangeAngleRelative}},
	{"", {&A_ChangeAngleAbsolute}},
	{"", {&A_PlaySound}},
	{"", {&A_FindTarget}},
	{"", {&A_FindTracer}},
	{"", {&A_SetTics}},
	{"", {&A_SetRandomTics}},
	{"", {&A_ChangeColorRelative}},
	{"", {&A_ChangeColorAbsolute}},
	{"", {&A_MoveRelative}},
	{"", {&A_MoveAbsolute}},
	{"", {&A_Thrust}},
	{"", {&A_ZThrust}},
	{"", {&A_SetTargetsTarget}},
	{"", {&A_SetObjectFlags}},
	{"", {&A_SetObjectFlags2}},
	{"", {&A_RandomState}},
	{"", {&A_RandomStateRange}},
	{"", {&A_DualAction}},
	{"", {&A_RemoteAction}},
	{"", {&A_ToggleFlameJet}},
};

static inline void GDF_CleanUp(char* p, size_t size)
{
	char *q = p;
	char *s = p;
	char *last;
	INT32 i, j, k;
	INT32 inquotes = 0;
	INT32 inbrackets = 0;

	if (!p)
		return;

	/* STAGE 1 :: Set Last Character to a special thing*/
	last = p + size;
	*last = UINT8_MAX;

	/* STAGE 2 :: Convert '\t' -> ' ' and '\r' -> '\n' */
	p = s;
	q = s;
	inquotes = 0;

	if (devparm)
		CONS_Printf("GDF_CleanUp: Converting special characters...\n");

	while (*p)
	{
		if (*p == '\t')
			*p = ' ';
		else if (*p == '\r')
			*p = '\n';

		p++;
	}

	/* STAGE 3 :: Remove Comments */
	p = s;
	q = s;
	inquotes = 0;

	if (devparm)
		CONS_Printf("GDF_CleanUp: Removing comments...\n");

	while (*p)
	{
		if (!inquotes)
		{
			if ((*p == '/') && (*(p+1) == '/'))
			{
				while (*p != '\n')
					p++;
			}
			else if ((*p == '/') && (*(p+1) == '*'))
			{
				while ((*p != '*') && (*(p+1) != '/'))
					p++;

				p++;
			}
			else if (*p == '\"')
				inquotes = 1;
		}
		else
		{
			if (*p == '\"')
				inquotes = 0;
		}

		*q = *p;
		q++;
		p++;
	}

	/* STAGE 4 :: Remove unuseable garbage */
	p = s;
	q = s;
	inquotes = 0;

	if (devparm)
		CONS_Printf("GDF_CleanUp: Garbage cleaning...\n");

	while (*p)
	{
		if ((*p != (char)UINT8_MAX) &&
			(*p != '\n') &&
			!((*p >= 32) && (*p <= 126)))
			*p = ' ';
		p++;
	}

	/* STAGE 5 :: Convert "\n\n"... -> '\n' and "  "... -> " " */
	p = s;
	q = s;
	inquotes = 0;

	if (devparm)
		CONS_Printf("GDF_CleanUp: Removing duplicate linefeeds and spaces...\n");

	while (*p)
	{
		while ((*p == '\n') && (*(p+1) == '\n'))
			p++;

		if (!inquotes)
		{
			while ((*p == ' ') && (*(p+1) == ' '))
				p++;

			if (*p == '\"')
				inquotes = 1;
		}
		else
		{
			if (*p == '\"')
				inquotes = 0;
		}

		*q = *p;
		p++;
		q++;
	}

	/* STAGE 6 :: Remove whitespace in brackets */
	p = s;
	q = s;
	inquotes = 0;
	inbrackets = 0;

	if (devparm)
		CONS_Printf("GDF_CleanUp: Removing whitespace in brackets...\n");

	while (*p)
	{
		if (!inbrackets)
		{
			if (!inquotes)
			{
				if (*p == '\"')
					inquotes++;
				else if (*p == '{')
					inbrackets++;
			}
			else
			{
				if (*p == '\"')
					inquotes = 0;
			}
		}
		else
		{
			if (!inquotes)
			{
				while (*p == ' ')
					p++;

				if (*p == '\"')
					inquotes = 1;
				else if (*p == '}')
					inbrackets--;
				else if (*p == '{')
					inbrackets++;
			}
			else
			{
				if (*p == '\"')
					inquotes = 0;
			}
		}

		*q = *p;
		p++;
		q++;
	}

	/* STAGE 7 :: Remove newlines in brackets */
	p = s;
	q = s;
	inquotes = 0;
	inbrackets = 0;

	if (devparm)
		CONS_Printf("GDF_CleanUp: Removing newlines in brackets...\n");

	while (*p)
	{
		if (!inbrackets)
		{
			if (!inquotes)
			{
				if (*p == '\"')
					inquotes++;
				else if (*p == '{')
					inbrackets++;
			}
			else
			{
				if (*p == '\"')
					inquotes = 0;
			}
		}
		else
		{
			if (!inquotes)
			{
				while (*p == '\n')
				{
					if ((((*(p+1) >= 'a') && (*(p+1) <= 'z')) ||
						((*(p+1) >= 'A') && (*(p+1) <= 'Z'))) &&
						(*(p-1) != '{'))
						*p = ';';
					else
						p++;
				}

				if (*p == '\"')
					inquotes = 1;
				else if (*p == '}')
					inbrackets--;
				else if (*p == '{')
					inbrackets++;
			}
			else
			{
				if (*p == '\"')
					inquotes = 0;
			}
		}

		*q = *p;
		p++;
		q++;
	}

	/* STAGE 8 :: Put single definitions all on the same line */
	p = s;
	q = s;
	i = 0;
	j = 0;
	k = 0;
	inquotes = 0;
	inbrackets = 0;

	if (devparm)
		CONS_Printf("GDF_CleanUp: Place single definitons on a single line...\n");

	while (*p)
	{
		if ((*p == '\n') && (*(p+1) == '{'))
		{
			if (*(p-1) != ' ')
				*p = ' ';
			else
				p++;
		}

		*q = *p;
		p++;
		q++;
	}

	/* STAGE 9 :: Convert everything that isn't quoted to spaces */
	p = s;
	q = s;
	i = 0;
	j = 0;
	k = 0;
	inquotes = 0;
	inbrackets = 0;

	if (devparm)
		CONS_Printf("GDF_CleanUp: Lowercasing non-quoted strings...\n");

	while (*p)
	{
		if (!inquotes)
		{
			if (*p == '\"')
				inquotes = 1;

			if ((*p >= 'A') && (*p <= 'Z'))
				*q = (char)tolower(*p);
			else
				*q = *p;
		}
		else
		{
			if (*p == '\"')
				inquotes = 0;

			*q = *p;
		}

		p++;
		q++;
	}

	/* STAGE 11 :: terminate the end with a zero */
	p = s;

	if (devparm)
		CONS_Printf("GDF_CleanUp: Terminating...\n");

	while (*p)
	{
		if (*p != (char)UINT8_MAX)
			p++;
		else
		{
			*p = 0;
			break;
		}
	}
}

GDFstate_t* GDFstates = NULL;
size_t NUMGDFSTATES = 0;
GDFthing_t* GDFthings = NULL;
size_t NUMGDFTHINGS = 0;

static inline void GDF_Format(char* p, size_t size, lumpnum_t lumpnum)
{
	UINT8 *tData = Z_Calloc(1024, PU_STATIC, NULL);
	UINT8 *w = tData;
	UINT8 *q = (void *)p;
	UINT8 *r = (void *)p;
	UINT8 *s = (void *)p;
	size_t i;
	INT32 j, k;
	INT32 breakout = 0;
	INT32 stopreadingdefinition = 0;
	UINT8 buf[128];
	//byte buf2[128];
	INT32 readloop = 1;

	if (devparm)
		CONS_Printf("GDF_Format: Formatting...\n");

	(void)size; //ununsed
	(void)lumpnum; //ununsd

	while (*p)
	{
		if (strncmp(p, "frame", 5) == 0)
		{
			GDFstate_t *t;
			GDFstates = realloc(GDFstates,
				(sizeof(GDFstate_t) * NUMGDFSTATES) + sizeof(GDFstate_t));
			NUMGDFSTATES++;
			t = &GDFstates[NUMGDFSTATES-1];
			p += 5;
			p++;

			/******************************************************************/
			/*                           STATE NAME                           */
			/******************************************************************/
			i = 0;
			while ((*p != ' ') && i < MAX_GDFNAME)
			{
				t->GDF_statename[i] = *p;

				p++;
				i++;
			}
			p += 2;

			t->GDF_statename[MAX_GDFNAME - 1] = 0;

			readloop = 1;
			while (readloop)
			{
				if (strncmp(p, "cmp=", 4) == 0)
				{
					p += 5;

					stopreadingdefinition = 0;
					i = 0;
					memset(buf, 0, sizeof(buf));
					while ((*p != '\"') && (i < sizeof(buf)))
					{
						buf[i] = *p;
						p++;
						i++;
					}

					if (i < sizeof(buf))
						buf[i] = 0;
					else
						buf[sizeof(buf)-1] = 0;

					q = buf;
					r = buf;

					while (*r != 0)
					{
						while (*r == ' ')
							r++;
						*q = *r;
						q++;
						r++;
					}
					*q = 0;

					// Pipes!
					q = buf;
					i = 0;
					j = 0;

					/**************************************************************/
					/*                            SPRITE                          */
					/**************************************************************/
					i = 0;
					while (((*q != '|') && (*q != '\"')) && (i < 4))
					{
						t->GDF_sprite[i] = *q;
						q++;
						i++;
					}

					t->GDF_sprite[4] = 0;

					if (*q == '\"')
						stopreadingdefinition = 1;
					q++;

					/**************************************************************/
					/*                             FRAME                          */
					/**************************************************************/
					k = 0;

					if (!stopreadingdefinition)
					{
						if (*q == '*')
						{
							k = 0;
							q++;
						}
						else
						{
							// Letter or a number?
							if (isalpha(*r) == 0)	// Number
							{
								k = 0;

								r = q;
								while ((*r != '|') && (*r != '\"'))
									r++;
								r--;

								while (r >= q)
								{
									k += (*r - '0');
									r--;

									if (*r == '|')
										break;
									else
									{
										if (*r == '-')
										{
											k *= -1;
											break;
										}
										else
											k *= 10;
									}
								}
							}
							else					// Letter
							{
								k = tolower(*r) - 'a';
								r++;
							}

							while ((*q != '|') && (*q != '\"'))
								q++;
						}

						if (*q == '\"')
							stopreadingdefinition = 1;
						q++;
					}

					t->GDF_frame = k;

					/**************************************************************/
					/*                            BRIGHT                          */
					/**************************************************************/
					if (!stopreadingdefinition)
					{
						if ((*q == 'T') || (*q == '1'))
							t->GDF_lit = 1;
						else
							t->GDF_lit = 0;

						if (*q == '\"')
						{
							stopreadingdefinition = true;
							q++;
						}
						else
							q += 2;
					}
					else
						t->GDF_lit = 0;

					/**************************************************************/
					/*                           DURATION                         */
					/**************************************************************/
					k = 0;
					if (!stopreadingdefinition)
					{
						k = 0;

						r = q;
						while ((*r != '|') && (*r != '\"'))
							r++;
						r--;

						while (r >= q)
						{
							k += (*r - '0');
							r--;

							if (*r == '|')
								break;
							else
							{
								if (*r == '-')
								{
									k *= -1;
									break;
								}
								else
									k *= 10;
							}
						}

						while ((*q != '|') && (*q != '\"'))
							q++;

						if (*q == '\"')
							stopreadingdefinition = 1;
						q++;
					}
					else
						k = 0;

					t->GDF_duration = k;

					/**************************************************************/
					/*                            ACTION                          */
					/**************************************************************/
					if (!stopreadingdefinition)
					{
						if (*q == '*')
						{
							t->GDF_actionname[0] = '*';
							t->GDF_actionname[1] = 0;
							q++;
						}
						else
						{
							i = 0;
							while (((*q != '|') && (*q != '\"')) && (i < MAX_GDFACTIONNAME))
							{
								t->GDF_actionname[i] = *q;
								q++;
								i++;
							}

							t->GDF_actionname[i] = 0;
							t->GDF_actionname[MAX_GDFACTIONNAME - 1] = 0;
						}

						if (*q == '\"')
							stopreadingdefinition = 1;
						q++;
					}
					else
					{
						t->GDF_actionname[0] = '*';
						t->GDF_actionname[1] = 0;
					}

					/**************************************************************/
					/*                          NEXTSTATE                         */
					/**************************************************************/
					if (!stopreadingdefinition)
					{
						if (*q == '*')
						{
							t->GDF_nextstate[0] = '*';
							t->GDF_nextstate[1] = 0;
							q++;
						}
						else
						{
							i = 0;
							while ((*q != '\"') && (i < MAX_GDFNAME))
							{
								t->GDF_nextstate[i] = *q;
								q++;
								i++;
							}
							q++;

							t->GDF_nextstate[i] = 0;
							t->GDF_nextstate[MAX_GDFNAME - 1] = 0;
						}
					}
					else
					{
						t->GDF_nextstate[0] = '*';
						t->GDF_nextstate[1] = 0;
					}
				}
				/*else if (strncmp(p, "dehackednum=", 12) == 0)
				{
					p += 12;
				}*/
				else
				{
					while ((*p != ';') && (*p != '}') && (*p != '\n'))
						p++;
					p++;

					readloop = 0;
				}
			}

			if (devparm)
				CONS_Printf("Made frame: %s (%s, %i, %s, %i, %s, %s)\n",
					t->GDF_statename,
					t->GDF_sprite,
					t->GDF_frame,
					(t->GDF_lit ? "lit" : "unlit"),
					t->GDF_duration,
					t->GDF_actionname,
					t->GDF_nextstate);
		}
		else if (strncmp(p, "thingtype", 9) == 0)
		{
			GDFthing_t *t;
			GDFthings = realloc(GDFthings,
				(sizeof(GDFthing_t) * NUMGDFTHINGS) + sizeof(GDFthing_t));
			NUMGDFTHINGS++;
			t = &GDFthings[NUMGDFTHINGS-1];

			p += 9;

			i = 0;
			while ((*p != ' ') && i < MAX_GDFNAME)
			{
				t->GDF_thingname[i] = *p;

				p++;
				i++;
			}
			p += 2;

			t->GDF_thingname[MAX_GDFNAME - 1] = 0;

			readloop = 1;
			while (readloop)
			{
				if (strncmp(p, "doomednum=", 10) == 0)
				{
					p += 10;
					//t->GDF_doomednum = GDF_ReadInt(&p);
				}
				else
				{
					while ((*p != ';') && (*p != '}') && (*p != '\n'))
						p++;
					p++;

					if ((*p == '}') || (*p == '\n'))
					{
						if (*p == '}')
						{
							while (*p != '\n')
								p++;
							p++;
						}
						readloop = 0;
					}
				}
			}

			if (devparm)
				CONS_Printf("Made thing: %s ("
					"dehackednum=%i, "
					"spawnstate=%s, "
					"seestate=%s, "
					"painstate=%s, "
					"meleestate=%s, "
					"missilestate=%s, "
					"deathstate=%s, "
					"xdeathstate=%s, "
					"raisestate=%s, "
					"seesound=%s, "
					"attacksound=%s, "
					"painsound=%s, "
					"deathsound=%s, "
					"doomednum=%i, "
					"spawnhealth=%i, "
					"reactiontime=%i, "
					"painchance=%i, "
					"radius=%i, "
					"height=%i, "
					"flags=%i, "
					"flags2=%i, "
					"flags3=%i, "
					"transfx=%s"
					")\n",
					t->GDF_thingname,
					t->GDF_dehackednum,
					t->GDF_spawnstate,
					t->GDF_seestate,
					t->GDF_painstate,
					t->GDF_meleestate,
					t->GDF_missilestate,
					t->GDF_deathstate,
					t->GDF_xdeathstate,
					t->GDF_raisestate,
					t->GDF_seesound,
					t->GDF_attacksound,
					t->GDF_painsound,
					t->GDF_deathsound,
					t->GDF_doomednum,
					t->GDF_spawnhealth,
					t->GDF_reactiontime,
					t->GDF_painchance,
					t->GDF_radius,
					t->GDF_height,
					t->GDF_flags,
					t->GDF_flags2,
					t->GDF_flags3,
					t->GDF_transfx);
			break;
		}
		else if (strncmp(p, "includeifenabled", 16) == 0)
		{
			WRITEUINT8(w, 1);
			p += 16;

			while (*p != '\"')
				p++;
			p++;

			while (*p != '\"')
			{
				WRITECHAR(w, *p);
				p++;
			}
			WRITECHAR(w, 0);

			while (*p != '\"')
				p++;
			p++;

			while (*p != '\"')
			{
				WRITECHAR(w, *p);
				p++;
			}
			WRITECHAR(w, 0);
		}
		/*else if (strncmp(p, "sound", 5) == 0)
		{
			WRITEUINT8(i, 3);
			p += 5;
			break;
		}*/

		// Skip to new line
		while (*p != '\n')
		{
			if (*p == 0)
			{
				breakout = 1;
				break;
			}

			p++;
		}

		if (breakout)
			break;

		p++;
	}

	M_Memcpy(s, tData, 1024);
	Z_Free(tData);
}


INT32 GDF_LoadGDF(void)
{
	return 0;
}

statenum_t GDF_FindState(char* state)
{
	(void)state;
	return 0;
}

mobjtype_t GDF_FindMobjType(char* mobjtype)
{
	(void)mobjtype;
	return 0;
}

spritenum_t GDF_FindSpriteNum(char* sprite)
{
	(void)sprite;
	return 0;
}
#endif
