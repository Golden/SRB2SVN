/*
 *  CB_ZDSlopes.cpp
 *  SRB2Cineblast
 *
 *  Imported from ZDoom, Copyright Randy Heit
 *  Imported for SRB2 by Kalaron and Conic
 *
 */

#include "CB_ZDSlopes.h"
#include "../doomdef.h"
#include "../r_defs.h"

#ifdef ZSLOPE

//===========================================================================
//
// [RH] Set slopes for sectors, based on line specials
//
// P_AlignPlane
//
// Aligns the floor or ceiling of a sector to the corresponding plane
// on the other side of the reference line. (By definition, line must be
// two-sided.)
//
// If (which & 1), sets floor.
// If (which & 2), sets ceiling.
//
//===========================================================================
//
// Use "INT32 which" with either linedef flag, or 2 linedef slope setting effect numbers
//
//======================================
//

void P_AlignPlane(sector_t *sec, line_t *line, INT32 which)
{
	sector_t *refsec;
	fixed_t bestdist = 0;
	vertex_t *refvert = (*sec->lines)->v1;	// Shut up, GCC
	vector_t p, v1, v2, cross;
	const secplane_t *refplane;
	secplane_t *srcplane;
	fixed_t srcheight, destheight;
	INT64 i;
	line_t **probe;

	if (!line->backsector)
		return;

	// Find furthest vertex from the reference line. It, along with the two ends
	// of the line will define the plane.
	for (i = sec->linecount*2, probe = sec->lines; i > 0; i--)
	{
		fixed_t dist;
		vertex_t *vert;

		// Do calculations with only the upper bits, because the lower ones
		// are all zero, and we would overflow for a lot of distances if we
		// kept them around.

		if (i & 1)
			vert = (*probe++)->v2;
		else
			vert = (*probe)->v1;
		dist = abs (((line->v1->y - vert->y) >> FRACBITS) * (line->dx >> FRACBITS) -
					((line->v1->x - vert->x) >> FRACBITS) * (line->dy >> FRACBITS));

		if (dist > bestdist)
		{
			bestdist = dist;
			refvert = vert;
		}
	}

	refsec = line->frontsector == sec ? line->backsector : line->frontsector;

	refplane = (which == 0) ? &refsec->floorplane : &refsec->ceilingplane;
	srcplane = (which == 0) ? &sec->floorplane : &sec->ceilingplane;
	// find out what gettexz does in zdoom, and use it in SRB2
	// edit: yes, this is correctly assosciated
	srcheight = (which == 0) ? sec->floorheight : sec->ceilingheight;
	destheight = (which == 0) ? refsec->floorheight : refsec->ceilingheight;

	p.x = line->v1->x;
	p.y = line->v1->y;
	p.z = destheight;
	v1.x = line->dx;
	v1.y = line->dy;
	v1.z = 0;
	v2.x = refvert->x - line->v1->x;
	v2.y = refvert->y - line->v1->y;
	v2.z = srcheight - destheight;

	FV_Cross(&v1, &v2, &cross);

	// Fix backward normals
	if ((cross.z < 0 && which == 0) || (cross.z > 0 && which == 1))
	{
		FV_Mul(&cross, -FRACUNIT);
	}

	// srcplane is the secplane_t output of the function

	srcplane->a = cross.x;
	srcplane->b = cross.y;
	srcplane->c = cross.z;
	srcplane->ic = FixedDiv(FRACUNIT, srcplane->c);
	srcplane->d = -TMulScale16 (srcplane->a, line->v1->x,
	                            srcplane->b, line->v1->y,
	                            srcplane->c, destheight);
}

#endif
