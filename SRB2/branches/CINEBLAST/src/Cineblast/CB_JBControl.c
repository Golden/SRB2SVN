/*
 *  CB_JBControl.c
 *  SRB2Cineblast
 *
 *  Thanks to: JTE, Flame.
 *
 */

#include "../doomstat.h"
#include "../doomdef.h"

#include "../g_game.h"
#include "../p_local.h"
#include "../r_main.h"
#include "../z_zone.h"

#include "../hu_stuff.h"
#include "../i_system.h"

#include "CB_JBControl.h"

#ifdef CBOTS

consvar_t cv_botcontrol = {"botcontrol", "Off", 0, CV_OnOff, NULL, 0, NULL, NULL, 0, 0, NULL};

void CB_BCInit(void)
{
	CV_RegisterVar(&cv_botcontrol);
}

static boolean JBC_AngleMove(player_t *player)
{
	if ((player->rmomx > 0 && player->rmomy > 0) && (/*player->mo->angle >= 0 &&*/ player->mo->angle < ANGLE_90)) // Quadrant 1
		return 1;
	else if ((player->rmomx < 0 && player->rmomy > 0) && (player->mo->angle >= ANGLE_90 && player->mo->angle < ANGLE_180)) // Quadrant 2
		return 1;
	else if ((player->rmomx < 0 && player->rmomy < 0) && (player->mo->angle >= ANGLE_180 && player->mo->angle < ANGLE_270)) // Quadrant 3
		return 1;
	else if ((player->rmomx > 0 && player->rmomy < 0) && (player->mo->angle >= ANGLE_270 || player->mo->angle <= ANGLE_45)) // Quadrant 4
		return 1;
	else if (player->rmomx > 0 && (player->mo->angle >= ANGLE_315 || player->mo->angle <= ANGLE_45))
		return 1;
	else if (player->rmomx < 0 && (player->mo->angle >= ANGLE_135 && player->mo->angle <= ANGLE_225))
		return 1;
	else if (player->rmomy > 0 && (player->mo->angle >= ANGLE_45 && player->mo->angle <= ANGLE_135))
		return 1;
	else if (player->rmomy < 0 && (player->mo->angle >= ANGLE_225 && player->mo->angle <= ANGLE_315))
		return 1;

	return 0;
}

static mobj_t *JBC_Look4Collect(INT32 playernum)
{
	thinker_t* think;
	mobj_t *mo, *lastmo, *botmo = players[playernum].mo;
	fixed_t dist, lastdist;

	for(think = thinkercap.next, mo = lastmo = NULL, lastdist = 0;
		think != &thinkercap; think = think->next)
	{
		if (think->function.acp1 != (actionf_p1)P_MobjThinker) // Not a mobj thinker
			continue;

		mo = (mobj_t*)think;

		if (botmo == mo)
			continue;
		switch(mo->type)
		{
			case MT_PLAYER:
				// If it's not REALLY a player or if it's not alive
				// just skip it... No point in worrying.
				if (!mo->player
				|| mo->player->playerstate != PST_LIVE
				|| mo->player->powers[pw_flashing]
				|| mo->player->powers[pw_invulnerability]
				|| mo->player->spectator)
					continue;
				// Spectator
				if (gametype == GT_CTF && !mo->player->ctfteam)
					continue;
				// Same team
				if (gametype == GT_CTF && mo->player->ctfteam == botmo->player->ctfteam)
					continue;
				if (gametype == GT_MATCH && cv_matchtype.value == 1
				&& mo->player->skincolor == botmo->player->skincolor)
					continue;
				if (gametype == GT_MATCH && cv_matchtype.value == 2
				&& mo->player->skin == botmo->player->skin)
					continue;
				// If player is not close, I cannot see them,
				// or I don't have the rings to do anything about it anyway...
				// I do not worry.
				dist = P_AproxDistance(P_AproxDistance(botmo->x - mo->x, botmo->y - mo->y), botmo->z - mo->z);
				if (botmo->health <= 2
				|| mo->z > botmo->z + (128<<FRACBITS)
				// Don't go after them if you're not flashing...
				|| (!botmo->player->powers[pw_flashing]
					// And they're farther then 1024 units from you.
					&& dist > (1024<<FRACBITS))
				|| !P_CheckSight(botmo,mo))
							continue;
				// Otherwise... I worry.
				// I can't look for rings if I'm being watched.
				// I must fight, ready or not!
				return mo;

			case MT_RING:
			case MT_COIN:
			case MT_FLINGRING:
			case MT_FLINGCOIN:
			//case MT_HOMINGRING:
			case MT_RAILRING:
			//case MT_INFINITYRING:
			case MT_AUTOMATICRING:
			case MT_EXPLOSIONRING:
			case MT_EXTRALARGEBUBBLE: // Take bubbles too!
				// Yes! It's a ring! Score! :D
				if (mo->z < botmo->z + (128<<FRACBITS)
				&& P_CheckSight(botmo,mo))
					break;
				// Can't see it or don't think you can jump to it? Too bad...
				continue;

			default:
				// Monitor? Go for it.
				if (mo->flags & MF_MONITOR
				&& !(mo->flags & MF_NOCLIP)
				&& mo->health
				&& mo->z < botmo->z + (128<<FRACBITS)
				&& P_CheckSight(botmo,mo))
					break;
				// Check if a spring is the closest thing to you.
				// Only use it if you're within stepping distance
				// as well as closer to you then anything else
				// that you find... Otherwise, forget it.
				if (mo->flags & MF_SPRING
				&& botmo->state-states != S_PLAY_PLG1
				&& abs(botmo->z - mo->z) < 128<<FRACBITS
				&& P_CheckSight(botmo,mo))
					break;
				// Not anything I need to look at.
				continue;
		}
		dist = P_AproxDistance(P_AproxDistance(botmo->x - mo->x, botmo->y - mo->y), botmo->z - mo->z);
		if (lastmo && dist > lastdist) // Last one is closer to you?
			continue;

		// Found a target
		lastmo = mo;
		lastdist = dist;
	}
	return lastmo;
}

static mobj_t *JBC_Look4Players(INT32 playernum)
{
	thinker_t* think;
	mobj_t *mo, *lastmo, *botmo = players[playernum].mo;
	fixed_t dist, lastdist;

	for(think = thinkercap.next, mo = lastmo = NULL, lastdist = 0;
		think != &thinkercap; think = think->next)
	{
		if (think->function.acp1 != (actionf_p1)P_MobjThinker) // Not a mobj thinker
			continue;

		mo = (mobj_t*)think;

		if (botmo == mo)
			continue;
		switch(mo->type)
		{
			case MT_PLAYER:
				// If it's not REALLY a player or if it's not alive
				// just skip it... No point in chasing.
				if (!mo->player
				|| mo->player->playerstate != PST_LIVE
				|| mo->player->powers[pw_flashing]
				|| mo->player->powers[pw_invulnerability]
				|| mo->player->spectator)
					continue;
				// Spectator
				if (gametype == GT_CTF && !mo->player->ctfteam)
					continue;
				// Same team
				if (gametype == GT_CTF && mo->player->ctfteam == botmo->player->ctfteam)
					continue;
				if (gametype == GT_MATCH && cv_matchtype.value == 1
				&& mo->player->skincolor == botmo->player->skincolor)
					continue;
				if (gametype == GT_MATCH && cv_matchtype.value == 2
				&& mo->player->skin == botmo->player->skin)
					continue;
				// If the player is visible, go for it.
				if (P_CheckSight(botmo,mo))
					break;
				continue;

			case MT_RING:
			case MT_COIN:
			case MT_FLINGRING:
			case MT_FLINGCOIN:
			//case MT_HOMINGRING:
			case MT_RAILRING:
			//case MT_INFINITYRING:
			case MT_AUTOMATICRING:
			case MT_EXPLOSIONRING:
			case MT_EXTRALARGEBUBBLE: // Take bubbles too!
				// A ring?... Only take
				// it if noone's around!
				if ((!lastmo || lastmo->type != MT_PLAYER)
				&& mo->z < botmo->z + (128<<FRACBITS)
				&& P_CheckSight(botmo,mo))
					break;
				// Can't see it?! Too bad...
				continue;

			default:
				// Not anything I need to look at.
				continue;
		}

		dist = P_AproxDistance(P_AproxDistance(botmo->x - mo->x, botmo->y - mo->y), botmo->z - mo->z);
		if (lastmo && dist > lastdist) // Last one is closer to you?
			continue;

		// Found a target
		lastmo = mo;
		lastdist = dist;
	}
	return lastmo;
}

static mobj_t *JBC_Look4Spring(INT32 playernum)
{
	thinker_t* think;
	mobj_t *mo, *lastmo, *botmo = players[playernum].mo;
	fixed_t dist, lastdist;

	// Already springing? Don't look for another!
	if (botmo->state-states == S_PLAY_PLG1)
		return NULL;

	for(think = thinkercap.next, mo = lastmo = NULL, lastdist = 0;
		think != &thinkercap; think = think->next)
	{
		if (think->function.acp1 != (actionf_p1)P_MobjThinker) // Not a mobj thinker
			continue;

		mo = (mobj_t*)think;

		if (!(mo->flags & MF_SPRING)) // Not a spring...
			continue;

		// Can't get it if you can't see it!
		if (!P_CheckSight(botmo,mo))
			continue;

		dist = P_AproxDistance(P_AproxDistance(botmo->x - mo->x, botmo->y - mo->y), botmo->z - mo->z);
		if (lastmo && dist > lastdist) // Last one is closer to you?
			continue;

		// Found a target
		lastmo = mo;
		lastdist = dist;
	}
	return lastmo;
}

static mobj_t *JBC_Look4Air(INT32 playernum)
{
	thinker_t* think;
	mobj_t *mo, *lastmo, *botmo = players[playernum].mo;
	fixed_t dist, lastdist;

	for(think = thinkercap.next, mo = lastmo = NULL, lastdist = 0;
		think != &thinkercap; think = think->next)
	{
		if (think->function.acp1 != (actionf_p1)P_MobjThinker) // Not a mobj thinker
			continue;

		mo = (mobj_t*)think;

		// Ignore anything EXCEPT bubbles
		if (mo->type != MT_EXTRALARGEBUBBLE
		&& !(mo->flags & MF_SPRING) // Not spring...
		&& (mo->state->action.acp1 != (actionf_p1)A_BubbleSpawn // Not a bubble spawn...
		&& mo->state->action.acp1 != (actionf_p1)A_BubbleCheck))
			continue;

		// Can't get it if you can't see it!
		if (!P_CheckSight(botmo,mo))
			continue;

		dist = P_AproxDistance(P_AproxDistance(botmo->x - mo->x, botmo->y - mo->y), botmo->z - mo->z);
		if (lastmo && dist > lastdist) // Last one is closer to you?
			continue;

		// Found a target
		lastmo = mo;
		lastdist = dist;
	}
	return lastmo;
}

/////////////////////
// THINK FUNCTIONS //
/////////////////////

static void JBC_BotWander(INT32 playernum)
{
	player_t* player = &players[playernum];
	ticcmd_t* cmd = &players[playernum].cmd;

	cmd->angleturn = TICCMD_RECEIVED; // Thee halth recieved thy command!
	cmd->forwardmove = 50/NEWTICRATERATIO; // Go full speed. Always.

	// Ability stuff
	switch(player->charability)
	{
		// Thok
		case 0:
			if (!((player->mo->z <= player->mo->floorz) || (player->powers[pw_tailsfly])
				|| (player->mo->flags2 & (MF2_ONMOBJ))) && !(player->pflags & PF_JUMPDOWN) && player->mo->momz <= 0)
				cmd->buttons |= BT_JUMP;
			else if (((player->mo->z <= player->mo->floorz) || (player->powers[pw_tailsfly])
				|| (player->mo->flags2 & (MF2_ONMOBJ))) || ((player->pflags & PF_JUMPDOWN) && player->mo->momz > 0))
				cmd->buttons |= BT_JUMP;
			break;

		default:
			break;
	}
}

static void JBC_Jump4Air(INT32 playernum)
{
	player_t* player = &players[playernum];
	ticcmd_t* cmd = &players[playernum].cmd;

	cmd->angleturn = TICCMD_RECEIVED; // Thee halth recieved thy command!
	cmd->forwardmove = 0; // Don't bother moving.

	// Use your ability, whatever it is, at full jump height.
	if (!((player->mo->z <= player->mo->floorz) || (player->powers[pw_tailsfly])
		|| (player->mo->flags2 & (MF2_ONMOBJ))) && !(player->pflags & PF_JUMPDOWN) && player->mo->momz <= 0)
		cmd->buttons |= BT_JUMP;
	// Jump, jump, jump, as high as you can. You can't catch me, I'm in need of air, man!
	else if (((player->mo->z <= player->mo->floorz) || (player->powers[pw_tailsfly])
		|| (player->mo->flags2 & (MF2_ONMOBJ))) || ((player->pflags & PF_JUMPDOWN) && player->mo->momz > 0))
		cmd->buttons |= BT_JUMP;
}

void BotControl_Init(ticcmd_t *cmd)
{
	boolean aimed, abilityjump;

	// Client bot code
	player_t *player = &players[consoleplayer];

	// Bot info.
	fixed_t dist;
	angle_t angle;
	sector_t *nextsector;

	if ((cv_botcontrol.value
	&& !(gamestate == GS_INTRO || gamestate == GS_INTRO2
	|| gamestate == GS_TITLESCREEN || gamestate == GS_INTERMISSION))
	&& !paused
	)
	{

	// Targeting
	if (!player->mo->target || !player->mo->target->player || player->health <= 5)
	{
		P_SetTarget(&player->mo->target, NULL);
		// P_SetTarget(&player->mo->target, NULL);
		if (player->powers[pw_underwater]/* && player->powers[pw_underwater] < 15*TICRATE*/)
		{
			if (!P_SetTarget(&player->mo->target, JBC_Look4Air(consoleplayer))) // Uh oh... No air?! Try to jump as high as you can, then!
			{
				JBC_Jump4Air(consoleplayer);
				return;
			}
		}
		else if (player->health <= 10)
			P_SetTarget(&player->mo->target, JBC_Look4Collect(consoleplayer));
		else
		{
			if (P_SetTarget(&player->mo->target, JBC_Look4Players(consoleplayer))
			 && player->mo->target->state-states == S_PLAY_PLG1)
				P_SetTarget(&player->mo->target, JBC_Look4Spring(consoleplayer));
			else if (!player->mo->target)
				P_SetTarget(&player->mo->target, JBC_Look4Collect(consoleplayer));
		}
	}

	// No target?
	if (!player->mo->target)
	{
		JBC_BotWander(consoleplayer);
		return;
	}

	// Target info
	dist = P_AproxDistance(P_AproxDistance(
			player->mo->target->x - player->mo->x,
			player->mo->target->y - player->mo->y),
			player->mo->target->z - player->mo->z) / FRACUNIT;
	angle = R_PointToAngle2(player->mo->x, player->mo->y, player->mo->target->x, player->mo->target->y); // You facing target.
	nextsector = R_PointInSubsector(player->mo->x + player->mo->momx*2, player->mo->y + player->mo->momy*2)->sector;
	// localaiming = P_AimLineAttack(player->mo, player->mo->angle, 4096*FRACUNIT)<<FRACBITS;
	// P_PathTraverse(player->mo->x, player->mo->y, player->mo->target->x, player->mo->target->y, PT_ADDLINES|PT_ADDTHINGS, PTR_AimTraverse);

	// Turning movement
	aimed = false;
	cmd->angleturn = 0; // Command pending...
	if (!player->climbing)
	{
		if ((player->mo->angle - ANG10) - angle < angle - (player->mo->angle - ANG10))
			cmd->angleturn = (INT16)(cmd->angleturn - (2560/NEWTICRATERATIO)); // Turn right!
		else if ((player->mo->angle + ANG10) - angle > angle - (player->mo->angle + ANG10))
			cmd->angleturn = (INT16)(cmd->angleturn + (2560/NEWTICRATERATIO)); // Turn left!
		else if (JBC_AngleMove(player))
			aimed = true;
		player->mo->angle += (cmd->angleturn<<16); // Set the angle of your mobj
		cmd->angleturn = (INT16)(player->mo->angle>>16); // And set that to your turning. For some reason.
		localangle = player->mo->angle;
	}
	cmd->angleturn |= TICCMD_RECEIVED; // Thee halth recieved thy command!

	// Ability stuff
	abilityjump = false;
	switch(player->charability)
	{
		case 0: // Thok
			if (player->mo->target->flags & MF_SPRING // No thok over spring!
			|| player->mo->target->type == MT_EXTRALARGEBUBBLE
			|| player->health <= 5) // No thok over rings if no ammo!
				break;
			// Thok!
			else if (aimed && !((player->mo->z <= player->mo->floorz) || (player->powers[pw_tailsfly])) && !(player->pflags & PF_JUMPDOWN) && player->mo->momz <= 0)
				cmd->buttons |= BT_JUMP;
			// Jump to full height!
			else if ((!(player->pflags & PF_JUMPDOWN) && ((player->mo->z <= player->mo->floorz) || (player->powers[pw_tailsfly]))) || ((player->pflags & PF_JUMPDOWN) && player->mo->momz > 0))
				cmd->buttons |= BT_JUMP;
			// Ready the jump button!
			else
				cmd->buttons &= ~BT_JUMP;
			// Ability is controlling jump button! MWAHAHAHA!
			abilityjump = true;
			break;

		case 1: // Fly
		case 7: // Swim
			if (player->mo->target->flags & MF_SPRING // No fly over spring!
			|| (player->charability == 7 // No swim out of water!
				&& !(player->mo->eflags & MFE_UNDERWATER))
			|| player->health <= 10) // No snipe without ammo!
					break;
			// Fly!
			else if (!((player->mo->z <= player->mo->floorz) || (player->powers[pw_tailsfly])) && !(player->pflags & PF_JUMPDOWN) && player->mo->momz <= 0)
				cmd->buttons |= BT_JUMP;
			// Jump to full height!
			else if ((!(player->pflags & PF_JUMPDOWN) && ((player->mo->z <= player->mo->floorz) || (player->powers[pw_tailsfly]))) || ((player->pflags & PF_JUMPDOWN) && player->mo->momz > 0))
				cmd->buttons |= BT_JUMP;
			// Ready the jump button!
			else
				cmd->buttons &= ~BT_JUMP;
			// Ability is controlling jump button! MWAHAHAHA!
			abilityjump = true;
			break;

		case 2: // Glide and climb
		case 3: // Glide with no climb
			if (player->mo->target->flags & MF_SPRING // No glide over spring!
			|| player->mo->target->type == MT_EXTRALARGEBUBBLE)
				break;
			if (player->mo->target->z > player->mo->z // Target still above you
			&& !((player->mo->z <= player->mo->floorz) || (player->powers[pw_tailsfly])) && !(player->pflags & PF_JUMPDOWN) // You're in the air but not holding the jump button
			&& player->mo->momz <= 0) // You aren't gonna get high enough
			{ // So what do you do? Glide!... I dunno.
				cmd->buttons |= BT_JUMP;
				abilityjump = true;
			}
			break;

		case 4: // Double-Jump
			// Jump again at top of jump height!
			if (!((player->mo->z <= player->mo->floorz) || (player->powers[pw_tailsfly])) && !(player->pflags & PF_JUMPDOWN) && player->mo->momz <= 0)
				cmd->buttons |= BT_JUMP;
			break;

		default:
			break;
	}

	// Forward movement.
	if (player->mo->momz > 0 && !(player->pflags & PF_JUMPDOWN) // If you're bouncing on a spring...
		&& player->mo->state == &states[S_PLAY_PLG1]) // And you're already moving in a direction from it...
			cmd->forwardmove = 0; // Do nothing. Moving could ruin it.
	else if (nextsector->special == 4 // If the next sector is HARMFUL to you...
		|| nextsector->special == 5
		|| nextsector->special == 7
		|| nextsector->special == 9
		|| nextsector->special == 11
		|| nextsector->special == 16
		|| nextsector->special == 18
		|| nextsector->special == 519
		|| nextsector->special == 978
		|| nextsector->special == 980
		|| nextsector->special == 983
		|| nextsector->special == 984)
			cmd->forwardmove = -50/NEWTICRATERATIO; // STOP RUNNING TWARDS IT! AGH!
	else if (!aimed && !player->mo->target->player) // If you're not aimed properly at something that isn't a person...
		cmd->forwardmove = 25/NEWTICRATERATIO; // Start slowing down.
	else // Otherwise...
		cmd->forwardmove = 50/NEWTICRATERATIO; // Go full speed. Always.

	// (player->pflags & PF_JUMPDOWN) stuff
	if (abilityjump) // Ability has changed the state of your jump button already?
		; // Then don't mess with it!
	else if (!((player->mo->z <= player->mo->floorz) || (player->powers[pw_tailsfly])) && !(player->pflags & PF_JUMPDOWN)) // In the air but not holding the jump button?
		cmd->buttons &= ~BT_JUMP; // Don't press it again, then.
	else if (nextsector->floorheight > player->mo->z // If the next sector is above you...
	&& nextsector->floorheight - player->mo->z < 128*FRACUNIT) // And you can jump up on it...
		cmd->buttons |= BT_JUMP; // Then jump!
	else if (player->mo->target->z > player->mo->z // If your target's still above you...
		&& (player->pflags & PF_JUMPDOWN) // And you're already holding the jump button...
		&& player->mo->momz > 0) // And you're (player->pflags & PF_JUMPDOWN) and still going up...
			cmd->buttons |= BT_JUMP; // Continue to do so!
	else if (player->mo->target->z > player->mo->z + player->mo->height // If your target is above your head...
		&& !(player->pflags & PF_JUMPDOWN) // And you're not (player->pflags & PF_JUMPDOWN) already...
		&& player->mo->target->state != &states[S_PLAY_PLG1]) // And they didn't just fly off a spring...
			cmd->buttons |= BT_JUMP; // Then jump!
	else if ((player->mo->target->flags & MF_ENEMY // If the target
		|| player->mo->target->flags & MF_BOSS // NEEDS to be popped...
		|| player->mo->target->flags & MF_MONITOR)
		&& dist < 128 // And you're getting close to it...
		&& !(player->pflags & PF_JUMPDOWN)) // And you're not (player->pflags & PF_JUMPDOWN) already...
			cmd->buttons |= BT_JUMP; // Then jump!
	else if (player->playerstate == PST_DEAD // If you died.
		&& player->mo->z + player->mo->height < player->mo->floorz) // Wait to be completely under the floor
			cmd->buttons |= BT_JUMP; // Then jump to recover!
	else // Otherwise...
		cmd->buttons &= ~BT_JUMP; // I guess you shouldn't be (player->pflags & PF_JUMPDOWN), then...

	// Shooting stuff
	if (cmd->buttons & BT_ATTACK) // If you're holding the button down...
		cmd->buttons &= ~BT_ATTACK; // DO NOT HOLD THE BUTTON DOWN!
	else if (aimed // If you're properly aimed...
	&& player->mo->target->player // At a player...
	&& player->health > 2) // And you have at least one ring to spare...
		cmd->buttons |= BT_ATTACK; // Fire away!
	// PTR_AimTraverse

	} // End the Autotarget statement
}

#endif
