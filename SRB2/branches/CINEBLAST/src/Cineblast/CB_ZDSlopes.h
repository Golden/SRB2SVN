/*
 *  CB_ZDSlopes.h
 *  SRB2Cineblast
 *
 *  Imported from ZDoom, Copyright Randy Heit
 *  Imported for SRB2 by Kalaron and Conic
 *
 */

#ifndef __CB_ZDSlopes__
#define __CB_ZDSlopes__

#include "../doomdef.h"
#include "../r_defs.h"

#ifdef ZSLOPE

void P_AlignPlane(sector_t * sec, line_t * line, INT32 which);

#endif // ZSLOPE
#endif
