/*
 *  CB_JBControl.h
 *  SRB2Cineblast
 *
 *  Thanks to: JTE, Flame.
 *
 */

#ifndef __CB_JBControl__
#define __CB_JBControl__

void CB_BCInit(void);
void BotControl_Init(ticcmd_t *cmd);
extern consvar_t cv_botcontrol;
#endif
