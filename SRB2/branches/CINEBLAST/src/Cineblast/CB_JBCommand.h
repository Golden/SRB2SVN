/*
 *  CB_JBCommand.h
 *  SRB2Cineblast
 *
 *  Imported from SRB2JTE, Copyright Jason the Echidna
 *
 */

#ifdef JTEBOTS
#ifndef __JB_COMMAND__
#define __JB_COMMAND__

void JB_Init(void);

extern boolean jb_cmdwait;
extern consvar_t cv_maxbots;
extern consvar_t cv_botcanflyme;
extern consvar_t cv_botcoopai;

#endif
#endif
