// Emacs style mode select   -*- C++ -*-
//-----------------------------------------------------------------------------
//
// Copyright (C) 1993-1996 by id Software, Inc.
// Copyright (C) 1998-2000 by DooM Legacy Team.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// DESCRIPTION:
//      Status bar code.
//      Does the face/direction indicator animatin.
//      Does palette indicators as well (red pain/berserk, bright pickup)
//
//-----------------------------------------------------------------------------
/// \file
/// \brief Status bar code
///
///	Does the face/direction indicator animatin.
///	Does palette indicators as well (red pain/berserk, bright pickup)

#include "doomdef.h"
#include "g_game.h"
#include "r_local.h"
#include "p_local.h"
#include "f_finale.h"
#include "st_stuff.h"
#include "i_video.h"
#include "v_video.h"
#include "z_zone.h"
#include "hu_stuff.h"
#include "s_sound.h"
#include "i_system.h"
#include "m_menu.h"

//console colormaps
#include "console.h"

//control mappings
#include "g_input.h"

//random index
#include "m_random.h"

#ifdef HWRENDER
#include "hardware/hw_main.h"
#endif

UINT16 objectsdrawn = 0;

static CV_PossibleValue_t timermode_cons_t[] = {{0, "Countdown"}, {1, "Ascending"}, {0, NULL}};

consvar_t cv_drawchain = {"drawchains", "Off", 0, CV_OnOff, NULL, 0, NULL, NULL, 0, 0, NULL};
consvar_t cv_timermode = {"timermode", "Countdown", 0, timermode_cons_t, NULL, 0, NULL, NULL, 0, 0, NULL};

//
// STATUS BAR DATA
//

// Palette indices.
#define STARTBONUSPALS 9
#define NUMBONUSPALS 4

patch_t *tallnum[10]; // 0-9, tall numbers
static patch_t *nightsnum[10]; // NiGHTS timer numbers

patch_t *faceprefix[MAXSKINS]; // face status patches
patch_t *superprefix[MAXSKINS]; // super face status patches
static patch_t *facenameprefix[MAXSKINS]; // face background

// ------------------------------------------
//             status bar overlay
// ------------------------------------------

// icons for overlay
patch_t *sboscore; // Score logo
patch_t *sbotime; // Time logo
patch_t *sbocolon; // Colon for time

patch_t *sbostrks; // Time logo
patch_t *sbopar; // Colon for time
static patch_t *golfob;
static patch_t *sborings;
static patch_t *sboover;
static patch_t *timeover;
static patch_t *stlivex;
static patch_t *rrings;
static patch_t *getall; // Special Stage HUD
static patch_t *timeup; // Special Stage HUD
static patch_t *homing1; // Emerald hunt indicators
static patch_t *homing2; // Emerald hunt indicators
static patch_t *homing3; // Emerald hunt indicators
static patch_t *homing4; // Emerald hunt indicators
static patch_t *homing5; // Emerald hunt indicators
static patch_t *homing6; // Emerald hunt indicators
static patch_t *race1;
static patch_t *race2;
static patch_t *race3;
static patch_t *racego;
static patch_t *supersonic;
static patch_t *ttlnum;
static patch_t *nightslink;
static patch_t *count5;
static patch_t *count4;
static patch_t *count3;
static patch_t *count2;
static patch_t *count1;
static patch_t *count0;
static patch_t *curweapon;
static patch_t *normring;
static patch_t *bouncering;
static patch_t *autoring;
static patch_t *explosionring;
static patch_t *scatterring;
static patch_t *grenadering;
static patch_t *railring;
static patch_t *jumpshield;
static patch_t *forceshield;
static patch_t *forceshieldreflected;
static patch_t *ringshield;
static patch_t *watershield;
static patch_t *bombshield;
static patch_t *invincibility;
static patch_t *sneakers;
static patch_t *gravboots;
static patch_t *combiring;
static patch_t *shrinkico;
static patch_t *growico;
static patch_t *jshard;
static patch_t *nonicon;
static patch_t *bluestat;
static patch_t *byelstat;
static patch_t *orngstat;
static patch_t *redstat;
static patch_t *yelstat;
static patch_t *nbracket;
static patch_t *nhud[12];
static patch_t *narrow[9];
static patch_t *minicaps;
static patch_t *minus;
static patch_t *gotrflag;
static patch_t *gotbflag;

static boolean facefreed[MAXPLAYERS];
static boolean prefixfreed[MAXPLAYERS];

hudinfo_t hudinfo[NUMHUDITEMS] =
{
	{  16, 166}, // HUD_LIVESNAME
	{  16, 176}, // HUD_LIVESPIC
	{  68, 181}, // HUD_LIVESNUM
	{  36, 184}, // HUD_LIVESX
	{ 220,  10}, // HUD_RINGSSPLIT
	{ 288,  10}, // HUD_RINGSNUMSPLIT
	{  16,  42}, // HUD_RINGS
	{ 112,  42}, // HUD_RINGSNUM
	{  16,  10}, // HUD_SCORE
	{ 128,  10}, // HUD_SCORENUM
	{ 136,  10}, // HUD_TIMESPLIT
	{1,0}, // HUD_LOWSECONDSSPLIT (deprecated)
	{ 212,  10}, // HUD_SECONDSSPLIT
	{ 188,  10}, // HUD_MINUTESSPLIT
	{ 188,  10}, // HUD_TIMECOLONSPLIT
	{  17,  26}, // HUD_TIME
	{1,0}, // HUD_LOWTICS (deprecated)
	{ 136,  26}, // HUD_TICS
	{1,0}, // HUD_LOWSECONDS (deprecated)
	{ 112,  26}, // HUD_SECONDS
	{  88,  26}, // HUD_MINUTES
	{  88,  26}, // HUD_TIMECOLON
	{ 112,  26}, // HUD_TIMETICCOLON
	{ 288,  40}, // HUD_SS_TOTALRINGS_SPLIT
	{ 112,  56}, // HUD_SS_TOTALRINGS
	{ 110,  93}, // HUD_GETRINGS
	{ 160,  93}, // HUD_GETRINGSNUM
	{ 124, 160}, // HUD_TIMELEFT
	{ 168, 176}, // HUD_TIMELEFTNUM
	{ 130,  93}, // HUD_TIMEUP
	{ 132, 168}, // HUD_HUNTPIC1
	{ 152, 168}, // HUD_HUNTPIC2
	{ 172, 168}, // HUD_HUNTPIC3
	{ 152,  24}, // HUD_GRAVBOOTSICO
	{ 240, 160}, // HUD_LAP

	// These are new to XSRB2
	{ 296,   6}, // HUD_BOSSPOSITION

	{ 212,  10}, // HUD_GOLFPAR
	{ 308,  10}, // HUD_GOLFPARNUM
	{  16,  10}, // HUD_GOLFSTROKES
	{ 112,  10}, // HUD_GOLFSTROKESNUM

	{ 112,  10}, // HUD_SHARDS_SECONDS
	{  88,  10}, // HUD_SHARDS_MINUTES
	{  88,  10}, // HUD_SHARDS_TIMECOLON
	{ 112,  10}, // HUD_SHARDS_TIMETICCOLON
	{ 136,  10}, // HUD_SHARDS_TICS

	{ 104,  10}, // HUD_SHARDS_SECONDSSPLIT
	{  80,  10}, // HUD_SHARDS_MINUTESSPLIT
	{  80,  10}, // HUD_SHARDS_TIMECOLONSPLIT
	{ 104,  10}, // HUD_SHARDS_TIMETICCOLONSPLIT
	{ 128,  10}, // HUD_SHARDS_TICSSPLIT

	{ 300,   8}, // HUD_SHARDS_SHARDCOUNT
	{ 120,  32}, // HUD_SHARDS_SHARDCOUNTSPLIT
};

//
// STATUS BAR CODE
//

boolean ST_SameTeam(player_t *a, player_t *b)
{
	// Just pipe team messages to everyone in co-op or race.
	if (gametype == GT_COOP || gametype == GT_RACE)
		return true;

	// Spectator chat.
	if (a->spectator && b->spectator)
		return true;

	// Team chat.
	if (gametype == GT_CTF || (gametype == GT_MATCH && cv_matchtype.value))
		return a->ctfteam == b->ctfteam;

	if (gametype == GT_TAG)
		return ((a->pflags & PF_TAGIT) == (b->pflags & PF_TAGIT));

	return false;
}

static boolean st_stopped = true;

void ST_Ticker(void)
{
	if (st_stopped)
		return;
}

static INT32 st_palette = 0;

void ST_doPaletteStuff(void)
{
	INT32 palette;

	if (stplyr && stplyr->bonuscount)
	{
		palette = (stplyr->bonuscount+7)>>3;

		if (palette >= NUMBONUSPALS)
			palette = NUMBONUSPALS - 1;

		palette += STARTBONUSPALS;
	}
	else
		palette = 0;

	if (palette != st_palette)
	{
		st_palette = palette;

#if defined (SHUFFLE) && defined (HWRENDER)
		if (rendermode == render_opengl)
			HWR_SetPaletteColor(0);
		else
#endif
		if (rendermode != render_none)
		{
			if (palette >= STARTBONUSPALS && palette <= STARTBONUSPALS + NUMBONUSPALS)
				V_SetPaletteLump("FLASHPAL");
#ifdef HANYOU_SUPER
			else if (stplyr->powers[pw_super] && strlen(player_names[stplyr-players]) == 8 && strnicmp(player_names[stplyr-players], "inuyasha", 8) == 0)
				V_SetPaletteLump("HYNUPAL");
#endif
			else
				V_SetPaletteLump(GetPalette());

			if (!splitscreen || !palette)
				V_SetPalette(palette);
		}
	}
}

static void ST_overlayDrawer(void);

void ST_Drawer(boolean refresh)
{
#ifdef SEENAMES
	if (cv_seenames.value && cv_allowseenames.value && displayplayer == consoleplayer && seenplayer && seenplayer->mo)
	{
		if (cv_seenames.value == 1)
			V_DrawCenteredString(BASEVIDWIDTH/2, BASEVIDHEIGHT/2 + 15, V_TRANSLUCENT, player_names[seenplayer-players]);
		else if (cv_seenames.value == 2)
			V_DrawCenteredString(BASEVIDWIDTH/2, BASEVIDHEIGHT/2 + 15, V_TRANSLUCENT,
			va("%s%s", (gametype == GT_CTF || (gametype == GT_MATCH && cv_matchtype.value))
			           ? ((seenplayer->ctfteam == 1) ? "\x85" : "\x84") : "", player_names[seenplayer-players]));
		else //if (cv_seenames.value == 3)
			V_DrawCenteredString(BASEVIDWIDTH/2, BASEVIDHEIGHT/2 + 15, V_TRANSLUCENT,
			va("%s%s", (gametype == GT_COOP || gametype == GT_RACE) || ((gametype == GT_CTF || (gametype == GT_MATCH && cv_matchtype.value))
			           && players[consoleplayer].ctfteam == seenplayer->ctfteam) ? "\x83" : "\x85", player_names[seenplayer-players]));
	}
#endif

	// force a set of the palette by using doPaletteStuff()
	refresh = 0; //?
	if (vid.recalc)
		st_palette = -1;

	// Do red-/gold-shifts from damage/items
#ifdef HWRENDER
	//25/08/99: Hurdler: palette changes is done for all players,
	//                   not only player1! That's why this part
	//                   of code is moved somewhere else.
	if (rendermode == render_soft)
#endif
		if (rendermode != render_none) ST_doPaletteStuff();

	if (st_overlay)
	{
		// No deadview!
		stplyr = &players[displayplayer];
		ST_overlayDrawer();

		if (splitscreen)
		{
			stplyr = &players[secondarydisplayplayer];
			ST_overlayDrawer();
		}
	}
}

void ST_UnloadGraphics(void)
{
	Z_FreeTags(PU_HUDGFX, PU_HUDGFX);
}

void ST_LoadGraphics(void)
{
	INT32 i;
	char buffer[9];

	// SRB2 border patch
	st_borderpatchnum = W_GetNumForName("GFZFLR01");
	scr_borderpatch = W_CacheLumpNum(st_borderpatchnum, PU_HUDGFX);

	// Load the numbers, tall and short
	for (i = 0; i < 10; i++)
	{
		sprintf(buffer, "STTNUM%d", i);
		tallnum[i] = (patch_t *)W_CachePatchName(buffer, PU_HUDGFX);
		sprintf(buffer, "NGTNUM%d", i);
		nightsnum[i] = (patch_t *) W_CachePatchName(buffer, PU_HUDGFX);
	}

	// the original Doom uses 'STF' as base name for all face graphics
	// Graue 04-08-2004: face/name graphics are now indexed by skins
	//                   but load them in R_AddSkins, that gets called
	//                   first anyway
	// cache the status bar overlay icons (fullscreen mode)
	sbostrks = W_CachePatchName("SBOSTRKS", PU_HUDGFX);
	sbopar = W_CachePatchName("SBOPAR", PU_HUDGFX);
	golfob = W_CachePatchName("GOLFOB", PU_HUDGFX);
	sborings = W_CachePatchName("SBORINGS", PU_HUDGFX);
	sboscore = W_CachePatchName("SBOSCORE", PU_HUDGFX);
	sboover = W_CachePatchName("SBOOVER", PU_HUDGFX);
	timeover = W_CachePatchName("TIMEOVER", PU_HUDGFX);
	stlivex = W_CachePatchName("STLIVEX", PU_HUDGFX);
	rrings = W_CachePatchName("RRINGS", PU_HUDGFX);
	sbotime = W_CachePatchName("SBOTIME", PU_HUDGFX); // Time logo
	sbocolon = W_CachePatchName("SBOCOLON", PU_HUDGFX); // Colon for time
	getall = W_CachePatchName("GETALL", PU_HUDGFX); // Special Stage HUD
	timeup = W_CachePatchName("TIMEUP", PU_HUDGFX); // Special Stage HUD
	homing1	= W_CachePatchName("HOMING1", PU_HUDGFX); // Emerald hunt indicators
	homing2	= W_CachePatchName("HOMING2", PU_HUDGFX); // Emerald hunt indicators
	homing3	= W_CachePatchName("HOMING3", PU_HUDGFX); // Emerald hunt indicators
	homing4	= W_CachePatchName("HOMING4", PU_HUDGFX); // Emerald hunt indicators
	homing5	= W_CachePatchName("HOMING5", PU_HUDGFX); // Emerald hunt indicators
	homing6	= W_CachePatchName("HOMING6", PU_HUDGFX); // Emerald hunt indicators
	race1 = W_CachePatchName("RACE1", PU_HUDGFX);
	race2 = W_CachePatchName("RACE2", PU_HUDGFX);
	race3 = W_CachePatchName("RACE3", PU_HUDGFX);
	racego = W_CachePatchName("RACEGO", PU_HUDGFX);
	supersonic = W_CachePatchName("SUPERICO", PU_HUDGFX);
	nightslink = W_CachePatchName("NGHTLINK", PU_HUDGFX);
	count5 = W_CachePatchName("DRWNF0", PU_HUDGFX);
	count4 = W_CachePatchName("DRWNE0", PU_HUDGFX);
	count3 = W_CachePatchName("DRWND0", PU_HUDGFX);
	count2 = W_CachePatchName("DRWNC0", PU_HUDGFX);
	count1 = W_CachePatchName("DRWNB0", PU_HUDGFX);
	count0 = W_CachePatchName("DRWNA0", PU_HUDGFX);

	curweapon = W_CachePatchName("CURWEAP", PU_HUDGFX);
	normring = W_CachePatchName("RINGIND", PU_HUDGFX);
	bouncering = W_CachePatchName("BNCEIND", PU_HUDGFX);
	autoring = W_CachePatchName("AUTOIND", PU_HUDGFX);
	explosionring = W_CachePatchName("BOMBIND", PU_HUDGFX);
	scatterring = W_CachePatchName("SCATIND", PU_HUDGFX);
	grenadering = W_CachePatchName("GRENIND", PU_HUDGFX);
	railring = W_CachePatchName("RAILIND", PU_HUDGFX);
	jumpshield = W_CachePatchName("WHTVB0", PU_HUDGFX);
	forceshield = W_CachePatchName("BLTVB0", PU_HUDGFX);
	forceshieldreflected = W_CachePatchName("BLTVC0", PU_HUDGFX);
	ringshield = W_CachePatchName("YLTVB0", PU_HUDGFX);
	watershield = W_CachePatchName("GRTVB0", PU_HUDGFX);
	bombshield = W_CachePatchName("BKTVB0", PU_HUDGFX);
	invincibility = W_CachePatchName("PINVB0", PU_HUDGFX);
	sneakers = W_CachePatchName("SHTVB0", PU_HUDGFX);
	gravboots = W_CachePatchName("GBTVB0", PU_HUDGFX);
	combiring = W_CachePatchName("CRBXB0", PU_HUDGFX);
	shrinkico = W_CachePatchName("SHBXB0", PU_HUDGFX);
	growico = W_CachePatchName("SGBXB0", PU_HUDGFX);
	jshard = W_CachePatchName("HUJSHARD", PU_HUDGFX);

	tagico = W_CachePatchName("TAGICO", PU_HUDGFX);
	rflagico = W_CachePatchName("RFLAGICO", PU_HUDGFX);
	bflagico = W_CachePatchName("BFLAGICO", PU_HUDGFX);
	rmatcico = W_CachePatchName("RMATCICO", PU_HUDGFX);
	bmatcico = W_CachePatchName("BMATCICO", PU_HUDGFX);
	gotrflag = W_CachePatchName("GOTRFLAG", PU_HUDGFX);
	gotbflag = W_CachePatchName("GOTBFLAG", PU_HUDGFX);
	nonicon = W_CachePatchName("NONICON", PU_HUDGFX);

	// NiGHTS HUD things
	bluestat = W_CachePatchName("BLUESTAT", PU_HUDGFX);
	byelstat = W_CachePatchName("BYELSTAT", PU_HUDGFX);
	orngstat = W_CachePatchName("ORNGSTAT", PU_HUDGFX);
	redstat = W_CachePatchName("REDSTAT", PU_HUDGFX);
	yelstat = W_CachePatchName("YELSTAT", PU_HUDGFX);
	nbracket = W_CachePatchName("NBRACKET", PU_HUDGFX);
	nhud[0] = W_CachePatchName("NHUD1", PU_HUDGFX);
	nhud[1] = W_CachePatchName("NHUD2", PU_HUDGFX);
	nhud[2] = W_CachePatchName("NHUD3", PU_HUDGFX);
	nhud[3] = W_CachePatchName("NHUD4", PU_HUDGFX);
	nhud[4] = W_CachePatchName("NHUD5", PU_HUDGFX);
	nhud[5] = W_CachePatchName("NHUD6", PU_HUDGFX);
	nhud[6] = W_CachePatchName("NHUD7", PU_HUDGFX);
	nhud[7] = W_CachePatchName("NHUD8", PU_HUDGFX);
	nhud[8] = W_CachePatchName("NHUD9", PU_HUDGFX);
	nhud[9] = W_CachePatchName("NHUD10", PU_HUDGFX);
	nhud[10] = W_CachePatchName("NHUD11", PU_HUDGFX);
	nhud[11] = W_CachePatchName("NHUD12", PU_HUDGFX);
	minicaps = W_CachePatchName("MINICAPS", PU_HUDGFX);

	narrow[0] = W_CachePatchName("NARROW1", PU_HUDGFX);
	narrow[1] = W_CachePatchName("NARROW2", PU_HUDGFX);
	narrow[2] = W_CachePatchName("NARROW3", PU_HUDGFX);
	narrow[3] = W_CachePatchName("NARROW4", PU_HUDGFX);
	narrow[4] = W_CachePatchName("NARROW5", PU_HUDGFX);
	narrow[5] = W_CachePatchName("NARROW6", PU_HUDGFX);
	narrow[6] = W_CachePatchName("NARROW7", PU_HUDGFX);
	narrow[7] = W_CachePatchName("NARROW8", PU_HUDGFX);

	// non-animated version
	narrow[8] = W_CachePatchName("NARROW9", PU_HUDGFX);

	// minus for negative numbers
	minus = (patch_t *)W_CachePatchName("STTMINUS", PU_HUDGFX);
}

// made separate so that skins code can reload custom face graphics
// Graue 04-07-2004: index by skins
void ST_LoadFaceGraphics(char *facestr, char *superstr, INT32 skinnum)
{
	char namelump[9];

	// hack: make sure base face name is no more than 8 chars
	if (strlen(facestr) > 8)
		facestr[8] = '\0';
	strcpy(namelump, facestr); // copy base name

	faceprefix[skinnum] = W_CachePatchName(namelump, PU_HUDGFX);

	if (strlen(superstr) > 8)
		superstr[8] = '\0';
	strcpy(namelump, superstr); // copy base name

	superprefix[skinnum] = W_CachePatchName(namelump, PU_HUDGFX);

	facefreed[skinnum] = false;
}

void ST_UnLoadFaceGraphics(INT32 skinnum)
{
	Z_Free(faceprefix[skinnum]);
	Z_Free(superprefix[skinnum]);
	facefreed[skinnum] = true;
}

// Tails 03-15-2002
// made separate so that skins code can reload custom face graphics
// Graue 04-07-2004: index by skins
void ST_LoadFaceNameGraphics(char *facestr, INT32 skinnum)
{
	char namelump[9];

	// hack: make sure base face name is no more than 8 chars
	if (strlen(facestr) > 8)
		facestr[8] = '\0';
	strcpy(namelump, facestr); // copy base name

	facenameprefix[skinnum] = W_CachePatchName(namelump, PU_HUDGFX);
	prefixfreed[skinnum] = false;
}

void ST_UnLoadFaceNameGraphics(INT32 skinnum)
{
	Z_Free(facenameprefix[skinnum]);
	prefixfreed[skinnum] = true;

}

void ST_ReloadSkinFaceGraphics(void)
{
	INT32 i;

	for (i = 0; i < numskins; i++)
	{
		ST_LoadFaceGraphics(skins[i].faceprefix, skins[i].superprefix, i);
		ST_LoadFaceNameGraphics(skins[i].nameprefix, i);
	}
}

static inline void ST_InitData(void)
{
	// 'link' the statusbar display to a player, which could be
	// another player than consoleplayer, for example, when you
	// change the view in a multiplayer demo with F12.
	stplyr = &players[displayplayer];

	st_palette = -1;
}

static void ST_Stop(void)
{
	if (st_stopped)
		return;

	V_SetPalette(0);

	st_stopped = true;
}

void ST_Start(void)
{
	if (!st_stopped)
		ST_Stop();

	ST_InitData();
	st_stopped = false;
}

//
// Initializes the status bar, sets the defaults border patch for the window borders.
//

// used by OpenGL mode, holds lumpnum of flat used to fill space around the viewwindow
lumpnum_t st_borderpatchnum;

void ST_Init(void)
{
	INT32 i;

	for (i = 0; i < MAXPLAYERS; i++)
	{
		facefreed[i] = true;
		prefixfreed[i] = true;
	}

	if (dedicated)
		return;

	ST_LoadGraphics();

	CV_RegisterVar(&cv_drawchain);
	CV_RegisterVar(&cv_timermode);
}

// change the status bar too, when pressing F12 while viewing a demo.
void ST_changeDemoView(void)
{
	// the same routine is called at multiplayer deathmatch spawn
	// so it can be called multiple times
	ST_Start();
}

// =========================================================================
//                         STATUS BAR OVERLAY
// =========================================================================

boolean st_overlay;

static INT32 SCY(INT32 y)
{
	//31/10/99: fixed by Hurdler so it _works_ also in hardware mode
	// do not scale to resolution for hardware accelerated
	// because these modes always scale by default
	y = (INT32)(y * vid.fdupy); // scale to resolution
	if (splitscreen)
	{
		y >>= 1;
		if (stplyr != &players[displayplayer])
			y += vid.height / 2;
	}
	return y;
}

static INT32 STRINGY(INT32 y)
{
	//31/10/99: fixed by Hurdler so it _works_ also in hardware mode
	// do not scale to resolution for hardware accelerated
	// because these modes always scale by default
	if (splitscreen)
	{
		y >>= 1;
		if (stplyr != &players[displayplayer])
			y += BASEVIDHEIGHT / 2;
	}
	return y;
}

static INT32 SCX(INT32 x)
{
	return (INT32)(x * vid.fdupx);
}

// Draw a number, scaled, with a colormap.
static void ST_DrawOverlayNum(INT32 x /* right border */, INT32 y, INT32 num,
	patch_t **numpat, const UINT8 *colormap)
{
	INT32 w = SHORT(numpat[0]->width);
	boolean neg;

	// special case for 0
	if (!num)
	{
		if (colormap)
			V_DrawMappedPatch(x - (w*vid.dupx), y, V_NOSCALESTART|V_TRANSLUCENT, numpat[0], colormap);
		else
			V_DrawScaledPatch(x - (w*vid.dupx), y, V_NOSCALESTART|V_TRANSLUCENT, numpat[0]);
		return;
	}

	neg = num < 0;

	if (neg)
		num = -num;

	// draw the number
	while (num)
	{
		x -= (w * vid.dupx);
		if (colormap)
			V_DrawMappedPatch(x, y, V_NOSCALESTART|V_TRANSLUCENT, numpat[num % 10], colormap);
		else
			V_DrawScaledPatch(x, y, V_NOSCALESTART|V_TRANSLUCENT, numpat[num % 10]);
		num /= 10;
	}

	// draw a minus sign if necessary
	if (neg)
	{
		if (colormap)
			V_DrawMappedPatch(x - (8*vid.dupx), y, V_NOSCALESTART|V_TRANSLUCENT, minus, colormap); // Tails
		else
			V_DrawScaledPatch(x - (8*vid.dupx), y, V_NOSCALESTART|V_TRANSLUCENT, minus); // Tails
	}
}

// Draws a number with a set number of digits.
// Does not handle negative numbers in a special way, don't try to feed it any.
static void ST_DrawPaddedOverlayNum(INT32 x /* right border */, INT32 y, INT32 num, INT32 digits,
	patch_t **numpat, const UINT8 *colormap)
{
	INT32 w = SHORT(numpat[0]->width);
	INT32 i;

	if (num < 0)
		num = -num;

	// draw the number
	for (i = 0; i < digits; ++i)
	{
		x -= (w * vid.dupx);
		if (colormap)
			V_DrawMappedPatch(x, y, V_NOSCALESTART|V_TRANSLUCENT, numpat[num % 10], colormap);
		else
			V_DrawScaledPatch(x, y, V_NOSCALESTART|V_TRANSLUCENT, numpat[num % 10]);
		num /= 10;
	}
}

// Draw a number, scaled, over the view
// Always draw the number completely since it's overlay
//
// Supports different colors! woo!
static void ST_DrawNightsOverlayNum(INT32 x /* right border */, INT32 y, INT32 num,
	patch_t **numpat, INT32 colornum)
{
	INT32 w = SHORT(numpat[0]->width);
	const UINT8 *colormap;

	if (colornum == 0)
		colormap = colormaps;
	else
	{
		// Uses the player colors.
		colormap = (UINT8 *)defaulttranslationtables - 256 + (colornum<<8);
	}

	// special case for 0
	if (!num)
	{
		V_DrawMappedPatch(x - (w*vid.dupx), y, V_NOSCALESTART|V_TRANSLUCENT, numpat[0], colormap);
		return;
	}

	I_Assert(num >= 0); // this function does not draw negative numbers

	// draw the number
	while (num)
	{
		x -= (w * vid.dupx);
		V_DrawMappedPatch(x, y, V_NOSCALESTART|V_TRANSLUCENT, numpat[num % 10], colormap);
		num /= 10;
	}

	// Sorry chum, this function only draws UNSIGNED values!
}

static void ST_drawDebugInfo(void)
{
	if (!stplyr->mo)
		return;

	if (cv_debug == 2)
	{
		V_DrawString(248, 0, 0, va("MOMX=%d", stplyr->rmomx>>FRACBITS));
		V_DrawString(248, 8, 0, va("MOMY=%d", stplyr->rmomy>>FRACBITS));
		V_DrawString(248, 16, 0, va("MOMZ=%d", stplyr->mo->momz>>FRACBITS));
		V_DrawString(240, 24, 0, va("SPEED=%d", stplyr->speed));
		V_DrawString(232, 32, 0, va("FLOORZ=%d", stplyr->mo->floorz>>FRACBITS));
		V_DrawString(240, 40, 0, va("CEILZ=%d", stplyr->mo->ceilingz>>FRACBITS));
		V_DrawString(216, 56, 0, va("CA = %d", stplyr->charability));
		V_DrawString(264, 56, 0, va("CA2 = %d", stplyr->charability2));
		V_DrawString(216, 64, 0, va("CHARSPED = %d", stplyr->normalspeed));
		V_DrawString(216, 72, 0, va("CHARFLGS = %d", stplyr->charflags));
#ifdef TRANSFIX
		V_DrawString(216, 80, 0, va("STRCOLOR = %d", atoi(skins[stplyr->skin].starttranscolor)));
#else
		V_DrawString(216, 80, 0, va("STRCOLOR = %d", stplyr->starttranscolor));
#endif
		V_DrawString(216, 96, 0, va("DEDTIMER = %d", stplyr->deadtimer));
		V_DrawString(216, 104, 0, va("JUMPFACT = %d", stplyr->jumpfactor));
		V_DrawString(240, 112, 0, va("X = %d", stplyr->mo->x>>FRACBITS));
		V_DrawString(240, 120, 0, va("Y = %d", stplyr->mo->y>>FRACBITS));
		V_DrawString(240, 128, 0, va("Z = %d", stplyr->mo->z>>FRACBITS));
		V_DrawString(216, 136, 0, va("Angle = %d", stplyr->mo->angle>>FRACBITS));
		V_DrawString(192, 152, 0, va("Underwater = %d", stplyr->powers[pw_underwater]));
		V_DrawString(192, 160, 0, va("MF_JUMPED = %u", (stplyr->pflags & PF_JUMPED)));
		V_DrawString(192, 168, 0, va("MF_SPINNING = %u", (stplyr->pflags & PF_SPINNING)));
		V_DrawString(192, 176, 0, va("MF_STARDASH = %u", (stplyr->pflags & PF_STARTDASH)));
		V_DrawString(192, 184, 0, va("Jumping = %d", stplyr->jumping));
		V_DrawString(192, 192, 0, va("Scoreadd = %d", stplyr->scoreadd));
	}
	else if (cv_debug == 3) // randomizer testing
	{
		V_DrawString(192, 184, 0, va("Seed: %010u", P_GetRandSeed()));
		V_DrawString(192, 192, 0, va("==  : %d", P_DebugRandom()));
	}
	else if (cv_debug)
	{
		const fixed_t d = AngleFixed(stplyr->mo->angle);
		V_DrawString(252, 168, 0, va("X: %d", stplyr->mo->x>>FRACBITS));
		V_DrawString(252, 176, 0, va("Y: %d", stplyr->mo->y>>FRACBITS));
		V_DrawString(252, 184, 0, va("Z: %d", stplyr->mo->z>>FRACBITS));
		V_DrawString(252, 192, 0, va("A: %d", FixedInt(d)));
	}
}

static void ST_drawScore(void)
{
	// SCORE:
	V_DrawScaledPatch(SCX(hudinfo[HUD_SCORE].x), SCY(hudinfo[HUD_SCORE].y), V_NOSCALESTART|V_TRANSLUCENT, sboscore);

	if (gametype == GT_SHARDS)
	{
		INT32 minutes = G_TicsToMinutes(stplyr->score, true);
		INT32 seconds = G_TicsToSeconds(stplyr->score);
		INT32 tics = G_TicsToCentiseconds(stplyr->score);

		// minutes time
		ST_DrawOverlayNum(SCX(hudinfo[(splitscreen) ? HUD_SHARDS_MINUTESSPLIT : HUD_SHARDS_MINUTES].x),
		                  SCY(hudinfo[(splitscreen) ? HUD_SHARDS_MINUTESSPLIT : HUD_SHARDS_MINUTES].y), minutes, tallnum, NULL);

		// colon location
		V_DrawScaledPatch(SCX(hudinfo[(splitscreen) ? HUD_SHARDS_TIMECOLONSPLIT : HUD_SHARDS_TIMECOLON].x),
		                  SCY(hudinfo[(splitscreen) ? HUD_SHARDS_TIMECOLONSPLIT : HUD_SHARDS_TIMECOLON].y), V_NOSCALESTART|V_TRANSLUCENT, sbocolon);

		// seconds time
		ST_DrawPaddedOverlayNum(SCX(hudinfo[(splitscreen) ? HUD_SHARDS_SECONDSSPLIT : HUD_SHARDS_SECONDS].x),
		                        SCY(hudinfo[(splitscreen) ? HUD_SHARDS_SECONDSSPLIT : HUD_SHARDS_SECONDS].y), seconds, 2, tallnum, NULL);

		// colon location
		V_DrawScaledPatch(SCX(hudinfo[(splitscreen) ? HUD_SHARDS_TIMETICCOLONSPLIT : HUD_SHARDS_TIMETICCOLON].x),
		                  SCY(hudinfo[(splitscreen) ? HUD_SHARDS_TIMETICCOLONSPLIT : HUD_SHARDS_TIMETICCOLON].y), V_NOSCALESTART|V_TRANSLUCENT, sbocolon);

		// tics time
		ST_DrawPaddedOverlayNum(SCX(hudinfo[(splitscreen) ? HUD_SHARDS_TICSSPLIT : HUD_SHARDS_TICS].x),
		                        SCY(hudinfo[(splitscreen) ? HUD_SHARDS_TICSSPLIT : HUD_SHARDS_TICS].y), tics, 2, tallnum, NULL);

		// Jewel Shards
		if (stplyr->powers[pw_jewelshards])
		{
			hudnum_t shardarea = (splitscreen) ? HUD_SHARDS_SHARDCOUNTSPLIT : HUD_SHARDS_SHARDCOUNT;

			if (stplyr->powers[pw_jewelshards] <= 5)
			{
				int i,x;
				for (i = 0,x = hudinfo[shardarea].x; i < stplyr->powers[pw_jewelshards]; i++,x-=8)
				{
					V_DrawScaledPatch(x, STRINGY(hudinfo[shardarea].y), V_SNAPTOTOP|V_SNAPTORIGHT, jshard);
				}
			}
			else
			{
				V_DrawRightAlignedString(hudinfo[shardarea].x-8, STRINGY(hudinfo[shardarea].y),
				                         V_SNAPTOTOP|V_SNAPTORIGHT, va("\x81%d", stplyr->powers[pw_jewelshards]));
				V_DrawScaledPatch(hudinfo[shardarea].x, STRINGY(hudinfo[shardarea].y), V_SNAPTOTOP|V_SNAPTORIGHT, jshard);
			}
		}
	}
	else
	{
		ST_DrawOverlayNum(SCX(hudinfo[HUD_SCORENUM].x), SCY(hudinfo[HUD_SCORENUM].y), stplyr->score, tallnum, NULL);
	}
}

static void ST_drawTime(void)
{
	INT32 seconds, minutes, tictrn, tics;
	boolean addminus = false;

	// TIME:
	V_DrawScaledPatch(SCX(hudinfo[(splitscreen) ? HUD_TIMESPLIT : HUD_TIME].x),
	                  SCY(hudinfo[(splitscreen) ? HUD_TIMESPLIT : HUD_TIME].y), V_NOSCALESTART|V_TRANSLUCENT, sbotime);

	if (cv_objectplace.value)
	{
		tics    = objectsdrawn;
		seconds = objectsdrawn%100;
		minutes = objectsdrawn/100;
		tictrn  = 0;
	}
	else
	{
		if (!cv_timermode.value && !(gametype == GT_COOP || gametype == GT_RACE))
		{
			if (gametype == GT_TAG)
			{
				addminus = true;

				if (leveltime < (hidetime*TICRATE))
					tics = (hidetime*TICRATE) - leveltime;
				else if (!cv_timelimit.value)
				{
					tics = leveltime - (hidetime*TICRATE);
					addminus = false;
				}
				else
					tics = timelimitintics - leveltime;
			}
			else if (cv_timelimit.value)
			{
				addminus = true;
				tics = timelimitintics - leveltime;
			}
			else // Meh.
				tics = leveltime;

			if (tics <= 0)
			{
				tics = 0;
				addminus = false; //zero is not negative!
			}
			else if (splitscreen && addminus)
			{
				if (cv_timetic.value == 1 && tics > 9999)
					tics = 9999; // Splitscreen can't handle 4 digit tics plus the minus sign
				else if (tics > 20999)
					tics = 20999; // Splitscreen can't handle 2 digit minutes plus the minus sign
			}
		}
		else
			tics = stplyr->realtime;

		seconds = tics/TICRATE % 60;
		minutes = tics/(60*TICRATE);
		tictrn  = G_TicsToCentiseconds(tics);
	}

	if (addminus) // draw a minus!
	{
		INT32 position;
		INT32 z;

		if (cv_timetic.value == 1) // Tics only
		{
			position = hudinfo[(splitscreen) ? HUD_SECONDSSPLIT : HUD_SECONDS].x - 8;
			z = tics;
		}
		else
		{
			position = hudinfo[(splitscreen) ? HUD_MINUTESSPLIT : HUD_MINUTES].x - 8;
			z = minutes;
		}

		do
			position -= 8;
		while (z /= 10);

		V_DrawScaledPatch(SCX(position), SCY(hudinfo[(splitscreen) ? HUD_TIMECOLONSPLIT : HUD_TIMECOLON].y), V_NOSCALESTART|V_TRANSLUCENT, minus);
	}

	if (cv_timetic.value == 1) // Tics only -- how simple is this?
		ST_DrawOverlayNum(SCX(hudinfo[(splitscreen) ? HUD_SECONDSSPLIT : HUD_SECONDS].x),
		                  SCY(hudinfo[(splitscreen) ? HUD_SECONDSSPLIT : HUD_SECONDS].y), tics, tallnum, NULL);
	else
	{
		// Minutes
		ST_DrawOverlayNum(SCX(hudinfo[(splitscreen) ? HUD_MINUTESSPLIT : HUD_MINUTES].x),
		                  SCY(hudinfo[(splitscreen) ? HUD_MINUTESSPLIT : HUD_MINUTES].y), minutes, tallnum, NULL);
		// Colon
		V_DrawScaledPatch(SCX(hudinfo[(splitscreen) ? HUD_TIMECOLONSPLIT : HUD_TIMECOLON].x),
		                  SCY(hudinfo[(splitscreen) ? HUD_TIMECOLONSPLIT : HUD_TIMECOLON].y), V_NOSCALESTART|V_TRANSLUCENT, sbocolon);
		// Seconds
		ST_DrawPaddedOverlayNum(SCX(hudinfo[(splitscreen) ? HUD_SECONDSSPLIT : HUD_SECONDS].x),
		                        SCY(hudinfo[(splitscreen) ? HUD_SECONDSSPLIT : HUD_SECONDS].y), seconds, 2, tallnum, NULL);

		if (!splitscreen && (cv_timetic.value == 2 || timeattacking)) // there's not enough room for tics in splitscreen, don't even bother trying!
		{
			// Colon
			V_DrawScaledPatch(SCX(hudinfo[HUD_TIMETICCOLON].x), SCY(hudinfo[HUD_TIMETICCOLON].y), V_NOSCALESTART|V_TRANSLUCENT, sbocolon);
			// Tics
			ST_DrawPaddedOverlayNum(SCX(hudinfo[HUD_TICS].x), SCY(hudinfo[HUD_TICS].y), tictrn, 2, tallnum, NULL);
		}
	}
}

static void ST_drawRings(void)
{
	hudnum_t ringHUD    = (splitscreen) ? HUD_RINGSSPLIT    : HUD_RINGS;
	hudnum_t ringnumHUD = (splitscreen) ? HUD_RINGSNUMSPLIT : HUD_RINGSNUM;

	V_DrawScaledPatch(SCX(hudinfo[ringHUD].x), SCY(hudinfo[ringHUD].y), V_NOSCALESTART|V_TRANSLUCENT,
	                  (stplyr->health <= 1 && leveltime/5 & 1) ? rrings : sborings);

	if (!useNightsSS && gamemap >= sstage_start && gamemap <= sstage_end)
	{
		INT32 ringscollected = 0; // Total # everyone has collected
		INT32 i;

		for (i = 0; i < MAXPLAYERS; i++)
			if (playeringame[i] && players[i].mo && players[i].mo->health > 1)
				ringscollected += players[i].mo->health - 1;

		ST_DrawOverlayNum(SCX(hudinfo[ringnumHUD].x), SCY(hudinfo[ringnumHUD].y), ringscollected, tallnum,
		                  (stplyr->health <= 1 && leveltime/5 & 1) ? hredmap : NULL);
	}
	else
	{
		ST_DrawOverlayNum(SCX(hudinfo[ringnumHUD].x), SCY(hudinfo[ringnumHUD].y), stplyr->health > 0 ? stplyr->health-1 : 0, tallnum,
		                  (stplyr->health <= 1 && leveltime/5 & 1) ? hredmap : NULL);
	}
}

static void ST_drawLives(void)
{
	if ((stplyr->powers[pw_super]) || (stplyr->pflags & PF_NIGHTSMODE))
	{
		if (!stplyr->skincolor) // 'default' color
		{
			V_DrawSmallScaledPatch(SCX(hudinfo[HUD_LIVESPIC].x), SCY(hudinfo[HUD_LIVESPIC].y - (splitscreen ? 8 : 0)),
				V_NOSCALESTART|V_TRANSLUCENT,superprefix[stplyr->skin]);
		}
		else
		{
			const UINT8 *colormap = (UINT8 *)translationtables[stplyr->skin] - 256 + (((stplyr->powers[pw_super]) ? 15 : stplyr->skincolor)<<8);
			V_DrawSmallMappedPatch(SCX(hudinfo[HUD_LIVESPIC].x), SCY(hudinfo[HUD_LIVESPIC].y - (splitscreen ? 8 : 0)),
				V_NOSCALESTART|V_TRANSLUCENT,superprefix[stplyr->skin], colormap);
		}
	}
	else
	{
		if (!stplyr->skincolor) // 'default' color
		{
			V_DrawSmallScaledPatch(SCX(hudinfo[HUD_LIVESPIC].x), SCY(hudinfo[HUD_LIVESPIC].y - (splitscreen ? 8 : 0)),
				V_NOSCALESTART|V_TRANSLUCENT,faceprefix[stplyr->skin]);
		}
		else
		{
			const UINT8 *colormap = (UINT8 *)translationtables[stplyr->skin] - 256 + ((stplyr->skincolor)<<8);
			V_DrawSmallMappedPatch(SCX(hudinfo[HUD_LIVESPIC].x), SCY(hudinfo[HUD_LIVESPIC].y - (splitscreen ? 8 : 0)),
				V_NOSCALESTART|V_TRANSLUCENT,faceprefix[stplyr->skin], colormap);
		}
	}

	if (splitscreen) // raise a bit
	{
		V_DrawScaledPatch(SCX(hudinfo[HUD_LIVESNAME].x), SCY(hudinfo[HUD_LIVESNAME].y - 16),
			V_NOSCALESTART|V_TRANSLUCENT, facenameprefix[stplyr->skin]);

		// draw the number of lives
		ST_DrawOverlayNum(SCX(hudinfo[HUD_LIVESNUM].x), SCY(hudinfo[HUD_LIVESNUM].y - 4), stplyr->lives, tallnum, NULL);

		// now draw the "x"
		V_DrawScaledPatch(SCX(hudinfo[HUD_LIVESX].x), SCY(hudinfo[HUD_LIVESX].y - 2), V_NOSCALESTART|V_TRANSLUCENT, stlivex);
	}
	else
	{
		V_DrawScaledPatch(SCX(hudinfo[HUD_LIVESNAME].x), SCY(hudinfo[HUD_LIVESNAME].y),
			V_NOSCALESTART|V_TRANSLUCENT, facenameprefix[stplyr->skin]);

		// draw the number of lives
		ST_DrawOverlayNum(SCX(hudinfo[HUD_LIVESNUM].x), SCY(hudinfo[HUD_LIVESNUM].y), stplyr->lives, tallnum, NULL);

		// now draw the "x"
		V_DrawScaledPatch(SCX(hudinfo[HUD_LIVESX].x), SCY(hudinfo[HUD_LIVESX].y), V_NOSCALESTART|V_TRANSLUCENT, stlivex);
	}
}

void ST_drawLevelTitle(void)
{
	char *lvlttl = mapheaderinfo[gamemap-1].lvlttl;
	char *subttl = mapheaderinfo[gamemap-1].subttl;
	INT32 xmod = 0;
	INT32 lvlttlxpos;
	INT32 ttlnumxpos;
	INT32 zonexpos;
	INT32 actnum = mapheaderinfo[gamemap-1].actnum;
	boolean nonumber = false;

	if (timeinmap >= 116 || lvlttl[0] == '\0')
		return;

	if (actnum > 0)
	{
		ttlnum = W_CachePatchName(va("TTL%.2d", actnum), PU_CACHE);
		lvlttlxpos = ((BASEVIDWIDTH/2) - (V_LevelNameWidth(lvlttl)/2)) - 16;
	}
	else
	{
		nonumber = true;
		lvlttlxpos = ((BASEVIDWIDTH/2) - (V_LevelNameWidth(lvlttl)/2));
	}

	ttlnumxpos = lvlttlxpos + V_LevelNameWidth(lvlttl);
	zonexpos = ttlnumxpos - V_LevelNameWidth(golfmode ? text[COURSE] : text[ZONE]);

	if (lvlttlxpos < 0)
		lvlttlxpos = 0;

	if (timeinmap > 106)
		xmod = (timeinmap-106)*32;
	else
		xmod = 0;

	if (!nonumber && ttlnumxpos+xmod+SHORT(ttlnum->width) < 320)
		V_DrawScaledPatch(SCX(ttlnumxpos+xmod), (int)(104*vid.fdupy), V_NOSCALESTART, ttlnum);
	V_DrawLevelTitle(lvlttlxpos-xmod, 80, 0, lvlttl);

	if (!mapheaderinfo[gamemap-1].nozone)
		V_DrawLevelTitle(zonexpos+xmod, 104, 0, golfmode ? text[COURSE] : text[ZONE]);

	if (128+(xmod/2) < 200)
		V_DrawCenteredString(BASEVIDWIDTH/2, 128+(xmod/2), 0, subttl);
}

static void ST_drawMonitorIcons(void)
{
	if (splitscreen) //old-school monitor icons in Splitscreen
	{
		if (stplyr->powers[pw_combiring])
			V_DrawScaledPatch(304, STRINGY(32), V_SNAPTOBOTTOM, combiring);

		if (stplyr->powers[pw_jumpshield])
			V_DrawScaledPatch(304, STRINGY(60), V_SNAPTORIGHT, jumpshield);
		else if (stplyr->powers[pw_forceshield] == 2)
			V_DrawScaledPatch(304, STRINGY(60), V_SNAPTORIGHT, (stplyr->powers[pw_reflecttimer]) ? forceshieldreflected : forceshield);
		else if (stplyr->powers[pw_forceshield] == 1 && (leveltime & 1))
			V_DrawScaledPatch(304, STRINGY(60), V_SNAPTORIGHT, (stplyr->powers[pw_reflecttimer]) ? forceshieldreflected : forceshield);
		else if (stplyr->powers[pw_watershield])
			V_DrawScaledPatch(304, STRINGY(60), V_SNAPTORIGHT, watershield);
		else if (stplyr->powers[pw_bombshield])
			V_DrawScaledPatch(304, STRINGY(60), V_SNAPTORIGHT, bombshield);
		else if (stplyr->powers[pw_ringshield])
			V_DrawScaledPatch(304, STRINGY(60), V_SNAPTORIGHT, ringshield);

		if (stplyr->playerstate != PST_DEAD && ((stplyr->powers[pw_invulnerability] > 3*TICRATE || (stplyr->powers[pw_invulnerability]
			&& leveltime & 1)) || ((stplyr->powers[pw_flashing] && leveltime & 1))))
			V_DrawScaledPatch(304, STRINGY(88), V_SNAPTORIGHT|V_TRANSLUCENT, invincibility);

		if (stplyr->powers[pw_sneakers] > 3*TICRATE || (stplyr->powers[pw_sneakers] && leveltime & 1))
			V_DrawScaledPatch(304, STRINGY(116), V_SNAPTORIGHT|V_TRANSLUCENT, sneakers);

		return;
	}

	if (stplyr->powers[pw_combiring])
		V_DrawScaledPatch(110, STRINGY(192), V_SNAPTOBOTTOM, combiring);

	if (stplyr->powers[pw_jumpshield])
		V_DrawScaledPatch(130, STRINGY(192), V_SNAPTOBOTTOM, jumpshield);
	else if (stplyr->powers[pw_forceshield] == 2)
		V_DrawScaledPatch(130, STRINGY(192), V_SNAPTOBOTTOM, (stplyr->powers[pw_reflecttimer]) ? forceshieldreflected : forceshield);
	else if (stplyr->powers[pw_forceshield] == 1 && (leveltime & 1))
		V_DrawScaledPatch(130, STRINGY(192), V_SNAPTOBOTTOM, (stplyr->powers[pw_reflecttimer]) ? forceshieldreflected : forceshield);
	else if (stplyr->powers[pw_watershield])
		V_DrawScaledPatch(130, STRINGY(192), V_SNAPTOBOTTOM, watershield);
	else if (stplyr->powers[pw_bombshield])
		V_DrawScaledPatch(130, STRINGY(192), V_SNAPTOBOTTOM, bombshield);
	else if (stplyr->powers[pw_ringshield])
		V_DrawScaledPatch(130, STRINGY(192), V_SNAPTOBOTTOM, ringshield);

	if (stplyr->powers[pw_sneakers] > 3*TICRATE || (stplyr->powers[pw_sneakers]
		&& leveltime & 1))
		V_DrawScaledPatch(150, STRINGY(192), V_SNAPTOBOTTOM|V_TRANSLUCENT, sneakers);

	if ((stplyr == &players[consoleplayer] && !cv_chasecam.value)
		|| ((splitscreen && stplyr == &players[secondarydisplayplayer]) && !cv_chasecam2.value)
		|| (stplyr == &players[displayplayer] && !cv_chasecam.value))
	{
		if (stplyr->playerstate != PST_DEAD && ((stplyr->powers[pw_invulnerability] > 3*TICRATE || (stplyr->powers[pw_invulnerability]
			&& leveltime & 1)) || ((stplyr->powers[pw_flashing] && leveltime & 1))))
			V_DrawScaledPatch(170, STRINGY(192), V_SNAPTOBOTTOM|V_TRANSLUCENT, invincibility);
	}
	else if (stplyr->powers[pw_invulnerability] > 3*TICRATE || (stplyr->powers[pw_invulnerability] && leveltime & 1))
		V_DrawScaledPatch(170, STRINGY(192), V_SNAPTOBOTTOM|V_TRANSLUCENT, invincibility);

	if (stplyr->powers[pw_scaletimer] > 3*TICRATE || (stplyr->powers[pw_scaletimer] && leveltime & 1))
	{
		if (stplyr->mo && stplyr->mo->destscale < 100)
			V_DrawScaledPatch(190, STRINGY(192), V_SNAPTOBOTTOM|V_TRANSLUCENT, shrinkico);
		else
			V_DrawScaledPatch(190, STRINGY(192), V_SNAPTOBOTTOM|V_TRANSLUCENT, growico);
	}

	if (stplyr->powers[pw_gravityboots] < 90*TICRATE && (stplyr->powers[pw_gravityboots] > 3*TICRATE || (stplyr->powers[pw_gravityboots] && leveltime & 1)))
		V_DrawScaledPatch(210, STRINGY(192), V_SNAPTOBOTTOM|V_TRANSLUCENT, gravboots);
}

static void ST_drawFirstPersonHUD(void)
{
	player_t *player = stplyr;
	patch_t *p = NULL;

	// Display the countdown drown numbers!
	if ((player->powers[pw_underwater] <= 11*TICRATE + 1
		&& player->powers[pw_underwater] >= 10*TICRATE + 1)
		|| (player->powers[pw_spacetime] <= 11*TICRATE + 1
		&& player->powers[pw_spacetime] >= 10*TICRATE + 1))
	{
		p = count5;
	}
	else if ((player->powers[pw_underwater] <= 9*TICRATE + 1
		&& player->powers[pw_underwater] >= 8*TICRATE + 1)
		|| (player->powers[pw_spacetime] <= 9*TICRATE + 1
		&& player->powers[pw_spacetime] >= 8*TICRATE + 1))
	{
		p = count4;
	}
	else if ((player->powers[pw_underwater] <= 7*TICRATE + 1
		&& player->powers[pw_underwater] >= 6*TICRATE + 1)
		|| (player->powers[pw_spacetime] <= 7*TICRATE + 1
		&& player->powers[pw_spacetime] >= 6*TICRATE + 1))
	{
		p = count3;
	}
	else if ((player->powers[pw_underwater] <= 5*TICRATE + 1
		&& player->powers[pw_underwater] >= 4*TICRATE + 1)
		|| (player->powers[pw_spacetime] <= 5*TICRATE + 1
		&& player->powers[pw_spacetime] >= 4*TICRATE + 1))
	{
		p = count2;
	}
	else if ((player->powers[pw_underwater] <= 3*TICRATE + 1
		&& player->powers[pw_underwater] >= 2*TICRATE + 1)
		|| (player->powers[pw_spacetime] <= 3*TICRATE + 1
		&& player->powers[pw_spacetime] >= 2*TICRATE + 1))
	{
		p = count1;
	}
	else if ((player->powers[pw_underwater] <= 1*TICRATE + 1
		&& player->powers[pw_underwater] > 1)
		|| (player->powers[pw_spacetime] <= 1*TICRATE + 1
		&& player->powers[pw_spacetime] > 1))
	{
		p = count0;
	}

	if (p)
		V_DrawTranslucentPatch(SCX((BASEVIDWIDTH/2) - (SHORT(p->width)/2) + SHORT(p->leftoffset)), SCY(120 - SHORT(p->topoffset)),
			V_NOSCALESTART, p);
}

static void ST_drawNiGHTSHUD(void)
{
	if (stplyr->linkcount > 1)
	{
		INT32 colornum;

		colornum = ((stplyr->linkcount-1) / 5)%14;

		if (splitscreen)
		{
			ST_DrawNightsOverlayNum(SCX(256), SCY(160), (stplyr->linkcount-1), nightsnum, colornum);
			V_DrawMappedPatch(SCX(264), SCY(160), V_NOSCALESTART, nightslink,
				colornum == 0 ? colormaps : (UINT8 *)defaulttranslationtables - 256 + (colornum<<8));
		}
		else
		{
			ST_DrawNightsOverlayNum(SCX(160), SCY(176), (stplyr->linkcount-1), nightsnum, colornum);
			V_DrawMappedPatch(SCX(168), SCY(176), V_NOSCALESTART, nightslink,
				colornum == 0 ? colormaps : (UINT8 *)defaulttranslationtables - 256 + (colornum<<8));
		}
	}

	if (stplyr->pflags & PF_NIGHTSMODE)
	{
		INT32 locx, locy;

		if (splitscreen)
		{
			locx = 110;
			locy = 188;
		}
		else
		{
			locx = 16;
			locy = 144;
		}

		if (!(stplyr->drillmeter & 1))
		{
			V_DrawFill(locx-2, STRINGY(locy-2), 100, 8, 48);
			V_DrawFill(locx, STRINGY(locy), 96, 4, 31);
			V_DrawFill(locx, STRINGY(locy), stplyr->drillmeter/20, 4, 160);
		}
		else
		{
			V_DrawFill(locx-2, STRINGY(locy-2), 100, 8, 37);
			V_DrawFill(locx, STRINGY(locy), 96, 4, 8);
			V_DrawFill(locx, STRINGY(locy), stplyr->drillmeter/20, 4, 164);
		}
	}

	if (gametype == GT_RACE)
	{
		ST_drawScore();
		ST_drawTime();
		return;
	}

	if (stplyr->bonustime > 1)
		V_DrawCenteredString(BASEVIDWIDTH/2, STRINGY(100), 0, "BONUS TIME START!");

	V_DrawScaledPatch(SCX(16), SCY(8), V_NOSCALESTART|V_TRANSLUCENT, nbracket);
	V_DrawScaledPatch(SCX(24), (INT32)(SCY(8) + 8*vid.fdupy), V_NOSCALESTART|V_TRANSLUCENT, nhud[(leveltime/2)%12]);

	if (stplyr->capsule && !cv_objectplace.value)
	{
		INT32 amount;
		INT32 origamount;
		const INT32 length = 88;

		V_DrawScaledPatch(SCX(72), SCY(8), V_NOSCALESTART|V_TRANSLUCENT, nbracket);
		V_DrawScaledPatch(SCX(74), (INT32)(SCY(8) + 4*vid.fdupy), V_NOSCALESTART|V_TRANSLUCENT,
			minicaps);

		if (stplyr->capsule->reactiontime != 0)
		{
			INT32 r;
			const INT32 orblength = 20;

			for (r = 0; r < 5; r++)
			{
				V_DrawScaledPatch(SCX(230 - (7*r)), SCY(144), V_NOSCALESTART|V_TRANSLUCENT,
					redstat);
				V_DrawScaledPatch(SCX(188 - (7*r)), SCY(144), V_NOSCALESTART|V_TRANSLUCENT,
					orngstat);
				V_DrawScaledPatch(SCX(146 - (7*r)), SCY(144), V_NOSCALESTART|V_TRANSLUCENT,
					yelstat);
				V_DrawScaledPatch(SCX(104 - (7*r)), SCY(144), V_NOSCALESTART|V_TRANSLUCENT,
					byelstat);
			}
			origamount = stplyr->capsule->spawnpoint->angle & 1023;

			amount = (origamount - stplyr->capsule->health);

			amount = (amount * orblength)/origamount;

			if (amount > 0)
			{
				INT32 t;

				// Fill up the bar with blue orbs... in reverse! (yuck)
				for (r = amount; r >= 0; r--)
				{
					t = r;

					if (r > 14)
						t += 1;
					if (r > 9)
						t += 1;
					if (r > 4)
						t += 1;

					V_DrawScaledPatch(SCX(76 + (7*t)), SCY(144), V_NOSCALESTART|V_TRANSLUCENT,
						bluestat);
				}
			}
		}
		else
		{
			// Lil' white box!
			V_DrawFill(15, STRINGY(8) + 34, length + 2, 5, 0);
			V_DrawFill(16, STRINGY(8)+35, length/4, 3, 103);
			V_DrawFill(16 + length/4, STRINGY(8) + 35, length/4, 3, 85);
			V_DrawFill(16 + (length/4)*2, STRINGY(8) + 35, length/4, 3, 87);
			V_DrawFill(16 + (length/4)*3, STRINGY(8) + 35, length/4, 3, 131);
			origamount = stplyr->capsule->spawnpoint->angle & 1023;

			if (origamount <= 0)
				CONS_Printf("Give the egg capsule on mare %d a ring requirement.\n", stplyr->capsule->threshold);
			else
			{
				amount = (origamount - stplyr->capsule->health);
				amount = (amount * length)/origamount;

				if (amount > 0)
					V_DrawFill(16, STRINGY(8) + 35, amount, 3, 229);
			}
		}
		V_DrawScaledPatch(SCX(40), (INT32)(SCY(8) + 5*vid.fdupy), V_NOSCALESTART|V_TRANSLUCENT, narrow[(leveltime/2)%8]);
	}
	else
		V_DrawScaledPatch(SCX(40), (INT32)(SCY(8) + 5*vid.fdupy), V_NOSCALESTART|V_TRANSLUCENT, narrow[8]);

	ST_DrawOverlayNum(SCX(68), (INT32)(SCY(8) + 11*vid.fdupy), stplyr->health > 0 ? stplyr->health - 1 : 0, tallnum, NULL);

	ST_DrawNightsOverlayNum(SCX(288), SCY(12), stplyr->score, nightsnum, 7); // Blue

	if (stplyr->nightstime > 0)
	{
		INT32 numbersize;

		if (stplyr->nightstime < 10)
			numbersize = SCX(16)/2;
		else if (stplyr->nightstime < 100)
			numbersize = SCX(32)/2;
		else
			numbersize = SCX(48)/2;

		if (stplyr->nightstime < 10)
			ST_DrawNightsOverlayNum(SCX(160) + numbersize, SCY(32), stplyr->nightstime,
				nightsnum, 6); // Red
		else
			ST_DrawNightsOverlayNum(SCX(160) + numbersize, SCY(32), stplyr->nightstime,
				nightsnum, 15); // Yellow
	}
}

static void ST_drawMatchHUD(void)
{
	INT32 offset = 80;

	if (gametype == GT_MATCH || gametype == GT_TAG || gametype == GT_CTF || gametype == GT_SHARDS
		|| cv_ringslinger.value)
	{
		if (stplyr->health > 1)
			V_DrawScaledPatch(8 + offset, STRINGY(162), V_SNAPTOLEFT, normring);
		else
			V_DrawTranslucentPatch(8 + offset, STRINGY(162), V_SNAPTOLEFT|V_8020TRANS, normring);

		if (!stplyr->currentweapon)
			V_DrawScaledPatch(6 + offset, STRINGY(162 - (splitscreen ? 4 : 2)), V_SNAPTOLEFT, curweapon);
	}

	offset += 20;

	if (stplyr->powers[pw_automaticring])
	{
		INT32 yelflag = 0;

		if (stplyr->powers[pw_automaticring] >= MAX_AUTOMATIC)
			yelflag = V_YELLOWMAP;

		if ((stplyr->ringweapons & RW_AUTO) && stplyr->health > 1)
			V_DrawScaledPatch(8 + offset, STRINGY(162), V_SNAPTOLEFT, autoring);
		else
			V_DrawTranslucentPatch(8 + offset, STRINGY(162), V_SNAPTOLEFT|V_8020TRANS, autoring);

		if (stplyr->powers[pw_automaticring] > 99)
			V_DrawTinyNum(8 + offset + 1, STRINGY(162), V_TRANSLUCENT | V_SNAPTOLEFT | yelflag,
				stplyr->powers[pw_automaticring]);
		else
			V_DrawString(8 + offset, STRINGY(162), V_TRANSLUCENT | V_SNAPTOLEFT | yelflag,
				va("%d", stplyr->powers[pw_automaticring]));

		if (stplyr->currentweapon == WEP_AUTO)
			V_DrawScaledPatch(6 + offset, STRINGY(162 - (splitscreen ? 4 : 2)), V_SNAPTOLEFT, curweapon);
	}
	else if (stplyr->ringweapons & RW_AUTO)
		V_DrawTranslucentPatch(8 + offset, STRINGY(162), V_SNAPTOLEFT, autoring);

	offset += 20;

	if (stplyr->powers[pw_bouncering])
	{
		INT32 yelflag = 0;

		if (stplyr->powers[pw_bouncering] >= MAX_BOUNCE)
			yelflag = V_YELLOWMAP;

		if ((stplyr->ringweapons & RW_BOUNCE) && stplyr->health > 1)
			V_DrawScaledPatch(8 + offset, STRINGY(162), V_SNAPTOLEFT, bouncering);
		else
			V_DrawTranslucentPatch(8 + offset, STRINGY(162), V_SNAPTOLEFT|V_8020TRANS, bouncering);

		if (stplyr->powers[pw_bouncering] > 99)
			V_DrawTinyNum(8 + offset + 1, STRINGY(162), V_TRANSLUCENT | V_SNAPTOLEFT | yelflag,
				stplyr->powers[pw_bouncering]);
		else
			V_DrawString(8 + offset, STRINGY(162), V_TRANSLUCENT | V_SNAPTOLEFT | yelflag,
				va("%d", stplyr->powers[pw_bouncering]));

		if (stplyr->currentweapon == WEP_BOUNCE)
			V_DrawScaledPatch(6 + offset, STRINGY(162 - (splitscreen ? 4 : 2)), V_SNAPTOLEFT, curweapon);
	}
	else if (stplyr->ringweapons & RW_BOUNCE)
		V_DrawTranslucentPatch(8 + offset, STRINGY(162), V_SNAPTOLEFT, bouncering);

	offset += 20;

	if (stplyr->powers[pw_scatterring])
	{
		INT32 yelflag = 0;

		if (stplyr->powers[pw_scatterring] >= MAX_SCATTER)
			yelflag = V_YELLOWMAP;

		if ((stplyr->ringweapons & RW_SCATTER) && stplyr->health > 1)
			V_DrawScaledPatch(8 + offset, STRINGY(162), V_SNAPTOLEFT, scatterring);
		else
			V_DrawTranslucentPatch(8 + offset, STRINGY(162), V_SNAPTOLEFT|V_8020TRANS, scatterring);

		if (stplyr->powers[pw_scatterring] > 99)
			V_DrawTinyNum(8 + offset + 1, STRINGY(162), V_TRANSLUCENT | V_SNAPTOLEFT | yelflag,
				stplyr->powers[pw_scatterring]);
		else
			V_DrawString(8 + offset, STRINGY(162), V_TRANSLUCENT | V_SNAPTOLEFT | yelflag,
				va("%d", stplyr->powers[pw_scatterring]));

		if (stplyr->currentweapon == WEP_SCATTER)
			V_DrawScaledPatch(6 + offset, STRINGY(162 - (splitscreen ? 4 : 2)), V_SNAPTOLEFT, curweapon);
	}
	else if (stplyr->ringweapons & RW_SCATTER)
		V_DrawTranslucentPatch(8 + offset, STRINGY(162), V_SNAPTOLEFT, scatterring);

	offset += 20;

	if (stplyr->powers[pw_grenadering])
	{
		INT32 yelflag = 0;

		if (stplyr->powers[pw_grenadering] >= MAX_GRENADE)
			yelflag = V_YELLOWMAP;

		if ((stplyr->ringweapons & RW_GRENADE) && stplyr->health > 1)
			V_DrawScaledPatch(8 + offset, STRINGY(162), V_SNAPTOLEFT, grenadering);
		else
			V_DrawTranslucentPatch(8 + offset, STRINGY(162), V_SNAPTOLEFT|V_8020TRANS, grenadering);

		if (stplyr->powers[pw_grenadering] > 99)
			V_DrawTinyNum(8 + offset + 1, STRINGY(162), V_TRANSLUCENT | V_SNAPTOLEFT | yelflag,
				stplyr->powers[pw_grenadering]);
		else
			V_DrawString(8 + offset, STRINGY(162), V_TRANSLUCENT | V_SNAPTOLEFT | yelflag,
				va("%d", stplyr->powers[pw_grenadering]));

		if (stplyr->currentweapon == WEP_GRENADE)
			V_DrawScaledPatch(6 + offset, STRINGY(162 - (splitscreen ? 4 : 2)), V_SNAPTOLEFT, curweapon);
	}
	else if (stplyr->ringweapons & RW_GRENADE)
		V_DrawTranslucentPatch(8 + offset, STRINGY(162), V_SNAPTOLEFT, grenadering);

	offset += 20;

	if (stplyr->powers[pw_explosionring])
	{
		INT32 yelflag = 0;

		if (stplyr->powers[pw_explosionring] >= MAX_EXPLOSION)
			yelflag = V_YELLOWMAP;

		if ((stplyr->ringweapons & RW_EXPLODE) && stplyr->health > 1)
			V_DrawScaledPatch(8 + offset, STRINGY(162), V_SNAPTOLEFT, explosionring);
		else
			V_DrawTranslucentPatch(8 + offset, STRINGY(162), V_SNAPTOLEFT|V_8020TRANS, explosionring);

		if (stplyr->powers[pw_explosionring] > 99)
			V_DrawTinyNum(8 + offset + 1, STRINGY(162), V_TRANSLUCENT | V_SNAPTOLEFT | yelflag,
				stplyr->powers[pw_explosionring]);
		else
			V_DrawString(8 + offset, STRINGY(162), V_TRANSLUCENT | V_SNAPTOLEFT | yelflag,
				va("%d", stplyr->powers[pw_explosionring]));

		if (stplyr->currentweapon == WEP_EXPLODE)
			V_DrawScaledPatch(6 + offset, STRINGY(162 - (splitscreen ? 4 : 2)), V_SNAPTOLEFT, curweapon);
	}
	else if (stplyr->ringweapons & RW_EXPLODE)
		V_DrawTranslucentPatch(8 + offset, STRINGY(162), V_SNAPTOLEFT, explosionring);

	offset += 20;

	if (stplyr->powers[pw_railring])
	{
		INT32 yelflag = 0;

		if (stplyr->powers[pw_railring] >= MAX_RAIL)
			yelflag = V_YELLOWMAP;

		if ((stplyr->ringweapons & RW_RAIL) && stplyr->health > 1)
			V_DrawScaledPatch(8 + offset, STRINGY(162), V_SNAPTOLEFT, railring);
		else
			V_DrawTranslucentPatch(8 + offset, STRINGY(162), V_SNAPTOLEFT|V_8020TRANS, railring);

		if (stplyr->powers[pw_railring] > 99)
			V_DrawTinyNum(8 + offset + 1, STRINGY(162), V_TRANSLUCENT | V_SNAPTOLEFT | yelflag,
				stplyr->powers[pw_railring]);
		else
			V_DrawString(8 + offset, STRINGY(162), V_TRANSLUCENT | V_SNAPTOLEFT | yelflag,
				va("%d", stplyr->powers[pw_railring]));

		if (stplyr->currentweapon == WEP_RAIL)
			V_DrawScaledPatch(6 + offset, STRINGY(162 - (splitscreen ? 4 : 2)), V_SNAPTOLEFT, curweapon);
	}
	else if (stplyr->ringweapons & RW_RAIL)
		V_DrawTranslucentPatch(8 + offset, STRINGY(162), V_SNAPTOLEFT, railring);

	offset += 20;

	// Power Stones collected
	offset = 136; // Used for Y now

	if (stplyr->powers[pw_emeralds] & EMERALD1)
		V_DrawScaledPatch(28, STRINGY(offset), V_SNAPTOLEFT, tinyemeraldpics[0]);

	offset += 8;

	if (stplyr->powers[pw_emeralds] & EMERALD2)
		V_DrawScaledPatch(40, STRINGY(offset), V_SNAPTOLEFT, tinyemeraldpics[1]);

	if (stplyr->powers[pw_emeralds] & EMERALD6)
		V_DrawScaledPatch(16, STRINGY(offset), V_SNAPTOLEFT, tinyemeraldpics[5]);

	offset += 16;

	if (stplyr->powers[pw_emeralds] & EMERALD3)
		V_DrawScaledPatch(40, STRINGY(offset), V_SNAPTOLEFT, tinyemeraldpics[2]);

	if (stplyr->powers[pw_emeralds] & EMERALD5)
		V_DrawScaledPatch(16, STRINGY(offset), V_SNAPTOLEFT, tinyemeraldpics[4]);

	offset += 8;

	if (stplyr->powers[pw_emeralds] & EMERALD4)
		V_DrawScaledPatch(28, STRINGY(offset), V_SNAPTOLEFT, tinyemeraldpics[3]);

	offset -= 16;

	if (stplyr->powers[pw_emeralds] & EMERALD7)
		V_DrawScaledPatch(28, STRINGY(offset), V_SNAPTOLEFT, tinyemeraldpics[6]);
}

static void ST_drawRaceHUD(void)
{
	if (leveltime > TICRATE && leveltime <= 2*TICRATE)
		V_DrawScaledPatch(SCX((BASEVIDWIDTH - SHORT(race3->width))/2), (INT32)(SCY(BASEVIDHEIGHT/2)), V_NOSCALESTART, race3);
	else if (leveltime > 2*TICRATE && leveltime <= 3*TICRATE)
		V_DrawScaledPatch(SCX((BASEVIDWIDTH - SHORT(race2->width))/2), (INT32)(SCY(BASEVIDHEIGHT/2)), V_NOSCALESTART, race2);
	else if (leveltime > 3*TICRATE && leveltime <= 4*TICRATE)
		V_DrawScaledPatch(SCX((BASEVIDWIDTH - SHORT(race1->width))/2), (INT32)(SCY(BASEVIDHEIGHT/2)), V_NOSCALESTART, race1);
	else if (leveltime > 4*TICRATE && leveltime <= 5*TICRATE)
		V_DrawScaledPatch(SCX((BASEVIDWIDTH - SHORT(racego->width))/2), (INT32)(SCY(BASEVIDHEIGHT/2)), V_NOSCALESTART, racego);

	// Sound effects yay!
	if (leveltime == TICRATE || leveltime == 2*TICRATE || leveltime == 3*TICRATE)
		S_StartSound(NULL, sfx_race_countdown);
	else if (leveltime == 4*TICRATE)
		S_StartSound(NULL, sfx_race_go);

	if (circuitmap)
	{
		if (stplyr->exiting)
			V_DrawString(hudinfo[HUD_LAP].x, STRINGY(hudinfo[HUD_LAP].y), V_YELLOWMAP, "FINISHED!");
		else
			V_DrawString(hudinfo[HUD_LAP].x, STRINGY(hudinfo[HUD_LAP].y), 0, va("Lap: %u/%d", stplyr->laps+1, cv_numlaps.value));
	}
}

static void ST_drawTagText(void)
{
	char pstext[33] = "";

	if (leveltime < hidetime * TICRATE) //during the hide time, the seeker and hiders have different messages on their HUD.
	{
		if (stplyr->pflags & PF_TAGIT)
			sprintf(pstext, "WAITING FOR PLAYERS TO HIDE...");
		else
		{
			if (cv_tagtype.value == 1) //hide and seek.
				sprintf(pstext, "HIDE BEFORE TIME RUNS OUT!");
			else //default
				sprintf(pstext, "FLEE BEFORE YOU ARE HUNTED!");
		}
	}
	else
	{
		if (stplyr->pflags & PF_TAGIT)
			sprintf(pstext, "YOU'RE IT!");
		else
		{
			if (cv_tagtype.value == 1) //hide and seek.
				sprintf(pstext, "IN HIDING...");
			else //default
				sprintf(pstext, "FLEE YOUR PURSUERS!");
		}
	}

	// Print the stuff.
	if (pstext[0])
	{
		if (splitscreen)
			V_DrawCenteredString(BASEVIDWIDTH/2, STRINGY(184), 0, pstext);
		else
			V_DrawCenteredString(BASEVIDWIDTH/2, STRINGY(192), 0, pstext);
	}
}

static void ST_drawCTFHUD(void)
{
	INT32 i;
	UINT16 whichflag = 0;

	// Draw the flags
	V_DrawSmallScaledPatch(256, (splitscreen) ? STRINGY(160) : STRINGY(176), 0, rflagico);
	V_DrawSmallScaledPatch(288, (splitscreen) ? STRINGY(160) : STRINGY(176), 0, bflagico);

	for (i = 0; i < MAXPLAYERS; i++)
	{
		if (players[i].gotflag & MF_REDFLAG) // Red flag isn't at base
			V_DrawScaledPatch(256, (splitscreen) ? STRINGY(156) : STRINGY(174), 0, nonicon);
		else if (players[i].gotflag & MF_BLUEFLAG) // Blue flag isn't at base
			V_DrawScaledPatch(288, (splitscreen) ? STRINGY(156) : STRINGY(174), 0, nonicon);

		whichflag |= players[i].gotflag;
		if ((whichflag & (MF_REDFLAG|MF_BLUEFLAG)) == (MF_REDFLAG|MF_BLUEFLAG))
			break; // both flags were found, let's stop early
	}

	if (stplyr->gotflag & MF_REDFLAG)
	{
		// YOU HAVE THE RED FLAG
		V_DrawScaledPatch(224, (splitscreen) ? STRINGY(160) : STRINGY(176), 0, gotrflag);
	}
	else if (stplyr->gotflag & MF_BLUEFLAG)
	{
		// YOU HAVE THE BLUE FLAG
		V_DrawScaledPatch(224, (splitscreen) ? STRINGY(160) : STRINGY(176), 0, gotbflag);
	}

	// Display a countdown timer showing how much time left until the flag your team dropped returns to base.
	{
		char timeleft[33];
		if (redflag && redflag->fuse)
		{
			sprintf(timeleft, "%u", (redflag->fuse / TICRATE));
			V_DrawCenteredString(268, STRINGY(184), V_YELLOWMAP, timeleft);
		}

		if (blueflag && blueflag->fuse)
		{
			sprintf(timeleft, "%u", (blueflag->fuse / TICRATE));
			V_DrawCenteredString(300, STRINGY(184), V_YELLOWMAP, timeleft);
		}
	}
}

// Draws "Red Team", "Blue Team", or "Spectator" for team gametypes.
static void ST_drawTeamName(void)
{
	if (stplyr->ctfteam == 1)
		V_DrawString(256, (splitscreen) ? STRINGY(184) : STRINGY(192), V_TRANSLUCENT, "RED TEAM");
	else if (stplyr->ctfteam == 2)
		V_DrawString(248, (splitscreen) ? STRINGY(184) : STRINGY(192), V_TRANSLUCENT, "BLUE TEAM");
	else
		V_DrawString(244, (splitscreen) ? STRINGY(184) : STRINGY(192), V_TRANSLUCENT, "SPECTATOR");
}

static void ST_drawChain(void)
{
	static INT32 prevchain = 0;
	static boolean inchain = false;
	static INT32 disptime = 0;

	if (splitscreen)
	{
		// Just draw the chain message
		if (stplyr->scoreadd > 1)
		{
			ST_DrawNightsOverlayNum(SCX(272-40), SCY(28), stplyr->scoreadd, nightsnum, 15);
			V_DrawString(272-40, STRINGY(56), V_YELLOWMAP, "CHAIN!");
		}
		return;
	}

	if (stplyr->scoreadd)
	{
		if (stplyr->scoreadd > 1)
		{
			ST_DrawNightsOverlayNum(SCX(272-40), SCY(28), stplyr->scoreadd, nightsnum, 15);
			V_DrawString(272-40, STRINGY(40), V_YELLOWMAP, "CHAIN!");
		}

		inchain = true;
		prevchain = stplyr->scoreadd;
	}
	else if (inchain)
	{
		inchain = false;
		if (prevchain > 3)
		{
			disptime = 3*TICRATE;
			S_StartSound(0, sfx_chain_end);
		}
		else
			disptime = 0;
	}

	if (!inchain && disptime)
	{
		const char *displaytext = "OK!";

		disptime--;

		ST_DrawNightsOverlayNum(SCX(272-40), SCY(28), prevchain, nightsnum, 15);
		V_DrawString(272-40, STRINGY(40), V_YELLOWMAP, "CHAIN!");

		if (prevchain >= 999)
			displaytext = "DO YOU HAVE A LIFE?";
		else if (prevchain >= 60)
			displaytext = "PARAMOUNT!";
		else if (prevchain >= 40)
			displaytext = "MARVELOUS!";
		else if (prevchain >= 30)
			displaytext = "INCREDIBLE!";
		else if (prevchain >= 20)
			displaytext = "FANTASTIC!";
		else if (prevchain >= 15)
			displaytext = "WONDERFUL!";
		else if (prevchain >= 12)
			displaytext = "COOL!";
		else if (prevchain >= 9)
			displaytext = "NICE!";
		else if (prevchain >= 7)
			displaytext = "GREAT!";
		else if (prevchain >= 5)
			displaytext = "GOOD!";

		V_DrawRightAlignedString(280, STRINGY(48), V_GREENMAP, displaytext);
	}
}

static void ST_drawSpecialStageHUD(void)
{
	if (hu_showscores && (netgame || multiplayer))
		return; //hide in netplay only

	if (totalrings > 0)
	{
		if (splitscreen)
			ST_DrawOverlayNum(SCX(hudinfo[HUD_SS_TOTALRINGS_SPLIT].x), SCY(hudinfo[HUD_SS_TOTALRINGS_SPLIT].y), totalrings, tallnum, NULL);
		else
			ST_DrawOverlayNum(SCX(hudinfo[HUD_SS_TOTALRINGS].x), SCY(hudinfo[HUD_SS_TOTALRINGS].y), totalrings, tallnum, NULL);
	}

	if (leveltime < 5*TICRATE && totalrings > 0)
	{
		V_DrawScaledPatch(hudinfo[HUD_GETRINGS].x, (INT32)(SCY(hudinfo[HUD_GETRINGS].y)/vid.fdupy), V_TRANSLUCENT, getall);
		ST_DrawOverlayNum(SCX(hudinfo[HUD_GETRINGSNUM].x), SCY(hudinfo[HUD_GETRINGSNUM].y), totalrings, tallnum, NULL);
	}

	if (sstimer)
	{
		V_DrawString(hudinfo[HUD_TIMELEFT].x, STRINGY(hudinfo[HUD_TIMELEFT].y), 0, "TIME LEFT");
		ST_DrawNightsOverlayNum(SCX(hudinfo[HUD_TIMELEFTNUM].x), SCY(hudinfo[HUD_TIMELEFTNUM].y), sstimer/TICRATE, tallnum, 13);
	}
	else
		V_DrawScaledPatch(SCX(hudinfo[HUD_TIMEUP].x), SCY(hudinfo[HUD_TIMEUP].y), V_NOSCALESTART|V_TRANSLUCENT, timeup);
}

static void ST_drawContinueHUD(void)
{
	char stimeleft[33];
	patch_t *contsonic;
	// Do continue screen here.
	// Initialize music
	// For some reason the code doesn't like a simple ==...
	if (stplyr->deadtimer < gameovertics && stplyr->deadtimer > gameovertics - 2)
	{
		// Force a screen wipe

		stplyr->deadtimer--;

		S_ChangeMusic(mus_contsc, false);
		S_StopSounds();
		oncontinuescreen = true;

		if (rendermode != render_none)
		{
			// First, read the current screen
			F_WipeStartScreen();

			// Then, draw what the new screen will look like.
			V_DrawFill(0, 0, vid.width, vid.height, 31);

			contsonic = W_CachePatchName("CONT1", PU_CACHE);
			V_DrawScaledPatch((BASEVIDWIDTH-SHORT(contsonic->width))/2, 64, 0, contsonic);
			V_DrawString(128,128,0, "CONTINUE?");
			sprintf(stimeleft, "%d", (stplyr->deadtimer - (gameovertics-11*TICRATE))/TICRATE);
			V_DrawString(stplyr->deadtimer >= (gameovertics-TICRATE) ? 152 : 160,144,0, stimeleft);

			// Now, read the end screen we want to fade to.
			F_WipeEndScreen(0, 0, vid.width, vid.height);

			// Do the wipe-io!
			F_RunWipe(0, true);
		}
	}

	V_DrawFill(0, 0, vid.width, vid.height, 31);
	V_DrawString(128, 128, 0, "CONTINUE?");
	// Draw a Sonic!
	contsonic = W_CachePatchName("CONT1", PU_CACHE);
	V_DrawScaledPatch((BASEVIDWIDTH - SHORT(contsonic->width))/2, 64, 0, contsonic);
	sprintf(stimeleft, "%d", (stplyr->deadtimer - (gameovertics-11*TICRATE))/TICRATE);
	V_DrawString(stplyr->deadtimer >= (gameovertics-TICRATE) ? 152 : 160, 144, 0, stimeleft);
	if (stplyr->deadtimer < (gameovertics-10*TICRATE))
		Command_ExitGame_f();
	if (stplyr->deadtimer < gameovertics-TICRATE && (stplyr->cmd.buttons & BT_JUMP || stplyr->cmd.buttons & BT_USE))
	{
		if (stplyr->continues != -1)
			stplyr->continues--;

		// Reset score
		stplyr->score = 0;

		// Allow tokens to come back if not a netgame.
		if (!(netgame || multiplayer))
		{
			tokenlist = 0;
			token = 0;
			imcontinuing = true;
		}

		// Reset # of lives
		if (ultimatemode)
			stplyr->lives = 1;
		else
			stplyr->lives = 3;

		// Clear any starpost data
		stplyr->starpostangle = 0;
		stplyr->starpostbit = 0;
		stplyr->starpostnum = 0;
		stplyr->starposttime = 0;
		stplyr->starpostx = 0;
		stplyr->starposty = 0;
		stplyr->starpostz = 0;
		stplyr->starpostscale = 0;

		contsonic = W_CachePatchName("CONT2", PU_CACHE);
		V_DrawScaledPatch((BASEVIDWIDTH - SHORT(contsonic->width))/2, 64, 0, contsonic);
	}
}

static void ST_drawEmeraldHuntIcon(mobj_t *hunt, INT32 graphic)
{
	patch_t *p;
	INT32 interval;
	fixed_t dist = P_AproxDistance(P_AproxDistance(stplyr->mo->x - hunt->x, stplyr->mo->y - hunt->y),
		stplyr->mo->z - hunt->z);

	if (dist < 128<<FRACBITS)
	{
		p = homing6;
		interval = 5;
	}
	else if (dist < 512<<FRACBITS)
	{
		p = homing5;
		interval = 10;
	}
	else if (dist < 1024<<FRACBITS)
	{
		p = homing4;
		interval = 20;
	}
	else if (dist < 2048<<FRACBITS)
	{
		p = homing3;
		interval = 30;
	}
	else if (dist < 3072<<FRACBITS)
	{
		p = homing2;
		interval = 35;
	}
	else
	{
		p = homing1;
		interval = 0;
	}

	V_DrawScaledPatch(hudinfo[graphic].x, STRINGY(hudinfo[graphic].y), V_TRANSLUCENT, p);
	if (interval > 0 && leveltime % interval == 0)
		S_StartSound(NULL, sfx_emfind);
}

static void ST_drawBossHUD(INT32 bosshealth, INT32 bossihealth)
{
	static INT32 framealt, lastbosshealth;  //fancy effects stuff?

	//checking effects related stuff.
	if (bosshealth > 20)
		bosshealth = 20; //(let's limit ourselves, kay?)
	if (bossihealth > 20)
		bossihealth = 20; //(if a boss has more than 20 health, then... well, come on, that's overkill in the first place)
	if (!lastbosshealth || lastbosshealth > bossihealth)
		lastbosshealth = 0; //clear this on certain conditions

	//actually drawing stuff!
	M_DrawTextBox((hudinfo[HUD_BOSSPOSITION].x)-(bossihealth*8), hudinfo[HUD_BOSSPOSITION].y, bossihealth, 1, V_SNAPTORIGHT|V_SNAPTOTOP);
	if (lastbosshealth>bosshealth)
		framealt += (lastbosshealth-bosshealth)*8;
	if (bosshealth)
		V_DrawFill((hudinfo[HUD_BOSSPOSITION].x)-(bossihealth*8)+8, hudinfo[HUD_BOSSPOSITION].y+8, bosshealth*8+framealt, 8, 128|V_SNAPTORIGHT|V_SNAPTOTOP);
	if (framealt)
		V_DrawFill((hudinfo[HUD_BOSSPOSITION].x)-(bossihealth*8)+(bosshealth*8)+8, hudinfo[HUD_BOSSPOSITION].y+8, framealt, 8, 103|V_SNAPTORIGHT|V_SNAPTOTOP);
	V_DrawString((hudinfo[HUD_BOSSPOSITION].x)-(bossihealth*8)+9, hudinfo[HUD_BOSSPOSITION].y+8, V_TRANSLUCENT|V_SNAPTORIGHT|V_SNAPTOTOP, "BOSS");

	//setting effect related stuff for the future.
	lastbosshealth = bosshealth;
	if (framealt)
		--framealt;
}

static void ST_drawCheckPointTimes(void)
{
	if (stplyr->exiting || stplyr->playerstate != PST_LIVE || timeinmap < (TICRATE*2)+2)
		return;

	//normal checkpoint times
	if (stplyr->starposttime && stplyr->starposttime+(TICRATE*2) >= stplyr->realtime)
	{
		tic_t startime = stplyr->starposttime;

		V_DrawCenteredString(BASEVIDWIDTH/2, 160, ((leveltime%2) ? V_TRANSLUCENT : 0),
		                     va("%d:%02d.%02d", G_TicsToMinutes(startime, true), G_TicsToSeconds(startime), G_TicsToCentiseconds(startime)));
	}

	//per-lap checkpoint times
	if (circuitmap && stplyr->laps && ((stplyr->starpostnum - 1) + (numstarposts+1)*stplyr->laps) > (unsigned)numstarposts
		&& ((stplyr->starpostnum - 1) + (numstarposts+1)*stplyr->laps) < 256 //SIGSEGV prevention
		&& stplyr->starposttime+(TICRATE*2) >= stplyr->realtime)
	{
		INT32 postnum = ((numstarposts+1)*(stplyr->laps - (stplyr->starpostnum ? 0 : 1))) - 1;
		tic_t laptime = stplyr->starposttime - stplyr->checkpointtimes[postnum];

		V_DrawCenteredString(BASEVIDWIDTH/2, 152, V_YELLOWMAP|((leveltime%2) ? V_TRANSLUCENT : 0),
		                     va("%d:%02d.%02d", G_TicsToMinutes(laptime, true), G_TicsToSeconds(laptime), G_TicsToCentiseconds(laptime)));
	}

	//ahead/behind times for Race gametype only
	//good frikkin' luck trying to read this, it can be a bit confusing for sure!
	//P_CheckPlayerAhead handles SIGSEGV prevention here, so no need to worry -- the values will be correct
	if (gametype == GT_RACE && stplyr->playerahead)
	{
		INT32 playerahead = stplyr->playerahead - 1;
		INT32 postnum = (stplyr->starpostnum - 1) + (numstarposts+1)*stplyr->laps;

		if (playerahead == 65535 && stplyr->starposttime+(TICRATE*2) >= stplyr->realtime) //special case, means we tied
			V_DrawCenteredString(BASEVIDWIDTH/2, 168, ((leveltime%2) ? V_TRANSLUCENT : 0), "0:00.00");

		else if (playerahead & 256) //P_CheckPlayerAhead modified our stat because the second placed man decided to finally show up
		{
			if (players[playerahead & 255].checkpointtimes[postnum]+(TICRATE*2) >= stplyr->realtime)
			{
				INT32 startime = players[playerahead & 255].checkpointtimes[postnum] - stplyr->checkpointtimes[postnum];

				V_DrawCenteredString(BASEVIDWIDTH/2, 168, ((leveltime%2) ? V_TRANSLUCENT : 0),
				                     va("\x84-%d:%02d.%02d", G_TicsToMinutes(startime, true), G_TicsToSeconds(startime), G_TicsToCentiseconds(startime)));
			}
		}

		else //uh oh, WE'RE the one who's behind!
		{
			if (stplyr->starposttime+(TICRATE*2) >= stplyr->realtime)
			{
				INT32 startime = stplyr->checkpointtimes[postnum] - players[playerahead].checkpointtimes[postnum];

				V_DrawCenteredString(BASEVIDWIDTH/2, 168, ((leveltime%2) ? V_TRANSLUCENT : 0),
				                     va("\x85+%d:%02d.%02d", G_TicsToMinutes(startime, true), G_TicsToSeconds(startime), G_TicsToCentiseconds(startime)));
			}
		}
	}
}

// used in the intermission screen too
const char *ST_determineGolfScore(INT32 score, INT32 par)
{
	INT32 combined = score-par;

	if (score == 1) // overrides all
		return "HOLE IN ONE";

	switch (combined)
	{
		case -6: return "PHOENIX";
		case -5: return "OSTRICH";
		case -4: return "CONDOR";
		case -3: return "ALBATROSS";
		case -2: return "EAGLE";
		case -1: return "BIRDIE";
		case 0:  return "PAR";
		case 1:  return "BOGEY";
		case 2:  return "DOUBLE BOGEY";
		case 3:  return "TRIPLE BOGEY";
		default:
			if (combined > 0)
				return va("%d OVER PAR", combined);
			else
				return va("%d UNDER PAR", -combined);
	}
}

static void ST_drawGolfHud(void)
{
	static INT32 hudeffects;

	//since after this function is called, we stop drawing the hud, we should do this here
	ST_drawLevelTitle();

	//Hide these if the scores are being shown
	if (!hu_showscores)
	{
		//strokes.  as it says.
		ST_DrawOverlayNum(SCX(hudinfo[HUD_GOLFSTROKESNUM].x), SCY(hudinfo[HUD_GOLFSTROKESNUM].y), golf_strokes[gamemap-1], tallnum, NULL);
		V_DrawScaledPatch(SCX(hudinfo[HUD_GOLFSTROKES].x), SCY(hudinfo[HUD_GOLFSTROKES].y), V_NOSCALESTART|V_TRANSLUCENT, sbostrks);

		//par, taken from the map header infos
		ST_DrawOverlayNum(SCX(hudinfo[HUD_GOLFPARNUM].x), SCY(hudinfo[HUD_GOLFPARNUM].y), mapheaderinfo[gamemap-1].golfpar, tallnum, NULL);
		V_DrawScaledPatch(SCX(hudinfo[HUD_GOLFPAR].x), SCY(hudinfo[HUD_GOLFPAR].y), V_NOSCALESTART|V_TRANSLUCENT, sbopar);
	}

	if (stplyr->playerstate == PST_DEAD) //they've gone OB on us.  nice.  $10 says they jumped off on their own accord! =P
		V_DrawScaledPatch((BASEVIDWIDTH - golfob->width)/2, STRINGY(BASEVIDHEIGHT/2 - (golfob->height/2)), 0, golfob);

	if (hudeffects >= 0 && (stplyr->pflags & PF_STARTDASH)) //set hudeffects properly so we can get going!
		hudeffects = -1;

	//POWER:
	if (hudeffects)
	{
		INT32 ymod = 0; //special effects

		switch (abs(hudeffects))
		{
			case 1:
				ymod = 24;
				break;
			case 2:
				ymod = 16;
				break;
			case 3:
				ymod = 8;
				break;
			default:
				ymod = 0;
				break;
		}

		M_DrawTextBox(8, BASEVIDHEIGHT-30+ymod, 36, 1, V_SNAPTOBOTTOM);

		if (stplyr->dashspeed == 0 && (hudeffects < 0 || hudeffects > 3))
		{
			hudeffects = 3;
		}
		if (stplyr->dashspeed == 18)
		{
			V_DrawFill(16, BASEVIDHEIGHT-22+ymod, 288, 8, 103|V_SNAPTOBOTTOM);
			if (!(stplyr->pflags & PF_STARTDASH))  //they're moving
				V_DrawString(227, BASEVIDHEIGHT-22+ymod, V_TRANSLUCENT|V_SNAPTOBOTTOM, "Nice shot!");
		}
		else if ((18-abs(stplyr->dashspeed-18)) <= 8)
		{
			V_DrawFill(16, BASEVIDHEIGHT-22+ymod, 8*(18-abs(stplyr->dashspeed-18)), 8, 128|V_SNAPTOBOTTOM);
		}
		else if ((18-abs(stplyr->dashspeed-18)) <= 14)
		{
			V_DrawFill(16, BASEVIDHEIGHT-22+ymod, (8*8)+16*((18-abs(stplyr->dashspeed-18))-8), 8, 128|V_SNAPTOBOTTOM);
		}
		else
		{
			V_DrawFill(16, BASEVIDHEIGHT-22+ymod, (8*8)+(16*6)+24*((18-abs(stplyr->dashspeed-18))-14), 8, 128|V_SNAPTOBOTTOM);
		}

		V_DrawString(17, BASEVIDHEIGHT-22+ymod, V_TRANSLUCENT|V_SNAPTOBOTTOM, "POWER");

		if (!(stplyr->pflags & PF_STARTDASH) && hudeffects < 0) //hudeffects is negative, and they're on their way; they just started to get going!
			hudeffects = 2*TICRATE; //override it!
		else if (hudeffects)
			hudeffects--;
	}

	if (stplyr->exiting) //player is exiting, show them how well they did
	{
		// don't show (+9) if it's already saying "9 OVER PAR", redundancy is silly.
		const char *scoretext = ST_determineGolfScore(golf_strokes[gamemap-1], mapheaderinfo[gamemap-1].golfpar);

		if (hudeffects > 3)
			hudeffects = 3; // force the power bar to hide

		V_DrawLevelTitle((BASEVIDWIDTH/2) - (V_LevelNameWidth(scoretext)/2), 174, 0, scoretext); //using drawleveltitle because it looks nice here
		V_DrawCenteredString((BASEVIDWIDTH/2), 190, 0, va("(%+d)", golf_strokes[gamemap-1] - mapheaderinfo[gamemap-1].golfpar));
	}

	//draw debug info here since we won't do it normally
	ST_drawDebugInfo();

	if (hu_showscores)
		return;

	//draw powerup icons
	if (stplyr->powers[pw_golf_fullshot] || stplyr->powers[pw_jumpshield] || stplyr->powers[pw_forceshield]
	 || stplyr->powers[pw_watershield] || stplyr->powers[pw_bombshield] || stplyr->powers[pw_ringshield])
	{
		INT32 monitor1pos = 160, monitor2pos = 160;

		if (
			(stplyr->powers[pw_golf_fullshot] /* || put future powerups here */)
			&&
			(stplyr->powers[pw_jumpshield] || stplyr->powers[pw_forceshield] || stplyr->powers[pw_watershield] ||
			 stplyr->powers[pw_bombshield] || stplyr->powers[pw_ringshield])
		   )
		{ // spread the powerups out if there's more than one
			monitor1pos = 150;
			monitor2pos = 170;
		}

		if (stplyr->powers[pw_golf_fullshot])
			V_DrawScaledPatch(monitor1pos, 22, V_SNAPTORIGHT|V_TRANSLUCENT, sneakers);

		if (stplyr->powers[pw_jumpshield])
			V_DrawScaledPatch(monitor2pos, 22, V_SNAPTOTOP, jumpshield);
		else if (stplyr->powers[pw_forceshield] == 2 || (stplyr->powers[pw_forceshield] == 1 && (leveltime & 1)))
			V_DrawScaledPatch(monitor2pos, 22, V_SNAPTOTOP, forceshield);
		else if (stplyr->powers[pw_watershield])
			V_DrawScaledPatch(monitor2pos, 22, V_SNAPTOTOP, watershield);
		else if (stplyr->powers[pw_bombshield])
			V_DrawScaledPatch(monitor2pos, 22, V_SNAPTOTOP, bombshield);
		else if (stplyr->powers[pw_ringshield])
			V_DrawScaledPatch(monitor2pos, 22, V_SNAPTOTOP, ringshield);

		V_DrawCenteredString(160, 2, V_SNAPTOTOP|V_YELLOWMAP, "POWERUPS");
	}
}

// Draw the status bar overlay, customisable: the user chooses which
// kind of information to overlay
//
/// \todo Split up this 1400 line function into multiple functions!
//
static void ST_overlayDrawer(void)
{
	if (golfmode) // Golf HUD is so different that we just draw it alone.
	{
		ST_drawGolfHud();
		return;
	}

	if (!(hu_showscores && (netgame || multiplayer)))
	{
		if (maptol & TOL_NIGHTS)
			ST_drawNiGHTSHUD();
		else if (!(hu_showscores && (netgame || multiplayer))) //hu_showscores = auto hide score/time/rings when tab rankings are shown
		{
			ST_drawScore();
			ST_drawTime();
			if (!ultimatemode)
			{
				ST_drawRings();
				if (gametype == GT_COOP || gametype == GT_RACE)
					ST_drawLives();
			}

			//draw the starpost times, too!
			ST_drawCheckPointTimes();
		}
	}

	// GAME OVER pic
	if ((gametype == GT_COOP || gametype == GT_RACE) && stplyr->lives <= 0 && !(hu_showscores && (netgame || multiplayer)))
	{
		patch_t *p;

		if (countdown == 1)
			p = timeover;
		else
			p = sboover;

		V_DrawScaledPatch((BASEVIDWIDTH - SHORT(p->width))/2, STRINGY(BASEVIDHEIGHT/2 - (SHORT(p->height)/2)), 0, p);
	}

	if (cv_objectplace.value && stplyr->mo && stplyr->mo->target)
	{
		char x[8], y[8], z[8];
		char doomednum[8], thingflags[8];
		sprintf(x, "%d", stplyr->mo->x>>FRACBITS);
		sprintf(y, "%d", stplyr->mo->y>>FRACBITS);
		sprintf(z, "%d", stplyr->mo->z>>FRACBITS);
		sprintf(doomednum, "%d", stplyr->mo->target->info->doomednum);
		sprintf(thingflags, "%d", cv_objflags.value);
		V_DrawString(16, 98, 0, "X =");
		V_DrawString(48, 98, 0, x);
		V_DrawString(16, 108, 0, "Y =");
		V_DrawString(48, 108, 0, y);
		V_DrawString(16, 118, 0, "Z =");
		V_DrawString(48, 118, 0, z);
		V_DrawString(16, 128, 0, "thing # =");
		V_DrawString(16+84, 128, 0, doomednum);
		V_DrawString(16, 138, 0, "flags =");
		V_DrawString(16+56, 138, 0, thingflags);
		V_DrawString(16, 148, 0, "snap =");
		V_DrawString(16+48, 148, 0, cv_snapto.string);
	}

	if (!hu_showscores) // hide the following if TAB is held
	{
		// Draw Match-related stuff
		//\note Match HUD is drawn no matter what gametype.
		// ... just not if you're a spectator.
		if (!((((gametype == GT_MATCH && !cv_matchtype.value) || gametype == GT_TAG) && stplyr->spectator)
		// ... or if you're in Tag and not IT.
		 || (gametype == GT_TAG && !(stplyr->pflags & PF_TAGIT))
		// or if you're a spectator in Shards.
		 || (gametype == GT_SHARDS && stplyr->spectator)
		 || (((gametype == GT_MATCH && cv_matchtype.value) || gametype == GT_CTF) && !stplyr->ctfteam)))
			ST_drawMatchHUD();

		// Race HUD Stuff
		if (gametype == GT_RACE)
			ST_drawRaceHUD();
		// CTF HUD Stuff
		else if (gametype == GT_CTF)
			ST_drawCTFHUD();

		// Team names for team gametypes
		if ((gametype == GT_CTF) || (gametype == GT_MATCH && cv_matchtype.value))
			ST_drawTeamName();

		// draw current chain count
		if ((
#ifdef CHAOSISNOTDEADYET
			(gametype == GT_CHAOS) ||
#endif
			(cv_drawchain.value && (gametype == GT_COOP || gametype == GT_RACE)))
				&& !(hu_showscores && (netgame || multiplayer)))
			ST_drawChain();
	}

	// Special Stage HUD
	if (!useNightsSS && gamemap >= sstage_start && gamemap <= sstage_end)
		ST_drawSpecialStageHUD();

	if (!hu_showscores) // again, hide the following if TAB is held
	{
		// Emerald Hunt Indicators
		if (hunt1 && hunt1->health)
			ST_drawEmeraldHuntIcon(hunt1, HUD_HUNTPIC1);
		if (hunt2 && hunt2->health)
			ST_drawEmeraldHuntIcon(hunt2, HUD_HUNTPIC2);
		if (hunt3 && hunt3->health)
			ST_drawEmeraldHuntIcon(hunt3, HUD_HUNTPIC3);

		if(!P_IsLocalPlayer(stplyr))
		{
			char name[MAXPLAYERNAME+1];
			// shorten the name if its more than twelve characters.
			strlcpy(name, player_names[stplyr-players], 13);

			// Show name of player being displayed
			V_DrawCenteredString((BASEVIDWIDTH/6), BASEVIDHEIGHT-80, 0, "Viewpoint:");
			V_DrawCenteredString((BASEVIDWIDTH/6), BASEVIDHEIGHT-64, V_ALLOWLOWERCASE, name);
		}

		// This is where we draw all the fun cheese if you have the chasecam off!
		if ((stplyr == &players[consoleplayer] && !cv_chasecam.value)
			|| ((splitscreen && stplyr == &players[secondarydisplayplayer]) && !cv_chasecam2.value)
			|| (stplyr == &players[displayplayer] && !cv_chasecam.value))
		{
			ST_drawFirstPersonHUD();
		}
	}

	//always draw monitor icons
	ST_drawMonitorIcons();

	// boss!
	if (gametype == GT_COOP && !stplyr->exiting && !((netgame || multiplayer) && hu_showscores)) //all this should be cleaned up later
	{
		thinker_t *th;
		mobj_t *mo2;
		boolean bossexists = false;
		INT32 bossinithealth = 0, bosshealth = 0;

		// scan the thinkers to find any bosses!
		for (th = thinkercap.next; th != &thinkercap; th = th->next)
		{
			if (th->function.acp1 != (actionf_p1)P_MobjThinker)
				continue;
			mo2 = (mobj_t *)th;

			if (mo2->flags & MF_BOSS && (!(mo2->flags2 & MF2_DEADBOSS)))
			{
				if (mo2->target) //only show it if it has a target; ie: it's spotted the player
					bossexists = 1;
				bossinithealth += mo2->info->spawnhealth; //multiple bosses support, ala lbfz3 or arena zone =P
				bosshealth += mo2->health;
			}
		}
		if (bossexists)
			ST_drawBossHUD(bosshealth, bossinithealth);
	}

	// Snarky cheater message.
	if (!cv_debug && ((stplyr->pflags  & PF_GODMODE)
	               || (stplyr->pflags  & PF_NOCLIP)
	               || (stplyr->pflags2 & PF2_NONTARGETABLE)))
		V_DrawCenteredString(BASEVIDWIDTH/2, 170, V_ALLOWLOWERCASE|V_TRANSLUCENT, "\x84" "Cheaters always prosper.");

	if (!(netgame || multiplayer) && !modifiedgame && gamemap == 11 && ALL7EMERALDS(emeralds)
		&& stplyr->mo && stplyr->mo->subsector && stplyr->mo->subsector->sector-sectors == 1361)
	{
		if (grade & 2048)
		{
			V_DrawCenteredString(BASEVIDWIDTH/2, 70, 0, "I, Pope Rededict XVI proclaim");
			V_DrawCenteredString(BASEVIDWIDTH/2, 80, 0, "AJ & Amy");
			V_DrawCenteredString(BASEVIDWIDTH/2, 90, 0, "Husband & Wife");
			V_DrawCenteredString(BASEVIDWIDTH/2, 100, 0, "on this day");
			V_DrawCenteredString(BASEVIDWIDTH/2, 110, 0, "May 16, 2009");

			P_GivePlayerRings(stplyr, 9999, true);
		}
		else
		{
			V_DrawCenteredString(BASEVIDWIDTH/2,  60, 0, "Oh... it's you again...");
			V_DrawCenteredString(BASEVIDWIDTH/2,  80, 0, "Look, I wanted to apologize for the way");
			V_DrawCenteredString(BASEVIDWIDTH/2,  90, 0, "I've acted in the past.");
			V_DrawCenteredString(BASEVIDWIDTH/2, 110, 0, "I've seen the error of my ways");
			V_DrawCenteredString(BASEVIDWIDTH/2, 120, 0, "and turned over a new leaf.");
			V_DrawCenteredString(BASEVIDWIDTH/2, 140, 0, "Instead of sending people to hell,");
			V_DrawCenteredString(BASEVIDWIDTH/2, 150, 0, "I now send them to heaven!");

			P_LinedefExecute(4200, stplyr->mo, stplyr->mo->subsector->sector);
			P_LinedefExecute(4201, stplyr->mo, stplyr->mo->subsector->sector);
			stplyr->mo->momx = stplyr->mo->momy = 0;
		}
	}

	if (mariomode && stplyr->exiting)
	{
		/// \todo doesn't belong in status bar code AT ALL
		thinker_t *th;
		mobj_t *mo2;
		boolean foundtoad = false;

		// scan the remaining thinkers
		// to find toad
		for (th = thinkercap.next; th != &thinkercap; th = th->next)
		{
			if (th->function.acp1 != (actionf_p1)P_MobjThinker)
				continue;

			mo2 = (mobj_t *)th;
			if (mo2->type == MT_TOAD)
			{
				foundtoad = true;
				break;
			}
		}

		if (foundtoad)
		{
			V_DrawCenteredString(160, 32+16, 0, "Thank you!");
			V_DrawCenteredString(160, 44+16, 0, "But our earless leader is in");
			V_DrawCenteredString(160, 56+16, 0, "another castle!");
		}
	}

	// draw level title Tails
	if (*mapheaderinfo[gamemap-1].lvlttl != '\0' && !(hu_showscores && (netgame || multiplayer)))
		ST_drawLevelTitle();

	if (!hu_showscores && netgame && (gametype == GT_RACE || gametype == GT_COOP) && stplyr->lives <= 0 && displayplayer == consoleplayer && countdown != 1)
		V_DrawCenteredString(BASEVIDWIDTH/2, BASEVIDHEIGHT/3, 0, "Press F12 to watch another player.");

	if (!hu_showscores && netgame && (gametype == GT_TAG && cv_tagtype.value) && displayplayer == consoleplayer &&
		(!stplyr->spectator && !(stplyr->pflags & PF_TAGIT)) && (leveltime > hidetime * TICRATE))
	{
		V_DrawCenteredString(BASEVIDWIDTH/2, (splitscreen) ? STRINGY(136) : STRINGY(140), V_TRANSLUCENT, "You cannot move while hiding.");
		V_DrawCenteredString(BASEVIDWIDTH/2,                                STRINGY(150), V_TRANSLUCENT, "Press F12 to watch another player.");
	}

	if (!hu_showscores && (netgame || splitscreen))
	{
		if ((gametype == GT_MATCH || gametype == GT_TAG || gametype == GT_CTF || gametype == GT_SHARDS)
			&& stplyr->playerstate == PST_DEAD && stplyr->lives) //Death overrides spectator text.
		{
			V_DrawCenteredString(BASEVIDWIDTH/2, STRINGY(132), V_TRANSLUCENT, va("Press %s to respawn.",
			                     G_GamekeyToString(gc_jump, (splitscreen && stplyr == &players[secondarydisplayplayer]))));

			if (((gametype == GT_MATCH && !cv_matchtype.value) || gametype == GT_TAG || gametype == GT_SHARDS) && !stplyr->spectator)
				V_DrawCenteredString(BASEVIDWIDTH/2, STRINGY(148), V_TRANSLUCENT, va("Press %s to Spectate.",
				                     G_GamekeyToString(gc_tossflag, (splitscreen && stplyr == &players[secondarydisplayplayer]))));
		}
		else if ((((gametype == GT_MATCH && !cv_matchtype.value) || gametype == GT_TAG || gametype == GT_SHARDS) && stplyr->spectator)
		 || (((gametype == GT_MATCH && cv_matchtype.value) || gametype == GT_CTF) && !stplyr->ctfteam))
		{
			V_DrawCenteredString(BASEVIDWIDTH/2, STRINGY(60), V_TRANSLUCENT, "You are a spectator.");
			if ((gametype == GT_MATCH && cv_matchtype.value) || gametype == GT_CTF)
				V_DrawCenteredString(BASEVIDWIDTH/2, STRINGY(132), V_TRANSLUCENT, va("Press %s to be assigned to a team.",
				                     G_GamekeyToString(gc_fire, (splitscreen && stplyr == &players[secondarydisplayplayer]))));
			else
				V_DrawCenteredString(BASEVIDWIDTH/2, STRINGY(132), V_TRANSLUCENT, va("Press %s to enter the game.",
				                     G_GamekeyToString(gc_fire, (splitscreen && stplyr == &players[secondarydisplayplayer]))));
			V_DrawCenteredString(BASEVIDWIDTH/2, STRINGY(148), V_TRANSLUCENT, "Press F12 to watch another player.");
			V_DrawCenteredString(BASEVIDWIDTH/2, STRINGY(164), V_TRANSLUCENT, va("Press %s to float, %s to sink.",
			                     G_GamekeyToString(gc_jump, (splitscreen && stplyr == &players[secondarydisplayplayer])),
			                     G_GamekeyToString(gc_use, (splitscreen && stplyr == &players[secondarydisplayplayer]))));
		}
	}

	if ((netgame || splitscreen) && !hu_showscores)
	{
		if ((stplyr->playerstate == PST_DEAD && stplyr->lives) && (gametype != GT_COOP && gametype != GT_RACE))
			V_DrawCenteredString(BASEVIDWIDTH/2, (splitscreen) ? STRINGY(184) : STRINGY(192), 0,
			                     va("Auto-respawn in %d", (stplyr->deadtimer-(30*TICRATE))/TICRATE));

		else if (gametype == GT_TAG && stplyr->playerstate != PST_DEAD && !stplyr->spectator)
			ST_drawTagText();

		// OVERTIME! OVERTIME! OVERTIME! OVERTIME! OVERTIME! OVERTIME! OVERTIME! OVERTIME! OVERTIME! OVERTIME! OVERTIME! OVERTIME! You failed, stalemate!
		else if ((gametype == GT_MATCH || gametype == GT_CTF) && cv_overtime.value
			&& (leveltime > (timelimitintics + TICRATE/2)) && cv_timelimit.value && (leveltime/TICRATE % 2 == 0))
			V_DrawCenteredString(BASEVIDWIDTH/2, (splitscreen) ? STRINGY(184) : STRINGY(192), 0, "OVERTIME!");

		// Countdown timer for Race Mode
		else if (countdown)
			V_DrawCenteredString(BASEVIDWIDTH/2, (splitscreen) ? STRINGY(184) : STRINGY(192), 0,
			                     va("%s%d", (countdown/TICRATE < 10 && leveltime/5 & 1) ? "\x85" : "", countdown/TICRATE));
	}

	if (stplyr->deadtimer > 0 && (stplyr->deadtimer < gameovertics) && stplyr->lives <= 0)
	{
		if (!netgame && !multiplayer)
		{
			if (stplyr->continues != 0) // Player has continues, so let's use them!
				ST_drawContinueHUD();
			else // Just go to the title screen
				Command_ExitGame_f();
		}
	}

	ST_drawDebugInfo();
}
