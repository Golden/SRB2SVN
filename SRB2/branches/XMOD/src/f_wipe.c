// Emacs style mode select   -*- C++ -*-
//-----------------------------------------------------------------------------
//
// Copyright (C) 1993-1996 by id Software, Inc.
// Copyright (C) 1998-2000 by DooM Legacy Team.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//-----------------------------------------------------------------------------
/// \file
/// \brief Wipe screen special effect.

#include "i_video.h"
#include "v_video.h"
#include "r_draw.h" // transtable
#include "p_pspr.h" // tr_transxxx
#include "i_system.h"
#include "m_menu.h"
#include "f_finale.h"
#if defined (SHUFFLE) && defined (HWRENDER)
#include "hardware/hw_main.h"
#endif

#if NUMSCREENS < 3
#define NOWIPE // do not enable wipe image post processing for ARM, SH and MIPS CPUs
#endif

#include "doomstat.h"

//--------------------------------------------------------------------------
//                        SCREEN WIPE PACKAGE
//--------------------------------------------------------------------------

boolean WipeInAction = false;
#ifndef NOWIPE

static UINT8 *wipe_scr_start; //screens 2
static UINT8 *wipe_scr_end; //screens 3
static UINT8 *wipe_scr; //screens 0

// definitions
static INT32 F_DownwardsWipe(INT32 width, INT32 height, tic_t ticks);
static INT32 F_NormalWipe(INT32 width, INT32 height, tic_t ticks);

// number of changed pixels from wipe start to wipe end, used for determining...
// - whether to wipe at all (0 changed)
// - when to end the wipe (sometimes there will be "stuck" pixels that constantly change, etc)
static INT32 wipe_num_changed_pixels;
static INT32 drawnwipetics = 0;

// Which wipe function?
static INT32 (*f_wipefunc)(INT32, INT32, tic_t) = F_NormalWipe;

/**	\brief	start the wipe

	\param	width	width of wipe
	\param	height	height of wipe
	\param	ticks	ticks for wipe

	\return	unknown


*/
static inline INT32 F_InitWipe(INT32 width, INT32 height, tic_t ticks)
{
	if(rendermode != render_soft)
		return 0;
	(void)ticks;
	M_Memcpy(wipe_scr, wipe_scr_start, width*height*scr_bpp);
	return 0;
}

/**	\brief	downwards wipe ticker

	\param	width	width of wipe
	\param	height	height of wipe
	\param	ticks	ticks for wipe

	\return	0
*/
static INT32 F_DownwardsWipe(INT32 width, INT32 height, tic_t ticks)
{
	UINT8 *w;
	UINT8 *e;
	UINT8 newval;
	INT32 wipeheight;

	while (ticks--)
	{

#ifdef SHUFFLE
		if(rendermode != render_soft)
		{
			// Just do a normal wipe. Sorry, OpenGL users!
			HWR_DoScreenWipe();
			continue;
		}
#endif

		w = wipe_scr;
		e = wipe_scr_end;

		wipeheight = ((height / BASEVIDHEIGHT) + ((height % BASEVIDHEIGHT) ? 1 : 0)) * 8 * (drawnwipetics+1);
		if (wipeheight > height)
			wipeheight = height;

		while (w != wipe_scr + width*wipeheight)
		{
			if (*w != *e)
			{
				if (((newval = transtables[(*e<<8) + *w + ((tr_trans80-1)<<FF_TRANSSHIFT)]) == *w)
					&& ((newval = transtables[(*e<<8) + *w + ((tr_trans50-1)<<FF_TRANSSHIFT)]) == *w)
					&& ((newval = transtables[(*w<<8) + *e + ((tr_trans80-1)<<FF_TRANSSHIFT)]) == *w))
				{
					newval = *e;
				}
				*w = newval;
			}
			w++;
			e++;
		}
	}

	++drawnwipetics;
	return (drawnwipetics >= 35);
}

/**	\brief	wipe ticker

	\param	width	width of wipe
	\param	height	height of wipe
	\param	ticks	ticks for wipe

	\return	the change in wipe
*/
static INT32 F_NormalWipe(INT32 width, INT32 height, tic_t ticks)
{
	INT32 changed = 0;
	UINT8 *w;
	UINT8 *e;
	UINT8 newval;
	static INT32 slowdown = 0;

	while (ticks--)
	{
		// slowdown
		if (slowdown++)
		{
			slowdown = 0;
			return false;
		}

#ifdef SHUFFLE
		if(rendermode != render_soft)
		{

			HWR_DoScreenWipe();
			//TODO: make this work for hardware, somehow.
			changed = wipe_num_changed_pixels;
			continue;
		}
#endif
		w = wipe_scr;
		e = wipe_scr_end;

		while (w != wipe_scr + width*height)
		{
			if (*w != *e)
			{
				if (((newval = transtables[(*e<<8) + *w + ((tr_trans80-1)<<FF_TRANSSHIFT)]) == *w)
					&& ((newval = transtables[(*e<<8) + *w + ((tr_trans50-1)<<FF_TRANSSHIFT)]) == *w)
					&& ((newval = transtables[(*w<<8) + *e + ((tr_trans80-1)<<FF_TRANSSHIFT)]) == *w))
				{
					newval = *e;
				}
				*w = newval;
				++changed;
			}
			w++;
			e++;
		}
	}
	++drawnwipetics;

	if (drawnwipetics >= 16)
		return true; //screw it, we are DONE!
	else
		// when less than a sixteenth of the pixels that we know were supposed to be changed from start to
		// finish have been changed, assume it's done and end the wipe.
		return (changed < wipe_num_changed_pixels/16);
}
#endif

/** Save the "before" screen of a wipe.
  */
void F_WipeStartScreen(void)
{
#ifndef NOWIPE
	if(rendermode != render_soft)
	{
#if defined (SHUFFLE) && defined (HWRENDER)
		HWR_StartScreenWipe();
#endif
		return;
	}
	wipe_scr_start = screens[2];
	if(rendermode == render_soft)
		I_ReadScreen(wipe_scr_start);
#endif
}

/** Save the "after" screen of a wipe.
  *
  * \param x      Starting x coordinate of the starting screen to restore.
  * \param y      Starting y coordinate of the starting screen to restore.
  * \param width  Width of the starting screen to restore.
  * \param height Height of the starting screen to restore.
  */
void F_WipeEndScreen(INT32 x, INT32 y, INT32 width, INT32 height)
{
	if(rendermode != render_soft)
	{
#if defined (SHUFFLE) && defined (HWRENDER)
		HWR_EndScreenWipe();
#endif
		return;
	}
#ifdef NOWIPE
	(void)x;
	(void)y;
	(void)width;
	(void)height;
#else
	wipe_scr_end = screens[3];
	I_ReadScreen(wipe_scr_end);
	V_DrawBlock(x, y, 0, width, height, wipe_scr_start);
#endif
}

/**	\brief	wipe screen

	\param	x	x starting point
	\param	y	y starting point
	\param	width	width of wipe
	\param	height	height of wipe
	\param	ticks	ticks for wipe

	\return	if true, the wipe is done


*/

INT32 F_ScreenWipe(INT32 x, INT32 y, INT32 width, INT32 height, tic_t ticks)
{
	INT32 rc = 1;
	// initial stuff
	(void)x;
	(void)y;
#ifdef NOWIPE
	width = height = ticks = 0;
#else
	if (!WipeInAction)
	{
		WipeInAction = true;
		wipe_scr = screens[0];
		F_InitWipe(width, height, ticks);
	}

	rc = f_wipefunc(width, height, ticks);

	if (rc)
		WipeInAction = false; //Alam: All done?
#endif
	return rc;
}

//
// F_RunWipe
//
//
// After setting up the screens you want to
// wipe, calling this will do a 'typical'
// wipe.
//
void F_RunWipe(INT32 wipenum, boolean drawMenu)
{
#ifdef NOWIPE
	(void)wipenum;
	(void)drawMenu;
#else
	tic_t wipestart, tics, nowtime;
	boolean done;

	drawnwipetics = 0;
	wipestart = I_GetTime() - 1;

	// wipe selection
	switch (wipenum)
	{
		case 1:  f_wipefunc = F_DownwardsWipe; break;
		default: f_wipefunc = F_NormalWipe; break;
	}

	if (cv_debug)
		CONS_Printf("Wipe number: %d (function: %p)\n", wipenum, f_wipefunc);

	// now, determine the number of changed pixels for future calculations
	if (rendermode != render_soft) // Meh.
		wipe_num_changed_pixels = vid.width * vid.height;
	else
	{
		UINT8 *st = wipe_scr_start;
		UINT8 *en = wipe_scr_end;
		INT32 width = vid.width, height = vid.height;

		wipe_num_changed_pixels = 0;

		for (;st != wipe_scr_start + width * height;++st,++en)
		{
			if (*st != *en)
				++wipe_num_changed_pixels;
		}
	}

	if (!wipe_num_changed_pixels)
		return; //No point.

	do
	{
		do
		{
			nowtime = I_GetTime();
			tics = nowtime - wipestart;
			if (!tics) I_Sleep();
		} while (!tics);
		wipestart = nowtime;

#ifdef SHUFFLE
		done = F_ScreenWipe(0, 0, vid.width, vid.height, tics);
#else
		if (rendermode == render_soft)
			done = F_ScreenWipe(0, 0, vid.width, vid.height, tics);
		else
			done = true;
#endif
		I_OsPolling();
		I_UpdateNoBlit();

		if (drawMenu)
			M_Drawer(); // menu is drawn even on top of wipes

		if (rendermode == render_soft)
			I_FinishUpdate(); // page flip or blit buffer
	} while (!done);
#endif
}
