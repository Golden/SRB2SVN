// Emacs style mode select   -*- C++ -*-
//-----------------------------------------------------------------------------
//
// Copyright (C) 1993-1996 by id Software, Inc.
// Copyright (C) 1998-2000 by DooM Legacy Team.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//-----------------------------------------------------------------------------
/// \file
/// \brief XMOD's extremely revamped menu system.

#ifdef __GNUC__
#include <unistd.h>
#endif

#include "m_menu.h"

#include "doomdef.h"
#include "dstrings.h"
#include "d_main.h"
#include "d_netcmd.h"
#include "console.h"
#include "r_local.h"
#include "hu_stuff.h"
#include "g_game.h"
#include "g_input.h"
#include "m_argv.h"

// Data.
#include "sounds.h"
#include "s_sound.h"
#include "i_system.h"

#include "v_video.h"
#include "i_video.h"
#include "keys.h"
#include "z_zone.h"
#include "w_wad.h"
#include "p_local.h"
#include "f_finale.h"

#ifdef HWRENDER
#include "hardware/hw_main.h"
#endif

#include "d_net.h"
#include "mserv.h"
#include "m_misc.h"
#include "byteptr.h"
#include "st_stuff.h"
#include "i_sound.h"

#ifdef PC_DOS
#include <stdio.h> // for snprintf
int	snprintf(char *str, size_t n, const char *fmt, ...);
//int	vsnprintf(char *str, size_t n, const char *fmt, va_list ap);
#endif

#define SKULLXOFF -32
#define LINEHEIGHT 16
#define STRINGHEIGHT 8
#define FONTBHEIGHT 20
#define SMALLLINEHEIGHT 8
#define SLIDER_RANGE 10
#define SLIDER_WIDTH (8*SLIDER_RANGE+6)
#define MAXSTRINGLENGTH 32
#define SERVERS_PER_PAGE 10

// Stuff for customizing the player select screen Tails 09-22-2003
description_t description[15] =
{
	{"             Fastest\n                 Speed Thok\n             Not a good pick\nfor starters, but when\ncontrolled properly,\nSonic is the most\npowerful of the three.", "SONCCHAR", "", "SONIC"},
	{"             Slowest\n                 Fly/Swim\n             Good for\nbeginners. Tails\nhandles the best. His\nflying and swimming\nwill come in handy.", "TAILCHAR", "", "TAILS"},
	{"             Medium\n                 Glide/Climb\n             A well rounded\nchoice, Knuckles\ncompromises the speed\nof Sonic with the\nhandling of Tails.", "KNUXCHAR", "", "KNUCKLES"},
	{"             Unknown\n                 Unknown\n             None", "SONCCHAR", "", ""},
	{"             Unknown\n                 Unknown\n             None", "SONCCHAR", "", ""},
	{"             Unknown\n                 Unknown\n             None", "SONCCHAR", "", ""},
	{"             Unknown\n                 Unknown\n             None", "SONCCHAR", "", ""},
	{"             Unknown\n                 Unknown\n             None", "SONCCHAR", "", ""},
	{"             Unknown\n                 Unknown\n             None", "SONCCHAR", "", ""},
	{"             Unknown\n                 Unknown\n             None", "SONCCHAR", "", ""},
	{"             Unknown\n                 Unknown\n             None", "SONCCHAR", "", ""},
	{"             Unknown\n                 Unknown\n             None", "SONCCHAR", "", ""},
	{"             Unknown\n                 Unknown\n             None", "SONCCHAR", "", ""},
	{"             Unknown\n                 Unknown\n             None", "SONCCHAR", "", ""},
	{"             Unknown\n                 Unknown\n             None", "SONCCHAR", "", ""},
};

static emblem_t *M_GetLevelEmblem(INT32 mapnum, INT32 player);

boolean menuactive = false;
boolean fromlevelselect = false;

customsecrets_t customsecretinfo[15];
INT32 inlevelselect = 0;

static INT32 saveSlotSelected = 0;
static char joystickInfo[8][25];
#ifndef NONET
static UINT32 serverlistpage;
#endif

static saveinfo_t savegameinfo[MAXSAVEGAMES]; // Extra info about the save games.

INT16 startmap; // Mario, NiGHTS, or just a plain old normal game?

static INT16 itemOn = 1; // menu item skull is on, Hack by Tails 09-18-2002
static INT16 skullAnimCounter = 10; // skull animation counter

static  boolean setupcontrols_secondaryplayer;
static  INT32   (*setupcontrols)[2];  // pointer to the gamecontrols of the player being edited

// shhh... what am I doing... nooooo!
static INT32 vidm_testingmode = 0;
static INT32 vidm_previousmode;
static INT32 vidm_current = 0;
static INT32 vidm_nummodes;
static INT32 vidm_column_size;

// what a headache.
static boolean shiftdown = false;

//
// PROTOTYPES
//

static void M_StopMessage(INT32 choice);

#ifndef NONET
static void M_NextServerPage(void);
static void M_PrevServerPage(void);
static void M_RoomInfoMenu(INT32 choice);
#endif
static void M_SortServerList(void);

static boolean listHasNoMaps = false;
#ifndef NONET
const char *room_motd;
#endif

static void M_DoMenuPatching(boolean islevelselect);

// Prototyping is fun, innit?
// ==========================================================================
// NEEDED FUNCTION PROTOTYPES GO HERE
// ==========================================================================

// the haxor message menu
menu_t MessageDef;

// Sky Room
static void M_Checklist(INT32 choice);
static void M_CustomSecretsMenu(INT32 choice);
static void M_NightsGame(INT32 choice);
static void M_MarioGame(INT32 choice);
static void M_SonicGolf(INT32 choice);
static void M_NAGZGame(INT32 choice);
static void M_CustomWarp(INT32 choice);
static void M_UltimateCheat(INT32 choice);
static void M_LevelSelect(INT32 choice);
static void M_GetAllEmeralds(INT32 choice);
static void M_DestroyRobots(INT32 choice);
static void M_LevelSelectWarp(INT32 choice);
static void M_PandorasBox(INT32 choice);

// Misc. Main Menu
static void M_Options(INT32 choice);
static void M_SelectableClearMenus(INT32 choice);
static void M_Retry(INT32 choice);
static void M_EndGame(INT32 choice);
static void M_MapChange(INT32 choice);
static void M_ChangeLevel(INT32 choice);
static void M_ConfirmSpectate(INT32 choice);
static void M_ConfirmEnterGame(INT32 choice);
static void M_ConfirmTeamScramble(INT32 choice);
static void M_ConfirmTeamChange(INT32 choice);
static void M_SecretsMenu(INT32 choice);
static void M_SetupChoosePlayer(INT32 choice);
static void M_QuitSRB2(INT32 choice);
menu_t SP_MainDef, MP_MainDef, OP_MainDef;
menu_t MISC_ScrambleTeamDef, MISC_ChangeTeamDef;

// Single Player
static void M_LoadGame(INT32 choice);
static void M_TimeAttack(INT32 choice);
static void M_StaffTime(INT32 choice);
static void M_ReplayTimeAttack(INT32 choice);
static void M_ChooseTimeAttackNoRecord(INT32 choice);
static void M_ChooseTimeAttack(INT32 choice);
static void M_ChoosePlayer(INT32 choice);
menu_t SP_StatsDef, SP_ReplayAttackDef;

// Multiplayer
#ifndef NONET
static void M_StartServerMenu(INT32 choice);
static void M_ConnectMenu(INT32 choice);
static void M_ConnectLANMenu(INT32 choice);
static void M_ConnectIPMenu(INT32 choice);
#endif
static void M_StartSplitServerMenu(INT32 choice);
static void M_StartServer(INT32 choice);
#ifndef NONET
static void M_Refresh(INT32 choice);
static void M_Connect(INT32 choice);
#endif
static void M_SetupMultiPlayer(INT32 choice);
static void M_SetupMultiPlayer2(INT32 choice);

// Options
// Split into multiple parts due to size
// Controls
menu_t OP_ControlsDef, OP_ControlListDef, OP_MoveControlsDef;
menu_t OP_MPControlsDef, OP_CameraControlsDef, OP_MiscControlsDef;
menu_t OP_P1ControlsDef, OP_P2ControlsDef, OP_MouseOptionsDef;
menu_t OP_Mouse2OptionsDef, OP_Joystick1Def, OP_Joystick2Def;
static void M_Setup1PControlsMenu(INT32 choice);
static void M_Setup2PControlsMenu(INT32 choice);
static void M_Setup1PJoystickMenu(INT32 choice);
static void M_Setup2PJoystickMenu(INT32 choice);
static void M_AssignJoystick(INT32 choice);
static void M_ChangeControl(INT32 choice);

// Video & Sound
menu_t OP_VideoOptionsDef, OP_VideoModeDef;
#ifdef HWRENDER
menu_t OP_OpenGLOptionsDef, OP_OpenGLFogDef, OP_OpenGLColorDef;
#endif
menu_t OP_SoundOptionsDef;
static void M_ToggleSFX(void);
static void M_ToggleDigital(void);
static void M_ToggleMIDI(void);

//Misc
menu_t OP_DataOptionsDef, OP_GameOptionsDef, OP_ServerOptionsDef;
menu_t OP_NetgameOptionsDef, OP_GametypeOptionsDef;
menu_t OP_MonitorToggleDef;
static void M_EraseData(INT32 choice);

// Drawing functions
static void M_DrawGenericMenu(void);
static void M_DrawCenteredMenu(void);
static void M_DrawPauseMenu(void);
static void M_DrawServerMenu(void);
static void M_DrawLevelSelectMenu(void);
static void M_DrawImageDef(void);
static void M_DrawLoad(void);
static void M_DrawStats(void);
static void M_DrawTimeAttackMenu(void);
static void M_DrawSetupChoosePlayerMenu(void);
static void M_DrawControl(void);
static void M_DrawVideoMode(void);
#ifdef HWRENDER
static void M_OGL_DrawFogMenu(void);
static void M_OGL_DrawColorMenu(void);
#endif
#ifndef NONET
static void M_DrawConnectMenu(void);
static void M_DrawConnectLANMenu(void);
static void M_DrawConnectIPMenu(void);
static void M_DrawRoomInfoMenu(void);
#endif
static void M_DrawJoystick(void);
static void M_DrawSetupMultiPlayerMenu(void);

// Handling functions
#ifndef NONET
static boolean M_CancelConnect(void);
#endif
static boolean M_ExitPandorasBox(void);
static boolean M_QuitMultiPlayerMenu(void);
static void M_HandleLevelPack(INT32 choice);
static void M_HandleImageDef(INT32 choice);
static void M_HandleLoadSave(INT32 choice);
static void M_BuzzBuzz(INT32 choice);
static void M_HandleStats(INT32 choice);
#ifndef NONET
static void M_HandleConnectIP(INT32 choice);
#endif
static void M_HandleSetupMultiPlayer(INT32 choice);
#ifdef HWRENDER
static void M_HandleFogColor(INT32 choice);
#endif
static void M_HandleVideoMode(INT32 choice);

// Consvar onchange functions
static void Nextmap_OnChange(void);
static void Newgametype_OnChange(void);
#ifndef NONET
static void Chooseroom_OnChange(void);
#endif

// ==========================================================================
// CONSOLE VARIABLES AND THEIR POSSIBLE VALUES GO HERE.
// ==========================================================================

static CV_PossibleValue_t map_cons_t[LEVELARRAYSIZE] = {
	// Ugh, spread it out!!
	{1,"MAP01"},	{2,"MAP02"},	{3,"MAP03"},	{4,"MAP04"},
	{5,"MAP05"},	{6,"MAP06"},	{7,"MAP07"},	{8,"MAP08"},
	{9,"MAP09"},	{10,"MAP10"},	{11,"MAP11"},	{12,"MAP12"},
	{13,"MAP13"},	{14,"MAP14"},	{15,"MAP15"},	{16,"MAP16"},
	{17,"MAP17"},	{18,"MAP18"},	{19,"MAP19"},	{20,"MAP20"},
	{21,"MAP21"},	{22,"MAP22"},	{23,"MAP23"},	{24,"MAP24"},
	{25,"MAP25"},	{26,"MAP26"},	{27,"MAP27"},	{28,"MAP28"},
	{29,"MAP29"},	{30,"MAP30"},	{31,"MAP31"},	{32,"MAP32"},
	{33,"MAP33"},	{34,"MAP34"},	{35,"MAP35"},	{36,"MAP36"},
	{37,"MAP37"},	{38,"MAP38"},	{39,"MAP39"},	{40,"MAP40"},
	{41,"MAP41"},	{42,"MAP42"},	{43,"MAP43"},	{44,"MAP44"},
	{45,"MAP45"},	{46,"MAP46"},	{47,"MAP47"},	{48,"MAP48"},
	{49,"MAP49"},	{50,"MAP50"},	{51,"MAP51"},	{52,"MAP52"},
	{53,"MAP53"},	{54,"MAP54"},	{55,"MAP55"},	{56,"MAP56"},
	{57,"MAP57"},	{58,"MAP58"},	{59,"MAP59"},	{60,"MAP60"},
	{61,"MAP61"},	{62,"MAP62"},	{63,"MAP63"},	{64,"MAP64"},
	{65,"MAP65"},	{66,"MAP66"},	{67,"MAP67"},	{68,"MAP68"},
	{69,"MAP69"},	{70,"MAP70"},	{71,"MAP71"},	{72,"MAP72"},
	{73,"MAP73"},	{74,"MAP74"},	{75,"MAP75"},	{76,"MAP76"},
	{77,"MAP77"},	{78,"MAP78"},	{79,"MAP79"},	{80,"MAP80"},
	{81,"MAP81"},	{82,"MAP82"},	{83,"MAP83"},	{84,"MAP84"},
	{85,"MAP85"},	{86,"MAP86"},	{87,"MAP87"},	{88,"MAP88"},
	{89,"MAP89"},	{90,"MAP90"},	{91,"MAP91"},	{92,"MAP92"},
	{93,"MAP93"},	{94,"MAP94"},	{95,"MAP95"},	{96,"MAP96"},
	{97,"MAP97"},	{98,"MAP98"},	{99,"MAP99"},	{100, "MAP100"},
	{101, "MAP101"},	{102, "MAP102"},	{103, "MAP103"},	{104, "MAP104"},
	{105, "MAP105"},	{106, "MAP106"},	{107, "MAP107"},	{108, "MAP108"},
	{109, "MAP109"},	{110, "MAP110"},	{111, "MAP111"},	{112, "MAP112"},
	{113, "MAP113"},	{114, "MAP114"},	{115, "MAP115"},	{116, "MAP116"},
	{117, "MAP117"},	{118, "MAP118"},	{119, "MAP119"},	{120, "MAP120"},
	{121, "MAP121"},	{122, "MAP122"},	{123, "MAP123"},	{124, "MAP124"},
	{125, "MAP125"},	{126, "MAP126"},	{127, "MAP127"},	{128, "MAP128"},
	{129, "MAP129"},	{130, "MAP130"},	{131, "MAP131"},	{132, "MAP132"},
	{133, "MAP133"},	{134, "MAP134"},	{135, "MAP135"},	{136, "MAP136"},
	{137, "MAP137"},	{138, "MAP138"},	{139, "MAP139"},	{140, "MAP140"},
	{141, "MAP141"},	{142, "MAP142"},	{143, "MAP143"},	{144, "MAP144"},
	{145, "MAP145"},	{146, "MAP146"},	{147, "MAP147"},	{148, "MAP148"},
	{149, "MAP149"},	{150, "MAP150"},	{151, "MAP151"},	{152, "MAP152"},
	{153, "MAP153"},	{154, "MAP154"},	{155, "MAP155"},	{156, "MAP156"},
	{157, "MAP157"},	{158, "MAP158"},	{159, "MAP159"},	{160, "MAP160"},
	{161, "MAP161"},	{162, "MAP162"},	{163, "MAP163"},	{164, "MAP164"},
	{165, "MAP165"},	{166, "MAP166"},	{167, "MAP167"},	{168, "MAP168"},
	{169, "MAP169"},	{170, "MAP170"},	{171, "MAP171"},	{172, "MAP172"},
	{173, "MAP173"},	{174, "MAP174"},	{175, "MAP175"},	{176, "MAP176"},
	{177, "MAP177"},	{178, "MAP178"},	{179, "MAP179"},	{180, "MAP180"},
	{181, "MAP181"},	{182, "MAP182"},	{183, "MAP183"},	{184, "MAP184"},
	{185, "MAP185"},	{186, "MAP186"},	{187, "MAP187"},	{188, "MAP188"},
	{189, "MAP189"},	{190, "MAP190"},	{191, "MAP191"},	{192, "MAP192"},
	{193, "MAP193"},	{194, "MAP194"},	{195, "MAP195"},	{196, "MAP196"},
	{197, "MAP197"},	{198, "MAP198"},	{199, "MAP199"},	{200, "MAP200"},
	{201, "MAP201"},	{202, "MAP202"},	{203, "MAP203"},	{204, "MAP204"},
	{205, "MAP205"},	{206, "MAP206"},	{207, "MAP207"},	{208, "MAP208"},
	{209, "MAP209"},	{210, "MAP210"},	{211, "MAP211"},	{212, "MAP212"},
	{213, "MAP213"},	{214, "MAP214"},	{215, "MAP215"},	{216, "MAP216"},
	{217, "MAP217"},	{218, "MAP218"},	{219, "MAP219"},	{220, "MAP220"},
	{221, "MAP221"},	{222, "MAP222"},	{223, "MAP223"},	{224, "MAP224"},
	{225, "MAP225"},	{226, "MAP226"},	{227, "MAP227"},	{228, "MAP228"},
	{229, "MAP229"},	{230, "MAP230"},	{231, "MAP231"},	{232, "MAP232"},
	{233, "MAP233"},	{234, "MAP234"},	{235, "MAP235"},	{236, "MAP236"},
	{237, "MAP237"},	{238, "MAP238"},	{239, "MAP239"},	{240, "MAP240"},
	{241, "MAP241"},	{242, "MAP242"},	{243, "MAP243"},	{244, "MAP244"},
	{245, "MAP245"},	{246, "MAP246"},	{247, "MAP247"},	{248, "MAP248"},
	{249, "MAP249"},	{250, "MAP250"},	{251, "MAP251"},	{252, "MAP252"},
	{253, "MAP253"},	{254, "MAP254"},	{255, "MAP255"},	{256, "MAP256"},
	{257, "MAP257"},	{258, "MAP258"},	{259, "MAP259"},	{260, "MAP260"},
	{261, "MAP261"},	{262, "MAP262"},	{263, "MAP263"},	{264, "MAP264"},
	{265, "MAP265"},	{266, "MAP266"},	{267, "MAP267"},	{268, "MAP268"},
	{269, "MAP269"},	{270, "MAP270"},	{271, "MAP271"},	{272, "MAP272"},
	{273, "MAP273"},	{274, "MAP274"},	{275, "MAP275"},	{276, "MAP276"},
	{277, "MAP277"},	{278, "MAP278"},	{279, "MAP279"},	{280, "MAP280"},
	{281, "MAP281"},	{282, "MAP282"},	{283, "MAP283"},	{284, "MAP284"},
	{285, "MAP285"},	{286, "MAP286"},	{287, "MAP287"},	{288, "MAP288"},
	{289, "MAP289"},	{290, "MAP290"},	{291, "MAP291"},	{292, "MAP292"},
	{293, "MAP293"},	{294, "MAP294"},	{295, "MAP295"},	{296, "MAP296"},
	{297, "MAP297"},	{298, "MAP298"},	{299, "MAP299"},	{300, "MAP300"},
	{301, "MAP301"},	{302, "MAP302"},	{303, "MAP303"},	{304, "MAP304"},
	{305, "MAP305"},	{306, "MAP306"},	{307, "MAP307"},	{308, "MAP308"},
	{309, "MAP309"},	{310, "MAP310"},	{311, "MAP311"},	{312, "MAP312"},
	{313, "MAP313"},	{314, "MAP314"},	{315, "MAP315"},	{316, "MAP316"},
	{317, "MAP317"},	{318, "MAP318"},	{319, "MAP319"},	{320, "MAP320"},
	{321, "MAP321"},	{322, "MAP322"},	{323, "MAP323"},	{324, "MAP324"},
	{325, "MAP325"},	{326, "MAP326"},	{327, "MAP327"},	{328, "MAP328"},
	{329, "MAP329"},	{330, "MAP330"},	{331, "MAP331"},	{332, "MAP332"},
	{333, "MAP333"},	{334, "MAP334"},	{335, "MAP335"},	{336, "MAP336"},
	{337, "MAP337"},	{338, "MAP338"},	{339, "MAP339"},	{340, "MAP340"},
	{341, "MAP341"},	{342, "MAP342"},	{343, "MAP343"},	{344, "MAP344"},
	{345, "MAP345"},	{346, "MAP346"},	{347, "MAP347"},	{348, "MAP348"},
	{349, "MAP349"},	{350, "MAP350"},	{351, "MAP351"},	{352, "MAP352"},
	{353, "MAP353"},	{354, "MAP354"},	{355, "MAP355"},	{356, "MAP356"},
	{357, "MAP357"},	{358, "MAP358"},	{359, "MAP359"},	{360, "MAP360"},
	{361, "MAP361"},	{362, "MAP362"},	{363, "MAP363"},	{364, "MAP364"},
	{365, "MAP365"},	{366, "MAP366"},	{367, "MAP367"},	{368, "MAP368"},
	{369, "MAP369"},	{370, "MAP370"},	{371, "MAP371"},	{372, "MAP372"},
	{373, "MAP373"},	{374, "MAP374"},	{375, "MAP375"},	{376, "MAP376"},
	{377, "MAP377"},	{378, "MAP378"},	{379, "MAP379"},	{380, "MAP380"},
	{381, "MAP381"},	{382, "MAP382"},	{383, "MAP383"},	{384, "MAP384"},
	{385, "MAP385"},	{386, "MAP386"},	{387, "MAP387"},	{388, "MAP388"},
	{389, "MAP389"},	{390, "MAP390"},	{391, "MAP391"},	{392, "MAP392"},
	{393, "MAP393"},	{394, "MAP394"},	{395, "MAP395"},	{396, "MAP396"},
	{397, "MAP397"},	{398, "MAP398"},	{399, "MAP399"},	{400, "MAP400"},
	{401, "MAP401"},	{402, "MAP402"},	{403, "MAP403"},	{404, "MAP404"},
	{405, "MAP405"},	{406, "MAP406"},	{407, "MAP407"},	{408, "MAP408"},
	{409, "MAP409"},	{410, "MAP410"},	{411, "MAP411"},	{412, "MAP412"},
	{413, "MAP413"},	{414, "MAP414"},	{415, "MAP415"},	{416, "MAP416"},
	{417, "MAP417"},	{418, "MAP418"},	{419, "MAP419"},	{420, "MAP420"},
	{421, "MAP421"},	{422, "MAP422"},	{423, "MAP423"},	{424, "MAP424"},
	{425, "MAP425"},	{426, "MAP426"},	{427, "MAP427"},	{428, "MAP428"},
	{429, "MAP429"},	{430, "MAP430"},	{431, "MAP431"},	{432, "MAP432"},
	{433, "MAP433"},	{434, "MAP434"},	{435, "MAP435"},	{436, "MAP436"},
	{437, "MAP437"},	{438, "MAP438"},	{439, "MAP439"},	{440, "MAP440"},
	{441, "MAP441"},	{442, "MAP442"},	{443, "MAP443"},	{444, "MAP444"},
	{445, "MAP445"},	{446, "MAP446"},	{447, "MAP447"},	{448, "MAP448"},
	{449, "MAP449"},	{450, "MAP450"},	{451, "MAP451"},	{452, "MAP452"},
	{453, "MAP453"},	{454, "MAP454"},	{455, "MAP455"},	{456, "MAP456"},
	{457, "MAP457"},	{458, "MAP458"},	{459, "MAP459"},	{460, "MAP460"},
	{461, "MAP461"},	{462, "MAP462"},	{463, "MAP463"},	{464, "MAP464"},
	{465, "MAP465"},	{466, "MAP466"},	{467, "MAP467"},	{468, "MAP468"},
	{469, "MAP469"},	{470, "MAP470"},	{471, "MAP471"},	{472, "MAP472"},
	{473, "MAP473"},	{474, "MAP474"},	{475, "MAP475"},	{476, "MAP476"},
	{477, "MAP477"},	{478, "MAP478"},	{479, "MAP479"},	{480, "MAP480"},
	{481, "MAP481"},	{482, "MAP482"},	{483, "MAP483"},	{484, "MAP484"},
	{485, "MAP485"},	{486, "MAP486"},	{487, "MAP487"},	{488, "MAP488"},
	{489, "MAP489"},	{490, "MAP490"},	{491, "MAP491"},	{492, "MAP492"},
	{493, "MAP493"},	{494, "MAP494"},	{495, "MAP495"},	{496, "MAP496"},
	{497, "MAP497"},	{498, "MAP498"},	{499, "MAP499"},	{500, "MAP500"},
	{501, "MAP501"},	{502, "MAP502"},	{503, "MAP503"},	{504, "MAP504"},
	{505, "MAP505"},	{506, "MAP506"},	{507, "MAP507"},	{508, "MAP508"},
	{509, "MAP509"},	{510, "MAP510"},	{511, "MAP511"},	{512, "MAP512"},
	{513, "MAP513"},	{514, "MAP514"},	{515, "MAP515"},	{516, "MAP516"},
	{517, "MAP517"},	{518, "MAP518"},	{519, "MAP519"},	{520, "MAP520"},
	{521, "MAP521"},	{522, "MAP522"},	{523, "MAP523"},	{524, "MAP524"},
	{525, "MAP525"},	{526, "MAP526"},	{527, "MAP527"},	{528, "MAP528"},
	{529, "MAP529"},	{530, "MAP530"},	{531, "MAP531"},	{532, "MAP532"},
	{533, "MAP533"},	{534, "MAP534"},	{535, "MAP535"},	{536, "MAP536"},
	{537, "MAP537"},	{538, "MAP538"},	{539, "MAP539"},	{540, "MAP540"},
	{541, "MAP541"},	{542, "MAP542"},	{543, "MAP543"},	{544, "MAP544"},
	{545, "MAP545"},	{546, "MAP546"},	{547, "MAP547"},	{548, "MAP548"},
	{549, "MAP549"},	{550, "MAP550"},	{551, "MAP551"},	{552, "MAP552"},
	{553, "MAP553"},	{554, "MAP554"},	{555, "MAP555"},	{556, "MAP556"},
	{557, "MAP557"},	{558, "MAP558"},	{559, "MAP559"},	{560, "MAP560"},
	{561, "MAP561"},	{562, "MAP562"},	{563, "MAP563"},	{564, "MAP564"},
	{565, "MAP565"},	{566, "MAP566"},	{567, "MAP567"},	{568, "MAP568"},
	{569, "MAP569"},	{570, "MAP570"},	{571, "MAP571"},	{572, "MAP572"},
	{573, "MAP573"},	{574, "MAP574"},	{575, "MAP575"},	{576, "MAP576"},
	{577, "MAP577"},	{578, "MAP578"},	{579, "MAP579"},	{580, "MAP580"},
	{581, "MAP581"},	{582, "MAP582"},	{583, "MAP583"},	{584, "MAP584"},
	{585, "MAP585"},	{586, "MAP586"},	{587, "MAP587"},	{588, "MAP588"},
	{589, "MAP589"},	{590, "MAP590"},	{591, "MAP591"},	{592, "MAP592"},
	{593, "MAP593"},	{594, "MAP594"},	{595, "MAP595"},	{596, "MAP596"},
	{597, "MAP597"},	{598, "MAP598"},	{599, "MAP599"},	{600, "MAP600"},
	{601, "MAP601"},	{602, "MAP602"},	{603, "MAP603"},	{604, "MAP604"},
	{605, "MAP605"},	{606, "MAP606"},	{607, "MAP607"},	{608, "MAP608"},
	{609, "MAP609"},	{610, "MAP610"},	{611, "MAP611"},	{612, "MAP612"},
	{613, "MAP613"},	{614, "MAP614"},	{615, "MAP615"},	{616, "MAP616"},
	{617, "MAP617"},	{618, "MAP618"},	{619, "MAP619"},	{620, "MAP620"},
	{621, "MAP621"},	{622, "MAP622"},	{623, "MAP623"},	{624, "MAP624"},
	{625, "MAP625"},	{626, "MAP626"},	{627, "MAP627"},	{628, "MAP628"},
	{629, "MAP629"},	{630, "MAP630"},	{631, "MAP631"},	{632, "MAP632"},
	{633, "MAP633"},	{634, "MAP634"},	{635, "MAP635"},	{636, "MAP636"},
	{637, "MAP637"},	{638, "MAP638"},	{639, "MAP639"},	{640, "MAP640"},
	{641, "MAP641"},	{642, "MAP642"},	{643, "MAP643"},	{644, "MAP644"},
	{645, "MAP645"},	{646, "MAP646"},	{647, "MAP647"},	{648, "MAP648"},
	{649, "MAP649"},	{650, "MAP650"},	{651, "MAP651"},	{652, "MAP652"},
	{653, "MAP653"},	{654, "MAP654"},	{655, "MAP655"},	{656, "MAP656"},
	{657, "MAP657"},	{658, "MAP658"},	{659, "MAP659"},	{660, "MAP660"},
	{661, "MAP661"},	{662, "MAP662"},	{663, "MAP663"},	{664, "MAP664"},
	{665, "MAP665"},	{666, "MAP666"},	{667, "MAP667"},	{668, "MAP668"},
	{669, "MAP669"},	{670, "MAP670"},	{671, "MAP671"},	{672, "MAP672"},
	{673, "MAP673"},	{674, "MAP674"},	{675, "MAP675"},	{676, "MAP676"},
	{677, "MAP677"},	{678, "MAP678"},	{679, "MAP679"},	{680, "MAP680"},
	{681, "MAP681"},	{682, "MAP682"},	{683, "MAP683"},	{684, "MAP684"},
	{685, "MAP685"},	{686, "MAP686"},	{687, "MAP687"},	{688, "MAP688"},
	{689, "MAP689"},	{690, "MAP690"},	{691, "MAP691"},	{692, "MAP692"},
	{693, "MAP693"},	{694, "MAP694"},	{695, "MAP695"},	{696, "MAP696"},
	{697, "MAP697"},	{698, "MAP698"},	{699, "MAP699"},	{700, "MAP700"},
	{701, "MAP701"},	{702, "MAP702"},	{703, "MAP703"},	{704, "MAP704"},
	{705, "MAP705"},	{706, "MAP706"},	{707, "MAP707"},	{708, "MAP708"},
	{709, "MAP709"},	{710, "MAP710"},	{711, "MAP711"},	{712, "MAP712"},
	{713, "MAP713"},	{714, "MAP714"},	{715, "MAP715"},	{716, "MAP716"},
	{717, "MAP717"},	{718, "MAP718"},	{719, "MAP719"},	{720, "MAP720"},
	{721, "MAP721"},	{722, "MAP722"},	{723, "MAP723"},	{724, "MAP724"},
	{725, "MAP725"},	{726, "MAP726"},	{727, "MAP727"},	{728, "MAP728"},
	{729, "MAP729"},	{730, "MAP730"},	{731, "MAP731"},	{732, "MAP732"},
	{733, "MAP733"},	{734, "MAP734"},	{735, "MAP735"},	{736, "MAP736"},
	{737, "MAP737"},	{738, "MAP738"},	{739, "MAP739"},	{740, "MAP740"},
	{741, "MAP741"},	{742, "MAP742"},	{743, "MAP743"},	{744, "MAP744"},
	{745, "MAP745"},	{746, "MAP746"},	{747, "MAP747"},	{748, "MAP748"},
	{749, "MAP749"},	{750, "MAP750"},	{751, "MAP751"},	{752, "MAP752"},
	{753, "MAP753"},	{754, "MAP754"},	{755, "MAP755"},	{756, "MAP756"},
	{757, "MAP757"},	{758, "MAP758"},	{759, "MAP759"},	{760, "MAP760"},
	{761, "MAP761"},	{762, "MAP762"},	{763, "MAP763"},	{764, "MAP764"},
	{765, "MAP765"},	{766, "MAP766"},	{767, "MAP767"},	{768, "MAP768"},
	{769, "MAP769"},	{770, "MAP770"},	{771, "MAP771"},	{772, "MAP772"},
	{773, "MAP773"},	{774, "MAP774"},	{775, "MAP775"},	{776, "MAP776"},
	{777, "MAP777"},	{778, "MAP778"},	{779, "MAP779"},	{780, "MAP780"},
	{781, "MAP781"},	{782, "MAP782"},	{783, "MAP783"},	{784, "MAP784"},
	{785, "MAP785"},	{786, "MAP786"},	{787, "MAP787"},	{788, "MAP788"},
	{789, "MAP789"},	{790, "MAP790"},	{791, "MAP791"},	{792, "MAP792"},
	{793, "MAP793"},	{794, "MAP794"},	{795, "MAP795"},	{796, "MAP796"},
	{797, "MAP797"},	{798, "MAP798"},	{799, "MAP799"},	{800, "MAP800"},
	{801, "MAP801"},	{802, "MAP802"},	{803, "MAP803"},	{804, "MAP804"},
	{805, "MAP805"},	{806, "MAP806"},	{807, "MAP807"},	{808, "MAP808"},
	{809, "MAP809"},	{810, "MAP810"},	{811, "MAP811"},	{812, "MAP812"},
	{813, "MAP813"},	{814, "MAP814"},	{815, "MAP815"},	{816, "MAP816"},
	{817, "MAP817"},	{818, "MAP818"},	{819, "MAP819"},	{820, "MAP820"},
	{821, "MAP821"},	{822, "MAP822"},	{823, "MAP823"},	{824, "MAP824"},
	{825, "MAP825"},	{826, "MAP826"},	{827, "MAP827"},	{828, "MAP828"},
	{829, "MAP829"},	{830, "MAP830"},	{831, "MAP831"},	{832, "MAP832"},
	{833, "MAP833"},	{834, "MAP834"},	{835, "MAP835"},	{836, "MAP836"},
	{837, "MAP837"},	{838, "MAP838"},	{839, "MAP839"},	{840, "MAP840"},
	{841, "MAP841"},	{842, "MAP842"},	{843, "MAP843"},	{844, "MAP844"},
	{845, "MAP845"},	{846, "MAP846"},	{847, "MAP847"},	{848, "MAP848"},
	{849, "MAP849"},	{850, "MAP850"},	{851, "MAP851"},	{852, "MAP852"},
	{853, "MAP853"},	{854, "MAP854"},	{855, "MAP855"},	{856, "MAP856"},
	{857, "MAP857"},	{858, "MAP858"},	{859, "MAP859"},	{860, "MAP860"},
	{861, "MAP861"},	{862, "MAP862"},	{863, "MAP863"},	{864, "MAP864"},
	{865, "MAP865"},	{866, "MAP866"},	{867, "MAP867"},	{868, "MAP868"},
	{869, "MAP869"},	{870, "MAP870"},	{871, "MAP871"},	{872, "MAP872"},
	{873, "MAP873"},	{874, "MAP874"},	{875, "MAP875"},	{876, "MAP876"},
	{877, "MAP877"},	{878, "MAP878"},	{879, "MAP879"},	{880, "MAP880"},
	{881, "MAP881"},	{882, "MAP882"},	{883, "MAP883"},	{884, "MAP884"},
	{885, "MAP885"},	{886, "MAP886"},	{887, "MAP887"},	{888, "MAP888"},
	{889, "MAP889"},	{890, "MAP890"},	{891, "MAP891"},	{892, "MAP892"},
	{893, "MAP893"},	{894, "MAP894"},	{895, "MAP895"},	{896, "MAP896"},
	{897, "MAP897"},	{898, "MAP898"},	{899, "MAP899"},	{900, "MAP900"},
	{901, "MAP901"},	{902, "MAP902"},	{903, "MAP903"},	{904, "MAP904"},
	{905, "MAP905"},	{906, "MAP906"},	{907, "MAP907"},	{908, "MAP908"},
	{909, "MAP909"},	{910, "MAP910"},	{911, "MAP911"},	{912, "MAP912"},
	{913, "MAP913"},	{914, "MAP914"},	{915, "MAP915"},	{916, "MAP916"},
	{917, "MAP917"},	{918, "MAP918"},	{919, "MAP919"},	{920, "MAP920"},
	{921, "MAP921"},	{922, "MAP922"},	{923, "MAP923"},	{924, "MAP924"},
	{925, "MAP925"},	{926, "MAP926"},	{927, "MAP927"},	{928, "MAP928"},
	{929, "MAP929"},	{930, "MAP930"},	{931, "MAP931"},	{932, "MAP932"},
	{933, "MAP933"},	{934, "MAP934"},	{935, "MAP935"},	{936, "MAP936"},
	{937, "MAP937"},	{938, "MAP938"},	{939, "MAP939"},	{940, "MAP940"},
	{941, "MAP941"},	{942, "MAP942"},	{943, "MAP943"},	{944, "MAP944"},
	{945, "MAP945"},	{946, "MAP946"},	{947, "MAP947"},	{948, "MAP948"},
	{949, "MAP949"},	{950, "MAP950"},	{951, "MAP951"},	{952, "MAP952"},
	{953, "MAP953"},	{954, "MAP954"},	{955, "MAP955"},	{956, "MAP956"},
	{957, "MAP957"},	{958, "MAP958"},	{959, "MAP959"},	{960, "MAP960"},
	{961, "MAP961"},	{962, "MAP962"},	{963, "MAP963"},	{964, "MAP964"},
	{965, "MAP965"},	{966, "MAP966"},	{967, "MAP967"},	{968, "MAP968"},
	{969, "MAP969"},	{970, "MAP970"},	{971, "MAP971"},	{972, "MAP972"},
	{973, "MAP973"},	{974, "MAP974"},	{975, "MAP975"},	{976, "MAP976"},
	{977, "MAP977"},	{978, "MAP978"},	{979, "MAP979"},	{980, "MAP980"},
	{981, "MAP981"},	{982, "MAP982"},	{983, "MAP983"},	{984, "MAP984"},
	{985, "MAP985"},	{986, "MAP986"},	{987, "MAP987"},	{988, "MAP988"},
	{989, "MAP989"},	{990, "MAP990"},	{991, "MAP991"},	{992, "MAP992"},
	{993, "MAP993"},	{994, "MAP994"},	{995, "MAP995"},	{996, "MAP996"},
	{997, "MAP997"},	{998, "MAP998"},	{999, "MAP999"},	{1000, "MAP1000"},
	{1001, "MAP1001"},	{1002, "MAP1002"},	{1003, "MAP1003"},	{1004, "MAP1004"},
	{1005, "MAP1005"},	{1006, "MAP1006"},	{1007, "MAP1007"},	{1008, "MAP1008"},
	{1009, "MAP1009"},	{1010, "MAP1010"},	{1011, "MAP1011"},	{1012, "MAP1012"},
	{1013, "MAP1013"},	{1014, "MAP1014"},	{1015, "MAP1015"},	{1016, "MAP1016"},
	{1017, "MAP1017"},	{1018, "MAP1018"},	{1019, "MAP1019"},	{1020, "MAP1020"},
	{1021, "MAP1021"},	{1022, "MAP1022"},	{1023, "MAP1023"},	{1024, "MAP1024"},
	{1025, "MAP1025"},	{1026, "MAP1026"},	{1027, "MAP1027"},	{1028, "MAP1028"},
	{1029, "MAP1029"},	{1030, "MAP1030"},	{1031, "MAP1031"},	{1032, "MAP1032"},
	{1033, "MAP1033"},	{1034, "MAP1034"},	{1035, "MAP1035"},
	{INT16_MAX, NULL}
};
consvar_t cv_nextmap = {"nextmap", "MAP01", CV_HIDEN|CV_CALL, map_cons_t, Nextmap_OnChange, 0, NULL, NULL, 0, 0, NULL};

static CV_PossibleValue_t skins_cons_t[MAXSKINS+1] = {{1, DEFAULTSKIN}};
consvar_t cv_chooseskin = {"chooseskin", DEFAULTSKIN, CV_HIDEN|CV_CALL, skins_cons_t, Nextmap_OnChange, 0, NULL, NULL, 0, 0, NULL};

// This gametype list is integral for many different reasons.
// When you add gametypes here, don't forget to update them in CV_AddValue!
CV_PossibleValue_t gametype_cons_t[] =
{
	{GT_COOP, "Co-op"}, {GT_MATCH, "Match"},
	{GTF_TEAMMATCH, "Team Match"}, {GT_RACE, "Race"},
	{GTF_CLASSICRACE, "Classic Race"}, {GT_TAG, "Tag"},
	{GTF_HIDEANDSEEK, "Hide and Seek"},
	{GT_CTF, "CTF"},
#ifdef CHAOSISNOTDEADYET
	{GT_CHAOS, "Chaos"},
#endif
	{GT_SHARDS, "Shards"},
	{0, NULL}
};
consvar_t cv_newgametype = {"newgametype", "Co-op", CV_HIDEN|CV_CALL, gametype_cons_t, Newgametype_OnChange, 0, NULL, NULL, 0, 0, NULL};

static CV_PossibleValue_t serversort_cons_t[] = {
	{0,"Ping"},
	{1,"Players"},
	{2,"Gametype"},
	{0,NULL}
};
consvar_t cv_serversort = {"serversort", "Ping", CV_HIDEN | CV_CALL, serversort_cons_t, M_SortServerList, 0, NULL, NULL, 0, 0, NULL};

#ifndef NONET
static CV_PossibleValue_t serversearch_cons_t[] = {
	{0,"Local Lan"},
	{1,"Internet"},
	{0,NULL}};
static consvar_t cv_serversearch = {"serversearch", "Internet", CV_HIDEN, serversearch_cons_t, NULL, 0, NULL, NULL, 0, 0, NULL};
//static void M_Chooseroom_Onchange(void);

static CV_PossibleValue_t rooms_cons_t[MAXROOMS+1] = {{1, "Offline"}};
consvar_t cv_chooseroom = {"chooseroom", "Offline", CV_HIDEN | CV_CALL, rooms_cons_t, Chooseroom_OnChange, 0, NULL, NULL, 0, 0, NULL};
#endif

// ==========================================================================
// ORGANIZATION START.
// ==========================================================================
// Note: Never should we be jumping from one category of menu options to another
//       without first going to the Main Menu.
// Note: Ignore the above if you're working with the Pause menu.
// Note: (Prefix)_MainMenu should be the target of all Main Menu options that
//       point to submenus.

// ---------
// Main Menu
// ---------
static menuitem_t MainMenu[] =
{
	{IT_CALL    | IT_STRING, NULL, "Sky Room"         , M_SecretsMenu,         84},
	{IT_SUBMENU | IT_STRING, NULL, "1 player"         , &SP_MainDef,           92},
	{IT_SUBMENU | IT_STRING, NULL, "multiplayer"      , &MP_MainDef,          100},
	{IT_CALL    | IT_STRING, NULL, "options"          , M_Options,            108},
	{IT_CALL    | IT_STRING, NULL, "quit  game"       , M_QuitSRB2,           116},
};

typedef enum
{
	secrets = 0,
	singleplr,
	multiplr,
	options,
	quitdoom
} main_e;

// ---------------------
// Pause Menu MP Edition
// ---------------------
static menuitem_t MPauseMenu[] =
{
	{IT_STRING  | IT_SUBMENU, NULL, "Scramble Teams...", &MISC_ScrambleTeamDef, 16},
	{IT_STRING  | IT_CALL,    NULL, "Switch Map..."    , M_MapChange,           24},

	{IT_CALL | IT_STRING,    NULL, "Continue",             M_SelectableClearMenus,40},
	{IT_CALL | IT_STRING,    NULL, "Player 1 Setup",       M_SetupMultiPlayer,    48}, // splitscreen
	{IT_CALL | IT_STRING,    NULL, "Player 2 Setup",       M_SetupMultiPlayer2,   56}, // splitscreen

	{IT_STRING | IT_CALL,    NULL, "Spectate",             M_ConfirmSpectate,     48},
	{IT_STRING | IT_CALL,    NULL, "Enter Game",           M_ConfirmEnterGame,    48},
	{IT_STRING | IT_SUBMENU, NULL, "Switch Team...",       &MISC_ChangeTeamDef,   48},
	{IT_CALL | IT_STRING,    NULL, "Player Setup",         M_SetupMultiPlayer,    56}, // alone
	{IT_CALL | IT_STRING,    NULL, "Options",              M_Options,             64},

	{IT_CALL | IT_STRING,    NULL, "Return to Title",      M_EndGame,            80},
	{IT_CALL | IT_STRING,    NULL, "Quit Game",            M_QuitSRB2,           88},
};

typedef enum
{
	mpause_scramble = 0,
	mpause_switchmap,

	mpause_continue,
	mpause_psetupsplit,
	mpause_psetupsplit2,
	mpause_spectate,
	mpause_entergame,
	mpause_switchteam,
	mpause_psetup,
	mpause_options,

	mpause_title,
	mpause_quit
} mpause_e;

// ---------------------
// Pause Menu SP Edition
// ---------------------
static menuitem_t SPauseMenu[] =
{
	{IT_CALL | IT_STRING,    NULL, "Pandora's Box...",     M_PandorasBox,         32},

	{IT_CALL | IT_STRING,    NULL, "Continue",             M_SelectableClearMenus,48},
	{IT_CALL | IT_STRING,    NULL, "Retry",                M_Retry,               56},
	{IT_CALL | IT_STRING,    NULL, "Options",              M_Options,             64},

	{IT_CALL | IT_STRING,    NULL, "Return to Title",      M_EndGame,             80},
	{IT_CALL | IT_STRING,    NULL, "Quit Game",            M_QuitSRB2,            88},
};

typedef enum
{
	spause_pandora = 0,

	spause_continue,
	spause_retry,
	spause_options,
	spause_title,
	spause_quit
} spause_e;

// -----------------
// Misc menu options
// -----------------
// Prefix: MISC_
static menuitem_t MISC_ScrambleTeamMenu[] =
{
	{IT_STRING|IT_CVAR,      NULL, "Scramble Method", &cv_dummyscramble,     30},
	{IT_WHITESTRING|IT_CALL, NULL, "Confirm",         M_ConfirmTeamScramble, 90},
};

static menuitem_t MISC_ChangeTeamMenu[] =
{
	{IT_STRING|IT_CVAR,              NULL, "Select Team",             &cv_dummyteam,    30},
	{IT_WHITESTRING|IT_CALL,         NULL, "Confirm",           M_ConfirmTeamChange,    90},
};

static menuitem_t MISC_ChangeLevelMenu[] =
{
	{IT_STRING|IT_CVAR,              NULL, "Game Type",             &cv_newgametype,    30},
	{IT_STRING|IT_CVAR,              NULL, "Level",                 &cv_nextmap,        60},
	{IT_WHITESTRING|IT_CALL,         NULL, "Change Level",          M_ChangeLevel,     120},
};

static menuitem_t MISC_HelpMenu[] =
{
	{IT_KEYHANDLER | IT_NOTHING, NULL, "HELP", M_HandleImageDef, 0},
	{IT_KEYHANDLER | IT_NOTHING, NULL, "BULMER", M_HandleImageDef, 0},
};

// --------------------------------
// Sky Room and all of its submenus
// --------------------------------
// Prefix: SR_

// Pause Menu Pandora's Box Options
static menuitem_t SR_PandorasBox[] =
{
	{IT_STRING | IT_CVAR, NULL, "Rings",              &cv_dummyrings,      20},
	{IT_STRING | IT_CVAR, NULL, "Lives",              &cv_dummylives,      30},
	{IT_STRING | IT_CVAR, NULL, "Continues",          &cv_dummycontinues,  40},

	{IT_STRING | IT_CVAR, NULL, "Gravity",            &cv_gravity,         60},
	{IT_STRING | IT_CVAR, NULL, "Throw Rings",        &cv_ringslinger,     70},

	{IT_STRING | IT_CALL, NULL, "Get All Emeralds",   M_GetAllEmeralds,    90},
	{IT_STRING | IT_CALL, NULL, "Destroy All Robots", M_DestroyRobots,    100},

	{IT_STRING | IT_CALL, NULL, "Ultimate Cheat",     M_UltimateCheat,    120},
};

// Default Sky Room Setup
static menuitem_t SR_MainMenu[] =
{
	{IT_STRING | IT_CALL, NULL, "Secrets Checklist",  M_Checklist,       0},

	{IT_STRING | IT_CVAR, NULL, "Sound Test",         &cv_soundtest,    30},

	{IT_STRING | IT_CALL, NULL, "Level Select",       M_LevelSelect,    60},

	{IT_HEADER,           NULL, "Bonus Levels",       NULL,             90},
	{IT_STRING | IT_CALL, NULL, "Sonic Into Dreams",  M_NightsGame,    100},
	{IT_STRING | IT_CALL, NULL, "Mario Koopa Blast",  M_MarioGame,     110},
	{IT_STRING | IT_CALL, NULL, "Neo Aerial Garden",  M_NAGZGame,      120},
};

typedef enum
{
	unlockchecklist = 0,
	soundtest,
	levelselect,
	reward_header,
	nights,
	mario,
	nagz
} secrets_e;

// Sky Room Custom Unlocks
static menuitem_t SR_CustomMenu[] =
{
	{IT_STRING | IT_CALL, NULL, "Secrets Checklist",  M_Checklist,          0},
	{IT_STRING | IT_CALL, NULL, "Custom1",    M_LevelSelect,    10},
	{IT_STRING | IT_CALL, NULL, "Custom2",    M_CustomWarp,     20},
	{IT_STRING | IT_CALL, NULL, "Custom3",    M_CustomWarp,     30},
	{IT_STRING | IT_CALL, NULL, "Custom4",    M_CustomWarp,     40},
	{IT_STRING | IT_CALL, NULL, "Custom5",    M_CustomWarp,     50},
	{IT_STRING | IT_CALL, NULL, "Custom6",    M_CustomWarp,     60},
	{IT_STRING | IT_CALL, NULL, "Custom7",    M_CustomWarp,     70},
	{IT_STRING | IT_CALL, NULL, "Custom8",    M_CustomWarp,     80},
	{IT_STRING | IT_CALL, NULL, "Custom9",    M_CustomWarp,     90},
	{IT_STRING | IT_CALL, NULL, "Custom10",   M_CustomWarp,    100},
	{IT_STRING | IT_CALL, NULL, "Custom11",   M_CustomWarp,    110},
	{IT_STRING | IT_CALL, NULL, "Custom12",   M_CustomWarp,    120},
	{IT_STRING | IT_CALL, NULL, "Custom13",   M_CustomWarp,    130},
	{IT_STRING | IT_CALL, NULL, "Custom14",   M_CustomWarp,    140},
	{IT_STRING | IT_CALL, NULL, "Custom15",   M_CustomWarp,    150},
};

static menuitem_t SR_LevelSelectMenu[] =
{
	{IT_STRING|IT_KEYHANDLER,        NULL, "Level Pack",            M_HandleLevelPack,     50},
	{IT_STRING|IT_CVAR,              NULL, "Level",                 &cv_nextmap,        60},

	{IT_WHITESTRING|IT_CALL,         NULL, "Start",                 M_LevelSelectWarp,     120},
};

static menuitem_t SR_UnlockChecklistMenu[] =
{
	{IT_SUBMENU | IT_STRING, NULL, "NEXT", NULL, 192},
};

// --------------------------------
// 1 Player and all of its submenus
// --------------------------------
// Prefix: SP_

// Single Player Main
static menuitem_t SP_MainMenu[] =
{
	{IT_CALL | IT_STRING,                          NULL, "Start Game",    M_LoadGame,           92},
	{IT_SUBMENU | IT_STRING | IT_CALL_NOTMODIFIED, NULL, "Replay Attack", &SP_ReplayAttackDef, 100},
	{IT_STRING | IT_CALL,                          NULL, "Sonic Golf",    M_SonicGolf,         108},
	{IT_SUBMENU | IT_STRING | IT_CALL_NOTMODIFIED, NULL, "Statistics",    &SP_StatsDef,        116},
};

// Single Player Load Game
static menuitem_t SP_LoadGameMenu[] =
{
	{IT_KEYHANDLER | IT_NOTHING, NULL, "", M_HandleLoadSave, '\0'},     // dummy menuitem for the control func
};

// Single Player Level Select
static menuitem_t SP_LevelSelectMenu[] =
{
	{IT_STRING|IT_CVAR,              NULL, "Level",                 &cv_nextmap,        60},

	{IT_WHITESTRING|IT_CALL,         NULL, "Start",                 M_LevelSelectWarp,     120},
};

// Single Player Replay Attack
static menuitem_t SP_ReplayAttackMenu[] =
{
	{IT_CALL | IT_STRING, NULL,       "Time Attack",  M_TimeAttack, 100},
	{IT_KEYHANDLER | IT_STRING, NULL, "Ring Attack",  M_BuzzBuzz,   108},
	{IT_KEYHANDLER | IT_STRING, NULL, "Score Attack", M_BuzzBuzz,   116},
};

// Single Player Time Attack
static menuitem_t SP_TimeAttackMenu[] =
{
	{IT_STRING|IT_KEYHANDLER,  NULL, "Level Pack", M_HandleLevelPack,    50},
	{IT_STRING|IT_CVAR,        NULL, "Player",     &cv_chooseskin,       58},
	{IT_STRING|IT_CVAR,        NULL, "Level",      &cv_nextmap,          66},

	// repeated three times (sonic, tails, knux)
	{IT_DISABLED,              NULL, "Staff Time", M_StaffTime,          85},
	{IT_DISABLED,              NULL, "Staff Time", M_StaffTime,          85},
	{IT_DISABLED,              NULL, "Staff Time", M_StaffTime,          85},

	{IT_DISABLED,              NULL, "Replay",             M_ReplayTimeAttack,         100},
	{IT_WHITESTRING|IT_CALL,   NULL, "Start (No Record)",  M_ChooseTimeAttackNoRecord, 115},
	{IT_WHITESTRING|IT_CALL,   NULL, "Start (Record)",     M_ChooseTimeAttack,         130},
};

typedef enum
{
	tamod = 0,
	taplayer,
	talevel,

	// Staff
	tastaffsonic,
	tastafftails,
	tastaffknux,

	tareplay,
	tastart,
	tastartrecord
} timeattack_e;

// Statistics
static menuitem_t SP_StatsMenu[] =
{
	{IT_KEYHANDLER | IT_NOTHING, NULL, "", M_HandleStats, '\0'},     // dummy menuitem for the control func
};

// A rare case.
// External files modify this menu, so we can't call it static.
// And I'm too lazy to go through and rename it everywhere. ARRGH!
menuitem_t PlayerMenu[] =
{
	{IT_CALL | IT_STRING, NULL, "SONIC", M_ChoosePlayer,  0},
	{IT_CALL | IT_STRING, NULL, "TAILS", M_ChoosePlayer,  0},
	{IT_CALL | IT_STRING, NULL, "KNUCKLES", M_ChoosePlayer,  0},
	{IT_DISABLED,         NULL, "PLAYER4", M_ChoosePlayer,  0},
	{IT_DISABLED,         NULL, "PLAYER5", M_ChoosePlayer,  0},
	{IT_DISABLED,         NULL, "PLAYER6", M_ChoosePlayer,  0},
	{IT_DISABLED,         NULL, "PLAYER7", M_ChoosePlayer,  0},
	{IT_DISABLED,         NULL, "PLAYER8", M_ChoosePlayer,  0},
	{IT_DISABLED,         NULL, "PLAYER9", M_ChoosePlayer,  0},
	{IT_DISABLED,         NULL, "PLAYER10", M_ChoosePlayer,  0},
	{IT_DISABLED,         NULL, "PLAYER11", M_ChoosePlayer,  0},
	{IT_DISABLED,         NULL, "PLAYER12", M_ChoosePlayer,  0},
	{IT_DISABLED,         NULL, "PLAYER13", M_ChoosePlayer,  0},
	{IT_DISABLED,         NULL, "PLAYER14", M_ChoosePlayer,  0},
	{IT_DISABLED,         NULL, "PLAYER15", M_ChoosePlayer,  0},
};

// -----------------------------------
// Multiplayer and all of its submenus
// -----------------------------------
// Prefix: MP_
static menuitem_t MP_MainMenu[] =
{
#ifndef NONET
	{IT_CALL | IT_STRING, NULL, "HOST GAME",              M_StartServerMenu,      10},
	{IT_CALL | IT_STRING, NULL, "JOIN GAME (Internet)",	  M_ConnectMenu,		  30},
	{IT_CALL | IT_STRING, NULL, "JOIN GAME (LAN)",		  M_ConnectLANMenu,       40},
	{IT_CALL | IT_STRING, NULL, "JOIN GAME (Specify IP)", M_ConnectIPMenu,        50},
#endif
	{IT_CALL | IT_STRING, NULL, "TWO PLAYER GAME",        M_StartSplitServerMenu, 70},

	{IT_CALL | IT_STRING, NULL, "SETUP PLAYER 1",         M_SetupMultiPlayer,     90},
	{IT_CALL | IT_STRING, NULL, "SETUP PLAYER 2",         M_SetupMultiPlayer2,   100},
};

static menuitem_t MP_ServerMenu[] =
{
	{IT_STRING|IT_CVAR,              NULL, "Game Type",             &cv_newgametype,    10},
	{IT_STRING|IT_CVAR,              NULL, "Advertise on Internet", &cv_internetserver, 20},
#ifndef NONET
	{IT_STRING|IT_CVAR,              NULL, "Room",					&cv_chooseroom,		30},
	{IT_STRING|IT_CALL,              NULL, "Room Info",             M_RoomInfoMenu,     40},
#endif
	{IT_STRING|IT_CVAR|IT_CV_STRING, NULL, "Server Name",           &cv_servername,     50},

	{IT_STRING|IT_CVAR,              NULL, "Level",                 &cv_nextmap,        80},

	{IT_WHITESTRING|IT_CALL,         NULL, "Start",                 M_StartServer,     130},
};

#ifndef NONET
static menuitem_t MP_ConnectMenu[] =
{
	{IT_STRING | IT_CVAR,  NULL, "Room",		   &cv_chooseroom,	 4},
	{IT_STRING | IT_CALL,  NULL, "Room Info",	   M_RoomInfoMenu,	 12},
	{IT_STRING | IT_CVAR,  NULL, "Sort By",        &cv_serversort,   20},
	{IT_STRING | IT_CALL,  NULL, "Next Page",      M_NextServerPage, 28},
	{IT_STRING | IT_CALL,  NULL, "Previous Page",  M_PrevServerPage, 36},
	{IT_STRING | IT_CALL,  NULL, "Refresh",        M_Refresh,        44},
	{IT_STRING | IT_SPACE, NULL, "",              M_Connect,          60-4},
	{IT_STRING | IT_SPACE, NULL, "",              M_Connect,          72-4},
	{IT_STRING | IT_SPACE, NULL, "",              M_Connect,          84-4},
	{IT_STRING | IT_SPACE, NULL, "",              M_Connect,          96-4},
	{IT_STRING | IT_SPACE, NULL, "",              M_Connect,         108-4},
	{IT_STRING | IT_SPACE, NULL, "",              M_Connect,         120-4},
	{IT_STRING | IT_SPACE, NULL, "",              M_Connect,         132-4},
	{IT_STRING | IT_SPACE, NULL, "",              M_Connect,         144-4},
	{IT_STRING | IT_SPACE, NULL, "",              M_Connect,         156-4},
	{IT_STRING | IT_SPACE, NULL, "",              M_Connect,         168-4},
};

static menuitem_t MP_ConnectLANMenu[] =
{
	{IT_STRING | IT_CVAR,  NULL, "Sort By",        &cv_serversort,   20},
	{IT_STRING | IT_CALL,  NULL, "Next Page",      M_NextServerPage, 28},
	{IT_STRING | IT_CALL,  NULL, "Previous Page",  M_PrevServerPage, 36},
	{IT_STRING | IT_CALL,  NULL, "Refresh",        M_Refresh,        44},
	{IT_STRING | IT_SPACE, NULL, "",              M_Connect,          60-4},
	{IT_STRING | IT_SPACE, NULL, "",              M_Connect,          72-4},
	{IT_STRING | IT_SPACE, NULL, "",              M_Connect,          84-4},
	{IT_STRING | IT_SPACE, NULL, "",              M_Connect,          96-4},
	{IT_STRING | IT_SPACE, NULL, "",              M_Connect,         108-4},
	{IT_STRING | IT_SPACE, NULL, "",              M_Connect,         120-4},
	{IT_STRING | IT_SPACE, NULL, "",              M_Connect,         132-4},
	{IT_STRING | IT_SPACE, NULL, "",              M_Connect,         144-4},
	{IT_STRING | IT_SPACE, NULL, "",              M_Connect,         156-4},
	{IT_STRING | IT_SPACE, NULL, "",              M_Connect,         168-4},
};

static menuitem_t MP_ConnectIPMenu[] =
{
	{IT_KEYHANDLER | IT_STRING, NULL, "  IP Address:", M_HandleConnectIP, 0},
};

static menuitem_t MP_RoomInfoMenu[] =
{
	{IT_STRING | IT_CVAR,  NULL, "Room", &cv_chooseroom,	 0},
};
#endif

// Separated splitscreen and normal servers.
static menuitem_t MP_SplitServerMenu[] =
{
	{IT_STRING|IT_CVAR,              NULL, "Game Type",             &cv_newgametype,    10},
	{IT_STRING|IT_CVAR,              NULL, "Level",                 &cv_nextmap,        80},
	{IT_WHITESTRING|IT_CALL,         NULL, "Start",                 M_StartServer,     130},
};

static menuitem_t MP_PlayerSetupMenu[] =
{
	{IT_KEYHANDLER | IT_STRING,   NULL, "Your name",   M_HandleSetupMultiPlayer,   0},
	{IT_KEYHANDLER | IT_STRING,   NULL, "Your color",  M_HandleSetupMultiPlayer,  16},
	{IT_KEYHANDLER | IT_STRING,   NULL, "Your player", M_HandleSetupMultiPlayer,  96}, // Tails 01-18-2001
};

// ------------------------------------
// Options and most (?) of its submenus
// ------------------------------------
// Prefix: OP_
static menuitem_t OP_MainMenu[] =
{
	{IT_SUBMENU | IT_STRING, NULL, "Setup Controls...",     &OP_ControlsDef,      10},

	{IT_SUBMENU | IT_STRING, NULL, "Video Options...",      &OP_VideoOptionsDef,  30},
	{IT_SUBMENU | IT_STRING, NULL, "Sound Options...",      &OP_SoundOptionsDef,  40},
	{IT_SUBMENU | IT_STRING, NULL, "Data Options...",       &OP_DataOptionsDef,   50},

	{IT_SUBMENU | IT_STRING, NULL, "Game Options...",       &OP_GameOptionsDef,   70},
	{IT_SUBMENU | IT_STRING, NULL, "Server Options...",     &OP_ServerOptionsDef, 80},
};

static menuitem_t OP_ControlsMenu[] =
{
	{IT_SUBMENU | IT_STRING, NULL, "Player 1 Controls...", &OP_P1ControlsDef,  20},
	{IT_SUBMENU | IT_STRING, NULL, "Player 2 Controls...", &OP_P2ControlsDef,  30},

	{IT_STRING  | IT_CVAR, NULL, "Controls per key", &cv_controlperkey, 60},
};

static menuitem_t OP_P1ControlsMenu[] =
{
	{IT_CALL    | IT_STRING, NULL, "Control Configuration...", M_Setup1PControlsMenu,   10},
	{IT_SUBMENU | IT_STRING, NULL, "Mouse Options...", &OP_MouseOptionsDef, 20},
	{IT_SUBMENU | IT_STRING, NULL, "Joystick Options...", &OP_Joystick1Def  ,  30},

	{IT_STRING  | IT_CVAR, NULL, "Camera"  , &cv_chasecam  ,  50},

	{IT_STRING  | IT_CVAR, NULL, "Analog Control", &cv_useranalog,  70},
	{IT_STRING  | IT_CVAR, NULL, "Autoaim" , &cv_autoaim   ,  80},
	{IT_STRING  | IT_CVAR, NULL, "Crosshair", &cv_crosshair , 90},
};

static menuitem_t OP_P2ControlsMenu[] =
{
	{IT_CALL    | IT_STRING, NULL, "Control Configuration...", M_Setup2PControlsMenu,   10},
	{IT_SUBMENU | IT_STRING, NULL, "Second Mouse Options...", &OP_Mouse2OptionsDef, 20},
	{IT_SUBMENU | IT_STRING, NULL, "Second Joystick Options...", &OP_Joystick2Def  ,  30},

	{IT_STRING  | IT_CVAR, NULL, "Camera"  , &cv_chasecam2 , 50},

	{IT_STRING  | IT_CVAR, NULL, "Analog Control", &cv_useranalog2,  70},
	{IT_STRING  | IT_CVAR, NULL, "Autoaim" , &cv_autoaim2  , 80},
	{IT_STRING  | IT_CVAR, NULL, "Crosshair", &cv_crosshair2, 90},
};

static menuitem_t OP_ControlListMenu[] =
{
	{IT_SUBMENU | IT_STRING, NULL, "Movement Controls...",      &OP_MoveControlsDef,   10},
	{IT_SUBMENU | IT_STRING, NULL, "Multiplayer Controls...",   &OP_MPControlsDef,     20},
	{IT_SUBMENU | IT_STRING, NULL, "Camera Controls...",        &OP_CameraControlsDef, 30},
	{IT_SUBMENU | IT_STRING, NULL, "Miscellaneous Controls...", &OP_MiscControlsDef,   40},
};

static menuitem_t OP_MoveControlsMenu[] =
{
	{IT_CALL | IT_STRING2, NULL, "Forward",      M_ChangeControl, gc_forward    },
	{IT_CALL | IT_STRING2, NULL, "Reverse",      M_ChangeControl, gc_backward   },
	{IT_CALL | IT_STRING2, NULL, "Turn Left",    M_ChangeControl, gc_turnleft   },
	{IT_CALL | IT_STRING2, NULL, "Turn Right",   M_ChangeControl, gc_turnright  },
	{IT_CALL | IT_STRING2, NULL, "Jump",         M_ChangeControl, gc_jump       },
	{IT_CALL | IT_STRING2, NULL, "Spin",         M_ChangeControl, gc_use        },
	{IT_CALL | IT_STRING2, NULL, "Strafe Left",  M_ChangeControl, gc_strafeleft },
	{IT_CALL | IT_STRING2, NULL, "Strafe Right", M_ChangeControl, gc_straferight},
	{IT_CALL | IT_STRING2, NULL, "Strafe On",    M_ChangeControl, gc_strafe     },
};

static menuitem_t OP_MPControlsMenu[] =
{
	{IT_CALL | IT_STRING2, NULL, "Talk key",         M_ChangeControl, gc_talkkey      },
	{IT_CALL | IT_STRING2, NULL, "Team-Talk key",    M_ChangeControl, gc_teamkey      },
	{IT_CALL | IT_STRING2, NULL, "Rankings/Scores",  M_ChangeControl, gc_scores       },
	{IT_CALL | IT_STRING2, NULL, "Toss Flag",        M_ChangeControl, gc_tossflag     },
	{IT_CALL | IT_STRING2, NULL, "Next Weapon",      M_ChangeControl, gc_weaponnext   },
	{IT_CALL | IT_STRING2, NULL, "Prev Weapon",      M_ChangeControl, gc_weaponprev   },
	{IT_CALL | IT_STRING2, NULL, "Weapon Slot 1",    M_ChangeControl, gc_normalring   },
	{IT_CALL | IT_STRING2, NULL, "Weapon Slot 2",    M_ChangeControl, gc_autoring     },
	{IT_CALL | IT_STRING2, NULL, "Weapon Slot 3",    M_ChangeControl, gc_bouncering   },
	{IT_CALL | IT_STRING2, NULL, "Weapon Slot 4",    M_ChangeControl, gc_scatterring  },
	{IT_CALL | IT_STRING2, NULL, "Weapon Slot 5",    M_ChangeControl, gc_grenadering  },
	{IT_CALL | IT_STRING2, NULL, "Weapon Slot 6",    M_ChangeControl, gc_explosionring},
	{IT_CALL | IT_STRING2, NULL, "Weapon Slot 7",    M_ChangeControl, gc_railring     },
	{IT_CALL | IT_STRING2, NULL, "Ring Toss",        M_ChangeControl, gc_fire         },
	{IT_CALL | IT_STRING2, NULL, "Ring Toss Normal", M_ChangeControl, gc_firenormal   },
};

static menuitem_t OP_CameraControlsMenu[] =
{
	{IT_CALL | IT_STRING2, NULL, "Look Up",          M_ChangeControl, gc_lookup       },
	{IT_CALL | IT_STRING2, NULL, "Look Down",        M_ChangeControl, gc_lookdown     },
	{IT_CALL | IT_STRING2, NULL, "Rotate Camera L",  M_ChangeControl, gc_camleft      },
	{IT_CALL | IT_STRING2, NULL, "Rotate Camera R",  M_ChangeControl, gc_camright     },
	{IT_CALL | IT_STRING2, NULL, "Center View",      M_ChangeControl, gc_centerview   },
	{IT_CALL | IT_STRING2, NULL, "Mouselook",        M_ChangeControl, gc_mouseaiming  },
	{IT_CALL | IT_STRING2, NULL, "Reset Camera",     M_ChangeControl, gc_camreset     },
};

static menuitem_t OP_MiscControlsMenu[] =
{
	{IT_CALL | IT_STRING2, NULL, "Taunt",            M_ChangeControl, gc_taunt        },
	{IT_CALL | IT_STRING2, NULL, "Pause",            M_ChangeControl, gc_pause        },
	{IT_CALL | IT_STRING2, NULL, "Console",          M_ChangeControl, gc_console      },
};

static menuitem_t OP_Joystick1Menu[] =
{
	{IT_STRING | IT_CALL,  NULL, "Select Joystick...", M_Setup1PJoystickMenu,  10},
	{IT_STRING | IT_CVAR,  NULL, "Axis For Turning"  , &cv_turnaxis         ,  20},
	{IT_STRING | IT_CVAR,  NULL, "Axis For Moving"   , &cv_moveaxis         ,  30},
	{IT_STRING | IT_CVAR,  NULL, "Axis For Strafe"   , &cv_sideaxis         ,  40},
	{IT_STRING | IT_CVAR,  NULL, "Axis For Looking"  , &cv_lookaxis         ,  50},
	{IT_STRING | IT_CVAR,  NULL, "Axis For Firing"   , &cv_fireaxis         ,  60},
	{IT_STRING | IT_CVAR,  NULL, "Axis For NFiring"  , &cv_firenaxis        ,  70},
};

static menuitem_t OP_Joystick2Menu[] =
{
	{IT_STRING | IT_CALL,  NULL, "Select Joystick...", M_Setup2PJoystickMenu, 10},
	{IT_STRING | IT_CVAR,  NULL, "Axis For Turning"  , &cv_turnaxis2        , 20},
	{IT_STRING | IT_CVAR,  NULL, "Axis For Moving"   , &cv_moveaxis2        , 30},
	{IT_STRING | IT_CVAR,  NULL, "Axis For Strafe"   , &cv_sideaxis2        , 40},
	{IT_STRING | IT_CVAR,  NULL, "Axis For Looking"  , &cv_lookaxis2        , 50},
	{IT_STRING | IT_CVAR,  NULL, "Axis For Firing"   , &cv_fireaxis2        , 60},
	{IT_STRING | IT_CVAR,  NULL, "Axis For NFiring"  , &cv_firenaxis2       , 70},
};

static menuitem_t OP_JoystickSetMenu[] =
{
	{IT_CALL | IT_NOTHING, "None", NULL, M_AssignJoystick, '0'},
	{IT_CALL | IT_NOTHING, "", NULL, M_AssignJoystick, '1'},
	{IT_CALL | IT_NOTHING, "", NULL, M_AssignJoystick, '2'},
	{IT_CALL | IT_NOTHING, "", NULL, M_AssignJoystick, '3'},
	{IT_CALL | IT_NOTHING, "", NULL, M_AssignJoystick, '4'},
	{IT_CALL | IT_NOTHING, "", NULL, M_AssignJoystick, '5'},
	{IT_CALL | IT_NOTHING, "", NULL, M_AssignJoystick, '6'},
};

static menuitem_t OP_MouseOptionsMenu[] =
{
	{IT_STRING | IT_CVAR, NULL, "Use Mouse",        &cv_usemouse,         10},
	{IT_STRING | IT_CVAR, NULL, "Always MouseLook", &cv_alwaysfreelook,   20},
	{IT_STRING | IT_CVAR, NULL, "Mouse Move",       &cv_mousemove,        30},
	{IT_STRING | IT_CVAR, NULL, "Invert Mouse",     &cv_invertmouse,      40},
	{IT_STRING | IT_CVAR | IT_CV_SLIDER,
	                      NULL, "Mouse Speed",      &cv_mousesens,        50},
	{IT_STRING | IT_CVAR | IT_CV_SLIDER,
	                      NULL, "MouseLook Speed",  &cv_mlooksens,        60},
};

static menuitem_t OP_Mouse2OptionsMenu[] =
{
	{IT_STRING | IT_CVAR, NULL, "Use Mouse 2",      &cv_usemouse2,        10},
	{IT_STRING | IT_CVAR, NULL, "Second Mouse Serial Port",
	                                                &cv_mouse2port,       20},
	{IT_STRING | IT_CVAR, NULL, "Always MouseLook", &cv_alwaysfreelook2,  30},
	{IT_STRING | IT_CVAR, NULL, "Mouse Move",       &cv_mousemove2,       40},
	{IT_STRING | IT_CVAR, NULL, "Invert Mouse",    &cv_invertmouse2,      50},
	{IT_STRING | IT_CVAR | IT_CV_SLIDER,
	                      NULL, "Mouse Speed",     &cv_mousesens2,        60},
	{IT_STRING | IT_CVAR | IT_CV_SLIDER,
	                      NULL, "MouseLook Speed",      &cv_mlooksens2,   70},
};

static menuitem_t OP_VideoOptionsMenu[] =
{
	{IT_STRING | IT_SUBMENU, NULL, "Video Modes...",      &OP_VideoModeDef,     10},

#ifdef HWRENDER
	{IT_SUBMENU|IT_STRING, NULL,   "3D Card Options...",  &OP_OpenGLOptionsDef,    30},
#endif

#if defined (__unix__) || defined (UNIXCOMMON) || defined (SDL)
	{IT_STRING|IT_CVAR,      NULL, "Fullscreen",          &cv_fullscreen,    50},
#endif

	{IT_STRING | IT_CVAR | IT_CV_SLIDER,
	                         NULL, "Brightness",          &cv_usegamma,      60},

	{IT_STRING | IT_CVAR,    NULL, "V-SYNC",              &cv_vidwait,       70},

	{IT_STRING | IT_CVAR,    NULL, "Rain/Snow Density",   &cv_precipdensity, 90}, // Changed all to normal string Tails 11-30-2000
	{IT_STRING | IT_CVAR,    NULL, "Rain/Snow Draw Dist", &cv_precipdist,    100}, // Changed all to normal string Tails 11-30-2000
	{IT_STRING | IT_CVAR,    NULL, "FPS Meter",           &cv_ticrate,       110},
};

static menuitem_t OP_VideoModeMenu[] =
{
	{IT_KEYHANDLER | IT_NOTHING, NULL, "", M_HandleVideoMode, '\0'},     // dummy menuitem for the control func
};

#ifdef HWRENDER
static menuitem_t OP_OpenGLOptionsMenu[] =
{
	{IT_STRING|IT_CVAR,         NULL, "Field of view",   &cv_grfov,            10},
	{IT_STRING|IT_CVAR,         NULL, "Quality",         &cv_scr_depth,        20},
	{IT_STRING|IT_CVAR,         NULL, "Texture Filter",  &cv_grfiltermode,     30},
	{IT_STRING|IT_CVAR,         NULL, "Anisotropic",     &cv_granisotropicmode,40},
#ifdef _WINDOWS
	{IT_STRING|IT_CVAR,         NULL, "Fullscreen",      &cv_fullscreen,       50},
#endif
	{IT_STRING|IT_CVAR|IT_CV_SLIDER,
	                            NULL, "Translucent HUD", &cv_grtranslucenthud, 60},
	{IT_SUBMENU|IT_WHITESTRING, NULL, "Fog...",          &OP_OpenGLFogDef,          80},
	{IT_SUBMENU|IT_WHITESTRING, NULL, "Gamma...",        &OP_OpenGLColorDef,        90},
};

static menuitem_t OP_OpenGLFogMenu[] =
{
	{IT_STRING|IT_CVAR,       NULL, "Fog",         &cv_grfog,        10},
	{IT_STRING|IT_KEYHANDLER, NULL, "Fog color",   M_HandleFogColor, 20},
	{IT_STRING|IT_CVAR,       NULL, "Fog density", &cv_grfogdensity, 30},
};

static menuitem_t OP_OpenGLColorMenu[] =
{
	{IT_STRING|IT_CVAR|IT_CV_SLIDER, NULL, "red",   &cv_grgammared,   10},
	{IT_STRING|IT_CVAR|IT_CV_SLIDER, NULL, "green", &cv_grgammagreen, 20},
	{IT_STRING|IT_CVAR|IT_CV_SLIDER, NULL, "blue",  &cv_grgammablue,  30},
};
#endif

static menuitem_t OP_SoundOptionsMenu[] =
{
	{IT_STRING | IT_CVAR | IT_CV_SLIDER,
                              NULL, "Sound Volume" , &cv_soundvolume,     10},
	{IT_STRING | IT_CVAR | IT_CV_SLIDER,
                              NULL, "Music Volume" , &cv_digmusicvolume,  20},
	{IT_STRING | IT_CVAR | IT_CV_SLIDER,
                              NULL, "MIDI Volume"  , &cv_midimusicvolume, 30},
#ifdef PC_DOS
	{IT_STRING | IT_CVAR | IT_CV_SLIDER,
                              NULL, "CD Volume"    , &cd_volume,          40},
#endif

	{IT_STRING    | IT_CALL,  NULL,  "Toggle SFX"   , M_ToggleSFX,        50},
	{IT_STRING    | IT_CALL,  NULL,  "Toggle Digital Music", M_ToggleDigital,     60},
	{IT_STRING    | IT_CALL,  NULL,  "Toggle MIDI Music", M_ToggleMIDI,        70},
};

static menuitem_t OP_DataOptionsMenu[] =
{
	{IT_STRING | IT_CALL | IT_CALL_NOTINGAME, NULL, "Erase Time Attack Data", M_EraseData, 10},
	{IT_STRING | IT_CALL | IT_CALL_NOTINGAME, NULL, "Erase Secrets Data", M_EraseData, 20},
};

static menuitem_t OP_GameOptionsMenu[] =
{
	{IT_STRING | IT_CVAR | IT_CV_STRING,
	                      NULL, "Master server",          &cv_masterserver,     10},
	// Tails
	{IT_STRING | IT_CVAR, NULL, "Show HUD",               &cv_showhud,     40},
#ifdef SEENAMES
	{IT_STRING | IT_CVAR, NULL, "HUD Player Names",       &cv_seenames,    50},
#endif
	{IT_STRING | IT_CVAR, NULL, "High Resolution Timer",  &cv_timetic,     60},

	{IT_STRING | IT_CVAR, NULL, "Console Back Color",     &cons_backcolor, 80},
	{IT_STRING | IT_CVAR, NULL, "Uppercase Console",      &cv_allcaps,     90},
};

static menuitem_t OP_ServerOptionsMenu[] =
{
	{IT_STRING | IT_SUBMENU, NULL, "General netgame options...",  &OP_NetgameOptionsDef,  10},
	{IT_STRING | IT_SUBMENU, NULL, "Gametype options...",         &OP_GametypeOptionsDef, 20},

	{IT_STRING | IT_CVAR | IT_CV_STRING,
	                         NULL, "Server name",                 &cv_servername,         40},

	{IT_STRING | IT_CVAR,    NULL, "Consistency Protection",      &cv_consfailprotect,    70},
	{IT_STRING | IT_CVAR,    NULL, "Intermission Timer",          &cv_inttime,            80},
	{IT_STRING | IT_CVAR,    NULL, "Advance to next map",         &cv_advancemap,         90},
	{IT_STRING | IT_CVAR,    NULL, "Force Skin #",                &cv_forceskin,         100},

	{IT_STRING | IT_CVAR,    NULL, "Max Players",                 &cv_maxplayers,        120},
	{IT_STRING | IT_CVAR,    NULL, "Allow players to join",       &cv_allownewplayer,    130},
	{IT_STRING | IT_CVAR,    NULL, "Allow WAD Downloading",       &cv_downloading,       140},
#ifdef TEXTCOLOR
	{IT_STRING | IT_CVAR,    NULL, "Allow colors in chat",        &cv_allowcolors,       150},
#endif
};

static menuitem_t OP_NetgameOptionsMenu[] =
{
	{IT_STRING | IT_CVAR, NULL, "Time Limit",            &cv_timelimit,        10},
	{IT_STRING | IT_CVAR, NULL, "Point Limit",           &cv_pointlimit,       20},
	{IT_STRING | IT_CVAR, NULL, "Overtime Tie-Breaker",  &cv_overtime,         30},

	{IT_STRING | IT_CVAR, NULL, "Special Ring Weapons",  &cv_specialrings,     50},
	{IT_STRING | IT_CVAR, NULL, "Emeralds",              &cv_powerstones,      60},
	{IT_STRING | IT_CVAR, NULL, "Item Boxes",            &cv_matchboxes,       70},
	{IT_STRING | IT_CVAR, NULL, "Item Respawn",          &cv_itemrespawn,      80},
	{IT_STRING | IT_CVAR, NULL, "Item Respawn time",     &cv_itemrespawntime,  90},

	{IT_STRING | IT_CVAR, NULL, "Sudden Death",          &cv_suddendeath,     110},

	{IT_STRING | IT_SUBMENU, NULL, "Random Monitor Toggles...", &OP_MonitorToggleDef,130},
};

static menuitem_t OP_GametypeOptionsMenu[] =
{
	{IT_HEADER,           NULL, "CO-OP",                 NULL,                  0},
	{IT_STRING | IT_CVAR, NULL, "Players for exit",      &cv_playersforexit,    8},
	{IT_STRING | IT_CVAR, NULL, "Starting Lives",        &cv_startinglives,    16},

	{IT_HEADER,           NULL, "RACE",                  NULL,                 28},
	{IT_STRING | IT_CVAR, NULL, "Item Boxes",            &cv_raceitemboxes,    36},
	{IT_STRING | IT_CVAR, NULL, "Number of Laps",        &cv_numlaps,          44},
	{IT_STRING | IT_CVAR, NULL, "Countdown Time",        &cv_countdowntime,    52},

	{IT_HEADER,           NULL, "MATCH",                 NULL,                 64},
	{IT_STRING | IT_CVAR, NULL, "Scoring Type",          &cv_match_scoring,    72},

	{IT_HEADER,           NULL, "TAG",                   NULL,                 84},
	{IT_STRING | IT_CVAR, NULL, "Hide Time",             &cv_hidetime,         92},

	{IT_HEADER,           NULL, "CTF",                   NULL,                104},
	{IT_STRING | IT_CVAR, NULL, "Flag Respawn Time",     &cv_flagtime,        112},
	{IT_STRING | IT_CVAR, NULL, "Autobalance",           &cv_autobalance,     120},
	{IT_STRING | IT_CVAR, NULL, "Scramble on Map Change",&cv_scrambleonchange,128},

	{IT_HEADER,           NULL, "SHARDS",                NULL,                140},
	{IT_STRING | IT_CVAR, NULL, "Number of Jewel Shards",&cv_jewelshards,     148},
};

static menuitem_t OP_MonitorToggleMenu[] =
{
	{IT_STRING|IT_CVAR, NULL, "Combi-Ring",        &cv_combiring,     10},
	{IT_STRING|IT_CVAR, NULL, "Recycler",          &cv_recycler,      20},
	{IT_STRING|IT_CVAR, NULL, "Teleporters",       &cv_teleporters,   30},
	{IT_STRING|IT_CVAR, NULL, "Super Ring",        &cv_superring,     40},
	{IT_STRING|IT_CVAR, NULL, "Super Sneakers",    &cv_supersneakers, 50},
	{IT_STRING|IT_CVAR, NULL, "Invincibility",     &cv_invincibility, 60},
	{IT_STRING|IT_CVAR, NULL, "Jump Shield",       &cv_jumpshield,    70},
	{IT_STRING|IT_CVAR, NULL, "Elemental Shield",  &cv_watershield,   80},
	{IT_STRING|IT_CVAR, NULL, "Attraction Shield", &cv_ringshield,    90},
	{IT_STRING|IT_CVAR, NULL, "Force Shield",      &cv_forceshield,  100},
	{IT_STRING|IT_CVAR, NULL, "Armageddon Shield", &cv_bombshield,   110},
	{IT_STRING|IT_CVAR, NULL, "1 Up",              &cv_1up,          120},
	{IT_STRING|IT_CVAR, NULL, "Eggman Box",        &cv_eggmanbox,    130},
};

// ==========================================================================
// ALL MENU DEFINITIONS GO HERE
// ==========================================================================

// Main Menu and related
menu_t MainDef = CENTERMENUSTYLE(NULL, MainMenu, NULL, 72);
menu_t SPauseDef = PAUSEMENUSTYLE(SPauseMenu, 40, 72);
menu_t MPauseDef = PAUSEMENUSTYLE(MPauseMenu, 40, 72);

// Misc Main Menu
menu_t MISC_ScrambleTeamDef = DEFAULTMENUSTYLE(NULL, MISC_ScrambleTeamMenu, &MPauseDef, 27, 40);
menu_t MISC_ChangeTeamDef = DEFAULTMENUSTYLE(NULL, MISC_ChangeTeamMenu, &MPauseDef, 27, 40);
menu_t MISC_ChangeLevelDef = MAPICONMENUSTYLE(NULL, MISC_ChangeLevelMenu, &MPauseDef);
menu_t MISC_HelpDef = IMAGEDEF(MISC_HelpMenu);

// Sky Room
menu_t SR_PandoraDef =
{
	"M_OPTTTL",
	sizeof (SR_PandorasBox)/sizeof (menuitem_t),
	&SPauseDef,
	SR_PandorasBox,
	M_DrawGenericMenu,
	60,40,
	0,
	M_ExitPandorasBox
};
menu_t SR_MainDef = DEFAULTMENUSTYLE("M_OPTTTL", SR_MainMenu, &MainDef, 60, 40);
menu_t SR_CustomDef = DEFAULTMENUSTYLE("M_OPTTTL", SR_CustomMenu, &MainDef, 60, 40);
menu_t SR_LevelSelectDef =
{
	0,
	sizeof (SR_LevelSelectMenu)/sizeof (menuitem_t),
	&SR_MainDef,
	SR_LevelSelectMenu,
	M_DrawLevelSelectMenu,
	40, 40,
	0,
	NULL
};
menu_t SR_UnlockChecklistDef =
{
	NULL,
	1,
	&SR_MainDef,
	SR_UnlockChecklistMenu,
	NULL, // INTENTIONALLY NULL; Just making sure M_Checklist works properly
	280, 185,
	0,
	NULL
};

// Single Player
menu_t SP_MainDef = CENTERMENUSTYLE(NULL, SP_MainMenu, &MainDef, 72);
menu_t SP_LoadDef =
{
	"M_PICKG",
	1,
	&SP_MainDef,
	SP_LoadGameMenu,
	M_DrawLoad,
	68, 54,
	0,
	NULL
};
menu_t SP_LevelSelectDef = MAPICONMENUSTYLE(NULL, SP_LevelSelectMenu, &SP_LoadDef);
menu_t SP_ReplayAttackDef = CENTERMENUSTYLE(NULL, SP_ReplayAttackMenu, &SP_MainDef, 72);
menu_t SP_StatsDef =
{
	NULL,
	1,
	&SP_MainDef,
	SP_StatsMenu,
	M_DrawStats,
	280, 185,
	0,
	NULL
};
menu_t SP_TimeAttackDef =
{
	0,
	sizeof (SP_TimeAttackMenu)/sizeof (menuitem_t),
	&MainDef,  // Doesn't matter.
	SP_TimeAttackMenu,
	M_DrawTimeAttackMenu,
	40, 40,
	0,
	NULL
};
menu_t SP_PlayerDef =
{
	"M_PICKP",
	sizeof (PlayerMenu)/sizeof (menuitem_t),//player_end,
	&SP_MainDef,
	PlayerMenu,
	M_DrawSetupChoosePlayerMenu,
	24, 32,
	0,
	NULL
};

// Multiplayer
menu_t MP_MainDef = DEFAULTMENUSTYLE("M_MULTI", MP_MainMenu, &MainDef, 60, 40);
menu_t MP_ServerDef = MAPICONMENUSTYLE("M_MULTI", MP_ServerMenu, &MP_MainDef);
#ifndef NONET
menu_t MP_ConnectDef =
{
	"M_MULTI",
	sizeof (MP_ConnectMenu)/sizeof (menuitem_t),
	&MP_MainDef,
	MP_ConnectMenu,
	M_DrawConnectMenu,
	27,24,
	0,
	M_CancelConnect
};
menu_t MP_ConnectLANDef =
{
	"M_MULTI",
	sizeof (MP_ConnectLANMenu)/sizeof (menuitem_t),
	&MP_MainDef,
	MP_ConnectLANMenu,
	M_DrawConnectLANMenu,
	27,24,
	0,
	M_CancelConnect
};
menu_t MP_ConnectIPDef =
{
	"M_MULTI",
	sizeof (MP_ConnectIPMenu)/sizeof (menuitem_t),
	&MP_MainDef,
	MP_ConnectIPMenu,
	M_DrawConnectIPMenu,
	27,40,
	0,
	M_CancelConnect
};
menu_t MP_RoomInfoDef =
{
	"M_MULTI",
	sizeof (MP_RoomInfoMenu)/sizeof (menuitem_t),
	&MP_ConnectDef,
	MP_RoomInfoMenu,
	M_DrawRoomInfoMenu,
	27, 40,
	0,
	NULL
};
#endif
menu_t MP_SplitServerDef = MAPICONMENUSTYLE("M_MULTI", MP_SplitServerMenu, &MP_MainDef);
menu_t MP_PlayerSetupDef =
{
	"M_PICKP",
	sizeof (MP_PlayerSetupMenu)/sizeof (menuitem_t),
	&MP_MainDef,
	MP_PlayerSetupMenu,
	M_DrawSetupMultiPlayerMenu,
	27, 40,
	0,
	M_QuitMultiPlayerMenu
};

// Options
menu_t OP_MainDef = DEFAULTMENUSTYLE("M_OPTTTL", OP_MainMenu, &MainDef, 60, 30);
menu_t OP_ControlsDef = DEFAULTMENUSTYLE("M_CONTRO", OP_ControlsMenu, &OP_MainDef, 60, 30);
menu_t OP_ControlListDef = DEFAULTMENUSTYLE("M_CONTRO", OP_ControlListMenu, &OP_ControlsDef, 60, 30);
menu_t OP_MoveControlsDef = CONTROLMENUSTYLE(OP_MoveControlsMenu, &OP_ControlListDef);
menu_t OP_MPControlsDef = CONTROLMENUSTYLE(OP_MPControlsMenu, &OP_ControlListDef);
menu_t OP_CameraControlsDef = CONTROLMENUSTYLE(OP_CameraControlsMenu, &OP_ControlListDef);
menu_t OP_MiscControlsDef = CONTROLMENUSTYLE(OP_MiscControlsMenu, &OP_ControlListDef);
menu_t OP_P1ControlsDef = DEFAULTMENUSTYLE("M_CONTRO", OP_P1ControlsMenu, &OP_ControlsDef, 60, 30);
menu_t OP_P2ControlsDef = DEFAULTMENUSTYLE("M_CONTRO", OP_P2ControlsMenu, &OP_ControlsDef, 60, 30);
menu_t OP_MouseOptionsDef = DEFAULTMENUSTYLE("M_CONTRO", OP_MouseOptionsMenu, &OP_P1ControlsDef, 27, 30);
menu_t OP_Mouse2OptionsDef = DEFAULTMENUSTYLE("M_CONTRO", OP_Mouse2OptionsMenu, &OP_P2ControlsDef, 27, 30);
menu_t OP_Joystick1Def = DEFAULTMENUSTYLE("M_CONTRO", OP_Joystick1Menu, &OP_P1ControlsDef, 27, 30);
menu_t OP_Joystick2Def = DEFAULTMENUSTYLE("M_CONTRO", OP_Joystick2Menu, &OP_P2ControlsDef, 27, 30);
menu_t OP_JoystickSetDef =
{
	"M_CONTRO",
	sizeof (OP_JoystickSetMenu)/sizeof (menuitem_t),
	&OP_Joystick1Def,
	OP_JoystickSetMenu,
	M_DrawJoystick,
	50, 40,
	0,
	NULL
};

menu_t OP_VideoOptionsDef = DEFAULTMENUSTYLE("M_VIDEO", OP_VideoOptionsMenu, &OP_MainDef, 60, 30);
menu_t OP_VideoModeDef =
{
	"M_VIDEO",
	1,
	&OP_VideoOptionsDef,
	OP_VideoModeMenu,
	M_DrawVideoMode,
	48, 36,
	0,
	NULL
};
menu_t OP_SoundOptionsDef = DEFAULTMENUSTYLE("M_SVOL", OP_SoundOptionsMenu, &OP_MainDef, 60, 30);
menu_t OP_GameOptionsDef = DEFAULTMENUSTYLE("M_OPTTTL", OP_GameOptionsMenu, &OP_MainDef, 30, 30);
menu_t OP_ServerOptionsDef = DEFAULTMENUSTYLE("M_OPTTTL", OP_ServerOptionsMenu, &OP_MainDef, 30, 30);

menu_t OP_NetgameOptionsDef = DEFAULTMENUSTYLE("M_OPTTTL", OP_NetgameOptionsMenu, &OP_ServerOptionsDef, 30, 30);
menu_t OP_GametypeOptionsDef = DEFAULTMENUSTYLE("M_OPTTTL", OP_GametypeOptionsMenu, &OP_ServerOptionsDef, 30, 30);
menu_t OP_MonitorToggleDef = DEFAULTMENUSTYLE("M_OPTTTL", OP_MonitorToggleMenu, &OP_NetgameOptionsDef, 30, 30);

#ifdef HWRENDER
menu_t OP_OpenGLOptionsDef = DEFAULTMENUSTYLE("M_VIDEO", OP_OpenGLOptionsMenu, &OP_VideoOptionsDef, 30, 30);
menu_t OP_OpenGLFogDef =
{
	"M_VIDEO",
	sizeof (OP_OpenGLFogMenu)/sizeof (menuitem_t),
	&OP_OpenGLOptionsDef,
	OP_OpenGLFogMenu,
	M_OGL_DrawFogMenu,
	60, 40,
	0,
	NULL
};
menu_t OP_OpenGLColorDef =
{
	"M_VIDEO",
	sizeof (OP_OpenGLColorMenu)/sizeof (menuitem_t),
	&OP_OpenGLOptionsDef,
	OP_OpenGLColorMenu,
	M_OGL_DrawColorMenu,
	60, 40,
	0,
	NULL
};
#endif
menu_t OP_DataOptionsDef = DEFAULTMENUSTYLE("M_OPTTTL", OP_DataOptionsMenu, &OP_MainDef, 60, 30);

// ==========================================================================
// CVAR ONCHANGE EVENTS GO HERE
// ==========================================================================
// (there's only a couple anyway)

// Prototypes
static INT32 M_FindFirstMap(INT32 gtype);
static boolean M_PatchLevelNameTable(INT32 mode);
static boolean M_PatchLevelNameTableByMod(INT32 mode);

// Nextmap.  Used for Time Attack.
static void Nextmap_OnChange(void)
{
	if (currentMenu != &SP_TimeAttackDef)
		return;

	SP_TimeAttackMenu[tareplay].status = IT_DISABLED;
	SP_TimeAttackMenu[tastaffsonic].status = IT_DISABLED;
	SP_TimeAttackMenu[tastafftails].status = IT_DISABLED;
	SP_TimeAttackMenu[tastaffknux].status = IT_DISABLED;

	// Check if file exists, if not, disable REPLAY option
	if (FIL_FileExists(va("%s"PATHSEP"replay"PATHSEP"%s"PATHSEP"%s-%02d.lmp", srb2home, modtaf[selectedmod], G_BuildMapName(cv_nextmap.value), cv_chooseskin.value-1)))
		SP_TimeAttackMenu[tareplay].status = IT_WHITESTRING|IT_CALL;

	if (mapheaderinfo[cv_nextmap.value-1].forcecharacter != 255 && &skins[(mapheaderinfo[cv_nextmap.value-1].forcecharacter)])
		CV_Set(&cv_chooseskin, skins[mapheaderinfo[cv_nextmap.value-1].forcecharacter].name);

	//TODO: remove this hack.
	// In other words, change demo format. grr...
	if (W_CheckNumForName(va("STS%s",G_BuildMapName(cv_nextmap.value))) != LUMPERROR)
		SP_TimeAttackMenu[tastaffsonic].status = IT_WHITESTRING|IT_CALL;
	else if (W_CheckNumForName(va("STT%s",G_BuildMapName(cv_nextmap.value))) != LUMPERROR)
		SP_TimeAttackMenu[tastafftails].status = IT_WHITESTRING|IT_CALL;
	else if (W_CheckNumForName(va("STK%s",G_BuildMapName(cv_nextmap.value))) != LUMPERROR)
		SP_TimeAttackMenu[tastaffknux].status = IT_WHITESTRING|IT_CALL;
}

// Newgametype.  Used for gametype changes.
static void Newgametype_OnChange(void)
{
	if (menuactive)
	{
		if ((cv_newgametype.value == GT_COOP && !(mapheaderinfo[cv_nextmap.value-1].typeoflevel & TOL_COOP)) ||
			((cv_newgametype.value == GT_RACE || cv_newgametype.value == GTF_CLASSICRACE) && !(mapheaderinfo[cv_nextmap.value-1].typeoflevel & TOL_RACE)) ||
			((cv_newgametype.value == GT_MATCH || cv_newgametype.value == GTF_TEAMMATCH) && !(mapheaderinfo[cv_nextmap.value-1].typeoflevel & TOL_MATCH)) ||
#ifdef CHAOSISNOTDEADYET
			(cv_newgametype.value == GT_CHAOS && !(mapheaderinfo[cv_nextmap.value-1].typeoflevel & TOL_CHAOS)) ||
#endif
			(cv_newgametype.value == GT_SHARDS && !(mapheaderinfo[cv_nextmap.value-1].typeoflevel & TOL_SHARDS)) ||
			((cv_newgametype.value == GT_TAG || cv_newgametype.value == GTF_HIDEANDSEEK) && !(mapheaderinfo[cv_nextmap.value-1].typeoflevel & TOL_TAG)) ||
			(cv_newgametype.value == GT_CTF && !(mapheaderinfo[cv_nextmap.value-1].typeoflevel & TOL_CTF)))
		{
			INT32 value = 0;

			switch (cv_newgametype.value)
			{
				case GT_COOP:
					value = TOL_COOP;
					break;
				case GT_RACE:
					value = TOL_RACE;
					break;
				case GT_MATCH:
					value = TOL_MATCH;
					break;
				case GT_SHARDS:
					value = TOL_SHARDS;
					break;
				case GT_TAG:
					value = TOL_TAG;
					break;
				case GT_CTF:
					value = TOL_CTF;
					break;
			}

			CV_SetValue(&cv_nextmap, M_FindFirstMap(value));
			CV_AddValue(&cv_nextmap, -1);
			CV_AddValue(&cv_nextmap, 1);
		}
	}
}

#ifndef NONET
// Chooseroom.  Used for room info.
static void Chooseroom_OnChange(void)
{
	if (currentMenu == &MP_RoomInfoDef)
		M_AlterRoomInfo();
}
#endif

// ==========================================================================
// END ORGANIZATION STUFF.
// ==========================================================================

// current menudef
menu_t *currentMenu = &MainDef;

// =========================================================================
// BASIC MENU HANDLING
// =========================================================================

static void M_ChangeCvar(INT32 choice)
{
	consvar_t *cv = (consvar_t *)currentMenu->menuitems[itemOn].itemaction;

	if (((currentMenu->menuitems[itemOn].status & IT_CVARTYPE) == IT_CV_SLIDER)
	    ||((currentMenu->menuitems[itemOn].status & IT_CVARTYPE) == IT_CV_NOMOD))
	{
		CV_SetValue(cv,cv->value+(choice*2-1));
	}
	else if (cv->flags & CV_FLOAT)
	{
		char s[20];
		sprintf(s,"%f",FIXED_TO_FLOAT(cv->value)+(choice*2-1)*(1.0f/16.0f));
		CV_Set(cv,s);
	}
	else
		CV_AddValue(cv,choice*2-1);
}

static boolean M_ChangeStringCvar(INT32 choice)
{
	consvar_t *cv = (consvar_t *)currentMenu->menuitems[itemOn].itemaction;
	char buf[255];
	size_t len;

	switch (choice)
	{
		case KEY_BACKSPACE:
			len = strlen(cv->string);
			if (len > 0)
			{
				M_Memcpy(buf, cv->string, len);
				buf[len-1] = 0;
				CV_Set(cv, buf);
			}
			return true;
		default:
			if (choice >= 32 && choice <= 127)
			{
				len = strlen(cv->string);
				if (len < MAXSTRINGLENGTH - 1)
				{
					M_Memcpy(buf, cv->string, len);
					buf[len++] = (char)choice;
					buf[len] = 0;
					CV_Set(cv, buf);
				}
				return true;
			}
			break;
	}
	return false;
}

//
// M_Responder
//
boolean M_Responder(event_t *ev)
{
	INT32 ch = -1;
//	INT32 i;
	static tic_t joywait = 0, mousewait = 0;
	static INT32 pmousex = 0, pmousey = 0;
	static INT32 lastx = 0, lasty = 0;
	void (*routine)(INT32 choice); // for some casting problem

	if (dedicated || gamestate == GS_INTRO || gamestate == GS_INTRO2 || gamestate == GS_CUTSCENE)
		return false;

	if (ev->type == ev_keyup && (ev->data1 == KEY_LSHIFT || ev->data1 == KEY_RSHIFT))
	{
		shiftdown = false;
		return false;
	}
	else if (ev->type == ev_keydown)
	{
		ch = ev->data1;

		// added 5-2-98 remap virtual keys (mouse & joystick buttons)
		switch (ch)
		{
			case KEY_LSHIFT:
			case KEY_RSHIFT:
				shiftdown = true;
				break; //return false;
			case KEY_MOUSE1:
			case KEY_JOY1:
			case KEY_JOY1 + 2:
				ch = KEY_ENTER;
				break;
			case KEY_JOY1 + 3:
				ch = 'n';
				break;
			case KEY_MOUSE1 + 1:
			case KEY_JOY1 + 1:
				ch = KEY_BACKSPACE;
				break;
			case KEY_HAT1:
				ch = KEY_UPARROW;
				break;
			case KEY_HAT1 + 1:
				ch = KEY_DOWNARROW;
				break;
			case KEY_HAT1 + 2:
				ch = KEY_LEFTARROW;
				break;
			case KEY_HAT1 + 3:
				ch = KEY_RIGHTARROW;
				break;
		}
	}
	else if (menuactive)
	{
		if (ev->type == ev_joystick  && ev->data1 == 0 && joywait < I_GetTime())
		{
			if (ev->data3 == -1)
			{
				ch = KEY_UPARROW;
				joywait = I_GetTime() + TICRATE/7;
			}
			else if (ev->data3 == 1)
			{
				ch = KEY_DOWNARROW;
				joywait = I_GetTime() + TICRATE/7;
			}

			if (ev->data2 == -1)
			{
				ch = KEY_LEFTARROW;
				joywait = I_GetTime() + TICRATE/17;
			}
			else if (ev->data2 == 1)
			{
				ch = KEY_RIGHTARROW;
				joywait = I_GetTime() + TICRATE/17;
			}
		}
		else if (ev->type == ev_mouse && mousewait < I_GetTime())
		{
			pmousey += ev->data3;
			if (pmousey < lasty-30)
			{
				ch = KEY_DOWNARROW;
				mousewait = I_GetTime() + TICRATE/7;
				pmousey = lasty -= 30;
			}
			else if (pmousey > lasty + 30)
			{
				ch = KEY_UPARROW;
				mousewait = I_GetTime() + TICRATE/7;
				pmousey = lasty += 30;
			}

			pmousex += ev->data2;
			if (pmousex < lastx - 30)
			{
				ch = KEY_LEFTARROW;
				mousewait = I_GetTime() + TICRATE/7;
				pmousex = lastx -= 30;
			}
			else if (pmousex > lastx+30)
			{
				ch = KEY_RIGHTARROW;
				mousewait = I_GetTime() + TICRATE/7;
				pmousex = lastx += 30;
			}
		}
	}

	if (ch == -1)
		return false;

	// F-Keys
	if (!menuactive || ch == KEY_F8) //allow screenshots
	{
		switch (ch)
		{
			case KEY_F1: // Help key
				M_StartControlPanel();
				currentMenu = &MISC_HelpDef;
				itemOn = 0;
				return true;

			case KEY_F2: // Empty
				return true;

			case KEY_F3: // Empty
				return true;

			case KEY_F4: // Sound Volume
				M_StartControlPanel();
				currentMenu = &OP_SoundOptionsDef;
				itemOn = 0;
				return true;

#ifndef DC
			case KEY_F5: // Video Mode
				M_StartControlPanel();
				M_SetupNextMenu(&OP_VideoModeDef);
				return true;
#endif

			case KEY_F6: // Empty
				return true;

			case KEY_F7: // Options
				M_StartControlPanel();
				M_SetupNextMenu(&OP_MainDef);
				return true;

			case KEY_F8: // Screenshot
				COM_ImmedExecute("screenshot\n");
				return true;

			case KEY_F9: // Empty
				return true;

			case KEY_F10: // Quit SRB2
				M_QuitSRB2(0);
				return true;

			case KEY_F11: // Gamma Level
				CV_AddValue(&cv_usegamma, 1);
				return true;

			case KEY_ESCAPE: // Pop up menu
				if (chat_on)
				{
					HU_clearChatChars();
					chat_on = false;
#ifdef TEXTCOLOR
					insert_color = false;
#endif
				}
				else
					M_StartControlPanel();
				return true;
		}
		return false;
	}

	routine = currentMenu->menuitems[itemOn].itemaction;

	// Handle menuitems which need a specific key handling
	if (routine && (currentMenu->menuitems[itemOn].status & IT_TYPE) == IT_KEYHANDLER)
	{
		if (shiftdown && ch >= 32 && ch <= 127)
			ch = shiftxform[ch];
		routine(ch);
		return true;
	}

	if (currentMenu->menuitems[itemOn].status == IT_MSGHANDLER)
	{
		if (currentMenu->menuitems[itemOn].alphaKey != MM_EVENTHANDLER)
		{
			if (ch == ' ' || ch == 'n' || ch == 'y' || ch == KEY_ESCAPE || ch == KEY_ENTER)
			{
				if (routine)
					routine(ch);
				M_StopMessage(0);
				return true;
			}
			return true;
		}
		else
		{
			// dirty hack: for customising controls, I want only buttons/keys, not moves
			if (ev->type == ev_mouse || ev->type == ev_mouse2 || ev->type == ev_joystick
				|| ev->type == ev_joystick2)
				return true;
			if (routine)
			{
				void (*otherroutine)(event_t *sev) = currentMenu->menuitems[itemOn].itemaction;
				otherroutine(ev); //Alam: what a hack
			}
			return true;
		}
	}

	// BP: one of the more big hack i have never made
	if (routine && (currentMenu->menuitems[itemOn].status & IT_TYPE) == IT_CVAR)
	{
		if ((currentMenu->menuitems[itemOn].status & IT_CVARTYPE) == IT_CV_STRING)
		{
			if (shiftdown && ch >= 32 && ch <= 127)
				ch = shiftxform[ch];
			if (M_ChangeStringCvar(ch))
				return true;
			else
				routine = NULL;
		}
		else
			routine = M_ChangeCvar;
	}

	// Keys usable within menu
	switch (ch)
	{
		case KEY_DOWNARROW:
			do
			{
				if (itemOn + 1 > currentMenu->numitems - 1)
					itemOn = 0;
				else
					itemOn++;
			} while ((currentMenu->menuitems[itemOn].status & IT_TYPE) == IT_SPACE);

			S_StartSound(NULL, sfx_menu1);
			return true;

		case KEY_UPARROW:
			do
			{
				if (!itemOn)
					itemOn = (INT16)(currentMenu->numitems - 1);
				else
					itemOn--;
			} while ((currentMenu->menuitems[itemOn].status & IT_TYPE) == IT_SPACE);

			S_StartSound(NULL, sfx_menu1);
			return true;

		case KEY_LEFTARROW:
			if (routine && ((currentMenu->menuitems[itemOn].status & IT_TYPE) == IT_ARROWS
				|| (currentMenu->menuitems[itemOn].status & IT_TYPE) == IT_CVAR))
			{
				if (currentMenu != &OP_SoundOptionsDef)
					S_StartSound(NULL, sfx_menu1);
				routine(0);
			}
			return true;

		case KEY_RIGHTARROW:
			if (routine && ((currentMenu->menuitems[itemOn].status & IT_TYPE) == IT_ARROWS
				|| (currentMenu->menuitems[itemOn].status & IT_TYPE) == IT_CVAR))
			{
				if (currentMenu != &OP_SoundOptionsDef)
					S_StartSound(NULL, sfx_menu1);
				routine(1);
			}
			return true;

		case KEY_ENTER:
			currentMenu->lastOn = itemOn;
			if (routine)
			{
				if (((currentMenu->menuitems[itemOn].status & IT_TYPE)==IT_CALL
				 || (currentMenu->menuitems[itemOn].status & IT_TYPE)==IT_SUBMENU)
                 && (currentMenu->menuitems[itemOn].status & IT_CALLTYPE))
				{
					if (((currentMenu->menuitems[itemOn].status & IT_CALLTYPE) & IT_CALL_NOTINGAME) && (Playing()))
					{
						S_StartSound(NULL, sfx_menu1);
						M_StartMessage("You can't use this while in a game!\n\n(Press a key)", NULL, MM_NOTHING);
						return true;
					}

					if (((currentMenu->menuitems[itemOn].status & IT_CALLTYPE) & IT_CALL_NOTMODIFIED) && modifiedgame && !savemoddata)
					{
						S_StartSound(NULL, sfx_menu1);
						M_StartMessage("This cannot be done in a modified game.\n\n(Press a key)", NULL, MM_NOTHING);
						return true;
					}
				}
				switch (currentMenu->menuitems[itemOn].status & IT_TYPE)
				{
					case IT_CVAR:
					case IT_ARROWS:
						routine(1); // right arrow
						S_StartSound(NULL, sfx_menu1);
						break;
					case IT_CALL:
						routine(itemOn);
						S_StartSound(NULL, sfx_menu1);
						break;
					case IT_SUBMENU:
						currentMenu->lastOn = itemOn;
						M_SetupNextMenu((menu_t *)currentMenu->menuitems[itemOn].itemaction);
						S_StartSound(NULL, sfx_menu1);
						break;
				}
			}
			return true;

		case KEY_ESCAPE:
			currentMenu->lastOn = itemOn;
			if (currentMenu->prevMenu)
			{
				//If we entered the game search menu, but didn't enter a game,
				//make sure the game doesn't still think we're in a netgame.
				if (!Playing() && netgame && multiplayer)
				{
					MSCloseUDPSocket();		// Clean up so we can re-open the connection later.
					netgame = false;
					multiplayer = false;
				}

				if (currentMenu == &SP_TimeAttackDef)
				{
					// Fade to black first
					if (rendermode != render_none)
					{
						V_DrawFill(0, 0, vid.width, vid.height, 31);
						F_WipeEndScreen(0, 0, vid.width, vid.height);

						F_RunWipe(0, false);
					}
					menuactive = false;
					D_StartTitle();
				}
				else
					M_SetupNextMenu(currentMenu->prevMenu);
			}
			else
			{
				M_ClearMenus(true);
				if (newmoon)
					S_StartSound(NULL, sfx_iy_menu_close);
			}
			return true;

		case KEY_BACKSPACE:
			if ((currentMenu->menuitems[itemOn].status) == IT_CONTROL)
			{
				// detach any keys associated with the game control
				G_ClearControlKeys(setupcontrols, currentMenu->menuitems[itemOn].alphaKey);
				return true;
			}
			currentMenu->lastOn = itemOn;
			if (currentMenu->prevMenu)
				M_SetupNextMenu(currentMenu->prevMenu);
			return true;

		default:
			break;
	}

	return true;
}

//
// M_Drawer
// Called after the view has been rendered,
// but before it has been blitted.
//
void M_Drawer(void)
{
	if (currentMenu == &MessageDef)
		menuactive = true;

	if (!menuactive)
		return;

	// now that's more readable with a faded background (yeah like Quake...)
	if (!WipeInAction)
		V_DrawFadeScreen();

	if (currentMenu->drawroutine)
		currentMenu->drawroutine(); // call current menu Draw routine
}

//
// M_StartControlPanel
//
void M_StartControlPanel(void)
{
	// intro might call this repeatedly
	if (menuactive)
	{
		CON_ToggleOff(); // move away console
		return;
	}

	menuactive = true;

	if (!Playing())
	{
		MainMenu[secrets].status = (grade > 0) ? (IT_STRING | IT_CALL) : (IT_DISABLED);

		if (externmodloaded)
			MainMenu[secrets].itemaction = M_CustomSecretsMenu;
		else
			MainMenu[secrets].itemaction = M_SecretsMenu;

		currentMenu = &MainDef;
		itemOn = singleplr;
	}
	else if (!(netgame || multiplayer)) // Single Player
	{
		if (gamestate != GS_LEVEL || ultimatemode) // intermission, so gray out stuff.
		{
			SPauseMenu[spause_pandora].status = (grade & 8) ? (IT_GRAYEDOUT) : (IT_DISABLED);
			SPauseMenu[spause_retry].status = IT_GRAYEDOUT;
		}
		else
		{
			SPauseMenu[spause_pandora].status = (grade & 8) ? (IT_STRING | IT_CALL) : (IT_DISABLED);

			// The list of things that can disable retrying is a little too complex for me to want to use
			// the short if statement syntax
			if (!(&players[consoleplayer] && players[consoleplayer].lives > 1) || G_IsSpecialStage(gamemap))
				SPauseMenu[spause_retry].status = (IT_GRAYEDOUT);
			else
				SPauseMenu[spause_retry].status = (IT_STRING | IT_CALL);
		}

		currentMenu = &SPauseDef;
		itemOn = spause_continue;
	}
	else // multiplayer
	{
		MPauseMenu[mpause_switchmap].status = IT_DISABLED;
		MPauseMenu[mpause_scramble].status = IT_DISABLED;
		MPauseMenu[mpause_psetupsplit].status = IT_DISABLED;
		MPauseMenu[mpause_psetupsplit2].status = IT_DISABLED;
		MPauseMenu[mpause_spectate].status = IT_DISABLED;
		MPauseMenu[mpause_entergame].status = IT_DISABLED;
		MPauseMenu[mpause_switchteam].status = IT_DISABLED;
 		MPauseMenu[mpause_psetup].status = IT_DISABLED;

		if ((server || adminplayer == consoleplayer))
		{
			MPauseMenu[mpause_switchmap].status = IT_STRING | IT_CALL;
			if((gametype == GT_MATCH && cv_matchtype.value) || gametype == GT_CTF)
				MPauseMenu[mpause_scramble].status = IT_STRING | IT_SUBMENU;
		}

		if (splitscreen)
			MPauseMenu[mpause_psetupsplit].status = MPauseMenu[mpause_psetupsplit2].status = IT_STRING | IT_CALL;
		else
		{
 			MPauseMenu[mpause_psetup].status = IT_STRING | IT_CALL;

			if ((gametype == GT_MATCH && cv_matchtype.value) || gametype == GT_CTF)
				MPauseMenu[mpause_switchteam].status = IT_STRING | IT_SUBMENU;
			else if ((gametype == GT_MATCH || gametype == GT_TAG || gametype == GT_CTF || gametype == GT_SHARDS) && !splitscreen)
				MPauseMenu[((&players[consoleplayer] && players[consoleplayer].spectator) ? mpause_entergame : mpause_spectate)].status = IT_STRING | IT_CALL;
			else // in this odd case, we still want something to be on the menu even if it's useless
				MPauseMenu[mpause_spectate].status = IT_GRAYEDOUT;
		}

		currentMenu = &MPauseDef;
		itemOn = mpause_continue;
	}

	CON_ToggleOff(); // move away console

	if (timeattacking) // Cancel recording
	{
		G_CheckDemoStatus();

		if (gamestate == GS_LEVEL || gamestate == GS_INTERMISSION)
			Command_ExitGame_f();

		currentMenu = &SP_TimeAttackDef;
		itemOn = currentMenu->lastOn;
		timeattacking = false;
		G_SetGamestate(GS_TIMEATTACK);
		S_ChangeMusic(mus_racent, true);
		CV_AddValue(&cv_nextmap, 1);
		CV_AddValue(&cv_nextmap, -1);
		return;
	}
	if (newmoon)
		S_StartSound(NULL, sfx_iy_menu_enter);
}

//
// M_ClearMenus
//
void M_ClearMenus(boolean callexitmenufunc)
{
	if (!menuactive)
		return;

	if (currentMenu->quitroutine && callexitmenufunc && !currentMenu->quitroutine())
		return; // we can't quit this menu (also used to set parameter from the menu)

#ifndef DC // Save the config file. I'm sick of crashing the game later and losing all my changes!
	COM_BufAddText(va("saveconfig \"%s\" -silent\n", configfile));
#endif //Alam: But not on the Dreamcast's VMUs

	if (currentMenu == &MessageDef) // Oh sod off!
		currentMenu = &MainDef; // Not like it matters
	menuactive = false;
}

//
// M_SetupNextMenu
//
void M_SetupNextMenu(menu_t *menudef)
{
	INT32 i;

	if (currentMenu->quitroutine)
	{
		if (!currentMenu->quitroutine())
			return; // we can't quit this menu (also used to set parameter from the menu)
	}
	currentMenu = menudef;
	itemOn = currentMenu->lastOn;

	// in case of...
	if (itemOn >= currentMenu->numitems)
		itemOn = (INT16)(currentMenu->numitems - 1);

	// the curent item can be disabled,
	// this code go up until an enabled item found
	if ((currentMenu->menuitems[itemOn].status & IT_TYPE) == IT_SPACE)
	{
		for (i = 0; i < currentMenu->numitems; i++)
		{
			if ((currentMenu->menuitems[i].status & IT_TYPE) != IT_SPACE)
			{
				itemOn = (INT16)i;
				break;
			}
		}
	}
}

//
// M_Ticker
//
void M_Ticker(void)
{
	if (dedicated)
		return;

	if (--skullAnimCounter <= 0)
		skullAnimCounter = 8 * NEWTICRATERATIO;

	//added : 30-01-98 : test mode for five seconds
	if (vidm_testingmode > 0)
	{
		// restore the previous video mode
		if (--vidm_testingmode == 0)
			setmodeneeded = vidm_previousmode + 1;
	}
}

//
// M_Init
//
void M_Init(void)
{
	CV_RegisterVar(&cv_nextmap);
	CV_RegisterVar(&cv_newgametype);
	CV_RegisterVar(&cv_chooseskin);

	if (dedicated)
		return;

#ifdef HWRENDER
	// Permanently hide some options based on render mode
	if (rendermode == render_soft)
		OP_VideoOptionsMenu[1].status = IT_DISABLED;
#endif

#ifndef NONET
	CV_RegisterVar(&cv_serversearch);
	CV_RegisterVar(&cv_serversort);
	CV_RegisterVar(&cv_chooseroom);
#endif

	//todo put this somewhere better...
	CV_RegisterVar(&cv_allcaps);
}

// ==========================================================================
// SPECIAL MENU OPTION DRAW ROUTINES GO HERE
// ==========================================================================

// Converts a string into question marks.
// Used for the secrets menu, to hide yet-to-be-unlocked stuff.
static const char *M_CreateSecretMenuOption(const char *str)
{
	static char qbuf[32];
	int i;

	for (i = 0; i < 31; ++i)
	{
		if (!str[i])
		{
			qbuf[i] = '\0';
			return qbuf;
		}
		else if (str[i] != ' ')
			qbuf[i] = '?';
		else
			qbuf[i] = ' ';
	}

	qbuf[31] = '\0';
	return qbuf;
}

static void M_DrawThermo(INT32 x, INT32 y, consvar_t *cv)
{
	INT32 xx = x, i;
	lumpnum_t leftlump, rightlump, centerlump[2], cursorlump;
	patch_t *p;

	leftlump = W_GetNumForName("M_THERML");
	rightlump = W_GetNumForName("M_THERMR");
	centerlump[0] = W_GetNumForName("M_THERMM");
	centerlump[1] = W_GetNumForName("M_THERMM");
	cursorlump = W_GetNumForName("M_THERMO");

	V_DrawScaledPatch(xx, y, 0, p = W_CachePatchNum(leftlump,PU_CACHE));
	xx += SHORT(p->width) - SHORT(p->leftoffset);
	for (i = 0; i < 16; i++)
	{
		V_DrawScaledPatch(xx, y, V_WRAPX, W_CachePatchNum(centerlump[i & 1], PU_CACHE));
		xx += 8;
	}
	V_DrawScaledPatch(xx, y, 0, W_CachePatchNum(rightlump, PU_CACHE));

	xx = (cv->value - cv->PossibleValue[0].value) * (15*8) /
		(cv->PossibleValue[1].value - cv->PossibleValue[0].value);

	V_DrawScaledPatch((x + 8) + xx, y, 0, W_CachePatchNum(cursorlump, PU_CACHE));
}

//  A smaller 'Thermo', with range given as percents (0-100)
static void M_DrawSlider(INT32 x, INT32 y, const consvar_t *cv)
{
	INT32 i;
	INT32 range;
	patch_t *p;

	for (i = 0; cv->PossibleValue[i+1].strvalue; i++);

	range = ((cv->value - cv->PossibleValue[0].value) * 100 /
	 (cv->PossibleValue[i].value - cv->PossibleValue[0].value));

	if (range < 0)
		range = 0;
	if (range > 100)
		range = 100;

	x = BASEVIDWIDTH - x - SLIDER_WIDTH;

	V_DrawScaledPatch(x - 8, y, 0, W_CachePatchName("M_SLIDEL", PU_CACHE));

	p =  W_CachePatchName("M_SLIDEM", PU_CACHE);
	for (i = 0; i < SLIDER_RANGE; i++)
		V_DrawScaledPatch (x+i*8, y, 0,p);

	p = W_CachePatchName("M_SLIDER", PU_CACHE);
	V_DrawScaledPatch(x+SLIDER_RANGE*8, y, 0, p);

	// draw the slider cursor
	p = W_CachePatchName("M_SLIDEC", PU_CACHE);
	V_DrawMappedPatch(x + ((SLIDER_RANGE-1)*8*range)/100, y, 0, p, yellowmap);
}

//
//  Draw a textbox, like Quake does, because sometimes it's difficult
//  to read the text with all the stuff in the background...
//
void M_DrawTextBox(INT32 x, INT32 y, INT32 width, INT32 boxlines, INT32 flags)
{
	patch_t *p;
	INT32 cx, cy, n;
	INT32 step, boff;

	step = 8;
	boff = 8;

	// draw left side
	cx = x;
	cy = y;
	V_DrawScaledPatch(cx, cy, flags, W_CachePatchNum(viewborderlump[BRDR_TL], PU_CACHE));
	cy += boff;
	p = W_CachePatchNum(viewborderlump[BRDR_L], PU_CACHE);
	for (n = 0; n < boxlines; n++)
	{
		V_DrawScaledPatch(cx, cy, V_WRAPY|flags, p);
		cy += step;
	}
	V_DrawScaledPatch(cx, cy, flags, W_CachePatchNum(viewborderlump[BRDR_BL], PU_CACHE));

	// draw middle
	V_DrawFlatFill(x + boff, y + boff, width*step, boxlines*step, st_borderpatchnum, flags);

	cx += boff;
	cy = y;
	while (width > 0)
	{
		V_DrawScaledPatch(cx, cy, V_WRAPX|flags, W_CachePatchNum(viewborderlump[BRDR_T], PU_CACHE));
		V_DrawScaledPatch(cx, y + boff + boxlines*step, V_WRAPX|flags, W_CachePatchNum(viewborderlump[BRDR_B], PU_CACHE));
		width--;
		cx += step;
	}

	// draw right side
	cy = y;
	V_DrawScaledPatch(cx, cy, flags, W_CachePatchNum(viewborderlump[BRDR_TR], PU_CACHE));
	cy += boff;
	p = W_CachePatchNum(viewborderlump[BRDR_R], PU_CACHE);
	for (n = 0; n < boxlines; n++)
	{
		V_DrawScaledPatch(cx, cy, V_WRAPY|flags, p);
		cy += step;
	}
	V_DrawScaledPatch(cx, cy, flags, W_CachePatchNum(viewborderlump[BRDR_BR], PU_CACHE));
}

//
// Draw border for the savegame description
//
static void M_DrawSaveLoadBorder(INT32 x,INT32 y)
{
	INT32 i;

	V_DrawScaledPatch (x-8,y+7,0,W_CachePatchName("M_LSLEFT",PU_CACHE));

	for (i = 0;i < 24;i++)
	{
		V_DrawScaledPatch (x,y+7,0,W_CachePatchName("M_LSCNTR",PU_CACHE));
		x += 8;
	}

	V_DrawScaledPatch (x,y+7,0,W_CachePatchName("M_LSRGHT",PU_CACHE));
}

// horizontally centered text
static void M_CentreText(INT32 y, const char *string)
{
	INT32 x;
	//added : 02-02-98 : centre on 320, because V_DrawString centers on vid.width...
	x = (BASEVIDWIDTH - V_StringWidth(string))>>1;
	V_DrawString(x,y,0,string);
}

static void M_DrawMenuTitle(void)
{
	if (currentMenu->menutitlepic)
	{
		patch_t *p = W_CachePatchName(currentMenu->menutitlepic, PU_CACHE);

		INT32 xtitle = (BASEVIDWIDTH - SHORT(p->width))/2;
		INT32 ytitle = (30 - SHORT(p->height))/2;

		if (xtitle < 0)
			xtitle = 0;
		if (ytitle < 0)
			ytitle = 0;
		V_DrawScaledPatch(xtitle, ytitle, 0, p);
	}
}

static void M_DrawGenericMenu(void)
{
	INT32 x, y, i, cursory = 0;

	// DRAW MENU
	x = currentMenu->x;
	y = currentMenu->y;

	// draw title (or big pic)
	M_DrawMenuTitle();

	for (i = 0; i < currentMenu->numitems; i++)
	{
		if (i == itemOn)
			cursory = y;
		switch (currentMenu->menuitems[i].status & IT_DISPLAY)
		{
			case IT_PATCH:
				if (currentMenu->menuitems[i].patch && currentMenu->menuitems[i].patch[0])
				{
					if (currentMenu->menuitems[i].status & IT_CENTER)
					{
						patch_t *p;
						p = W_CachePatchName(currentMenu->menuitems[i].patch, PU_CACHE);
						V_DrawScaledPatch((BASEVIDWIDTH - SHORT(p->width))/2, y, 0, p);
					}
					else
					{
						V_DrawScaledPatch(x, y, 0,
							W_CachePatchName(currentMenu->menuitems[i].patch, PU_CACHE));
					}
				}
			case IT_NOTHING:
			case IT_DYBIGSPACE:
				y += LINEHEIGHT;
				break;
			case IT_BIGSLIDER:
				M_DrawThermo(x, y, (consvar_t *)currentMenu->menuitems[i].itemaction);
				y += LINEHEIGHT;
				break;
			case IT_STRING:
			case IT_WHITESTRING:
				if (currentMenu->menuitems[i].alphaKey)
					y = currentMenu->y+currentMenu->menuitems[i].alphaKey;
				if (i == itemOn)
					cursory = y;

				if ((currentMenu->menuitems[i].status & IT_DISPLAY)==IT_STRING)
					V_DrawString(x, y, 0, currentMenu->menuitems[i].text);
				else
					V_DrawString(x, y, V_YELLOWMAP, currentMenu->menuitems[i].text);

				// Cvar specific handling
				switch (currentMenu->menuitems[i].status & IT_TYPE)
					case IT_CVAR:
					{
						consvar_t *cv = (consvar_t *)currentMenu->menuitems[i].itemaction;
						switch (currentMenu->menuitems[i].status & IT_CVARTYPE)
						{
							case IT_CV_SLIDER:
								M_DrawSlider(x, y, cv);
							case IT_CV_NOPRINT: // color use this
								break;
							case IT_CV_STRING:
								M_DrawTextBox(x, y + 4, MAXSTRINGLENGTH, 1, 0);
								V_DrawString(x + 8, y + 12, V_ALLOWLOWERCASE, cv->string);
								if (skullAnimCounter < 4 && i == itemOn)
									V_DrawCharacter(x + 8 + V_StringWidth(cv->string), y + 12,
										'_' | 0x80, false);
								y += 16;
								break;
							default:
								V_DrawString(BASEVIDWIDTH - x - V_StringWidth(cv->string), y,
									V_YELLOWMAP, cv->string);
								break;
						}
						break;
					}
					y += STRINGHEIGHT;
					break;
			case IT_STRING2:
				V_DrawString(x, y, 0, currentMenu->menuitems[i].text);
			case IT_DYLITLSPACE:
				y += SMALLLINEHEIGHT;
				break;
			case IT_GRAYPATCH:
				if (currentMenu->menuitems[i].patch && currentMenu->menuitems[i].patch[0])
					V_DrawMappedPatch(x, y, 0,
						W_CachePatchName(currentMenu->menuitems[i].patch,PU_CACHE), graymap);
				y += LINEHEIGHT;
				break;
			case IT_TRANSTEXT:
				if (currentMenu->menuitems[i].alphaKey)
					y = currentMenu->y+currentMenu->menuitems[i].alphaKey;
			case IT_TRANSTEXT2:
				V_DrawString(x, y, V_TRANSLUCENT, currentMenu->menuitems[i].text);
				y += SMALLLINEHEIGHT;
				break;
			case IT_QUESTIONMARKS:
				if (currentMenu->menuitems[i].alphaKey)
					y = currentMenu->y+currentMenu->menuitems[i].alphaKey;

				V_DrawString(x, y, V_TRANSLUCENT, M_CreateSecretMenuOption(currentMenu->menuitems[i].text));
				y += SMALLLINEHEIGHT;
				break;
			case IT_HEADERTEXT: // draws 16 pixels to the left, in yellow text
				if (currentMenu->menuitems[i].alphaKey)
					y = currentMenu->y+currentMenu->menuitems[i].alphaKey;

				V_DrawString(x-16, y, V_YELLOWMAP, currentMenu->menuitems[i].text);
				y += SMALLLINEHEIGHT;
				break;
		}
	}

	// DRAW THE SKULL CURSOR
	if (((currentMenu->menuitems[itemOn].status & IT_DISPLAY) == IT_PATCH)
		|| ((currentMenu->menuitems[itemOn].status & IT_DISPLAY) == IT_NOTHING))
	{
		V_DrawScaledPatch(currentMenu->x + SKULLXOFF, cursory - 5, 0,
			W_CachePatchName("M_CURSOR", PU_CACHE));
	}
	else
	{
		V_DrawScaledPatch(currentMenu->x - 24, cursory, 0,
			W_CachePatchName("M_CURSOR", PU_CACHE));
		V_DrawString(currentMenu->x, cursory, V_YELLOWMAP, currentMenu->menuitems[itemOn].text);
	}
}

static void M_DrawPauseMenu(void)
{
	char names[8], hours[4], minutes[4], seconds[4];
	INT32 j;
	emblem_t *emblem;

	if (!(netgame || multiplayer || G_IsSpecialStage(gamemap)))
	{
		M_DrawTextBox(27, 16, 32, 4, 0);

		sprintf(names, "%c %c %c", skins[0].name[0], skins[1].name[0], skins[2].name[0]);
		V_DrawString(250, 24, 0, names);

		for (j = 0;j < 3;j++)
		{
			emblem = M_GetLevelEmblem(gamemap, j);
			if (emblem)
				V_DrawScaledPatch(248 + (j * 12), 32, 0, W_CachePatchName((emblem->collected) ? "GOTIT" : "NEEDIT", PU_CACHE));
		}

		if (mapheaderinfo[gamemap-1].actnum != 0)
			V_DrawString(40, 32, V_YELLOWMAP, va("%s %d", mapheaderinfo[gamemap-1].lvlttl, mapheaderinfo[gamemap-1].actnum));
		else
			V_DrawString(40, 32, V_YELLOWMAP, mapheaderinfo[gamemap-1].lvlttl);

		if (timedata[gamemap-1].time)
		{
			sprintf(minutes, "%02i", G_TicsToMinutes(timedata[gamemap-1].time, true));
			sprintf(seconds, "%02i", G_TicsToSeconds(timedata[gamemap-1].time));
			sprintf(hours, "%02i", G_TicsToCentiseconds(timedata[gamemap-1].time));

			V_DrawString(40, 40, V_YELLOWMAP, "Best Time Attack:");
			V_DrawRightAlignedString(284, 40, 0, va("%s:%s:%s", minutes,seconds,hours));
		}
	}
	
	M_DrawGenericMenu();
}

static void M_DrawCenteredMenu(void)
{
	INT32 x, y, i, cursory = 0;

	// DRAW MENU
	x = currentMenu->x;
	y = currentMenu->y;

	// draw title (or big pic)
	M_DrawMenuTitle();

	for (i = 0; i < currentMenu->numitems; i++)
	{
		if (i == itemOn)
			cursory = y;
		switch (currentMenu->menuitems[i].status & IT_DISPLAY)
		{
			case IT_PATCH:
				if (currentMenu->menuitems[i].patch && currentMenu->menuitems[i].patch[0])
				{
					if (currentMenu->menuitems[i].status & IT_CENTER)
					{
						patch_t *p;
						p = W_CachePatchName(currentMenu->menuitems[i].patch, PU_CACHE);
						V_DrawScaledPatch((BASEVIDWIDTH - SHORT(p->width))/2, y, 0, p);
					}
					else
					{
						V_DrawScaledPatch(x, y, 0,
							W_CachePatchName(currentMenu->menuitems[i].patch, PU_CACHE));
					}
				}
			case IT_NOTHING:
			case IT_DYBIGSPACE:
				y += LINEHEIGHT;
				break;
			case IT_BIGSLIDER:
				M_DrawThermo(x, y, (consvar_t *)currentMenu->menuitems[i].itemaction);
				y += LINEHEIGHT;
				break;
			case IT_STRING:
			case IT_WHITESTRING:
				if (currentMenu->menuitems[i].alphaKey)
					y = currentMenu->y+currentMenu->menuitems[i].alphaKey;
				if (i == itemOn)
					cursory = y;

				if ((currentMenu->menuitems[i].status & IT_DISPLAY)==IT_STRING)
					V_DrawCenteredString(x, y, 0, currentMenu->menuitems[i].text);
				else
					V_DrawCenteredString(x, y, V_YELLOWMAP, currentMenu->menuitems[i].text);

				// Cvar specific handling
				switch(currentMenu->menuitems[i].status & IT_TYPE)
					case IT_CVAR:
					{
						consvar_t *cv = (consvar_t *)currentMenu->menuitems[i].itemaction;
						switch(currentMenu->menuitems[i].status & IT_CVARTYPE)
						{
							case IT_CV_SLIDER:
								M_DrawSlider(x, y, cv);
							case IT_CV_NOPRINT: // color use this
								break;
							case IT_CV_STRING:
								M_DrawTextBox(x, y + 4, MAXSTRINGLENGTH, 1, 0);
								V_DrawString(x + 8, y + 12, V_ALLOWLOWERCASE, cv->string);
								if (skullAnimCounter < 4 && i == itemOn)
									V_DrawCharacter(x + 8 + V_StringWidth(cv->string), y + 12,
										'_' | 0x80, false);
								y += 16;
								break;
							default:
								V_DrawString(BASEVIDWIDTH - x - V_StringWidth(cv->string), y,
									V_YELLOWMAP, cv->string);
								break;
						}
						break;
					}
					y += STRINGHEIGHT;
					break;
			case IT_STRING2:
				V_DrawCenteredString(x, y, 0, currentMenu->menuitems[i].text);
			case IT_DYLITLSPACE:
				y += SMALLLINEHEIGHT;
				break;
			case IT_GRAYPATCH:
				if (currentMenu->menuitems[i].patch && currentMenu->menuitems[i].patch[0])
					V_DrawMappedPatch(x, y, 0,
						W_CachePatchName(currentMenu->menuitems[i].patch,PU_CACHE), graymap);
				y += LINEHEIGHT;
				break;
		}
	}

	// DRAW THE SKULL CURSOR
	if (((currentMenu->menuitems[itemOn].status & IT_DISPLAY) == IT_PATCH)
		|| ((currentMenu->menuitems[itemOn].status & IT_DISPLAY) == IT_NOTHING))
	{
		V_DrawScaledPatch(x + SKULLXOFF, cursory - 5, 0,
			W_CachePatchName("M_CURSOR", PU_CACHE));
	}
	else
	{
		V_DrawScaledPatch(x - V_StringWidth(currentMenu->menuitems[itemOn].text)/2 - 24, cursory, 0,
			W_CachePatchName("M_CURSOR", PU_CACHE));
		V_DrawCenteredString(x, cursory, V_YELLOWMAP, currentMenu->menuitems[itemOn].text);
	}
}

//
// M_StringHeight
//
// Find string height from hu_font chars
//
static inline size_t M_StringHeight(const char *string)
{
	size_t h = 8, i;

	for (i = 0; i < strlen(string); i++)
		if (string[i] == '\n')
			h += 8;

	return h;
}

// ==========================================================================
// Extraneous menu patching functions
// ==========================================================================

//
// M_PatchSkinNameTable
//
// Like M_PatchLevelNameTable, but for cv_chooseskin
//
static void M_PatchSkinNameTable(void)
{
	INT32 j;

	memset(skins_cons_t, 0, sizeof (skins_cons_t));

	for (j = 0; j < MAXSKINS; j++)
	{
		if (skins[j].name[0] != '\0')
		{
			skins_cons_t[j].strvalue = skins[j].name;
			skins_cons_t[j].value = j+1;
		}
		else
		{
			skins_cons_t[j].strvalue = NULL;
			skins_cons_t[j].value = 0;
		}
	}

	CV_SetValue(&cv_chooseskin, cv_chooseskin.value); // This causes crash sometimes?!

	CV_SetValue(&cv_chooseskin, 1);
	CV_AddValue(&cv_chooseskin, -1);
	CV_AddValue(&cv_chooseskin, 1);

	return;
}

//
// M_PatchLevelNameTable
//
// Populates the cv_nextmap variable
//
// Modes:
// 0 = Create Server Menu
// 1 = Level Select Menu
// 2 = Time Attack Menu
// 3 = SRB1 Level Select Menu
//
static boolean M_PatchLevelNameTable(INT32 mode)
{
	size_t i;
	INT32 j;
	INT32 currentmap;
	boolean foundone = false;

	for (j = 0; j < LEVELARRAYSIZE-2; j++)
	{
		i = 0;
		currentmap = map_cons_t[j].value-1;

		if (mapheaderinfo[currentmap].lvlttl[0] && ((mode == 0 && !mapheaderinfo[currentmap].hideinmenu && !((mapheaderinfo[currentmap].typeoflevel & TOL_SRB1) && !(grade & 2))) || (mode == 1 && mapheaderinfo[currentmap].levelselect && !(mapheaderinfo[currentmap].typeoflevel & TOL_SRB1)) || (mode == 2 && mapheaderinfo[currentmap].timeattack && mapvisited[currentmap]) || (mode == 3 && mapheaderinfo[currentmap].levelselect && (mapheaderinfo[currentmap].typeoflevel & TOL_SRB1))))
		{
			strlcpy(lvltable[j], mapheaderinfo[currentmap].lvlttl, sizeof (lvltable[j]));

			i += strlen(mapheaderinfo[currentmap].lvlttl);

			if (!mapheaderinfo[currentmap].nozone)
			{
				lvltable[j][i++] = ' ';
				lvltable[j][i++] = 'Z';
				lvltable[j][i++] = 'O';
				lvltable[j][i++] = 'N';
				lvltable[j][i++] = 'E';
			}

			if (mapheaderinfo[currentmap].actnum)
			{
				char actnum[3];
				INT32 g;

				lvltable[j][i++] = ' ';

				sprintf(actnum, "%d", mapheaderinfo[currentmap].actnum);

				for (g = 0; g < 3; g++)
				{
					if (actnum[g] == '\0')
						break;

					lvltable[j][i++] = actnum[g];
				}
			}

			lvltable[j][i++] = '\0';
			foundone = true;
		}
		else
			lvltable[j][0] = '\0';

		if (lvltable[j][0] == '\0')
			map_cons_t[j].strvalue = NULL;
		else
			map_cons_t[j].strvalue = lvltable[j];
	}

	if (!foundone)
		return false;

	CV_SetValue(&cv_nextmap, cv_nextmap.value); // This causes crash sometimes?!

	if (mode > 0)
	{
		INT32 value = 0;

		switch (cv_newgametype.value)
		{
			case GT_COOP:
				value = TOL_COOP;
				break;
			case GT_RACE:
				value = TOL_RACE;
				break;
			case GT_MATCH:
				value = TOL_MATCH;
				break;
			case GT_SHARDS:
				value = TOL_SHARDS;
				break;
			case GT_TAG:
				value = TOL_TAG;
				break;
			case GT_CTF:
				value = TOL_CTF;
				break;
		}

		CV_SetValue(&cv_nextmap, M_FindFirstMap(value));
		CV_AddValue(&cv_nextmap, -1);
		CV_AddValue(&cv_nextmap, 1);
	}
	else
		Newgametype_OnChange(); // Make sure to start on an appropriate map if wads have been added

	return true;
}

static boolean M_PatchLevelNameTableByMod(INT32 mode)
{
	size_t i;
	INT32 j;
	INT32 currentmap;
	boolean foundone = false;

	for (j = 0; j < LEVELARRAYSIZE-2; j++)
	{
		i = 0;
		currentmap = map_cons_t[j].value-1;

		if (mapheaderinfo[currentmap].lvlttl[0] && mapheaderinfo[currentmap].modlistnum == selectedmod
		 && ((mode == 0 && !mapheaderinfo[currentmap].hideinmenu && !((mapheaderinfo[currentmap].typeoflevel & TOL_SRB1) && !(grade & 2)))
		  || (mode == 1 && mapheaderinfo[currentmap].levelselect)
		  || (mode == 2 && mapheaderinfo[currentmap].timeattack && mapvisited[currentmap])))
		{
			strlcpy(lvltable[j], mapheaderinfo[currentmap].lvlttl, sizeof (lvltable[j]));

			i += strlen(mapheaderinfo[currentmap].lvlttl);

			if (!mapheaderinfo[currentmap].nozone)
			{
				lvltable[j][i++] = ' ';
				lvltable[j][i++] = 'Z';
				lvltable[j][i++] = 'O';
				lvltable[j][i++] = 'N';
				lvltable[j][i++] = 'E';
			}

			if (mapheaderinfo[currentmap].actnum)
			{
				char actnum[3];
				INT32 g;

				lvltable[j][i++] = ' ';

				sprintf(actnum, "%d", mapheaderinfo[currentmap].actnum);

				for (g = 0; g < 3; g++)
				{
					if (actnum[g] == '\0')
						break;

					lvltable[j][i++] = actnum[g];
				}
			}

			lvltable[j][i++] = '\0';
			foundone = true;
		}
		else
			lvltable[j][0] = '\0';

		if (lvltable[j][0] == '\0')
			map_cons_t[j].strvalue = NULL;
		else
			map_cons_t[j].strvalue = lvltable[j];
	}

	if (!foundone)
		return false;

	CV_SetValue(&cv_nextmap, cv_nextmap.value); // This causes crash sometimes?!

	Newgametype_OnChange(); // Make sure to start on an appropriate map if wads have been added

	return true;
}

//
// M_PatchRoomsTable
//
// Like M_PatchSkinNameTable, but for cv_chooseroom.
//
#ifndef NONET
static int M_PatchRoomsTable(boolean hosting)
{
	INT32 i = -1;
	memset(rooms_cons_t, 0, sizeof (rooms_cons_t));

	if(GetRoomsList(hosting) < 0)
	{
		return false;
	}

	for (i = 0; room_list[i].header.buffer[0]; i++)
	{
		if(room_list[i].name != '\0')
		{
			rooms_cons_t[i].strvalue = room_list[i].name;
			rooms_cons_t[i].value = room_list[i].id;
		}
		else
		{
			rooms_cons_t[i].strvalue = NULL;
			rooms_cons_t[i].value = 0;
		}
	}

	CV_SetValue(&cv_chooseroom, rooms_cons_t[0].value);
	CV_AddValue(&cv_chooseroom, 1);
	CV_AddValue(&cv_chooseroom, -1);

	return true;
}
#endif

static void M_DoMenuPatching(boolean islevelselect)
{
	if (islevelselect) // level select mode
	{
		if (!M_PatchLevelNameTableByMod(1))
		{
			SR_LevelSelectMenu[1].status = IT_DISABLED;
			SR_LevelSelectMenu[2].status = IT_DISABLED;
			listHasNoMaps = true;
		}
		else
		{
			SR_LevelSelectMenu[1].status = IT_STRING|IT_CVAR;
			SR_LevelSelectMenu[2].status = IT_WHITESTRING|IT_CALL;
			CV_SetValue(&cv_nextmap, spstage_start);
			CV_AddValue(&cv_nextmap, -1);
			CV_AddValue(&cv_nextmap, 1);
			listHasNoMaps = false;
		}
	}
	else // time attack mode
	{
		if (!M_PatchLevelNameTableByMod(2))
		{
			SP_TimeAttackMenu[taplayer].status = IT_DISABLED;
			SP_TimeAttackMenu[talevel].status = IT_DISABLED;
			SP_TimeAttackMenu[tareplay].status = IT_DISABLED;
			SP_TimeAttackMenu[tastart].status = IT_DISABLED;
			SP_TimeAttackMenu[tastartrecord].status = IT_DISABLED;
			listHasNoMaps = true;
		}
		else
		{
			SP_TimeAttackMenu[taplayer].status = IT_STRING|IT_CVAR;
			SP_TimeAttackMenu[talevel].status = IT_STRING|IT_CVAR;
			SP_TimeAttackMenu[tastart].status = IT_WHITESTRING|IT_CALL;
			SP_TimeAttackMenu[tastartrecord].status = IT_WHITESTRING|IT_CALL;
			CV_SetValue(&cv_nextmap, spstage_start);
			CV_AddValue(&cv_nextmap, -1);
			CV_AddValue(&cv_nextmap, 1);
			listHasNoMaps = false;
		}
	}
}

static void M_HandleLevelPack(INT32 choice)
{
	boolean exitmenu = false; // exit to previous menu

	switch (choice)
	{
		case KEY_DOWNARROW:
			S_StartSound(NULL, sfx_menu1);
			if (!listHasNoMaps)
				itemOn++;
			break;

		case KEY_UPARROW:
			S_StartSound(NULL, sfx_menu1);
			if (!listHasNoMaps)
			{
				if (!itemOn)
					itemOn = (INT16)(currentMenu->numitems-1);
				else itemOn--;
			}
			break;

		case KEY_ESCAPE:
			exitmenu = true;
			break;

		case KEY_ENTER:
		case KEY_RIGHTARROW:
			S_StartSound(NULL, sfx_menu1);
			if (externmodloaded)
				break;

			if (selectedmod++ >= 2)
				selectedmod = 0;

			//cheap hack for the meantime until I finish MULTIMOD
			switch (selectedmod)
			{
				case 1:
					if (grade & 2)
					{
						spstage_start = 101;
						break;
					}
					else
						selectedmod = 2; //no break on purpose
				case 2: spstage_start = 928; break;
				case 0: spstage_start = 1;   break;
				default: break; //shuts some compilers up
			}

			M_DoMenuPatching((currentMenu != &SP_TimeAttackDef));
			break;

		case KEY_LEFTARROW:
			S_StartSound(NULL, sfx_menu1);
			if (externmodloaded)
				break;

			if (selectedmod-- == 0)
				selectedmod = 2;

			//cheap hack for the meantime until I finish MULTIMOD
			switch (selectedmod)
			{
				case 1:
					if (grade & 2)
					{
						spstage_start = 101;
						break;
					}
					else
						selectedmod = 0; //no break on purpose
				case 0: spstage_start = 1;   break;
				case 2: spstage_start = 928; break;
				default: break; //shuts some compilers up
			}

			M_DoMenuPatching((currentMenu != &SP_TimeAttackDef));
			break;

		default:
			break;
	}
	if (exitmenu)
	{
		if (currentMenu != &SP_TimeAttackDef)
		{
			if (currentMenu->prevMenu)
				M_SetupNextMenu(currentMenu->prevMenu);
			else
				M_ClearMenus(true);

			return;
		}
		// Fade to black first
		if (rendermode != render_none)
		{
			V_DrawFill(0, 0, vid.width, vid.height, 31);
			F_WipeEndScreen(0, 0, vid.width, vid.height);

			F_RunWipe(0, false);
		}
		menuactive = false;
		D_StartTitle();
	}
}

// ==================================================
// MESSAGE BOX (aka: a hacked, cobbled together menu)
// ==================================================
static void M_DrawMessageMenu(void);

// Because this is just a hack-ish 'menu', I'm not putting this with the others
static menuitem_t MessageMenu[] =
{
	// TO HACK
	{0,NULL, NULL, NULL,0}
};

menu_t MessageDef =
{
	NULL,               // title
	1,                  // # of menu items
	NULL,               // previous menu       (TO HACK)
	MessageMenu,        // menuitem_t ->
	M_DrawMessageMenu,  // drawing routine ->
	0, 0,               // x, y                (TO HACK)
	0,                  // lastOn, flags       (TO HACK)
	NULL
};


void M_StartMessage(const char *string, void *routine,
	menumessagetype_t itemtype)
{
	size_t max = 0, start = 0, i, strlines;
	static char *message = NULL;
	Z_Free(message);
	message = Z_StrDup(string);
	DEBFILE(message);

	M_StartControlPanel(); // can't put menuactive to true

	if (currentMenu == &MessageDef) // Prevent recursion
		MessageDef.prevMenu = &MainDef;
	else
		MessageDef.prevMenu = currentMenu;

	MessageDef.menuitems[0].text     = message;
	MessageDef.menuitems[0].alphaKey = (UINT8)itemtype;
	if (!routine && itemtype != MM_NOTHING) itemtype = MM_NOTHING;
	switch (itemtype)
	{
		case MM_NOTHING:
			MessageDef.menuitems[0].status     = IT_MSGHANDLER;
			MessageDef.menuitems[0].itemaction = M_StopMessage;
			break;
		case MM_YESNO:
			MessageDef.menuitems[0].status     = IT_MSGHANDLER;
			MessageDef.menuitems[0].itemaction = routine;
			break;
		case MM_EVENTHANDLER:
			MessageDef.menuitems[0].status     = IT_MSGHANDLER;
			MessageDef.menuitems[0].itemaction = routine;
			break;
	}
	//added : 06-02-98: now draw a textbox around the message
	// compute lenght max and the numbers of lines
	for (strlines = 0; *(message+start); strlines++)
	{
		for (i = 0;i < strlen(message+start);i++)
		{
			if (*(message+start+i) == '\n')
			{
				if (i > max)
					max = i;
				start += i;
				i = (size_t)-1; //added : 07-02-98 : damned!
				start++;
				break;
			}
		}

		if (i == strlen(message+start))
			start += i;
	}

	MessageDef.x = (INT16)((BASEVIDWIDTH  - 8*max-16)/2);
	MessageDef.y = (INT16)((BASEVIDHEIGHT - M_StringHeight(message))/2);

	MessageDef.lastOn = (INT16)((strlines<<8)+max);

	//M_SetupNextMenu();
	currentMenu = &MessageDef;
	itemOn = 0;
}

#define MAXMSGLINELEN 256

static void M_DrawMessageMenu(void)
{
	INT32 y = currentMenu->y;
	size_t i, start = 0;
	INT16 max;
	char string[MAXMSGLINELEN];
	INT32 mlines;
	const char *msg = currentMenu->menuitems[0].text;

	mlines = currentMenu->lastOn>>8;
	max = (INT16)((UINT8)(currentMenu->lastOn & 0xFF)*8);
	M_DrawTextBox(currentMenu->x, y - 8, (max+7)>>3, mlines, 0);

	while (*(msg+start))
	{
		size_t len = strlen(msg+start);

		for (i = 0; i < len; i++)
		{
			if (*(msg+start+i) == '\n')
			{
				memset(string, 0, MAXMSGLINELEN);
				if (i >= MAXMSGLINELEN)
				{
					CONS_Printf("M_DrawMessageMenu: too long segment in %s\n", msg);
					return;
				}
				else
				{
					strncpy(string,msg+start, i);
					string[i] = '\0';
					start += i;
					i = (size_t)-1; //added : 07-02-98 : damned!
					start++;
				}
				break;
			}
		}

		if (i == strlen(msg+start))
		{
			if (i >= MAXMSGLINELEN)
			{
				CONS_Printf("M_DrawMessageMenu: too long segment in %s\n", msg);
				return;
			}
			else
			{
				strcpy(string, msg + start);
				start += i;
			}
		}

		V_DrawString((BASEVIDWIDTH - V_StringWidth(string))/2,y,0,string);
		y += 8; //SHORT(hu_font[0]->height);
	}
}

// default message handler
static void M_StopMessage(INT32 choice)
{
	(void)choice;
	if (menuactive)
		M_SetupNextMenu(MessageDef.prevMenu);
}

// =========
// IMAGEDEFS
// =========

// Draw an Image Def.  Aka, Help images/BULMER.
// Defines what image is used in (menuitem_t)->text.
// You can even put multiple images in one menu!
static void M_DrawImageDef(void)
{
	// Grr.  Need to autodetect for pic_ts.
	pic_t *pictest = (pic_t *)W_CachePatchName(currentMenu->menuitems[itemOn].text,PU_CACHE);
	if (!pictest->zero)
		V_DrawScaledPic(0,0,0,W_GetNumForName(currentMenu->menuitems[itemOn].text));
	else
		V_DrawScaledPatch(0,0,0,W_CachePatchName(currentMenu->menuitems[itemOn].text,PU_CACHE));

	HU_Drawer(); // BULMER secret needs this.

	if (currentMenu->numitems > 1)
		V_DrawString(0,192,V_TRANSLUCENT, va("PAGE %hd of %hd", itemOn+1, currentMenu->numitems));
}

// Handles the ImageDefs.  Just a specialized function that
// uses left and right movement.
static void M_HandleImageDef(INT32 choice)
{
	switch (choice)
	{
		case KEY_RIGHTARROW:
			S_StartSound(NULL, sfx_menu1);
			if (itemOn >= (INT16)(currentMenu->numitems-1))
				itemOn = 0;
            else itemOn++;
			break;

		case KEY_LEFTARROW:
			S_StartSound(NULL, sfx_menu1);
			if (!itemOn)
				itemOn = (INT16)(currentMenu->numitems-1);
			else itemOn--;
			break;

		case KEY_ESCAPE:
		case KEY_ENTER:
			M_ClearMenus(true);
			break;
	}
}

// ======================
// MISC MAIN MENU OPTIONS
// ======================

static void M_PandorasBox(INT32 choice)
{
	CV_StealthSetValue(&cv_dummyrings, max(players[consoleplayer].health - 1, 0));
	CV_StealthSetValue(&cv_dummylives, players[consoleplayer].lives);
	CV_StealthSetValue(&cv_dummycontinues, players[consoleplayer].continues);
	M_SetupNextMenu(&SR_PandoraDef);
}

static boolean M_ExitPandorasBox(void)
{
	if (cv_dummyrings.value != max(players[consoleplayer].health - 1, 0))
		COM_ImmedExecute(va("setrings %d", cv_dummyrings.value));
	if (cv_dummylives.value != players[consoleplayer].lives)
		COM_ImmedExecute(va("setlives %d", cv_dummylives.value));
	if (cv_dummycontinues.value != players[consoleplayer].continues)
		COM_ImmedExecute(va("setcontinues %d", cv_dummycontinues.value));
	return true;
}

static void M_ChangeLevel(INT32 choice)
{
	char mapname[6];
	(void)choice;

	strlcpy(mapname, G_BuildMapName(cv_nextmap.value), sizeof (mapname));
	strlwr(mapname);
	mapname[5] = '\0';

	M_ClearMenus(true);
	COM_BufAddText(va("map %s -gametype \"%s\"\n", mapname, cv_newgametype.string));
}

static void M_ConfirmSpectate(INT32 choice)
{
	(void)choice;
	M_ClearMenus(true);
	COM_ImmedExecute("changeteam spectator");
}

static void M_ConfirmEnterGame(INT32 choice)
{
	(void)choice;
	M_ClearMenus(true);
	COM_ImmedExecute("changeteam playing");
}

static void M_ConfirmTeamScramble(INT32 choice)
{
	(void)choice;
	M_ClearMenus(true);

	switch (cv_dummyscramble.value)
	{
		case 0:
			COM_ImmedExecute("teamscramble 1");
			break;
		case 1:
			COM_ImmedExecute("teamscramble 2");
			break;
	}
}

static void M_ConfirmTeamChange(INT32 choice)
{
	(void)choice;
	if (!cv_allowteamchange.value && cv_dummyteam.value)
	{
		M_StartMessage("The server is not allowing\n team changes at this time.\nPress a key.", NULL, MM_NOTHING);
		return;
	}

	M_ClearMenus(true);

	switch (cv_dummyteam.value)
	{
		case 0:
			COM_ImmedExecute("changeteam spectator");
			break;
		case 1:
			COM_ImmedExecute("changeteam red");
			break;
		case 2:
			COM_ImmedExecute("changeteam blue");
			break;
	}
}

static void M_Options(INT32 choice)
{
	(void)choice;

	// if the player is not admin or server, disable server options
	OP_MainMenu[5].status = (Playing() && !(server || adminplayer == consoleplayer)) ? (IT_GRAYEDOUT) : (IT_STRING|IT_SUBMENU);

	OP_MainDef.prevMenu = currentMenu;
	M_SetupNextMenu(&OP_MainDef);
}

static void M_RetryResponse(INT32 ch)
{
	if (ch != 'y' && ch != KEY_ENTER)
		return;

	if (!&players[consoleplayer] || netgame || multiplayer) // Should never happen!
		return;

	if (!golfmode && cv_resetmusic.value) // Reset Music hack?
		S_StopMusic();

	// No lives in golf.  Also no starposts, technically, either.
	if (golfmode)
		players[consoleplayer].starpostnum = 0;
	// Costs a life to retry ... unless the player in question is dead already.
	else if (players[consoleplayer].playerstate == PST_LIVE)
		players[consoleplayer].lives -= 1;

	M_ClearMenus(true);
	G_DoLoadLevel(true);
}

static void M_Retry(INT32 choice)
{
	(void)choice;
	if (golfmode)
		M_StartMessage("Retry this hole from the beginning?\nYour stroke count will not be reset!\n\n(Press 'Y' to confirm)\n",M_RetryResponse,MM_YESNO);
	else
		M_StartMessage("Retry this act from the last starpost?\n\n(Press 'Y' to confirm)\n",M_RetryResponse,MM_YESNO);
}

static void M_SelectableClearMenus(INT32 choice)
{
	(void)choice;
	M_ClearMenus(true);
}

// ======
// CHEATS
// ======

FUNCNORETURN static ATTRNORETURN void M_UltimateCheat(INT32 choice)
{
	(void)choice;
	I_Quit();
}

static void M_GetAllEmeralds(INT32 choice)
{
	(void)choice;

	emeralds = ((EMERALD7)*2)-1;
	M_StartMessage("You now have all 7 emeralds.",NULL,MM_NOTHING);
}

static void M_DestroyRobotsResponse(INT32 ch)
{
	if (ch != 'y' && ch != KEY_ENTER)
		return;

	// Destroy all robots
	P_DestroyRobots();
}

static void M_DestroyRobots(INT32 choice)
{
	(void)choice;

	M_StartMessage("Do you want to destroy all\nrobots in the current level?\n\n(Press 'Y' to confirm)\n",M_DestroyRobotsResponse,MM_YESNO);
}

static void M_LevelSelectWarp(INT32 choice)
{
	boolean fromloadgame = (currentMenu == &SP_LevelSelectDef);

	(void)choice;

	if (W_CheckNumForName(G_BuildMapName(cv_nextmap.value)) == LUMPERROR)
	{
//		CONS_Printf("\2Internal game map '%s' not found\n", G_BuildMapName(cv_nextmap.value));
		return;
	}

	if (!fromloadgame)
	{
		cursaveslot = -1;
		SP_PlayerDef.prevMenu = currentMenu;
		M_SetupNextMenu(&SP_PlayerDef);
	}

	startmap = (INT16)(cv_nextmap.value);

	fromlevelselect = true;

	if (fromloadgame)
		G_LoadGame((UINT32)cursaveslot, startmap);
}

// ========
// SKY ROOM
// ========

boolean M_GotEnoughEmblems(INT32 number)
{
	INT32 i;
	INT32 gottenemblems = 0;

	for (i = 0; i < MAXEMBLEMS; i++)
	{
		if (emblemlocations[i].collected)
			gottenemblems++;
	}

	if (gottenemblems >= number)
		return true;

	return false;
}

boolean M_GotLowEnoughTime(INT32 ptime)
{
	INT32 seconds = 0;
	INT32 i;

	for (i = 0; i < NUMMAPS; i++)
	{
		if (!(mapheaderinfo[i].timeattack))
			continue;

		if (timedata[i].time > 0)
			seconds += timedata[i].time;
		else
			seconds += 800*TICRATE;
	}

	seconds /= TICRATE;

	if (seconds <= ptime)
		return true;

	return false;
}

#define NUMCHECKLIST 9
static void M_DrawCustomChecklist(void)
{
	INT32 i;
	checklist_t checklist[NUMCHECKLIST];

	memset(checklist, 0, sizeof (checklist));

	for (i = 0; i < NUMCHECKLIST; i++)
	{
		checklist[i].unlocked = false;

		if (customsecretinfo[i].neededemblems && !M_GotEnoughEmblems(customsecretinfo[i].neededemblems))
			continue;

		if (customsecretinfo[i].neededtime && !M_GotLowEnoughTime(customsecretinfo[i].neededtime))
			continue;

		if (customsecretinfo[i].neededgrade && !(grade & customsecretinfo[i].neededgrade))
			continue;

		checklist[i].unlocked = true;
	}
	for (i = 0; i < NUMCHECKLIST; i++)
	{
		if (customsecretinfo[i].name[0] == 0)
			continue;

		V_DrawString(8, 8+(24*i), V_RETURN8, customsecretinfo[i].name);
		V_DrawString(160, 8+(24*i), V_RETURN8|V_WORDWRAP, customsecretinfo[i].objective);

		if (checklist[i].unlocked)
			V_DrawString(308, 8+(24*i), V_YELLOWMAP, "Y");
		else
			V_DrawString(308, 8+(24*i), V_YELLOWMAP, "N");
	}
}

static void M_DrawUnlockChecklist(void)
{
	checklist_t checklist[NUMCHECKLIST];
	INT32 i = 0;
	INT32 y = 8;

	checklist[i].name = "Level Select";
	checklist[i].requirement = "Find All Emblems";
	checklist[i].unlocked = (grade & 8);
	i++;

	checklist[i].name = "SRB1 Remake";
	checklist[i].requirement = "Finish 1P\nw/ Emeralds";
	checklist[i].unlocked = (grade & 2);
	i++;

	checklist[i].name = "Sonic Into Dreams";
	checklist[i].requirement = "Find 10 Emblems";
	checklist[i].unlocked = (grade & 16);
	i++;

	checklist[i].name = "Mario Koopa Blast";
	checklist[i].requirement = "Find 20 Emblems";
	checklist[i].unlocked = (grade & 4);
	i++;

	checklist[i].name = "Pandora's Box";
	checklist[i].requirement = "Find All Emblems";
	checklist[i].unlocked = (grade & 8);
	i++;

	checklist[i].name = "Extra Emblem #1";
	checklist[i].requirement = "Finish 1P";
	checklist[i].unlocked = (emblemlocations[MAXEMBLEMS-2].collected);
	i++;

	checklist[i].name = "Extra Emblem #2";
	checklist[i].requirement = "Finish 1P\nw/ Emeralds";
	checklist[i].unlocked = (emblemlocations[MAXEMBLEMS-1].collected);
	i++;

	checklist[i].name = "Extra Emblem #3";
	checklist[i].requirement = "Finish 1P in\n23 minutes";
	checklist[i].unlocked = (emblemlocations[MAXEMBLEMS-3].collected);
	i++;

	checklist[i].name = "Extra Emblem #4";
	checklist[i].requirement = "Perfect Bonus on\nany stage";
	checklist[i].unlocked = (emblemlocations[MAXEMBLEMS-4].collected);
	i++;

	for (i = 0; i < NUMCHECKLIST; i++)
	{
		V_DrawString(8, y, V_RETURN8, checklist[i].name);
		V_DrawString(160, y, V_RETURN8, checklist[i].requirement);

		if (checklist[i].unlocked)
			V_DrawString(308, y, V_YELLOWMAP, "Y");
		else
			V_DrawString(308, y, V_YELLOWMAP, "N");

		y += 20;
	}
}

static void M_Checklist(INT32 choice)
{
	(void)choice;

	if (externmodloaded)
	{
		SR_UnlockChecklistDef.prevMenu = &SR_CustomDef;
		SR_UnlockChecklistDef.drawroutine = M_DrawCustomChecklist;
		SR_UnlockChecklistMenu[0].itemaction = &SR_CustomDef;
	}
	else
	{
		SR_UnlockChecklistDef.prevMenu = &SR_MainDef;
		SR_UnlockChecklistDef.drawroutine = M_DrawUnlockChecklist;
		SR_UnlockChecklistMenu[0].itemaction = &SR_MainDef;
	}
	M_SetupNextMenu(&SR_UnlockChecklistDef);
}

static void M_DrawLevelSelectMenu(void)
{
	M_DrawGenericMenu();

	V_DrawRightAlignedString(280, 50+currentMenu->y, V_YELLOWMAP, modname[selectedmod]);

	if (listHasNoMaps)
	{
		V_DrawCenteredString(BASEVIDWIDTH/2, 120, 0, "No selectable maps found");
		V_DrawCenteredString(BASEVIDWIDTH/2, 130, 0, "for this level pack.");
		return;
	}

	if (cv_nextmap.value)
	{
		lumpnum_t lumpnum;
		patch_t *PictureOfLevel;

		//  A 160x100 image of the level as entry MAPxxP
		lumpnum = W_CheckNumForName(va("%sP", G_BuildMapName(cv_nextmap.value)));

		if (lumpnum != LUMPERROR)
			PictureOfLevel = W_CachePatchName(va("%sP", G_BuildMapName(cv_nextmap.value)), PU_CACHE);
		else
			PictureOfLevel = W_CachePatchName("BLANKLVL", PU_CACHE);

		V_DrawSmallScaledPatch(200, 110, 0, PictureOfLevel);
	}
}

static void M_LevelSelect(INT32 choice)
{
	(void)choice;
	SR_LevelSelectDef.prevMenu = currentMenu;
	inlevelselect = 1;

	M_DoMenuPatching(true);

	M_SetupNextMenu(&SR_LevelSelectDef);
}

// Entering the default secrets menu
static void M_SecretsMenu(INT32 choice)
{
	(void)choice;

	// Check grade and enable options as appropriate
	SR_MainMenu[soundtest].status   = (grade & 1)    ? (IT_STRING | IT_CVAR) : (IT_SECRET);
	SR_MainMenu[mario].status       = (grade & 4)    ? (IT_STRING | IT_CALL) : (IT_SECRET);
	SR_MainMenu[levelselect].status = (grade & 8)    ? (IT_STRING | IT_CALL) : (IT_SECRET);
	SR_MainMenu[nights].status      = (grade & 16)   ? (IT_STRING | IT_CALL) : (IT_SECRET);
	SR_MainMenu[nagz].status        = (grade & 2048) ? (IT_STRING | IT_CALL) : (IT_SECRET);

//	if (grade & 256)
//		Insert reward for beating Ultimate here!

	M_SetupNextMenu(&SR_MainDef);
}

// Entering a custom secrets menu
static void M_CustomSecretsMenu(INT32 choice)
{
	INT32 i;
	boolean unlocked;

	// Disable all the menu choices
	(void)choice;
	for (i = 1; i < 16;i++)
		SR_CustomMenu[i].status = IT_DISABLED;

	for (i = 0; i < 15; i++)
	{
		unlocked = false;

		if (customsecretinfo[i].neededemblems)
		{
			unlocked = M_GotEnoughEmblems(customsecretinfo[i].neededemblems);

			if (unlocked && customsecretinfo[i].neededtime)
				unlocked = M_GotLowEnoughTime(customsecretinfo[i].neededtime);

			if (unlocked && customsecretinfo[i].neededgrade)
				unlocked = (grade & customsecretinfo[i].neededgrade);
		}
		else if (customsecretinfo[i].neededtime)
		{
			unlocked = M_GotLowEnoughTime(customsecretinfo[i].neededtime);

			if (unlocked && customsecretinfo[i].neededgrade)
				unlocked = (grade & customsecretinfo[i].neededgrade);
		}
		else
			unlocked = (grade & customsecretinfo[i].neededgrade);

		if (unlocked)
		{
			SR_CustomMenu[1+i].status = IT_STRING|IT_CALL;

			switch (customsecretinfo[i].type)
			{
				case 0:
					SR_CustomMenu[1+i].itemaction = M_LevelSelect;
					break;
				case 1:
					SR_CustomMenu[1+i].itemaction = M_CustomWarp;
				default:
					break;
			}

			SR_CustomMenu[1+i].text = customsecretinfo[i].name;
		}
	}

	M_SetupNextMenu(&SR_CustomDef);
}

// ==================
// NEW GAME FUNCTIONS
// ==================

INT32 ultimate_selectable = false;

static void M_NewGame(void)
{
	fromlevelselect = false;

	startmap = spstage_start;
	CV_SetValue(&cv_newgametype, GT_COOP); // Graue 09-08-2004

	M_SetupChoosePlayer(0);
}

static void M_SonicGolf(INT32 choice)
{
	(void)choice;

	startmap = 1000;

	M_SetupChoosePlayer(0);
}

static void M_NightsGame(INT32 choice)
{
	(void)choice;

	startmap = 29;

	M_SetupChoosePlayer(0);
}

static void M_MarioGame(INT32 choice)
{
	(void)choice;

	startmap = 30;

	M_SetupChoosePlayer(0);
}

static void M_NAGZGame(INT32 choice)
{
	(void)choice;

	startmap = 40;

	M_SetupChoosePlayer(0);
}

static void M_CustomWarp(INT32 choice)
{
	startmap = (INT16)(customsecretinfo[choice-1].variable);

	M_SetupChoosePlayer(0);
}

// ==================
// SINGLE PLAYER MENU
// ==================

static void M_LoadGameLevelSelect(INT32 choice)
{
	(void)choice;
	inlevelselect = 1;

	//do this manually, since we disable the level pack selection here
	if (!(M_PatchLevelNameTableByMod(1)))
	{
		M_StartMessage("No selectable levels found.\n",NULL,MM_NOTHING);
		return;
	}
	else
	{
		CV_SetValue(&cv_nextmap, spstage_start);
		CV_AddValue(&cv_nextmap, -1);
		CV_AddValue(&cv_nextmap, 1);
	}

	M_SetupNextMenu(&SP_LevelSelectDef);
}

// plays buzz sound if they press enter on us.
static void M_BuzzBuzz(INT32 choice)
{
	boolean exitmenu = false; // exit to previous menu

	switch (choice)
	{
		case KEY_DOWNARROW:
			S_StartSound(NULL,sfx_menu1); // Tails
			if (itemOn+1 >= currentMenu->numitems)
				itemOn = 0;
			else itemOn++;
			break;

		case KEY_UPARROW:
			S_StartSound(NULL,sfx_menu1); // Tails
			if (!itemOn)
				itemOn = (INT16)(currentMenu->numitems-1);
			else itemOn--;
			break;

		case KEY_ESCAPE:
			exitmenu = true;
			break;

		case KEY_ENTER:
			S_StartSound(NULL, sfx_lose);
			break;
	}
	if (exitmenu)
	{
		if (currentMenu->prevMenu)
			M_SetupNextMenu(currentMenu->prevMenu);
		else
			M_ClearMenus(true);
	}
}

// ==============
// LOAD GAME MENU
// ==============

static short saveMenuLocation = 0;
static short menumovedir = 0;

static void M_DrawGameStats(void)
{
	INT32 ecks;
	saveSlotSelected = saveMenuLocation;

	ecks = SP_LoadDef.x + 24;
	M_DrawTextBox(SP_LoadDef.x-8,144, 23, 4, 0);

	if (saveSlotSelected == NOSAVESLOT) // last slot is play without saving
	{
		if (ultimate_selectable)
		{
			V_DrawCenteredString(ecks + 68, 152, 0, "\x87" "ULTIMATE MODE");
			V_DrawCenteredString(ecks + 68, 160, 0, "NO RINGS, NO ONE-UPS,");
			V_DrawCenteredString(ecks + 68, 168, 0, "NO CONTINUES, ONE LIFE,");
			V_DrawCenteredString(ecks + 68, 176, 0, "FINAL DESTINATION.");
		}
		else
		{
			V_DrawCenteredString(ecks + 68, 152, 0, "\x87" "PLAY WITHOUT SAVING");
			V_DrawCenteredString(ecks + 68, 160, 0, "THIS GAME WILL NOT BE");
			V_DrawCenteredString(ecks + 68, 168, 0, "SAVED, BUT YOU CAN STILL");
			V_DrawCenteredString(ecks + 68, 176, 0, "GET EMBLEMS AND SECRETS.");
		}
		return;
	}

	if (savegameinfo[saveSlotSelected].lives == -42) // Empty
	{
		V_DrawCenteredString(ecks + 68, 160, 0, "NO DATA");
		return;
	}

	if (savegameinfo[saveSlotSelected].lives == -666) // savegame is bad
	{
		V_DrawCenteredString(ecks + 68, 152, 0, "CORRUPT SAVE FILES");
		V_DrawCenteredString(ecks + 68, 160, 0, "CAN NOT BE LOADED.");
		V_DrawCenteredString(ecks + 68, 176, 0, "DELETE USING BACKSPACE.");
		return;
	}

	if (savegameinfo[saveSlotSelected].skincolor == 0)
		V_DrawScaledPatch ((INT32)((SP_LoadDef.x+4)*vid.fdupx),(INT32)((144+8)*vid.fdupy), V_NOSCALESTART,W_CachePatchName(skins[savegameinfo[saveSlotSelected].skinnum].faceprefix, PU_CACHE));
	else
	{
		const UINT8 *colormap = (const UINT8 *) translationtables[savegameinfo[saveSlotSelected].skinnum] - 256 + (savegameinfo[saveSlotSelected].skincolor<<8);
		V_DrawMappedPatch((INT32)((SP_LoadDef.x+4)*vid.fdupx),(INT32)((144+8)*vid.fdupy), V_NOSCALESTART,W_CachePatchName(skins[savegameinfo[saveSlotSelected].skinnum].faceprefix, PU_CACHE), colormap);
	}

	V_DrawString(ecks + 16, 152, 0, savegameinfo[saveSlotSelected].playername);

	if (savegameinfo[saveSlotSelected].gamemap & 8192)
		V_DrawString(ecks + 16, 160, 0, "\x83" "CLEAR!");
	else
		V_DrawString(ecks + 16, 160, 0, va("%s", savegameinfo[saveSlotSelected].levelname));

	V_DrawScaledPatch(ecks + 16, 168, 0, W_CachePatchName("CHAOS1", PU_CACHE));
	V_DrawString(ecks + 36, 172, 0, va("x %d", savegameinfo[saveSlotSelected].numemeralds));

	V_DrawScaledPatch(ecks + 64, 169, 0, W_CachePatchName("ONEUP", PU_CACHE));
	V_DrawString(ecks + 84, 172, 0, va("x %d", savegameinfo[saveSlotSelected].lives));

	V_DrawScaledPatch(ecks + 120, 168, 0, W_CachePatchName("CONTINS", PU_CACHE));
	V_DrawString(ecks + 140, 172, 0, va("x %d", savegameinfo[saveSlotSelected].continues));
}

#define LOADBARHEIGHT SP_LoadDef.y + (LINEHEIGHT * (j+1)) + ymod
#define CURSORHEIGHT  SP_LoadDef.y + (LINEHEIGHT*3) - 1
static void M_DrawLoad(void)
{
	INT32 i, j;
	INT32 ymod = 0, offset = 0;

	M_DrawMenuTitle();

	if (menumovedir != 0) //movement illusion
	{
		ymod = (-(LINEHEIGHT/4))*menumovedir;
		offset = ((menumovedir > 0) ? -1 : 1);
	}

	V_DrawCenteredString(BASEVIDWIDTH/2, 36, 0, "Press backspace to delete a save.");
	V_DrawCenteredString(BASEVIDWIDTH/2, 44, 0, "Press left or right to change level packs.");
	V_DrawCenteredString(BASEVIDWIDTH/2, SP_LoadDef.y, V_YELLOWMAP, va("* %s *", modname[selectedmod]));

	for (i = MAXSAVEGAMES + saveMenuLocation - 2 + offset, j = 0;i <= MAXSAVEGAMES + saveMenuLocation + 2 + offset; i++, j++)
	{
		if ((menumovedir < 0 && j == 4) || (menumovedir > 0 && j == 0))
			continue; //this helps give the illusion of movement

		M_DrawSaveLoadBorder(SP_LoadDef.x, LOADBARHEIGHT);

		if ((i%MAXSAVEGAMES) == NOSAVESLOT) // play without saving
		{
			if (ultimate_selectable)
				V_DrawCenteredString(SP_LoadDef.x+92, LOADBARHEIGHT - 1, 0, "\x87" "ULTIMATE MODE");
			else
				V_DrawCenteredString(SP_LoadDef.x+92, LOADBARHEIGHT - 1, 0, "\x87" "PLAY WITHOUT SAVING");
			continue;
		}

		if (savegameinfo[i%MAXSAVEGAMES].lives == -42)
			V_DrawString(SP_LoadDef.x-6, LOADBARHEIGHT - 1, V_TRANSLUCENT, "NO DATA");
		else if (savegameinfo[i%MAXSAVEGAMES].lives == -666)
			V_DrawString(SP_LoadDef.x-6, LOADBARHEIGHT - 1, 0, "\x85" "CORRUPT SAVE FILE");
		else if (savegameinfo[i%MAXSAVEGAMES].gamemap & 8192)
			V_DrawString(SP_LoadDef.x-6, LOADBARHEIGHT - 1, 0, "\x83" "CLEAR!");
		else
			V_DrawString(SP_LoadDef.x-6, LOADBARHEIGHT - 1, 0, va("%s", savegameinfo[i%MAXSAVEGAMES].levelname));

		//Draw the save slot number on the right side
		V_DrawRightAlignedString(SP_LoadDef.x+192, LOADBARHEIGHT - 1, 0, va("%d",(i%MAXSAVEGAMES) + 1));
	}

	//Draw cursors on both sides.
	V_DrawScaledPatch( 32, CURSORHEIGHT, 0, W_CachePatchName("M_CURSOR", PU_CACHE));
	V_DrawScaledPatch(274, CURSORHEIGHT, 0, W_CachePatchName("M_CURSOR", PU_CACHE));

	M_DrawGameStats();

	//finishing the movement illusion
	if (menumovedir)
		menumovedir += ((menumovedir > 0) ? 1 : -1);
	if (abs(menumovedir) > 3)
		menumovedir = 0;
}
#undef LOADBARHEIGHT
#undef CURSORHEIGHT

//
// User wants to load this game
//
static void M_LoadSelect(INT32 choice)
{
	(void)choice;

	if (saveSlotSelected == NOSAVESLOT) //last slot is play without saving
	{
		M_NewGame();
		cursaveslot = -1;
		return;
	}

	if (!FIL_ReadFileOK(va(modsgn[selectedmod], saveSlotSelected)))
	{
		// This slot is empty, so start a new game here.
		M_NewGame();
	}
	else if (savegameinfo[saveSlotSelected].gamemap & 8192) // Completed
		M_LoadGameLevelSelect(saveSlotSelected + 1);
	else
		G_LoadGame((UINT32)saveSlotSelected, 0);

	cursaveslot = saveSlotSelected;
}

#define VERSIONSIZE             16
#define BADSAVE { savegameinfo[slot].lives = -666; Z_Free(savebuffer); return; }
#define CHECKPOS if (save_p >= end_p) BADSAVE
// Reads the save file to list lives, level, player, etc.
// Tails 05-29-2003
static void M_ReadSavegameInfo(UINT32 slot)
{
	size_t length;
	char savename[255];
	UINT8 *savebuffer;
	UINT8 *end_p; // buffer end point, don't read past here
	UINT8 *save_p;
	INT32 fake; // Dummy variable
	char temp[sizeof(modtaf[selectedmod])];

	sprintf(savename, modsgn[selectedmod], slot);

	length = FIL_ReadFile(savename, &savebuffer);
	if (length == 0)
	{
		CONS_Printf("%s %s", text[HUSTR_MSGU], savename);
		savegameinfo[slot].lives = -42;
		return;
	}

	end_p = savebuffer + length;

	// skip the description field
	save_p = savebuffer;

	save_p += VERSIONSIZE;

	// dearchive all the modifications
	// P_UnArchiveMisc()

	CHECKPOS
	fake = READINT16(save_p);

	if (((fake-1) & ~8192) >= NUMMAPS) BADSAVE
	strcpy(savegameinfo[slot].levelname, mapheaderinfo[(fake-1) & ~8192].lvlttl);

	if (fake == 24) //meh, let's count old Clear! saves too
		fake |= 8192;
	savegameinfo[slot].gamemap = fake;

	savegameinfo[slot].actnum = mapheaderinfo[(fake-1) & ~8192].actnum;

	CHECKPOS
	fake = READUINT16(save_p)-357; // emeralds

	savegameinfo[slot].numemeralds = 0;

	if (fake & EMERALD1)
		savegameinfo[slot].numemeralds++;
	if (fake & EMERALD2)
		savegameinfo[slot].numemeralds++;
	if (fake & EMERALD3)
		savegameinfo[slot].numemeralds++;
	if (fake & EMERALD4)
		savegameinfo[slot].numemeralds++;
	if (fake & EMERALD5)
		savegameinfo[slot].numemeralds++;
	if (fake & EMERALD6)
		savegameinfo[slot].numemeralds++;
	if (fake & EMERALD7)
		savegameinfo[slot].numemeralds++;

	CHECKPOS
	READSTRINGN(save_p, temp, sizeof(temp)); // mod it belongs to

	if (strcmp(temp, modtaf[selectedmod])) BADSAVE

	// P_UnArchivePlayer()
	CHECKPOS
	savegameinfo[slot].skincolor = READUINT8(save_p);
	CHECKPOS
	savegameinfo[slot].skinnum = READUINT8(save_p);
	strcpy(savegameinfo[slot].playername,
		skins[savegameinfo[slot].skinnum].name);

	CHECKPOS
	(void)READINT32(save_p); // Score

	CHECKPOS
	savegameinfo[slot].lives = READINT32(save_p); // lives
	CHECKPOS
	savegameinfo[slot].continues = READINT32(save_p); // continues

	// done
	Z_Free(savebuffer);
}
#undef CHECKPOS
#undef BADSAVE

//
// M_ReadSaveStrings
//  read the strings from the savegame files
//  and put it in savegamestrings global variable
//
static void M_ReadSaveStrings(void)
{
	FILE *handle;
	UINT32 i;
	char name[256];

	for (i = 0; i < MAXSAVEGAMES; i++)
	{
		snprintf(name, sizeof name, modsgn[selectedmod], i);
		name[sizeof name - 1] = '\0';

		handle = fopen(name, "rb");
		if (handle == NULL)
		{
			savegameinfo[i].lives = -42;
			continue;
		}
		fclose(handle);
		M_ReadSavegameInfo(i);
	}
}

//
// User wants to delete this game
//
static void M_SaveGameDeleteResponse(INT32 ch)
{
	char name[256];

	if (ch != 'y' && ch != KEY_ENTER)
		return;

	// delete savegame
	snprintf(name, sizeof name, modsgn[selectedmod], saveMenuLocation);
	name[sizeof name - 1] = '\0';
	remove(name);

	// Refresh savegame menu info
	M_ReadSaveStrings();
}

static void M_HandleLoadSave(INT32 choice)
{
	boolean exitmenu = false; // exit to previous menu

	switch (choice)
	{
		case KEY_DOWNARROW:
			S_StartSound(NULL, sfx_menu1);
			++saveMenuLocation;
			if (saveMenuLocation >= MAXSAVEGAMES)
				saveMenuLocation -= MAXSAVEGAMES;
			menumovedir = 1;
			break;

		case KEY_UPARROW:
			S_StartSound(NULL, sfx_menu1);
			--saveMenuLocation;
			if (saveMenuLocation < 0)
				saveMenuLocation += MAXSAVEGAMES;
			menumovedir = -1;
			break;

		case KEY_RIGHTARROW:
			S_StartSound(NULL, sfx_menu1);
			if (externmodloaded)
				break;

			if (selectedmod++ >= 2)
				selectedmod = 0;

			//cheap hack for the meantime until I finish MULTIMOD
			switch (selectedmod)
			{
				case 1:
					if (grade & 2)
					{
						spstage_start = 101;
						break;
					}
					else
						selectedmod = 2; //no break on purpose
				case 2: spstage_start = 928; break;
				case 0: spstage_start = 1;   break;
				default: break; //shuts some compilers up
			}

			//reread save strings
			M_ReadSaveStrings();
			break;

		case KEY_LEFTARROW:
			S_StartSound(NULL, sfx_menu1);
			if (externmodloaded)
				break;

			if (selectedmod-- == 0)
				selectedmod = 2;

			//cheap hack for the meantime until I finish MULTIMOD
			switch (selectedmod)
			{
				case 1:
					if (grade & 2)
					{
						spstage_start = 101;
						break;
					}
					else
						selectedmod = 0; //no break on purpose
				case 0: spstage_start = 1;   break;
				case 2: spstage_start = 928; break;
				default: break; //shuts some compilers up
			}

			//reread save strings
			M_ReadSaveStrings();
			break;

		case KEY_ENTER:
			S_StartSound(NULL, sfx_menu1);
			if (savegameinfo[saveMenuLocation].lives != -666) // don't allow loading of "bad saves"
				M_LoadSelect(saveMenuLocation);
			break;

		case KEY_ESCAPE:
			exitmenu = true;
			break;

		case KEY_BACKSPACE:
			S_StartSound(NULL, sfx_menu1);
			// Don't allow people to 'delete' "Play without Saving."
			// Nor allow people to 'delete' slots with no saves in them.
			if (saveMenuLocation != NOSAVESLOT && savegameinfo[saveMenuLocation].lives != -42)
				M_StartMessage("Are you sure you want to delete\nthis save game?\n\n(Press 'Y' to confirm)\n",M_SaveGameDeleteResponse,MM_YESNO);
			break;
	}
	if (exitmenu)
	{
		if (currentMenu->prevMenu)
			M_SetupNextMenu(currentMenu->prevMenu);
		else
			M_ClearMenus(true);
	}
}

//
// Selected from SRB2 menu
//
static void M_LoadGame(INT32 choice)
{
	(void)choice;

	M_ReadSaveStrings();
	M_SetupNextMenu(&SP_LoadDef);
}

// ================
// CHARACTER SELECT
// ================

static void M_SetupChoosePlayer(INT32 choice)
{
	(void)choice;

	if (mapheaderinfo[startmap].forcecharacter != 255)
	{
		M_ChoosePlayer(0); //oh for crying out loud just get STARTED, it doesn't matter!
		return;
	}

	if (Playing() == false)
	{
		S_StopMusic();
		S_ChangeMusic(mus_chrsel, true);
	}

	SP_PlayerDef.prevMenu = currentMenu;
	M_SetupNextMenu(&SP_PlayerDef);
}

// Draw the choose player setup menu, had some fun with player anim
static void M_DrawSetupChoosePlayerMenu(void)
{
	INT32      mx = SP_PlayerDef.x, my = SP_PlayerDef.y;
	patch_t *patch;

	// Black BG
	V_DrawFill(0, 0, vid.width, vid.height, 31);

	{
		// Compact the menu
		INT32 i;
		UINT8 alpha = 0;
		for (i = 0; i < currentMenu->numitems; i++)
		{
			if (currentMenu->menuitems[i].status == 0
			|| currentMenu->menuitems[i].status == IT_DISABLED)
				continue;

			currentMenu->menuitems[i].alphaKey = alpha;
			alpha += 8;
		}
	}

	// use generic drawer for cursor, items and title
	M_DrawGenericMenu();

	// TEXT BOX!
	// For the character
	M_DrawTextBox(mx+152,my, 16, 16, 0);

	// For description
	M_DrawTextBox(mx-24, my+72, 20, 10, 0);

	patch = W_CachePatchName(description[itemOn].picname, PU_CACHE);

	V_DrawString(mx-16, my+80, V_YELLOWMAP, "Speed:\nAbility:\nNotes:");

	V_DrawScaledPatch(mx+160,my+8,0,patch);
	V_DrawString(mx-16, my+80, 0, description[itemOn].info);
}

// Chose the player you want to use Tails 03-02-2002
static void M_ChoosePlayer(INT32 choice)
{
	INT32 skinnum;
	boolean ultmode = (ultimate_selectable && SP_PlayerDef.prevMenu == &SP_LoadDef && saveSlotSelected == NOSAVESLOT);

	M_ClearMenus(true);

	strlwr(description[choice].skinname);

	skinnum = R_SkinAvailable(description[choice].skinname);

	if (startmap != spstage_start)
		cursaveslot = -1;

	lastmapsaved = 0;
	gamecomplete = false;

	G_DeferedInitNew(ultmode, G_BuildMapName(startmap), skinnum, 0, fromlevelselect);
	COM_BufAddText("dummyconsvar 1\n"); // G_DeferedInitNew doesn't do this
}

// ===============
// STATISTICS MENU
// ===============

static INT32 statsLocation;

//
// M_GetLevelEmblem
//
// Returns pointer to an emblem if an emblem exists
// for that level, and exists for that player.
// NULL if not found.
//
static emblem_t *M_GetLevelEmblem(INT32 mapnum, INT32 player)
{
	INT32 i;

	for (i = 0; i < numemblems; i++)
	{
		if (emblemlocations[i].level == mapnum
			&& emblemlocations[i].player == player)
			return &emblemlocations[i];
	}
	return NULL;
}

static void M_DrawStatsMaps(int location)
{
	INT32 y = 80, i, j, lastvalid = 0;
	char names[8], hours[4], minutes [4], seconds[4];
	emblem_t *emblem;
	boolean drewanything = false;

	V_DrawString(32+36, y-16, 0, "LEVEL NAME");
	V_DrawString(224+28, y-16, 0, "BEST TIME");

	sprintf(names, "%c %c %c", skins[0].name[0], skins[1].name[0], skins[2].name[0]);
	V_DrawString(32, y-16, 0, names);

	for (i = 0; i < NUMMAPS; i++)
	{

		if (mapheaderinfo[i].lvlttl[0] == '\0')
			continue;

		if (!(mapheaderinfo[i].typeoflevel & TOL_SP))
			continue;

		if (!mapvisited[i])
			continue;

		if (location)
		{
			lastvalid = i;
			location--;
			continue;
		}

		drawAnyway:
		drewanything = true;

		for (j = 0;j < 3;j++)
		{
			emblem = M_GetLevelEmblem(i+1, j);
			if (emblem)
				V_DrawScaledPatch(30 + (j * 12), y, 0, W_CachePatchName((emblem->collected) ? "GOTIT" : "NEEDIT", PU_CACHE));
		}

		if (mapheaderinfo[i].actnum != 0)
			V_DrawString(32+36, y, V_YELLOWMAP, va("%s %d", mapheaderinfo[i].lvlttl, mapheaderinfo[i].actnum));
		else
			V_DrawString(32+36, y, V_YELLOWMAP, mapheaderinfo[i].lvlttl);

		if (timedata[i].time)
		{
			sprintf(minutes, "%02i", G_TicsToMinutes(timedata[i].time, true));
			sprintf(seconds, "%02i", G_TicsToSeconds(timedata[i].time));
			sprintf(hours, "%02i", G_TicsToCentiseconds(timedata[i].time));

			V_DrawString(224+28, y, 0, va("%s:%s:%s", minutes,seconds,hours));
		}

		y += 8;

		if (y >= BASEVIDHEIGHT-8)
			return;
	}
	if (!drewanything)
	{
		i = lastvalid;
		statsLocation -= location+1;
		goto drawAnyway; //Oh cripes, a goto into a loop.
	}
}

static void M_DrawStats(void)
{
	INT32 found = 0;
	INT32 i;
	char hours[4];
	char minutes[4];
	char seconds[4];
	tic_t besttime = 0;
	boolean displaytimeattack = true;

	for (i = 0; i < MAXEMBLEMS; i++)
	{
		if (emblemlocations[i].collected)
			found++;
	}

	V_DrawString(64, 32, 0, va("x %d/%d", found, numemblems));
	V_DrawScaledPatch(32, 32-4, 0, W_CachePatchName("EMBLICON", PU_STATIC));

	sprintf(hours, "%02i:", G_TicsToHours(totalplaytime));
	sprintf(minutes, "%02i", G_TicsToMinutes(totalplaytime, false));
	sprintf(seconds, "%02i", G_TicsToSeconds(totalplaytime));

	V_DrawCenteredString(224, 8, 0, "Total Play Time:");
	V_DrawCenteredString(224, 20, 0, va("%s:%s:%s", hours, minutes, seconds));

	for (i = 0; i < NUMMAPS; i++)
	{
		if (!(mapheaderinfo[i].timeattack))
			continue;

		if (timedata[i].time > 0)
			besttime += timedata[i].time;
		else
			displaytimeattack = false;
	}

	if (displaytimeattack)
	{
		sprintf(hours, "%02i", G_TicsToHours(besttime));
		sprintf(minutes, "%02i", G_TicsToMinutes(besttime, false));
		sprintf(seconds, "%02i", G_TicsToSeconds(besttime));

		V_DrawCenteredString(224, 36, 0, "Best Time Attack:");
		V_DrawCenteredString(224, 48, 0, va("%s:%s:%s", hours, minutes, seconds));
	}

	M_DrawStatsMaps(statsLocation);
}

// Handle statistics.
static void M_HandleStats(INT32 choice)
{
	boolean exitmenu = false; // exit to previous menu

	switch (choice)
	{
		case KEY_DOWNARROW:
			S_StartSound(NULL, sfx_menu1);
			++statsLocation;
			break;

		case KEY_UPARROW:
			S_StartSound(NULL, sfx_menu1);
			if (statsLocation)
				--statsLocation;
			break;

		case KEY_RIGHTARROW:
			S_StartSound(NULL, sfx_menu1);
			statsLocation += 14;
			break;

		case KEY_LEFTARROW:
			S_StartSound(NULL, sfx_menu1);
			statsLocation -= (statsLocation < 14) ? statsLocation : 14;
			break;

		case KEY_ESCAPE:
			S_StartSound(NULL, sfx_menu1);
			exitmenu = true;
			break;
	}
	if (exitmenu)
	{
		if (currentMenu->prevMenu)
			M_SetupNextMenu(currentMenu->prevMenu);
		else
			M_ClearMenus(true);
	}
}

// ===========
// TIME ATTACK
// ===========

// Drawing function for Time Attack
void M_DrawTimeAttackMenu(void)
{
	patch_t *PictureOfLevel;
	lumpnum_t lumpnum;
	char hours[4];
	char minutes[4];
	char seconds[4];
	char tics[4];
	tic_t besttime = 0;
	INT32 i;
	INT32 mapsunfinished = 0;

	S_ChangeMusic(mus_racent, true); // Eww, but needed for when user hits escape during demo playback

	V_DrawPatchFill(W_CachePatchName("SRB2BACK", PU_CACHE));

	V_DrawRightAlignedString(280, 50+SP_TimeAttackDef.y, V_YELLOWMAP, modname[selectedmod]);
	if (listHasNoMaps)
	{
		V_DrawCenteredString(BASEVIDWIDTH/2, 120, 0, "No time-attackable maps found");
		V_DrawCenteredString(BASEVIDWIDTH/2, 130, 0, "for this level pack.");
		M_DrawGenericMenu();
		return;
	}

	if (W_CheckNumForName(description[cv_chooseskin.value-1].picname) != LUMPERROR)
		V_DrawSmallScaledPatch(224, 16, 0, W_CachePatchName(description[cv_chooseskin.value-1].picname, PU_CACHE));

	//  A 160x100 image of the level as entry MAPxxP
	lumpnum = W_CheckNumForName(va("%sP", G_BuildMapName(cv_nextmap.value)));

	if (lumpnum != LUMPERROR)
		PictureOfLevel = W_CachePatchName(va("%sP", G_BuildMapName(cv_nextmap.value)), PU_CACHE);
	else
		PictureOfLevel = W_CachePatchName("BLANKLVL", PU_CACHE);

	V_DrawSmallScaledPatch(208, 128, 0, PictureOfLevel);

	for (i = 0; i < NUMMAPS; i++)
	{
		if (!(mapheaderinfo[i].timeattack))
			continue;

		if (mapheaderinfo[i].modlistnum != selectedmod)
			continue;

		if (timedata[i].time > 0)
			besttime += timedata[i].time;
		else
			mapsunfinished++;
	}

	sprintf(hours,   "%02i", G_TicsToHours(besttime));
	sprintf(minutes, "%02i", G_TicsToMinutes(besttime, false));
	sprintf(seconds, "%02i", G_TicsToSeconds(besttime));
	sprintf(tics,    "%02i", G_TicsToCentiseconds(besttime));

	V_DrawCenteredString(128, 34, 0, "Best Time Attack:");
	V_DrawCenteredString(128, 46, V_YELLOWMAP, va("%s:%s:%s.%s", hours, minutes, seconds, tics));
	if (mapsunfinished)
		V_DrawCenteredString(128, 54, 0, va("(%d unfinished)", mapsunfinished));

	if (cv_nextmap.value)
	{
		if (timedata[cv_nextmap.value-1].time > 0)
			V_DrawCenteredString(BASEVIDWIDTH/2, 116, 0, va("Best Time: %i:%02i.%02i", G_TicsToMinutes(timedata[cv_nextmap.value-1].time, true),
				G_TicsToSeconds(timedata[cv_nextmap.value-1].time), G_TicsToCentiseconds(timedata[cv_nextmap.value-1].time)));
	}

	M_DrawGenericMenu();
}

// Going to Time Attack menu...
static void M_TimeAttack(INT32 choice)
{
	(void)choice;

	memset(skins_cons_t, 0, sizeof (skins_cons_t));

	M_DoMenuPatching(false);

	inlevelselect = 2; // Don't be dependent on cv_newgametype

	M_PatchSkinNameTable();

	M_SetupNextMenu(&SP_TimeAttackDef);

	G_SetGamestate(GS_TIMEATTACK);
	S_ChangeMusic(mus_racent, true);
}

// Player has selected the "START" from the time attack screen
static void M_ChooseTimeAttack(INT32 choice)
{
	(void)choice;
	emeralds = 0;
	M_ClearMenus(true);
	timeattacking = true;
	G_RecordDemo("temp");
	G_BeginRecording();
	G_DeferedInitNew(false, G_BuildMapName(cv_nextmap.value), cv_chooseskin.value-1, false, false);
	timeattacking = true;
}

// Like M_ChooseTimeAttack, but doesn't record a demo.
static void M_ChooseTimeAttackNoRecord(INT32 choice)
{
	(void)choice;
	emeralds = 0;
	M_ClearMenus(true);
	timeattacking = true;
	remove(va("%s"PATHSEP"temp.lmp", srb2home));
	G_DeferedInitNew(false, G_BuildMapName(cv_nextmap.value), cv_chooseskin.value-1, false, false);
	timeattacking = true;
}

// Player has selected the "REPLAY" from the time attack screen
static void M_ReplayTimeAttack(INT32 choice)
{
	(void)choice;
	M_ClearMenus(true);
	G_DoPlayDemo(va("%s"PATHSEP"replay"PATHSEP"%s"PATHSEP"%s-%02d", srb2home, modtaf[selectedmod], G_BuildMapName(cv_nextmap.value), cv_chooseskin.value-1));

	timeattacking = true;
}

// Player has selected the "STAFF REPLAY" from the time attack screen
static void M_StaffTime(INT32 choice)
{
	M_ClearMenus(true);
	//todo remove this hack (fix demos)
	if (choice == tastaffknux)
	{
		CV_SetValue(&cv_chooseskin, 3);
		G_DoPlayDemo(va("STK%s",G_BuildMapName(cv_nextmap.value)));
	}
	else if (choice == tastafftails)
	{
		CV_SetValue(&cv_chooseskin, 2);
		G_DoPlayDemo(va("STT%s",G_BuildMapName(cv_nextmap.value)));
	}
	else
	{
		CV_SetValue(&cv_chooseskin, 1);
		G_DoPlayDemo(va("STS%s",G_BuildMapName(cv_nextmap.value)));
	}

	timeattacking = true;
}

// ========
// END GAME
// ========

static void M_ExitGameResponse(INT32 ch)
{
	if (ch != 'y' && ch != KEY_ENTER)
		return;

	//Command_ExitGame_f();
	G_SetExitGameFlag();
	M_ClearMenus(true);
}

static void M_EndGame(INT32 choice)
{
	(void)choice;
	if (demoplayback || demorecording)
		return;

	if (!Playing())
		return;

	M_StartMessage(text[ENDGAME],M_ExitGameResponse,MM_YESNO);
}

//===========================================================================
// Connect Menu
//===========================================================================

#define FIRSTSERVERLINE 6
#define FIRSTLANSERVERLINE 4
#define SERVERHEADERHEIGHT 56
#define SERVERLINEHEIGHT 12

#define S_LINEY(n) currentMenu->y + SERVERHEADERHEIGHT + (n * SERVERLINEHEIGHT)

#ifndef NONET
static UINT32 localservercount;

static void M_NextServerPage(void)
{
	if ((serverlistpage + 1) * SERVERS_PER_PAGE < serverlistcount) serverlistpage++;
}

static void M_PrevServerPage(void)
{
	if (serverlistpage > 0) serverlistpage--;
}

static void M_Connect(INT32 choice)
{
	// do not call menuexitfunc
	M_ClearMenus(false);

	COM_BufAddText(va("connect node %d\n", serverlist[choice-FIRSTSERVERLINE + serverlistpage * SERVERS_PER_PAGE].node));
}

static void M_Refresh(INT32 choice)
{
	(void)choice;
	CL_UpdateServerList(cv_serversearch.value, cv_chooseroom.value);

	// first page of servers
	serverlistpage = 0;
}

static void M_DrawRoomInfoMenu(void)
{
	// use generic drawer for cursor, items and title
	M_DrawGenericMenu();
	M_DrawTextBox(0, 56, 38, 12, 0);
	V_DrawString(8, 64, V_WORDWRAP|V_ALLOWLOWERCASE, room_motd);
}


static void M_DrawConnectMenu(void)
{
	UINT16 i, j;
	const char *gt = "Unknown";

	for (i = FIRSTSERVERLINE; i < min(localservercount, SERVERS_PER_PAGE)+FIRSTSERVERLINE; i++)
		MP_ConnectMenu[i].status = IT_STRING | IT_SPACE;

	// Horizontal line!
	V_DrawFill(1, currentMenu->y+54, 318, 1, 0);

	// Changed room warning!
	if (cv_chooseroom.value != oldroomnum)
		V_DrawString(currentMenu->x+40, currentMenu->y+4, 0, "\x85" "*CHANGED*");

	if (rooms_cons_t[0].value < 0)
		V_DrawCenteredString(BASEVIDWIDTH/2,currentMenu->y+SERVERHEADERHEIGHT, 0, "Error contacting the Master Server");
	else if (serverlistcount <= 0)
		V_DrawString(currentMenu->x,currentMenu->y+SERVERHEADERHEIGHT, 0, "No servers found");
	else
	for (i = 0; i < min(serverlistcount - serverlistpage * SERVERS_PER_PAGE, SERVERS_PER_PAGE); i++)
	{
		INT32 slindex = i + serverlistpage * SERVERS_PER_PAGE;
		UINT32 globalflags = ((serverlist[slindex].info.numberofplayer >= serverlist[slindex].info.maxplayer) ? V_TRANSLUCENT : 0)
			|((itemOn == FIRSTSERVERLINE+i) ? V_YELLOWMAP : 0)|V_ALLOWLOWERCASE;

		V_DrawString(currentMenu->x, S_LINEY(i), globalflags, serverlist[slindex].info.servername);

		if (serverlist[slindex].info.modifiedgame)
			V_DrawSmallString(currentMenu->x+202, S_LINEY(i)+8, globalflags, "\x85" "Game Modified");

		V_DrawSmallString(currentMenu->x, S_LINEY(i)+8, globalflags,
		                     va("Ping: %u", (UINT32)LONG(serverlist[slindex].info.time)));

		gt = "Unknown";
		for (j = 0; gametype_cons_t[j].strvalue; j++)
		{
			if (gametype_cons_t[j].value == serverlist[slindex].info.gametype)
				gt = gametype_cons_t[j].strvalue;
		}

		V_DrawSmallString(currentMenu->x+46,S_LINEY(i)+8, globalflags,
		                         va("Players: %02d/%02d", serverlist[slindex].info.numberofplayer, serverlist[slindex].info.maxplayer));

		V_DrawSmallString(currentMenu->x+112, S_LINEY(i)+8, globalflags, va("Gametype: %s", gt));

		MP_ConnectMenu[i+FIRSTSERVERLINE].status = IT_STRING | IT_CALL;
	}

	localservercount = serverlistcount;

	M_DrawGenericMenu();
}

static void M_DrawConnectLANMenu(void)
{
	UINT16 i, j;
	const char *gt = NULL;

	for (i = FIRSTLANSERVERLINE; i < min(localservercount, SERVERS_PER_PAGE)+FIRSTLANSERVERLINE; i++)
		MP_ConnectLANMenu[i].status = IT_STRING | IT_SPACE;

	// Horizontal line!
	V_DrawFill(1, currentMenu->y+54, 318, 1, 0);

	if (serverlistcount <= 0)
		V_DrawString (currentMenu->x,currentMenu->y+SERVERHEADERHEIGHT, 0, "No servers found");
	else
	for (i = 0; i < min(serverlistcount - serverlistpage * SERVERS_PER_PAGE, SERVERS_PER_PAGE); i++)
	{
		INT32 slindex = i + serverlistpage * SERVERS_PER_PAGE;
		UINT32 globalflags = ((serverlist[slindex].info.numberofplayer >= serverlist[slindex].info.maxplayer) ? V_TRANSLUCENT : 0)
			|((itemOn == FIRSTSERVERLINE+i) ? V_YELLOWMAP : 0)|V_ALLOWLOWERCASE;

		V_DrawString(currentMenu->x, S_LINEY(i), globalflags, serverlist[slindex].info.servername);

		if (serverlist[slindex].info.modifiedgame)
			V_DrawSmallString(currentMenu->x+202, S_LINEY(i)+8, globalflags, "\x85" "Game Modified");

		V_DrawSmallString(currentMenu->x, S_LINEY(i)+8, globalflags,
		                     va("Ping: %u", (UINT32)LONG(serverlist[slindex].info.time)));

		gt = "Unknown";
		for (j = 0; gametype_cons_t[j].strvalue; j++)
		{
			if (gametype_cons_t[j].value == serverlist[slindex].info.gametype)
				gt = gametype_cons_t[j].strvalue;
		}

		V_DrawSmallString(currentMenu->x+46,S_LINEY(i)+8, globalflags,
		                         va("Players: %02d/%02d", serverlist[slindex].info.numberofplayer, serverlist[slindex].info.maxplayer));

		V_DrawSmallString(currentMenu->x+112, S_LINEY(i)+8, globalflags, va("Gametype: %s", gt));

		MP_ConnectLANMenu[i+FIRSTLANSERVERLINE].status = IT_STRING | IT_CALL;
	}

	localservercount = serverlistcount;

	M_DrawGenericMenu();
}

static boolean M_CancelConnect(void)
{
	D_CloseConnection();
	return true;
}

// Ascending order, not descending.
// The casts are safe as long as the caller doesn't do anything stupid.
#define SERVER_LIST_ENTRY_COMPARATOR(key) \
static int ServerListEntryComparator_##key(const void *entry1, const void *entry2) \
{ \
	return ((const serverelem_t*)entry1)->info.key - ((const serverelem_t*)entry2)->info.key; \
}

SERVER_LIST_ENTRY_COMPARATOR(time)
SERVER_LIST_ENTRY_COMPARATOR(numberofplayer)
SERVER_LIST_ENTRY_COMPARATOR(gametype)
#endif

static void M_SortServerList(void)
{
#ifndef NONET
	switch(cv_serversort.value)
	{
	case 0:		// Ping.
		qsort(serverlist, serverlistcount, sizeof(serverelem_t), ServerListEntryComparator_time);
		break;
	case 1:		// Players.
		qsort(serverlist, serverlistcount, sizeof(serverelem_t), ServerListEntryComparator_numberofplayer);
		break;
	case 2:		// Gametype.
		qsort(serverlist, serverlistcount, sizeof(serverelem_t), ServerListEntryComparator_gametype);
		break;
	}
#endif
}

#ifndef NONET
#ifdef UPDATE_ALERT
static int M_CheckMODVersion(void)
{
	char updatestring[500];
	const char *updatecheck = GetMODVersion();
	if(updatecheck)
	{
		sprintf(updatestring, UPDATE_ALERT_STRING, updatecheck, VERSIONSTRING);
		M_StartMessage(updatestring, NULL, MM_NOTHING);
		return false;
	} else
		return true;
}
#endif

static void M_ConnectMenu(INT32 choice)
{
	(void)choice;
	if (modifiedgame)
	{
		M_StartMessage("You have wad files loaded and/or\nmodified the game in some way.\nPlease restart SRB2 before\nconnecting.", NULL, MM_NOTHING);
		return;
	}

	// Display a little "please wait" message.
	M_DrawTextBox(52, BASEVIDHEIGHT/2-10, 25, 3, 0);
	V_DrawCenteredString(BASEVIDWIDTH/2, BASEVIDHEIGHT/2, 0, "Contacting list server...");
	V_DrawCenteredString(BASEVIDWIDTH/2, (BASEVIDHEIGHT/2)+12, 0, "Please wait.");
	I_OsPolling();
	I_UpdateNoBlit();
	if (rendermode == render_soft)
		I_FinishUpdate(); // page flip or blit buffer

	// first page of servers
	serverlistpage = 0;
	cv_serversearch.value = 1;
	if(!M_PatchRoomsTable(false))
		return;
#ifdef UPDATE_ALERT
	if(M_CheckMODVersion())
	{
#endif
		M_SetupNextMenu(&MP_ConnectDef);
		M_Refresh(0);
#ifdef UPDATE_ALERT
	}
#endif
}

static void M_ConnectLANMenu(INT32 choice)
{
	(void)choice;
	if (modifiedgame)
	{
		M_StartMessage("You have wad files loaded and/or\nmodified the game in some way.\nPlease restart SRB2 before\nconnecting.", NULL, MM_NOTHING);
		return;
	}

	// Display a little "please wait" message.
	M_DrawTextBox(52, BASEVIDHEIGHT/2-10, 25, 3, 0);
	V_DrawCenteredString(BASEVIDWIDTH/2, BASEVIDHEIGHT/2, 0, "Searching Local Network for Servers...");
	V_DrawCenteredString(BASEVIDWIDTH/2, (BASEVIDHEIGHT/2)+12, 0, "Please wait.");
	I_OsPolling();
	I_UpdateNoBlit();
	if (rendermode == render_soft)
		I_FinishUpdate(); // page flip or blit buffer

	// first page of servers
	cv_serversearch.value = 0;
	serverlistpage = 0;
	M_SetupNextMenu(&MP_ConnectLANDef);
	M_Refresh(0);
}

static void M_RoomInfoMenu(INT32 choice)
{
	(void)choice;

	M_AlterRoomInfo();

	MP_RoomInfoDef.prevMenu = currentMenu;

	M_SetupNextMenu(&MP_RoomInfoDef);
}

void M_AlterRoomInfo(void)
{
	INT32 i = -1;

	for (i = 0; room_list[i].header.buffer[0]; i++)
	{
		if(cv_chooseroom.value == room_list[i].id)
		{
			room_motd = room_list[i].motd;
			break;
		}
	}
}
#endif //NONET

//===========================================================================
// Start Server Menu
//===========================================================================

//
// FindFirstMap
//
// Finds the first map of a particular gametype
// Defaults to 1 if nothing found.
//
static INT32 M_FindFirstMap(INT32 gtype)
{
	INT32 i;

	for (i = 0; i < NUMMAPS; i++)
	{
		if (mapheaderinfo[i].typeoflevel & gtype)
			return i + 1;
	}

	return 1;
}

static void M_StartServer(INT32 choice)
{
	boolean StartSplitScreenGame = (currentMenu == &MP_SplitServerDef);

	(void)choice;
	if (!StartSplitScreenGame)
		netgame = true;

	multiplayer = true;

	// Special Cases
	if (cv_newgametype.value == GTF_TEAMMATCH)
		CV_SetValue(&cv_matchtype, 1);
	else if (cv_newgametype.value == GTF_CLASSICRACE)
		CV_SetValue(&cv_racetype, 1);
	else if (cv_newgametype.value == GTF_HIDEANDSEEK)
		CV_SetValue(&cv_tagtype, 1);
	else if (cv_newgametype.value == GT_MATCH)
		CV_SetValue(&cv_matchtype, 0);
	else if (cv_newgametype.value == GT_RACE)
		CV_SetValue(&cv_racetype, 0);
	else if (cv_newgametype.value == GT_TAG)
		CV_SetValue(&cv_tagtype, 0);

	// Still need to remove DEVMODE.
	cv_debug = 0;

	if (!StartSplitScreenGame)
	{
		if (demoplayback)
			COM_BufAddText("stopdemo\n");
		D_MapChange(cv_nextmap.value, cv_newgametype.value, false, 1, 1, false, false);
		COM_BufAddText("dummyconsvar 1\n");
	}
	else // split screen
	{
		paused = false;
		if (demoplayback)
			COM_BufAddText("stopdemo\n");

		SV_StartSinglePlayerServer();
		if (!splitscreen)
		{
			splitscreen = true;
			SplitScreen_OnChange();
		}
		D_MapChange(cv_nextmap.value, cv_newgametype.value, false, 1, 1, false, false);
	}

	M_ClearMenus(true);
}

static void M_DrawServerMenu(void)
{
	lumpnum_t lumpnum;
	patch_t *PictureOfLevel;

	M_DrawGenericMenu();

	//  A 160x100 image of the level as entry MAPxxP
	lumpnum = W_CheckNumForName(va("%sP", G_BuildMapName(cv_nextmap.value)));

	if (lumpnum != LUMPERROR)
		PictureOfLevel = W_CachePatchName(va("%sP", G_BuildMapName(cv_nextmap.value)), PU_CACHE);
	else
		PictureOfLevel = W_CachePatchName("BLANKLVL", PU_CACHE);

	V_DrawSmallScaledPatch((BASEVIDWIDTH*3/4)-(SHORT(PictureOfLevel->width)/4), ((BASEVIDHEIGHT*3/4)-(SHORT(PictureOfLevel->height)/4)+10), 0, PictureOfLevel);
}

static void M_MapChange(INT32 choice)
{
	(void)choice;

	inlevelselect = 0;
	M_PatchLevelNameTable(0);

	// Special Cases
	if (gametype == GT_MATCH && cv_matchtype.value) // Team Match
		CV_SetValue(&cv_newgametype, GTF_TEAMMATCH);
	else if (gametype == GT_RACE && cv_racetype.value) // Time-Only Race
		CV_SetValue(&cv_newgametype, GTF_CLASSICRACE);
	else if (gametype == GT_TAG && cv_tagtype.value) // Hide and Seek Mode
		CV_SetValue(&cv_newgametype, GTF_HIDEANDSEEK);
	else
		CV_SetValue(&cv_newgametype, gametype);

	CV_SetValue(&cv_nextmap, gamemap);

	M_SetupNextMenu(&MISC_ChangeLevelDef);
}

static void M_StartSplitServerMenu(INT32 choice)
{
	(void)choice;
	inlevelselect = 0;
	M_PatchLevelNameTable(0);
	M_SetupNextMenu(&MP_SplitServerDef);
}

#ifndef NONET
static void M_StartServerMenu(INT32 choice)
{
	(void)choice;
	inlevelselect = 0;
	M_PatchLevelNameTable(0);
	M_AlterRoomOptions();
	M_SetupNextMenu(&MP_ServerDef);

}

void M_AlterRoomOptions(void)
{
	if (cv_internetserver.value)
	{
		MP_ServerMenu[2].status = IT_STRING|IT_CVAR; // Make room option available.
		MP_ServerMenu[3].status = IT_STRING|IT_CALL; // Make room info option available.
#ifdef UPDATE_ALERT
		if(M_CheckMODVersion())
		{
#endif
			if(!M_PatchRoomsTable(true))
			{
				MP_ServerMenu[2].status = IT_DISABLED; // Make room option unavailable.
				MP_ServerMenu[3].status = IT_DISABLED; // Same for Room info.
				CV_SetValue(&cv_internetserver, 0);
				return;
			}
#ifdef UPDATE_ALERT
		} else {
			MP_ServerMenu[2].status = IT_DISABLED; // Make room option unavailable.
			MP_ServerMenu[3].status = IT_DISABLED; // Same for Room info.
			CV_SetValue(&cv_internetserver, 0);
		}
#endif
	}
	else
	{
		MP_ServerMenu[2].status = IT_DISABLED; // No room.
		MP_ServerMenu[3].status = IT_DISABLED; // Same for Room info.
	}
}
#endif

// ==============
// CONNECT VIA IP
// ==============

#ifndef NONET
static char setupm_ip[16];

// Connect using IP address Tails 11-19-2002
static void M_ConnectIPMenu(INT32 choice)
{
	(void)choice;
	if (modifiedgame)
	{
		M_StartMessage("You have wad files loaded and/or\nmodified the game in some way.\nPlease restart SRB2 before\nconnecting.", NULL, MM_NOTHING);
		return;
	}

	M_SetupNextMenu(&MP_ConnectIPDef);
}

// Draw the funky Connect IP menu. Tails 11-19-2002
// So much work for such a little thing!
static void M_DrawConnectIPMenu(void)
{
	// use generic drawer for cursor, items and title
	M_DrawGenericMenu();

	// draw name string
	V_DrawString (128,40,0,setupm_ip);

	// draw text cursor for name
	if (itemOn == 0 &&
	    skullAnimCounter < 4)   //blink cursor
		V_DrawCharacter(128+V_StringWidth(setupm_ip),40,'_',false);
}

// Tails 11-19-2002
static void M_ConnectIP(INT32 choice)
{
	(void)choice;
	COM_BufAddText(va("connect %s\n", setupm_ip));

	// A little "please wait" message.
	M_DrawTextBox(56, BASEVIDHEIGHT/2-12, 24, 2, 0);
	V_DrawCenteredString(BASEVIDWIDTH/2, BASEVIDHEIGHT/2, 0, "Connecting to server...");
	I_OsPolling();
	I_UpdateNoBlit();
	if (rendermode == render_soft)
		I_FinishUpdate(); // page flip or blit buffer
}

// Tails 11-19-2002
static void M_HandleConnectIP(INT32 choice)
{
	size_t   l;
	boolean  exitmenu = false;  // exit to previous menu and send name change

	switch (choice)
	{
		case KEY_ENTER:
			S_StartSound(NULL,sfx_menu1); // Tails
			M_ClearMenus(true);
			M_ConnectIP(1);
			break;

		case KEY_ESCAPE:
			exitmenu = true;
			break;

		case KEY_BACKSPACE:
			if ((l = strlen(setupm_ip))!=0 && itemOn == 0)
			{
				S_StartSound(NULL,sfx_menu1); // Tails
				setupm_ip[l-1] =0;
			}
			break;

		default:
			l = strlen(setupm_ip);
			if (l < 16-1 && (choice == 46 || (choice >= 48 && choice <= 57))) // Rudimentary number and period enforcing
			{
				S_StartSound(NULL,sfx_menu1); // Tails
				setupm_ip[l] =(char)choice;
				setupm_ip[l+1] =0;
			}
			else if (l < 16-1 && choice >= 199 && choice <= 211 && choice != 202 && choice != 206) //numpad too!
			{
				XBOXSTATIC char keypad_translation[] = {'7','8','9','-','4','5','6','+','1','2','3','0','.'};
				choice = keypad_translation[choice - 199];
				S_StartSound(NULL,sfx_menu1); // Tails
				setupm_ip[l] =(char)choice;
				setupm_ip[l+1] =0;
			}
			break;
	}

	if (exitmenu)
	{
		if (currentMenu->prevMenu)
			M_SetupNextMenu (currentMenu->prevMenu);
		else
			M_ClearMenus(true);
	}
}
#endif //!NONET

// ========================
// MULTIPLAYER PLAYER SETUP
// ========================
// Tails 03-02-2002

#define PLBOXW    8
#define PLBOXH    9

static INT32      multi_tics;
static state_t   *multi_state;

// this is set before entering the MultiPlayer setup menu,
// for either player 1 or 2
static char       setupm_name[MAXPLAYERNAME+1];
static player_t  *setupm_player;
static consvar_t *setupm_cvskin;
static consvar_t *setupm_cvcolor;
static consvar_t *setupm_cvname;
static INT32      setupm_fakeskin;
static INT32      setupm_fakecolor;

static void M_DrawSetupMultiPlayerMenu(void)
{
	INT32 mx, my, st;
	spritedef_t *sprdef;
	spriteframe_t *sprframe;
	patch_t *patch;

	mx = MP_PlayerSetupDef.x;
	my = MP_PlayerSetupDef.y;

	// use generic drawer for cursor, items and title
	M_DrawGenericMenu();

	// draw name string
	M_DrawTextBox(mx + 90, my - 8, MAXPLAYERNAME, 1, 0);
	V_DrawString(mx + 98, my, V_ALLOWLOWERCASE, setupm_name);

	// draw skin string
	V_DrawString(mx + 90, my + 96, 0, skins[setupm_fakeskin].name);

	// draw the name of the color you have chosen
	// Just so people don't go thinking that "Default" is Green.
	V_DrawString(208, 72, 0, Color_Names[setupm_fakecolor]);

	// draw text cursor for name
	if (!itemOn && skullAnimCounter < 4) // blink cursor
		V_DrawCharacter(mx + 98 + V_StringWidth(setupm_name), my, '_',false);

	// anim the player in the box
	if (--multi_tics <= 0)
	{
		st = multi_state->nextstate;
		if (st != S_NULL)
			multi_state = &states[st];
		multi_tics = multi_state->tics;
		if (multi_tics == -1)
			multi_tics = 15;
	}

	// skin 0 is default player sprite
	if (R_SkinAvailable(skins[setupm_fakeskin].name) != -1)
		sprdef = &skins[R_SkinAvailable(skins[setupm_fakeskin].name)].spritedef;
	else
		sprdef = &skins[0].spritedef;

	sprframe = &sprdef->spriteframes[multi_state->frame & FF_FRAMEMASK];
	patch = W_CachePatchNum(sprframe->lumppat[0], PU_CACHE);

	// draw box around guy
	M_DrawTextBox(mx + 90, my + 8, PLBOXW, PLBOXH, 0);

	// draw player sprite
	if (!setupm_fakecolor) // should never happen but hey, who knows
	{
		if (atoi(skins[setupm_fakeskin].highres))
			V_DrawScaledPatch(mx + 98 + (PLBOXW*8/2), my + 16 + (PLBOXH*8) - 8, 0, patch);
		else
			V_DrawScaledPatch(mx + 98 + (PLBOXW*8/2), my + 16 + (PLBOXH*8) - 8, 0, patch);
	}
	else
	{
		const UINT8 *colormap = (const UINT8 *)translationtables[setupm_fakeskin] - 256
			+ (setupm_fakecolor<<8);

		if (atoi(skins[setupm_fakeskin].highres))
			V_DrawSmallMappedPatch(mx + 98 + (PLBOXW*8/2), my + 16 + (PLBOXH*8) - 8, 0, patch, colormap);
		else
			V_DrawMappedPatch(mx + 98 + (PLBOXW*8/2), my + 16 + (PLBOXH*8) - 8, 0, patch, colormap);
	}
}

// Handle 1P/2P MP Setup
static void M_HandleSetupMultiPlayer(INT32 choice)
{
	size_t   l;
	boolean  exitmenu = false;  // exit to previous menu and send name change

	switch (choice)
	{
		case KEY_DOWNARROW:
			S_StartSound(NULL,sfx_menu1); // Tails
			if (itemOn+1 >= currentMenu->numitems)
				itemOn = 0;
			else itemOn++;
			break;

		case KEY_UPARROW:
			S_StartSound(NULL,sfx_menu1); // Tails
			if (!itemOn)
				itemOn = (INT16)(currentMenu->numitems-1);
			else itemOn--;
			break;

		case KEY_LEFTARROW:
			if (itemOn == 2)       //player skin
			{
				S_StartSound(NULL,sfx_menu1); // Tails
				setupm_fakeskin--;
			}
			else if (itemOn == 1) // player color
			{
				S_StartSound(NULL,sfx_menu1); // Tails
				setupm_fakecolor--;
			}
			break;

		case KEY_RIGHTARROW:
			if (itemOn == 2)       //player skin
			{
				S_StartSound(NULL,sfx_menu1); // Tails
				setupm_fakeskin++;
			}
			else if (itemOn == 1) // player color
			{
				S_StartSound(NULL,sfx_menu1); // Tails
				setupm_fakecolor++;
			}
			break;

		case KEY_ESCAPE:
			exitmenu = true;
			break;

		case KEY_BACKSPACE:
			if ((l = strlen(setupm_name))!=0 && itemOn == 0)
			{
				S_StartSound(NULL,sfx_menu1); // Tails
				setupm_name[l-1] =0;
			}
			break;

		default:
			if (choice < 32 || choice > 127 || itemOn != 0)
				break;
			l = strlen(setupm_name);
			if (l < MAXPLAYERNAME-1)
			{
				S_StartSound(NULL,sfx_menu1); // Tails
				setupm_name[l] =(char)choice;
				setupm_name[l+1] =0;
			}
			break;
	}

	// check skin
	if (setupm_fakeskin < 0)
		setupm_fakeskin = numskins-1;
	if (setupm_fakeskin > numskins-1)
		setupm_fakeskin = 0;

	// check color
	if (setupm_fakecolor < 1)
		setupm_fakecolor = 15;
	if (setupm_fakecolor > 15)
		setupm_fakecolor = 1;

	if (exitmenu)
	{
		if (currentMenu->prevMenu)
			M_SetupNextMenu (currentMenu->prevMenu);
		else
			M_ClearMenus(true);
	}
}

// start the multiplayer setup menu
static void M_SetupMultiPlayer(INT32 choice)
{
	(void)choice;

	multi_state = &states[mobjinfo[MT_PLAYER].seestate];
	multi_tics = multi_state->tics;
	strcpy(setupm_name, cv_playername.string);

	// set for player 1
	setupm_player = &players[consoleplayer];
	setupm_cvskin = &cv_skin;
	setupm_cvcolor = &cv_playercolor;
	setupm_cvname = &cv_playername;

	// For whatever reason this doesn't work right if you just use ->value
	setupm_fakeskin = R_SkinAvailable(setupm_cvskin->string);
	if (setupm_fakeskin == -1)
		setupm_fakeskin = 0;
	setupm_fakecolor = setupm_cvcolor->value;

	MP_PlayerSetupDef.prevMenu = currentMenu;
	M_SetupNextMenu(&MP_PlayerSetupDef);
}

// start the multiplayer setup menu, for secondary player (splitscreen mode)
static void M_SetupMultiPlayer2(INT32 choice)
{
	(void)choice;

	multi_state = &states[mobjinfo[MT_PLAYER].seestate];
	multi_tics = multi_state->tics;
	strcpy (setupm_name, cv_playername2.string);

	// set for splitscreen secondary player
	setupm_player = &players[secondarydisplayplayer];
	setupm_cvskin = &cv_skin2;
	setupm_cvcolor = &cv_playercolor2;
	setupm_cvname = &cv_playername2;

	// For whatever reason this doesn't work right if you just use ->value
	setupm_fakeskin = R_SkinAvailable(setupm_cvskin->string);
	if (setupm_fakeskin == -1)
		setupm_fakeskin = 0;
	setupm_fakecolor = setupm_cvcolor->value;

	MP_PlayerSetupDef.prevMenu = currentMenu;
	M_SetupNextMenu(&MP_PlayerSetupDef);
}

static boolean M_QuitMultiPlayerMenu(void)
{
	size_t l;
	// send name if changed
	if (strcmp(setupm_name, setupm_cvname->string))
	{
		// remove trailing whitespaces
		for (l= strlen(setupm_name)-1;
		    (signed)l >= 0 && setupm_name[l] ==' '; l--)
			setupm_name[l] =0;
		COM_BufAddText (va("%s \"%s\"\n",setupm_cvname->name,setupm_name));
	}
	// you know what? always putting these in the buffer won't hurt anything.
	COM_BufAddText (va("%s \"%s\"\n",setupm_cvskin->name,skins[setupm_fakeskin].name));
	COM_BufAddText (va("%s %d\n",setupm_cvcolor->name,setupm_fakecolor));
	return true;
}

// =================
// DATA OPTIONS MENU
// =================

static void M_TimeDataResponse(INT32 ch)
{
	INT32 i;
	if (ch != 'y' && ch != KEY_ENTER)
		return;

	// Delete the data
	for (i = 0; i < NUMMAPS; i++)
		timedata[i].time = 0;
}

static void M_SecretsDataResponse(INT32 ch)
{
	INT32 i;
	if (ch != 'y' && ch != KEY_ENTER)
		return;

	// Delete the data
	for (i = 0; i < MAXEMBLEMS; i++)
		emblemlocations[i].collected = false;

	grade = 0;
	timesbeaten = 0;
}

static void M_EraseData(INT32 choice)
{
	if (choice == 0)
		M_StartMessage("Are you sure you want to delete\nthe time attack data?\n\n(Press 'Y' to confirm)\n",M_TimeDataResponse,MM_YESNO);
	else // 1
		M_StartMessage("Are you sure you want to delete\nthe secrets data?\n\n(Press 'Y' to confirm)\n",M_SecretsDataResponse,MM_YESNO);
}

// =============
// JOYSTICK MENU
// =============

// Start the controls menu, setting it up for either the console player,
// or the secondary splitscreen player

static void M_DrawJoystick(void)
{
	INT32 i;

	M_DrawGenericMenu();

	for (i = 0;i < 8; i++)
	{
		M_DrawSaveLoadBorder(OP_JoystickSetDef.x, OP_JoystickSetDef.y+LINEHEIGHT*i);

		if ((setupcontrols_secondaryplayer && (i == cv_usejoystick2.value))
			|| (!setupcontrols_secondaryplayer && (i == cv_usejoystick.value)))
			V_DrawString(OP_JoystickSetDef.x, OP_JoystickSetDef.y+LINEHEIGHT*i,V_GREENMAP,joystickInfo[i]);
		else
			V_DrawString(OP_JoystickSetDef.x, OP_JoystickSetDef.y+LINEHEIGHT*i,0,joystickInfo[i]);
	}
}

static void M_SetupJoystickMenu(INT32 choice)
{
	INT32 i = 0;
	const char *joyname = "None";
	const char *joyNA = "Unavailable";
	INT32 n = I_NumJoys();
	(void)choice;

	strcpy(joystickInfo[i], joyname);

	for (i = 1; i < 8; i++)
	{
		if (i <= n && (joyname = I_GetJoyName(i)) != NULL)
		{
			strncpy(joystickInfo[i], joyname, 24);
			joystickInfo[i][24] = '\0';
		}
		else
			strcpy(joystickInfo[i], joyNA);
	}

	M_SetupNextMenu(&OP_JoystickSetDef);
}

static void M_Setup1PJoystickMenu(INT32 choice)
{
	setupcontrols_secondaryplayer = false;
	OP_JoystickSetDef.prevMenu = &OP_Joystick1Def;
	M_SetupJoystickMenu(choice);
}

static void M_Setup2PJoystickMenu(INT32 choice)
{
	setupcontrols_secondaryplayer = true;
	OP_JoystickSetDef.prevMenu = &OP_Joystick2Def;
	M_SetupJoystickMenu(choice);
}

static void M_AssignJoystick(INT32 choice)
{
	if (setupcontrols_secondaryplayer)
		CV_SetValue(&cv_usejoystick2, choice);
	else
		CV_SetValue(&cv_usejoystick, choice);
}

// =============
// CONTROLS MENU
// =============

static void M_Setup1PControlsMenu(INT32 choice)
{
	(void)choice;
	setupcontrols_secondaryplayer = false;
	setupcontrols = gamecontrol;        // was called from main Options (for console player, then)
	currentMenu->lastOn = itemOn;

	// Unhide the three non-P2 controls
	OP_MPControlsMenu[0].status = IT_CALL|IT_STRING2;
	OP_MPControlsMenu[1].status = IT_CALL|IT_STRING2;
	OP_MPControlsMenu[2].status = IT_CALL|IT_STRING2;
	OP_MiscControlsMenu[1].status = IT_CALL|IT_STRING2;
	OP_MiscControlsMenu[2].status = IT_CALL|IT_STRING2;

	OP_ControlListDef.prevMenu = &OP_P1ControlsDef;
	M_SetupNextMenu(&OP_ControlListDef);
}

static void M_Setup2PControlsMenu(INT32 choice)
{
	(void)choice;
	setupcontrols_secondaryplayer = true;
	setupcontrols = gamecontrolbis;
	currentMenu->lastOn = itemOn;

	// Hide the three non-P2 controls
	OP_MPControlsMenu[0].status = IT_GRAYEDOUT2;
	OP_MPControlsMenu[1].status = IT_GRAYEDOUT2;
	OP_MPControlsMenu[2].status = IT_GRAYEDOUT2;
	OP_MiscControlsMenu[1].status = IT_GRAYEDOUT2;
	OP_MiscControlsMenu[2].status = IT_GRAYEDOUT2;

	OP_ControlListDef.prevMenu = &OP_P2ControlsDef;
	M_SetupNextMenu(&OP_ControlListDef);
}

// Draws the Customise Controls menu
static void M_DrawControl(void)
{
	char     tmp[50];
	INT32    i;
	INT32    keys[2];

	// draw title, strings and submenu
	M_DrawGenericMenu();

	M_CentreText(30,
		 (setupcontrols_secondaryplayer ? "SET CONTROLS FOR SECONDARY PLAYER" :
		                                  "PRESS ENTER TO CHANGE, BACKSPACE TO CLEAR"));

	for (i = 0;i < currentMenu->numitems;i++)
	{
		if (currentMenu->menuitems[i].status != IT_CONTROL)
			continue;

		keys[0] = setupcontrols[currentMenu->menuitems[i].alphaKey][0];
		keys[1] = setupcontrols[currentMenu->menuitems[i].alphaKey][1];

		tmp[0] ='\0';
		if (keys[0] == KEY_NULL && keys[1] == KEY_NULL)
		{
			strcpy(tmp, "---");
		}
		else
		{
			if (keys[0] != KEY_NULL)
				strcat (tmp, G_KeynumToString (keys[0]));

			if (keys[0] != KEY_NULL && keys[1] != KEY_NULL)
				strcat(tmp," or ");

			if (keys[1] != KEY_NULL)
				strcat (tmp, G_KeynumToString (keys[1]));


		}
		V_DrawRightAlignedString(BASEVIDWIDTH-currentMenu->x, currentMenu->y + i*8, V_YELLOWMAP, tmp);
	}
}

static INT32 controltochange;

static void M_ChangecontrolResponse(event_t *ev)
{
	INT32        control;
	INT32        found;
	INT32        ch = ev->data1;

	// ESCAPE cancels
	if (ch != KEY_ESCAPE)
	{

		switch (ev->type)
		{
			// ignore mouse/joy movements, just get buttons
			case ev_mouse:
			case ev_mouse2:
			case ev_joystick:
			case ev_joystick2:
				ch = KEY_NULL;      // no key
			break;

			// keypad arrows are converted for the menu in cursor arrows
			// so use the event instead of ch
			case ev_keydown:
				ch = ev->data1;
			break;

			default:
			break;
		}

		control = controltochange;

		// check if we already entered this key
		found = -1;
		if (setupcontrols[control][0] ==ch)
			found = 0;
		else if (setupcontrols[control][1] ==ch)
			found = 1;
		if (found >= 0)
		{
			// replace mouse and joy clicks by double clicks
			if (ch >= KEY_MOUSE1 && ch <= KEY_MOUSE1+MOUSEBUTTONS)
				setupcontrols[control][found] = ch-KEY_MOUSE1+KEY_DBLMOUSE1;
			else if (ch >= KEY_JOY1 && ch <= KEY_JOY1+JOYBUTTONS)
				setupcontrols[control][found] = ch-KEY_JOY1+KEY_DBLJOY1;
			else if (ch >= KEY_2MOUSE1 && ch <= KEY_2MOUSE1+MOUSEBUTTONS)
				setupcontrols[control][found] = ch-KEY_2MOUSE1+KEY_DBL2MOUSE1;
			else if (ch >= KEY_2JOY1 && ch <= KEY_2JOY1+JOYBUTTONS)
				setupcontrols[control][found] = ch-KEY_2JOY1+KEY_DBL2JOY1;
		}
		else
		{
			// check if change key1 or key2, or replace the two by the new
			found = 0;
			if (setupcontrols[control][0] == KEY_NULL)
				found++;
			if (setupcontrols[control][1] == KEY_NULL)
				found++;
			if (found == 2)
			{
				found = 0;
				setupcontrols[control][1] = KEY_NULL;  //replace key 1,clear key2
			}
			G_CheckDoubleUsage(ch);
			setupcontrols[control][found] = ch;
		}

	}

	M_StopMessage(0);
}

static void M_ChangeControl(INT32 choice)
{
	static char tmp[55];

	controltochange = currentMenu->menuitems[choice].alphaKey;
	sprintf(tmp, "Hit the new key for\n%s\nESC for Cancel",
		currentMenu->menuitems[choice].text);

	M_StartMessage(tmp, M_ChangecontrolResponse, MM_EVENTHANDLER);
}

// =====
// SOUND
// =====

// Toggles sound systems in-game.
static void M_ToggleSFX(void)
{
	if (nosound)
	{
		nosound = false;
		I_StartupSound();
		if (nosound) return;
		S_Init(cv_soundvolume.value, cv_digmusicvolume.value, cv_midimusicvolume.value);
		M_StartMessage("SFX Enabled", NULL, MM_NOTHING);
	}
	else
	{
		if (sound_disabled)
		{
			sound_disabled = false;
			M_StartMessage("SFX Enabled", NULL, MM_NOTHING);
		}
		else
		{
			sound_disabled = true;
			S_StopSounds();
			M_StartMessage("SFX Disabled", NULL, MM_NOTHING);
		}
	}
}

static void M_ToggleDigital(void)
{
	if (nodigimusic)
	{
		nodigimusic = false;
		I_InitDigMusic();
		if (nodigimusic) return;
		S_Init(cv_soundvolume.value, cv_digmusicvolume.value, cv_midimusicvolume.value);
		S_StopMusic();
		S_ChangeMusic(mus_lclear, false);
		M_StartMessage("Digital Music Enabled", NULL, MM_NOTHING);
	}
	else
	{
		if (digital_disabled)
		{
			digital_disabled = false;
			M_StartMessage("Digital Music Enabled", NULL, MM_NOTHING);
		}
		else
		{
			digital_disabled = true;
			S_StopMusic();
			M_StartMessage("Digital Music Disabled", NULL, MM_NOTHING);
		}
	}
}

static void M_ToggleMIDI(void)
{
	if (nomidimusic)
	{
		nomidimusic = false;
		I_InitMIDIMusic();
		if (nomidimusic) return;
		S_Init(cv_soundvolume.value, cv_digmusicvolume.value, cv_midimusicvolume.value);
		S_ChangeMusic(mus_lclear, false);
		M_StartMessage("MIDI Music Enabled", NULL, MM_NOTHING);
	}
	else
	{
		if (music_disabled)
		{
			music_disabled = false;
			M_StartMessage("MIDI Music Enabled", NULL, MM_NOTHING);
		}
		else
		{
			music_disabled = true;
			S_StopMusic();
			M_StartMessage("MIDI Music Disabled", NULL, MM_NOTHING);
		}
	}
}

// ===============
// VIDEO MODE MENU
// ===============

//added : 30-01-98:
#define MAXCOLUMNMODES   10     //max modes displayed in one column
#define MAXMODEDESCS     (MAXCOLUMNMODES*3)

static modedesc_t modedescs[MAXMODEDESCS];

// Draw the video modes list, a-la-Quake
static void M_DrawVideoMode(void)
{
	INT32 i, j, vdup, row, col, nummodes;
	const char *desc;
	char temp[80];
	INT32 width, height;

	// draw title
	M_DrawMenuTitle();

#if (defined (__unix__) && !defined (MSDOS)) || defined (UNIXCOMMON) || defined (SDL)
	VID_PrepareModeList(); // FIXME: hack
#endif
	vidm_nummodes = 0;
	nummodes = VID_NumModes();

#ifdef _WINDOWS
	// clean that later: skip windowed mode 0, video modes menu only shows FULL SCREEN modes
	if (nummodes < 1)
	{
		// put the windowed mode so that there is at least one mode
		modedescs[0].modenum = 0;
		modedescs[0].desc = VID_GetModeName(0);
		modedescs[0].iscur = 1;
		vidm_nummodes = 1;
	}
	for (i = 1; i <= nummodes && vidm_nummodes < MAXMODEDESCS; i++)
#else
	// DOS does not skip mode 0, because mode 0 is ALWAYS present
	for (i = 0; i < nummodes && vidm_nummodes < MAXMODEDESCS; i++)
#endif
	{
		desc = VID_GetModeName(i);
		if (desc)
		{
			vdup = 0;

			// when a resolution exists both under VGA and VESA, keep the
			// VESA mode, which is always a higher modenum
			for (j = 0; j < vidm_nummodes; j++)
			{
				if (!strcmp(modedescs[j].desc, desc))
				{
					// mode(0): 320x200 is always standard VGA, not vesa
					if (modedescs[j].modenum)
					{
						modedescs[j].modenum = i;
						vdup = 1;

						if (i == vid.modenum)
							modedescs[j].iscur = 1;
					}
					else
						vdup = 1;

					break;
				}
			}

			if (!vdup)
			{
				modedescs[vidm_nummodes].modenum = i;
				modedescs[vidm_nummodes].desc = desc;
				modedescs[vidm_nummodes].iscur = 0;

				if (i == vid.modenum)
					modedescs[vidm_nummodes].iscur = 1;

				vidm_nummodes++;
			}
		}
	}

	vidm_column_size = (vidm_nummodes+2) / 3;

	row = 41;
	col = OP_VideoModeDef.y;
	for (i = 0; i < vidm_nummodes; i++)
	{
		// Pull out the width and height
		sscanf(modedescs[i].desc, "%u%*c%u", &width, &height);

		// Show multiples of 320x200 as green.
		if ((width % BASEVIDWIDTH == 0 && height % BASEVIDHEIGHT == 0) &&
			(width / BASEVIDWIDTH == height / BASEVIDHEIGHT))
			V_DrawString(row, col, modedescs[i].iscur ? V_YELLOWMAP : V_GREENMAP, modedescs[i].desc);
		else
			V_DrawString(row, col, modedescs[i].iscur ? V_YELLOWMAP : 0, modedescs[i].desc);

		col += 8;
		if ((i % vidm_column_size) == (vidm_column_size-1))
		{
			row += 7*13;
			col = 36;
		}
	}

	V_DrawCenteredString(BASEVIDWIDTH/2, 168, V_GREENMAP, "Green modes are recommended.");
	V_DrawCenteredString(BASEVIDWIDTH/2, 176, V_GREENMAP, "Non-green modes are known to cause");
	V_DrawCenteredString(BASEVIDWIDTH/2, 184, V_GREENMAP, "random crashes. Use at own risk.");

	if (vidm_testingmode > 0)
	{
		sprintf(temp, "TESTING MODE %s", modedescs[vidm_current].desc);
		M_CentreText(OP_VideoModeDef.y + 80 + 16, temp);
		M_CentreText(OP_VideoModeDef.y + 90 + 16, "Please wait 5 seconds...");
	}
	else
	{
		M_CentreText(OP_VideoModeDef.y + 60 + 16, "Press ENTER to set mode");
		M_CentreText(OP_VideoModeDef.y + 70 + 16, "T to test mode for 5 seconds");

		sprintf(temp, "D to make %s the default", VID_GetModeName(vid.modenum));
		M_CentreText(OP_VideoModeDef.y + 80 + 16,temp);

		sprintf(temp, "Current default is %dx%d (%d bits)", cv_scr_width.value,
			cv_scr_height.value, cv_scr_depth.value);
		M_CentreText(OP_VideoModeDef.y + 90 + 16,temp);

		M_CentreText(OP_VideoModeDef.y + 100 + 16,"Press ESC to exit");
	}

	// Draw the cursor for the VidMode menu
	if (skullAnimCounter < 4) // use the Skull anim counter to blink the cursor
	{
		i = 41 - 10 + ((vidm_current / vidm_column_size)*7*13);
		j = OP_VideoModeDef.y + ((vidm_current % vidm_column_size)*8);
		V_DrawCharacter(i - 8, j, '*',false);
	}
}

// special menuitem key handler for video mode list
static void M_HandleVideoMode(INT32 ch)
{
	if (vidm_testingmode > 0)
	{
		// change back to the previous mode quickly
		if (ch == KEY_ESCAPE)
		{
			setmodeneeded = vidm_previousmode + 1;
			vidm_testingmode = 0;
		}
		return;
	}

	switch (ch)
	{
		case KEY_DOWNARROW:
			S_StartSound(NULL, sfx_menu1);
			vidm_current++;
			if (vidm_current >= vidm_nummodes)
				vidm_current = 0;
			break;

		case KEY_UPARROW:
			S_StartSound(NULL, sfx_menu1);
			vidm_current--;
			if (vidm_current < 0)
				vidm_current = vidm_nummodes - 1;
			break;

		case KEY_LEFTARROW:
			S_StartSound(NULL, sfx_menu1);
			vidm_current -= vidm_column_size;
			if (vidm_current < 0)
				vidm_current = (vidm_column_size*3) + vidm_current;
			if (vidm_current >= vidm_nummodes)
				vidm_current = vidm_nummodes - 1;
			break;

		case KEY_RIGHTARROW:
			S_StartSound(NULL, sfx_menu1);
			vidm_current += vidm_column_size;
			if (vidm_current >= (vidm_column_size*3))
				vidm_current %= vidm_column_size;
			if (vidm_current >= vidm_nummodes)
				vidm_current = vidm_nummodes - 1;
			break;

		case KEY_ENTER:
			S_StartSound(NULL, sfx_menu1);
			if (!setmodeneeded) // in case the previous setmode was not finished
				setmodeneeded = modedescs[vidm_current].modenum + 1;
			break;

		case KEY_ESCAPE: // this one same as M_Responder
			if (currentMenu->prevMenu)
				M_SetupNextMenu(currentMenu->prevMenu);
			else
				M_ClearMenus(true);
			break;

		case 'T':
		case 't':
			vidm_testingmode = TICRATE*5;
			vidm_previousmode = vid.modenum;
			if (!setmodeneeded) // in case the previous setmode was not finished
				setmodeneeded = modedescs[vidm_current].modenum + 1;
			break;

		case 'D':
		case 'd':
			// current active mode becomes the default mode.
			SCR_SetDefaultMode();
			break;

		default:
			break;
	}
}

// =========
// Quit Game
// =========
static INT32 quitsounds2[8] =
{
	sfx_spring, // Tails 11-09-99
	sfx_itemup, // Tails 11-09-99
	sfx_jump, // Tails 11-09-99
	sfx_pop,
	sfx_gloop, // Tails 11-09-99
	sfx_splash, // Tails 11-09-99
	sfx_floush, // Tails 11-09-99
	sfx_chchng // Tails 11-09-99
};

void M_QuitResponse(INT32 ch)
{
	tic_t ptime;
	if (ch != 'y' && ch != KEY_ENTER)
		return;
	if (!(netgame || cv_debug))
	{
		if (quitsounds2[(gametic>>2)&7]) S_StartSound(NULL, quitsounds2[(gametic>>2)&7]); // Use quitsounds2, not quitsounds Tails 11-09-99

		//added : 12-02-98: do that instead of I_WaitVbl which does not work
		ptime = I_GetTime() + TICRATE*3; // Shortened the quit time, used to be 2 seconds Tails 03-26-2001
		while (ptime > I_GetTime())
		{
			V_DrawScaledPatch(0, 0, 0, W_CachePatchName("GAMEQUIT", PU_CACHE)); // Demo 3 Quit Screen Tails 06-16-2001
			I_FinishUpdate(); // Update the screen with the image Tails 06-19-2001
			I_Sleep();
		}
	}
	I_Quit();
}

static void M_QuitSRB2(INT32 choice)
{
	// We pick index 0 which is language sensitive, or one at random,
	// between 1 and maximum number.
	static char s[200];
	(void)choice;
	sprintf(s, text[DOSY], text[QUITMSG + (gametic % NUM_QUITMESSAGES)]);
	M_StartMessage(s, M_QuitResponse, MM_YESNO);
}

#ifdef HWRENDER
// =====================================================================
// OpenGL specific options
// =====================================================================

#define FOG_COLOR_ITEM  1
// ===================
// M_OGL_DrawFogMenu()
// ===================
static void M_OGL_DrawFogMenu(void)
{
	INT32 mx, my;

	mx = currentMenu->x;
	my = currentMenu->y;
	M_DrawGenericMenu(); // use generic drawer for cursor, items and title
	V_DrawString(BASEVIDWIDTH - mx - V_StringWidth(cv_grfogcolor.string),
		my + currentMenu->menuitems[FOG_COLOR_ITEM].alphaKey, V_YELLOWMAP, cv_grfogcolor.string);
	// blink cursor on FOG_COLOR_ITEM if selected
	if (itemOn == FOG_COLOR_ITEM && skullAnimCounter < 4)
		V_DrawCharacter(BASEVIDWIDTH - mx,
			my + currentMenu->menuitems[FOG_COLOR_ITEM].alphaKey, '_' | 0x80,false);
}

// =====================
// M_OGL_DrawColorMenu()
// =====================
static void M_OGL_DrawColorMenu(void)
{
	INT32 mx, my;

	mx = currentMenu->x;
	my = currentMenu->y;
	M_DrawGenericMenu(); // use generic drawer for cursor, items and title
	V_DrawString(mx, my + currentMenu->menuitems[0].alphaKey - 10,
		V_YELLOWMAP, "Gamma correction");
}

//===================
// M_HandleFogColor()
//===================
static void M_HandleFogColor(INT32 choice)
{
	size_t i, l;
	char temp[8];
	boolean exitmenu = false; // exit to previous menu and send name change

	switch (choice)
	{
		case KEY_DOWNARROW:
			S_StartSound(NULL, sfx_menu1);
			itemOn++;
			break;

		case KEY_UPARROW:
			S_StartSound(NULL, sfx_menu1);
			itemOn--;
			break;

		case KEY_ESCAPE:
			S_StartSound(NULL, sfx_menu1);
			exitmenu = true;
			break;

		case KEY_BACKSPACE:
			S_StartSound(NULL, sfx_menu1);
			strcpy(temp, cv_grfogcolor.string);
			strcpy(cv_grfogcolor.zstring, "000000");
			l = strlen(temp)-1;
			for (i = 0; i < l; i++)
				cv_grfogcolor.zstring[i + 6 - l] = temp[i];
			break;

		default:
			if ((choice >= '0' && choice <= '9') || (choice >= 'a' && choice <= 'f')
				|| (choice >= 'A' && choice <= 'F'))
			{
				S_StartSound(NULL, sfx_menu1);
				strcpy(temp, cv_grfogcolor.string);
				strcpy(cv_grfogcolor.zstring, "000000");
				l = strlen(temp);
				for (i = 0; i < l; i++)
					cv_grfogcolor.zstring[5 - i] = temp[l - i];
					cv_grfogcolor.zstring[5] = (char)choice;
			}
			break;
	}
	if (exitmenu)
	{
		if (currentMenu->prevMenu)
			M_SetupNextMenu(currentMenu->prevMenu);
		else
			M_ClearMenus(true);
	}
}
#endif
