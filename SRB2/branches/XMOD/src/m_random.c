// Emacs style mode select   -*- C++ -*-
//-----------------------------------------------------------------------------
//
// Copyright (C) 1993-1996 by id Software, Inc.
// Copyright (C) 1998-2000 by DooM Legacy Team.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//-----------------------------------------------------------------------------
/// \file
/// \brief LCG PRNG created for XMOD

#include "doomdef.h"
#include "doomtype.h"
#include "m_random.h"

// Holds the current seed.
static UINT32 randomseed = 0;

// Provides a random byte and sets the seed appropriately.
// The nature of this PRNG allows it to cycle through about two million numbers
// before it finally starts repeating numeric sequences.
// That's more than good enough for our purposes.
UINT8 P_Random(void)
{
	randomseed = (randomseed*746151647)+48205429;
	return (UINT8)((randomseed >> 17)%256);
}

// Returns a random number from -255 to 255.
INT32 P_SignedRandom(void)
{
	INT32 r = P_Random();
	return r - P_Random();
}

// Provides a completely (?) random byte.
// Don't use this for any gameplay logic!
UINT8 M_Random(void)
{
	// just use rand() instead
	return (UINT8)(rand()%256);
}

// Provides a random byte without saving what the seed would be.
// Used just to debug the PRNG.
UINT8 P_DebugRandom(void)
{
	UINT32 r = (randomseed*746151647)+48205429;
	return (UINT8)((r >> 17)%256);
}

// Gets the current random seed.
// Used for netgames.
UINT32 P_GetRandSeed(void)
{
	return randomseed;
}

// Sets the random seed.
// Used at the beginning of the game, and also for netgames.
void P_SetRandSeed(UINT32 seed)
{
	randomseed = seed;
}
