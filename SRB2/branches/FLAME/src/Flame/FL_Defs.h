// SRB2FLAME - Defines
// Ported from the SRB2109FLAME branch
// By Flame

#if ((defined(SMASH) && defined(ZELDA)) || (defined(ZELDA) && defined(BOMB)) || (defined(BOMB) && defined(SMASH)))
#error It is not reccomended that you compile more than two projects at once.\
		Please refine your defines and try again.
#endif

#ifdef FLAME
	// Miscellaneous things of mine.
	#define QUAKECOLOR // Colored text formatting like in Quake. (Ie; ^1<Text>, ^3<Text>)
	#define FLAMEMISC
	// #define AUTOTARGET // Code that lets you take on the mind of a JTE bot. Quite rudimentary. Should not be used under ANY circumstances.
	// #define FLAMEMISC // Miscellaneous things of mine. The "Possibly might be used in the future" list
	// #define WATERSLIDE // Water Slide!! Slip and slide all over the place.

	#define FLAMEBOSS // One boss I have longed to make.

	// change slope
	// dist = P_AproxDistance(P_AproxDistance([SOURCE]->tracer->x - [SOURCE]->x, [SOURCE]->tracer->y - [SOURCE]->y), [SOURCE]->tracer->z - [SOURCE]->z);
	// I MIGHT be able to use this for something in the future. Along with FINESINE(dist) [S: O/H, aiming] and FINECOSINE(dist) [C: A/H, distance]
#endif

#ifdef SMASH
	#define SMASHCAM // Smash Bros camera.
	#define SPRITEROLL // Turtle Man: Sprite/Model Rotation. Think loops.

	// Sprite Scaling!
	#define SCALESIZE // Copied from SRB2JTE made by JTE.
	#define SCALESPEEDS // Copied from SRB2JTE made by JTE.
	#define SCALEGRAVITY // Copied from SRB2JTE made by JTE.
	#define SCALECVAR // Turtle Man: Changed "numlaps" to "scale". Disable for RELEASE.

	// Old and/or unused define statements.
	// #define CHAOSBLAST_OLD // Old version of how the chaos blast orb was spawned. The new one uses a modified armegeddon shield blast.

	#define SHIELDLIFE (8*TICRATE) // How long (in seconds) does your sheild last until BUST?
	#define FLAMEMISC
#endif

#ifdef BOMB
	// Camera Modifiers
	//#define OLDBOMBCAMFIXED // Camera focused on a fixed point. Like the lookaway view camera.
	#define BOMBCAMDYNAMIC // Camera following the player
	#if (defined(BOMBCAMFIXED) || defined(BOMBCAMDYNAMIC))
		#define BOMBCAMCVAR // Bomberman Camera console variable
	#endif

	#define FLAMEMISC

	//#define REVERSI // New Bomberman game mode: Reversi battle mode!
#endif

#ifdef ZELDA
	#define HEARTSEGS (2) // How many peices are hearts broken up into?
	#define HEARTS(x) (x*HEARTSEGS) // How many hearts are there?
	#define FLAMEMISC
#endif

#ifdef FLAMEMISC
	#define PARTICLES // Shuffle's partices!
	//#define PRVMSG // Like the command 'SAYTO' command but only works whoever has the EXE with this define.
					// This is best for development on a network protocol. DISABLE FOR RELEASE
#endif
