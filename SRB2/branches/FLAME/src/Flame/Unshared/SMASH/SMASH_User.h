// SRB2FLAME - Unique code for SSB:SS (SMASH)
// Code which is unique/exclusive to its define.
// By Flame

#ifdef SMASH

#ifndef __SMASH_USER__
#define __SMASH_USER__

#ifdef SMASHCAM
fixed_t P_SSBSSCamera(camera_t *thiscam);
#endif
void P_SmashEtc(player_t *player, ticcmd_t *cmd);
void P_PlayerSpecialAtk(player_t *player, ticcmd_t *cmd);
#endif // __SMASH_USER__

#endif // SMASH
