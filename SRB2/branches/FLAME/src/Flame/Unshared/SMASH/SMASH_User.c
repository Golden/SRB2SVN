// SRB2FLAME - Unique code for SSB:SS (SMASH)
// Code which is unique/exclusive to its define.
// By Flame

#include "../../../doomstat.h"
#include "../../../doomdef.h"

#include "../../../g_game.h"
#include "../../../p_local.h"
#include "../../../r_main.h"
#include "../../../z_zone.h"
#include "../../../s_sound.h"

#include "../../../r_sky.h"
#include "../../../i_video.h"

#include "../../FL_Common.h"
#include "SMASH_User.h"

#if defined(SMASH)

//
// P_SetAllSectorLightLevel
// Fades the light level in EVERY sector to a new value
// param level The final light value in these sectors.
//
static void P_SetAllSectorLightlevel(INT32 level)
{
	size_t k = 0;
	sector_t *sector = &sectors[k];

	for (k = 0; k < numsectors; k++, sector++)
	{
		//P_RemoveLighting(sector); // remove the old lighting effect first
		if (sector->ceilingpic == skyflatnum) // Only for the sky.
		{
			if (rendermode == render_soft)
				sectors[k].lightlevel = level;
			else
				sectors[k].lightlevel = level - 30;
		}
		else // Everything else is a little darker
		{
			if (rendermode == render_soft)
				sectors[k].lightlevel = level - 40;
			else // extremly darker in OpenGL.
				sectors[k].lightlevel = level - 60;
		}
	}
}

//
// P_LookForPlayersH
// Looks for a player to hit, if true we go after the player - Used in P_PlayerSpecialAtk
//
static boolean P_LookForPlayersH(player_t *player)
{
	mobj_t *mo;
	thinker_t *think;
	mobj_t *closestmo = NULL;

	for (think = thinkercap.next; think != &thinkercap; think = think->next)
	{
		if (think->function.acp1 != (actionf_p1)P_MobjThinker)
			continue; // not a mobj thinker

		mo = (mobj_t *)think;

		if (mo->type != MT_PLAYER)
			continue;

		if (mo == player->mo)
			continue;

		if (!mo->player)
			continue;

		if (mo->health <= 0) // dead
			continue;

		if (mo->player->powers[pw_super]
		|| mo->player->powers[pw_flashing]
		|| mo->player->powers[pw_invulnerability]
		|| (cv_matchtype.value == 1 && mo->player->skincolor == player->skincolor)
		|| (cv_matchtype.value == 2 && mo->player->skin == player->skin))
			continue;

		if (mo->y != player->mo->y)
			continue;

		//if (mo->z > player->mo->z+MAXSTEPMOVE)
		//	continue; // Don't home upwards!

		if (P_AproxDistance(P_AproxDistance(player->mo->x-mo->x, player->mo->y-mo->y),
			player->mo->z-mo->z) > RING_DIST)
			continue; // out of range

		if (closestmo && P_AproxDistance(P_AproxDistance(player->mo->x-mo->x, player->mo->y-mo->y),
			player->mo->z-mo->z) > P_AproxDistance(P_AproxDistance(player->mo->x-closestmo->x,
			player->mo->y-closestmo->y), player->mo->z-closestmo->z))
			continue;

		if (!P_CheckSight(player->mo, mo))
			continue; // out of sight

		closestmo = mo;
	}

	if (closestmo)
	{
		// Found a target monster
		P_SetTarget(&player->mo->tracer, closestmo);
		player->mo->angle = R_PointToAngle2(player->mo->x, player->mo->y, player->mo->tracer->x, player->mo->tracer->y);
		return true;
	}

	return false;
}

#ifdef SMASHCAM
#define CAMERA_MAXMOVE (numplayers == 1 ? 20*FRACUNIT : (players[displayplayer].powers[pw_flashing] || players[displayplayer].powers[pw_super] ? 30*FRACUNIT : (players[secondarydisplayplayer].powers[pw_flashing] || players[secondarydisplayplayer].powers[pw_super]) && twodlevel ? 30*FRACUNIT : 18*FRACUNIT))
fixed_t P_SSBSSCamera(camera_t *thiscam) // Dynamic 2D camera.
{
	// Oh boy, lets set up what we will need..
	size_t i, numplayers = 0;
	fixed_t numpeeps = FRACUNIT, current = 0;
	fixed_t centerX = 0, farthestplayerX = 0;
	fixed_t centerZ = 0, farthestplayerZ = 0;
	fixed_t NewDist;
	mobj_t *cammin = P_GetMobjForType(MT_CAMMIN);
	mobj_t *cammax = P_GetMobjForType(MT_CAMMAX);

	// Find the center of all of the players and use that as the focus point.
	// SSNTails 07-21-2006
	for (i = 0; i < MAXPLAYERS; i++) // Calculate central X and central Z
	{
		if (!playeringame[i])
			continue;

		if (!players[i].mo)
			continue;

		if (!players[i].lives)
			continue;

		if (!players[i].health)
			continue;

		numplayers++;

		// Is the player of Espio using his/her final smash?
		if ((players[i].mo->flameflags & FLF_FINALSMASHUSE)
			&& ((players[i].mo->flags2 & MF2_DONTDRAW) == MF2_DONTDRAW)
			&& players[i].skin == 5)
			continue; // Ignore them, look for the next player.

		centerX = FixedMul(centerX, FixedDiv(numpeeps-FRACUNIT, numpeeps));
		centerX += FixedMul(players[i].mo->x, FixedDiv(FRACUNIT, numpeeps));

		centerZ = FixedMul(centerZ, FixedDiv(numpeeps-FRACUNIT, numpeeps));
		centerZ += FixedMul(players[i].mo->z, FixedDiv(FRACUNIT, numpeeps));

		numpeeps += FRACUNIT;
	}

	// Don't move the camera when all of the players are dead.
	if (numplayers == 0)
		return 0;

	// Move the camera's X
	{
		fixed_t dist = centerX-(thiscam->x+thiscam->momx);

		if (dist > 0)
		{
			if (dist > CAMERA_MAXMOVE)
				thiscam->x += CAMERA_MAXMOVE;
			else
				thiscam->x += dist;
		}
		else if (dist < 0)
		{
			if (dist < -CAMERA_MAXMOVE)
				thiscam->x += -CAMERA_MAXMOVE;
			else
				thiscam->x += dist;
		}

		if (cammin && thiscam->x < cammin->x)
			thiscam->x = cammin->x;
		else if (cammax && thiscam->x > cammax->x)
			thiscam->x = cammax->x;
	}

	// Move the camera's Z
	centerZ += cv_cam_height.value;
	{
		fixed_t dist = centerZ-(thiscam->z+thiscam->momz);

		if (dist > 0)
		{
			if (dist > CAMERA_MAXMOVE)
				thiscam->z += CAMERA_MAXMOVE;
			else
				thiscam->z += dist;
		}
		else if (dist < 0)
		{
			if (dist < -CAMERA_MAXMOVE)
				thiscam->z += -CAMERA_MAXMOVE;
			else
				thiscam->z += dist;
		}

		if (cammin && thiscam->z < cammin->z)
		{
			thiscam->z = cammin->z;
		}
		else if (cammax && thiscam->z > cammax->z)
		{
			thiscam->z = cammax->z;
		}
	}

	for (i = 0; i < MAXPLAYERS; i++) // Calculate farthest players
	{
		if (!playeringame[i])
			continue;

		if (!players[i].mo)
			continue;

		if (!players[i].lives)
			continue;

		if (!players[i].health)
			continue;

		if ((players[i].mo->flameflags & FLF_FINALSMASHUSE)
			&& ((players[i].mo->flags2 & MF2_DONTDRAW) == MF2_DONTDRAW)
			&& players[i].skin == 5)
			continue;

		current = abs(players[i].mo->x-centerX);

		if (current > farthestplayerX)
			farthestplayerX = current;

		current = abs(players[i].mo->z-centerZ);

		if (current > farthestplayerZ)
			farthestplayerZ = current;
	}

	// Subtract a little so the player isn't right on the edge of the camera.
	NewDist = -(farthestplayerX + farthestplayerZ + 64*FRACUNIT);

	// Move the camera's Y
	{
		fixed_t dist = NewDist-(thiscam->y+thiscam->momy);

		if (dist > 0)
		{
			if (dist > CAMERA_MAXMOVE)
				thiscam->y += CAMERA_MAXMOVE;
			else
				thiscam->y += dist;
		}
		else if (dist < 0)
		{
			if (dist < -CAMERA_MAXMOVE)
				thiscam->y += -CAMERA_MAXMOVE;
			else
				thiscam->y += dist;
		}

		// This may seem backward but its not.
		if (cammin && thiscam->y > cammin->y)
		{
			thiscam->y = cammin->y;
		}
		else if (cammax && thiscam->y < cammax->y)
		{
			thiscam->y = cammax->y;
		}
	}

	return 0;
}
#undef CAMERA_MAXMOVE
#endif // SMASHCAM

//
// P_SmashEtc
//
// Handles lighting and darkening, for when the player has powers[pw_finalsmash]
// And some player->shielding stuff.
//
void P_SmashEtc(player_t *player, ticcmd_t *cmd)
{
	mobj_t *mo;
	mo = player->mo;

	if (mo->fuse)
		mo->fuse--;

	////////////////////////////
	// Lighting and darkening //
	////////////////////////////
	if (player->powers[pw_finalsmash] || player->powers[pw_super])
	{
		if (player->powers[pw_finalsmash])
		{
			if (player->powers[pw_sectorlight] > 170)
				player->powers[pw_sectorlight] -= 4; // Smooth fade to dark.
			else if (!player->powers[pw_sectorlight])
				player->powers[pw_sectorlight] = 255;
		}
		else if (player->powers[pw_super])
		{
			if (player->powers[pw_sectorlight] < 255)
				player->powers[pw_sectorlight] += 4; // Smooth fade to light.
		}

		P_SetAllSectorLightlevel(player->powers[pw_sectorlight]);
	}
	else if (!(player->powers[pw_finalsmash] || player->powers[pw_super]) && player->powers[pw_sectorlight] != 0)
	{
		if (player->powers[pw_sectorlight] < 255)
			player->powers[pw_sectorlight] += 4;

		P_SetAllSectorLightlevel(player->powers[pw_sectorlight]);
	}

	//////////////////
	// Shield stuff //
	//////////////////
	if (!player->exiting && !(mo->state == &states[mo->info->painstate]
		|| player->powers[pw_flashing] || player->powers[pw_super])
		&& !(mo->reactiontime || player->pflags & PF_STASIS || player->powers[pw_nocontrol]))
	{
		if ((cmd->buttons & BT_SHIELD) && player->speed < 10 && !mo->momz && P_IsObjectOnGround(mo) && !player->shielding)
		{
			P_ResetScore(player);
			// P_SetPlayerMobjState(mo, S_PLAY_GUARD);
			P_SetPlayerMobjState(mo, S_PLAY_STND);
			player->shielding = true;
		}
		else if ((cmd->buttons & BT_SHIELD) && player->shielding)
		{
			mo->momx = player->cmomx;
			mo->momy = player->cmomy;

			if (!player->shieldspawned)
			{
				/*mobj_t *item;
				// Now spawn the shield circle.
				P_SpawnSpinMobj(player, MT_SHIELD);*/
				player->shieldspawned = true;
			}
		}
		else if (!(cmd->buttons & BT_SHIELD) && player->shielding && player->shieldspawned)
		{
			//P_SetMobjState(item, S_DISS);
			player->shielding = false;
			player->shieldspawned = false;
		}

		if (player->shielding && player->shieldspawned && player->shieldtimer <= 120) // SHIELD BREAK!
		{
			//S_PlaySound(NULL, sfx_shieldbreak);
			P_SetObjectMomZ(mo, 10*FRACUNIT, false);
			P_SetPlayerMobjState(mo, S_PLAY_PAIN);
			player->shielding = false;
			player->shieldspawned = false;
			player->shieldtimer = SHIELDLIFE;
			mo->reactiontime = SHIELDLIFE/2; // Make the player unable to move!
		}
	}
}

//
// P_PlayerSpecialAtk
//
// Variety of Special Attacks are listed here. Common stuff for Smash bros.
//
// Standard B is a special homing attack (Does not apply for Tails, Knuckles, Espio or Amy)
// Up + B launches you into the air
// Side + B brings you into a spindash (Does not apply for Amy)
// Down + B puts you into a spindash and revs it up. (Does not apply for Amy)
// Down B, standard B, side B, and up B all in one if statement! :D
//
void P_PlayerSpecialAtk(player_t *player, ticcmd_t *cmd)
{
	mobj_t *mo;
	mo = player->mo;

	if (player->pflags & PF_STASIS || player->powers[pw_nocontrol])
		return;

	/////////////////////
	// SPECIAL ATTACKS //
	/////////////////////
	if ((twodlevel || (mo->flags2 & MF2_TWOD)) // This moveset is for 2D levels ONLY
	&& !(mo->state-states == S_PLAY_PAIN
	|| (player->powers[pw_super] && player->skin == 0) // If the player isn't super and if the skin isn't sonic
	|| mo->reactiontime // Can you move!?
	|| player->exiting
	|| player->shielding // Do not do anything if you're shielding
	|| (mo->flameflags & FLF_SMASHABILITYUSE))) // The player didn't already use their Up + B special
	{
		// Standard B
		// Homing attack
		if ((cmd->buttons & BT_B)
		&& !cmd->sidemove && !cmd->forwardmove
		&& !((cmd->buttons & BT_DUCK)
		|| (player->pflags & PF_JUMPDOWN)
		|| (player->pflags & PF_STARTDASH)
		|| (player->pflags & PF_SPINNING)
		|| (player->pflags & PF_USEDOWN)
		|| player->spectator))
		{
			if ((player->pflags & PF_SUPERREADY) && !player->powers[pw_super] // Ready to turn super and not super already?
			&& !player->powers[pw_jumpshield] && !player->powers[pw_forceshield]
			&& !player->powers[pw_watershield] && !player->powers[pw_ringshield]
			&& !player->powers[pw_bombshield] && !player->powers[pw_invulnerability]
			&& !(maptol & TOL_NIGHTS) // don't turn 'regular super' in nights levels
			/*&& (player->charflags & SF_ALLOWSUPER)*/ && player->powers[pw_finalsmash])
			{
				P_ResetPlayer(player);
				mo->momx = mo->momy = mo->momz = 0;
				P_DoSuperTransformation(player, true);
				player->powers[pw_finalsmash] = false;
				player->pflags |= PF_USEDOWN;
				player->pflags &= ~PF_SUPERREADY;
				if (player->skin != 3)
					mo->reactiontime = TICRATE;
			}
			else if (/*(player->charflags & SF_HOMING)*/ // Only for characters with a homing ability
			(player->charability == CA_HOMINGTHOK)
			&& !(player->pflags & PF_JUMPDOWN)
			&& (player->skin == 0
			|| player->skin == 3)
			&& !(player->pflags & PF_SPINNING)
			&& !player->weapondelay)
			{
				mo->momx /= 2;
				// Special Homing attack routine:
				if (P_IsObjectOnGround(mo)) // First check if the player is on the ground
				{
					if (mo->eflags & MFE_UNDERWATER)
						P_SetObjectMomZ(mo, 8*FRACUNIT, false);
					else
						P_SetObjectMomZ(mo, 6*FRACUNIT, false);
				}
				else if (!P_IsObjectOnGround(mo)) // Otherwise if not on the ground...
				{
					// Smooth stop in the air
					/*if (mo->momz < 0)
						mo->momz += FRACUNIT
					if (mo->momz > 0)
						mo->momz -= FRACUNIT;
					else if (mo->momz == 0)*/
						mo->momz = 0;
				}
				S_StartSound(player, sfx_spin);
				P_SetPlayerMobjState(mo, S_PLAY_ATK1); // Have the player rise up and go into a spin.

				player->pflags |= PF_JUMPED;

				if (((player->charability == CA_HOMINGTHOK))
				&& !player->homing)
				{
					// Search for any other players in range and NOT YOURSELF
					if (P_LookForPlayersH(player))
					{
						if (mo->tracer) // Have a tracer?
							player->homing = TICRATE;// Then do the homing attack function!
					}
				}
				mo->flameflags |= FLF_SMASHABILITYUSE;
				player->secondjump = 1; // Prevent the double jump
			}
			player->pflags |= PF_USEDOWN;
		}
		// Up + B
		// Catapult Launch
		else if ((player->pflags & PF_JUMPDOWN) && cmd->buttons & BT_B
		&& !((player->pflags & PF_STARTDASH)
		|| (player->pflags & PF_SPINNING)
		|| (player->pflags & PF_USEDOWN)))
		{
			// Catapult the player upward
			/*if (mo->eflags & MFE_UNDERWATER)
				P_SetObjectMomZ(mo, (player->jumpfactor/5)*(FRACUNIT/2), false);
			else*/
				P_SetObjectMomZ(mo, (player->jumpfactor/6)*FRACUNIT, false);

			if (player->speed >= 8)
			{
				if (mo->angle == 0)
					mo->momx = 5*FRACUNIT;
				else if (mo->angle == ANGLE_180)
					mo->momx = -(5*FRACUNIT);
			}

			if (mo->info->attacksound && !player->spectator)
				S_StartSound(mo, mo->info->attacksound);

			P_SetPlayerMobjState(mo, S_PLAY_ATK1);
			player->pflags &= ~PF_JUMPED; // If true, the jump height can be cut, we don't want that.
			player->secondjump = 1; // Prevent the double jump
			mo->flameflags |= FLF_SMASHABILITYUSE;
			player->pflags |= PF_THOKKED;
		}
		// If not moving up or down, and travelling faster than a speed of four while not holding
		// down the spin button and not spinning.
		// AKA Just go into a spin on the ground, you idiot. ;)
		// Side + B
		// Spindash
		else if ((cmd->buttons & BT_B)
		&& !player->climbing
		//&& !mo->momz && onground // You CAN be in the air to use this now.
		&& cmd->sidemove
		&& (player->charability2 == CA2_SPINDASH)
		&& !((player->pflags & PF_JUMPDOWN)
		|| (player->pflags & PF_USEDOWN)
		|| (player->pflags & PF_SPINNING)))
		{
			// Old stuff here for refrence.
			/* P_ResetScore(player);
			 * player->pflags |= PF_SPINNING;
			 * P_SetPlayerMobjState(mo, S_PLAY_ATK1);
			 * S_StartSound(mo, sfx_spin);
			 * player->pflags |= PF_USEDOWN;
			 * if (mo->eflags & MF_VERTICALFLIP)
			 * 	mo->z = mo->ceilingz - P_GetPlayerSpinHeight(player);
			 * mo->flameflags |= FLF_SMASHABILITYUSE; */

			if (cmd->buttons & BT_B) // Then start the spindash with B
			{
				P_ResetScore(player);
				player->mo->momx = player->cmomx;
				player->mo->momy = player->cmomy;
				player->pflags |= PF_STARTDASH;
				player->pflags |= PF_SPINNING;
				player->dashspeed = FRACUNIT/NEWTICRATERATIO;
				P_SetPlayerMobjState(player->mo, S_PLAY_ATK1);
				player->pflags |= PF_USEDOWN;
				if (player->mo->eflags & MFE_VERTICALFLIP)
					player->mo->z = player->mo->ceilingz - P_GetPlayerSpinHeight(player);
				player->sidespin = true; // Be prepared to bounce if airborne.
			}
		}
		// Spin Charge
		else if (!cmd->sidemove
		&& (player->charability2 == CA2_SPINDASH)
		&& (cmd->buttons & BT_DUCK) // Duck first...
		&& !((player->pflags & PF_JUMPDOWN)
		|| (player->pflags & PF_USEDOWN)
		|| (player->pflags & PF_SPINNING)))
		{
			if (cmd->buttons & BT_B) // Then start the spin charge with B
			{
				P_ResetScore(player);
				player->mo->momx = player->cmomx;
				player->mo->momy = player->cmomy;
				player->pflags |= PF_STARTDASH;
				player->pflags |= PF_SPINNING;
				player->dashspeed = FRACUNIT/NEWTICRATERATIO;
				P_SetPlayerMobjState(player->mo, S_PLAY_ATK1);
				player->pflags |= PF_USEDOWN;
				if (player->mo->eflags & MFE_VERTICALFLIP)
					player->mo->z = player->mo->ceilingz - P_GetPlayerSpinHeight(player);
				player->sidespin = false; // Be prepared to NOT bounce if airborne.
			}
			else if (P_IsObjectOnGround(mo) && !(player->pflags & PF_STARTDASH || player->pflags & PF_SPINNING))
			{
				player->dashspeed = 0;
				P_SetPlayerMobjState(mo, S_PLAY_ATK4); // Ducking
				//P_GetPlayerSpinHeight(player);
				mo->height = P_GetPlayerSpinHeight(player);
				mo->momx = player->speed = cmd->sidemove /= 2;
				//CONS_Printf("The player is ready to spindash\n");
			}
		}
		else if ((cmd->buttons & BT_B) && (player->pflags & PF_STARTDASH)) // Rev up the spindash
		{
			player->dashspeed += FRACUNIT/NEWTICRATERATIO;

			if ((leveltime % (TICRATE/10)) == 0)
			{
				if (!player->spectator)
					S_StartSound(mo, sfx_spndsh); // Make the rev sound!

				if (!(mo->flameflags & FLF_FINALSMASHUSE && player->skin == 5) && !player->spectator)
					P_SpawnGhostMobj(mo);
			}
		}
	}
	else if (!(twodlevel || (mo->flags2 & MF2_TWOD)) // If it's NOT a 2D level...
	&& !(mo->state-states == S_PLAY_PAIN
	|| (player->powers[pw_super] && player->skin == 0)
	|| mo->reactiontime // Can you move!?
	|| player->exiting
	|| player->shielding))
	{

		// You just have your Down + B which is changed to your standard B and Side/Moving B move.
		// Standard B
		// Spin Charge
		if ((cmd->buttons & BT_B)
		&& !(player->pflags & PF_USEDOWN)
		&& player->speed < 5)
		{
			if ((player->pflags & PF_SUPERREADY) && !player->powers[pw_super] // Ready to turn super and not super already?
			&& !player->powers[pw_jumpshield] && !player->powers[pw_forceshield]
			&& !player->powers[pw_watershield] && !player->powers[pw_ringshield]
			&& !player->powers[pw_bombshield] && !player->powers[pw_invulnerability]
			&& !(maptol & TOL_NIGHTS) // don't turn 'regular super' in nights levels
			/*&& (player->charflags & SF_ALLOWSUPER)*/ && player->powers[pw_finalsmash])
			{
				P_ResetPlayer(player);
				mo->momx = mo->momy = mo->momz = 0;
				P_DoSuperTransformation(player, true);
				player->powers[pw_finalsmash] = false;
				player->pflags |= PF_USEDOWN;
				player->pflags &= ~PF_SUPERREADY;
				if (player->skin != 3)
					mo->reactiontime = TICRATE;
			}
			else if ((player->charability2 == CA2_SPINDASH)
			&& player->speed < 5
			&& !mo->momz && P_IsObjectOnGround(mo)
			&& !(player->pflags & PF_SPINNING))
			{
				P_ResetScore(player);
				mo->momx = player->cmomx;
				mo->momy = player->cmomy;
				player->pflags |= PF_STARTDASH;
				player->pflags |= PF_SPINNING;
				player->dashspeed = FRACUNIT/NEWTICRATERATIO;
				P_SetPlayerMobjState(mo, S_PLAY_ATK1);
				player->pflags |= PF_USEDOWN;
				if (mo->eflags & MFE_VERTICALFLIP)
					mo->z = mo->ceilingz - P_GetPlayerSpinHeight(player);
			}
		}
		else if ((cmd->buttons & BT_B) && (player->pflags & PF_STARTDASH)) // Rev up the spindash
		{
			player->dashspeed += FRACUNIT/NEWTICRATERATIO;

			if ((leveltime % (TICRATE/10)) == 0)
			{
				if (!player->spectator)
					S_StartSound(mo, sfx_spndsh); // Make the rev sound!
				// Now spawn the color thok circle.

				if (!(mo->flameflags & FLF_FINALSMASHUSE && (player->skin == 5)))
					P_SpawnGhostMobj(mo);
			}
		}
		// If not moving up or down, and travelling faster than a speed of four while not holding
		// down the spin button and not spinning.
		// AKA Just go into a spin on the ground, you idiot. ;)
		// Side/Moving + B
		// Spin Dash
		else if ((cmd->buttons & BT_B)
		&& !player->climbing
		&& !mo->momz && P_IsObjectOnGround(mo) && player->speed > 5
		&& !(player->pflags & PF_USEDOWN) && !(player->pflags & PF_SPINNING) && (player->charability2 == CA2_SPINDASH))
		{
			P_ResetScore(player);
			player->pflags |= PF_SPINNING;
			P_SetPlayerMobjState(mo, S_PLAY_ATK1);
			if (!player->spectator)
				S_StartSound(mo, sfx_spin);
			player->pflags |= PF_USEDOWN;
			if (mo->eflags & MFE_VERTICALFLIP)
				mo->z = mo->ceilingz - P_GetPlayerSpinHeight(player);
		}
	}

	// Oh dear, important collision detection with the ground?
	if (P_IsObjectOnGround(mo) && (player->pflags & PF_SPINNING) && !(player->pflags & PF_STARTDASH)
		&& (player->rmomx < 5*FRACUNIT/NEWTICRATERATIO
		&& player->rmomx > -5*FRACUNIT/NEWTICRATERATIO)
		&& (player->rmomy < 5*FRACUNIT/NEWTICRATERATIO
		&& player->rmomy > -5*FRACUNIT/NEWTICRATERATIO))
	{
		if (GETSECSPECIAL(player->mo->subsector->sector->special, 4) == 7 || (mo->ceilingz - mo->floorz < P_GetPlayerHeight(player)))
			P_InstaThrust(mo, mo->angle, 10*FRACUNIT);
		else
		{
			player->pflags &= ~PF_SPINNING;
			P_SetPlayerMobjState(mo, S_PLAY_STND);
			mo->momx = player->cmomx;
			mo->momy = player->cmomy;
			P_ResetScore(player);
			if (mo->eflags & MFE_VERTICALFLIP)
				mo->z = player->mo->ceilingz - P_GetPlayerHeight(player);
		}
	}

	// Catapult the player from a spindash rev!
	if (!(player->pflags & PF_USEDOWN) && player->dashspeed && (player->pflags & PF_STARTDASH) && (player->pflags & PF_SPINNING))
	{
		if (player->powers[pw_ingoop])
			player->dashspeed = 0;

		player->pflags &= ~PF_STARTDASH;
		if (!(gametype == GT_RACE && leveltime < 4*TICRATE))
		{
			if (!P_IsObjectOnGround(mo))
			{
				if (player->sidespin == true)
					P_SetObjectMomZ(player->mo, 8*FRACUNIT, false);
				else // Just to be nice.
					P_SetObjectMomZ(player->mo, FRACUNIT, false);
				P_InstaThrust(player->mo, player->mo->angle, player->dashspeed/2+1); // catapult forward ho!!
				player->mo->flameflags |= FLF_SMASHABILITYUSE;
				player->secondjump = 1; // Prevent double jump.
			}
			else
				P_InstaThrust(player->mo, player->mo->angle, player->dashspeed); // catapult forward ho!!

			if (!player->spectator)
				S_StartSound(mo, sfx_zoom);
		}
		player->dashspeed = 0;
	}

	if ((player->pflags & PF_SPINNING)
		&& !(player->mo->state >= &states[S_PLAY_ATK1]
		&& player->mo->state <= &states[S_PLAY_ATK4]))
		P_SetPlayerMobjState(player->mo, S_PLAY_ATK1);
}
#endif // SMASH
