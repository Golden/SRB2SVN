// SRB2FLAME - Unique code for TML - The Missing Link (ZELDA)
// Code which is unique/exclusive to its define.
// "The missing link, 'cause Link lost the chain links on his necklace."
// By Shuffle, modified by Flame


#include "../../../doomstat.h"
#include "../../../byteptr.h"
#include "../../../doomdef.h"
#include "../../../command.h"
#include "../../../d_player.h"
#include "../../../d_netcmd.h"
#include "../../../g_game.h"

#include "TML_Netcmd.h"

#ifdef ZELDA

static void Got_Equip(UINT8 **cp, INT32 playernum);
static void Equip_OnChange(void);

static CV_PossibleValue_t zeldaequip_cons_t[] = {{0, "MIN"}, {3, "MAX"}, {0, NULL}}; // I'm assuming; 0 = Nothing, 1 = Bombs, 2 = Arrows, 3 = Fire wand
consvar_t cv_zeldaequip = {"equip", "0", CV_CALL|CV_NOINIT|CV_HIDEN|CV_NOSHOWHELP, zeldaequip_cons_t, Equip_OnChange, 0, NULL, NULL, 0, 0, NULL};

static void Equip_OnChange(void)
{
	XBOXSTATIC UINT8 buf[1];

	// Do you have your it in your inventory? Are you equipping it?
	if (!(players[consoleplayer].equips & cv_zeldaequip.value<<1))
		return;
	if (cv_debug)
		CONS_Printf("Sent equip packet!\n");
	buf[0] = cv_zeldaequip.value;
	SendNetXCmd(XD_ZELDAEQUIP, &buf, 1);
}

void Zelda_NetInit(void)
{
	CV_RegisterVar(&cv_zeldaequip);
	RegisterNetXCmd(XD_ZELDAEQUIP, Got_Equip);
}

static void Got_Equip(UINT8 **cp, INT32 playernum)
{
	player_t *player = &players[playernum];
	if (cv_debug)
		CONS_Printf("Recieving equip packet!\n");
	player->equipped = READUINT8(*cp);
}
#endif
