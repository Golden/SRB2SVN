// SRB2FLAME - Unique code for TML - The Missing Link (ZELDA)
// Code which is unique/exclusive to its define.
// "The missing link, 'cause Link lost the chain links on his necklace."
// By Shuffle, modified by Flame

#ifdef ZELDA

#ifndef __TML_NETCMD__
#define __TML_NETCMD__
#include "../../../command.h"

extern consvar_t cv_zeldaequip;
void Zelda_NetInit(void);

#endif // __TML_NETCMD__
#endif // ZELDA
