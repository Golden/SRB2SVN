// SRB2FLAME - Unique code for Super Bomberman Blast 2 (BOMB)
// Code which is unique/exclusive to its define.
// By Flame

#include "../../../doomstat.h"
#include "../../../doomdata.h"
#include "../../../g_game.h"
#include "../../../m_random.h"
#include "../../../p_mobj.h"
#include "../../../p_local.h" // P_CanPickupItem
#include "../../../s_sound.h"

#include "../../FL_Common.h"
#include "../../FL_Info.h"
#include "../../FL_Misc.h" // PARTICLES
#include "BOMB_Mobj.h"
#include "BOMB_Netcmd.h"

#ifdef BOMB

//
// Check to see if it's a Bomberman-type Mobj
// If it is, it returns true. Otherwise it's not.
//
boolean P_IsBomberMobj(mobj_t *mobj)
{
	if ((mobj->flameflags == FLF_BOMBKICK) // Bombs
		|| (mobj->type == MT_MINEBOMB)

		// Fire debris
		|| (mobj->type == MT_REDEXPLODE)
		|| (mobj->type == MT_BLUEXPLODE)

		// Panels
		|| (mobj->type == MT_BOMBUP)
		|| (mobj->type == MT_FIREUP)
		|| (mobj->type == MT_QUESTIONPANEL)
		|| (mobj->type == MT_RUBBERBOMBP)
		|| (mobj->type == MT_POWERBOMBP)
		|| (mobj->type == MT_MINEBOMBP)
		|| (mobj->type == MT_REMOTEBOMBP)
		|| (mobj->type == MT_SPIKEBOMBP)

		// Enemies
		|| (mobj->type == MT_BAROM))
		return true;
	else
		return false;
}

//
// P_BombPanelCollect
//
// /** Copied from P_TouchSpecialThing **/
// Collect item panels!
//
void P_BombPanelCollect(mobj_t *mobj, mobj_t *toucher)
{
	player_t *player = toucher->player;
	sfxenum_t sound = sfx_None;
	mobj_t *temp;

	if (cv_objectplace.value)
		return;

	// Dead thing touching.
	// Can happen with a sliding player corpse.
	if (toucher->health <= 0)
		return;

	// Check for the height of these panels in relation to where the player is.
	if (toucher->z > (mobj->z + mobj->height))
		return;
	if (mobj->z > (toucher->z + toucher->height))
		return;

	if (mobj->health <= 0)
		return;

	if (!player) // Only players can touch stuff!
		return;

	if ((netgame || multiplayer) && player->spectator)
		return;

	if (mobj->state == &states[S_DISS]) // Don't collect if in "disappearing" mode
		return;

	switch (mobj->type)
	{
		case MT_BOMBUP:
			if (!(P_CanPickupItem(toucher->player, true)))
				return;
			if (player->bombcount > MAXBOMBS) // Wut?
				return;

			sound = sfx_bgetitem;
			player->bombcount += mobj->reactiontime;
			if (player->bombcount >= MAXBOMBS)
				player->bombcount = MAXBOMBS;

			temp = P_SpawnMobj(mobj->x, mobj->y, mobj->z, mobj->type);
			temp->flags &= ~MF_SPECIAL;
			temp->health = 0;
			temp->momz = FRACUNIT;
			temp->fuse = mobj->info->damage;

			if (mobj->eflags & MFE_VERTICALFLIP)
				temp->momz = -temp->momz;

			P_SetMobjState(temp, mobj->info->raisestate);
			break;
		case MT_FIREUP:
		// case MT_FULLFIRE:
			if (!(P_CanPickupItem(toucher->player, true)))
				return;
			if (player->firepower > MAXFIREPOWER) // Wut?
				return;

			sound = sfx_bgetitem;
			player->firepower += mobj->reactiontime;
			if (player->firepower >= MAXFIREPOWER)
				player->firepower = MAXFIREPOWER;

			temp = P_SpawnMobj(mobj->x, mobj->y, mobj->z, mobj->type);
			temp->flags &= ~MF_SPECIAL;
			temp->health = 0;
			temp->momz = FRACUNIT;
			temp->fuse = mobj->info->damage;

			if (mobj->eflags & MFE_VERTICALFLIP)
				temp->momz = -temp->momz;

			P_SetMobjState(temp, mobj->info->raisestate);
			break;

		case MT_QUESTIONPANEL:
			if (!(P_CanPickupItem(toucher->player, true)))
				return;

			sound = sfx_bgetitem;
			player->questiontimer = mobj->reactiontime; // How long is the ailment lasting?

			// Lets choose the type of ailment you'll have then!

			// First unload every single ailment.
			player->ailment[bal_bomberrhea] = player->ailment[bal_constipation] =
			player->ailment[bal_ctrlreverse] = player->ailment[bal_unstoppable] = false;

			// And THEN choose the correct ailment.
			switch (P_Random()/51)
			{
				case 0: // Diarrhea of the bombs
					if (cv_debug)
						CONS_Printf("You got Bomberrhea!\n");
					player->ailment[bal_bomberrhea] = true;
					break;
				case 1: // Bomber Constipation
					if (cv_debug)
						CONS_Printf("You got Constipation!\n");
					player->ailment[bal_constipation] = true;
					break;
				case 2: // Reversed controls
					if (cv_debug)
						CONS_Printf("You got reversed controls!\n");
					player->ailment[bal_ctrlreverse] = true;
					break;
				case 3: // Moving forward continuously until you press a button.
					if (cv_debug)
						CONS_Printf("You got unstoppable!\n");
					player->ailment[bal_unstoppable] = true;
					break;
				default: // Dummy Panel. Tricking the opponent.
					break;
			}

			// No Ailment?
			if (!(player->ailment[bal_bomberrhea] || player->ailment[bal_constipation] ||
			player->ailment[bal_ctrlreverse] || player->ailment[bal_unstoppable]))
				player->questiontimer = 0; // Why have a timer then?

			// Show to others that you have an ailment!
			if (player->questiontimer)
			{
				mobj_t *aura;
				aura = P_SpawnMobj(player->mo->x, player->mo->y, player->mo->z, MT_CURSEAURA);
				aura->flags |= MF_TRANSLATION|MF_NOCLIPHEIGHT;
				aura->color = 3;
				P_SetTarget(&aura->target, player->mo);
				aura->fuse = player->questiontimer + 1; // Dissappear at the same time the player's ailment does.
			}

			temp = P_SpawnMobj(mobj->x, mobj->y, mobj->z, mobj->type);
			temp->flags &= ~MF_SPECIAL;
			temp->health = 0;
			temp->momz = FRACUNIT;
			temp->fuse = mobj->info->damage;

			if (mobj->eflags & MFE_VERTICALFLIP)
				temp->momz = -temp->momz;

			P_SetMobjState(temp, mobj->info->raisestate);
			break;
		// A P at the end of these map objects indicates this is a collectable panel
		case MT_RUBBERBOMBP:
		case MT_POWERBOMBP:
		case MT_MINEBOMBP:
		case MT_REMOTEBOMBP:
		case MT_SPIKEBOMBP:
			if (!(P_CanPickupItem(toucher->player, true)))
				return;

			sound = sfx_bgetitem;

			// player->bombtype[bpw_tower] = player->bombtype[bpw_line] = false;
			// Destroy the power that the player currently has.
			player->bombtype[bpw_remote] = player->bombtype[bpw_rubber] =
			player->bombtype[bpw_mine] = player->bombtype[bpw_spike] =
			player->bombtype[bpw_power] = false;

			player->bombtype[mobj->info->speed] = true;

			temp = P_SpawnMobj(mobj->x, mobj->y, mobj->z, mobj->type);
			temp->flags &= ~MF_SPECIAL;
			temp->health = 0;
			temp->momz = FRACUNIT;
			temp->fuse = mobj->info->damage;
			temp->state = mobj->state;

			if (mobj->eflags & MFE_VERTICALFLIP)
				temp->momz = -temp->momz;

			P_SetMobjState(temp, mobj->info->raisestate);
			break;
		default:
			break;
	}

	P_SetMobjState(mobj, S_DISS);
	P_UnsetThingPosition(mobj);
	mobj->flags &= ~MF_NOBLOCKMAP;
	P_SetThingPosition(mobj);

	if (sound != sfx_None)
		S_StartSound(player->mo, sound);
}

//
// P_BombExplosion
//
// .. More like a copy of A_OldRingExplode with some modifications to it.
//
static void P_BombExplosion(mobj_t *actor, mobjtype_t type)
{
	UINT8 i, f; // Byte
	mobj_t *mo;
	fixed_t ns = twodlevel ? 15 * FRACUNIT : 20 * FRACUNIT;

	if (!actor->target) // Shouldn't happen, but just in case..
	{
		CONS_Printf("ACTOR HAS NO TARGET!\n");
		return;
	}

	// 'ns' modifier
	// Lets try and enlarge the explosion!
	if (actor->target->player) // Is your target a player?
	{
		if (actor->type != MT_POWERBOMB // Does your target NOT have the Power Bomb Powerup? 
			&& actor->target->player->firepower > 1)// AND does the player have a firepower greater than one?
		{
			for (f = 2; f < actor->target->player->firepower+1; f++)
				ns += (15/2)*FRACUNIT; // Increase by increments of 15 FRACUNITS until the maximum firepower is reached!
		}
		else if (actor->type == MT_POWERBOMB) // What if your target has the Power Bomb Powerup?
			ns *= 5; // MAX POWAH
	}
	

	// 'ns' modifier continued...
	// Check the actor against possible water content before anything below.
	P_MobjCheckWater(actor);

	if ((actor->eflags & MFE_UNDERWATER) || (actor->eflags & MFE_TOUCHWATER)) // Is the mobj under water?
		ns = ns / 2; // Make the debris travel at half speed then.

	S_StartSound(NULL, actor->info->deathsound);

	if (twodlevel)
	{
		for (i = 0; i < 4; i++) // Create how many explosions? 4!
		{
			const angle_t fa = (i*FINEANGLES/4) & FINEMASK; // How many angles can the explosion debris go in? 4!

			mo = P_SpawnMobj(actor->x, actor->y, actor->z, type);
			P_SetTarget(&mo->target, actor->target); // Transfer target so player gets the points

			mo->momx = FixedMul(FINESINE(fa),ns);
			mo->momy = FixedMul(FINECOSINE(fa),ns);

			// Apply the following to the leading explosion object:

			mo->flags &= ~MF_FLOAT;
			mo->flags &= ~MF_NOGRAVITY; // Allow it to have gravity
			if (actor->eflags & MFE_VERTICALFLIP) // Is the bomb upside down?
				mo->flags2 |= MF2_OBJECTFLIP; // Make the mobj have upside down properties too then!
			mo->flags2 |= MF2_DEBRIS|MF2_DONTDRAW; // Apply MF2_DONTDRAW to the leading explosion object.
			mo->fuse = TICRATE/2+1; // Die in half a second.
		}
	}
	else
	{
		for (i = 0; i < 32; i++) // Spawn how many explosions? 32!
		{
			const angle_t fa = (i*FINEANGLES/16) & FINEMASK; // How many angles can the explosion debris go in? 16!

			mo = P_SpawnMobj(actor->x, actor->y, actor->z, type);
			P_SetTarget(&mo->target, actor->target); // Transfer target so player gets the points

			mo->momx = FixedMul(FINESINE(fa),ns);
			mo->momy = FixedMul(FINECOSINE(fa),ns);

			if (i > 15)
			{
				// For every other explosion object above 15,
				// apply alternating momz to the explosion debris.
				if (i & 1)
					mo->momz = (ns*3)/5;
				else
					mo->momz = -((ns*3)/5);
			}

			// Apply the following to the leading explosion object:
			mo->flags &= ~MF_FLOAT;
			mo->flags &= ~MF_NOGRAVITY; // Allow it to have gravity
			mo->flags2 |= MF2_DEBRIS|MF2_DONTDRAW; // Apply MF2_DONTDRAW to the leading explosion object to a graphical oddity.
			mo->fuse = TICRATE/2+1; // Die in half a second.
		}
	}

	/*mo = P_SpawnMobj(actor->x, actor->y, actor->z, type);
	P_SetTarget(&mo->target, actor->target);
	mo->flags2 |= MF2_DEBRIS;*/
}

//
// P_BombermanMobjThinkers
//
// This tells how we want certain mobjs to act. Such as bombs and even fire debris!
//
void P_BombermanMobjThinkers(mobj_t *mobj)
{
	switch (mobj->type)
	{
		case MT_BOMB:
		case MT_POWERBOMB:
		case MT_SPIKEBOMB:
		case MT_RUBBERBOMB:
			if ((mobj->type == MT_BOMB || mobj->type == MT_POWERBOMB)  && (leveltime % (TICRATE/10) == 0))
			{
				// Fancy 'lil fuse.
				mobj_t *temp;
				temp = P_SpawnMobj(mobj->x, mobj->y, (mobj->eflags & MFE_VERTICALFLIP) ? mobj->z - mobj->info->height : (mobj->z + mobj->info->height), MT_SPARK);
				temp->momz = FRACUNIT;
				if (mobj->eflags & MFE_VERTICALFLIP)
					temp->momz = -FRACUNIT;
			}

			// Code for making the Mobj solid if the player walks a certain distance away.
			if (mobj->target && !(mobj->flags & MF_SOLID) && mobj->health > 0 && mobj->state != &states[S_DISS])
			{
				thinker_t *think;
				mobj_t *mo;

				for (think = thinkercap.next; think != &thinkercap; think = think->next)
				{
					if (think->function.acp1 != (actionf_p1)P_MobjThinker) // Eliminate anything except Mobjs
						continue;

					mo = (mobj_t *)think;

					// If the mobj doesn't have any health, or if it's disappearing, ignore it.
					if ((mo->health <= 0) || (mo->state == &states[S_DISS]))
						continue;

					// Is the bomb targeted to the owner?
					if (mo != mobj->target)
						continue;

					// Check to see if you're a certain distance away from the bomb.
					if (!(P_AproxDistance(P_AproxDistance(mo->x-mobj->x, mo->y-mobj->y),
						mo->z-mobj->z) >= (mo->radius + mobj->radius + 10*FRACUNIT)))
						continue;

					// Hm, good, you're far enough away from the bomb, so now we can change it's flags
					mobj->flags |= MF_SOLID;
					mobj->flags &= ~MF_NOCLIP;
					mobj->flags2 |= MF2_STANDONME;
					if (cv_debug)
						CONS_Printf("P_BombermanMobjThinkers: Bomb is solid.\n");
					break;
				}
			}

			if (mobj->fuse == 46)
			{
				// Turn the bomb into a strawberry red color at this moment.
				//S_StartSound(NULL, sfx_fuse);
				if (mobj->type != MT_RUBBERBOMB) // However, don't apply this to the Rubber Bombs.
				{
					if (mobj->type == MT_POWERBOMB)
						P_SetMobjState(mobj, S_POWERBOMBRED1);
					else if (mobj->type == MT_SPIKEBOMB)
						P_SetMobjState(mobj, S_SPIKEBOMBRED1);
					else
						P_SetMobjState(mobj, S_BOMBRED1);
				}
			}
			else if (mobj->fuse == 5) // Get ready to explode!
			{
				if (mobj->target && mobj->target->player
					&& !(mobj->target->player->playerstate == PST_REBORN
					|| mobj->target->player->playerstate == PST_DEAD))
					mobj->target->player->bombsout -= 1;

				if (cv_debug)
					CONS_Printf("The bomb is exploding!\n");

				// Explode.
				if (mobj->type == MT_SPIKEBOMB)
					P_BombExplosion(mobj, MT_BLUEXPLODE);
				else
					P_BombExplosion(mobj, MT_REDEXPLODE);

				/* Mass copy/paste of the following code until P_SetMobjState.
				 *
				 * "Flame! Why did you do this here and not in P_BombExplosion?"
				 *
				 * Apparently, during testing I had an issue where the player
				 * would 'kick' a bomb, the bomb would then proceed to explode
				 * and lock up/freeze the application.
				 *
				 * It became so fusturating to have it there that I outright
				 * took it out of the function. At least having this section
				 * of code doesn't lock up/freeze the application for some
				 * reason. If you wish to look into this, you can.
				 */

				// Check to see what flags the mobj has and remove/add them.
				if (mobj->flags & MF_SOLID)
					mobj->flags &= ~MF_SOLID;
				if (!(mobj->flags & MF_NOCLIP))
					mobj->flags |= MF_NOCLIP;

				if (mobj->flags2 & MF2_STANDONME)
					mobj->flags2 &= ~MF2_STANDONME;

				if (mobj->flameflags & FLF_BOMBKICK)
					mobj->flameflags &= ~FLF_BOMBKICK;

				// P_KillMobj caused a sigsegv when used here.
				// Used P_SetMobjState instead.
				P_SetMobjState(mobj, S_DISS);
			}
			break;

		case MT_MINEBOMB: // Special case with the Landmine.
			mobj->momx = mobj->momy = 0;

			if (mobj->reactiontime)
				mobj->reactiontime--;

			// Special threshold modifier to make the mobj flash
			// (Like the 'Flash player after being hit' code)
			if ((mobj->threshold > 18) || ((mobj->threshold < 18) && (mobj->threshold > 0)))
				mobj->threshold--;

			// Commence the flashing!
			if (mobj->threshold & 1)
			{
				// ANNOYING SOUND
				S_StopSound(mobj);
				S_StartSound(mobj, mobj->info->activesound);

				if (mobj->state == &states[S_MINEBOMB2])
					P_SetMobjState(mobj, S_MINEBOMB1);
				mobj->flags2 &= ~MF2_DONTDRAW;
			}
			else if (mobj->threshold == 18) // SPECIAL CASE: Does the threshold equal 18?
				P_SetMobjState(mobj, S_MINEBOMB2); // Make this Landmine harder to see.
			else
				mobj->flags2 |= MF2_DONTDRAW;

			
			if (!mobj->reactiontime
				&& (mobj->threshold == 18)
				&& mobj->target
				&& (mobj->type == MT_MINEBOMB) 
				&& mobj->health > 0 && (mobj->state != &states[S_DISS]))
			{
				thinker_t *think;
				mobj_t *mo;

				for (think = thinkercap.next; think != &thinkercap; think = think->next)
				{
					if (think->function.acp1 != (actionf_p1)P_MobjThinker)
						continue;

					mo = (mobj_t *)think;

					// If the mobj doesn't have any health, or if it's disappearing, ignore it.
					if ((mo->health <= 0) || (mo->state == &states[S_DISS]))
						continue;

					// Check against mobj types
					if (mo == mobj)
						continue; // This mo is the same as the mobj, ignore it.

					// Check to see if you're in the radius of the bomb
					if (P_AproxDistance(P_AproxDistance(mo->x-mobj->x, mo->y-mobj->y),
						mo->z-mobj->z) >= (mo->radius + mobj->radius + 10*FRACUNIT))
						continue; // Out of range

					if (cv_debug)
						CONS_Printf("P_BombermanMobjThinkers: The Landmine is about to explode!\n");
					mobj->threshold -= 1; // You might want to run.
				}
			}

			// Uh oh, someone stepped on the land mine.
			if (mobj->threshold == 1) // Get ready to explode!
			{
				if (mobj->target && mobj->target->player
					&& !(mobj->target->player->playerstate == PST_REBORN
					|| mobj->target->player->playerstate == PST_DEAD))
					mobj->target->player->bombsout -= 1;

				if (cv_debug)
					CONS_Printf("The Landmine is exploding!\n");

				// Explode.
				P_BombExplosion(mobj, MT_REDEXPLODE);

				/* Mass copy/paste of the following code until P_SetMobjState.
				 *
				 * "Flame! Why did you do this here and not in P_BombExplosion?"
				 *
				 * Apparently, during testing I had an issue where the player
				 * would 'kick' a bomb, the bomb would then proceed to explode
				 * and lock up/freeze the application.
				 *
				 * It became so fusturating to have it there that I outright
				 * took it out of the function. At least having this section
				 * of code doesn't lock up/freeze the application for some
				 * reason. If you wish to look into this, you can.
				 */

				// Check to see what flags the mobj has and remove/add them.
				if (mobj->flags & MF_SOLID)
					mobj->flags &= ~MF_SOLID;
				if (!(mobj->flags & MF_NOCLIP))
					mobj->flags |= MF_NOCLIP;

				if (mobj->flags2 & MF2_STANDONME)
					mobj->flags2 &= ~MF2_STANDONME;

				if (mobj->flameflags & FLF_BOMBKICK)
					mobj->flameflags &= ~FLF_BOMBKICK;

				// P_KillMobj caused a sigsegv when used here.
				// Used P_SetMobjState instead.
				P_SetMobjState(mobj, S_DISS);
			}
			break;

		case MT_REMOTEBOMB:
			// Code for making the Mobj solid if the player walks a certain distance away.
			if (mobj->target && !(mobj->flags & MF_SOLID) && mobj->health > 0 && mobj->state != &states[S_DISS])
			{
				thinker_t *think;
				mobj_t *mo;

				for (think = thinkercap.next; think != &thinkercap; think = think->next)
				{
					if (think->function.acp1 != (actionf_p1)P_MobjThinker) // Eliminate anything except Mobjs
						continue;

					mo = (mobj_t *)think;

					// If the mobj doesn't have any health, or if it's disappearing, ignore it.
					if ((mo->health <= 0) || (mo->state == &states[S_DISS]))
						continue;

					// Is the bomb targeted to the owner?
					if (mo != mobj->target)
						continue;

					// Check to see if you're a certain distance away from the bomb.
					if (!(P_AproxDistance(P_AproxDistance(mo->x-mobj->x, mo->y-mobj->y),
						mo->z-mobj->z) >= (mo->radius + mobj->radius + 10*FRACUNIT)))
						continue;

					// Hm, good, you're far enough away from the bomb, so now we can change it's flags
					mobj->flags |= MF_SOLID;
					mobj->flags &= ~MF_NOCLIP;
					mobj->flags2 |= MF2_STANDONME;
					if (cv_debug)
						CONS_Printf("P_BombermanMobjThinkers: Remote Bomb is solid.\n");
					break;
				}
			}

			// Colored bombs?
			if (mobj->target
				&& mobj->flags & MF_TRANSLATION 
				&& (mobj->color != mobj->target->player->skincolor))
				mobj->color = mobj->target->player->skincolor; // Keep colors updated on-the-fly

			// Detonate this bomb via a button press.
			if (mobj->target && mobj->target->player
				&& ((mobj->target->player->cmd.buttons & BT_ATTACK)
				&& !(mobj->target->player->pflags & PF_ATTACKDOWN))
				// && (mobj->state >= &states[S_RBOMBEXPL1] || mobj->state <= &states[S_RBOMBEXPL5])
				)
			{
				// Don't throttle explosions.
				mobj->target->player->pflags |= PF_JUMPDOWN;

				if (mobj->target && mobj->target->player
					&& !(mobj->target->player->playerstate == PST_REBORN
					|| mobj->target->player->playerstate == PST_DEAD))
					mobj->target->player->bombsout -= 1;

				// Explode.
				P_BombExplosion(mobj, mobj->info->raisestate);

				/* Mass copy/paste of the following code until P_SetMobjState.
				 *
				 * "Flame! Why did you do this here and not in P_BombExplosion?"
				 *
				 * Apparently, during testing I had an issue where the player
				 * would 'kick' a bomb, the bomb would then proceed to explode
				 * and lock up/freeze the application.
				 *
				 * It became so fusturating to have it there that I outright
				 * took it out of the function. At least having this section
				 * of code doesn't lock up/freeze the application for some
				 * reason. If you wish to look into this, you can.
				 */

				// Check to see what flags the mobj has and remove/add them.
				if (mobj->flags & MF_SOLID)
					mobj->flags &= ~MF_SOLID;
				if (!(mobj->flags & MF_NOCLIP))
					mobj->flags |= MF_NOCLIP;

				if (mobj->flags2 & MF2_STANDONME)
					mobj->flags2 &= ~MF2_STANDONME;

				if (mobj->flameflags & FLF_BOMBKICK)
					mobj->flameflags &= ~FLF_BOMBKICK;

				// P_KillMobj caused a sigsegv when used here.
				// Used P_SetMobjState instead.
				P_SetMobjState(mobj, S_DISS);
			}
			else if (!(mobj->target->player->cmd.buttons & BT_ATTACK)) // Not pressing the button?
				mobj->target->player->pflags &= ~PF_ATTACKDOWN; // Get rid of the flag then!
			break;

		case MT_REDEXPLODE: // Standard bomb explosion debris. Disappears upon contact with a bustable block.
		case MT_BLUEXPLODE: // More powerful bomb explosion debris. Continues through bustable blocks.
#if 0 // Instead of outright extinguishing the fire object, lets half the speed on it. (See: P_BombExplosion)
			// Check this mobj against possible water content before ANYTHING.
			P_MobjCheckWater(mobj);

			if ((mobj->flags & MF_FIRE) // If it's a fire object.
				&& (mobj->flags2 & MF2_DONTDRAW) // The LEADING fire object.
				&& ((mobj->eflags & MFE_UNDERWATER) || (mobj->eflags & MFE_TOUCHWATER))) // And it's underwater..
				P_SetMobjState(mobj, S_DISS); // Extinguish it.
#endif

			// Does the leading fire object have momentum?
			if ((mobj->momx || mobj->momy)
				&& ((mobj->flags2 & MF2_DONTDRAW) == MF2_DONTDRAW)
				&& (mobj->state != &states[S_DISS])
#if NEWTICRATERATIO > 1
				&& ((leveltime % NEWTICRATERATIO) == 0)
#endif
			   )
			{
				mobj_t *trail;
				trail = P_SpawnMobj(mobj->x, mobj->y, mobj->z + 1, mobj->type); // Spawn more of your kind!
				P_SetTarget(&trail->target, mobj->target);
				trail->flags |= MF_MISSILE;
				if (mobj->momx > 0)
					trail->momx = 1;
				else if (mobj->momx < 0)
					trail->momx = -1;
				else if (mobj->momy > 0)
					trail->momy = 1 ;
				else if (mobj->momy < 0)
					trail->momy = -1;
			}

			// Trailing fire debris.
			if (mobj->momz && ((mobj->flags2 & MF2_DONTDRAW) != MF2_DONTDRAW))
				mobj->momz = 0;

			// What happens when this mobj hits a bustable block?
			if (CheckForBustableBlocks)
			{
				msecnode_t* node;
				fixed_t oldx;
				fixed_t oldy;

				INT32 prandom;
				mobjtype_t newpanel;

				mobjtype_t spawnchance[48];
				INT32 i = 0;
				INT32 oldi = 0;
				INT32 numchoices = 0;

				oldx = mobj->x;
				oldy = mobj->y;

				P_UnsetThingPosition(mobj);
				mobj->x += mobj->momx;
				mobj->y += mobj->momy;
				P_SetThingPosition(mobj);

				for (node = mobj->touching_sectorlist; node; node = node->m_snext)
				{
					if (!node->m_sector)
						break;

					if (node->m_sector->ffloors)
					{
						ffloor_t *rover;

						for (rover = node->m_sector->ffloors; rover; rover = rover->next)
						{
							if (!(rover->flags & FF_EXISTS)) continue;

							if (!(rover->flags & FF_BUSTUP)) continue;

							// Needs ML_PASSUSE flag for pushables to break it
							// if (!(rover->master->flags & ML_PASSUSE)) continue;
							// No it doesn't. :(

							if (!rover->master->frontsector->crumblestate)
							{
								// Height checks
								if (rover->flags & FF_SHATTERBOTTOM)
								{
									if (mobj->z+mobj->momz + mobj->height < *rover->bottomheight)
										continue;

									if (mobj->z+mobj->height > *rover->bottomheight)
										continue;
								}
								else if (rover->flags & FF_SPINBUST)
								{
									if (mobj->z+mobj->momz > *rover->topheight)
										continue;

									if (mobj->z+mobj->height < *rover->bottomheight)
										continue;
								}
								else if (rover->flags & FF_SHATTER)
								{
									if (mobj->z+mobj->momz > *rover->topheight)
										continue;

									if (mobj->z+mobj->momz + mobj->height < *rover->bottomheight)
										continue;
								}
								else
								{
									if (mobj->z >= *rover->topheight)
										continue;

									if (mobj->z+mobj->height < *rover->bottomheight)
										continue;
								}

								EV_CrumbleChain(node->m_sector, rover);

								// Lets see what can come out of a breakable block!
#if 0 // Old code for reference
								switch (P_Random()/31)
								{
									case 0:// Nothing..?
									case 6:
									case 7:
									default: // As you can see the dissapearing mobj gets a lot of attention.
										P_SpawnMobj (node->m_sector->soundorg.x,
											node->m_sector->soundorg.y,
											node->m_sector->floorheight, MT_DISS);
										break;
									case 1: // Bomb up panel
										P_SpawnMobj (node->m_sector->soundorg.x,
											node->m_sector->soundorg.y,
											node->m_sector->floorheight, MT_BOMBUP);
										break;
									case 2: // Fire up panel
										P_SpawnMobj (node->m_sector->soundorg.x,
										node->m_sector->soundorg.y,
										node->m_sector->floorheight, MT_FIREUP);
										break;
									case 3: // Question panel (Equivilent of a Skull)
										P_SpawnMobj (node->m_sector->soundorg.x,
										node->m_sector->soundorg.y,
										node->m_sector->floorheight, MT_QUESTIONPANEL);
										break;
									case 4: // Power Bomb Panel
										P_SpawnMobj (node->m_sector->soundorg.x,
										node->m_sector->soundorg.y,
										node->m_sector->floorheight, MT_POWERBOMBP);
										break;
									case 5: // Rubber Bomb Panel
										P_SpawnMobj (node->m_sector->soundorg.x,
										node->m_sector->soundorg.y,
										node->m_sector->floorheight, MT_RUBBERBOMBP);
										break;
								}
#else
								// Rehash of MT_RANDOMBOX code

								prandom = P_Random();

								if (cv_powerbombp.value)
								{
									oldi = i;

									for (; i < oldi + cv_powerbombp.value; i++)
									{
										spawnchance[i] = MT_POWERBOMBP;
										numchoices++;
									}
								}

								if (cv_rubberbombp.value)
								{
									oldi = i;

									for (; i < oldi + cv_rubberbombp.value; i++)
									{
										spawnchance[i] = MT_RUBBERBOMBP;
										numchoices++;
									}
								}

								if (cv_bombupp.value)
								{
									oldi = i;

									for (; i < oldi + cv_bombupp.value; i++)
									{
										spawnchance[i] = MT_BOMBUP;
										numchoices++;
									}
								}

								if (cv_fireupp.value)
								{
									oldi = i;

									for (; i < oldi + cv_fireupp.value; i++)
									{
										spawnchance[i] = MT_FIREUP;
										numchoices++;
									}
								}

								if (cv_questionp.value)
								{
									oldi = i;

									for (; i < oldi + cv_questionp.value; i++)
									{
										spawnchance[i] = MT_QUESTIONPANEL;
										numchoices++;
									}
								}

								if (cv_spikebombp.value)
								{
									oldi = i;

									for (; i < oldi + cv_spikebombp.value; i++)
									{
										spawnchance[i] = MT_SPIKEBOMBP;
										numchoices++;
									}
								}

								if (cv_landminep.value)
								{
									oldi = i;

									for (; i < oldi + cv_landminep.value; i++)
									{
										spawnchance[i] = MT_MINEBOMBP;
										numchoices++;
									}
								}

								if (cv_remotebombp.value)
								{
									oldi = i;

									for (; i < oldi + cv_spikebombp.value; i++)
									{
										spawnchance[i] = MT_REMOTEBOMBP;
										numchoices++;
									}
								}

								// CONS_Printf("%d\n", (int)(sizeof(spawnchance) / sizeof(mobjtype_t)));

								// We assume if the player turned off every panel,
								// he doesn't want to be spawning any.
								if (numchoices == 0)
								{
									if (cv_debug)
										CONS_Printf("Note: All Panels turned off.\n");
								}
								else // However there still is a chance at getting nothing..
								{
									oldi = i;

									for (; i < oldi + 8; i++) // A HIGH chance.
									{
										spawnchance[i] = MT_DISS;
										numchoices++;
									}

									newpanel = spawnchance[prandom%numchoices];

									if (newpanel)
										P_SpawnMobj (node->m_sector->soundorg.x,
											node->m_sector->soundorg.y,
											node->m_sector->floorheight, newpanel);
								}
#endif

								// Because MT_BLUEXPLODE is hotter, it doesn't dissapear!
								if (mobj->type == MT_REDEXPLODE)
									mobj->fuse = 1;

								goto bustupdone;
							}
						}
					}
				}
bustupdone:
				P_UnsetThingPosition(mobj);
				mobj->x = oldx;
				mobj->y = oldy;
				P_SetThingPosition(mobj);
			}
			break;
		case MT_CURSEAURA:
			// Don't clip under the floor.
			if (mobj->z < mobj->floorz)
				mobj->z = mobj->floorz;

			// No target? Don't do anything!
			if (!mobj->target)
				return;

			if (mobj->fuse > 0)
				mobj->fuse--;

			// Did target die, walk off his curse, or grab a new curse?
			if (mobj->target->player
				&& ((mobj->target->player->playerstate == PST_DEAD)
					|| (mobj->fuse <= 1)
					|| (mobj->target->player->questiontimer <= 1)
					|| ((unsigned)mobj->fuse != mobj->target->player->questiontimer)))
				P_SetMobjState(mobj, S_DISS); // You're gone!

			// Continue following your target.
			P_UnsetThingPosition(mobj);
			mobj->x = mobj->target->x;
			mobj->y = mobj->target->y;
			mobj->z = mobj->target->z - (FIXEDSCALE(mobj->target->info->height, mobj->target->scale) - mobj->target->height) / 3 + FIXEDSCALE(2*FRACUNIT, mobj->target->scale);
			P_SetScale(mobj, mobj->target->scale);
			P_SetThingPosition(mobj);
			break;

		case MT_BOMBUP:
		case MT_FIREUP:
		case MT_QUESTIONPANEL:
		case MT_RUBBERBOMBP:
		case MT_POWERBOMBP:
		case MT_MINEBOMBP:
		case MT_REMOTEBOMBP:
		case MT_SPIKEBOMBP:
			if (mobj->health == 0) // Fading tile
			{
				INT32 value = mobj->info->damage/10;
				value = mobj->fuse/value;
				value = 10-value;
				value--;

				if (value <= 0)
					value = 1;

				mobj->frame &= ~FF_TRANSMASK;
				mobj->frame |= value << FF_TRANSSHIFT;
			}
			break;
		default:
			break;
	}
}
#endif
