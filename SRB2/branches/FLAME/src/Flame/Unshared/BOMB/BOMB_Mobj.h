// SRB2FLAME - Unique code for Super Bomberman Blast 2 (BOMB)
// Code which is unique/exclusive to its define.
// By Flame

#ifdef BOMB
#ifndef __BOMB_MOBJ__
#define __BOMB_MOBJ__

#define MAXFIREPOWER 5
#define MAXBOMBS 4

boolean P_IsBomberMobj(mobj_t *mobj);
void P_BombPanelCollect(mobj_t *mobj, mobj_t *toucher);
void P_BombermanMobjThinkers(mobj_t *mobj);

#endif // __BOMB_MOBJ__
#endif
