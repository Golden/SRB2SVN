// SRB2FLAME - Unique code for Super Bomberman Blast 2 (BOMB)
// Code which is unique/exclusive to its define.
// By Flame

#include "../../../doomstat.h"
#include "../../../p_local.h"
#include "../../../g_game.h"
#include "../../../s_sound.h"

#include "../../FL_Common.h"
#include "BOMB_User.h"
#include "BOMB_Command.h"

#ifdef BOMB

//
// P_BOMBCamera
//
// Camera thinker for the Bomberman (BOMB) define.
//
fixed_t P_BOMBCamera(camera_t *thiscam)
{
	fixed_t CAMDIST = 270<<FRACBITS;

#if defined(BOMBCAMCVAR)
	mobj_t *cam = P_GetMobjForType(MT_BOMBCAM);

	if (cv_bombcam == 0) // Dynamic camera
	{
		// A camera that follows the player. Can be manipulated with cam_height or cam_dist values.
		thiscam->x = players[consoleplayer].mo->x; // Set the camera's x equal to the player's x.
		thiscam->z = players[consoleplayer].mo->z + (CAMDIST/2 + cv_cam_height.value);
		thiscam->y = players[consoleplayer].mo->y - (CAMDIST + cv_cam_dist.value); // Set the camera's y a bit back from the player's y.
	}
	else if (cv_bombcam == 1) // Scrolling camera
	{
		// A camera that follows the player on a fixed axis. Can be manipulated by the cam_height value.
		if (cam) // Failsafe
		{
			thiscam->x = players[consoleplayer].mo->x; // Set the camera's x equal to the player's x.
			thiscam->y = cam->y + (150*FRACUNIT); // Static y coordinate
			thiscam->z = players[consoleplayer].mo->z + (CAMDIST/2 + cv_cam_height.value + 180*FRACUNIT);
		}
		else
		{
			thiscam->x = players[consoleplayer].mo->x; // Set the camera's x equal to the player's x.
			thiscam->y = -(1132*FRACUNIT); // Static y coordinate
			thiscam->z = players[consoleplayer].mo->z + (CAMDIST/2 + cv_cam_height.value + 250*FRACUNIT);
		}
	}
	else if (cv_bombcam == 2) // Static camera.
	{
		// Camera at a fixed point. Cannot be manipulated with the cam_height or cam_dist values.
		if (cam) // Failsafe
		{
			// Static points
			thiscam->x = cam->x;
			thiscam->y = cam->y;
			thiscam->z = cam->z;
		}
		else // cv_bombcam == 1
		{
			thiscam->x = players[consoleplayer].mo->x; // Set the camera's x equal to the player's x.
			thiscam->y = -(1132*FRACUNIT); // Static y coordinate
			thiscam->z = players[consoleplayer].mo->z + (CAMDIST/2 + cv_cam_height.value + 250*FRACUNIT); // Set the height of the camera.
		}
	}
#else
#if defined(BOMBCAMDYNAMIC)
	// Camera with a fixed position, following the player.
	thiscam->x = players[consoleplayer].mo->x; // Set the camera's x equal to the player's x.
	thiscam->z = players[consoleplayer].mo->z + (CAMDIST/2 + cv_cam_height.value);
	thiscam->y = players[consoleplayer].mo->y - (CAMDIST + cv_cam_dist.value); // Set the camera's y a bit back from the player's y.
#elif defined(OLDBOMBCAMFIXED)
	// Camera with a fixed position, fixed point.
	thiscam->x = 0; // Set the camera along the X axis.
	thiscam->z = (800*FRACUNIT); // Set the height of the camera.

	thiscam->y = -(300 + FRACUNIT); // Set the distance of the camera along the Y axis.
#endif
#endif // BOMBCAMCVAR
	return 0;
}

//
// P_BOMB2dMovement
//
// Control scheme for 2D movement of the Bomberman (BOMB) define.
//
void P_BOMB2dMovement(player_t *player)
{
	ticcmd_t *cmd = &player->cmd;
	INT32 topspeed, acceleration, thrustfactor;
	fixed_t movepushforward = 0;
	angle_t movepushangle = 0;
	fixed_t normalspd = player->normalspeed;

	if (player->exiting
		|| (player->pflags & PF_STASIS)
		|| (player->powers[pw_nocontrol]) || (player->powers[pw_ingoop]))
	{
		cmd->forwardmove = cmd->sidemove = 0;
		if (player->pflags & PF_GLIDING)
			player->pflags &= ~PF_GLIDING;
		if (player->pflags & PF_SPINNING && !player->exiting)
		{
			player->pflags &= ~PF_SPINNING;
			P_SetPlayerMobjState(player->mo, S_PLAY_STND);
		}
	}

	// cmomx/cmomy stands for the conveyor belt speed.
	if (player->onconveyor == 2) // Wind/Current
	{
		//if (player->mo->z > player->mo->watertop || player->mo->z + player->mo->height < player->mo->waterbottom)
		if (!(player->mo->eflags & MFE_UNDERWATER) && !(player->mo->eflags & MFE_TOUCHWATER))
			player->cmomx = player->cmomy = 0;
	}
	else if (player->onconveyor == 4 && !P_IsObjectOnGround(player->mo)) // Actual conveyor belt
		player->cmomx = player->cmomy = 0;
	else if (player->onconveyor != 2 && player->onconveyor != 4)
		player->cmomx = player->cmomy = 0;

	player->rmomx = player->mo->momx - player->cmomx;
	player->rmomy = player->mo->momy - player->cmomy;

	// Calculates player's speed based on distance-of-a-line formula
	player->speed = (abs(player->rmomx) + abs(player->rmomy))>>FRACBITS;

	if ((cmd->sidemove || cmd->forwardmove)
		&& !(player->climbing)
		&& !(!(player->pflags & PF_SLIDING)
		&& player->mo->state == &states[player->mo->info->painstate]
		&& player->powers[pw_flashing]))
	{
		if (cmd->sidemove)
		{
			if (cmd->sidemove > 0)
				player->mo->angle = 0;
			else if (cmd->sidemove < 0)
				player->mo->angle = ANGLE_180;

			player->mo->momy = player->mo->momy/2;
		}
		else if (cmd->forwardmove)
		{
			if (cmd->forwardmove > 0)
				player->mo->angle = ANGLE_90;
			else if (cmd->forwardmove < 0)
				player->mo->angle = ANGLE_270;

			player->mo->momx = player->mo->momx/2;
		}
	}

	if (player == &players[consoleplayer])
		localangle = player->mo->angle;
	else if (splitscreen && player == &players[secondarydisplayplayer])
		localangle2 = player->mo->angle;

	if ((cmd->sidemove > 0) && !cmd->forwardmove)
		movepushangle = 0;
	else if ((cmd->sidemove < 0) && cmd->forwardmove)
		movepushangle = ANGLE_180;
	else if ((cmd->forwardmove > 0) && !cmd->sidemove)
		movepushangle = ANGLE_90;
	else if ((cmd->forwardmove < 0) && !cmd->sidemove)
		movepushangle = ANGLE_270;
	else
		movepushangle = player->mo->angle;

	player->aiming = cmd->aiming<<FRACBITS;

//	if (player->speed >= 5 + player->speedcount)
//		player->speed = (5 + player->speedcount);

		normalspd = (normalspd / 2);
	thrustfactor = player->thrustfactor;
	acceleration = player->accelstart + player->speed*player->acceleration;

	if (player->powers[pw_tailsfly])
		topspeed = normalspd/2;
	else if (player->mo->eflags & MFE_UNDERWATER && !(player->pflags & PF_SLIDING))
	{
		topspeed = normalspd/2;
		acceleration = (acceleration * 2) / 3;
	}
	else
		topspeed = normalspd;

	if ((cmd->sidemove != 0 || cmd->forwardmove != 0) && !(player->climbing || (player->pflags & PF_GLIDING) || player->exiting
		|| (!(player->pflags & PF_SLIDING) && player->mo->state == &states[player->mo->info->painstate] && player->powers[pw_flashing]
		&& !P_IsObjectOnGround(player->mo))))
	{
		if (cmd->sidemove && !cmd->forwardmove)
		{
			if (player->powers[pw_sneakers] || player->powers[pw_super]) // do you have super sneakers?
				movepushforward = abs(cmd->sidemove) * ((thrustfactor*2)*acceleration);
			else // if not, then run normally
				movepushforward = abs(cmd->sidemove) * (thrustfactor*acceleration);
		}
		else if (cmd->forwardmove && !cmd->sidemove)
		{
			if (player->powers[pw_sneakers] || player->powers[pw_super]) // do you have super sneakers?
				movepushforward = abs(cmd->forwardmove) * ((thrustfactor*2)*acceleration);
			else // if not, then run normally
				movepushforward = abs(cmd->forwardmove) * ((thrustfactor)*acceleration);
		}

		// allow very small movement while in air for gameplay
		if (!P_IsObjectOnGround(player->mo))
			movepushforward >>= 1; // Proper air movement

		if (((player->rmomx >> FRACBITS) < topspeed) && (cmd->sidemove > 0)) // Sonic's Speed
			P_Thrust(player->mo, movepushangle, movepushforward);
		else if (((player->rmomx >> FRACBITS) > -topspeed) && (cmd->sidemove < 0))
			P_Thrust(player->mo, movepushangle, movepushforward);
		else if (((player->rmomy >> FRACBITS) < topspeed) && (cmd->forwardmove > 0))
			P_Thrust(player->mo, movepushangle, movepushforward);
		else if (((player->rmomy >> FRACBITS) > -topspeed) && (cmd->forwardmove < 0))
			P_Thrust(player->mo, movepushangle, movepushforward);
	}
}

//
// P_IsMobjToCloseToMe
//
// Long boolean statement. Checks to see if a bomb mobj is
// too close to a player.
//
// Used in P_SpawnBombMobjByType.
//
static boolean P_IsMobjTooCloseToMe(mobj_t *me)
{
	mobj_t *mo = NULL;
	thinker_t *think;

	if (!me || !me->player)
		return false;

	for (think = thinkercap.next; think != &thinkercap; think = think->next)
	{
		if (think->function.acp1 != (actionf_p1)P_MobjThinker) // Not a mobj thinker
			continue;

		mo = (mobj_t *)think;

		// Is the mobj a bomberman mobj?
		if (!((mo->flameflags & FLF_BOMBKICK) || mo->type == MT_MINEBOMB))
			continue;

		// Check to see if you're in the radius of the bomb
		if (P_AproxDistance(P_AproxDistance(me->x - mo->x, me->y - mo->y),
			me->z - mo->z) >= (mo->radius + me->radius + 10*FRACUNIT))
			continue; // Out of range

		return true; // Too close
	}
	return false; // Too far
}

//
// P_SpawnBombMobjByType
//
// Here is where we spawn a Bomb Mobj based on 
// what bombtype power the player has.
//
mobj_t *P_SpawnBombMobjByType(player_t *player)
{
	mobj_t *mo = player->mo;
	fixed_t zheight = player->mo->z - FixedDiv(P_GetPlayerHeight(player) - player->mo->height,3*FRACUNIT); // Middle
	mobj_t *bombmo = NULL;

	// Spawning a bomb ontop of a bomb? This shouldn't happen.
	if (P_IsMobjTooCloseToMe(mo))
	{
		if (cv_debug)
			CONS_Printf("The bomb can't spawn because the player is on top of a bomb.\n");
		return NULL;
	}

	if (player->bombtype[bpw_mine] && !P_GetMobjForType(MT_MINEBOMB))
	{
		// First bomb planted is a a specific bomb.
		bombmo = P_SpawnMobj(mo->x, mo->y, zheight, MT_MINEBOMB);
		bombmo->threshold = 46; // Special threshold!
		bombmo->reactiontime = bombmo->threshold; // Don't confuse the players, okay?
	}
	else if (player->bombtype[bpw_remote])
		bombmo = P_SpawnMobj(mo->x, mo->y, zheight, MT_REMOTEBOMB);
	else if (player->bombtype[bpw_power] && !P_GetMobjForType(MT_POWERBOMB))
		bombmo = P_SpawnMobj(mo->x, mo->y, zheight, MT_POWERBOMB);
	else if (player->bombtype[bpw_spike])
		bombmo = P_SpawnMobj(mo->x, mo->y, zheight, MT_SPIKEBOMB);
	else if (player->bombtype[bpw_rubber])
		bombmo = P_SpawnMobj(mo->x, mo->y, zheight, MT_RUBBERBOMB);
	else
		bombmo = P_SpawnMobj(mo->x, mo->y, zheight, MT_BOMB);

	if (bombmo)
	{
		player->bombsout += 1;
		S_StartSound(bombmo, sfx_bplant);

		P_SetTarget(&bombmo->target, mo); // Set the bombs target to the player who spawned it.

		if (mo->eflags & MFE_VERTICALFLIP) // Is the player who planted the bomb upside down?
			bombmo->flags2 |= MF2_OBJECTFLIP;// Make the bomb upside down!

		bombmo->angle -= ANGLE_90; // Mainly for the remote bombs to properly face the screen.

		if (bombmo->type != MT_MINEBOMB)
			bombmo->flameflags |= FLF_BOMBKICK; // Make these bombs kickable!

		if (bombmo->type == MT_REMOTEBOMB)
			bombmo->flags |= MF_TRANSLATION;

		if (!bombmo->fuse && (bombmo->type != MT_MINEBOMB && bombmo->type != MT_REMOTEBOMB))
		{
			if (gametype == GT_TAG)
				bombmo->fuse = 46; // In tag, we want the Bombs to explode faster.
			else
				bombmo->fuse = 102; // Otherwise, explode at normal speed.
		}

		if (cv_debug)
			CONS_Printf("Planted the bomb succesfully.\n");
	}
	else
		I_Error("P_SpawnBombMobjByType screwed up somewhere! bombmo = NULL!");

	return bombmo;
}

//
// P_BomberPlayerThink
//
// Some little block of code that does stuff to the player.
//
void P_BomberPlayerThink(player_t *player)
{
	// Does the player have a question timer?
	if (player->questiontimer > 0)
		player->questiontimer--; // Keep decreasing the count!
	else // Otherwise, we assume the player is finished having his status ailment.
	{
		// Then we want to remove whatever status ailment the player has.
		player->ailment[bal_bomberrhea] = player->ailment[bal_constipation] =
		player->ailment[bal_ctrlreverse] = player->ailment[bal_unstoppable] = false;
	}
}
#endif
