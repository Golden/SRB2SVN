// SRB2FLAME - Unique code for Super Bomberman Blast 2 (BOMB)
// Code which is unique/exclusive to its define.
// By Flame

#ifdef BOMB
#ifndef __BOMB_COMMAND__
#define __BOMB_COMMAND__
#ifdef BOMBCAMCVAR
extern INT32 cv_bombcam;
#endif

void BOMB_InitCons(void);

#endif // __BOMB_COMMAND__
#endif
