// SRB2FLAME - Unique code for Super Bomberman Blast 2 (BOMB)
// Code which is unique/exclusive to its define.
// By Flame

#ifdef BOMB
#ifndef __BOMB_USER__
#define __BOMB_USER__

fixed_t P_BOMBCamera(camera_t *thiscam);
void P_BOMB2dMovement(player_t *player);

mobj_t *P_SpawnBombMobjByType(player_t *player);
void P_BomberPlayerThink(player_t *player);

#endif // __BOMB_USER__
#endif
