// SRB2FLAME - Unique code for Super Bomberman Blast 2 (BOMB)
// Code which is unique/exclusive to its define.
// By Flame

#ifdef BOMB
#ifndef __BOMB_NETCMD__
#define __BOMB_NETCMD__
#include "../../../command.h"

extern consvar_t cv_bombupp, cv_fireupp;
extern consvar_t cv_powerbombp, cv_rubberbombp, cv_spikebombp, cv_landminep, cv_remotebombp;
extern consvar_t cv_questionp;
void BOMB_NetInit(void);

#endif // __BOMB_NETCMD__
#endif // BOMB
