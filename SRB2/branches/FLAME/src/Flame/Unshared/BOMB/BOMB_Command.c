// SRB2FLAME - Unique code for Super Bomberman Blast 2 (BOMB)
// Code which is unique/exclusive to its define.
// By Flame

#include "../../../doomdef.h"
#include "../../../doomstat.h"
#include "../../../doomtype.h"
#include "../../../command.h"
#include "../../../d_player.h"
#include "../../../g_game.h"
#include "../../../g_state.h"

#include "BOMB_Command.h"
#include "BOMB_Mobj.h"

#ifdef BOMB
#ifdef BOMBCAMCVAR
INT32 cv_bombcam;
static void Command_BombCam_f(void);
#endif
static void Command_Firepower_f(void);

void BOMB_InitCons(void)
{
	if (dedicated)
		return;

	COM_AddCommand("firepower", Command_Firepower_f);
#if defined(BOMBCAMCVAR)
	COM_AddCommand("bombcam", Command_BombCam_f);
#endif
}

static void Command_Firepower_f(void)
{
	size_t i;
	player_t *p = &players[consoleplayer];

	if (gamestate != GS_LEVEL)
	{
		CONS_Printf("You must be in a level to be able to modify your firepower!\n");
		return;
	}

	if (!(cv_debug || devparm))
	{
		CONS_Printf("Devmode must be enabled.\n");
		return;
	}

	if (netgame)
	{
		CONS_Printf("You can't change your firepower while in a netgame!\n");
		return;
	}

	//     0        1
	// firepower <value>

	if ((cv_debug || devparm) && (COM_Argc() != 2))
	{
		CONS_Printf("Firepower <number>: set your firepower!\n");
		CONS_Printf("Your current firepower is %d\n", p->firepower);
		return;
	}

	if (!p->mo || p->spectator)
	{
		CONS_Printf("The player is dead, non-existant, or is a spectator.\n");
		return;
	}

	i = atoi(COM_Argv(1));
	if (i > MAXFIREPOWER)
		p->firepower = MAXFIREPOWER;
	else
		p->firepower = i;
}

#if defined(BOMBCAMCVAR)
static void Command_BombCam_f(void)
{
	/* A certain specific Bomberman Camera console command
	 *
	 * 0; Dynamic Camera: Follows your movement on the x, y, z plane, 
	 *	up close to your character, player is able to manipulate
	 *	the camera with the cam_height or cam_dist variables.
	 *
	 * 1; Scrolling Camera: Camera fixed at a point on the y axis. 
	 *	Gives full display of what is below you. Follows you on the x 
	 *	and z axes. Cannot be manipulated by the cam_dist varialble.
	 *
	 * 2; Static Camera Camera at a universal fixed point, much like the cut away 
	 *	camera. Cannot be moved.
	 */

	if (COM_Argc() < 1)
	{
		CONS_Printf("bombcam <number>: change how the bomberman camera functions!\n");
		CONS_Printf("The current bombcam value is %d: ", cv_bombcam);
		if (cv_bombcam == 0)
			CONS_Printf("Dynamic\n");
		else if (cv_bombcam == 1)
			CONS_Printf("Scroll\n");
		else if (cv_bombcam == 2)
			CONS_Printf("Static\n");
		else // failsafe
			CONS_Printf("INVALID VALUE\n");
		return;
	}

	if (gamestate != GS_LEVEL)
	{
		CONS_Printf("You need to be in a level to change your camera\n");
		return;
	}

	//    0       1
	// bombcam <value>

	if (!strcasecmp(COM_Argv(1), "dynamic") || !strcasecmp(COM_Argv(1), "0"))
		cv_bombcam = 0;
	else if (!strcasecmp(COM_Argv(1), "scroll") || !strcasecmp(COM_Argv(1), "1"))
		cv_bombcam = 1;
	else if (!strcasecmp(COM_Argv(1), "static") || !strcasecmp(COM_Argv(1), "2"))
		cv_bombcam = 2;

	if (cv_bombcam > 2) // Loop
		cv_bombcam = 0;

	CONS_Printf("Bomberman camera set to %d: ", cv_bombcam);
	if (cv_bombcam == 0)
		CONS_Printf("Dynamic\n");
	else if (cv_bombcam == 1)
		CONS_Printf("Scroll\n");
	else if (cv_bombcam == 2)
		CONS_Printf("Static\n");
	else // failsafe
		CONS_Printf("INVALID VALUE\n");
}
#endif // BOMBCAMCVAR
#endif
