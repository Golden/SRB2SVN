// SRB2FLAME - Unique code for Super Bomberman Blast 2 (BOMB)
// Code which is unique/exclusive to its define.
// By Flame

#ifdef BOMB
#ifndef __BOMB_TYPE__
#define __BOMB_TYPE__

typedef enum
{
	bpw_invalid = 0,

	// Standard bomb powers from various bomberman games.
	// bpw_* = Bomb PoWer abbreviation

	bpw_remote, // Remote control (RC) bomb (Can be detonated with a button specified)
	bpw_rubber, // Rubber bomb (Bounces off walls)
	bpw_mine, // Mine bomb (First bomb planted hides under ground until stepped on all other bombs planted are normal)
	bpw_spike, // Spike bomb (Ability for bombs to bust through multiple blocks at once)
	bpw_power, // Power bomb (First bomb planted has max firepower, all other bombs planted are normal)

	bpw_tower, // Tower bomb (Spawns all of your bombs stacked on top of one another, like a TOWER)
	bpw_line, // Line bomb (Spawns all of your bombs in a LINE, horizontally)
			  // These are actually not types of bombs, but a special ability for bomb planting.

	BOMBPOWERS
} bombpower_t;

typedef enum
{
	bal_invalid = 0,

	// Standard player ailments from commonly known bomberman games.
	// bal_* = Bomberman AiLment abbreviation

	bal_bomberrhea, // The player continuously sets bombs when possible.
	bal_constipation, // The player is unable to set bombs.
	bal_ctrlreverse, // Reversed controls.
	bal_unstoppable, // You can't stop moving in the direction you're moving.

	BOMBSTATUS
} bombstatus_t;

// extern bombpower_t bombtype[BOMBPOWERS];
#endif //__BOMB_TYPE__
#endif // BOMB
