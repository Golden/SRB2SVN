// SRB2FLAME - Unique code for Super Bomberman Blast 2 (BOMB)
// Code which is unique/exclusive to its define.
// By Flame

#include "../../../doomstat.h"
#include "../../../doomdef.h"
#include "../../../command.h"
#include "../../../d_player.h"
#include "../../../d_netcmd.h"
#include "../../../g_game.h"

#include "BOMB_Netcmd.h"

#ifdef BOMB
static CV_PossibleValue_t bombchances_cons_t[] = {{0, "Off"}, {1, "Low"}, {2, "Medium"}, {3, "High"},
	{0, NULL}};

consvar_t cv_bombupp = {"bombup", "Medium", CV_NETVAR|CV_CHEAT, bombchances_cons_t, NULL, 0, NULL, NULL, 0, 0, NULL};
consvar_t cv_fireupp = {"fireup", "Medium", CV_NETVAR|CV_CHEAT, bombchances_cons_t, NULL, 0, NULL, NULL, 0, 0, NULL};
consvar_t cv_powerbombp = {"powerbomb", "Low", CV_NETVAR|CV_CHEAT, bombchances_cons_t, NULL, 0, NULL, NULL, 0, 0, NULL};
consvar_t cv_rubberbombp = {"rubberbomb", "Low", CV_NETVAR|CV_CHEAT, bombchances_cons_t, NULL, 0, NULL, NULL, 0, 0, NULL};
consvar_t cv_spikebombp = {"spikebomb", "Low", CV_NETVAR|CV_CHEAT, bombchances_cons_t, NULL, 0, NULL, NULL, 0, 0, NULL};
consvar_t cv_landminep = {"landmine", "Low", CV_NETVAR|CV_CHEAT, bombchances_cons_t, NULL, 0, NULL, NULL, 0, 0, NULL};
consvar_t cv_remotebombp = {"remotebomb", "Low", CV_NETVAR|CV_CHEAT, bombchances_cons_t, NULL, 0, NULL, NULL, 0, 0, NULL};
consvar_t cv_questionp = {"question", "Medium", CV_NETVAR|CV_CHEAT, bombchances_cons_t, NULL, 0, NULL, NULL, 0, 0, NULL};

void BOMB_NetInit(void)
{
	CV_RegisterVar(&cv_bombupp);
	CV_RegisterVar(&cv_fireupp);
	CV_RegisterVar(&cv_powerbombp);
	CV_RegisterVar(&cv_rubberbombp);
	CV_RegisterVar(&cv_spikebombp);
	CV_RegisterVar(&cv_landminep);
	CV_RegisterVar(&cv_remotebombp);
	CV_RegisterVar(&cv_questionp);
}
#endif
