// SRB2FLAME - Shared code
// Pieces of code which is shared among defines.
// By Flame

#include "../doomstat.h"
#include "../p_local.h"
#include "../m_random.h"
#include "../r_main.h" // R_Point*
#include "../sounds.h"

#include "FL_Common.h"
#include "Unshared/BOMB/BOMB_User.h"

#if defined(SMASH) || defined(BOMB) || defined(ZELDA)

//
// P_GetMobjForType
//
// Return the first mobj that is found with the right type,
// if no map object was found, it returns NULL.
//
// This is (or will be) used for every define.
//
mobj_t *P_GetMobjForType(mobjtype_t type)
{
	thinker_t *th;
	mobj_t *mobj;

	for (th = thinkercap.next; th != &thinkercap; th = th->next)
	{
		if (th->function.acp1 != (actionf_p1)P_MobjThinker)
			continue;

		mobj = (mobj_t *)th;
		if (mobj->type != type)
			continue;

		return mobj;
	}

	return NULL;
}

#if defined(SMASH) || defined(ZELDA)
//
// P_<Fist/Sword>Attack: This is used to attack your enemies.
//
#if defined(SMASH)
static void P_FistAttack(player_t *player, tic_t waittime)
#else // #elif defined(ZELDA)
static void P_SwordAttack(player_t *player, tic_t waittime)
#endif
{
	mobj_t *mobj, *mo = player->mo;
	fixed_t zheight = mo->z - FixedDiv(mo->info->height - mo->height,3*FRACUNIT);

	// Don't attack if...

	if ((player->pflags & PF_CARRIED) // If being carried..
	|| (player->pflags & PF_ITEMHANG) // If hanging on something..
	|| (player->pflags & PF_ROPEHANG) // Again, if hanging on something..
	|| (player->pflags & PF_MINECART) // If inside a mine cart..
	|| (player->pflags & PF_SPINNING) // Spinning..
	|| player->playerstate == PST_DEAD // Dead.
	|| mo->reactiontime // Reeling from a hit.
	|| player->pflags & PF_STASIS || player->powers[pw_nocontrol] // Have no control over your person.
	|| player->attacking // Already attacking.
	|| player->spectator) // A Spectator
		return;

	if (!P_IsObjectOnGround(mo)) // In the air..
	{
#ifdef SMASH
		// TODO: P_AirFistAttack
#endif
		return;
	}

#ifdef SMASH
	if (player->powers[pw_super]) // Super
		return;

	/* if (abs(cmd->sidemove) < 10) // If you just pressed the movement button.
	{
		// TODO: P_SmashAttack
		return;
	}*/

	if (player->speed > 5) // Moving too fast for a punch..
	{
		// TODO: P_SlideFistAttack
		return;
	}

	/* if (!cmd->sidemove && player->speed <= 5)
	{
		// TODO: P_StandardAttack
		// Ie. Merge everything below into this
		return;
	}*/

	if (mo->fuse <= 0 && player->attackcount > 0) // If you ran out of time to preform your attacks..
		player->attackcount = 0; // Reset your attack count!

	// Do an attack!
	if (player->attackcount == 0) // First attack
		P_SetPlayerMobjState(mo, S_PLAY_PUNCH1);
	else if (player->attackcount == 1) // Second attack
		P_SetPlayerMobjState(mo, S_PLAY_PUNCH4);
	else if (player->attackcount == 2) // Third attack
		P_SetPlayerMobjState(mo, S_PLAY_KICK1);

	if (zheight < mo->floorz)
		zheight = mo->floorz;

//	mo->momx = mo->momy = 0; // HALT YOUR MOVEMENT!
	mo->momx /= 2;
	mo->momy /= 2; // SLOW YOUR MOVEMENT! Just stopping is choppy.

	mobj = P_SpawnMobj(mo->x, mo->y, zheight, MT_FIST);

	if (player->attackcount < 2)
	{
		player->attacking = waittime;
		mo->fuse = waittime+5; // Keep attacking!
		//player->mo->reactiontime = waittime; // Don't do anything else.
		player->attackcount++;
	}
	else
	{
		player->attacking = waittime*3; // Wait time is doubled to preform next series of attacks.
		player->attackcount = 0;
	}
	P_SetTarget(&mobj->target, mo);
	//A_CapeChase(mobj);
#else // #elif defined(ZELDA)
	switch(M_Random()/64)
	{
		case 0:
		default:
			//S_StartSound(mo, sfx_swipe);
			break;
		case 1:
			//S_StartSound(mo, sfx_swipe2);
			break;
		case 2:
			//S_StartSound(mo, sfx_swipe3);
			break;
		case 3:
			//S_StartSound(mo, sfx_swipe4);
			break;
	}
	//P_SetPlayerMobjState(mo, S_PLAY_SWORD_ATK1);

	mo->momx = mo->momy = 0;
	//player->rmomx = player->rmomy = 0;

	player->attacking = waittime;

	if (zheight < mo->floorz)
		zheight = mo->floorz;

	mobj = P_SpawnMobj(mo->x, mo->y, zheight, MT_SWORD);
	P_SetTarget(&mobj->target, mo);
	//A_CapeChase(mobj);
#endif
}
#endif // SMASH / ZELDA

#if defined(BOMB) || defined(ZELDA)
//
// P_BombStuff
// Drop a bomb...
//
void P_BombStuff(player_t* player, ticcmd_t *cmd)
{
#ifndef BOMB
	mobj_t *mobj;
#endif
	// Target: Point distribution
	// Tracer: Used for pickup

	if (player->pflags & PF_STASIS || player->powers[pw_nocontrol])
		return;

#ifdef BOMB
	// Bomber Constipation?
	if (player->ailment[bal_constipation])
		return; // Can't plant bombs then, no matter how much you want to!
#endif

	if ((gametype == GT_MATCH || (gametype == GT_TAG && (player->pflags & PF_TAGIT)) || gametype == GT_COOP)
		&& (cmd->buttons & BT_USE)

		// What don't we want the player to be doing then?
		&& !(player->pflags & PF_USEDOWN)
		&& !player->pickedup
		&& player->playerstate != PST_REBORN
		&& player->playerstate != PST_DEAD
		&& !(player->powers[pw_bombshield] && (maptol & TOL_UNUSED))
		&& !player->exiting
		&& !player->powers[pw_tailsfly]
		&& (player->mo->state != &states[player->mo->info->painstate])
	)
	{
		// If you have the maximum amount of bombs you can plant on the field...
		if (player->bombsout >= (2 + player->bombcount))
			return; // Don't plant anymore! That would be silly!
		
#ifdef BOMB
		P_SpawnBombMobjByType(player); // See BOMB_Mobj.c.
#else
		player->bombsout += 1;

		// Spawn the bomb where you are, and make it your target to give you points.
		mobj = P_SetTarget(&P_SpawnMobj(player->mo->x, player->mo->y, player->mo->z, MT_BOMB)->target, player->mo);
		mobj->flags |= MF_TRANSLATION;
		mobj->color = player->skincolor;
		mobj->flameflags |= FLF_BOMBKICK;
		// And Set the time limit before it explodes!
		if (gametype == GT_TAG)
			mobj->fuse = 46; // In tag, we want the Bombs to explode faster.
		else
			mobj->fuse = 102;
		if (cv_debug)
			CONS_Printf("Planted the bomb succesfully.\n");
#endif
	}

	// Special Bomb checks.
	if (player->mo->tracer && (player->mo->tracer->state-states == S_DISS))
	{
		if (player->pickedup)
		{
			player->pickedup = false;
			P_SetTarget(&player->mo->tracer, NULL);
		}
	}
	else if (player->playerstate == PST_DEAD)
	{
		player->bombsout = 0;
		P_SetTarget(&player->mo->target, P_SetTarget(&player->mo->tracer, NULL));
		player->pickedup = false;
	}
}
#endif // BOMB / ZELDA

//
// P_LookForItem
//
static mobj_t *P_LookForItem(mobj_t *me)
{
	mobj_t *mo, *lastmo;
	thinker_t *think;

	if (!me || !me->player)
		return NULL;

	for (think = thinkercap.next, mo = NULL, lastmo = NULL; think != &thinkercap; think = think->next)
	{
		if (think->function.acp1 != (actionf_p1)P_MobjThinker) // Not a mobj thinker
			continue;

		mo = (mobj_t *)think;
#ifdef SMASH
		// Mobj check
		if (mo->type != MT_HEARTCONTAINER && mo->type != MT_SHELL)
			continue;

		// State check
		if (!mo->health	|| !(mo->state-states == S_DISS
		|| mo->state-states == S_XPLD1
		|| mo->state-states == S_XPLD2
		|| mo->state-states == S_XPLD3
		|| mo->state-states == S_XPLD4
		|| mo->state-states == S_SHELL1
		|| mo->state-states == S_SHELL2
		|| mo->state-states == S_SHELL3
		|| mo->state-states == S_SHELL4))
			continue;
#elif defined(BOMB) || defined(ZELDA)
		// Only pick up certain types of mobjs...
		if (!(mo->flameflags == FLF_BOMBKICK))
			continue;
#endif

		// Too far away to pick up normally?
		if ((P_AproxDistance(me->x - mo->x, me->y - mo->y) > me->radius*2 + mo->radius*2
		|| me->z + me->height < mo->z || me->z > mo->z + mo->height))
			continue;

		if (lastmo && // Last mo is closer to me?
		P_AproxDistance(P_AproxDistance(me->x-mo->x, me->y-mo->y), me->z-mo->z) >
		P_AproxDistance(P_AproxDistance(me->x-lastmo->x, me->y-lastmo->y), me->z-lastmo->z))
			continue;

		// Found a tracer.
		lastmo = mo;
	}
	return lastmo;
}

//
// P_PlayerAction
//
// Lets make the player do an action!
// If an object is close to an object, make the player do the corresponding action!
//
// Originally from the SRB2LOZ source r619 and modified
//
void P_PlayerAction(player_t *player, ticcmd_t *cmd)
{
	mobj_t *mo, *pickup;

	mo = player->mo;
	pickup = mo->tracer;

	if (player->pflags & PF_STASIS || player->powers[pw_nocontrol])
		return;

	// If you've picked up an object...
	if (pickup && player->pickedup && pickup->flags & MF_SCENERY)
	{
#ifdef SMASH
		mo->fuse = 15*TICRATE; // Keep the object from dissapearing
#endif

		// Set its position to yours.
		// P_UnsetThingPosition(pickup);
		//pickup->momx = pickup->momy = pickup->momz = pickup->pmomz = 0;
#if defined(BOMB) || defined(ZELDA)
		if (pickup->type == MT_BOMB
#ifdef BOMB
			|| pickup->type == MT_POWERBOMB
			|| pickup->type == MT_SPIKEBOMB
			|| pickup->type == MT_RUBBERBOMB
#endif
		)
		{
			if (gametype == GT_TAG && pickup->fuse <= 46)
				pickup->fuse = 45;
			else if (pickup->fuse <= 74)
				pickup->fuse = 75;

#ifdef BOMB
			if (pickup->type == MT_POWERBOMB)
				P_SetMobjState(pickup, S_POWERBOMB1);
			else if (pickup->type == MT_SPIKEBOMB)
				P_SetMobjState(pickup, S_SPIKEBOMB1);
			else if (pickup->type == MT_RUBBERBOMB)
				P_SetMobjState(pickup, S_RUBBERBOMB1);
			else
#endif
				P_SetMobjState(pickup, S_BOMB1);
		}
#endif // BOMB / ZELDA

		if (twodlevel || (player->mo->flags2 & MF2_TWOD)) // In 2D, make the item hover above your head.
		{
			pickup->x = mo->x;
			pickup->y = mo->y;
			pickup->z = mo->z + 65*FRACUNIT;
		}
		else if (!(twodlevel || (player->mo->flags2 & MF2_TWOD))) // In 3D, make the item slightly in front of you
		{
			pickup->x = mo->x + pickup->momx + P_ReturnThrustX(mo, mo->angle, mo->radius*3);
			pickup->y = mo->y + pickup->momy + P_ReturnThrustY(mo, mo->angle, mo->radius*3);
			pickup->z = mo->z + 40*FRACUNIT;
		}
		pickup->ceilingz = pickup->z + pickup->height;
		pickup->floorz = pickup->z;
		pickup->angle = mo->angle;
#ifdef SMASH
		if (pickup->type == MT_HEARTCONTAINER) // Special case for a heart container.
		{
			// Heal the player!
			player->percent -= 100;
			if (player->percent < 0)
				player->percent = 0;
			player->pickedup = false;
			P_SetMobjState(pickup, S_XPLD1);
			pickup->momx = pickup->momz = 0;
			P_SetTarget(&pickup, P_SetTarget(&mo->tracer, NULL));
		}
#endif
		//P_SetThingPosition(pickup);
	}

	if (pickup && player->pickedup && pickup->flags & MF_SCENERY
		&& !player->weapondelay
#if defined(SMASH)
		&& (cmd->buttons & BT_THROW)
#elif defined(BOMB) || defined(ZELDA)
		&& (cmd->buttons & BT_JUMP)
#endif
	)
	{
		// Toss the pickup!
		player->weapondelay = TICRATE/4;

		//P_UnsetThingPosition(pickup);
		if (twodlevel)
			P_Thrust(pickup, mo->angle, 4*FRACUNIT);
		else
		{
			P_Thrust(pickup, mo->angle, 8*FRACUNIT);

			// Your speed affects how far you throw the item.
			pickup->momx += mo->momx;
			pickup->momy += mo->momy;
		}
#if defined(BOMB) || defined(ZELDA)
		// Your Z movement also affects how high you throw the bomb
		if (!mo->momz)
			pickup->momz = 10*FRACUNIT;
		else
			pickup->momz += 10*mo->momz;

#elif defined(SMASH)
		pickup->momz += mo->momz;

		//pickup->flags = pickup->info->flags;
		if (pickup->type == MT_SHELL)
		{
			pickup->flags |= MF_SPECIAL;
			P_SetMobjState(pickup, S_SHELL1);
		}
#endif
		pickup->flags &= ~(MF_NOGRAVITY|MF_NOCLIP|MF_SCENERY);
		P_SetTarget(&pickup, P_SetTarget(&mo->tracer, NULL));
		//P_SetThingPosition(pickup);
		player->pickedup = false;
		//CONS_Printf("The player (should have) just threw an object!\n");
	}
	else if (!pickup && !player->weapondelay
#ifdef SMASH
	// && (cmd->buttons & BT_A)
#elif defined(BOMB) || defined(ZELDA)
	&& (cmd->buttons & BT_JUMP)
#endif
	)
	{
		// Check for pickup
		pickup = P_SetTarget(&mo->tracer, P_LookForItem(mo));
		if (pickup)
		{
			player->weapondelay = TICRATE/4;
			// Flag Checks:
			pickup->flags |= MF_SCENERY;
			if (!(pickup->flags & MF_NOGRAVITY))
				pickup->flags |= MF_NOGRAVITY;
			if (!(pickup->flags & MF_NOCLIP))
				pickup->flags |= MF_NOCLIP;
			if (pickup->flags & MF_SPECIAL)
				pickup->flags &= ~MF_SPECIAL;
#ifdef SMASH
			player->attackcount = 0;
#endif
			player->pickedup = true;
			//CONS_Printf("The player (should have) just picked up an object!\n");
		}
#if defined(SMASH) || defined(ZELDA)
		else if (!pickup && (twodlevel || (player->mo->flags2 & MF2_TWOD)))
		{
			// Having these two separate defines like this may
			// not make sense now, but they will soon.
#if defined(SMASH) // Attack with your fists!
			P_FistAttack(player, 15);
#else // #elif defined(ZELDA) // Attack with your sword!
			P_SwordAttack(player, TICRATE/2);
#endif
		}
#endif // SMASH / ZELDA
	}
}
#endif // SMASH / BOMB / ZELDA
