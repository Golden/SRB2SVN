// SRB2FLAME - Resource file for Mobj objects, states, and sounds.
// Ported from the SRB2109FLAME Branch
// By Flame, based off the JTESHUFFLE source

#include "../doomdef.h"
#include "../g_game.h"
#include "../m_menu.h"
#include "../m_random.h"
#include "../p_local.h"
#include "../p_mobj.h"
#include "../r_main.h"
#include "../s_sound.h"
#include "FL_Info.h"

#ifdef HWRENDER
#include "../hardware/hw_light.h"
#endif

#if 0
//
// FL_MakeMobj
//
// Make use of this function to cut down file size significantly
// The scary thing about this is that it works properly.
//
// Example of usage:
// FL_MakeMobj(MT_BOMB, -1, S_BOMB1, 1, S_DISS, -1, sfx_None, S_DISS, 0, 0, 0, S_DISS, S_DISS, S_DISS, sfx_bexplode, 8, 10*FRACUNIT, 26*FRACUNIT, 0, 16, 0, sfx_None, MF_SHOOTABLE|MF_NOCLIP, MT_REDEXPLODE);
//
static void FL_MakeMobj(mobjtype_t mobj,
						INT32 doomn,
						statenum_t spwnst,
						INT32 spwnh,
						statenum_t seest,
						INT32 reactt,
						sfxenum_t atksnd,
						statenum_t painst,
						INT32 painch,
						sfxenum_t painsnd,
						statenum_t meleest,
						statenum_t missilest,
						statenum_t deathst,
						statenum_t xdeathst,
						sfxenum_t deathsnd,
						fixed_t spd,
						fixed_t rad,
						fixed_t ht,
						fixed_t dispoffset,
						INT32 ma,
						INT32 dmg,
						sfxenum_t activsnd,
						INT32 fl,
						statenum_t raisest)
{
	// Default Mobj info
	mobjinfo[mobj] = mobjinfo[MT_DISS];

	if (spwnst)
		mobjinfo[mobj].spawnstate = spwnst;
	if (spwnh)
		mobjinfo[mobj].spawnhealth = spwnh;
	if (seest)
		mobjinfo[mobj].seestate = seest;
	if (reactt)
		mobjinfo[mobj].reactiontime = reactt;
	if (atksnd)
		mobjinfo[mobj].attacksound = atksnd;
	if (painst)
		mobjinfo[mobj].painstate = painst;
	if (painch)
		mobjinfo[mobj].painchance = painch;
	if (painsnd)
		mobjinfo[mobj].painsound = painsnd;
	if (meleest)
		mobjinfo[mobj].meleestate = meleest;
	if (deathst)
		mobjinfo[mobj].deathstate = deathst;
	if (xdeathst)
		mobjinfo[mobj].xdeathstate = xdeathst;
	if (deathsnd)
		mobjinfo[mobj].deathsound = deathsnd;
	if (spd)
		mobjinfo[mobj].speed = spd;
	if (rad)
		mobjinfo[mobj].radius = rad;
	if (ht)
		mobjinfo[mobj].height = ht;
	if (dispoffset)
		mobjinfo[mobj].dispoffset = dispoffset;
	if (ma)
		mobjinfo[mobj].mass = ma;
	if (dmg)
		mobjinfo[mobj].damage = dmg;
	if (activsnd)
		mobjinfo[mobj].activesound = activsnd;
	if (fl)
		mobjinfo[mobj].flags = fl;
	if (raisest)
		mobjinfo[mobj].raisestate = raisest;
}
#endif 

#ifdef SMASH // Resources for Super Smash Brothers: Sonic Showdown
void SMASH_Init(void)
{
	size_t thing;

	//****** Mobj types ******//
	thing = MT_FIST; // 'Fist' object. This is more or less a type of hitbox for punching
	mobjinfo[thing] = mobjinfo[MT_DISS];
	mobjinfo[thing].radius = 25*FRACUNIT;
	mobjinfo[thing].height = 55*FRACUNIT;
	mobjinfo[thing].speed = 8;
	mobjinfo[thing].flags = MF_FLOAT|MF_NOGRAVITY|MF_MISSILE;

	thing = MT_STAR; // Starman
	mobjinfo[thing] = mobjinfo[MT_DISS];
	mobjinfo[thing].spawnstate = S_STAR;
	mobjinfo[thing].reactiontime = TICRATE;
	mobjinfo[thing].radius = 16*FRACUNIT;
	mobjinfo[thing].height = 32*FRACUNIT;
	mobjinfo[thing].mass = 100;
	mobjinfo[thing].flags = MF_SPECIAL|MF_BOUNCE;

	thing = MT_HEARTCONTAINER; // Heart Container
	mobjinfo[thing] = mobjinfo[MT_STAR];
	mobjinfo[thing].spawnstate = S_HEART;
	mobjinfo[thing].flags = MF_SHOOTABLE|MF_NOCLIPTHING|MF_PUSHABLE|MF_SLIDEME;

	thing = MT_SMASHBALL; // Smash ball. 'HIT IT!'
	mobjinfo[thing] = mobjinfo[MT_STAR];
	mobjinfo[thing].spawnstate = S_SMSHBALLUP;
	mobjinfo[thing].spawnhealth = 3;
	mobjinfo[thing].reactiontime = TICRATE/2;
	mobjinfo[thing].painstate = S_SMSHBALLUP;
	mobjinfo[thing].painsound = sfx_dmpain;
	mobjinfo[thing].flags = MF_SPECIAL|MF_SHOOTABLE|MF_NOGRAVITY|MF_FLOAT;

	thing = MT_SMASHBALLFLAME;
	mobjinfo[thing] = mobjinfo[MT_BLACKORB];
	mobjinfo[thing].spawnstate = S_SMASHFLAME1;
	mobjinfo[thing].speed = pw_finalsmash;

	thing = MT_SUPERFLICKY;
	mobjinfo[thing] = mobjinfo[MT_BIRD];
	mobjinfo[thing].doomednum = -1;
	mobjinfo[thing].spawnstate = S_SFLICK1;
	mobjinfo[thing].speed = 30*FRACUNIT;
	mobjinfo[thing].flags |= MF_TRANSLATION;

	thing = MT_SHIELD;
	mobjinfo[thing] = mobjinfo[MT_THOK];
	mobjinfo[thing].doomednum = -1;
	mobjinfo[thing].spawnstate = S_SHLD1;
	mobjinfo[thing].height = 56*FRACUNIT;

#ifdef CHAOSBLAST_OLD
	thing = MT_CBLASTORB;
	mobjinfo[thing] = mobjinfo[MT_THOK];
	mobjinfo[thing].doomednum = -1;
	mobjinfo[thing].spawnstate = S_CBLASTORB;
#endif

#ifdef SMASHCAM // Who knows? Someone might want to disable the SMASHCAM define.
	thing = MT_CAMMAX; // [Outer/Right] Camera Boundary
	mobjinfo[thing] = mobjinfo[MT_DISS];
	mobjinfo[thing].doomednum = 400;
	mobjinfo[thing].spawnstate = S_INVISIBLE;

	thing = MT_CAMMIN; // [Inner/Left] Camera Boundary
	mobjinfo[thing] = mobjinfo[MT_CAMMAX];
	mobjinfo[thing].doomednum = 401;
#endif

	//****** States ******//

	// Extra Player frames
	// First Punch
	thing = S_PLAY_PUNCH1;
	states[thing].sprite = SPR_PLAY;
	states[thing].frame = 34;
	states[thing].tics = 4;
	states[thing].nextstate = S_PLAY_PUNCH2;

	thing = S_PLAY_PUNCH2;
	states[thing] = states[S_PLAY_PUNCH1];
	states[thing].frame = 35;
	states[thing].nextstate = S_PLAY_PUNCH3;

	thing = S_PLAY_PUNCH3;
	states[thing] = states[S_PLAY_PUNCH1];
	states[thing].frame = 36;
	states[thing].nextstate = S_PLAY_STND;

	// Second Punch
	thing = S_PLAY_PUNCH4;
	states[thing] = states[S_PLAY_PUNCH1];
	states[thing].frame = 37;
	states[thing].nextstate = S_PLAY_PUNCH5;

	thing = S_PLAY_PUNCH5;
	states[thing] = states[S_PLAY_PUNCH1];
	states[thing].frame = 38;
	states[thing].nextstate = S_PLAY_PUNCH6;

	thing = S_PLAY_PUNCH6;
	states[thing] = states[S_PLAY_PUNCH3];
	states[thing].frame = 39;

	// Kicks
	thing = S_PLAY_KICK1;
	states[thing].sprite = SPR_PLAY;
	states[thing].frame = 40;
	states[thing].tics = 6;
	states[thing].nextstate = S_PLAY_KICK2;

	thing = S_PLAY_KICK2;
	states[thing] = states[S_PLAY_KICK1];
	states[thing].frame = 41;
	states[thing].nextstate = S_PLAY_KICK3;

	thing = S_PLAY_KICK3;
	states[thing] = states[S_PLAY_KICK1];
	states[thing].frame = 42;
	states[thing].nextstate = S_PLAY_KICK4;

	thing = S_PLAY_KICK4;
	states[thing] = states[S_PLAY_PUNCH1]; // This is just so I can get the tics
	states[thing].frame = 43;
	states[thing].nextstate = S_PLAY_KICK4;

	thing = S_PLAY_KICK5;
	states[thing] = states[S_PLAY_PUNCH1];
	states[thing].frame = 44;
	states[thing].nextstate = S_PLAY_KICK6;

	thing = S_PLAY_KICK6;
	states[thing] = states[S_PLAY_PUNCH1];
	states[thing].frame = 45;
	states[thing].nextstate = S_PLAY_STND;


	/* // Aaand.. Here's that 'fist' state for that 'fist' object.
	thing = S_FIST;
	states[thing] = states[S_DISS];
	states[thing].tics = 10;*/

	thing = S_STAR; // Starman
	states[thing] = states[S_DISS];
	states[thing].sprite = SPR_STAR;
	states[thing].frame = 32768;
	states[thing].nextstate = S_STAR;

	thing = S_HEART; // Heart Container
	states[thing] = states[S_DISS];
	states[thing].sprite = SPR_HART;
	states[thing].frame = 32768;
	states[thing].nextstate = S_HEART;

	thing = S_SMSHBALLUP; // Smashball (Moving up?)
	states[thing] = states[S_DISS];
	states[thing].sprite = SPR_FSMH;
	states[thing].frame = 32768;
	states[thing].nextstate = S_SMSHBALLDOWN;

	thing = S_SMSHBALLDOWN; // Smashball (Moving down?)
	states[thing] = states[S_SMSHBALLUP];
	states[thing].frame = 32768;
	states[thing].nextstate = S_SMSHBALLUP;

	// Final Smash Fire Aura animation
	thing = S_SMASHFLAME1;
	states[thing] = states[S_DISS];
	states[thing].sprite = SPR_SFIR;
	states[thing].frame = 32768;
	states[thing].tics = 1;
	states[thing].nextstate = S_SMASHFLAME2;

	thing = S_SMASHFLAME2;
	states[thing] = states[S_SMASHFLAME1];
	states[thing].frame = 32769;
	states[thing].nextstate = S_SMASHFLAME3;

	thing = S_SMASHFLAME3;
	states[thing] = states[S_SMASHFLAME1];
	states[thing].frame = 32770;
	states[thing].nextstate = S_SMASHFLAME4;

	thing = S_SMASHFLAME4;
	states[thing] = states[S_SMASHFLAME1];
	states[thing].frame = 32771;
	states[thing].nextstate = S_SMASHFLAME5;

	thing = S_SMASHFLAME5;
	states[thing] = states[S_SMASHFLAME1];
	states[thing].frame = 32772;
	states[thing].nextstate = S_SMASHFLAME6;

	thing = S_SMASHFLAME6;
	states[thing] = states[S_SMASHFLAME1];
	states[thing].frame = 32773;
	states[thing].nextstate = S_SMASHFLAME7;

	thing = S_SMASHFLAME7;
	states[thing] = states[S_SMASHFLAME1];
	states[thing].frame = 32774;
	states[thing].nextstate = S_SMASHFLAME1;

	// Flicky animations [TAILS ONLY]
	thing = S_SFLICK1;
	states[thing] = states[S_BIRD1];
	states[thing].frame = 32768;
	states[thing].nextstate = S_SFLICK2;

	thing = S_SFLICK2;
	states[thing] = states[S_BIRD1];
	states[thing].frame = 32769;
	states[thing].nextstate = S_SFLICK1;


	thing = S_SHLD1; // SSB:SS Shield.
	states[thing] = states[S_DISS];
	states[thing].sprite = SPR_SHLD;
	states[thing].tics = -1;

	// Shadow doing a Chaos Blast blast animation
	thing = S_SHADBLST1;
	states[thing] = states[S_DISS];
	states[thing].sprite = SPR_SHCB;
	states[thing].frame = 0;
	states[thing].tics = 4;
	states[thing].nextstate = S_SHADBLST2;

	thing = S_SHADBLST2;
	states[thing] = states[S_SHADBLST1];
	states[thing].frame = 1;
	states[thing].nextstate = S_SHADBLST3;

	thing = S_SHADBLST3;
	states[thing] = states[S_SHADBLST1];
	states[thing].frame = 32770; // 32770 = 2nd frame, with.. lighting effects?
	states[thing].nextstate = S_SHADBLST4;

	thing = S_SHADBLST4;
	states[thing] = states[S_SHADBLST1];
	states[thing].frame = 3;
	states[thing].tics = 3;
	states[thing].nextstate = S_SHADBLST5;

	thing = S_SHADBLST5;
	states[thing] = states[S_SHADBLST4];
	states[thing].frame = 4;
	states[thing].nextstate = S_SHADBLST6;

	thing = S_SHADBLST6;
	states[thing] = states[S_SHADBLST4];
	states[thing].frame = 5;
	states[thing].nextstate = S_SHADBLST7;

	thing = S_SHADBLST7;
	states[thing] = states[S_SHADBLST1];
	states[thing].frame = 6;
	states[thing].tics = 224;
	states[thing].nextstate = S_NIGHTSDRONE1;

#ifdef CHAOSBLAST_OLD
	thing = S_CBLASTORB;
	states[thing] = states[S_THOK1];
	states[thing].tics = -1;
#endif

	//***** Sprite files? *****//
	thing = SPR_STAR; // Starman
	strcpy(sprnames[thing],"STAR");
#ifdef HWRENDER
	t_lspr[thing] = &lspr[NOLIGHT];
#endif

	thing = SPR_HART; // Heart Container
	strcpy(sprnames[thing],"HART");
#ifdef HWRENDER
	t_lspr[thing] = &lspr[NOLIGHT];
#endif

	thing = SPR_FSMH; // Final Smash
	strcpy(sprnames[thing],"FSMH");
#ifdef HWRENDER
	t_lspr[thing] = &lspr[NOLIGHT];
#endif

	thing = SPR_SFIR; // Final Smash Fire Aura
	strcpy(sprnames[thing],"SFIR");
#ifdef HWRENDER
	t_lspr[thing] = &lspr[NOLIGHT];
#endif

	thing = SPR_TARG; // Target
	strcpy(sprnames[thing],"TARG");
#ifdef HWRENDER
	t_lspr[thing] = &lspr[NOLIGHT];
#endif

	thing = SPR_SHLD; // Shield
	strcpy(sprnames[thing],"SHLD");
#ifdef HWRENDER
	t_lspr[thing] = &lspr[NOLIGHT];
#endif

	thing = SPR_SHCB; // Shadow Chaos Blast sprite set
	strcpy(sprnames[thing],"SHCB");
#ifdef HWRENDER
	t_lspr[thing] = &lspr[NOLIGHT];
#endif

	//***** Sound files *****//
	// SSB Item sounds
	thing = sfx_sbounce;
	S_sfx[thing] = S_sfx[sfx_None];
	S_sfx[thing].name = "sbounce";
	S_sfx[thing].singularity = true;
	S_sfx[thing].priority = 96;
	// S_sfx[thing].pitch = SF_NOINTERRUPT|SF_X4AWAYSOUND; // Refrence

	thing = sfx_itemspwn;
	S_sfx[thing] = S_sfx[sfx_sbounce];
	S_sfx[thing].name = "itemspwn";

	// Announcer Sound files
	thing = sfx_ssbsay1; // "ONE!"
	S_sfx[thing] = S_sfx[sfx_None];
	S_sfx[thing].name = "ssbsay1";
	S_sfx[thing].singularity = false;
	S_sfx[thing].priority = 127;

	thing = sfx_ssbsay2; // "TWO!"
	S_sfx[thing] = S_sfx[sfx_ssbsay1];
	S_sfx[thing].name = "ssbsay2";

	thing = sfx_ssbsay3; // "THREE!"
	S_sfx[thing] = S_sfx[sfx_ssbsay1];
	S_sfx[thing].name = "ssbsay3";

	thing = sfx_ssbsaygo; // "GO!!"
	S_sfx[thing] = S_sfx[sfx_ssbsay1];
	S_sfx[thing].name = "ssbsaygo";

	thing = sfx_ssbgames; // "GAME SET!"
	S_sfx[thing] = S_sfx[sfx_ssbsay1];
	S_sfx[thing].name = "ssbgames";
}
#endif // SMASH

#ifdef BOMB // Resources for Super Bomberman Blast (2 ?)
void BOMB_Init(void)
{
	size_t thing;

	//****** Mobj types ******//
	// See BOMB_Type.h for more information about these mobjs.
	thing = MT_BOMB;
	mobjinfo[thing] = mobjinfo[MT_DISS];
	mobjinfo[thing].spawnstate = S_BOMB1;
	mobjinfo[thing].spawnhealth = 1;
	mobjinfo[thing].deathsound = sfx_bexplode;
	mobjinfo[thing].radius = 10*FRACUNIT;
	mobjinfo[thing].height = 26*FRACUNIT;
	mobjinfo[thing].mass = 16;
	mobjinfo[thing].speed = 8;
	mobjinfo[thing].flags = MF_SHOOTABLE|MF_NOCLIP;
	mobjinfo[thing].raisestate = MT_REDEXPLODE;

	thing = MT_POWERBOMB;
	mobjinfo[thing] = mobjinfo[MT_BOMB]; // Copy just about everything of MT_BOMB.
	mobjinfo[thing].spawnstate = S_POWERBOMB1;

	thing = MT_SPIKEBOMB;
	mobjinfo[thing] = mobjinfo[MT_BOMB]; // Copy just about everything of MT_BOMB.
	mobjinfo[thing].spawnstate = S_SPIKEBOMB1;
	mobjinfo[thing].raisestate = MT_BLUEXPLODE;

	thing = MT_RUBBERBOMB;
	mobjinfo[thing] = mobjinfo[MT_BOMB]; // Copy just about everything of MT_BOMB.
	mobjinfo[thing].spawnstate = S_RUBBERBOMB1;
	mobjinfo[thing].flags |= MF_BOUNCE;

	thing = MT_MINEBOMB;
	mobjinfo[thing] = mobjinfo[MT_BOMB]; // Copy just about everything of MT_BOMB.
	mobjinfo[thing].spawnstate = S_MINEBOMB1;
	mobjinfo[thing].activesound = sfx_gbeep;
	mobjinfo[thing].flags &= ~MF_SHOOTABLE;

	thing = MT_REMOTEBOMB;
	mobjinfo[thing] = mobjinfo[MT_BOMB]; // Copy just about everything of MT_BOMB.
	mobjinfo[thing].spawnstate = S_REMOTEBOMB1;

	thing = MT_BOMBUP;
	mobjinfo[thing] = mobjinfo[MT_DISS];
	mobjinfo[thing].doomednum = 7000;
	mobjinfo[thing].spawnstate = S_BOMBUP1;
	mobjinfo[thing].spawnhealth = 1;
	mobjinfo[thing].speed = 0;
	mobjinfo[thing].reactiontime = 1;
	mobjinfo[thing].radius = 15*FRACUNIT;
	mobjinfo[thing].height = 28*FRACUNIT;
	mobjinfo[thing].damage = 2*TICRATE;
	mobjinfo[thing].mass = 16;
	mobjinfo[thing].flags = MF_SPECIAL|MF_FLOAT|MF_NOGRAVITY;
	mobjinfo[thing].raisestate = S_BOMBUPFADE1;

	thing = MT_FIREUP;
	mobjinfo[thing] = mobjinfo[MT_BOMBUP];
	mobjinfo[thing].doomednum = 7001;
	mobjinfo[thing].spawnstate = S_FIREUP1;
	mobjinfo[thing].raisestate = S_FIREUPFADE1;

	thing = MT_QUESTIONPANEL;
	mobjinfo[thing] = mobjinfo[MT_BOMBUP];
	mobjinfo[thing].doomednum = 7002;
	mobjinfo[thing].spawnstate = S_QUESTIN1;
	mobjinfo[thing].reactiontime = 30*TICRATE;
	mobjinfo[thing].raisestate = S_QUESTINFADE1;

	thing = MT_RUBBERBOMBP;
	mobjinfo[thing] = mobjinfo[MT_BOMBUP];
	mobjinfo[thing].doomednum = 7003;
	mobjinfo[thing].spawnstate = S_RUBBERPANEL1;
	mobjinfo[thing].reactiontime = 0;
	mobjinfo[thing].speed = bpw_rubber;
	mobjinfo[thing].raisestate = S_RUBBERPANELFADE1;

	thing = MT_POWERBOMBP;
	mobjinfo[thing] = mobjinfo[MT_BOMBUP];
	mobjinfo[thing].doomednum = 7004;
	mobjinfo[thing].spawnstate = S_POWERPANEL1;
	mobjinfo[thing].speed = bpw_power;
	mobjinfo[thing].raisestate = S_POWERPANELFADE1;

	thing = MT_MINEBOMBP;
	mobjinfo[thing] = mobjinfo[MT_BOMBUP];
	mobjinfo[thing].doomednum = 7005;
	mobjinfo[thing].spawnstate = S_MINEPANEL1;
	mobjinfo[thing].speed = bpw_mine;
	mobjinfo[thing].raisestate = S_MINEPANELFADE1;

	thing = MT_REMOTEBOMBP;
	mobjinfo[thing] = mobjinfo[MT_BOMBUP];
	mobjinfo[thing].doomednum = 7006;
	mobjinfo[thing].spawnstate = S_REMOTEPANEL1;
	mobjinfo[thing].speed = bpw_remote;
	mobjinfo[thing].raisestate = S_REMOTEPANELFADE1;

	thing = MT_SPIKEBOMBP;
	mobjinfo[thing] = mobjinfo[MT_BOMBUP];
	mobjinfo[thing].doomednum = 7007;
	mobjinfo[thing].spawnstate = S_SPIKEPANEL1;
	mobjinfo[thing].speed = bpw_spike;
	mobjinfo[thing].raisestate = S_SPIKEPANELFADE1;

	thing = MT_BAROM;
	mobjinfo[thing] = mobjinfo[MT_DISS];
	mobjinfo[thing].doomednum = -1; // TODO: CHANGE THIS NUMBER; Have it so mappers can place enemies on the map.
	mobjinfo[thing].spawnstate = S_BAROM_STND;
	mobjinfo[thing].spawnhealth = 1;
	mobjinfo[thing].seestate = S_BAROM_MOVE1;
	mobjinfo[thing].deathstate = S_BAROM_DIE;
	mobjinfo[thing].radius = 16*FRACUNIT;
	mobjinfo[thing].height = 56*FRACUNIT;
	mobjinfo[thing].mass = 100;
	mobjinfo[thing].speed = 3;
	mobjinfo[thing].flags = MF_ENEMY|MF_SPECIAL|MF_SHOOTABLE;

	thing = MT_REDEXPLODE;
	mobjinfo[thing] = mobjinfo[MT_BOMBUP];
	mobjinfo[thing].doomednum = -1;
	mobjinfo[thing].spawnstate = S_RBOMBEXPL1;
	mobjinfo[thing].deathstate = S_DISS;
	mobjinfo[thing].radius = 10*FRACUNIT;
	mobjinfo[thing].height = 22*FRACUNIT;
	mobjinfo[thing].flags |= MF_FIRE;

	thing = MT_BLUEXPLODE;
	mobjinfo[thing] = mobjinfo[MT_REDEXPLODE];
	mobjinfo[thing].spawnstate = S_BBOMBEXPL1;

	thing = MT_CURSEAURA;
	mobjinfo[thing] = mobjinfo[MT_BLUEORB];
	mobjinfo[thing].spawnstate = S_CURSEA1;
	mobjinfo[thing].reactiontime = 0;
	mobjinfo[thing].deathstate = S_DISS;
	mobjinfo[thing].speed = 0;

#ifdef BOMBCAMCVAR
	thing = MT_BOMBCAM; // Bomberman Camera
	mobjinfo[thing] = mobjinfo[MT_DISS];
	mobjinfo[thing].doomednum = 2914; // 7010..?
	mobjinfo[thing].spawnstate = S_INVISIBLE;
#endif

	thing = MT_BOSSTHROWNBOMB;
	mobjinfo[thing] = mobjinfo[MT_THROWNEXPLOSION];
	mobjinfo[thing].speed = 30*FRACUNIT;

	//****** States ******//

	// EGGMAN *BOMBERMAN* MODIFIER //

	thing = S_EGGMOBILE_STND;
	states[thing].sprite = SPR_EGGM;
	states[thing].frame = 0;
	states[thing].tics = 70;
	states[thing].action.acp1 = (actionf_p1)A_MoveRelative;
	states[thing].var1 = 180;
	states[thing].var2 = 10;
	states[thing].nextstate = S_EGGMOBILE_FLYUP;

	thing = S_EGGMOBILE_LATK1;
	states[thing].sprite = SPR_EGGM;
	states[thing].frame = 1;
	states[thing].tics = 35;
	states[thing].action.acp1 = (actionf_p1)A_FaceTarget;
//	states[thing].var1 = 0;
//	states[thing].var2 = 0;
	states[thing].nextstate = S_EGGMOBILE_LATK2;

	thing = S_EGGMOBILE_LATK2;
	states[thing].sprite = SPR_EGGM;
	states[thing].frame = 2;
	states[thing].tics = 35;
	states[thing].action.acp1 = (actionf_p1)A_FireShot;
	states[thing].var1 = MT_BOSSTHROWNBOMB;
	states[thing].nextstate = S_EGGMOBILE_LATK3;

	thing = S_EGGMOBILE_LATK3;
	states[thing] = states[S_EGGMOBILE_LATK2];
	states[thing].frame = 4;
	states[thing].nextstate = S_EGGMOBILE_LATK4;

	thing = S_EGGMOBILE_LATK4;
	states[thing] = states[S_EGGMOBILE_LATK2];
	states[thing].frame = 2;
	states[thing].nextstate = S_EGGMOBILE_PANIC1;

	thing = S_EGGMOBILE_PANIC1;
	states[thing].sprite = SPR_EGGM;
	states[thing].frame = 1;
	states[thing].tics = 35;
	states[thing].action.acp1 = (actionf_p1)A_FaceTarget;
	states[thing].nextstate = S_EGGMOBILE_PANIC2;

	thing = S_EGGMOBILE_PANIC2;
	states[thing].sprite = SPR_EGGM;
	states[thing].frame = 1;
	states[thing].tics = 35;
	states[thing].action.acp1 = (actionf_p1)A_MoveRelative;
	states[thing].var2 = 10;
	states[thing].nextstate = S_EGGMOBILE_FLYDOWN;

	thing = S_EGGMOBILE_PAIN;
	states[thing].sprite = SPR_EGGM;
	states[thing].frame = 5;
	states[thing].tics = 24;
	states[thing].action.acp1 = (actionf_p1)A_Pain;
	states[thing].nextstate = S_EGGMOBILE_FLYUP;

	thing = S_EGGMOBILE_FLYUP;
	states[thing].sprite = SPR_EGGM;
	states[thing].frame = 0;
	states[thing].tics = 35;
	states[thing].action.acp1 = (actionf_p1)A_BossFlyUp;
	states[thing].nextstate = S_EGGMOBILE_LATK1;

	thing = S_EGGMOBILE_FLYDOWN;
	states[thing] = states[S_EGGMOBILE_FLYUP];
	states[thing].tics = 70;
	states[thing].action.acp1 = (actionf_p1)A_BossFlyDown;
	states[thing].nextstate = S_EGGMOBILE_STND;

	// END EGGMAN *BOMBERMAN* MODIFIER //

	// Make every ring related object disappear.
	thing = S_RING1;
	states[thing] = states[S_DISS];

	// Standard bomb mobj
	thing = S_BOMB1; // 'Bouncing' Bomb Animation!
	states[thing].sprite = SPR_BOMB;
	states[thing].frame = 0;
	states[thing].tics = 3;
	states[thing].nextstate = S_BOMB2;

	thing = S_BOMB2;
	states[thing] = states[S_BOMB1];
	states[thing].frame = 1;
	states[thing].nextstate = S_BOMB3;

	thing = S_BOMB3;
	states[thing] = states[S_BOMB1];
	states[thing].frame = 2;
	states[thing].nextstate = S_BOMB4;

	thing = S_BOMB4;
	states[thing] = states[S_BOMB1];
	states[thing].frame = 3;
	states[thing].nextstate = S_BOMB5;

	thing = S_BOMB5;
	states[thing] = states[S_BOMB1];
	states[thing].frame = 4;
	states[thing].nextstate = S_BOMB6;

	thing = S_BOMB6;
	states[thing] = states[S_BOMB1];
	states[thing].frame = 5;
	states[thing].nextstate = S_BOMB1;

	thing = S_BOMBRED1; // 'Bouncing' Red Bomb Animation (AKA. About to explode)
	states[thing] = states[S_BOMB1]; // Copy the inital bomb frame. I mean, it IS still the same bomb. :/
	states[thing].frame = 6;
	states[thing].tics = 3;
	states[thing].nextstate = S_BOMBRED2;

	thing = S_BOMBRED2;
	states[thing] = states[S_BOMBRED1];
	states[thing].frame = 7;
	states[thing].nextstate = S_BOMBRED3;

	thing = S_BOMBRED3;
	states[thing] = states[S_BOMBRED1];
	states[thing].frame = 8;
	states[thing].nextstate = S_BOMBRED4;

	thing = S_BOMBRED4;
	states[thing] = states[S_BOMBRED1];
	states[thing].frame = 9;
	states[thing].nextstate = S_BOMBRED5;

	thing = S_BOMBRED5;
	states[thing] = states[S_BOMBRED1];
	states[thing].frame = 10;
	states[thing].nextstate = S_BOMBRED6;

	thing = S_BOMBRED6;
	states[thing] = states[S_BOMBRED1];
	states[thing].frame = 11;
	states[thing].nextstate = S_BOMBRED1;

	// Power Bomb mobj
	thing = S_POWERBOMB1; // 'Bouncing' Bomb Animation!
	states[thing].sprite = SPR_POWB;
	states[thing].frame = 0;
	states[thing].tics = 3;
	states[thing].nextstate = S_POWERBOMB2;

	thing = S_POWERBOMB2;
	states[thing] = states[S_POWERBOMB1];
	states[thing].frame = 1;
	states[thing].nextstate = S_POWERBOMB3;

	thing = S_POWERBOMB3;
	states[thing] = states[S_POWERBOMB1];
	states[thing].frame = 2;
	states[thing].nextstate = S_POWERBOMB4;

	thing = S_POWERBOMB4;
	states[thing] = states[S_POWERBOMB1];
	states[thing].frame = 3;
	states[thing].nextstate = S_POWERBOMB5;

	thing = S_POWERBOMB5;
	states[thing] = states[S_POWERBOMB1];
	states[thing].frame = 4;
	states[thing].nextstate = S_POWERBOMB6;

	thing = S_POWERBOMB6;
	states[thing] = states[S_POWERBOMB1];
	states[thing].frame = 5;
	states[thing].nextstate = S_POWERBOMB1;

	thing = S_POWERBOMBRED1; // 'Bouncing' Red Bomb Animation (AKA. About to explode)
	states[thing] = states[S_POWERBOMB1]; // Copy the inital bomb frame.
	states[thing].frame = 6;
	states[thing].tics = 3;
	states[thing].nextstate = S_POWERBOMBRED2;

	thing = S_POWERBOMBRED2;
	states[thing] = states[S_POWERBOMBRED1];
	states[thing].frame = 7;
	states[thing].nextstate = S_POWERBOMBRED3;

	thing = S_POWERBOMBRED3;
	states[thing] = states[S_POWERBOMBRED1];
	states[thing].frame = 8;
	states[thing].nextstate = S_POWERBOMBRED4;

	thing = S_POWERBOMBRED4;
	states[thing] = states[S_POWERBOMBRED1];
	states[thing].frame = 9;
	states[thing].nextstate = S_POWERBOMBRED5;

	thing = S_POWERBOMBRED5;
	states[thing] = states[S_POWERBOMBRED1];
	states[thing].frame = 10;
	states[thing].nextstate = S_POWERBOMBRED6;

	thing = S_POWERBOMBRED6;
	states[thing] = states[S_POWERBOMBRED1];
	states[thing].frame = 11;
	states[thing].nextstate = S_POWERBOMBRED1;

	// Pass-through bomb mobj
	thing = S_SPIKEBOMB1; // 'Bouncing' Bomb Animation!
	states[thing].sprite = SPR_SPKB;
	states[thing].frame = 0;
	states[thing].tics = 3;
	states[thing].nextstate = S_SPIKEBOMB2;

	thing = S_SPIKEBOMB2;
	states[thing] = states[S_SPIKEBOMB1];
	states[thing].frame = 1;
	states[thing].nextstate = S_SPIKEBOMB3;

	thing = S_SPIKEBOMB3;
	states[thing] = states[S_SPIKEBOMB1];
	states[thing].frame = 2;
	states[thing].nextstate = S_SPIKEBOMB4;

	thing = S_SPIKEBOMB4;
	states[thing] = states[S_SPIKEBOMB1];
	states[thing].frame = 3;
	states[thing].nextstate = S_SPIKEBOMB5;

	thing = S_SPIKEBOMB5;
	states[thing] = states[S_SPIKEBOMB1];
	states[thing].frame = 4;
	states[thing].nextstate = S_SPIKEBOMB6;

	thing = S_SPIKEBOMB6;
	states[thing] = states[S_SPIKEBOMB1];
	states[thing].frame = 5;
	states[thing].nextstate = S_SPIKEBOMB1;

	thing = S_SPIKEBOMBRED1; // 'Bouncing' Red Bomb Animation (AKA. About to explode)
	states[thing] = states[S_SPIKEBOMB1]; // Copy the inital bomb frame.
	states[thing].frame = 6;
	states[thing].tics = 3;
	states[thing].nextstate = S_SPIKEBOMBRED2;

	thing = S_SPIKEBOMBRED2;
	states[thing] = states[S_SPIKEBOMBRED1];
	states[thing].frame = 7;
	states[thing].nextstate = S_SPIKEBOMBRED3;

	thing = S_SPIKEBOMBRED3;
	states[thing] = states[S_SPIKEBOMBRED1];
	states[thing].frame = 8;
	states[thing].nextstate = S_SPIKEBOMBRED4;

	thing = S_SPIKEBOMBRED4;
	states[thing] = states[S_SPIKEBOMBRED1];
	states[thing].frame = 9;
	states[thing].nextstate = S_SPIKEBOMBRED5;

	thing = S_SPIKEBOMBRED5;
	states[thing] = states[S_SPIKEBOMBRED1];
	states[thing].frame = 10;
	states[thing].nextstate = S_SPIKEBOMBRED6;

	thing = S_SPIKEBOMBRED6;
	states[thing] = states[S_SPIKEBOMBRED1];
	states[thing].frame = 11;
	states[thing].nextstate = S_SPIKEBOMBRED1;

	// Rubber (Jelly?) bomb mobj
	thing = S_RUBBERBOMB1; // 'Bouncing' Bomb Animation!
	states[thing].sprite = SPR_RUBB;
	states[thing].frame = 0;
	states[thing].tics = 3;
	states[thing].nextstate = S_RUBBERBOMB2;

	thing = S_RUBBERBOMB2;
	states[thing] = states[S_RUBBERBOMB1];
	states[thing].frame = 1;
	states[thing].nextstate = S_RUBBERBOMB3;

	thing = S_RUBBERBOMB3;
	states[thing] = states[S_RUBBERBOMB1];
	states[thing].frame = 2;
	states[thing].nextstate = S_RUBBERBOMB4;

	thing = S_RUBBERBOMB4;
	states[thing] = states[S_RUBBERBOMB1];
	states[thing].frame = 3;
	states[thing].nextstate = S_RUBBERBOMB5;

	thing = S_RUBBERBOMB5;
	states[thing] = states[S_RUBBERBOMB1];
	states[thing].frame = 4;
	states[thing].nextstate = S_RUBBERBOMB6;

	thing = S_RUBBERBOMB6;
	states[thing] = states[S_RUBBERBOMB1];
	states[thing].frame = 5;
	states[thing].nextstate = S_RUBBERBOMB1;

	/* This rubber bomb doesn't get a bouncing 'Red Bomb' animation.
	 * It would look plainly unnatural if it did.
	 * Colored Hershey's Kisses anyone?
	 */

	// Mine bomb (Landmine)
	thing = S_MINEBOMB1;
	states[thing].sprite = SPR_MNEB;
	states[thing].frame = 0;
	states[thing].tics = -1;
	states[thing].nextstate = S_MINEBOMB2;

	thing = S_MINEBOMB2;
	states[thing] = states[S_MINEBOMB1];
	states[thing].frame = 1;
	states[thing].nextstate = S_MINEBOMB1;

	// Remote bomb mobj
	thing = S_REMOTEBOMB1; // Bomb Animation!
	states[thing].sprite = SPR_REMB;
	states[thing].frame = 0;
	states[thing].tics = 3;
	states[thing].nextstate = S_REMOTEBOMB2;

	thing = S_REMOTEBOMB2;
	states[thing] = states[S_REMOTEBOMB1];
	states[thing].frame = 1;
	states[thing].nextstate = S_REMOTEBOMB3;

	thing = S_REMOTEBOMB3;
	states[thing] = states[S_REMOTEBOMB1];
	states[thing].frame = 2;
	states[thing].nextstate = S_REMOTEBOMB4;

	thing = S_REMOTEBOMB4;
	states[thing] = states[S_REMOTEBOMB1];
	states[thing].frame = 3;
	states[thing].nextstate = S_REMOTEBOMB1;

	// Bomberman power up panels
	// Bomb Panel
	thing = S_BOMBUP1;
	states[thing].sprite = SPR_BOMU;
	states[thing].frame = 0;
	states[thing].tics = 1;
	states[thing].nextstate = S_BOMBUP2;

	thing = S_BOMBUP2;
	states[thing] = states[S_BOMBUP1];
	states[thing].frame = 1;
	states[thing].nextstate = S_BOMBUP3;

	thing = S_BOMBUP3;
	states[thing] = states[S_BOMBUP1];
	states[thing].frame = 2;
	states[thing].nextstate = S_BOMBUP4;

	thing = S_BOMBUP4;
	states[thing] = states[S_BOMBUP1];
	states[thing].frame = 3;
	states[thing].nextstate = S_BOMBUP5;

	thing = S_BOMBUP5;
	states[thing] = states[S_BOMBUP1];
	states[thing].frame = 4;
	states[thing].nextstate = S_BOMBUP6;

	thing = S_BOMBUP6;
	states[thing] = states[S_BOMBUP1];
	states[thing].frame = 5;
	states[thing].nextstate = S_BOMBUP7;

	thing = S_BOMBUP7;
	states[thing] = states[S_BOMBUP1];
	states[thing].frame = 6;
	states[thing].nextstate = S_BOMBUP8;

	thing = S_BOMBUP8;
	states[thing] = states[S_BOMBUP1];
	states[thing].frame = 7;
	states[thing].nextstate = S_BOMBUP9;

	thing = S_BOMBUP9;
	states[thing] = states[S_BOMBUP1];
	states[thing].frame = 8;
	states[thing].nextstate = S_BOMBUP10;

	thing = S_BOMBUP10;
	states[thing] = states[S_BOMBUP1];
	states[thing].frame = 9;
	states[thing].nextstate = S_BOMBUP11;

	thing = S_BOMBUP11;
	states[thing] = states[S_BOMBUP1];
	states[thing].frame = 10;
	states[thing].nextstate = S_BOMBUP12;

	thing = S_BOMBUP12;
	states[thing] = states[S_BOMBUP1];
	states[thing].frame = 11;
	states[thing].nextstate = S_BOMBUP1; // Go back to S_BOMBUP1 and repeat.

	// Bomb Panel (Fade)
	thing = S_BOMBUPFADE1;
	states[thing] = states[S_BOMBUP1];
	states[thing].nextstate = S_BOMBUPFADE2;

	thing = S_BOMBUPFADE2;
	states[thing] = states[S_BOMBUPFADE1];
	states[thing].frame = 2;
	states[thing].nextstate = S_BOMBUPFADE3;

	thing = S_BOMBUPFADE3;
	states[thing] = states[S_BOMBUPFADE1];
	states[thing].frame = 4;
	states[thing].nextstate = S_BOMBUPFADE4;

	thing = S_BOMBUPFADE4;
	states[thing] = states[S_BOMBUPFADE1];
	states[thing].frame = 6;
	states[thing].nextstate = S_BOMBUPFADE5;

	thing = S_BOMBUPFADE5;
	states[thing] = states[S_BOMBUPFADE1];
	states[thing].frame = 8;
	states[thing].nextstate = S_BOMBUPFADE6;

	thing = S_BOMBUPFADE6;
	states[thing] = states[S_BOMBUPFADE1];
	states[thing].frame = 10;
	states[thing].nextstate = S_BOMBUPFADE1; // Go back to S_BOMBUPFADE1 and repeat.

	// Fire Panel
	thing = S_FIREUP1;
	states[thing].sprite = SPR_FIRU;
	states[thing].frame = 0;
	states[thing].tics = 1;
	states[thing].nextstate = S_FIREUP2;

	thing = S_FIREUP2;
	states[thing] = states[S_FIREUP1];
	states[thing].frame = 1;
	states[thing].nextstate = S_FIREUP3;

	thing = S_FIREUP3;
	states[thing] = states[S_FIREUP1];
	states[thing].frame = 2;
	states[thing].nextstate = S_FIREUP4;

	thing = S_FIREUP4;
	states[thing] = states[S_FIREUP1];
	states[thing].frame = 3;
	states[thing].nextstate = S_FIREUP5;

	thing = S_FIREUP5;
	states[thing] = states[S_FIREUP1];
	states[thing].frame = 4;
	states[thing].nextstate = S_FIREUP6;

	thing = S_FIREUP6;
	states[thing] = states[S_FIREUP1];
	states[thing].frame = 5;
	states[thing].nextstate = S_FIREUP7;

	thing = S_FIREUP7;
	states[thing] = states[S_FIREUP1];
	states[thing].frame = 6;
	states[thing].nextstate = S_FIREUP8;

	thing = S_FIREUP8;
	states[thing] = states[S_FIREUP1];
	states[thing].frame = 7;
	states[thing].nextstate = S_FIREUP9;

	thing = S_FIREUP9;
	states[thing] = states[S_FIREUP1];
	states[thing].frame = 8;
	states[thing].nextstate = S_FIREUP10;

	thing = S_FIREUP10;
	states[thing] = states[S_FIREUP1];
	states[thing].frame = 9;
	states[thing].nextstate = S_FIREUP11;

	thing = S_FIREUP11;
	states[thing] = states[S_FIREUP1];
	states[thing].frame = 10;
	states[thing].nextstate = S_FIREUP12;

	thing = S_FIREUP12;
	states[thing] = states[S_FIREUP1];
	states[thing].frame = 11;
	states[thing].nextstate = S_FIREUP1; // Go back to S_FIREUP1 and repeat.

	// Fire Panel (Fade)
	thing = S_FIREUPFADE1;
	states[thing] = states[S_FIREUP1];
	states[thing].nextstate = S_FIREUPFADE2;

	thing = S_FIREUPFADE2;
	states[thing] = states[S_FIREUPFADE1];
	states[thing].frame = 2;
	states[thing].nextstate = S_FIREUPFADE3;

	thing = S_FIREUPFADE3;
	states[thing] = states[S_FIREUPFADE1];
	states[thing].frame = 4;
	states[thing].nextstate = S_FIREUPFADE4;

	thing = S_FIREUPFADE4;
	states[thing] = states[S_FIREUPFADE1];
	states[thing].frame = 6;
	states[thing].nextstate = S_FIREUPFADE5;

	thing = S_FIREUPFADE5;
	states[thing] = states[S_FIREUPFADE1];
	states[thing].frame = 8;
	states[thing].nextstate = S_FIREUPFADE6;

	thing = S_FIREUPFADE6;
	states[thing] = states[S_FIREUPFADE1];
	states[thing].frame = 10;
	states[thing].nextstate = S_FIREUPFADE1; // Go to S_FIREUPFADE1 and repeat.

	// Question panel
	thing = S_QUESTIN1;
	states[thing].sprite = SPR_UNKN;
	states[thing].frame = 0;
	states[thing].tics = 1;
	states[thing].nextstate = S_QUESTIN2;

	thing = S_QUESTIN2;
	states[thing] = states[S_QUESTIN1];
	states[thing].frame = 1;
	states[thing].nextstate = S_QUESTIN3;

	thing = S_QUESTIN3;
	states[thing] = states[S_QUESTIN1];
	states[thing].frame = 2;
	states[thing].nextstate = S_QUESTIN4;

	thing = S_QUESTIN4;
	states[thing] = states[S_QUESTIN1];
	states[thing].frame = 3;
	states[thing].nextstate = S_QUESTIN5;

	thing = S_QUESTIN5;
	states[thing] = states[S_QUESTIN1];
	states[thing].frame = 4;
	states[thing].nextstate = S_QUESTIN6;

	thing = S_QUESTIN6;
	states[thing] = states[S_QUESTIN1];
	states[thing].frame = 5;
	states[thing].nextstate = S_QUESTIN7;

	thing = S_QUESTIN7;
	states[thing] = states[S_QUESTIN1];
	states[thing].frame = 6;
	states[thing].nextstate = S_QUESTIN8;

	thing = S_QUESTIN8;
	states[thing] = states[S_QUESTIN1];
	states[thing].frame = 7;
	states[thing].nextstate = S_QUESTIN9;

	thing = S_QUESTIN9;
	states[thing] = states[S_QUESTIN1];
	states[thing].frame = 8;
	states[thing].nextstate = S_QUESTIN10;

	thing = S_QUESTIN10;
	states[thing] = states[S_QUESTIN1];
	states[thing].frame = 9;
	states[thing].nextstate = S_QUESTIN11;

	thing = S_QUESTIN11;
	states[thing] = states[S_QUESTIN1];
	states[thing].frame = 10;
	states[thing].nextstate = S_QUESTIN12;

	thing = S_QUESTIN12;
	states[thing] = states[S_QUESTIN1];
	states[thing].frame = 11;
	states[thing].nextstate = S_QUESTIN1; // Go back to S_QUESTIN1 and repeat.

	// Question Panel (Fade)
	thing = S_QUESTINFADE1;
	states[thing] = states[S_QUESTIN1];
	states[thing].nextstate = S_QUESTINFADE2;

	thing = S_QUESTINFADE2;
	states[thing] = states[S_QUESTINFADE1];
	states[thing].frame = 2;
	states[thing].nextstate = S_QUESTINFADE3;

	thing = S_QUESTINFADE3;
	states[thing] = states[S_QUESTINFADE1];
	states[thing].frame = 4;
	states[thing].nextstate = S_QUESTINFADE4;

	thing = S_QUESTINFADE4;
	states[thing] = states[S_QUESTINFADE1];
	states[thing].frame = 6;
	states[thing].nextstate = S_QUESTINFADE5;

	thing = S_QUESTINFADE5;
	states[thing] = states[S_QUESTINFADE1];
	states[thing].frame = 8;
	states[thing].nextstate = S_QUESTINFADE6;

	thing = S_QUESTINFADE6;
	states[thing] = states[S_QUESTINFADE1];
	states[thing].frame = 10;
	states[thing].nextstate = S_QUESTINFADE1; // Go back to S_QUESTINFADE1 and repeat.

	// Skull Aura (Re: Question panel)
	thing = S_CURSEA1;
	states[thing].sprite = SPR_AURA;
	states[thing].frame = 0;
	states[thing].tics = 2;
	states[thing].nextstate = S_CURSEA2;

	thing = S_CURSEA2;
	states[thing] = states[S_CURSEA1];
	states[thing].frame = 1;
	states[thing].nextstate = S_CURSEA3;

	thing = S_CURSEA3;
	states[thing] = states[S_CURSEA1];
	states[thing].frame = 2;
	states[thing].nextstate = S_CURSEA4;

	thing = S_CURSEA4;
	states[thing] = states[S_CURSEA1];
	states[thing].frame = 3;
	states[thing].nextstate = S_CURSEA1;

	// Power Bomb Powerup panel
	thing = S_POWERPANEL1;
	states[thing].sprite = SPR_POWP;
	states[thing].frame = 0;
	states[thing].tics = 1;
	states[thing].nextstate = S_POWERPANEL2;

	thing = S_POWERPANEL2;
	states[thing] = states[S_POWERPANEL1];
	states[thing].frame = 1;
	states[thing].nextstate = S_POWERPANEL3;

	thing = S_POWERPANEL3;
	states[thing] = states[S_POWERPANEL1];
	states[thing].frame = 2;
	states[thing].nextstate = S_POWERPANEL4;

	thing = S_POWERPANEL4;
	states[thing] = states[S_POWERPANEL1];
	states[thing].frame = 3;
	states[thing].nextstate = S_POWERPANEL5;

	thing = S_POWERPANEL5;
	states[thing] = states[S_POWERPANEL1];
	states[thing].frame = 4;
	states[thing].nextstate = S_POWERPANEL6;

	thing = S_POWERPANEL6;
	states[thing] = states[S_POWERPANEL1];
	states[thing].frame = 5;
	states[thing].nextstate = S_POWERPANEL7;

	thing = S_POWERPANEL7;
	states[thing] = states[S_POWERPANEL1];
	states[thing].frame = 6;
	states[thing].nextstate = S_POWERPANEL8;

	thing = S_POWERPANEL8;
	states[thing] = states[S_POWERPANEL1];
	states[thing].frame = 7;
	states[thing].nextstate = S_POWERPANEL9;

	thing = S_POWERPANEL9;
	states[thing] = states[S_POWERPANEL1];
	states[thing].frame = 8;
	states[thing].nextstate = S_POWERPANEL10;

	thing = S_POWERPANEL10;
	states[thing] = states[S_POWERPANEL1];
	states[thing].frame = 9;
	states[thing].nextstate = S_POWERPANEL11;

	thing = S_POWERPANEL11;
	states[thing] = states[S_POWERPANEL1];
	states[thing].frame = 10;
	states[thing].nextstate = S_POWERPANEL12;

	thing = S_POWERPANEL12;
	states[thing] = states[S_POWERPANEL1];
	states[thing].frame = 11;
	states[thing].nextstate = S_POWERPANEL1; // Go back to S_POWERPANEL1 and repeat.

	// Power Bomb Powerup Panel (Fade)
	thing = S_POWERPANELFADE1;
	states[thing] = states[S_POWERPANEL1];
	states[thing].nextstate = S_POWERPANELFADE2;

	thing = S_POWERPANELFADE2;
	states[thing] = states[S_POWERPANELFADE1];
	states[thing].frame = 2;
	states[thing].nextstate = S_POWERPANELFADE3;

	thing = S_POWERPANELFADE3;
	states[thing] = states[S_POWERPANELFADE1];
	states[thing].frame = 4;
	states[thing].nextstate = S_POWERPANELFADE4;

	thing = S_POWERPANELFADE4;
	states[thing] = states[S_POWERPANELFADE1];
	states[thing].frame = 6;
	states[thing].nextstate = S_POWERPANELFADE5;

	thing = S_POWERPANELFADE5;
	states[thing] = states[S_POWERPANELFADE1];
	states[thing].frame = 8;
	states[thing].nextstate = S_POWERPANELFADE6;

	thing = S_POWERPANELFADE6;
	states[thing] = states[S_POWERPANELFADE1];
	states[thing].frame = 10;
	states[thing].nextstate = S_POWERPANELFADE1; // Go back to S_POWERPANELFADE1 and repeat.

	// Rubber Bomb Powerup panel
	thing = S_RUBBERPANEL1;
	states[thing].sprite = SPR_RUBP;
	states[thing].frame = 0;
	states[thing].tics = 1;
	states[thing].nextstate = S_RUBBERPANEL2;

	thing = S_RUBBERPANEL2;
	states[thing] = states[S_RUBBERPANEL1];
	states[thing].frame = 1;
	states[thing].nextstate = S_RUBBERPANEL3;

	thing = S_RUBBERPANEL3;
	states[thing] = states[S_RUBBERPANEL1];
	states[thing].frame = 2;
	states[thing].nextstate = S_RUBBERPANEL4;

	thing = S_RUBBERPANEL4;
	states[thing] = states[S_RUBBERPANEL1];
	states[thing].frame = 3;
	states[thing].nextstate = S_RUBBERPANEL5;

	thing = S_RUBBERPANEL5;
	states[thing] = states[S_RUBBERPANEL1];
	states[thing].frame = 4;
	states[thing].nextstate = S_RUBBERPANEL6;

	thing = S_RUBBERPANEL6;
	states[thing] = states[S_RUBBERPANEL1];
	states[thing].frame = 5;
	states[thing].nextstate = S_RUBBERPANEL7;

	thing = S_RUBBERPANEL7;
	states[thing] = states[S_RUBBERPANEL1];
	states[thing].frame = 6;
	states[thing].nextstate = S_RUBBERPANEL8;

	thing = S_RUBBERPANEL8;
	states[thing] = states[S_RUBBERPANEL1];
	states[thing].frame = 7;
	states[thing].nextstate = S_RUBBERPANEL9;

	thing = S_RUBBERPANEL9;
	states[thing] = states[S_RUBBERPANEL1];
	states[thing].frame = 8;
	states[thing].nextstate = S_RUBBERPANEL10;

	thing = S_RUBBERPANEL10;
	states[thing] = states[S_RUBBERPANEL1];
	states[thing].frame = 9;
	states[thing].nextstate = S_RUBBERPANEL11;

	thing = S_RUBBERPANEL11;
	states[thing] = states[S_RUBBERPANEL1];
	states[thing].frame = 10;
	states[thing].nextstate = S_RUBBERPANEL12;

	thing = S_RUBBERPANEL12;
	states[thing] = states[S_RUBBERPANEL1];
	states[thing].frame = 11;
	states[thing].nextstate = S_RUBBERPANEL1; // Go back to S_RUBBERPANEL1 and repeat.

	// Rubber Bomb Powerup Panel (Fade)
	thing = S_RUBBERPANELFADE1;
	states[thing] = states[S_RUBBERPANEL1];
	states[thing].nextstate = S_RUBBERPANELFADE2;

	thing = S_RUBBERPANELFADE2;
	states[thing] = states[S_RUBBERPANELFADE1];
	states[thing].frame = 2;
	states[thing].nextstate = S_RUBBERPANELFADE3;

	thing = S_RUBBERPANELFADE3;
	states[thing] = states[S_RUBBERPANELFADE1];
	states[thing].frame = 4;
	states[thing].nextstate = S_RUBBERPANELFADE4;

	thing = S_RUBBERPANELFADE4;
	states[thing] = states[S_RUBBERPANELFADE1];
	states[thing].frame = 6;
	states[thing].nextstate = S_RUBBERPANELFADE5;

	thing = S_RUBBERPANELFADE5;
	states[thing] = states[S_RUBBERPANELFADE1];
	states[thing].frame = 8;
	states[thing].nextstate = S_RUBBERPANELFADE6;

	thing = S_RUBBERPANELFADE6;
	states[thing] = states[S_RUBBERPANELFADE1];
	states[thing].frame = 10;
	states[thing].nextstate = S_RUBBERPANELFADE1; // Go back to S_RUBBERPANELFADE1 and repeat.

	// Mine Bomb Powerup panel
	thing = S_MINEPANEL1;
	states[thing].sprite = SPR_MNEP;
	states[thing].frame = 0;
	states[thing].tics = 1;
	states[thing].nextstate = S_MINEPANEL2;

	thing = S_MINEPANEL2;
	states[thing] = states[S_MINEPANEL1];
	states[thing].frame = 1;
	states[thing].nextstate = S_MINEPANEL3;

	thing = S_MINEPANEL3;
	states[thing] = states[S_MINEPANEL1];
	states[thing].frame = 2;
	states[thing].nextstate = S_MINEPANEL4;

	thing = S_MINEPANEL4;
	states[thing] = states[S_MINEPANEL1];
	states[thing].frame = 3;
	states[thing].nextstate = S_MINEPANEL5;

	thing = S_MINEPANEL5;
	states[thing] = states[S_MINEPANEL1];
	states[thing].frame = 4;
	states[thing].nextstate = S_MINEPANEL6;

	thing = S_MINEPANEL6;
	states[thing] = states[S_MINEPANEL1];
	states[thing].frame = 5;
	states[thing].nextstate = S_MINEPANEL7;

	thing = S_MINEPANEL7;
	states[thing] = states[S_MINEPANEL1];
	states[thing].frame = 6;
	states[thing].nextstate = S_MINEPANEL8;

	thing = S_MINEPANEL8;
	states[thing] = states[S_MINEPANEL1];
	states[thing].frame = 7;
	states[thing].nextstate = S_MINEPANEL9;

	thing = S_MINEPANEL9;
	states[thing] = states[S_MINEPANEL1];
	states[thing].frame = 8;
	states[thing].nextstate = S_MINEPANEL10;

	thing = S_MINEPANEL10;
	states[thing] = states[S_MINEPANEL1];
	states[thing].frame = 9;
	states[thing].nextstate = S_MINEPANEL11;

	thing = S_MINEPANEL11;
	states[thing] = states[S_MINEPANEL1];
	states[thing].frame = 10;
	states[thing].nextstate = S_MINEPANEL12;

	thing = S_MINEPANEL12;
	states[thing] = states[S_MINEPANEL1];
	states[thing].frame = 11;
	states[thing].nextstate = S_MINEPANEL1; // Go back to S_MINEPANEL1 and repeat.

	// Mine Bomb Powerup Panel (Fade)
	thing = S_MINEPANELFADE1;
	states[thing] = states[S_MINEPANEL1];
	states[thing].nextstate = S_MINEPANELFADE2;

	thing = S_MINEPANELFADE2;
	states[thing] = states[S_MINEPANELFADE1];
	states[thing].frame = 2;
	states[thing].nextstate = S_MINEPANELFADE3;

	thing = S_MINEPANELFADE3;
	states[thing] = states[S_MINEPANELFADE1];
	states[thing].frame = 4;
	states[thing].nextstate = S_MINEPANELFADE4;

	thing = S_MINEPANELFADE4;
	states[thing] = states[S_MINEPANELFADE1];
	states[thing].frame = 6;
	states[thing].nextstate = S_MINEPANELFADE5;

	thing = S_MINEPANELFADE5;
	states[thing] = states[S_MINEPANELFADE1];
	states[thing].frame = 8;
	states[thing].nextstate = S_MINEPANELFADE6;

	thing = S_MINEPANELFADE6;
	states[thing] = states[S_MINEPANELFADE1];
	states[thing].frame = 10;
	states[thing].nextstate = S_MINEPANELFADE1; // Go back to S_MINEPANELFADE1 and repeat.

	// Remote Bomb Powerup panel
	thing = S_REMOTEPANEL1;
	states[thing].sprite = SPR_REMP;
	states[thing].frame = 0;
	states[thing].tics = 1;
	states[thing].nextstate = S_REMOTEPANEL2;

	thing = S_REMOTEPANEL2;
	states[thing] = states[S_REMOTEPANEL1];
	states[thing].frame = 1;
	states[thing].nextstate = S_REMOTEPANEL3;

	thing = S_REMOTEPANEL3;
	states[thing] = states[S_REMOTEPANEL1];
	states[thing].frame = 2;
	states[thing].nextstate = S_REMOTEPANEL4;

	thing = S_REMOTEPANEL4;
	states[thing] = states[S_REMOTEPANEL1];
	states[thing].frame = 3;
	states[thing].nextstate = S_REMOTEPANEL5;

	thing = S_REMOTEPANEL5;
	states[thing] = states[S_REMOTEPANEL1];
	states[thing].frame = 4;
	states[thing].nextstate = S_REMOTEPANEL6;

	thing = S_REMOTEPANEL6;
	states[thing] = states[S_REMOTEPANEL1];
	states[thing].frame = 5;
	states[thing].nextstate = S_REMOTEPANEL7;

	thing = S_REMOTEPANEL7;
	states[thing] = states[S_REMOTEPANEL1];
	states[thing].frame = 6;
	states[thing].nextstate = S_REMOTEPANEL8;

	thing = S_REMOTEPANEL8;
	states[thing] = states[S_REMOTEPANEL1];
	states[thing].frame = 7;
	states[thing].nextstate = S_REMOTEPANEL9;

	thing = S_REMOTEPANEL9;
	states[thing] = states[S_REMOTEPANEL1];
	states[thing].frame = 8;
	states[thing].nextstate = S_REMOTEPANEL10;

	thing = S_REMOTEPANEL10;
	states[thing] = states[S_REMOTEPANEL1];
	states[thing].frame = 9;
	states[thing].nextstate = S_REMOTEPANEL11;

	thing = S_REMOTEPANEL11;
	states[thing] = states[S_REMOTEPANEL1];
	states[thing].frame = 10;
	states[thing].nextstate = S_REMOTEPANEL12;

	thing = S_REMOTEPANEL12;
	states[thing] = states[S_REMOTEPANEL1];
	states[thing].frame = 11;
	states[thing].nextstate = S_REMOTEPANEL1; // Go back to S_REMOTEPANEL1 and repeat.

	// Remote Bomb Powerup Panel (Fade)
	thing = S_REMOTEPANELFADE1;
	states[thing] = states[S_REMOTEPANEL1];
	states[thing].nextstate = S_REMOTEPANELFADE2;

	thing = S_REMOTEPANELFADE2;
	states[thing] = states[S_REMOTEPANELFADE1];
	states[thing].frame = 2;
	states[thing].nextstate = S_REMOTEPANELFADE3;

	thing = S_REMOTEPANELFADE3;
	states[thing] = states[S_REMOTEPANELFADE1];
	states[thing].frame = 4;
	states[thing].nextstate = S_REMOTEPANELFADE4;

	thing = S_REMOTEPANELFADE4;
	states[thing] = states[S_REMOTEPANELFADE1];
	states[thing].frame = 6;
	states[thing].nextstate = S_REMOTEPANELFADE5;

	thing = S_REMOTEPANELFADE5;
	states[thing] = states[S_REMOTEPANELFADE1];
	states[thing].frame = 8;
	states[thing].nextstate = S_REMOTEPANELFADE6;

	thing = S_REMOTEPANELFADE6;
	states[thing] = states[S_REMOTEPANELFADE1];
	states[thing].frame = 10;
	states[thing].nextstate = S_REMOTEPANELFADE1; // Go back to S_REMOTEPANELFADE1 and repeat.

	// Spike Bomb Powerup panel
	thing = S_SPIKEPANEL1;
	states[thing].sprite = SPR_SPKP;
	states[thing].frame = 0;
	states[thing].tics = 1;
	states[thing].nextstate = S_SPIKEPANEL2;

	thing = S_SPIKEPANEL2;
	states[thing] = states[S_SPIKEPANEL1];
	states[thing].frame = 1;
	states[thing].nextstate = S_SPIKEPANEL3;

	thing = S_SPIKEPANEL3;
	states[thing] = states[S_SPIKEPANEL1];
	states[thing].frame = 2;
	states[thing].nextstate = S_SPIKEPANEL4;

	thing = S_SPIKEPANEL4;
	states[thing] = states[S_SPIKEPANEL1];
	states[thing].frame = 3;
	states[thing].nextstate = S_SPIKEPANEL5;

	thing = S_SPIKEPANEL5;
	states[thing] = states[S_SPIKEPANEL1];
	states[thing].frame = 4;
	states[thing].nextstate = S_SPIKEPANEL6;

	thing = S_SPIKEPANEL6;
	states[thing] = states[S_SPIKEPANEL1];
	states[thing].frame = 5;
	states[thing].nextstate = S_SPIKEPANEL7;

	thing = S_SPIKEPANEL7;
	states[thing] = states[S_SPIKEPANEL1];
	states[thing].frame = 6;
	states[thing].nextstate = S_SPIKEPANEL8;

	thing = S_SPIKEPANEL8;
	states[thing] = states[S_SPIKEPANEL1];
	states[thing].frame = 7;
	states[thing].nextstate = S_SPIKEPANEL9;

	thing = S_SPIKEPANEL9;
	states[thing] = states[S_SPIKEPANEL1];
	states[thing].frame = 8;
	states[thing].nextstate = S_SPIKEPANEL10;

	thing = S_SPIKEPANEL10;
	states[thing] = states[S_SPIKEPANEL1];
	states[thing].frame = 9;
	states[thing].nextstate = S_SPIKEPANEL11;

	thing = S_SPIKEPANEL11;
	states[thing] = states[S_SPIKEPANEL1];
	states[thing].frame = 10;
	states[thing].nextstate = S_SPIKEPANEL12;

	thing = S_SPIKEPANEL12;
	states[thing] = states[S_SPIKEPANEL1];
	states[thing].frame = 11;
	states[thing].nextstate = S_SPIKEPANEL1; // Go back to S_SPIKEPANEL1 and repeat.

	// Spike Bomb Powerup Panel (Fade)
	thing = S_SPIKEPANELFADE1;
	states[thing] = states[S_SPIKEPANEL1];
	states[thing].nextstate = S_SPIKEPANELFADE2;

	thing = S_SPIKEPANELFADE2;
	states[thing] = states[S_SPIKEPANELFADE1];
	states[thing].frame = 2;
	states[thing].nextstate = S_SPIKEPANELFADE3;

	thing = S_SPIKEPANELFADE3;
	states[thing] = states[S_SPIKEPANELFADE1];
	states[thing].frame = 4;
	states[thing].nextstate = S_SPIKEPANELFADE4;

	thing = S_SPIKEPANELFADE4;
	states[thing] = states[S_SPIKEPANELFADE1];
	states[thing].frame = 6;
	states[thing].nextstate = S_SPIKEPANELFADE5;

	thing = S_SPIKEPANELFADE5;
	states[thing] = states[S_SPIKEPANELFADE1];
	states[thing].frame = 8;
	states[thing].nextstate = S_SPIKEPANELFADE6;

	thing = S_SPIKEPANELFADE6;
	states[thing] = states[S_SPIKEPANELFADE1];
	states[thing].frame = 10;
	states[thing].nextstate = S_SPIKEPANELFADE1; // Go back to S_SPIKEPANELFADE1 and repeat.


	// RED colored Exploding GFX
	thing = S_RBOMBEXPL1;
	states[thing].sprite = SPR_REXP;
	states[thing].frame = 32768; // Sprite frame 0 with a full lighting effect 32768
	states[thing].tics = 3;
	states[thing].nextstate = S_RBOMBEXPL2;

	thing = S_RBOMBEXPL2;
	states[thing] = states[S_RBOMBEXPL1];
	states[thing].frame = 32769;
	states[thing].tics = 6;
	states[thing].nextstate = S_RBOMBEXPL3;

	thing = S_RBOMBEXPL3;
	states[thing] = states[S_RBOMBEXPL1];
	states[thing].frame = 32770;
	states[thing].nextstate = S_RBOMBEXPL4;

	thing = S_RBOMBEXPL4;
	states[thing] = states[S_RBOMBEXPL1];
	states[thing].frame = 32771;
	states[thing].nextstate = S_RBOMBEXPL5;

	thing = S_RBOMBEXPL5;
	states[thing] = states[S_RBOMBEXPL1];
	states[thing].frame = 32772;
	states[thing].nextstate = S_DISS;

	// BLUE colored Exploding GFX
	thing = S_BBOMBEXPL1;
	states[thing].sprite = SPR_BEXP;
	states[thing].frame = 32768; // Sprite frame 0 with a full lighting effect 32768
	states[thing].tics = 3;
	states[thing].nextstate = S_BBOMBEXPL2;

	thing = S_BBOMBEXPL2;
	states[thing] = states[S_BBOMBEXPL1];
	states[thing].frame = 32769;
	states[thing].tics = 6;
	states[thing].nextstate = S_BBOMBEXPL3;

	thing = S_BBOMBEXPL3;
	states[thing] = states[S_BBOMBEXPL1];
	states[thing].frame = 32770;
	states[thing].nextstate = S_BBOMBEXPL4;

	thing = S_BBOMBEXPL4;
	states[thing] = states[S_BBOMBEXPL1];
	states[thing].frame = 32771;
	states[thing].nextstate = S_BBOMBEXPL5;

	thing = S_BBOMBEXPL5;
	states[thing] = states[S_BBOMBEXPL1];
	states[thing].frame = 32772;
	states[thing].nextstate = S_DISS;

	// Bomberman Enemies!
	// Barom!
	thing = S_BAROM_STND;	
	states[thing].sprite = SPR_BLUN;
	states[thing].frame = 0;
	states[thing].tics = -1;
	states[thing].nextstate = S_BAROM_MOVE1;

	thing = S_BAROM_MOVE1;
	states[thing] = states[S_BAROM_STND];
	states[thing].frame = 1;
	states[thing].tics = 3;
	states[thing].nextstate = S_BAROM_MOVE2;

	thing = S_BAROM_MOVE1;
	states[thing] = states[S_BAROM_MOVE1];
	states[thing].frame = 2;
	states[thing].nextstate = S_BAROM_MOVE2;

	thing = S_BAROM_MOVE2;
	states[thing] = states[S_BAROM_MOVE1];
	states[thing].frame = 3;
	states[thing].nextstate = S_BAROM_MOVE3;

	thing = S_BAROM_MOVE3;
	states[thing] = states[S_BAROM_MOVE1];
	states[thing].frame = 4;
	states[thing].nextstate = S_BAROM_MOVE4;

	thing = S_BAROM_MOVE4;
	states[thing] = states[S_BAROM_MOVE1];
	states[thing].frame = 5;
	states[thing].nextstate = S_BAROM_MOVE5;

	thing = S_BAROM_MOVE5;
	states[thing] = states[S_BAROM_MOVE1];
	states[thing].frame = 6;
	states[thing].nextstate = S_BAROM_MOVE1;

	thing = S_BAROM_DIE;
	states[thing] = states[S_BAROM_MOVE1];
	states[thing].tics = -1;
	states[thing].frame = 7;
	states[thing].nextstate = S_BAROM_MOVE1;

	//***** Sprite files? *****//
	thing = SPR_BOMB;
	strcpy(sprnames[thing],"BOMB");

	thing = SPR_POWB;
	strcpy(sprnames[thing],"POWB");

	thing = SPR_SPKB;
	strcpy(sprnames[thing],"SPKB");

	thing = SPR_RUBB;
	strcpy(sprnames[thing],"RUBB");

	thing = SPR_MNEB;
	strcpy(sprnames[thing],"MNEB");

	thing = SPR_REMB;
	strcpy(sprnames[thing],"REMB");

	thing = SPR_BOMU;
	strcpy(sprnames[thing],"BOMU");
#ifdef HWRENDER
	t_lspr[thing] = &lspr[NOLIGHT];
#endif

	thing = SPR_FIRU;
	strcpy(sprnames[thing],"FIRU");
#ifdef HWRENDER
	t_lspr[thing] = &lspr[NOLIGHT];
#endif

	thing = SPR_UNKN;
	strcpy(sprnames[thing],"UNKN");
#ifdef HWRENDER
	t_lspr[thing] = &lspr[NOLIGHT];
#endif

	thing = SPR_AURA;
	strcpy(sprnames[thing],"AURA");
#ifdef HWRENDER
	t_lspr[thing] = &lspr[NOLIGHT];
#endif

	thing = SPR_POWP;
	strcpy(sprnames[thing],"POWP");
#ifdef HWRENDER
	t_lspr[thing] = &lspr[NOLIGHT];
#endif

	thing = SPR_RUBP;
	strcpy(sprnames[thing],"RUBP");
#ifdef HWRENDER
	t_lspr[thing] = &lspr[NOLIGHT];
#endif

	thing = SPR_MNEP;
	strcpy(sprnames[thing],"MNEP");
#ifdef HWRENDER
	t_lspr[thing] = &lspr[NOLIGHT];
#endif

	thing = SPR_REMP;
	strcpy(sprnames[thing],"REMP");
#ifdef HWRENDER
	t_lspr[thing] = &lspr[NOLIGHT];
#endif

	thing = SPR_SPKP;
	strcpy(sprnames[thing],"SPKP");
#ifdef HWRENDER
	t_lspr[thing] = &lspr[NOLIGHT];
#endif

	thing = SPR_REXP;
	strcpy(sprnames[thing],"REXP");
#ifdef HWRENDER
	t_lspr[thing] = &lspr[NOLIGHT];
#endif

	thing = SPR_BEXP;
	strcpy(sprnames[thing],"BEXP");
#ifdef HWRENDER
	t_lspr[thing] = &lspr[NOLIGHT];
#endif

	thing = SPR_BLUN;
	strcpy(sprnames[thing],"BLUN");

	//***** Sound files *****//
	thing = sfx_bplant;
	S_sfx[thing] = S_sfx[sfx_None];
	S_sfx[thing].name = "bplant";
	S_sfx[thing].singularity = true;
	S_sfx[thing].priority = 130;

	thing = sfx_fuse;
	S_sfx[thing] = S_sfx[sfx_None];
	S_sfx[thing].name = "fuse";
	S_sfx[thing].singularity = false;
	S_sfx[thing].priority = 129;
	// S_sfx[thing].pitch = SF_NOINTERRUPT|SF_X4AWAYSOUND; // Refrence

	thing = sfx_bexplode;
	S_sfx[thing] = S_sfx[sfx_None];
	S_sfx[thing].name = "bexplode";
	S_sfx[thing].singularity = true;
	S_sfx[thing].priority = 130;
	S_sfx[thing].pitch = 24;

	thing = sfx_bgetitem;
	S_sfx[thing] = S_sfx[sfx_fuse];
	S_sfx[thing].name = "bgetitem";
}
#endif // BOMB

#ifdef ZELDA // Resources for Shuffle's TML
void ZELDA_Init(void)
{
	size_t thing;

	//****** Mobj types ******//
	thing = MT_SWORD; // Sword 'hitbox'. Spawned similarly to the MT_FIST in the SMASH define.
	mobjinfo[thing] = mobjinfo[MT_DISS];
	mobjinfo[thing].radius = 25*FRACUNIT;
	mobjinfo[thing].height = 55*FRACUNIT;
	mobjinfo[thing].speed = 8;
	mobjinfo[thing].flags = MF_FLOAT|MF_NOGRAVITY|MF_MISSILE;

	thing = MT_BOMB;
	mobjinfo[thing] = mobjinfo[MT_DISS];
	mobjinfo[thing].spawnstate = S_BOMB1;
	mobjinfo[thing].seestate = S_BOMB1;
	mobjinfo[thing].radius = 10*FRACUNIT;
	mobjinfo[thing].height = 26*FRACUNIT;
	mobjinfo[thing].mass = 16;
	mobjinfo[thing].speed = 8;
	mobjinfo[thing].flags = MF_SHOOTABLE|MF_NOCLIP;

	thing = MT_REDEXPLODE;
	mobjinfo[thing] = mobjinfo[MT_DISS];
	mobjinfo[thing].doomednum = -1;
	mobjinfo[thing].doomednum = -1;
	mobjinfo[thing].spawnstate = S_RBOMBEXPL1;
	mobjinfo[thing].seestate = S_RBOMBEXPL1;
	mobjinfo[thing].spawnhealth = 1000;
	mobjinfo[thing].speed = 0;
	mobjinfo[thing].radius = 10*FRACUNIT;
	mobjinfo[thing].height = 22*FRACUNIT;
	mobjinfo[thing].mass = 16;
	mobjinfo[thing].flags = MF_SPECIAL|MF_FIRE|MF_FLOAT|MF_NOGRAVITY;

	thing = MT_BOSSTHROWNBOMB;
	mobjinfo[thing] = mobjinfo[MT_THROWNEXPLOSION];
	mobjinfo[thing].speed = 30*FRACUNIT;

	//****** States ******//

	thing = S_BOMB1; // 'Bouncing' Bomb Animation!
	states[thing].sprite = SPR_BOMB;
	states[thing].frame = 0;
	states[thing].tics = 3;
	states[thing].nextstate = S_BOMB2;

	thing = S_BOMB2;
	states[thing] = states[S_BOMB1];
	states[thing].frame = 1;
	states[thing].nextstate = S_BOMB3;

	thing = S_BOMB3;
	states[thing] = states[S_BOMB1];
	states[thing].frame = 2;
	states[thing].nextstate = S_BOMB4;

	thing = S_BOMB4;
	states[thing] = states[S_BOMB1];
	states[thing].frame = 3;
	states[thing].nextstate = S_BOMB5;

	thing = S_BOMB5;
	states[thing] = states[S_BOMB1];
	states[thing].frame = 4;
	states[thing].nextstate = S_BOMB6;

	thing = S_BOMB6;
	states[thing] = states[S_BOMB1];
	states[thing].frame = 5;
	states[thing].nextstate = S_BOMB1;


	thing = S_BOMBRED1; // 'Bouncing' Red Bomb Animation (AKA. About to explode)
	states[thing] = states[S_BOMB1]; // Copy the inital bomb frame. I mean, it IS still the same bomb. :/
	states[thing].frame = 6;
	states[thing].nextstate = S_BOMBRED2;

	thing = S_BOMBRED2;
	states[thing] = states[S_BOMB1];
	states[thing].frame = 7;
	states[thing].nextstate = S_BOMBRED3;

	thing = S_BOMBRED3;
	states[thing] = states[S_BOMB1];
	states[thing].frame = 8;
	states[thing].nextstate = S_BOMBRED4;

	thing = S_BOMBRED4;
	states[thing] = states[S_BOMB1];
	states[thing].frame = 9;
	states[thing].nextstate = S_BOMBRED5;

	thing = S_BOMBRED5;
	states[thing] = states[S_BOMB1];
	states[thing].frame = 10;
	states[thing].nextstate = S_BOMBRED6;

	thing = S_BOMBRED6;
	states[thing] = states[S_BOMB1];
	states[thing].frame = 11;
	states[thing].nextstate = S_BOMBRED1;

	// RED Exploding GFX
	thing = S_RBOMBEXPL1;
	states[thing].sprite = SPR_REXP;
	states[thing].frame = 32768; // Sprite frame 0 with a full lighting effect 32768
	states[thing].tics = 3;
	states[thing].nextstate = S_RBOMBEXPL2;

	thing = S_RBOMBEXPL2;
	states[thing] = states[S_RBOMBEXPL1];
	states[thing].frame = 32769;
	states[thing].tics = 6;
	states[thing].nextstate = S_RBOMBEXPL3;

	thing = S_RBOMBEXPL3;
	states[thing] = states[S_RBOMBEXPL1];
	states[thing].frame = 32770;
	states[thing].nextstate = S_RBOMBEXPL4;

	thing = S_RBOMBEXPL4;
	states[thing] = states[S_RBOMBEXPL1];
	states[thing].frame = 32771;
	states[thing].nextstate = S_RBOMBEXPL5;

	thing = S_RBOMBEXPL5;
	states[thing] = states[S_RBOMBEXPL1];
	states[thing].frame = 32772;
	states[thing].nextstate = S_DISS;

	//***** Sprite files? *****//
	thing = SPR_BOMB;
	strcpy(sprnames[thing],"BOMB");

	thing = SPR_REXP;
	strcpy(sprnames[thing],"REXP");
#ifdef HWRENDER
	t_lspr[thing] = &lspr[NOLIGHT];
#endif

	//***** Sound files *****//


}
#endif

#ifdef FLAME
void FLAME_Init(void)
{
#ifdef FLAMEBOSS
	size_t thing;

	//****** Mobj types ******//
	// Runaway Eggmobile
	thing = MT_FLAME_BOSS1;
	mobjinfo[thing] = mobjinfo[MT_EGGMOBILE];
	mobjinfo[thing].doomednum = 7070;
#ifdef BOMB
	mobjinfo[thing].spawnhealth = 8;
	mobjinfo[thing].meleestate = S_EGGMOBILE_ATK3;
	mobjinfo[thing].missilestate = S_EGGMOBILE_ATK1;
	mobjinfo[thing].raisestate = S_EGGMOBILE_PANIC1;
#else
	mobjinfo[thing].speed = 20*FRACUNIT; // SKULLSPEED
#endif
#endif // FLAMEBOSS

	//****** States ******//


	//***** Sprite files? *****//


	//***** Sound files *****//


}
#endif // FLAME

#if defined(PARTICLES)
// Shuffle's Particles
void AM_InitPretty(void)
{
	size_t thing;

	//****** Mobj types ******//
	thing = MT_SHFPARTICLE;
	mobjinfo[thing] = mobjinfo[MT_DISS];
	mobjinfo[thing].spawnstate = S_SHFPARTICLE;
	mobjinfo[thing].flags = MF_SCENERY|MF_NOCLIPTHING;

	thing = MT_SHFLIGHTPARTICLE;
	mobjinfo[thing] =  mobjinfo[MT_SHFPARTICLE];
	mobjinfo[thing].spawnstate = S_SHFLIGHTPARTICLE;

	thing = MT_SHFWATERFOUNTAIN;
	mobjinfo[thing] = mobjinfo[MT_GFZFLOWER1];
	mobjinfo[thing].doomednum = 2876;
	mobjinfo[thing].spawnstate = S_SHFFOUNTAIN;
	mobjinfo[thing].flags |= MF_NOGRAVITY;

	thing = MT_SHFSPIRALFOUNTAIN;
	mobjinfo[thing] = mobjinfo[MT_SHFWATERFOUNTAIN];
	mobjinfo[thing].doomednum = 2877;
	mobjinfo[thing].spawnstate = S_SHFFOUNTAIN;

	//****** States ******//
	thing = S_SHFPARTICLE;
	states[thing] = states[S_DISS];
	states[thing].sprite = SPR_SHFP;
	states[thing].tics = -1;
	states[thing].frame = 0;

	thing = S_SHFPARTICLEDISS;
	states[thing] = states[S_SHFPARTICLE];
	states[thing].sprite = SPR_SHFP;
	states[thing].tics = -1;
	states[thing].frame = 1;

	thing = S_SHFLIGHTPARTICLE;
	states[thing] = states[S_SHFPARTICLE];
	states[thing].sprite = SPR_LYTE;
	states[thing].tics = -1;
	states[thing].frame = 0;

	thing = S_SHFLIGHTPARTICLEDISS;
	states[thing] = states[S_SHFLIGHTPARTICLE];
	states[thing].tics = -1;
	states[thing].frame = 1;

	thing = S_SHFFOUNTAIN;
	states[thing] = states[S_PARTICLE];
	states[thing].sprite = SPR_DISS;
	states[thing].tics = -1;
	states[thing].frame = 0;

	//***** Sprite files? *****//
	thing = SPR_SHFP;
	strcpy(sprnames[thing],"SHFP");
#ifdef HWRENDER
	t_lspr[thing] = &lspr[NOLIGHT];
#endif

	thing = SPR_LYTE;
	strcpy(sprnames[thing],"SHFP");
#ifdef HWRENDER
	lspr[PARTICLE_L] = lspr[GREYSHINE_L];
	lspr[PARTICLE_L].dynamic_radius = 0.0f;
	lspr[PARTICLE_L].corona_radius = 16.0f;
	t_lspr[thing] = &lspr[PARTICLE_L];
#endif
}
#endif // PARTICLES
