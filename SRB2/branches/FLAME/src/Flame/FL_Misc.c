// SRB2FLAME - Shared code
// Pieces of code which is shared among defines.
// By Flame and Shuffle

#include "../doomdef.h"
#include "../g_game.h"
#include "../i_video.h" //rendermode
#include "../m_menu.h"
#include "../m_random.h"
#include "../p_local.h"
#include "../p_mobj.h"
#include "../r_main.h"
#include "../s_sound.h"
#include "FL_Misc.h"

#ifdef PRVMSG
#include "../command.h"
#include "../hu_stuff.h" // HU_SetCEcho* and HU_DoCEcho
#include "../i_system.h" // I_OutputMsg

static void Secret_OnChange(void);

consvar_t cv_secret = {"secret", "Off", CV_CALL|CV_NOINIT, CV_OnOff, Secret_OnChange, 0, NULL, NULL, 0, 0, NULL};

void FL_MiscInit(void)
{
	CV_RegisterVar(&cv_secret);
}

static void Secret_OnChange(void)
{
	if (gamestate == GS_LEVEL)
	{
		if (cv_secret.value)
		{
			HU_SetCEchoFlags(0);
			HU_SetCEchoDuration(5);
			HU_DoCEcho("\\You are now chatting with\\people who have this executable.\\\\\\\\\\\\");
			I_OutputMsg("You are now chatting with people who have this executable.\n");
		}
		else
		{
			HU_SetCEchoFlags(0);
			HU_SetCEchoDuration(5);
			HU_DoCEcho("\\You are no longer chatting with\\people who have this executable.\\\\\\\\\\\\");
			I_OutputMsg("You are now chatting with people who have this executable.\n");
		}
	}
}
#endif

#if defined(PARTICLES)
//
// SHF_EmitParticle
// Emits particles from the source.
// By Shuffle, modified a bit by Flame.
//
// Frequency defines how often a call is ignored. Alive is how long the particle is alive.
// Color and speed are self-explainatory.
// For size, 0 is small, 1 is large(ish). (Size is only for MT_SHFPARTICLE and MT_SHFLIGHTPARTICLE)
//
void SHF_EmitParticle(mobj_t* source, mobjtype_t type, UINT8 frequency, fixed_t speed, fixed_t zspeed, INT32 alive, UINT8 pgravity, UINT8 addmomentum, UINT8 color, UINT8 repeat, UINT8 size)
{
	UINT8 ignore;
	UINT16 prand, i;
	for(i=0; i<repeat; i++)
	{
		mobj_t* particle;

		prand = P_Random()*2;
		ignore = P_Random();

		if (ignore > frequency)
			return;

		particle = P_SpawnMobj(source->x, source->y, source->z, type);
		if (particle->eflags & MFE_VERTICALFLIP)
			particle->z--; // Particle can't be directly on the ceiling or it disappears!
		else
			particle->z++; // Particle can't be directly on the floor or it disappears!

		// Thrust the object in a random direction
		P_InstaThrust(particle, FixedAngle(prand*FRACUNIT), speed);
		P_SetObjectMomZ(particle, zspeed, false);

		// Used for following the object that spawned it.
		if (addmomentum)
		{
			particle->momx += source->momx;
			particle->momy += source->momy;
			P_SetObjectMomZ(particle, source->momz, true);
		}

		// Make sure you only try to change the state of particle sprites..
		if (size && (type == MT_SHFPARTICLE || type == MT_SHFLIGHTPARTICLE))
			P_SetMobjState(particle, S_SHFPARTICLEDISS);

		// Change color, tics, flags, and gravity
		particle->tics = alive;
		particle->flags |= MF_TRANSLATION;
		particle->color = color;
		particle->flags |= MF_NOCLIPHEIGHT;
		particle->flameflags |= FLF_PARTICLE;

		if (!pgravity)
			particle->flags |= MF_NOGRAVITY|MF_FLOAT;
 		else if (pgravity == 2)
			particle->flameflags |= FLF_LOWGRAV;
	}
}

//
// SHF_EmitSpinnyParticle
// Makes a spiral out of particles.
// By Shuffle, modified a bit by Flame.
//
void SHF_EmitSpinnyParticle(mobj_t* source, fixed_t speed, fixed_t zspeed, INT32 alive, UINT8 pgravity, UINT8 color, UINT8 size)
{
	mobj_t* lightparticle;

	// Every other particle should have a light.
	lightparticle = P_SpawnMobj(source->x, source->y, source->z, MT_SHFLIGHTPARTICLE);

	// Particle can't be directly on the floor/ceiling or it disappears!
	if (lightparticle->eflags & MFE_VERTICALFLIP)
		lightparticle->z--;
	else
		lightparticle->z++;

	// Every other particle thrusts in back
	P_InstaThrust(lightparticle, FixedAngle((source->angle*FRACUNIT)+ANGLE_90), speed);
	P_SetObjectMomZ(lightparticle, zspeed, false);

	// Change color, tics, and gravity
	lightparticle->tics = alive;
	lightparticle->flags |= MF_TRANSLATION;
	lightparticle->color = color;
	lightparticle->flags |= MF_NOCLIPHEIGHT;
 	lightparticle->flameflags |= FLF_PARTICLE;

	// Make sure you only try to change the state of particle sprites..
	if (size)
		P_SetMobjState(lightparticle, S_SHFLIGHTPARTICLEDISS);

	if (!pgravity)
		lightparticle->flags |= MF_NOGRAVITY;
 	else if (pgravity == 2)
		lightparticle->flameflags |= FLF_LOWGRAV;

	if (source->angle == 360) // Quite rudimentary, but just in case..
		source->angle = 0;

	source->angle += 20;
}

//
// SHF_ParticleMobjThinkers
//
// Lets see how some of Shuffle's mobjs would think with particles!
// By Flame
//
void SHF_ParticleMobjThinkers(mobj_t *mobj)
{
	switch (mobj->type)
	{
			// Scenery Objects
			case MT_SHFPARTICLE:
			case MT_SHFLIGHTPARTICLE:
				if ((mobj->z <= mobj->floorz)
					|| (mobj->z >= mobj->ceilingz))
					mobj->tics = 1; // Hit the ground
				break;
			case MT_SHFWATERFOUNTAIN:
				if (rendermode != render_soft)
				{
					SHF_EmitParticle(mobj, MT_SHFPARTICLE, 100, 2*FRACUNIT, 9*FRACUNIT, -1, 2, false, 7, 1, 1);
					SHF_EmitParticle(mobj, MT_SHFLIGHTPARTICLE, 75, 2*FRACUNIT, 10*FRACUNIT, -1, 2, false, 7, 1, 1);
					SHF_EmitParticle(mobj, MT_SHFPARTICLE, 100, 2*FRACUNIT, 11*FRACUNIT, -1, 2, false, 7, 1, 1);
				}
				else // Software mode can't really handle too many sprites.
					SHF_EmitParticle(mobj, MT_SHFPARTICLE, 255, 2*FRACUNIT, 9*FRACUNIT, -1, 2, false, 7, 1, 1);
				break;
			case MT_SHFSPIRALFOUNTAIN:
				if (leveltime%(TICRATE/5)==0)
					SHF_EmitSpinnyParticle(mobj, 2*FRACUNIT, 10*FRACUNIT, -1, 2, 7, 1);
				break;

			// Other Objects
			case MT_FLAME:
				SHF_EmitParticle(mobj, MT_SMOK, 30, FRACUNIT, FRACUNIT/2, TICRATE, false, false, 10, 1, 0);
				break;
			case MT_STEAM:
				if (mobj->state == &states[S_STEAM1])
				{
					SHF_EmitParticle(mobj, MT_SHFPARTICLE, 255, FRACUNIT, 5*FRACUNIT, TICRATE+5, false, false, 4, 3, 0);
					SHF_EmitParticle(mobj, MT_SHFPARTICLE, 255, FRACUNIT, 7*FRACUNIT, TICRATE+5, false, false, 4, 3, 0);
					SHF_EmitParticle(mobj, MT_SHFPARTICLE, 255, FRACUNIT, 10*FRACUNIT, TICRATE+5, false, false, 4, 3, 0);
					SHF_EmitParticle(mobj, MT_SHFPARTICLE, 255, FRACUNIT, 12*FRACUNIT, TICRATE+5, false, false, 4, 3, 0);
				}
				break;
			default:
				break;
	}
}
#endif
