// SRB2FLAME - Shared code
// Pieces of code which is shared among defines.
// By Flame

#if defined(SMASH) || defined(BOMB) || defined(ZELDA)
#ifndef __FL_COMMON__
#define __FL_COMMON__

mobj_t *P_GetMobjForType(mobjtype_t type);

/*#if defined(SMASH)
void P_FistAttack(player_t *player, tic_t waittime);
#elif defined(ZELDA)
void P_SwordAttack(player_t *player, tic_t waittime);
#endif*/

#if defined(BOMB) || defined(ZELDA)
void P_BombStuff(player_t* player, ticcmd_t *cmd);
#endif // BOMB / ZELDA

void P_PlayerAction(player_t *player, ticcmd_t *cmd);

#endif // __FL_COMMON__
#endif // SMASH / BOMB / ZELDA
