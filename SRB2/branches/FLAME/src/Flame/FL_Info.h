// SRB2FLAME - Resource file for Mobj objects, states, and sounds.
// Ported from the SRB2109FLAME Branch
// By Flame, based off the JTESHUFFLE source

#ifndef __FL_INFO__
#define __FL_INFO__

#ifdef SMASH
void SMASH_Init(void);
#endif
#ifdef BOMB
void BOMB_Init(void);
#endif
#ifdef ZELDA
void ZELDA_Init(void);
#endif
#ifdef FLAME
void FLAME_Init(void);
#endif
#ifdef PARTICLES
void AM_InitPretty(void);
#endif

#endif //__FL_INFO__
