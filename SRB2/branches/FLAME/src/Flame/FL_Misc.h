// SRB2FLAME - Shared code
// Pieces of code which is shared among defines.
// By Flame and Shuffle

#ifndef __FL_MISC__
#define __FL_MISC__

#ifdef PRVMSG
#include "../command.h"
extern consvar_t cv_secret;
void FL_MiscInit(void);
#endif

#if defined(PARTICLES)
void SHF_EmitParticle(mobj_t* source, mobjtype_t type, UINT8 frequency, fixed_t speed, fixed_t zspeed, INT32 alive, UINT8 pgravity, UINT8 addmomentum, UINT8 color, UINT8 repeat, UINT8 size);
void SHF_EmitSpinnyParticle(mobj_t* source, fixed_t speed, fixed_t zspeed, INT32 alive, UINT8 pgravity, UINT8 color, UINT8 size);
void SHF_ParticleMobjThinkers(mobj_t *mobj);
#endif

#endif // __FL_MISC__
