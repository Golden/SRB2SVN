// SRB2FLAME - Forbidden code
// Contains experimental code that should not be used under ANY circumstances. "Cheating"
// By Flame


#ifndef __FL_FORBID__
#define __FL_FORBID__
#if defined(FLAME) && !(defined(SMASH) || defined(BOMB) || defined(ZELDA))
#include "../../command.h"

void FLAME_ForbidInit(void);

#ifdef AUTOTARGET
void AutoTarget_Init(ticcmd_t *cmd);
extern consvar_t cv_autotarget;
#endif

#endif // SMASH / BOMB / ZELDA
#endif // __FL_FORBID__
