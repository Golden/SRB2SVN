#!/usr/bin/python
# Boots SRB2 through abusing(?) the ctypes library to go straight into D_SRB2Main()!
import srb2console
import stackless
from ctypes import cdll
srb2 = cdll.LoadLibrary("./libsrb2.so")
srb2.D_SRB2Main()
srb2.D_SRB2Prep()

console = srb2console.SRB2ConsoleWindow(env=globals())

def SRB2CTasklet():
  while True:
    srb2.D_SRB2Loop()
    stackless.schedule()

stackless.tasklet(SRB2CTasklet)()
stackless.tasklet(console.tick)()
stackless.run()
