// Emacs style mode select   -*- C++ -*- 
//-----------------------------------------------------------------------------
//
// Copyright (C) 1993-1996 by id Software, Inc.
// Copyright (C) 1998-2000 by DooM Legacy Team.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//-----------------------------------------------------------------------------
/// \file
/// \brief Globally defined strings.

#include "dstrings.h"

const char *text[NUMTEXT] =
{
	"Development mode ON.\n",
	"CD-ROM Version: default.cfg from c:\\srb2data\n",
	"press a key.",
	"press y or n.",
	"only the server can do a load net game!\n\npress a key.",
	"you can't quickload during a netgame!\n\npress a key.",
	"you haven't picked a quicksave slot yet!\n\npress a key.",
	"you can't save if you aren't playing!\n\npress a key.",
	"quicksave over your game named\n\n'%s'?\n\npress y or n.",
	"do you want to quickload the game named\n\n'%s'?\n\npress y or n.",
	"You are in a network game.\n""End it?\n(Y/N).\n",
	"are you sure? this skill level\nisn't even remotely fair.\n\npress y or n.",
	"Messages OFF",
	"Messages ON",
	"you can't end a netgame!\n\npress a key.",
	"are you sure you want to end the game?\n\npress y or n.",

	"%s\n\n(press 'y' to quit)",

	"Gamma correction OFF",
	"Gamma correction level 1",
	"Gamma correction level 2",
	"Gamma correction level 3",
	"Gamma correction level 4",
	"empty slot",

	"game saved.",
	"[Message unsent]",

	"Music Change",
	"IMPOSSIBLE SELECTION, you stupid-head!",

	"Changing Level...",

	" #",

	"Thousands of years ago,\n"\
	"there was a hero named Link.\n"\
	"He was known as The Hero of Time\n#",

	"Using the legendary Master Sword,\n"\
	"the Triforce of Courage,\nand the Sage's powers,\n"\
	"he defeated a horrible monster named\n"\
	"Ganondorf. But enough about that.\n"\
	"Today.. a new Hero is born..\n#",

	"\nLink is sleeping in his little tree\n"\
	"hut# when a terrible dream awakens him.\n"
	"'Ugh, why am I having these dreams..?'\n#",

	"He looks down and notices something \n"\
	"different.. His necklace is gone! All\n"\
	"that is left is the little yellow triangle \n"\
	"that hangs from it.\n#",

	"'What happened to my necklace?!'\n"\
	"Just then, he noticed a note on the floor\n#"

	"It read 'Link, if you want the rest of your\n"\
	"magic necklace back, bring me the'\n"\
	"yellow triangle that hangs from it\n#",

	"'..why didn't he take it when he took the\n"\
	"rest of the necklace?'. He thought for a minute..\n"\
	"'It's not important, I need to go ask the\n"\
	"forest Sage for advice!'\n#",

	"\"Curses!\" Eggman yelled. \"That hedgehog\n"\
	"and his ridiculous friends will pay\n"\
	"dearly for this!\" Just then his scanner\n"\
	"blipped as the Black Rock made its\n"\
	"appearance from nowhere. Eggman looked at\n"\
	"the screen, and just shrugged it off.\n#",

	"It was only later\n"\
	"that he had an\n"\
	"idea. \"The Black\n"\
	"Rock usually has a\n"\
	"lot of energy\n"\
	"within it... if I\n"\
	"can somehow\n"\
	"harness this, I\n"\
	"can turn it into\n"\
	"the ultimate\n"\
	"battle station,\n"\
	"and every last\n"\
	"person will be\n"\
	"begging for mercy,\n"\
	"including Sonic!\"\n#",

	"\n\nBefore beginning his scheme,\n"\
	"Eggman decided to give Sonic\n"\
	"a reunion party...\n#",

	"\"We're ready to fire in 15 seconds!\"\n"\
	"The robot said, his voice crackling a\n"
	"little down the com-link. \"Good!\"\n"\
	"Eggman sat back in his Egg-Mobile and\n"\
	"began to count down as he saw the\n"\
	"GreenFlower city on the main monitor.\n#",

	"\"10...9...8...\"\n"\
	"Meanwhile, Sonic was tearing across the\n"\
	"zones, and everything became nothing but\n"\
	"a blur as he ran around loops, skimmed\n"\
	"over water, and catapulted himself off\n"\
	"rocks with his phenomenal speed.\n#",

	"\"5...4...3...\"\n"\
	"Sonic knew he was getting closer to the\n"\
	"City, and pushed himself harder. Finally,\n"\
	"the city appeared in the horizon.\n"\
	"\"2...1...Zero.\"\n#",

	"GreenFlower City was gone.\n"\
	"Sonic arrived just in time to see what\n"\
	"little of the 'ruins' were left. Everyone\n"\
	"and everything in the city had been\n"\
	"obliterated.\n#",

	"\"You're not quite as dead as we thought,\n"\
	"huh? Are you going to tell us your plan as\n"\
	"usual or will I 'have to work it out' or\n"\
	"something?\"                         \n"\
	"\"We'll see... let's give you a quick warm\n"\
	"up, Sonic! JETTYSYNS! Open fire!\"\n#",

	"Eggman took this\n"\
	"as his cue and\n"\
	"blasted off,\n"\
	"leaving Sonic\n"\
	"and Tails behind.\n"\
	"Tails looked at\n"\
	"the ruins of the\n"\
	"Greenflower City\n"\
	"with a grim face\n"\
	"and sighed.           \n"\
	"\"Now what do we\n"\
	"do?\", he asked.\n#",

#if 0
	"\"Easy! We go\n"\
	"find Eggman\n"\
	"and stop his\n"\
	"latest\n"\
	"insane plan.\n"\
	"Just like\n"\
	"we've always\n"\
	"done, right?                 \n\n"\
	"...                    \n\n"\
	"\"Tails,what\n"\
	"*ARE* you\n"\
	"doing?\"\n#",

	"\"I'm just finding what mission obje...\n"\
	"a-ha! Here it is! This will only give\n"\
	"the robot's primary objective. It says,\n"\
	"* LOCATE AND RETRIEVE CHAOS EMERALD.\n"\
	"ESTIMATED LOCATION: GREENFLOWER ZONE *\"\n"\
	"\"All right, then let's go!\"\n#",

#endif
/*
"What are we waiting for? The first emerald is ours!" Sonic was about to
run, when he saw a shadow pass over him, he recognized the silhouette
instantly.
	"Knuckles!" Sonic said. The echidna stopped his glide and landed
facing Sonic. "What are you doing here?"
	He replied, "This crisis affects the Floating Island,
if that explosion I saw is anything to go by."
	"If you're willing to help then... let's go!"
	*/

	"Ganon's tied explosives\nto princess Zelda, and\nwill activate them if\nyou press the 'Y' key!\nPress 'N' to save her!",
	"What would The Great Deku\n Tree say if he saw you\n quitting the game?",
	"Hey!\nWhere do ya think you're goin'?",
	"Forget your studies!\nPlay some more!",
	"You're trying to say you\nlike Zelda II better than\nthis, right?",
	"don't leave yet -- there's a\ntriforce piece around that corner!",
	"Don't stop playing, or I'll\nsick Navi on you.",
	"go ahead and leave. see if i care...\n*sniffle*",

	"If you leave now,\nGanon will take over Hyrule!",
	"Don't quit!\nThere are Sages\nto save!",
	"Aw c'mon, just stab\na few more Octoroks!",
	"Just because you can't\nget that Triforce Piece...",
	"If you leave, I'll use\nmy spin attack on you!",
	"Don't go!\nYou might find the hidden\nlevels!",
	"Hit the 'N' key, Link!\nThe 'N' key!",

	"===========================================================================\n"
	"                       Sonic Robo Blast II!\n"
	"                       by Sonic Team Junior\n"
	"                      http://www.srb2.org\n"
	"      This is a modified version. Go to our site for the original.\n"
	"===========================================================================\n",

	"===========================================================================\n"
	"                   We hope you enjoy this game as\n"
	"                     much as we did making it!\n"
	"                            ...wait. =P\n"
	"===========================================================================\n",

	"M_LoadDefaults: Load system defaults.\n",
	"Z_Init: Init zone memory allocation daemon. \n",
	"W_Init: Init WADfiles.\n",
	"M_Init: Init miscellaneous info.\n",
	"R_Init: Init SRB2 refresh daemon - ",
	"\nP_Init: Init Playloop state.\n",
	"I_Init: Setting up machine state.\n",
	"D_CheckNetGame: Checking network game status.\n",
	"S_Init: Setting up sound.\n",
	"HU_Init: Setting up heads up display.\n",
	"ST_Init: Init status bar.\n",

	"srb2.srb",

	"c:\\srb2data\\"SAVEGAMENAME"%u.ssg",
	SAVEGAMENAME"%u.ssg",

	//BP: here is special dehacked handling, include centering and version
	"Sonic Robo Blast 2: v1.09.3.16",
};

char savegamename[256];
