// JTE Misc
// By Jason the Echidna
// MINE! NO STEALY!

#ifndef JTEMISC_H
#define JTEMISC_H

#if defined(CCPAL) || defined(HOLIDAYS)
void JTE_ReinitLightTables(void);
#endif

#ifdef A_TORNADOPARTICLE
#include "../p_mobj.h"
void A_TornadoParticle(mobj_t* mobj);
#endif

#ifdef JTEMISC
#include "../d_player.h"
void JTE_DeSuper(player_t* player);
void JTE_HwLights(void);
#endif

#ifdef MOBJSCALE
void JTE_InitScale(void);
#ifdef SCALEVAR
#include "../command.h"
extern consvar_t cv_myscale;
void JTE_SendMyScale(void);
#endif
#ifdef SCALEMARIO
#include "../d_player.h"
void MAR_MarioScale(player_t* player, boolean reverse);
boolean MAR_MarioDamageMobj(player_t* player);
void MAR_InitHUD(void);
void MAR_DrawHUD(void);
#endif
#endif

#ifdef SPRITEOFFS
typedef struct {
	float x[4],y[4],z[4];
} spriteoffs_t;
extern spriteoffs_t *spriteoffs[NUMSPRITES];
void JTE_InitSpriteOffs(void);
#endif

#ifdef HOLIDAYS
#include "../dehacked.h"

enum
{
	H_NONE = 0, // Nothing special. :(

	H_VALN, // Valentine's day
	H_PTRK, // Saint Patrik's day
	H_FOOL, // April Fool's day
	H_ESTR, // Easter
	H_WEEN, // Halloween
	H_THXG, // Thanksgiving
	H_XMAS, // Christmas
	H_NEWY, // New Year

	NUMHOLIDAYS
};
extern const char *holidaynicks[NUMHOLIDAYS];
typedef struct holidayreplace_s
{
	char original[9];
	char replace[9];
	byte type;
	struct holidayreplace_s *next;
} holidayreplace_t;
extern holidayreplace_t *holidayreplace;
void JTE_InitHolidaySprites(void);
void JTE_LoadHolidayCfg(void);
byte JTE_ReadHoliday(const char *name);
void JTE_ReadReplace(MYFILE *f, byte type);
void JTE_TitlePrecip(void);
#endif

#ifdef DIGGING
void JTE_InitDigging(void);
#endif

#ifdef JTEWAD
void JTE_Encrypt(void *src, unsigned int size);
void JTE_Decrypt(void *src, unsigned int size);
#endif

#endif
