#ifdef SRB2JTE
	// Official edits/additions
	//#define CHAOSISNOTDEADYET

	// Edits (OF EXISTING THINGS)
	#define JTEMISC // Misc. fun stuff. Bunches of small bug fixes and other odditys I have yet to create new groups for.
	#define SUPEREXIT // De-Super when exiting

	// Additions
	#define JTEBOTS // Bots! w00t!
	//#define SUCKYBOTWAYPOINTS // THESE WAYPOINT TEH SUCKS.
	#define JTEFLYCHAINS // Sonic Heroes-style flying chains
	#define DARKWORLD // Dark World map type
	#define SPMOVES // SPMoves character abilities
	#define CCPAL // The Chaos Control palette change for the SPMoves ability
	//#define UNIVERSE // SRB2 Universe. Incomplete.
	#define MULTIHPENEMIES // Multiple hit enemies.
	#define CENTEREDMENU // M_DrawCenteredMenu for the title screen menu and
	                     // any other new menus which would look nicer with centering.
	                     // Doesn't center options or sliders, though, so don't use it on options menus.
	//#define SUPERVIRUS0 // Teh original "fair" SRB2 SuperVirus!1 Overrides SUPERVIRUS1
	//#define SUPERVIRUS1 // Teh SRB2 SuperVirus!1
	//#define SUPERVIRUS2 // Teh SRB2 SuperVirus!1 ... On Eggman!
	//#define SUPERVIRUS3 // Replaces the intro wiff my own. >:3
	//#define SACITY // SA City extentions
	#define RUSSIA // Soviet Russia Blast 2!!
	//#define ZELDA // TML / LOZSRB2 partial port by JTE. Incomplete.
	#define MOBJSCALE // Scale up/down objects
	//#define SPRITEOFFS // Speshule sucky hardware sprite effects (Eggmobile) Do not enable.
	//#define DIGGING // Digging ability for Knuckles, by Shuffle. To be reviewed and fixed.
	#define BUGFIXES // Random bug fixes all over the place.
	#define HOLIDAYS // Holiday changes!
	//#define JTEWAD // Simple, but effective, encrypted wad format. Incomplete. Sucky idea. :<
	//#define RANDOMFIX // Well, actually... It's more like a complete rewrite. Currently incomplete.
	                  // Makes use of a pseudo-random number generator rather than a horribly lacking array.
	#define JTEINTRO // Custom SRB2JTE intro.
#endif

#ifdef SHUFFLE
	#define ACIDSOCKS // You don't wanna touch these socks.
	#define ACIDMOBJS // New mobj types for Acid Missile.
	#define BETTERMARIOMODE
	#define BUGFIXES
	#define RINGCOLLECT // I think this will be a fun new game mode. I hope you think so, too!
	#define BETTER2DMODE
	#define MAXCAMERADIST 140*FRACUNIT // Max distance the camera can be in front of the player (2D mode)
//	#define PARTICLES
	#define NOLAGGYWATER // Don't lag anymore when you jump in and out of water.
	#define CRASHFIX
	#define MULTIRINGS
	#define SEENAMES
	#define MAPCREDITS
	//#define FLAME
	#define WATERWAVING
	#define FLASHFIX
//	#define MOTIONBLUR
//	#define NEWPARTICLES
//	#define MAXPARTICLES 10000
//	#define COUNTPOLYS
	#ifndef _WIN32_WCE
//	#define NEWSTUFF
//	#define TIMEOFDAY // Oooooh snap!
	#endif
#endif

#ifdef SPMOVES
	#define HOLDDIST (pickup->radius*2 + mo->radius*2)
	#define TOSSMIN (3*FRACUNIT)
	#define TOSSMAX (24*FRACUNIT)
	#define TOSSMOD (pickup->radius/4)
#endif

#ifdef UNIVERSE
	#define SULOBBYMAP 20
	#define SUHUBMAP 21
#endif

#ifdef ZELDA
	#define HEARTSEGS 2
	#define HEARTS(x) (x*HEARTSEGS)
#endif

#ifdef SUPERVIRUS0
	#ifndef SUPERVIRUS1
		#define SUPERVIRUS1
	#endif
	#define VIRSPEED (20*FRACUNIT)
#elif defined(SUPERVIRUS1)
	#define VIRSPEED (50*FRACUNIT)
#endif

#if defined(SUPERVIRUS1) || defined(SUPERVIRUS3) || defined(HOLIDAYS) || defined(RINGCOLLECT)
	// JTERAND turns num into a long between 0 and max, assuming num is a number from 0 to 255.
	#define JTERAND(num,max) ((long)(max)*(long)(num)/256)
#endif

#if defined(SUPERVIRUS1) || defined(SUPERVIRUS2)
	#define SUPERVIRUS
#endif

#ifdef BETTERMARIOMODE
	#define MOBJSCALE
	#define BLOCKHITSPEED (-6*FRACUNIT)
	#define MAXZMOVE (20*FRACUNIT)
#define MARIOTIMELIMIT 400
#endif

#ifdef MOBJSCALE
	#ifdef BETTERMARIOMODE
		#define SCALEMARIO // Mario mode scaling (Mushroom replaces shields)
		#define SCALESPEEDS // Scale mobj speeds as well
		#define SCALEMISC2
	#else
		#define SCALEVAR // Console variable "scale"
		#define SCALEMISC // Scale other things (Shields, sparkles, etc)
		//#define SCALEGRAVITY // Scale gravity and jumping
		//#define SCALESPEEDS // Scale mobj speeds as well
	#endif
	#define SCALESIZE // Scale the object's height and radius as well as it's sprite.
	#define SCALEMACS // Scaling macros, required for scaling stuffs.
#endif

#ifdef SCALEMARIO
	#define MARIOSIZE 50
#endif

#ifdef SCALEMACS
	// Scale is an integer percent. So multiply by scale and divide by 100.
	// FLOATSCALE is because OpenGL uses real floating point rather then fixed_t.
	#define FLOATSCALE(x,y) ((y)*(x)/100.0f)
	#define FIXEDSCALE(x,y) FixedDiv(FixedMul((y)<<FRACBITS,(x)),100<<FRACBITS)
	#define FIXEDUNSCALE(x,y) ((y) == 0 ? 0 : FixedDiv(FixedMul((100<<FRACBITS),(x)),(y)<<FRACBITS))
#endif

#if defined(SUPERVIRUS2) || defined(SACITY) || defined(JTEMISC)
	#define A_TORNADOPARTICLE // Tornado particle thinker: Swirls around it's target while flying upwards and outwards.
#endif

#ifdef BUGFIXES // OPENGL IS SUPPORTED KTHXBYE
	//#define SRB2TODO // bugfixes from main SRB2109

	// Straight fixes
	//#define EXITFIX // A billion extra checks for player->exiting in P_Spec.c so you don't die while exiting! Totally unneccessary.
	#define TRANSFIX // Gets translation table from mobj->skin rather then player->skin. Doesn't affect SRB2 official.
//	#define HARDWAREFIX2 // Hardware rendering mode colormap and fog stuffs. It's fixed now.
//	#define HARDWAREFIX3 // Alternate light level mapping in OpenGL that some people dislike purely because it takes away their "OpenGL advantage"... Also fixed.
	//#define HURTFIX // When you move too far on the Z axis in one tic, you get hurt. Currently broken.
	//Example: Jumping from really high and trying to hit a crawla, you SHOULD hit the Crawla,
	// but if you are going quickly enough, you would be above the enemy last tic,
	// but have already landed on this tic, so you completely missed.
	#define SHADOWFIX // Shadows aren't supposed to show colors, they're supposed to be black! :/
#endif

#if defined(PARTICLES) || defined(ACIDSOCKS) || defined(HURTFIX)
	#define NEWFLAGS
#endif
