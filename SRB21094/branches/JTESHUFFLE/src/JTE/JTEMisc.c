// JTE Misc
// By Jason the Echidna
// MINE! NO STEALY!

#include "JTEDefs.h"
#include "../doomstat.h"
#include "../r_main.h"
#include "../st_stuff.h"
#include "../p_local.h"
#include "../g_game.h"
#include "../z_zone.h"
#include "../w_wad.h"
#include "../v_video.h"

#include "JTEMisc.h"
#include "JB_Bot.h" // JTEBOTS

#ifdef BETTERMARIOMODE
static patch_t* marionum[10];	// 0-9, Mario numbers
static patch_t* mariotime;		// "TIME" HUD graphic
static patch_t* mariocoin[3];	// Coin HUD graphic
static patch_t* marioscore;		// "SCORE" HUD graphic

///************ Drawing stuff **************///
static inline int SCX(int x)
{
	return (int)(x * vid.fdupx);
}

static int SCY(int y)
{
	y = (int)(y * vid.fdupy); // scale to resolution
	if (splitscreen)
	{
		y >>= 1;
		if (stplyr != &players[displayplayer])
			y += vid.height / 2;
	}
	return y;
}
///*************************************///
#endif

#if defined(CCPAL) || defined(HOLIDAYS)
#define DISTMAP         2
void JTE_ReinitLightTables(void)
{
	int         i;
	int         j;
	int         level;
	int         startmap;
	int         scale;

	// Calculate the light levels to use for each level/scale combination.
	for (i=0 ; i< LIGHTLEVELS ; i++)
	{
		startmap = ((LIGHTLEVELS-1-i)*2)*NUMCOLORMAPS/LIGHTLEVELS;
		for (j=0 ; j<MAXLIGHTZ ; j++)
		{
			// zlight table
			scale = FixedDiv ((BASEVIDWIDTH/2*FRACUNIT), (j+1)<<LIGHTZSHIFT);
			scale >>= LIGHTSCALESHIFT;
			level = startmap - scale/DISTMAP;

			if (level < 0)
				level = 0;

			if (level >= NUMCOLORMAPS)
				level = NUMCOLORMAPS-1;

			zlight[i][j] = colormaps + level*256;
		}

		for(j = 0; j < MAXLIGHTSCALE; j++)
		{
			// scalelight table
			level = startmap - j*vid.width/(viewwidth)/DISTMAP;

			if (level < 0)
				level = 0;

			if (level >= NUMCOLORMAPS)
				level = NUMCOLORMAPS - 1;

			scalelight[i][j] = colormaps + level*256;
		}
	}
}
#endif

#ifdef A_TORNADOPARTICLE
#include "../p_local.h"
#include "../m_random.h"
// Function: A_TornadoParticle
//
// Description: Spins the mobj around it's target
// in a random outward path rising upwards until
// it hits the ceiling. It's angle is unaffected.
//
// var1 = unused
// var2 = unused
//
void A_TornadoParticle(mobj_t* mobj)
{
	angle_t angle;

	// No particle or nothing to spin around? For shame!
	if (!mobj || !mobj->target)
		return;

	// Get this tic's random direction!
	angle = R_PointToAngle2(mobj->x, mobj->y, mobj->target->x, mobj->target->y);
	angle += (ANG90+ANG45) - (P_Random()%9)*ANGLE_10;

	// Spin! Spin!! Faster!
	P_InstaThrust(mobj, angle, 16*FRACUNIT);
	P_SetObjectMomZ(mobj, 8*FRACUNIT, false);

	// Hit the ceiling? Byebye!
	if (mobj->z + mobj->height >= mobj->ceilingz)
		P_SetMobjState(mobj,S_DISS);
}
#endif

#ifdef JTEMISC
void JTE_DeSuper(player_t* player)
{
	player->powers[pw_super] = false;

	if (player->mo->health > 0)
	{
		if ((player->pflags & PF_JUMPED) || (player->pflags & PF_SPINNING))
			P_SetPlayerMobjState(player->mo, S_PLAY_ATK1);
		else if (player->pflags & PF_RUNNINGANIM)
			P_SetPlayerMobjState(player->mo, S_PLAY_SPD1);
		else if (player->pflags & PF_WALKINGANIM)
			P_SetPlayerMobjState(player->mo, S_PLAY_RUN1);
		else
			P_SetPlayerMobjState(player->mo, S_PLAY_STND);
	}

	// Resume normal music if you're the console player
	if (P_IsLocalPlayer(player))
		P_RestoreMusic(player);

	// If you had a shield, restore its visual significance.
	P_SpawnShieldOrb(player);
}

// Why isn't ROCKET_SPR used anywhere?! It seriously rocks!!
#ifdef HWRENDER
#include "../hardware/hw_light.h"
void JTE_HwLights(void)
{
	lightspritenum_t thing;

	lspr[RINGSPARK_L].type = 0x13;
	lspr[SUPERSONIC_L].type = 0x13;
	lspr[INVINCIBLE_L].type = 0x13;
	lspr[GREENSHIELD_L].type = 0x13;
	lspr[BLUESHIELD_L].type = 0x13;
	lspr[YELLOWSHIELD_L].type = 0x13;
	lspr[REDSHIELD_L].type = 0x13;
	lspr[BLACKSHIELD_L].type = 0x13;
	lspr[WHITESHIELD_L].type = 0x13;

	thing = SUPERSPARK_L;
	lspr[thing].type = 0x13;
	lspr[thing].light_yoffset = 16.0f;
	lspr[thing].dynamic_radius = 16.0f;
}
#endif
#endif

#ifdef MOBJSCALE
#include "../p_local.h"
#include "../m_random.h"
#include "../g_game.h"
#include "../p_mobj.h"
#include "../r_things.h"
#ifdef SCALESIZE
#include "../console.h"
#endif

#ifdef SCALEVAR
#include "../byteptr.h"

static void MyScale_OnChange(void);
static CV_PossibleValue_t mobjscale_cons_t[] = {{10, "MIN"}, {400, "MAX"}, {0, NULL}};
consvar_t cv_myscale = {"scale", "100", CV_CALL|CV_NOINIT, mobjscale_cons_t, MyScale_OnChange, 0, NULL, NULL, 0, 0, NULL};

void JTE_SendMyScale(void)
{
	USHORT buf;

	// Outside of netgames, just set the destscale.
	if (!netgame)
	{
		players[consoleplayer].mo->destscale = cv_myscale.value;
		return;
	}

	// In netgames, send net cmd.
	buf = cv_myscale.value;
	SendNetXCmd(XD_MYSCALE,&buf,sizeof(USHORT));
}

static void MyScale_OnChange(void)
{
	if (!Playing()
	|| !playeringame[consoleplayer]
	|| players[consoleplayer].playerstate != PST_LIVE
	|| !players[consoleplayer].mo)
		return;

	if (!stricmp(cv_myscale.string, "MAX"))
		cv_myscale.value = 400;
	JTE_SendMyScale();
}

static void Got_MyScale(byte** p, int playernum)
{
	if (!playeringame[playernum]
	|| players[playernum].playerstate != PST_LIVE
	|| !players[playernum].mo)
	{
		READUSHORT(*p);
		return;
	}
	players[playernum].mo->destscale = READUSHORT(*p);
}
#endif

void JTE_InitScale(void)
{
#ifdef SCALEVAR
	CV_RegisterVar(&cv_myscale);
	RegisterNetXCmd(XD_MYSCALE, Got_MyScale);
#endif
}
#endif

#ifdef SPRITEOFFS
#include "../info.h"

spriteoffs_t *spriteoffs[NUMSPRITES];
#define AllocOffs(x)\
		spriteoffs[x] = ZZ_Alloc(sizeof(spriteoffs_t));\
		memset(spriteoffs[x],0,sizeof(spriteoffs_t))
void JTE_InitSpriteOffs(void)
{
	size_t thing;
	memset(spriteoffs,0,sizeof(spriteoffs));

	// Mobjs

	thing = MT_EGGMOBILE_FG;
	mobjinfo[thing] = mobjinfo[MT_DISS];
	mobjinfo[thing].spawnstate = S_EGGMFG;
	mobjinfo[thing].flags = MF_NOBLOCKMAP|MF_NOCLIP|MF_NOGRAVITY|MF_SCENERY;

	thing = MT_EGGMOBILE_BG;
	mobjinfo[thing] = mobjinfo[MT_DISS];
	mobjinfo[thing].spawnstate = S_EGGMBG;
	mobjinfo[thing].flags = MF_NOBLOCKMAP|MF_NOCLIP|MF_NOGRAVITY|MF_SCENERY;

	// States

	thing = S_EGGMFG;
	states[thing] = states[S_NULL];
	states[thing].sprite = SPR_EMFG;

	thing = S_EGGMBG;
	states[thing] = states[S_NULL];
	states[thing].sprite = SPR_EMBG;

	// Sprites

	// Things to remember...
	//
	// The sprite vertexes go:
	//  3--2
	//  | /|
	//  |/ |
	//  0--1
	//
	// Negative z means closer to the camera,
	// while positive means farther away.
	//

	thing = SPR_EMFG;
	strcpy(sprnames[thing],"EMFG");
#ifdef HWRENDER
	t_lspr[thing] = &lspr[NOLIGHT];
#endif
	AllocOffs(thing);
	spriteoffs[thing]->z[3] = spriteoffs[thing]->z[2] = -32;
	spriteoffs[thing]->z[0] = spriteoffs[thing]->z[1] = -1;

	thing = SPR_EMBG;
	strcpy(sprnames[thing],"EMBG");
#ifdef HWRENDER
	t_lspr[thing] = &lspr[NOLIGHT];
#endif
	AllocOffs(thing);
	spriteoffs[thing]->z[3] = spriteoffs[thing]->z[2] = 32;
	spriteoffs[thing]->z[0] = spriteoffs[thing]->z[1] = 1;
}
#undef AllocOffs
#endif

#ifdef HOLIDAYS
#include "../info.h"
#include "../i_video.h"
#include "../v_video.h"
#include "../w_wad.h"
#include "../z_zone.h"
holidayreplace_t *holidayreplace = NULL;
const char *holidaynicks[NUMHOLIDAYS] = {
	NULL,
	"VALN","PTRK","FOOL","ESTR",
	"WEEN","THXG","XMAS","NEWY"
};

#ifdef HWRENDER
#define ADDSPRRANGE(start, finish, name) {\
	for(thing = (start)+1; thing < (finish); thing++)\
	{\
		strcpy(sprnames[thing], va(name"%d",(thing-(start))));\
		t_lspr[thing] = &lspr[NOLIGHT];\
	}\
}
#else
#define ADDSPRRANGE(start, finish, name) {\
	for(thing = (start)+1; thing < (finish); thing++)\
		strcpy(sprnames[thing], va(name"%d",(thing-(start))));\
}
#endif

void JTE_InitHolidaySprites(void)
{
	size_t thing;

	thing = SPR_VALN;
	strcpy(sprnames[thing], "VALN");
#ifdef HWRENDER
	t_lspr[thing] = &lspr[NOLIGHT];
#endif
	ADDSPRRANGE(SPR_VALN, SPR_PTRK, "VAL");

	thing = SPR_PTRK;
	strcpy(sprnames[thing], "PTRK");
#ifdef HWRENDER
	t_lspr[thing] = &lspr[NOLIGHT];
#endif
	ADDSPRRANGE(SPR_PTRK, SPR_FOOL, "PTK");

	thing = SPR_FOOL;
	strcpy(sprnames[thing], "FOOL");
#ifdef HWRENDER
	t_lspr[thing] = &lspr[NOLIGHT];
#endif
	ADDSPRRANGE(SPR_FOOL, SPR_ESTR, "FOL");

	thing = SPR_ESTR;
	strcpy(sprnames[thing], "ESTR");
#ifdef HWRENDER
	t_lspr[thing] = &lspr[NOLIGHT];
#endif
	ADDSPRRANGE(SPR_ESTR, SPR_WEEN, "EST");

	thing = SPR_WEEN;
	strcpy(sprnames[thing], "WEEN");
#ifdef HWRENDER
	t_lspr[thing] = &lspr[NOLIGHT];
#endif
	ADDSPRRANGE(SPR_WEEN, SPR_THXG, "WEN");

	thing = SPR_THXG;
	strcpy(sprnames[thing], "THXG");
#ifdef HWRENDER
	t_lspr[thing] = &lspr[NOLIGHT];
#endif
	ADDSPRRANGE(SPR_THXG, SPR_XMAS, "THX");

	thing = SPR_XMAS;
	strcpy(sprnames[thing], "XMAS");
#ifdef HWRENDER
	t_lspr[thing] = &lspr[NOLIGHT];
#endif

	// Xmas sprites can't use ADDSPRRANGE because
	// XMS1, XMS2, and XMS3 already exist.
	for(thing = SPR_XMAS+1; thing < SPR_NEWY; thing++)
	{
		strcpy(sprnames[thing], va("XMS%i",(thing-SPR_XMAS+3)));
#ifdef HWRENDER
		t_lspr[thing] = &lspr[NOLIGHT];
#endif
	}

	thing = SPR_NEWY;
	strcpy(sprnames[thing], "NEWY");
#ifdef HWRENDER
	t_lspr[thing] = &lspr[NOLIGHT];
#endif
	ADDSPRRANGE(SPR_NEWY, SPR_NEWY+10, "NEW");
}

void JTE_LoadHolidayCfg(void)
{
	lumpnum_t clump;
	const char *holidaystr;
	holidayreplace_t *replace, *next;

	// Clear the previous replace list.
	for(replace = holidayreplace; replace; replace = next)
	{
		next = replace->next;
		Z_Free(replace);
	}
	holidayreplace = NULL;

	// Find out what holiday it is!
	if (holidaynicks[holiday])
		holidaystr = va("%sCFG",holidaynicks[holiday]);
	else
		return;

	// Get the lump... No lump? No special stuffs!
	clump = W_CheckNumForName(holidaystr);
	if (clump == LUMPERROR)
		return;

	// And load it up. :3
	CONS_Printf("Refreshing holiday config...\n");
	DEH_LoadDehackedLump(clump);
}

byte JTE_ReadHoliday(const char *name)
{
	byte found = H_NONE, i;
	if (!name || !*name)
		return H_NONE;
	if (isdigit(*name))
		return (byte)atoi(name);
	for(i = H_NONE; i < NUMHOLIDAYS; i++)
		if (holidaynicks[i]
		&& !stricmp(holidaynicks[i],name))
		{
			found = i;
			break;
		}
	return found;
}

#if !defined (__MINGW32__) && !defined (__MINGW64__) && !defined (__CYGWIN__) && !defined (_MSC_VER)
/** Converts a string to uppercase in-place.
  * Replaces strupr() from the Microsoft C runtime.
  *
  * \param n String to uppercase.
  * \return Pointer to the same string passed in, now in all caps.
  * \author Alam Arias
  */
static char *strup1(char *n)
{
	int i;
	char *upr = n;
	if (!n) return NULL;
	for (i = 0; n[i]; i++)
		upr[i] = toupper(n[i]);
	return upr;
}
#undef strupr
#define strupr(n) strup1(n)
#endif

void JTE_ReadReplace(MYFILE *f, byte type)
{
	XBOXSTATIC char s[MAXLINELEN];
	char *word;
	char *word2;
	char *tmp;
	holidayreplace_t *replace, *find/*, *last*/;

	// Remove old replacements of this type.
	/*for(replace = holidayreplace, last = NULL; replace; replace = find)
	{
		find = replace->next;
		if (replace->type == type)
		{
			if (last)
				last->next = find;
			if (replace == holidayreplace)
				holidayreplace = find;
			Z_Free(replace);
		}
		else
			last = replace;
	}*/

	// Load in new ones.
	do
	{
		if (myfgets(s, sizeof (s), f))
		{
			if (s[0] == '\n')
				break;

			// First remove trailing newline, if there is one
			tmp = strchr(s, '\n');
			if (tmp)
				*tmp = '\0';

			tmp = strchr(s, '#');
			if (tmp)
				*tmp = '\0';

			// Get the part before the " = "
			tmp = strchr(s, '=');
			*(tmp-1) = '\0';
			word = strupr(s);

			// Now get the part after
			tmp += 2;
			word2 = strupr(tmp);

			// Malloc and link replace
			replace = ZZ_Alloc(sizeof(*replace));
			if (!holidayreplace)
				holidayreplace = replace;
			else for(find = holidayreplace; find; find = find->next)
				if (find->next == NULL)
				{
					find->next = replace;
					break;
				}

			// Copy in the data
			strncpy(replace->original,word,8);
			replace->original[8] = '\0';

			// SFX replace
			if (type == 3)
			{
				strlwr(replace->original);
				snprintf(replace->replace,8,"%d",S_AddSoundFx(word2,false,16,false));
				replace->replace[8] = '\0';
				replace->type = 5;
				continue;
			}
			// Other replace
			else
			{
				strncpy(replace->replace,word2,8);
				replace->replace[8] = '\0';
				replace->type = type;
			}
		}
	} while (!myfeof(f));
}

typedef struct titleprecip_s
{
	USHORT x,y;
	char momx,momy;
	patch_t *patch;

	struct titleprecip_s *next;
} titleprecip_t;
static titleprecip_t *titleprecip = NULL;

void JTE_TitlePrecip(void)
{
	titleprecip_t *precip, *last;
	static int preciptimer = 0;

	// Add another one!
	{
		char momx = 0,momy = 0;
		patch_t *patch = NULL;

		preciptimer++;
		switch(holiday)
		{
			case H_WEEN:
				patch = W_CachePatchName("RAINA0", PU_CACHE);
				momx = (char)(JTERAND(M_Random(),6)+8);
				momy = (char)(JTERAND(M_Random(),8)+8);
				preciptimer = -2;
				break;

			case H_XMAS:
				if (preciptimer < 4)
					break;
				patch = W_CachePatchName(va("SNO1%c0", (int)(JTERAND(M_Random(),3)+'A')), PU_CACHE);
				momx = (char)(JTERAND(M_Random(),6)-3);
				momy = 1;
				preciptimer = -1;
				break;

			default:
				return;
		}
		while(preciptimer < 0)
		{
			precip = ZZ_Alloc(sizeof(titleprecip_t));
			precip->next = titleprecip;
			titleprecip = precip;

			precip->patch = patch;
			precip->x = (USHORT)JTERAND(M_Random(),BASEVIDWIDTH);
			if (precip->momy >= 0)
				precip->y = (USHORT)(rendermode == render_soft ? precip->patch->topoffset : 0);
			else
				precip->y = (USHORT)(BASEVIDWIDTH + precip->patch->topoffset);
			precip->momx = momx;
			precip->momy = momy;
			preciptimer++;
		}
	}

	// Draw and tick the precipitation
	for(precip = titleprecip, last = NULL; precip; precip = precip->next)
	{
		V_DrawScaledPatch(precip->x, precip->y, 0, precip->patch);

		// Move it
		precip->x = (USHORT)(precip->x + precip->momx);
		precip->y = (USHORT)(precip->y + precip->momy);

		// Wrap around the X axis
		if (precip->x > BASEVIDWIDTH)
		{
			if (precip->momx < 0)
				precip->x += BASEVIDWIDTH;
			else
				precip->x -= BASEVIDWIDTH;
		}

		// At the bottom? Free it.
		if (precip->y > BASEVIDHEIGHT + precip->patch->topoffset)
		{
			if (last)
				last->next = precip->next;
			else
				last = precip->next;
			if (titleprecip == precip)
				titleprecip = last;
			Z_Free(precip);
			precip = last;
			continue;
		}

		// Repeat
		last = precip;
	}
}
#endif

#ifdef SCALEMARIO
#include "../s_sound.h"

// Function to freeze the player in place, then make him grow and shrink repeatedly. A-la Super Mario Bros.
// Called from P_MovePlayer, where player->weapondelay needs to be != 0
void MAR_MarioScale(player_t* player, boolean reverse)
{
	mobj_t* pmo; // So I don't have to type player->mo a million times. Also read somewhere that this is faster *shrugs*
	pmo = player->mo;

	//Player isn't ready. Ready up!
	if (!pmo->reactiontime)
	{
		player->cmomx = pmo->momx;
		player->cmomy = pmo->momy;
		pmo->pmomz = pmo->momz;
		pmo->momx = pmo->momy = pmo->momz = 0;
		player->weapondelay = TICRATE;
		pmo->flags |= MF_NOGRAVITY;
		pmo->reactiontime = TICRATE;
		return;
	}

	if (!reverse)
	{
		// Only do scaling for mushrooms
		if (player->powers[pw_fireflower] == 1)
		{
			// Player is already ready to begin his funky transformation!
			if (player->weapondelay > 1)
			{
				switch(player->weapondelay)
				{
					case 30:
						P_SetScale(player->mo, 30);
						break;
					case 25:
						P_SetScale(player->mo, 60);
						break;
					case 20:
						P_SetScale(player->mo, 10);
						break;
					case 15:
						P_SetScale(player->mo, 40);
						break;
					case 10:
						P_SetScale(player->mo, MARIOSIZE*2);
						break;
				}
				return;
			}
		}
		// Change colors for fire flower
		else if(player->powers[pw_fireflower] == 2)
		{
			// Player is already ready to begin his funky transformation!
			if (player->weapondelay > 1)
			{
				switch(player->weapondelay)
				{
					case 30:
						player->mo->flags = (player->mo->flags & ~MF_TRANSLATION)
					| ((13)<<MF_TRANSSHIFT);
						break;
					case 25:
						player->mo->flags = (player->mo->flags & ~MF_TRANSLATION)
					| ((player->skincolor)<<MF_TRANSSHIFT);
						break;
					case 20:
						player->mo->flags = (player->mo->flags & ~MF_TRANSLATION)
					| ((13)<<MF_TRANSSHIFT);
						break;
					case 15:
						player->mo->flags = (player->mo->flags & ~MF_TRANSLATION)
					| ((player->skincolor)<<MF_TRANSSHIFT);
						break;
					case 10:
						player->mo->flags = (player->mo->flags & ~MF_TRANSLATION)
					| ((13)<<MF_TRANSSHIFT);
						break;
				}
				return;
			}
		}
	}
	else
	{
		if (player->weapondelay > 1)
		{
			switch(player->weapondelay)
			{
				case 30:
					P_SetScale(player->mo, 10);
					break;
				case 25:
					P_SetScale(player->mo, 80);
					break;
				case 20:
					P_SetScale(player->mo, 20);
					break;
				case 15:
					P_SetScale(player->mo, 90);
					break;
				case 10:
					P_SetScale(player->mo, MARIOSIZE);
					break;
			}
			return;
		}
	}

	// Player is already done with his shroom!
	if ((!player->mo->momx || !player->mo->momy || !player->mo->momz)
		&& player->weapondelay == 1)
	{
		// Give the player his original speed and momentum back
		pmo->momx = player->cmomx;
		pmo->momy = player->cmomy;
		pmo->momz = pmo->pmomz;
		pmo->flags &= ~MF_NOGRAVITY;

		// Clear out the other crap
		player->cmomx = player->cmomy = pmo->pmomz = 0;
	}
}

#ifdef BETTERMARIOMODE
// This will probably expand in the future
boolean MAR_MarioDamageMobj(player_t* player)
{
	if (player->exiting)
		return false;
	if (player->powers[pw_flashing])
		return false;
	if (player->pflags & PF_GODMODE)
		return false;
	if (player->mo->reactiontime)
		return false;

	// Already out of health? Die.
	if (player->powers[pw_fireflower] == 0)
	{
		P_SetObjectMomZ(player->mo, 15*FRACUNIT, false);
		P_KillMobj(player->mo, NULL, NULL);
		S_StartSound(player->mo, sfx_mariodeath);
	}
	else
	{

		player->powers[pw_fireflower] = 0;
		player->mo->flags = (player->mo->flags & ~MF_TRANSLATION)
						| ((player->skincolor)<<MF_TRANSSHIFT);
		player->powers[pw_flashing] = flashingtics-1;
		player->mo->destscale = MARIOSIZE;
		player->weapondelay = 1;
		S_StartSound(player->mo, sfx_altow4);
	}
	return true;
}

void MAR_InitHUD(void)
{
	byte i;
	char namebuf[9];

	for (i = 0; i < 10; i++)
	{
		// Numbers
		sprintf(namebuf, "MARNUM%d", i);
		marionum[i] = (patch_t *)W_CachePatchName(namebuf, PU_STATIC);

		// Coin HUD
		if (i<3)
		{
			sprintf(namebuf, "COINHUD%d", i);
			mariocoin[i] = (patch_t *)W_CachePatchName(namebuf, PU_STATIC);
		}
	}

	mariotime = W_CachePatchName("MARTIME", PU_STATIC);
	marioscore = W_CachePatchName("MARSCORE", PU_STATIC);

}

// Mario HUD. Duh
void MAR_DrawHUD(void)
{
	//Draw time
	int seconds = MARIOTIMELIMIT;

	seconds = seconds - stplyr->realtime/(TICRATE/2);
	
	if(seconds < 0)
		seconds = 0;

	// Draw "TIME" Graphic
	V_DrawScaledPatch(SCX(284), SCY(20), V_NOSCALESTART|V_TRANSLUCENT, mariotime);
	// Draw the time.
	if (seconds >= 0)
		ST_DrawOverlayNum(SCX(300), SCY(30), seconds, marionum);
	// Draw the extra zeros on the left
	if (seconds < 10)
		ST_DrawOverlayNum(SCX(292), SCY(30), 0, marionum);
	if (seconds < 100)
		ST_DrawOverlayNum(SCX(284), SCY(30), 0, marionum);

	// Draw coins
	ST_DrawOverlayNum(SCX(218), SCY(30), stplyr->health > 0 ? stplyr->health-1 : 0,	marionum);
	// Draw the extra zeros on the left.
	if (stplyr->health-1 < 10)
		ST_DrawOverlayNum(SCX(210), SCY(30), 0,	marionum);
	if (stplyr->health-1 < 100)
		ST_DrawOverlayNum(SCX(202), SCY(30), 0,	marionum);

	// Draw Coin Graphic
	V_DrawScaledPatch(SCX(180), SCY(30), V_NOSCALESTART|V_TRANSLUCENT, mariocoin[(leveltime/6)%3]);

	// Draw score
	if (stplyr->score < 999999)
		ST_DrawOverlayNum(SCX(80), SCY(30), stplyr->score, marionum);
	// Draw the extra zeros on the left
	if (stplyr->score < 10)
		ST_DrawOverlayNum(SCX(72), SCY(30), 0, marionum);
	if (stplyr->score < 100)
		ST_DrawOverlayNum(SCX(64), SCY(30), 0, marionum);
	if (stplyr->score < 1000)
		ST_DrawOverlayNum(SCX(56), SCY(30), 0, marionum);
	if (stplyr->score < 10000)
		ST_DrawOverlayNum(SCX(48), SCY(30), 0, marionum);
	if (stplyr->score < 999999)
		ST_DrawOverlayNum(SCX(40), SCY(30), 0, marionum);
	else
		ST_DrawOverlayNum(SCX(32), SCY(30), 999999, marionum);

	//Draw "SCORE" Graphic
	V_DrawScaledPatch(SCX(32), SCY(20), V_NOSCALESTART|V_TRANSLUCENT|V_SNAPTOLEFT, marioscore);

	// Do objectplace and devmode stuff since it doesn't go through the normal HUD drawing stuff.
	if (cv_objectplace.value && stplyr->mo && stplyr->mo->target)
	{
		char x[8], y[8], z[8];
		char doomednum[8], thingflags[8];
		sprintf(x, "%d", stplyr->mo->x >> FRACBITS);
		sprintf(y, "%d", stplyr->mo->y >> FRACBITS);
		sprintf(z, "%d", stplyr->mo->z >> FRACBITS);
		sprintf(doomednum, "%d", stplyr->mo->target->info->doomednum);
		sprintf(thingflags, "%d", cv_objflags.value);
		V_DrawString(16, 98, 0, "X =");
		V_DrawString(48, 98, 0, x);
		V_DrawString(16, 108, 0, "Y =");
		V_DrawString(48, 108, 0, y);
		V_DrawString(16, 118, 0, "Z =");
		V_DrawString(48, 118, 0, z);
		V_DrawString(16, 128, 0, "thing # =");
		V_DrawString(16+84, 128, 0, doomednum);
		V_DrawString(16, 138, 0, "flags =");
		V_DrawString(16+56, 138, 0, thingflags);
		V_DrawString(16, 148, 0, "snap =");
		V_DrawString(16+48, 148, 0, cv_snapto.string);
	}
	// Princess, another castle, etc.
	if (mariomode && stplyr->exiting)
	{
		/// \todo doesn't belong in status bar code AT ALL
		thinker_t *th;
		mobj_t *mo2;
		boolean foundtoad = false;

		// scan the remaining thinkers
		// to find toad
		for (th = thinkercap.next; th != &thinkercap; th = th->next)
		{
			if (th->function.acp1 != (actionf_p1)P_MobjThinker)
				continue;

			mo2 = (mobj_t *)th;
			if (mo2->type == MT_TOAD)
			{
				foundtoad = true;
				break;
			}
		}

		if (foundtoad)
		{
			V_DrawCenteredString(160, 32+16, 0, "Thank you!");
			V_DrawCenteredString(160, 44+16, 0, "But our earless leader is in");
			V_DrawCenteredString(160, 56+16, 0, "another castle!");
		}
	}
}
#endif
#endif

#ifdef DIGGING
void JTE_InitDigging(void)
{
	size_t thing;

	thing = SPR_DIRT;
	// sprnames and HW lighting
	strcpy(sprnames[thing], "DIRT");
#ifdef HWRENDER
	t_lspr[thing] = &lspr[NOLIGHT];
#endif

	// states
	thing = S_DIRT;
	states[thing] = states[S_DISS];
	states[thing].sprite = SPR_DIRT;
	states[thing].tics = 15;

#if 0
	{           // MT_DIRT
		-1,             // doomednum
		S_DIRT,			// spawnstate
		1000,           // spawnhealth
		S_NULL,         // seestate
		sfx_ambint,     // seesound
		0,              // reactiontime
		sfx_None,       // attacksound
		S_NULL,         // painstate
		255,            // painchance
		sfx_None,       // painsound
		S_NULL,         // meleestate
		S_NULL,         // missilestate
		S_DISS,         // deathstate
		S_NULL,         // xdeathstate
		sfx_None,       // deathsound
		0,              // speed
		8*FRACUNIT,     // radius
		16*FRACUNIT,    // height
		1000,           // mass
		0,              // damage
		sfx_None,       // activesound
		MF_NOBLOCKMAP,  // flags
		S_NULL          // raisestate
	},
#endif
	thing = MT_DIRT;
	mobjinfo[thing] = mobjinfo[MT_ROCKCRUMBLE1];
	mobjinfo[thing].spawnstate = S_DIRT;
}
#endif

#ifdef JTEWAD
static char JTEcrypt[] = {
	0x1A, 0x27, 0x7B, 0x39, 0x13, 0xE8, 0x68, 0xE7,
	0xCF, 0x43, 0x8B, 0x80, 0x86, 0xE8, 0xFB, 0x4F,
	0x34, 0x3B, 0x9A, 0xE1, 0x3A, 0x32, 0x85, 0x32,
	0xBF, 0xC7, 0xEC, 0x55, 0xF3, 0xED, 0x04, 0x56,
	0xB5, 0x30, 0xAF, 0x43, 0xE8, 0x71, 0x76, 0x61,
	0x31, 0xA6, 0x55, 0x28, 0xDE, 0xB5, 0x05, 0x1A,
	0xB7, 0x63, 0xA0, 0xDC, 0x35, 0xF9, 0xCB, 0x1A,
	0x6E, 0x9D, 0x80, 0x6B, 0x19, 0x8C, 0xB4, 0xF9
};
void JTE_Encrypt(void *src, unsigned int size)
{
	byte *p;
	for(p = (char *)src; (unsigned int)p < (unsigned int)src + size; p++)
		*p = *p + JTEcrypt[abs((unsigned int)p - (unsigned int)src)%sizeof(JTEcrypt)];
}
void JTE_Decrypt(void *src, unsigned int size)
{
	byte *p;
	for(p = (char *)src; (unsigned int)p < (unsigned int)src + size; p++)
		*p = *p - JTEcrypt[abs((unsigned int)p - (unsigned int)src)%sizeof(JTEcrypt)];
}
#endif

#ifdef JTEINTRO
#include "../d_main.h"
#include "../f_finale.h"
#include "../g_input.h"
#include "../hu_stuff.h"

static struct {
	USHORT stage;
	ULONG ticks;
	ULONG next;
	USHORT letters;
	USHORT nextletter;
	const char *text;
	char *string;
} intro;

#define TYPETIME (TICRATE/6)
#define NEXTLETTER (TYPETIME + JTERAND(M_Random(),4)*(TYPETIME/2))

static void JTE_InitIntroStage(USHORT newstage)
{
	intro.ticks = 0;

	intro.letters = 0;
	intro.nextletter = 0;

	intro.text = NULL;
	if (intro.string)
	{
		Z_Free(intro.string);
		intro.string = NULL;
	}

	switch(newstage)
	{
		case 0:
			intro.next = TICRATE/2;
			break;
		case 1:
			intro.text = "SONIC TEAM JR";
			intro.next = TICRATE;
			break;
		case 2:
			intro.text = "THE ECHIDNA TRIBE";
			intro.next = 3*TICRATE;
			break;
		case 3:
		case 5:
		case 7:
		case 8:
			intro.next = TICRATE;
			break;
		case 4:
			intro.next = 5*TICRATE;
			break;
		case 6:
			intro.next = 2*TICRATE;
			break;
		default:
			D_StartTitle();
			break;
	}
	intro.stage = newstage;
}

static void JTE_IntroTextTicker(void)
{
	if (intro.nextletter && intro.letters < strlen(intro.text))
		intro.nextletter--;
	if (!intro.nextletter)
	{
		intro.letters++;
		if (!intro.string)
			intro.string = ZZ_Alloc(strlen(intro.text)+1);
		switch(intro.stage)
		{
			case 1:
				strncpy(intro.string,intro.text,strlen(intro.text) - intro.letters);
				intro.string[strlen(intro.text) - intro.letters] = '\0';
				intro.nextletter = TYPETIME/2;
				break;
			default:
				strncpy(intro.string,intro.text,intro.letters);
				intro.string[intro.letters] = '\0';
				intro.nextletter = NEXTLETTER;
				break;
		}
	}
}

void F_StartIntro(void)
{
	if (introtoplay)
	{
		F_StartCustomCutscene(introtoplay - 1, false, false);
		return;
	}

	gamestate = GS_INTRO;
	gameaction = ga_nothing;
	playerdeadview = false;
	paused = false;
	CON_ToggleOff();
	CON_ClearHUD();

	memset(&intro,0,sizeof(intro));
	JTE_InitIntroStage(0);
}

void F_IntroTicker(void)
{
	intro.ticks++;

	if (intro.next && (!intro.text || intro.letters >= strlen(intro.text)))
		intro.next--;
	if (!intro.next)
		JTE_InitIntroStage(intro.stage+1);

	if (intro.text)
		JTE_IntroTextTicker();
}

void F_IntroDrawer(void)
{
	V_DrawFill(0, 0, vid.width, vid.height, 0);
	switch(intro.stage)
	{
		case 0:
			V_DrawCreditString(160 - (V_CreditStringWidth("SONIC TEAM JR")/2), 80, 0, "SONIC TEAM JR");
			V_DrawCreditString(160 - (V_CreditStringWidth("PRESENTS")/2), 96, 0, "PRESENTS");
			break;

		case 1:
		case 2:
			if (intro.string)
				V_DrawCreditString(160 - (V_CreditStringWidth(intro.text)/2), 80, 0, intro.string);
			V_DrawCreditString(160 - (V_CreditStringWidth("PRESENTS")/2), 96, 0, "PRESENTS");
			break;

		case 4:
			V_DrawCreditString(160 - (V_CreditStringWidth("THE OFFICIAL")/2), 64, 0, "THE OFFICIAL");
			V_DrawCreditString(160 - (V_CreditStringWidth("SRB TO JTE")/2), 80, 0, "SRB TO JTE");
			V_DrawCreditString(160 - (V_CreditStringWidth("CONVERSION")/2), 96, 0, "CONVERSION");
			V_DrawCreditString(160 - (V_CreditStringWidth("UTILITY")/2), 112, 0, "UTILITY");
			break;

		case 6:
			V_DrawCreditString(160 - (V_CreditStringWidth("AND STUFF")/2), 88, 0, "AND STUFF");
			break;

		case 8:
			V_DrawCreditString(160 - (V_CreditStringWidth("OH NOES")/2), 80, 0, "OH NOES");
			V_DrawCreditString(160 - (V_CreditStringWidth("VIRUS")/2), 96, 0, "VIRUS");
			break;

		default:
			break;
	}
}

boolean F_IntroResponder(event_t *event)
{
	int key = event->data1;

	// remap virtual keys (mouse & joystick buttons)
	switch (key)
	{
		case KEY_MOUSE1:
			key = KEY_ENTER;
			break;
		case KEY_MOUSE1 + 1:
			key = KEY_BACKSPACE;
			break;
		case KEY_JOY1:
		case KEY_JOY1 + 2:
			key = KEY_ENTER;
			break;
		case KEY_JOY1 + 3:
			key = 'n';
			break;
		case KEY_JOY1 + 1:
			key = KEY_BACKSPACE;
			break;
		case KEY_HAT1:
			key = KEY_UPARROW;
			break;
		case KEY_HAT1 + 1:
			key = KEY_DOWNARROW;
			break;
		case KEY_HAT1 + 2:
			key = KEY_LEFTARROW;
			break;
		case KEY_HAT1 + 3:
			key = KEY_RIGHTARROW;
			break;
	}

	if (event->type != ev_keydown && key != 301)
		return false;

	if (key != 27 && key != KEY_ENTER && key != KEY_SPACE && key != KEY_BACKSPACE)
		return false;

	JTE_InitIntroStage(-1);
	return true;
}
#endif
