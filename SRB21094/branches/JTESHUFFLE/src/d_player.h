// Emacs style mode select   -*- C++ -*-
//-----------------------------------------------------------------------------
//
// Copyright (C) 1993-1996 by id Software, Inc.
// Copyright (C) 1998-2000 by DooM Legacy Team.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//-----------------------------------------------------------------------------
/// \file
/// \brief player data structures

#ifndef __D_PLAYER__
#define __D_PLAYER__

// The player data structure depends on a number
// of other structs: items (internal inventory),
// animation states (closely tied to the sprites
// used to represent them, unfortunately).
#include "p_pspr.h"

// In addition, the player is just a special
// case of the generic moving object/actor.
#include "p_mobj.h"

// Finally, for odd reasons, the player input
// is buffered within the player data struct,
// as commands per game tick.
#include "d_ticcmd.h"

#include "JTE/Universe/SU_Type.h" // UNIVERSE

// Extra abilities/settings for skins (combinable stuff)
typedef enum
{
	SF_RUNONWATER       =    1, // Allow this character to run on water
	SF_SPINALLOWED      =    2, // Is the player allowed to spin/spindash (on floor)?
	SF_NOJUMPSPIN       =    4, // Play the "Spring Up" animation for jumping
	SF_ALLOWSUPER       =    8, // Allow this character to become super
	SF_SUPERANIMS       =   16, // If super, use the super sonic animations
	SF_SUPERSPIN        =   32, // Should spin frames be played while super?
	SF_MULTIABILITY     =   64, // Modifies an ability to allow things like multi-glide, multi-thok, etc.
	SF_LIGHTDASH        =  128, // Overrides cv_lightdash
	SF_HOMING           =  256, // Overrides cv_homing
	SF_RINGSLINGER      =  512, // Overrides cv_ringslinger
	SF_WATERSKIP        = 1024, // Can you skip across the water?

#ifdef SPMOVES
	SF_CHAOSCONTROL     = 0x00800, // Shadow's Chaos Control
	SF_METAL            = 0x01000, // Metal Sonic!
	SF_AMYDASH          = 0x02000, // Amy's Dash
	SF_HMS123           = 0x04000, // Everything
	SF_FLAME            = 0x08000, // Fire abilities
	SF_NODROWN          = 0x10000, // Can't drown
	SF_GRABOBJ          = 0x20000 // Can pick up other objects *NOT SILVER'S ABILITY*
#endif
} skinflags_t;

//
// Player states.
//
typedef enum
{
	// Playing or camping.
	PST_LIVE,
	// Dead on the ground, view follows killer.
	PST_DEAD,
	// Ready to restart/respawn???
	PST_REBORN
} playerstate_t;

// If more player frames are added after S_PLAY_SUPERHIT, update this.
extern long playerstatetics[MAXPLAYERS][S_PLAY_SUPERHIT+1];

//
// Player internal flags
//
typedef enum
{
	PF_AUTOAIM				=       0x1,

	// True if button down last tic.
	PF_ATTACKDOWN			=       0x2,
	PF_USEDOWN				=       0x4,
	PF_JUMPDOWN				=       0x8,

	// No damage, no health loss.
	PF_GODMODE				=      0x10,

	// No clipping, walk through barriers.
	PF_NOCLIP				=      0x20,

	// Did you get a time-over?
	PF_TIMEOVER				=      0x40,

	// Ready for Super?
	PF_SUPERREADY			=      0x80,

	// Are animation frames playing?
	PF_WALKINGANIM			=     0x100,
	PF_RUNNINGANIM			=     0x200,
	PF_SPINNINGANIM			=     0x400,

	// Character action status
	PF_JUMPED				=     0x800,
	PF_SPINNING				=    0x1000,
	PF_STARTDASH			=    0x2000,
	PF_THOKKED				=    0x4000,

	// Are you gliding?
	PF_GLIDING				=    0x8000,

	// Tails pickup!
	PF_CARRIED				=   0x10000,

	/*** NIGHTS STUFF ***/
	// Is the player in NiGHTS mode?
	PF_NIGHTSMODE			=  0x100000,
	PF_TRANSFERTOCLOSEST	=  0x200000,

	// Spill rings after falling
	PF_NIGHTSFALL			=  0x400000,
	PF_DRILLING				=  0x800000,
	PF_SKIDDOWN				= 0x1000000,

} pflags_t;

// Player powers. (don't edit this comment)
typedef enum
{
	pw_invulnerability,
	pw_sneakers,
	pw_flashing,
	pw_jumpshield, // jump shield
	pw_fireshield, // fire shield
	pw_tailsfly, // tails flying
	pw_underwater, // underwater timer
	pw_spacetime, // In space, no one can hear you spin!
	pw_extralife, // Extra Life timer
	pw_ringshield, // ring shield
	pw_bombshield, // bomb shield
	pw_watershield, // water shield
	pw_super, // Are you super?

	// Mario-specific
	pw_fireflower,

	// New Multiplayer Weapons
	pw_homingring,
	pw_railring,
	pw_infinityring,
	pw_automaticring,
	pw_explosionring,

	// NiGHTS powerups
	pw_superparaloop,
	pw_nightshelper,

#ifdef SPMOVES
	pw_amydash,
#endif
#ifdef UNIVERSE
	pw_sword,
#endif
#ifdef MULTIRINGS
	pw_multiring,
#endif

	NUMPOWERS
} powertype_t;

// ========================================================================
//                          PLAYER STRUCTURE
// ========================================================================
typedef struct player_s
{
	mobj_t *mo;

	playerstate_t playerstate;
	ticcmd_t cmd;

	// Determine POV, including viewpoint bobbing during movement.
	// Focal origin above r.z
	fixed_t viewz;
	// Base height above floor for viewz.
	fixed_t viewheight;
	// Bob/squat speed.
	fixed_t deltaviewheight;
	// bounded/scaled total momentum.
	fixed_t bob;

	// Mouse aiming, where the guy is looking at!
	// It is updated with cmd->aiming.
	angle_t aiming;

	angle_t awayviewaiming; // Used for cut-away view

	// This is only used between levels,
	// mo->health is used during levels.
	long health;

	// Power ups. invinc and invis are tic counters.
	long powers[NUMPOWERS];

	// Bit flags.
	// See pflags_t, above.
	pflags_t pflags;

	// For screen flashing (bright).
	long bonuscount;

	long specialsector; // lava/slime/water...

	// Player skin colorshift, 0-15 for which color to draw player.
	long skincolor;

	long skin;

	ULONG score; // player score
	long dashspeed; // dashing speed

	int normalspeed; // Normal ground

	int runspeed; // Speed you break into the run animation

	int thrustfactor; // Thrust = thrustfactor * acceleration
	int accelstart; // Starting acceleration if speed = 0.
	int acceleration; // Acceleration

	int charability; // Ability definition
                     //
	                 // 0 = Sonic Thok
	                 // 1 = Tails Fly
	                 // 2 = Knuckles Glide/Climb Combo
	                 // 3 = Glide Only
	                 // 4 = Double Jump
	                 // 5 = Super Sonic Float
	                 //

	ULONG charflags; // Extra abilities/settings for skins (combinable stuff)
	               //
	               // See SF_ flags

	mobjtype_t slingitem; // Object # for SF_RINGSLINGER to fire

	mobjtype_t thokitem; // Object # to spawn for the thok
	mobjtype_t spinitem; // Object # to spawn for spindash/spinning

	int actionspd; // Speed of thok/glide/fly
	int mindash; // Minimum spindash speed
	int maxdash; // Maximum spindash speed

	int supercolor; // Color to transform to when turning super

	int jumpfactor; // How high can the player jump?

	int boxindex; // Life icon index for 1up box (See A_1upThinker in p_enemy.c)

#ifndef TRANSFIX
	int starttranscolor; // Start position for the changeable color of a skin
	int endtranscolor; // End position for the changeable color of a skin
#endif

	int prefcolor; // forced color in single player, default in multi

	long lives;
	long continues; // continues that player has acquired

	long xtralife; // Ring Extra Life counter

	long speed; // Player's speed (distance formula of MOMX and MOMY values)
	long jumping; // Jump counter

	long secondjump; // for charability == 4 and charability == 5

	long fly1; // Tails flying
	ULONG scoreadd; // Used for multiple enemy attack bonus
	tic_t glidetime; // Glide counter for thrust
	long climbing; // Climbing on the wall
	long deadtimer; // End game if game over lasts too long
	long splish; // Don't make splish repeat tons
	tic_t exiting; // Exitlevel timer
	long blackow;

	byte homing; // Are you homing?

	////////////////////////////
	// Conveyor Belt Movement //
	////////////////////////////
	fixed_t cmomx; // Conveyor momx
	fixed_t cmomy; // Conveyor momy
	fixed_t rmomx; // "Real" momx (momx - cmomx)
	fixed_t rmomy; // "Real" momy (momy - cmomy)

	/////////////////////
	// Race Mode Stuff //
	/////////////////////
	long numboxes; // Number of item boxes obtained for Race Mode
	long totalring; // Total number of rings obtained for Race Mode
	tic_t realtime; // integer replacement for leveltime
	ULONG racescore; // Total of won categories
	ULONG laps; // Number of laps (optional)

	////////////////////
	// Tag Mode Stuff //
	////////////////////
	long tagit; // The player is it! For Tag Mode
	ULONG tagcount; // Number of tags player has avoided
	long tagzone; // Tag Zone timer
	long taglag; // Don't go back in the tag zone too early

	////////////////////
	// CTF Mode Stuff //
	////////////////////
	long ctfteam; // 0 == Spectator, 1 == Red, 2 == Blue
	USHORT gotflag; // 1 == Red, 2 == Blue Do you have the flag?

	long dbginfo; // Debugger
	long emeraldhunt; // # of emeralds found
	byte snowbuster; // Snow Buster upgrade!
	long bustercount; // Charge for Snow Buster

	long weapondelay; // Delay (if any) to fire the weapon again
	tic_t taunttimer; // Delay before you can use the taunt again

	// Starpost information
	long starpostx;
	long starposty;
	long starpostz;
	long starpostnum; // The number of the last starpost you hit
	tic_t starposttime; // Your time when you hit the starpost
	angle_t starpostangle; // Angle that the starpost is facing - you respawn facing this way
	ULONG starpostbit; // List of starposts hit

	/////////////////
	// NiGHTS Stuff//
	/////////////////
	angle_t angle_pos;
	angle_t old_angle_pos;

	mobj_t *axis1;
	mobj_t *axis2;
	tic_t bumpertime; // Currently being bounced by MT_NIGHTSBUMPER
	long flyangle;
	tic_t drilltimer;
	long linkcount;
	tic_t linktimer;
	long anotherflyangle;
	tic_t nightstime; // How long you can fly as NiGHTS.
	long drillmeter;
	byte drilldelay;
	byte bonustime; // Capsule destroyed, now it's bonus time!
	mobj_t *capsule; // Go inside the capsule
	byte mare; // Current mare

	short lastsidehit, lastlinehit;

	long losscount; // # of times you've lost only 1 ring

	mobjtype_t currentthing; // For the object placement mode

	boolean lightdash; // Experimental fun thing
	byte lightdashallowed;

	long onconveyor; // You are on a conveyor belt if nonzero

	mobj_t *awayviewmobj;
	long awayviewtics;

#if defined(JTEFLYCHAINS) || defined(JTEBOTS)
	boolean carrying;
#endif
#ifdef JTEBOTS
	struct bot_s* bot;
#endif

#ifdef SPMOVES
	byte chaoscontrol; // You are chaos controlling.
	byte thokked;
	mobj_t* jet; // Jet following you?
#endif

#ifdef UNIVERSE
	byte sulevel;
	ULONG suexp;

	USHORT sumaxhp;
	USHORT sumaxtp;

	USHORT sutp;
	USHORT sumoney;
	byte  suitems[NUMSUITEMS];

	ULONG equipweapon;
	ULONG equipbarrier;
	ULONG equiparmor;
	ULONG equipslot[4];
	ULONG equipmag;
#endif

#ifdef SUPERVIRUS1
	USHORT infected;
	byte confusion;
	byte confusiontime;
#endif
#ifdef SUPERVIRUS
	byte fizz;
#endif

#ifdef RUSSIA
	mobj_t* ring; // In Soviet Russia, ring collects YOU!
#endif

#ifdef ZELDA
	byte jumpnumber; // Three different jumps, because I'm cool.
	byte maxhearts;
	byte equipped; // Weapon equipped
	byte bombsout; // How many bombs you have laying on the ground
	byte swording; // Count-down until player can attack again
	tic_t bombtics; // How long you have the bomb bag for in Match.
	boolean bowmode; // Are you in bow and arrow mode? For drawing the HUD and shooting arrows
	boolean justwentintobowmodethistic; // Long variable name
	boolean swimming; // Is el player el swimming-o?
	byte gettingiteminfo; // Is there item information showing?

	//boolean pickup; // 0 = Not ready, 1 = Ready, 2 = Picked up.. Do I even need this anymore?
	//boolean falldamageready; // Link just fell from a cliff! KICK HIS ASS!

	boolean fairy; // Player is getting healed.
#endif

#ifdef ACIDCHARSTUFF
	long superdrain; // Rate at which rings drain from a super character
	long neededrings; // How many rings required to turn super
#endif

#ifdef HURTFIX
	int oldmomz; // Z momentum last tic
#endif

#ifdef BETTER2DMODE
	fixed_t camrelativex;
#endif
#ifdef EXTRALOG
	int totaljumps;
	int totalshotrings;
#endif
#ifdef SEENAMES
	char seename[32];
#endif
} player_t;

#endif
