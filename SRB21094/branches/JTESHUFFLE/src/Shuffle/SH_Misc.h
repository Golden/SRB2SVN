#if defined(PARTICLES) || defined(NEWPARTICLES)
void SHF_EmitParticle(mobj_t* source, mobjtype_t type, byte frequency, fixed_t speed, fixed_t zspeed, int alive, boolean pgravity, boolean addmomentum, byte color, byte repeat, boolean size);
void SHF_EmitSpinnyParticle(mobj_t* source, fixed_t speed, fixed_t zspeed, int alive, boolean pgravity, byte color, boolean size);
#endif
