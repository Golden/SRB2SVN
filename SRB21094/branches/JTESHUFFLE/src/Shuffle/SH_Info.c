// ShufflarB2 Stuff
// By Shuffle
#include "../d_englsh.h"
#include "../g_game.h"
#include "../m_menu.h"
#include "../m_random.h"
#include "../p_local.h"
#include "../p_mobj.h"
#include "../r_main.h"
#include "../s_sound.h"
#include "SH_Info.h"

#ifdef HWRENDER
#include "../hardware/hw_light.h"
#endif

#ifdef SHUFFLE
#ifdef SUPEREMERALDS
boolean usesuperemeralds;
short superemeralds; // Number of super emeralds collected
#endif
void AM_Init(void)
{
	size_t thing;
#ifdef SUPEREMERALDS
	thing = MT_SEMERALD;
	mobjinfo[thing] = mobjinfo[MT_EMERALD8];
	mobjinfo[thing].spawnstate = S_SEMERALD;
	mobjinfo[thing].doomednum = 6969;
	mobjinfo[thing].flags |= MF_HIRES;

	thing = S_SEMERALD;
	states[thing] = states[S_CEMK];
	states[thing].sprite = SPR_SMRL;
	states[thing].frame = 0;

	thing = SPR_SMRL;
	strcpy(sprnames[thing],"SMRL");
#ifdef HWRENDER
	t_lspr[thing] = &lspr[NOLIGHT];
#endif
#endif
#ifdef SEENAMES
	thing = MT_NAMECHECK;
	mobjinfo[thing] = mobjinfo[MT_GFZFLOWER1];
	mobjinfo[thing].flags = MF_NOBLOCKMAP|MF_MISSILE|MF_NOGRAVITY|MF_NOSECTOR|MF_NOTHINK;
	mobjinfo[thing].spawnstate = S_NAMECHECK;
	mobjinfo[thing].deathstate = S_DISS;
	mobjinfo[thing].speed = 60*FRACUNIT;

	thing = S_NAMECHECK;
	states[thing] = states[S_DISS];
	states[thing].sprite = SPR_DISS;
#endif
#ifdef ACIDMOBJS
	thing = MT_FLAMEJET;
	mobjinfo[thing] = mobjinfo[MT_GFZFLOWER1];
	mobjinfo[thing].flags |= MF_NOGRAVITY;
	mobjinfo[thing].doomednum = 6970;
	mobjinfo[thing].spawnstate = S_FLAMEJETSTND;

	thing = MT_VERTICALFLAMEJET;
	mobjinfo[thing] = mobjinfo[MT_GFZFLOWER1];
	mobjinfo[thing].flags |= MF_NOGRAVITY;
	mobjinfo[thing].doomednum = 6971;
	mobjinfo[thing].spawnstate = S_FLAMEJETSTND;

	thing = MT_METALFLOWER1;
	mobjinfo[thing] = mobjinfo[MT_GFZFLOWER1];
	mobjinfo[thing].doomednum = 6980;
	mobjinfo[thing].spawnstate = S_METALFLOWER1A;

	thing = MT_METALFLOWER2;
	mobjinfo[thing] = mobjinfo[MT_GFZFLOWER2];
	mobjinfo[thing].doomednum = 6981;
	mobjinfo[thing].spawnstate = S_METALFLOWER2A;
#ifdef BETTERMARIOMODE
	thing = MT_MUSHROOM;
	mobjinfo[thing] = mobjinfo[MT_FIREFLOWER];
	mobjinfo[thing].doomednum = 6990;
	mobjinfo[thing].spawnstate = S_MUSHROOM;
	mobjinfo[thing].reactiontime = TICRATE;
	mobjinfo[thing].flags |= MF_BOUNCE;

	thing = MT_1UPSHROOM;
	mobjinfo[thing] = mobjinfo[MT_MUSHROOM];
	mobjinfo[thing].doomednum = 6991;
	mobjinfo[thing].spawnstate = S_1UPSHROOM;

	thing = MT_STARMAN;
	mobjinfo[thing] = mobjinfo[MT_MUSHROOM];
	mobjinfo[thing].doomednum = 6992;
	mobjinfo[thing].spawnstate = S_STARMAN;

	thing = S_MUSHROOM;
	states[thing].frame = 0;
	states[thing].sprite = SPR_SHRM;
	states[thing].tics = 1;
	states[thing].nextstate = S_MUSHROOM;
	
	thing = S_1UPSHROOM;
	states[thing].frame = 0;
	states[thing].sprite = SPR_LSHM;
	states[thing].tics = 1;
	states[thing].nextstate = S_1UPSHROOM;

	thing = S_STARMAN;
	states[thing].frame = 0;
	states[thing].sprite = SPR_STRM;
	states[thing].tics = 1;
	states[thing].nextstate = S_STARMAN;

	thing = SPR_SHRM;
	strcpy(sprnames[thing],"SHRM");
#ifdef HWRENDER
	t_lspr[thing] = &lspr[NOLIGHT];
#endif

	thing = SPR_LSHM;
	strcpy(sprnames[thing],"LSHM");
#ifdef HWRENDER
	t_lspr[thing] = &lspr[NOLIGHT];
#endif

	thing = SPR_STRM;
	strcpy(sprnames[thing],"STRM");
#ifdef HWRENDER
	t_lspr[thing] = &lspr[NOLIGHT];
#endif
#endif
	thing = S_FLAMEJETSTND;
	states[thing] = states[S_DISS];
	states[thing].frame = 0;
	states[thing].sprite = SPR_DISS;
	states[thing].tics = 2*TICRATE;
	states[thing].nextstate = S_FLAMEJETSTART;

	thing = S_FLAMEJETSTART;
	states[thing] = states[S_DISS];
	states[thing].frame = 0;
	states[thing].sprite = SPR_DISS;
	states[thing].action.acp1 = (actionf_p1)A_ToggleFlameJet;
	states[thing].tics = 3*TICRATE;
	states[thing].nextstate = S_FLAMEJETSTOP;

	thing = S_FLAMEJETSTOP;
	states[thing] = states[S_DISS];
	states[thing].frame = 0;
	states[thing].sprite = SPR_DISS;
	states[thing].action.acp1 = (actionf_p1)A_ToggleFlameJet;
	states[thing].tics = 1;
	states[thing].nextstate = S_FLAMEJETSTND;

	thing = MT_FLAMEJETFLAME;
	mobjinfo[thing] = mobjinfo[MT_KOOPAFLAME];
	mobjinfo[thing].spawnstate = S_FLAMEJETFLAME1;

	thing = S_FLAMEJETFLAME1;
	states[thing] = states[S_FIREBALL1];
	states[thing].frame = 0;
	states[thing].frame |= tr_transfir<<FF_TRANSSHIFT;
	states[thing].sprite = SPR_FLME;
	states[thing].tics = 4;
	states[thing].nextstate = S_FLAMEJETFLAME2;

	thing = S_FLAMEJETFLAME2;
	states[thing] = states[S_FIREBALL1];
	states[thing].frame = 1;
	states[thing].frame |= tr_transfir<<FF_TRANSSHIFT;
	states[thing].sprite = SPR_FLME;
	states[thing].tics = 5;
	states[thing].nextstate = S_FLAMEJETFLAME3;

	thing = S_FLAMEJETFLAME3;
	states[thing] = states[S_FIREBALL1];
	states[thing].frame = 2;
	states[thing].frame |= tr_transfir<<FF_TRANSSHIFT;
	states[thing].sprite = SPR_FLME;
	states[thing].tics = 11;
	states[thing].nextstate = S_DISS;

	// Short Metal Flower
	thing = S_METALFLOWER1A;
	states[thing] = states[S_GFZFLOWERA];
	states[thing].frame = 0;
	states[thing].nextstate = S_METALFLOWER1B;
	states[thing].sprite = SPR_FWR5;
	states[thing].tics = TICRATE/2;

	thing = S_METALFLOWER1B;
	states[thing] = states[S_GFZFLOWERA2];
	states[thing].frame = 1;
	states[thing].nextstate = S_METALFLOWER1A;
	states[thing].sprite = SPR_FWR5;
	states[thing].tics = TICRATE/2;

	// Tall Metal Flower
	thing = S_METALFLOWER2A;
	states[thing] = states[S_GFZFLOWERB1];
	states[thing].frame = 0;
	states[thing].nextstate = S_METALFLOWER2B;
	states[thing].sprite = SPR_FWR6;
	states[thing].tics = TICRATE/4;

	thing = S_METALFLOWER2B;
	states[thing] = states[S_GFZFLOWERB2];
	states[thing].frame = 1;
	states[thing].nextstate = S_METALFLOWER2A;
	states[thing].sprite = SPR_FWR6;
	states[thing].tics = TICRATE/4;
	
	thing = SPR_FWR5;
	strcpy(sprnames[thing],"FWR5");
#ifdef HWRENDER
	t_lspr[thing] = &lspr[NOLIGHT];
#endif

	thing = SPR_FWR6;
	strcpy(sprnames[thing],"FWR6");
#ifdef HWRENDER
	t_lspr[thing] = &lspr[NOLIGHT];
#endif
#ifdef RINGCOLLECT
	thing = SPR_MAGT;
	strcpy(sprnames[thing], "MAGT");
#ifdef HWRENDER
	t_lspr[thing] = &lspr[NOLIGHT];
#endif

	thing = MT_REDRINGMAGNET;
	mobjinfo[thing] = mobjinfo[MT_GFZFLOWER1];
	mobjinfo[thing].spawnstate = S_RINGMAGNET;
	mobjinfo[thing].radius = 15*FRACUNIT;
	mobjinfo[thing].flags = MF_SOLID;
	mobjinfo[thing].reactiontime = 3*TICRATE+TICRATE/3;

	thing = MT_BLUERINGMAGNET;
	mobjinfo[thing] = mobjinfo[MT_GFZFLOWER1];
	mobjinfo[thing].spawnstate = S_RINGMAGNET;
	mobjinfo[thing].radius = 15*FRACUNIT;
	mobjinfo[thing].flags = MF_SOLID;
	mobjinfo[thing].reactiontime = 3*TICRATE+TICRATE/3;

	thing = S_RINGMAGNET;
	states[thing] = states[S_GFZFLOWERD1];
	states[thing].frame = 0;
	states[thing].sprite = SPR_MAGT;
	states[thing].tics = -1;

	thing = MT_SUCKRING;
	mobjinfo[thing] = mobjinfo[MT_FLINGRING];
	mobjinfo[thing].spawnstate = S_SUCKRING1;
	mobjinfo[thing].flags &= ~MF_SPECIAL;
	mobjinfo[thing].speed = 20*FRACUNIT;
#ifdef MULTIRINGS
	thing = MT_MULTIRING;
	mobjinfo[thing] = mobjinfo[MT_FLINGRING];
	mobjinfo[thing].spawnstate = S_SUCKRING1;
	mobjinfo[thing].flags = MF_SLIDEME|MF_SPECIAL;
#endif
	thing = S_SUCKRING1;
	states[thing] = states[S_RRNG1];
	states[thing].nextstate = S_SUCKRING2;
	states[thing].action.acp1 = NULL;
	states[thing].sprite = SPR_RRNG;
	states[thing].frame = 0;

	thing = S_SUCKRING2;
	states[thing] = states[S_RRNG2];
	states[thing].nextstate = S_SUCKRING3;
	states[thing].action.acp1 = NULL;
	states[thing].sprite = SPR_RRNG;
	states[thing].frame = 1;

	thing = S_SUCKRING3;
	states[thing] = states[S_RRNG3];
	states[thing].nextstate = S_SUCKRING4;
	states[thing].action.acp1 = NULL;
	states[thing].sprite = SPR_RRNG;
	states[thing].frame = 2;

	thing = S_SUCKRING4;
	states[thing] = states[S_RRNG4];
	states[thing].nextstate = S_SUCKRING5;
	states[thing].action.acp1 = NULL;
	states[thing].sprite = SPR_RRNG;
	states[thing].frame = 3;

	thing = S_SUCKRING5;
	states[thing] = states[S_RRNG5];
	states[thing].nextstate = S_SUCKRING6;
	states[thing].action.acp1 = NULL;
	states[thing].sprite = SPR_RRNG;
	states[thing].frame = 4;

	thing = S_SUCKRING6;
	states[thing] = states[S_RRNG6];
	states[thing].nextstate = S_SUCKRING7;
	states[thing].action.acp1 = NULL;
	states[thing].sprite = SPR_RRNG;
	states[thing].frame = 5;

	thing = S_SUCKRING7;
	states[thing] = states[S_RRNG7];
	states[thing].nextstate = S_SUCKRING1;
	states[thing].action.acp1 = NULL;
	states[thing].sprite = SPR_RRNG;
	states[thing].frame = 6;

#endif
	thing = SPR_FLME;
	strcpy(sprnames[thing],"FLME");
#ifdef HWRENDER
	t_lspr[thing] = &lspr[NOLIGHT];
#endif

	thing = sfx_fire;
	S_sfx[thing] = S_sfx[sfx_None];
	S_sfx[thing].name = "fire";
	S_sfx[thing].singularity = false;
	S_sfx[thing].priority = 64;
	S_sfx[thing].pitch = SF_NOINTERRUPT;
#endif
}
#endif
#if defined(PARTICLES) || defined(NEWPARTICLES)
void AM_InitPretty(void)
{
	size_t thing;

	//****** Mobj types ******//
	thing = MT_PARTICLE;
	mobjinfo[thing] = mobjinfo[MT_DISS];
	mobjinfo[thing].spawnstate = S_PARTICLE;
	mobjinfo[thing].flags = MF_SCENERY|MF_NOCLIPTHING;

	thing = MT_WATERFOUNTAIN;
	mobjinfo[thing] = mobjinfo[MT_GFZFLOWER1];
	mobjinfo[thing].flags |= MF_NOGRAVITY;
	mobjinfo[thing].doomednum = 6972;
	mobjinfo[thing].spawnstate = S_WATERFOUNTAIN;

	thing = MT_LIGHTPARTICLE;
	mobjinfo[thing] =  mobjinfo[MT_PARTICLE];
	mobjinfo[thing].spawnstate = S_LIGHTPARTICLE;

	thing = MT_SPIRALFOUNTAIN;
	mobjinfo[thing] = mobjinfo[MT_WATERFOUNTAIN];
	mobjinfo[thing].doomednum = 6973;
	mobjinfo[thing].spawnstate = S_SPIRALFOUNTAIN;

	//****** States ******//
	thing = S_PARTICLE;
	states[thing] = states[S_DISS];
	states[thing].sprite = SPR_PART;
	states[thing].tics = -1;
	states[thing].frame = 0;

	thing = S_PARTICLE2;
	states[thing] = states[S_PARTICLE];
	states[thing].frame = 1;

	thing = S_WATERFOUNTAIN;
	states[thing] = states[S_PARTICLE];
	states[thing].sprite = SPR_DISS;
	states[thing].tics = -1;
	states[thing].frame = 0;

	thing = S_LIGHTPARTICLE;
	states[thing] = states[S_PARTICLE];
	states[thing].sprite = SPR_LYTE;

	thing = S_LIGHTPARTICLE2;
	states[thing] = states[S_LIGHTPARTICLE];
	states[thing].frame = 1;

	thing = S_SPIRALFOUNTAIN;
	states[thing] = states[S_WATERFOUNTAIN];

	//***** Uhh.. SPRs? *****//
	thing = SPR_PART;
	strcpy(sprnames[thing],"PART");
#ifdef HWRENDER
	t_lspr[thing] = &lspr[NOLIGHT];
#endif

	thing = SPR_LYTE;
	strcpy(sprnames[thing],"PART");
#ifdef HWRENDER
	lspr[PARTICLE_L] = lspr[GREYSHINE_L];
	lspr[PARTICLE_L].dynamic_radius = 0.0f;
	lspr[PARTICLE_L].corona_radius = 16.0f;
	t_lspr[thing] = &lspr[PARTICLE_L];
#endif

}
#endif

#ifdef BETTERMARIOMODE
void MAR_InitMario(void)
{
	size_t thing;

	thing = MT_KOOPATROOPA;
	mobjinfo[thing] = mobjinfo[MT_BLUECRAWLA];
	mobjinfo[thing].doomednum = 6974;
	mobjinfo[thing].spawnstate = S_KOOPASTND1;
	mobjinfo[thing].seestate = S_KOOPAWALK1;
	mobjinfo[thing].speed = 0;
	mobjinfo[thing].height = 42*FRACUNIT;
	mobjinfo[thing].radius = 19*FRACUNIT;
	mobjinfo[thing].painchance = 0;

	thing = S_KOOPASTND1;
	states[thing] = states[S_DISS];
	states[thing].sprite = SPR_KOPA;
	states[thing].action.acp1 = (actionf_p1)A_Look;
	states[thing].tics = 1;
	states[thing].frame = 0;
	states[thing].nextstate = S_KOOPASTND2;

	thing = S_KOOPASTND2;
	states[thing] = states[S_DISS];
	states[thing].sprite = SPR_KOPA;
	states[thing].action.acp1 = (actionf_p1)A_Look;
	states[thing].tics = 1;
	states[thing].frame = 0;
	states[thing].nextstate = S_KOOPASTND1;

	thing = S_KOOPAWALK1;
	states[thing] = states[S_KOOPAWALK1];
	states[thing].sprite = SPR_KOPA;
	states[thing].action.acp1 = (actionf_p1)A_Chase;
	states[thing].tics = 5;
	states[thing].frame = 0;
	states[thing].nextstate = S_KOOPAWALK2;

	thing = S_KOOPAWALK2;
	states[thing] = states[S_KOOPAWALK2];
	states[thing].sprite = SPR_KOPA;
	states[thing].action.acp1 = (actionf_p1)A_Chase;
	states[thing].tics = -1;
	states[thing].frame = 1;
	states[thing].nextstate = S_KOOPAWALK3;

	thing = S_KOOPAWALK3;
	states[thing] = states[S_KOOPAWALK3];
	states[thing].sprite = SPR_KOPA;
	states[thing].action.acp1 = (actionf_p1)A_Chase;
	states[thing].tics = -1;
	states[thing].frame = 2;
	states[thing].nextstate = S_KOOPAWALK1;

	thing = SPR_KOPA;
	strcpy(sprnames[thing],"KOPA");
#ifdef HWRENDER
	t_lspr[thing] = &lspr[NOLIGHT];
#endif

	thing = sfx_crunch;
	S_sfx[thing] = S_sfx[sfx_crumbl];
	S_sfx[thing].name = "crunch";
	S_sfx[thing].singularity = true;
	S_sfx[thing].priority = 64;
	S_sfx[thing].pitch = SF_NOINTERRUPT|SF_X4AWAYSOUND;

	thing = sfx_mariodeath;
	S_sfx[thing] = S_sfx[sfx_None];
	S_sfx[thing].name = "mardead";
	S_sfx[thing].singularity = true;
	S_sfx[thing].priority = 127;

	thing = sfx_smallmariojump;
	S_sfx[thing] = S_sfx[sfx_None];
	S_sfx[thing].name = "smarjmp";
	S_sfx[thing].singularity = true;
	S_sfx[thing].priority = 127;
}
#endif
