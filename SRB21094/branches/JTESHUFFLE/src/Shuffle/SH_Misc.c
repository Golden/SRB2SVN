// ShufflarB2 Stuff
// By Shuffle
#include "../d_englsh.h"
#include "../g_game.h"
#include "../m_menu.h"
#include "../m_random.h"
#include "../p_local.h"
#include "../p_mobj.h"
#include "../r_main.h"
#include "../s_sound.h"
#include "SH_Misc.h"
#ifdef NEWPARTICLES
particle_t particle[MAXPARTICLES];
#endif

// SHF_EmitParticle
// Emits particles from the source.
// Frequency defines how often a call is ignored. Alive is how long the particle is alive.
// Color and speed are self-explainatory.
// For size, 0 is small, 1 is large(ish). (Size is only for MT_PARTICLE and MT_LIGHTPARTICLE)
#ifdef NEWPARTICLES
void SHF_EmitParticle(mobj_t* source, mobjtype_t type, byte frequency, fixed_t speed, fixed_t zspeed, int alive, boolean pgravity, boolean addmomentum, byte color, byte repeat, boolean size)
{
/*	byte ignore;
	int prand, i;
	for(i=0; i<repeat; i++)
	{
		mobj_t* particle;

		prand = P_Random()*2;
		ignore = P_Random();

		if (ignore > frequency)
			return;

		particle = P_SpawnMobj(source->x, source->y, source->z, type);
		if (particle->eflags & MF_VERTICALFLIP)
			particle->z--; // Particle can't be directly on the ceiling or it disappears!
		else
			particle->z++; // Particle can't be directly on the floor or it disappears!

		// Thrust the object in a random direction
		P_InstaThrust(particle, FixedAngle(prand*FRACUNIT), speed);
		P_SetObjectMomZ(particle, zspeed, false);

		// Used for following the object that spawned it.
		if (addmomentum)
		{
			particle->momx += source->momx;
			particle->momy += source->momy;
			P_SetObjectMomZ(particle, source->momz, true);
		}

		// Change color, tics, flags, and gravity
		particle->tics = alive;
		particle->flags = 0;
		particle->flags = (particle->flags & ~MF_TRANSLATION) | (color<<MF_TRANSSHIFT);
		particle->flags |= MF_NOBLOCKMAP|MF_NOCLIP|MF_SCENERY;
		particle->flags2 |= MF2_NOCLIPHEIGHT;
		particle->eflags |= SHF_PARTICLE;

		// Make sure you only try to change the state of particle sprites..
		if (size && (type == MT_PARTICLE || type == MT_LIGHTPARTICLE))
			P_SetMobjState(particle, S_PARTICLE2);

		if (!pgravity)
			particle->flags |= MF_NOGRAVITY|MF_FLOAT;
		else if (pgravity == 2)
			particle->eflags |= SHF_LOWGRAVITY;
	}*/
}
// Makes a spiral out of particles.
void SHF_EmitSpinnyParticle(mobj_t* source, fixed_t speed, fixed_t zspeed, int alive, boolean pgravity, byte color, boolean size)
{
	static int particlecount = 0;
	int i;
	for(i=0;i<MAXPARTICLES;i++)
	{
		// Don't go over the particle limit
		if(i==MAXPARTICLES-1)
		{
			CONS_Printf("No more particles!\n");
			return;
		}

		// Already alive? Move on and make a new one..
		if(particle[i].alive)
			continue;

		// Set the position and momentum.

		particle[i].x = source->x;
		particle[i].y = source->y;
		particle[i].z = source->z+1;
		particle[i].momz = zspeed;

		// Found a slot and setup a particle!
		particle[i].alive = true;

		particlecount++;
		CONS_Printf("%i\n", particlecount);
		break;

	}

	// Every other particle thrusts in back
	P_InstaThrustParticle(i, FixedAngle((source->angle*FRACUNIT)), speed);
	P_InstaThrustParticle(i, FixedAngle((source->angle*FRACUNIT)+ANG90), speed);

	// Change color, tics, and gravity
/*	lightparticle->flags = 0;
	lightparticle->flags = (lightparticle->flags & ~MF_TRANSLATION) | (color<<MF_TRANSSHIFT);
	lightparticle->flags |= MF_NOBLOCKMAP|MF_NOCLIP|MF_SCENERY;
	lightparticle->flags2 |= MF2_NOCLIPHEIGHT;

	lightparticle->tics = alive;

	lightparticle->eflags |= SHF_PARTICLE;

	if (!pgravity)
		lightparticle->flags |= MF_NOGRAVITY;
	
	else if (pgravity == 2)
		lightparticle->eflags |= SHF_LOWGRAVITY;

	// Make sure you only try to change the state of particle sprites..
	if (size)
		P_SetMobjState(lightparticle, S_LIGHTPARTICLE2);
*/
	source->angle+= 20;
}
#elif defined (PARTICLES)
void SHF_EmitParticle(mobj_t* source, mobjtype_t type, byte frequency, fixed_t speed, fixed_t zspeed, int alive, boolean pgravity, boolean addmomentum, byte color, byte repeat, boolean size)
{
	byte ignore;
	int prand, i;
	for(i=0; i<repeat; i++)
	{
		mobj_t* particle;

		prand = P_Random()*2;
		ignore = P_Random();

		if (ignore > frequency)
			return;

		particle = P_SpawnMobj(source->x, source->y, source->z, type);
		if (particle->eflags & MF_VERTICALFLIP)
			particle->z--; // Particle can't be directly on the ceiling or it disappears!
		else
			particle->z++; // Particle can't be directly on the floor or it disappears!

		// Thrust the object in a random direction
		P_InstaThrust(particle, FixedAngle(prand*FRACUNIT), speed);
		P_SetObjectMomZ(particle, zspeed, false);

		// Used for following the object that spawned it.
		if (addmomentum)
		{
			particle->momx += source->momx;
			particle->momy += source->momy;
			P_SetObjectMomZ(particle, source->momz, true);
		}

		// Change color, tics, flags, and gravity
		particle->tics = alive;
		particle->flags = 0;
		particle->flags = (particle->flags & ~MF_TRANSLATION) | (color<<MF_TRANSSHIFT);
		particle->flags |= MF_NOBLOCKMAP|MF_NOCLIP|MF_SCENERY;
		particle->flags2 |= MF2_NOCLIPHEIGHT;
		particle->eflags |= SHF_PARTICLE;

		// Make sure you only try to change the state of particle sprites..
		if (size && (type == MT_PARTICLE || type == MT_LIGHTPARTICLE))
			P_SetMobjState(particle, S_PARTICLE2);

		if (!pgravity)
			particle->flags |= MF_NOGRAVITY|MF_FLOAT;
		else if (pgravity == 2)
			particle->eflags |= SHF_LOWGRAVITY;
	}
}

// Makes a spiral out of particles.
void SHF_EmitSpinnyParticle(mobj_t* source, fixed_t speed, fixed_t zspeed, int alive, boolean pgravity, byte color, boolean size)
{
	mobj_t* lightparticle;

	// Every other particle should have a light.
	lightparticle = P_SpawnMobj(source->x, source->y, source->z, MT_LIGHTPARTICLE);

	// Particle can't be directly on the floor/ceiling or it disappears!
	if (lightparticle->eflags & MF_VERTICALFLIP)
		lightparticle->z--;
	else
		lightparticle->z++;

	// Every other particle thrusts in back
	P_InstaThrust(lightparticle, FixedAngle((source->angle*FRACUNIT)+ANG90), speed);

	P_SetObjectMomZ(lightparticle, zspeed, false);

	// Change color, tics, and gravity
	lightparticle->flags = 0;
	lightparticle->flags = (lightparticle->flags & ~MF_TRANSLATION) | (color<<MF_TRANSSHIFT);
	lightparticle->flags |= MF_NOBLOCKMAP|MF_NOCLIP|MF_SCENERY;
	lightparticle->flags2 |= MF2_NOCLIPHEIGHT;

	lightparticle->tics = alive;

	lightparticle->eflags |= SHF_PARTICLE;

	if (!pgravity)
		lightparticle->flags |= MF_NOGRAVITY;
	
	else if (pgravity == 2)
		lightparticle->eflags |= SHF_LOWGRAVITY;

	// Make sure you only try to change the state of particle sprites..
	if (size)
		P_SetMobjState(lightparticle, S_LIGHTPARTICLE2);

	source->angle+= 20;
}
#endif
