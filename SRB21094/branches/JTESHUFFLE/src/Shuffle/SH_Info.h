#ifdef SHUFFLE
void AM_Init(void);
#endif
#if defined(PARTICLES) || defined(NEWPARTICLES)
void AM_InitPretty(void);
#endif
#ifdef BETTERMARIOMODE
void MAR_InitMario(void);
#endif
