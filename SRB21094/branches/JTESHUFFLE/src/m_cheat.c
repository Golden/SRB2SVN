// Emacs style mode select   -*- C++ -*-
//-----------------------------------------------------------------------------
//
// Copyright (C) 1993-1996 by id Software, Inc.
// Copyright (C) 1998-2000 by DooM Legacy Team.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//-----------------------------------------------------------------------------
/// \file
/// \brief Cheat sequence checking

#include "doomdef.h"
#include "dstrings.h"

#include "g_game.h"

#include "m_cheat.h"

//
// CHEAT SEQUENCE PACKAGE
//

#define SCRAMBLE(a) \
((((a)&1)<<7) + (((a)&2)<<5) + ((a)&4) + (((a)&8)<<1) \
 + (((a)&16)>>1) + ((a)&32) + (((a)&64)>>5) + (((a)&128)>>7))

typedef struct
{
	byte *sequence;
	byte *p;
} cheatseq_t;

// ==========================================================================
//                             CHEAT Structures
// ==========================================================================

static byte cheat_god_seq[] =
{
	SCRAMBLE('h'), SCRAMBLE('e'), SCRAMBLE('l'), SCRAMBLE('e'), SCRAMBLE('n'), 0xff // HELEN - Johnny's love!
};

static unsigned char cheat_emerald_seq[] =
{
	SCRAMBLE('f'), SCRAMBLE('i'), SCRAMBLE('s'), SCRAMBLE('h'), SCRAMBLE('c'), SCRAMBLE('a'), SCRAMBLE('k'), SCRAMBLE('e'), 0xff
};

//
static byte cheat_devmode_seq[] =
{
	SCRAMBLE('d'), SCRAMBLE('e'), SCRAMBLE('v'), SCRAMBLE('m'), SCRAMBLE('o'), SCRAMBLE('d'), SCRAMBLE('e'), 0xff
};

// Now what?
static cheatseq_t cheat_god = { cheat_god_seq, 0 };
static cheatseq_t cheat_emerald = { cheat_emerald_seq, 0 };
static cheatseq_t cheat_devmode = { cheat_devmode_seq, 0 };

// ==========================================================================
//                        CHEAT SEQUENCE PACKAGE
// ==========================================================================

static byte cheat_xlate_table[256];

void cht_Init(void)
{
	size_t i = 0;
	byte pi = 0;
	for (; i < 256; i++, pi++)
	{
		const int cc = SCRAMBLE(pi);
		cheat_xlate_table[i] = (byte)cc;
	}
}

//
// Called in st_stuff module, which handles the input.
// Returns a 1 if the cheat was successful, 0 if failed.
//
static int cht_CheckCheat(cheatseq_t *cht, char key)
{
	int rc = 0;

	if (!cht->p)
		cht->p = cht->sequence; // initialize if first time

	if (*cht->p == 0)
		*(cht->p++) = key;
	else if (cheat_xlate_table[(byte)key] == *cht->p)
		cht->p++;
	else
		cht->p = cht->sequence;

	if (*cht->p == 1)
		cht->p++;
	else if (*cht->p == 0xff) // end of sequence character
	{
		cht->p = cht->sequence;
		rc = 1;
	}

	return rc;
}

static inline void cht_GetParam(cheatseq_t *cht, char *buffer)
{
	byte *p;
	byte c;

	p = cht->sequence;
	while (*(p++) != 1)
		;

	do
	{
		c = *p;
		*(buffer++) = c;
		*(p++) = 0;
	} while (c && *p != 0xff);

	if (*p == 0xff)
		*buffer = 0;
}

boolean cht_Responder(event_t *ev)
{
	static player_t *plyr;

	if (ev->type == ev_keydown)
	{
		plyr = &players[consoleplayer];

		// cheat for toggleable god mode
		if (cht_CheckCheat(&cheat_god, (char)ev->data1))
		{
			if (!modifiedgame || savemoddata)
			{
				modifiedgame = true;
				savemoddata = false;
				if (!(netgame || multiplayer))
					CONS_Printf(GAMEMODIFIED);
			}

			plyr->pflags ^= PF_GODMODE;
			if (plyr->pflags & PF_GODMODE)
				CONS_Printf("Smells bad!\n");
			else
				CONS_Printf("...smells REALLY bad!\n");
		}
		// devmode cheat
		else if (cht_CheckCheat(&cheat_devmode, (char)ev->data1))
		{
			Command_Devmode_f();
		}
		else if (cht_CheckCheat(&cheat_emerald, (char)ev->data1))
		{
			Command_Resetemeralds_f();
		}
	}
	return false;
}

// command that can be typed at the console!

void Command_CheatNoClip_f(void)
{
	player_t *plyr;
	if (multiplayer)
		return;

	plyr = &players[consoleplayer];
	plyr->pflags ^= PF_NOCLIP;
	CONS_Printf("No Clipping %s\n", plyr->pflags & PF_NOCLIP ? "On" : "Off");

	if (!modifiedgame || savemoddata)
	{
		modifiedgame = true;
		savemoddata = false;
		if (!(netgame || multiplayer))
			CONS_Printf(GAMEMODIFIED);
	}
}

void Command_CheatGod_f(void)
{
	player_t *plyr;

	if (multiplayer)
		return;

	plyr = &players[consoleplayer];
	plyr->pflags ^= PF_GODMODE;
	CONS_Printf("Sissy Mode %s\n", plyr->pflags & PF_GODMODE ? "On" : "Off");

	if (!modifiedgame || savemoddata)
	{
		modifiedgame = true;
		savemoddata = false;
		if (!(netgame || multiplayer))
			CONS_Printf(GAMEMODIFIED);
	}
}

void Command_Resetemeralds_f(void)
{
	if (netgame || multiplayer)
	{
		CONS_Printf(SINGLEPLAYERONLY);
		return;
	}

	emeralds = 0;

	CONS_Printf("Emeralds reset to zero.\n");
}

void Command_Devmode_f(void)
{
#ifndef CONSISTENCYTEST
	if (netgame || multiplayer)
		return;
#endif
	if (COM_Argc() > 1)
		cv_debug = atoi(COM_Argv(1));
	else if (!cv_debug)
		cv_debug = 1;
	else
		cv_debug = 0;

	if (!modifiedgame || savemoddata)
	{
		modifiedgame = true;
		savemoddata = false;
		if (!(netgame || multiplayer))
			CONS_Printf(GAMEMODIFIED);
	}
}
