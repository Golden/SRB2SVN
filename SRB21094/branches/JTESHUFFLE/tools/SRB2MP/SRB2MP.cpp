// SRB2MP.cpp : Defines the entry point for the application.
//

#include "stdafx.h"
#include "lump.h"

#define APPTITLE "SRB2 Music Player"
#define APPVERSION "v0.1"
#define APPAUTHOR "SSNTails"
#define APPCOMPANY "Sonic Team Junior"

typedef int boolean;
typedef unsigned char byte;

FSOUND_STREAM *fmus = NULL;
HWND g_hDlg;
HINSTANCE g_hInst;
char TempString[256];
char WADfilename[512];

void M_SetVolume(int volume)
{
	FSOUND_SetVolume(0, volume);
}

boolean M_InitMusic()
{
	if (FSOUND_GetVersion() < FMOD_VERSION)
	{
		// ("Error : You are using the wrong DLL version!\nYou should be using FMOD %.02f\n", FMOD_VERSION)
		return false;
	}

	if (!FSOUND_Init(44100, 1, FSOUND_INIT_GLOBALFOCUS))
	{
		// ("%s\n", FMOD_ErrorString(FSOUND_GetError()));
		return false;
	}

	M_SetVolume(128);
	return true;
}

void M_ShutdownMusic()
{
	FSOUND_Stream_Stop(fmus);
	FSOUND_Stream_Close(fmus);

	FSOUND_Close();
//	remove("tmp.egg"); // Delete the temp file
	return;
}

void M_PauseMusic()
{
	if(FSOUND_IsPlaying(0))
		FSOUND_SetPaused(0, true);

	return;
}

void M_ResumeMusic()
{
	if(FSOUND_GetPaused(0))
		FSOUND_SetPaused(0, false);
	return;
}

void M_StopMusic()
{
	if(FSOUND_IsPlaying(0))
		FSOUND_Stream_Stop(fmus);
}

void M_StartFMODSong (char* musicname, int looping)
{
	char filename[255];
//	int lumpnum;

	if(fmus != NULL)
	{
		FSOUND_Stream_Stop(fmus);
		FSOUND_Stream_Close(fmus);
	}

	// Create the filename we need
	sprintf(filename, "%s.ogg", musicname);

	if(looping)
		fmus = FSOUND_Stream_Open(filename, FSOUND_LOOP_NORMAL, 0, 0);
	else
		fmus = FSOUND_Stream_Open(filename, 0, 0, 0);

	if (!fmus)
	{
		MessageBox(g_hDlg, "Error", "Error", MB_OK|MB_APPLMODAL);
		// ("%s:\n%s", FMOD_ErrorString(FSOUND_GetError()), filename);
		return;
	}

	// Scan the OGG for the COMMENT= field for a custom loop point
	if(looping)
	{
		int scan;
		int size;
		FILE* data;
		byte* origpos;
		byte* dataum;
		char looplength[64];
		unsigned int loopstart = 0;
		int newcount = 0;
		const int freq = 44100;
		int length = FSOUND_Stream_GetLengthMs(fmus);
		char title[256];
		char artist[256];
		int titlecount = 0;
		int artistcount = 0;
		boolean titlefound = false;
		boolean artistfound = false;

		data = fopen(filename,"rb");

		if(data==NULL) // Sorry, file not found
		{
			// ("I_StartFMODSong: File not found: %s\n", filename);
			return;
		}

		fseek(data, 0, SEEK_END);
		size = ftell(data);
		fseek(data, 0, SEEK_SET);

		dataum = (byte*)malloc(size);

		if(dataum==NULL) // Out of memory
		{
			// ("I_StartFMODSong: Out of memory to load %s\n", filename);
			return;
		}
		fread( dataum, 1, size, data );
		fclose(data);

		origpos = dataum;

		for(scan = 0; scan < size; scan++)
		{
			if(!titlefound)
			{
				if(*dataum++ == 'T')
				if(*dataum++ == 'I')
				if(*dataum++ == 'T')
				if(*dataum++ == 'L')
				if(*dataum++ == 'E')
				if(*dataum++ == '=')
				{
					char buffer[512];
					char length = *(dataum-10) - 6;

					while(titlecount < length)
						title[titlecount++] = *dataum++;

					title[titlecount] = '\0';

					sprintf(buffer, "Title: %s", title);

					SendMessage(GetDlgItem(g_hDlg, IDC_TITLE), WM_SETTEXT, 0, (LPARAM)(LPCSTR)buffer);

					titlefound = true;
				}
			}
		}

		dataum = origpos;

		for(scan = 0; scan < size; scan++)
		{
			if(!artistfound)
			{
				if(*dataum++ == 'A')
				if(*dataum++ == 'R')
				if(*dataum++ == 'T')
				if(*dataum++ == 'I')
				if(*dataum++ == 'S')
				if(*dataum++ == 'T')
				if(*dataum++ == '=')
				{
					char buffer[512];
					char length = *(dataum-11) - 7;

					while(artistcount < length)
						artist[artistcount++] = *dataum++;

					artist[artistcount] = '\0';

					sprintf(buffer, "By: %s", artist);

					SendMessage(GetDlgItem(g_hDlg, IDC_ARTIST), WM_SETTEXT, 0, (LPARAM)(LPCSTR)buffer);

					artistfound = true;
				}
			}
		}

		dataum = origpos;

		for(scan = 0; scan < size; scan++)
		{
			if(*dataum++ == 'C')
			if(*dataum++ == 'O')
			if(*dataum++ == 'M')
			if(*dataum++ == 'M')
			if(*dataum++ == 'E')
			if(*dataum++ == 'N')
			if(*dataum++ == 'T')
			if(*dataum++ == '=')
			if(*dataum++ == 'L')
			if(*dataum++ == 'O')
			if(*dataum++ == 'O')
			if(*dataum++ == 'P')
			if(*dataum++ == 'P')
			if(*dataum++ == 'O')
			if(*dataum++ == 'I')
			if(*dataum++ == 'N')
			if(*dataum++ == 'T')
			if(*dataum++ == '=')
			{
				while (*dataum != 1 && newcount < 63)
				{
					looplength[newcount++] = *dataum++;
				}
	
				looplength[newcount] = '\n';
	
				loopstart = atoi(looplength);
			}
			else
				dataum--;
			else
				dataum--;
			else
				dataum--;
			else
				dataum--;
			else
				dataum--;
			else
				dataum--;
			else
				dataum--;
			else
				dataum--;
			else
				dataum--;
			else
				dataum--;
			else
				dataum--;
			else
				dataum--;
			else
				dataum--;
			else
				dataum--;
			else
				dataum--;
			else
				dataum--;
			else
				dataum--;
		}
		free(origpos);

		if (loopstart > 0)
		{
			const unsigned int loopend = (unsigned int)((freq/1000.0f)*length-(freq/1000.0f));
		//	const unsigned int loopend = (((freq/2)*length)/500)-8;
			if (!FSOUND_Stream_SetLoopPoints(fmus, loopstart, loopend));
	//			CONS_Printf("FMOD(Start,FSOUND_Stream_SetLoopPoints): %s\n",
	//				FMOD_ErrorString(FSOUND_GetError()));
		}
	}

	FSOUND_Stream_Play(0, fmus);
}

void ReadWADLumps(void)
{
	struct lumplist *startitem, *enditem = NULL;
	char *startname = NULL, *endname = NULL;
	HWND listbox;
	listbox = GetDlgItem(g_hDlg, IDC_PLAYLIST);
	FILE* f;
	struct wadfile* wfptr;

	SendMessage(listbox, LB_RESETCONTENT, 0, 0);
				
	f = fopen(WADfilename, "rb");
	wfptr = read_wadfile(f);
	fclose(f);

	/* Find startitem and enditem, using startname and endname */
	if(startname == NULL) startitem = wfptr->head;
	else {
		startitem = find_previous_lump(wfptr->head, NULL, startname);
		startitem = startitem->next;
		if(endname == NULL) {
			if(startitem->next != NULL) {
				char *itemname;

				enditem = startitem->next;
				itemname = get_lump_name(enditem->cl);
				while(  strcmp(itemname, "THINGS"  ) == 0 ||
						strcmp(itemname, "LINEDEFS") == 0 ||
						strcmp(itemname, "SIDEDEFS") == 0 ||
						strcmp(itemname, "VERTEXES") == 0 ||
						strcmp(itemname, "SEGS"    ) == 0 ||
						strcmp(itemname, "SSECTORS") == 0 ||
						strcmp(itemname, "NODES"   ) == 0 ||
						strcmp(itemname, "SECTORS" ) == 0 ||
						strcmp(itemname, "REJECT"  ) == 0 ||
						strcmp(itemname, "BLOCKMAP") == 0) {
					enditem = enditem->next;
					if(enditem == NULL) break;
					free(itemname);
					itemname = get_lump_name(enditem->cl);
				}
				free(itemname);
				if(enditem != NULL) enditem = enditem->next;
			} else enditem = NULL;
		} else {
			enditem = find_previous_lump(startitem, NULL, endname);
			enditem = enditem->next;
		}
	} /* end of finding startitem and enditem */

	/* start of C_LIST */
	{
		struct lumplist *curlump;
		int verbose = 0, i = 0;

		/* Loop through the lump list, printing lump info */
		for(curlump = startitem->next; curlump != enditem; curlump =
				curlump->next) {
			i++;

			if(!strncmp(get_lump_name(curlump->cl), "O_", 2))
				SendMessage(listbox, LB_ADDSTRING, 0, (LPARAM)(LPCSTR)get_lump_name(curlump->cl));
		}
	} /* end of C_LIST */
	free_wadfile(wfptr);
}

//
// OpenWadfile
//
// Provides a common dialog box
// for selecting the desired wad file.
//
void OpenWadfile(void)
{
	OPENFILENAME ofn;
	char FileBuffer[256];

	ZeroMemory(&ofn, sizeof(ofn));
	ofn.hwndOwner = NULL;
	FileBuffer[0] = '\0';
	ofn.lpstrFilter = "WAD Files\0*.wad\0All Files\0*.*\0\0";
	ofn.lpstrInitialDir = NULL;
	ofn.lpstrFile = FileBuffer;
	ofn.lStructSize = sizeof(ofn);
	ofn.nMaxFile = sizeof(FileBuffer);
	ofn.nFilterIndex = 1;
	ofn.lpstrFileTitle = NULL;
	ofn.nMaxFileTitle = 0;
	ofn.Flags = OFN_PATHMUSTEXIST | OFN_FILEMUSTEXIST | OFN_HIDEREADONLY;

	if(GetOpenFileName(&ofn))
	{
		strcpy(WADfilename, FileBuffer);
		ReadWADLumps();
	}
}

LRESULT CALLBACK MainProc(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	switch(message)
	{
		case WM_INITDIALOG:
			g_hDlg = hDlg;
			M_InitMusic();

			break;
	
		case WM_DESTROY:
		{
			PostQuitMessage(0);
			M_ShutdownMusic();
			break;
		}
			
		case WM_COMMAND:
		{
			switch(LOWORD(wParam))
			{
				case 2:
					PostMessage(hDlg, WM_DESTROY, 0, 0);
					break;
				case IDC_ABOUT: // The About button.
					sprintf(TempString, "%s %s by %s - %s", APPTITLE, APPVERSION, APPAUTHOR, APPCOMPANY);
					MessageBox(hDlg, TempString, "About", MB_OK|MB_APPLMODAL);
					break;
				case IDC_OPEN:
					OpenWadfile();
					break;
				case IDC_PLAY:
			{
				struct lumplist *startitem, *enditem = NULL;
				char *startname = NULL, *endname = NULL;
				HWND listbox;
				struct wadfile* wfptr;
				FILE* f;
				listbox = GetDlgItem(hDlg, IDC_PLAYLIST);
				
				f = fopen(WADfilename, "rb");
				wfptr = read_wadfile(f);
				fclose(f);

				/* Find startitem and enditem, using startname and endname */
				if(startname == NULL) startitem = wfptr->head;
				else {
					startitem = find_previous_lump(wfptr->head, NULL, startname);
					startitem = startitem->next;
					if(endname == NULL) {
						if(startitem->next != NULL) {
							char *itemname;

							enditem = startitem->next;
							itemname = get_lump_name(enditem->cl);
							while(  strcmp(itemname, "THINGS"  ) == 0 ||
									strcmp(itemname, "LINEDEFS") == 0 ||
									strcmp(itemname, "SIDEDEFS") == 0 ||
									strcmp(itemname, "VERTEXES") == 0 ||
									strcmp(itemname, "SEGS"    ) == 0 ||
									strcmp(itemname, "SSECTORS") == 0 ||
									strcmp(itemname, "NODES"   ) == 0 ||
									strcmp(itemname, "SECTORS" ) == 0 ||
									strcmp(itemname, "REJECT"  ) == 0 ||
									strcmp(itemname, "BLOCKMAP") == 0) {
								enditem = enditem->next;
								if(enditem == NULL) break;
								free(itemname);
								itemname = get_lump_name(enditem->cl);
							}
							free(itemname);
							if(enditem != NULL) enditem = enditem->next;
						} else enditem = NULL;
					} else {
						enditem = find_previous_lump(startitem, NULL, endname);
						enditem = enditem->next;
					}
				} /* end of finding startitem and enditem */


        /* Start of C_EXTRACT */{
            struct lumplist *extracted;
			char argv[9];

			HWND listbox;
			listbox = GetDlgItem(hDlg, IDC_PLAYLIST);

			int cursel = SendMessage(listbox, LB_GETCURSEL, 0, 0);
			SendMessage(listbox, LB_GETTEXT, cursel, (LPARAM)(LPCSTR)argv);

            /* Extract LUMPNAME FILENAME pairs */
            printf("Extracting lumps from %s...\n", argv);
             {

                /* Find the lump to extract */
                extracted = find_previous_lump(startitem, enditem, argv);
                extracted = extracted->next;

                /* Open the file to extract to */
                f = fopen("temp.ogg", "wb");

                /* Extract lump */
                if(fwrite(extracted->cl->data, extracted->cl->len, 1, f)
                        < 1) {
                    return EXIT_FAILURE;
                }

                /* Close the file */
                fclose(f);

            } /* end of extracting LUMPNAME FILENAME pairs */

			M_StartFMODSong("temp", true);
        } /* end of C_EXTRACT */
		free_wadfile(wfptr);

			}
					break;
			    default:
					break;
			}

			break;
		}
	}

	return 0;
}

void RegisterDialogClass(char* name, WNDPROC callback)
{
	WNDCLASS wnd;

	wnd.style			= CS_HREDRAW | CS_VREDRAW;
	wnd.cbWndExtra		= DLGWINDOWEXTRA;
	wnd.cbClsExtra		= 0;
	wnd.hCursor			= LoadCursor(NULL,MAKEINTRESOURCE(IDC_ARROW));
	wnd.hIcon			= LoadIcon(NULL,MAKEINTRESOURCE(IDI_ICON1));
	wnd.hInstance		= g_hInst;
	wnd.lpfnWndProc		= callback;
	wnd.lpszClassName	= name;
	wnd.lpszMenuName	= NULL;
	wnd.hbrBackground	= (HBRUSH)(COLOR_WINDOW);

	if(!RegisterClass(&wnd))
	{
		return;
	}
}

int APIENTRY WinMain(HINSTANCE hInstance,
                     HINSTANCE hPrevInstance,
                     LPSTR     lpCmdLine,
                     int       nCmdShow)
{
	// Prevent multiples instances of this app.
	CreateMutex(NULL, true, APPTITLE);

	if(GetLastError() == ERROR_ALREADY_EXISTS)
		return 0;

	g_hInst = hInstance;

	RegisterDialogClass("SRB2MP", MainProc);

	DialogBox(g_hInst, (LPCSTR)IDD_MAIN, NULL, (DLGPROC)MainProc);

	return 0;
}


