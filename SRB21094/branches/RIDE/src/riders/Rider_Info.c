#ifndef NORMALSRB2
// Riders Info
// Original by Jason the Echidna

#include "../doomstat.h"
#include "../d_player.h"
#include "../z_zone.h"
#include "../s_sound.h"

#ifdef HWRENDER
#include "../hardware/hw_light.h"
#endif

#include "Rider_Info.h"

void Rider_Init(void)
{
	size_t thing;

	//////////////
	////THINGS////
	//////////////

	thing = MT_SPEEDMONITOR;
	mobjinfo[thing].doomednum = 300;
	mobjinfo[thing].spawnstate = S_SPEEDMONITOR1;
	mobjinfo[thing].spawnhealth = 1000;
	mobjinfo[thing].reactiontime = 8;
	mobjinfo[thing].radius = 16*FRACUNIT;
	mobjinfo[thing].height = 40*FRACUNIT;
	mobjinfo[thing].mass = 100;
	mobjinfo[thing].flags = MF_NOCLIP|MF_SCENERY;

	thing = MT_FLYMONITOR;
	mobjinfo[thing].doomednum = 301;
	mobjinfo[thing].spawnstate = S_FLYMONITOR1;
	mobjinfo[thing].spawnhealth = 1000;
	mobjinfo[thing].reactiontime = 8;
	mobjinfo[thing].radius = 16*FRACUNIT;
	mobjinfo[thing].height = 40*FRACUNIT;
	mobjinfo[thing].mass = 100;
	mobjinfo[thing].flags = MF_NOCLIP|MF_SCENERY;

	thing = MT_POWERMONITOR;
	mobjinfo[thing].doomednum = 302;
	mobjinfo[thing].spawnstate = S_POWERMONITOR1;
	mobjinfo[thing].spawnhealth = 1000;
	mobjinfo[thing].reactiontime = 8;
	mobjinfo[thing].radius = 16*FRACUNIT;
	mobjinfo[thing].height = 40*FRACUNIT;
	mobjinfo[thing].mass = 100;
	mobjinfo[thing].flags = MF_NOCLIP|MF_SCENERY;

	thing = MT_FLAYM;
	mobjinfo[thing].doomednum = 303;
	mobjinfo[thing].spawnstate = S_FLAYM1;
	mobjinfo[thing].spawnhealth = 1000;
	mobjinfo[thing].reactiontime = 8;
	mobjinfo[thing].radius = 8*FRACUNIT;
	mobjinfo[thing].height = 32*FRACUNIT;
	mobjinfo[thing].mass = 100;
	mobjinfo[thing].flags = MF_NOGRAVITY|MF_SPECIAL|MF_FIRE;

	thing = MT_DEVIL;
	mobjinfo[thing].doomednum = 306;
	mobjinfo[thing].spawnstate = S_DEVIL;
	mobjinfo[thing].spawnhealth = 1000;
	mobjinfo[thing].reactiontime = 8;
	mobjinfo[thing].radius = 16*FRACUNIT;
	mobjinfo[thing].height = 40*FRACUNIT;
	mobjinfo[thing].mass = 100;
	mobjinfo[thing].flags = MF_SOLID;

	thing = MT_ANGEL;
	mobjinfo[thing].doomednum = 307;
	mobjinfo[thing].spawnstate = S_ANGEL;
	mobjinfo[thing].spawnhealth = 1000;
	mobjinfo[thing].reactiontime = 8;
	mobjinfo[thing].radius = 16*FRACUNIT;
	mobjinfo[thing].height = 40*FRACUNIT;
	mobjinfo[thing].mass = 100;
	mobjinfo[thing].flags = MF_SOLID;

	thing = MT_AIRTANKBOX;
	mobjinfo[thing].doomednum = 308;
	mobjinfo[thing].spawnstate = S_AIRTANKBOX;
	mobjinfo[thing].spawnhealth = 1;
	mobjinfo[thing].reactiontime = 8;
	mobjinfo[thing].painstate = S_AIRTANKBOX;
	mobjinfo[thing].deathstate = S_AIRTANKBOX2;
	mobjinfo[thing].deathsound = sfx_pop;
	mobjinfo[thing].speed = MT_CAPSULEEXPLOSION;
	mobjinfo[thing].radius = 16*FRACUNIT;
	mobjinfo[thing].height = 32*FRACUNIT;
	mobjinfo[thing].mass = 100;
	mobjinfo[thing].damage = MT_AIRICO;
	mobjinfo[thing].flags = MF_SOLID|MF_SHOOTABLE|MF_MONITOR;

	thing = MT_AIRICO;
	mobjinfo[thing].doomednum = -1;
	mobjinfo[thing].spawnstate = S_AIRTANKBOX2;
	mobjinfo[thing].spawnhealth = 1;
	mobjinfo[thing].seesound = sfx_wdjump;
	mobjinfo[thing].reactiontime = 10;
	mobjinfo[thing].speed = 2*FRACUNIT;
	mobjinfo[thing].radius = 8*FRACUNIT;
	mobjinfo[thing].height = 14*FRACUNIT;
	mobjinfo[thing].mass = 100;
	mobjinfo[thing].damage = 62*FRACUNIT;
	mobjinfo[thing].flags = MF_NOBLOCKMAP|MF_NOCLIP|MF_SCENERY|MF_NOGRAVITY|MF_MONITORICON;

	thing = MT_RSUPERRINGBOX;
	mobjinfo[thing].doomednum = 314;
	mobjinfo[thing].spawnstate = S_SUPERRINGBOX4;
	mobjinfo[thing].spawnhealth = 1;
	mobjinfo[thing].reactiontime = 8;
	mobjinfo[thing].painstate = S_SUPERRINGBOX4;
	mobjinfo[thing].deathstate = S_SUPERRINGBOX6;
	mobjinfo[thing].deathsound = sfx_pop;
	mobjinfo[thing].speed = MT_CAPSULEEXPLOSION;
	mobjinfo[thing].radius = 16*FRACUNIT;
	mobjinfo[thing].height = 32*FRACUNIT;
	mobjinfo[thing].mass = 100;
	mobjinfo[thing].damage = MT_RSUPERICO;
	mobjinfo[thing].flags = MF_SOLID|MF_SHOOTABLE|MF_MONITOR;

	thing = MT_RSUPERICO;
	mobjinfo[thing].doomednum = -1;
	mobjinfo[thing].spawnstate = S_SUPERRINGBOX6;
	mobjinfo[thing].spawnhealth = 1;
	mobjinfo[thing].seesound = sfx_itemup;
	mobjinfo[thing].reactiontime = 10;
	mobjinfo[thing].speed = 2*FRACUNIT;
	mobjinfo[thing].radius = 8*FRACUNIT;
	mobjinfo[thing].height = 14*FRACUNIT;
	mobjinfo[thing].mass = 100;
	mobjinfo[thing].damage = 62*FRACUNIT;
	mobjinfo[thing].flags = MF_NOBLOCKMAP|MF_NOCLIP|MF_SCENERY|MF_NOGRAVITY|MF_MONITORICON;

	thing = MT_RGREYRINGBOX;
	mobjinfo[thing].doomednum = 315;
	mobjinfo[thing].spawnstate = S_GREYRINGBOX4;
	mobjinfo[thing].spawnhealth = 1;
	mobjinfo[thing].reactiontime = 8;
	mobjinfo[thing].painstate = S_GREYRINGBOX4;
	mobjinfo[thing].deathstate = S_GREYRINGBOX6;
	mobjinfo[thing].deathsound = sfx_pop;
	mobjinfo[thing].speed = MT_CAPSULEEXPLOSION;
	mobjinfo[thing].radius = 16*FRACUNIT;
	mobjinfo[thing].height = 32*FRACUNIT;
	mobjinfo[thing].mass = 100;
	mobjinfo[thing].damage = MT_RGREYICO;
	mobjinfo[thing].flags = MF_SOLID|MF_SHOOTABLE|MF_MONITOR;

	thing = MT_RGREYICO;
	mobjinfo[thing].doomednum = -1;
	mobjinfo[thing].spawnstate = S_GREYRINGBOX6;
	mobjinfo[thing].spawnhealth = 1;
	mobjinfo[thing].seesound = sfx_itemup;
	mobjinfo[thing].reactiontime = 25;
	mobjinfo[thing].speed = 2*FRACUNIT;
	mobjinfo[thing].radius = 8*FRACUNIT;
	mobjinfo[thing].height = 14*FRACUNIT;
	mobjinfo[thing].mass = 100;
	mobjinfo[thing].damage = 62*FRACUNIT;
	mobjinfo[thing].flags = MF_NOBLOCKMAP|MF_NOCLIP|MF_SCENERY|MF_NOGRAVITY|MF_MONITORICON;

	thing = MT_RINV;
	mobjinfo[thing].doomednum = 305;
	mobjinfo[thing].spawnstate = S_PINV5;
	mobjinfo[thing].spawnhealth = 1;
	mobjinfo[thing].reactiontime = 8;
	mobjinfo[thing].painstate = S_PINV5;
	mobjinfo[thing].deathstate = S_PINV7;
	mobjinfo[thing].deathsound = sfx_pop;
	mobjinfo[thing].speed = MT_CAPSULEEXPLOSION;
	mobjinfo[thing].radius = 16*FRACUNIT;
	mobjinfo[thing].height = 32*FRACUNIT;
	mobjinfo[thing].mass = 100;
	mobjinfo[thing].damage = MT_RINVCICO;
	mobjinfo[thing].flags = MF_SOLID|MF_SHOOTABLE|MF_MONITOR;

	thing = MT_RINVCICO;
	mobjinfo[thing].doomednum = -1;
	mobjinfo[thing].spawnstate = S_PINV7;
	mobjinfo[thing].spawnhealth = 1;
	mobjinfo[thing].seesound = sfx_None;
	mobjinfo[thing].reactiontime = 25;
	mobjinfo[thing].speed = 2*FRACUNIT;
	mobjinfo[thing].radius = 8*FRACUNIT;
	mobjinfo[thing].height = 14*FRACUNIT;
	mobjinfo[thing].mass = 100;
	mobjinfo[thing].damage = 62*FRACUNIT;
	mobjinfo[thing].flags = MF_NOBLOCKMAP|MF_NOCLIP|MF_SCENERY|MF_NOGRAVITY|MF_MONITORICON;

	thing = MT_YELLOWCAPSULE;
	mobjinfo[thing].doomednum = 317;
	mobjinfo[thing].spawnstate = S_SHBX;
	mobjinfo[thing].spawnhealth = 1;
	mobjinfo[thing].reactiontime = 8;
	mobjinfo[thing].painstate = S_SHBX;
	mobjinfo[thing].deathstate = S_SHBX2;
	mobjinfo[thing].deathsound = sfx_pop;
	mobjinfo[thing].speed = MT_CAPSULEEXPLOSION;
	mobjinfo[thing].radius = 16*FRACUNIT;
	mobjinfo[thing].height = 32*FRACUNIT;
	mobjinfo[thing].mass = 100;
	mobjinfo[thing].damage = MT_YSHIELDCAPICO;
	mobjinfo[thing].flags = MF_SOLID|MF_SHOOTABLE|MF_MONITOR;

	thing = MT_YSHIELDCAPICO;
	mobjinfo[thing].doomednum = -1;
	mobjinfo[thing].spawnstate = S_SHBX2;
	mobjinfo[thing].spawnhealth = 1;
	mobjinfo[thing].seesound = sfx_shield;
	mobjinfo[thing].reactiontime = 25;
	mobjinfo[thing].speed = 2*FRACUNIT;
	mobjinfo[thing].radius = 8*FRACUNIT;
	mobjinfo[thing].height = 14*FRACUNIT;
	mobjinfo[thing].mass = 100;
	mobjinfo[thing].damage = 62*FRACUNIT;
	mobjinfo[thing].flags = MF_NOBLOCKMAP|MF_NOCLIP|MF_SCENERY|MF_NOGRAVITY|MF_MONITORICON;

	thing = MT_REDDIAG2;
	mobjinfo[thing].doomednum = 304;
	mobjinfo[thing].spawnstate = S_RDIAG1;
	mobjinfo[thing].spawnhealth = 1;
	mobjinfo[thing].seestate = S_RDIAG2;
	mobjinfo[thing].reactiontime = 8;
	mobjinfo[thing].painsound = sfx_spring;
	mobjinfo[thing].speed = 24*FRACUNIT;
	mobjinfo[thing].radius = 16*FRACUNIT;
	mobjinfo[thing].height = 16*FRACUNIT;
	mobjinfo[thing].mass = 100;
	mobjinfo[thing].damage = 48*FRACUNIT;
	mobjinfo[thing].flags = MF_SOLID|MF_SPRING;

	thing = MT_REDDIAG3;
	mobjinfo[thing].doomednum = 309;
	mobjinfo[thing].spawnstate = S_RDIAG1;
	mobjinfo[thing].spawnhealth = 1;
	mobjinfo[thing].seestate = S_RDIAG2;
	mobjinfo[thing].reactiontime = 8;
	mobjinfo[thing].painsound = sfx_spring;
	mobjinfo[thing].speed = 20*FRACUNIT;
	mobjinfo[thing].radius = 16*FRACUNIT;
	mobjinfo[thing].height = 16*FRACUNIT;
	mobjinfo[thing].mass = 100;
	mobjinfo[thing].damage = 16*FRACUNIT;
	mobjinfo[thing].flags = MF_SOLID|MF_SPRING;

	thing = MT_REDDIAG4;
	mobjinfo[thing].doomednum = 310;
	mobjinfo[thing].spawnstate = S_RDIAG1;
	mobjinfo[thing].spawnhealth = 1;
	mobjinfo[thing].seestate = S_RDIAG2;
	mobjinfo[thing].reactiontime = 8;
	mobjinfo[thing].painsound = sfx_spring;
	mobjinfo[thing].speed = 24*FRACUNIT;
	mobjinfo[thing].radius = 16*FRACUNIT;
	mobjinfo[thing].height = 16*FRACUNIT;
	mobjinfo[thing].mass = 100;
	mobjinfo[thing].damage = 32*FRACUNIT;
	mobjinfo[thing].flags = MF_SOLID|MF_SPRING;

	thing = MT_QUESTIONBOX2;
	mobjinfo[thing].doomednum = 311;
	mobjinfo[thing].spawnstate = S_RANDOMITEM1;
	mobjinfo[thing].spawnhealth = 1;
	mobjinfo[thing].reactiontime = 8;
	mobjinfo[thing].painstate = S_RANDOMITEM1;
	mobjinfo[thing].deathstate = S_RANDOMITEM3;
	mobjinfo[thing].deathsound = sfx_pop;
	mobjinfo[thing].speed = MT_ITEMEXPLOSION;
	mobjinfo[thing].radius = 16*FRACUNIT;
	mobjinfo[thing].height = 32*FRACUNIT;
	mobjinfo[thing].mass = 100;
	mobjinfo[thing].damage = 0;
	mobjinfo[thing].flags = MF_SOLID|MF_SHOOTABLE|MF_MONITOR;

	thing = MT_QUESTIONBOX3;
	mobjinfo[thing].doomednum = 316;
	mobjinfo[thing].spawnstate = S_RANDOMCAPSULE1;
	mobjinfo[thing].spawnhealth = 1;
	mobjinfo[thing].reactiontime = 8;
	mobjinfo[thing].painstate = S_RANDOMCAPSULE1;
	mobjinfo[thing].deathstate = S_RANDOMCAPSULE3;
	mobjinfo[thing].deathsound = sfx_pop;
	mobjinfo[thing].speed = MT_CAPSULEEXPLOSION;
	mobjinfo[thing].radius = 16*FRACUNIT;
	mobjinfo[thing].height = 32*FRACUNIT;
	mobjinfo[thing].mass = 100;
	mobjinfo[thing].damage = 0;
	mobjinfo[thing].flags = MF_SOLID|MF_SHOOTABLE|MF_MONITOR;

	thing = MT_CAPSULEEXPLOSION;
	mobjinfo[thing].doomednum = -1;
	mobjinfo[thing].spawnstate = S_MONITOREXPLOSION6;
	mobjinfo[thing].spawnhealth = 1;
	mobjinfo[thing].reactiontime = 8;
	mobjinfo[thing].painstate = S_MONITOREXPLOSION6;
	mobjinfo[thing].radius = 16*FRACUNIT;
	mobjinfo[thing].height = 32*FRACUNIT;
	mobjinfo[thing].mass = 100;
	mobjinfo[thing].damage = 0;
	mobjinfo[thing].flags = MF_NOCLIP;

	thing = MT_ITEMEXPLOSION;
	mobjinfo[thing].doomednum = -1;
	mobjinfo[thing].spawnstate = S_ITEMEXPLOSION1;
	mobjinfo[thing].spawnhealth = 1;
	mobjinfo[thing].reactiontime = 8;
	mobjinfo[thing].painstate = S_ITEMEXPLOSION1;
	mobjinfo[thing].radius = 16*FRACUNIT;
	mobjinfo[thing].height = 32*FRACUNIT;
	mobjinfo[thing].mass = 100;
	mobjinfo[thing].damage = 0;
	mobjinfo[thing].flags = MF_NOCLIP;

	thing = MT_MKSTAR;
	mobjinfo[thing].doomednum = -1;
	mobjinfo[thing].spawnstate = S_MKSTAR;
	mobjinfo[thing].spawnhealth = 1;
	mobjinfo[thing].reactiontime = 8;
	mobjinfo[thing].painstate = S_MKSTAR;
	mobjinfo[thing].deathstate = S_MKSTAR3;
	mobjinfo[thing].deathsound = sfx_pop;
	mobjinfo[thing].speed = MT_ITEMEXPLOSION;
	mobjinfo[thing].radius = 16*FRACUNIT;
	mobjinfo[thing].height = 32*FRACUNIT;
	mobjinfo[thing].mass = 100;
	mobjinfo[thing].damage = MT_MKSTRICO;
	mobjinfo[thing].flags = MF_SOLID|MF_SHOOTABLE|MF_MONITOR;

	thing = MT_MKSTRICO;
	mobjinfo[thing].doomednum = -1;
	mobjinfo[thing].spawnstate = S_MKSTAR3;
	mobjinfo[thing].spawnhealth = 1;
	mobjinfo[thing].reactiontime = 8;
	mobjinfo[thing].speed = 2*FRACUNIT;
	mobjinfo[thing].radius = 8*FRACUNIT;
	mobjinfo[thing].height = 14*FRACUNIT;
	mobjinfo[thing].mass = 100;
	mobjinfo[thing].damage = 62*FRACUNIT;
	mobjinfo[thing].flags = MF_NOBLOCKMAP|MF_NOCLIP|MF_SCENERY|MF_NOGRAVITY|MF_MONITORICON;

	thing = MT_MUSHROOM;
	mobjinfo[thing].doomednum = -1;
	mobjinfo[thing].spawnstate = S_SHTV4;
	mobjinfo[thing].spawnhealth = 1;
	mobjinfo[thing].reactiontime = 8;
	mobjinfo[thing].painstate = S_SHTV4;
	mobjinfo[thing].deathstate = S_SHTV6;
	mobjinfo[thing].deathsound = sfx_pop;
	mobjinfo[thing].speed = MT_ITEMEXPLOSION;
	mobjinfo[thing].radius = 16*FRACUNIT;
	mobjinfo[thing].height = 32*FRACUNIT;
	mobjinfo[thing].mass = 100;
	mobjinfo[thing].damage = MT_MUSHICO;
	mobjinfo[thing].flags = MF_SOLID|MF_SHOOTABLE|MF_MONITOR;

	thing = MT_MUSHICO;
	mobjinfo[thing].doomednum = -1;
	mobjinfo[thing].spawnstate = S_SHTV6;
	mobjinfo[thing].spawnhealth = 1;
	mobjinfo[thing].reactiontime = 8;
	mobjinfo[thing].speed = 2*FRACUNIT;
	mobjinfo[thing].radius = 8*FRACUNIT;
	mobjinfo[thing].height = 14*FRACUNIT;
	mobjinfo[thing].mass = 100;
	mobjinfo[thing].damage = 62*FRACUNIT;
	mobjinfo[thing].flags = MF_NOBLOCKMAP|MF_NOCLIP|MF_SCENERY|MF_NOGRAVITY|MF_MONITORICON;

	thing = MT_SHELLBOX;
	mobjinfo[thing].doomednum = -1;
	mobjinfo[thing].spawnstate = S_SHELLBOX;
	mobjinfo[thing].spawnhealth = 1;
	mobjinfo[thing].reactiontime = 8;
	mobjinfo[thing].painstate = S_SHELLBOX;
	mobjinfo[thing].deathstate = S_SHELLBOX2;
	mobjinfo[thing].deathsound = sfx_pop;
	mobjinfo[thing].speed = MT_ITEMEXPLOSION;
	mobjinfo[thing].radius = 16*FRACUNIT;
	mobjinfo[thing].height = 32*FRACUNIT;
	mobjinfo[thing].mass = 100;
	mobjinfo[thing].damage = MT_SHLLICO;
	mobjinfo[thing].flags = MF_SOLID|MF_SHOOTABLE|MF_MONITOR;

	thing = MT_SHLLICO;
	mobjinfo[thing].doomednum = -1;
	mobjinfo[thing].spawnstate = S_SHELLBOX2;
	mobjinfo[thing].spawnhealth = 1;
	mobjinfo[thing].reactiontime = 8;
	mobjinfo[thing].speed = 2*FRACUNIT;
	mobjinfo[thing].radius = 8*FRACUNIT;
	mobjinfo[thing].height = 14*FRACUNIT;
	mobjinfo[thing].mass = 100;
	mobjinfo[thing].damage = 62*FRACUNIT;
	mobjinfo[thing].flags = MF_NOBLOCKMAP|MF_NOCLIP|MF_SCENERY|MF_NOGRAVITY|MF_MONITORICON;

	thing = MT_REDSHELLBOX;
	mobjinfo[thing].doomednum = -1;
	mobjinfo[thing].spawnstate = S_REDSHELLBOX;
	mobjinfo[thing].spawnhealth = 1;
	mobjinfo[thing].reactiontime = 8;
	mobjinfo[thing].painstate = S_REDSHELLBOX;
	mobjinfo[thing].deathstate = S_REDSHELLBOX2;
	mobjinfo[thing].deathsound = sfx_pop;
	mobjinfo[thing].speed = MT_ITEMEXPLOSION;
	mobjinfo[thing].radius = 16*FRACUNIT;
	mobjinfo[thing].height = 32*FRACUNIT;
	mobjinfo[thing].mass = 100;
	mobjinfo[thing].damage = MT_REDSHLLICO;
	mobjinfo[thing].flags = MF_SOLID|MF_SHOOTABLE|MF_MONITOR;

	thing = MT_REDSHLLICO;
	mobjinfo[thing].doomednum = -1;
	mobjinfo[thing].spawnstate = S_REDSHELLBOX2;
	mobjinfo[thing].spawnhealth = 1;
	mobjinfo[thing].reactiontime = 8;
	mobjinfo[thing].speed = 2*FRACUNIT;
	mobjinfo[thing].radius = 8*FRACUNIT;
	mobjinfo[thing].height = 14*FRACUNIT;
	mobjinfo[thing].mass = 100;
	mobjinfo[thing].damage = 62*FRACUNIT;
	mobjinfo[thing].flags = MF_NOBLOCKMAP|MF_NOCLIP|MF_SCENERY|MF_NOGRAVITY|MF_MONITORICON;

	thing = MT_BANANABOX;
	mobjinfo[thing].doomednum = -1;
	mobjinfo[thing].spawnstate = S_BANANABOX;
	mobjinfo[thing].spawnhealth = 1;
	mobjinfo[thing].reactiontime = 8;
	mobjinfo[thing].painstate = S_BANANABOX;
	mobjinfo[thing].deathstate = S_BANANABOX2;
	mobjinfo[thing].deathsound = sfx_pop;
	mobjinfo[thing].speed = MT_ITEMEXPLOSION;
	mobjinfo[thing].radius = 16*FRACUNIT;
	mobjinfo[thing].height = 32*FRACUNIT;
	mobjinfo[thing].mass = 100;
	mobjinfo[thing].damage = MT_NANAICO;
	mobjinfo[thing].flags = MF_SOLID|MF_SHOOTABLE|MF_MONITOR;

	thing = MT_NANAICO;
	mobjinfo[thing].doomednum = -1;
	mobjinfo[thing].spawnstate = S_BANANABOX2;
	mobjinfo[thing].spawnhealth = 1;
	mobjinfo[thing].reactiontime = 8;
	mobjinfo[thing].speed = 2*FRACUNIT;
	mobjinfo[thing].radius = 8*FRACUNIT;
	mobjinfo[thing].height = 14*FRACUNIT;
	mobjinfo[thing].mass = 100;
	mobjinfo[thing].damage = 62*FRACUNIT;
	mobjinfo[thing].flags = MF_NOBLOCKMAP|MF_NOCLIP|MF_SCENERY|MF_NOGRAVITY|MF_MONITORICON;

	thing = MT_PEEL;
	mobjinfo[thing].doomednum = 312;
	mobjinfo[thing].spawnstate = S_PEEL;
	mobjinfo[thing].spawnhealth = 1;
	mobjinfo[thing].reactiontime = 8;
	mobjinfo[thing].deathstate = S_PEEL;
	mobjinfo[thing].radius = 16*FRACUNIT;
	mobjinfo[thing].height = 32*FRACUNIT;
	mobjinfo[thing].mass = 100;
	mobjinfo[thing].damage = 1;
	mobjinfo[thing].flags = MF_SPECIAL|MF_SHOOTABLE|MF_MISSILE;

	thing = MT_FAKEBOX;
	mobjinfo[thing].doomednum = -1;
	mobjinfo[thing].spawnstate = S_FAKEBOX;
	mobjinfo[thing].spawnhealth = 1;
	mobjinfo[thing].reactiontime = 8;
	mobjinfo[thing].painstate = S_FAKEBOX;
	mobjinfo[thing].deathstate = S_FAKEBOX2;
	mobjinfo[thing].deathsound = sfx_pop;
	mobjinfo[thing].speed = MT_ITEMEXPLOSION;
	mobjinfo[thing].radius = 16*FRACUNIT;
	mobjinfo[thing].height = 32*FRACUNIT;
	mobjinfo[thing].mass = 100;
	mobjinfo[thing].damage = MT_FAKEICO;
	mobjinfo[thing].flags = MF_SOLID|MF_SHOOTABLE|MF_MONITOR;

	thing = MT_FAKEICO;
	mobjinfo[thing].doomednum = -1;
	mobjinfo[thing].spawnstate = S_FAKEBOX2;
	mobjinfo[thing].spawnhealth = 1;
	mobjinfo[thing].reactiontime = 8;
	mobjinfo[thing].speed = 2*FRACUNIT;
	mobjinfo[thing].radius = 8*FRACUNIT;
	mobjinfo[thing].height = 14*FRACUNIT;
	mobjinfo[thing].mass = 100;
	mobjinfo[thing].damage = 62*FRACUNIT;
	mobjinfo[thing].flags = MF_NOBLOCKMAP|MF_NOCLIP|MF_SCENERY|MF_NOGRAVITY|MF_MONITORICON;

	thing = MT_FAKE;
	mobjinfo[thing].doomednum = 313;
	mobjinfo[thing].spawnstate = S_FAKE;
	mobjinfo[thing].spawnhealth = 1;
	mobjinfo[thing].reactiontime = 8;
	mobjinfo[thing].deathstate = S_FAKE;
	mobjinfo[thing].radius = 16*FRACUNIT;
	mobjinfo[thing].height = 32*FRACUNIT;
	mobjinfo[thing].mass = 100;
	mobjinfo[thing].damage = 1;
	mobjinfo[thing].flags = MF_SPECIAL|MF_SHOOTABLE|MF_MISSILE;

	thing = MT_BOOMBOX;
	mobjinfo[thing].doomednum = -1;
	mobjinfo[thing].spawnstate = S_BOOMBOX;
	mobjinfo[thing].spawnhealth = 1;
	mobjinfo[thing].reactiontime = 8;
	mobjinfo[thing].painstate = S_BOOMBOX;
	mobjinfo[thing].deathstate = S_BOOMBOX2;
	mobjinfo[thing].deathsound = sfx_pop;
	mobjinfo[thing].speed = MT_ITEMEXPLOSION;
	mobjinfo[thing].radius = 16*FRACUNIT;
	mobjinfo[thing].height = 32*FRACUNIT;
	mobjinfo[thing].mass = 100;
	mobjinfo[thing].damage = MT_BOOMICO;
	mobjinfo[thing].flags = MF_SOLID|MF_SHOOTABLE|MF_MONITOR;

	thing = MT_BOOMICO;
	mobjinfo[thing].doomednum = -1;
	mobjinfo[thing].spawnstate = S_BOOMBOX2;
	mobjinfo[thing].spawnhealth = 1;
	mobjinfo[thing].reactiontime = 8;
	mobjinfo[thing].speed = 2*FRACUNIT;
	mobjinfo[thing].radius = 8*FRACUNIT;
	mobjinfo[thing].height = 14*FRACUNIT;
	mobjinfo[thing].mass = 100;
	mobjinfo[thing].damage = 62*FRACUNIT;
	mobjinfo[thing].flags = MF_NOBLOCKMAP|MF_NOCLIP|MF_SCENERY|MF_NOGRAVITY|MF_MONITORICON;

	thing = MT_BUNNIE;
	mobjinfo[thing].doomednum = -1;
	mobjinfo[thing].spawnstate = S_BUNNIE1;
	mobjinfo[thing].spawnhealth = 1000;
	mobjinfo[thing].seestate = S_BUNNIE1;
	mobjinfo[thing].reactiontime = 8;
	mobjinfo[thing].speed = 15*FRACUNIT;
	mobjinfo[thing].radius = 16*FRACUNIT;
	mobjinfo[thing].height = 16*FRACUNIT;
	mobjinfo[thing].mass = 100;
	mobjinfo[thing].damage = 62*FRACUNIT;
	mobjinfo[thing].flags = MF_NOCLIPTHING;

	thing = MT_MONKEY;
	mobjinfo[thing].doomednum = -1;
	mobjinfo[thing].spawnstate = S_MONKEY1;
	mobjinfo[thing].spawnhealth = 1000;
	mobjinfo[thing].reactiontime = 8;
	mobjinfo[thing].speed = 15*FRACUNIT;
	mobjinfo[thing].radius = 16*FRACUNIT;
	mobjinfo[thing].height = 16*FRACUNIT;
	mobjinfo[thing].mass = 100;
	mobjinfo[thing].damage = 62*FRACUNIT;
	mobjinfo[thing].flags = MF_NOCLIPTHING;

	thing = MT_EAGLE;
	mobjinfo[thing].doomednum = -1;
	mobjinfo[thing].spawnstate = S_EAGLE1;
	mobjinfo[thing].spawnhealth = 1000;
	mobjinfo[thing].seestate = S_EAGLE1;
	mobjinfo[thing].reactiontime = 8;
	mobjinfo[thing].speed = 8;
	mobjinfo[thing].radius = 16*FRACUNIT;
	mobjinfo[thing].height = 16*FRACUNIT;
	mobjinfo[thing].mass = 16;
	mobjinfo[thing].damage = 62*FRACUNIT;
	mobjinfo[thing].flags = MF_NOCLIPTHING|MF_FLOAT|MF_NOGRAVITY;

	thing = MT_CHICKEN;
	mobjinfo[thing].doomednum = -1;
	mobjinfo[thing].spawnstate = S_CHICKEN1;
	mobjinfo[thing].spawnhealth = 1000;
	mobjinfo[thing].seestate = S_CHICKEN1;
	mobjinfo[thing].reactiontime = 8;
	mobjinfo[thing].speed = 8;
	mobjinfo[thing].radius = 16*FRACUNIT;
	mobjinfo[thing].height = 16*FRACUNIT;
	mobjinfo[thing].mass = 16;
	mobjinfo[thing].flags = MF_NOCLIPTHING;

	thing = MT_PIGGIE;
	mobjinfo[thing].doomednum = -1;
	mobjinfo[thing].spawnstate = S_PIGGIE1;
	mobjinfo[thing].spawnhealth = 1000;
	mobjinfo[thing].seestate = S_PIGGIE1;
	mobjinfo[thing].reactiontime = 8;
	mobjinfo[thing].speed = 5*FRACUNIT;
	mobjinfo[thing].radius = 16*FRACUNIT;
	mobjinfo[thing].height = 16*FRACUNIT;
	mobjinfo[thing].mass = 16;
	mobjinfo[thing].flags = MF_NOCLIPTHING;

	thing = MT_SEEL;
	mobjinfo[thing].doomednum = -1;
	mobjinfo[thing].spawnstate = S_SEEL1;
	mobjinfo[thing].spawnhealth = 1000;
	mobjinfo[thing].seestate = S_SEEL1;
	mobjinfo[thing].reactiontime = 8;
	mobjinfo[thing].speed = 5*FRACUNIT;
	mobjinfo[thing].radius = 16*FRACUNIT;
	mobjinfo[thing].height = 16*FRACUNIT;
	mobjinfo[thing].mass = 16;
	mobjinfo[thing].flags = MF_NOCLIPTHING;

	thing = MT_PENGUIN;
	mobjinfo[thing].doomednum = -1;
	mobjinfo[thing].spawnstate = S_PENGUIN1;
	mobjinfo[thing].spawnhealth = 1000;
	mobjinfo[thing].seestate = S_PENGUIN1;
	mobjinfo[thing].reactiontime = 8;
	mobjinfo[thing].speed = 8;
	mobjinfo[thing].radius = 16*FRACUNIT;
	mobjinfo[thing].height = 16*FRACUNIT;
	mobjinfo[thing].mass = 16;
	mobjinfo[thing].flags = MF_NOCLIPTHING;

	thing = MT_TEDDY;
	mobjinfo[thing].doomednum = -1;
	mobjinfo[thing].spawnstate = S_TEDDY1;
	mobjinfo[thing].spawnhealth = 1000;
	mobjinfo[thing].seestate = S_TEDDY1;
	mobjinfo[thing].reactiontime = 8;
	mobjinfo[thing].speed = 5*FRACUNIT;
	mobjinfo[thing].radius = 16*FRACUNIT;
	mobjinfo[thing].height = 16*FRACUNIT;
	mobjinfo[thing].mass = 16;
	mobjinfo[thing].flags = MF_NOCLIPTHING;

	thing = MT_TURTLE;
	mobjinfo[thing].doomednum = -1;
	mobjinfo[thing].spawnstate = S_TURTLE1;
	mobjinfo[thing].spawnhealth = 1;
	mobjinfo[thing].reactiontime = 8;
	mobjinfo[thing].speed = 2*FRACUNIT;
	mobjinfo[thing].radius = 4*FRACUNIT;
	mobjinfo[thing].height = 4*FRACUNIT;
	mobjinfo[thing].mass = 100;
	mobjinfo[thing].flags = MF_NOCLIPTHING;

	thing = MT_FLAG;
	mobjinfo[thing].doomednum = 318;
	mobjinfo[thing].spawnstate = S_FLAG;
	mobjinfo[thing].spawnhealth = 1000;
	mobjinfo[thing].reactiontime = 8;
	mobjinfo[thing].radius = 16*FRACUNIT;
	mobjinfo[thing].height = 40*FRACUNIT;
	mobjinfo[thing].mass = 100;
	mobjinfo[thing].flags = MF_NOCLIP|MF_SCENERY;

	thing = MT_PALMTREE;
	mobjinfo[thing].doomednum = 319;
	mobjinfo[thing].spawnstate = S_PALMTREE;
	mobjinfo[thing].spawnhealth = 1000;
	mobjinfo[thing].reactiontime = 8;
	mobjinfo[thing].radius = 16*FRACUNIT;
	mobjinfo[thing].height = 40*FRACUNIT;
	mobjinfo[thing].mass = 100;
	mobjinfo[thing].flags = MF_NOCLIP|MF_SCENERY;

	thing = MT_CHEESE;
	mobjinfo[thing].doomednum = -1;
	mobjinfo[thing].spawnstate = S_CHEESE1;
	mobjinfo[thing].spawnhealth = 1000;
	mobjinfo[thing].seestate = S_CHEESE7;
	mobjinfo[thing].reactiontime = 8;
	mobjinfo[thing].radius = 64*FRACUNIT;
	mobjinfo[thing].height = 64*FRACUNIT;
	mobjinfo[thing].mass = 16;
	mobjinfo[thing].flags = MF_NOBLOCKMAP|MF_NOCLIP|MF_NOGRAVITY|MF_SCENERY;

	thing = MT_CHIP;
	mobjinfo[thing].doomednum = -1;
	mobjinfo[thing].spawnstate = S_CHIP1;
	mobjinfo[thing].spawnhealth = 1000;
	mobjinfo[thing].seestate = S_CHIP7;
	mobjinfo[thing].reactiontime = 8;
	mobjinfo[thing].radius = 64*FRACUNIT;
	mobjinfo[thing].height = 64*FRACUNIT;
	mobjinfo[thing].mass = 16;
	mobjinfo[thing].flags = MF_NOBLOCKMAP|MF_NOCLIP|MF_NOGRAVITY|MF_SCENERY;

	thing = MT_BIRDIE;
	mobjinfo[thing].doomednum = -1;
	mobjinfo[thing].spawnstate = S_BIRDIE1;
	mobjinfo[thing].spawnhealth = 1000;
	mobjinfo[thing].seestate = S_BIRDIE9;
	mobjinfo[thing].reactiontime = 8;
	mobjinfo[thing].radius = 64*FRACUNIT;
	mobjinfo[thing].height = 64*FRACUNIT;
	mobjinfo[thing].mass = 16;
	mobjinfo[thing].flags = MF_NOBLOCKMAP|MF_NOCLIP|MF_NOGRAVITY|MF_SCENERY;

	thing = MT_HEDGEHOG;
	mobjinfo[thing].doomednum = 320;
	mobjinfo[thing].spawnstate = S_HEDGEHOG;
	mobjinfo[thing].spawnhealth = 1000;
	mobjinfo[thing].reactiontime = 8;
	mobjinfo[thing].radius = 64*FRACUNIT;
	mobjinfo[thing].height = 64*FRACUNIT;
	mobjinfo[thing].mass = 16;
	mobjinfo[thing].flags = MF_NOBLOCKMAP|MF_NOCLIP|MF_NOGRAVITY|MF_SCENERY;

	thing = MT_REDSHELL;
	mobjinfo[thing].doomednum = -1;
	mobjinfo[thing].spawnstate = S_RSHELL1;
	mobjinfo[thing].spawnhealth = 1000;
	mobjinfo[thing].reactiontime = 8;
	mobjinfo[thing].speed = 90*FRACUNIT;
	mobjinfo[thing].radius = 8*FRACUNIT;
	mobjinfo[thing].height = 16*FRACUNIT;
	mobjinfo[thing].mass = 100;
	mobjinfo[thing].damage = 1;
	mobjinfo[thing].flags = MF_NOBLOCKMAP|MF_TRANSLATION;

	thing = MT_SIDESPRING;
	mobjinfo[thing].doomednum = 321;
	mobjinfo[thing].spawnstate = S_SIDESPRING1;
	mobjinfo[thing].spawnhealth = 1000;
	mobjinfo[thing].seestate = S_SIDESPRING2;
	mobjinfo[thing].reactiontime = 8;
	mobjinfo[thing].painsound = sfx_spring;
	mobjinfo[thing].speed = 0;
	mobjinfo[thing].radius = 20*FRACUNIT;
	mobjinfo[thing].height = 16*FRACUNIT;
	mobjinfo[thing].mass = 100;
	mobjinfo[thing].damage = 20*FRACUNIT;
	mobjinfo[thing].flags = MF_SOLID|MF_NOGRAVITY|MF_SPRING;

	thing = MT_TALLBUSH;
	mobjinfo[thing].doomednum = 322;
	mobjinfo[thing].spawnstate = S_BUSH1;
	mobjinfo[thing].spawnhealth = 1000;
	mobjinfo[thing].reactiontime = 8;
	mobjinfo[thing].speed = 0;
	mobjinfo[thing].radius = 16*FRACUNIT;
	mobjinfo[thing].height = 32*FRACUNIT;
	mobjinfo[thing].mass = 100;
	mobjinfo[thing].damage = 0;
	mobjinfo[thing].flags = MF_NOCLIP|MF_SCENERY;

	thing = MT_WAYPOINT;
	mobjinfo[thing].doomednum = 323;
	mobjinfo[thing].spawnhealth = 1000;
	mobjinfo[thing].reactiontime = 0;
	mobjinfo[thing].speed = 0;
	mobjinfo[thing].radius = 1*FRACUNIT;
	mobjinfo[thing].height = 2*FRACUNIT;
	mobjinfo[thing].mass = 100;
	mobjinfo[thing].damage = 0;
	mobjinfo[thing].flags = MF_NOBLOCKMAP|MF_NOSECTOR|MF_NOCLIP|MF_NOGRAVITY;

	thing = MT_TWEE;
	mobjinfo[thing].doomednum = 328;
	mobjinfo[thing].spawnstate = S_TWEE;
	mobjinfo[thing].spawnhealth = 1000;
	mobjinfo[thing].reactiontime = 8;
	mobjinfo[thing].radius = 16*FRACUNIT;
	mobjinfo[thing].height = 40*FRACUNIT;
	mobjinfo[thing].mass = 100;
	mobjinfo[thing].flags = MF_NOCLIP|MF_SCENERY;

	thing = MT_HYDRANT;
	mobjinfo[thing].doomednum = 329;
	mobjinfo[thing].spawnstate = S_HYDRANT;
	mobjinfo[thing].spawnhealth = 1000;
	mobjinfo[thing].reactiontime = 8;
	mobjinfo[thing].radius = 16*FRACUNIT;
	mobjinfo[thing].height = 40*FRACUNIT;
	mobjinfo[thing].mass = 100;
	mobjinfo[thing].flags = MF_SCENERY;

	thing = MT_BOO;
	mobjinfo[thing].doomednum = 330;
	mobjinfo[thing].spawnstate = S_BOO1;
	mobjinfo[thing].spawnhealth = 1000;
	mobjinfo[thing].reactiontime = 8;
	mobjinfo[thing].radius = 16*FRACUNIT;
	mobjinfo[thing].height = 40*FRACUNIT;
	mobjinfo[thing].mass = 100;
	mobjinfo[thing].flags = MF_NOBLOCKMAP|MF_NOCLIP|MF_SCENERY;

	thing = MT_GMBA;
	mobjinfo[thing].doomednum = 331;
	mobjinfo[thing].spawnstate = S_GMBA1;
	mobjinfo[thing].spawnhealth = 1000;
	mobjinfo[thing].reactiontime = 8;
	mobjinfo[thing].radius = 16*FRACUNIT;
	mobjinfo[thing].height = 40*FRACUNIT;
	mobjinfo[thing].mass = 100;
	mobjinfo[thing].flags = MF_NOBLOCKMAP|MF_NOCLIP|MF_SCENERY;

	thing = MT_SHYG;
	mobjinfo[thing].doomednum = 332;
	mobjinfo[thing].spawnstate = S_SHYG1;
	mobjinfo[thing].spawnhealth = 1000;
	mobjinfo[thing].reactiontime = 8;
	mobjinfo[thing].radius = 16*FRACUNIT;
	mobjinfo[thing].height = 40*FRACUNIT;
	mobjinfo[thing].mass = 100;
	mobjinfo[thing].flags = MF_NOBLOCKMAP|MF_NOCLIP|MF_SCENERY;

	thing = MT_SNIF;
	mobjinfo[thing].doomednum = 333;
	mobjinfo[thing].spawnstate = S_SNIF1;
	mobjinfo[thing].spawnhealth = 1000;
	mobjinfo[thing].reactiontime = 8;
	mobjinfo[thing].radius = 16*FRACUNIT;
	mobjinfo[thing].height = 40*FRACUNIT;
	mobjinfo[thing].mass = 100;
	mobjinfo[thing].flags = MF_NOBLOCKMAP|MF_NOCLIP|MF_SCENERY;

	thing = MT_BOB_BOMB;
	mobjinfo[thing].doomednum = -1;
	mobjinfo[thing].spawnstate = S_THROWNEXPLOSION8;
	mobjinfo[thing].spawnhealth = 1000;
	mobjinfo[thing].reactiontime = 8;
	mobjinfo[thing].painchance = 128*FRACUNIT;
	mobjinfo[thing].deathstate = S_NIGHTSEXPLOSION;
	mobjinfo[thing].speed = 0;
	mobjinfo[thing].radius = 16*FRACUNIT;
	mobjinfo[thing].height = 24*FRACUNIT;
	mobjinfo[thing].mass = 7*TICRATE;
	mobjinfo[thing].damage = 1;
	mobjinfo[thing].activesound = sfx_bomb;
	mobjinfo[thing].flags = MF_SLIDEME|MF_FLOAT|MF_NOCLIPTHING|MF_MISSILE|MF_SHOOTABLE;

	thing = MT_EXPLOSION;
	mobjinfo[thing].doomednum = -1;
	mobjinfo[thing].spawnstate = S_XPLD1;
	mobjinfo[thing].spawnhealth = 1000;
	mobjinfo[thing].reactiontime = 8;
	mobjinfo[thing].speed = 0;
	mobjinfo[thing].radius = 2*FRACUNIT;
	mobjinfo[thing].height = 4*FRACUNIT;
	mobjinfo[thing].mass = 100;
	mobjinfo[thing].damage = 1;
	mobjinfo[thing].flags = MF_SPECIAL|MF_NOGRAVITY|MF_SCENERY;

	thing = MT_GROUND;
	mobjinfo[thing].doomednum = 324;
	mobjinfo[thing].spawnstate = S_GROUND1;
	mobjinfo[thing].spawnhealth = 1000;
	mobjinfo[thing].reactiontime = 8;
	mobjinfo[thing].speed = 0;
	mobjinfo[thing].radius = 16*FRACUNIT;
	mobjinfo[thing].height = 32*FRACUNIT;
	mobjinfo[thing].mass = 100;
	mobjinfo[thing].damage = 0;
	mobjinfo[thing].flags = MF_NOCLIP|MF_SCENERY;

	thing = MT_CHRISPY;
	mobjinfo[thing].doomednum = 325;
	mobjinfo[thing].spawnstate = S_CHRISPY1;
	mobjinfo[thing].spawnhealth = 1000;
	mobjinfo[thing].reactiontime = 8;
	mobjinfo[thing].speed = 0;
	mobjinfo[thing].radius = 16*FRACUNIT;
	mobjinfo[thing].height = 32*FRACUNIT;
	mobjinfo[thing].mass = 100;
	mobjinfo[thing].damage = 0;
	mobjinfo[thing].flags = MF_NOCLIP|MF_SCENERY;

	thing = MT_SENKU;
	mobjinfo[thing].doomednum = 326;
	mobjinfo[thing].spawnstate = S_SENKU1;
	mobjinfo[thing].spawnhealth = 1000;
	mobjinfo[thing].reactiontime = 8;
	mobjinfo[thing].speed = 0;
	mobjinfo[thing].radius = 16*FRACUNIT;
	mobjinfo[thing].height = 32*FRACUNIT;
	mobjinfo[thing].mass = 100;
	mobjinfo[thing].damage = 0;
	mobjinfo[thing].flags = MF_NOCLIP|MF_SCENERY;

	thing = MT_FLAMETH;
	mobjinfo[thing].doomednum = 327;
	mobjinfo[thing].spawnstate = S_FLAMETH1;
	mobjinfo[thing].spawnhealth = 1000;
	mobjinfo[thing].reactiontime = 8;
	mobjinfo[thing].speed = 0;
	mobjinfo[thing].radius = 16*FRACUNIT;
	mobjinfo[thing].height = 32*FRACUNIT;
	mobjinfo[thing].mass = 100;
	mobjinfo[thing].damage = 0;
	mobjinfo[thing].flags = MF_NOCLIP|MF_SCENERY;

	//////////////
	////FRAMES////
	//////////////

	// Actual Player frames. The one on info.c are the old ones.
	thing = S_PLAY_STND;
	states[thing].sprite = SPR_RIDR;
	states[thing].frame = 0;
	states[thing].tics = 1;
	states[thing].nextstate = S_PLAY_STND;

	thing = S_PLAY_RUN1;
	states[thing].sprite = SPR_RIDR;
	states[thing].frame = 1;
	states[thing].tics = 4;
	states[thing].nextstate = S_PLAY_RUN2;

	thing = S_PLAY_RUN2;
	states[thing].sprite = SPR_RIDR;
	states[thing].frame = 2;
	states[thing].tics = 4;
	states[thing].nextstate = S_PLAY_RUN3;

	thing = S_PLAY_RUN3;
	states[thing].sprite = SPR_RIDR;
	states[thing].frame = 3;
	states[thing].tics = 4;
	states[thing].nextstate = S_PLAY_RUN4;

	thing = S_PLAY_RUN4;
	states[thing].sprite = SPR_RIDR;
	states[thing].frame = 4;
	states[thing].tics = 4;
	states[thing].nextstate = S_PLAY_RUN5;

	thing = S_PLAY_RUN5;
	states[thing].sprite = SPR_RIDR;
	states[thing].frame = 5;
	states[thing].tics = 4;
	states[thing].nextstate = S_PLAY_RUN6;

	thing = S_PLAY_RUN6;
	states[thing].sprite = SPR_RIDR;
	states[thing].frame = 6;
	states[thing].tics = 4;
	states[thing].nextstate = S_PLAY_RUN7;

	thing = S_PLAY_RUN7;
	states[thing].sprite = SPR_RIDR;
	states[thing].frame = 7;
	states[thing].tics = 4;
	states[thing].nextstate = S_PLAY_RUN8;

	thing = S_PLAY_RUN8;
	states[thing].sprite = SPR_RIDR;
	states[thing].frame = 8;
	states[thing].tics = 4;
	states[thing].nextstate = S_PLAY_RUN1;

	thing = S_PLAY_SPD1;
	states[thing].sprite = SPR_RIDR;
	states[thing].frame = 9;
	states[thing].tics = 4;
	states[thing].nextstate = S_PLAY_SPD2;

	thing = S_PLAY_SPD2;
	states[thing].sprite = SPR_RIDR;
	states[thing].frame = 10;
	states[thing].tics = 4;
	states[thing].nextstate = S_PLAY_SPD3;

	thing = S_PLAY_SPD3;
	states[thing].sprite = SPR_RIDR;
	states[thing].frame = 11;
	states[thing].tics = 4;
	states[thing].nextstate = S_PLAY_SPD4;

	thing = S_PLAY_SPD4;
	states[thing].sprite = SPR_RIDR;
	states[thing].frame = 12;
	states[thing].tics = 4;
	states[thing].nextstate = S_PLAY_SPD1;

	thing = S_PLAY_ATK1;
	states[thing].sprite = SPR_RIDR;
	states[thing].frame = 13;
	states[thing].tics = 2;
	states[thing].nextstate = S_PLAY_ATK2;

	thing = S_PLAY_ATK2;
	states[thing].sprite = SPR_RIDR;
	states[thing].frame = 14;
	states[thing].tics = 2;
	states[thing].nextstate = S_PLAY_ATK3;

	thing = S_PLAY_ATK3;
	states[thing].sprite = SPR_RIDR;
	states[thing].frame = 15;
	states[thing].tics = 2;
	states[thing].nextstate = S_PLAY_ATK4;

	thing = S_PLAY_ATK4;
	states[thing].sprite = SPR_RIDR;
	states[thing].frame = 16;
	states[thing].tics = 2;
	states[thing].nextstate = S_PLAY_ATK1;

	thing = S_PLAY_JUMP;
	states[thing].sprite = SPR_RIDR;
	states[thing].frame = 17;
	states[thing].tics = -1;
	states[thing].nextstate = S_PLAY_RUN1;

	thing = S_PLAY_PLG1;
	states[thing].sprite = SPR_RIDR;
	states[thing].frame = 18;
	states[thing].tics = -1;
	states[thing].nextstate = S_NULL;

	thing = S_PLAY_ABL1;
	states[thing].sprite = SPR_RIDR;
	states[thing].frame = 19;
	states[thing].tics = 2;
	states[thing].nextstate = S_PLAY_ABL2;

	thing = S_PLAY_ABL2;
	states[thing].sprite = SPR_RIDR;
	states[thing].frame = 20;
	states[thing].tics = 2;
	states[thing].nextstate = S_PLAY_ABL3;

	thing = S_PLAY_ABL3;
	states[thing].sprite = SPR_RIDR;
	states[thing].frame = 21;
	states[thing].tics = 2;
	states[thing].nextstate = S_PLAY_ABL4;

	thing = S_PLAY_ABL4;
	states[thing].sprite = SPR_RIDR;
	states[thing].frame = 22;
	states[thing].tics = 2;
	states[thing].nextstate = S_PLAY_ABL5;

	thing = S_PLAY_ABL5;
	states[thing].sprite = SPR_RIDR;
	states[thing].frame = 23;
	states[thing].tics = 2;
	states[thing].nextstate = S_PLAY_ABL6;

	thing = S_PLAY_ABL6;
	states[thing].sprite = SPR_RIDR;
	states[thing].frame = 24;
	states[thing].tics = 2;
	states[thing].nextstate = S_PLAY_ABL7;

	thing = S_PLAY_ABL7;
	states[thing].sprite = SPR_RIDR;
	states[thing].frame = 25;
	states[thing].tics = 2;
	states[thing].nextstate = S_PLAY_ABL8;

	thing = S_PLAY_ABL8;
	states[thing].sprite = SPR_RIDR;
	states[thing].frame = 26;
	states[thing].tics = 2;
	states[thing].nextstate = S_PLAY_ABL1;

	thing = S_PLAY_PAIN;
	states[thing].sprite = SPR_RIDR;
	states[thing].frame = 27;
	states[thing].tics = -1;
	states[thing].nextstate = S_PLAY_STND;

	thing = S_PLAY_DIE1;
	states[thing].sprite = SPR_RIDR;
	states[thing].frame = 27;
	states[thing].tics = 8;
	states[thing].action.acp1 = (actionf_p1)A_Fall;
	states[thing].nextstate = S_PLAY_DIE2;

	thing = S_PLAY_DIE2;
	states[thing].sprite = SPR_RIDR;
	states[thing].frame = 27;
	states[thing].tics = 7;
	states[thing].nextstate = S_PLAY_DIE3;

	thing = S_PLAY_DIE3;
	states[thing].sprite = SPR_RIDR;
	states[thing].frame = 27;
	states[thing].tics = -1;
	states[thing].nextstate = S_NULL;

	thing = S_PLAY_SUPERRUN;
	states[thing].sprite = SPR_RIDR;
	states[thing].frame = 28;
	states[thing].tics = -1;
	states[thing].nextstate = S_NULL;

	thing = S_PLAY_SUPERFLY1;
	states[thing].sprite = SPR_RIDR;
	states[thing].frame = 29;
	states[thing].tics = 7;
	states[thing].nextstate = S_PLAY_SUPERFLY2;

	thing = S_PLAY_SUPERFLY2;
	states[thing].sprite = SPR_RIDR;
	states[thing].frame = 29;
	states[thing].tics = 7;
	states[thing].nextstate = S_PLAY_SUPERFLY1;

	thing = S_PLAY_SUPERJUMP;
	states[thing].sprite = SPR_RIDR;
	states[thing].frame = 28;
	states[thing].tics = -1;
	states[thing].nextstate = S_NULL;

	thing = S_PLAY_SUPERSPRING;
	states[thing].sprite = SPR_RIDR;
	states[thing].frame = 28;
	states[thing].tics = -1;
	states[thing].nextstate = S_NULL;

	thing = S_PLAY_KARTSTND1;
	states[thing].sprite = SPR_RIDR;
	states[thing].frame = 30;
	states[thing].tics = 2;
	states[thing].nextstate = S_PLAY_KARTSTND2;

	thing = S_PLAY_KARTSTND2;
	states[thing].sprite = SPR_RIDR;
	states[thing].frame = 31;
	states[thing].tics = 2;
	states[thing].nextstate = S_PLAY_KARTSTND1;

	thing = S_PLAY_KARTWALK1;
	states[thing].sprite = SPR_RIDR;
	states[thing].frame = 32;
	states[thing].tics = 4;
	states[thing].nextstate = S_PLAY_KARTWALK2;

	thing = S_PLAY_KARTWALK2;
	states[thing].sprite = SPR_RIDR;
	states[thing].frame = 33;
	states[thing].tics = 4;
	states[thing].nextstate = S_PLAY_KARTWALK3;

	thing = S_PLAY_KARTWALK3;
	states[thing].sprite = SPR_RIDR;
	states[thing].frame = 34;
	states[thing].tics = 4;
	states[thing].nextstate = S_PLAY_KARTWALK4;

	thing = S_PLAY_KARTWALK4;
	states[thing].sprite = SPR_RIDR;
	states[thing].frame = 35;
	states[thing].tics = 4;
	states[thing].nextstate = S_PLAY_KARTWALK1;

	thing = S_PLAY_KARTRUN1;
	states[thing].sprite = SPR_RIDR;
	states[thing].frame = 33;
	states[thing].tics = 2;
	states[thing].nextstate = S_PLAY_KARTRUN2;

	thing = S_PLAY_KARTRUN2;
	states[thing].sprite = SPR_RIDR;
	states[thing].frame = 35;
	states[thing].tics = 2;
	states[thing].nextstate = S_PLAY_KARTRUN1;

	thing = S_PLAY_KARTATK;
	states[thing].sprite = SPR_RIDR;
	states[thing].frame = 30;
	states[thing].tics = -1;
	states[thing].nextstate = S_NULL;

	thing = S_PLAY_KARTPLG1;
	states[thing].sprite = SPR_RIDR;
	states[thing].frame = 30;
	states[thing].tics = -1;
	states[thing].nextstate = S_NULL;

	thing = S_PLAY_KARTPAIN;
	states[thing].sprite = SPR_RIDR;
	states[thing].frame = 36;
	states[thing].tics = -1;
	states[thing].nextstate = S_PLAY_KARTSTND1;

	thing = S_PLAY_KARTDIE1;
	states[thing].sprite = SPR_RIDR;
	states[thing].frame = 36;
	states[thing].tics = 8;
	states[thing].action.acp1 = (actionf_p1)A_Fall;
	states[thing].nextstate = S_PLAY_KARTDIE2;

	thing = S_PLAY_KARTDIE2;
	states[thing].sprite = SPR_RIDR;
	states[thing].frame = 36;
	states[thing].tics = 7;
	states[thing].nextstate = S_PLAY_KARTDIE3;

	thing = S_PLAY_KARTDIE3;
	states[thing].sprite = SPR_RIDR;
	states[thing].frame = 36;
	states[thing].tics = -1;
	states[thing].nextstate = S_NULL;

	thing = S_PLAY_RSTND;
	states[thing].sprite = SPR_RIDR;
	states[thing].frame = 37;
	states[thing].tics = 1;
	states[thing].nextstate = S_PLAY_RSTND;

	thing = S_PLAY_RRUN1;
	states[thing].sprite = SPR_RIDR;
	states[thing].frame = 38;
	states[thing].tics = 4;
	states[thing].nextstate = S_PLAY_RRUN2;

	thing = S_PLAY_RRUN2;
	states[thing].sprite = SPR_RIDR;
	states[thing].frame = 39;
	states[thing].tics = 4;
	states[thing].nextstate = S_PLAY_RRUN3;

	thing = S_PLAY_RRUN3;
	states[thing].sprite = SPR_RIDR;
	states[thing].frame = 40;
	states[thing].tics = 4;
	states[thing].nextstate = S_PLAY_RRUN4;

	thing = S_PLAY_RRUN4;
	states[thing].sprite = SPR_RIDR;
	states[thing].frame = 41;
	states[thing].tics = 4;
	states[thing].nextstate = S_PLAY_RRUN5;

	thing = S_PLAY_RRUN5;
	states[thing].sprite = SPR_RIDR;
	states[thing].frame = 42;
	states[thing].tics = 4;
	states[thing].nextstate = S_PLAY_RRUN6;

	thing = S_PLAY_RRUN6;
	states[thing].sprite = SPR_RIDR;
	states[thing].frame = 43;
	states[thing].tics = 4;
	states[thing].nextstate = S_PLAY_RRUN7;

	thing = S_PLAY_RRUN7;
	states[thing].sprite = SPR_RIDR;
	states[thing].frame = 44;
	states[thing].tics = 4;
	states[thing].nextstate = S_PLAY_RRUN8;

	thing = S_PLAY_RRUN8;
	states[thing].sprite = SPR_RIDR;
	states[thing].frame = 45;
	states[thing].tics = 4;
	states[thing].nextstate = S_PLAY_RRUN1;

	thing = S_PLAY_RSPD1;
	states[thing].sprite = SPR_RIDR;
	states[thing].frame = 46;
	states[thing].tics = 2;
	states[thing].nextstate = S_PLAY_RSPD2;

	thing = S_PLAY_RSPD2;
	states[thing].sprite = SPR_RIDR;
	states[thing].frame = 47;
	states[thing].tics = 2;
	states[thing].nextstate = S_PLAY_RSPD3;

	thing = S_PLAY_RSPD3;
	states[thing].sprite = SPR_RIDR;
	states[thing].frame = 48;
	states[thing].tics = 2;
	states[thing].nextstate = S_PLAY_RSPD4;

	thing = S_PLAY_RSPD4;
	states[thing].sprite = SPR_RIDR;
	states[thing].frame = 49;
	states[thing].tics = 2;
	states[thing].nextstate = S_PLAY_RSPD1;

	thing = S_PLAY_SKATE1;
	states[thing].sprite = SPR_RIDR;
	states[thing].frame = 46;
	states[thing].tics = 4;
	states[thing].nextstate = S_PLAY_SKATE2;

	thing = S_PLAY_SKATE2;
	states[thing].sprite = SPR_RIDR;
	states[thing].frame = 47;
	states[thing].tics = 4;
	states[thing].nextstate = S_PLAY_SKATE3;

	thing = S_PLAY_SKATE3;
	states[thing].sprite = SPR_RIDR;
	states[thing].frame = 48;
	states[thing].tics = 4;
	states[thing].nextstate = S_PLAY_SKATE4;

	thing = S_PLAY_SKATE4;
	states[thing].sprite = SPR_RIDR;
	states[thing].frame = 49;
	states[thing].tics = 4;
	states[thing].nextstate = S_PLAY_SKATE5;

	thing = S_PLAY_SKATE5;
	states[thing].sprite = SPR_RIDR;
	states[thing].frame = 55;
	states[thing].tics = 4;
	states[thing].nextstate = S_PLAY_SKATE6;

	thing = S_PLAY_SKATE6;
	states[thing].sprite = SPR_RIDR;
	states[thing].frame = 56;
	states[thing].tics = 4;
	states[thing].nextstate = S_PLAY_SKATE7;

	thing = S_PLAY_SKATE7;
	states[thing].sprite = SPR_RIDR;
	states[thing].frame = 57;
	states[thing].tics = 4;
	states[thing].nextstate = S_PLAY_SKATE8;

	thing = S_PLAY_SKATE8;
	states[thing].sprite = SPR_RIDR;
	states[thing].frame = 58;
	states[thing].tics = 4;
	states[thing].nextstate = S_PLAY_SKATE9;

	thing = S_PLAY_SKATE9;
	states[thing].sprite = SPR_RIDR;
	states[thing].frame = 59;
	states[thing].tics = 4;
	states[thing].nextstate = S_PLAY_SKATE10;

	thing = S_PLAY_SKATE10;
	states[thing].sprite = SPR_RIDR;
	states[thing].frame = 60;
	states[thing].tics = 4;
	states[thing].nextstate = S_PLAY_SKATE1;

	thing = S_PLAY_CRUN1;
	states[thing].sprite = SPR_RIDR;
	states[thing].frame = 46;
	states[thing].tics = 4;
	states[thing].nextstate = S_PLAY_CRUN2;

	thing = S_PLAY_CRUN2;
	states[thing].sprite = SPR_RIDR;
	states[thing].frame = 47;
	states[thing].tics = 4;
	states[thing].nextstate = S_PLAY_CRUN3;

	thing = S_PLAY_CRUN3;
	states[thing].sprite = SPR_RIDR;
	states[thing].frame = 48;
	states[thing].tics = 4;
	states[thing].nextstate = S_PLAY_CRUN4;

	thing = S_PLAY_CRUN4;
	states[thing].sprite = SPR_RIDR;
	states[thing].frame = 49;
	states[thing].tics = 4;
	states[thing].nextstate = S_PLAY_CRUN5;

	thing = S_PLAY_CRUN5;
	states[thing].sprite = SPR_RIDR;
	states[thing].frame = 55;
	states[thing].tics = 4;
	states[thing].nextstate = S_PLAY_CRUN6;

	thing = S_PLAY_CRUN6;
	states[thing].sprite = SPR_RIDR;
	states[thing].frame = 56;
	states[thing].tics = 4;
	states[thing].nextstate = S_PLAY_CRUN7;

	thing = S_PLAY_CRUN7;
	states[thing].sprite = SPR_RIDR;
	states[thing].frame = 57;
	states[thing].tics = 4;
	states[thing].nextstate = S_PLAY_CRUN8;

	thing = S_PLAY_CRUN8;
	states[thing].sprite = SPR_RIDR;
	states[thing].frame = 58;
	states[thing].tics = 4;
	states[thing].nextstate = S_PLAY_CRUN1;

	thing = S_PLAY_RATK1;
	states[thing].sprite = SPR_RIDR;
	states[thing].frame = 50;
	states[thing].tics = 4;
	states[thing].nextstate = S_PLAY_RATK2;

	thing = S_PLAY_RATK2;
	states[thing].sprite = SPR_RIDR;
	states[thing].frame = 51;
	states[thing].tics = 4;
	states[thing].nextstate = S_PLAY_RATK3;

	thing = S_PLAY_RATK3;
	states[thing].sprite = SPR_RIDR;
	states[thing].frame = 52;
	states[thing].tics = 4;
	states[thing].nextstate = S_PLAY_RATK4;

	thing = S_PLAY_RATK4;
	states[thing].sprite = SPR_RIDR;
	states[thing].frame = 53;
	states[thing].tics = 4;
	states[thing].nextstate = S_PLAY_RATK1;

	thing = S_PLAY_BOUNCE;
	states[thing].sprite = SPR_RIDR;
	states[thing].frame = 50;
	states[thing].tics = -1;
	states[thing].nextstate = S_NULL;

	thing = S_PLAY_FALL;
	states[thing].sprite = SPR_RIDR;
	states[thing].frame = 51;
	states[thing].tics = -1;
	states[thing].nextstate = S_NULL;

	thing = S_PLAY_SLIDE;
	states[thing].sprite = SPR_RIDR;
	states[thing].frame = 52;
	states[thing].tics = 1;
	states[thing].nextstate = S_PLAY_SLIDE;

	thing = S_PLAY_RPLG1;
	states[thing].sprite = SPR_RIDR;
	states[thing].frame = 54;
	states[thing].tics = -1;
	states[thing].nextstate = S_NULL;

	thing = S_PLAY_SILV1;
	states[thing].sprite = SPR_RIDR;
	states[thing].frame = 32823;
	states[thing].tics = -1;
	states[thing].nextstate = S_NULL;

	thing = S_PLAY_SILV2;
	states[thing].sprite = SPR_RIDR;
	states[thing].frame = 32824;
	states[thing].tics = -1;
	states[thing].nextstate = S_NULL;

	thing = S_PLAY_RABL1;
	states[thing].sprite = SPR_RIDR;
	states[thing].frame = 55;
	states[thing].tics = 1;
	states[thing].nextstate = S_PLAY_RABL2;

	thing = S_PLAY_RABL2;
	states[thing].sprite = SPR_RIDR;
	states[thing].frame = 56;
	states[thing].tics = 1;
	states[thing].nextstate = S_PLAY_RABL1;

	thing = S_PLAY_CLIMB1;
	states[thing].sprite = SPR_RIDR;
	states[thing].frame = 57;
	states[thing].tics = -1;
	states[thing].nextstate = S_NULL;

	thing = S_PLAY_CLIMB2;
	states[thing].sprite = SPR_RIDR;
	states[thing].frame = 58;
	states[thing].tics = 5;
	states[thing].nextstate = S_PLAY_CLIMB3;

	thing = S_PLAY_CLIMB3;
	states[thing].sprite = SPR_RIDR;
	states[thing].frame = 59;
	states[thing].tics = 5;
	states[thing].nextstate = S_PLAY_CLIMB4;

	thing = S_PLAY_CLIMB4;
	states[thing].sprite = SPR_RIDR;
	states[thing].frame = 60;
	states[thing].tics = 5;
	states[thing].nextstate = S_PLAY_CLIMB5;

	thing = S_PLAY_CLIMB5;
	states[thing].sprite = SPR_RIDR;
	states[thing].frame = 59;
	states[thing].tics = 5;
	states[thing].nextstate = S_PLAY_CLIMB2;

	thing = S_PLAY_FLY1;
	states[thing].sprite = SPR_RIDR;
	states[thing].frame = 55;
	states[thing].tics = 1;
	states[thing].nextstate = S_PLAY_FLY2;

	thing = S_PLAY_FLY2;
	states[thing].sprite = SPR_RIDR;
	states[thing].frame = 56;
	states[thing].tics = 1;
	states[thing].nextstate = S_PLAY_FLY3;

	thing = S_PLAY_FLY3;
	states[thing].sprite = SPR_RIDR;
	states[thing].frame = 57;
	states[thing].tics = 1;
	states[thing].nextstate = S_PLAY_FLY4;

	thing = S_PLAY_FLY4;
	states[thing].sprite = SPR_RIDR;
	states[thing].frame = 58;
	states[thing].tics = 1;
	states[thing].nextstate = S_PLAY_FLY1;

	thing = S_PLAY_CFLY1;
	states[thing].sprite = SPR_RIDR;
	states[thing].frame = 46;
	states[thing].tics = 2;
	states[thing].nextstate = S_PLAY_CFLY2;

	thing = S_PLAY_CFLY2;
	states[thing].sprite = SPR_RIDR;
	states[thing].frame = 47;
	states[thing].tics = 2;
	states[thing].nextstate = S_PLAY_CFLY3;

	thing = S_PLAY_CFLY3;
	states[thing].sprite = SPR_RIDR;
	states[thing].frame = 48;
	states[thing].tics = 2;
	states[thing].nextstate = S_PLAY_CFLY4;

	thing = S_PLAY_CFLY4;
	states[thing].sprite = SPR_RIDR;
	states[thing].frame = 49;
	states[thing].tics = 2;
	states[thing].nextstate = S_PLAY_CFLY5;

	thing = S_PLAY_CFLY5;
	states[thing].sprite = SPR_RIDR;
	states[thing].frame = 55;
	states[thing].tics = 2;
	states[thing].nextstate = S_PLAY_CFLY6;

	thing = S_PLAY_CFLY6;
	states[thing].sprite = SPR_RIDR;
	states[thing].frame = 56;
	states[thing].tics = 2;
	states[thing].nextstate = S_PLAY_CFLY7;

	thing = S_PLAY_CFLY7;
	states[thing].sprite = SPR_RIDR;
	states[thing].frame = 57;
	states[thing].tics = 2;
	states[thing].nextstate = S_PLAY_CFLY8;

	thing = S_PLAY_CFLY8;
	states[thing].sprite = SPR_RIDR;
	states[thing].frame = 58;
	states[thing].tics = 2;
	states[thing].nextstate = S_PLAY_CFLY1;

	thing = S_PLAY_RSUPERSTAND;
	states[thing].sprite = SPR_RIDR;
	states[thing].frame = 55;
	states[thing].tics = -1;
	states[thing].nextstate = S_NULL;

	thing = S_PLAY_RSUPERWALK;
	states[thing].sprite = SPR_RIDR;
	states[thing].frame = 55;
	states[thing].tics = -1;
	states[thing].nextstate = S_NULL;

	thing = S_PLAY_RSUPERRUN;
	states[thing].sprite = SPR_RIDR;
	states[thing].frame = 56;
	states[thing].tics = -1;
	states[thing].nextstate = S_NULL;

	thing = S_PLAY_RFLOAT;
	states[thing].sprite = SPR_RIDR;
	states[thing].frame = 61;
	states[thing].tics = -1;
	states[thing].nextstate = S_PLAY_RSTND;

	thing = S_PLAY_RPAIN;
	states[thing].sprite = SPR_RIDR;
	states[thing].frame = 61;
	states[thing].tics = -1;
	states[thing].nextstate = S_PLAY_RSTND;

	thing = S_PLAY_RDIE1;
	states[thing].sprite = SPR_RIDR;
	states[thing].frame = 61;
	states[thing].tics = 8;
	states[thing].action.acp1 = (actionf_p1)A_Fall;
	states[thing].nextstate = S_PLAY_RDIE2;

	thing = S_PLAY_RDIE2;
	states[thing].sprite = SPR_RIDR;
	states[thing].frame = 61;
	states[thing].tics = 7;
	states[thing].nextstate = S_PLAY_RDIE3;

	thing = S_PLAY_RDIE3;
	states[thing].sprite = SPR_RIDR;
	states[thing].frame = 61;
	states[thing].tics = -1;
	states[thing].nextstate = S_NULL;

	thing = S_PLAY_DRONE1;
	states[thing].sprite = SPR_RIDR;
	states[thing].frame = 62;
	states[thing].tics = -1;
	states[thing].nextstate = S_PLAY_DRONE2;

	thing = S_PLAY_DRONE2;
	states[thing].sprite = SPR_RIDR;
	states[thing].frame = 62;
	states[thing].tics = -1;
	states[thing].nextstate = S_PLAY_DRONE1;

	thing = S_PLAY_FLY1A;
	states[thing].sprite = SPR_RIDR;
	states[thing].frame = 63;
	states[thing].tics = 1;
	states[thing].nextstate = S_PLAY_FLY1B;

	thing = S_PLAY_FLY1B;
	states[thing].sprite = SPR_RIDR;
	states[thing].frame = 63;
	states[thing].tics = 1;
	states[thing].nextstate = S_PLAY_FLY1A;

	thing = S_PLAY_FLY2A;
	states[thing].sprite = SPR_RIDR;
	states[thing].frame = 64;
	states[thing].tics = 1;
	states[thing].nextstate = S_PLAY_FLY2B;

	thing = S_PLAY_FLY2B;
	states[thing].sprite = SPR_RIDR;
	states[thing].frame = 64;
	states[thing].tics = 1;
	states[thing].nextstate = S_PLAY_FLY2A;

	thing = S_PLAY_FLY3A;
	states[thing].sprite = SPR_RIDR;
	states[thing].frame = 65;
	states[thing].tics = 1;
	states[thing].nextstate = S_PLAY_FLY3B;

	thing = S_PLAY_FLY3B;
	states[thing].sprite = SPR_RIDR;
	states[thing].frame = 65;
	states[thing].tics = 1;
	states[thing].nextstate = S_PLAY_FLY3A;

	thing = S_PLAY_FLY4A;
	states[thing].sprite = SPR_RIDR;
	states[thing].frame = 66;
	states[thing].tics = 1;
	states[thing].nextstate = S_PLAY_FLY4B;

	thing = S_PLAY_FLY4B;
	states[thing].sprite = SPR_RIDR;
	states[thing].frame = 66;
	states[thing].tics = 1;
	states[thing].nextstate = S_PLAY_FLY4A;

	thing = S_PLAY_FLY5A;
	states[thing].sprite = SPR_RIDR;
	states[thing].frame = 67;
	states[thing].tics = 1;
	states[thing].nextstate = S_PLAY_FLY5B;

	thing = S_PLAY_FLY5B;
	states[thing].sprite = SPR_RIDR;
	states[thing].frame = 67;
	states[thing].tics = 1;
	states[thing].nextstate = S_PLAY_FLY5A;

	thing = S_PLAY_FLY6A;
	states[thing].sprite = SPR_RIDR;
	states[thing].frame = 68;
	states[thing].tics = 1;
	states[thing].nextstate = S_PLAY_FLY6B;

	thing = S_PLAY_FLY6B;
	states[thing].sprite = SPR_RIDR;
	states[thing].frame = 68;
	states[thing].tics = 1;
	states[thing].nextstate = S_PLAY_FLY6A;

	thing = S_PLAY_FLY7A;
	states[thing].sprite = SPR_RIDR;
	states[thing].frame = 69;
	states[thing].tics = 1;
	states[thing].nextstate = S_PLAY_FLY7B;

	thing = S_PLAY_FLY7B;
	states[thing].sprite = SPR_RIDR;
	states[thing].frame = 69;
	states[thing].tics = 1;
	states[thing].nextstate = S_PLAY_FLY7A;

	thing = S_PLAY_FLY8A;
	states[thing].sprite = SPR_RIDR;
	states[thing].frame = 70;
	states[thing].tics = 1;
	states[thing].nextstate = S_PLAY_FLY8B;

	thing = S_PLAY_FLY8B;
	states[thing].sprite = SPR_RIDR;
	states[thing].frame = 70;
	states[thing].tics = 1;
	states[thing].nextstate = S_PLAY_FLY8A;

	thing = S_PLAY_FLY9A;
	states[thing].sprite = SPR_RIDR;
	states[thing].frame = 71;
	states[thing].tics = 1;
	states[thing].nextstate = S_PLAY_FLY9B;

	thing = S_PLAY_FLY9B;
	states[thing].sprite = SPR_RIDR;
	states[thing].frame = 71;
	states[thing].tics = 1;
	states[thing].nextstate = S_PLAY_FLY9A;

	// Speed Monitor
	thing = S_SPEEDMONITOR1;
	states[thing].sprite = SPR_SPED;
	states[thing].frame = 0;
	states[thing].tics = 4;
	states[thing].nextstate = S_SPEEDMONITOR2;

	thing = S_SPEEDMONITOR2;
	states[thing].sprite = SPR_SPED;
	states[thing].frame = 1;
	states[thing].tics = 4;
	states[thing].nextstate = S_SPEEDMONITOR3;

	thing = S_SPEEDMONITOR3;
	states[thing].sprite = SPR_SPED;
	states[thing].frame = 2;
	states[thing].tics = 4;
	states[thing].nextstate = S_SPEEDMONITOR4;

	thing = S_SPEEDMONITOR4;
	states[thing].sprite = SPR_SPED;
	states[thing].frame = 3;
	states[thing].tics = 4;
	states[thing].nextstate = S_SPEEDMONITOR1;

	// Fly Monitor
	thing = S_FLYMONITOR1;
	states[thing].sprite = SPR_FLYM;
	states[thing].frame = 0;
	states[thing].tics = 4;
	states[thing].nextstate = S_FLYMONITOR2;

	thing = S_FLYMONITOR2;
	states[thing].sprite = SPR_FLYM;
	states[thing].frame = 1;
	states[thing].tics = 4;
	states[thing].nextstate = S_FLYMONITOR3;

	thing = S_FLYMONITOR3;
	states[thing].sprite = SPR_FLYM;
	states[thing].frame = 2;
	states[thing].tics = 4;
	states[thing].nextstate = S_FLYMONITOR4;

	thing = S_FLYMONITOR4;
	states[thing].sprite = SPR_FLYM;
	states[thing].frame = 3;
	states[thing].tics = 4;
	states[thing].nextstate = S_FLYMONITOR1;

	// Power Monitor
	thing = S_POWERMONITOR1;
	states[thing].sprite = SPR_POWR;
	states[thing].frame = 0;
	states[thing].tics = 4;
	states[thing].nextstate = S_POWERMONITOR2;

	thing = S_POWERMONITOR2;
	states[thing].sprite = SPR_POWR;
	states[thing].frame = 1;
	states[thing].tics = 4;
	states[thing].nextstate = S_POWERMONITOR3;

	thing = S_POWERMONITOR3;
	states[thing].sprite = SPR_POWR;
	states[thing].frame = 2;
	states[thing].tics = 4;
	states[thing].nextstate = S_POWERMONITOR4;

	thing = S_POWERMONITOR4;
	states[thing].sprite = SPR_POWR;
	states[thing].frame = 3;
	states[thing].tics = 4;
	states[thing].nextstate = S_POWERMONITOR1;

	// Flame (with no corona)
	thing = S_FLAYM1;
	states[thing].sprite = SPR_FLAM;
	states[thing].frame = 0;
	states[thing].tics = 3;
	states[thing].nextstate = S_FLAYM2;

	thing = S_FLAYM2;
	states[thing].sprite = SPR_FLAM;
	states[thing].frame = 1;
	states[thing].tics = 3;
	states[thing].nextstate = S_FLAYM3;

	thing = S_FLAYM3;
	states[thing].sprite = SPR_FLAM;
	states[thing].frame = 2;
	states[thing].tics = 3;
	states[thing].nextstate = S_FLAYM4;

	thing = S_FLAYM4;
	states[thing].sprite = SPR_FLAM;
	states[thing].frame = 3;
	states[thing].tics = 3;
	states[thing].nextstate = S_FLAYM1;

	// Devil Gargoyle
	thing = S_DEVIL;
	states[thing].sprite = SPR_DEVL;
	states[thing].frame = 0;
	states[thing].tics = -1;

	// Naked Angel Gargoyle... yeah.
	thing = S_ANGEL;
	states[thing].sprite = SPR_ANGE;
	states[thing].frame = 0;
	states[thing].tics = -1;

	// Air Tank Box
	thing = S_AIRTANKBOX;
	states[thing].sprite = SPR_ATTV;
	states[thing].frame = 0;
	states[thing].tics = 2;
	states[thing].nextstate = S_AIRTANKBOX1;

	thing = S_AIRTANKBOX1;
	states[thing].sprite = SPR_RTEX;
	states[thing].frame = 0;
	states[thing].tics = 1;
	states[thing].nextstate = S_AIRTANKBOX;

	thing = S_AIRTANKBOX2;
	states[thing].sprite = SPR_ATTV;
	states[thing].frame = 1;
	states[thing].tics = 18;
	states[thing].action.acp1 = (actionf_p1)A_MonitorPop;
	states[thing].nextstate = S_AIRTANKBOX3;

	thing = S_AIRTANKBOX3;
	states[thing].sprite = SPR_ATTV;
	states[thing].frame = 1;
	states[thing].tics = 18;
	states[thing].action.acp1 = (actionf_p1)A_AirBox;
	states[thing].nextstate = S_DISS;

	// Riders Random Item
	thing = S_RANDOMCAPSULE1;
	states[thing].sprite = SPR_TION;
	states[thing].frame = 0;
	states[thing].tics = 2;
	states[thing].nextstate = S_RANDOMCAPSULE2;

	thing = S_RANDOMCAPSULE2;
	states[thing].sprite = SPR_RTEX;
	states[thing].frame = 0;
	states[thing].tics = 1;
	states[thing].nextstate = S_RANDOMCAPSULE1;

	thing = S_RANDOMCAPSULE3;
	states[thing].sprite = SPR_RNDM;
	states[thing].frame = 0;
	states[thing].tics = 1;
	states[thing].action.acp1 = (actionf_p1)A_CapsulePop;
	states[thing].nextstate = S_DISS;

	// Mario Kart Random Item
	thing = S_RANDOMITEM1;
	states[thing].sprite = SPR_RNDM;
	states[thing].frame = 0;
	states[thing].tics = 2;
	states[thing].nextstate = S_RANDOMITEM2;

	thing = S_RANDOMITEM2;
	states[thing].sprite = SPR_KTEX;
	states[thing].frame = 0;
	states[thing].tics = 1;
	states[thing].nextstate = S_RANDOMITEM1;

	thing = S_RANDOMITEM3;
	states[thing].sprite = SPR_RNDM;
	states[thing].frame = 0;
	states[thing].tics = 1;
	states[thing].action.acp1 = (actionf_p1)A_ItemPop;
	states[thing].nextstate = S_DISS;

	// Mario Kart Item Explosion
	thing = S_ITEMEXPLOSION1;
	states[thing].sprite = SPR_SPRK;
	states[thing].frame = 0;
	states[thing].tics = 2;
	states[thing].nextstate = S_ITEMEXPLOSION2;

	thing = S_ITEMEXPLOSION2;
	states[thing].sprite = SPR_SPRK;
	states[thing].frame = 1;
	states[thing].tics = 2;
	states[thing].nextstate = S_ITEMEXPLOSION3;

	thing = S_ITEMEXPLOSION3;
	states[thing].sprite = SPR_SPRK;
	states[thing].frame = 2;
	states[thing].tics = 2;
	states[thing].nextstate = S_ITEMEXPLOSION4;

	thing = S_ITEMEXPLOSION4;
	states[thing].sprite = SPR_SPRK;
	states[thing].frame = 3;
	states[thing].tics = 2;
	states[thing].nextstate = S_ITEMEXPLOSION5;

	thing = S_ITEMEXPLOSION5;
	states[thing].sprite = SPR_DISS;
	states[thing].frame = 0;
	states[thing].tics = -1;
	states[thing].nextstate = S_NULL;

	// Mario Kart Item Explosion
	thing = S_ITEMEXPLOSION1;
	states[thing].sprite = SPR_SPRK;
	states[thing].frame = 0;
	states[thing].tics = 2;
	states[thing].nextstate = S_ITEMEXPLOSION2;

	thing = S_ITEMEXPLOSION2;
	states[thing].sprite = SPR_SPRK;
	states[thing].frame = 1;
	states[thing].tics = 2;
	states[thing].nextstate = S_ITEMEXPLOSION3;

	thing = S_ITEMEXPLOSION3;
	states[thing].sprite = SPR_SPRK;
	states[thing].frame = 2;
	states[thing].tics = 2;
	states[thing].nextstate = S_ITEMEXPLOSION4;

	thing = S_ITEMEXPLOSION4;
	states[thing].sprite = SPR_SPRK;
	states[thing].frame = 3;
	states[thing].tics = 2;
	states[thing].nextstate = S_ITEMEXPLOSION5;

	thing = S_ITEMEXPLOSION5;
	states[thing].sprite = SPR_DISS;
	states[thing].frame = 0;
	states[thing].tics = -1;
	states[thing].nextstate = S_NULL;

	// Mario Kart Star Box
	thing = S_MKSTAR;
	states[thing].sprite = SPR_MSTR;
	states[thing].frame = 0;
	states[thing].tics = 2;
	states[thing].nextstate = S_MKSTAR2;

	thing = S_MKSTAR2;
	states[thing].sprite = SPR_KTEX;
	states[thing].frame = 0;
	states[thing].tics = 1;
	states[thing].nextstate = S_MKSTAR;

	thing = S_MKSTAR3;
	states[thing].sprite = SPR_DISS;
	states[thing].frame = 0;
	states[thing].tics = 18;
	states[thing].action.acp1 = (actionf_p1)A_MonitorPop;
	states[thing].nextstate = S_MKSTAR4;

	thing = S_MKSTAR4;
	states[thing].sprite = SPR_DISS;
	states[thing].frame = 0;
	states[thing].tics = 18;
	states[thing].action.acp1 = (actionf_p1)A_Invincibility;
	states[thing].nextstate = S_DISS;

	// Shell Box
	thing = S_SHELLBOX;
	states[thing].sprite = SPR_SHEL;
	states[thing].frame = 0;
	states[thing].tics = 2;
	states[thing].nextstate = S_SHELLBOX1;

	thing = S_SHELLBOX1;
	states[thing].sprite = SPR_KTEX;
	states[thing].frame = 0;
	states[thing].tics = 1;
	states[thing].nextstate = S_SHELLBOX;

	thing = S_SHELLBOX2;
	states[thing].sprite = SPR_DISS;
	states[thing].frame = 0;
	states[thing].tics = 18;
	states[thing].action.acp1 = (actionf_p1)A_MonitorPop;
	states[thing].nextstate = S_SHELLBOX3;

	thing = S_SHELLBOX3;
	states[thing].sprite = SPR_DISS;
	states[thing].frame = 0;
	states[thing].tics = 18;
	states[thing].action.acp1 = (actionf_p1)A_JumpShield;
	states[thing].nextstate = S_DISS;

	// Red Shell Box
	thing = S_REDSHELLBOX;
	states[thing].sprite = SPR_RSHL;
	states[thing].frame = 0;
	states[thing].tics = 2;
	states[thing].nextstate = S_REDSHELLBOX1;

	thing = S_REDSHELLBOX1;
	states[thing].sprite = SPR_KTEX;
	states[thing].frame = 0;
	states[thing].tics = 1;
	states[thing].nextstate = S_REDSHELLBOX;

	thing = S_REDSHELLBOX2;
	states[thing].sprite = SPR_DISS;
	states[thing].frame = 0;
	states[thing].tics = 18;
	states[thing].action.acp1 = (actionf_p1)A_MonitorPop;
	states[thing].nextstate = S_REDSHELLBOX3;

	thing = S_REDSHELLBOX3;
	states[thing].sprite = SPR_DISS;
	states[thing].frame = 0;
	states[thing].tics = 18;
	states[thing].action.acp1 = (actionf_p1)A_RingShield;
	states[thing].nextstate = S_DISS;

	// Banana Box
	thing = S_BANANABOX;
	states[thing].sprite = SPR_NANA;
	states[thing].frame = 0;
	states[thing].tics = 2;
	states[thing].nextstate = S_BANANABOX1;

	thing = S_BANANABOX1;
	states[thing].sprite = SPR_KTEX;
	states[thing].frame = 0;
	states[thing].tics = 1;
	states[thing].nextstate = S_BANANABOX;

	thing = S_BANANABOX2;
	states[thing].sprite = SPR_DISS;
	states[thing].frame = 0;
	states[thing].tics = 18;
	states[thing].action.acp1 = (actionf_p1)A_MonitorPop;
	states[thing].nextstate = S_BANANABOX3;

	thing = S_BANANABOX3;
	states[thing].sprite = SPR_DISS;
	states[thing].frame = 0;
	states[thing].tics = 18;
	states[thing].action.acp1 = (actionf_p1)A_FireShield;
	states[thing].nextstate = S_DISS;

	// Peel
	thing = S_PEEL;
	states[thing].sprite = SPR_PEEL;
	states[thing].frame = 0;
	states[thing].tics = 2;
	states[thing].nextstate = S_PEEL;

	thing = S_PEEL2;
	states[thing].sprite = SPR_PEEL;
	states[thing].frame = 0;
	states[thing].tics = 2;
	states[thing].nextstate = S_PEEL2;

	thing = S_PEEL3;
	states[thing].sprite = SPR_PEEL;
	states[thing].frame = 0;
	states[thing].tics = 2;
	states[thing].nextstate = S_PEEL3;

	thing = S_PEEL4;
	states[thing].sprite = SPR_PEEL;
	states[thing].frame = 0;
	states[thing].tics = 2;
	states[thing].nextstate = S_PEEL4;

	thing = S_PEEL5;
	states[thing].sprite = SPR_PEEL;
	states[thing].frame = 0;
	states[thing].tics = -1;
	states[thing].nextstate = S_PEEL5;

	// Fake Item Box
	thing = S_FAKEBOX;
	states[thing].sprite = SPR_NANA;
	states[thing].frame = 0;
	states[thing].tics = 2;
	states[thing].nextstate = S_FAKEBOX1;

	thing = S_FAKEBOX1;
	states[thing].sprite = SPR_KTEX;
	states[thing].frame = 0;
	states[thing].tics = 1;
	states[thing].nextstate = S_FAKEBOX;

	thing = S_FAKEBOX2;
	states[thing].sprite = SPR_DISS;
	states[thing].frame = 0;
	states[thing].tics = 18;
	states[thing].action.acp1 = (actionf_p1)A_MonitorPop;
	states[thing].nextstate = S_FAKEBOX3;

	thing = S_FAKEBOX3;
	states[thing].sprite = SPR_DISS;
	states[thing].frame = 0;
	states[thing].tics = 18;
	states[thing].action.acp1 = (actionf_p1)A_WaterShield;
	states[thing].nextstate = S_DISS;

	// Fake
	thing = S_FAKE;
	states[thing].sprite = SPR_FAIK;
	states[thing].frame = 0;
	states[thing].tics = 2;
	states[thing].nextstate = S_FAKE;

	thing = S_FAKE2;
	states[thing].sprite = SPR_FAIK;
	states[thing].frame = 0;
	states[thing].tics = 2;
	states[thing].nextstate = S_FAKE2;

	thing = S_FAKE3;
	states[thing].sprite = SPR_FAIK;
	states[thing].frame = 0;
	states[thing].tics = 2;
	states[thing].nextstate = S_FAKE3;

	thing = S_FAKE4;
	states[thing].sprite = SPR_FAIK;
	states[thing].frame = 0;
	states[thing].tics = 2;
	states[thing].nextstate = S_FAKE4;

	thing = S_FAKE5;
	states[thing].sprite = SPR_FAIK;
	states[thing].frame = 0;
	states[thing].tics = -1;
	states[thing].nextstate = S_FAKE5;

	// Bomb Box
	thing = S_BOOMBOX;
	states[thing].sprite = SPR_NANA;
	states[thing].frame = 0;
	states[thing].tics = 2;
	states[thing].nextstate = S_BOOMBOX1;

	thing = S_BOOMBOX1;
	states[thing].sprite = SPR_KTEX;
	states[thing].frame = 0;
	states[thing].tics = 1;
	states[thing].nextstate = S_BOOMBOX;

	thing = S_BOOMBOX2;
	states[thing].sprite = SPR_DISS;
	states[thing].frame = 0;
	states[thing].tics = 18;
	states[thing].action.acp1 = (actionf_p1)A_MonitorPop;
	states[thing].nextstate = S_BOOMBOX3;

	thing = S_BOOMBOX3;
	states[thing].sprite = SPR_DISS;
	states[thing].frame = 0;
	states[thing].tics = 18;
	states[thing].action.acp1 = (actionf_p1)A_BombShield;
	states[thing].nextstate = S_DISS;

	// Riders Super Ring Box
	thing = S_SUPERRINGBOX4;
	states[thing].sprite = SPR_SRTV;
	states[thing].frame = 0;
	states[thing].tics = 2;
	states[thing].nextstate = S_SUPERRINGBOX5;

	thing = S_SUPERRINGBOX5;
	states[thing].sprite = SPR_RTEX;
	states[thing].frame = 0;
	states[thing].tics = 1;
	states[thing].nextstate = S_SUPERRINGBOX4;

	thing = S_SUPERRINGBOX6;
	states[thing].sprite = SPR_SRTV;
	states[thing].frame = 1;
	states[thing].tics = 18;
	states[thing].action.acp1 = (actionf_p1)A_MonitorPop;
	states[thing].nextstate = S_SUPERRINGBOX7;

	thing = S_SUPERRINGBOX7;
	states[thing].sprite = SPR_SRTV;
	states[thing].frame = 1;
	states[thing].tics = 18;
	states[thing].action.acp1 = (actionf_p1)A_RingBox;
	states[thing].nextstate = S_DISS;

	// Riders Grey Ring Box
	thing = S_GREYRINGBOX4;
	states[thing].sprite = SPR_GRTV;
	states[thing].frame = 0;
	states[thing].tics = 2;
	states[thing].nextstate = S_GREYRINGBOX5;

	thing = S_GREYRINGBOX5;
	states[thing].sprite = SPR_RTEX;
	states[thing].frame = 0;
	states[thing].tics = 1;
	states[thing].nextstate = S_GREYRINGBOX4;

	thing = S_GREYRINGBOX6;
	states[thing].sprite = SPR_GRTV;
	states[thing].frame = 1;
	states[thing].tics = 18;
	states[thing].action.acp1 = (actionf_p1)A_MonitorPop;
	states[thing].nextstate = S_GREYRINGBOX7;

	thing = S_GREYRINGBOX7;
	states[thing].sprite = SPR_GRTV;
	states[thing].frame = 1;
	states[thing].tics = 18;
	states[thing].action.acp1 = (actionf_p1)A_RingBox;
	states[thing].nextstate = S_DISS;

	// Riders Invincibility Box
	thing = S_PINV5;
	states[thing].sprite = SPR_RINV;
	states[thing].frame = 0;
	states[thing].tics = 2;
	states[thing].nextstate = S_PINV6;

	thing = S_PINV6;
	states[thing].sprite = SPR_RTEX;
	states[thing].frame = 0;
	states[thing].tics = 1;
	states[thing].nextstate = S_PINV5;

	thing = S_PINV7;
	states[thing].sprite = SPR_RINV;
	states[thing].frame = 1;
	states[thing].tics = 18;
	states[thing].action.acp1 = (actionf_p1)A_MonitorPop;
	states[thing].nextstate = S_PINV8;

	thing = S_PINV8;
	states[thing].sprite = SPR_RINV;
	states[thing].frame = 1;
	states[thing].tics = 18;
	states[thing].action.acp1 = (actionf_p1)A_Invincibility;
	states[thing].nextstate = S_DISS;

	// Mario Kart Mushroom
	thing = S_SHTV4;
	states[thing].sprite = SPR_KTEX;
	states[thing].frame = 0;
	states[thing].tics = 2;
	states[thing].nextstate = S_SHTV5;

	thing = S_SHTV5;
	states[thing].sprite = SPR_KTEX;
	states[thing].frame = 0;
	states[thing].tics = 1;
	states[thing].nextstate = S_SHTV4;

	thing = S_SHTV6;
	states[thing].sprite = SPR_DISS;
	states[thing].frame = 0;
	states[thing].tics = 18;
	states[thing].action.acp1 = (actionf_p1)A_MonitorPop;
	states[thing].nextstate = S_SHTV7;

	thing = S_SHTV7;
	states[thing].sprite = SPR_DISS;
	states[thing].frame = 0;
	states[thing].tics = 18;
	states[thing].action.acp1 = (actionf_p1)A_SuperSneakers;
	states[thing].nextstate = S_DISS;

	// Riders Ring Shield Box
	thing = S_SHBX;
	states[thing].sprite = SPR_YLBX;
	states[thing].frame = 0;
	states[thing].tics = 2;
	states[thing].nextstate = S_SHBX1;

	thing = S_SHBX1;
	states[thing].sprite = SPR_RTEX;
	states[thing].frame = 0;
	states[thing].tics = 1;
	states[thing].nextstate = S_SHBX;

	thing = S_SHBX2;
	states[thing].sprite = SPR_YLBX;
	states[thing].frame = 1;
	states[thing].tics = 18;
	states[thing].action.acp1 = (actionf_p1)A_MonitorPop;
	states[thing].nextstate = S_SHBX3;

	thing = S_SHBX3;
	states[thing].sprite = SPR_YLBX;
	states[thing].frame = 1;
	states[thing].tics = 18;
	states[thing].action.acp1 = (actionf_p1)A_RingShield;
	states[thing].nextstate = S_DISS;

	// Mario Kart Fake Item
	thing = S_BORB9;
	states[thing].sprite = SPR_FAIK;
	states[thing].frame = 0;
	states[thing].tics = 1;
	states[thing].action.acp1 = (actionf_p1)A_KartItems;
	states[thing].nextstate = S_BORB10;

	thing = S_BORB10;
	states[thing].sprite = SPR_FAIK;
	states[thing].frame = 0;
	states[thing].tics = 1;
	states[thing].action.acp1 = (actionf_p1)A_KartItems;
	states[thing].nextstate = S_BORB9;

	// Mario Kart Bomb Item
	thing = S_KORB9;
	states[thing].sprite = SPR_BOMB;
	states[thing].frame = 0;
	states[thing].tics = 1;
	states[thing].action.acp1 = (actionf_p1)A_KartItems;
	states[thing].nextstate = S_KORB10;

	thing = S_KORB10;
	states[thing].sprite = SPR_BOMB;
	states[thing].frame = 0;
	states[thing].tics = 1;
	states[thing].action.acp1 = (actionf_p1)A_KartItems;
	states[thing].nextstate = S_KORB9;

	// Mario Kart Banana Peel
	thing = S_RORB9;
	states[thing].sprite = SPR_PEEL;
	states[thing].frame = 0;
	states[thing].tics = 1;
	states[thing].action.acp1 = (actionf_p1)A_KartItems;
	states[thing].nextstate = S_RORB10;

	thing = S_RORB10;
	states[thing].sprite = SPR_PEEL;
	states[thing].frame = 0;
	states[thing].tics = 1;
	states[thing].action.acp1 = (actionf_p1)A_KartItems;
	states[thing].nextstate = S_RORB9;

	// Mario Kart Shell
	thing = S_WORB9;
	states[thing].sprite = SPR_SHLL;
	states[thing].frame = 0;
	states[thing].tics = 1;
	states[thing].action.acp1 = (actionf_p1)A_KartItems;
	states[thing].nextstate = S_WORB10;

	thing = S_WORB10;
	states[thing].sprite = SPR_SHLL;
	states[thing].frame = 0;
	states[thing].tics = 1;
	states[thing].action.acp1 = (actionf_p1)A_KartItems;
	states[thing].nextstate = S_WORB11;

	thing = S_WORB11;
	states[thing].sprite = SPR_SHLL;
	states[thing].frame = 2;
	states[thing].tics = 1;
	states[thing].action.acp1 = (actionf_p1)A_KartItems;
	states[thing].nextstate = S_WORB12;

	thing = S_WORB12;
	states[thing].sprite = SPR_SHLL;
	states[thing].frame = 2;
	states[thing].tics = 1;
	states[thing].action.acp1 = (actionf_p1)A_KartItems;
	states[thing].nextstate = S_WORB9;

	// Mario Kart Red Shell
	thing = S_YORB9;
	states[thing].sprite = SPR_SHL2;
	states[thing].frame = 0;
	states[thing].tics = 1;
	states[thing].action.acp1 = (actionf_p1)A_KartItems;
	states[thing].nextstate = S_YORB10;

	thing = S_YORB10;
	states[thing].sprite = SPR_SHL2;
	states[thing].frame = 0;
	states[thing].tics = 1;
	states[thing].action.acp1 = (actionf_p1)A_KartItems;
	states[thing].nextstate = S_YORB11;

	thing = S_YORB11;
	states[thing].sprite = SPR_SHL2;
	states[thing].frame = 2;
	states[thing].tics = 1;
	states[thing].action.acp1 = (actionf_p1)A_KartItems;
	states[thing].nextstate = S_YORB12;

	thing = S_YORB12;
	states[thing].sprite = SPR_SHL2;
	states[thing].frame = 2;
	states[thing].tics = 1;
	states[thing].action.acp1 = (actionf_p1)A_KartItems;
	states[thing].nextstate = S_YORB9;

	// Riders Invinsible Bumper
	thing = S_NIGHTSBUMPER13;
	states[thing].sprite = SPR_DISS;
	states[thing].frame = 0;
	states[thing].tics = -1;
	states[thing].nextstate = S_NULL;

	// Mario Kart Bomb
	thing = S_THROWNEXPLOSION8;
	states[thing].sprite = SPR_BOMB;
	states[thing].frame = 0;
	states[thing].tics = 1;
	states[thing].action.acp1 = (actionf_p1)A_GrenadeRing;
	states[thing].nextstate = S_THROWNEXPLOSION9;

	thing = S_THROWNEXPLOSION9;
	states[thing].sprite = SPR_BOMB;
	states[thing].frame = 0;
	states[thing].tics = 1;
	states[thing].action.acp1 = (actionf_p1)A_GrenadeRing;
	states[thing].nextstate = S_THROWNEXPLOSION8;

	// NiGHTS Explosion
	thing = S_NIGHTSEXPLOSION;
	states[thing].sprite = SPR_DISS;
	states[thing].frame = 0;
	states[thing].tics = 1;
	states[thing].action.acp1 = (actionf_p1)A_NiGHTSExplosion;
	states[thing].var1 = MT_REDRING;
	states[thing].nextstate = S_XPLD1;

	// Riders Random Box
	thing = S_RANDOMBOX4;
	states[thing].sprite = SPR_TION;
	states[thing].frame = 0;
	states[thing].tics = 2;
	states[thing].nextstate = S_RANDOMBOX5;

	thing = S_RANDOMBOX5;
	states[thing].sprite = SPR_RTEX;
	states[thing].frame = 0;
	states[thing].tics = 1;
	states[thing].nextstate = S_RANDOMBOX4;

	thing = S_RANDOMBOX6;
	states[thing].sprite = SPR_TION;
	states[thing].frame = 0;
	states[thing].tics = 1;
	states[thing].action.acp1 = (actionf_p1)A_MonitorPop;
	states[thing].nextstate = S_DISS;

	// Riders Item Explosion
	thing = S_MONITOREXPLOSION6;
	states[thing].sprite = SPR_RTEX;
	states[thing].frame = 1;
	states[thing].tics = 2;
	states[thing].nextstate = S_MONITOREXPLOSION7;

	thing = S_MONITOREXPLOSION7;
	states[thing].sprite = SPR_RTEX;
	states[thing].frame = 2;
	states[thing].tics = 2;
	states[thing].nextstate = S_MONITOREXPLOSION8;

	thing = S_MONITOREXPLOSION8;
	states[thing].sprite = SPR_RTEX;
	states[thing].frame = 3;
	states[thing].tics = 2;
	states[thing].nextstate = S_MONITOREXPLOSION9;

	thing = S_MONITOREXPLOSION9;
	states[thing].sprite = SPR_RTEX;
	states[thing].frame = 4;
	states[thing].tics = 2;
	states[thing].nextstate = S_MONITOREXPLOSION10;

	thing = S_MONITOREXPLOSION10;
	states[thing].sprite = SPR_RTEX;
	states[thing].frame = 5;
	states[thing].tics = -1;
	states[thing].nextstate = S_NULL;

	// Alternate View 2
	thing = S_ALTVIEWMAN;
	states[thing].sprite = SPR_TNT1;
	states[thing].frame = 0;
	states[thing].tics = 1;
	states[thing].var1 = 1;
	states[thing].action.acp1 = (actionf_p1)A_Look;
	states[thing].nextstate = S_ALTVIEWMAN;

	thing = S_ALTVIEWMAN1;
	states[thing].sprite = SPR_TNT1;
	states[thing].frame = 0;
	states[thing].tics = 1;
	states[thing].action.acp1 = (actionf_p1)A_FaceTarget;
	states[thing].nextstate = S_ALTVIEWMAN;

	// Freed B Rabbit
	thing = S_BUNNIE1;
	states[thing].sprite = SPR_BUNY;
	states[thing].frame = 0;
	states[thing].tics = 3;
	states[thing].action.acp1 = (actionf_p1)A_MouseThink;
	states[thing].nextstate = S_BUNNIE2;

	thing = S_BUNNIE2;
	states[thing].sprite = SPR_BUNY;
	states[thing].frame = 1;
	states[thing].tics = 3;
	states[thing].action.acp1 = (actionf_p1)A_MouseThink;
	states[thing].nextstate = S_BUNNIE1;

	// Freed Monkey
	thing = S_MONKEY1;
	states[thing].sprite = SPR_MNKY;
	states[thing].frame = 0;
	states[thing].tics = 3;
	states[thing].action.acp1 = (actionf_p1)A_MouseThink;
	states[thing].nextstate = S_MONKEY2;

	thing = S_MONKEY2;
	states[thing].sprite = SPR_MNKY;
	states[thing].frame = 1;
	states[thing].tics = 3;
	states[thing].action.acp1 = (actionf_p1)A_MouseThink;
	states[thing].nextstate = S_MONKEY1;

	// Freed Eagle
	thing = S_EAGLE1;
	states[thing].sprite = SPR_EGLE;
	states[thing].frame = 0;
	states[thing].tics = 4;
	states[thing].nextstate = S_EAGLE2;

	thing = S_EAGLE2;
	states[thing].sprite = SPR_EGLE;
	states[thing].frame = 0;
	states[thing].tics = 4;
	states[thing].action.acp1 = (actionf_p1)A_Chase;
	states[thing].nextstate = S_EAGLE3;

	thing = S_EAGLE3;
	states[thing].sprite = SPR_EGLE;
	states[thing].frame = 1;
	states[thing].tics = 4;
	states[thing].action.acp1 = (actionf_p1)A_Chase;
	states[thing].nextstate = S_EAGLE2;

	// Freed Chicken
	thing = S_CHICKEN1;
	states[thing].sprite = SPR_CHKN;
	states[thing].frame = 0;
	states[thing].tics = 4;
	states[thing].nextstate = S_CHICKEN2;

	thing = S_CHICKEN2;
	states[thing].sprite = SPR_CHKN;
	states[thing].frame = 1;
	states[thing].tics = 64;
	states[thing].nextstate = S_CHICKEN3;

	thing = S_CHICKEN3;
	states[thing].sprite = SPR_CHKN;
	states[thing].frame = 0;
	states[thing].tics = 2;
	states[thing].var1 = 6;
	states[thing].var2 = 3;
	states[thing].action.acp1 = (actionf_p1)A_BunnyHop;
	states[thing].nextstate = S_CHICKEN4;

	thing = S_CHICKEN4;
	states[thing].sprite = SPR_CHKN;
	states[thing].frame = 1;
	states[thing].tics = 2;
	states[thing].action.acp1 = (actionf_p1)A_Chase;
	states[thing].nextstate = S_CHICKEN5;

	thing = S_CHICKEN5;
	states[thing].sprite = SPR_CHKN;
	states[thing].frame = 0;
	states[thing].tics = 2;
	states[thing].action.acp1 = (actionf_p1)A_Chase;
	states[thing].nextstate = S_CHICKEN6;

	thing = S_CHICKEN6;
	states[thing].sprite = SPR_CHKN;
	states[thing].frame = 1;
	states[thing].tics = 2;
	states[thing].action.acp1 = (actionf_p1)A_Chase;
	states[thing].nextstate = S_CHICKEN7;

	thing = S_CHICKEN7;
	states[thing].sprite = SPR_CHKN;
	states[thing].frame = 0;
	states[thing].tics = 2;
	states[thing].action.acp1 = (actionf_p1)A_Chase;
	states[thing].nextstate = S_CHICKEN8;

	thing = S_CHICKEN8;
	states[thing].sprite = SPR_CHKN;
	states[thing].frame = 1;
	states[thing].tics = 2;
	states[thing].action.acp1 = (actionf_p1)A_Chase;
	states[thing].nextstate = S_CHICKEN2;

	// Freed Piggie
	thing = S_PIGGIE1;
	states[thing].sprite = SPR_PIGY;
	states[thing].frame = 0;
	states[thing].tics = 4;
	states[thing].action.acp1 = (actionf_p1)A_MouseThink;
	states[thing].nextstate = S_PIGGIE2;

	thing = S_PIGGIE2;
	states[thing].sprite = SPR_PIGY;
	states[thing].frame = 1;
	states[thing].tics = 4;
	states[thing].action.acp1 = (actionf_p1)A_MouseThink;
	states[thing].nextstate = S_PIGGIE1;

	// Freed Seel
	thing = S_SEEL1;
	states[thing].sprite = SPR_SEEL;
	states[thing].frame = 0;
	states[thing].tics = 4;
	states[thing].action.acp1 = (actionf_p1)A_MouseThink;
	states[thing].nextstate = S_SEEL2;

	thing = S_SEEL2;
	states[thing].sprite = SPR_SEEL;
	states[thing].frame = 1;
	states[thing].tics = 4;
	states[thing].action.acp1 = (actionf_p1)A_MouseThink;
	states[thing].nextstate = S_SEEL1;

	// Freed Penguin
	thing = S_PENGUIN1;
	states[thing].sprite = SPR_PNGN;
	states[thing].frame = 0;
	states[thing].tics = 4;
	states[thing].nextstate = S_PENGUIN2;

	thing = S_PENGUIN2;
	states[thing].sprite = SPR_PNGN;
	states[thing].frame = 1;
	states[thing].tics = 64;
	states[thing].nextstate = S_PENGUIN3;

	thing = S_PENGUIN3;
	states[thing].sprite = SPR_PNGN;
	states[thing].frame = 0;
	states[thing].tics = 2;
	states[thing].var1 = 6;
	states[thing].var2 = 3;
	states[thing].action.acp1 = (actionf_p1)A_BunnyHop;
	states[thing].nextstate = S_PENGUIN4;

	thing = S_PENGUIN4;
	states[thing].sprite = SPR_PNGN;
	states[thing].frame = 1;
	states[thing].tics = 2;
	states[thing].action.acp1 = (actionf_p1)A_Chase;
	states[thing].nextstate = S_PENGUIN5;

	thing = S_PENGUIN5;
	states[thing].sprite = SPR_PNGN;
	states[thing].frame = 0;
	states[thing].tics = 2;
	states[thing].action.acp1 = (actionf_p1)A_Chase;
	states[thing].nextstate = S_PENGUIN6;

	thing = S_PENGUIN6;
	states[thing].sprite = SPR_PNGN;
	states[thing].frame = 1;
	states[thing].tics = 2;
	states[thing].action.acp1 = (actionf_p1)A_Chase;
	states[thing].nextstate = S_PENGUIN7;

	thing = S_PENGUIN7;
	states[thing].sprite = SPR_PNGN;
	states[thing].frame = 0;
	states[thing].tics = 2;
	states[thing].action.acp1 = (actionf_p1)A_Chase;
	states[thing].nextstate = S_PENGUIN8;

	thing = S_PENGUIN8;
	states[thing].sprite = SPR_PNGN;
	states[thing].frame = 1;
	states[thing].tics = 2;
	states[thing].action.acp1 = (actionf_p1)A_Chase;
	states[thing].nextstate = S_PENGUIN2;

	// Freed Teddy
	thing = S_TEDDY1;
	states[thing].sprite = SPR_TEDY;
	states[thing].frame = 0;
	states[thing].tics = 4;
	states[thing].action.acp1 = (actionf_p1)A_MouseThink;
	states[thing].nextstate = S_TEDDY2;

	thing = S_TEDDY2;
	states[thing].sprite = SPR_TEDY;
	states[thing].frame = 1;
	states[thing].tics = 4;
	states[thing].action.acp1 = (actionf_p1)A_MouseThink;
	states[thing].nextstate = S_TEDDY1;

	// Freed Turtle
	thing = S_TURTLE1;
	states[thing].sprite = SPR_TRTL;
	states[thing].frame = 0;
	states[thing].tics = 5;
	states[thing].action.acp1 = (actionf_p1)A_MouseThink;
	states[thing].nextstate = S_TURTLE2;

	thing = S_TURTLE2;
	states[thing].sprite = SPR_TRTL;
	states[thing].frame = 1;
	states[thing].tics = 5;
	states[thing].action.acp1 = (actionf_p1)A_MouseThink;
	states[thing].nextstate = S_TURTLE1;

	thing = S_TURRET2;
	states[thing].sprite = SPR_TRET;
	states[thing].frame = 32768;
	states[thing].tics = 1;
	states[thing].action.acp1 = (actionf_p1)A_TurretStop;
	states[thing].nextstate = S_TURRETFIRE2;

	thing = S_TURRETFIRE2;
	states[thing].sprite = SPR_TRET;
	states[thing].frame = 32768;
	states[thing].tics = 1;
	states[thing].action.acp1 = (actionf_p1)A_TurretFire;
	states[thing].nextstate = S_TURRETFIRE2;

	// Flag
	thing = S_FLAG;
	states[thing].sprite = SPR_FLAG;
	states[thing].frame = 0;
	states[thing].tics = -1;
	states[thing].nextstate = S_NULL;

	// Palm Tree
	thing = S_PALMTREE;
	states[thing].sprite = SPR_PALM;
	states[thing].frame = 0;
	states[thing].tics = -1;
	states[thing].nextstate = S_NULL;

	// Cheese
	thing = S_CHEESE1;
	states[thing].sprite = SPR_CHES;
	states[thing].frame = 0;
	states[thing].tics = 1;
	states[thing].var1 = 1;
	states[thing].action.acp1 = (actionf_p1)A_CheeseNChips;
	states[thing].nextstate = S_CHEESE2;

	thing = S_CHEESE2;
	states[thing].sprite = SPR_CHES;
	states[thing].frame = 0;
	states[thing].tics = 1;
	states[thing].var1 = 1;
	states[thing].action.acp1 = (actionf_p1)A_CheeseNChips;
	states[thing].nextstate = S_CHEESE3;

	thing = S_CHEESE3;
	states[thing].sprite = SPR_CHES;
	states[thing].frame = 0;
	states[thing].tics = 1;
	states[thing].var1 = 1;
	states[thing].action.acp1 = (actionf_p1)A_CheeseNChips;
	states[thing].nextstate = S_CHEESE4;

	thing = S_CHEESE4;
	states[thing].sprite = SPR_CHES;
	states[thing].frame = 1;
	states[thing].tics = 1;
	states[thing].var1 = 1;
	states[thing].action.acp1 = (actionf_p1)A_CheeseNChips;
	states[thing].nextstate = S_CHEESE5;

	thing = S_CHEESE5;
	states[thing].sprite = SPR_CHES;
	states[thing].frame = 1;
	states[thing].tics = 1;
	states[thing].var1 = 1;
	states[thing].action.acp1 = (actionf_p1)A_CheeseNChips;
	states[thing].nextstate = S_CHEESE6;

	thing = S_CHEESE6;
	states[thing].sprite = SPR_CHES;
	states[thing].frame = 1;
	states[thing].tics = 1;
	states[thing].var1 = 1;
	states[thing].action.acp1 = (actionf_p1)A_CheeseNChips;
	states[thing].nextstate = S_CHEESE1;

	thing = S_CHEESE7;
	states[thing].sprite = SPR_CHES;
	states[thing].frame = 2;
	states[thing].tics = 1;
	states[thing].var1 = 1;
	states[thing].action.acp1 = (actionf_p1)A_CheeseNChips;
	states[thing].nextstate = S_CHEESE8;

	thing = S_CHEESE8;
	states[thing].sprite = SPR_CHES;
	states[thing].frame = 2;
	states[thing].tics = 1;
	states[thing].var1 = 1;
	states[thing].action.acp1 = (actionf_p1)A_CheeseNChips;
	states[thing].nextstate = S_CHEESE9;

	thing = S_CHEESE9;
	states[thing].sprite = SPR_CHES;
	states[thing].frame = 2;
	states[thing].tics = 1;
	states[thing].var1 = 1;
	states[thing].action.acp1 = (actionf_p1)A_CheeseNChips;
	states[thing].nextstate = S_CHEESE10;

	thing = S_CHEESE10;
	states[thing].sprite = SPR_CHES;
	states[thing].frame = 3;
	states[thing].tics = 1;
	states[thing].var1 = 1;
	states[thing].action.acp1 = (actionf_p1)A_CheeseNChips;
	states[thing].nextstate = S_CHEESE11;

	thing = S_CHEESE11;
	states[thing].sprite = SPR_CHES;
	states[thing].frame = 3;
	states[thing].tics = 1;
	states[thing].var1 = 1;
	states[thing].action.acp1 = (actionf_p1)A_CheeseNChips;
	states[thing].nextstate = S_CHEESE12;

	thing = S_CHEESE12;
	states[thing].sprite = SPR_CHES;
	states[thing].frame = 3;
	states[thing].tics = 1;
	states[thing].var1 = 1;
	states[thing].action.acp1 = (actionf_p1)A_CheeseNChips;
	states[thing].nextstate = S_CHEESE7;

	// Chip
	thing = S_CHIP1;
	states[thing].sprite = SPR_CHIP;
	states[thing].frame = 0;
	states[thing].tics = 1;
	states[thing].var1 = 2;
	states[thing].action.acp1 = (actionf_p1)A_CheeseNChips;
	states[thing].nextstate = S_CHIP2;

	thing = S_CHIP2;
	states[thing].sprite = SPR_CHIP;
	states[thing].frame = 0;
	states[thing].tics = 1;
	states[thing].var1 = 2;
	states[thing].action.acp1 = (actionf_p1)A_CheeseNChips;
	states[thing].nextstate = S_CHIP3;

	thing = S_CHIP3;
	states[thing].sprite = SPR_CHIP;
	states[thing].frame = 0;
	states[thing].tics = 1;
	states[thing].var1 = 2;
	states[thing].action.acp1 = (actionf_p1)A_CheeseNChips;
	states[thing].nextstate = S_CHIP4;

	thing = S_CHIP4;
	states[thing].sprite = SPR_CHIP;
	states[thing].frame = 1;
	states[thing].tics = 1;
	states[thing].var1 = 2;
	states[thing].action.acp1 = (actionf_p1)A_CheeseNChips;
	states[thing].nextstate = S_CHIP5;

	thing = S_CHIP5;
	states[thing].sprite = SPR_CHIP;
	states[thing].frame = 1;
	states[thing].tics = 1;
	states[thing].var1 = 2;
	states[thing].action.acp1 = (actionf_p1)A_CheeseNChips;
	states[thing].nextstate = S_CHIP6;

	thing = S_CHIP6;
	states[thing].sprite = SPR_CHIP;
	states[thing].frame = 1;
	states[thing].tics = 1;
	states[thing].var1 = 2;
	states[thing].action.acp1 = (actionf_p1)A_CheeseNChips;
	states[thing].nextstate = S_CHIP1;


	thing = S_CHIP7;
	states[thing].sprite = SPR_CHIP;
	states[thing].frame = 2;
	states[thing].tics = 1;
	states[thing].var1 = 2;
	states[thing].action.acp1 = (actionf_p1)A_CheeseNChips;
	states[thing].nextstate = S_CHIP8;

	thing = S_CHIP8;
	states[thing].sprite = SPR_CHIP;
	states[thing].frame = 2;
	states[thing].tics = 1;
	states[thing].var1 = 2;
	states[thing].action.acp1 = (actionf_p1)A_CheeseNChips;
	states[thing].nextstate = S_CHIP9;

	thing = S_CHIP9;
	states[thing].sprite = SPR_CHIP;
	states[thing].frame = 2;
	states[thing].tics = 1;
	states[thing].var1 = 2;
	states[thing].action.acp1 = (actionf_p1)A_CheeseNChips;
	states[thing].nextstate = S_CHIP10;

	thing = S_CHIP10;
	states[thing].sprite = SPR_CHIP;
	states[thing].frame = 3;
	states[thing].tics = 1;
	states[thing].var1 = 2;
	states[thing].action.acp1 = (actionf_p1)A_CheeseNChips;
	states[thing].nextstate = S_CHIP11;

	thing = S_CHIP11;
	states[thing].sprite = SPR_CHIP;
	states[thing].frame = 3;
	states[thing].tics = 1;
	states[thing].var1 = 2;
	states[thing].action.acp1 = (actionf_p1)A_CheeseNChips;
	states[thing].nextstate = S_CHIP12;

	thing = S_CHIP12;
	states[thing].sprite = SPR_CHIP;
	states[thing].frame = 3;
	states[thing].tics = 1;
	states[thing].var1 = 2;
	states[thing].action.acp1 = (actionf_p1)A_CheeseNChips;
	states[thing].nextstate = S_CHIP7;

	// Birdie
	thing = S_BIRDIE1;
	states[thing].sprite = SPR_BRDY;
	states[thing].frame = 0;
	states[thing].tics = 1;
	states[thing].var1 = 3;
	states[thing].action.acp1 = (actionf_p1)A_CheeseNChips;
	states[thing].nextstate = S_BIRDIE2;

	thing = S_BIRDIE2;
	states[thing].sprite = SPR_BRDY;
	states[thing].frame = 0;
	states[thing].tics = 1;
	states[thing].var1 = 3;
	states[thing].action.acp1 = (actionf_p1)A_CheeseNChips;
	states[thing].nextstate = S_BIRDIE3;

	thing = S_BIRDIE3;
	states[thing].sprite = SPR_BRDY;
	states[thing].frame = 0;
	states[thing].tics = 1;
	states[thing].var1 = 3;
	states[thing].action.acp1 = (actionf_p1)A_CheeseNChips;
	states[thing].nextstate = S_BIRDIE4;

	thing = S_BIRDIE4;
	states[thing].sprite = SPR_BRDY;
	states[thing].frame = 0;
	states[thing].tics = 1;
	states[thing].var1 = 3;
	states[thing].action.acp1 = (actionf_p1)A_CheeseNChips;
	states[thing].nextstate = S_BIRDIE5;

	thing = S_BIRDIE5;
	states[thing].sprite = SPR_BRDY;
	states[thing].frame = 1;
	states[thing].tics = 1;
	states[thing].var1 = 3;
	states[thing].action.acp1 = (actionf_p1)A_CheeseNChips;
	states[thing].nextstate = S_BIRDIE6;

	thing = S_BIRDIE6;
	states[thing].sprite = SPR_BRDY;
	states[thing].frame = 1;
	states[thing].tics = 1;
	states[thing].var1 = 3;
	states[thing].action.acp1 = (actionf_p1)A_CheeseNChips;
	states[thing].nextstate = S_BIRDIE7;

	thing = S_BIRDIE7;
	states[thing].sprite = SPR_BRDY;
	states[thing].frame = 1;
	states[thing].tics = 1;
	states[thing].var1 = 3;
	states[thing].action.acp1 = (actionf_p1)A_CheeseNChips;
	states[thing].nextstate = S_BIRDIE8;

	thing = S_BIRDIE8;
	states[thing].sprite = SPR_BRDY;
	states[thing].frame = 1;
	states[thing].tics = 1;
	states[thing].var1 = 3;
	states[thing].action.acp1 = (actionf_p1)A_CheeseNChips;
	states[thing].nextstate = S_BIRDIE1;

	thing = S_BIRDIE9;
	states[thing].sprite = SPR_BRDY;
	states[thing].frame = 2;
	states[thing].tics = 1;
	states[thing].var1 = 3;
	states[thing].action.acp1 = (actionf_p1)A_CheeseNChips;
	states[thing].nextstate = S_BIRDIE10;

	thing = S_BIRDIE10;
	states[thing].sprite = SPR_BRDY;
	states[thing].frame = 2;
	states[thing].tics = 1;
	states[thing].var1 = 3;
	states[thing].action.acp1 = (actionf_p1)A_CheeseNChips;
	states[thing].nextstate = S_BIRDIE11;

	thing = S_BIRDIE11;
	states[thing].sprite = SPR_BRDY;
	states[thing].frame = 2;
	states[thing].tics = 1;
	states[thing].var1 = 3;
	states[thing].action.acp1 = (actionf_p1)A_CheeseNChips;
	states[thing].nextstate = S_BIRDIE12;

	thing = S_BIRDIE12;
	states[thing].sprite = SPR_BRDY;
	states[thing].frame = 3;
	states[thing].tics = 1;
	states[thing].var1 = 3;
	states[thing].action.acp1 = (actionf_p1)A_CheeseNChips;
	states[thing].nextstate = S_BIRDIE13;

	thing = S_BIRDIE13;
	states[thing].sprite = SPR_BRDY;
	states[thing].frame = 3;
	states[thing].tics = 1;
	states[thing].var1 = 3;
	states[thing].action.acp1 = (actionf_p1)A_CheeseNChips;
	states[thing].nextstate = S_BIRDIE14;

	thing = S_BIRDIE14;
	states[thing].sprite = SPR_BRDY;
	states[thing].frame = 3;
	states[thing].tics = 1;
	states[thing].var1 = 3;
	states[thing].action.acp1 = (actionf_p1)A_CheeseNChips;
	states[thing].nextstate = S_BIRDIE9;

	// Hedgehog
	thing = S_HEDGEHOG;
	states[thing].sprite = SPR_HEDG;
	states[thing].frame = 0;
	states[thing].tics = -1;
	states[thing].nextstate = S_NULL;

	// Red Turtle Shell
	thing = S_RSHELL1;
	states[thing].sprite = SPR_SHL2;
	states[thing].frame = 0;
	states[thing].tics = 2;
	states[thing].action.acp1 = (actionf_p1)A_ThrownRing;
	states[thing].nextstate = S_RSHELL2;

	thing = S_RSHELL2;
	states[thing].sprite = SPR_SHL2;
	states[thing].frame = 1;
	states[thing].tics = 2;
	states[thing].action.acp1 = (actionf_p1)A_ThrownRing;
	states[thing].nextstate = S_RSHELL3;

	thing = S_RSHELL3;
	states[thing].sprite = SPR_SHL2;
	states[thing].frame = 2;
	states[thing].tics = 2;
	states[thing].action.acp1 = (actionf_p1)A_ThrownRing;
	states[thing].nextstate = S_RSHELL4;

	thing = S_RSHELL4;
	states[thing].sprite = SPR_SHL2;
	states[thing].frame = 3;
	states[thing].tics = 2;
	states[thing].action.acp1 = (actionf_p1)A_ThrownRing;
	states[thing].nextstate = S_RSHELL1;

	// Sideways Spring
	thing = S_SIDESPRING1;
	states[thing].sprite = SPR_SPRZ;
	states[thing].frame = 0;
	states[thing].tics = -1;
	states[thing].nextstate = S_NULL;

	thing = S_SIDESPRING2;
	states[thing].sprite = SPR_SPRZ;
	states[thing].frame = 1;
	states[thing].tics = 4;
	states[thing].action.acp1 = (actionf_p1)A_Pain;
	states[thing].nextstate = S_SIDESPRING3;

	thing = S_SIDESPRING3;
	states[thing].sprite = SPR_SPRZ;
	states[thing].frame = 2;
	states[thing].tics = 1;
	states[thing].nextstate = S_SIDESPRING4;

	thing = S_SIDESPRING4;
	states[thing].sprite = SPR_SPRZ;
	states[thing].frame = 3;
	states[thing].tics = 1;
	states[thing].nextstate = S_SIDESPRING5;

	thing = S_SIDESPRING5;
	states[thing].sprite = SPR_SPRZ;
	states[thing].frame = 3;
	states[thing].tics = 1;
	states[thing].nextstate = S_SIDESPRING1;

	// Tall Bush
	thing = S_BUSH1;
	states[thing].sprite = SPR_BUS2;
	states[thing].frame = 1;
	states[thing].tics = -1;
	states[thing].nextstate = S_NULL;

	// Twee
	thing = S_TWEE;
	states[thing].sprite = SPR_TWEE;
	states[thing].frame = 0;
	states[thing].tics = -1;
	states[thing].nextstate = S_NULL;

	// Hydrant
	thing = S_HYDRANT;
	states[thing].sprite = SPR_HYDR;
	states[thing].frame = 0;
	states[thing].tics = -1;
	states[thing].nextstate = S_NULL;

	// Toad Hopping
	thing = S_TOAD2;
	states[thing].sprite = SPR_TOAD;
	states[thing].frame = 1;
	states[thing].tics = 20;
	states[thing].action.acp1 = (actionf_p1)A_BunnyHop;
	states[thing].var1 = 7;
	states[thing].var2 = 0;
	states[thing].nextstate = S_TOAD;

	// Boo Hopping
	thing = S_BOO1;
	states[thing].sprite = SPR_BBOO;
	states[thing].frame = 0;
	states[thing].tics = 5;
	states[thing].nextstate = S_BOO2;

	thing = S_BOO2;
	states[thing].sprite = SPR_BBOO;
	states[thing].frame = 1;
	states[thing].tics = 20;
	states[thing].action.acp1 = (actionf_p1)A_BunnyHop;
	states[thing].var1 = 7;
	states[thing].var2 = 0;
	states[thing].nextstate = S_BOO1;

	// Goomba Hopping
	thing = S_GMBA1;
	states[thing].sprite = SPR_GMBA;
	states[thing].frame = 0;
	states[thing].tics = 5;
	states[thing].nextstate = S_GMBA2;

	thing = S_GMBA2;
	states[thing].sprite = SPR_GMBA;
	states[thing].frame = 1;
	states[thing].tics = 20;
	states[thing].action.acp1 = (actionf_p1)A_BunnyHop;
	states[thing].var1 = 7;
	states[thing].var2 = 0;
	states[thing].nextstate = S_GMBA1;

	// Shyguy Hopping
	thing = S_SHYG1;
	states[thing].sprite = SPR_SHYG;
	states[thing].frame = 0;
	states[thing].tics = 5;
	states[thing].nextstate = S_SHYG2;

	thing = S_SHYG2;
	states[thing].sprite = SPR_SHYG;
	states[thing].frame = 1;
	states[thing].tics = 20;
	states[thing].action.acp1 = (actionf_p1)A_BunnyHop;
	states[thing].var1 = 7;
	states[thing].var2 = 0;
	states[thing].nextstate = S_SHYG1;

	// Snifit Hopping
	thing = S_SNIF1;
	states[thing].sprite = SPR_SNIF;
	states[thing].frame = 0;
	states[thing].tics = 5;
	states[thing].nextstate = S_SNIF2;

	thing = S_SNIF2;
	states[thing].sprite = SPR_SNIF;
	states[thing].frame = 1;
	states[thing].tics = 20;
	states[thing].action.acp1 = (actionf_p1)A_BunnyHop;
	states[thing].var1 = 7;
	states[thing].var2 = 0;
	states[thing].nextstate = S_SNIF1;

	// GROUND!!!
	thing = S_GROUND1;
	states[thing].sprite = SPR_GRND;
	states[thing].frame = 0;
	states[thing].tics = 4;
	states[thing].nextstate = S_GROUND2;

	thing = S_GROUND2;
	states[thing].sprite = SPR_GRND;
	states[thing].frame = 1;
	states[thing].tics = 4;
	states[thing].nextstate = S_GROUND3;

	thing = S_GROUND3;
	states[thing].sprite = SPR_GRND;
	states[thing].frame = 2;
	states[thing].tics = 4;
	states[thing].nextstate = S_GROUND4;

	thing = S_GROUND4;
	states[thing].sprite = SPR_GRND;
	states[thing].frame = 3;
	states[thing].tics = 4;
	states[thing].nextstate = S_GROUND1;

	// Chrispy!
	thing = S_CHRISPY1;
	states[thing].sprite = SPR_RISP;
	states[thing].frame = 0;
	states[thing].tics = 4;
	states[thing].nextstate = S_CHRISPY2;

	thing = S_CHRISPY2;
	states[thing].sprite = SPR_RISP;
	states[thing].frame = 1;
	states[thing].tics = 4;
	states[thing].nextstate = S_CHRISPY3;

	thing = S_CHRISPY3;
	states[thing].sprite = SPR_RISP;
	states[thing].frame = 2;
	states[thing].tics = 4;
	states[thing].nextstate = S_CHRISPY4;

	thing = S_CHRISPY4;
	states[thing].sprite = SPR_RISP;
	states[thing].frame = 3;
	states[thing].tics = 4;
	states[thing].nextstate = S_CHRISPY1;

	// Senku!
	thing = S_SENKU1;
	states[thing].sprite = SPR_SENK;
	states[thing].frame = 0;
	states[thing].tics = -1;
	states[thing].nextstate = S_NULL;

	// Flame The Hedgehog!
	thing = S_FLAMETH1;
	states[thing].sprite = SPR_FLTH;
	states[thing].frame = 0;
	states[thing].tics = -1;
	states[thing].nextstate = S_NULL;

	///////////////
	////SPRITES////
	///////////////

	thing = SPR_SPED;
	strcpy(sprnames[thing],"SPED");
#ifdef HWRENDER
	t_lspr[thing] = &lspr[NOLIGHT];
#endif

	thing = SPR_FLYM;
	strcpy(sprnames[thing],"FLYM");
#ifdef HWRENDER
	t_lspr[thing] = &lspr[NOLIGHT];
#endif

	thing = SPR_POWR;
	strcpy(sprnames[thing],"POWR");
#ifdef HWRENDER
	t_lspr[thing] = &lspr[NOLIGHT];
#endif

	thing = SPR_DEVL;
	strcpy(sprnames[thing],"DEVL");
#ifdef HWRENDER
	t_lspr[thing] = &lspr[NOLIGHT];
#endif

	thing = SPR_ANGE;
	strcpy(sprnames[thing],"ANGE");
#ifdef HWRENDER
	t_lspr[thing] = &lspr[NOLIGHT];
#endif

	thing = SPR_ATTV;
	strcpy(sprnames[thing],"ATTV");
#ifdef HWRENDER
	t_lspr[thing] = &lspr[NOLIGHT];
#endif

	thing = SPR_RNDM;
	strcpy(sprnames[thing],"RNDM");
#ifdef HWRENDER
	t_lspr[thing] = &lspr[NOLIGHT];
#endif

	thing = SPR_KTEX;
	strcpy(sprnames[thing],"KTEX");
#ifdef HWRENDER
	t_lspr[thing] = &lspr[NOLIGHT];
#endif

	thing = SPR_MSTR;
	strcpy(sprnames[thing],"MSTR");
#ifdef HWRENDER
	t_lspr[thing] = &lspr[NOLIGHT];
#endif

	thing = SPR_SHEL;
	strcpy(sprnames[thing],"SHEL");
#ifdef HWRENDER
	t_lspr[thing] = &lspr[NOLIGHT];
#endif

	thing = SPR_RSHL;
	strcpy(sprnames[thing],"RSHL");
#ifdef HWRENDER
	t_lspr[thing] = &lspr[NOLIGHT];
#endif

	thing = SPR_NANA;
	strcpy(sprnames[thing],"NANA");
#ifdef HWRENDER
	t_lspr[thing] = &lspr[NOLIGHT];
#endif

	thing = SPR_PEEL;
	strcpy(sprnames[thing],"PEEL");
#ifdef HWRENDER
	t_lspr[thing] = &lspr[NOLIGHT];
#endif

	thing = SPR_FAKE;
	strcpy(sprnames[thing],"FAKE");
#ifdef HWRENDER
	t_lspr[thing] = &lspr[NOLIGHT];
#endif

	thing = SPR_FAIK;
	strcpy(sprnames[thing],"FAIK");
#ifdef HWRENDER
	t_lspr[thing] = &lspr[NOLIGHT];
#endif

	thing = SPR_BOOM;
	strcpy(sprnames[thing],"BOOM");
#ifdef HWRENDER
	t_lspr[thing] = &lspr[NOLIGHT];
#endif

	thing = SPR_BOMB;
	strcpy(sprnames[thing],"BOMB");
#ifdef HWRENDER
	t_lspr[thing] = &lspr[NOLIGHT];
#endif

	thing = SPR_SRTV;
	strcpy(sprnames[thing],"SRTV");
#ifdef HWRENDER
	t_lspr[thing] = &lspr[NOLIGHT];
#endif

	thing = SPR_GRTV;
	strcpy(sprnames[thing],"GRTV");
#ifdef HWRENDER
	t_lspr[thing] = &lspr[NOLIGHT];
#endif

	thing = SPR_RINV;
	strcpy(sprnames[thing],"RINV");
#ifdef HWRENDER
	t_lspr[thing] = &lspr[NOLIGHT];
#endif

	thing = SPR_YLBX;
	strcpy(sprnames[thing],"YLBX");
#ifdef HWRENDER
	t_lspr[thing] = &lspr[NOLIGHT];
#endif

	thing = SPR_TION;
	strcpy(sprnames[thing],"TION");
#ifdef HWRENDER
	t_lspr[thing] = &lspr[NOLIGHT];
#endif

	thing = SPR_RTEX;
	strcpy(sprnames[thing],"RTEX");
#ifdef HWRENDER
	t_lspr[thing] = &lspr[NOLIGHT];
#endif

	thing = SPR_SHL2;
	strcpy(sprnames[thing],"SHL2");
#ifdef HWRENDER
	t_lspr[thing] = &lspr[NOLIGHT];
#endif

	thing = SPR_BUNY;
	strcpy(sprnames[thing],"BUNY");
#ifdef HWRENDER
	t_lspr[thing] = &lspr[NOLIGHT];
#endif

	thing = SPR_MNKY;
	strcpy(sprnames[thing],"MNKY");
#ifdef HWRENDER
	t_lspr[thing] = &lspr[NOLIGHT];
#endif

	thing = SPR_EGLE;
	strcpy(sprnames[thing],"EGLE");
#ifdef HWRENDER
	t_lspr[thing] = &lspr[NOLIGHT];
#endif

	thing = SPR_CHKN;
	strcpy(sprnames[thing],"CHKN");
#ifdef HWRENDER
	t_lspr[thing] = &lspr[NOLIGHT];
#endif

	thing = SPR_PIGY;
	strcpy(sprnames[thing],"PIGY");
#ifdef HWRENDER
	t_lspr[thing] = &lspr[NOLIGHT];
#endif

	thing = SPR_SEEL;
	strcpy(sprnames[thing],"SEEL");
#ifdef HWRENDER
	t_lspr[thing] = &lspr[NOLIGHT];
#endif

	thing = SPR_PNGN;
	strcpy(sprnames[thing],"PNGN");
#ifdef HWRENDER
	t_lspr[thing] = &lspr[NOLIGHT];
#endif

	thing = SPR_TEDY;
	strcpy(sprnames[thing],"TEDY");
#ifdef HWRENDER
	t_lspr[thing] = &lspr[NOLIGHT];
#endif

	thing = SPR_TRTL;
	strcpy(sprnames[thing],"TRTL");
#ifdef HWRENDER
	t_lspr[thing] = &lspr[NOLIGHT];
#endif

	thing = SPR_FLAG;
	strcpy(sprnames[thing],"FLAG");
#ifdef HWRENDER
	t_lspr[thing] = &lspr[NOLIGHT];
#endif

	thing = SPR_PALM;
	strcpy(sprnames[thing],"PALM");
#ifdef HWRENDER
	t_lspr[thing] = &lspr[NOLIGHT];
#endif

	thing = SPR_CHES;
	strcpy(sprnames[thing],"CHES");
#ifdef HWRENDER
	t_lspr[thing] = &lspr[NOLIGHT];
#endif

	thing = SPR_CHIP;
	strcpy(sprnames[thing],"CHIP");
#ifdef HWRENDER
	t_lspr[thing] = &lspr[NOLIGHT];
#endif

	thing = SPR_BRDY;
	strcpy(sprnames[thing],"BRDY");
#ifdef HWRENDER
	t_lspr[thing] = &lspr[NOLIGHT];
#endif

	thing = SPR_HEDG;
	strcpy(sprnames[thing],"HEDG");
#ifdef HWRENDER
	t_lspr[thing] = &lspr[NOLIGHT];
#endif

	thing = SPR_SPRZ;
	strcpy(sprnames[thing],"SPRZ");
#ifdef HWRENDER
	t_lspr[thing] = &lspr[NOLIGHT];
#endif

	thing = SPR_TWEE;
	strcpy(sprnames[thing],"TWEE");
#ifdef HWRENDER
	t_lspr[thing] = &lspr[NOLIGHT];
#endif

	thing = SPR_HYDR;
	strcpy(sprnames[thing],"HYDR");
#ifdef HWRENDER
	t_lspr[thing] = &lspr[NOLIGHT];
#endif

	thing = SPR_BBOO;
	strcpy(sprnames[thing],"BBOO");
#ifdef HWRENDER
	t_lspr[thing] = &lspr[NOLIGHT];
#endif

	thing = SPR_GMBA;
	strcpy(sprnames[thing],"GMBA");
#ifdef HWRENDER
	t_lspr[thing] = &lspr[NOLIGHT];
#endif

	thing = SPR_SHYG;
	strcpy(sprnames[thing],"SHYG");
#ifdef HWRENDER
	t_lspr[thing] = &lspr[NOLIGHT];
#endif

	thing = SPR_SNIF;
	strcpy(sprnames[thing],"SNIF");
#ifdef HWRENDER
	t_lspr[thing] = &lspr[NOLIGHT];
#endif

	thing = SPR_GRND;
	strcpy(sprnames[thing],"GRND");
#ifdef HWRENDER
	t_lspr[thing] = &lspr[NOLIGHT];
#endif

	thing = SPR_RISP;
	strcpy(sprnames[thing],"RISP");
#ifdef HWRENDER
	t_lspr[thing] = &lspr[NOLIGHT];
#endif

	thing = SPR_SENK;
	strcpy(sprnames[thing],"SENK");
#ifdef HWRENDER
	t_lspr[thing] = &lspr[NOLIGHT];
#endif

	thing = SPR_FLTH;
	strcpy(sprnames[thing],"FLTH");
#ifdef HWRENDER
	t_lspr[thing] = &lspr[NOLIGHT];
#endif

	//////////////
	////SOUNDS////
	//////////////

	thing = sfx_boop;
	S_sfx[thing] = S_sfx[sfx_None];
	S_sfx[thing].name = "boop";
	S_sfx[thing].singularity = true;
	S_sfx[thing].priority = 64;

	thing = sfx_beep;
	S_sfx[thing] = S_sfx[sfx_None];
	S_sfx[thing].name = "beep";
	S_sfx[thing].singularity = true;
	S_sfx[thing].priority = 64;

	thing = sfx_menu2;
	S_sfx[thing] = S_sfx[sfx_None];
	S_sfx[thing].name = "menu2";
	S_sfx[thing].singularity = true;
	S_sfx[thing].priority = 1;

	thing = sfx_grind;
	S_sfx[thing] = S_sfx[sfx_None];
	S_sfx[thing].name = "grind";
	S_sfx[thing].priority = 1;

	thing = sfx_boost;
	S_sfx[thing] = S_sfx[sfx_None];
	S_sfx[thing].name = "boost";
	S_sfx[thing].priority = 1;

	thing = sfx_lvlup;
	S_sfx[thing] = S_sfx[sfx_None];
	S_sfx[thing].name = "lvlup";
	S_sfx[thing].priority = 1;

	thing = sfx_board;
	S_sfx[thing] = S_sfx[sfx_None];
	S_sfx[thing].name = "board";
	S_sfx[thing].priority = 48;
	S_sfx[thing].pitch = SF_X4AWAYSOUND;

	thing = sfx_bike;
	S_sfx[thing] = S_sfx[sfx_None];
	S_sfx[thing].name = "bike";
	S_sfx[thing].priority = 48;
	S_sfx[thing].pitch = SF_X4AWAYSOUND;

	thing = sfx_kart1;
	S_sfx[thing] = S_sfx[sfx_None];
	S_sfx[thing].name = "kart1";
	S_sfx[thing].priority = 48;
	S_sfx[thing].pitch = SF_X8AWAYSOUND;

	thing = sfx_kart2;
	S_sfx[thing] = S_sfx[sfx_None];
	S_sfx[thing].name = "kart2";
	S_sfx[thing].priority = 48;
	S_sfx[thing].pitch = SF_X8AWAYSOUND;

	thing = sfx_kart3;
	S_sfx[thing] = S_sfx[sfx_None];
	S_sfx[thing].name = "kart3";
	S_sfx[thing].priority = 48;
	S_sfx[thing].pitch = SF_X8AWAYSOUND;

	thing = sfx_jump2;
	S_sfx[thing] = S_sfx[sfx_None];
	S_sfx[thing].name = "jump2";
	S_sfx[thing].priority = 60;

	thing = sfx_skate;
	S_sfx[thing] = S_sfx[sfx_None];
	S_sfx[thing].name = "skate";
	S_sfx[thing].priority = 48;
	S_sfx[thing].pitch = SF_X8AWAYSOUND;

	thing = sfx_bounce;
	S_sfx[thing] = S_sfx[sfx_None];
	S_sfx[thing].name = "bounce";
	S_sfx[thing].priority = 48;
	S_sfx[thing].pitch = SF_X8AWAYSOUND;

	thing = sfx_sfloat;
	S_sfx[thing] = S_sfx[sfx_None];
	S_sfx[thing].name = "sfloat";
	S_sfx[thing].priority = 60;

	thing = sfx_sfall;
	S_sfx[thing] = S_sfx[sfx_None];
	S_sfx[thing].name = "sfall";
	S_sfx[thing].priority = 60;

	thing = sfx_popr;
	S_sfx[thing] = S_sfx[sfx_None];
	S_sfx[thing].name = "popr";
	S_sfx[thing].priority = 127;
	S_sfx[thing].pitch = SF_X4AWAYSOUND;

	thing = sfx_bmper2;
	S_sfx[thing] = S_sfx[sfx_None];
	S_sfx[thing].name = "bmper2";
	S_sfx[thing].priority = 127;

	thing = sfx_pulpul;
	S_sfx[thing] = S_sfx[sfx_None];
	S_sfx[thing].name = "pulpul";
	S_sfx[thing].priority = 64;

	thing = sfx_mlap;
	S_sfx[thing] = S_sfx[sfx_None];
	S_sfx[thing].name = "mlap";
	S_sfx[thing].singularity = true;
	S_sfx[thing].priority = 64;

	thing = sfx_bomb;
	S_sfx[thing] = S_sfx[sfx_None];
	S_sfx[thing].name = "bomb";
	S_sfx[thing].singularity = true;
	S_sfx[thing].priority = 64;

#ifdef _X_
	thing = sfx_mkitem1;
	S_sfx[thing] = S_sfx[sfx_None];
	S_sfx[thing].name = "mkitm1";
	S_sfx[thing].singularity = true;
	S_sfx[thing].priority = 72;

	thing = sfx_mkitem2;
	S_sfx[thing] = S_sfx[sfx_None];
	S_sfx[thing].name = "mkitm2";
	S_sfx[thing].singularity = true;
	S_sfx[thing].priority = 72;

	thing = sfx_mkitem3;
	S_sfx[thing] = S_sfx[sfx_None];
	S_sfx[thing].name = "mkitm3";
	S_sfx[thing].singularity = true;
	S_sfx[thing].priority = 72;

	thing = sfx_mkitem4;
	S_sfx[thing] = S_sfx[sfx_None];
	S_sfx[thing].name = "mkitm4";
	S_sfx[thing].singularity = true;
	S_sfx[thing].priority = 72;

	thing = sfx_mkitem5;
	S_sfx[thing] = S_sfx[sfx_None];
	S_sfx[thing].name = "mkitm5";
	S_sfx[thing].singularity = true;
	S_sfx[thing].priority = 72;

	thing = sfx_mkitem6;
	S_sfx[thing] = S_sfx[sfx_None];
	S_sfx[thing].name = "mkitm6";
	S_sfx[thing].singularity = true;
	S_sfx[thing].priority = 72;

	thing = sfx_mkitem7;
	S_sfx[thing] = S_sfx[sfx_None];
	S_sfx[thing].name = "mkitm7";
	S_sfx[thing].singularity = true;
	S_sfx[thing].priority = 72;

	thing = sfx_mkitem8;
	S_sfx[thing] = S_sfx[sfx_None];
	S_sfx[thing].name = "mkitm8";
	S_sfx[thing].singularity = true;
	S_sfx[thing].priority = 72;

	thing = sfx_mkitemF;
	S_sfx[thing] = S_sfx[sfx_None];
	S_sfx[thing].name = "mkitmf";
	S_sfx[thing].singularity = true;
	S_sfx[thing].priority = 72;
#endif

	/////////////
	////MUSIC////
	/////////////

	thing = mus_mfin;
	S_music[thing] = S_music[mus_None];
	S_music[thing].name = "mfin";

	thing = mus_rfin;
	S_music[thing] = S_music[mus_None];
	S_music[thing].name = "rfin";

	thing = mus_menu;
	S_music[thing] = S_music[mus_None];
	S_music[thing].name = "menu";

	thing = mus_opmenu;
	S_music[thing] = S_music[mus_None];
	S_music[thing].name = "opmenu";

	thing = mus_ngguu;
	S_music[thing] = S_music[mus_None];
	S_music[thing].name = "ngguu";
}
#endif
