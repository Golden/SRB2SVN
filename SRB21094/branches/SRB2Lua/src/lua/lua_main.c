#include <lua.h>
#include <lualib.h>
#include <lauxlib.h>

#include "lua_srb2base.h"
#include "lua_main.h"
#include "../doomdef.h"
#include "../w_wad.h"
#include "../z_zone.h"

lua_State *SRB2L = NULL;

typedef struct {
	int size;
	byte *data;
} rawlua_t;

static const luaL_Reg lualibs[] = {
  {"", luaopen_base},
//  {LUA_LOADLIBNAME, luaopen_package},
  {LUA_TABLIBNAME, luaopen_table},
//  {LUA_IOLIBNAME, luaopen_io},
//  {LUA_OSLIBNAME, luaopen_os},
  {LUA_STRLIBNAME, luaopen_string},
  {LUA_MATHLIBNAME, luaopen_math},
//  {LUA_DBLIBNAME, luaopen_debug},
  {"", luaopen_srb2base},
  {NULL, NULL}
};

static void OpenLibs(lua_State *L) {
  const luaL_Reg *lib = lualibs;
  for (; lib->func; lib++) {
    lua_pushcfunction(L,lib->func);
    lua_pushstring(L,lib->name);
    lua_call(L,1,0);
  }
}

FUNCNORETURN static ATTRNORETURN int LuaPanic(lua_State *L)
{
	I_Error("Internal Lua Error: %s\n",lua_tostring(L,-1));
#ifndef __GNUC__
	return 0;
#endif
}

static void OpenLua(void)
{
	SRB2L = luaL_newstate();
	lua_atpanic(SRB2L,LuaPanic);
	OpenLibs(SRB2L);
}

static const char *reader(lua_State *L, void *data, size_t *size)
{
	rawlua_t *f = data;
	byte *read = f->data;
	(void)L;
	*size = f->size;
	if (f->data) {
		Z_Free(f->data);
		f->data = NULL;
		f->size = 0;
	}
	return (void *)read;
}

static void Command_LuaFunc(void)
{
	size_t i;
	lua_getfield(SRB2L,LUA_REGISTRYINDEX,va("CON_CMD:%s",COM_Argv(0)));
	lua_pushnil(SRB2L); // TODO: Replace this with players[consoleplayer]
	for(i = 1; i < COM_Argc(); i++)
		lua_pushstring(SRB2L,COM_Argv(i+1));
	if (lua_pcall(SRB2L,COM_Argc(),0,0))
		CONS_Printf("Lua Error: %s\n",lua_tostring(SRB2L,-1));
}

static void PreLoad(lua_State *L)
{
	lua_newtable(L);
	lua_setglobal(L,"COMMAND");

	lua_newtable(L);
	lua_setglobal(L,"ACTION");
}

static void PostLoad(lua_State *L)
{
	lua_getglobal(L,"COMMAND");
		lua_pushnil(L);
		while(lua_next(L,-2)) {
			if (!lua_isstring(L,-2)) {
				// TODO: Lua error message here
				lua_pop(L,1);
				continue;
			}
			{
				const char *name = lua_tostring(L,-2);
				COM_AddCommand(name,Command_LuaFunc);
				lua_setfield(L,LUA_REGISTRYINDEX,va("CON_CMD:%s",name));
			}
		}
	lua_pop(L,1);

	lua_pushnil(L);
	lua_setglobal(L,"COMMAND");

	lua_getglobal(L,"ACTION");
	lua_pop(L,1);

	lua_pushnil(L);
	lua_setglobal(L,"ACTION");
}

void lua_LoadLump(int lump)
{
	rawlua_t f;
	if (!SRB2L)
		OpenLua();

	f.size = W_LumpLength(lump);
	f.data = ZZ_Alloc(f.size);
	W_ReadLump(lump, f.data);
	PreLoad(SRB2L);
	if (lua_load(SRB2L,reader,&f,"LuaLump") || lua_pcall(SRB2L,0,0,0))
		CONS_Printf("Lua Error: %s\n",lua_tostring(SRB2L,-1));
	PostLoad(SRB2L);
}
