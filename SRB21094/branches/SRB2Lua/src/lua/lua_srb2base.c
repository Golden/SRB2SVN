#include <lua.h>
#include <lualib.h>
#include <lauxlib.h>

#include "../doomdef.h"
#include "lua_srb2base.h"

static int luaB_print(lua_State *L)
{
	int n = lua_gettop(L);	/* number of arguments */
	int i;
	luaL_Buffer b;
	luaL_buffinit(L, &b);
	lua_getglobal(L, "tostring");
	for (i=1; i<=n; i++) {
		lua_pushvalue(L, -1);	/* function to be called */
		lua_pushvalue(L, i);	 /* value to print */
		lua_call(L, 1, 1);
		if (lua_tostring(L, -1) == NULL)
			return luaL_error(L, LUA_QL("tostring") " must return a string to "
													 LUA_QL("print"));
		if (i>1) luaL_addchar(&b,' ');
		luaL_addvalue(&b);
	}
	luaL_addchar(&b,'\n');
	luaL_pushresult(&b);

	CONS_Printf(lua_tostring(L,-1));
	return 0;
}

static const luaL_Reg base_funcs[] = {
	{"print", luaB_print},
	//{"CONS_Printf", luaB_print},
	{NULL, NULL}
};

int luaopen_srb2base(lua_State *L)
{
	luaL_register(L,"_G",base_funcs);
	return 1;
}
