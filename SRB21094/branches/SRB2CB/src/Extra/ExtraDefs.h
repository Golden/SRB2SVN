/*
 *  ExtraDefs.h
 *  SRB2CB
 *
 *
 */
 // This file allows us to use different game features, but are organized into group definitions,
 // this way, we can easily test new features without having to go back and comment out code if we don't want it to be compiled.
 // If something is defined, anything with an "#ifdef" and the definition's name is going to be compiled.

// SRB2CBTODO: this really needs #ifdef organizing
 
#define DIGGING // Knuckles can dig - for level teleports with digging
// New stuff
#define FREEFLY // Fly freely through a nights level, no eggcapsule or time limit
//#define SPEEDZONE // Sonic Boosting feature, faster speeds, etc, can be defined in a map header, defined, but to be re organized
#define NOLIVES // Gives the options of allowing modders to allow plaers to infinately try
#define JUMPSTUFF // Jump physics
#define NEWSECTORTYPES // New sector effects
#define NEWLINETYPES // Ne lindef effects
#define RUNTOP // Run on top of waterm can be defined per fof
#define SLIDING // Stafe shifting support
#define MISC // Other stuff
#define LIMITDRAW // Limit draw distance to speed up levels with large amounts of objects
#define OTHERGLFIXES
#define CBGLFIXES
#define CBBUGFIXES

// Imports from other mods here, any imports extend the features implemented from other mods
#ifndef _WIN32_WCE
// Allows us to use a day to night feature - from Shufflarb2
#define DAYTONIGHT
#endif
#define TRANSFIX // JTE's translation fix thingy
// bots don't crash anymore, but they may need somehow need better memory management because they may cause allocation errors 
// Only thing left to fix is coop bots being Null when their owner dies in a level with noreload in it
//#define JTEBOTS
// This is for bots in race mode! Bots must already be defined as above of course, and this doesn't currently work yet
//#define BOTWAYPOINTS
//#define WATERWAVING // this is used but not defined
#define SEENAMES
// Shuffrb2's particles, now soccable and stuff
#define PARTICLES // gameplay particles SRB2CBTODO: unstable with some things, need to switch to a better method
#ifdef PARTICLES
#define PARTICLES2 // extra effects
#endif
//#define ANGLE2D // SRB2CBTODO! 2D mode in nights, seperate once complete, current replaces nights mode






 
