/*
 *  m_vectors.h
 *  SRB2CB
 *
 *  Vector math routines for SRB2
 *  Imported directly from ZDoom's v_vector.h, Copyright Randy Heit
 *  
 *
 */
// This file must be converted to plain C in order to be compatabile with the rest of SRB2
// A p_slopes.cpp file currently uses this header, once this header is converted to C,
// the function in p_slopes.c can be used as a static function in p_spec.c, and the lindef case
// 181 can use it correctly for slope setup

#include "doomdef.h"
#include "m_fixed.h"

#ifdef ZSLOPE


#define EQUAL_EPSILON (1/FRACUNIT)
#ifdef __cplusplus

template<class vec_t> struct TRotator;
template<class vec_t> struct TVector3;

template<class vec_t>
struct TVector2
{
	vec_t X, Y;
	
	TVector2 ()
	{
	}
	
	TVector2 (double a, double b)
	: X(a), Y(b)
	{
	}
	
	TVector2 (const TVector2 &other)
	: X(other.X), Y(other.Y)
	{
	}
	
	TVector2 (const TVector3<vec_t> &other)	// Copy the X and Y from the 3D vector and discard the Z
	: X(other.X), Y(other.Y)
	{
	}
	
	void Zero()
	{
		Y = X = 0;
	}
	
	TVector2 &operator= (const TVector2 &other)
	{
		// This might seem backwards, but this helps produce smaller code when a newly
		// created vector is assigned, because the components can just be popped off
		// the FPU stack in order without the need for fxch. For platforms with a
		// more sensible registered-based FPU, of course, the order doesn't matter.
		// (And, yes, I know fxch can improve performance in the right circumstances,
		// but this isn't one of those times. Here, it's little more than a no-op that
		// makes the exe 2 bytes larger whenever you assign one vector to another.)
		Y = other.Y, X = other.X;
		return *this;
	}
	
	// Access X and Y as an array
	vec_t &operator[] (int index)
	{
		return *(&X + index);
	}
	
	const vec_t &operator[] (int index) const
	{
		return *(&X + index);
	}
	
	// Test for equality
	bool operator== (const TVector2 &other) const
	{
		return X == other.X && Y == other.Y;
	}
	
	// Test for inequality
	bool operator!= (const TVector2 &other) const
	{
		return X != other.X || Y != other.Y;
	}
	
	// Test for approximate equality
	bool ApproximatelyEquals (const TVector2 &other) const
	{
		return fabs(X - other.X) < EQUAL_EPSILON && fabs(Y - other.Y) < EQUAL_EPSILON;
	}
	
	// Test for approximate inequality
	bool DoesNotApproximatelyEqual (const TVector2 &other) const
	{
		return fabs(X - other.X) >= EQUAL_EPSILON || fabs(Y - other.Y) >= EQUAL_EPSILON;
	}
	
	// Unary negation
	TVector2 operator- () const
	{
		return TVector2(-X, -Y);
	}
	
	// Scalar addition
	TVector2 &operator+= (double scalar)
	{
		X += scalar, Y += scalar;
		return *this;
	}
	
	friend TVector2 operator+ (const TVector2 &v, double scalar)
	{
		return TVector2(v.X + scalar, v.Y + scalar);
	}
	
	friend TVector2 operator+ (double scalar, const TVector2 &v)
	{
		return TVector2(v.X + scalar, v.Y + scalar);
	}
	
	// Scalar subtraction
	TVector2 &operator-= (double scalar)
	{
		X -= scalar, Y -= scalar;
		return *this;
	}
	
	TVector2 operator- (double scalar) const
	{
		return TVector2(X - scalar, Y - scalar);
	}
	
	// Scalar multiplication
	TVector2 &operator*= (double scalar)
	{
		X *= scalar, Y *= scalar;
		return *this;
	}
	
	friend TVector2 operator* (const TVector2 &v, double scalar)
	{
		return TVector2(v.X * scalar, v.Y * scalar);
	}
	
	friend TVector2 operator* (double scalar, const TVector2 &v)
	{
		return TVector2(v.X * scalar, v.Y * scalar);
	}
	
	// Scalar division
	TVector2 &operator/= (double scalar)
	{
		scalar = 1 / scalar, X *= scalar, Y *= scalar;
		return *this;
	}
	
	TVector2 operator/ (double scalar) const
	{
		scalar = 1 / scalar;
		return TVector2(X * scalar, Y * scalar);
	}
	
	// Vector addition
	TVector2 &operator+= (const TVector2 &other)
	{
		X += other.X, Y += other.Y;
		return *this;
	}
	
	TVector2 operator+ (const TVector2 &other) const
	{
		return TVector2(X + other.X, Y + other.Y);
	}
	
	// Vector subtraction
	TVector2 &operator-= (const TVector2 &other)
	{
		X -= other.X, Y -= other.Y;
		return *this;
	}
	
	TVector2 operator- (const TVector2 &other) const
	{
		return TVector2(X - other.X, Y - other.Y);
	}
	
	// Vector length
	double Length() const
	{
		return sqrt (X*X + Y*Y);
	}
	
	double LengthSquared() const
	{
		return X*X + Y*Y;
	}
	
	// Return a unit vector facing the same direction as this one
	TVector2 Unit() const
	{
		double len = Length();
		if (len != 0) len = 1 / len;
		return *this * len;
	}
	
	// Scales this vector into a unit vector
	void MakeUnit()
	{
		double len = Length();
		if (len != 0) len = 1 / len;
		*this *= len;
	}
	
	// Dot product
	double operator | (const TVector2 &other) const
	{
		return X*other.X + Y*other.Y;
	}
	
	// Returns the angle (in radians) that the ray (0,0)-(X,Y) faces
	double Angle() const
	{
		return atan2 (X, Y);
	}
	
	// Returns a rotated vector. TAngle is in radians.
	TVector2 Rotated (double angle)
	{
		double cosval = cos (angle);
		double sinval = sin (angle);
		return TVector2(X*cosval - Y*sinval, Y*cosval + X*sinval);
	}
	
	// Returns a vector rotated 90 degrees clockwise.
	TVector2 Rotated90CW()
	{
		return TVector2(Y, -X);
	}
	
	// Returns a vector rotated 90 degrees counterclockwise.
	TVector2 Rotated90CCW()
	{
		return TVector2(-Y, X);
	}
};

template<class vec_t>
struct TVector3
{
	typedef TVector2<vec_t> Vector2;
	
	vec_t X, Y, Z;
	
	TVector3 ()
	{
	}
	
	TVector3 (double a, double b, double c)
	: X(vec_t(a)), Y(vec_t(b)), Z(vec_t(c))
	{
	}
	
	TVector3 (const TVector3 &other)
	: X(other.X), Y(other.Y), Z(other.Z)
	{
	}
	
	TVector3 (const Vector2 &xy, double z)
	: X(xy.X), Y(xy.Y), Z(z)
	{
	}
	
	TVector3 (const TRotator<vec_t> &rot);
	
	void Zero()
	{
		Z = Y = X = 0;
	}
	
	TVector3 &operator= (const TVector3 &other)
	{
		Z = other.Z, Y = other.Y, X = other.X;
		return *this;
	}
	
	// Access X and Y and Z as an array
	vec_t &operator[] (int index)
	{
		return *(&X + index);
	}
	
	const vec_t &operator[] (int index) const
	{
		return *(&X + index);
	}
	
	// Test for equality
	bool operator== (const TVector3 &other) const
	{
		return X == other.X && Y == other.Y && Z == other.Z;
	}
	
	// Test for inequality
	bool operator!= (const TVector3 &other) const
	{
		return X != other.X || Y != other.Y || Z != other.Z;
	}
	
	// Test for approximate equality
	bool ApproximatelyEquals (const TVector3 &other) const
	{
		return fabs(X - other.X) < EQUAL_EPSILON && fabs(Y - other.Y) < EQUAL_EPSILON && fabs(Z - other.Z) < EQUAL_EPSILON;
	}
	
	// Test for approximate inequality
	bool DoesNotApproximatelyEqual (const TVector3 &other) const
	{
		return fabs(X - other.X) >= EQUAL_EPSILON || fabs(Y - other.Y) >= EQUAL_EPSILON || fabs(Z - other.Z) >= EQUAL_EPSILON;
	}
	
	// Unary negation
	TVector3 operator- () const
	{
		return TVector3(-X, -Y, -Z);
	}
	
	// Scalar addition
	TVector3 &operator+= (double scalar)
	{
		X += scalar, Y += scalar, Z += scalar;
		return *this;
	}
	
	friend TVector3 operator+ (const TVector3 &v, double scalar)
	{
		return TVector3(v.X + scalar, v.Y + scalar, v.Z + scalar);
	}
	
	friend TVector3 operator+ (double scalar, const TVector3 &v)
	{
		return TVector3(v.X + scalar, v.Y + scalar, v.Z + scalar);
	}
	
	// Scalar subtraction
	TVector3 &operator-= (double scalar)
	{
		X -= scalar, Y -= scalar, Z -= scalar;
		return *this;
	}
	
	TVector3 operator- (double scalar) const
	{
		return TVector3(X - scalar, Y - scalar, Z - scalar);
	}
	
	// Scalar multiplication
	TVector3 &operator*= (double scalar)
	{
		X = vec_t(X *scalar), Y = vec_t(Y * scalar), Z = vec_t(Z * scalar);
		return *this;
	}
	
	friend TVector3 operator* (const TVector3 &v, double scalar)
	{
		return TVector3(v.X * scalar, v.Y * scalar, v.Z * scalar);
	}
	
	friend TVector3 operator* (double scalar, const TVector3 &v)
	{
		return TVector3(v.X * scalar, v.Y * scalar, v.Z * scalar);
	}
	
	// Scalar division
	TVector3 &operator/= (double scalar)
	{
		scalar = 1 / scalar, X = vec_t(X * scalar), Y = vec_t(Y * scalar), Z = vec_t(Z * scalar);
		return *this;
	}
	
	TVector3 operator/ (double scalar) const
	{
		scalar = 1 / scalar;
		return TVector3(X * scalar, Y * scalar, Z * scalar);
	}
	
	// Vector addition
	TVector3 &operator+= (const TVector3 &other)
	{
		X += other.X, Y += other.Y, Z += other.Z;
		return *this;
	}
	
	TVector3 operator+ (const TVector3 &other) const
	{
		return TVector3(X + other.X, Y + other.Y, Z + other.Z);
	}
	
	// Vector subtraction
	TVector3 &operator-= (const TVector3 &other)
	{
		X -= other.X, Y -= other.Y, Z - other.Z;
		return *this;
	}
	
	TVector3 operator- (const TVector3 &other) const
	{
		return TVector3(X - other.X, Y - other.Y, Z - other.Z);
	}
	
	// Add a 2D vector to this 3D vector, leaving Z unchanged.
	TVector3 &operator+= (const Vector2 &other)
	{
		X += other.X, Y += other.Y;
		return *this;
	}
	
	// Subtract a 2D vector from this 3D vector, leaving Z unchanged.
	TVector3 &operator-= (const Vector2 &other)
	{
		X -= other.X, Y -= other.Y;
		return *this;
	}
	
	// Add a 3D vector and a 2D vector.
	// Discards the Z component of the 3D vector and returns a 2D vector.
	friend Vector2 operator+ (const TVector3 &v3, const Vector2 &v2)
	{
		return Vector2(v3.X + v2.X, v3.Y + v2.Y);
	}
	
	friend Vector2 operator+ (const Vector2 &v2, const TVector3 &v3)
	{
		return Vector2(v2.X + v3.X, v2.Y + v3.Y);
	}
	
	// Subtract a 3D vector and a 2D vector.
	// Discards the Z component of the 3D vector and returns a 2D vector.
	friend Vector2 operator- (const TVector3 &v3, const Vector2 &v2)
	{
		return Vector2(v3.X - v2.X, v3.Y - v2.Y);
	}
	
	friend Vector2 operator- (const TVector2<vec_t> &v2, const TVector3 &v3)
	{
		return Vector2(v2.X - v3.X, v2.Y - v3.Y);
	}
	
	// Vector length
	double Length() const
	{
		return sqrt (X*X + Y*Y + Z*Z);
	}
	
	double LengthSquared() const
	{
		return X*X + Y*Y + Z*Z;
	}
	
	// Return a unit vector facing the same direction as this one
	TVector3 Unit() const
	{
		double len = Length();
		if (len != 0) len = 1 / len;
		return *this * len;
	}
	
	// Scales this vector into a unit vector
	void MakeUnit()
	{
		double len = Length();
		if (len != 0) len = 1 / len;
		*this *= len;
	}
	
	// Resizes this vector to be the specified length (if it is not 0)
	TVector3 &Resize(double len)
	{
		double nowlen = Length();
		X = vec_t(X * (len /= nowlen));
		Y = vec_t(Y * len);
		Z = vec_t(Z * len);
		return *this;
	}
	
	// Dot product
	double operator | (const TVector3 &other) const
	{
		return X*other.X + Y*other.Y + Z*other.Z;
	}
	
	// Cross product
	TVector3 operator ^ (const TVector3 &other) const
	{
		return TVector3(Y*other.Z - Z*other.Y,
						Z*other.X - X*other.Z,
						X*other.Y - Y*other.X);
	}
	
	TVector3 &operator ^= (const TVector3 &other)
	{
		*this = *this ^ other;
	}
};

typedef TVector2<float>		FVector2;
typedef TVector3<float>		FVector3;
typedef TRotator<float>		FRotator;
#endif
#endif



