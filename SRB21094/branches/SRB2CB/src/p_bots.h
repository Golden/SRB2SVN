/*
 *  p_bots.h
 *  SRB2CB
 *
 *  JTEBOTS
 *  By: JTE (Jason the Echidna)
 *
 */

#include "Extra/ExtraDefs.h"

#ifdef JTEBOTS
#ifndef __JB_BOT__
#define __JB_BOT__
#include "r_defs.h"

typedef struct botdontlook_s
	{
		mobj_t *data; // all the information the bot's target list has
		short timer; // how long left in the handling of this target
		struct botdontlook_s *next; // your next target
		struct botdontlook_s *last; // your last target
	} botdontlook_t;
#ifdef BOTWAYPOINTS
typedef struct botwaypoint_s
	{
		fixed_t x,y,z;
		sector_t *sec;
		boolean springpoint;
		struct botwaypoint_s *next;
	} botwaypoint_t;
#endif
typedef struct bot_s
	{
		player_t* player; // Your player struct
		byte ownernum; // Your owner's number
		boolean springmove; // If you hit a diagonal spring or not
		mobj_t* target; // The mobj you're following
#ifdef BOTWAYPOINTS
		botwaypoint_t *waypoint; // Your waypoint in race
		fixed_t waydist; // Distance from the last waypoint to the next one
#endif
		short targettimer; // How long you've been trying to get to the same mobj
		botdontlook_t *targetlist; // Don't look at these mobjs again until timer runs out.
		botdontlook_t *dontlook; // seperate thingy to switch to for the targetlist
	} bot_t;

extern bot_t bots[MAXPLAYERS];
extern char charselbots[15][16];
#endif
#endif


