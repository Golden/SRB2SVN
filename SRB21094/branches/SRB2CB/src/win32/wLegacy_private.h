/* THIS FILE WILL BE OVERWRITTEN BY DEV-C++ */
/* DO NOT EDIT ! */

#ifndef WLEGACY_PRIVATE_H
#define WLEGACY_PRIVATE_H

/* VERSION DEFINITIONS */
#define VER_STRING	"1.0.2.0"
#define VER_MAJOR	1
#define VER_MINOR	0
#define VER_RELEASE	2
#define VER_BUILD	0
#define COMPANY_NAME	"SRB2 Community"
#define FILE_VERSION	"1.02CB"
#define FILE_DESCRIPTION	"SRB2 - Community Build"
#define INTERNAL_NAME	"SRB2CB"
#define LEGAL_COPYRIGHT	"Copyright � 1998-2008 Sonic Team Junior"
#define LEGAL_TRADEMARKS	"Sonic the Hedgehog and related characters are trademarks of Sega."
#define ORIGINAL_FILENAME	"SRB2CB.exe"
#define PRODUCT_NAME	"SRB2CB"
#define PRODUCT_VERSION	"1.02CB"

#endif /*WLEGACY_PRIVATE_H*/
