/*
 *  p_slope.cpp
 *  SRB2CB
 *
 *  Imported from ZDoom
 *
 */

#include "m_vectors.h" // Needs to be converted to a C compatible header file, then #ifdef ZSLOPE can be defined
#include "p_slopes.h"
#include "r_defs.h"
#include "doomdef.h"

#ifdef ZSLOPE



//===========================================================================
//
// [RH] Set slopes for sectors, based on line specials
//
// P_AlignPlane
//
// Aligns the floor or ceiling of a sector to the corresponding plane
// on the other side of the reference line. (By definition, line must be
// two-sided.)
//
// If (which & 1), sets floor.
// If (which & 2), sets ceiling.
//
//===========================================================================
//
// Use "int which" with either linedef flag, or 2 linedef slope setting effect numbers
//
//======================================
//
void P_AlignPlane(sector_t *sec, line_t *line, int which)
{
	sector_t *refsec;
	int bestdist;
	vertex_t *refvert = (*sec->lines)->v1;	// Shut up, GCC
	int i;
	line_t **probe;
	
	if (!line->backsector)
		return;
	
	// Find furthest vertex from the reference line. It, along with the two ends
	// of the line will define the plane.
	bestdist = 0;
	for (i = sec->linecount*2, probe = sec->lines; i > 0; i--)
	{
		int dist;
		vertex_t *vert;
		
		// Do calculations with only the upper bits, because the lower ones
		// are all zero, and we would overflow for a lot of distances if we
		// kept them around.
		
		if (i & 1)
			vert = (*probe++)->v2;
		else
			vert = (*probe)->v1;
		dist = abs (((line->v1->y - vert->y) >> FRACBITS) * (line->dx >> FRACBITS) -
					((line->v1->x - vert->x) >> FRACBITS) * (line->dy >> FRACBITS));
		
		if (dist > bestdist)
		{
			bestdist = dist;
			refvert = vert;
		}
	}
	
	refsec = line->frontsector == sec ? line->backsector : line->frontsector;
	
	// get ZDoom's usage of FVector3 and import it to SRB2
	
	FVector3 p, v1, v2, cross;
	
	// extend secplane's use in srb2
	const secplane_t *refplane;
	secplane_t *srcplane;
	fixed_t srcheight, destheight;
	
	refplane = (which == 0) ? &refsec->floorplane : &refsec->ceilingplane;
	srcplane = (which == 0) ? &sec->floorplane : &sec->ceilingplane;
	// find out what gettexz does in zdoom, and use it in SRB2
	// edit: yes, this is correctly assosciated
	srcheight = (which == 0) ? sec->floorheight : sec->ceilingheight;
	destheight = (which == 0) ? refsec->floorheight : refsec->ceilingheight;
	
	// change "FIXED2FLOAT" to srb2's built in FIXED_TO_FLOAT; p, v1, and cross takes care of themselves if FVector3 is defined
	p[0] = FIXED_TO_FLOAT (line->v1->x);
	p[1] = FIXED_TO_FLOAT (line->v1->y);
	p[2] = FIXED_TO_FLOAT (destheight);
	v1[0] = FIXED_TO_FLOAT (line->dx);
	v1[1] = FIXED_TO_FLOAT (line->dy);
	v1[2] = 0;
	v2[0] = FIXED_TO_FLOAT (refvert->x - line->v1->x);
	v2[1] = FIXED_TO_FLOAT (refvert->y - line->v1->y);
	v2[2] = FIXED_TO_FLOAT (srcheight - destheight);
	
	// Overloaded inline call, SRB2 needs to support this correctly
	cross = (v1^v2).Unit();
	
	// Fix backward normals
	if ((cross.Z < 0 && which == 0) || (cross.Z > 0 && which == 1))
	{
		cross = -cross;
	}
	
// srcplane is the secplane_t output of the function	
	
	srcplane->a = FLOAT2FIXED (cross[0]);
	srcplane->b = FLOAT2FIXED (cross[1]);
	srcplane->c = FLOAT2FIXED (cross[2]);
	srcplane->ic = DivScale32 (1, srcplane->c);
	srcplane->d = -TMulScale16 (srcplane->a, line->v1->x,
								srcplane->b, line->v1->y,
								srcplane->c, destheight);
}

#endif



