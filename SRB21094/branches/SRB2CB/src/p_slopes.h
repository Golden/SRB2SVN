/*
 *  p_slopes.h
 *  SRB2CB
 *
 * ZDoom import
 *
 */

#include "r_defs.h"
#include "doomdef.h"

#ifdef ZSLOPE


#ifdef __cplusplus
extern "C"
{
#endif
	
void P_AlignPlane(sector_t * sec, line_t * line, int which);
	
#ifdef __cplusplus
}
#endif

#endif // ZSLOPE

