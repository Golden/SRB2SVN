// Emacs style mode select   -*- C++ -*- 
//-----------------------------------------------------------------------------
//
// Copyright (C) 1998-2000 by DooM Legacy Team.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
//-----------------------------------------------------------------------------
/// \file
/// \brief imports/exports for the 3D hardware low-level interface API

#ifndef __HWR_DRV_H__
#define __HWR_DRV_H__

// this must be here 19991024 by Kin
#include "../screen.h"
#include "hw_data.h"
#include "hw_defs.h"
#include "hw_md2.h"

#include "hw_dll.h"
#include "../Extra/ExtraDefs.h"

// ==========================================================================
//                                                       STANDARD DLL EXPORTS
// ==========================================================================

#ifdef SDL
#undef VID_X11
#endif

boolean Init(I_Error_t ErrorFunction);
#ifndef SDL
void GL_Shutdown(void);
#endif
#ifdef _WINDOWS
void GetModeList(vmode_t **pvidmodes, int *numvidmodes);
#endif
#ifdef VID_X11
Window HWRAPI(HookXwin) (Display *, int, int, boolean);
#endif
#if defined (PURESDL) || defined (macintosh)
void GL_SetPalette(int *, RGBA_t *gamma);
#else
void GL_SetPalette (RGBA_t *pal, RGBA_t *gamma);
#endif
void FinishUpdate(int waitvbl);
void Draw2DLine(F2DCoord *v1, F2DCoord *v2, RGBA_t Color);
void DrawPolygon(FSurfaceInfo *pSurf, FOutVector *pOutVerts, FUINT iNumPts, FBITFIELD PolyFlags);
void SetBlend(FBITFIELD PolyFlags);
void ClearBuffer(FBOOLEAN ColorMask, FBOOLEAN DepthMask, FRGBAFloat *ClearColor);
void SetTexture(FTextureInfo *TexInfo);
void ReadRect(int x, int y, int width, int height, int dst_stride, unsigned short *dst_data);
void GClipRect(int minx, int miny, int maxx, int maxy, float nearclip);
void ClearMipMapCache(void);

//Hurdler: added for backward compatibility
void SetSpecialState(hwdspecialstate_t IdState, int Value);

//Hurdler: added for new development
void DrawMD2(int *gl_cmd_buffer, md2_frame_t *frame, FTransform *pos, float scale);
void SetTransform(FTransform *transform);
int GetTextureUsed(void);
int GetRenderVersion(void);
// SRB2CBTODO: get rid of unneeded arguments
void WaterWaving(float points[45][45][2]);
void MotionBlur(float points[45][45][2], int blur);
void GLScreenFlip(float points[45][45][2]);
void FreezeScreen(float points[45][45][2]);
void HeatWave(float points[45][45][2]);
void ScreenShake(float points[45][45][2]);

#ifdef VID_X11 // ifdef to be removed as soon as windoze supports that as well
// metzgermeister: added for Voodoo detection
char *GetRenderer(void);
#endif

// ==========================================================================
//                                      HWR DRIVER OBJECT, FOR CLIENT PROGRAM
// ==========================================================================

#if !defined (_CREATE_DLL_)

//Hurdler: 16/10/99: added for OpenGL gamma correction
//extern RGBA_t  gamma_correction;

#define HWD hwdriver

#endif //not defined _CREATE_DLL_

#endif //__HWR_DRV_H__

