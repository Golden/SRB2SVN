// Emacs style mode select   -*- C++ -*- 
//-----------------------------------------------------------------------------
//
// Copyright (C) 1998-2000 by DooM Legacy Team.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//-----------------------------------------------------------------------------
/// \file
/// \brief Dynamic/Static lighting & coronas add on by Hurdler
// !! Finished implementation :D !!

#include "hw_light.h"
#include "hw_drv.h"
#include "../i_video.h"
#include "../z_zone.h"
#include "../m_random.h"
#include "../m_bbox.h"
#include "../w_wad.h"
#include "../r_state.h"
#include "../r_main.h"
#include "../p_local.h"
#include "../Extra/ExtraDefs.h"

//=============================================================================
//                                                                      DEFINES
//=============================================================================

//static dynlights_t view_dynlights[2]; // 2 players in splitscreen mode
//static dynlights_t *dynlights = &view_dynlights[0];

#define UNDEFINED_SPR   0x0 // actually just for testing
#define CORONA_SPR      0x1 // a light source which only emit a corona
#define DYNLIGHT_SPR    0x2 // a light source which is only used for dynamic lighting
#define LIGHT_SPR       (DYNLIGHT_SPR|CORONA_SPR)
#define ROCKET_SPR      (DYNLIGHT_SPR|CORONA_SPR|0x10)

//Hurdler: now we can change those values via FS :)
light_t lspr[NUMLIGHTS] =
{
	// type       coronas color, c_size,light color,l_radius
	// UNDEFINED: 0  
	{ UNDEFINED_SPR,  0x00000000,  24.0f},
	// weapons
	// RINGSPARK_L
	{ LIGHT_SPR,      0x0000e0ff,  16.0f}, // Tails 09-08-2002
	// SUPERSONIC_L
	{ DYNLIGHT_SPR,   0xff00e0ff,  32.0f}, // Tails 09-08-2002
	// SUPERSPARK_L
	{ LIGHT_SPR,      0xe0ffffff,   8.0f},
	// INVINCIBLE_L
	{ DYNLIGHT_SPR,   0x10ffaaaa,  16.0f},
	// GREENSHIELD_L
	{ DYNLIGHT_SPR,   0x602b7337,  32.0f},
	// BLUESHIELD_L
	{ DYNLIGHT_SPR,   0x60cb0000,  32.0f},

	// tall lights
	// YELLOWSHIELD_L
	{ DYNLIGHT_SPR,   0x601f7baf,  32.0f},

	// REDSHIELD_L
	{ DYNLIGHT_SPR,   0x600000cb,  32.0f},

	// BLACKSHIELD_L // Black light?
	{ DYNLIGHT_SPR,   0x60010101,  32.0f},

	// WHITESHIELD_L
	{ DYNLIGHT_SPR,   0x60ffffff,  32.0f},

	// SMALLREDBALL_L
	{ DYNLIGHT_SPR,   0x606060f0,   0.0f},

	// small lights
	// RINGLIGHT_L
	{ DYNLIGHT_SPR,   0x60b0f0f0,   0.0f},
	// GREENSMALL_L
	{    LIGHT_SPR,   0x6070ff70,  60.0f},
	// REDSMALL_L
	{    LIGHT_SPR,   0x705070ff,  60.0f},

	// type       coronas color, c_size,light color,l_radius
	// GREENSHINE_L
	{    LIGHT_SPR,   0xff00ff00,  64.0f},
	// ORANGESHINE_L
	{    LIGHT_SPR,   0xff0080ff,  64.0f},
	// PINKSHINE_L
	{    LIGHT_SPR,   0xffe080ff,  64.0f},
	// BLUESHINE_L
	{    LIGHT_SPR,   0xffff0000,  64.0f},
	// REDSHINE_L
	{    LIGHT_SPR,   0xff0000ff,  64.0f},
	// LBLUESHINE_L
	{    LIGHT_SPR,   0xffff8080,  64.0f},
	// GREYSHINE_L
	{    LIGHT_SPR,   0xffe0e0e0,  64.0f},

	// monsters
	// REDBALL_L
	{ DYNLIGHT_SPR,   0x606060ff,   0.0f},
	// GREENBALL_L
	{ DYNLIGHT_SPR,   0x6060ff60, 120.0f},
	// BLUEBALL_L
	{ DYNLIGHT_SPR,   0x60ff6060, 120.0f},

	// NIGHTSLIGHT_L
	{    LIGHT_SPR,   0x60ffffff,  16.0f},

	// JETLIGHT_L
	{ DYNLIGHT_SPR,   0x60ffaaaa,  16.0f},

	// GOOPLIGHT_L
	{ DYNLIGHT_SPR,   0x60ff00ff,  16.0f},

	// STREETLIGHT_L
	{    LIGHT_SPR,   0xffe0e0e0,  64.0f},
};

light_t *t_lspr[NUMSPRITES] =
{
	&lspr[NOLIGHT],     // SPR_MISL
	&lspr[NOLIGHT],     // SPR_PLAY
	&lspr[NOLIGHT],     // SPR_POSS
	&lspr[NOLIGHT],     // SPR_SPOS
	&lspr[NOLIGHT],     // SPR_EGGM
	&lspr[NOLIGHT],     // SPR_BON1
	&lspr[NOLIGHT],     // SPR_SRBX
	&lspr[NOLIGHT],     // SPR_GRBX
	&lspr[NOLIGHT],     // SPR_EMMY
	&lspr[NOLIGHT],     // SPR_PINV
	&lspr[NOLIGHT],     // SPR_GRTV
	&lspr[NOLIGHT],     // SPR_SPRY
	&lspr[NOLIGHT],     // SPR_SUDY
	&lspr[NOLIGHT],     // SPR_SHTV
	&lspr[NOLIGHT],     // SPR_FANS
	&lspr[NOLIGHT],     // SPR_BUBL
	&lspr[NOLIGHT],     // SPR_YLTV
	&lspr[NOLIGHT],     // SPR_FWR1
	&lspr[NOLIGHT],     // SPR_SPRR
	&lspr[NOLIGHT],     // SPR_SUDR
	&lspr[NOLIGHT],     // SPR_SMOK
	&lspr[NOLIGHT],     // SPR_SPLA
	&lspr[NOLIGHT],     // SPR_TNT1
	&lspr[NOLIGHT],     // SPR_BIRD
	&lspr[NOLIGHT],     // SPR_SQRL
	&lspr[YELLOWSHIELD_L],     // SPR_YORB
	&lspr[BLUESHIELD_L],     // SPR_BORB
	&lspr[BLACKSHIELD_L],     // SPR_KORB
	&lspr[RINGSPARK_L],     // SPR_SPRK
	&lspr[INVINCIBLE_L],     // SPR_IVSP
	&lspr[INVINCIBLE_L],     // SPR_IVSQ
	&lspr[NOLIGHT],     // SPR_DISS
	&lspr[NOLIGHT],     // SPR_BUBP
	&lspr[NOLIGHT],     // SPR_BUBO
	&lspr[NOLIGHT],     // SPR_BUBN
	&lspr[NOLIGHT],     // SPR_BUBM
	&lspr[NOLIGHT],     // SPR_CNTA
	&lspr[NOLIGHT],     // SPR_CNTB
	&lspr[NOLIGHT],     // SPR_CNTC
	&lspr[NOLIGHT],     // SPR_CNTD
	&lspr[NOLIGHT],     // SPR_CNTE
	&lspr[NOLIGHT],     // SPR_CNTF
	&lspr[NOLIGHT],     // SPR_POPP
	&lspr[NOLIGHT],     // SPR_PRUP
	&lspr[NOLIGHT],     // SPR_BKTV
	&lspr[NOLIGHT],     // SPR_SCRA
	&lspr[NOLIGHT],     // SPR_SCRB
	&lspr[NOLIGHT],     // SPR_SCRC
	&lspr[NOLIGHT],     // SPR_SCRD
	&lspr[SUPERSPARK_L],     // SPR_SSPK
	&lspr[NOLIGHT],     // SPR_GRAS
	&lspr[NOLIGHT],     // SPR_YSPR
	&lspr[NOLIGHT],     // SPR_RSPR
	&lspr[NOLIGHT],     // SPR_YSUD
	&lspr[NOLIGHT],     // SPR_RSUD
	&lspr[NOLIGHT],     // SPR_SKIM
	&lspr[NOLIGHT],     // SPR_MINE
	&lspr[NOLIGHT],     // SPR_FISH
	&lspr[NOLIGHT],     // SPR_GARG
	&lspr[NOLIGHT],     // SPR_SPLH
	&lspr[NOLIGHT],     // SPR_THOK
	&lspr[NOLIGHT],     // SPR_THZP
	&lspr[NOLIGHT],     // SPR_SIGN
	&lspr[RINGLIGHT_L],     // SPR_RRNG
	&lspr[NOLIGHT],     // SPR_TTAG
	&lspr[NOLIGHT],     // SPR_STEM
	&lspr[REDBALL_L],     // SPR_RFLG
	&lspr[BLUEBALL_L],     // SPR_BFLG
	&lspr[NOLIGHT],     // SPR_GFLG
	&lspr[BLUEBALL_L],     // SPR_TOKE
	&lspr[GREENSHINE_L],     // SPR_CEMG
	&lspr[ORANGESHINE_L],     // SPR_CEMO
	&lspr[PINKSHINE_L],     // SPR_CEMP
	&lspr[BLUESHINE_L],     // SPR_CEMB
	&lspr[REDSHINE_L],     // SPR_CEMR
	&lspr[LBLUESHINE_L],     // SPR_CEML
	&lspr[GREYSHINE_L],     // SPR_CEMY
	&lspr[NOLIGHT],     // SPR_JETB
	&lspr[NOLIGHT],     // SPR_JETG
	&lspr[NOLIGHT],     // SPR_JBUL
	&lspr[NOLIGHT],     // SPR_MOUS
	&lspr[NOLIGHT],     // SPR_DETN
	&lspr[NOLIGHT],     // SPR_XPLD
	&lspr[NOLIGHT],     // SPR_CHAN
	&lspr[NOLIGHT],     // SPR_CAPE
	&lspr[NOLIGHT],     // SPR_SNO1
	&lspr[NOLIGHT],     // SPR_SANT
	&lspr[NOLIGHT],     // SPR_EMER
	&lspr[NOLIGHT],     // SPR_EMES
	&lspr[NOLIGHT],     // SPR_EMET
	&lspr[NOLIGHT],     // SPR_SBLL
	&lspr[NOLIGHT],     // SPR_SPIK
	&lspr[NOLIGHT],     // SPR_CCOM
	&lspr[NOLIGHT],     // SPR_RAIN
	&lspr[NOLIGHT],     // SPR_DSPK
	&lspr[NOLIGHT],     // SPR_USPK
	//Fab:
	&lspr[NOLIGHT],     // SPR_STPT
	&lspr[RINGLIGHT_L],     // SPR_RNGM
	&lspr[RINGLIGHT_L],     // SPR_RNGR
	&lspr[RINGLIGHT_L],     // SPR_RNGS
	&lspr[RINGLIGHT_L],     // SPR_RNGA
	&lspr[RINGLIGHT_L],     // SPR_RNGE
	&lspr[RINGLIGHT_L],     // SPR_TAEH
	&lspr[RINGLIGHT_L],     // SPR_TAER
	&lspr[RINGLIGHT_L],     // SPR_THER
	&lspr[RINGLIGHT_L],     // SPR_TAHR
	&lspr[RINGLIGHT_L],     // SPR_THOM
	&lspr[RINGLIGHT_L],     // SPR_TAUT
	&lspr[RINGLIGHT_L],     // SPR_TEXP
	&lspr[NOLIGHT],     // SPR_BUS1
	&lspr[NOLIGHT],     // SPR_BUS2
	&lspr[NOLIGHT],     // SPR_FWR2
	&lspr[NOLIGHT],     // SPR_FWR3
	&lspr[NOLIGHT],     // SPR_MIXU
	&lspr[NOLIGHT],     // SPR_QUES
	&lspr[NOLIGHT],     // SPR_MTEX
	&lspr[REDBALL_L],     // SPR_FLAM // Tails 09-06-2002

	// Mario-specific stuff Tails 09-10-2002
	&lspr[REDBALL_L],     // SPR_PUMA
	&lspr[NOLIGHT],     // SPR_HAMM
	&lspr[NOLIGHT],     // SPR_KOOP
	&lspr[NOLIGHT],     // SPR_SHLL
	&lspr[NOLIGHT],     // SPR_MAXE
	&lspr[REDBALL_L],     // SPR_BFLM
	&lspr[SMALLREDBALL_L],     // SPR_FBLL
	&lspr[NOLIGHT],     // SPR_FFWR

	&lspr[NOLIGHT],     // SPR_NSPK

	&lspr[SUPERSONIC_L],     // SPR_SUPE // NiGHTS character flying
	&lspr[SUPERSONIC_L],     // SPR_SUPZ // NiGHTS hurt
	&lspr[SUPERSONIC_L],     // SPR_NDRN // NiGHTS drone
	&lspr[SUPERSONIC_L],     // SPR_NDRL // NiGHTS character drilling
	&lspr[NIGHTSLIGHT_L],     // SPR_SEED // Sonic CD flower seed
	&lspr[JETLIGHT_L],     // SPR_JETF // Boss jet fumes
	&lspr[NOLIGHT],     // SPR_HOOP
	&lspr[NOLIGHT],     // SPR_NSCR
	&lspr[NOLIGHT],     // SPR_NWNG
	&lspr[NOLIGHT],     // SPR_EGGN
	&lspr[NOLIGHT],     // SPR_GOOP
	&lspr[SUPERSPARK_L],     // SPR_BPLD
	&lspr[REDBALL_L],     // SPR_ALRM
	&lspr[NOLIGHT],     // SPR_RDTV
	&lspr[REDSHIELD_L],     // SPR_RORB
	&lspr[NOLIGHT],     // SPR_EGGB
	&lspr[NOLIGHT], // SPR_SFLM
	&lspr[NOLIGHT], // SPR_TNKA
	&lspr[NOLIGHT], // SPR_TNKB
	&lspr[NOLIGHT], // SPR_SPNK
	&lspr[SUPERSPARK_L], // SPR_TFOG
	&lspr[NOLIGHT], // SPR_EEGG
	&lspr[STREETLIGHT_L], // SPR_LITE
	&lspr[NOLIGHT], // SPR_TRET
	&lspr[SMALLREDBALL_L], // SPR_TRLS
	&lspr[NOLIGHT], // SPR_FWR4
	&lspr[NOLIGHT], // SPR_GOOM
	&lspr[NOLIGHT], // SPR_BGOM
	&lspr[NOLIGHT], // SPR_MUS1
	&lspr[NOLIGHT], // SPR_MUS2
	&lspr[NOLIGHT], // SPR_TOAD
	&lspr[NOLIGHT], // SPR_COIN
	&lspr[NOLIGHT], // SPR_CPRK
	&lspr[NOLIGHT], // SPR_XMS1
	&lspr[NOLIGHT], // SPR_XMS2
	&lspr[NOLIGHT], // SPR_XMS3
	&lspr[NOLIGHT], // SPR_CAPS
	&lspr[SUPERSONIC_L], // SPR_SUPT
	&lspr[NOLIGHT], // SPR_ROIA
	&lspr[NOLIGHT], // SPR_ROIB
	&lspr[NOLIGHT], // SPR_ROIC
	&lspr[NOLIGHT], // SPR_ROID
	&lspr[NOLIGHT], // SPR_ROIE
	&lspr[NOLIGHT], // SPR_ROIF
	&lspr[NOLIGHT], // SPR_ROIG
	&lspr[NOLIGHT], // SPR_ROIH
	&lspr[NOLIGHT], // SPR_ROII
	&lspr[NOLIGHT], // SPR_ROIJ
	&lspr[NOLIGHT], // SPR_ROIK
	&lspr[NOLIGHT], // SPR_ROIL
	&lspr[NOLIGHT], // SPR_ROIM
	&lspr[NOLIGHT], // SPR_ROIN
	&lspr[NOLIGHT], // SPR_ROIO
	&lspr[NOLIGHT], // SPR_ROIP
	&lspr[NOLIGHT], // SPR_NPRA
	&lspr[NOLIGHT], // SPR_NPRB
	&lspr[NOLIGHT], // SPR_NPRC
	&lspr[NOLIGHT], // SPR_REDX
	&lspr[NOLIGHT], // SPR_SPRB Graue 01-08-2004
	&lspr[NOLIGHT], // SPR_BUZZ Graue 03-10-2004
	&lspr[NOLIGHT], // SPR_RBUZ Graue 03-10-2004
	&lspr[GREYSHINE_L], // SPR_CEMK, // 8th Emerald
	&lspr[NOLIGHT],     // SPR_WHTV
	&lspr[WHITESHIELD_L],     // SPR_WORB
	&lspr[NOLIGHT], // SPR_TURR

	// Free slots
	&lspr[NOLIGHT],
	&lspr[NOLIGHT],
	&lspr[NOLIGHT],
	&lspr[NOLIGHT],
	&lspr[NOLIGHT],
	&lspr[NOLIGHT],
	&lspr[NOLIGHT],
	&lspr[NOLIGHT],
	&lspr[NOLIGHT],
	&lspr[NOLIGHT],
	&lspr[NOLIGHT],
	&lspr[NOLIGHT],
	&lspr[NOLIGHT],
	&lspr[NOLIGHT],
	&lspr[NOLIGHT],
	&lspr[NOLIGHT],
	&lspr[NOLIGHT],
	&lspr[NOLIGHT],
	&lspr[NOLIGHT],
	&lspr[NOLIGHT],
	&lspr[NOLIGHT],
	&lspr[NOLIGHT],
	&lspr[NOLIGHT],
	&lspr[NOLIGHT],
	&lspr[NOLIGHT],
	&lspr[NOLIGHT],
	&lspr[NOLIGHT],
	&lspr[NOLIGHT],
	&lspr[NOLIGHT],
	&lspr[NOLIGHT],
	&lspr[NOLIGHT],
	&lspr[NOLIGHT],
	&lspr[NOLIGHT],
	&lspr[NOLIGHT],
	&lspr[NOLIGHT],
	&lspr[NOLIGHT],
	&lspr[NOLIGHT],
	&lspr[NOLIGHT],
	&lspr[NOLIGHT],
	&lspr[NOLIGHT],
	&lspr[NOLIGHT],
	&lspr[NOLIGHT],
	&lspr[NOLIGHT],
	&lspr[NOLIGHT],
	&lspr[NOLIGHT],
	&lspr[NOLIGHT],
	&lspr[NOLIGHT],
	&lspr[NOLIGHT],
	&lspr[NOLIGHT],
	&lspr[NOLIGHT],
	&lspr[NOLIGHT],
	&lspr[NOLIGHT],
	&lspr[NOLIGHT],
	&lspr[NOLIGHT],
	&lspr[NOLIGHT],
	&lspr[NOLIGHT],
	&lspr[NOLIGHT],
	&lspr[NOLIGHT],
	&lspr[NOLIGHT],
	&lspr[NOLIGHT],
	&lspr[NOLIGHT],
	&lspr[NOLIGHT],
	&lspr[NOLIGHT],
	&lspr[NOLIGHT],
	&lspr[NOLIGHT],
	&lspr[NOLIGHT],
	&lspr[NOLIGHT],
	&lspr[NOLIGHT],
	&lspr[NOLIGHT],
	&lspr[NOLIGHT],
	&lspr[NOLIGHT],
	&lspr[NOLIGHT],
	&lspr[NOLIGHT],
	&lspr[NOLIGHT],
	&lspr[NOLIGHT],
	&lspr[NOLIGHT],
	&lspr[NOLIGHT],
	&lspr[NOLIGHT],
	&lspr[NOLIGHT],
	&lspr[NOLIGHT],
	&lspr[NOLIGHT],
	&lspr[NOLIGHT],
	&lspr[NOLIGHT],
	&lspr[NOLIGHT],
	&lspr[NOLIGHT],
	&lspr[NOLIGHT],
	&lspr[NOLIGHT],
	&lspr[NOLIGHT],
	&lspr[NOLIGHT],
	&lspr[NOLIGHT],
	&lspr[NOLIGHT],
	&lspr[NOLIGHT],
	&lspr[NOLIGHT],
	&lspr[NOLIGHT],
	&lspr[NOLIGHT],
	&lspr[NOLIGHT],
	&lspr[NOLIGHT],
	&lspr[NOLIGHT],
	&lspr[NOLIGHT],
	&lspr[NOLIGHT],
	&lspr[NOLIGHT],
	&lspr[NOLIGHT],
	&lspr[NOLIGHT],
	&lspr[NOLIGHT],
	&lspr[NOLIGHT],
	&lspr[NOLIGHT],
	&lspr[NOLIGHT],
	&lspr[NOLIGHT],
	&lspr[NOLIGHT],
	&lspr[NOLIGHT],
	&lspr[NOLIGHT],
	&lspr[NOLIGHT],
	&lspr[NOLIGHT],
	&lspr[NOLIGHT],
	&lspr[NOLIGHT],
	&lspr[NOLIGHT],
	&lspr[NOLIGHT],
	&lspr[NOLIGHT],
	&lspr[NOLIGHT],
	&lspr[NOLIGHT],
	&lspr[NOLIGHT],
	&lspr[NOLIGHT],
	&lspr[NOLIGHT],
	&lspr[NOLIGHT],
	&lspr[NOLIGHT],
	&lspr[NOLIGHT],
};

//=============================================================================
// Completely new method of light drawing
//=============================================================================

static GlidePatch_t lightmappatch;
static int coronalumpnum;

void HWR_InitLight(void)
{
	//size_t i;

// Find the square radius
	//for (i = 0;i < NUMLIGHTS;i++)
	//	lspr[i].dynamic_sqrradius = lspr[i].dynamic_radius*lspr[i].dynamic_radius;

	lightmappatch.mipmap.downloaded = false;
	coronalumpnum = W_GetNumForName("corona");
}


// Epic new light method!
// Get the position from the light's sprite's location,
// and draw the polygon in the sprite drawing function itself
void HWR_DrawCoronas(gr_vissprite_t *spr)
{
	if (!cv_grcoronas.value) // You don't have to have coronas if you don't want to
		return;
	
	HWR_GetPic(coronalumpnum); // Get the corona image!
		FOutVector      light[4];
		FSurfaceInfo    Surf;
		float           cx = FIXED_TO_FLOAT(spr->mobj->x);
		// This part makes no sense, but it works
		float           cz = FIXED_TO_FLOAT(spr->mobj->y);
		float           cy = FIXED_TO_FLOAT(spr->mobj->z);//+FIXED_TO_FLOAT(spr->mobj->height>>1);
		float           size;
		light_t   *p_lspr;
		p_lspr = t_lspr[spr->mobj->sprite];
		
		// Make sure the object has been defined to emit light
		if (!(p_lspr->type & CORONA_SPR))
			return; // If it doesn't have light, stop right here

		transform(&cx,&cy,&cz);
		
		// Get the color from the light defines
		Surf.FlatColor.rgba = p_lspr->corona_color;
		// Always draw it full brightness
		Surf.FlatColor.s.alpha = 0xff;

        // Set the size of the light
        size = p_lspr->corona_radius;
		
        // We can also proportionally scale the corona sizes	
		size = ((float)(FIXED_TO_FLOAT(cv_grcoronasize.value<<1)*size))/1.5;

		light[0].x = cx-size;  light[0].z = cz;
		light[0].y = cy-size;
		light[0].sow = 0.0f;   light[0].tow = 0.0f;

		light[1].x = cx+size;  light[1].z = cz;
		light[1].y = cy-size;
		light[1].sow = 1.0f;   light[1].tow = 0.0f;

		light[2].x = cx+size;  light[2].z = cz;
		light[2].y = cy+size;
		light[2].sow = 1.0f;   light[2].tow = 1.0f;

		light[3].x = cx-size;  light[3].z = cz;
		light[3].y = cy+size;
		light[3].sow = 0.0f;   light[3].tow = 1.0f;

		DrawPolygon (&Surf, light, 4, PF_Modulated | PF_Additive | PF_Clip | PF_NoDepthTest | PF_Corona);
}



/**
 \todo 

  - Les coronas ne sont pas gÔøΩer avec le nouveau systeme, seul le dynamic lighting l'est
  - calculer l'offset des coronas au chargement du level et non faire la moyenne
	au moment de l'afficher
	 BP: euh non en fait il faux encoder la position de la light dans le sprite
		 car c'est pas focement au mileux de plus il peut en y avoir plusieur (chandelier)
  - changer la comparaison pour l'affichage des coronas (+ un epsilon)
	BP: non non j'ai trouver mieux :) : lord du AddSprite tu rajoute aussi la coronas
		dans la sprite list ! avec un z de epsilon (attention au ZCLIP_PLANE) et donc on 
		l'affiche en dernier histoir qu'il puisse etre cacher par d'autre sprite :)
		Bon fait metre pas mal de code special dans hwr_project sprite mais sa vaux le 
		coup
  - gerer dynamic et static : retenir le nombre de lightstatic et clearer toute les 
		light > lightstatic (les dynamique) et les lightmap correspondant dans les segs
		puit refaire une passe avec le code si dessus mais rien que pour les dynamiques
		(tres petite modification)
  - finalement virer le hack splitscreen, il n'est plus necessaire !
*/
