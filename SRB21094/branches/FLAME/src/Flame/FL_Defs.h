// SRB2109FLAME - Defines
// By Flame

#ifdef FLAME
	// Miscellaneous things of mine.
	#define PRVMSG // Like the command 'SAYTO' command but only works whoever has the EXE with this define.
	#define QUAKECOLOR // Colored text like quake. (Ie; ^1<Text>, ^3<Text>)
	// #define FLAMEMISC // Miscellaneous things of mine. The "Possibly might be used in the future" list
	// #define WATERSLIDE // Water Slide!! Slip and slide all over the place.

	#define FLAMEBOSS // One boss I have longed to make.

	// change slope
	// dist = P_AproxDistance(P_AproxDistance([SOURCE]->tracer->x - [SOURCE]->x, [SOURCE]->tracer->y - [SOURCE]->y), [SOURCE]->tracer->z - [SOURCE]->z);
	// I MIGHT be able to use this for something in the future. Along with FINESINE(dist) [S: O/H, aiming] and FINECOSINE(dist) [C: A/H, distance]
#endif

#ifdef SMASH
	#define SMASHCAM // Smash Bros camera.
	#define SPRITEROLL // Turtle Man: Sprite/Model Rotation. Think loops.

	// Sprite Scaling!
	#define SCALESIZE // Copied from SRB2JTE made by JTE.
	#define SCALESPEEDS // Copied from SRB2JTE made by JTE.
	#define SCALEGRAVITY // Copied from SRB2JTE made by JTE.
	#define SCALECVAR // Turtle Man: Changed "numlaps" to "scale". Disable for RELEASE.

	// Old and/or unused define statements.
	// #define CHAOSBLAST_OLD // Old version of how the chaos blast orb was spawned. The new one uses a modified armegeddon shield blast.
#endif

#if (defined SCALESIZE || defined SCALESPEEDS || defined SCALEGRAVITY)
	#define FIXEDSCALE(x,y) FixedDiv(FixedMul((y)<<FRACBITS,(x)),100<<FRACBITS)
	#define FIXEDUNSCALE(x,y) ((y) == 0 ? 0 : FixedDiv(FixedMul((100<<FRACBITS),(x)),(y)<<FRACBITS))
#endif

#ifdef BOMB
	// Camera Modifiers
	// #define BOMBCAMFIXED // Camera focused on a fixed point. Like the lookaway view camera.
	#define BOMBCAMDYNAMIC // Camera following the player
#endif
