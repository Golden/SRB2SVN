// SRB2109FLAME - Resource file for Mobj objects, states, and sounds.
// By Flame, based off the JTESHUFFLE source [Whoops!]

#include "../d_englsh.h"
#include "../g_game.h"
#include "../m_menu.h"
#include "../m_random.h"
#include "../p_local.h"
#include "../p_mobj.h"
#include "../r_main.h"
#include "../s_sound.h"
#include "FL_Info.h"

#ifdef HWRENDER
#include "../hardware/hw_light.h"
#endif

#ifdef SMASH // Resources for Super Smash Brothers: Sonic Showdown
void SMASH_Init(void)
{
	size_t thing;

	//****** Mobj types ******//
	thing = MT_FIST; // 'Fist' object. This is more or less a type of hitbox for punching
	mobjinfo[thing] = mobjinfo[MT_DISS];
	mobjinfo[thing].radius = 25*FRACUNIT;
	mobjinfo[thing].height = 55*FRACUNIT;
	mobjinfo[thing].speed = 8;
	mobjinfo[thing].flags = MF_FLOAT|MF_NOGRAVITY|MF_MISSILE;

	thing = MT_STAR; // Starman
	mobjinfo[thing] = mobjinfo[MT_DISS];
	mobjinfo[thing].spawnstate = S_STAR;
	mobjinfo[thing].reactiontime = TICRATE;
	mobjinfo[thing].radius = 16*FRACUNIT;
	mobjinfo[thing].height = 32*FRACUNIT;
	mobjinfo[thing].mass = 100;
	mobjinfo[thing].flags = MF_SPECIAL|MF_BOUNCE;

	thing = MT_HEARTCONTAINER; // Heart Container
	mobjinfo[thing] = mobjinfo[MT_STAR];
	mobjinfo[thing].spawnstate = S_HEART;
	mobjinfo[thing].flags = MF_SHOOTABLE|MF_NOCLIPTHING|MF_PUSHABLE|MF_SLIDEME;

	thing = MT_SMASHBALL; // Smash ball. 'HIT IT!'
	mobjinfo[thing] = mobjinfo[MT_STAR];
	mobjinfo[thing].spawnstate = S_SMSHBALLUP;
	mobjinfo[thing].spawnhealth = 3;
	mobjinfo[thing].reactiontime = TICRATE/2;
	mobjinfo[thing].painstate = S_SMSHBALLUP;
	mobjinfo[thing].painsound = sfx_dmpain;
	mobjinfo[thing].flags = MF_SPECIAL|MF_SHOOTABLE|MF_NOGRAVITY|MF_FLOAT;

	thing = MT_SMASHBALLFLAME;
	mobjinfo[thing] = mobjinfo[MT_REDORB];
	mobjinfo[thing].spawnstate = S_SMASHFLAME1;
	mobjinfo[thing].speed = pw_finalsmash;

	thing = MT_SUPERFLICKY;
	mobjinfo[thing] = mobjinfo[MT_BIRD];
	mobjinfo[thing].doomednum = -1;
	mobjinfo[thing].spawnstate = S_SFLICK1;
	mobjinfo[thing].speed = 30*FRACUNIT;
	mobjinfo[thing].flags |= MF_TRANSLATION;

	thing = MT_SHIELD;
	mobjinfo[thing] = mobjinfo[MT_THOK];
	mobjinfo[thing].doomednum = -1;
	mobjinfo[thing].spawnstate = S_SHLD1;
	mobjinfo[thing].height = 56*FRACUNIT;

#ifdef CHAOSBLAST_OLD
	thing = MT_CBLASTORB;
	mobjinfo[thing] = mobjinfo[MT_THOK];
	mobjinfo[thing].doomednum = -1;
	mobjinfo[thing].spawnstate = S_CBLASTORB;
#endif

#ifdef SMASHCAM // Who knows? Someone might want to disable the SMASHCAM define.
	thing = MT_CAMMAX; // [Outer/Right] Camera Boundary
	mobjinfo[thing] = mobjinfo[MT_DISS];
	mobjinfo[thing].doomednum = 400;
	mobjinfo[thing].spawnstate = S_TNT1;

	thing = MT_CAMMIN; // [Inner/Left] Camera Boundary
	mobjinfo[thing] = mobjinfo[MT_CAMMAX];
	mobjinfo[thing].doomednum = 401;
#endif

	//****** States ******//

	// Extra Player frames
	// First Punch
	thing = S_PLAY_PUNCH1;
	states[thing].sprite = SPR_PLAY;
	states[thing].frame = 34;
	states[thing].tics = 4;
	states[thing].nextstate = S_PLAY_PUNCH2;

	thing = S_PLAY_PUNCH2;
	states[thing] = states[S_PLAY_PUNCH1];
	states[thing].frame = 35;
	states[thing].nextstate = S_PLAY_PUNCH3;

	thing = S_PLAY_PUNCH3;
	states[thing] = states[S_PLAY_PUNCH1];
	states[thing].frame = 36;
	states[thing].nextstate = S_PLAY_STND;

	// Second Punch
	thing = S_PLAY_PUNCH4;
	states[thing] = states[S_PLAY_PUNCH1];
	states[thing].frame = 37;
	states[thing].nextstate = S_PLAY_PUNCH5;

	thing = S_PLAY_PUNCH5;
	states[thing] = states[S_PLAY_PUNCH1];
	states[thing].frame = 38;
	states[thing].nextstate = S_PLAY_PUNCH6;

	thing = S_PLAY_PUNCH6;
	states[thing] = states[S_PLAY_PUNCH3];
	states[thing].frame = 39;

	// Kicks
	thing = S_PLAY_KICK1;
	states[thing].sprite = SPR_PLAY;
	states[thing].frame = 40;
	states[thing].tics = 6;
	states[thing].nextstate = S_PLAY_KICK2;

	thing = S_PLAY_KICK2;
	states[thing] = states[S_PLAY_KICK1];
	states[thing].frame = 41;
	states[thing].nextstate = S_PLAY_KICK3;

	thing = S_PLAY_KICK3;
	states[thing] = states[S_PLAY_KICK1];
	states[thing].frame = 42;
	states[thing].nextstate = S_PLAY_KICK4;

	thing = S_PLAY_KICK4;
	states[thing] = states[S_PLAY_PUNCH1]; // This is just so I can get the tics
	states[thing].frame = 43;
	states[thing].nextstate = S_PLAY_KICK4;

	thing = S_PLAY_KICK5;
	states[thing] = states[S_PLAY_PUNCH1];
	states[thing].frame = 44;
	states[thing].nextstate = S_PLAY_KICK6;

	thing = S_PLAY_KICK6;
	states[thing] = states[S_PLAY_PUNCH1];
	states[thing].frame = 45;
	states[thing].nextstate = S_PLAY_STND;


	/* // Aaand.. Here's that 'fist' state for that 'fist' object.
	thing = S_FIST;
	states[thing] = states[S_DISS];
	states[thing].tics = 10;
	*/

	thing = S_STAR; // Starman
	states[thing] = states[S_DISS];
	states[thing].sprite = SPR_STAR;
	states[thing].nextstate = S_STAR;

	thing = S_HEART; // Heart Container
	states[thing] = states[S_DISS];
	states[thing].sprite = SPR_HART;
	states[thing].nextstate = S_HEART;

	thing = S_SMSHBALLUP; // Smashball (Moving up?)
	states[thing] = states[S_DISS];
	states[thing].sprite = SPR_FSMH;
	states[thing].nextstate = S_SMSHBALLDOWN;

	thing = S_SMSHBALLDOWN; // Smashball (Moving down?)
	states[thing] = states[S_SMSHBALLUP];
	states[thing].nextstate = S_SMSHBALLUP;

	// Final Smash Fire Aura animation
	thing = S_SMASHFLAME1;
	states[thing] = states[S_DISS];
	states[thing].sprite = SPR_SFIR;
	states[thing].frame = 0;
	states[thing].tics = 1;
	states[thing].nextstate = S_SMASHFLAME2;

	thing = S_SMASHFLAME2;
	states[thing] = states[S_SMASHFLAME1];
	states[thing].frame = 1;
	states[thing].nextstate = S_SMASHFLAME3;

	thing = S_SMASHFLAME3;
	states[thing] = states[S_SMASHFLAME1];
	states[thing].frame = 2;
	states[thing].nextstate = S_SMASHFLAME4;

	thing = S_SMASHFLAME4;
	states[thing] = states[S_SMASHFLAME1];
	states[thing].frame = 3;
	states[thing].nextstate = S_SMASHFLAME5;

	thing = S_SMASHFLAME5;
	states[thing] = states[S_SMASHFLAME1];
	states[thing].frame = 4;
	states[thing].nextstate = S_SMASHFLAME6;

	thing = S_SMASHFLAME6;
	states[thing] = states[S_SMASHFLAME1];
	states[thing].frame = 5;
	states[thing].nextstate = S_SMASHFLAME7;

	thing = S_SMASHFLAME7;
	states[thing] = states[S_SMASHFLAME1];
	states[thing].frame = 6;
	states[thing].nextstate = S_SMASHFLAME1;

	// Flicky animations [TAILS ONLY]
	thing = S_SFLICK1;
	states[thing] = states[S_BIRD1];
	states[thing].nextstate = S_SFLICK2;

	thing = S_SFLICK2;
	states[thing] = states[S_BIRD1];
	states[thing].frame = 1;
	states[thing].nextstate = S_SFLICK1;


	thing = S_SHLD1; // SSB:SS Shield.
	states[thing] = states[S_DISS];
	states[thing].sprite = SPR_SHLD;
	states[thing].tics = -1;

	// Shadow doing a Chaos Blast blast animation
	thing = S_SHADBLST1;
	states[thing] = states[S_DISS];
	states[thing].sprite = SPR_SHCB;
	states[thing].frame = 0;
	states[thing].tics = 4;
	states[thing].nextstate = S_SHADBLST2;

	thing = S_SHADBLST2;
	states[thing] = states[S_SHADBLST1];
	states[thing].frame = 1;
	states[thing].nextstate = S_SHADBLST3;

	thing = S_SHADBLST3;
	states[thing] = states[S_SHADBLST1];
	states[thing].frame = 32770; // 32770 = 2nd frame, with.. lighting effects?
	states[thing].nextstate = S_SHADBLST4;

	thing = S_SHADBLST4;
	states[thing] = states[S_SHADBLST1];
	states[thing].frame = 3;
	states[thing].tics = 3;
	states[thing].nextstate = S_SHADBLST5;

	thing = S_SHADBLST5;
	states[thing] = states[S_SHADBLST4];
	states[thing].frame = 4;
	states[thing].nextstate = S_SHADBLST6;

	thing = S_SHADBLST6;
	states[thing] = states[S_SHADBLST4];
	states[thing].frame = 5;
	states[thing].nextstate = S_SHADBLST7;

	thing = S_SHADBLST7;
	states[thing] = states[S_SHADBLST1];
	states[thing].frame = 6;
	states[thing].tics = 224;
	states[thing].nextstate = S_NIGHTSDRONE1;

#ifdef CHAOSBLAST_OLD
	thing = S_CBLASTORB;
	states[thing] = states[S_THOK1];
	states[thing].tics = -1;
#endif

	//***** Sprite files? *****//
	thing = SPR_STAR; // Starman
	strcpy(sprnames[thing],"STAR");
#ifdef HWRENDER
	t_lspr[thing] = &lspr[NOLIGHT];
#endif

	thing = SPR_HART; // Heart Container
	strcpy(sprnames[thing],"HART");
#ifdef HWRENDER
	t_lspr[thing] = &lspr[NOLIGHT];
#endif

	thing = SPR_FSMH; // Final Smash
	strcpy(sprnames[thing],"FSMH");
#ifdef HWRENDER
	t_lspr[thing] = &lspr[NOLIGHT];
#endif

	thing = SPR_SFIR; // Final Smash Fire Aura
	strcpy(sprnames[thing],"SFIR");
#ifdef HWRENDER
	t_lspr[thing] = &lspr[NOLIGHT];
#endif

	thing = SPR_TARG; // Target
	strcpy(sprnames[thing],"TARG");
#ifdef HWRENDER
	t_lspr[thing] = &lspr[NOLIGHT];
#endif

	thing = SPR_SHLD; // Shield
	strcpy(sprnames[thing],"SHLD");
#ifdef HWRENDER
	t_lspr[thing] = &lspr[NOLIGHT];
#endif

	thing = SPR_SHCB; // Shadow Chaos Blast sprite set
	strcpy(sprnames[thing],"SHCB");
#ifdef HWRENDER
	t_lspr[thing] = &lspr[NOLIGHT];
#endif

	//***** Sound files *****//
	// SSB Item sounds
	thing = sfx_sbounce;
	S_sfx[thing] = S_sfx[sfx_None];
	S_sfx[thing].name = "sbounce";
	S_sfx[thing].singularity = true;
	S_sfx[thing].priority = 96;
	// S_sfx[thing].pitch = SF_NOINTERRUPT|SF_X4AWAYSOUND; // Refrence

	thing = sfx_itemspwn;
	S_sfx[thing] = S_sfx[sfx_sbounce];
	S_sfx[thing].name = "itemspwn";

	// Announcer Sound files
	thing = sfx_ssbsay1; // "ONE!"
	S_sfx[thing] = S_sfx[sfx_None];
	S_sfx[thing].name = "ssbsay1";
	S_sfx[thing].singularity = false;
	S_sfx[thing].priority = 127;

	thing = sfx_ssbsay2; // "TWO!"
	S_sfx[thing] = S_sfx[sfx_ssbsay1];
	S_sfx[thing].name = "ssbsay2";

   	thing = sfx_ssbsay3; // "THREE!"
	S_sfx[thing] = S_sfx[sfx_ssbsay1];
	S_sfx[thing].name = "ssbsay3";

	thing = sfx_ssbsaygo; // "GO!!"
	S_sfx[thing] = S_sfx[sfx_ssbsay1];
	S_sfx[thing].name = "ssbsaygo";

	thing = sfx_ssbgames; // "GAME SET!"
	S_sfx[thing] = S_sfx[sfx_ssbsay1];
	S_sfx[thing].name = "ssbgames";
}
#endif

#ifdef BOMB // Resources for Super Bomberman Blast (2 ?)
void BOMB_Init(void)
{
	size_t thing;

	//****** Mobj types ******//
	thing = MT_BOMB;
	mobjinfo[thing] = mobjinfo[MT_DISS];
	mobjinfo[thing].spawnstate = S_BOMB;
	mobjinfo[thing].seestate = S_BOMB;
	mobjinfo[thing].radius = 10*FRACUNIT;
	mobjinfo[thing].height = 26*FRACUNIT;
	mobjinfo[thing].mass = 16;
	mobjinfo[thing].speed = 8;
	mobjinfo[thing].flags = MF_SHOOTABLE|MF_NOCLIP;

	/*thing = MT_BOMBBOOM;
	mobjinfo[thing] = mobjinfo[MT_DISS];
	mobjinfo[thing].spawnstate = S_BOMBBOOM;
	mobjinfo[thing].seesound = sfx_rlaunc;
	mobjinfo[thing].radius = 50*FRACUNIT;
	mobjinfo[thing].height = 50*FRACUNIT;
	mobjinfo[thing].mass = 100;
	mobjinfo[thing].damage = 20;
	mobjinfo[thing].flags = MF_NOBLOCKMAP|MF_MISSILE|MF_NOGRAVITY;*/

	thing = MT_BOMBUP;
	mobjinfo[thing] = mobjinfo[MT_DISS];
	mobjinfo[thing].doomednum = 7000;
	mobjinfo[thing].spawnstate = S_BOMBUP;
	mobjinfo[thing].spawnhealth = 1000;
	mobjinfo[thing].seestate = S_BOMBUP;
	// mobjinfo[thing].deathstate = S_IVSP1;
	mobjinfo[thing].speed = 0;
	mobjinfo[thing].radius = 15*FRACUNIT;
	mobjinfo[thing].height = 28*FRACUNIT;
	mobjinfo[thing].mass = 16;
	mobjinfo[thing].flags = MF_SPECIAL|MF_FLOAT|MF_NOGRAVITY;

	thing = MT_FIREUP;
	mobjinfo[thing] = mobjinfo[MT_BOMBUP];
	mobjinfo[thing].doomednum = 7001;
	mobjinfo[thing].spawnstate = S_FIREUP;
	mobjinfo[thing].seestate = S_FIREUP;

	thing = MT_SPEEDUP;
	mobjinfo[thing] = mobjinfo[MT_BOMBUP];
	mobjinfo[thing].doomednum = 7002;
	mobjinfo[thing].spawnstate = S_SPEEDUP;
	mobjinfo[thing].seestate = S_SPEEDUP;

	thing = MT_FULLFIRE;
	mobjinfo[thing] = mobjinfo[MT_BOMBUP];
	mobjinfo[thing].doomednum = 7003;
	mobjinfo[thing].spawnstate = S_FULLFIRE;
	mobjinfo[thing].seestate = S_FULLFIRE;

	thing = MT_BOMBPLODEGFX;
	mobjinfo[thing] = mobjinfo[MT_BOMBUP];
	mobjinfo[thing].doomednum = -1;
	mobjinfo[thing].spawnstate = S_EXPL1;
	mobjinfo[thing].seestate = S_EXPL1;
	mobjinfo[thing].radius = 10*FRACUNIT;
	mobjinfo[thing].height = 22*FRACUNIT;
	mobjinfo[thing].flags |= MF_FIRE;

	thing = MT_BOSSTHROWNBOMB;
	mobjinfo[thing] = mobjinfo[MT_THROWNEXPLOSION];
	mobjinfo[thing].speed = 30*FRACUNIT;

	//****** States ******//

	// EGGMAN *BOMBERMAN* MODIFIER //

	thing = S_EGGMOBILE_STND;
	states[thing].sprite = SPR_EGGM;
	states[thing].frame = 0;
	states[thing].tics = 70;
	states[thing].action.acp1 = (actionf_p1)A_MoveRelative;
	states[thing].var1 = 180;
	states[thing].var2 = 10;
	states[thing].nextstate = S_EGGMOBILE_FLYUP;

	thing = S_EGGMOBILE_ATK1;
	states[thing].sprite = SPR_EGGM;
	states[thing].frame = 1;
	states[thing].tics = 35;
	states[thing].action.acp1 = (actionf_p1)A_FaceTarget;
//	states[thing].var1 = 0;
//	states[thing].var2 = 0;
	states[thing].nextstate = S_EGGMOBILE_ATK2;

	thing = S_EGGMOBILE_ATK2;
	states[thing].sprite = SPR_EGGM;
	states[thing].frame = 2;
	states[thing].tics = 35;
	states[thing].action.acp1 = (actionf_p1)A_FireShot;
	states[thing].var1 = MT_BOSSTHROWNBOMB;
	states[thing].nextstate = S_EGGMOBILE_ATK3;

	thing = S_EGGMOBILE_ATK3;
	states[thing] = states[S_EGGMOBILE_ATK2];
	states[thing].frame = 4;
	states[thing].nextstate = S_EGGMOBILE_ATK4;

	thing = S_EGGMOBILE_ATK4;
	states[thing] = states[S_EGGMOBILE_ATK2];
	states[thing].frame = 2;
	states[thing].nextstate = S_EGGMOBILE_PANIC1;

	thing = S_EGGMOBILE_PANIC1;
	states[thing].sprite = SPR_EGGM;
	states[thing].frame = 1;
	states[thing].tics = 35;
	states[thing].action.acp1 = (actionf_p1)A_FaceTarget;
	states[thing].nextstate = S_EGGMOBILE_PANIC2;

	thing = S_EGGMOBILE_PANIC2;
	states[thing].sprite = SPR_EGGM;
	states[thing].frame = 1;
	states[thing].tics = 35;
	states[thing].action.acp1 = (actionf_p1)A_MoveRelative;
	states[thing].var2 = 10;
	states[thing].nextstate = S_EGGMOBILE_FLYDOWN;

	thing = S_EGGMOBILE_PAIN;
	states[thing].sprite = SPR_EGGM;
	states[thing].frame = 5;
	states[thing].tics = 24;
	states[thing].action.acp1 = (actionf_p1)A_Pain;
	states[thing].nextstate = S_EGGMOBILE_FLYUP;

	thing = S_EGGMOBILE_FLYUP;
	states[thing].sprite = SPR_EGGM;
	states[thing].frame = 0;
	states[thing].tics = 35;
	states[thing].action.acp1 = (actionf_p1)A_BossFlyUp;
	states[thing].nextstate = S_EGGMOBILE_ATK1;

	thing = S_EGGMOBILE_FLYDOWN;
	states[thing] = states[S_EGGMOBILE_FLYUP];
	states[thing].tics = 70;
	states[thing].action.acp1 = (actionf_p1)A_BossFlyDown;
	states[thing].nextstate = S_EGGMOBILE_STND;

	// END EGGMAN *BOMBERMAN* MODIFIER //

	thing = S_BOMB; // Bomb
	states[thing].sprite = SPR_BOMB;
	states[thing].frame = 0;
	states[thing].tics = 2;
	states[thing].nextstate = S_BOMB2;

	thing = S_BOMB2;
	states[thing].sprite = SPR_BOMB;
	states[thing].frame = 1;
	states[thing].tics = 2;
	states[thing].nextstate = S_BOMB;

	thing = S_BOMBPLS; // Bomb Pulse
	states[thing].sprite = SPR_BOMB;
	states[thing].frame = 2;
	states[thing].tics = 5;
	states[thing].nextstate = S_BOMBPLS2;

	thing = S_BOMBPLS2; // Bomb Pulse (About to explode)
	states[thing].sprite = SPR_BOMB;
	states[thing].frame = 3;
	states[thing].tics = -1;

	thing = S_BOMBBOOM; // Bomb explosion
	states[thing] = states[S_DISS];
	states[thing].tics = 1;
	states[thing].nextstate = S_FLAME1;

	thing = S_BOMBBOOM3D; // Bomb explosion (In non-2D maps)
	states[thing] = states[S_RINGEXPLODE];
	states[thing].nextstate = S_DISS;



	thing = S_BOMBUP; // Bombs +1
	states[thing].sprite = SPR_BOMU;
	states[thing].frame = 0;
	states[thing].tics = -1;

	thing = S_FIREUP; // Fire power +1
	states[thing] = states[S_BOMBUP];
	states[thing].sprite = SPR_FIRU;

	thing = S_SPEEDUP; // Speed up +1
	states[thing] = states[S_BOMBUP];
	states[thing].sprite = SPR_SPDU;

	thing = S_FULLFIRE; // Full Fire (Maxes Fire power)
	states[thing] = states[S_BOMBUP];
	states[thing].sprite = SPR_FFIR;

	// Exploding GFX
	thing = S_EXPL1;
	states[thing].sprite = SPR_EXPL;
	states[thing].frame = 0;
	states[thing].tics = 3;
	states[thing].nextstate = S_EXPL2;

	thing = S_EXPL2;
	states[thing] = states[S_EXPL1];
	states[thing].frame = 1;
	states[thing].nextstate = S_EXPL3;

	thing = S_EXPL3;
	states[thing] = states[S_EXPL1];
	states[thing].frame = 2;
	states[thing].nextstate = S_EXPL4;

	thing = S_EXPL4;
	states[thing] = states[S_EXPL1];
	states[thing].frame = 3;
	states[thing].nextstate = S_EXPL5;

	thing = S_EXPL5;
	states[thing] = states[S_EXPL1];
	states[thing].frame = 4;
	states[thing].nextstate = S_DISS;

	//***** Sprite files? *****//
	thing = SPR_BOMB;
	strcpy(sprnames[thing],"BOMB");


	thing = SPR_BOMU;
	strcpy(sprnames[thing],"BOMU");
#ifdef HWRENDER
	t_lspr[thing] = &lspr[NOLIGHT];
#endif

	thing = SPR_FIRU;
	strcpy(sprnames[thing],"FIRU");
#ifdef HWRENDER
	t_lspr[thing] = &lspr[NOLIGHT];
#endif

	thing = SPR_SPDU;
	strcpy(sprnames[thing],"SPDU");
#ifdef HWRENDER
	t_lspr[thing] = &lspr[NOLIGHT];
#endif

	thing = SPR_FFIR;
	strcpy(sprnames[thing],"FFIR");
#ifdef HWRENDER
	t_lspr[thing] = &lspr[NOLIGHT];
#endif

	thing = SPR_EXPL;
	strcpy(sprnames[thing],"EXPL");
#ifdef HWRENDER
	t_lspr[thing] = &lspr[NOLIGHT];
#endif

	//***** Sound files *****//
	thing = sfx_fuse;
	S_sfx[thing] = S_sfx[sfx_None];
	S_sfx[thing].name = "fuse";
	S_sfx[thing].singularity = false;
	S_sfx[thing].priority = 129;
	// S_sfx[thing].pitch = SF_NOINTERRUPT|SF_X4AWAYSOUND; // Refrence

	thing = sfx_boom;
	S_sfx[thing] = S_sfx[sfx_None];
	S_sfx[thing].name = "boom";
	S_sfx[thing].singularity = true;
	S_sfx[thing].priority = 130;
	S_sfx[thing].pitch = 24;

	thing = sfx_getitem;
	S_sfx[thing] = S_sfx[sfx_fuse];
	S_sfx[thing].name = "getitem";
}
#endif

#ifdef FLAME
void FLAME_Init(void)
{
    size_t thing;

#ifdef FLAMEBOSS
	// Runaway Eggmobile
	thing = MT_EGGMOBILE3;
	mobjinfo[thing] = mobjinfo[MT_EGGMOBILE];
	mobjinfo[thing].doomednum = 7070;
#ifdef BOMB
	mobjinfo[thing].spawnhealth = 8;
	mobjinfo[thing].meleestate = S_EGGMOBILE_ATK3;
	mobjinfo[thing].missilestate = S_EGGMOBILE_ATK1;
	mobjinfo[thing].raisestate = S_EGGMOBILE_PANIC1;
#else
	mobjinfo[thing].speed = 20*FRACUNIT; // SKULLSPEED
#endif

#endif // FLAMEBOSS
}
#endif // FLAME
