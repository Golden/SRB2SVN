// Emacs style mode select   -*- C++ -*-
//-----------------------------------------------------------------------------
//
// Copyright (C) 2000 by DooM Legacy Team.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
//-----------------------------------------------------------------------------

#ifdef __GNUC__
#include <unistd.h>
#endif
#include <typeinfo>
#include <string.h>
#include <stdarg.h>
#include "ipcs.h"
#include "common.h"
#include "srvlist.h"
#include "stats.h"
#include <mysql/mysql.h>

//=============================================================================

#ifdef __GNUC__
#define ATTRPACK __attribute__ ((packed))
#else
#define ATTRPACK
#endif

#define PT_ASKINFOVIAMS 15

static CServerList servers_list;
static CServerSocket server_socket;
static CServerStats server_stats;

FILE *logfile;

#if defined(_MSC_VER)
#pragma pack(1)
#endif

typedef struct
{
	char ip[16];			// Big enough to hold a full address.
	unsigned short port;
	unsigned char padding1[2];
	unsigned long time;
} ATTRPACK ms_holepunch_packet_t;

typedef struct
{
	char clientaddr[22];
	unsigned char padding1[2];
	unsigned long time;
} ATTRPACK msaskinfo_pak;

//
// SRB2 network packet data.
//
typedef struct
{
	unsigned checksum;
	unsigned char ack; // if not null the node asks for acknowledgement, the receiver must resend the ack
	unsigned char ackreturn; // the return of the ack number

	unsigned char packettype;
	unsigned char reserved; // padding

	msaskinfo_pak msaskinfo;
} ATTRPACK doomdata_t;

#if defined(_MSC_VER)
#pragma pack()
#endif

//=============================================================================

#define HOSTNAME "changeme"
#define USER "changeme"
#define PASSWORD "changeme"
#define DATABASE "changeme"

// MySQL Stuff :D

   char *server = HOSTNAME;
   char *user = USER;
   char *password = PASSWORD;
   char *database = DATABASE;
   time_t lastupdate;

   MYSQL *conn;
   MYSQL_RES *res;
   MYSQL_ROW row;
   int mysqlconnected = 0;
   
void MySQL_Conn(void) {
	if(mysqlconnected != 1) {
		char *server = HOSTNAME;
		char *user = USER;
		char *password = PASSWORD;
		char *database = DATABASE;

		conn = mysql_init(NULL);
   
		/* Connect to database */
		if (!mysql_real_connect(conn, server,
			user, password, database, 8219, NULL, 0)) {
			printf("%s\n", mysql_error(conn));
		}
		conn = mysql_init(NULL);
   
		/* Connect to database */
		if (!mysql_real_connect(conn, server, user, password, database, 0, NULL, 0)) {
			printf("%s\n", mysql_error(conn));
			mysqlconnected = 0;
		} else {
			printf("Connection to MySQL Database successful!\n");
			mysqlconnected = 1;
		}
	}
}

void MySQL_Stats_Update(void) {
     
        MySQL_Conn();
        time_t current_time;
        current_time = time (NULL);
        char copyquery[500];
        
        if(lastupdate <= (current_time-(10)) && lastupdate >= 0) {
              logPrintf(logfile, "Updating the statistics!\n");
              sprintf(copyquery, "UPDATE `ms_core` SET `server_readds` = server_readds+%d, `server_unique` = server_unique+%d, `server_removals` = server_removals+%d, `server_auto` = server_auto+%d, `server_list` = server_list+%d, `server_connections` = server_connections+%d, `server_bad` = server_bad+%d LIMIT 1", server_stats.num_readds, server_stats.num_unique, server_stats.num_removal, server_stats.num_autoremoval, server_stats.num_retrieval, server_stats.num_connections, server_stats.num_badconnection);
              if(!mysql_query(conn, copyquery)) {
                logPrintf(logfile, "Copied the data from memory successfully! Now to reset the stats!\n");
                server_stats.num_readds = 0;
                logPrintf(logfile, "Re-adds, done. ");
                server_stats.num_unique = 0;
                logPrintf(logfile, "Unique, done. ");
                server_stats.num_removal = 0;
                logPrintf(logfile, "Removals, done. ");
                server_stats.num_autoremoval = 0;
                logPrintf(logfile, "Autoremovals, done. ");
                server_stats.num_retrieval = 0;
                logPrintf(logfile, "Retrievals, done. ");
                server_stats.num_connections = 0;
                logPrintf(logfile, "Connections, done. ");
                server_stats.num_badconnection = 0;
                logPrintf(logfile, "Bad Connections, done.\n");
                lastupdate = time (NULL);
                logPrintf(logfile, "Updated lastupdate time.\n");
              } else {
                logPrintf(logfile, "MYSQL ERROR: %s\n", mysql_error(conn));
              }
              logPrintf(logfile, "Free result.\n");
              mysql_free_result(res);
              logPrintf(logfile, "Result free.\n");
        } else if(lastupdate == 0) {
              lastupdate = current_time-(60*1);
        }
}

void MySQL_Stats(int type) {
        char *statsquery = "";
        char timequery[500];
        time_t timestamp;
        timestamp = time (NULL);
        if(type == 1)
//        statsquery = "UPDATE `ms_core_temp` SET `server_list` = server_list+1 LIMIT 1";
          server_stats.num_retrieval++;
        else if(type == 2)
//        statsquery = "UPDATE `ms_core_temp` SET `server_unique` = server_unique+1 LIMIT 1";
          server_stats.num_unique++;
        else if(type == 3)
//        statsquery = "UPDATE `ms_core_temp` SET `server_bad` = server_bad+1 LIMIT 1";
          server_stats.num_badconnection++;
        else if(type == 4)
//        statsquery = "UPDATE `ms_core_temp` SET `server_removals` = server_removals+1 LIMIT 1";
          server_stats.num_removal++;
        else if(type == 5)
//        statsquery = "UPDATE `ms_core_temp` SET `server_auto` = server_auto+1 LIMIT 1";
          server_stats.num_autoremoval++;
        else if(type == 6)
//        statsquery = "UPDATE `ms_core_temp` SET `server_connections` = server_connections+1 LIMIT 1";
          server_stats.num_connections++;
        else if(type == 7)
//          statsquery = "UPDATE `ms_core_temp` SET `server_readds` = server_readds+1 LIMIT 1";
          server_stats.num_readds++;
        else if(type == 8)
          statsquery = "UPDATE `ms_core` SET `upsince` = '%ld' LIMIT 1";

        MySQL_Conn();
        if(type == 8) {
          logPrintf(logfile, "Updating uptime to %ld...\n", timestamp);
          sprintf(timequery, statsquery, timestamp);
          if(mysql_query(conn, timequery)) {
            logPrintf(logfile, "MYSQL ERROR: %s\n", mysql_error(conn));
          } else {
            logPrintf(logfile, "Server uptime updated successfully!\n");
          }
        }
        MySQL_Stats_Update();
}

int MySQL_CheckBanList(char *ip) {
        MySQL_Conn();
        char banqueryp1[500] = "SELECT * FROM ms_bans WHERE INET_ATON('%s') BETWEEN `ipstart` AND `ipend` AND (`endtime` > %ld OR `permanent` = '1') LIMIT 1";
        char banquery[500];
        time_t current_time = time (NULL);
        sprintf(banquery, banqueryp1, ip, current_time);
        logPrintf(logfile, "Executing MySQL Query: %s\n", banquery);
        if(mysql_query(conn, banquery)) {
          logPrintf(logfile, "MYSQL ERROR: %s\n", mysql_error(conn));
          return false;
        } else {
          res = mysql_store_result(conn);
          if(mysql_num_rows(res) >= 1) {
            return true;
          } else
            return false;
          mysql_free_result(res);
        }
}

int MySQL_CheckBan(char *ip) {
        MySQL_Conn();
        char banqueryp1[500] = "SELECT * FROM ms_bans WHERE INET_ATON('%s') BETWEEN `ipstart` AND `ipend` AND (`full_endtime` > %ld OR `permanent` = '1') LIMIT 1";
        char exqueryp1[500] = "SELECT * FROM ms_exceptions WHERE `ip` = '%s' AND `bid` = '%s' LIMIT 1";
        char exquery[500];
        char banquery[500];
        time_t current_time = time (NULL);
        sprintf(banquery, banqueryp1, ip, current_time);
        logPrintf(logfile, "Executing MySQL Query: %s\n", banquery);
        if(mysql_query(conn, banquery)) {
          logPrintf(logfile, "MYSQL ERROR: %s\n", mysql_error(conn));
          return false;
        } else {
          res = mysql_store_result(conn);
          if(mysql_num_rows(res) >= 1) {
            row = mysql_fetch_row(res);
            mysql_free_result(res);
            sprintf(exquery, exqueryp1, ip, row[0]);
            logPrintf(logfile, "Executing MySQL Query: %s\n", exquery);
            if(mysql_query(conn, exquery)) {
              logPrintf(logfile, "MYSQL ERROR: %s\n", mysql_error(conn));
              return true;
            } else {
              res = mysql_store_result(conn);
              if(mysql_num_rows(res) >= 1)
                return false;
              else
                return true;
            }
          } else {
            return false;
            mysql_free_result(res);
          }
        }
}

void MySQL_AddServer(char *ip, char *port, char *name, char *version) {
        char escapedName[255];
        char escapedIP[32];
        char escapedPort[10];
        char escapedVersion[10];
        char insertquery[5000];
        char checkquery[500];
        char updatequery[5000];
        char queryp1[5000] = "INSERT INTO `ms_servers` (`name`,`ip`,`port`,`version`,`timestamp`) VALUES ('%s','%s','%s','%s','%ld')";
        char checkqueryp1[500] = "SELECT * FROM `ms_servers` WHERE `ip` = '%s'";
        char updatequeryp1[5000] = "UPDATE `ms_servers` SET `name` = '%s', `port` = '%s', `version` = '%s', timestamp = '%ld', upnow = '1' WHERE `ip` = '%s'";
        MySQL_Conn();
        mysql_real_escape_string(conn, escapedName, name, strlen(name));
        mysql_real_escape_string(conn, escapedIP, ip, strlen(ip));
        mysql_real_escape_string(conn, escapedPort, port, strlen(port));
        mysql_real_escape_string(conn, escapedVersion, version, strlen(version));
     if(MySQL_CheckBan(ip) == 0) {
        sprintf(checkquery, checkqueryp1, escapedIP);
        time_t timestamp;
        timestamp = time (NULL);
        logPrintf(logfile, "Checking for existing servers in table with the same IP...\n");
        logPrintf(logfile, "Executing MySQL Query: %s\n", checkquery);
        if(mysql_query(conn, checkquery)) {
          logPrintf(logfile, "MYSQL ERROR: %s\n", mysql_error(conn));
        } else {
          res = mysql_store_result(conn);
          logPrintf(logfile, "Found %d rows...\n", mysql_num_rows(res));
          if(mysql_num_rows(res) < 1) {
             mysql_free_result(res);
	     logPrintf(logfile, "Adding the temporary server: %s:%s || Name: %s || Version: %s || Time: %ld\n", ip, port, name, version, timestamp);
             sprintf(insertquery, queryp1, escapedName, escapedIP, escapedPort, escapedVersion, timestamp);
             logPrintf(logfile, "Executing MySQL Query: %s\n", insertquery);
	     if(mysql_query(conn, insertquery)) {
                logPrintf(logfile, "MYSQL ERROR: %s\n", mysql_error(conn));
             } else {
                logPrintf(logfile, "Server added successfully!\n");
                MySQL_Stats(2);
             }
          } else {                                 
            mysql_free_result(res);
            logPrintf(logfile, "Server's IP already exists, so let's just update it instead...\n");
            logPrintf(logfile, "Updating Server Data for %s\n", ip);
            sprintf(updatequery, updatequeryp1, escapedName, escapedPort, escapedVersion, timestamp, escapedIP);
            logPrintf(logfile, "Executing MySQL Query: %s\n", updatequery);
            if(mysql_query(conn, updatequery)) {
               logPrintf(logfile, "MYSQL ERROR: %s\n", mysql_error(conn));
            } else {
               logPrintf(logfile, "Server status for Server: %s:%s || Name: %s || Version: %s || Time: %ld set successfully.\n", ip, port, name, version, timestamp);
               MySQL_Stats(7);
            }
          }
        }
     } else {
            logPrintf(logfile, "IP %s is banned so do nothing.\n", ip);
     }
}

void MySQL_ListServers(int id, long type, char *ip) {
       msg_t msg;
       int writecode;
       MySQL_Conn();
       msg.id = id;
       msg.type = type;
       char servquery[1000] = "SELECT ip, port, name, version FROM ( SELECT * FROM `ms_servers` WHERE `upnow` = '1' ORDER BY `sid` ASC) as t2 ORDER BY `sticky` DESC";
     if(MySQL_CheckBanList(ip) == 0) {
       logPrintf(logfile, "Executing MySQL Query: %s\n", servquery);
       if(mysql_query(conn, servquery)) {
          logPrintf(logfile, "MYSQL ERROR: %s\n", mysql_error(conn));
       } else {
          res = mysql_store_result(conn);
          logPrintf(logfile, "Found %d servers...\n", mysql_num_rows(res));
          while (row = mysql_fetch_row(res))
          {
		    msg_server_t *info = (msg_server_t *) msg.buffer;

      		info->header[0] = '\0'; // nothing interresting in it (for now)
       		strcpy(info->ip,      row[0]);
         	strcpy(info->port,    row[1]);
          	strcpy(info->name,    row[2]);
           	strcpy(info->version, row[3]);

            msg.length = sizeof (msg_server_t);
            logPrintf(logfile, "Sending message?\n");
            writecode = server_socket.write(&msg);
            logPrintf(logfile, "Message sent! :D\n");
            if (writecode < 0)
            {
            	dbgPrintf(LIGHTRED, "Write error... %d client %d "
             	"deleted\n", writecode, id);
              	return;
            }
		//p = (CServerItem *)servers_list.getNext();
       }
         mysql_free_result(res);
       }
     } else {
            logPrintf(logfile, "IP %s is banned so do nothing.\n", ip);
     }
}

void MySQL_ListServServers(int id, long type, char *ip) {
       msg_t msg;
       int writecode;
       static char str[1024];
       //char banqueryp1[500] = "SELECT reason,name,FROM_UNIXTIME(endtime) FROM ms_bans WHERE INET_ATON('%s') BETWEEN `ipstart` AND `ipend` AND `endtime` < %ld LIMIT 1";
       //char banquery[500];
       //time_t current_time = time (NULL);
       char servquery[1000] = "SELECT ip, port, name, version, permanent FROM ( SELECT * FROM `ms_servers` WHERE `upnow` = '1' OR `permanent` = '1' ORDER BY `sid` ASC) as ms_servers ORDER BY `sticky` DESC";
       MySQL_Conn();
     if(MySQL_CheckBanList(ip) == 0) {
       msg.id = id;
       msg.type = type;
       logPrintf(logfile, "Executing MySQL Query: %s\n", servquery);
       if(mysql_query(conn, servquery)) {
          logPrintf(logfile, "MYSQL ERROR: %s\n", mysql_error(conn));
       } else {
          res = mysql_store_result(conn);
          logPrintf(logfile, "Found %d servers...\n", mysql_num_rows(res));
          while (row = mysql_fetch_row(res))
	  {
                if(strcmp(row[4], "0") == 1)
                snprintf(str, sizeof str, "IP\t\t: %s\nPort\t\t: %s\nHostname\t: %s\nVersion\t\t: %s\nPermanent\t: %s\n", row[0], row[1], row[2], row[3], "Yes");
		else
                snprintf(str, sizeof str, "IP\t\t: %s\nPort\t\t: %s\nHostname\t: %s\nVersion\t\t: %s\nPermanent\t: %s\n", row[0], row[1], row[2], row[3], "No");

        msg.length = strlen(str)+1; // send also the '\0'
		strcpy(msg.buffer, str);
		dbgPrintf(CYAN, "Writing: (%d)\n%s\n", msg.length, msg.buffer);
		writecode = server_socket.write(&msg);
		if (writecode < 0)
		{
			dbgPrintf(LIGHTRED, "Write error... %d client %d deleted\n", writecode, id);
			return;
		}
	 }
         mysql_free_result(res);
       }
     } else {
            logPrintf(logfile, "IP %s is banned so do nothing! Let them find out for themselves.\n", ip);
     }
}

void MySQL_RemoveServer(char *ip, char *port, char *name, char *version) {
        char escapedName[255];
        char updatequery[5000];
        char updatequeryp1[5000] = "UPDATE `ms_servers` SET upnow = '0' WHERE `ip` = '%s' AND `permanent` = '0'";
        MySQL_Conn();
        mysql_real_escape_string(conn, escapedName, name, strlen(name));
        sprintf(updatequery, updatequeryp1, ip);
        logPrintf(logfile, "Executing MySQL Query: %s\n", updatequery);
        if(mysql_query(conn, updatequery)) {
           logPrintf(logfile, "MYSQL ERROR: %s\n", mysql_error(conn));
        } else {
           logPrintf(logfile, "Server: %s:%s || Name: %s || Version: %s removed successfully.\n", ip, port, name, version);
        }
}

/*
** checkPassword()
*/
static int checkPassword(char *pw)
{
	char *cpw;

	pw[15] = '\0'; // for security reason
	cpw = pCrypt(pw, "04");
	memset(pw, 0, 16); // erase that ASAP!
	if (strcmp(cpw, "04.2Fk3Ex6DI2"))
	{
		logPrintf(logfile, "Bad password\n");
		return 0;
	}
	return 1;
}

/*
** sendServersInformations()
*/
static void sendServersInformations(int id)
{
	msg_t msg;
	int writecode;
	char ip[16];
        strcpy(ip, server_socket.getClientIP(id));
	//CServerItem *p = (CServerItem *)servers_list.getFirst();

	logPrintf(logfile, "Sending servers informations\n");
	msg.id = id;
	msg.type = SEND_SERVER_MSG;
/*	while (p)
	{
		const char *str = p->getString();

		msg.length = strlen(str)+1; // send also the '\0'
		strcpy(msg.buffer, str);
		dbgPrintf(CYAN, "Writing: (%d)\n%s\n", msg.length, msg.buffer);
		writecode = server_socket.write(&msg);
		if (writecode < 0)
		{
			dbgPrintf(LIGHTRED, "Write error... %d client %d deleted\n", writecode, id);
			return;
		}
		p = (CServerItem *)servers_list.getNext();
	}*/ // BURN THE (OLD) WITCH!
	MySQL_ListServServers(id, SEND_SERVER_MSG, ip); // Awesome new MySQL Code!
	msg.length = 0;
	//dbgPrintf(CYAN, "Writing: (%d) %s\n", msg.length, "");
	writecode = server_socket.write(&msg);
	if (writecode < 0)
	{
		dbgPrintf(LIGHTRED, "Write error... %d client %d deleted\n",
			writecode, id);
	}
	MySQL_Stats(1);
}

/*
** sendShortServersInformations()
*/
static void sendShortServersInformations(int id)
{
	msg_t msg;
	int writecode;
	char ip[16];
        strcpy(ip, server_socket.getClientIP(id));
	//CServerItem *p = (CServerItem *)servers_list.getFirst();

	logPrintf(logfile, "Sending short servers informations\n");
	msg.id = id;
	msg.type = SEND_SHORT_SERVER_MSG;
	/*while (p)
	{
		msg_server_t *info = (msg_server_t *) msg.buffer;

		info->header[0] = '\0'; // nothing interresting in it (for now)
		strcpy(info->ip,      p->getIP());
		strcpy(info->port,    p->getPort());
		strcpy(info->name,    p->getName());
		strcpy(info->version, p->getVersion());

		msg.length = sizeof (msg_server_t);
		writecode = server_socket.write(&msg);
		if (writecode < 0)
		{
			dbgPrintf(LIGHTRED, "Write error... %d client %d "
				"deleted\n", writecode, id);
			return;
		}
		p = (CServerItem *)servers_list.getNext();
	}*/ // Old code that's full of fail! :D
	MySQL_ListServers(id, SEND_SHORT_SERVER_MSG, ip); // New code that's full of win!
	msg.length = 0;
	writecode = server_socket.write(&msg);
	if (writecode < 0)
	{
		dbgPrintf(LIGHTRED, "Write error... %d client %d deleted\n",
			writecode, id);
	}
	MySQL_Stats(1);
}

/*
** addServer()
*/
static void addServer(int id, char *buffer)
{
	msg_server_t *info;
	char oldversion = 0;

	//TODO: Be sure there is no flood from a given IP:
	//      If a host need more than 2 servers, then it should be registrated
	//      manually

	info = (msg_server_t *)buffer;

	// I want to be sure the informations are correct, of course!
	info->port[sizeof (info->port)-1] = '\0';
	info->name[sizeof (info->name)-1] = '\0';
	info->version[sizeof (info->version)-1] = '\0';
	// retrieve the true ip of the server
	strcpy(info->ip, server_socket.getClientIP(id));
	//strcpy(info->port, server_socket.getClientPort(id));

	if (info->version[0] == '1' &&
		info->version[1] == '.' &&
		info->version[2] == '0' &&
		info->version[3] == '9' &&
		info->version[4] == '.')
		{
			if ((info->version[5] == '2') || (info->version[5] == '3'))
			{
					oldversion = 1;
			}
		}

	if (info->version[0] == '1' &&
		info->version[1] == '.' &&
		info->version[2] == '6' &&
		info->version[3] == '9' &&
		info->version[4] == '.' &&
		info->version[5] == '6')
		{
					oldversion = 1;
		}

	if (!oldversion)
	{
        MySQL_AddServer(info->ip, info->port, info->name, info->version);
		/*server_stats.num_servers++;
		server_stats.num_add++;
		server_stats.putLastServer(info);
		*/ // OLD
		MySQL_Stats(2); // NEW! :D
	}
	else
	{
		logPrintf(logfile, "Not adding the temporary server: %s %s %s %s\n", info->ip, info->port, info->name, info->version);
	}
}

/*
** addServerv2()
*/
static void addServerv2(int id, char *buffer)
{
	msg_server_t *info;
	char Handshakepassed = 0;

	//TODO: Be sure there is no flood from a given IP:
	//      If a host need more than 2 servers, then it should be registrated
	//      manually

	info = (msg_server_t *)buffer;

	// I want to be sure the informations are correct, of course!
	info->port[sizeof (info->port)-1] = '\0';
	info->name[sizeof (info->name)-1] = '\0';
	info->version[sizeof (info->version)-1] = '\0';
	// retrieve the true ip of the server
	strcpy(info->ip, server_socket.getClientIP(id));
	//strcpy(info->port, server_socket.getClientPort(id));

	//blah blah blah http://orospakr.is-a-geek.org/projects/srb2/ticket/164 etc etc

	if (Handshakepassed)
	{
		logPrintf(logfile, "Adding the temporary server: %s %s %s %s\n", info->ip, info->port, info->name, info->version);
		servers_list.insert(info->ip, info->port, info->name, info->version, ST_TEMPORARY);
		server_stats.num_servers++;
		server_stats.num_add++;
		server_stats.putLastServer(info);
	}
}

/*
** addPermanentServer()
*/
static void addPermanentServer(char *buffer)
{
	msg_server_t *info;

	info = (msg_server_t *)buffer;

	// I want to be sure the informations are correct, of course!
	info->ip[sizeof (info->ip)-1] = '\0';
	info->port[sizeof (info->port)-1] = '\0';
	info->name[sizeof (info->name)-1] = '\0';
	info->version[sizeof (info->version)-1] = '\0';

	if (!checkPassword(info->header))
		return;

	logPrintf(logfile, "Adding the permanent server: %s %s %s %s\n", info->ip, info->port, info->name, info->version);
	servers_list.insert(info->ip, info->port, info->name, info->version, ST_PERMANENT);
	//servers_list.insert(new CServerItem(info->ip, info->port, info->name, info->version, ST_PERMANENT));
	server_stats.num_servers++;
	server_stats.num_add++;
	server_stats.putLastServer(info);
}

/*
** removeServer()
*/
static void removeServer(int id, char *buffer)
{
	msg_server_t *info;

	info = (msg_server_t *)buffer;

	// I want to be sure the informations are correct, of course!
	info->port[sizeof (info->port)-1] = '\0';
	info->name[sizeof (info->name)-1] = '\0';
	info->version[sizeof (info->version)-1] = '\0';

	// retrieve the true ip of the server
	strcpy(info->ip, server_socket.getClientIP(id));

	logPrintf(logfile, "Removing the temporary server: %s %s %s %s\n", info->ip, info->port, info->name, info->version);
/*	if (servers_list.remove(info->ip, info->port, info->name, info->version, ST_TEMPORARY))
		server_stats.num_servers--;
	else
		server_stats.num_badconnection++;
	server_stats.num_removal++;*/ // Old and fail
	MySQL_RemoveServer(info->ip, info->port, info->name, info->version); // New and win.
	MySQL_Stats(4);
}

/*
** removePermanentServer()
*/
static void removePermanentServer(char *buffer)
{
	msg_server_t *info;

	info = (msg_server_t *)buffer;

	// I want to be sure the informations are correct, of course!
	info->ip[sizeof (info->ip)-1] = '\0';
	info->port[sizeof (info->port)-1] = '\0';
	info->name[sizeof (info->name)-1] = '\0';
	info->version[sizeof (info->version)-1] = '\0';

	if (!checkPassword(info->header))
		return;

	logPrintf(logfile, "Removing the pemanent server: %s %s %s %s\n", info->ip, info->port, info->name, info->version);
	if (servers_list.remove(info->ip, info->port, info->name, info->version, ST_PERMANENT))
		server_stats.num_servers--;
	server_stats.num_removal++;
}

/*
** sendFile()
*/
static void sendFile(int id, char *buffer, FILE *f)
{
	msg_t msg;
	int count;
	int writecode;
	msg_server_t *info;

	info = (msg_server_t *)buffer;
	if (!checkPassword(info->header))
		return;

	logPrintf(logfile, "Sending file\n");
	msg.id = id;
	msg.type = SEND_FILE_MSG;
	fseek(f, 0, SEEK_SET);
	while ((count = fread(msg.buffer, 1, PACKET_SIZE, f)) > 0)
	{
		msg.length = count;
		if (count != PACKET_SIZE) // send a null terminated string
		{
			msg.length++;
			msg.buffer[count] = '\0';
		}
		writecode = server_socket.write(&msg);
		if (writecode < 0)
		{
			dbgPrintf(LIGHTRED, "Write error... %d client %d "
				"deleted\n", writecode, id);
			return;
		}
	}
	msg.length = 0;
	//dbgPrintf(CYAN, "Writing: (%d) %s\n", msg.length, "");
	writecode = server_socket.write(&msg);
	if (writecode < 0)
	{
		dbgPrintf(LIGHTRED, "Write error... %d client %d deleted\n",
			writecode, id);
	}
}

/*
** sendHttpLine()
*/
static void sendHttpLine(int id, char *lpFmt, ...)
{
	msg_t msg;
	int writecode;
	va_list arglist;

	va_start(arglist, lpFmt);
	vsnprintf(msg.buffer, sizeof msg.buffer, lpFmt, arglist);
	va_end(arglist);
	strcat(msg.buffer, "\n");

	msg.id = id;
	msg.type = SEND_HTTP_REQUEST_MSG;
	msg.length = strlen(msg.buffer);

	writecode = server_socket.write(&msg);
	if (writecode < 0)
	{
		dbgPrintf(LIGHTRED, "Write error... %d client %d deleted\n",
			writecode, id);
		return;
	}
}

/*
** sendHttpServersList()
*/
static void sendHttpServersList(int id)
{
	CServerItem *p = (CServerItem *)servers_list.getFirst();

	sendHttpLine(id, "<FONT COLOR=\"#00FF00\"><CODE>");
	if (!p)
	{
		sendHttpLine(id, "<A NAME=\"%23s\">No server connected.</A>",
			"00000000000000000000000");
	}
	else while (p)
	{
		sendHttpLine(id,
"<A NAME=\"%23s\">IP: %15s    Port: %5s    Name: %-31s    Version: %7s </A>",
p->getGuid(),p->getIP(), p->getPort(), p->getName(), p->getVersion());
		p = (CServerItem *)servers_list.getNext();
	}
	sendHttpLine(id, "</CODE></FONT>");
}

static void sendtextServersList(int id)
{
	CServerItem *p = (CServerItem *)servers_list.getFirst();

	if (!p)
	{
		sendHttpLine(id, "No SRB2 Games are running, "
			"so why don't you start one?");
	}
	else while (p)
	{
		sendHttpLine(id,
			"IP: %15s | Port: %5s | Name: %s | Version: %7s",
			p->getIP(), p->getPort(), p->getName(),
			p->getVersion());
		p = (CServerItem *)servers_list.getNext();
	}
}

static void sendRSS10ServersList(int id)
{
	CServerItem *p = (CServerItem *)servers_list.getFirst();

	sendHttpLine(id, "<items> <rdf:Seq>");
	if (!p)
	{
		sendHttpLine(id, "<rdf:li rdf:resource=\"http://srb2.servegame.org/#00000000000000000000000\" />");
	}
	else while (p)
	{
		sendHttpLine(id, "<rdf:li rdf:resource=\"http://srb2.servegame.org/#%23s\" />",p->getGuid());
		p = (CServerItem *)servers_list.getNext();
	}

	sendHttpLine(id, "</rdf:Seq> </items> </channel>");

	p = (CServerItem *)servers_list.getFirst();

	if (!p)
	{
		sendHttpLine(id, "<item rdf:about=\"http://srb2.servegame.org/#00000000000000000000000\"><title>No servers</title></item>");
	}
	else while (p)
	{
		sendHttpLine(id, "<item rdf:about=\"http://srb2.servegame.org/#%23s\"><title>%s</title><link>http://srb2.servegame.org/#%23s</link><srb2ms:address>%s</srb2ms:address><srb2ms:port>%5s</srb2ms:port><srb2ms:version>%s</srb2ms:version><dc:date>%24s</dc:date></item>",
			p->getGuid(), p->getName(), p->getGuid(), p->getIP(), p->getPort(), p->getVersion(), p->getRegtime(), p->getGuid());
		p = (CServerItem *)servers_list.getNext();
	}
}

static void sendRSS92ServersList(int id)
{
	CServerItem *p = (CServerItem *)servers_list.getFirst();

	if (!p)
	{
		sendHttpLine(id, "<item><title>No servers</title><description>I'm sorry, but no servers are running now</description></item>");
	}
	else while (p)
	{
		sendHttpLine(id, "<item><title>%s</title><description>IP: %s | Port: %5s | Version: %s</description></item>", p->getName(), p->getIP(), p->getPort(), p->getVersion());
		p = (CServerItem *)servers_list.getNext();
	}
}

static void sendMOTDText(int id)
{
	sendHttpLine(id, SERVERMOTDTEXT);
}

/*
** sendHttpRequest()
*/
static void sendHttpRequest(int id)
{
	server_stats.num_http_con++;
	logPrintf(logfile, "Sending http request\n");
	// Status
	sendHttpLine(id, "<P>Server up and running since: %s (%d days %d hours)<BR>",
		server_stats.getUptime(), server_stats.getDays(), server_stats.getHours());
	sendHttpLine(id, "Total number of connections: %d (%d HTTP requests)<BR>",
		server_stats.num_connections, server_stats.num_http_con);
	sendHttpLine(id, "Number of bad connections: %d<BR>",
		server_stats.num_badconnection);
	sendHttpLine(id, "Current number of servers: %d<BR></P>",
		server_stats.num_servers);
	// Motd
	sendHttpLine(id, "<H3>Message of the day</H3><P>%s</P>",
		server_stats.getMotd());
	// Usage
	sendHttpLine(id, "<H3>Usage</H3>");
	sendHttpLine(id, "<P>Number of server adds: %d<BR>",
		server_stats.num_add);
	sendHttpLine(id, "Number of server removals: %d<BR>",
		server_stats.num_removal);
	sendHttpLine(id, "Number of auto removals: %d<BR>",
		server_stats.num_autoremoval);
	sendHttpLine(id, "Number of list retrievals: %d<BR></P>",
		server_stats.num_retrieval);
	// Servers' list
	sendHttpLine(id, "<H3>Servers' list</H3><PRE>");
	sendHttpServersList(id);
	// Last server registered
	sendHttpLine(id, "<H3>Last server registered</H3>");
	sendHttpLine(id, "<P>%s</PRE>",
		server_stats.getLastServers());
	// Version
	sendHttpLine(id, "<H3>Version</H3>");
	sendHttpLine(id, "<P>Build date/time: %s</P>",
		server_stats.getVersion());

	server_socket.deleteClient(id); // close connection with the script
}

static void sendtextRequest(int id)
{
	server_stats.num_text_con++;
	logPrintf(logfile, "Sending text request\n");
	sendtextServersList(id);
	server_socket.deleteClient(id); // close connection with the script
}

static void sendRSS92Request(int id)
{
	server_stats.num_RSS92_con++;
	logPrintf(logfile, "Sending RSS .92 feed items request\n");
	sendRSS92ServersList(id);
	server_socket.deleteClient(id); // close connection with the script
}

static void sendRSS10Request(int id)
{
	server_stats.num_RSS10_con++;
	logPrintf(logfile, "Sending RSS 1.0 feed items request\n");
	sendRSS10ServersList(id);
	server_socket.deleteClient(id); // close connection with the script
}

static void sendMOTDRequest(int id)
{
	logPrintf(logfile, "Sending MOTD request\n");
	sendMOTDText(id);
	server_socket.deleteClient(id); // close connection with the script
}

/*
** eraseLogFile()
*/
static void eraseLogFile(char *buffer)
{
	msg_server_t *info;

	info = (msg_server_t *)buffer;
	if (!checkPassword(info->header))
		return;

	logPrintf(logfile, "Erasing file\n"); // well, quite useless!
	fclose(logfile);
	logfile = fopen("server.log", "w+t");
}

/*
** CheckHeartBeats(): check the last time a heartbeat was sent by the servers
**                    in the last 20 secs, if we had not got one, remove them
*/
static void CheckHeartBeats()
{
	static time_t lastHeartBeatcheck = 0;
	const time_t HB_timeout = 20;
	time_t cur_time = time(NULL);

	if (cur_time - lastHeartBeatcheck > HB_timeout/2) // it's high time to check the servers
	{
		CServerItem *p = (CServerItem *)servers_list.getFirst();
		while (p)
		{
			if (lastHeartBeatcheck - (p->HeartBeat) > HB_timeout // do not allow a ping higher than HB_timeout
				&& p->type != ST_PERMANENT)
			{
				CServerItem *q = p;
				p = (CServerItem *)servers_list.getNext();
				if (servers_list.remove(q))
				{
					//server_stats.num_servers--;
					//server_stats.num_autoremoval++;
					MySQL_Stats(5);
				}
			}
			else
				p = (CServerItem *)servers_list.getNext();
		}
		lastHeartBeatcheck = cur_time;
	}
}

/*
** updateServerHB()
*/
static void updateServerHB()
{
	CServerItem *p = (CServerItem *)servers_list.getFirst();
	const char *fromIP = server_socket.getUdpIP();
	const char *fromPort = server_socket.getUdpPort(false);
	bool match = false;

	while (p && !match)
	{
		if (!strcmp(fromIP, p->getIP())
			&& !strcmp(fromPort, p->getPort()))
		{
			p->HeartBeat = time(NULL);
			match = true;
		}
		p = (CServerItem *)servers_list.getNext();
	}

	// ok, if the HeartBeat doesn't match both IP and port, check on
	// just IP, and update the port of that ServerItem, yes Mystic,
	// I'm talking about you and your bad NAT router
	p = (CServerItem *)servers_list.getFirst(); // go back to start of list
	while (p && !match)
	{
		if (strcmp(fromIP, p->getIP()) == 0)
		{
			if (p->setPort(fromPort))
			{
				p->HeartBeat = time(NULL);
				match = true;
			}
		}
		p = (CServerItem *)servers_list.getNext();
	}
}

/*
** analyseUDP()
*/
static int analyseUDP(long size, char *buffer)
{
	// this would be the part of reading the PT_SERVERINFO packet,
	// but i'm not about to backport that sloppy code
	(void)size;
	(void)buffer;
	return INVALID_MSG;
}


//
// SRB2 packet checksum
//
static unsigned SRB2Checksum(doomdata_t *srb2packet, int length)
{
	unsigned c = 0x1234567;
	const int l = length - 4;
	const unsigned char *buf = (unsigned char *)srb2packet + 4;
	int i;

	for (i = 0; i < l; i++, buf++)
		c += (*buf) * (i+1);

	return c;
}

/*
** forwardAskInfoRequest()
*/
static int forwardAskInfoRequest(ms_holepunch_packet_t *mshpp)
{
	doomdata_t srb2packet;
	const char *fromIP = server_socket.getUdpIP();
	const char *fromPort = server_socket.getUdpPort(false);

	// Secretarial stuff.
	srb2packet.ack = srb2packet.ackreturn = 0;
	srb2packet.packettype = PT_ASKINFOVIAMS;

	// Time for ping calculation.
	srb2packet.msaskinfo.time = mshpp->time;

	// Client's address.
	strcpy(srb2packet.msaskinfo.clientaddr, fromIP);
	strcat(srb2packet.msaskinfo.clientaddr, ":");
	strcat(srb2packet.msaskinfo.clientaddr, fromPort);

	// Calculate checksum for SRB2.
	srb2packet.checksum = SRB2Checksum(&srb2packet, sizeof(srb2packet));

	// Send the packet.
	server_socket.writeUDP((char*)&srb2packet, sizeof(srb2packet), mshpp->ip, mshpp->port);

	return 0;
}

/*
** processAskInfoRequest()
*/
static int processAskInfoRequest(ms_holepunch_packet_t *mshpp)
{
	// Before telling the server about the prospective new client, we need
	// to make sure that the server is actually on our list. This isn't
	// strictly necessary, but it's probably a good idea.

	CServerItem *p = (CServerItem *)servers_list.getFirst();

	while (p)
	{
		if (!strcmp(mshpp->ip, p->getIP())
			&& mshpp->port == atoi(p->getPort()))
		{
			// We have a match, so send a request to the server.
			forwardAskInfoRequest(mshpp);
			return 0;
		}
		p = (CServerItem *)servers_list.getNext();
	}

	dbgPrintf(LIGHTGREEN, "%s:%d isn't on the serverlist.\n",
		mshpp->ip, mshpp->port);
	return 0;	// Not necessarily an error: server might have shut down.
}

/*
** UDPMessage()
*/
static int UDPMessage(long size, char *buffer)
{
	if (size == 4)
	{
		updateServerHB();
		return 0;
	}
	else if (size == sizeof(ms_holepunch_packet_t))
	{
		return processAskInfoRequest((ms_holepunch_packet_t*)buffer);
	}
	else if (size > 58)
	{
		dbgPrintf(LIGHTGREEN, "Got a UDP %d byte long message\n", size);
		return analyseUDP(size, buffer);
	}
	else
		return INVALID_MSG;
}

/*
** analyseMessage()
*/
static int analyseMessage(msg_t *msg)
{
	switch (msg->type)
	{
	case UDP_RECV_MSG:
		return UDPMessage(msg->length, msg->buffer);
	case ACCEPT_MSG:
		MySQL_Stats(6);
		break;
	case ADD_SERVER_MSG:
		addServer(msg->id, msg->buffer);
		break;
	case ADD_SERVERv2_MSG:
		addServerv2(msg->id, msg->buffer);
		break;
	case REMOVE_SERVER_MSG:
		removeServer(msg->id, msg->buffer);
		break;
	case ADD_PSERVER_MSG:
		addPermanentServer(msg->buffer);
		break;
	case REMOVE_PSERVER_MSG:
		removePermanentServer(msg->buffer);
		break;
	case GET_LOGFILE_MSG:
		sendFile(msg->id, msg->buffer, logfile);
		break;
	case ERASE_LOGFILE_MSG:
		eraseLogFile(msg->buffer);
		break;
	case ADD_CLIENT_MSG: // New client (unsupported)
		// TODO: add him in the player list
		break;
	case GET_SERVER_MSG:
		sendServersInformations(msg->id);
		break;
	case GET_SHORT_SERVER_MSG:
		sendShortServersInformations(msg->id);
		break;
	case HTTP_REQUEST_MSG:
		sendHttpRequest(msg->id);
		break;
	case TEXT_REQUEST_MSG:
		sendtextRequest(msg->id);
		break;
	case RSS92_REQUEST_MSG:
		sendRSS92Request(msg->id);
		break;
	case RSS10_REQUEST_MSG:
		sendRSS10Request(msg->id);
		break;
	case GET_MOTD_MSG:
		sendMOTDRequest(msg->id);
		break;
	default:
		return INVALID_MSG;
	}
	return 0;
}

/*
** main()
*/
int main(int argc, char *argv[])
{
	msg_t msg;

	if (argc <= 1)
	{
		fprintf(stderr, "usage: %s port\n", argv[0]);
		exit(1);
	}

	if (server_socket.listen(argv[1]) < 0)
	{
		fprintf(stderr, "Error while initializing the server\n");
		exit(2);
	}

	logfile = openFile("server.log");
    MySQL_Stats(8);
#if !defined (DEBUG) && !defined (_WIN32)
	switch (fork())
	{
	case 0: break;  // child
	case -1: printf("Error while launching the server in background\n"); return -1;
	default: return 0; // parent: keep child in background
	}
#endif
	srand((unsigned)time(NULL)); // Alam: GUIDs
	for (;;)
	{
		memset(&msg, 0, sizeof (msg)); // remove previous message
		if (!server_socket.read(&msg))
		{
			// valid message: header message seems ok
			analyseMessage(&msg);
			//servers_list.show(); // for debug purpose
		}
		CheckHeartBeats();
	}
	
	/* NOTREACHED */
	return 0;
}
