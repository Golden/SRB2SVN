// =================== SRB2Live Functions Code =====================
// Mod coded by Cueball61 and Oogaland
//     Thanks to:
//          Alam Arias for random bits of help
//          Logan_GBA for the SVN and help :3
//          The Beta Testers:
//              Luke The Hedgehog/Someone
//              SRB2-Playah
//              Flame_The_Hedgehog/XP-Tan
//              BlazingPhoenix
//              Furyhunter
//              Naga-c
//          Sonic Team Junior for releasing the SRB2 Source Code
//   Stored on the Sonic Robo Blast 2 Subversion Repository
//   File Includes:
//        The Connection Function
//        Achievement Check Functions
//        The Updater Function
// =================== SRB2Live Functions Code =====================

#ifdef ADVANCED

#include "../d_englsh.h"
#include "../g_game.h"
#include "../m_menu.h"
#include "../m_random.h"
#include "../p_local.h"
#include "../p_mobj.h"
#include "../r_main.h"
#include "../s_sound.h"
#include "LIVE_LULS.h"

#define BLOODFLAGS (MF_NOBLOCKMAP|MF_SLIDEME|MF_NOCLIPTHING)
#define BLOODFUSE (10*TICRATE)
void LULS_PlayerPop(player_t *player)
{
	const fixed_t ns = 10*FRACUNIT;
	int i;
	mobj_t *mo;

	// Blood SPEWS everywhere!
	for (i = 0; i < 32; i++)
	{
		const angle_t fa = (i*FINEANGLES/16) & FINEMASK;
		mo = P_SpawnMobj(player->mo->x, player->mo->y, player->mo->z+player->mo->height/2, MT_GOOP);
		mo->momx = FixedMul(finesine[fa],ns);
		mo->momy = FixedMul(finecosine[fa],ns);
		if (i > 15)
			P_SetObjectMomZ(mo, ns, false);
		else
			mo->momz = player->mo->momz;
		mo->flags = BLOODFLAGS | (6<<MF_TRANSSHIFT);
		mo->fuse = BLOODFUSE;
	}

	// And one that goes straight up and back down, too.
	mo = P_SpawnMobj(player->mo->x, player->mo->y, player->mo->z, MT_GOOP);
	P_SetObjectMomZ(mo, ns*2, false);
	mo->flags = BLOODFLAGS | (6<<MF_TRANSSHIFT);
	mo->fuse = BLOODFUSE;

	// Spawn monitor pop
	S_StartSound(player->mo,sfx_pop);
	mo = P_SpawnMobj(player->mo->x, player->mo->y, player->mo->z, MT_MONITOREXPLOSION);
	P_SetMobjState(mo, S_XPLD1);

	// Spawn shoes
	mo = P_SpawnMobj(player->mo->x, player->mo->y, player->mo->z, MT_EMPTYSHOES);
	mo->angle = player->mo->angle;
	P_InstaThrust(mo, mo->angle, player->speed*FRACUNIT);
	mo->momz = player->mo->momz;
	P_SetObjectMomZ(mo, 8*FRACUNIT, true);
	mo->fuse = BLOODFUSE;

	// Pwn player
	P_ResetPlayer(player);
	P_KillMobj(player->mo, NULL, NULL);
	player->mo->health = 0; // So the rings stop following you and MF2_DONTDRAW isn't unset.
	P_UnsetThingPosition(player->mo);
	player->mo->momx = player->mo->momy = player->mo->momz = player->mo->pmomz = 0;
	player->mo->flags = MF_NOBLOCKMAP|MF_NOGRAVITY|MF_NOCLIP;
	player->mo->flags2 |= MF2_DONTDRAW;
	P_SetThingPosition(player->mo);

}
#undef BLOODFLAGS
#undef BLOODFUSE
#endif
