// LIVE_Sockets.h by Cueball61
// Keep your hands off >:(


#ifdef LIVE_SOCKETS
#ifndef __LIVE_SOCKETS__
#define __LIVE_SOCKETS__

void LIVE_Achievement(int userid, int achievementid, char *userpass);
void LIVE_Connect(void);
void LIVE_CheckVersion(void);
void LIVE_Disconnect(void);
void LIVE_Send(void);
int LIVE_Login(char *username, char *userpass);
void LIVE_StatusSingle(int location, char *live_username);
void LIVE_StatusDual(int location, char szIP[16], char *live_username);

typedef struct
{
		int iMessageType;
		int iLocation;
		int iParam1, iParam2;
		char szPassword[16];
		char szUsername[256];
		char *szVersion;
		char szIP[16];
		char *userEmail;
		char *userZone;
		char *userTotalRings;
		char *userSRB2Score;
		char *userReputation;
	} LIVE_PACKET;

enum MESSAGE_TYPES
{
	MSG_ACHIEVEMENT,
	MSG_LOGIN,
	MSG_STATISTICS,
	MSG_CONFIRM_ACHIEVEMENT,
	MSG_VERSION,
	MSG_LOCATION,
	MSG_LOGIN_SUCCESS,
	MSG_LOGIN_FAILED
};

enum LOCATIONS
{
	LOC_INGAME,
	LOC_HOSTING,
	LOC_MENU,
	LOC_SINGLE
};
#endif
#endif
