

#include "LIVE_Info.h"
#include "../doomstat.h"
#include "../s_sound.h"

#ifdef ADVANCED

void LIVE_Init(void)
{
     size_t thing;
     
     thing = sfx_rfcap1;
     S_sfx[thing] = S_sfx[sfx_None];
     S_sfx[thing].name = "rfcap1";
     S_sfx[thing].singularity = true;
     S_sfx[thing].priority = 64;
     S_sfx[thing].pitch = SF_X4AWAYSOUND|SF_X8AWAYSOUND|SF_NOINTERRUPT; 
     
     thing = sfx_rftak1;
     S_sfx[thing] = S_sfx[sfx_None];
     S_sfx[thing].name = "rftak1";
     S_sfx[thing].singularity = true;
     S_sfx[thing].priority = 64;
     S_sfx[thing].pitch = SF_X4AWAYSOUND|SF_X8AWAYSOUND|SF_NOINTERRUPT; 
     
     thing = sfx_rfdro1;
     S_sfx[thing] = S_sfx[sfx_None];
     S_sfx[thing].name = "rfdro1";
     S_sfx[thing].singularity = true;
     S_sfx[thing].priority = 64;
     S_sfx[thing].pitch = SF_X4AWAYSOUND|SF_X8AWAYSOUND|SF_NOINTERRUPT; 
     
     thing = sfx_rfret1;
     S_sfx[thing] = S_sfx[sfx_None];
     S_sfx[thing].name = "rfret1";
     S_sfx[thing].singularity = true;
     S_sfx[thing].priority = 64;
     S_sfx[thing].pitch = SF_X4AWAYSOUND|SF_X8AWAYSOUND|SF_NOINTERRUPT; 
     
     thing = sfx_rfres1;
     S_sfx[thing] = S_sfx[sfx_None];
     S_sfx[thing].name = "rtres1";
     S_sfx[thing].singularity = true;
     S_sfx[thing].priority = 64;
     S_sfx[thing].pitch = SF_X4AWAYSOUND|SF_X8AWAYSOUND|SF_NOINTERRUPT; 
     
     thing = sfx_bfcap1;
     S_sfx[thing] = S_sfx[sfx_None];
     S_sfx[thing].name = "bfcap1";
     S_sfx[thing].singularity = true;
     S_sfx[thing].priority = 64;
     S_sfx[thing].pitch = SF_X4AWAYSOUND|SF_X8AWAYSOUND|SF_NOINTERRUPT; 
     
     thing = sfx_bftak1;
     S_sfx[thing] = S_sfx[sfx_None];
     S_sfx[thing].name = "bftak1";
     S_sfx[thing].singularity = true;
     S_sfx[thing].priority = 64;
     S_sfx[thing].pitch = SF_X4AWAYSOUND|SF_X8AWAYSOUND|SF_NOINTERRUPT; 
     
     thing = sfx_bfdro1;
     S_sfx[thing] = S_sfx[sfx_None];
     S_sfx[thing].name = "bfdro1";
     S_sfx[thing].singularity = true;
     S_sfx[thing].priority = 64;
     S_sfx[thing].pitch = SF_X4AWAYSOUND|SF_X8AWAYSOUND|SF_NOINTERRUPT; 
     
     thing = sfx_bfret1;
     S_sfx[thing] = S_sfx[sfx_None];
     S_sfx[thing].name = "bfret1";
     S_sfx[thing].singularity = true;
     S_sfx[thing].priority = 64;
     S_sfx[thing].pitch = SF_X4AWAYSOUND|SF_X8AWAYSOUND|SF_NOINTERRUPT; 
     
     thing = sfx_bfres1;
     S_sfx[thing] = S_sfx[sfx_None];
     S_sfx[thing].name = "btres1";
     S_sfx[thing].singularity = true;
     S_sfx[thing].priority = 64;
     S_sfx[thing].pitch = SF_X4AWAYSOUND|SF_X8AWAYSOUND|SF_NOINTERRUPT; 

     thing = sfx_eggdm1;
     S_sfx[thing] = S_sfx[sfx_None];
     S_sfx[thing].name = "eggdm1";
     S_sfx[thing].singularity = true;
     S_sfx[thing].priority = 64;
     S_sfx[thing].pitch = SF_X4AWAYSOUND|SF_X8AWAYSOUND|SF_NOINTERRUPT; 
     
     thing = sfx_eggko1;
     S_sfx[thing] = S_sfx[sfx_None];
     S_sfx[thing].name = "eggko1";
     S_sfx[thing].singularity = true;
     S_sfx[thing].priority = 64;
     S_sfx[thing].pitch = SF_X4AWAYSOUND|SF_X8AWAYSOUND|SF_NOINTERRUPT;
     
     thing = sfx_eggsp1;
     S_sfx[thing] = S_sfx[sfx_None];
     S_sfx[thing].name = "eggsp1";
     S_sfx[thing].singularity = true;
     S_sfx[thing].priority = 64;
     S_sfx[thing].pitch = SF_X4AWAYSOUND|SF_X8AWAYSOUND|SF_NOINTERRUPT; 
     
     thing = sfx_eggom1;
     S_sfx[thing] = S_sfx[sfx_None];
     S_sfx[thing].name = "eggom1";
     S_sfx[thing].singularity = true;
     S_sfx[thing].priority = 64;
     S_sfx[thing].pitch = SF_X4AWAYSOUND|SF_X8AWAYSOUND|SF_NOINTERRUPT;
     
     thing = sfx_eggcf1;
     S_sfx[thing] = S_sfx[sfx_None];
     S_sfx[thing].name = "eggcf1";
     S_sfx[thing].singularity = true;
     S_sfx[thing].priority = 64;
     S_sfx[thing].pitch = SF_X4AWAYSOUND|SF_X8AWAYSOUND|SF_NOINTERRUPT;  
     
     thing = sfx_sontd1;
     S_sfx[thing] = S_sfx[sfx_None];
     S_sfx[thing].name = "sontd1";
     S_sfx[thing].singularity = true;
     S_sfx[thing].priority = 64;
     S_sfx[thing].pitch = SF_X4AWAYSOUND|SF_X8AWAYSOUND|SF_NOINTERRUPT; 
     
     thing = sfx_sonas1;
     S_sfx[thing] = S_sfx[sfx_None];
     S_sfx[thing].name = "sonas1";
     S_sfx[thing].singularity = true;
     S_sfx[thing].priority = 64;
     S_sfx[thing].pitch = SF_X4AWAYSOUND|SF_X8AWAYSOUND|SF_NOINTERRUPT; 
     
     thing = sfx_soncf1;
     S_sfx[thing] = S_sfx[sfx_None];
     S_sfx[thing].name = "soncf1";
     S_sfx[thing].singularity = true;
     S_sfx[thing].priority = 64;
     S_sfx[thing].pitch = SF_X4AWAYSOUND|SF_X8AWAYSOUND|SF_NOINTERRUPT; 
     
     thing = sfx_sonom1;
     S_sfx[thing] = S_sfx[sfx_None];
     S_sfx[thing].name = "sonom1";
     S_sfx[thing].singularity = true;
     S_sfx[thing].priority = 64;
     S_sfx[thing].pitch = SF_X4AWAYSOUND|SF_X8AWAYSOUND|SF_NOINTERRUPT; 

	 thing = mus_rickastley;
	 S_music[thing] = S_music[mus_None];
	 S_music[thing].name = "rickastley";
     
     thing = MT_EMPTYSHOES;
	 mobjinfo[thing] = mobjinfo[MT_DISS];
	 mobjinfo[thing].spawnstate = S_EMPTYSHOES;
	 //mobjinfo[thing].seesound = sfx_pop;
	 mobjinfo[thing].flags = MF_NOBLOCKMAP|MF_SCENERY;

	 thing = S_EMPTYSHOES;
	 states[thing] = states[S_NULL];
	 states[thing].sprite = SPR_SHOE;

	 thing = SPR_SHOE;
	 strcpy(sprnames[thing],"SHOE");
}
#endif
