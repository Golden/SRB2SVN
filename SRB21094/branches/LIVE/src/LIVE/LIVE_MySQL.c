// =================== SRB2Live MySQL Code =====================
// Mod coded by Cueball61 and Oogaland
//     Thanks to:
//          Alam Arias for random bits of help
//          Logan_GBA for the SVN and help :3
//          The Beta Testers:
//              Luke The Hedgehog/Someone
//              SRB2-Playah
//              Spazzo
//              Flame_The_Hedgehog/XP-Tan
//              BlazingPhoenix
//              Furyhunter
//              Naga-c
//   Sonic Team Junior for releasing the SRB2 Source Code
//   Stored on the Sonic Robo Blast 2 Subversion Repository
//   File Includes:
//        The Connection Function
//	      The MySQL Functions
// =================== SRB2Live MySQL Code =====================

#ifdef MYSQL
#include "mysql_version.h"
#include "mysql.h"
#endif
#include <stdio.h>
#include "../doomdef.h"
#ifdef SEPWICH
#define HOSTNAME "mysql.sepwich.com"
#define USER "cue_srb2live"
#define PASSWORD "jtehasrabieslol345"
#define DATABASE "cue_srb2live"
#else
#define HOSTNAME "86.16.151.87"
#define USER "srb2live_client"
#define PASSWORD "jtehasrabieslol345"
#define DATABASE "srb2"
#endif
#ifdef MYSQL
void LIVE_CheckVersion(void) {

   if (mysql_query(conn, "SELECT * FROM `settings` LIMIT 0,1")) {
      I_Error("%s\n", mysql_error(conn));
   }

   res = mysql_use_result(conn);
   
   while ((checkver = mysql_fetch_row(res))) {
         if (strcmp(checkver[3], VERSIONSTRING) != 0) {
             ShellExecute(NULL, "open", checkver[5], NULL, NULL, SW_SHOWNORMAL);
             I_Error(" You do not have the latest SRB2Live version!\n Current Version is%s, you have%s!\n Please update your version of SRB2Live by going to the release thread\n on the Sonic Robo Blast 2 Message Board!\n", checkver[3], VERSIONSTRING);
         } else {
             CONS_Printf("\nYour version of SRB2Live is up to date.\n\n");
             CONS_Printf("SRB2Live is a project that took many months in planning, procrastination, laziness and coding. Thanks to all the voice actors, coding consultants, beta testers etc.\n\n");
             CONS_Printf("SRB2Live is not affiliated in any way with Microsoft or Xbox Live, it's just a name, damnit!\n\n");
             CONS_Printf("---------SRB2Live MOTD---------\n       %s\n-------End SRB2Live MOTD-------\n\n", checkver[1]);
         }
}

   /* Release memory used to store results and close connection */
   mysql_free_result(res);
}

void LIVE_Login(char *loginusername, char *loginpassword) {
   CONS_Printf("Checking for user %s with the password %s", loginusername, loginpassword);
   char *server = HOSTNAME;
   char *user = USER;
   char *password = PASSWORD;
   char *database = DATABASE;
   int liveloggedin;
//   long hashedpassword;
   MYSQL *conn;
   MYSQL_RES *res;
   MYSQL_ROW logindetails;
   char *querypart1;
   char *queryfull;
   
   querypart1 = "SELECT * FROM `users` WHERE `username` = ";
   queryfull = strcat(querypart1, loginusername);
   
   conn = mysql_init(NULL);
   
     /* Connect to database */
   if (!mysql_real_connect(conn, server,
      user, password, database, 0, NULL, 0)) {
      CONS_Printf("%s\n", mysql_error(conn));
   }
   
   /* send SQL query */
   if (mysql_query(conn, queryfull)) {
      CONS_Printf("%s\n", mysql_error(conn));
   }
   res = mysql_use_result(conn);
   if (mysql_num_rows(res) != 1) {
       CONS_Printf("Username does not exist!");
   }
   else if (strcmp(logindetails[1],loginpassword) != 0) {
       CONS_Printf("Password incorrect!");
   } else {
       logindetails = mysql_fetch_row(res);
       CONS_Printf("Login successful!");
       char *liveuser_name;
       char *liveuser_srb2score;
       char *liveuser_reputation;
       char *liveuser_zone;
       char *liveuser_id;
       char *liveuser_password;
       
       liveuser_password = logindetails[1];
       liveuser_id = logindetails[2];
       liveuser_name = logindetails[0];
       liveuser_srb2score = logindetails[3];
       liveuser_reputation = logindetails[4];
       liveuser_zone = logindetails[7];
       liveloggedin = 1;
   }
       
}
     
#endif
