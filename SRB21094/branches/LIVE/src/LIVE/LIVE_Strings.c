// SRB2Live String Functions
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "LIVE_Strings.h"
// String Replacement

void strrepl(char *input, const char *stra, const char *strb, char **output)
{
	const int lena = strlen(stra);
	const int lenb = strlen(strb);
	const int delta = lenb - lena;
	int outputbuflen = strlen(input) + 1;
	char *p;
	
	char *p2 = *output = malloc(outputbuflen);

	while ((p = strstr(input, stra)))
	{
		// Resize the allocated memory if needed
		if (delta > 0)
			realloc(*output, outputbuflen += delta);

		strncpy(p2, input, p-input);

		p2 += p-input;
		strcpy(p2, strb);
		p2 += lenb;

		input = p + lena;
	}

	strcpy(p2, input);
}
