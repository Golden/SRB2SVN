// =================== SRB2Live Functions Code =====================
// Mod coded by Cueball61 and Oogaland
//     Thanks to:
//          Alam Arias for random bits of help
//          Logan_GBA for the SVN and help :3
//          The Beta Testers:
//              Luke The Hedgehog/Someone
//              SRB2-Playah
//              Flame_The_Hedgehog/XP-Tan
//              BlazingPhoenix
//              Furyhunter
//              Naga-c
//          Sonic Team Junior for releasing the SRB2 Source Code
//   Stored on the Sonic Robo Blast 2 Subversion Repository
//   File Includes:
//        The Connection Function
//        Achievement Check Functions
//        The Updater Function
// =================== SRB2Live Functions Code =====================
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "../doomdef.h"
#include "../doomstat.h"
#ifdef LIVE_SOCKETS
void LIVE_SaveLogin(char *username, char *password)
{
  FILE *file; /* declare a FILE pointer */

  file = fopen("srb2live.cfg", "w"); 
  /* create a text file for writing */

  if(file==NULL) {
    CONS_Printf("Error: can't create srb2live.cfg.\n");
    /* fclose(file); DON'T PASS A NULL POINTER TO fclose !! */
  }
  else {
    printf("srb2live.cfg created. Now closing it...\n");
    fclose(file);
  }

    fprintf(file, "// SRB2Live Login Details.\n"); // Header message
    fprintf(file, "live_username %s\n", username);
    fprintf(file, "live_password %s\n", password);
	fclose(file);
}

void LIVE_ReadLogin(void)
{
     FILE *lpf = fopen("srb2live.cfg", "r");
     if(lpf == fopen("srb2live.cfg", "r")) {
// We limit ourselves to 256-char buffers. This is bad, but will
// suffice for this example.
     char szBuffer[256];

     while(fgets(szBuffer, 256, lpf))
     {
	char *szToken = strtok(szBuffer, " \t");

	if(szToken)
	{
		if(strcmp(szToken, "live_username") == 0)
		{
			szToken = strtok(NULL, "");
                        live_username = malloc(sizeof(char) * (strlen(szToken) + 1));
                        strcpy(live_username, szToken);
                        
		}
		else if(strcmp(szToken, "live_password") == 0)
		{
			szToken = strtok(NULL, "");
                        live_password = malloc(sizeof(char) * (strlen(szToken) + 1));
                        strcpy(live_password, szToken);
		}
		else {} // No matching var name.
	}
	else {} // Blank/whitespace-only line.
}

fclose(lpf);
CONS_Printf("Taken Username: %s and Password: %s from SRB2Live.cfg", live_username, live_password);
} else {
        CONS_Printf("SRB2Live.cfg not found, please use the SRB2Live Login Menu to login!");
}
}
#endif
