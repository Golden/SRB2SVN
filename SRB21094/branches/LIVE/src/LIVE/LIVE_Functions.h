// =================== SRB2Live Functions Code =====================
// Mod coded by Cueball61 and Oogaland
//     Thanks to:
//          Alam Arias for random bits of help
//          Logan_GBA for the SVN and help :3
//          The Beta Testers:
//              Luke The Hedgehog/Someone
//              SRB2-Playah
//              Flame_The_Hedgehog/XP-Tan
//              BlazingPhoenix
//              Furyhunter
//              Naga-c
//          Sonic Team Junior for releasing the SRB2 Source Code
//   Stored on the Sonic Robo Blast 2 Subversion Repository
//   File Includes:
//        The Connection Function
//        Achievement Check Functions
//        The Updater Function
// =================== SRB2Live Functions Code =====================
#ifdef ADVANCED
#ifndef __LIVE_FUNCTIONS__
#define __LIVE_FUNCTIONS__
void LIVE_SaveLogin(char *username, char *password);
void LIVE_ReadLogin(void);
#endif
#endif
