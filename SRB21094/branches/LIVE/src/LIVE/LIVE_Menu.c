// =================== SRB2Live Menu Code =====================
// Mod coded by Cueball61 and Oogaland
//     Thanks to:
//          Alam Arias for random bits of help
//          Logan_GBA for the SVN and help :3
//          The Beta Testers:
//              Luke The Hedgehog/Someone
//              SRB2-Playah
//              Flame_The_Hedgehog/XP-Tan
//              BlazingPhoenix
//              Furyhunter
//              Naga-c
//          Sonic Team Junior for releasing the SRB2 Source Code
//   Stored on the Sonic Robo Blast 2 Subversion Repository
//   File Includes:
//        Menu Code
//        SRB2Live Guide Code
// =================== SRB2Live Menu Code =====================

//===========================================================================
//                        SRB2Live Main Menu
//===========================================================================




#ifdef LIVE
//===========================================================================
//                        SRB2Live Login Menu
//===========================================================================
static char username[MAXPLAYERNAME],password[MAXPLAYERNAME],hiddenpass[MAXPLAYERNAME];

static void N_HandleUsername(int choice)
{
	size_t   l;
	boolean  exitmenu = false;  // exit to previous menu and send name change

	switch (choice)
	{
		case KEY_DOWNARROW:
			S_StartSound(NULL,sfx_menu1); // Tails
			if (itemOn+1 >= currentMenu->numitems)
				itemOn = 1;
			else itemOn++;
			break;

		case KEY_UPARROW:
			S_StartSound(NULL,sfx_menu1); // Tails
			if (!itemOn)
				itemOn = (short)(currentMenu->numitems-1);
			else itemOn--;
			break;

		case KEY_ENTER:
			if(!username[0])
				break;
			if(!password[0])
			{
				itemOn = 2;
				break;
			}
			//M_Live();
			break;

		case KEY_ESCAPE:
			exitmenu = true;
			break;

		case KEY_BACKSPACE:
			if ((l = strlen(username))!=0)
			{
				S_StartSound(NULL,sfx_menu1); // Tails
				username[l-1] =0;
			}
			break;

		default:
			if (choice < 32 || choice > 127)
				break;
			l = strlen(username);
			if (l < MAXPLAYERNAME-1)
			{
				S_StartSound(NULL,sfx_menu1); // Tails
				username[l] =(char)choice;
				username[l+1] =0;
			}
			break;
	}

	if (exitmenu)
	{
		if (currentMenu->prevMenu)
			M_SetupNextMenu (currentMenu->prevMenu);
		else
			M_ClearMenus(true);
	}
}

static void N_HandlePassword(int choice)
{
	size_t   l;
	boolean  exitmenu = false;  // exit to previous menu and send name change

	switch (choice)
	{
		case KEY_DOWNARROW:
			S_StartSound(NULL,sfx_menu1); // Tails
			if (itemOn+1 >= currentMenu->numitems)
				itemOn = 1;
			else itemOn++;
			break;

		case KEY_UPARROW:
			S_StartSound(NULL,sfx_menu1); // Tails
			if (!itemOn)
				itemOn = (short)(currentMenu->numitems-1);
			else itemOn--;
			break;

		case KEY_ENTER:
			if(!password[0])
				break;
			if(!username[0])
			{
				itemOn = 1;
				break;
			}
			//M_Live();
			break;

		case KEY_ESCAPE:
			exitmenu = true;
			break;

		case KEY_BACKSPACE:
			if ((l = strlen(password))!=0)
			{
				S_StartSound(NULL,sfx_menu1); // Tails
				password[l-1] =0;
				hiddenpass[l-1] =0;
			}
			break;

		default:
			if (choice < 32 || choice > 127)
				break;
			l = strlen(password);
			if (l < MAXPLAYERNAME-1)
			{
				S_StartSound(NULL,sfx_menu1); // Tails
				password[l] =(char)choice;
				password[l+1] =0;
				hiddenpass[l] = '*';
				hiddenpass[l+1] =0;
			}
			break;
	}

	if (exitmenu)
	{
		if (currentMenu->prevMenu)
			M_SetupNextMenu (currentMenu->prevMenu);
		else
			M_ClearMenus(true);
	}
}

static menuitem_t  LoginMenu[] =
{
	{IT_WHITESTRING | IT_SPACE, NULL, "              Log on to the SRB2Live Server", NULL, 0},
	{IT_KEYHANDLER | IT_STRING, NULL,  "Username:", N_HandleUsername, 16},
	{IT_KEYHANDLER | IT_STRING, NULL, "   Password:", N_HandlePassword, 24}
};

static void M_DrawLoginMenu(void)
{
	M_DrawTextBox(128-84-8,120,strlen(LoginMenu[1].text)+MAXPLAYERNAME,2);

	// use generic drawer for cursor, items and title
	M_DrawGenericMenu();

	// draw name string
	V_DrawString (128,128,0,username);
	V_DrawString (128,136,0,hiddenpass);

	// draw text cursor for name
	if(skullAnimCounter < 4)
		switch(itemOn)
		{
			case 1:
				V_DrawCharacter(128+V_StringWidth(username),128,'|');
				break;
			case 2:
				V_DrawCharacter(128+V_StringWidth(hiddenpass),136,'|');
				break;
			default:
				break;
		}
}

menu_t LoginDef =
{
	NULL,
	"Log in",
	sizeof(LoginMenu) / sizeof(menuitem_t),
	&MainDef,
	LoginMenu,
	M_DrawLoginMenu,
	128-84,112,
	1,
	NULL
};

void M_Live(int choice)
{
	(void)choice;
	memset(username,0,sizeof(username));
	memset(password,0,sizeof(password));
	memset(hiddenpass,0,sizeof(hiddenpass));
	currentMenu->lastOn = itemOn;
	M_SetupNextMenu(&LoginDef);
}
#endif
