#include "LIVE_Functions.h"
#ifdef LIVE_SOCKETS
#include <stdio.h>
#include <stdlib.h>
#include <winsock2.h>
#include "../md5.h"
#include "../Doomdef.h"
#include "LIVE_Sockets.h"
#include <string.h>
char* IPAddress = "192.168.2.4";
int LiveReceive;
#define SERVER_PORT 5031


SOCKET livesock;
int sockerror = 0;
LIVE_PACKET livepak;
SOCKET ConnectSocket(char* IPAddress)
{
	DWORD dwDestAddr;
	SOCKADDR_IN sockAddrDest;
	SOCKET sockDest;

	// Create socket
	sockDest = socket(AF_INET, SOCK_STREAM, 0);

	if(sockDest == SOCKET_ERROR)
	{
		CONS_Printf("Could not create socket: %d\n", WSAGetLastError());
		return INVALID_SOCKET;
	}

	// Convert address to in_addr (binary) format
	dwDestAddr = inet_addr(IPAddress);

	// Initialize SOCKADDR_IN with IP address, port number and address family
	memcpy(&sockAddrDest.sin_addr, &dwDestAddr, sizeof(DWORD));
	
	sockAddrDest.sin_port = htons(SERVER_PORT);
	sockAddrDest.sin_family = AF_INET;

	// Attempt to connect to server
	if(connect(sockDest, (LPSOCKADDR)&sockAddrDest, sizeof(sockAddrDest)) == SOCKET_ERROR)
	{
		CONS_Printf("Could not connect to SRB2Live Server. Error Code: %d\n", WSAGetLastError());
		closesocket(sockDest);
		return INVALID_SOCKET;
	}
	
	return sockDest;
}

void LIVE_Connect(void)
{
    WSADATA wsaData;
    
    CONS_Printf("Connecting to SRB2Live Server...\n");
    
    if(WSAStartup(MAKEWORD(1, 1), &wsaData) != 0)
    {
		CONS_Printf("Could not initialize sockets.\n");
		sockerror = 1;
    }
	livesock = ConnectSocket(IPAddress);
	if(livesock == INVALID_SOCKET)
	{
		CONS_Printf("Socket Error: %d\n", WSAGetLastError());
		sockerror = 1;
	}
    if(sockerror == 0)
    CONS_Printf("Connection to SRB2Live Server Successful!\n");
}

void LIVE_Send(void)
{
        if(!livesock)
        LIVE_Connect();
   	send(livesock, (char*)&livepak, sizeof(livepak), 0);
}
int LIVE_Receive(void)
{
    LiveReceive = recv(livesock, (char*)&livepak, sizeof(livepak), 0);
    if(LiveReceive != 0) {
           if(livepak.iMessageType == MSG_VERSION)
           {
             CONS_Printf("Server sent version number:%s\n", livepak.szVersion);
             if(strcmp(livepak.szVersion, VERSIONSTRING) == 1)
             return 1;
             else
             return 0;
           } else if(livepak.iMessageType == MSG_CONFIRM_ACHIEVEMENT) {
             CONS_Printf("Achievement ID: %d confirmed.\n", livepak.iParam1);
             return 1;
           } else if(livepak.iMessageType == MSG_LOGIN_SUCCESS) {
             CONS_Printf("Login with username %s successful!\n", livepak.szUsername);
             return 1;
           } else if(livepak.iMessageType == MSG_LOGIN_FAILED) {
             CONS_Printf("Username or password incorrect.\n");
             return 0;
           } else {
             return 0;
           }
    } else {
             return 0;
    }
}
void LIVE_Disconnect(void)
{
   if(livesock)
    closesocket(livesock);
}

void LIVE_Achievement(int userid, int achievementid, char *userpass)
{
	livepak.iMessageType = MSG_ACHIEVEMENT;
	livepak.iParam1 = userid;
	livepak.iParam2 = achievementid;
        md5_buffer(userpass, sizeof(userpass), livepak.szPassword);
	LIVE_Send();
	LIVE_Receive();
}

void LIVE_StatusSingle(int location, char *live_username)
{
     if(sockerror == 0) {
        livepak.iMessageType = MSG_LOCATION;
        livepak.iLocation = location;
        strcpy(livepak.szUsername, live_username);
        LIVE_Send();
     }
}

void LIVE_StatusDual(int location, char szIP[16], char *live_username)
{
     if(sockerror == 0) {
        livepak.iMessageType = MSG_LOCATION;
        livepak.iLocation = location;
        strncpy(livepak.szIP, szIP, 16);
        strcpy(livepak.szUsername, live_username);
        LIVE_Send();
     }
}

void LIVE_CheckVersion(void)
{
   if(sockerror == 0) {
    livepak.iMessageType = MSG_VERSION;
    strcpy(livepak.szVersion,VERSIONSTRING);
    LIVE_Send();
    LIVE_Receive();
   }
}

int LIVE_Login(char *username, char *userpass)
{
   if(sockerror == 0) {
    LIVE_SaveLogin(username, userpass);

    livepak.iMessageType = MSG_LOGIN;
    strcpy(livepak.szUsername, username);
    strcpy(livepak.szVersion, VERSIONSTRING);
    md5_buffer(userpass, sizeof(userpass), livepak.szPassword);
    LIVE_Send();
    if(LIVE_Receive() == 1) {
       return 1;
    } else {
       return 0;
    }
  } else {
    return 0;
  }
}
#endif
