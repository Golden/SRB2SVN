// SU - SRB2 Universe
// By Jason the Echidna
// MINE! NO STEALY!

#ifdef UNIVERSE
#ifndef __SU_MENU__
#define __SU_MENU__

// Menus
#include "../../m_menu.h"
extern menu_t SUDef;

// Functions
void M_DrawCenteredMenu(void);
void SU_SkillMenu(void);
void SU_Teleport2LobbyMenu(void);
void SU_Teleport2HUBMenu(void);
void SU_Teleport2MapMenu(void);
void SU_Teleport2NextMenu(void);
void SU_Teleport2LastMenu(void);

#endif
#endif
