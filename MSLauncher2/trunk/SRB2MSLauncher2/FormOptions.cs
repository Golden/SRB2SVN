using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace SRB2MSLauncher2
{
	public partial class FormOptions : Form
	{
		private Options options;

		public FormOptions(Options options)
		{
			InitializeComponent();
			this.options = options;
			options.AddBinariesToListView(listviewBinaries);
			textboxParams.Text = options.Params;
			textboxMSAddress.Text = options.MSAddress;
			textboxMSPort.Text = Convert.ToString(options.MSPort);
			chkShowDefaultWads.Checked = options.ShowDefaultWads;
		}

		private void btnOK_Click(object sender, EventArgs e)
		{
			options.SetBinariesFromListView(listviewBinaries);
			options.Params = textboxParams.Text;
			options.MSAddress = textboxMSAddress.Text;
			try { options.MSPort = Convert.ToUInt16(textboxMSPort.Text); }
			catch { options.MSPort = Options.MS_DEFAULT_PORT; }
			options.ShowDefaultWads = chkShowDefaultWads.Checked;
			Close();
		}

		private void btnAdd_Click(object sender, EventArgs e)
		{
			listviewBinaries.Items.Add(new ListViewItem(new string[] {"[New Version]", ""}));
		}

		private void btnDel_Click(object sender, EventArgs e)
		{
			if (listviewBinaries.SelectedItems.Count > 0)
				listviewBinaries.Items.Remove(listviewBinaries.SelectedItems[0]);
		}

		private void btnBrowse_Click(object sender, EventArgs e)
		{
			if (listviewBinaries.SelectedItems.Count > 0 &&
				openFileDialog1.ShowDialog() == DialogResult.OK)
				textboxBinary.Text = openFileDialog1.FileName;
		}

		private void textboxVersion_TextChanged(object sender, EventArgs e)
		{
			if (listviewBinaries.SelectedItems.Count > 0)
				listviewBinaries.SelectedItems[0].Text = textboxVersion.Text;
		}

		private void textboxBinary_TextChanged(object sender, EventArgs e)
		{
			if (listviewBinaries.SelectedItems.Count > 0)
				listviewBinaries.SelectedItems[0].SubItems[1].Text = textboxBinary.Text;
		}

		private void btnCancel_Click(object sender, EventArgs e)
		{
			Close();
		}

		private void listviewBinaries_SelectedIndexChanged(object sender, EventArgs e)
		{
			if (listviewBinaries.SelectedItems.Count > 0)
			{
				btnDel.Enabled = true;
				btnBrowse.Enabled = true;
				textboxVersion.Text = listviewBinaries.SelectedItems[0].Text;
				textboxBinary.Text = listviewBinaries.SelectedItems[0].SubItems[1].Text;
				textboxVersion.Enabled = true;
				textboxBinary.Enabled = true;
			}
			else
			{
				btnDel.Enabled = false;
				btnBrowse.Enabled = false;
				textboxVersion.Text = "";
				textboxBinary.Text = "";
				textboxVersion.Enabled = false;
				textboxBinary.Enabled = false;
			}
		}

		
	}
}