using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Win32;
using System.Windows.Forms;

namespace SRB2MSLauncher2
{
	public class Options
	{
		public const ushort MS_DEFAULT_PORT = 28900;

		Dictionary<string, string> dicBinaries = new Dictionary<string, string>();

		public void SetBinary(string strVersion, string strBinary)
		{
			dicBinaries[strVersion] = strBinary;
		}

		public string GetBinary(string strVersion)
		{
			return dicBinaries[strVersion];
		}

		string strParams;
		public string Params
		{
			get { return strParams; }
			set { strParams = value; }
		}

		string strMSAddress;
		public string MSAddress
		{
			get { return strMSAddress; }
			set { strMSAddress = value; }
		}

		ushort unMSPort;
		public ushort MSPort
		{
			get { return unMSPort; }
			set { unMSPort = value; }
		}

		bool bShowDefaultWads;
		public bool ShowDefaultWads
		{
			get { return bShowDefaultWads; }
			set { bShowDefaultWads = value; }
		}

		public List<string> VersionStrings
		{
			get { return new List<string>(dicBinaries.Keys); }
		}

		public bool HasBinaryForVersion(string strVersion)
		{
			return dicBinaries.ContainsKey(strVersion) && dicBinaries[strVersion] != "";
		}

		public void LoadFromRegistry()
		{
			RegistryKey rk = Registry.CurrentUser.CreateSubKey(@"Software\SRB2MSLauncher");
			RegistryKey rkBinaries = rk.CreateSubKey("Binaries");

			strParams = (string)rk.GetValue("Params", "");
			strMSAddress = (string)rk.GetValue("Masterserver", "ms.srb2.org");

			try { unMSPort = Convert.ToUInt16(rk.GetValue("MSPort", MS_DEFAULT_PORT)); }
			catch { unMSPort = MS_DEFAULT_PORT; }

			try { bShowDefaultWads = Convert.ToInt32(rk.GetValue("ShowDefaultWads", 0)) != 0; }
			catch { bShowDefaultWads = true; }

			dicBinaries.Clear();
			foreach (string strName in rkBinaries.GetValueNames())
				dicBinaries[strName] = (string)rkBinaries.GetValue(strName);

			rkBinaries.Close();
			rk.Close();
		}

		public void WriteToRegistry()
		{
			RegistryKey rk = Registry.CurrentUser.CreateSubKey(@"Software\SRB2MSLauncher");

			rk.SetValue("Params", strParams);
			rk.SetValue("Masterserver", strMSAddress);
			rk.SetValue("MSPort", unMSPort, RegistryValueKind.DWord);
			rk.SetValue("ShowDefaultWads", Convert.ToInt32(bShowDefaultWads), RegistryValueKind.DWord);

			rk.DeleteSubKey("Binaries", false);
			RegistryKey rkBinaries = rk.CreateSubKey("Binaries");
			foreach (string strName in dicBinaries.Keys)
				rkBinaries.SetValue(strName, dicBinaries[strName]);

			rkBinaries.Close();
			rk.Close();
		}

		public void AddBinariesToListView(ListView lv)
		{
			foreach (string strName in dicBinaries.Keys)
				lv.Items.Add(new ListViewItem(new string[] { strName, dicBinaries[strName] }));
		}

		public void SetBinariesFromListView(ListView lv)
		{
			dicBinaries.Clear();
			foreach (ListViewItem lvi in lv.Items)
				dicBinaries[lvi.Text] = lvi.SubItems[1].Text;
		}
	}
}
