using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace SRB2MSLauncher2
{
	public partial class FormJoinIP : Form
	{
		ConnectDetails cd;
		private const int DEFAULT_PORT = 5029;

		public FormJoinIP(List<string> listVersions, ConnectDetails cd)
		{
			InitializeComponent();
			this.cd = cd;
			cmbVersions.Items.AddRange(listVersions.ToArray());
			cmbVersions.SelectedIndex = 0;
			txtPort.Text = Convert.ToString(DEFAULT_PORT);
		}

		private void btnOK_Click(object sender, EventArgs e)
		{
			cd.strVersion = (string)cmbVersions.SelectedItem;
			cd.strAddress = txtAddress.Text;
			try { cd.unPort = Convert.ToUInt16(txtPort.Text); }
			catch { cd.unPort = DEFAULT_PORT; }

			Close();
		}

		public class ConnectDetails
		{
			public string strVersion;
			public string strAddress;
			public ushort unPort;
		}

		private void btnCancel_Click(object sender, EventArgs e)
		{
			Close();
		}
	}
}