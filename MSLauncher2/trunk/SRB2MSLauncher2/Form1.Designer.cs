namespace SRB2MSLauncher2
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
			this.listViewServers = new System.Windows.Forms.ListView();
			this.colhdrName = new System.Windows.Forms.ColumnHeader("(none)");
			this.colhdrGametype = new System.Windows.Forms.ColumnHeader();
			this.colhdrPing = new System.Windows.Forms.ColumnHeader();
			this.colhdrPlayers = new System.Windows.Forms.ColumnHeader();
			this.colhdrVersion = new System.Windows.Forms.ColumnHeader();
			this.btnOptions = new System.Windows.Forms.Button();
			this.statusStrip1 = new System.Windows.Forms.StatusStrip();
			this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
			this.backgroundWorkerQueryServers = new System.ComponentModel.BackgroundWorker();
			this.imageList1 = new System.Windows.Forms.ImageList(this.components);
			this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
			this.btnConnect = new System.Windows.Forms.Button();
			this.btnJoinIP = new System.Windows.Forms.Button();
			this.btnRefresh = new System.Windows.Forms.Button();
			this.panelButtons = new System.Windows.Forms.Panel();
			this.statusStrip1.SuspendLayout();
			this.panelButtons.SuspendLayout();
			this.SuspendLayout();
			// 
			// listViewServers
			// 
			this.listViewServers.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
						| System.Windows.Forms.AnchorStyles.Left)
						| System.Windows.Forms.AnchorStyles.Right)));
			this.listViewServers.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.colhdrName,
            this.colhdrGametype,
            this.colhdrPing,
            this.colhdrPlayers,
            this.colhdrVersion});
			this.listViewServers.FullRowSelect = true;
			this.listViewServers.HideSelection = false;
			this.listViewServers.Location = new System.Drawing.Point(14, 12);
			this.listViewServers.Name = "listViewServers";
			this.listViewServers.ShowItemToolTips = true;
			this.listViewServers.Size = new System.Drawing.Size(443, 231);
			this.listViewServers.TabIndex = 0;
			this.listViewServers.UseCompatibleStateImageBehavior = false;
			this.listViewServers.View = System.Windows.Forms.View.Details;
			this.listViewServers.ItemActivate += new System.EventHandler(this.listViewServers_ItemActivate);
			this.listViewServers.SelectedIndexChanged += new System.EventHandler(this.listViewServers_SelectedIndexChanged);
			this.listViewServers.ColumnClick += new System.Windows.Forms.ColumnClickEventHandler(this.listViewServers_ColumnClick);
			// 
			// colhdrName
			// 
			this.colhdrName.Tag = "";
			this.colhdrName.Text = "Server Name";
			this.colhdrName.Width = 190;
			// 
			// colhdrGametype
			// 
			this.colhdrGametype.Text = "Gametype";
			// 
			// colhdrPing
			// 
			this.colhdrPing.Text = "Ping (ms)";
			this.colhdrPing.Width = 55;
			// 
			// colhdrPlayers
			// 
			this.colhdrPlayers.Text = "Players";
			this.colhdrPlayers.Width = 48;
			// 
			// colhdrVersion
			// 
			this.colhdrVersion.Text = "Version";
			// 
			// btnOptions
			// 
			this.btnOptions.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.btnOptions.Location = new System.Drawing.Point(0, 0);
			this.btnOptions.Name = "btnOptions";
			this.btnOptions.Size = new System.Drawing.Size(92, 31);
			this.btnOptions.TabIndex = 1;
			this.btnOptions.Text = "&Options...";
			this.btnOptions.UseVisualStyleBackColor = true;
			this.btnOptions.Click += new System.EventHandler(this.btnOptions_Click);
			// 
			// statusStrip1
			// 
			this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1});
			this.statusStrip1.Location = new System.Drawing.Point(0, 284);
			this.statusStrip1.Name = "statusStrip1";
			this.statusStrip1.Size = new System.Drawing.Size(469, 22);
			this.statusStrip1.TabIndex = 3;
			this.statusStrip1.Text = "statusStrip1";
			// 
			// toolStripStatusLabel1
			// 
			this.toolStripStatusLabel1.AutoSize = false;
			this.toolStripStatusLabel1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
			this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
			this.toolStripStatusLabel1.Size = new System.Drawing.Size(454, 17);
			this.toolStripStatusLabel1.Spring = true;
			this.toolStripStatusLabel1.Text = "SRB2 MS Launcher";
			this.toolStripStatusLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// backgroundWorkerQueryServers
			// 
			this.backgroundWorkerQueryServers.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorkerQueryServers_DoWork);
			// 
			// imageList1
			// 
			this.imageList1.ColorDepth = System.Windows.Forms.ColorDepth.Depth8Bit;
			this.imageList1.ImageSize = new System.Drawing.Size(16, 16);
			this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
			// 
			// openFileDialog1
			// 
			this.openFileDialog1.DefaultExt = "exe";
			this.openFileDialog1.Filter = "Executables files|*.exe|All files|*.*";
			// 
			// btnConnect
			// 
			this.btnConnect.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.btnConnect.Enabled = false;
			this.btnConnect.Location = new System.Drawing.Point(351, 0);
			this.btnConnect.Name = "btnConnect";
			this.btnConnect.Size = new System.Drawing.Size(92, 32);
			this.btnConnect.TabIndex = 4;
			this.btnConnect.Text = "&Connect";
			this.btnConnect.UseVisualStyleBackColor = true;
			this.btnConnect.Click += new System.EventHandler(this.btnConnect_Click);
			// 
			// btnJoinIP
			// 
			this.btnJoinIP.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
			this.btnJoinIP.Location = new System.Drawing.Point(234, 0);
			this.btnJoinIP.Name = "btnJoinIP";
			this.btnJoinIP.Size = new System.Drawing.Size(92, 31);
			this.btnJoinIP.TabIndex = 3;
			this.btnJoinIP.Text = "&Join by IP...";
			this.btnJoinIP.UseVisualStyleBackColor = true;
			this.btnJoinIP.Click += new System.EventHandler(this.btnJoinIP_Click);
			// 
			// btnRefresh
			// 
			this.btnRefresh.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
			this.btnRefresh.Location = new System.Drawing.Point(117, 0);
			this.btnRefresh.Name = "btnRefresh";
			this.btnRefresh.Size = new System.Drawing.Size(92, 31);
			this.btnRefresh.TabIndex = 2;
			this.btnRefresh.Text = "&Refresh List";
			this.btnRefresh.UseVisualStyleBackColor = true;
			this.btnRefresh.Click += new System.EventHandler(this.btnRefresh_Click);
			// 
			// panelButtons
			// 
			this.panelButtons.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
						| System.Windows.Forms.AnchorStyles.Right)));
			this.panelButtons.Controls.Add(this.btnConnect);
			this.panelButtons.Controls.Add(this.btnJoinIP);
			this.panelButtons.Controls.Add(this.btnRefresh);
			this.panelButtons.Controls.Add(this.btnOptions);
			this.panelButtons.Location = new System.Drawing.Point(14, 249);
			this.panelButtons.Name = "panelButtons";
			this.panelButtons.Size = new System.Drawing.Size(443, 32);
			this.panelButtons.TabIndex = 8;
			this.panelButtons.Layout += new System.Windows.Forms.LayoutEventHandler(this.panelButtons_Layout);
			// 
			// Form1
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(469, 306);
			this.Controls.Add(this.panelButtons);
			this.Controls.Add(this.statusStrip1);
			this.Controls.Add(this.listViewServers);
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.Name = "Form1";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "SRB2 MS Launcher";
			this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Form1_FormClosed);
			this.statusStrip1.ResumeLayout(false);
			this.statusStrip1.PerformLayout();
			this.panelButtons.ResumeLayout(false);
			this.ResumeLayout(false);
			this.PerformLayout();

        }

        #endregion

		private System.Windows.Forms.ListView listViewServers;
		private System.Windows.Forms.Button btnOptions;
        private System.Windows.Forms.ColumnHeader colhdrGametype;
        private System.Windows.Forms.ColumnHeader colhdrPing;
		private System.Windows.Forms.ColumnHeader colhdrVersion;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
		private System.ComponentModel.BackgroundWorker backgroundWorkerQueryServers;
		private System.Windows.Forms.ColumnHeader colhdrPlayers;
		private System.Windows.Forms.ColumnHeader colhdrName;
		private System.Windows.Forms.ImageList imageList1;
		private System.Windows.Forms.OpenFileDialog openFileDialog1;
		private System.Windows.Forms.Button btnConnect;
		private System.Windows.Forms.Button btnJoinIP;
		private System.Windows.Forms.Button btnRefresh;
		private System.Windows.Forms.Panel panelButtons;
    }
}

