using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace SRB2MSLauncher2
{
	public partial class Form1 : Form
	{
		private ServerQuerier sq;
		private Options options = new Options();

		public Form1()
		{
			InitializeComponent();
			sq = new ServerQuerier();
			ServerInfoListViewAdder silva = new ServerInfoListViewAdder(sq, this);

			// Load options.
			options.LoadFromRegistry();

			try
			{
				sq.SetMasterServer(options.MSAddress, options.MSPort);
			}
			catch(Exception exception)
			{
				toolStripStatusLabel1.Text = "Couldn't resolve MS: " + exception.Message;
			}

			sq.StartListening(silva);

			// Query the MS and the individual servers in another thread.
			backgroundWorkerQueryServers.RunWorkerAsync();
		}

		private void btnRefresh_Click(object sender, EventArgs e)
		{
			if (!backgroundWorkerQueryServers.IsBusy)
			{
				// Clear the server list.
				listViewServers.Items.Clear();

				// Disable the Connect button.
				btnConnect.Enabled = false;

				// Query the MS and the individual servers in another thread.
				backgroundWorkerQueryServers.RunWorkerAsync();
			}
		}

		private void backgroundWorkerQueryServers_DoWork(object sender, DoWorkEventArgs e)
		{
			MSClient msclient = new MSClient();

			statusStrip1.Items[0].Text = "Querying Master Server...";
			
			try
			{
				List<MSServerEntry> listServers = msclient.GetServerList(options.MSAddress, options.MSPort);

				statusStrip1.Items[0].Text = "Master Server reported " + listServers.Count + " servers. Querying individual servers...";

				// Query each of the individual servers asynchronously.
				foreach (MSServerEntry msse in listServers)
				{
					sq.Query(msse.strAddress, msse.unPort);
				}
			}
			catch (System.Net.Sockets.SocketException sockexception)
			{
				statusStrip1.Items[0].Text = "Network error: " + sockexception.Message;
			}
			catch (Exception exception)
			{
				statusStrip1.Items[0].Text = exception.Message;
			}
		}

		private void btnConnect_Click(object sender, EventArgs e)
		{
			if(listViewServers.SelectedItems.Count > 0)
			{
				ConnectToServerFromListItem(listViewServers.SelectedItems[0]);
			}
		}

		private void ConnectToServerFromListItem(ListViewItem lvi)
		{
			ServerQuerier.SRB2ServerInfo srb2si = (ServerQuerier.SRB2ServerInfo)lvi.Tag;

			// Prompt to get a binary if we need one.
			if(!options.HasBinaryForVersion(srb2si.strVersion) &&
				MessageBox.Show("To join this game, you must register an executable file for version " + srb2si.strVersion + ". Would you like to do so?", Text, MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation) == DialogResult.Yes &&
				openFileDialog1.ShowDialog() == DialogResult.OK)
			{
				options.SetBinary(srb2si.strVersion, openFileDialog1.FileName);
			}

			// Go!
			ConnectToServer(srb2si.strAddress, srb2si.unPort, srb2si.strVersion);
		}

		private void ConnectToServer(string strAddress, ushort unPort, string strVersion)
		{
			// Make sure we now have a binary.
			if(options.HasBinaryForVersion(strVersion))
			{
				try
				{
					string strBinary = options.GetBinary(strVersion);
					string strDirectory = System.IO.Path.GetDirectoryName(strBinary);
					if (strDirectory != String.Empty)
						System.IO.Directory.SetCurrentDirectory(strDirectory);
					System.Diagnostics.Process.Start(strBinary, System.String.Format("-connect {0}:{1} {2}", strAddress, unPort, options.Params)).Close();
				}
				catch (Exception exception)
				{
					MessageBox.Show("Unable to start SRB2: " + exception.Message + ".", "SRB2 MS Launcher", MessageBoxButtons.OK, MessageBoxIcon.Error);
				}
			}
		}

		private class ServerInfoListViewAdder : ServerQuerier.ServerInfoReceiveHandler
		{
			private delegate ListViewItem AddToListCallback(ListViewItem lvi);
			private Form1 form1;
			private Dictionary<byte, string> dicGametypes = new Dictionary<byte,string>();
			private static Dictionary<ServerQuerier.ServerInfoVer, List<String>> dicDefaultFiles =
				new Dictionary<ServerQuerier.ServerInfoVer, List<String>>();

			static ServerInfoListViewAdder()
			{
				dicDefaultFiles.Add(
					ServerQuerier.ServerInfoVer.SIV_PREME,
					new List<String>(
						new string[] {
							"srb2.srb", "sonic.plr", "tails.plr", "knux.plr",
							"auto.wpn", "bomb.wpn", "home.wpn", "rail.wpn", "infn.wpn",
							"drill.dta", "soar.dta", "music.dta"
						})
					);

				dicDefaultFiles.Add(
					ServerQuerier.ServerInfoVer.SIV_ME,
					new List<String>(
						new string[] {
							"srb2.wad", "sonic.plr", "tails.plr", "knux.plr",
							"rings.wpn", "drill.dta", "soar.dta", "music.dta"
						})
					);
			}
					

			public ServerInfoListViewAdder(ServerQuerier sq, Form1 form1) : base(sq)
			{
				this.form1 = form1;

				// Gametypes.
				dicGametypes.Add(0, "Co-op");
				dicGametypes.Add(1, "Match");
				dicGametypes.Add(2, "Race");
				dicGametypes.Add(3, "Tag");
				dicGametypes.Add(4, "CTF");
				dicGametypes.Add(5, "Chaos");

				// Don't think these are actually used.
				dicGametypes.Add(42, "Team Match");
				dicGametypes.Add(43, "Time-Only Race");
			}

			public override void ProcessServerInfo(ServerQuerier.SRB2ServerInfo srb2si)
			{
				ListView lv = form1.listViewServers;

				// Build a list item.
				ListViewItem lvi = new ListViewItem(srb2si.strName);

				// So we can get address and whatever else we might need.
				lvi.Tag = srb2si;

				// Gametype string, or number if not recognised.
				if(dicGametypes.ContainsKey(srb2si.byGametype))
					lvi.SubItems.Add(dicGametypes[srb2si.byGametype]);
				else
					lvi.SubItems.Add(Convert.ToString(srb2si.byGametype));

				lvi.SubItems.Add(Convert.ToString(srb2si.uiTime));
				lvi.SubItems.Add(srb2si.byPlayers + "/" + srb2si.byMaxplayers);
				lvi.SubItems.Add(srb2si.strVersion);

				// Make the tooltip.
				BuildTooltip(lvi, form1.options.ShowDefaultWads);

				// Is the game full?
				if (srb2si.byPlayers >= srb2si.byMaxplayers)
					lvi.ForeColor = Color.LightGray;
				// Modified?
				else if (srb2si.bModified)
					lvi.ForeColor = Color.CornflowerBlue;

				// Thread-safe goodness.
				if (lv.InvokeRequired)
				{
					// Call ourselves in the context of the form's thread.
					AddToListCallback addtolistcallback = new AddToListCallback(lv.Items.Add);
					lv.Invoke(addtolistcallback, new object[] { lvi });
				}
				else
				{
					// Add it!
					lv.Items.Add(lvi);
				}

				form1.statusStrip1.Items[0].Text = String.Format("{0} server{1} listed.", lv.Items.Count, lv.Items.Count == 1 ? "" : "s");
			}

			public override void HandleException(Exception e)
			{
				form1.statusStrip1.Items[0].Text = e.Message;
			}

			public static void BuildTooltip(ListViewItem lvi, bool bShowDefaultWads)
			{
				string strWads = String.Empty;
				ServerQuerier.SRB2ServerInfo srb2si = (ServerQuerier.SRB2ServerInfo)lvi.Tag;

				foreach (ServerQuerier.AddedWad aw in srb2si.listFiles)
				{
					List<string> listDefaultFiles = dicDefaultFiles[srb2si.siv];

					if (bShowDefaultWads || !listDefaultFiles.Contains(aw.strFilename))
					{
						strWads += String.Format("\n{0} ({1:f1} KB)", aw.strFilename, Convert.ToSingle(aw.uiSize) / 1024);
						if (aw.bImportant)
						{
							if (aw.downloadtype == ServerQuerier.DownloadTypes.DT_TOOBIG)
								strWads += " (too big to download)";
							else if (aw.downloadtype == ServerQuerier.DownloadTypes.DT_DISABLED)
								strWads += " (downloading disabled)";
						}
						else strWads += " (unimportant)";
					}
				}

				lvi.ToolTipText = "Current map: " + srb2si.strMapName + "\n";
				if (strWads != String.Empty)
					lvi.ToolTipText += "Wads added:" + strWads;
				else lvi.ToolTipText += "No wads added";
			}
		}

		private class ListViewSorter : System.Collections.IComparer
		{
			private int iColumn;
			public int Column { get { return iColumn; } }
			public SortOrder so;

			public ListViewSorter(int iColumn)
			{
				this.iColumn = iColumn;
				so = SortOrder.Ascending;
			}

			public int Compare(object x, object y)
			{
				ListViewItem lviX = (ListViewItem)x;
				ListViewItem lviY = (ListViewItem)y;

				return ((so == SortOrder.Ascending) ? 1 : -1) * String.Compare(lviX.SubItems[iColumn].Text, lviY.SubItems[iColumn].Text);
			}

			public void ToggleSortOrder()
			{
				if (so != SortOrder.Ascending)
					so = SortOrder.Ascending;
				else
					so = SortOrder.Descending;
			}
		}

		private void listViewServers_ColumnClick(object sender, ColumnClickEventArgs e)
		{
			if (listViewServers.ListViewItemSorter != null &&
				((ListViewSorter)listViewServers.ListViewItemSorter).Column == e.Column)
			{
				((ListViewSorter)listViewServers.ListViewItemSorter).ToggleSortOrder();
				listViewServers.Sort();
			}
			else
			{
				listViewServers.ListViewItemSorter = new ListViewSorter(e.Column);
			}
		}

		private void listViewServers_SelectedIndexChanged(object sender, EventArgs e)
		{
			btnConnect.Enabled = (listViewServers.SelectedItems.Count > 0);
		}

		private void btnOptions_Click(object sender, EventArgs e)
		{
			new FormOptions(options).ShowDialog();

			try
			{
				sq.SetMasterServer(options.MSAddress, options.MSPort);
			}
			catch (Exception exception)
			{
				toolStripStatusLabel1.Text = "Couldn't resolve MS: " + exception.Message;
			}

			// Reconstruct tooltips.
			foreach (ListViewItem lvi in listViewServers.Items)
				ServerInfoListViewAdder.BuildTooltip(lvi, options.ShowDefaultWads);
		}

		private void Form1_FormClosed(object sender, FormClosedEventArgs e)
		{
			options.WriteToRegistry();
		}

		private void btnJoinIP_Click(object sender, EventArgs e)
		{
			List<string> listVersions = options.VersionStrings;

			if (listVersions.Count > 0)
			{
				FormJoinIP.ConnectDetails cd = new FormJoinIP.ConnectDetails();
				if (new FormJoinIP(listVersions, cd).ShowDialog() == DialogResult.OK)
					ConnectToServer(cd.strAddress, cd.unPort, cd.strVersion);
			}
			else MessageBox.Show("Before joining a game, you must configure at least one version in the Options dialogue.", "SRB2 MS Launcher", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
		}

		private void listViewServers_ItemActivate(object sender, EventArgs e)
		{
			ConnectToServerFromListItem(listViewServers.SelectedItems[0]);
		}

		private void panelButtons_Layout(object sender, LayoutEventArgs e)
		{
			int iThird = (panelButtons.Width - btnConnect.Width) / 3;
			btnRefresh.SetBounds(iThird, 0, 0, 0, BoundsSpecified.X);
			btnJoinIP.SetBounds(2 * iThird, 0, 0, 0, BoundsSpecified.X);
		}
	}
}
