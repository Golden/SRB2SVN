SRB2 MS Launcher
by Oogaland (oogaland@gmail.com)
http://workbench.srb2.org/launcher/
Version 2.5

Requires .NET Framework 2.0

This was created mostly to give me a quick introduction to .NET programming. I
hope it's useful.

Grey servers are full, blue servers are modified. Hover over a server for the
current map number and a list of wads. You can register an executable for each
version, so for example you can register SRB2JTE.EXE with 1.69.10, and SRB2JTE
games will automatically use that binary.

Future updates will probably be limited to bug-fixes, unless somebody suggests
something spectacularly useful and easy to implement. Do feel free to try,
though. :P We're at version 2.x already because of my old VB6 launcher, in case
you're wondering.

Version 2.5 supports most of the changes made to the protocol by SRB2 2.0. NAT
traversal hasn't been implemented, so certain servers won't show up even though
they are in fact joinable. There were assorted other fixes, too.
