/* THIS FILE WILL BE OVERWRITTEN BY DEV-C++ */
/* DO NOT EDIT ! */

#ifndef BLITZZENGINE_PRIVATE_H
#define BLITZZENGINE_PRIVATE_H

/* VERSION DEFINITIONS */
#define VER_STRING	"0.0.5.20"
#define VER_MAJOR	0
#define VER_MINOR	0
#define VER_RELEASE	5
#define VER_BUILD	20
#define COMPANY_NAME	"Cirrus Creative"
#define FILE_VERSION	""
#define FILE_DESCRIPTION	"BlitzzEngine"
#define INTERNAL_NAME	"blitzwin.exe"
#define LEGAL_COPYRIGHT	"(C) 2008 Cirrus Creative"
#define LEGAL_TRADEMARKS	""
#define ORIGINAL_FILENAME	"blitzwin.exe"
#define PRODUCT_NAME	"BlitzzEngine"
#define PRODUCT_VERSION	""

#endif /*BLITZZENGINE_PRIVATE_H*/
