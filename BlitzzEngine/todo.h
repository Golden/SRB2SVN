/* TODO LIST
List of things to get done at.. some point

  OBJECTS
- Linked lists for objects
- If we wanna get really finnicky about performance, a seperate structure and loop for particles
- Get some kind of bouyant force working on non-player objects
- Allow an object to "attach" to another object for more complex objects

  COLLISION
- Completely revamp slope collision by merging portions of the collision code and uniting with object entities

  FUNCTIONS
- Empty

  INTERFACE // Includes everything from HUD, to menus, to controls
- HUD
- Attack button

  SOUND
- Footsteps? I personally think they add a nice touch and really make a 2D game feel more modern :)

  GRAPHICS // Tiles and graphics probably don't go here, if this TODO list is just for the engine, and not a game.
- Some kind of state system for sprite animation

  RESOURCES
- Make levels external
- Create a file format for storing actor properties and states
- Preload only necessary actors, and any other actors that they depend on

  COMPLETED TODO OBJECTS
- AllegroOgg KILL IT KILL IT
- Fix moving down and jumping on slopes
- Create a function for pushing an object in a specific direction, so we don't need to fight directly with momx/momy
- Navigatable menu system with selectable entries corresponding to different menus/options
- Change sound playback to use Allegro's low-level voice functions instead of using the easy way out
- Add support for logg (It's still possible to find on the internet)
- Create a target variable(member? forgot the terminology) in the object struct that targets other objects

Lets try to remember to keep this list as up to date as possible.
Add new things to it when you think of them.
Move completed TODOs to the COMPLETED TODO OBJECTS list.
If there's something that's been added or moved in the TODO list, mention it in the SVN log update.
/*
