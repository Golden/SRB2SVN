/* THIS FILE WILL BE OVERWRITTEN BY DEV-C++ */
/* DO NOT EDIT ! */

#ifndef MONKEYWRENCH_PRIVATE_H
#define MONKEYWRENCH_PRIVATE_H

/* VERSION DEFINITIONS */
#define VER_STRING	"0.0.6.24"
#define VER_MAJOR	0
#define VER_MINOR	0
#define VER_RELEASE	6
#define VER_BUILD	24
#define COMPANY_NAME	"Cirrus Creative"
#define FILE_VERSION	""
#define FILE_DESCRIPTION	"Monkeywrench"
#define INTERNAL_NAME	"beditwin.exe"
#define LEGAL_COPYRIGHT	"(C) 2008 Cirrus Creative"
#define LEGAL_TRADEMARKS	""
#define ORIGINAL_FILENAME	"beditwin.exe"
#define PRODUCT_NAME	"Monkeywrench"
#define PRODUCT_VERSION	""

#endif /*MONKEYWRENCH_PRIVATE_H*/
