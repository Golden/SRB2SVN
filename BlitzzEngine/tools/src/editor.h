#include <allegro.h>

// Zone tools
#define ZT_TILES 1
#define ZT_OBJPLACE 2
#define ZT_OBJMOVE 3

// Mouse states
#define CM_CURSOR 1
#define CM_RECTANGLE 2
#define CM_DEADCLICK 3
#define CM_STARTMOVE 4
#define CM_MOVE 5

// Global selection
#define SEL_SOMETHING 1
#define SEL_HOVER 2

// Individual object selected state
#define OBJ_SEL_HOVERED 1
#define OBJ_SEL_CLICKED 2

// Scrolling modes
#define SCR_ABSOLUTE 1
#define SCR_RELATIVE 2

extern int edit_curtile;
extern int edit_tilescrollx;

extern int edit_selectx1;
extern int edit_selecty1;
extern int edit_selectx2;
extern int edit_selecty2;

extern char edit_keypressed;
extern int edit_mousemovex;
extern int edit_mousemovey;
extern int edit_oldmousex;
extern int edit_oldmousey;
extern char edit_mouselocked;
extern char edit_drawingtile;

extern char edit_clickmode;
extern char edit_scrollcontrol;
extern int edit_gridsize;

extern int edit_zonetool;

extern char edit_selection;
extern int edit_selectcount;

extern MENU menu_file[];
extern MENU menu_edit[];
extern MENU menu_tools[];
extern MENU menu_goto[];
extern MENU menu_help[];
extern MENU menu_contextzone[];
extern MENU menu_contextobj[];
extern DIALOG gui_aboutbox[];
extern DIALOG gui_zoneedit[];

void edit_doaction(void);

int zone_think(int msg, DIALOG *d, int c);
int edit_showabout(int msg, DIALOG *d, int c);

void edit_keycallback(int scancode);
int edit_zonenew(void);
int edit_zoneload(void);
int edit_zonesave(void);
void edit_lockmouse(void);
