// Monkeywrench editor functions
// By FoxBlitzz
// Contains most of the functions used for the editor, such as selections and
// user interface interaction

#include <string.h>
#include <allegro.h>

#include "editor.h"
#include "object.h"
#include "coregfx.h"
#include "gamesys.h"
#include "coresound.h"
#include "../Monkeywrench_private.h"

#include "resources.h"
#include "r_editor.h"

#include "editor.h"

#define NA_ABOUT 1

#define FLAG_TILES 4
#define FLAG_OBJPLACE 8
#define FLAG_OBJMOVE 16

int edit_curtile = 3;
int edit_tilescrollx;

int edit_selectx1;
int edit_selecty1;
int edit_selectx2;
int edit_selecty2;

char edit_keypressed;
int edit_mousemovex;
int edit_mousemovey;
int edit_oldmousex;
int edit_oldmousey;
char edit_mouselocked;
char edit_drawingtile;

static int edit_nextaction;
char edit_clickmode;
char edit_scrollcontrol;

int edit_zonetool;

char edit_selection;
int edit_selectcount;

int edit_gridsize=32;

void edit_keycallback(int scancode)
{
	(void)scancode; //ununsed
	edit_keypressed = 1;
}

END_OF_FUNCTION(edit_keycallback)

// Toggles a menu item. If the item has a datapointer to another item, it is checked as well.
static int gui_menuchk(void)
{
	MENU *seconditem;

	active_menu->flags ^= D_SELECTED;

	if (active_menu->dp)
	{
		seconditem = active_menu->dp;
		seconditem->flags ^= D_SELECTED;
	}

	return D_O_K;
}

static int gui_menupicktool(void)
{
	MENU *seconditem;

	active_menu->flags |= D_SELECTED;

	if (active_menu->dp)
	{
		seconditem = active_menu->dp;
		seconditem->flags |= D_SELECTED;
	}

	if (active_menu->flags & FLAG_TILES)
	{
		edit_zonetool = ZT_TILES;
		set_window_title(GAMENAME " - Tiles");
		menu_tools[1].flags &= ~D_SELECTED; // Messy but gets the job done.
		menu_tools[2].flags &= ~D_SELECTED;
		menu_contextzone[13].flags &= ~D_SELECTED;
		menu_contextzone[14].flags &= ~D_SELECTED;
	}
	else if (active_menu->flags & FLAG_OBJPLACE)
	{
		edit_zonetool = ZT_OBJPLACE;
		set_window_title(GAMENAME " - Object Placement");
		menu_tools[0].flags &= ~D_SELECTED;
		menu_tools[2].flags &= ~D_SELECTED;
		menu_contextzone[12].flags &= ~D_SELECTED;
		menu_contextzone[14].flags &= ~D_SELECTED;
	}
	else if (active_menu->flags & FLAG_OBJMOVE)
	{
		edit_zonetool = ZT_OBJMOVE;
		set_window_title(GAMENAME " - Object Adjustment");
		menu_tools[0].flags &= ~D_SELECTED;
		menu_tools[1].flags &= ~D_SELECTED;
		menu_contextzone[12].flags &= ~D_SELECTED;
		menu_contextzone[13].flags &= ~D_SELECTED;
	}

	return D_O_K;
}

static int gui_menuabout(void)
{
	edit_nextaction = NA_ABOUT;
	return D_O_K;
}

static int gui_menuquit(void)
{
	simulate_keypress(27 + (KEY_ESC << 8));
	return D_O_K;
}

char *gui_credittext[] =
{
	GAMENAMELONG,
	"Developed by:",
	"Dan Hagerstrand, Alam Arias, Shuffle",
	"With special thanks to:",
	"Shawn Hargreaves and his contributors",
	"Whom Allegro wouldn't be possible without!",
	"Welcome to Allegro!",
	"---",
	"Monkeywrench is an all-in-one editor",
	"Used to help with modifications or new games.",
	"It has been designed to be as intuitive as possible.",
	"For questions and feedback, send an e-mail to...",
	"danhagerstrand [at sign] gmail [dot] com",
	"---",
	"The biggest function of the editor is for making zones.",
	"Zones are the maps where the action takes place.",
	"Every part of the game, even the title, uses a zone.",
	"Worlds are the tilesets used in zones.",
	"Each tile has its own collision box and clipping area.",
	"Using clipping will improve layered rendering speed.",
	"Actors are files containing sprite and state data.",
	"They form animations as well as action sequencing.",
	"Object properties can be adjusted as well.",
	"You cannot define the behaviors themselves.",
	"You can, however, choose which behaviors to use.",
	"Good luck!",
	"---",
};


MENU menu_file[] =
{
	{ "&New           ", edit_zonenew,  NULL, 0,          NULL },
	{ "&Load...       ", edit_zoneload, NULL, 0,          NULL },
	{ "&Save          ", NULL,          NULL, D_DISABLED, NULL },
	{ "Save &As...    ", edit_zonesave, NULL, 0,          NULL },
	{ "",                NULL,          NULL, 0,          NULL },
	{ "&Quit          ", gui_menuquit,  NULL, 0,          NULL },
	{ NULL,              NULL,          NULL, 0,          NULL }
};

MENU menu_edit[] =
{
	{ "C&ut",          NULL,        NULL, D_DISABLED, NULL                  },
	{ "&Copy",         NULL,        NULL, D_DISABLED, NULL                  },
	{ "&Paste",        NULL,        NULL, D_DISABLED, NULL                  },
	{ "&Delete",       NULL,        NULL, D_DISABLED, NULL                  },
	{ "",              NULL,        NULL, 0,          NULL                  },
	{ "Display &Grid", gui_menuchk, NULL, 0,          &menu_contextzone[7]  },
	{ "&Snap-to Grid", gui_menuchk, NULL, D_SELECTED, &menu_contextzone[8]  },
	{ "&Adjust Grid",  NULL,        NULL, D_DISABLED, &menu_contextzone[9]  },
	{ "S&how Guides",  gui_menuchk, NULL, 0,          &menu_contextzone[10] },
	{ NULL,            NULL,        NULL, 0,          NULL                  }
};

MENU menu_tools[] =
{
	{ "&Tiles",              gui_menupicktool, NULL, FLAG_TILES,    &menu_contextzone[12] },
	{ "Object &Placement",   gui_menupicktool, NULL, FLAG_OBJPLACE, &menu_contextzone[13] },
	{ "Object &Adjustment",  gui_menupicktool, NULL, FLAG_OBJMOVE,  &menu_contextzone[14] },
	{ "",                    NULL,             NULL, 0,             NULL                  },
	{ "&Zone Properties",    NULL,             NULL, D_DISABLED,    NULL                  },
	{ "&Editor Preferences", NULL,             NULL, D_DISABLED,    NULL                  },
	{ NULL,                  NULL,             NULL, 0,             NULL                  }
};

MENU menu_goto[] =
{
	{ "&World Editor",  NULL, NULL, D_DISABLED, NULL },
	{ "&Actor Editor",  NULL, NULL, D_DISABLED, NULL },
	{ "&Object Editor", NULL, NULL, D_DISABLED, NULL },
	{ NULL,             NULL, NULL, 0,          NULL }
};

MENU menu_help[] =
{
	{ "&Controls",        NULL,          NULL, D_DISABLED, NULL },
	{ "&Getting Started", NULL,          NULL, D_DISABLED, NULL },
	{ "",                 NULL,          NULL, 0,          NULL },
	{ "&About",           gui_menuabout, NULL, 0,          NULL },
	{ NULL,               NULL,          NULL, 0,          NULL }
};

MENU menu_topzone[] =
{
	{ "&File    ", NULL, menu_file,  0, NULL },
	{ "&Edit    ", NULL, menu_edit,  0, NULL },
	{ "&Tools   ", NULL, menu_tools, 0, NULL },
	{ "&Goto    ", NULL, menu_goto,  0, NULL },
	{ "&Help    ", NULL, menu_help,  0, NULL },
	{ NULL,        NULL, NULL,       0, NULL }
};

MENU menu_contextzone[] =
{
	{ "C&ut",                NULL,             NULL, D_DISABLED,    NULL },
	{ "&Copy",               NULL,             NULL, D_DISABLED,    NULL },
	{ "&Paste",              NULL,             NULL, D_DISABLED,    NULL },
	{ "&Delete",             NULL,             NULL, D_DISABLED,    NULL },
	{ "",                    NULL,             NULL, 0,             NULL },
	{ "&Object Properties",  NULL,             NULL, D_DISABLED,    NULL },
	{ "",                    NULL,             NULL, 0,             NULL },
	{ "Display &Grid",       gui_menuchk,      NULL, 0,             &menu_edit[5] },
	{ "&Snap-to Grid",       gui_menuchk,      NULL, D_SELECTED,    &menu_edit[6] },
	{ "&Adjust Grid",        NULL,             NULL, D_DISABLED,    &menu_edit[7] },
	{ "S&how Guides",        gui_menuchk,      NULL, 0,             &menu_edit[8] },
	{ "",                    NULL,             NULL, 0,             NULL },
	{ "&Tiles",              gui_menupicktool, NULL, FLAG_TILES,    &menu_tools[0] },
	{ "Object &Placement",   gui_menupicktool, NULL, FLAG_OBJPLACE, &menu_tools[1] },
	{ "Object &Adjustment",  gui_menupicktool, NULL, FLAG_OBJMOVE,  &menu_tools[2] },
	{ "",                    NULL,             NULL, 0,             NULL },
	{ "&Zone Properties",    NULL,             NULL, D_DISABLED,    NULL },
	{ "&Editor Preferences", NULL,             NULL, D_DISABLED,    NULL },
	{ NULL,                  NULL,             NULL, 0,             NULL }
};

MENU menu_contextobj[] =
{
	{ "C&ut",               NULL, NULL, D_DISABLED, NULL },
	{ "&Copy",              NULL, NULL, D_DISABLED, NULL },
	{ "&Paste",             NULL, NULL, D_DISABLED, NULL },
	{ "&Delete",            NULL, NULL, D_DISABLED, NULL },
	{ "",                   NULL, NULL, 0,          NULL },
	{ "&Object Properties", NULL, NULL, D_DISABLED, NULL },
	{ NULL,                 NULL, NULL, 0,          NULL }
};

DIALOG gui_aboutbox[] =
{
	// func              x  y   w    h    fg bg key flag d1 d2 dp    dp2   dp3
	{ edit_showabout,    5, 5,  450, 250, 0, 0, 0,  0,   0, 0, NULL, NULL, NULL },
	{ d_yield_proc,      0, 0,  0,   0,   0, 0, 0,  0,   0, 0, NULL, NULL, NULL },
	{ NULL,              0, 0,  0,   0,   0, 0, 0,  0,   0, 0, NULL, NULL, NULL }
};

DIALOG gui_zoneedit[] =
{
	// func         x  y  w    h    fg bg key flag d1 d2 dp    dp2   dp3
	{ zone_think,   0, 0, 640, 480, 0, 0, 0,  0,   0, 0, NULL,         NULL, NULL },
	{ d_menu_proc,  0, 0, 0,   16,  0, 0, 0,  0,   0, 0, menu_topzone, NULL, NULL },
	{ d_yield_proc, 0, 0, 0,   0,   0, 0, 0,  0,   0, 0, NULL,         NULL, NULL },
	{ NULL,         0, 0, 0,   0,   0, 0, 0,  0,   0, 0, NULL,         NULL, NULL }
};

void edit_doaction(void)
{
	switch (edit_nextaction)
	{
		case NA_ABOUT:
			do_dialog(gui_aboutbox,-1);
			break;
	}

	edit_nextaction = 0;
}

int edit_showabout(int msg, DIALOG *d, int c)
{
	int returner = D_O_K;

	char *letter = NULL;
	char oneletter[2];
	int length;
	int letterlen;
	int i = 0;
	int pos = 0;
	int fader = 0;

	(void)c; //unused

	BITMAP *creditbuffer;

	switch (msg)
	{
		case MSG_START:
			game_render();
			blit(d->dp, screen, 0, 0, d->x, d->y, d->w, d->h);
			textprintf_right_ex(screen, font, d->x + d->w - 2, d->y + 2, makecol(255, 255, 255), makecol(0, 0, 0), "v. %s", VER_STRING);

			d->d2 = 0;

			if (d->d1)
			{
				rectfill(screen, d->x, d->y + d->h, d->x + d->w-1, d->y + d->h + 50, makecol(0,0,0));
				creditbuffer = create_bitmap(d->w, 70);
				d->dp2 = creditbuffer;
			}
			break;

		case MSG_IDLE:

			poll_mouse();

			if (mouse_b > 0)
				returner = D_EXIT;

			else if (d->d1 && game_tic)
			{
				game_tic = 0;
				returner = D_REDRAWME;
			}
			break;

		case MSG_DRAW:
			if (!d->d1)
				break;

			clear_to_color(d->dp2, makecol(0,0,0));
			d->d1++;

			if (d->d1 > 190)
			{
				d->d1 = 1;
				d->d2++;
				break;
			}

			if (gui_credittext[d->d2])
			{
				length = text_length(font, gui_credittext[d->d2]);

				letter = gui_credittext[d->d2];

				while (*letter)
				{

					oneletter[0] = gui_credittext[d->d2][i];
					oneletter[1] = '\0';

					if (d->d1-i < 32)
						fader = 128-((d->d1-i)*4);
					else if (d->d1-i > 108)
						fader = ((d->d1-i)-109)*4;

					//fader = 128-fixsin((d->d1-(i))*UNIT*(128/(150.0)))*127/UNIT;

					if (fader < 0)
						fader = 0;
					else if (fader > 127)
						fader = 127;

					letterlen = text_length(font, oneletter);
					textout_ex(d->dp2, font, oneletter, (d->w/2) - (length/2) + pos, 30+(fixsin(((i-d->d1)<<16)*2)*(fader/4)>>16), makecol(255-(fader*2), 255-(fader*2), 255-(fader*2)), -1);
					//textprintf_ex(d->dp2, font, (d->w/2) - (length/2) + pos, 40+(i%3)*10, makecol(255-(fader*2), 255, 255), -1, "%i", fader);


					pos+= letterlen;
					i++;
					letter++;
				};

			}
			else
				d->d2 = 0;

			blit(d->dp2, screen, 0, 0, d->x, d->y-20 + d->h, d->w, d->h);

			break;

		case MSG_END:
			object_message(&gui_zoneedit[0], MSG_DRAW, 0);
			edit_clickmode = CM_DEADCLICK;
			d->d1 = 1;

			destroy_bitmap(d->dp2);
			break;

	}

	return returner;
}
