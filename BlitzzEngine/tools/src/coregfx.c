// BlitzzEngine drawing functions (AKA "Allegro is GOD at 2D Graphics")
// By FoxBlitzz
// Draws all background tiles, sprites, and miscellany onto a buffer image,
// and then sends it to the screen in one of two ways selectable through
// blitzz.ini
//
// Remember, add your new item AFTER acquire_bitmap(buffer); and BEFORE
// release_bitmap(buffer);! Failure to adhere to this rule will result
// in a dumbfounded look on your face as your graphics don't appear on-screen.
// It's so important, I shall repeat it upwards of two times.
//
// Remember, add your new item AFTER acquire_bitmap(buffer); and BEFORE
// release_bitmap(buffer);! Remember, add your new item AFTER
// acquire_bitmap(buffer); and BEFORE release_bitmap(buffer);! Remember, add
// your new item AFTER acquire_bitmap(buffer); and BEFORE
// release_bitmap(buffer);!
//
// Thank you, and have a good day.

#include <allegro.h>
#include <math.h>

#include "gamesys.h"
#include "object.h"
#include "coreproc.h"
#include "coregfx.h"
#include "console.h"
#include "coresound.h"

#include "editor.h"

#include "resources.h"
#include "zone.h"
#include "r_core.h"
#include "r_ui.h"
#include "r_world.h"
#include "r_actors.h"

#define SQRTCIRC (0.70710678118654752440084436210495)

BITMAP *buffer;

COLOR_MAP transtable[GRFX_8BITAPRECISION];

static tile_t* tile;

static int tiley;
static int tilex;
static int liney;
static int linex;
int scrollx=0;
int scrolly=0;
char grfx_noedgescroll=1;

static char lastclicked=0;

char consbuf[300] = "\0";

static void grfx_blit_bg(BITMAP *buffer, BITMAP *bgimage,int offx, int offy, int destx, int desty)
{
	blit(bgimage,buffer,offx,offy,destx,desty,(TILESIZE/2),(TILESIZE/2));
	blit(bgimage,buffer,offx+(TILESIZE/2),offy,destx+(TILESIZE/2),desty,(TILESIZE/2),(TILESIZE/2));
	blit(bgimage,buffer,offx,offy+(TILESIZE/2),destx,desty+(TILESIZE/2),(TILESIZE/2),(TILESIZE/2));
	blit(bgimage,buffer,offx+(TILESIZE/2),offy+(TILESIZE/2),destx+(TILESIZE/2),desty+(TILESIZE/2),(TILESIZE/2),(TILESIZE/2));
	//blit(bgimage,buffer,offx,offy,destx,desty,TILESIZE,TILESIZE);
}

static void grfx_blit_fg(BITMAP *buffer, RLE_SPRITE *fgimage, int destx, int desty, char visibility, layer_t *layer)
{
	int mycolor;

	if (visibility == TILE_DRAWTINTED)
	{
		if (bitmap_color_depth(screen) != 8)
		{
			mycolor = 128;
			set_trans_blender(255, 50, 0, 0);
		}
		else
		{
			mycolor = makecol8(255, 50, 0);
			color_map = &transtable[GRFX_8BITAPRECISION/2];
		}

		draw_lit_rle_sprite(buffer, fgimage, destx, desty, mycolor);
	}
	else if (visibility)
		draw_rle_sprite(buffer, fgimage, destx, desty);

	if (uiflags & UI_DEBUGWORLD && layer)
	{
		linex = tilex+scrollx/TILESIZE-1;
		liney = tiley+scrolly/TILESIZE-1;
		if (linex < 0) {linex = 0;}
		if (linex > layer->sizex-1) {linex = layer->sizex-1;}
		if (liney < 0) {liney = 0;}
		if (liney > layer->sizey-1) {liney = layer->sizey-1;}

		tile = &tilelist[layer->tilerow[linex][liney]];
		rect(buffer,destx-tile->cropleft+tile->colleft,desty-tile->croptop+tile->coltop,destx-tile->cropleft+tile->colright,desty-tile->croptop+tile->colbottom,makecol(255, 0, 0));
		textprintf_centre_ex(buffer, font, destx-tile->cropleft+(TILESIZE/2), desty-tile->croptop+(TILESIZE/2), makecol(255,255,(tile->solid*255)), makecol(0,0,0), "ID: %i", layer->tilerow[linex][liney]);
	}
}

int grfx_camthink(void)
{
	int oldscrollx = scrollx;
	int oldscrolly = scrolly;

	// Mouse scrolling control
	get_mouse_mickeys(&edit_mousemovex, &edit_mousemovey);

	// Absolute scrolling
	if ((key[KEY_TAB] || mouse_b & 8) && !(mouse_b & 1 && (edit_clickmode == CM_DEADCLICK || !edit_clickmode)) && mouse_x >= 0 )
	{
		edit_lockmouse();

		scrollx+=edit_mousemovex;
		scrolly+=edit_mousemovey;

		edit_scrollcontrol = SCR_ABSOLUTE;
	}

	// Continuous momentum scrolling
	else if ((key[KEY_TAB] || mouse_b & 8) && mouse_x >= 0)
	{
		if (edit_scrollcontrol != 0)
		{
			scrollx+=(mouse_x-edit_oldmousex)/2;
			scrolly+=(mouse_y-edit_oldmousey)/2;
		}
		else
		{
			edit_oldmousex=mouse_x;
			edit_oldmousey=mouse_y;
		}
		edit_scrollcontrol = SCR_RELATIVE;
	}
	else if (mouse_x >= 0)
	{
		if (key[KEY_LEFT])
			scrollx-=8;
		if (key[KEY_RIGHT])
			scrollx+=8;
		if (key[KEY_UP])
			scrolly-=8;
		if (key[KEY_DOWN])
			scrolly+=8;

		edit_scrollcontrol = 0;
	}

	//Point-of-no-scroll

	if (grfx_noedgescroll)
	{
		if (scrollx<0) { scrollx=0; }
		if (scrollx>(curlayer->sizex*TILESIZE)-(GAMEWIDTH)) { scrollx=(curlayer->sizex*TILESIZE)-(GAMEWIDTH); }
		if (scrolly<0) { scrolly=0; }
		if (scrolly>(curlayer->sizey*TILESIZE)-(GAMEHEIGHT)) { scrolly=(curlayer->sizey*TILESIZE)-(GAMEHEIGHT); }
	}

	if (oldscrollx != scrollx || oldscrolly != scrolly || edit_mousemovex || edit_mousemovey || edit_keypressed || (edit_clickmode && edit_clickmode != CM_DEADCLICK) || lastclicked != mouse_b || mouse_z)
	{
		lastclicked = mouse_b;
		return 1;
	}
	else
		return 0;
}

void grfx_renderworld(BITMAP *buffer, int scrollx, int scrolly)
{
	int linerow;
	int grid;
	char visibility;
	layer_t *layer;
	int **map;

	float waterdepth;

	// Clip for faster drawing of a single tile
	if (edit_drawingtile)
		set_clip_rect(buffer, edit_selectx2/TILESIZE*TILESIZE-scrollx, edit_selecty2/TILESIZE*TILESIZE-scrolly, edit_selectx2/TILESIZE*TILESIZE-scrollx+TILESIZE, edit_selecty2/TILESIZE*TILESIZE-scrolly+TILESIZE);

	// Set the layer
	layer = (void *)curzone.firstlayer;

	//Draw a tiled, far background
	for (tiley=0; tiley<(buffer->h/TILESIZE)+3; tiley++)
	{
		for (tilex=0; tilex<(buffer->w/TILESIZE)+3; tilex++)
		{
			grfx_blit_bg(buffer,r_world[R_WRL_BACK1].dat,0,0,tilex*TILESIZE-TILESIZE-((scrollx)/4)%TILESIZE,tiley*TILESIZE-TILESIZE-((scrolly)/4)%TILESIZE);
		}
	}

	//Draw foreground tiles
	for (tiley=0; tiley<(buffer->h/TILESIZE)+3; tiley++)
	{
		for (tilex=0; tilex<(buffer->w/TILESIZE)+3; tilex++)
		{

			linex = tilex+scrollx/TILESIZE-1;
			liney = tiley+scrolly/TILESIZE-1;
			if (linex < 0) {linex = 0;}
			if (linex > layer->sizex-1) {linex = layer->sizex-1;}
			if (liney < 0) {liney = 0;}
			if (liney > layer->sizey-1) {liney = layer->sizey-1;}

			map = layer->tilerow;

			if (map[linex][liney])
			{
				tile = &tilelist[curlayer->tilerow[linex][liney]];
				if (edit_zonetool != ZT_TILES || edit_drawingtile || linex != edit_selectx2/TILESIZE || liney != edit_selecty2/TILESIZE)
					visibility = tile->visibility;
				else
					visibility = TILE_DRAWTINTED;

				grfx_blit_fg(buffer,r_world[map[linex][liney]].dat,tilex*TILESIZE-TILESIZE-(scrollx)%TILESIZE,tiley*TILESIZE-TILESIZE-(scrolly)%TILESIZE,visibility,NULL);
			}
		}
	}

	// Draw the sprites
	grfx_updatesprites(buffer, scrollx, scrolly);

	color_map = &transtable[GRFX_8BITAPRECISION*3/4];
	set_trans_blender(180, 180, 180, 180);

	// Draw current tile
	if (edit_zonetool == ZT_TILES && !edit_clickmode && r_world[edit_curtile].dat && tilelist[edit_curtile].visibility != TILE_NODRAW)
		draw_trans_rle_sprite(buffer, r_world[edit_curtile].dat, edit_selectx2/TILESIZE*TILESIZE-scrollx, edit_selecty2/TILESIZE*TILESIZE-scrolly);

	drawing_mode(DRAW_MODE_XOR,buffer,0,0);

	if (menu_edit[5].flags & D_SELECTED) // Show Grid mode
	{
		if (edit_zonetool == ZT_TILES)
			grid = TILESIZE;
		else
			grid = edit_gridsize;

		for (tiley=(-scrolly)%grid; tiley<GAMEHEIGHT; tiley+= grid)
		{
			for (tilex=(-scrollx)%grid; tilex<GAMEWIDTH; tilex+= grid)
			{
				line(buffer, tilex-3, tiley, tilex+3, tiley, makecol(255,255,255));
				line(buffer, tilex, tiley-3, tilex, tiley+3, makecol(255,255,255));
			}
		}
	}

	drawing_mode(DRAW_MODE_SOLID,buffer,0,0);

	// Simple water reflection
	if (curzone.watery>-1 && curzone.watery-scrolly<=buffer->h)
	{

		// Water distortion
		for (linerow=curzone.watery-scrolly; linerow<=buffer->h-1; linerow+=1)
		{
			if (curzone.watery>0)
				waterdepth=(scrolly-curzone.watery+(buffer->h/2.0))/(buffer->h/2.0);
			else
				waterdepth=3.0;

			linex=sin(linerow/32.0)*(waterdepth+3.0); // Coordinates for X offset of current row
			liney=linerow+((sin(linerow/16.0)+1))*(waterdepth+1.0); // Coordinates for Y offset of row

			// Fix the off-screen artifacts by adding... a different artifact!
			// In this case, it repeats the last row/pixel with graphic information.
			// Otherwise you can see through the other side to all the strings.
			if (liney<buffer->h)
				blit(buffer,buffer,linex,liney,0,linerow,buffer->w,1);
			else
				blit(buffer,buffer,linex,buffer->h-1,0,linerow,buffer->w,1);
			if (linex<0)
				hline(buffer,0,linerow,-linex,getpixel(buffer,-linex,linerow));
			if (linex>0)
				hline(buffer,buffer->w-1,linerow,buffer->w-linex-1,getpixel(buffer,buffer->w-linex-1,linerow));
		}

		// Water coloring is disabled for now. When I make all the graphics
		// paletted, I'll do it, but doing a giant filled rectangle with
		// true-color semi-transparency all the way across the screen isn't
		// healthy on the framerate.

		//color_map = &transtable[GRFX_8BITAPRECISION/4];

		//drawing_mode(DRAW_MODE_TRANS,buffer,0,0);
		//rectfill(buffer,0,(watery/UNIT)-(WORLDY),639,479,makecol(255,20,0));
		//drawing_mode(DRAW_MODE_SOLID,buffer,0,0);

		if (curzone.watery>0)
		{
			for (tiley=0; tiley<128; tiley+=(tiley/32.0)+2)
				blit(buffer,buffer,sin((tiley/8.0)/32.0)*4,(curzone.watery-scrolly)-tiley,0,(curzone.watery-scrolly)+tiley,GAMEWIDTH,1);
		}
	}

	set_clip_rect(buffer, 0, 0, GAMEWIDTH-1, GAMEHEIGHT-1);
}

static void grfx_renderdebug(void)
{
	rectfill(buffer, 0, 0, GAMEWIDTH-1, 10, makecol(0,0,0));

	if (mouse_x >= 0)
		textprintf_ex(buffer, font, 0, 0, makecol(255, 255, 255), -1, "Mouse: %i, %i   On Tile: %i, %i", mouse_x+scrollx, mouse_y+scrolly, (mouse_x+scrollx)/TILESIZE, (mouse_y+scrolly)/TILESIZE);
	textprintf_ex(buffer, font, 0, 16, makecol(255, 255, 255), makecol(0, 0, 0), "Objects: %i", obj_count());
	textprintf_ex(buffer, font, 0, 32, makecol(255, 255, 255), makecol(0, 0, 0), "Debug: %i", edit_clickmode);
	if (edit_selection & SEL_SOMETHING)
		textprintf_right_ex(buffer, font, GAMEWIDTH-1, 0, makecol(255, 255, 255), -1, "Selection Active. Count: %i", edit_selectcount);
}

void game_render(void)
{
	int placex;
	//int placey;

	if (!(gfx_capabilities & GFX_HW_CURSOR))
		show_mouse(NULL);

	//Acquiring bitmaps for drawing provides a small performance boost
	acquire_bitmap(buffer);
	//ADD ALL GRAPHICS FUNCTION CALLS AFTER THE ABOVE LINE


    if (edit_zonetool != ZT_TILES || edit_clickmode != CM_RECTANGLE) // Not doing tile selection, render normally
	{
		grfx_renderworld(buffer,scrollx,scrolly);

		color_map = &transtable[GRFX_8BITAPRECISION/2];
		drawing_mode(DRAW_MODE_TRANS,buffer,0,0);
		set_trans_blender(128, 128, 128, 128);

		if (edit_scrollcontrol == SCR_RELATIVE && mouse_x >= 0)
			fastline(buffer, mouse_x, mouse_y, edit_oldmousex, edit_oldmousey, makecol(255,50,0));

		if (edit_zonetool == ZT_OBJMOVE && edit_clickmode == CM_RECTANGLE)
			rectfill(buffer, edit_selectx1-scrollx, edit_selecty1-scrolly, edit_selectx2-scrollx, edit_selecty2-scrolly, makecol(50,150,200));

		drawing_mode(DRAW_MODE_XOR,buffer,0,0);

		if (menu_edit[8].flags & D_SELECTED) // Show Guides mode
		{
			rect(buffer, mouse_x-320, mouse_y-240, mouse_x+319, mouse_y+239, makecol(255,255,255));
			line(buffer, 0, mouse_y, GAMEWIDTH-1, mouse_y, makecol(255,255,255));
			line(buffer, mouse_x, 0, mouse_x, GAMEHEIGHT-1, makecol(255,255,255));
		}

		drawing_mode(DRAW_MODE_SOLID,buffer,0,0);
	}
	else // Tile list display
	{
		rectfill(buffer, 0, (GAMEHEIGHT/2)-4, GAMEWIDTH-1, (GAMEHEIGHT/2)+TILESIZE+4, makecol(0,0,0));

		for (tilex=0; tilex<(buffer->w/TILESIZE)+3; tilex++)
		{
			linex = tilex+edit_tilescrollx/TILESIZE-1;
			if (linex < 0) {linex = 0;}
			if (linex > MAXTILES) {linex = MAXTILES;}

			placex = tilex*TILESIZE-TILESIZE-(edit_tilescrollx)%TILESIZE;

			if (r_world[linex].dat && tilelist[linex].visibility != TILE_NODRAW)
			{
				if (linex == edit_curtile)
				{
					draw_rle_sprite(buffer, r_world[linex].dat, placex, (GAMEHEIGHT/2));
					rect(buffer, placex, (GAMEHEIGHT/2), placex+TILESIZE-1, (GAMEHEIGHT/2)+TILESIZE-1, makecol(255,50,0));
				}
				else
				{

					if (bitmap_color_depth(screen) != 8)
					{
						set_trans_blender(0, 0, 0, 64);
						draw_lit_rle_sprite(buffer, r_world[linex].dat, placex, (GAMEHEIGHT/2), 64);
					}
					else
					{
						color_map = &transtable[GRFX_8BITAPRECISION/4];
						draw_lit_rle_sprite(buffer, r_world[linex].dat, placex, (GAMEHEIGHT/2), makecol8(0,0,0));
					}
				}
			}
		}
	}

	grfx_renderdebug();

	if (!(gfx_capabilities & GFX_HW_CURSOR))
		show_mouse(buffer);

	//Release the bitmap after drawing or else the game will freeze!
	release_bitmap(buffer);
	// DO NOT ADD ANY DRAWING CODE AFTER THE ABOVE LINE. DOING SO WILL THROW OFF
	// TIMING OR PREVENT YOUR GRAPHICS FROM BEING DRAWN.

	grfx_refresh();
}

void grfx_updatesprites(BITMAP *buffer, int scrollx, int scrolly)
{
	int i;

	drawing_mode(DRAW_MODE_SOLID,buffer,0,0);

	for (i=0;i<MAXOBJS;i++)
	{
		if (objectlist[i].alive < AO_ACTIVE)
			continue;

		switch (objectlist[i].drawstyle)
		{
			case DS_LINE:
			{
				fastline(buffer,(objectlist[i].x-(objectlist[i].width/2)-scrollx),(objectlist[i].y-(objectlist[i].height/2)+objectlist[i].heightoffset-scrolly),(objectlist[i].x+(objectlist[i].width/2)-scrollx),(objectlist[i].y+(objectlist[i].height/2)+objectlist[i].heightoffset-scrolly),makecol(objectlist[i].colorr, objectlist[i].colorg, objectlist[i].colorb));
				break;
			}

			case DS_SPRITE:
			{
				BITMAP *myactor;
				myactor = (BITMAP *)r_actor[objectlist[i].sprite].dat;

				switch (objectlist[i].facedir)
				{
					case FACELEFT: // Object faces left
					{
						draw_sprite_h_flip(buffer,myactor,(objectlist[i].x-scrollx)-(myactor->w/2),(objectlist[i].y-scrolly)-(myactor->h/2));
						break;
					}
					case FACERIGHT: // Object faces right
					{
						draw_sprite(buffer,myactor,(objectlist[i].x-scrollx)-(myactor->w/2),(objectlist[i].y-scrolly)-(myactor->h/2));
						break;
					}
					case FACEVLEFT: // Object faces left, flipped vertically
					{
						draw_sprite_vh_flip(buffer,myactor,(objectlist[i].x-scrollx)-(myactor->w/2),(objectlist[i].y-scrolly)-(myactor->h/2));
						break;
					}
					case FACEVRIGHT: // Object faces right, flipped vertically
					{
						draw_sprite_v_flip(buffer,myactor,(objectlist[i].x-scrollx)-(myactor->w/2),(objectlist[i].y-scrolly)-(myactor->h/2));
						break;
					}
				}
				break;
			}

			case DS_RLE:
			{
				RLE_SPRITE* myRLEActor;
				int mycolor;

				myRLEActor = (RLE_SPRITE *)r_actor[objectlist[i].sprite].dat;

				if (objectlist[i].colora == 255)
					draw_rle_sprite(buffer,myRLEActor,(objectlist[i].x-scrollx)-(myRLEActor->w/2),(objectlist[i].y-scrolly)-(myRLEActor->h/2));
				else
				{

					if (bitmap_color_depth(screen) != 8)
					{
						mycolor = objectlist[i].colora;
						set_trans_blender(objectlist[i].colorr, objectlist[i].colorg, objectlist[i].colorb, 0);
					}
					else
					{
						mycolor = makecol8(objectlist[i].colorr, objectlist[i].colorg, objectlist[i].colorb);
						color_map = &transtable[(255-objectlist[i].colora)*GRFX_8BITAPRECISION/255];
					}

					draw_lit_rle_sprite(buffer,myRLEActor,(objectlist[i].x-scrollx)-(myRLEActor->w/2),(objectlist[i].y-scrolly)-(myRLEActor->h/2),mycolor);
				}
				break;
			}

			case DS_3DQUAD:
			{
				BITMAP *myactor;
				myactor = (BITMAP *)r_actor[objectlist[i].sprite].dat;

				switch (objectlist[i].facedir)
				{
					case FACELEFT: // Object faces left
					{
						draw_sprite_h_flip(buffer,myactor,(objectlist[i].x-scrollx)-(myactor->w/2),(objectlist[i].y-scrolly)-(myactor->h/2));
						break;
					}
					case FACERIGHT: // Object faces right
					{
						draw_sprite(buffer,myactor,(objectlist[i].x-scrollx)-(myactor->w/2),(objectlist[i].y-scrolly)-(myactor->h/2));
						break;
					}
					case FACEVLEFT: // Object faces left, flipped vertically
					{
						draw_sprite_vh_flip(buffer,myactor,(objectlist[i].x-scrollx)-(myactor->w/2),(objectlist[i].y-scrolly)-(myactor->h/2));
						break;
					}
					case FACEVRIGHT: // Object faces right, flipped vertically
					{
						draw_sprite_v_flip(buffer,myactor,(objectlist[i].x-scrollx)-(myactor->w/2),(objectlist[i].y-scrolly)-(myactor->h/2));
						break;
					}
				}
				break;
			}
		}

		if (uiflags & UI_DEBUGWORLD || objectlist[i].selected)
			rect(buffer,(objectlist[i].x-(objectlist[i].width/2)-scrollx),(objectlist[i].y-(objectlist[i].height/2)+objectlist[i].heightoffset-scrolly),((objectlist[i].x+(objectlist[i].width/2)-scrollx))-1,((objectlist[i].y+(objectlist[i].height/2)+objectlist[i].heightoffset-scrolly))-1,makecol(50,50,200));

		if (objectlist[i].selected == 2)
		{
			color_map = &transtable[GRFX_8BITAPRECISION/2];
			drawing_mode(DRAW_MODE_TRANS,buffer,0,0);
			set_trans_blender(128, 128, 128, 128);

			rectfill(buffer,(objectlist[i].x-(objectlist[i].width/2)-scrollx),(objectlist[i].y-(objectlist[i].height/2)+objectlist[i].heightoffset-scrolly),((objectlist[i].x+(objectlist[i].width/2)-scrollx))-1,((objectlist[i].y+(objectlist[i].height/2)+objectlist[i].heightoffset-scrolly))-1,makecol(50,50,200));

			drawing_mode(DRAW_MODE_SOLID,buffer,0,0);
		}
	}
}
