#define MAXTILES 255 // Unsigned char is 0-255. Making MAXTILES 256 will cause an infinite loop unless you change tile_define()

#define RESPOND_NONE 0
#define RESPOND_UP 1
#define RESPOND_DOWN 2
#define RESPOND_LEFT 3
#define RESPOND_RIGHT 4
#define RESPOND_AUTOY 5
#define RESPOND_AUTOX 6

#define TILE_NODRAW 0
#define TILE_DRAWOPAQUE 1
#define TILE_DRAWMASKED 2
#define TILE_DRAWTINTED 3

#define UPLEFT 1
#define UPRIGHT 2
#define DOWNLEFT 3
#define DOWNRIGHT 4

extern int tilenum;

extern int scrollx;
extern int scrolly;

typedef struct
{
	int sizex; // X size of this layer
	int sizey; // Y size of this layer
	int scrollcoefx; // X scrolling coefficient (how fast layer moves with camera)
	int scrollcoefy; // Y scrolling coefficient
	int scrollmomx; // Constant scroll speed along X
	int scrollmomy; // Constant scroll speed along Y
	int *tilemap; // Pointer to tilemap array for this layer
	int **tilerow; // Pointer to tilemap rows for this layer
	struct layer_t *nextlayer;
}layer_t;

typedef struct
{
	int tilepic; // Which sprite index to use as the visible tile
	char solid; // Can you collide with this tile?
	unsigned char coltop; // Top collision position in terms of bounding box
	unsigned char colbottom; // Bottom collision position
	unsigned char colleft; // Left collision position
	unsigned char colright; // Right collision position
	unsigned char croptop; // Top cropping position in tile
	unsigned char cropbottom; // Bottom cropping position
	unsigned char cropleft; // Left cropping position
	unsigned char cropright; // Right cropping position
	unsigned char type; // Tile's behavior type
	char visibility; // Contains visibility information to speed up tile drawing
	char slope; // Slope behavior of tile
}tile_t;

typedef struct
{
	char *zonename; // Full name of the current zone
	char *nextzone; // Filename for the next zone to load
	int gravx; // Default gravity X value
	int gravy; // Default gravity Y value
	int watery; // Water height
	int objlayer; // Layer that performs object interaction
	struct tile_t *tileset; // Pointer to level's tileset attributes
	struct layer_t *firstlayer; // Pointer to the first layer in the zone
}zone_t;

extern zone_t curzone;

extern tile_t tilelist[MAXTILES];

extern layer_t *curlayer;

// prepare.c functions
void tile_define(char solid, unsigned char top, unsigned char bottom, unsigned char left, unsigned char right, unsigned char type, char slope, char visibility);
void game_savezone(const char *target);
// zone.c functions
int tile_collide(int top, int bottom, int left, int right, char response);
