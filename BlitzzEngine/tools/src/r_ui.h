/* Allegro datafile object indexes, produced by grabber v4.2.2, MinGW32 */
/* Datafile: g:\Work\C\BlitzzEngine\data\ui.dat */
/* Date: Tue Mar 25 00:58:01 2008 */
/* Do not hand edit! */

#define R_UI_BEEPERROR                   0        /* SAMP */
#define R_UI_BEEPNOTE                    1        /* SAMP */
#define R_UI_CONSOLEFONT                 2        /* FONT */
#define R_UI_CORNERPIC                   3        /* RLE  */
#define R_UI_LOADER                      4        /* RLE  */
#define R_UI_MENUBACK                    5        /* SAMP */
#define R_UI_MENUCHOOSE                  6        /* SAMP */
#define R_UI_MENUFONT                    7        /* FONT */
#define R_UI_MENUSELECT                  8        /* SAMP */
#define R_UI_COUNT                       9

