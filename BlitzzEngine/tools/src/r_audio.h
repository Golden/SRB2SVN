/* Allegro datafile object indexes, produced by grabber v4.2.2, MinGW32 */
/* Datafile: g:\Work\C\BlitzzEngine\data\audio.dat */
/* Date: Mon Apr 14 18:11:33 2008 */
/* Do not hand edit! */

#define R_SFX_EXPLODE                    0        /* SAMP */
#define R_SFX_PLAYJUMP                   1        /* SAMP */
#define R_SFX_POWERUP                    2        /* SAMP */
#define R_SFX_PULSE                      3        /* SAMP */
#define R_SFX_SPRINGUP                   4        /* SAMP */
#define R_SFX_COUNT                      5

