//Console routines
//By FoxBlitzz
//Creates a console which can view game messages and execute certain commands

#include <allegro.h>

#include "console.h"

char cons_buffer[CONS_BUFFSIZE][256];
unsigned char cons_pos=CONS_BUFFSIZE-1;

void cons_addline(char *text)
{
	char *start = text, *end = text; //The following code courtesy of AJ. Thanks!
	char newlineyet=0;

	while (*end)
	{
		if (*end == '\n')
		{
			if (!newlineyet && cons_buffer[cons_pos])
			{
				newlineyet=1;
				strncat(cons_buffer[cons_pos], start, end-start);
			}
			else
				strncpy(cons_buffer[cons_pos], start, end-start);

			cons_pos++;

			if (cons_pos>CONS_BUFFSIZE-1)
				cons_pos=0;

			memset(cons_buffer[cons_pos],0,256);

			start = end + 1;
		}
		end++;
	} strncpy(cons_buffer[cons_pos], start, end-start);

}
