// Zone-related editing functions
// By FoxBlitzz

// An aside from editor.c to place all the zone functions. The split is because
// keeping everything in editor.c would make the file very long and would result
// in painfully scrolling through a giant file trying to locate what I need.
// Or maybe I just need a new IDE.

#include <string.h>
#include <stdio.h>
#include <allegro.h>

#include "object.h"
#include "editor.h"
#include "gamesys.h"
#include "coregfx.h"
#include "coresound.h"
#include "zone.h"

void edit_lockmouse(void)
{
	if (!edit_mouselocked && gfx_capabilities & GFX_HW_CURSOR)
	{
		edit_mouselocked = 1;
		edit_oldmousex = mouse_x;
		edit_oldmousey = mouse_y;
	}
}

int edit_zonenew(void)
{
	game_unprepare();
	game_prepare(NULL);

	return D_REDRAW;
}

int edit_zoneload(void)
{
	char target[512]="";

	strcat(target,game_datapath);
	strcat(target,"zones/");

	if (file_select_ex("Load Zone...", target, "bkz", 512, 512, 384))
	{
		if (exists(target) && target[strlen(target)-1] != OTHER_PATH_SEPARATOR)
		{
			game_unprepare();
			game_prepare(target);
		}
		else
        {
			game_printf("\nError: File does not exist!");
			alert("Error: File does not exist!", NULL, NULL, "O&k", NULL, 'k', 0);
		}
	}

	return D_REDRAW;
}

int edit_zonesave(void)
{
	char target[512]="";
	int i = 0;

	strcat(target,game_datapath);
	strcat(target,"zones/");

	if (!(gfx_capabilities & GFX_HW_CURSOR))
		show_mouse(screen);

	// If dialog was accepted, and the file either doesn't exist or user says it's okay to overwrite...
	if (file_select_ex("Save Zone As...", target, "bkz", 512, 512, 384) && (!exists(target) || alert("The file you have chosen already exists.", NULL, "Do you want to overwrite the file?", "&No", "&Yes", 'n', 'y') == 2))
	{
		// The user is stupid and forgot a filename.
		if (target[strlen(target)-1] == OTHER_PATH_SEPARATOR)
		{
			char newtarget[512]="";

			do
			{
				sprintf(newtarget, "%szone%i.bkz", target, i);
				i++;

			} while (i <= 1000 && exists(newtarget));

			strcpy(target,newtarget);
		}

		game_savezone(target);
	}

	return D_O_K;
}

static void edit_zonetiles(void)
{
	if (mouse_z)
	{
		edit_curtile -= mouse_z;
	}

	if (edit_clickmode == CM_RECTANGLE)
	{
		edit_lockmouse();
		edit_tilescrollx += edit_mousemovex*4;
		edit_curtile = (edit_tilescrollx+(GAMEWIDTH/2))/TILESIZE;
	}
	else
		edit_tilescrollx = (edit_curtile*TILESIZE)-((GAMEWIDTH-TILESIZE)/2);

	if (edit_clickmode == CM_CURSOR && !edit_scrollcontrol)
	{
		edit_drawingtile = 1;

		if (mouse_b & 1)
		{
			int tilex = edit_selectx2/TILESIZE;
			int tiley = edit_selecty2/TILESIZE;

			if (tilex < 0) {tilex = 0;}
			if (tilex > curlayer->sizex-1) {tilex = curlayer->sizex-1;}
			if (tiley < 0) {tiley = 0;}
			if (tiley > curlayer->sizey-1) {tiley = curlayer->sizey-1;}

			curlayer->tilerow[tilex][tiley] = edit_curtile;
		}
		else if (key[KEY_DEL])
		{
			int tilex = edit_selectx2/TILESIZE;
			int tiley = edit_selecty2/TILESIZE;

			if (tilex < 0) {tilex = 0;}
			if (tilex > curlayer->sizex-1) {tilex = curlayer->sizex-1;}
			if (tiley < 0) {tiley = 0;}
			if (tiley > curlayer->sizex-1) {tiley = curlayer->sizex-1;}

			curlayer->tilerow[tilex][tiley] = 0;
		}
	}
	else
		edit_drawingtile = 0;

	// Choose mouse operating mode...
	if ((key[KEY_SPACE] || mouse_b & 4) && !edit_scrollcontrol)
		edit_clickmode = CM_RECTANGLE; // Select tile
	else if ((mouse_b & 1 || key[KEY_DEL]) && !edit_scrollcontrol && !edit_clickmode)
		edit_clickmode = CM_CURSOR; // Place tile
	else if (!(mouse_b & 1 || key[KEY_DEL]) && !(key[KEY_TAB] || mouse_b & 8))
		edit_clickmode = 0;

	// Disable the auto-hide menu so that it won't pop up when you don't want it to.
	if (edit_clickmode || edit_scrollcontrol)
		gui_zoneedit[1].flags = D_DISABLED;
	else
		gui_zoneedit[1].flags = 0;

}

static void edit_zonemove(void)
{
	int i;
	obj_t* obj;
	obj_t* objselection = NULL;

	edit_selection = 0;
	edit_selectcount = 0;

	for(i=0;i<MAXOBJS;i++)
	{
		obj = &objectlist[i];

		// If object is moused over or in selection rectangle
		if (obj && zone_getobjselection(obj, edit_clickmode))
		{
			if (mouse_b & 1 && !(key_shifts & KB_CTRL_FLAG)) // If object is clicked, do click selection
			{
				// Mark this object as the single selected object unless overwritten.
				if (!(key_shifts & KB_SHIFT_FLAG) && !(key_shifts & KB_CTRL_FLAG) && edit_clickmode != CM_RECTANGLE)
					objselection = obj;
				else if (!(key_shifts & KB_CTRL_FLAG))
				{
					obj->selected = OBJ_SEL_CLICKED;
				}
			}
			// If object wasn't previously selected, do basic hover selection (also deselect when using Ctrl)
			else if (obj->selected < 2 || (mouse_b & 1 && key_shifts & KB_CTRL_FLAG))
				obj->selected = OBJ_SEL_HOVERED;

			edit_selection |= SEL_HOVER;
		}
		// Deselect objects if Shift isn't held, and when they aren't over the selection rectangle
		// If an object is already click-selected but not necessarily hovered over, we have at least one selection.
		else if (obj && !(key_shifts & KB_SHIFT_FLAG) && !(key_shifts & KB_CTRL_FLAG) && (obj->selected < OBJ_SEL_CLICKED || ( edit_clickmode == CM_RECTANGLE )))
			obj->selected = 0;
		if (obj->selected == OBJ_SEL_CLICKED)
		{
			edit_selection |= SEL_SOMETHING;
			edit_selectcount++;
		}
		// Move objects if selected and in dragging mode.
		if (edit_clickmode == CM_MOVE && obj->selected > 0)
		{
			if (menu_edit[6].flags & D_SELECTED)
			{
				obj->x = (edit_selectx2 - obj->offsetx + (edit_gridsize/2)) / (edit_gridsize) * (edit_gridsize);
				obj->y = (edit_selecty2 - obj->offsety + (edit_gridsize/2)) / (edit_gridsize) * (edit_gridsize);
			}
			else
			{
				obj->x = (edit_selectx2 - obj->offsetx);
				obj->y = (edit_selecty2 - obj->offsety);
			}
		}
		// Update offsets for next movement operation.
		else
		{
			obj->offsetx = edit_selectx2 - obj->x;
			obj->offsety = edit_selecty2 - obj->y;
		}
	}

	// If there is a single selection object found, select it
	if (objselection)
	{
		edit_clickmode = CM_STARTMOVE; // User already pressed mouse down, don't check again. Prepare for object drag.

		if (objselection->selected < OBJ_SEL_CLICKED)
		{
			// Deselect all the other objects
			for(i=0;i<MAXOBJS;i++)
			{
				obj = &objectlist[i];
				if (obj != objselection)
					obj->selected = 0;
				else
					obj->selected = OBJ_SEL_CLICKED;
			}
		}
	}

	// Choose mouse operating mode...
	if (mouse_b & 1 && edit_scrollcontrol == 0 && !(edit_selection & SEL_HOVER) && !edit_clickmode)
		edit_clickmode = CM_RECTANGLE;
	else if (edit_scrollcontrol == SCR_RELATIVE)
		edit_clickmode = CM_DEADCLICK;
	else if (mouse_b & 1 && !edit_clickmode)
		edit_clickmode = CM_CURSOR;
	else if (mouse_b & 1 && edit_clickmode == CM_STARTMOVE && (edit_mousemovex || edit_mousemovey))
		edit_clickmode = CM_MOVE;
	else if (!(mouse_b & 1))
		edit_clickmode = 0;

	// Disable the auto-hide menu so that it won't pop up when you don't want it to.
	if (edit_clickmode || edit_scrollcontrol)
		gui_zoneedit[1].flags = D_DISABLED;
	else
		gui_zoneedit[1].flags = 0;

}

int zone_think(int msg, DIALOG *d, int c)
{

	int returner = D_O_K;
	//int mykey=0;

	(void)d; //unused
	(void)c; //unused

	switch (msg)
	{
		case MSG_START:
			game_render();
			do_dialog(gui_aboutbox,-1);

		case MSG_IDLE:

			if (game_end != 0)
				return D_CLOSE;

			edit_doaction();

			poll_keyboard();
			poll_mouse();

			/*if (keypressed()) {mykey=readkey();}

			if ((mykey >> 8) == KEY_D)
			{
				uiflags ^= UI_DEBUGWORLD;
			}*/

			// Update camera's position according to various rules of
			// the controlled object, taking EG. velocity into account.

			if ((game_tic || edit_drawingtile) && grfx_camthink() && !(mouse_b & 2))
			{
				returner = D_REDRAWME;
				game_tic = 0;
			}

			if (mouse_x >= 0)
			{
				if (edit_clickmode != CM_RECTANGLE && edit_clickmode != CM_MOVE )
				{
					edit_selectx1 = mouse_x+scrollx;
					edit_selecty1 = mouse_y+scrolly;
				}
				edit_selectx2 = mouse_x+scrollx;
				edit_selecty2 = mouse_y+scrolly;
			}

			if (returner == D_REDRAWME)
			{
				switch (edit_zonetool)
				{
					case ZT_TILES:
						edit_zonetiles();
						break;
					case ZT_OBJMOVE:
						edit_zonemove();
						break;
				}
			}

			if (edit_mouselocked) // Limit mouse movement
				position_mouse(edit_oldmousex, edit_oldmousey);

			if (edit_scrollcontrol != SCR_ABSOLUTE && (edit_zonetool != ZT_TILES || edit_clickmode != CM_RECTANGLE)) // Unlock when no longer needed
				edit_mouselocked = 0;

			if (mouse_b & 2)
			{
				if (!(gfx_capabilities & GFX_HW_CURSOR))
					show_mouse(screen);

				if (edit_selection & SEL_HOVER)
					do_menu(menu_contextobj, mouse_x, mouse_y);
				else
					do_menu(menu_contextzone, mouse_x, mouse_y);
			}

			edit_keypressed = 0;
			position_mouse_z(0);

			sdfx_poll();
			break;

		case MSG_DRAW:
			game_render();
			break;

	}

	return returner;
}
