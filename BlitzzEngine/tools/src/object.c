// Code by Shuffle
// This file should hold the majority of functions that work with or modify
// objects. Over time, obj_think and obj_setattributes will become more
// and more complex as we add specific thinkers and attributes to new objects.

#include <stdio.h>
#include <math.h>

#include "object.h"
#include "zone.h"

#include "gamesys.h"
#include "config.h"
#include "coregfx.h"
#include "resources.h"
#include "coresound.h"

#include "r_audio.h"

// This array keeps track of all of the objects currently in the game.
obj_t objectlist[MAXOBJS];

// Spawn an object at the given location.
obj_t* obj_spawn(int type, int x, int y)
{
	int i;
	obj_t* object = NULL;

	for(i=0;i<MAXOBJS;i++)
	{
		// Maximum number of objects reached, don't spawn.
		if(i == MAXOBJS-1)
		{
			game_printf("\nWARNING! Cannot spawn object of type %i - Reached object limit!", type);
			return 0;
		}

		// This slot is taken, move along..
		if(objectlist[i].alive)
			continue;

		// Found a slot!
		object = &objectlist[i];
		break;
	}

	memset(object, 0, sizeof(obj_t));
	object->x = x;
	object->y = y;
	object->type = type;
	object->alive = AO_ACTIVE;

	obj_setattributes(object);

	return object;
}

void obj_kill(obj_t* object)
{
	memset(object, 0, sizeof(obj_t));
	object->alive = AO_EMPTY;
}

// Counts the total number of object slots being used.
int obj_count(void)
{
	int i;
	int count = 0;

	for(i=0;i<MAXOBJS;i++)
	{
		if(objectlist[i].alive)
			count++;
	}

	return count;
}
