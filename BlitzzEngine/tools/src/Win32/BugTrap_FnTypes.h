typedef BOOL (APIENTRY *BT_READVERSIONINFO)(HMODULE hModule);
typedef void (APIENTRY *BT_SETSUPPORTURL)(LPCTSTR pszSupportURL);
typedef void (APIENTRY *BT_SETSUPPORTEMAIL)(LPCTSTR pszSupportEMail);
typedef void (APIENTRY *BT_SETFLAGS)(DWORD dwFlags);
typedef void (APIENTRY *BT_SETAPPNAME)(LPCTSTR pszAppName);
typedef void (APIENTRY *BT_SETAPPVERSION)(LPCTSTR pszAppVersion);
typedef void (APIENTRY *BT_SETDIALOGMESSAGE)(BUGTRAP_DIALOGMESSAGE eDialogMessage, LPCTSTR pszMessage);
