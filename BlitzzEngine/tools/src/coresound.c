// BlitzzEngine sound functions
// By FoxBlitzz
// This code will handle all functions dealing with sound voices and background
// music through the mixing functions. Using voices allows greater control over
// how a sound behaves. You will be able to change pan, pitch, and "attach" a
// sound to an object entity.

#include <allegro.h>
#include "logg.h"

#include "gamesys.h"
#include "coregfx.h"
#include "object.h"
#include "coresound.h"

#define PM_GLOBAL 0
#define PM_TRACKOBJ 1

#define NUMVOICES 256

typedef struct
{
	obj_t* object;
	int playmode;
	int priority;
	int volume;
	int strength;
} voice_t;

static voice_t voicelist[NUMVOICES];

static LOGG_Stream *oggsong = NULL;

// Stops any playing song and then starts a new stream.
void sdfx_loadsong(const char *song)
{
	char target[512]="";
	char message[80]="  Playing ";


	strcat(target,game_datapath);
	strcat(target,"OGG/");
	strcat(target,song);

	strcat(message, song);
	strcat(message, "...");
	memset(message+strlen(message), ' ',60-strlen(message));

	game_printf(message);

	if (oggsong)
		logg_destroy_stream(oggsong);

	oggsong = logg_get_stream(target, 190, 128, 1);
	voice_set_priority(oggsong->audio_stream->voice,255);

	voicelist[oggsong->audio_stream->voice].playmode = PM_GLOBAL; // Reset playmode for this voice
	voicelist[oggsong->audio_stream->voice].object = NULL;
	voicelist[oggsong->audio_stream->voice].priority = 255;


	if (oggsong)
	{
		game_showsucc();
	}
	else
		game_showerror("Could not read from %s!\n\nPlease ensure that the game is installed properly,\nand that you have read access to the file.",target);
}

// Counts how many voices are being played.
int sdfx_count(void)
{
	int i;
	int voices = 0;

	for (i = 0; i < get_mixer_voices(); i++)
	{
		if (voice_check(i) != NULL)
			voices++;
	}

	return voices;
}

// Checks if any sounds are free, or if there are any used voices that have lower
// priority than the sound.
int sdfx_checkfree(SAMPLE *sound)
{
	int i;

	for (i = 0; i < get_mixer_voices(); i++)
	{
		if (voice_check(i) == NULL || voicelist[i].priority < sound->priority)
			return 1;
	}

	return 0;
}

// Checks if any instances of the specified sound are playing and returns the first match.
// If object is not null, it searches for any voice that has both the sound and object attachment.
int sdfx_find(SAMPLE *sound, obj_t* checkobj)
{
	int i;
	SAMPLE *checkvoice;

	for (i = 0; i < get_mixer_voices(); i++)
	{
		checkvoice = voice_check(i);

		if (checkobj && (obj_t*)voicelist[i].object == checkobj && checkvoice == sound)
			return i;

		else if ( !(checkobj) && checkvoice == sound)
			return i;
	}

	return -1;
}

// Checks if any instances of the specified sound are playing, and stops them when found.
void sdfx_stop(SAMPLE *sound)
{
	int i;
	SAMPLE *checkvoice;

	for (i = 0; i < get_mixer_voices(); i++)
	{
		checkvoice = voice_check(i);
		if (checkvoice == sound)
			deallocate_voice(i); // Halt sound and free the voice
	}
}

// Starts a voice using the specified volume, pan, pitch, and playback mode
int sdfx_play(SAMPLE *sound, int mode, int volume, int pan, int freq, int loop)
{
	int newvoice;

	if (mode == SP_INTERRUPT)
		sdfx_stop(sound); // Stop any existing voices if specified

	// Don't play new instances if one is already playing.
	if (mode == SP_PLAYONCE && sdfx_find(sound, NULL) == -1)
		return -1;

	newvoice = allocate_voice(sound); // Create sound voice but don't play yet

	if (newvoice != -1)
	{
		voicelist[newvoice].playmode = PM_GLOBAL; // Reset playmode for this voice
		voicelist[newvoice].object = NULL;
		voicelist[newvoice].priority = sound->priority;

		voice_set_priority(newvoice,sound->priority);
		voice_set_volume(newvoice, volume); // Adjust parameters
		if (freq != 1000)
			voice_set_frequency(newvoice,voice_get_frequency(newvoice)*freq/1000.0);
		voice_set_pan(newvoice, pan);

		if (!loop)
			voice_set_playmode(newvoice,PLAYMODE_PLAY);
		else
			voice_set_playmode(newvoice,PLAYMODE_LOOP);

		voice_start(newvoice); // Actually play the sound

		//if (!loop)
			//release_voice(newvoice); // Automatically free sound voice when done playing
	}

	return newvoice;
}

// Gives a voice an object ID for stereo tracking
void sdfx_attach(int voice, obj_t* target, int volume, int strength)
{
	voicelist[voice].playmode = PM_TRACKOBJ;
	voicelist[voice].object = target;
	voicelist[voice].volume = volume;
	voicelist[voice].strength = strength;
}

void sdfx_fadesong(int time)
{

	voice_ramp_volume(oggsong->audio_stream->voice,time,0);

}

// Must be called once every render frame (not gametic) to update 3D sounds and prevent Source Engine Syndrome
void sdfx_poll(void)
{
	int i;
	int xpos;
	int ypos;
	int pan;
	float vol;
	obj_t* object;

	if (oggsong)
		logg_update_stream(oggsong);
	//voice_set_frequency(oggsong->audio_stream->voice,sin(game_tics*100)*10000+40000);

	for (i = 0; i < get_mixer_voices(); i++)
	{
		// Get rid of zero-volume or finished voices to conserve channels
		if (voice_get_volume(i) <= 0 || voice_get_position(i) == -1)
		{
			deallocate_voice(i);
			voicelist[i].object = NULL;
			continue;
		}

		if (voicelist[i].playmode == PM_TRACKOBJ)
		{
			object = (obj_t*)voicelist[i].object;

			if (object->alive < AO_ACTIVE)
			{
				voice_set_volume(i,voice_get_volume(i)-10);
				continue;
			}

			xpos = ((object->x-scrollx-(GAMEWIDTH/2))/voicelist[i].strength);
			ypos = ((object->y-scrolly-(GAMEHEIGHT/2))/voicelist[i].strength);
			pan = xpos + 128;

			vol = voicelist[i].volume-(fixhypot(xpos*65536,ypos*65536)/65536);

			if (pan > 255) {pan = 255;} else if (pan < 0) {pan = 0;}
			if (vol > 255) {vol = 255;} else if (vol < 0) {vol = 0;}

			voice_set_pan(i,pan);
			voice_set_volume(i,vol);
		}
	}

}

