// BlitzzEngine start-up routines
// By FoxBlitzz
// Basic start-up code that writes lots of fun stuffs to a log file while it
// initiates all of Allegro's little subsystems. It also reads from blitzz.ini
// here so that it knows what options to start the graphics system with.

#include <stdio.h>
#include <time.h>
#include <string.h>

#include <allegro.h>

#ifdef BUGTRAP
#include <winalleg.h>
#include "Win32/BugTrap.h"
#include "Win32/BugTrap_FnTypes.h"
#endif

#include "../Monkeywrench_private.h"
#include "config.h"
#include "gamesys.h"
#include "console.h"

#include "resources.h"
#include "r_core.h"
#include "r_editor.h"

#include "editor.h"

#define GAMEFRAMERATE 60

FILE *logfile;

//Initiate counter variables
volatile int game_tic=0;


//User interfacing
unsigned char uiflags = 0;

// Prints line to console/log

void game_printf(const char *format, ...)
{
	char textbuffer[512];
	va_list ap;

	va_start(ap, format);
	uvszprintf(textbuffer, sizeof(textbuffer), format, ap);
	va_end(ap);

	cons_addline(textbuffer);
	fprintf(logfile, textbuffer);
	printf(textbuffer);
	fflush(logfile);
}

static void sys_close_handle(void)
{
	simulate_keypress(27 + (KEY_ESC << 8));
	simulate_keypress(27 + (KEY_ESC << 8));
	game_end = 1;
}

END_OF_FUNCTION(sys_close_handle)

static void inc_game_timer(void)
{

	game_tic = 1;

}

END_OF_FUNCTION(inc_game_timer)

static void sys_switch_in(void)
{
	game_printf("\nSwitched into program...");
}

static void sys_switch_out(void)
{
	game_printf("\nSwitched out of program...");
}

#ifdef BUGTRAP

static HMODULE bugt_bugtrapdll;

static void bugt_installcrasher(void)
{
	//BT_SETSUPPORTEMAIL lpfnBT_SetSupportEMail;
	BT_SETFLAGS lpfnBT_SetFlags;
	BT_SETSUPPORTURL lpfnBT_SetSupportURL;
	//BT_READVERSIONINFO lpfnBT_ReadVersionInfo;
	BT_SETAPPNAME lpfnBT_SetAppName;
	BT_SETAPPVERSION lpfnBT_SetAppVersion;
	BT_SETDIALOGMESSAGE lpfnBT_SetDialogMessage;

	bugt_bugtrapdll = LoadLibrary("BugTrap.dll");

	//lpfnBT_SetSupportEMail = (BT_SETSUPPORTEMAIL)GetProcAddress(bugt_bugtrapdll, "BT_SetSupportEMail");
	lpfnBT_SetFlags = (BT_SETFLAGS)GetProcAddress(bugt_bugtrapdll, "BT_SetFlags");
	lpfnBT_SetSupportURL = (BT_SETSUPPORTURL)GetProcAddress(bugt_bugtrapdll, "BT_SetSupportURL");
	//lpfnBT_ReadVersionInfo = (BT_READVERSIONINFO)GetProcAddress(bugt_bugtrapdll, "BT_ReadVersionInfo");
	lpfnBT_SetAppName = (BT_SETAPPNAME)GetProcAddress(bugt_bugtrapdll, "BT_SetAppName");
	lpfnBT_SetAppVersion = (BT_SETAPPVERSION)GetProcAddress(bugt_bugtrapdll, "BT_SetAppVersion");
	lpfnBT_SetDialogMessage = (BT_SETDIALOGMESSAGE)GetProcAddress(bugt_bugtrapdll, "BT_SetDialogMessage");

	if(bugt_bugtrapdll)
	{
		lpfnBT_SetAppName(GAMENAME);
		lpfnBT_SetAppVersion(GAMENAMELONG);
		//lpfnBT_SetSupportEMail("danhagerstrand@gmail.com");
		lpfnBT_SetFlags(BTF_EDITMAIL | BTF_ATTACHREPORT);
		lpfnBT_SetSupportURL("http://chaos.foxmage.com/blitzzkrieg");
		lpfnBT_SetDialogMessage(BTDM_INTRO1, "Monkeywrench has encountered an error and needs to close.");
		lpfnBT_SetDialogMessage(BTDM_INTRO2, "It is likely that Monkeywrench tried to use a resource that "
											"hasn't been loaded yet, or an internal function performed erratically. "
											"By sending an error report, you can help us fix bugs. Please include "
											"a detailed explanation on what you were doing before the crash.");
		//lpfnBT_ReadVersionInfo(NULL);
	}
}

#endif

void game_initsystem(void)
{
	//Initiate time variables
	struct tm *local;
	time_t t;
	int i;

	t = time(NULL);

	local = localtime(&t);

	logfile = fopen("editlog.txt", "wt");

	#ifdef BUGTRAP
	bugt_installcrasher();
	#endif

	//Initiate Allegro!
	game_printf("%s\n\nLog time: %s",GAMENAMELONG,asctime(local));
	game_printf("\n\n___________________\n\nAttempting to boot Monkeywrench:\n\nInitializing Allegro system...                              ");

	if (allegro_init()==0)
		game_showsucc();
	else
		game_showerror("Could not start the Allegro system!");

	//Lock close-button function
	LOCK_FUNCTION(sys_close_handle);
	set_close_button_callback(sys_close_handle);

	game_printf("Grabbing config from blitzz.ini...                          ");
	game_getconfig();
	game_printf("Done!\n");

	//Initiate graphics
	game_printf("Opening video device...\n  Requested driver setting: %s (%dx%dx%d)\n  Attempting to find a mode...                              ",getgfxdriver, game_gfxwidth, game_gfxheight, game_gfxdepth);
	set_window_title(GAMENAME);
	if ((game_gfxdepth==0 && (game_setmode(16) || game_setmode(15) || game_setmode(32) || game_setmode(24))) || game_setmode(game_gfxdepth))
		game_printf("Success!\n    Using device: %s (%dx%dx%d)\n",gfx_driver->name,SCREEN_W,SCREEN_H,bitmap_color_depth(screen));
	else
		game_showerror("Could not find a video mode!");

	textprintf_centre_ex(screen, font, SCREEN_W/2, SCREEN_H-32, makecol(255, 255, 255), -1, "%s - Loading interface...",GAMENAMELONG);

	//Because people LOVE to bitch about Allegro programs freezing and crashing
	// on them, I'm going to switch the task mode so that it runs in the
	// background. Maybe that'll shut them up. ;P
	game_printf("Switching to background task mode...                        ");
	if (set_display_switch_mode(SWITCH_BACKGROUND)==0 || set_display_switch_mode(SWITCH_BACKAMNESIA)==0)
		game_showsucc();
	else
		game_showfail(); //Non-fatal error. Continue anyway.

	//Initiate mouse
	game_printf("Opening mouse device...                                     ");
	if (install_mouse()>0)
		game_showsucc();
	else
		game_showerror("Could not install mouse!");

	enable_hardware_cursor();
	select_mouse_cursor(MOUSE_CURSOR_ARROW);
	show_mouse(NULL);

	//Initiate keyboard
	game_printf("Opening keyboard device...                                  ");
	if (install_keyboard()==0)
		game_showsucc();
	else
		game_showerror("Could not install keyboard!");

	LOCK_FUNCTION(edit_keycallback);
	LOCK_VARIABLE(edit_keypressed);
	keyboard_lowlevel_callback = edit_keycallback;


	//Initiate timers
	game_printf("Installing timer...                                         ");
	if (install_timer()==0)
		game_showsucc();
	else
		game_showerror("Could not install timer!");

	LOCK_FUNCTION(inc_game_timer);
	LOCK_VARIABLE(game_tic);

	install_int_ex(inc_game_timer, BPS_TO_TIMER(GAMEFRAMERATE));

	#ifdef ALLEGRO_WINDOWS
	//Overlay seems to only work by pageflip mode on this machine. I guess it makes sense.
	if (strcmp(gfx_driver->name,"DirectDraw overlay")==0)
	{
		game_showwarn("You have requested DirectDraw Overlay mode with double-buffering.\nOn some machines, this may not work properly.\n\nThe game will start normally.\nIf you notice problems with the display, use page-flipping instead.");
	}
	#endif

	game_printf("Creating memory buffer bitmap...                            ");
	//Create double-buffer bitmap
	buffer = create_bitmap(GAMEWIDTH,GAMEHEIGHT);
	if (buffer)
		game_showsucc();
	else
		game_showerror("Could not create memory bitmap!");

	game_printf("Loading core information...\n");
	r_core=game_loaddata("core.dat");

	if (bitmap_color_depth(screen) == 8)
	{
		static RGB_MAP rgbtable;
		game_printf("Performing palette calculations...                          ");
		set_palette(r_core[R_COR_COLORPAL].dat);
		select_palette(r_core[R_COR_COLORPAL].dat);
		create_rgb_table(&rgbtable, r_core[R_COR_COLORPAL].dat, NULL);
		rgb_map = &rgbtable;

		for (i = 0; i < GRFX_8BITAPRECISION; i++)
		{
			create_trans_table(&transtable[i], r_core[R_COR_COLORPAL].dat, 255*(i+1)/GRFX_8BITAPRECISION, 255*(i+1)/GRFX_8BITAPRECISION, 255*(i+1)/GRFX_8BITAPRECISION, NULL);
		}

		color_map = &transtable[GRFX_8BITAPRECISION/4];
		game_printf("Done!\n");
	}

	//Color conversion
	set_color_conversion(COLORCONV_TOTAL + COLORCONV_KEEP_TRANS + COLORCONV_DITHER);

	//Initiate sound
	#ifdef ALLEGRO_WINDOWS
	game_printf("Opening audio device...\n  Requested driver setting: %s (%d)\n  Attempting to load system...                              ",getsounddriver, getsoundcard);
	#else
	game_printf("Opening audio device...\n  Requested driver setting: %s\n  Attempting to load system...                              ",getsounddriver);
	#endif
	reserve_voices(16,0);
	if (install_sound(game_sounddriver,MIDI_NONE,NULL)==0)
	{
		game_printf("Success!\n    Using device: %s \n",digi_driver->name);
	}
	else
		game_showfail(); //Non-fatal error. Continue anyway.

	set_volume_per_voice(0);

	// Install task switching callbacks, mainly to reduce CPU when the window is inactive.
	set_display_switch_callback(SWITCH_IN, sys_switch_in);
	set_display_switch_callback(SWITCH_OUT, sys_switch_out);

	// Load global user interface media
	game_printf("Loading user interface media...\n");
	r_ui=game_loaddata("ui.dat");
	r_editor=game_loaddata("editor.dat");

	clear_to_color(screen,makecol(0,0,0));

	game_printf("Calibrating GUI elements...                                 ");

	// Setup GUI stuff
	gui_mouse_focus = 0;

	// Size and position
	gui_zoneedit[0].w = gui_zoneedit[1].w = SCREEN_W;
	gui_zoneedit[0].h = SCREEN_H;
	centre_dialog(gui_aboutbox);

	// Data pointers
	gui_aboutbox[0].dp = r_editor[R_EDIT_SPLASH].dat;

	// GUI colors
	gui_fg_color = makecol(160,190,250);
	gui_mg_color = makecol(110,110,110);
	gui_bg_color = makecol(10,20,50);
	set_dialog_color(gui_zoneedit, makecol(160,190,250), makecol(10,20,50));
	set_dialog_color(gui_aboutbox, makecol(160,190,250), makecol(10,20,50));
	select_mouse_cursor(MOUSE_CURSOR_ALLEGRO);
	set_mouse_sprite_focus(0,0); // The default mouse sprite focus sucks. Really.
								 // It should be placed at, you know, the tip.

	game_printf("Done!\n\nProgram successfully booted!");
}

void game_killsystem(void)
{
	game_printf("\n\n___________________\n\nAttempting to shutdown Monkeywrench:\n\nDeinitializing Allegro...                                   ");
	allegro_exit();
	game_printf("Done!\n\nClosing normally...\n");
}

int game_setmode(int depth) {
	set_color_depth(depth);
	return set_gfx_mode(game_gfxdriver, game_gfxwidth, game_gfxheight, 0, 0) == 0;
}

void game_showsucc(void)
{
	game_printf("Success!\n");
}

void game_showfail(void)
{
	game_printf("FAILURE!\n  Non-fatal error. Continuing anyway...\n");
}

void game_showwarn(const char* format, ...) {
	char textbuffer[512];
	va_list ap;

	va_start(ap, format);
	uvszprintf(textbuffer, sizeof(textbuffer), format, ap);
	va_end(ap);

	if (game_showwarning)
		allegro_message("%s",textbuffer);
}

int game_showerror(const char* format, ...) {
	char textbuffer[512];
	va_list ap;

	va_start(ap, format);
	uvszprintf(textbuffer, sizeof(textbuffer), format, ap);
	va_end(ap);

	game_printf("\n\nFatal error: %s \n%s \n\nThe game will now abort.",textbuffer,allegro_error);
	set_gfx_mode(GFX_TEXT, 0, 0, 0, 0);
	allegro_message("\n\nFatal error: %s \n%s \n\nThe game will now abort.",textbuffer,allegro_error);
	exit(EXIT_FAILURE);
	return 0;
}
