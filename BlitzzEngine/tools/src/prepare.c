#include <stdio.h>
#include <string.h>
#include <allegro.h>

#include "config.h"

#include "resources.h"
#include "zone.h"
#include "r_ui.h"
#include "r_audio.h"
#include "r_world.h"

#include "gamesys.h"
#include "coregfx.h"
#include "object.h"
#include "coresound.h"

#define CHECKOPENLOAD if (!zonefile) { game_printf("\nError: Unable to open file for reading!"); alert("Error: Unable to open file for reading!", NULL, NULL, "O&k", NULL, 'k', 0); return; }
#define CHECKERRLOAD if (pack_ferror(zonefile)) { game_printf("\nError: Unable to read from the file!"); alert("Error: Unable to read from the file!", NULL, NULL, "O&k", NULL, 'k', 0); return; }
#define CHECKOPENSAVE if (!zonefile) { game_printf("\nError: Unable to open file for writing!"); alert("Error: Unable to open file for writing!", NULL, NULL, "O&k", NULL, 'k', 0); return; }
#define CHECKERRSAVE if (pack_ferror(zonefile)) { game_printf("\nError: Unable to write to the file!"); alert("Error: Unable to write to the file!", NULL, NULL, "O&k", NULL, 'k', 0); return; }
#define CHECKEOF if (pack_feof(zonefile)) { game_printf("\nError: Reached EOF!"); alert("Error: Reached EOF!", NULL, NULL, "O&k", NULL, 'k', 0); return; }

#define CHECKOPENFATAL if (!zonefile) { game_showerror("Unable to open file for reading!"); }
#define CHECKERRFATAL if (pack_ferror(zonefile)) { game_showerror("Unable to read from the file!"); }
#define CHECKEOFFATAL if (pack_feof(zonefile)) { game_showerror("Reached EOF!"); }

#define ZONEREVISION "r0\n"

#define CHUNK_LAYER 100

DATAFILE *r_core;
DATAFILE *r_ui;
DATAFILE *r_world;
DATAFILE *r_actor;
DATAFILE *r_audio;
DATAFILE *r_editor;

static int barindex;
static int barcolor;
static int colortoggle;

zone_t curzone;

static void game_loadingbar(DATAFILE *datafile)
{
	(void)datafile;


	if (barindex >= 256)
	{
		colortoggle ^= 1;
		barindex = 0;
	}
	else
	{
		if (colortoggle)
			barcolor = makecol(0,96,255);
		else
			barcolor = makecol(0,64,192);
		rectfill(screen, SCREEN_W/2-127+barindex, SCREEN_H*3/4+1, SCREEN_W/2-100+barindex, SCREEN_H*3/4+19, barcolor);
		barindex +=32;
	}
}

DATAFILE *game_loaddata(const char *datafile)
{
	DATAFILE *newdatafile;
	char target[512]="";
	char message[80]="  Opening ";

	strcat(target,game_datapath);
	strcat(target,datafile);

	strcat(message, datafile);
	strcat(message, "...");
	memset(message+strlen(message), ' ',60-strlen(message));

	game_printf(message);

	rectfill(screen, SCREEN_W/2-127, SCREEN_H*3/4+1, SCREEN_W/2+127, SCREEN_H*3/4+41, makecol(0,0,0));
	textprintf_centre_ex(screen, font, SCREEN_W/2, SCREEN_H*3/4+27, makecol(0, 128, 255), -1, "%s", datafile);
	rect(screen, SCREEN_W/2-128, SCREEN_H*3/4, SCREEN_W/2+128, SCREEN_H*3/4+20, makecol(0,64,192));
	barindex = 0;
	colortoggle = 1;
	newdatafile = load_datafile_callback(target, game_loadingbar);

	if (newdatafile)
	{
		game_showsucc();
		return newdatafile;
	}
	else
		game_showerror("Could not read from %s!\n\nPlease ensure that the game is installed properly,\nand that you have read access to the file.",target);
	return newdatafile;
}

void game_savezone(const char *target)
{
	PACKFILE *zonefile;
	//int result;

	layer_t *layer;
	int *map;

	game_printf("\n\nSaving zone:\n%s\n", target);

	// Open packfile for writing
	zonefile = pack_fopen(target, F_WRITE_NOPACK);
	CHECKOPENSAVE

	// Chunk 1: Version information (uncompressed)
	zonefile = pack_fopen_chunk(zonefile, 0);
	CHECKOPENSAVE

	// Name
	pack_fputs("BlitzzEngine Zone File\n", zonefile);
	CHECKERRSAVE
	// File format Revision
	pack_fputs(ZONEREVISION, zonefile);
	CHECKERRSAVE

	zonefile = pack_fclose_chunk(zonefile);
	CHECKOPENSAVE

	// Chunk 2: Layer information (compressed)
	zonefile = pack_fopen_chunk(zonefile, 1);
	CHECKOPENSAVE

	// First byte: Type of chunk (in this case 100 means layer)
	pack_putc(CHUNK_LAYER, zonefile);
	CHECKEOF

	// Next two bytes are the dimensions of this layer
	layer = (void *)curzone.firstlayer;
	pack_putc(layer->sizex, zonefile);
	CHECKEOF
	pack_putc(layer->sizey, zonefile);
	CHECKEOF

	// Next is the contiguous memory of the layer's tile array
	map = layer->tilemap;
	pack_fwrite(map, layer->sizex * layer->sizey * sizeof(int), zonefile);
	CHECKERRSAVE

	zonefile = pack_fclose_chunk(zonefile);
	CHECKOPENSAVE

	// Close packfile
	pack_fclose(zonefile);

	game_printf("\nFinished file output.\n");

}

void game_prepare(const char *target)
{
	PACKFILE *zonefile = NULL;
	char formatname[30];
	char formatrev[5];
	char loadmode = 0;

	int numrows = 64;
	int numcols = 32;
	int i;

	RLE_SPRITE *loaderpic;

	tilenum = 0;

	game_printf("\n\n___________________\n\nLoading area. Please Wait...                                ");

	acquire_bitmap(buffer);

	clear_to_color(buffer,makecol(0,0,0));

	loaderpic = (RLE_SPRITE *)r_ui[R_UI_LOADER].dat;
	draw_rle_sprite(buffer,loaderpic,(GAMEWIDTH/2)-(loaderpic->w/2),(GAMEHEIGHT/2)-(loaderpic->h/2));

	release_bitmap(buffer);

	grfx_refresh();

	if (target)
	{
		char result;

		game_printf("\n\nLoading zone:\n%s\n", target);

		// Open packfile for reading
		zonefile = pack_fopen(target, F_READ_PACKED);
		CHECKOPENFATAL

		// Chunk 1: Version information (uncompressed)
		zonefile = pack_fopen_chunk(zonefile, 1);
		CHECKOPENFATAL

		// Name
		pack_fgets(formatname, sizeof(formatname), zonefile);
		CHECKERRFATAL
		game_printf("\n - %s", formatname);

		// File format Revision
		pack_fgets(formatrev, sizeof(formatrev), zonefile);
		CHECKERRFATAL
		game_printf("\n - File Format Revision %s", formatrev);

		zonefile = pack_fclose_chunk(zonefile);
		CHECKOPENFATAL

		// Chunk 2: Layer information (compressed)
		zonefile = pack_fopen_chunk(zonefile, 1);
		CHECKOPENFATAL

		// First byte: Type of chunk (in this case 100 means layer)
		loadmode = pack_getc(zonefile);
		CHECKEOFFATAL

		if (loadmode == CHUNK_LAYER)
		{
			// Next two bytes are the dimensions of this layer
			result = pack_getc(zonefile);
			CHECKEOF
			numrows = result;
			result = pack_getc(zonefile);
			CHECKEOF
			numcols = result;
		}
	}

	game_printf("\nWriting Layers...                                           ");

	// Allocate a sample layer
	curzone.firstlayer = malloc(sizeof(layer_t));

	curlayer = (void *)curzone.firstlayer;

	// Set max dimensions
	curlayer->sizex = numrows;
	curlayer->sizey = numcols;

	// Allocate the array itself
	curlayer->tilemap = calloc(curlayer->sizex * curlayer->sizey, sizeof(int));
	if (curlayer->tilemap == NULL)
		game_showerror("Could not allocate the tile layer array.\nYou may be low on memory.");

	// Allocate the array row pointers
	curlayer->tilerow = malloc(curlayer->sizex * sizeof(int *));
	if (curlayer->tilerow == NULL)
		game_showerror("Could not allocate the tile layer pointers.\nYou may be low on memory.");

	// Adjust the pointers to go to each row.
	for (i = 0; i < curlayer->sizex; i++)
	{
		curlayer->tilerow[i] = curlayer->tilemap + (i * curlayer->sizey);
	}

	if (target && loadmode == CHUNK_LAYER)
	{

		// Next is the contiguous memory of the layer's tile array
		pack_fread(curlayer->tilemap, curlayer->sizex * curlayer->sizey * sizeof(int), zonefile);
		CHECKERRFATAL

		zonefile = pack_fclose_chunk(zonefile);
		CHECKOPENFATAL

		// Close packfile
		pack_fclose(zonefile);

	}

	game_printf("Success!\n\nOpening datafiles...\n");

	r_world=game_loaddata("worlds/testtmpl.dat");
	r_actor=game_loaddata("actors.dat");
	r_audio=game_loaddata("audio.dat");

	//game_printf("\nPreparing music...\n");

	//sdfx_loadsong("SZONE0.ogg");

	game_printf("\nDefining tiles...                                           ");

	// Insert pathetic tile definition stuff here. When the real loading is
	//  done, both this code and the related function will be abolished.
	tile_define(1,0,63,0,63,0,0,TILE_NODRAW);
	tile_define(1,0,63,0,63,0,0,TILE_DRAWMASKED);
	tile_define(1,0,63,0,63,0,0,TILE_DRAWMASKED);
	tile_define(1,0,63,0,63,0,0,TILE_DRAWMASKED);
	tile_define(1,0,63,0,63,0,0,TILE_DRAWMASKED);
	tile_define(1,0,63,0,63,0,0,TILE_DRAWMASKED);
	tile_define(1,0,63,0,63,0,0,TILE_DRAWMASKED);
	tile_define(1,0,63,0,63,0,0,TILE_DRAWMASKED);
	tile_define(1,0,63,0,63,0,0,TILE_DRAWMASKED);
	tile_define(1,0,63,0,63,0,0,TILE_DRAWMASKED);
	tile_define(1,0,63,0,63,0,0,TILE_DRAWMASKED);
	tile_define(1,0,63,0,63,0,0,TILE_DRAWMASKED);
	tile_define(1,0,63,0,63,0,0,TILE_DRAWMASKED);
	tile_define(1,0,63,0,63,0,0,TILE_DRAWMASKED);
	tile_define(1,0,63,0,63,0,0,TILE_DRAWMASKED);
	tile_define(1,0,63,0,63,0,0,TILE_DRAWMASKED);
	tile_define(1,0,63,0,63,0,0,TILE_DRAWMASKED);
	tile_define(1,0,63,0,63,0,0,TILE_DRAWMASKED);
	tile_define(1,0,63,0,63,0,0,TILE_DRAWMASKED);
	tile_define(1,0,63,0,63,0,0,TILE_DRAWMASKED);
	tile_define(1,0,63,0,63,0,0,TILE_DRAWMASKED);
	tile_define(1,0,63,0,63,0,0,TILE_DRAWMASKED);
	tile_define(1,0,63,0,63,0,0,TILE_DRAWMASKED);
	tile_define(1,0,63,0,63,0,0,TILE_DRAWMASKED);
	tile_define(1,0,63,0,63,0,0,TILE_DRAWMASKED);
	tile_define(1,0,63,0,63,0,0,TILE_DRAWMASKED);
	tile_define(1,0,63,0,63,0,0,TILE_DRAWMASKED);
	tile_define(1,0,63,0,63,0,0,TILE_DRAWMASKED);
	tile_define(1,0,63,0,63,0,0,TILE_DRAWMASKED);
	tile_define(1,0,63,0,63,0,0,TILE_DRAWMASKED);
	tile_define(1,0,63,0,63,0,0,TILE_DRAWMASKED);
	tile_define(1,0,63,0,63,0,0,TILE_DRAWMASKED);
	tile_define(1,0,63,0,63,0,0,TILE_DRAWMASKED);
	tile_define(1,0,63,0,63,0,0,TILE_DRAWMASKED);
	tile_define(1,0,63,0,63,0,0,TILE_DRAWMASKED);



	game_printf("Done!\n\nInitializing gameplay...                                    ");

	curzone.watery = -1;

	obj_spawn(OT_PLAYER,200,760);
	obj_spawn(OT_POLYTEST,800,1000);
	obj_spawn(OT_CRATE,320,1080);

	grfx_camthink();

	game_printf("Done!\n\nEntering main loop.\n");

}

void game_unprepare(void)
{
	int i = 0;
	tile_t* tile;
	obj_t* object;

	game_printf("\n\n___________________\n\nUnloading Zone...");

	game_printf("\n  Erasing layers...                                         ");

	curlayer = (void *)curzone.firstlayer;

	free(curlayer->tilerow);
	free(curlayer->tilemap);
	free(curlayer);

	game_printf("\n  Closing datafiles...                                      ");

	unload_datafile(r_world);
	unload_datafile(r_actor);
	unload_datafile(r_audio);

	game_printf("Done!\n  Resetting world...                                        ");

	for (i = 0;  i < MAXTILES; i++)
	{
		tile = &tilelist[i];
		memset(tile, 0, sizeof(tile_t));
	}

	for (i = 0;  i < MAXOBJS; i++)
	{
		object = &objectlist[i];
		obj_kill(object);
	}

	//if (zone)
	//unload_datafile(zone);

	game_printf("Done!\n\nZone unloaded.");
}

