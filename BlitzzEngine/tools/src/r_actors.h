/* Allegro datafile object indexes, produced by grabber v4.2.1, MinGW32 */
/* Datafile: g:\Work\C\BlitzzEngine\data\actors.dat */
/* Date: Sat Mar 15 19:56:07 2008 */
/* Do not hand edit! */

#define R_ACT_ACTORPAL                   0        /* PAL  */
#define R_ACT_CRATE                      1        /* BMP  */
#define R_ACT_PARTICLE                   2        /* RLE  */
#define R_ACT_PLAYER                     3        /* BMP  */
#define R_ACT_POLYTEST                   4        /* BMP  */
#define R_ACT_COUNT                      5

