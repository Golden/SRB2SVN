//functions
void grfx_updatesprites(BITMAP *buffer, int scrollx, int scrolly);
int grfx_camthink(void);
void grfx_refresh(void);
void grfx_renderworld(BITMAP *buffer, int scrollx, int scrolly);

// effects.c functions

extern int scrollx;
extern int scrolly;
extern char grfx_noedgescroll;
