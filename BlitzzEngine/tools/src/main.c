// Monkeywrench main source file
// By FoxBlitzz
//
// Outlines the bare skeleton of the editor. Unlike BlitzzEngine, the structure
// of the program is different, and as such, the program is run from various
// dialogs. To see what's going on, you should follow the editor-specific
// source files. Taking a dialog-based approach allows for smaller CPU usage
// when the program is inactive.


#include <stdio.h>
#include <allegro.h>
#ifdef DUMPRPT
#include <winalleg.h>
#endif

#include "gamesys.h"
#include "object.h"
#include "config.h"
#include "coresound.h"
#include "coregfx.h"
#include "coreproc.h"
#include "editor.h"

#include "resources.h"
#include "r_ui.h"
#include "r_editor.h"

volatile int game_end=0;
volatile int game_nextzone=0;
int game_showwarning=1;
int game_gfxwidth=640;
int game_gfxheight=480;
int game_gfxscale=0;
int game_gfxdepth=0;
int game_gfxdriver;
int game_gfxupdate=0;
int game_sounddriver=DIGI_AUTODETECT;

int main()
{
	#ifdef DUMPRPT
	LoadLibraryA("exchndl.dll");
	#endif


	game_initsystem();

	//do
	//{
		game_nextzone = 0;

		//Load gameplay
		game_prepare(NULL);

		sdfx_play(r_editor[R_EDIT_STARTUP].dat, 0, 255, 128, 1000, 0);

		do_dialog(gui_zoneedit,-1);

		game_printf("\nGame Flow: Traveling to Zone %i...\n",game_nextzone);

		game_unprepare();

	//} while (game_end == 0);

	game_killsystem();
	return 0;
}

END_OF_MAIN()
