#include <allegro.h>

#define SP_NORMAL 0
#define SP_INTERRUPT 1
#define SP_PLAYONCE 2

//functions
int sdfx_play(SAMPLE *sound, int mode, int volume, int pan, int freq, int loop);
void sdfx_poll(void);
void sdfx_stop(SAMPLE *sound);
int sdfx_count(void);
void sdfx_loadsong(const char *song);
int sdfx_checkfree(SAMPLE *sound);
void sdfx_fadesong(int time);

