#include <allegro.h>

#include "gamesys.h"
#include "coregfx.h"

void grfx_refresh(void)
{
	acquire_screen();
	//Blits finished image to screen. Rather slow but the speed
	//of memory blitting beforehand makes up for the performance loss

	blit(buffer,screen,0,0,(SCREEN_W-GAMEWIDTH)/2,(SCREEN_H-GAMEHEIGHT)/2,GAMEWIDTH,GAMEHEIGHT);
	release_screen();
}
