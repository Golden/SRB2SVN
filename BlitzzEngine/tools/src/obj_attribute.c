#include "object.h"
#include "gamesys.h"

#include "r_actors.h"

// Set an object's attributes based on its type;
void obj_setattributes(obj_t* object)
{
	// Set some basic variables for all objects here, then do some of the more custom stuff in the switch case.
	object->facedir = 1;
	object->drawstyle = DS_SPRITE;
	object->colora = 0;

	switch(object->type)
	{
	case OT_PLAYER:
		// Probably not the final stats, but lets just put something here.
		object->sprite=R_ACT_PLAYER;
		object->facedir = 1;
		object->height = 60;
		object->heightoffset = 2;
		object->width = 30;
		break;

	case OT_PARTICLE:
		object->sprite=R_ACT_PARTICLE;
		object->drawstyle=DS_RLE;
		object->height = 4;
		object->width = 4;
		object->colorr = 0;
		object->colorg = 200;
		object->colorb = 200;
		object->colora = 192;
		break;
	case OT_WATERPARTICLE:
		object->sprite=R_ACT_PARTICLE;
		object->drawstyle=DS_RLE;
		object->height = 4;
		object->width = 4;
		object->colorr = 200;
		object->colorg = 200;
		object->colorb = 200;
		object->colora = 192;
		break;

	case OT_CRATE:
		object->sprite=R_ACT_CRATE;
		object->height = 64;
		object->width = 60;
		break;
	case OT_POLYTEST:
		object->sprite=R_ACT_POLYTEST;
		object->drawstyle=DS_3DQUAD;
		object->height = 256;
		object->width = 256;
		object->colorr = 255;
		break;
	default:

		break;
	}
}
