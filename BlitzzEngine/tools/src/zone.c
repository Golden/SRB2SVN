// Tile handling functions
// By FoxBlitzz
// These functions give each tile their own unique behavior and allow colliding
// objects to respond appropriately. Their method of storage is not dissimilar
// to that of moving objects.

#include "zone.h"
#include "gamesys.h"
#include "object.h"
#include "editor.h"

#include "resources.h"
#include "r_world.h"

tile_t tilelist[MAXTILES];

layer_t *curlayer;

// Tilenum used so we don't need to manually insert a number for the array.
int tilenum = 0; // Reset to 0 at level load !!

void tile_define(char solid, unsigned char top, unsigned char bottom, unsigned char left, unsigned char right, unsigned char type, char slope, char visibility)
{
	tile_t* tile;

	if(tilenum == MAXTILES)
	{
		game_printf("WARNING: Can't define more than %d tiles!",MAXTILES+1);
		return;
	}

	tile = &tilelist[tilenum];
	memset(tile, 0, sizeof(tile_t));

	tile->solid = solid;
	tile->coltop = top;
	tile->colbottom = bottom;
	tile->colleft = left;
	tile->colright = right;
	tile->type = type;
	tile->slope = slope;
	tile->visibility = visibility;
	tile->croptop = 0;
	tile->cropbottom = TILESIZE-1;
	tile->cropleft = 0;
	tile->cropright = TILESIZE-1;

	tilenum++;
}

int zone_getobjselection(obj_t* obj, int mode)
{
	// Reverse the selection direction if it's backwards
	// (so that rect collision works correctly)
	int x1, x2, y1, y2;

	if (mode >= CM_DEADCLICK)
		return 0;

	if (edit_selectx1 <= edit_selectx2)
	{
		x1 = edit_selectx1;
		x2 = edit_selectx2;
	}
	else
	{
		x2 = edit_selectx1;
		x1 = edit_selectx2;
	}

	if (edit_selecty1 <= edit_selecty2)
	{
		y1 = edit_selecty1;
		y2 = edit_selecty2;
	}
	else
	{
		y2 = edit_selecty1;
		y1 = edit_selecty2;
	}

	// Detect four edges to see if the rectangle overlaps

	if (mode != CM_RECTANGLE)
	{
		if (obj->y-(obj->height/2)+obj->heightoffset>y2) {return 0;}
		if (obj->y+(obj->height/2)+obj->heightoffset-1<y1) {return 0;}
		if (obj->x-(obj->width/2)>x2) {return 0;}
		if (obj->x+(obj->width/2)-1<x1) {return 0;}
	}
	// Alternate box detection for selection rectangles
	else
	{
		if (obj->y-(obj->height/4)+obj->heightoffset>y2) {return 0;}
		if (obj->y+(obj->height/4)+obj->heightoffset-1<y1) {return 0;}
		if (obj->x-(obj->width/4)>x2) {return 0;}
		if (obj->x+(obj->width/4)-1<x1) {return 0;}
	}

	return 1;
}

