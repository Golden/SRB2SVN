#include <allegro.h>

#define GAMEWIDTH game_gfxwidth
#define GAMEHEIGHT game_gfxheight

#define GAMENAME FILE_DESCRIPTION " " VER_STRING
#define GAMENAMELONG FILE_DESCRIPTION " " VER_STRING " by " COMPANY_NAME

#define TILESIZE 64

#define GAME_HIDEWARNING 0
#define GAME_SHOWWARNING 1

#define GRFX_CENTER 0
#define GRFX_RESIZE 1
#define GRFX_TEMPORAL 2

#define GRFX_DBUFFER 0
#define GRFX_PAGEFLIP 1
#define GRFX_TBUFFER 2

#define GRFX_NOLIMITFPS 0
#define GRFX_LIMITFPS 1

#define GRFX_NOVSYNC 0
#define GRFX_VSYNC 1

#define GRFX_8BITAPRECISION 16

#define UI_DEBUGWORLD 1

//Game system
extern volatile int game_end;
extern volatile int game_nextzone;

extern int game_showwarning;
extern int game_gfxwidth;
extern int game_gfxheight;
extern int game_gfxscale;
extern int game_gfxdepth;
extern int game_gfxdriver;
extern const char *getgfxdriver;
extern int game_sounddriver;
extern const char *getsounddriver;
#ifdef ALLEGRO_WINDOWS
extern int getsoundcard;
#endif

extern const char *game_datapath;

//Game timer

extern volatile int game_tic;

//Graphics
extern BITMAP *buffer;

extern COLOR_MAP transtable[GRFX_8BITAPRECISION];


extern unsigned char uiflags;

//Sound


//Stuff...

void game_showsucc(void);
void game_showfail(void);
void game_showwarn(const char* format, ...);
int game_showerror(const char* format, ...);
void game_printf(const char *format, ...);

void game_initsystem(void);
void game_prepare(const char *target);
void game_unprepare(void);
void game_killsystem(void);
int game_setmode(int depth);
void game_render(void);

DATAFILE *game_loaddata(const char *datafile);
