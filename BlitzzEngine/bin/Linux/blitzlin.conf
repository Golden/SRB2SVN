# BlitzzEngine Configuration File (Linux X11 Version)
# -------------------------------------------------
#
# This file contains all settings used by BlitzzEngine, whether they be graphic
# modes or sound settings or control bindings.
#
# Before you attempt to change any settings, please read the comments on each 
# item. They will tell you what you need to know so that you have some idea of
# what you're doing.

[engine]
# Settings pertaining to the core game engine

# Datapath: The directory path to the game's data. When moving files around -
# for example, in a portable fashion - you will want to change this to point
# to the new data location.

datapath=../../data/

# Warnings: When you configure the game in a way that may be problematic but
# can possibly continue normally, the game will warn you about the settings you
# have chosen. If you choose not to be informed, set this value to "no".
#
# Possible values are: yes, no
#
#   Default mode is "yes".

show_warnings=yes

[graphics]
# Settings pertaining to the graphics display

# Screen Resolution: The game runs in a fixed 640x480 viewport. By default,
# the screen resolution is the same size as this viewport. You can change
# the resolution if you are having problems, but the viewport will not expand.
# The stretch mode determines whether to scale the viewport image to fit the 
# screen or simply center it.
#
# Values for width and height can be any combination supported by the monitor
# and video hardware. Values for stretch mode can be:
#
# center   - Does not stretch the image in any way.
# resize   - Does a fast pixel resize to scale the image up or down.
# temporal - Like resize, except the coefficients are toggled every other
#            frame in attempt to improve image quality at no performance loss.
#            Limited FPS and V-Sync are recommended for proper effect.
#
# Defaults are same size as viewport and centered.
#
# NOTE: These settings cannot be used under pageflip or triple-buffering!

width=640
height=480
stretch=center

# Color Depth: The game engine is designed to operate best at 16-bit color.
# If you encounter issues, you may try setting a different color depth. 8-bit
# looks the worst, 16-bit looks good yet runs fast, and 32-bit has the best
# precision but is very slow. 15-bit and 24-bit can also be used, but on new
# machines these may not be available in full-screen. A value of 0 denotes
# auto-detect.
#
# Possible values are: 0, 8, 15, 16, 24, 32
#
#   Default mode is 0, but values other than those listed will produce errors!

depth=24

# Graphics Driver: To update the display, the game can make use of several
# different drivers. By default, the game will attempt to auto-detect which
# driver should be used on your current machine. If for some reason you have
# problems, you may manually select a different driver here.
#
# Possible values are:
#
# autofull - Attempts to find the best full-screen mode. It will not fallback
#            to windowed, so it will fail if full-screen cannot be used.
# autowin  - Attempts to find the best windowed mode. It will not fallback to
#            full-screen, so it will fail if windowed cannot be used.
# xwindow  - Reliable windowed driver using X11, and should work on just about
#            every machine. The driver does not require root permissions.
# xwinfull - Reliable full-screen driver using X11. It will only run as fast
#            as the windowed drivers. The driver does not require root
#            permissions.
# dgaaccel - Uses the DGA 2.0 extension to provide faster full-screen support
#            with hardware acceleration. It requires root permissions and will 
#            fail if not installed on the current system.
# dgasoft  - Uses the DGA 2.0 extension in software mode to provide full-screen
#            support without hardware acceleration. Requires root permissions
#            and that DGA 2.0 is installed.
#
#   Any other value denotes the default behavior - auto-detect.

driver=autowin

# Update Mode: There are three ways to update the image on the display: double-
# buffering, page-flipping, and triple-buffering. If you need to, you can
# choose which update method to use here.
#
# Possible values are:
#
# dbuffer  - Draws all graphics on a single memory bitmap and then copies
#            the finished image to the screen.
# pageflip - Requires two video bitmaps. Graphics are drawn on one bitmap, and
#            that bitmap is shown on the screen. Then the next frame is drawn
#            on the other bitmap and the screen switches to that second image.
# tbuffer  - Like pageflip, but uses three video bitmaps. This allows the
#            screen update process to be more resourceful by drawing the next
#            frame while waiting to send the image to the screen, resulting in
#            faster performance. This option will not work on all platforms.
#
#   Default mode is double-buffering.

update=dbuffer

# Limit FPS: When enabled, the display will only update itself as often as 60
# frames-per-second, the same rate as the gameplay. This will provide a
# smoother display, especially for the page-flipping mode. Turn it off and the
# display will ignore this ceiling, updating as often as it can. Typically you
# only need to do this if you are doing benchmarking.
#
# Possible values are: yes, no
#
#   Default mode is "yes".

limit_fps=no

# V-Sync: The double-buffering mode typically updates at whatever chance it
# will get. This lack of synchronization results in a page tear partway down
# the screen and often times will look jittery. Enabling this value will make
# the update process wait until the vertical blank is about to go down the
# screen and then draws the image, resulting in a smoother and tear-free
# display.
#
# Possible values are: yes, no
#
#   Default mode is "no".

vsync=no

[sound]
# Settings pertaining to the audio system

# Sound System: Different machines have different configurations for how they
# play audio. Here you may choose which sound system you would prefer to use.
# The wrong driver might create errors or have poor playback quality.
#
# Possible values are:
#
# none     - Does not play any audio.
# openss   - Uses the Open Sound System to play audio.
# esdaemon - Uses the Enlightened Sound Daemon.
# arts     - Uses the aRts (Analog Real-Time Synthesizer)
# alsa     - Uses the ALSA sound system to play audio.
# jack     - Uses the JACK sound system to play audio.
#
#   Any other value denotes the default behavior - auto-detect.

driver=auto

