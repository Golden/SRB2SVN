# Microsoft Developer Studio Project File - Name="BlitzzEngine" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 6.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Application" 0x0101

CFG=BlitzzEngine - Win32 Debug
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "BlitzzEngine.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "BlitzzEngine.mak" CFG="BlitzzEngine - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "BlitzzEngine - Win32 Release" (based on "Win32 (x86) Application")
!MESSAGE "BlitzzEngine - Win32 Debug" (based on "Win32 (x86) Application")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 0
# PROP Scc_ProjName ""
# PROP Scc_LocalPath ""
CPP=cl.exe
MTL=midl.exe
RSC=rc.exe

!IF  "$(CFG)" == "BlitzzEngine - Win32 Release"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "Release"
# PROP Intermediate_Dir "Release"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_MBCS" /YX /FD /c
# ADD CPP /nologo /MD /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_MBCS" /FR /YX /FD /c
# ADD BASE MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x409 /d "NDEBUG"
# ADD RSC /l 0x409 /d "NDEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:windows /machine:I386
# ADD LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib alleg.lib libalogg.a /nologo /subsystem:windows /profile /machine:I386 /out:"C:\Documents and Settings\Matt\Desktop\BlitzzEngine\bin\Mingw\Blitzz.exe"

!ELSEIF  "$(CFG)" == "BlitzzEngine - Win32 Debug"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Debug"
# PROP BASE Intermediate_Dir "Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "Debug"
# PROP Intermediate_Dir "Debug"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_MBCS" /YX /FD /GZ /c
# ADD CPP /nologo /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_MBCS" /FR /YX /FD /GZ /c
# ADD BASE MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x409 /d "_DEBUG"
# ADD RSC /l 0x409 /d "_DEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:windows /debug /machine:I386 /pdbtype:sept
# ADD LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib alld.lib libalogg.a /nologo /subsystem:windows /debug /machine:I386 /out:"C:\Documents and Settings\Matt\Desktop\BlitzzEngine\bin\Mingw\BlitzzDebug.exe" /pdbtype:sept

!ENDIF 

# Begin Target

# Name "BlitzzEngine - Win32 Release"
# Name "BlitzzEngine - Win32 Debug"
# Begin Group "1_System"

# PROP Default_Filter "cpp;c;cxx;rc;def;r;odl;idl;hpj;bat"
# Begin Source File

SOURCE=.\src\config.c
# End Source File
# Begin Source File

SOURCE=.\src\config.h
# End Source File
# Begin Source File

SOURCE=.\src\gamesys.c
# End Source File
# Begin Source File

SOURCE=.\src\gamesys.h
# End Source File
# Begin Source File

SOURCE=.\src\main.c
# End Source File
# End Group
# Begin Group "2_Core"

# PROP Default_Filter "h;hpp;hxx;hm;inl"
# Begin Source File

SOURCE=.\src\coregfx.c
# End Source File
# Begin Source File

SOURCE=.\src\coregfx.h
# End Source File
# Begin Source File

SOURCE=.\src\coreinput.c
# End Source File
# Begin Source File

SOURCE=.\src\coreinput.h
# End Source File
# Begin Source File

SOURCE=.\src\coreproc.c
# End Source File
# Begin Source File

SOURCE=.\src\coreproc.h
# End Source File
# Begin Source File

SOURCE=.\src\coresound.c
# End Source File
# Begin Source File

SOURCE=.\src\coresound.h
# End Source File
# Begin Source File

SOURCE=.\src\gfxupdate.c
# End Source File
# End Group
# Begin Group "3_Process"

# PROP Default_Filter ""
# Begin Source File

SOURCE=.\src\collide.c
# End Source File
# Begin Source File

SOURCE=.\src\console.c
# End Source File
# Begin Source File

SOURCE=.\src\console.h
# End Source File
# Begin Source File

SOURCE=.\src\effects.c
# End Source File
# Begin Source File

SOURCE=.\src\menu.c
# End Source File
# Begin Source File

SOURCE=.\src\menu.h
# End Source File
# Begin Source File

SOURCE=.\src\obj_attribute.c
# End Source File
# Begin Source File

SOURCE=.\src\object.c
# End Source File
# Begin Source File

SOURCE=.\src\object.h
# End Source File
# Begin Source File

SOURCE=.\src\prepare.c
# End Source File
# Begin Source File

SOURCE=.\src\zone.c
# End Source File
# Begin Source File

SOURCE=.\src\zone.h
# End Source File
# End Group
# Begin Group "Resource Files"

# PROP Default_Filter "ico;cur;bmp;dlg;rc2;rct;bin;rgs;gif;jpg;jpeg;jpe"
# Begin Source File

SOURCE=.\src\r_actors.h
# End Source File
# Begin Source File

SOURCE=.\src\r_audio.h
# End Source File
# Begin Source File

SOURCE=.\src\r_core.h
# End Source File
# Begin Source File

SOURCE=.\src\r_ui.h
# End Source File
# Begin Source File

SOURCE=.\src\r_world.h
# End Source File
# Begin Source File

SOURCE=.\src\resources.h
# End Source File
# End Group
# Begin Source File

SOURCE=.\todo.h
# End Source File
# End Target
# End Project
