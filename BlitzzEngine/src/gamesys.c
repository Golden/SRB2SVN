// BlitzzEngine start-up routines
// By FoxBlitzz
// Basic start-up code that writes lots of fun stuffs to a log file while it
// initiates all of Allegro's little subsystems. It also reads from blitzz.ini
// here so that it knows what options to start the graphics system with.

#include <stdio.h>
#include <time.h>
#include <string.h>

#include <allegro.h>

#ifdef BUGTRAP
#include <winalleg.h>
#include "Win32/BugTrap.h"
#include "Win32/BugTrap_FnTypes.h"
#endif

#include "../BlitzzEngine_private.h"
#include "config.h"
#include "gamesys.h"
#include "console.h"
#include "resources.h"
#include "r_core.h"

#define GAMENAME FILE_DESCRIPTION " " VER_STRING
#define GAMENAMELONG FILE_DESCRIPTION " " VER_STRING " by " COMPANY_NAME
#define GAMEFRAMERATE 60

FILE *logfile;

//Initiate counter variables
volatile char game_ticskip=0;
volatile int game_tics=0;
volatile int game_runtics=0;
volatile int game_fps=0;
volatile int game_curfps=0;
volatile int game_lastfps=0;
volatile int game_ms=0;
unsigned char game_restcpu=0;
int game_timerender=0;
int game_timeupdate=0;
int game_timelogic=0;
int game_timerest=0;

char game_taskswitch=0;

//User interfacing
unsigned char uiflags = UI_GAMEWORLD + UI_DEBUG;

// Prints line to console/log

void game_printf(const char *format, ...)
{
	char textbuffer[512];
	va_list ap;

	va_start(ap, format);
	uvszprintf(textbuffer, sizeof(textbuffer), format, ap);
	va_end(ap);

	cons_addline(textbuffer);
	fprintf(logfile, textbuffer);
	printf(textbuffer);
	fflush(logfile);
}

// Timer interrupts

void inc_game_timer(void)
{
	game_ticskip++;
	game_tics++;

	if (game_run > 0)
		game_runtics++;
}

END_OF_FUNCTION(inc_game_timer)

void inc_game_fps(void)
{
	game_fps++;
}

END_OF_FUNCTION(inc_game_fps)

void inc_game_ms(void)
{
	game_ms++;
}

END_OF_FUNCTION(inc_game_ms)

static void sys_close_handle(void)
{
	game_end=1;
}

END_OF_FUNCTION(sys_close_handle)

static void sys_switch_in(void)
{
	game_restcpu=0;
	game_taskswitch=0;
	game_printf("\nSwitched into program...");
}

static void sys_switch_out(void)
{
	game_restcpu=10;
	game_taskswitch=1;
	game_printf("\nSwitched out of program...");
}

#ifndef I_HEART_SDL_FOREVER_ECKS_THREE
static const char* sdl_sux(void)
{
	char messageid;

	srand(time(NULL));

	messageid = rand()%10;

	switch (messageid)
	{
		case 0:
			return "Because SDL is slower than molasses going uphill in January!";
			break;
		case 1:
			return "Up to 400% faster than SDL out of the box!";
			break;
		case 2:
			return "Go tell your little SDL friends that SuperTux\ncan't draw fast graphics to save its life!";
			break;
		case 3:
			return "SDL's unstable too, you know! Like, ever notice\nwindows closing themselves? Not resizing properly?\nSDL_app displayed in the titlebar?\nOccasional serious graphic glitch on some games?\nI rest my case.";
			break;
		case 4:
			return "SDL should stand for Slow Dumbed-down Layer!";
			break;
		case 5:
			return "You can port SDL to a thousand platforms but that doesn't\nmake it stop sucking at graphic performance.";
			break;
		case 6:
			return "If I used SDL, I'd look like an open-source\ntree-hugger, and that's no good!";
			break;
		case 7:
			return "I need to stop attacking SDL.";
			break;
		case 8:
			return "One of these days my vindictiveness against SDL\nwill back-fire.";
			break;
		case 9:
			return "We need SDLAllegro. Super speed _and_ can be\nported to Obscure Platform #495!";
			break;
	}
	return "";
}
#endif

#ifdef BUGTRAP

static HMODULE bugt_bugtrapdll;

static void bugt_installcrasher(void)
{
	//BT_SETSUPPORTEMAIL lpfnBT_SetSupportEMail;
	BT_SETFLAGS lpfnBT_SetFlags;
	BT_SETSUPPORTURL lpfnBT_SetSupportURL;
	//BT_READVERSIONINFO lpfnBT_ReadVersionInfo;
	BT_SETAPPNAME lpfnBT_SetAppName;
	BT_SETAPPVERSION lpfnBT_SetAppVersion;
	BT_SETDIALOGMESSAGE lpfnBT_SetDialogMessage;

	bugt_bugtrapdll = LoadLibrary("BugTrap.dll");

	//lpfnBT_SetSupportEMail = (BT_SETSUPPORTEMAIL)GetProcAddress(bugt_bugtrapdll, "BT_SetSupportEMail");
	lpfnBT_SetFlags = (BT_SETFLAGS)GetProcAddress(bugt_bugtrapdll, "BT_SetFlags");
	lpfnBT_SetSupportURL = (BT_SETSUPPORTURL)GetProcAddress(bugt_bugtrapdll, "BT_SetSupportURL");
	//lpfnBT_ReadVersionInfo = (BT_READVERSIONINFO)GetProcAddress(bugt_bugtrapdll, "BT_ReadVersionInfo");
	lpfnBT_SetAppName = (BT_SETAPPNAME)GetProcAddress(bugt_bugtrapdll, "BT_SetAppName");
	lpfnBT_SetAppVersion = (BT_SETAPPVERSION)GetProcAddress(bugt_bugtrapdll, "BT_SetAppVersion");
	lpfnBT_SetDialogMessage = (BT_SETDIALOGMESSAGE)GetProcAddress(bugt_bugtrapdll, "BT_SetDialogMessage");

	if(bugt_bugtrapdll)
	{
		lpfnBT_SetAppName(GAMENAME);
		lpfnBT_SetAppVersion(GAMENAMELONG);
		//lpfnBT_SetSupportEMail("danhagerstrand@gmail.com");
		lpfnBT_SetFlags(BTF_EDITMAIL | BTF_ATTACHREPORT);
		lpfnBT_SetSupportURL("http://chaos.foxmage.com/blitzzkrieg");
		lpfnBT_SetDialogMessage(BTDM_INTRO1, "BlitzzEngine has encountered an error and needs to close.");
		lpfnBT_SetDialogMessage(BTDM_INTRO2, "It is likely that BlitzzEngine tried to use a resource that "
											"hasn't been loaded yet, or an internal function performed erratically. "
											"By sending an error report, you can help us fix bugs. Please include "
											"a detailed explanation on what you were doing before the crash.");
		//lpfnBT_ReadVersionInfo(NULL);
	}
}

#endif

void game_initsystem(void)
{
	//Initiate time variables
	struct tm *local;
	time_t t;
	int i;

	t = time(NULL);

	local = localtime(&t);

	logfile = fopen("log.txt", "wt");

	#ifdef BUGTRAP
	bugt_installcrasher();
	#endif

	//Initiate Allegro!
	game_printf("%s\n\nLog time: %s",GAMENAMELONG,asctime(local));
	#ifndef I_HEART_SDL_FOREVER_ECKS_THREE
	game_printf("\n\nThis game is proudly\npowered by Allegro 4.2.2!\n\n%s",sdl_sux());
	#endif
	game_printf("\n\n___________________\n\nAttempting to boot BlitzzEngine:\n\nInitializing Allegro system...                              ");

	if (allegro_init()==0)
		game_showsucc();
	else
		game_showerror("Could not start the Allegro system!");

	//Lock close-button function
	LOCK_FUNCTION(sys_close_handle);
	set_close_button_callback(sys_close_handle);

	game_printf("Grabbing config from blitzz.ini...                          ");
	game_getconfig();
	game_printf("Done!\n");

	//Initiate graphics
	game_printf("Opening video device...\n  Requested driver setting: %s (%dx%dx%d)\n  Attempting to find a mode...                              ",getgfxdriver, game_gfxwidth, game_gfxheight, game_gfxdepth);
	set_window_title(GAMENAME);
	if ((game_gfxdepth==0 && (game_setmode(16) || game_setmode(15) || game_setmode(32) || game_setmode(24))) || game_setmode(game_gfxdepth))
		game_printf("Success!\n    Using device: %s (%dx%dx%d)\n",gfx_driver->name,SCREEN_W,SCREEN_H,bitmap_color_depth(screen));
	else
		game_showerror("Could not find a video mode!");

	textprintf_centre_ex(screen, font, SCREEN_W/2, SCREEN_H-32, makecol(255, 255, 255), -1, "%s - Loading interface...",GAMENAMELONG);

	//Because people LOVE to bitch about Allegro programs freezing and crashing
	// on them, I'm going to switch the task mode so that it runs in the
	// background. Maybe that'll shut them up. ;P
	game_printf("Switching to background task mode...                        ");
	if (set_display_switch_mode(SWITCH_BACKGROUND)==0 || set_display_switch_mode(SWITCH_BACKAMNESIA)==0)
		game_showsucc();
	else
		game_showfail(); //Non-fatal error. Continue anyway.

	//Initiate keyboard
	game_printf("Opening keyboard device...                                  ");
	if (install_keyboard()==0)
		game_showsucc();
	else
		game_showerror("Could not install keyboard!");

	//Initiate timers
	game_printf("Installing timer...                                         ");
	if (install_timer()==0)
		game_showsucc();
	else
		game_showerror("Could not install timer!");

	//Lock timer variables and functions so Allegro can control them
	LOCK_VARIABLE(game_tics);
	LOCK_VARIABLE(game_runtics);
	LOCK_VARIABLE(game_ticskip);
	LOCK_FUNCTION(inc_game_timer);
	LOCK_VARIABLE(game_fps);
	LOCK_FUNCTION(inc_game_fps);
	LOCK_VARIABLE(game_ms);
	LOCK_FUNCTION(inc_game_ms);
	//Install game pacing timer and FPS counter timer
	install_int_ex(inc_game_timer, BPS_TO_TIMER(GAMEFRAMERATE));
	install_int(inc_game_fps, 1000);
	install_int(inc_game_ms, 1);

	if (game_gfxlimit==GRFX_LIMITFPS)
		game_printf("  FPS will be capped at 60hz.\n");
	else
		game_printf("  FPS will be uncapped.\n");

	#ifdef ALLEGRO_WINDOWS
	//GDI does not support video bitmaps. Catch this mistake here.
	if (strcmp(gfx_driver->name,"GDI")==0 && game_gfxupdate>=GRFX_PAGEFLIP)
	{
		game_showwarn("GDI does not support video pages! The game will fallback to double-buffering.");
		game_printf("Video paging unsupported in GDI! Falling back...\n");
		game_gfxupdate=GRFX_DBUFFER;
	}

	//Overlay seems to only work by pageflip mode on this machine. I guess it makes sense.
	if (strcmp(gfx_driver->name,"DirectDraw overlay")==0 && game_gfxupdate==GRFX_DBUFFER)
	{
		game_showwarn("You have requested DirectDraw Overlay mode with double-buffering.\nOn some machines, this may not work properly.\n\nThe game will start normally.\nIf you notice problems with the display, use page-flipping instead.");
	}
	#endif

	if (game_gfxupdate==GRFX_DBUFFER)
	{
		game_printf("Creating memory buffer bitmap...                            ");
		//Create double-buffer bitmap
		buffer = create_bitmap(GAMEWIDTH,GAMEHEIGHT);
		if (buffer)
			game_showsucc();
		else
			game_showerror("Could not create memory bitmap!");
	}

	if (game_gfxupdate==GRFX_PAGEFLIP)
	{
		game_printf("Creating two video pages...                                 ");
		//Create page-flip bitmaps
		page1 = create_video_bitmap(GAMEWIDTH,GAMEHEIGHT);
		page2 = create_video_bitmap(GAMEWIDTH,GAMEHEIGHT);
		if (page1 && page2)
			game_showsucc();
		else
			game_showerror("Could not create both video pages!");

		buffer=page2;
	}

	if (game_gfxupdate==GRFX_TBUFFER)
	{
		game_printf("Creating three video pages...                               ");
		//Create triple-buffer bitmaps
		page1 = create_video_bitmap(GAMEWIDTH,GAMEHEIGHT);
		page2 = create_video_bitmap(GAMEWIDTH,GAMEHEIGHT);
		page3 = create_video_bitmap(GAMEWIDTH,GAMEHEIGHT);
		if (page1 && page2 && page3)
			game_showsucc();
		else
			game_showerror("Could not create all three video pages!");

		buffer=page3;
	}

	game_printf("Creating miscellaneous bitmaps...                           ");
	//Create extra bitmaps for effects

	accumulate = create_bitmap(GAMEWIDTH,GAMEHEIGHT);
	snapshot = create_bitmap(GAMEWIDTH,GAMEHEIGHT);
	if (accumulate && snapshot)
		game_showsucc();
	else
		game_showerror("Could not create miscellaneous bitmaps!");

	//Color conversion
	set_color_conversion(COLORCONV_TOTAL + COLORCONV_KEEP_TRANS + COLORCONV_DITHER);

	game_printf("Loading core information...\n");
	r_core=game_loaddata("core.dat");

	if (bitmap_color_depth(screen) == 8)
	{
		static RGB_MAP rgbtable;
		game_printf("Performing palette calculations...                          ");
		set_palette(r_core[R_COR_COLORPAL].dat);
		select_palette(r_core[R_COR_COLORPAL].dat);
		create_rgb_table(&rgbtable, r_core[R_COR_COLORPAL].dat, NULL);
		rgb_map = &rgbtable;

		for (i = 0; i < GRFX_8BITAPRECISION; i++)
		{
			create_trans_table(&transtable[i], r_core[R_COR_COLORPAL].dat, 255*(i+1)/GRFX_8BITAPRECISION, 255*(i+1)/GRFX_8BITAPRECISION, 255*(i+1)/GRFX_8BITAPRECISION, NULL);
		}

		color_map = &transtable[GRFX_8BITAPRECISION/4];
		game_printf("Done!\n");
	}

	//Initiate sound
	#ifdef ALLEGRO_WINDOWS
	game_printf("Opening audio device...\n  Requested driver setting: %s (%d)\n  Attempting to load system...                              ",getsounddriver, getsoundcard);
	#else
	game_printf("Opening audio device...\n  Requested driver setting: %s\n  Attempting to load system...                              ",getsounddriver);
	#endif
	reserve_voices(16,0);
	if (install_sound(game_sounddriver,MIDI_NONE,NULL)==0)
	{
		game_printf("Success!\n    Using device: %s \n",digi_driver->name);
	}
	else
		game_showfail(); //Non-fatal error. Continue anyway.

	set_volume_per_voice(0);

	// Install task switching callbacks, mainly to reduce CPU when the window is inactive.
	set_display_switch_callback(SWITCH_IN, sys_switch_in);
	set_display_switch_callback(SWITCH_OUT, sys_switch_out);

	// Load global user interface media
	game_printf("Loading user interface media...\n");
	r_ui=game_loaddata("ui.dat");

	clear_to_color(screen,makecol(0,0,0));

	game_printf("\nGame successfully booted!");
}

void game_killsystem(void)
{
	game_printf("\n\n___________________\n\nAttempting to shutdown BlitzzEngine:\n\nDestroying bitmaps...                                       ");
	destroy_bitmap(page1);
	destroy_bitmap(page2);
	destroy_bitmap(page3);
	game_printf("Done!\nDeinitializing Allegro...                                   ");
	allegro_exit();
	game_printf("Done!\n\nClosing normally...\n");
}

int game_setmode(int depth) {
	set_color_depth(depth);
	if (game_gfxupdate == GRFX_DBUFFER)
		return set_gfx_mode(game_gfxdriver, game_gfxwidth, game_gfxheight, 0, 0) == 0;
	else
		return set_gfx_mode(game_gfxdriver, GAMEWIDTH, GAMEHEIGHT, 0, 0) == 0;
}

void game_showsucc(void)
{
	game_printf("Success!\n");
}

void game_showfail(void)
{
	game_printf("FAILURE!\n  Non-fatal error. Continuing anyway...\n");
}

void game_showwarn(const char* format, ...) {
	char textbuffer[512];
	va_list ap;

	va_start(ap, format);
	uvszprintf(textbuffer, sizeof(textbuffer), format, ap);
	va_end(ap);

	if (game_showwarning)
		allegro_message("%s",textbuffer);
}

int game_showerror(const char* format, ...) {
	char textbuffer[512];
	va_list ap;

	va_start(ap, format);
	uvszprintf(textbuffer, sizeof(textbuffer), format, ap);
	va_end(ap);

	game_printf("\n\nFatal error: %s \n%s \n\nThe game will now abort.",textbuffer,allegro_error);
	set_gfx_mode(GFX_TEXT, 0, 0, 0, 0);
	allegro_message("\n\nFatal error: %s \n%s \n\nThe game will now abort.",textbuffer,allegro_error);
	exit(EXIT_FAILURE);
	return 0;
}
