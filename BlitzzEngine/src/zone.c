// Tile handling functions
// By FoxBlitzz
// These functions give each tile their own unique behavior and allow colliding
// objects to respond appropriately. Their method of storage is not dissimilar
// to that of moving objects.

#include "zone.h"
#include "gamesys.h"
#include "object.h"

#include "resources.h"
#include "r_world.h"

#define BOXCENTERX ((left+right)/2)
#define BOXCENTERY ((top+bottom)/2)

#include "coregfx.h" //TEMPORARY

tile_t tilelist[MAXTILES];

// Tilenum used so we don't need to manually insert a number for the array.
int tilenum = 0; // Reset to 0 at level load !!

void tile_define(char solid, unsigned char top, unsigned char bottom, unsigned char left, unsigned char right, unsigned char type, char slope, char visibility)
{
	tile_t* tile;

	if(tilenum == MAXTILES)
	{
		game_printf("WARNING: Can't define more than %d tiles!",MAXTILES+1);
		return;
	}

	tile = &tilelist[tilenum];
	memset(tile, 0, sizeof(tile_t));

	tile->solid = solid;
	tile->coltop = top;
	tile->colbottom = bottom;
	tile->colleft = left;
	tile->colright = right;
	tile->type = type;
	tile->slope = slope;
	tile->visibility = visibility;
	tile->croptop = 0;
	tile->cropbottom = TILESIZE-1;
	tile->cropleft = 0;
	tile->cropright = TILESIZE-1;

	tilenum++;
}

// Tile collision - checks to see if the specified bounding box overlaps a tile.
//  If response is set to something other than 0, it will calculate a position
//  to snap the object to.
int tile_collide(int top, int bottom, int left, int right, char response)
{
	// In order to run efficiently, we need to only check the grid spaces that
	// overlap the specified bounding box. The number of tiles to check is
	// calculated using the positions of the bounding box, and then that many
	// tiles are considered while testing collisions with nearby tiles.
	int boxsizex=((right/UNIT-1)/TILESIZE)-(left/UNIT/TILESIZE)+1;
	int boxsizey=((bottom/UNIT-1)/TILESIZE)-(top/UNIT/TILESIZE)+1;
	// I think this part is horribly unoptimized. If you know of a better way
	// then please let me know. This just gets the start positions on the grid
	int startx=left/UNIT/TILESIZE;
	int starty=top/UNIT/TILESIZE;

	// Used for checking each tile for the collision.
	int checkx;
	int checky;

	// Stores the bounding box of each tile being checked, to see if it
	//  overlaps the one specified in the function call.
	int boxtop;
	int boxbottom;
	int boxleft;
	int boxright;

	// Offsets collision boundaries for slopes
	int midx;

	// Used for collision response.
	int newspot=0;
	int newspot2=0;

	int sizex=(right-left)/2.0;
	int sizey=(bottom-top)/2.0;

	tile_t* checktile;

	if (startx < 0) {startx = 0;}
	if (startx+boxsizex > ZONE_XSIZE) {startx = ZONE_XSIZE-boxsizex;}
	if (starty < 0) {starty = 0;}
	if (starty+boxsizey > ZONE_YSIZE) {starty = ZONE_YSIZE-boxsizey;}

	if (left < 0) {right = (right-left); left = 0;}
	if (top < 0) {bottom = (bottom-top); top = 0;}
	if (right > ZONE_XSIZE*TILESIZE*UNIT) {left = ZONE_XSIZE*TILESIZE*UNIT-(right-left); right = ZONE_XSIZE*TILESIZE*UNIT;}
	if (bottom > ZONE_YSIZE*TILESIZE*UNIT) {top = ZONE_YSIZE*TILESIZE*UNIT-(bottom-top); bottom = ZONE_YSIZE*TILESIZE*UNIT;}

	for (checky=starty; checky<starty+boxsizey; checky++)
	{
		for (checkx=startx; checkx<startx+boxsizex; checkx++)
		{
			// Grab the next tile
			checktile=&tilelist[zone[checkx][checky]];

			// Tile is intangible. Don't bother checking.
			if (!checktile->solid) {continue;}

			// Get the four sides of the current tile and convert to game world
			//  coordinates
			//Edge of tile        Offset
			boxtop=((int)checktile->coltop*UNIT)+(checky*UNIT*TILESIZE)+UNIT;
			boxbottom=((int)checktile->colbottom*UNIT)+(checky*UNIT*TILESIZE)+UNIT;
			boxleft=((int)checktile->colleft*UNIT)+(checkx*UNIT*TILESIZE)+UNIT;
			boxright=((int)checktile->colright*UNIT)+(checkx*UNIT*TILESIZE)+UNIT;

			// Check for slope
			midx = ((left-boxleft)+((right-left)/2.0));

			if (checktile->slope)
			{
				if ((response == RESPOND_RIGHT && (checktile->slope == UPLEFT || checktile->slope == DOWNLEFT)) || (response == RESPOND_LEFT && (checktile->slope == UPRIGHT || checktile->slope == DOWNRIGHT)))
					continue;
				if (response == RESPOND_UP && bottom<boxbottom && (checktile->slope == UPLEFT || checktile->slope == UPRIGHT))
					response = RESPOND_DOWN;
				if (response == RESPOND_DOWN && top>boxtop && (checktile->slope == DOWNLEFT || checktile->slope == DOWNRIGHT))
					response = RESPOND_UP;

				switch (checktile->slope)
				{
				case UPRIGHT:
					{
						if (midx<0)
							midx = 0;
						boxtop+= midx-1;
						break;
					}
				case UPLEFT:
					{
						if (midx>TILESIZE*UNIT)
							midx = TILESIZE*UNIT;
						boxtop+= (TILESIZE*UNIT)-1-midx;
						break;
					}
				case DOWNRIGHT:
					{
						if (midx<0)
							midx = 0;
						boxbottom+= -1-midx;
						break;
					}
				case DOWNLEFT:
					{
						if (midx>TILESIZE*UNIT)
							midx = TILESIZE*UNIT;
						boxbottom+= midx-1-(TILESIZE*UNIT);
						break;
					}
				}
			}

			// If the tile borders the lip of a slope, don't check.
			if (checkx+1<=ZONE_XSIZE-1)
			{
				checktile=&tilelist[zone[checkx+1][checky]];
				if ((checktile->slope == UPRIGHT || checktile->slope == DOWNRIGHT) && checkx+1<(startx+boxsizex))
					continue;
			}
			if (checkx-1>=0)
			{
				checktile=&tilelist[zone[checkx-1][checky]];
				if ((checktile->slope == UPLEFT || checktile->slope == DOWNLEFT) && checkx-1>=startx)
					continue;
			}
			checktile=&tilelist[zone[checkx][checky]];

			//TEMPORARY
			//clear_bitmap(buffer);
			//rect(buffer,(boxleft-scrollx)>>16,(boxtop-scrolly)>>16,(boxright-scrollx)>>16,(boxbottom-scrolly)>>16,makecol(255, 0, 0));

			// This checks to see if any side of the bounding box is on the
			//  other side of the tile box's opposing side. If none of these
			//  disproving factors are met, there is a collision at that
			//  position.
			if (boxtop>bottom) {continue;}
			if (boxbottom<top) {continue;}
			if (boxleft>right) {continue;}
			if (boxright<left) {continue;}

			// If it passed all the tests...
			switch (response)
			{
			case RESPOND_NONE:
				{
					// No response required. Just set to 1.
					newspot=1;
					break;
				}
				// For the next four cases, a variable is used to find the
				//  side furthest out for the response type specified. For
				//  example, if a response from moving downward is asked,
				//  it will find the highest top edge bounding box that found
				//  a collision. This method works very well in snapping the
				//  object to the wall.
			case RESPOND_DOWN:
				{
					if (!newspot || newspot>boxtop-1-sizey)
						newspot=boxtop-1-sizey;
					break;
				}
			case RESPOND_UP:
				{
					if (newspot<boxbottom+1+sizey)
						newspot=boxbottom+1+sizey;
					break;
				}
			case RESPOND_RIGHT:
				{
					if (!newspot || newspot>boxleft-1-sizex)
						newspot=boxleft-1-sizex;
					break;
				}
			case RESPOND_LEFT:
				{
					if (newspot<boxright+1+sizex)
						newspot=boxright+1+sizex;
					break;
				}
			case RESPOND_AUTOY:
				{
					if (!newspot || newspot>boxtop)
						newspot=boxtop;
					if (newspot2<boxbottom)
						newspot2=boxbottom;
					break;
				}
			case RESPOND_AUTOX:
				{
					if (!newspot || newspot>boxleft)
						newspot=boxleft;
					if (newspot2<boxright)
						newspot2=boxright;
					break;
				}
			}
		}
	}

	// If there is no collision, 0 is returned. If there is, the appropriate
	//  response is returned.

	// For the automatic x/y response, both the opposing response directions
	//  along that axis are found, and both are compared to the edge of the
	//  originating bounding box to see which is closer.

	// NOTE: When using AUTOX or AUTOY, the center of the new object's position
	//  is returned, not the side.

	if (newspot2 && response == RESPOND_AUTOY && abs(newspot2-top) < abs(newspot-bottom)) // Bottom
		newspot=newspot2+((bottom-top)/2)+1;
	else if (newspot && response == RESPOND_AUTOY && abs(newspot2-top) >= abs(newspot-bottom)) // Top
		newspot+=-((bottom-top)/2)-1;
	else if (newspot2 && response == RESPOND_AUTOX && abs(newspot2-left) < abs(newspot-right)) // Right
		newspot=newspot2+((right-left)/2)+1;
	else if (newspot && response == RESPOND_AUTOX && abs(newspot2-left) >= abs(newspot-right)) // Left
		newspot+=-((right-left)/2)-1;

	return newspot;
}

// Shove object out of tile.
//  This function's purpose is to move an object out of any tiles if it is
//  spawned inside one. Since the regular collision routine checks only for
//  velocity to determine which side of the tile to snap to, the new object
//  will appear on the other side of the wall if it is spawned inside the wall
//  with a velocity going away from said wall. Since this is incorrect, the
//  below function will automatically find the correct collision response based
//  on position rather than velocity. It gets the response for each axis being
//  tested and checks to see which one is the closest to the original position.
void tile_shoveobject(obj_t* object)
{
	int xyx=0; // X coordinate of response when checking X axis before Y
	int xyy=0; // Y coordinate of response when checking X axis before Y
	int yxx=0; // X coordinate of response when checking Y axis before X
	int yxy=0; // Y coordinate of response when checking Y axis before X

	int xyxexist=0;
	int yxyexist=0;

	int xydist; // Distance from original position when checking X axis before Y
	int yxdist; // Distance from original position when checking Y axis before X

	xyx = tile_collide(object->y-(object->height/2)+object->heightoffset,object->y+(object->height/2)+object->heightoffset,object->x-(object->width/2),object->x+(object->width/2),RESPOND_AUTOX);
	if (xyx) {xyxexist=1;}
	xyy = tile_collide(object->y-(object->height/2)+object->heightoffset,object->y+(object->height/2)+object->heightoffset,object->x-(object->width/2)+((xyx-object->x)*xyxexist),object->x+(object->width/2)+((xyx-object->x)*xyxexist),RESPOND_AUTOY);

	yxy = tile_collide(object->y-(object->height/2)+object->heightoffset,object->y+(object->height/2)+object->heightoffset,object->x-(object->width/2),object->x+(object->width/2),RESPOND_AUTOY);
	if (yxy) {yxyexist=1;}
	yxx = tile_collide(object->y-(object->height/2)+object->heightoffset+((yxy-(object->y+object->heightoffset))*yxyexist),object->y+(object->height/2)+object->heightoffset+((yxy-(object->y+object->heightoffset))*yxyexist),object->x-(object->width/2),object->x+(object->width/2),RESPOND_AUTOX);

	// If no collision is found, just return the original object position
	if (!xyx) {xyx=object->x;}
	if (!xyy) {xyy=object->y;} else {xyy+=-object->heightoffset;}
	if (!yxx) {yxx=object->x;}
	if (!yxy) {yxy=object->y;} else {yxy+=-object->heightoffset;}

	xydist = abs(object->x-xyx)+abs(object->y-xyy);
	yxdist = abs(object->x-yxx)+abs(object->y-yxy);

	if (xydist<yxdist) { object->x=xyx; object->y=xyy; }
	else { object->x=yxx; object->y=yxy; }
}


