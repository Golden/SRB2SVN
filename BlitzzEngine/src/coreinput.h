#define NUMKEYS 7

#define IK_KEYLEFT 0
#define IK_KEYRIGHT 1
#define IK_KEYUP 2
#define IK_KEYDOWN 3
#define IK_KEYJUMP 4
#define IK_KEYATCK 5
#define IK_KEYSPEC 6

//Controls
extern char input_game[NUMKEYS];
extern char input_menu;
extern char input_repeatpos;

//functions
void game_getinput(void);
