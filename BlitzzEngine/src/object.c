// Code by Shuffle
// This file should hold the majority of functions that work with or modify
// objects. Over time, obj_think and obj_setattributes will become more
// and more complex as we add specific thinkers and attributes to new objects.

#include <stdio.h>
#include <math.h>

#include "object.h"
#include "zone.h"

#include "gamesys.h"
#include "config.h"
#include "coreinput.h"
#include "coregfx.h"
#include "resources.h"
#include "coresound.h"

#include "r_audio.h"

obj_t* obj_player;

// This array keeps track of all of the objects currently in the game.
obj_t objectlist[MAXOBJS];

// Thrusts an object in the given direction
static inline void obj_thrust(obj_t* object, fixed angle, int power)
{
	object->momx = fixcos(angle)*(power/UNIT);
	object->momy = -fixsin(angle)*(power/UNIT);
}

// Move an object based on its weight and momentum.
//#ifdef SHUFCOLLIDE
//void obj_move(obj_t* object, int start)
//#endif
//#ifndef SHUFCOLLIDE
void obj_move(obj_t* object)
//#endif
{
	float movespeed;

	#ifdef SHUFCOLLIDE
	if(object->floorobj && (object->floorobj->momx || object->floorobj->extramomx))
		object->extramomx = object->floorobj->momx+object->floorobj->extramomx;
	else
		object->extramomx = 0;
	#endif

	// Player just landed in the water
	if (watery>-1 && object->type != OT_WATERPARTICLE && object->type != OT_PARTICLE && object->y > watery && !(object->flags & OF_INWATER))
	{
		int i;
		object->flags |= OF_INWATER;

		// Splash!
		for(i=0;i<32;i++)
		{
			obj_t* obj;
			obj = obj_spawn(OT_WATERPARTICLE,object->x, object->y-object->momy);
			if (!obj) { break; }
			obj->momx = ((rand()%20)-10)*UNIT;
			obj->momy = -(rand()%10)*UNIT;
		}
	}
	// Player just jumped out of the water
	if (watery>-1 && object->type != OT_WATERPARTICLE && object->type != OT_PARTICLE && object->y <= watery && (object->flags & OF_INWATER))
	{
		int i;
		object->flags &= ~OF_INWATER;

		// Splash!
		for(i=0;i<32;i++)
		{
			obj_t* obj;
			obj = obj_spawn(OT_WATERPARTICLE,object->x, object->y+object->momy);
			if (!obj) { break; }
			obj->momx = ((rand()%20)-10)*UNIT;
			obj->momy = -(rand()%10)*UNIT;
		}
	}

	// Update water movement factor
	if (object->flags & OF_INWATER)
		movespeed=UNDERWATERSPEED;
	else
		movespeed=1;

	// Left/right movement
	if (object->ioflags & IOLEFT && !(object->ioflags & IORIGHT) && object->momx > -(object->maxrun)) // Left
	{
		object->momx-=object->accx*movespeed;
		object->facedir = 0;
	}
	if (object->ioflags & IORIGHT && !(object->ioflags & IOLEFT) && object->momx < object->maxrun) // Right
	{
		object->momx+=object->accx*movespeed;
		object->facedir = 1;
	}

	// Deceleration while on ground (friction)
	if(object->flags & OF_FLOORSTATE)
	{
		if (object->momx>0 && !(object->ioflags & IORIGHT) && object->momx>object->decx)
			object->momx-=object->decx*movespeed;
		else if (object->momx<0 && !(object->ioflags & IOLEFT) && object->momx<-object->decx)
			object->momx+=object->decx*movespeed;
		else if (!(object->ioflags & IOLEFT) && !(object->ioflags & IORIGHT))
			object->momx=0;
	}
//	if(object->floorobj)
	{
	//	object->floorobj->extramass = 0;
	//	object->floorobj = NULL;
	}
	if (object->ioflags & IOUP && !(object->ioflags & IODOWN) && object->momy > -(object->maxrun)) // Left
	{
		object->momy-=object->accx;
	}
	if (object->ioflags & IODOWN && !(object->ioflags & IOUP) && object->momy < object->maxrun) // Right
	{
		object->momy+=object->accx;
	}


	// Object has let go of the jump key, allow it to jump again.
	if (!(object->ioflags & IOJUMP) && (object->flags & OF_AIRSTATE))
	{
		object->flags &= ~OF_AIRSTATE;
	}
	if(!(object->ioflags & IOSPEC) && (object->flags & OF_SPECDOWN))
	{
		object->flags &= ~OF_SPECDOWN;
	}

	// Jump movement
	if (object->ioflags & IOJUMP && !(object->flags & OF_AIRSTATE))
	{
		object->flags |= OF_AIRSTATE;
		object->momy = object->jumpstr;
		object->flags &= ~OF_FLOORSTATE;
		object->flags |= OF_ENTERSLOPE;
		if (object==obj_player)
			sdfx_play(r_audio[R_SFX_PLAYJUMP].dat,SP_NORMAL,255,128,1000,0);
		else
			sdfx_play(r_audio[R_SFX_PLAYJUMP].dat,SP_NORMAL,64,128,1000,0);
	}

	// Special key pressed
	if (object->ioflags & IOSPEC && !(object->flags & OF_SPECDOWN))
	{
		obj_t* obj;

		obj = obj_spawn(OT_CRATE,obj_player->x, obj_player->y-256*UNIT);

		if (obj)
		{
			obj->tics = -1;

			/*if(obj_player->facedir & FACERIGHT)
				obj_thrust(obj, -obj_player->rotation, 10*UNIT);
			else
				obj_thrust(obj, -obj_player->rotation+itofix(128), 10*UNIT);
		*/}

		object->flags |= OF_SPECDOWN;

	}
	collision_poll(object, movespeed);

}

// Make the objects think. AI, Movement, whatever.
// (Don't call this. It's just for use in the main game loop)
void obj_think(void)
{
	int i;
	obj_t* object;
	int voice;

	// Set the player object's input flags to match those given by the user.
	obj_player->ioflags=0;

	for (i=0; i<NUMKEYS; i++)
	{
		if (input_game[i] == 1)
			obj_player->ioflags |= (int)pow(2,i);
	}

	for(i=0;i<MAXOBJS;i++)
	{
		object = &objectlist[i];

		// Object is dead, it doesn't need to think..
		if(object->alive < AO_ACTIVE)
		{
			// Objects should still count down their timer when off-screen.
			object->tics--;

			// Die after counter runs out.
			if(object->tics == 0)
				obj_kill(object);

			continue;
		}
		//if(object && object->alive >= AO_ACTIVE)
		{
			switch (object->type)
			{
			case OT_PLAYER:
				// Simple AI thinker for an object that follows the player
				if (object != obj_player) {
					object->ioflags = 0;
					if (object->x > obj_player->x) { object->ioflags |= 1; }
					if (object->x < obj_player->x) { object->ioflags |= 2; }
					//if (object->y > obj_player->y) { object->ioflags |= 4; }
					//if (object->y < obj_player->y) { object->ioflags |= 8; }
					if (object->y > obj_player->y+(100*UNIT) && object->flags & OF_AIRSTATE) { object->ioflags &= ~16; }
					if (object->y > obj_player->y+(100*UNIT) && !(object->flags & OF_AIRSTATE)) { object->ioflags |= 16; }
				}

				//object->colorr=fixsin(game_runtics*45000)*127/UNIT+128;
				//object->colorg=fixsin(game_runtics*55000)*127/UNIT+128;
				//object->colorb=fixsin(game_runtics*65000)*127/UNIT+128;
				//object->colora=fixsin(game_runtics*75000)*127/UNIT+128;

				break;

			case OT_PARTICLE:
				// Keep it from going pitch black or overflowing back into the bright colors
				if(object->colorr > 20)
					object->colorr -= 1;
				if(object->colorg > 20)
					object->colorg -= 1;
				if(object->colorb > 20)
					object->colorb -= 1;
				//	object->colorg += rand()%10;
				//	object->colorb += rand()%10;

				/*if(object->x > obj_player->x)
				object->momx -= 3*UNIT;
				else
				object->momx += 3*UNIT;*/
				break;

			case OT_WATERPARTICLE:
				if(object->colorr > 20)
					object->colorr -= 1;
				if(object->colorg > 20)
					object->colorg -= 1;
				if(object->colorb > 20)
					object->colorb -= 1;

				if(object->y >= watery)
					object->tics = 1;
				break;

			case OT_POLYTEST:
				object->colorr=fixsin(game_runtics*45000)*127/UNIT+128;
				object->colorg=fixsin(game_runtics*55000)*127/UNIT+128;
				object->colorb=fixsin(game_runtics*65000)*127/UNIT+128;
				object->colora=fixsin(game_runtics*75000)*127/UNIT+128;

				if (sdfx_checkfree(r_audio[R_SFX_PULSE].dat) && sdfx_find(r_audio[R_SFX_PULSE].dat,object) == -1)
				{
					voice = sdfx_play(r_audio[R_SFX_PULSE].dat,SP_NORMAL,255,128,1000,1);
					if (voice != -1)
						sdfx_attach(voice, object, 255, 4);
				}
				break;
			case OT_CRATE:
				if (sdfx_checkfree(r_audio[R_SFX_PULSE].dat) && sdfx_find(r_audio[R_SFX_PULSE].dat,object) == -1)
				{
					voice = sdfx_play(r_audio[R_SFX_PULSE].dat,SP_NORMAL,255,128,1000,1);
					if (voice != -1)
						sdfx_attach(voice, object, 255, 4);
				}
			//	object->momx = 5*UNIT;
				break;

			default:
				// lolwut
				break;
			}

			object->rotation += object->momrot; // Rotation test?
			if(object->momrot > 0)
				object->momrot--;

			//#ifdef SHUFCOLLIDE
			//obj_move(object, i);
			//#else
			obj_move(object);
			//#endif

			object->tics--;
			#ifdef SHUFCOLLIDE
//			object->flags &= ~OF_JUSTSPAWNED;
			#endif

			// Die after counter runs out.
			if(object->tics == 0)
				obj_kill(object);
		}
	}
}

// Inactivates objects outside the screen under various conditions.
void obj_inactivation(void)
{
	int i;
	obj_t* object;

	for(i=0;i<MAXOBJS;i++)
	{
		object = &objectlist[i];
		switch (object->inactivatemode)
		{
			case IM_SLEEP:
			{
				if (abs(object->x-obj_player->x)>SCREENDISTANCE || abs(object->y-obj_player->y)>SCREENDISTANCE)
					object->alive = AO_INACTIVE;
				else
					object->alive = AO_ACTIVE;
				break;
			}
			case IM_KILL:
			{
				if (abs(object->x-obj_player->x)>SCREENDISTANCE || abs(object->y-obj_player->y)>SCREENDISTANCE)
					obj_kill(object);
				break;
			}
			case IM_UPONREST:
			{
				if ((abs(object->x-obj_player->x)>SCREENDISTANCE || abs(object->y-obj_player->y)>SCREENDISTANCE) && object->momx == 0 && object->momy == 0)
					object->alive = AO_INACTIVE;
				else
					object->alive = AO_ACTIVE;
				break;
			}
		}
	}
}

// Spawn an object at the given location.
obj_t* obj_spawn(int type, int x, int y)
{
	int i;
	obj_t* object = NULL;

	for(i=0;i<MAXOBJS;i++)
	{
		// Maximum number of objects reached, don't spawn.
		if(i == MAXOBJS-1)
		{
			game_printf("\nWARNING! Cannot spawn object of type %i - Reached object limit!", type);
			return 0;
		}

		// This slot is taken, move along..
		if(objectlist[i].alive)
			continue;

		// Found a slot!
		object = &objectlist[i];
		break;
	}

	memset(object, 0, sizeof(obj_t));
	object->x = x;
	object->y = y;
	object->type = type;
	object->alive = AO_ACTIVE;
	object->flags &= ~OF_AIRSTATE;
	object->flags &= ~OF_SPECDOWN;
	#ifdef SHUFCOLLIDE
	object->flags |= OF_SPAWNTOUCH;
	#endif

	obj_setattributes(object);

	tile_shoveobject(object);

	return object;
}

void obj_kill(obj_t* object)
{
	memset(object, 0, sizeof(obj_t));
	object->alive = AO_EMPTY;
}

// Counts the total number of object slots being used.
int obj_count(void)
{
	int i;
	int count = 0;

	for(i=0;i<MAXOBJS;i++)
	{
		if(objectlist[i].alive)
			count++;
	}

	return count;
}

// Does object-to-object collision
// Starts on the calling object's spot in the array
#ifdef SHUFCOLLIDE
void obj_collidex(obj_t* object, int start)
{
	int i, collision = 0;
	// Create a new variable so we only have to do this division once
	unsigned int objheight = object->height>>1;
	unsigned int objwidth = object->width>>1;
	unsigned int objpluswidth = object->x+objwidth;
	unsigned int objminuswidth = object->x-objwidth;
	unsigned int objplusheight = object->y+objheight;
	unsigned int objminusheight = object->y-objheight;
	unsigned int obj2height = 0;
	unsigned int obj2width = 0;
	obj_t* obj2;

	(void)start;

	//if(object->objcollisiontype == CT_NONE)
	//		return;

	for(i=0;i<MAXOBJS;i++)
	{
		obj2 = &objectlist[i];

		if(!obj2->alive)
			continue;

		if(obj2->objcollisiontype == CT_NONE)
			continue;

		obj2height = obj2->height>>1;
		obj2width = obj2->width>>1;

		// Object passes over
		if(objplusheight < obj2->y - obj2height)
			continue;

		if(obj2->y + obj2height < objminusheight)
			continue;

		if(object == obj2)
			continue;

		// We've gotten this far down the loop, it must've collided.
		collision = 1;

		if(obj2->flags & OF_SPAWNTOUCH)
			continue;

		if(object->flags & OF_SPAWNTOUCH)
			break;

		// X Collision
		if(objpluswidth > obj2->x - obj2width
			&& obj2->x + obj2width > objminuswidth)
		{
			if(object->momx < 0)
				object->x = obj2->x+obj2width+objwidth+1;
			else
				object->x = obj2->x-obj2width-objwidth-1;

			switch(object->objcollisiontype)
			{
				case CT_SLIDE:
					object->momx=0;
					break;
				case CT_HALT:
					object->momx = object->momy=0;
					break;
				case CT_STICK:
					object->momx = object->momy = object->gravx = object->gravy=0;
					break;
				case CT_BOUNCY:
					obj2->momx = object->momx;
					object->momx = -object->momx/2;
				//	if(object->momx > 0)
					//	object->x++;
					//else
					//	object->x--;
					break;
				case CT_REFLECT:
					obj2->momx = object->momx;
					object->momx = -object->momx;
					if(object->momx > 0)
						object->x++;
					else
						object->x--;
					break;
			}
		}
	}
	if(!collision)
		object->flags &= ~OF_SPAWNTOUCH;
}

void obj_collidey(obj_t* object, int start)
{
	int i, collision = 0;
	// Create a new variable so we only have to do this division once
	unsigned int objheight = object->height>>1;
	unsigned int objwidth = object->width>>1;
	unsigned int objpluswidth = object->x+objwidth;
	unsigned int objminuswidth = object->x-objwidth;
	unsigned int objplusheight = object->y+objheight;
	unsigned int objminusheight = object->y-objheight;
	unsigned int obj2height = 0;
	unsigned int obj2width = 0;
	obj_t* obj2;

	(void)start; //unused

//	if(object->objcollisiontype == CT_NONE)
	//		return;

	for(i=0;i<MAXOBJS;i++)
	{
		obj2 = &objectlist[i];

		if(!obj2->alive)
			continue;

		if(obj2->objcollisiontype == CT_NONE)
			continue;

		obj2height = obj2->height>>1;
		obj2width = obj2->width>>1;

		// Object passes at the left
		if(objpluswidth < obj2->x - obj2width)
		{
#ifdef MASS
			if(obj2 == object->floorobj)
			{
				object->floorobj->extramass = 0;
				object->floorobj = NULL;
			}
#endif
			continue;
		}

		if(obj2->x + obj2width < objminuswidth)
		{
#ifdef MASS
			if(obj2 == object->floorobj)
			{
				object->floorobj->extramass = 0;
				object->floorobj = NULL;
			}
#endif
			continue;
		}

		if(object == obj2)
			continue;

		// We've gotten this far down the loop, it must've collided.
		collision = 1;

		if(obj2->flags & OF_SPAWNTOUCH)
			continue;

		if(object->flags & OF_SPAWNTOUCH)
			break;

		// Y Collision
		if(objplusheight > obj2->y - obj2height
			&& obj2->y + obj2height > objminusheight)
		{
			if(object->momy < 0)
				object->y = obj2->y+obj2height+objheight+1;
			else
			{
				object->y = obj2->y-obj2height-objheight-1;
				object->floorobj = obj2;
#ifdef MASS
				object->floorobj->extramass = object->mass + object->extramass;
#endif
				object->flags |= OF_FLOORSTATE;
			}

			switch(object->objcollisiontype)
			{
				case CT_SLIDE:
					object->momy=0;
					break;
				case CT_HALT:
					object->momx = object->momy=0;
					break;
				case CT_STICK:
					object->momx = object->momy = object->gravx = object->gravy=0;
					break;
				case CT_BOUNCY:
					obj2->momy = object->momy;
					object->momy = -object->momy/2;
					//object->y += object->momy;
					break;
				case CT_REFLECT:
					obj2->momy = object->momy;
					object->momy = -object->momy;
					//object->y += object->momy;
					break;
			}
		}
		else
		{
			if(obj2 == object->floorobj)
			{
#ifdef MASS
				object->floorobj->extramass = 0;
#endif
				object->floorobj = NULL;
			}
		}
	}
	if(!collision)
	{
		if(object->floorobj)
		{
			//object->floorobj->extramass = 0;
			object->floorobj = NULL;
		}
		object->flags &= ~OF_SPAWNTOUCH;
	}
}
#endif
