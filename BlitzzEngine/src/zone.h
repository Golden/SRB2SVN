#define ZONE_XSIZE 32
#define ZONE_YSIZE 16
#define MAXTILES 255 // Unsigned char is 0-255. Making MAXTILES 256 will cause an infinite loop unless you change tile_define()

#define RESPOND_NONE 0
#define RESPOND_UP 1
#define RESPOND_DOWN 2
#define RESPOND_LEFT 3
#define RESPOND_RIGHT 4
#define RESPOND_AUTOY 5
#define RESPOND_AUTOX 6

#define TILE_NODRAW 0
#define TILE_DRAWOPAQUE 1
#define TILE_DRAWMASKED 2

#define UPLEFT 1
#define UPRIGHT 2
#define DOWNLEFT 3
#define DOWNRIGHT 4

extern int zone[ZONE_XSIZE][ZONE_YSIZE];

extern int tilenum;

typedef struct
{
	int tilepic; // Which sprite index to use as the visible tile
	char solid; // Can you collide with this tile?
	unsigned char coltop; // Top collision position in terms of bounding box
	unsigned char colbottom; // Bottom collision position
	unsigned char colleft; // Left collision position
	unsigned char colright; // Right collision position
	unsigned char croptop; // Top cropping position in tile
	unsigned char cropbottom; // Bottom cropping position
	unsigned char cropleft; // Left cropping position
	unsigned char cropright; // Right cropping position
	unsigned char type; // Tile's behavior type
	char visibility; // Contains visibility information to speed up tile drawing
	char slope; // Slope behavior of tile
}tile_t;

extern tile_t tilelist[MAXTILES];

// prepare.c functions
void tile_define(char solid, unsigned char top, unsigned char bottom, unsigned char left, unsigned char right, unsigned char type, char slope, char visibility);
// zone.c functions
int tile_collide(int top, int bottom, int left, int right, char response);
