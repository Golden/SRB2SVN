#include <allegro.h>

#include "gamesys.h"
#include "coregfx.h"

static char temporal=0;

void grfx_refresh(void)
{

	if (game_gfxupdate==GRFX_DBUFFER)
	{
		acquire_screen();
		//Blits finished image to screen. Rather slow but the speed
		//of memory blitting beforehand makes up for the performance loss
		if (game_vsync)
			vsync();

		switch (game_gfxscale)
		{
			case GRFX_CENTER:
				blit(buffer,screen,0,0,(SCREEN_W-GAMEWIDTH)/2,(SCREEN_H-GAMEHEIGHT)/2,GAMEWIDTH,GAMEHEIGHT);
				break;
			case GRFX_RESIZE:
				stretch_blit(buffer,screen,0,0,GAMEWIDTH,GAMEHEIGHT,0,0,SCREEN_W,SCREEN_H);
				break;
			case GRFX_TEMPORAL:
				temporal ^= 1;
				stretch_blit(buffer,screen,temporal,temporal,GAMEWIDTH,GAMEHEIGHT,0,0,SCREEN_W,SCREEN_H);
				break;
		}
		release_screen();
	}

	if (game_gfxupdate==GRFX_PAGEFLIP)
	{
		show_video_bitmap(buffer);

		if (buffer==page1)
			buffer=page2;
		else
			buffer=page1;
	}
	
	if (game_gfxupdate==GRFX_TBUFFER)
	{
		do {
		} while (poll_scroll());

		request_video_bitmap(buffer);

	if (buffer==page1)
			buffer=page2;
		else if (buffer==page2)
			buffer=page3;
		else if (buffer==page3)
			buffer=page1;
	}

}
