// Gameplay code loop
// By FoxBlitzz
// What actually makes a game a game (and this currently isn't a game until we
// actually have more crap to put in here. ;P ). So far it runs the object
// thinkers and updates the FPS timer...

#include <allegro.h>
#include "gamesys.h"
#include "coreproc.h"
#include "coregfx.h"
#include "coreinput.h"
#include "coresound.h"

#include "object.h"
#include "menu.h"

#include "resources.h"
#include "r_actors.h"
#include "r_ui.h"

char game_run;

void game_process(void)
{
	int mykey=0;

	//FPS timer

	if (game_fps>0)
	{
		game_lastfps=game_curfps;
		game_fps=0;
		game_curfps=0;
	}

	if (keypressed()) {mykey=readkey();}

	if (!(uiflags & UI_CONSOLE) && (mykey >> 8) == KEY_ESC && game_run > 0)
	{
		sdfx_play(r_ui[R_UI_MENUBACK].dat,SP_INTERRUPT,255,128,1000,0);
		game_run=0;
	}
	else if (!(uiflags & UI_CONSOLE) && (mykey >> 8) == KEY_ESC)
	{
		if (menu_historypos > 0)
		{
			menu_position = menu_history[0];
			menu_historypos = menu_selection = 0;
			sdfx_play(r_ui[R_UI_MENUBACK].dat,SP_INTERRUPT,255,128,1000,0);
		}
		else
		{
			sdfx_play(r_ui[R_UI_MENUCHOOSE].dat,SP_INTERRUPT,255,128,1000,0);
			game_run=1;
		}
	}

	if ((mykey >> 8) == KEY_TILDE && (uiflags & UI_CONSOLE))
	{
		sdfx_play(r_ui[R_UI_MENUCHOOSE].dat,SP_INTERRUPT,255,128,1000,0);
		uiflags &= ~UI_CONSOLE;
		game_run = 1;
	}
	else if ((mykey >> 8) == KEY_TILDE)
	{
		sdfx_play(r_ui[R_UI_MENUSELECT].dat,SP_INTERRUPT,255,128,1000,0);
		uiflags |= UI_CONSOLE;
		game_run = 0;
	}

	if ((mykey >> 8) == KEY_D && game_run > 0)
	{
		uiflags ^= UI_DEBUGWORLD;
	}

	if (game_run > 0 && game_taskswitch==1)
		game_run=0;
	else if (game_run <= 0)
		menu_think();		// Update menu cursor, etc.
	else if (game_run > 0)
	{

		// Bob water up and down

		//watery=(802+sin(game_runtics/256.0)*400)*UNIT;
		watery=-1;
		//watery=0;

		// Temporary placement, just for testing..

		if (input_game[IK_KEYATCK])
		{
			int i;
			obj_t* obj;

			for(i=0;i<4;i++)
			{
				obj = obj_spawn(OT_PARTICLE,obj_player->x, obj_player->y);

				if(obj)
				{
					obj->x += ((rand()%20)-10)*UNIT;
					obj->y += ((rand()%20)-10)*UNIT;
					obj->momx = ((rand()%400)-200)*UNIT/25 + obj_player->momx;
					obj->momy = ((rand()%400)-200)*UNIT/25 + obj_player->momy;
					obj->tics = 240;
				}
			}
		}

		if (key[KEY_R])
		{
			obj_player->x = 200*UNIT;
			obj_player->y = 700*UNIT;
			obj_player->momx = obj_player->momy = 0;
		}

		// Activate and inactivate object thinkers as necessary.
		obj_inactivation();

		// Run object thinker
		obj_think();

		// Update camera's position according to various rules of
		// the controlled object, taking EG. velocity into account.
		grfx_camthink();
	}
}
