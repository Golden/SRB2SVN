
// Menulist itemtypes
#define MI_STATIC 0 // Not a menu item: The cursor will skip over it. Good for EG. disclaimer text
#define MI_SUBMENU 1 // This menu item leads to a submenu as dictated by *target
#define MI_CALLFUNC 2 // This menu item calls function (*proc) when selected
#define MI_VARIABLE 3 // Item is a variable *itemvar that can be adjusted
#define MI_BOOLEAN 4 // Item is a set of flags with the bit boolbit adjustable
#define MI_GOBACK 5 // When chosen, goes back one step in history

// Contains a menu item. In an array, they form a menu list of multiple items.
typedef struct menu_t
{
	int xpos;               // X position of this item in the menu
	int ypos;               // Y position of this item in the menu
	const char *text;       // Displayed name for this menu item
	char itemtype;          // Whether static text, function caller, submenu, or variable setting
	int (*proc)(void);      // Function to call when item is selected
	struct menu_t *target;  // Submenu to go to when item is submenu type
	void *itemvar;          // Value that is adjusted for this menu item
	unsigned int boolbit;   // Bit to adjust for *itemvar when item is boolean type
} menu_t;

extern menu_t *menu_position;
extern int menu_selection;

extern menu_t menulist_home[];
extern menu_t menulist_options[];
extern menu_t *menu_history[10];
extern int menu_historypos;

//functions
void menu_think(void);
