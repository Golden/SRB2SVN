#define RFX_NONE 0
#define RFX_FUZZY 1
#define RFX_BLUR 2
#define RFX_BLOOM 3
#define RFX_BRIGHT 4

//functions
void grfx_updatesprites(BITMAP *buffer, int scrollx, int scrolly);
void grfx_camthink(void);
void grfx_refresh(void);
void grfx_renderworld(BITMAP *buffer, int scrollx, int scrolly, int effect, BITMAP *buffer2);

// effects.c functions
void effect_fade_spin(int mode);

extern int scrollx;
extern int scrolly;
extern char grfx_noedgescroll;

extern int watery;

