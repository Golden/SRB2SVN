#include <stdio.h>
#include <string.h>
#include <allegro.h>

#include "config.h"

#include "resources.h"
#include "zone.h"
#include "r_ui.h"
#include "r_audio.h"
#include "r_world.h"

#include "gamesys.h"
#include "coregfx.h"
#include "object.h"
#include "coresound.h"

DATAFILE *r_core;
DATAFILE *r_ui;
DATAFILE *r_world;
DATAFILE *r_actor;
DATAFILE *r_audio;

static int barindex;
static int barcolor;
static int colortoggle;

int zone[ZONE_XSIZE][ZONE_YSIZE];

static void game_loadingbar(DATAFILE *datafile)
{
	(void)datafile;


	if (barindex >= 256)
	{
		colortoggle ^= 1;
		barindex = 0;
	}
	else
	{
		if (colortoggle)
			barcolor = makecol(0,96,255);
		else
			barcolor = makecol(0,64,192);
		rectfill(screen, SCREEN_W/2-127+barindex, SCREEN_H*3/4+1, SCREEN_W/2-100+barindex, SCREEN_H*3/4+19, barcolor);
		barindex +=32;
	}
}

DATAFILE *game_loaddata(const char *datafile)
{
	DATAFILE *newdatafile;
	char target[512]="";
	char message[80]="  Opening ";

	strcat(target,game_datapath);
	strcat(target,datafile);

	strcat(message, datafile);
	strcat(message, "...");
	memset(message+strlen(message), ' ',60-strlen(message));

	game_printf(message);

	rectfill(screen, SCREEN_W/2-127, SCREEN_H*3/4+1, SCREEN_W/2+127, SCREEN_H*3/4+41, makecol(0,0,0));
	textprintf_centre_ex(screen, font, SCREEN_W/2, SCREEN_H*3/4+27, makecol(0, 128, 255), -1, "%s", datafile);
	rect(screen, SCREEN_W/2-128, SCREEN_H*3/4, SCREEN_W/2+128, SCREEN_H*3/4+20, makecol(0,64,192));
	barindex = 0;
	colortoggle = 1;
	newdatafile = load_datafile_callback(target, game_loadingbar);

	if (newdatafile)
	{
		game_showsucc();
		return newdatafile;
	}
	else
		game_showerror("Could not read from %s!\n\nPlease ensure that the game is installed properly,\nand that you have read access to the file.",target);
	return newdatafile;
}

void game_prepare(void)
{
	RLE_SPRITE *loaderpic;

	//Test Array Thing

	char loadzone[ZONE_XSIZE][ZONE_YSIZE] =
	{
		{1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,},
		{1,0,0,0,0,0,0,0,0,1,1,0,0,0,0,1,},
		{1,0,0,0,0,0,0,0,0,1,1,0,0,0,0,1,},
		{1,0,0,1,0,0,0,0,0,1,1,0,0,0,0,1,},
		{1,0,0,8,0,0,0,0,0,8,1,0,0,0,0,1,},
		{1,0,0,9,0,0,0,0,0,0,1,0,0,2,0,1,},
		{1,0,0,1,0,0,0,0,0,0,1,0,1,2,0,1,},
		{1,0,0,1,1,0,0,0,0,9,1,0,1,0,0,1,},
		{1,0,0,8,1,11,0,0,9,1,1,0,0,0,0,1,},
		{1,0,0,0,8,1,0,9,1,1,1,1,0,0,0,1,},
		{1,0,8,0,0,0,0,1,1,1,0,0,0,1,0,1,},
		{1,0,0,0,0,0,2,1,1,10,9,1,0,0,0,1,},
		{1,0,9,0,0,2,1,1,10,0,1,1,0,0,0,1,},
		{1,0,0,0,0,0,1,1,0,0,8,1,0,0,0,1,},
		{1,0,10,0,0,0,0,0,0,0,0,1,0,0,0,1,},
		{1,0,0,0,0,0,1,0,0,0,9,1,0,0,0,1,},
		{0,0,11,0,0,9,1,0,0,0,1,1,0,0,0,0,},
		{0,0,0,0,0,0,1,0,0,0,8,1,0,0,0,0,},
		{0,0,0,0,0,0,1,0,0,0,0,1,0,1,0,0,},
		{1,0,0,0,0,8,1,0,0,0,9,1,0,0,0,1,},
		{1,0,0,0,0,0,1,0,0,0,0,1,0,0,0,1,},
		{1,0,0,0,1,0,1,0,0,0,0,1,0,2,0,3,},
		{1,0,0,7,7,0,0,0,0,0,9,1,0,2,0,1,},
		{1,0,8,1,5,0,0,0,0,9,1,1,0,0,0,2,},
		{1,0,8,1,4,0,0,0,9,1,1,0,0,0,0,1,},
		{1,0,0,1,0,0,0,9,1,1,5,0,0,0,0,2,},
		{1,0,0,1,0,0,9,1,1,1,1,0,0,0,0,1,},
		{1,0,0,0,0,9,1,1,0,0,6,0,0,0,0,1,},
		{1,0,0,0,9,1,0,0,0,0,0,0,0,0,0,1,},
		{1,0,0,9,1,1,0,0,0,0,0,0,0,0,1,1,},
		{1,0,9,1,0,0,0,0,0,0,0,0,0,1,1,1,},
		{1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1},
	};

	int x;
	int y;

	game_ms = 0; // Reset timer
	tilenum = 0;

	for (y=0; y<ZONE_YSIZE; y++)
	{
		for (x=0; x<ZONE_XSIZE; x++)
		{
			zone[x][y]=loadzone[x][y];
		}
	}

	game_printf("\n\n___________________\n\nLoading Zone. Please Wait...                                ");

	acquire_bitmap(buffer);

	clear_to_color(buffer,makecol(0,0,0));

	loaderpic = (RLE_SPRITE *)r_ui[R_UI_LOADER].dat;
	draw_rle_sprite(buffer,loaderpic,(GAMEWIDTH/2)-(loaderpic->w/2),(GAMEHEIGHT/2)-(loaderpic->h/2));

	release_bitmap(buffer);

	grfx_refresh();

	game_printf("\nOpening datafiles...\n");

	r_world=game_loaddata("world0.dat");
	r_actor=game_loaddata("actors.dat");
	r_audio=game_loaddata("audio.dat");

	game_printf("\nPreparing music...\n");

	sdfx_loadsong("SZONE0.ogg");

	game_printf("\nDefining tiles...");

	// Insert pathetic tile definition stuff here. When the real loading is
	//  done, both this code and the related function will be abolished.
	tile_define(0,0,0,0,0,0,0,TILE_NODRAW);
	tile_define(1,0,127,0,127,0,0,TILE_DRAWOPAQUE);
	tile_define(1,64,127,0,127,0,0,TILE_DRAWOPAQUE);
	tile_define(1,0,63,0,127,0,0,TILE_DRAWOPAQUE);
	tile_define(1,0,127,0,127,0,0,TILE_DRAWOPAQUE);
	tile_define(1,0,127,0,63,0,0,TILE_DRAWOPAQUE);
	tile_define(1,0,127,64,127,0,0,TILE_DRAWOPAQUE);
	tile_define(1,0,127,0,127,0,0,TILE_DRAWOPAQUE);
	tile_define(1,0,127,0,127,0,UPRIGHT,TILE_DRAWMASKED);
	tile_define(1,0,127,0,127,0,UPLEFT,TILE_DRAWMASKED);
	tile_define(1,0,127,0,127,0,DOWNRIGHT,TILE_DRAWMASKED);
	tile_define(1,0,127,0,127,0,DOWNLEFT,TILE_DRAWMASKED);

	game_printf("\nInitializing gameplay...                                    ");

	obj_player=obj_spawn(OT_PLAYER,200*UNIT,760*UNIT);
	obj_spawn(OT_POLYTEST,800*UNIT,1000*UNIT);

	grfx_camthink();

	game_tics=0;
	game_runtics=0;
	game_ticskip=0;
	game_run=1;
	game_printf("Done!\n\nZone loaded in %dms.\nEntering main game loop.\n",game_ms);

}

void game_unprepare(void)
{
	int i = 0;
	tile_t* tile;
	obj_t* object;

	game_printf("\n\n___________________\n\nGame loop ended after %d tics and %d runtics.\nUnloading Zone...",game_tics,game_runtics);

	game_printf("\n  Closing datafiles...                                      ");

	unload_datafile(r_world);
	unload_datafile(r_actor);
	unload_datafile(r_audio);

	game_printf("Done!\n  Resetting world...                                        ");

	for (i = 0;  i < MAXTILES; i++)
	{
		tile = &tilelist[i];
		memset(tile, 0, sizeof(tile_t));
	}

	for (i = 0;  i < MAXOBJS; i++)
	{
		object = &objectlist[i];
		obj_kill(object);
	}

	//if (zone)
	//unload_datafile(zone);

	game_printf("Done!\n\nZone unloaded.");
}

