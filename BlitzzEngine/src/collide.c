// Collision routine
// By FoxBlitzz
// Figured I'd put this in its own file to make for less scrolling through
// other source files to find what I need. Provides automatic object movement
// and collision response.

#include "gamesys.h"
#include "object.h"
#include "zone.h"

void collision_poll(obj_t* object, float movespeed)
{
	int tilex;
	int tiley;

	int snapspot;
	tile_t* checktile;

	if (object->flags & OF_INWATER)
		movespeed=UNDERWATERSPEED;
	else
		movespeed=1;

	// Slope detection
	if (object->flags & OF_FLOORSTATE || !(object->flags & OF_ENTERSLOPE))
	{
		object->flags |= OF_ENTERSLOPE; // Fix for entering slope from top lip

		tilex = object->x/UNIT/TILESIZE;
		tiley = (object->y+(object->height/2)+object->heightoffset)/UNIT/TILESIZE;
		if (tilex < 0) {tilex = 0;}
		if (tilex > ZONE_XSIZE-1) {tilex = ZONE_XSIZE-1;}
		if (tiley < 0) {tiley = 0;}
		if (tiley > ZONE_YSIZE-1) {tiley = ZONE_YSIZE-1;}

		checktile = &tilelist[zone[tilex][tiley]];
		if (object->flags & OF_ENTERSLOPE && (checktile->slope != 0))
			object->momy+=abs(object->momx)+1; // Hug the slope
		else if (tiley > 0)
		{
			checktile = &tilelist[zone[tilex][tiley-1]];
			if (object->flags & OF_FLOORSTATE && (checktile->slope != 0))
				object->momy+=abs(object->momx)+1; // Fix for edges of slope tiles
			else
			{
				object->flags &= ~OF_ENTERSLOPE; // No longer on slope
				//object->momy-=abs(object->momx)+1;
			}
		}
		//if (object->OF_FLOORSTATE == 0 && object->enterslope == 0)
			//object->enterslope == 2;
	}

	if (!(object->flags & OF_FLOORSTATE))
		object->flags |= OF_ENTERSLOPE;

	// Do gravity
	object->momx += object->gravx*movespeed;
	object->momy += object->gravy*movespeed;

	// Don't move past MAXMOVE
	if(object->momx > MAXMOVE)
		object-> momx = MAXMOVE;
	else if(object->momx < -MAXMOVE)
		object->momx = -MAXMOVE;

	if(object->momy > MAXMOVE)
		object->momy = MAXMOVE;
	else if(object->momy < -MAXMOVE)
		object->momy = -MAXMOVE;

	// Reset floor state
	object->flags &= ~OF_FLOORSTATE;

	// Add momentum to X pos (Now takes water into account)
	object->x += (object->momx+object->extramomx)*movespeed;

	// Check collision for X axis.
	if (object->collisiontype!=CT_NONE && object->collisiontype!=CT_KILL && object->collisiontype!=CT_OOZE)
	{
		#ifdef SHUFCOLLIDE
		if((object->momx || object->extramomx || object->momy) && object->objcollisiontype != CT_NONE)
			obj_collidex(object, 0);
		#endif
		if (object->momx + object->extramomx>=0)
		{
			snapspot = tile_collide(object->y-(object->height/2)+object->heightoffset,object->y+(object->height/2)+object->heightoffset,object->x-(object->width/2),object->x+(object->width/2),RESPOND_RIGHT);
			if (snapspot)
			{
				switch(object->collisiontype)
				{
					case CT_SLIDE:
						object->momx=0;
						break;
					case CT_HALT:
						object->momx = object->momy=0;
						break;
					case CT_STICK:
						object->momx = object->momy = object->gravx = object->gravy=0;
						break;
					case CT_BOUNCY:
						object->momx = -object->momx/2;
						break;
					case CT_REFLECT:
						object->momx = -object->momx;
						break;
				}

				object->x=snapspot;
			}
		}
		else if (object->momx+object->extramomx<0)
		{
			snapspot = tile_collide(object->y-(object->height/2)+object->heightoffset,object->y+(object->height/2)+object->heightoffset,object->x-(object->width/2),object->x+(object->width/2),RESPOND_LEFT);
			if (snapspot)
			{
				switch(object->collisiontype)
				{
					case CT_SLIDE:
						object->momx=0;
						break;
					case CT_HALT:
						object->momx = object->momy=0;
						break;
					case CT_STICK:
						object->momx = object->momy = object->gravx = object->gravy=0;
						break;
					case CT_BOUNCY:
						object->momx = -object->momx/2;
						break;
					case CT_REFLECT:
						object->momx = -object->momx;
						break;
				}

				object->x=snapspot;
			}
		}
	}

	// Add momentum to Y pos.
	object->y += object->momy*movespeed;

	// Check collision for Y axis.
	// If object is set to kill upon landing, check collision once.
	if (object->collisiontype==CT_KILL && tile_collide(object->y-(object->height/2)+object->heightoffset,object->y+(object->height/2)+object->heightoffset,object->x-(object->width/2),object->x+(object->width/2),RESPOND_NONE))
		obj_kill(object);
	// Ooze down walls
	else if (object->collisiontype == CT_OOZE && tile_collide(object->y-(object->height/2)+object->heightoffset,object->y+(object->height/2)+object->heightoffset,object->x-(object->width/2),object->x+(object->width/2),RESPOND_NONE))
	{
		object->momx = 0;
		object->momy = -(rand()%(UNIT*8));
	}
	// Normal Collision
	else if (object->collisiontype!=CT_NONE)
	{
#ifdef SHUFCOLLIDE
		if(object->momy && object->objcollisiontype != CT_NONE)
			obj_collidey(object, 0);
#endif
		if (object->momy>=0)
		{
			snapspot = tile_collide(object->y-(object->height/2)+object->heightoffset,object->y+(object->height/2)+object->heightoffset,object->x-(object->width/2),object->x+(object->width/2),RESPOND_DOWN);
			if (snapspot)
			{
				switch(object->collisiontype)
				{
					case CT_SLIDE:
						object->momy=0;
						break;
					case CT_HALT:
						object->momx = object->momy=0;
						break;
					case CT_STICK:
						object->momx = object->momy = object->gravx = object->gravy=0;
						break;
					case CT_BOUNCY:
						object->momy = -object->momy/2;
						break;
					case CT_REFLECT:
						object->momy = -object->momy;
						break;
				}

				object->y=snapspot-object->heightoffset;
				object->flags |= OF_FLOORSTATE;
			}
		}
		else if (object->momy<0)
		{
			snapspot = tile_collide(object->y-(object->height/2)+object->heightoffset,object->y+(object->height/2)+object->heightoffset,object->x-(object->width/2),object->x+(object->width/2),RESPOND_UP);
			if (snapspot)
			{
				switch(object->collisiontype)
				{
					case CT_SLIDE:
						object->momy=0;
						break;
					case CT_HALT:
						object->momx = object->momy=0;
						break;
					case CT_STICK:
						object->momx = object->momy = object->gravx = object->gravy=0;
						break;
					case CT_BOUNCY:
						object->momy = -object->momy/2;
						break;
					case CT_REFLECT:
						object->momy = -object->momy;
						break;
				}
				object->y=snapspot-object->heightoffset;
			}
		}
	}
}
