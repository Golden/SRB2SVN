// BlitzzEngine drawing functions (AKA "Allegro is GOD at 2D Graphics")
// By FoxBlitzz
// Draws all background tiles, sprites, and miscellany onto a buffer image,
// and then sends it to the screen in one of two ways selectable through
// blitzz.ini
//
// Remember, add your new item AFTER acquire_bitmap(buffer); and BEFORE
// release_bitmap(buffer);! Failure to adhere to this rule will result
// in a dumbfounded look on your face as your graphics don't appear on-screen.
// It's so important, I shall repeat it upwards of two times.
//
// Remember, add your new item AFTER acquire_bitmap(buffer); and BEFORE
// release_bitmap(buffer);! Remember, add your new item AFTER
// acquire_bitmap(buffer); and BEFORE release_bitmap(buffer);! Remember, add
// your new item AFTER acquire_bitmap(buffer); and BEFORE
// release_bitmap(buffer);!
//
// Thank you, and have a good day.

#include <allegro.h>
#include <math.h>

#include "gamesys.h"
#include "coreinput.h"
#include "object.h"
#include "coreproc.h"
#include "coregfx.h"
#include "console.h"
#include "menu.h"
#include "coresound.h"

#include "resources.h"
#include "zone.h"
#include "r_core.h"
#include "r_ui.h"
#include "r_world.h"
#include "r_actors.h"

#define WORLDX (scrollx>>16)
#define WORLDY (scrolly>>16)

#define SQRTCIRC (0.70710678118654752440084436210495)

BITMAP *buffer;
BITMAP *page1;
BITMAP *page2;
BITMAP *page3;
BITMAP *accumulate;
BITMAP *snapshot;

COLOR_MAP transtable[GRFX_8BITAPRECISION];

static tile_t* tile;

static int tiley;
static int tilex;
static int liney;
static int linex;
int scrollx=0;
int scrolly=0;
static int scrollaheadx=0;
char grfx_noedgescroll=1;

int watery=-1;

static int shovex;
static int shovey;

char consbuf[300] = "\0";

static void grfx_blit_bg(BITMAP *buffer, BITMAP *bgimage,int offx, int offy, int destx, int desty)
{
	blit(bgimage,buffer,offx,offy,destx,desty,(TILESIZE/2),(TILESIZE/2));
	blit(bgimage,buffer,offx+(TILESIZE/2),offy,destx+(TILESIZE/2),desty,(TILESIZE/2),(TILESIZE/2));
	blit(bgimage,buffer,offx,offy+(TILESIZE/2),destx,desty+(TILESIZE/2),(TILESIZE/2),(TILESIZE/2));
	blit(bgimage,buffer,offx+(TILESIZE/2),offy+(TILESIZE/2),destx+(TILESIZE/2),desty+(TILESIZE/2),(TILESIZE/2),(TILESIZE/2));
	//blit(bgimage,buffer,offx,offy,destx,desty,TILESIZE,TILESIZE);
}

static void grfx_blit_fg(BITMAP *buffer, RLE_SPRITE *fgimage, int destx, int desty, char visibility)
{
	if (visibility != TILE_NODRAW)
		draw_rle_sprite(buffer, fgimage, destx, desty);

	if (uiflags & UI_DEBUGWORLD)
	{
		linex = tilex+(WORLDX)/TILESIZE-1;
		liney = tiley+((scrolly)>>16)/TILESIZE-1;
		if (linex < 0) {linex = 0;}
		if (linex > ZONE_XSIZE-1) {linex = ZONE_XSIZE-1;}
		if (liney < 0) {liney = 0;}
		if (liney > ZONE_YSIZE-1) {liney = ZONE_YSIZE-1;}

		tile = &tilelist[zone[linex][liney]];
		rect(buffer,destx-tile->cropleft+tile->colleft,desty-tile->croptop+tile->coltop,destx-tile->cropleft+tile->colright,desty-tile->croptop+tile->colbottom,makecol(255, 0, 0));
		textprintf_centre_ex(buffer, font, destx-tile->cropleft+(TILESIZE/2), desty-tile->croptop+(TILESIZE/2), makecol(255,255,(tile->solid*255)), makecol(0,0,0), "ID: %i", zone[linex][liney]);
	}
}

void grfx_camthink(void)
{
	float movespeed;
	int scrollspeed;

	if (obj_player->flags & OF_INWATER)
		movespeed=UNDERWATERSPEED;
	else
		movespeed=1;

	for (scrollspeed=0; scrollspeed<2; scrollspeed++)
	{
		if (scrollaheadx<(obj_player->momx*8*movespeed)+(4*UNIT))
			scrollaheadx+=1*UNIT;
		if (scrollaheadx>(obj_player->momx*8*movespeed)-(4*UNIT))
			scrollaheadx-=1*UNIT;
	}

	scrollx = obj_player->x-((GAMEWIDTH/2)*UNIT)+scrollaheadx;

	if (obj_player->flags & OF_FLOORSTATE)
		scrolly-=4*UNIT;
	else
		scrolly += obj_player->momy*movespeed/3*2;

	if (obj_player->y > scrolly+((GAMEHEIGHT/2)*UNIT)) { scrolly = obj_player->y-((GAMEHEIGHT/2)*UNIT); }
	if (obj_player->y < scrolly+((GAMEHEIGHT/3)*UNIT)) { scrolly = obj_player->y-((GAMEHEIGHT/3)*UNIT); }

	//Point-of-no-scroll

	if (grfx_noedgescroll)
	{
		if (scrollx<0) { scrollx=0; }
		if (scrollx>(ZONE_XSIZE*TILESIZE*UNIT)-(GAMEWIDTH*UNIT)) { scrollx=(ZONE_XSIZE*TILESIZE*UNIT)-(GAMEWIDTH*UNIT); }
		if (scrolly<0) { scrolly=0; }
		if (scrolly>(ZONE_YSIZE*TILESIZE*UNIT)-(GAMEHEIGHT*UNIT)) { scrolly=(ZONE_YSIZE*TILESIZE*UNIT)-(GAMEHEIGHT*UNIT); }
	}
}

void grfx_renderworld(BITMAP *buffer, int scrollx, int scrolly, int effect, BITMAP *buffer2)
{
	int linerow;

	int newx=0;
	int newy=0;
	float waterdepth;

	if (effect && bitmap_color_depth(screen) == 8) // Ignore effects for 8-bit mode
		buffer = buffer2;

	//Draw a tiled, far background
	for (tiley=0; tiley<(buffer->h/128)+3; tiley++)
	{
		for (tilex=0; tilex<(buffer->w/128)+3; tilex++)
		{
			grfx_blit_bg(buffer,r_world[R_WRL_BACK1].dat,0,0,tilex*TILESIZE-TILESIZE-(((scrollx)/2)>>16)%TILESIZE,tiley*TILESIZE-TILESIZE-(((scrolly)/2)>>16)%TILESIZE);
		}
	}

	//Draw foreground tiles
	for (tiley=0; tiley<(buffer->h/128)+3; tiley++)
	{
		for (tilex=0; tilex<(buffer->w/128)+3; tilex++)
		{

			linex = tilex+(WORLDX)/TILESIZE-1;
			liney = tiley+((scrolly)>>16)/TILESIZE-1;
			if (linex < 0) {linex = 0;}
			if (linex > ZONE_XSIZE-1) {linex = ZONE_XSIZE-1;}
			if (liney < 0) {liney = 0;}
			if (liney > ZONE_YSIZE-1) {liney = ZONE_YSIZE-1;}

			if (zone[linex][liney])
			{
				tile = &tilelist[zone[linex][liney]];
				grfx_blit_fg(buffer,r_world[zone[linex][liney]].dat,tilex*TILESIZE-TILESIZE-(WORLDX)%TILESIZE,tiley*TILESIZE-TILESIZE-(WORLDY)%TILESIZE,tile->visibility);
			}
		}
	}



	// Draw the sprites
	grfx_updatesprites(buffer, scrollx, scrolly);

	// Simple water reflection
	if (watery>-1 && (watery/UNIT)-WORLDY<=buffer->h)
	{

		// Water distortion
		for (linerow=(watery/UNIT)-WORLDY; linerow<=buffer->h-1; linerow+=1)
		{
			if (watery>0)
				waterdepth=(WORLDY-(watery/UNIT)+(buffer->h/2.0))/(buffer->h/2.0);
			else
				waterdepth=3.0;

			linex=sin((linerow/32.0)+(game_runtics/24.0))*(waterdepth+3.0); // Coordinates for X offset of current row
			liney=linerow+((sin((linerow/16.0)+(game_runtics/32.0))+1))*(waterdepth+1.0); // Coordinates for Y offset of row

			// Fix the off-screen artifacts by adding... a different artifact!
			// In this case, it repeats the last row/pixel with graphic information.
			// Otherwise you can see through the other side to all the strings.
			if (liney<buffer->h)
				blit(buffer,buffer,linex,liney,0,linerow,buffer->w,1);
			else
				blit(buffer,buffer,linex,buffer->h-1,0,linerow,buffer->w,1);
			if (linex<0)
				hline(buffer,0,linerow,-linex,getpixel(buffer,-linex,linerow));
			if (linex>0)
				hline(buffer,buffer->w-1,linerow,buffer->w-linex-1,getpixel(buffer,buffer->w-linex-1,linerow));
		}

		// Water coloring is disabled for now. When I make all the graphics
		// paletted, I'll do it, but doing a giant filled rectangle with
		// true-color semi-transparency all the way across the screen isn't
		// healthy on the framerate.

		//color_map = &transtable[GRFX_8BITAPRECISION/4];

		//drawing_mode(DRAW_MODE_TRANS,buffer,0,0);
		//rectfill(buffer,0,(watery/UNIT)-(WORLDY),639,479,makecol(255,20,0));
		//drawing_mode(DRAW_MODE_SOLID,buffer,0,0);

		if (watery>0)
		{
			for (tiley=0; tiley<128; tiley+=(tiley/32.0)+2)
				blit(buffer,buffer,sin((tiley/8.0)+game_runtics/32.0)*4,((watery/UNIT)-(WORLDY))-tiley,0,((watery/UNIT)-(WORLDY))+tiley,GAMEWIDTH,1);
		}
	}

	if (effect && bitmap_color_depth(screen) != 8)
	{
		shovex-=obj_player->momx/16;
		shovey-=obj_player->momy/16;

		do { newx--; shovex+=UNIT; } while (shovex<UNIT);
		do { newx++; shovex-=UNIT; } while (shovex>UNIT);
		do { newy--; shovey+=UNIT; } while (shovey<UNIT);
		do { newy++; shovey-=UNIT; } while (shovey>UNIT);

		if (effect == RFX_FUZZY)
			set_dissolve_blender(128, 128, 128, 192);
		else if (effect == RFX_BLUR)
			set_trans_blender(128, 128, 128, 192);
		else if (effect == RFX_BLOOM)
			set_add_blender(128, 128, 128, 128);
		else if (effect == RFX_BRIGHT)
			set_add_blender(128, 128, 128, 255);

		if (!is_video_bitmap(buffer2))
		{
			if (effect == RFX_BLOOM)
				blit(buffer,buffer2,0,0,0,0,GAMEWIDTH,GAMEHEIGHT);
			else if (effect == RFX_BRIGHT)
				rectfill(buffer2,0,0,GAMEWIDTH-1,GAMEHEIGHT-1,makecol(32,32,32));
			else
				blit(buffer2,buffer2,0,0,newx,newy,GAMEWIDTH,GAMEHEIGHT);
			if (effect == RFX_FUZZY)
				draw_trans_sprite(buffer2,buffer,(rand()%2)-1,(rand()%2)-1);
			else
				draw_trans_sprite(buffer2,buffer,0,0);
		}
		else
		{
			if (effect == RFX_BLOOM)
				blit(buffer,accumulate,0,0,0,0,GAMEWIDTH,GAMEHEIGHT);
			else if (effect == RFX_BRIGHT)
				rectfill(accumulate,0,0,GAMEWIDTH-1,GAMEHEIGHT-1,makecol(32,32,32));
			else
				blit(accumulate,accumulate,0,0,newx,newy,GAMEWIDTH,GAMEHEIGHT);
			if (effect == RFX_FUZZY)
				draw_trans_sprite(accumulate,buffer,(rand()%2)-1,(rand()%2)-1);
			else
				draw_trans_sprite(accumulate,buffer,0,0);
			blit(accumulate,buffer2,0,0,0,0,GAMEWIDTH,GAMEHEIGHT);
		}

		drawing_mode(DRAW_MODE_SOLID,buffer,0,0);
	}

}

static void grfx_rendermenu(menu_t *mymenu)
{
	int i;

	i=0;

	while (mymenu[i].text)
	{
		textprintf_ex(buffer, r_ui[R_UI_MENUFONT].dat, mymenu[i].xpos, mymenu[i].ypos, -1, -1, "%s", mymenu[i].text);
		if (menu_selection == i && mymenu[i].itemtype != MI_GOBACK)
			textprintf_ex(buffer, r_ui[R_UI_MENUFONT].dat, mymenu[i].xpos-32, mymenu[i].ypos, -1, -1, ">");
		else if (menu_selection == i)
			textprintf_ex(buffer, r_ui[R_UI_MENUFONT].dat, mymenu[i].xpos-32, mymenu[i].ypos, -1, -1, "<");

		switch (mymenu[i].itemtype)
		{
			case MI_SUBMENU:
				textprintf_ex(buffer, r_ui[R_UI_MENUFONT].dat, mymenu[i].xpos+text_length(r_ui[R_UI_MENUFONT].dat,mymenu[i].text), mymenu[i].ypos, -1, -1, "/");
				break;
			case MI_VARIABLE:
				textprintf_ex(buffer, r_ui[R_UI_MENUFONT].dat, mymenu[i].xpos+text_length(r_ui[R_UI_MENUFONT].dat,mymenu[i].text), mymenu[i].ypos, -1, -1, "%i", (*((int*)mymenu[i].itemvar)));
				break;
			case MI_BOOLEAN:
				if (*((int*)mymenu[i].itemvar) & mymenu[i].boolbit)
					textprintf_ex(buffer, r_ui[R_UI_MENUFONT].dat, mymenu[i].xpos+text_length(r_ui[R_UI_MENUFONT].dat,mymenu[i].text), mymenu[i].ypos, -1, -1, "ON");
				else
					textprintf_ex(buffer, r_ui[R_UI_MENUFONT].dat, mymenu[i].xpos+text_length(r_ui[R_UI_MENUFONT].dat,mymenu[i].text), mymenu[i].ypos, -1, -1, "OFF");
				break;

		}

		i++;
	}

}

static void grfx_renderdebug(void)
{

	//Write timing information to screen
	textprintf_ex(buffer, font, 0, 0, makecol(255, 255, 255), makecol(0, 0, 0), "Arrow Keys to move, Space to jump. Try: Z, X, C, V, B, N, R, P, Tilde (`)");
	textprintf_ex(buffer, font, 0, 10, makecol(255, 255, 255), makecol(0, 0, 0), "Tics: %d  Runtics: %d", game_tics, game_runtics);
	textprintf_ex(buffer, font, 0, 18, makecol(255, 255, 255), makecol(0, 0, 0), "FPS: %d", game_lastfps);
	textprintf_ex(buffer, font, 0, 26, makecol(255, 255, 255), makecol(0, 0, 0), "Objects: %i", obj_count());
	textprintf_ex(buffer, font, 0, 34, makecol(255, 255, 255), makecol(0, 0, 0), "Player Pos: %i, %i", obj_player->x/UNIT, obj_player->y/UNIT);
	textprintf_ex(buffer, font, 0, 42, makecol(255, 255, 255), makecol(0, 0, 0), "World Pos: %i, %i", obj_player->x/UNIT/TILESIZE, obj_player->y/UNIT/TILESIZE);
	textprintf_ex(buffer, font, 0, 52, makecol(255, 255, 255), makecol(0, 0, 0), "Debug: %i", 0);
	textprintf_ex(buffer, font, 0, 62, makecol(255, 255, 255), makecol(0, 0, 0), "Player Speed: %0.2f, %0.2f", (float)obj_player->momx/UNIT, (float)obj_player->momy/UNIT);
	textprintf_ex(buffer, font, 0, GAMEHEIGHT-56, makecol(255, 255, 255), makecol(0, 0, 0), "Sound Voices: %i/%i",sdfx_count(),get_mixer_voices());
	textprintf_ex(buffer, font, 0, GAMEHEIGHT-40, makecol(255, 255, 255), makecol(0, 0, 0), "Usage:");
	textprintf_ex(buffer, font, 0, GAMEHEIGHT-32, makecol(100, 100, 255), makecol(0, 0, 0), "Game Logic: %ims", game_timelogic);
	textprintf_ex(buffer, font, 0, GAMEHEIGHT-24, makecol(255, 100, 100), makecol(0, 0, 0), "Render: %ims", game_timerender);
	textprintf_ex(buffer, font, 0, GAMEHEIGHT-16, makecol(255, 255, 100), makecol(0, 0, 0), "Screen Update: %ims", game_timeupdate);
	textprintf_ex(buffer, font, 0, GAMEHEIGHT-8, makecol(100, 255, 100), makecol(0, 0, 0), "Rest: %ims", game_timerest);

	//rect(buffer,(obj_player->x-(obj_player->width/2)-scrollx)>>16,(obj_player->y-(obj_player->height/2)+obj_player->heightoffset-scrolly)>>16,(obj_player->x+(obj_player->width/2)-scrollx)>>16,(obj_player->y+(obj_player->height/2)+obj_player->heightoffset-scrolly)>>16,makecol(255,0,0));

	//if(consbuf[0] != '\0')
		//textprintf_ex(buffer, font, 0, 60, makecol(255, 255, 255), makecol(0, 0, 0), "%s", consbuf);
}

void game_render(void)
{
	int liney;
	RLE_SPRITE *myrle;

	int newx=0;
	int newy=0;



	//Acquiring bitmaps for drawing provides a small performance boost
	acquire_bitmap(buffer);
	//ADD ALL GRAPHICS FUNCTION CALLS AFTER THE ABOVE LINE

	if (game_run > 0)
	{
		uiflags &= ~UI_MENU;
		uiflags |= UI_GAMEWORLD;
	}

	if (uiflags & UI_GAMEWORLD) // GAME WORLD
	{
		if (key[KEY_C])
			grfx_renderworld(snapshot,scrollx,scrolly,RFX_FUZZY,buffer);
		else if (key[KEY_V])
			grfx_renderworld(snapshot,scrollx,scrolly,RFX_BLUR,buffer);
		else if (key[KEY_B])
			grfx_renderworld(snapshot,scrollx,scrolly,RFX_BLOOM,buffer);
		else if (key[KEY_N])
			grfx_renderworld(snapshot,scrollx,scrolly,RFX_BRIGHT,buffer);
		else
			grfx_renderworld(buffer,scrollx,scrolly,RFX_NONE,NULL);
	}

	if (uiflags & UI_HUD) // HUD
	{
		if (game_tics <= 480)
		{
			if (game_tics <= 240)
				set_trans_blender(128, 128, 128, 240);
			else
				set_trans_blender(128, 128, 128, 480-(game_tics));

			myrle=r_ui[R_UI_CORNERPIC].dat;
			draw_trans_rle_sprite(buffer, myrle,(GAMEWIDTH)-(myrle->w), (GAMEHEIGHT)-(myrle->h));
		}
	}

	if (game_run == 0)
	{
		grfx_renderworld(snapshot,scrollx,scrolly,RFX_NONE,NULL);

		color_map = &transtable[GRFX_8BITAPRECISION/4];

		drawing_mode(DRAW_MODE_TRANS,snapshot,0,0);
		set_trans_blender(128, 128, 128, 128);
		if (uiflags & UI_CONSOLE)
			rectfill(snapshot,0,0,GAMEWIDTH-1,GAMEHEIGHT-1,makecol(10,50,10));
		else
			rectfill(snapshot,0,0,GAMEWIDTH-1,GAMEHEIGHT-1,makecol(240,100,30));
		if (game_gfxupdate!=GRFX_DBUFFER)
			blit(snapshot,accumulate,0,0,0,0,GAMEWIDTH,GAMEHEIGHT);
		game_run = -1;
		uiflags &= ~UI_GAMEWORLD;
		uiflags |= UI_MENU;
	}

	if (uiflags & UI_MENU)
	{
		//set_trans_blender(128, 128, 128, 8);
		//drawing_mode(DRAW_MODE_TRANS,buffer,0,0);
		//rectfill(buffer,rand()%639,rand()%479,rand()%639,rand()%479,makecol(200,20,50));

		shovex-=obj_player->momx/16;
		shovey-=obj_player->momy/16;

		do { newx--; shovex+=UNIT; } while (shovex<UNIT);
		do { newx++; shovex-=UNIT; } while (shovex>UNIT);
		do { newy--; shovey+=UNIT; } while (shovey<UNIT);
		do { newy++; shovey-=UNIT; } while (shovey>UNIT);

		set_dissolve_blender(128, 128, 128, 64);
		//blit(snapshot,buffer,0,0,0,0,GAMEWIDTH,GAMEHEIGHT);
		if (game_gfxupdate==GRFX_DBUFFER && bitmap_color_depth(screen) != 8)
		{
			blit(buffer,buffer,0,0,newx,newy,GAMEWIDTH,GAMEHEIGHT);
			draw_trans_sprite(buffer,snapshot,(rand()%2)-1,(rand()%2)-1);
		}
		else if (bitmap_color_depth(screen) != 8)
		{
			blit(accumulate,accumulate,0,0,newx,newy,GAMEWIDTH,GAMEHEIGHT);
			draw_trans_sprite(accumulate,snapshot,(rand()%2)-1,(rand()%2)-1);
			blit(accumulate,buffer,0,0,0,0,GAMEWIDTH,GAMEHEIGHT);
		}
		else
			blit(snapshot,buffer,0,0,0,0,GAMEWIDTH,GAMEHEIGHT);

		drawing_mode(DRAW_MODE_SOLID,buffer,0,0);

		if (!(uiflags & UI_CONSOLE))
			grfx_rendermenu(menu_position);

	}

	if ((uiflags & UI_DEBUG) && !(uiflags & UI_CONSOLE))
		grfx_renderdebug();

	if (uiflags & UI_CONSOLE)
	{
		//drawing_mode(DRAW_MODE_SOLID,buffer,0,0);
		//rectfill(buffer,0,0,639,479,makecol(0,0,0));
		newy = cons_pos;
		for (liney=0;liney<CONS_BUFFSIZE;liney++)
		{
			textout_ex(buffer, font, cons_buffer[liney], 0, (CONS_BUFFSIZE-1-newy)*8, makecol(255, 255, 255), makecol(0, 0, 0));
			newy--;

			if (newy<0)
				newy=CONS_BUFFSIZE-1;
		}
	}

	//Release the bitmap after drawing or else the game will freeze!
	release_bitmap(buffer);
	// DO NOT ADD ANY DRAWING CODE AFTER THE ABOVE LINE. DOING SO WILL THROW OFF
	// TIMING OR PREVENT YOUR GRAPHICS FROM BEING DRAWN.

	game_timerender=game_ms; // Get timing information
	game_ms=0; // Reset timer
	grfx_refresh();

	//Increment FPS counter
	game_curfps++;
}

void grfx_updatesprites(BITMAP *buffer, int scrollx, int scrolly)
{
	int i;

	drawing_mode(DRAW_MODE_SOLID,buffer,0,0);

	for (i=0;i<MAXOBJS;i++)
	{
		if (objectlist[i].alive < AO_ACTIVE)
			continue;

		switch (objectlist[i].drawstyle)
		{
			case DS_LINE:
			{
				fastline(buffer,((objectlist[i].x-scrollx)>>16),((objectlist[i].y-scrolly)>>16),((objectlist[i].x-scrollx-objectlist[i].momx)>>16),((objectlist[i].y-scrolly-objectlist[i].momy)>>16),makecol(objectlist[i].colorr, objectlist[i].colorg, objectlist[i].colorb));
				break;
			}

			case DS_SPRITE:
			{
				BITMAP *myactor;
				myactor = (BITMAP *)r_actor[objectlist[i].sprite].dat;

				switch (objectlist[i].facedir)
				{
					case FACELEFT: // Object faces left
					{
						if (objectlist[i].rotation)
							rotate_sprite_v_flip(buffer,myactor,((objectlist[i].x-scrollx)>>16)-(myactor->w/2),((objectlist[i].y-scrolly)>>16)-(myactor->h/2), objectlist[i].rotation+itofix(128));
						else // No need to perform the intensive rotation on a 0-degree sprite
							draw_sprite_h_flip(buffer,myactor,((objectlist[i].x-scrollx)>>16)-(myactor->w/2),((objectlist[i].y-scrolly)>>16)-(myactor->h/2));
						break;
					}
					case FACERIGHT: // Object faces right
					{
						if (objectlist[i].rotation)
							rotate_sprite(buffer,myactor,((objectlist[i].x-scrollx)>>16)-(myactor->w/2),((objectlist[i].y-scrolly)>>16)-(myactor->h/2), objectlist[i].rotation);
						else
							draw_sprite(buffer,myactor,((objectlist[i].x-scrollx)>>16)-(myactor->w/2),((objectlist[i].y-scrolly)>>16)-(myactor->h/2));
						break;
					}
					case FACEVLEFT: // Object faces left, flipped vertically
					{
						if (objectlist[i].rotation)
							rotate_sprite(buffer,myactor,((objectlist[i].x-scrollx)>>16)-(myactor->w/2),((objectlist[i].y-scrolly)>>16)-(myactor->h/2), objectlist[i].rotation+itofix(128));
						else // No need to perform the intensive rotation on a 0-degree sprite
							draw_sprite_vh_flip(buffer,myactor,((objectlist[i].x-scrollx)>>16)-(myactor->w/2),((objectlist[i].y-scrolly)>>16)-(myactor->h/2));
						break;
					}
					case FACEVRIGHT: // Object faces right, flipped vertically
					{
						if (objectlist[i].rotation)
							rotate_sprite_v_flip(buffer,myactor,((objectlist[i].x-scrollx)>>16)-(myactor->w/2),((objectlist[i].y-scrolly)>>16)-(myactor->h/2), objectlist[i].rotation);
						else
							draw_sprite_v_flip(buffer,myactor,((objectlist[i].x-scrollx)>>16)-(myactor->w/2),((objectlist[i].y-scrolly)>>16)-(myactor->h/2));
						break;
					}
				}
				break;
			}

			case DS_RLE:
			{
				RLE_SPRITE* myRLEActor;
				int mycolor;

				myRLEActor = (RLE_SPRITE *)r_actor[objectlist[i].sprite].dat;

				if (objectlist[i].colora == 255)
					draw_rle_sprite(buffer,myRLEActor,((objectlist[i].x-scrollx)>>16)-(myRLEActor->w/2),((objectlist[i].y-scrolly)>>16)-(myRLEActor->h/2));
				else
				{

					if (bitmap_color_depth(screen) != 8)
					{
						mycolor = objectlist[i].colora;
						set_trans_blender(objectlist[i].colorr, objectlist[i].colorg, objectlist[i].colorb, 0);
					}
					else
					{
						mycolor = makecol8(objectlist[i].colorr, objectlist[i].colorg, objectlist[i].colorb);
						color_map = &transtable[(255-objectlist[i].colora)*GRFX_8BITAPRECISION/255];
					}

					draw_lit_rle_sprite(buffer,myRLEActor,((objectlist[i].x-scrollx)>>16)-(myRLEActor->w/2),((objectlist[i].y-scrolly)>>16)-(myRLEActor->h/2),mycolor);
				}
				break;
			}

			case DS_3DQUAD:
			{
				BITMAP *texture = (BITMAP *)r_actor[objectlist[i].sprite].dat;
				short flipx=-1;
				short flipy=1;
				int mycolor;

				V3D vert1 = { objectlist[i].x+fixsin(itofix(game_runtics)*2.3)*4+(fixcos(objectlist[i].rotation+itofix(160))*(texture->w*SQRTCIRC))-scrollx, objectlist[i].y+fixcos(itofix(game_runtics)*2.3)*4-objectlist[i].heightoffset+(fixsin(objectlist[i].rotation+itofix(160))*(texture->h*SQRTCIRC))-scrolly, 0, 0, 0, 0 };
				V3D vert2 = { objectlist[i].x+fixsin(itofix(game_runtics)*1.9)*4+(fixcos(objectlist[i].rotation+itofix(224))*(texture->w*SQRTCIRC))-scrollx, objectlist[i].y+fixcos(itofix(game_runtics)*1.8)*4-objectlist[i].heightoffset+(fixsin(objectlist[i].rotation+itofix(224))*(texture->h*SQRTCIRC))-scrolly, 0, 0, 0, 0 };
				V3D vert3 = { objectlist[i].x+fixsin(itofix(game_runtics)*2.7)*4+(fixcos(objectlist[i].rotation+itofix(288))*(texture->w*SQRTCIRC))-scrollx, objectlist[i].y+fixcos(itofix(game_runtics)*2.7)*4-objectlist[i].heightoffset+(fixsin(objectlist[i].rotation+itofix(288))*(texture->h*SQRTCIRC))-scrolly, 0, 0, 0, 0 };
				V3D vert4 = { objectlist[i].x+fixsin(itofix(game_runtics)*2.1)*4+(fixcos(objectlist[i].rotation+itofix(96))*(texture->w*SQRTCIRC))-scrollx, objectlist[i].y+fixcos(itofix(game_runtics)*2.5)*4-objectlist[i].heightoffset+(fixsin(objectlist[i].rotation+itofix(96))*(texture->h*SQRTCIRC))-scrolly, 0, 0, 0, 0 };


				if (objectlist[i].facedir & FACERIGHT) { flipx=1; }
				if (objectlist[i].facedir & FACEVLEFT) { flipy=-1; }

				if (bitmap_color_depth(screen) != 8)
				{
					mycolor = objectlist[i].colora;
					set_trans_blender(objectlist[i].colorr, objectlist[i].colorg, objectlist[i].colorb, objectlist[i].colora);
				}
				else
				{
					mycolor = makecol8(objectlist[i].colorr, objectlist[i].colorg, objectlist[i].colorb);
					color_map = &transtable[(254-objectlist[i].colora)*GRFX_8BITAPRECISION/255];
				}

				vert1.c = vert2.c = vert3.c = vert4.c = mycolor;

				vert2.u = vert3.u = (texture->w<<16)*flipx;
				vert3.v = vert4.v = (texture->h<<16)*flipy;

				if (objectlist[i].colora == 255)
					quad3d(buffer,POLYTYPE_ATEX_MASK,texture,&vert1,&vert2,&vert3,&vert4);
				else
				{
					quad3d(buffer,POLYTYPE_ATEX_MASK_LIT,texture,&vert1,&vert2,&vert3,&vert4);
				}

				break;
			}
		}

		if (uiflags & UI_DEBUGWORLD)
		{
			#ifdef SHUFCOLLIDE
			if (!(objectlist[i].flags & OF_SPAWNTOUCH))
			#endif
				rect(buffer,(objectlist[i].x-(objectlist[i].width/2)-scrollx)>>16,(objectlist[i].y-(objectlist[i].height/2)+objectlist[i].heightoffset-scrolly)>>16,(objectlist[i].x+(objectlist[i].width/2)-scrollx)>>16,(objectlist[i].y+(objectlist[i].height/2)+objectlist[i].heightoffset-scrolly)>>16,makecol(0,0,255));
			#ifdef SHUFCOLLIDE
			else // If spawntouch is true, draw rectangle green
				rect(buffer,(objectlist[i].x-(objectlist[i].width/2)-scrollx)>>16,(objectlist[i].y-(objectlist[i].height/2)+objectlist[i].heightoffset-scrolly)>>16,(objectlist[i].x+(objectlist[i].width/2)-scrollx)>>16,(objectlist[i].y+(objectlist[i].height/2)+objectlist[i].heightoffset-scrolly)>>16,makecol(0,255,0));

			// Connect floorobj and obj
			if (objectlist[i].floorobj)
				line(buffer,(objectlist[i].x-scrollx)>>16,(objectlist[i].y-scrolly)>>16, (objectlist[i].floorobj->x-scrollx)>>16, (objectlist[i].floorobj->y-scrolly)>>16, makecol(0,255,255));
			#endif
			#ifdef MASS
			// Total up mass and draw it
			textprintf_centre_ex(buffer, font, (objectlist[i].x-scrollx)>>16, (objectlist[i].y-scrolly)>>16, makecol(255,255,255), makecol(0,0,200), "M:%i", objectlist[i].mass+objectlist[i].extramass);
			#endif
			// Draw velocity vector
			line(buffer,(objectlist[i].x-scrollx)>>16,(objectlist[i].y-scrolly)>>16,(objectlist[i].x-scrollx+(objectlist[i].momx*2))>>16,(objectlist[i].y-scrolly+(objectlist[i].momy*2))>>16,makecol(255,255,255));
			#ifdef SHUFCOLLIDE
			// Connect obj and target
			if (objectlist[i].target)
				line(buffer,(objectlist[i].x-scrollx)>>16,(objectlist[i].y-scrolly)>>16, (objectlist[i].target->x-scrollx)>>16, (objectlist[i].target->y-scrolly)>>16, makecol(0,255,255));
			#endif
		}
		//rect(buffer,-120,120,240,240,makecol(0,0,255));
	}
}
