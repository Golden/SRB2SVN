// Code by Shuffle, modifications by FoxBlitzz
// This is where the obj struct goes, I guess. Also can contain various obj functions.
#define MAXOBJS 4096 // 2048 is a good number. We can freeze instances outside the screen -FB
#define MAXSRCNOBJS 256 // On-screen object limit? Is this really feasible? Oh well. -FB
#define GRAVITY 0.5*UNIT // Maybe we should make this changeable in the future..
#define UNDERWATERSPEED 0.7 // Factor for movement speed of all objects that are submerged. -FB

#define SCREENDISTANCE 640*UNIT // How far an object must be from the player object to be deactivated -FB

#define MAXMOVE 32*UNIT

#define IOLEFT 1
#define IORIGHT 2
#define IOUP 4
#define IODOWN 8
#define IOJUMP 16
#define IOATCK 32
#define IOSPEC 64

//#define AIRCANJUMP 0
//#define AIRINJUMP 1

#define FACELEFT 0
#define FACERIGHT 1
#define FACEVLEFT 2
#define FACEVRIGHT 3

#include <allegro.h>

#include "r_actors.h"

// Object types
typedef enum
{
	OT_PLAYER,
	OT_PARTICLE,
	OT_CRATE,
	OT_WATERPARTICLE,
	OT_POLYTEST
}objecttype;

// Collision types
typedef enum
{
	CT_NONE, // Ignores collision with tiles altogether
	CT_SLIDE, // Standard collision. Slides along while zeroing the momentum ONLY on the axis that found collision
	CT_BOUNCY, // Same as sliding, but rebounds the object at 1/2 the momentum it had upon impact
	CT_HALT, // Sets momentum along BOTH axes to 0 upon collision. It's just like Klik & Play!
	CT_STICK, // Sets momentum AND gravity to 0 on both axes upon collision, effectively sticking to the wall.
	CT_KILL, // Kills the object upon collision.                             If you plan to "unstick" an object, you must
	CT_OOZE, // Sets velocities to 0 but does not push out the object.                    reset the gravity!
	CT_KEEPSPEED, // Don't take away momentum when colliding
	CT_REFLECT // Same as bouncy, but all momentum is retained upon impact.
}collisiontype;

// Inactivation modes
typedef enum
{
	IM_IGNORE, // Ignore activation altogether.
	IM_SLEEP, // Turns off the object's thinker when leaving the screen. It will reactivate when it reenters.
	IM_KILL, // Kills the object when it leaves the screen. Good for projectiles.
	IM_UPONREST, // Same as sleep, but the object will only deactivate if its speed is zero.
	IM_BOSS, // Activates when the player passes through the Boss Activator placed in the level, and never deactivates.
}inactivemode;

#define AO_EMPTY 0
#define AO_INACTIVE 1
#define AO_ACTIVE 2

// Object drawing styles
typedef enum
{
	DS_NODRAW, //Don't draw the object at all
	DS_SPRITE, //Draw the object as a regular sprite (only for BITMAP type images)
	DS_RLE, // Draw the object as an RLE sprite, which is faster but less flexible (only for RLE type images)
	DS_LINE, // Draw the object as a moving line
	DS_3DQUAD, // Draw the object as a quad, using linear interpolation (PARTIAL IMPLEMENTATION)
	DS_3DCYLINDER // Draw the object as a cylinder, rotation refers to horizontal texture offset (NOT IMPLEMENTED)
}drawstyle;

// NOTE: The BITMAP must be square to render 3D styles properly!

typedef struct obj_s
{
	// NORMAL GAME ENTITY STRUCTURE (character, enemies) -FB

	// ATTENTION:
	// Shuffle, remember to make anything int that uses UNIT. Otherwise
	// there will be an overflow when doing the constant conversion -
	// multiplying a number by 65536 is obviously going to be bigger than
	// 256, the size of a char, which causes this warning, and would
	// most likely throw off measurements or even cause crashes.
	// However, if the resulting number will just be between 0 and 1,
	// use short int instead. -FB

	unsigned short int sprite; // Index of sprite to be drawn at object's position
	unsigned char drawstyle; // Manner in which to draw the sprite
	int x; // X Position
	int y; // Y Position
	int momx; // X momentum
	int momy; // Y momentum
	int extramomx; // Additional X momentum from EG. a conveyor belt
	int extramomy; // Additional Y momentum
	int type; // Dictates object's identity and behavior
	int accx; // Acceleration value - when inputs are down for left/right, increase by this much every tic. -FB
	int decx; // Deceleration value - when inputs are not down for left/right, decrease by this much. -FB
	int maxrun; // Maximum horizontal speed achievable under normal conditions (slopes, etc. override this) -FB
	int jumpstr; // Vertical velocity applied to object upon jumping. -FB
	int gravx; // Secondary velocity applied to x speed every tic. -FB
	int gravy; // Secondary velocity applied to y speed every tic. Ideally, would be facing downward. :P -FB
	int height; // Collision detection for floor/ceiling -FB
	int width; // Collision detection for walls (does not include slopes) -FB
	int heightoffset; // Vertical offset for the object's collision box. -FB
	fixed rotation; // Will we do rotation?
	char alive; // Dead or alive?
	int momrot; // Rotation momentum, just for fun :P
	int tics; // How long an object is alive in tics.
	char facedir; // Facing left or right, or flipped vertically?
	unsigned char ioflags; // Byte that stores eight flags for movement. -FB
	char collisiontype; // Type of collision
	#ifdef SHUFCOLLIDE
	char objcollisiontype; // Type of collision between objects
	#endif
	unsigned char colorr; // Used for tinting particles. -FB
	unsigned char colorg;
	unsigned char colorb;
	unsigned char colora;
	char inactivatemode; // Mode used to inactivate objects when they leave the viewing area. -FB
	unsigned char flags; // If we ever need 256 or higher for the flags, increase to short or int.
#ifdef SHUFCOLLIDE
	struct obj_s* floorobj;
	struct obj_s* target;
#ifdef MASS
	char mass;
	int extramass;
#endif
#endif
}obj_t;

// FLAGS
#define OF_INWATER 1 
#define OF_SPECDOWN 2 
#define OF_SPAWNTOUCH 4 
#define OF_AIRSTATE 8
#define OF_ENTERSLOPE 16
#define OF_FLOORSTATE 32

// functions
obj_t* obj_spawn(int type, int x, int y); // Spawns and returns an object pointer.
void obj_kill(obj_t* object);
void obj_inactivation(void);
void obj_think(void);
int obj_count(void);

void obj_move(obj_t* object);
void obj_setattributes(obj_t* object);
#ifdef SHUFCOLLIDE
void obj_collidex(obj_t* object, int start);
void obj_collidey(obj_t* object, int start);
#endif

// collide.c functions
void collision_poll(obj_t* object, float movespeed);
// zone.c functions
void tile_shoveobject(obj_t* object);
// coresound.c functions
void sdfx_attach(int voice, obj_t* target, int volume, int strength);
int sdfx_find(SAMPLE *sound, obj_t* checkobj);

extern obj_t objectlist[MAXOBJS];

// Contains the ID of the object that will be controlled by the player -FB
extern obj_t* obj_player;
