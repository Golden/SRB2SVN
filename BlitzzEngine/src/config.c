// Main configuration grabber
// By FoxBlitzz
// Loads all necessary settings from blitzz.ini. Any new settings can be
// defined in here.

#include <stdio.h>
#include <string.h>
#include <allegro.h>
#include "gamesys.h"
#include "config.h"

const char *game_datapath;

static const char *getshowwarn="yes";
const char *getgfxdriver="auto";
const char *getsounddriver="auto";
#ifdef ALLEGRO_WINDOWS
int getsoundcard=0;
#endif
static const char *getupdater="dbuffer";
static const char *getlimit="yes";
static const char *getvsync="no";
static const char *getscale="center";

void game_getconfig(void)
{
	//push_config_state();
	#ifdef ALLEGRO_WINDOWS
	set_config_file("blitzwin.ini");
	#elif defined (ALLEGRO_UNIX)
	set_config_file("blitzlin.conf");
	#else
	set_config_file("blitz.ini");
	#endif

	game_datapath=get_config_string("engine","datapath","data/");
	getshowwarn=get_config_string("engine","show_warnings","yes");

	game_gfxwidth=get_config_int("graphics","width",640);
	game_gfxheight=get_config_int("graphics","height",480);
	getscale=get_config_string("graphics","stretch","center");
	game_gfxdepth=get_config_int("graphics","depth",0);
	getgfxdriver=get_config_string("graphics","driver","auto");
	getupdater=get_config_string("graphics","update","dbuffer");
	getlimit=get_config_string("graphics","limit_fps","yes");
	getvsync=get_config_string("graphics","vsync","no");

	getsounddriver=get_config_string("sound","driver","auto");
	#ifdef ALLEGRO_WINDOWS
	getsoundcard=get_config_int("sound","soundcard",0);
	#endif

	if (strcmp(getshowwarn,"yes")==0) { game_showwarning=GAME_SHOWWARNING; }
	else { game_showwarning=GAME_HIDEWARNING; }

	if (strcmp(getscale,"center")==0) { game_gfxscale=GRFX_CENTER; }
	else if (strcmp(getscale,"resize")==0) { game_gfxscale=GRFX_RESIZE; }
	else if (strcmp(getscale,"temporal")==0) { game_gfxscale=GRFX_TEMPORAL; }

	if (strcmp(getgfxdriver,"autofull")==0) { game_gfxdriver=GFX_AUTODETECT_FULLSCREEN; }
	else if (strcmp(getgfxdriver,"autowin")==0) { game_gfxdriver=GFX_AUTODETECT_WINDOWED; }
	#ifdef ALLEGRO_WINDOWS
	else if (strcmp(getgfxdriver,"dxaccel")==0) { game_gfxdriver=GFX_DIRECTX_ACCEL; }
	else if (strcmp(getgfxdriver,"dxsoft")==0) { game_gfxdriver=GFX_DIRECTX_SOFT; }
	else if (strcmp(getgfxdriver,"dxsafe")==0) { game_gfxdriver=GFX_DIRECTX_SAFE; }
	else if (strcmp(getgfxdriver,"dxwin")==0) { game_gfxdriver=GFX_DIRECTX_WIN; }
	else if (strcmp(getgfxdriver,"dxover")==0) { game_gfxdriver=GFX_DIRECTX_OVL; }
	else if (strcmp(getgfxdriver,"gdiwin")==0) { game_gfxdriver=GFX_GDI; }
	#elif defined (ALLEGRO_UNIX)
	else if (strcmp(getgfxdriver,"xwindow")==0) { game_gfxdriver=GFX_XWINDOWS; }
	else if (strcmp(getgfxdriver,"xwinfull")==0) { game_gfxdriver=GFX_XWINDOWS_FULLSCREEN; }
	else if (strcmp(getgfxdriver,"dgaaccel")==0) { game_gfxdriver=GFX_XDGA2; }
	else if (strcmp(getgfxdriver,"dgasoft")==0) { game_gfxdriver=GFX_XDGA2_SOFT; }
	#endif

	if (strcmp(getupdater,"dbuffer")==0) { game_gfxupdate=GRFX_DBUFFER; }
	else if (strcmp(getupdater,"pageflip")==0) { game_gfxupdate=GRFX_PAGEFLIP; }
	else if (strcmp(getupdater,"tbuffer")==0) { game_gfxupdate=GRFX_TBUFFER; }

	if (strcmp(getlimit,"yes")==0) { game_gfxlimit=GRFX_LIMITFPS; }
	else { game_gfxlimit=GRFX_NOLIMITFPS; }

	if (strcmp(getvsync,"no")==0) { game_vsync=GRFX_NOVSYNC; }
	else { game_vsync=GRFX_VSYNC; }

	if (strcmp(getsounddriver,"none")==0) { game_sounddriver=DIGI_NONE; }
	#ifdef ALLEGRO_WINDOWS
	else if (strcmp(getsounddriver,"dxsound")==0) { game_sounddriver=DIGI_DIRECTX(getsoundcard); }
	else if (strcmp(getsounddriver,"dxsounda")==0) { game_sounddriver=DIGI_DIRECTAMX(getsoundcard); }
	else if (strcmp(getsounddriver,"waveouth")==0) { game_sounddriver=DIGI_WAVOUTID(0); }
	else if (strcmp(getsounddriver,"waveoutl")==0) { game_sounddriver=DIGI_WAVOUTID(1); }
	#elif defined (ALLEGRO_UNIX)
	else if (strcmp(getsounddriver,"openss")==0) { game_sounddriver=DIGI_OSS; }
	else if (strcmp(getsounddriver,"esdaemon")==0) { game_sounddriver=DIGI_ESD; }
	else if (strcmp(getsounddriver,"arts")==0) { game_sounddriver=DIGI_ARTS; }
	else if (strcmp(getsounddriver,"alsa")==0) { game_sounddriver=DIGI_ALSA; }
	else if (strcmp(getsounddriver,"jack")==0) { game_sounddriver=DIGI_JACK; }
	#endif

}
