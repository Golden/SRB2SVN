#include "object.h"
#include "gamesys.h"

#include "r_actors.h"

// Set an object's attributes based on its type;
void obj_setattributes(obj_t* object)
{
	// Set some basic variables for all objects here, then do some of the more custom stuff in the switch case.
	object->accx = .25*UNIT;
	object->decx = .25*UNIT;
	object->facedir = 1;
	object->collisiontype = CT_SLIDE;
	object->inactivatemode = IM_SLEEP;
	object->drawstyle = DS_SPRITE;
	object->colora = 0;

	switch(object->type)
	{
	case OT_PLAYER:
		// Probably not the final stats, but lets just put something here.
		object->sprite=R_ACT_PLAYER;
		object->accx = 0.25*UNIT;
		object->decx = 0.25*UNIT;
		object->maxrun = 10*UNIT;
		object->gravy = GRAVITY;
		object->facedir = 1;
		object->height = 60*UNIT;
		object->heightoffset = 2*UNIT;
		object->width = 30*UNIT;
		object->jumpstr = -14.5*UNIT;
		object->collisiontype = CT_SLIDE;
		#ifdef SHUFCOLLIDE
		object->objcollisiontype = CT_SLIDE;
		#endif
		object->inactivatemode = IM_IGNORE;
#ifdef MASS
		object->mass = 30;
#endif
		break;
	case OT_PARTICLE:
		object->sprite=R_ACT_PARTICLE;
		object->drawstyle=DS_RLE;
		object->decx = 0.25*UNIT;
		object->gravy = GRAVITY;
		object->height = 4*UNIT;
		object->width = 4*UNIT;
		object->collisiontype = CT_BOUNCY;
		#ifdef SHUFCOLLIDE
		object->objcollisiontype = CT_NONE;
		#endif
		object->colorr = 0;
		object->colorg = 200;
		object->colorb = 200;
		object->colora = 192;
#ifdef MASS
		object->mass = 1;
#endif
		object->inactivatemode = IM_IGNORE;
		break;
	case OT_WATERPARTICLE:
		object->sprite=R_ACT_PARTICLE;
		object->drawstyle=DS_RLE;
		object->decx = 0.25*UNIT;
		object->gravy = GRAVITY;
		object->height = 4*UNIT;
		object->width = 4*UNIT;
		object->collisiontype = CT_OOZE;
#ifdef SHUFCOLLIDE
		object->objcollisiontype = CT_NONE;
#endif
		object->colorr = 200;
		object->colorg = 200;
		object->colorb = 200;
		object->colora = 192;
#ifdef MASS
		object->mass = 1;
#endif
		object->inactivatemode = IM_KILL;
		break;
	case OT_CRATE:
		object->sprite=R_ACT_CRATE;
		object->decx = 0.25*UNIT;
		object->gravy = GRAVITY;
		object->collisiontype = CT_BOUNCY;
#ifdef SHUFCOLLIDE
		object->objcollisiontype = CT_SLIDE;
#endif
		object->height = 64*UNIT;
		object->width = 60*UNIT;
		//object->momrot = 0.5*UNIT;
#ifdef SHUFCOLLIDE
//		object->justspawned = 1;
#ifdef MASS
		object->mass = 20;
#endif
#endif
		break;
	case OT_POLYTEST:
		object->sprite=R_ACT_POLYTEST;
		object->drawstyle=DS_3DQUAD;
		object->gravy = 0;
		object->height = 256*UNIT;
		object->width = 256*UNIT;
		object->momrot = 2*UNIT;
		object->collisiontype = CT_NONE;
		object->colorr = 255;
#ifdef SHUFCOLLIDE
		object->objcollisiontype = CT_NONE;
#endif
		object->inactivatemode = IM_SLEEP;
		break;
	default:

		break;
	}
}
