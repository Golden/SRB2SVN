// BlitzzEngine main source file
// By FoxBlitzz
//
// Outlines the bare skeleton of the game. It initiates the main system,
// loads a level, and then runs a main game loop using the tic system.
//
// If you are adding a new thing to the game loop, I strongly recommend
// AGAINST adding it in here. Doing so will usually lead to odd side-effects.
//
// If it has to do with drawing, add it in game_render(), located in coregfx.c.
// Remember to place it after acquire_bitmap(buffer); or else your graphics
// won't draw with correct timing - in pageflip mode, they won't even appear!
//
// For gameplay-related items, add it into game_process(), located in
// coreproc.c. For inputs, add your entry into game_getinput(), located in
// coreinput.c, somewhere after poll_keyboard();.


#include <stdio.h>
#include <allegro.h>

#include "gamesys.h"
#include "object.h"
#include "config.h"
#include "coresound.h"
#include "coregfx.h"
#include "coreproc.h"
#include "coreinput.h"

#include "resources.h"
#include "r_ui.h"

volatile int game_end=0;
volatile int game_nextzone=0;
int game_showwarning=1;
int game_gfxwidth=640;
int game_gfxheight=480;
int game_gfxscale=0;
int game_gfxdepth=0;
int game_gfxdriver;
int game_gfxupdate=0;
int game_gfxlimit=0;
int game_vsync=0;
int game_sounddriver=DIGI_AUTODETECT;

int main()
{

	game_initsystem();

	do
	{
		game_nextzone = 0;

		//Load gameplay
		game_prepare();

		effect_fade_spin(0);

		do
		{
			if (game_ticskip>0 || game_gfxlimit==GRFX_NOLIMITFPS)
			{
				//Main game loop. Do NOT add new items here. Add them into the
				//respective existing functions below. More information above.
				if (game_ticskip>9)
					game_ticskip=9;

				game_timerest=game_ms; // Get timing information

				game_ms=0; // Reset timer
				while (game_ticskip>0)
				{
					game_getinput();
					game_process();

					game_ticskip--;
				}
				game_timelogic=game_ms; // Get timing information
				if (game_timelogic>=50)
				{
					sdfx_play(r_ui[R_UI_BEEPERROR].dat,SP_NORMAL,255,128,1000,0);
					game_printf("\nWARNING: Hitch detected in game logic! Check your code! Hitch lasted: %i ms", game_timelogic);
				}

				game_ms=0; // Reset timer
				game_render();
				game_timeupdate=game_ms; // Get timing information

				sdfx_poll();

				game_ms=0; // Reset timer
				if (game_restcpu)
					rest(game_restcpu);
				else if (game_gfxlimit==GRFX_LIMITFPS && game_ticskip == 0)
					rest(1);
			}

		} while (game_nextzone == 0 && game_end == 0);

		game_printf("\nGame Flow: Traveling to Zone %i...\n",game_nextzone);

		effect_fade_spin(1);

		game_unprepare();

	} while (game_end == 0);

	game_killsystem();
	return 0;
}

END_OF_MAIN()
