// Input functions
// By FoxBlitzz
// This part of the main loop deals with the keyboard and joystick. If you are
// to add more key controls, they would ideally go in front of poll_keyboard();.
// On some platforms, you must poll the input device before it actually grabs
// the input. Then, there is a short amount of time for it to read all of your
// keystrokes before it is called again. Adding Allegro's key[]; outside of here
// may result in the key not responding on certain platforms.

#include <allegro.h>
#include "coreinput.h"
#include "gamesys.h"

#define REPEATTIME 20
#define REPEATDELAY 4

static int input_bind[NUMKEYS] =
{KEY_LEFT, KEY_RIGHT, KEY_UP, KEY_DOWN, KEY_SPACE, KEY_Z, KEY_X};

char input_game[NUMKEYS];
char input_menu;

char input_repeatpos;
static char repeatspot;

void game_getinput(void)
{
	int i;
	int anykeys = 0;

	poll_keyboard();

	input_repeatpos++;

	for (i=0; i<NUMKEYS; i++)
	{
												// Also allow Enter to select menu items
		if (input_game[i] == 0 && input_menu != i && (key[input_bind[i]] || (i == IK_KEYJUMP && key[KEY_ENTER])))
		{
			input_menu = i;
			input_repeatpos = 0;
			repeatspot = REPEATTIME;
		}

		if (input_game[i] != 0 && input_menu == i && !(key[input_bind[i]] || (i == IK_KEYJUMP && key[KEY_ENTER])))
			anykeys = NUMKEYS;

		if (input_game[i] != 2)
			input_game[i] = 1;

		if (input_repeatpos > repeatspot)
		{
			input_repeatpos -= repeatspot;
			repeatspot = REPEATDELAY;
		}

		if (!key[input_bind[i]])
		{
			input_game[i] = 0;
			if (i != IK_KEYJUMP || !key[KEY_ENTER]) // Also allow Enter to select menu items
				anykeys++;
		}
	}

	if (anykeys >= NUMKEYS)
	{
		input_menu = -1;
		input_repeatpos = 0;
		repeatspot = REPEATTIME;
	}

	if (key[KEY_F4] && key_shifts & KB_ALT_FLAG)
		game_end = 1;

}
