/* Allegro datafile object indexes, produced by grabber v4.2.2, MinGW32 */
/* Datafile: g:\Work\C\BlitzzEngine\data\world0.dat */
/* Date: Sat Apr 19 19:17:42 2008 */
/* Do not hand edit! */

#define R_WRL_BACK1                      0        /* BMP  */
#define R_WRL_TILE000                    1        /* RLE  */
#define R_WRL_TILE001                    2        /* RLE  */
#define R_WRL_TILE002                    3        /* RLE  */
#define R_WRL_TILE003                    4        /* RLE  */
#define R_WRL_TILE004                    5        /* RLE  */
#define R_WRL_TILE005                    6        /* RLE  */
#define R_WRL_TILE006                    7        /* RLE  */
#define R_WRL_TILE007                    8        /* RLE  */
#define R_WRL_TILE008                    9        /* RLE  */
#define R_WRL_TILE009                    10       /* RLE  */
#define R_WRL_TILE010                    11       /* RLE  */
#define R_WRL_TILE011                    12       /* RLE  */
#define R_WRL_TILE012                    13       /* RLE  */
#define R_WRL_TILE013                    14       /* RLE  */
#define R_WRL_TILE014                    15       /* RLE  */
#define R_WRL_COUNT                      16

