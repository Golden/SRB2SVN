#include <allegro.h>

#define GAMEWIDTH 640
#define GAMEHEIGHT 480

#define UNIT 65536
#define TILESIZE 128

#define GAME_HIDEWARNING 0
#define GAME_SHOWWARNING 1

#define GRFX_CENTER 0
#define GRFX_RESIZE 1
#define GRFX_TEMPORAL 2

#define GRFX_DBUFFER 0
#define GRFX_PAGEFLIP 1
#define GRFX_TBUFFER 2

#define GRFX_NOLIMITFPS 0
#define GRFX_LIMITFPS 1

#define GRFX_NOVSYNC 0
#define GRFX_VSYNC 1

#define GRFX_8BITAPRECISION 16

#define UI_GAMEWORLD 1
#define UI_HUD 2
#define UI_DEBUG 4
#define UI_MENU 8
#define UI_CONSOLE 16
#define UI_DEBUGWORLD 32

//Game system
extern volatile int game_end;
extern volatile int game_nextzone;

extern int game_showwarning;
extern int game_gfxwidth;
extern int game_gfxheight;
extern int game_gfxscale;
extern int game_gfxdepth;
extern int game_gfxdriver;
extern const char *getgfxdriver;
extern int game_gfxupdate;
extern int game_gfxlimit;
extern int game_vsync;
extern int game_sounddriver;
extern const char *getsounddriver;
#ifdef ALLEGRO_WINDOWS
extern int getsoundcard;
#endif

extern const char *game_datapath;

extern char consbuf[300];

//Game timer
extern volatile char game_ticskip;
extern volatile int game_tics;
extern volatile int game_runtics;
extern void inc_game_timer(void);
//FPS timer
extern volatile int game_fps;
extern volatile int game_curfps;
extern volatile int game_lastfps;
extern void inc_game_fps(void);
//Millisecond-resolution timer
extern volatile int game_ms;
extern void inc_game_ms(void);

extern char game_run;
extern char game_taskswitch;

//Timed information
extern int game_timerender;
extern int game_timeupdate;
extern int game_timelogic;
extern int game_timerest;

//Graphics
extern BITMAP *buffer;
extern BITMAP *page1;
extern BITMAP *page2;
extern BITMAP *page3;
extern BITMAP *accumulate;
extern BITMAP *snapshot;
extern COLOR_MAP transtable[GRFX_8BITAPRECISION];


extern unsigned char uiflags;

//Sound


//Stuff...
void game_showsucc(void);
void game_showfail(void);
void game_showwarn(const char* format, ...);
int game_showerror(const char* format, ...);
void game_printf(const char *format, ...);

void game_initsystem(void);
void game_prepare(void);
void game_unprepare(void);
void game_killsystem(void);
int game_setmode(int depth);
void game_render(void);

DATAFILE *game_loaddata(const char *datafile);

extern unsigned char game_restcpu;
