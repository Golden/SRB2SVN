// Menu handling functions
// By FoxBlitzz
// Deals with various game menus and the structures that hold their data
// Special thanks to Oogaland and Damizean for helping me understand the pointer
// side of things.

#include "menu.h"
#include "gamesys.h"
#include "coregfx.h"
#include "coreinput.h"
#include "object.h"
#include "coresound.h"

#include "resources.h"
#include "r_ui.h"

menu_t *menu_position = menulist_home;
int menu_selection = 0;
menu_t *menu_history[10] =
{menulist_home, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL};
int menu_historypos = 0;

static int somevalue=42; // For testing purposes

static int menufunc_quit(void)
{
	game_end = 1;
	return 0;
}

static int menufunc_newgame(void)
{
	game_nextzone = 1;
	return 0;
}

static int menufunc_unpause(void)
{
	input_game[IK_KEYJUMP] = 2;
	game_run = 1;
	return 1;
}

menu_t menulist_test[] =
{
	{ (16*5),                (16*8),    "LIMIT FPS                ",        MI_BOOLEAN,  NULL, NULL, &game_gfxlimit, 1},
	{ (16*5),                (16*9),    "USE VSYNC                ",        MI_BOOLEAN,  NULL, NULL, &game_vsync,    1},
	{ (16*5),                (16*10),   "FOOBAR                   ",        MI_VARIABLE, NULL, NULL, &somevalue,     0},
	{ (16*5),                (16*13),   "/BACK",                            MI_GOBACK,   NULL, NULL, NULL,           0},
	{ (GAMEWIDTH-(16*29))/2, (16*18),   "abcdYET ANOTHER TEST MENUabcd",    MI_STATIC,   NULL, NULL, NULL,           0},
	{ (GAMEWIDTH-(16*32))/2, (16*20),   "TIP - HOLD SPACE Z AND OR X KEYS", MI_STATIC,   NULL, NULL, NULL,           0},
	{ (GAMEWIDTH-(16*26))/2, (16*21),   "TO CHANGE VARIABLES FASTER",       MI_STATIC,   NULL, NULL, NULL,           0},
	{ 0,                     0,         NULL,                               MI_STATIC,   NULL, NULL, NULL,           0}
};

menu_t menulist_options[] =
{
	{ (16*5),                (16*7),    "DEBUG MODE               ",           MI_BOOLEAN,  NULL, NULL,          &uiflags,           UI_DEBUG},
	{ (16*5),                (16*8),    "DEBUG WORLD MODE         ",           MI_BOOLEAN,  NULL, NULL,          &uiflags,           UI_DEBUGWORLD},
	{ (16*5),                (16*9.5),  "POINT-OF-NO-SCROLL       ",           MI_BOOLEAN,  NULL, NULL,          &grfx_noedgescroll, 1},
	{ (16*5),                (16*11),   "FOOBAR                   ",           MI_VARIABLE, NULL, NULL,          &somevalue,         0},
	{ (16*5),                (16*12),   "ANOTHER SUBMENU",                     MI_SUBMENU,  NULL, menulist_test, NULL,               0},
	{ (16*5),                (16*14),   "/BACK",                               MI_GOBACK,   NULL, NULL,          NULL,               0},
	{ (GAMEWIDTH-(16*35))/2, (16*20),   "THIS IS A TEST MENU. THE ITEMS HERE", MI_STATIC,   NULL, NULL,          NULL,               0},
	{ (GAMEWIDTH-(16*18))/2, (16*21),   "ARE SET AS STATIC.",                  MI_STATIC,   NULL, NULL,          NULL,               0},
	{ (GAMEWIDTH-(16*24))/2, (16*22.5), "THEY CANNOT BE SELECTED.",            MI_STATIC,   NULL, NULL,          NULL,               0},
	{ 0,                     0,         NULL,                                  MI_STATIC,   NULL, NULL,          NULL,               0}
};


menu_t menulist_home[] =
{
	{ (GAMEWIDTH-(16*11))/2, (16*16.5), "RESUME GAME", MI_CALLFUNC, menufunc_unpause, NULL,             NULL, 0},
	{ (GAMEWIDTH-(16*8))/2, (16*18),   "NEW GAME",    MI_CALLFUNC, menufunc_newgame, NULL,             NULL, 0},
	{ (GAMEWIDTH-(16*7))/2,  (16*19),   "OPTIONS",     MI_SUBMENU,  NULL,             menulist_options, NULL, 0},
	{ (GAMEWIDTH-(16*4))/2,  (16*20.5), "QUIT",        MI_CALLFUNC, menufunc_quit,    NULL,             NULL, 0},
	{ 0,                     0,         NULL,          MI_STATIC,   NULL,             NULL,             NULL, 0}
};


void menu_think(void)
{
	int menusize;
	int *menuvar = (int*)(void*)menu_position[menu_selection].itemvar;
	int setfactor;

	menusize = 0;

	setfactor = 1;

	while (menu_position[menusize].text)
	{
		menusize++;
	}

	if (input_game[IK_KEYJUMP])
		setfactor = setfactor * 5;
	if (input_game[IK_KEYATCK])
		setfactor = setfactor * 10;
	if (input_game[IK_KEYSPEC])
		setfactor = setfactor * 50;

	if (input_repeatpos == 1)
	{
		switch (input_menu)
		{
			case IK_KEYUP:
				sdfx_play(r_ui[R_UI_MENUSELECT].dat,SP_INTERRUPT,255,128,1000,0);

				do
				{
					if (menu_selection > 0)
						menu_selection--;
					else
						menu_selection = menusize-1;
				} while (menu_position[menu_selection].itemtype == MI_STATIC);
				break;

			case IK_KEYDOWN:
				sdfx_play(r_ui[R_UI_MENUSELECT].dat,SP_INTERRUPT,255,128,1000,0);

				do
				{
					if (menu_selection < menusize-1)
						menu_selection++;
					else
						menu_selection = 0;
				} while (menu_position[menu_selection].itemtype == MI_STATIC);
				break;

			case IK_KEYLEFT:
				switch (menu_position[menu_selection].itemtype)
				{
					case MI_VARIABLE:
						*menuvar = *menuvar - setfactor;
						sdfx_play(r_ui[R_UI_MENUSELECT].dat,SP_INTERRUPT,255,128,1000,0);
						break;

					case MI_BOOLEAN:
						*menuvar ^= menu_position[menu_selection].boolbit;
						sdfx_play(r_ui[R_UI_MENUSELECT].dat,SP_INTERRUPT,255,128,1000,0);
						break;
				}
				break;

			case IK_KEYRIGHT:
				switch (menu_position[menu_selection].itemtype)
				{
					case MI_VARIABLE:
						*menuvar = *menuvar + setfactor;
						sdfx_play(r_ui[R_UI_MENUSELECT].dat,SP_INTERRUPT,255,128,1000,0);
						break;

					case MI_BOOLEAN:
						*menuvar ^= menu_position[menu_selection].boolbit;
						sdfx_play(r_ui[R_UI_MENUSELECT].dat,SP_INTERRUPT,255,128,1000,0);
						break;
				}
				break;

			case IK_KEYJUMP:
				switch (menu_position[menu_selection].itemtype)
				{
					case MI_CALLFUNC:
						if (menu_position[menu_selection].proc() == 1)
						{
						sdfx_play(r_ui[R_UI_MENUCHOOSE].dat,SP_INTERRUPT,255,128,1000,0);
						}
						break;

					case MI_SUBMENU:
						menu_position = menu_position[menu_selection].target;
						menu_selection = 0;
						menu_historypos++;
						menu_history[menu_historypos] = menu_position;

						sdfx_play(r_ui[R_UI_MENUCHOOSE].dat,SP_INTERRUPT,255,128,1000,0);
						break;

					case MI_GOBACK:
						menu_historypos--;
						menu_position = menu_history[menu_historypos];
						menu_selection = 0;

						sdfx_play(r_ui[R_UI_MENUBACK].dat,SP_INTERRUPT,255,128,1000,0);
						break;

					case MI_BOOLEAN:
						*menuvar ^= menu_position[menu_selection].boolbit;
						sdfx_play(r_ui[R_UI_MENUSELECT].dat,SP_INTERRUPT,255,128,1000,0);
						break;
				}
				break;
		}
	}
}
