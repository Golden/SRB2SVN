#include <allegro.h>

#include "gamesys.h"
#include "coregfx.h"
#include "coresound.h"

#include "resources.h"
#include "r_ui.h"

static fixed rotation;
static fixed size;

void effect_fade_spin(int mode)
{
	BITMAP *drawtarget;

	if (!is_video_bitmap(buffer))
		drawtarget = buffer;
	else
		drawtarget = accumulate;


	if (mode)
	{
		game_printf("\nGame Flow: Fading out...\n");
		size = 1*UNIT;
		rotation = 0;
		sdfx_play(r_ui[R_UI_MENUCHOOSE].dat,SP_INTERRUPT,255,128,1000,0);
		sdfx_fadesong(720);
		blit(buffer,snapshot,0,0,0,0,GAMEWIDTH,GAMEHEIGHT);
	}
	else
	{
		game_printf("\nGame Flow: Fading in...\n");
		size = 0;
		rotation = 206*UNIT;
		grfx_renderworld(snapshot,scrollx,scrolly,RFX_NONE,NULL);
	}


	clear_to_color(drawtarget, makecol(0,0,0));

	do
	{
		if (game_ticskip>0) {

			if (game_ticskip>9)
				game_ticskip=9;

			while (game_ticskip>0)
			{
				rotation+=1*UNIT;
				if (mode)
					size-=0.03*UNIT;
				else
					size+=0.02*UNIT;

				game_ticskip--;
			}


			acquire_bitmap(drawtarget);

			if (mode)
			{
				color_map = &transtable[GRFX_8BITAPRECISION/3];
				set_trans_blender(32, 32, 32, 32);

				drawing_mode(DRAW_MODE_TRANS,drawtarget,0,0);
				rectfill(drawtarget,0,0,GAMEWIDTH-1,GAMEHEIGHT-1,makecol(0,0,0));
				drawing_mode(DRAW_MODE_SOLID,drawtarget,0,0);
			}

			if (size>0)
			rotate_scaled_sprite(drawtarget, snapshot, ((GAMEWIDTH/2)*(UNIT-(size)))/UNIT, ((GAMEHEIGHT/2)*(UNIT-(size)))/UNIT, rotation, size);

			if (is_video_bitmap(buffer))
				blit(drawtarget,buffer,0,0,0,0,GAMEWIDTH,GAMEHEIGHT);

			grfx_refresh();

			release_bitmap(drawtarget);

			sdfx_poll();



		}
	}	while ((mode && size>-0.5*UNIT) || (!mode && size<1*UNIT));
}
