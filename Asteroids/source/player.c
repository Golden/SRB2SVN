#include <PA9.h>
#include <math.h>
#include "player.h"
#include "defines.h"
#include "shoot.h"
#include "mobj.h"

extern player_t player;
extern bool aiming;

// Misc player stuff goes here, such as animations, cheat codes, and actions.
void PlayerThink(void)
{
   static byte i = 0;
   if(!player.alive)
   	return;

   if(!Stylus.Held)
   	PA_StartSpriteAnim(0, 127, 0, 0, 1);
   else if(Stylus.Newpress)
		PA_StartSpriteAnimEx(0, 127, 0, 2, 20, ANIM_UPDOWN, -1);
   
  	if(Pad.Newpress.L || Pad.Newpress.R)
   {
      PA_PlaySimpleSound(PA_GetFreeSoundChannel(), shoot);
   	ShootBullet();
 	}  	
  
 	if(TouchButtonCheck(MT_AIMBUTTON) && player.missiles && !aiming)
   	aiming = true;  

   if(Pad.Newpress.Right && (i == 0 || i == 2))
		i++;
	else if(Pad.Newpress.Up && i == 1)
		i++; 
	else if(Pad.Newpress.A && i == 3)
		i++; 
	else if (Pad.Newpress.Down && i >= 4)
		i++;
	else if(Pad.Newpress.A || Pad.Newpress.B || Pad.Newpress.X || Pad.Newpress.Y
	 || Pad.Newpress.L || Pad.Newpress.R
	 || Pad.Newpress.Left || Pad.Newpress.Right
	 || Pad.Newpress.Up || Pad.Newpress.Down)
		i = 0;
		
	if (i >= 6)
		PA_OutputText(1, 5, 17, "RURADD? Activated!", i);	
}

// MovePlayer handles player movement. NOTHING ELSE! I don't want it to look like SRB2's P_PlayerMove
void MovePlayer(void)
{
   s32 newx = 0, newy = 0, dist = 0;
   s32 x, y, momx, momy;
   u16 moveangle = 0, angle = 0;
   
   if(!player.alive)
   	return;
   	
   x = player.x;
   y = player.y;
   momx = player.momx;
   momy = player.momy;
   angle = player.angle;
   
   if (Stylus.Held) // Move forward
		{ 
			angle = PA_GetAngle(x>>8, y>>8, Stylus.X, Stylus.Y);
			PA_SetRotsetNoZoom(0, 0, angle); // Turn the ship in the correct direction
			
			// Divide it so it's not instant acceleration
			momx += PA_Cos(angle)/40;
			momy -= PA_Sin(angle)/40;
		}

		newx = x + momx;
		newy = y + momy;
		
		if(newx>x && newy>y)
			dist = sqrt((((newx-x)^2)+((newy-y)^2)));
		else if(newx>x && newy<y)
			dist = sqrt((((newx-x)^2)+((y-newy)^2)));
		else if(newx<x && newy>y)
			dist = sqrt((((x-newx)^2)+((newy-y)^2)));
		else if(newx<x && newy<y)
			dist = sqrt((((x-newx)^2)+((y-newy)^2)));
		
		if(dist > MAXMOVE)
		{
		   moveangle = PA_GetAngle(newx>>8, newy>>8, x>>8, y>>8);
		   momx += PA_Cos(moveangle)/40;
		   momy -= PA_Sin(moveangle)/40;
		   x += momx;
		   y += momy;
		   
			dist = MAXMOVE;
		}					
		else
		{
			x = newx;
			y = newy;	
		}		
		
		// Teleport to the other side of the map.		
		if(x > BORDERRIGHT + 16*UNIT)
			x = BORDERLEFT - 16*UNIT;
		else if(x < BORDERLEFT - 16*UNIT)
			x = BORDERRIGHT + 16*UNIT;
			
		if(y < BORDERTOP - 16*UNIT)
			y = BORDERBOTTOM + 16*UNIT;
		else if(y > BORDERBOTTOM + 16*UNIT)
			y = BORDERTOP - 16*UNIT;
		
		player.x = x;
		player.y = y;
		player.momx = momx;
		player.momy = momy;
		player.angle = angle;
		
		PA_SetSpriteXY(0, 127, (x>>8)-16, (y>>8)-16); // Sprite position converted to normal...
}
