typedef enum
{
   MT_BULLET,
   MT_LARGEMETROID,
   MT_MEDIUMMETROID,
   MT_SMALLMETROID,
   MT_LOCKON,
   MT_AIMBUTTON,
   MT_FIREBUTTON,
   MT_CANCELBUTTON,
   MT_MISSILE
}type_t;

typedef struct mobj_s
{
   s32 x;
   s32 y;
   u16 angle;
   byte momx;
   byte momy;
   s32 radius;
   byte speed;
   s16 fuse; // Tic.. tic.. tic..
   byte spritesizex;
   byte spritesizey;
   bool alive; // 0 is dead, 1 is alive
   byte spritenum;
   bool locked; // This enemy has a missile lock-on on it.
   bool exists; // Mobj and sprite already exist..
   struct mobj_s* target;
   
   type_t type;
}mobj_t;

void MoveMobj(mobj_t* mobj);
mobj_t* SpawnMobj(s32 x, s32 y, u16 angle, s32 radius, type_t type);
void CheckCollisions(mobj_t* mobj);
void KillMobj(mobj_t* mobj);
int FindSlot(void);
void InitPalettes(void);
void InitBackgrounds(void);
void ShootBullet(void);
void SpawnMetroid(byte size, s32 x, s32 y);
void SetSpriteToMobjXY(byte screen, mobj_t* mo);
void FindTarget(mobj_t* mobj);
