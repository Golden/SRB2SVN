//Gfx converted using Mollusk's PAGfx Converter

//This file contains all the .c, for easier inclusion in a project

#ifdef __cplusplus
extern "C" {
#endif

#include "all_gfx.h"


// Sprite files : 
#include "ship.c"
#include "bullet.c"
#include "lock.c"
#include "metroidm.c"
#include "metroids.c"
#include "missile.c"
#include "Aim.c"
#include "cancel.c"
#include "fire.c"
#include "metroidl.c"

// Background files : 
#include "black.c"

// Palette files : 
#include "ship.pal.c"
#include "bullet.pal.c"
#include "lock.pal.c"
#include "metroidm.pal.c"
#include "metroids.pal.c"
#include "missile.pal.c"
#include "Aim.pal.c"
#include "cancel.pal.c"
#include "fire.pal.c"
#include "metroidl.pal.c"
#include "black.pal.c"

// Background Pointers :
PAGfx_struct black = {(void*)black_Map, 768, (void*)black_Tiles, 64, (void*)black_Pal, (int*)black_Info };


#ifdef __cplusplus
}
#endif

