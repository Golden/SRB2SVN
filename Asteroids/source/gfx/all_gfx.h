//Gfx converted using Mollusk's PAGfx Converter

//This file contains all the .h, for easier inclusion in a project

#ifndef ALL_GFX_H
#define ALL_GFX_H

#ifndef PAGfx_struct
    typedef struct{
    void *Map;
    int MapSize;
    void *Tiles;
    int TileSize;
    void *Palette;
    int *Info;
} PAGfx_struct;
#endif


// Sprite files : 
extern const unsigned char ship_Sprite[3072] __attribute__ ((aligned (4))) ;  // Pal : ship_Pal
extern const unsigned char bullet_Sprite[64] __attribute__ ((aligned (4))) ;  // Pal : bullet_Pal
extern const unsigned char lock_Sprite[32768] __attribute__ ((aligned (4))) ;  // Pal : lock_Pal
extern const unsigned char metroidm_Sprite[256] __attribute__ ((aligned (4))) ;  // Pal : metroidm_Pal
extern const unsigned char metroids_Sprite[64] __attribute__ ((aligned (4))) ;  // Pal : metroids_Pal
extern const unsigned char missile_Sprite[2048] __attribute__ ((aligned (4))) ;  // Pal : missile_Pal
extern const unsigned char Aim_Sprite[2048] __attribute__ ((aligned (4))) ;  // Pal : Aim_Pal
extern const unsigned char cancel_Sprite[256] __attribute__ ((aligned (4))) ;  // Pal : cancel_Pal
extern const unsigned char fire_Sprite[2048] __attribute__ ((aligned (4))) ;  // Pal : fire_Pal
extern const unsigned char metroidl_Sprite[1024] __attribute__ ((aligned (4))) ;  // Pal : metroidl_Pal

// Background files : 
extern const int black_Info[3]; // BgMode, Width, Height
extern const unsigned short black_Map[768] __attribute__ ((aligned (4))) ;  // Pal : black_Pal
extern const unsigned char black_Tiles[64] __attribute__ ((aligned (4))) ;  // Pal : black_Pal
extern PAGfx_struct black; // background pointer


// Palette files : 
extern const unsigned short ship_Pal[10] __attribute__ ((aligned (4))) ;
extern const unsigned short bullet_Pal[2] __attribute__ ((aligned (4))) ;
extern const unsigned short lock_Pal[2] __attribute__ ((aligned (4))) ;
extern const unsigned short metroidm_Pal[4] __attribute__ ((aligned (4))) ;
extern const unsigned short metroids_Pal[4] __attribute__ ((aligned (4))) ;
extern const unsigned short missile_Pal[14] __attribute__ ((aligned (4))) ;
extern const unsigned short Aim_Pal[36] __attribute__ ((aligned (4))) ;
extern const unsigned short cancel_Pal[4] __attribute__ ((aligned (4))) ;
extern const unsigned short fire_Pal[46] __attribute__ ((aligned (4))) ;
extern const unsigned short metroidl_Pal[4] __attribute__ ((aligned (4))) ;
extern const unsigned short black_Pal[2] __attribute__ ((aligned (4))) ;


#endif

