typedef struct
{
	u8 score; // Player' score.
	s32 x;
   s32 y;
   u16 angle;
   s32 momx;
   s32 momy;
   s32 radius;
   byte missiles;
   byte guns;
   bool exists;
   bool alive;
   byte lockon;
}player_t;

void PlayerThink(void);
void MovePlayer(void);


