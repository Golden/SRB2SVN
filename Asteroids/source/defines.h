#define MAXMOVE 25
#define UNIT 256
// Screen borders
#define BORDERLEFT 0*UNIT 
#define BORDERRIGHT 256*UNIT
#define BORDERTOP 0*UNIT
#define BORDERBOTTOM 192*UNIT
#define MAXMOBJS 126
#define TICRATE 60

// Metroid sizes
#define METLARGE 3 
#define METMEDIUM 2
#define METSMALL 1

// GFX Numbers
#define SHIP 0
#define BULLET 1
#define METROIDL 2
#define METROIDM 3
#define METROIDS 4
#define LOCK 5
#define MISSILE 6
#define AIM 7
#define CANCEL 8
#define FIRE 9
