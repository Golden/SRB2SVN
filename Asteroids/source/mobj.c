#include <PA9.h>
#include "main.h"
#include "player.h"
#include "mobj.h"
#include "defines.h"
#include "explode.h"
#include "lockon.h"

// PAGfxConverter Include
#include "gfx/all_gfx.c"
#include "gfx/all_gfx.h"

extern byte objects;
extern byte curslot;
extern mobj_t mobjlist[];
extern player_t player;
extern bool buttondrawn;
extern bool firebuttondrawn;
u16 graffix[10];

// all_gfx.c is included here, so yes, there's a reason for it being in the wrong file..
void InitPalettes(void)
{
	PA_LoadSpritePal(0, 1, (void*)ship_Pal);
	PA_LoadSpritePal(0, 0, (void*)bullet_Pal);
	PA_LoadSpritePal(0, 2, (void*)metroidl_Pal);
	PA_LoadSpritePal(0, 3, (void*)metroidm_Pal); 
	PA_LoadSpritePal(0, 4, (void*)lock_Pal);
	PA_LoadSpritePal(0, 5, (void*)missile_Pal);
	PA_LoadSpritePal(0, 6, (void*)Aim_Pal);
	PA_LoadSpritePal(0, 7, (void*)cancel_Pal);
	PA_LoadSpritePal(0, 8, (void*)metroids_Pal); 
	PA_LoadSpritePal(0, 9, (void*)fire_Pal);   
}
// Also here.. same reason.
void InitBackgrounds(void)
{
	PA_EasyBgLoad(0, // Screen
   				0, // BG number
   				black); // Name
   PA_EnableSpecialFx(0, // Screen
							SFX_ALPHA, // Alpha blending mode
							SFX_BG0, // Nothing
							SFX_BG0 | SFX_BG1 | SFX_BG2 | SFX_OBJ | SFX_BG3); // Everything normal
	PA_SetSFXAlpha(0, // Screen
						15, // Alpha level, 0-15
						5); // DON'T leave this at 15. Wut u talkin' bout, willis?!   
}
// And again..
void InitGFX(void)
{
	graffix[0] = PA_CreateGfx(0, (void*)ship_Sprite, OBJ_SIZE_32X32, 1);
	graffix[1] = PA_CreateGfx(0, (void*)bullet_Sprite, OBJ_SIZE_8X8, 1);
	graffix[2] = PA_CreateGfx(0, (void*)metroidl_Sprite, OBJ_SIZE_32X32, 1);
	graffix[3] = PA_CreateGfx(0, (void*)metroidm_Sprite, OBJ_SIZE_16X16, 1);
	graffix[4] = PA_CreateGfx(0, (void*)metroids_Sprite, OBJ_SIZE_8X8, 1);
	graffix[5] = PA_CreateGfx(0, (void*)lock_Sprite, OBJ_SIZE_64X64, 1);
	graffix[6] = PA_CreateGfx(0, (void*)missile_Sprite, OBJ_SIZE_32X32, 1);
	graffix[7] = PA_CreateGfx(0, (void*)Aim_Sprite, OBJ_SIZE_64X32, 1);
	graffix[8] = PA_CreateGfx(0, (void*)cancel_Sprite, OBJ_SIZE_16X16, 1);
	graffix[9] = PA_CreateGfx(0, (void*)fire_Sprite, OBJ_SIZE_64X32, 1);
}
// Find the next available mobjlist slot and return it.
int FindSlot(void)
{
   byte i = 0;
	for(i=0;i<MAXMOBJS;i++)
	{
	   if(mobjlist[i].alive)
	   	continue;
	   
	   memset(&mobjlist[i], 0, sizeof(mobj_t));
	   mobjlist[i].spritenum = curslot = i;
		break;			
	}	
	return i;
}
// Move a mobj. Called once per tic.
void MoveMobj(mobj_t* mobj)
{			
	mobj->x += PA_Cos(mobj->angle)*mobj->speed/2;
	mobj->y -= PA_Sin(mobj->angle)*mobj->speed/2;

	// Teleport to the other side of the map.		
	if(mobj->x > BORDERRIGHT + mobj->spritesizex/2*UNIT)
		mobj->x = BORDERLEFT	- mobj->spritesizex/2*UNIT;
	else if(mobj->x < BORDERLEFT - mobj->spritesizex/2*UNIT)
		mobj->x = BORDERRIGHT + mobj->spritesizex/2*UNIT;
		
	if(mobj->y < BORDERTOP - mobj->spritesizey/2*UNIT)
		mobj->y = BORDERBOTTOM + mobj->spritesizey/2*UNIT;
	else if(mobj->y > BORDERBOTTOM + mobj->spritesizey/2*UNIT)
		mobj->y = BORDERTOP - mobj->spritesizey/2*UNIT;
	
	SetSpriteToMobjXY(0, mobj); 
}

mobj_t* SpawnMobj(s32 x, s32 y, u16 angle, s32 radius, type_t type)
{
   mobj_t* mobj;

	mobj = &mobjlist[FindSlot()];
	mobj->type = type;
	
	switch(mobj->type)
	{
		case MT_LARGEMETROID:
		   mobj->spritesizex = mobj->spritesizey = 32;
		   if(!mobj->exists)
				PA_CreateSpriteFromGfx(0, // Screen
											mobj->spritenum, // Sprite number..
											graffix[METROIDL], // Image
											OBJ_SIZE_32X32, 1, 2, -255, -255);
			else
				PA_SetSpriteGfx(0, mobj->spritenum, graffix[METROIDL]);
			mobj->fuse = -1;
			mobj->speed = 1;	
			PA_SetSpriteRotEnable(0, mobj->spritenum, 3);// Rotset 3
		   break;
		case MT_MEDIUMMETROID:
		   mobj->spritesizex = mobj->spritesizey = 16;
		   if(!mobj->exists)
		 	  PA_CreateSpriteFromGfx(0, // Screen
											mobj->spritenum, // Sprite number..
											graffix[METROIDM], // Image
											OBJ_SIZE_16X16, 1, 3, -255, -255);
				else
				PA_SetSpriteGfx(0, mobj->spritenum, graffix[METROIDM]);
			mobj->fuse = -1;
			mobj->speed = 1;
			PA_SetSpriteRotEnable(0, mobj->spritenum, 3);// Rotset 3
		   break;
		case MT_SMALLMETROID:
	  		mobj->spritesizex = mobj->spritesizey = 8;
	  		if(!mobj->exists)
	  			PA_CreateSpriteFromGfx(0, // Screen
											mobj->spritenum, // Sprite number..
											graffix[METROIDS], // Image
											OBJ_SIZE_8X8, 1, 8, -255, -255);
			else
				PA_SetSpriteGfx(0, mobj->spritenum, graffix[METROIDS]);
			mobj->fuse = -1;
			mobj->speed = 2;
			PA_SetSpriteRotEnable(0, mobj->spritenum, 3);// Rotset 3
			break;	
		case MT_BULLET:
			mobj->spritesizex = mobj->spritesizey = 8;
			if(!mobj->exists)
				PA_CreateSpriteFromGfx(0, // Screen
											mobj->spritenum, // Sprite number..
											graffix[BULLET], // Image
											OBJ_SIZE_8X8, 1, 0, -255, -255);
			else
				PA_SetSpriteGfx(0, mobj->spritenum, graffix[BULLET]);
			mobj->fuse = TICRATE;
			mobj->speed = 6;
			break;
		case MT_LOCKON:
		   PA_PlaySimpleSound(3, lockon);
	   	mobj->spritesizex = mobj->spritesizey = 64;
	   	if(!mobj->exists)
	   		PA_CreateSpriteFromGfx(0, // Screen
											mobj->spritenum, // Sprite number..
											graffix[LOCK], // Image
											OBJ_SIZE_64X64, 1, 4, -255, -255);
			else
				PA_SetSpriteGfx(0, mobj->spritenum, graffix[LOCK]);
	   	mobj->speed = 0;
	   	mobj->fuse = 1;
		   break;
		case MT_AIMBUTTON:
		   mobj->spritesizex = 64;
		  	mobj->spritesizey = 32;
		  	if(!mobj->exists)
		  		PA_CreateSpriteFromGfx(0, // Screen
											mobj->spritenum, // Sprite number..
											graffix[AIM], // Image
											OBJ_SIZE_64X32, 1, 6, -255, -255);
			else
				PA_SetSpriteGfx(0, mobj->spritenum, graffix[AIM]);
		  	PA_SetSpritePrio(0, mobj->spritenum, 0);
		  	mobj->fuse = -1;
		  	mobj->speed = 0;
		  	buttondrawn = true;
		  	break;
		case MT_CANCELBUTTON:
		   mobj->spritesizex = mobj->spritesizey = 16;
		   if(!mobj->exists)
			   PA_CreateSpriteFromGfx(0, // Screen
											mobj->spritenum, // Sprite number..
											graffix[CANCEL], // Image
											OBJ_SIZE_16X16, 1, 7, -255, -255);
			else
				PA_SetSpriteGfx(0, mobj->spritenum, graffix[CANCEL]);
			PA_SetSpritePrio(0, mobj->spritenum, 0);
			mobj->speed = 0;
			mobj->fuse = 1;
			buttondrawn = true;
		   break;
		case MT_FIREBUTTON:
		   mobj->spritesizex = 64;
		  	mobj->spritesizey = 32;
		  	if(!mobj->exists)
		  		PA_CreateSpriteFromGfx(0, // Screen
											mobj->spritenum, // Sprite number..
											graffix[FIRE], // Image
											OBJ_SIZE_64X32, 1, 9, -255, -255);
			else
				PA_SetSpriteGfx(0, mobj->spritenum, graffix[FIRE]);
		  	PA_SetSpritePrio(0, mobj->spritenum, 0);
		  	mobj->fuse = -1;
		  	mobj->speed = 0;
		  	firebuttondrawn = true;
		  	break;
		case MT_MISSILE:
		   mobj->spritesizex = 32;
			mobj->spritesizey = 32;
		   if(!mobj->exists)
		 	  PA_CreateSpriteFromGfx(0, // Screen
											mobj->spritenum, // Sprite number..
											graffix[MISSILE], // Image
											OBJ_SIZE_32X32, 1, 5, -255, -255);
				else
				PA_SetSpriteGfx(0, mobj->spritenum, graffix[MISSILE]);
			PA_StartSpriteAnimEx(0, // screen
										mobj->spritenum, // sprite number
										0, // first frame
										1, // last frame
										20, // Speed, Frames per second
										ANIM_LOOP, -1); // Play FOREVER.
			mobj->fuse = -1;
			mobj->speed = 1;
		   break;
		default:
		   if(!mobj->exists)
		 	  PA_CreateSpriteFromGfx(0, // Screen
											mobj->spritenum, // Sprite number..
											graffix[CANCEL], // Image
											OBJ_SIZE_16X16, 1, 7, -255, -255);
			else
				PA_SetSpriteGfx(0, mobj->spritenum, graffix[CANCEL]);
		   mobj->fuse = -1;
		   break;
	}			
	
	mobj->x = x;
	mobj->y = y;
	mobj->radius = radius;
	mobj->angle = angle;
	
	mobj->alive = true; // IT'S ALIIIIIVE!
	mobj->exists = true;
	
	objects++;
	return mobj;
}

// Kill a mobj, obviously.
void KillMobj(mobj_t* mobj)
{
	mobj->x = 256;
	mobj->y = 192;
	mobj->alive = false;
	PA_SetSpriteXY(0, mobj->spritenum, 256, 192);
	// Stop any animations the mobj may be going through..
	PA_StopSpriteAnim(0, mobj->spritenum);
	// Bullet is small and never falls over the edge of the screen, so change it to that..
//	PA_SetSpriteGfx(0, mobj->spritenum, graffix[BULLET]);
   objects--;
} 

void CheckCollisions(mobj_t* toucher)
{
   byte i;
   mobj_t* touched;
   for(i=0;i<MAXMOBJS;i++)
   {
   	touched = &mobjlist[i];
   	if(!touched->alive)
      	continue;
      // Within hit distance?
      if(!(PA_Distance(toucher->x, toucher->y, touched->x, touched->y) 
			<= (touched->radius*touched->radius + toucher->radius*toucher->radius)))
      	continue;
      	
	   switch(toucher->type)
	   {
	      case MT_AIMBUTTON:
	      case MT_FIREBUTTON:
	      case MT_CANCELBUTTON:
	         break; // Do nothing.. no collisions
	  		case MT_BULLET:
			   switch(touched->type)
				{ 
		      	case MT_LARGEMETROID:
					   player.score += 500;
					   SpawnMetroid(METMEDIUM, touched->x, touched->y);
			 			SpawnMetroid(METMEDIUM, touched->x, touched->y);
			 			KillMobj(touched);
			 			KillMobj(toucher);
			 			PA_PlaySimpleSound(PA_GetFreeSoundChannel(), explode);
					   break;
					case MT_MEDIUMMETROID:
					   player.score += 250;
					   SpawnMetroid(METSMALL, touched->x, touched->y);
			 			SpawnMetroid(METSMALL, touched->x, touched->y);
			 			KillMobj(touched);
			 			KillMobj(toucher);
			 			PA_PlaySimpleSound(PA_GetFreeSoundChannel(), explode);
			 			break;
					case MT_SMALLMETROID:
					   player.score += 100;
					   KillMobj(touched);
			 			KillMobj(toucher);
			 			PA_PlaySimpleSound(PA_GetFreeSoundChannel(), explode);
					   break;
					default:
					   break;
					   
				}   
				break; // MT_BULLET
			case MT_MISSILE:
				switch(touched->type)
				{
					case MT_LARGEMETROID: // Score + 3000
					   player.score += 1500;
					case MT_MEDIUMMETROID: // Score + 1500
					   player.score += 1000;
					case MT_SMALLMETROID:
					   player.score += 500;
					   KillMobj(touched);
			 			KillMobj(toucher);
			 			PA_PlaySimpleSound(PA_GetFreeSoundChannel(), explode);
			 			break;
			 		default:
						break; 
				}					 
				break; // MT_MISSILE			
			default:	
				break;		
		}	// End switch
	}	// End for loop
		
	// Between Player and Mobj..
   if(PA_Distance(toucher->x, toucher->y, player.x, player.y) <= (player.radius*player.radius + toucher->radius*toucher->radius)
		&& toucher->type != MT_MISSILE && toucher->type != MT_BULLET
		&& toucher->type != MT_AIMBUTTON && toucher->type != MT_CANCELBUTTON
		&& toucher->type != MT_FIREBUTTON
		&& player.alive)
		{
		   KillPlayer();
			InitPlayer();
		}			
}

void ShootBullet(void)
{
   mobj_t* mobj;
   
   if (objects > MAXMOBJS)
   	return;
   
   if(player.guns == 1 || player.guns == 3)
   {
	   mobj = SpawnMobj(player.x, player.y, player.angle, 2*UNIT, MT_BULLET);
	
		mobj->x += PA_Cos(mobj->angle)*10; // Shoot in front of the ship, so you don't shoot yourself..
		mobj->y -= PA_Sin(mobj->angle)*10;
	}		
	
	if(player.guns == 2 || player.guns == 3)
	{
	   // Left Blaster
	   mobj = SpawnMobj(player.x, player.y, player.angle, 2*UNIT, MT_BULLET);
	
		mobj->x += PA_Cos(mobj->angle-127)*10;
		mobj->y -= PA_Sin(mobj->angle-127)*10;
		
	   // Right Blaster
	   mobj = SpawnMobj(player.x, player.y, player.angle, 2*UNIT, MT_BULLET);
	
		mobj->x += PA_Cos(mobj->angle+127)*10;
		mobj->y -= PA_Sin(mobj->angle+127)*10;
	}
}

// X and Y only used on medium and small metroids
void SpawnMetroid(byte size, s32 x, s32 y)
{
   mobj_t* mobj;
   
   if (objects > MAXMOBJS)
   	return;
   	
	if(size == METLARGE)
	   mobj = SpawnMobj(x, y, 0, 16*UNIT, MT_LARGEMETROID);			
	else if(size == METMEDIUM)
	   mobj = SpawnMobj(x, y, 0, 8*UNIT, MT_MEDIUMMETROID);		
	else
	   mobj = SpawnMobj(x, y, 0, 4*UNIT, MT_SMALLMETROID);	
	
	mobj->angle = PA_Rand()%511;
		
}	

// Easier to work with. This way, I don't need to work with bitshifts,
// nor do I need to subtract half of the sprite's size.
void SetSpriteToMobjXY(byte screen, mobj_t* mo)
{
	PA_SetSpriteXY(0, mo->spritenum, (mo->x>>8)-(mo->spritesizex/2), (mo->y>>8)-(mo->spritesizey/2));
}

// Find and set a mobj to mobj->target
void FindTarget(mobj_t* mobj)
{
	byte i;
	for(i=0;i<MAXMOBJS;i++)
	{
	   // Skip dead mobjs
		if(!mobjlist[i].alive)
			continue;
		// Skip enemies that don't have a lockon
		if(!mobjlist[i].locked)
			continue;	
		// Only target enemies
		if(mobjlist[i].type != MT_LARGEMETROID
			&& mobjlist[i].type != MT_MEDIUMMETROID
			&& mobjlist[i].type != MT_SMALLMETROID)
			continue;
			
		mobj->target = &mobjlist[i];
		return;
	}
}
