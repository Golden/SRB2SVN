// Includes
#include <PA9.h>
#include <bgmusic.h>

// Program Includes
#include "main.h"
#include "player.h"
#include "mobj.h"
#include "defines.h"
#include "explode.h"
#include "lockon.h"

extern const unsigned char ship_Sprite[];
extern u16 graffix[];

void AimingThinker(void);
void ConsoleClear(void);
int TouchButtonCheck(type_t type);

mobj_t mobjlist[MAXMOBJS];
player_t player;
byte difficulty = 1;
bool gamestart = true;
byte curslot = 0;
bool enemyalive = 0;
bool aiming = false;
bool buttondrawn = false;
bool firebuttondrawn = false;
byte objects = 0;
u16 globalrotation = 0;

int main(int argc, char ** argv)
{
	PA_Init();
	PA_InitVBL();
	PA_InitText(1,0); // On the top screen
	PA_InitSound(); // Init sound
	
	PA_PlaySound(7, bgmusic, (u32)bgmusic_size, 90, 16000); // Start BG music
 	InitPlayer(); // Init the player
	
	InitGFX();
	InitPalettes();
	InitBackgrounds();
	
	while(1)
	{
	   u8 i, j;
	   static int songloop = 0;
	   enemyalive = false; // This is true if there is an enemy alive at the end of this loop..
	   
	   if(songloop >= 92*TICRATE)
	   {
	   	PA_PlaySound(7, bgmusic, (u32)bgmusic_size, 90, 16000); // Start BG music
	   	songloop = 0;
	 	}  
		
		if(aiming)
			goto skipthinkers;	
	   PlayerThink();
		MovePlayer();	
		DebugInfo();
		
		// Set the rotation for all asteroids
		globalrotation++; 
		PA_SetRotsetNoZoom(0, 3, globalrotation);
		
		if(gamestart)
	 	{
		   for(j=0; j<difficulty;j++)
	  			SpawnMetroid(METLARGE, 50*UNIT, 50*UNIT); // Spawn Metroids at the very start..
	  		
	  		if(difficulty >= 10)
	  			player.guns = 3;
			else if(difficulty >= 5)
	  			player.guns = 2;
	  		
	  		gamestart = false;
	 	} 
	 	
		for(i = 0; i < MAXMOBJS; i++)
		{		
			if(!mobjlist[i].alive)
				continue;
		
			MoveMobj(&mobjlist[i]);	
			CheckCollisions(&mobjlist[i]);	
			
			if(mobjlist[i].fuse > 0)
				mobjlist[i].fuse--;
				
			if(mobjlist[i].fuse == 0 && mobjlist[i].alive) // Kill mobjs when their fuse is gone.
				KillMobj(&mobjlist[i]);  
				
			if(mobjlist[i].type != MT_BULLET 
			&& mobjlist[i].type != MT_AIMBUTTON
			&& mobjlist[i].type != MT_FIREBUTTON
			&& mobjlist[i].type != MT_CANCELBUTTON)	
				enemyalive = true;
		}				
		
		if(enemyalive == false)
		{
			difficulty++;
			gamestart = true;
		}
		if(player.missiles && !buttondrawn)
			SpawnMobj(220*UNIT, 170*UNIT, 0, 0, MT_AIMBUTTON);			
			 
		skipthinkers:	
		if(aiming)
			AimingThinker();
			
		songloop++;
		PA_WaitForVBL();
		ConsoleClear();
	}
	return 0;
}
void ConsoleClear(void)
{
	byte i;
	for(i=0;i<23;i++)
		PA_OutputText(1, 5, i, "                             ");   
}
void DebugInfo(void)
{

   mobj_t* mobj = &mobjlist[0];
   PA_OutputText(1, 5, 10, "Angle : %d  ", mobj->angle);
	PA_OutputText(1, 5, 13, "x: %d ", (mobj->x>>8)-16);
	PA_OutputText(1, 5, 14, "y: %d ", (mobj->y>>8)-16);
	PA_OutputText(1, 5, 15, "spritenum: %d ", mobj->spritenum);
	PA_OutputText(1, 5, 16, "spritesizex: %d ", mobj->spritesizex);
	PA_OutputText(1, 5, 17, "spritesizey: %d ", mobj->spritesizey);
	PA_OutputText(1, 5, 18, "fuse: %d ", mobj->fuse);
	PA_OutputText(1, 5, 19, "alive: %d ", mobj->alive);
	PA_OutputText(1, 5, 19, "curslot: %d ", curslot);
/*
  
	PA_OutputText(1, 5, 10, "Angle : %d  ", player.angle);
	PA_OutputText(1, 5, 11, "momx: %d ", player.momx);
	PA_OutputText(1, 5, 12, "momy: %d ", player.momy);
	PA_OutputText(1, 5, 13, "x: %d ", (player.x>>8)-16);
	PA_OutputText(1, 5, 14, "y: %d ", (player.y>>8)-16);	
	PA_OutputText(1, 5, 16, "Objects: %d", objects);
	PA_OutputText(1, 5, 17, "curslot: %d", curslot);
	PA_OutputText(1, 5, 18, "guns: %d", player.guns);
*/
}

void AimingThinker(void)
{
   mobj_t* mobj;
   u16 i;
   
   PA_ShowBg(0, 0); // Show background 0
   PA_OutputText(1, 5, 16, "Curslot: %d", curslot);
   
   // Draw cancel button
   if (!buttondrawn)
   {
		mobj_t* mobj = SpawnMobj(245*UNIT, 10*UNIT, 0, 0, MT_CANCELBUTTON);   
		SetSpriteToMobjXY(0, mobj);
	}
	
   // Kill any aiming mode-only mobjs (buttons, lock-ons), and leave aiming mode.
	if(TouchButtonCheck(MT_CANCELBUTTON))
	{
	   for(i=0;i<MAXMOBJS;i++)
	   {
			if(!mobjlist[i].alive)   
				continue;
				
			if(mobjlist[i].locked)
				mobjlist[i].locked = false;
				
			if(mobjlist[i].type != MT_LOCKON && mobjlist[i].type != MT_FIREBUTTON)
				continue;
			
			KillMobj(&mobjlist[i]);
		}
	   PA_HideBg(0, 0); // Hide background 0
		aiming = false;
		buttondrawn = firebuttondrawn = false;
		return;
	}	
	else if (TouchButtonCheck(MT_FIREBUTTON))
	{
	   byte i;
	   for(i=0; i<player.lockon; i++)
	   {
	   	mobj_t* mobj = SpawnMobj(player.x, player.y, 0, 32*UNIT, MT_MISSILE);
	   	PA_SetSpriteRotEnable(0, mobj->spritenum, 8);// Rotset 8
	   	SetSpriteToMobjXY(0, mobj);
	 	}  	
	}  	
	// Mobj thinker for aiming mode
	for(i=0;i<MAXMOBJS;i++)
	{
	   if(!mobjlist[i].alive)
	      continue;
	   else if(mobjlist[i].locked) // Don't lock on more than once.
	   	continue;
				
			mobj = &mobjlist[i];   
			CheckCollisions(mobj);	
			
      // Set enemies and bullets to a lower priority (faded)
  		if(mobj->type == MT_BULLET
		   || mobj->type == MT_LARGEMETROID
			|| mobj->type == MT_MEDIUMMETROID
			|| mobj->type == MT_SMALLMETROID)
  			PA_SetSpritePrio(0, mobj->spritenum, 1); 			 	
  		
		switch(mobj->type)
   	{
   	   // Rotate Lockon sprite
		   case MT_LOCKON:
		      mobj->angle+= 10;
		   	PA_SetRotsetNoZoom(0, 1, mobj->angle);
		   	
		   	// Draw the fire button if the enemy has a lock on it.
		   	if (!firebuttondrawn)
		   	{
		   		mobj_t* button = SpawnMobj(220*UNIT, 170*UNIT, 0, 0, MT_FIREBUTTON);
					SetSpriteToMobjXY(0, button);
				}			
		   	break;
			
			// Aim button is useless now. Kill it.
			case MT_AIMBUTTON:
			   KillMobj(mobj);
			   buttondrawn = false;
			   break;
			   
			// Missile movement
			case MT_MISSILE:
			   if(!mobj->target)
			   	FindTarget(mobj);
			   	
			   mobj->angle = PA_GetAngle(mobj->x, mobj->y, mobj->target->x, mobj->target->y);
				PA_SetRotsetNoZoom(0, 8, mobj->angle-127); // Turn the missile toward the target
			
				mobj->x += PA_Cos(mobj->angle);
				mobj->y -= PA_Sin(mobj->angle);
				SetSpriteToMobjXY(0, mobj);
				break;  
			default:
			   break;
		}
		
	   if(PA_Distance(mobj->x, mobj->y, Stylus.X*UNIT, Stylus.Y*UNIT) <= (mobj->radius*mobj->radius)
		&& Stylus.Newpress)
	   {	   	
	      mobj_t* lockon = SpawnMobj(mobj->x, mobj->y, 0, 0, MT_LOCKON);   
	   	
	   	SetSpriteToMobjXY(0, lockon);
	   	PA_SetSpriteRotEnable(0, lockon->spritenum, 1);// Enable rotation for the lockon sprite. Rotset 1
	   	
	   	mobj->locked = true; // Lock on to this mobj.
	   	player.lockon++;
		  	PA_StartSpriteAnimEx(0, // screen
										lockon->spritenum, // sprite number
										0, // first frame
										7, // last frame
										20, // Speed, Frames per second
										ANIM_UPDOWN, 1); // Play it just once.
			return;
	   }			
	}			
	
}
int TouchButtonCheck(type_t type)
{
   u16 i;
   bool foundmobj = false;
 	mobj_t* mobj;
	   
   for(i=0;i<MAXMOBJS;i++)
   {
      if(!mobjlist[i].alive)
      	continue;
      if(mobjlist[i].type != type)
      	continue;
      	
      mobj = &mobjlist[i];
      
		if(Stylus.X*UNIT < mobj->x+mobj->spritesizex/2*UNIT && Stylus.X*UNIT > mobj->x-mobj->spritesizex/2*UNIT
			&& Stylus.Y*UNIT < mobj->y+mobj->spritesizey/2*UNIT && Stylus.Y*UNIT > mobj->y-mobj->spritesizey/2*UNIT
			&& Stylus.Newpress)
			 foundmobj = true;
	}
	return foundmobj;	 	   	    
}
// I don't want these in this file, but they NEED to be here. :|
// Initialize the player.
void InitPlayer(void)
{
   if(!player.exists)
		PA_CreateSpriteFromGfx(0, // Screen
										127, // Sprite number..
										graffix[SHIP], // Image
										OBJ_SIZE_32X32, 1, 1, -255, -255);
		else
			PA_SetSpriteGfx(0, 127, graffix[SHIP]);
   
	PA_SetSpriteRotEnable(0,127,0);// Enable rotations and use Rotset 0...
	
	player.x = 128*UNIT; // ship x position in 8bit fixed point
	player.y = 96*UNIT; // Y
	player.momx = 0;	// X momentum
	player.momy = 0;	// Y momentum
	player.angle = 128; // direction in which to move !
	player.radius = 12*UNIT;
	player.score = 0; // Player's score. 
	player.missiles = 3;  
	player.alive = true;
	player.lockon = false;
	if(difficulty >= 10)
	  	player.guns = 3;
	else if(difficulty >= 5)
	  	player.guns = 2;
	else
		player.guns = 1;
	PA_SetRotsetNoZoom(0, 0, player.angle); // Turn the ship in the correct direction
}

void KillPlayer(void)
{
   player.x = player.y = -255;
   PA_SetSpriteXY(0, 127, -255, -255);
   PA_PlaySimpleSound(5, explode);
	player.alive = false;
}
