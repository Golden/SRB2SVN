# Work-around to allow Dev-C++ to pass compiler flags to windres
RSOPT=--input-format=rc
RCFLAGS=--include-dir ./res
RESOPT=-O coff
RCFLAGS+=--preprocessor '$(CC) -E -xc-header -DRC_INVOKED $(CFLAGS)'
all-before: res/SRB2Builder.res

res/SRB2Builder.res: res/SRB2Builder.rc
	$(WINDRES) -i $<  $(RSOPT) $(RCFLAGS) -o $@ $(RESOPT)

clean-custom:
	${RM} res/SRB2Builder.res
