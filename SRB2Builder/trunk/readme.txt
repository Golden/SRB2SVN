SRB2 Workbench - README.TXT
http://workbench.srb2.org/
oogaland_NO@SPAM_gmail.com
---------------------------------------------------

This is the trunk, where the main line of development is done. Sometimes it will
work, and sometimes it won't. For day-to-day use, see one of the stable
branches.


System Requirements
-------------------

Supported operating systems: Windows XP, Windows Server 2003, Windows Vista.
It should also work fine on Windows 2000 and NT 4.0, although I haven't tested
those. ANSI builds will maybe work on Windows 98 and ME -- but maybe not.

I can't really put a figure on RAM, disk space and CPU requirements, but suffice
it to say that they're pretty modest.


User Guide
----------

No formal user guide exists yet. I plan to post some tips and tutorials at
http://workbench.srb2.org/ over time, however. Check out the SRB2 Wiki at
http://wiki.srb2.org/, too.


Compiling
---------

A workspace for Visual C++ 9 and a Makefile for GNU make with GCC (MinGW) are
provided. A reasonably up-to-date version of the Win32 Platform SDK is required.


Contributors
------------

SRB2 Workbench was written by Gregor Dick. Enormous thanks are due to Rob
Tisdell for testing and suggestions. It owes a large part of its design, and a
bit of its code, to Pascal vd Heiden's Doom Builder. It also incorporates a
window-docking library by James Brown and Jeff Glatt, and a bug-reporting
library by Maksim Pyatkovskiy. Thanks also to Alam Arias for writing a Makefile
and fixing GCC warnings. Some toolbar icons have been taken or adapted from Mark
James' (http://www.famfamfam.com/) "Silk" and "Mini" icon sets; some others are
by "Nev3r"; and the document icon was adapted from one of "kearone's" GNOME
icons.

ZenNode, which is packaged with Workbench's installer, is copyright (C) 2004
Mark Rousseau.


Licence
-------

SRB2 Workbench is licensed under version 2 of the GNU General Public License.
See the file named COPYING for details.
