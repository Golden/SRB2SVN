# NSIS script for SRB2 Workbench.

!include "MUI2.nsh"


Var STARTMENU_DIR
Var ZENNODE

!define APPNAME "SRB2 Workbench"
!define VERSION "(Trunk)"
!define INSTALLERREGKEY "Software\${APPNAME} Installer"

Name "${APPNAME} ${VERSION}"
OutFile "SRB2WBSetup.exe"
LicenseData "..\COPYING"
InstallDir "$PROGRAMFILES\${APPNAME}"
!define INSTALLLOCREGVALUE "InstallLocation"
InstallDirRegKey HKLM "${INSTALLERREGKEY}" "${INSTALLLOCREGVALUE}"
XPStyle on
SetCompressor /SOLID lzma
RequestExecutionLevel admin

!define MUI_ABORTWARNING

!insertmacro MUI_PAGE_LICENSE "..\COPYING"
!insertmacro MUI_PAGE_COMPONENTS
!insertmacro MUI_PAGE_DIRECTORY

!define MUI_STARTMENUPAGE_DEFAULTFOLDER "${APPNAME}"
!define MUI_STARTMENUPAGE_REGISTRY_ROOT "HKLM"
!define MUI_STARTMENUPAGE_REGISTRY_KEY "${INSTALLERREGKEY}"
!define MUI_STARTMENUPAGE_REGISTRY_VALUENAME "StartMenuFolder"
!insertmacro MUI_PAGE_STARTMENU Application $STARTMENU_DIR

!insertmacro MUI_PAGE_INSTFILES

!insertmacro MUI_UNPAGE_CONFIRM
!insertmacro MUI_UNPAGE_INSTFILES

!insertmacro MUI_LANGUAGE "English"



Section "${APPNAME}"

	# Put these files in the installation directory.
	SetOutPath "$INSTDIR"
	WriteRegStr HKLM "${INSTALLERREGKEY}" "${INSTALLLOCREGVALUE}" "$INSTDIR"

	# These files will be overwritten if they already exist.
	File "..\Release-MSVC9\SRB2Workbench.exe"

	CreateDirectory "$INSTDIR\conf\map"
	File "/oname=conf\map\Srb2-1094.cfg" "..\conf\map\Srb2-1094.cfg"

	File "..\COPYING"
	File "..\readme.txt"

	WriteUninstaller "UninstSRB2WB.exe"
	WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\SRB2Workbench" "DisplayName" "${APPNAME}"
	WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\SRB2Workbench" "DisplayIcon" "$INSTDIR\SRB2Workbench.exe,0"
	WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\SRB2Workbench" "URLInfoAbout" "http://workbench.srb2.org/"
	WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\SRB2Workbench" "HelpLink" "http://workbench.srb2.org/"
	WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\SRB2Workbench" "DisplayVersion" "${VERSION}"
	WriteRegDWORD HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\SRB2Workbench" "NoModify" 1
	WriteRegDWORD HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\SRB2Workbench" "NoRepair" 1
	WriteRegDWORD HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\SRB2Workbench" "EstimatedSize" 800
	WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\SRB2Workbench" "UninstallString" "$INSTDIR\UninstSRB2WB.exe"

	# Start Menu shortcuts
	!insertmacro MUI_STARTMENU_WRITE_BEGIN Application

		SetShellVarContext All
		CreateDirectory $SMPROGRAMS\$STARTMENU_DIR
		CreateShortCut "$SMPROGRAMS\$STARTMENU_DIR\${APPNAME}.lnk" $INSTDIR\SRB2Workbench.exe
		CreateShortCut "$SMPROGRAMS\$STARTMENU_DIR\Readme.lnk" $INSTDIR\readme.txt
		CreateShortCut "$SMPROGRAMS\$STARTMENU_DIR\Uninstall ${APPNAME}.lnk" $INSTDIR\UninstSRB2WB.exe

	!insertmacro MUI_STARTMENU_WRITE_END

SectionEnd

Section "ZenNode"
	WriteRegDWORD HKLM "${INSTALLERREGKEY}" "ZenNode" 0x1
	File "..\ZenNode.exe"
SectionEnd

Section "Uninstall"

	Delete "$INSTDIR\conf\map\Srb2-1094.cfg"
	RMDir "$INSTDIR\conf\map"
	RMDir "$INSTDIR\conf"

	Delete "$INSTDIR\SRB2Workbench.exe"
	Delete "$INSTDIR\COPYING"
	Delete "$INSTDIR\readme.txt"
	Delete "$INSTDIR\UninstSRB2WB.exe"

	# Delete ZenNode iff we installed it.
	ReadRegDWORD $ZENNODE HKLM "${INSTALLERREGKEY}" "ZenNode"
	DeleteRegValue HKLM Software\NSIS "ZenNode"
	IntCmp $ZENNODE 0 done done haveZN
	haveZN:
		Delete "$INSTDIR\ZenNode.exe"
	done:

	RMDir "$INSTDIR"

	DeleteRegValue HKLM "${INSTALLERREGKEY}" "${INSTALLLOCREGVALUE}"

	!insertmacro MUI_STARTMENU_GETFOLDER Application $STARTMENU_DIR
	SetShellVarContext All
	Delete "$SMPROGRAMS\$STARTMENU_DIR\${APPNAME}.lnk"
	Delete "$SMPROGRAMS\$STARTMENU_DIR\Readme.lnk"
	Delete "$SMPROGRAMS\$STARTMENU_DIR\Uninstall ${APPNAME}.lnk"
	RMDir "$SMPROGRAMS\$STARTMENU_DIR"

	# Delete the installation info key.
	DeleteRegKey /ifempty HKLM "${INSTALLERREGKEY}"

	# Delete the Add/Remove key.
	DeleteRegKey HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\SRB2Workbench"
SectionEnd
