/* SRB2 Workbench -- A map editor for Sonic Robo Blast 2.
 * Copyright (C) 2009 Gregor Dick
 * http://workbench.srb2.org/
 *
 * Incorporates portions of Doom Builder, (C) 2003 Pascal vd Heiden.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 * USA.
 *
 * preset.c: Routines for extracting and applying batches of non-positional
 * properties of map objects, i.e. "copy/paste selection properties".
 *
 * AMDG.
 */

#include <windows.h>

#include "general.h"
#include "map.h"
#include "selection.h"
#include "texture.h"
#include "preset.h"
#include "cliputil.h"


#define PRESET_CLIPBOARDFORMAT	TEXT("SRB2 Workbench Preset")


static UINT g_uiPresetClipboardFormat;


/* CreateSectorPresetFromSelection
 *   Finds the properties common to all sectors in a selection.
 *
 * Parameters:
 *   MAP*				lpmap				Pointer to map data.
 *   SELECTION_LIST*	lpsellistSectors	Selected sectors.
 *   MAPSECTOR*			lpsectorPreset		Properties are returned here.
 *
 * Return value: DWORD
 *   Flags indicating which fields of lpsectorPreset are valid. See
 *   ENUM_SECTOR_PRESET_FLAGS for values.
 */
DWORD CreateSectorPresetFromSelection(MAP *lpmap, SELECTION_LIST *lpsellistSectors, MAPSECTOR *lpsectorPreset)
{
	int i;

	/* Assume initially that all fields are valid. */
	DWORD dwFlags = SPF_ALL;

	/* Sanity check. */
	if(lpsellistSectors->iDataCount < 1) return 0;

	/* Start with all of the first sector's properties. */
	*lpsectorPreset = lpmap->sectors[lpsellistSectors->lpiIndices[0]];

	/* Loop through the remaining sectors, ignoring any fields that don't match.
	 */
	for(i = 1; i < lpsellistSectors->iDataCount; i++)
	{
		MAPSECTOR *lpsec = &lpmap->sectors[lpsellistSectors->lpiIndices[i]];

		if(lpsec->brightness != lpsectorPreset->brightness) dwFlags &= ~SPF_BRIGHTNESS;
		if(lpsec->hceiling != lpsectorPreset->hceiling) dwFlags &= ~SPF_CEILING;
		if(lpsec->hfloor != lpsectorPreset->hfloor) dwFlags &= ~SPF_FLOOR;
		if(lpsec->special != lpsectorPreset->special) dwFlags &= ~SPF_EFFECT;
		if(lpsec->tag != lpsectorPreset->tag) dwFlags &= ~SPF_TAG;
		if(lpsec->tceiling != lpsectorPreset->tceiling) dwFlags &= ~SPF_CEILINGTEX;
		if(lpsec->tfloor != lpsectorPreset->tfloor) dwFlags &= ~SPF_FLOORTEX;
	}

	return dwFlags;
}


/* CreateLinePresetFromSelection
 *   Finds the properties common to all lines in a selection.
 *
 * Parameters:
 *   MAP*				lpmap				Pointer to map data.
 *   SELECTION_LIST*	lpsellistLinedefs	Selected linedefs.
 *   MAPLINEDEF*		lpldPreset			Properties for the linedef are
 *											returned here.
 *   MAPSIDEDEF*		lpsdFrontPreset		Properties for the front and back
 *						lpsdBackPreset		sidedefs are returned here.
 *
 * Return value: DWORD
 *   Flags indicating which fields of the preset strutctures are valid. See
 *   ENUM_LINE_PRESET_FLAGS for values.
 */
DWORD CreateLinePresetFromSelection(MAP *lpmap, SELECTION_LIST *lpsellistLinedefs, MAPLINEDEF *lpldPreset, MAPSIDEDEF *lpsdFrontPreset, MAPSIDEDEF *lpsdBackPreset)
{
	int i;

	/* Assume initially that all fields are valid. */
	DWORD dwFlags = LPF_ALL;

	/* Sanity check. */
	if(lpsellistLinedefs->iDataCount < 1) return 0;

	/* Start with all the properties of the first linedef. */
	*lpldPreset = lpmap->linedefs[lpsellistLinedefs->lpiIndices[0]];

	/* If we have front/back sidedefs, use their properties; otherwise, we know
	 * they're invalid already.
	 */
	if(SidedefExists(lpmap, lpldPreset->s1))
		*lpsdFrontPreset = lpmap->sidedefs[lpldPreset->s1];
	else
		dwFlags &= ~LPF_ALLFRONT;

	if(SidedefExists(lpmap, lpldPreset->s2))
		*lpsdBackPreset = lpmap->sidedefs[lpldPreset->s2];
	else
		dwFlags &= ~LPF_ALLBACK;

	/* Loop through the remaining linedefs, clearing the bits for those fields
	 * that don't match.
	 */
	for(i = 1; i < lpsellistLinedefs->iDataCount; i++)
	{
		MAPLINEDEF *lpld = &lpmap->linedefs[lpsellistLinedefs->lpiIndices[i]];

		if(lpld->tag != lpldPreset->tag) dwFlags &= ~LPF_TAG;
		if(lpld->effect != lpldPreset->effect) dwFlags &= ~LPF_EFFECT;
		if(lpld->flags != lpldPreset->flags) dwFlags &= ~LPF_FLAGS;

		if(SidedefExists(lpmap, lpld->s1))
		{
			MAPSIDEDEF *lpsd = &lpmap->sidedefs[lpld->s1];

			if(strncmp(lpsd->upper, lpsdFrontPreset->upper, TEXNAME_WAD_BUFFER_LENGTH) != 0) dwFlags &= ~LPF_FRONTUPPER;
			if(strncmp(lpsd->middle, lpsdFrontPreset->middle, TEXNAME_WAD_BUFFER_LENGTH) != 0) dwFlags &= ~LPF_FRONTMIDDLE;
			if(strncmp(lpsd->lower, lpsdFrontPreset->lower, TEXNAME_WAD_BUFFER_LENGTH) != 0) dwFlags &= ~LPF_FRONTLOWER;
			if(lpsd->tx != lpsdFrontPreset->tx) dwFlags &= ~LPF_FRONTX;
			if(lpsd->ty != lpsdFrontPreset->ty) dwFlags &= ~LPF_FRONTY;
		}
		else dwFlags &= ~LPF_ALLFRONT;

		if(SidedefExists(lpmap, lpld->s2))
		{
			MAPSIDEDEF *lpsd = &lpmap->sidedefs[lpld->s2];

			if(strncmp(lpsd->upper, lpsdBackPreset->upper, TEXNAME_WAD_BUFFER_LENGTH) != 0) dwFlags &= ~LPF_BACKUPPER;
			if(strncmp(lpsd->middle, lpsdBackPreset->middle, TEXNAME_WAD_BUFFER_LENGTH) != 0) dwFlags &= ~LPF_BACKMIDDLE;
			if(strncmp(lpsd->lower, lpsdBackPreset->lower, TEXNAME_WAD_BUFFER_LENGTH) != 0) dwFlags &= ~LPF_BACKLOWER;
			if(lpsd->tx != lpsdBackPreset->tx) dwFlags &= ~LPF_BACKX;
			if(lpsd->ty != lpsdBackPreset->ty) dwFlags &= ~LPF_BACKY;
		}
		else dwFlags &= ~LPF_ALLBACK;
	}

	return dwFlags;
}


/* CreateThingPresetFromSelection
 *   Finds the properties common to all things in a selection.
 *
 * Parameters:
 *   MAP*				lpmap				Pointer to map data.
 *   SELECTION_LIST*	lpsellistThings		Selected things.
 *   MAPTHING*			lpthingPreset		Properties are returned here.
 *
 * Return value: DWORD
 *   Flags indicating which fields of lpthingPreset are valid. See
 *   ENUM_THING_PRESET_FLAGS for values.
 */
DWORD CreateThingPresetFromSelection(MAP *lpmap, SELECTION_LIST *lpsellistThings, MAPTHING *lpthingPreset)
{
	int i;

	/* Assume initially that all fields are valid. */
	DWORD dwFlags = TPF_ALL;

	/* Sanity check. */
	if(lpsellistThings->iDataCount < 1) return 0;

	/* Start with all of the first sector's properties. */
	*lpthingPreset = lpmap->things[lpsellistThings->lpiIndices[0]];

	/* Loop through the remaining things, ignoring any fields that don't match.
	 */
	for(i = 1; i < lpsellistThings->iDataCount; i++)
	{
		MAPTHING *lpthing = &lpmap->things[lpsellistThings->lpiIndices[i]];

		if(lpthing->thing != lpthingPreset->thing) dwFlags &= ~TPF_TYPE;
		if(lpthing->flag != lpthingPreset->flag) dwFlags &= ~TPF_FLAGS;
		if(lpthing->angle != lpthingPreset->angle) dwFlags &= ~TPF_ANGLE;
	}

	return dwFlags;
}


/* RegisterPresetClipboardFormat
 *   Registers the clipboard format for presets.
 *
 * Parameters:
 *   None.
 *
 * Return value: None.
 *
 * Remakrs:
 *   This function must be called before any of the other clipboard functions.
 */
void RegisterPresetClipboardFormat(void)
{
	g_uiPresetClipboardFormat = RegisterClipboardFormat(PRESET_CLIPBOARDFORMAT);
}


/* ClipboardContainsPreset
 *   Determines whether the clipboard contains a preset.
 *
 * Parameters:
 *   None.
 *
 * Return value: BOOL
 *   TRUE if the clipboard contains a preset; FALSE otherwise.
 */
BOOL ClipboardContainsPreset(void)
{
	return IsClipboardFormatAvailable(g_uiPresetClipboardFormat);
}


/* CopyPresetToClipboard
 *   Copies a preset to the clipboard.
 *
 * Parameters:
 *   PRESET*	lppreset	Preset.
 *
 * Return value: int
 *   Zero on success; non-zero on error.
 */
int CopyPresetToClipboard(PRESET *lppreset)
{
	return CopyBufferToClipboard(lppreset, sizeof(PRESET), g_uiPresetClipboardFormat);
}


/* PastePresetFromClipboard
 *   Pastes a preset from the clipboard.
 *
 * Parameters:
 *   None.
 *
 * Return value: PRESET*
 *   Pointer to the preset from the clipboard if successful; NULL on error
 *   (including there being no preset on the clipboard).
 *
 * Remarks:
 *   The caller is responsible for freeing the returned preset.
 */
PRESET* PastePresetFromClipboard(void)
{
	return (PRESET*)GetBufferFromClipboard(g_uiPresetClipboardFormat);
}


/* ApplySectorPresetToSelection
 *   Applies preset properties to all sectors in a selection.
 *
 * Parameters:
 *   MAP*				lpmap				Pointer to map data.
 *   SELECTION_LIST*	lpsellistSectors	Selected sectors.
 *   MAPSECTOR*			lpsectorPreset		Values to set.
 *   DWORD				dwFlags				Flags indicating which fields of
 *											lpsectorPreset are valid. See
 *											ENUM_SECTOR_PRESET_FLAGS for values.
 *
 * Return value: None.
 */
void ApplySectorPresetToSelection(MAP *lpmap, SELECTION_LIST *lpsellistSectors, MAPSECTOR *lpsectorPreset, DWORD dwFlags)
{
	int i;

	/* Loop through all selected sectors and apply the properties. */
	for(i = 0; i < lpsellistSectors->iDataCount; i++)
	{
		MAPSECTOR *lpsec = &lpmap->sectors[lpsellistSectors->lpiIndices[i]];

		if(dwFlags & SPF_EFFECT) lpsec->special = lpsectorPreset->special;
		if(dwFlags & SPF_FLOOR) lpsec->hfloor = lpsectorPreset->hfloor;
		if(dwFlags & SPF_CEILING) lpsec->hceiling = lpsectorPreset->hceiling;
		if(dwFlags & SPF_FLOORTEX) CopyMemory(lpsec->tfloor, lpsectorPreset->tfloor, TEXNAME_WAD_BUFFER_LENGTH);
		if(dwFlags & SPF_CEILINGTEX) CopyMemory(lpsec->tceiling, lpsectorPreset->tceiling, TEXNAME_WAD_BUFFER_LENGTH);
		if(dwFlags & SPF_TAG) lpsec->tag = lpsectorPreset->tag;
		if(dwFlags & SPF_BRIGHTNESS) lpsec->brightness = lpsectorPreset->brightness;
	}
}


/* ApplyLinePresetToSelection
 *   Applies preset properties to all lines in a selection.
 *
 * Parameters:
 *   MAP*				lpmap				Pointer to map data.
 *   SELECTION_LIST*	lpsellistLinedefs	Selected linedefs.
 *   MAPLINEDEF*		lpldPreset			Values to use for linedefs.
 *   MAPSIDEDEF*		lpsdFrontPreset		Values to use for the front and back
 *						lpsdBackPreset		sidedefs.
 *   DWORD				dwFlags				Flags indicating which fields of
 *											the structures are valid. See
 *											ENUM_LINE_PRESET_FLAGS for values.
 *
 * Return value: None.
 */
void ApplyLinePresetToSelection(MAP *lpmap, SELECTION_LIST *lpsellistLinedefs, MAPLINEDEF *lpldPreset, MAPSIDEDEF *lpsdFrontPreset, MAPSIDEDEF *lpsdBackPreset, DWORD dwFlags)
{
	int i;

	/* Loop through all selected linedefs and apply the properties. */
	for(i = 0; i < lpsellistLinedefs->iDataCount; i++)
	{
		MAPLINEDEF *lpld = &lpmap->linedefs[lpsellistLinedefs->lpiIndices[i]];

		if(dwFlags & LPF_EFFECT) lpld->effect = lpldPreset->effect;
		if(dwFlags & LPF_FLAGS) lpld->flags = lpldPreset->flags;
		if(dwFlags & LPF_TAG) lpld->tag = lpldPreset->tag;

		/* Do we have anything to do to the front sidedef, and if so, is there
		 * one there to work with?
		 */
		if(dwFlags & LPF_ALLFRONT && SidedefExists(lpmap, lpld->s1))
		{
			MAPSIDEDEF *lpsd = &lpmap->sidedefs[lpld->s1];

			if(dwFlags & LPF_FRONTUPPER) CopyMemory(lpsd->upper, lpsdFrontPreset->upper, TEXNAME_WAD_BUFFER_LENGTH);
			if(dwFlags & LPF_FRONTMIDDLE) CopyMemory(lpsd->middle, lpsdFrontPreset->middle, TEXNAME_WAD_BUFFER_LENGTH);
			if(dwFlags & LPF_FRONTLOWER) CopyMemory(lpsd->lower, lpsdFrontPreset->lower, TEXNAME_WAD_BUFFER_LENGTH);
			if(dwFlags & LPF_FRONTX) lpsd->tx = lpsdFrontPreset->tx;
			if(dwFlags & LPF_FRONTY) lpsd->ty = lpsdFrontPreset->ty;
		}

		/* Do we have anything to do to the back sidedef, and if so, is there
		 * one there to work with?
		 */
		if(dwFlags & LPF_ALLBACK && SidedefExists(lpmap, lpld->s2))
		{
			MAPSIDEDEF *lpsd = &lpmap->sidedefs[lpld->s2];

			if(dwFlags & LPF_BACKUPPER) CopyMemory(lpsd->upper, lpsdBackPreset->upper, TEXNAME_WAD_BUFFER_LENGTH);
			if(dwFlags & LPF_BACKMIDDLE) CopyMemory(lpsd->middle, lpsdBackPreset->middle, TEXNAME_WAD_BUFFER_LENGTH);
			if(dwFlags & LPF_BACKLOWER) CopyMemory(lpsd->lower, lpsdBackPreset->lower, TEXNAME_WAD_BUFFER_LENGTH);
			if(dwFlags & LPF_BACKX) lpsd->tx = lpsdBackPreset->tx;
			if(dwFlags & LPF_BACKY) lpsd->ty = lpsdBackPreset->ty;
		}
	}
}



/* ApplyThingPresetToSelection
 *   Applies preset properties to all things in a selection.
 *
 * Parameters:
 *   MAP*				lpmap				Pointer to map data.
 *   SELECTION_LIST*	lpsellistThings		Selected things.
 *   MAPTHING*			lpthingPreset		Values to set.
 *   DWORD				dwFlags				Flags indicating which fields of
 *											lpsectorPreset are valid. See
 *											ENUM_THING_PRESET_FLAGS for values.
 *
 * Return value: None.
 */
void ApplyThingPresetToSelection(MAP *lpmap, SELECTION_LIST *lpsellistThings, MAPTHING *lpthingPreset, DWORD dwFlags)
{
	int i;

	/* Loop through all selected sectors and apply the properties. */
	for(i = 0; i < lpsellistThings->iDataCount; i++)
	{
		MAPTHING *lpthing = &lpmap->things[lpsellistThings->lpiIndices[i]];

		if(dwFlags & TPF_TYPE) lpthing->thing = lpthingPreset->thing;
		if(dwFlags & TPF_FLAGS) lpthing->flag = lpthingPreset->flag;
		if(dwFlags & TPF_ANGLE) lpthing->angle = lpthingPreset->angle;
	}
}
