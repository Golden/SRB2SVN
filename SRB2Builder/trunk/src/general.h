/* SRB2 Workbench -- A map editor for Sonic Robo Blast 2.
 * Copyright (C) 2009 Gregor Dick
 * http://workbench.srb2.org/
 *
 * Incorporates portions of Doom Builder, (C) 2003 Pascal vd Heiden.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 * USA.
 *
 * general.h: Header for general.c.
 *
 * AMDG.
 */

#ifndef __SRB2B_GENERAL__
#define __SRB2B_GENERAL__

#include <windows.h>
#include <math.h>


/* Types. */
typedef struct _DYNAMICINTARRAY
{
	unsigned int uiBufSize, uiCount;
	int *lpiIndices;
} DYNAMICINTARRAY;

typedef struct _QUEUE QUEUE;

typedef struct _GROWARRAY
{
	DWORD	cbElement;
	DWORD	cAllocedElements;
	DWORD	cElements;
	HANDLE	hHeap;
	void	*lpvElements;
} GROWARRAY;



/* Globals. */
extern HINSTANCE g_hInstance;
extern TCHAR g_szAppName[];
extern HANDLE g_hProcHeap;
BOOL g_bWinNT, g_bHasCC6;


/* Function prototypes. */
void Die(int iMsgResource);
BOOL CALLBACK IncrementInteger(HWND hwndUnused, LPARAM lParam);
BOOL EqualPaths(LPCTSTR szPath1, LPCTSTR szPath2);
BOOL PathsReferToSameFile(LPCTSTR szPath1, LPCTSTR szPath2);
BOOL IsStringIntA(LPCSTR sz);
int MessageBoxFromStringTable(HWND hwnd, WORD wResourceString, UINT uiType);
int QsortIntegerComparison(const void *lpv1, const void *lpv2);
int QsortUShortComparison(const void *lpv1, const void *lpv2);
int CALLBACK ListViewIntegerComparison(LPARAM lParam1, LPARAM lParam2, LPARAM lParamSort);
void InitialiseDynamicIntArray(DYNAMICINTARRAY *lpdiarray, unsigned int ciInitialSize);
void FreeDynamicIntArray(DYNAMICINTARRAY *lpdiarray);
void AddToDynamicIntArray(DYNAMICINTARRAY *lpdiarray, int iValue);
void SortDynamicIntArray(DYNAMICINTARRAY *lpdiarray);
void LoadAndFormatFilterString(USHORT unStringID, LPTSTR szFilter, UINT cchFilter);
char Log2Floor(unsigned int ui);

QUEUE* AllocateQueue(void);
void DestroyQueue(QUEUE *lpqueue);
BOOL QueueIsEmpty(QUEUE *lpqueue);
void* DeQueue(QUEUE *lpqueue);
void EnQueue(QUEUE *lpqueue, void *lpvData);

int RandomInteger(int iMin, int iMax);
BOOL BufferSearch(const BYTE *lpbyBuffer, const BYTE *lpbySearch, DWORD cbBuffer, DWORD cbSearch);

void AddToGrowArray(GROWARRAY *lpgrowarray, DWORD cAdditionalElements);
void InitGrowArray(GROWARRAY *lpgrowarray, DWORD cbElement, DWORD cAllocInitial, HANDLE hHeap);
void DestroyGrowArray(GROWARRAY *lpgrowarray);


/* ProcHeapAlloc, ProcHeapReAlloc, ProcHeapFree
 *   Wrappers for Heap* functions to alloc/free from the process heap.
 */
static __inline LPVOID ProcHeapAlloc(DWORD cb) {return HeapAlloc(g_hProcHeap, HEAP_GENERATE_EXCEPTIONS, cb); }
static __inline LPVOID ProcHeapReAlloc(LPVOID lpv, DWORD cb) { return HeapReAlloc(g_hProcHeap, HEAP_GENERATE_EXCEPTIONS, lpv, cb); }
static __inline BOOL ProcHeapFree(LPVOID lpv) { return HeapFree(g_hProcHeap, 0, lpv); }


/* Inlined functions. */
static __inline void ClearDynamicIntArray(DYNAMICINTARRAY *lpdiarray)
{
	lpdiarray->uiCount = 0;
}

/* *lpdiarrayDest should NOT have been InitialiseDynamicIntArray-ed. */
static __inline void DeepCopyDynamicIntArray(DYNAMICINTARRAY *lpdiarrayDest, DYNAMICINTARRAY *lpdiarraySrc)
{
	lpdiarrayDest->uiBufSize = lpdiarraySrc->uiBufSize;
	lpdiarrayDest->uiCount = lpdiarraySrc->uiCount;
	lpdiarrayDest->lpiIndices = ProcHeapAlloc(lpdiarraySrc->uiBufSize * sizeof(int));
	CopyMemory(lpdiarrayDest->lpiIndices, lpdiarraySrc->lpiIndices, lpdiarraySrc->uiBufSize * sizeof(int));
}

/* Rounds a floating-point number to an integer. */
static __inline int Round(double d)
{
	if(d >= 0) return (int)(d + 0.5);
	else return (int)(d - 0.5);
}




/* Macros. */
#define NUM_ELEMENTS(arr) (sizeof(arr)/sizeof(arr[0]))

#define ROUND(f) (int)(floor((f) + 0.5))

#ifndef INT_MAX
#define INT_MAX 0x7FFFFFFF
#endif

#define CCH_SIGNED_INT 12

#endif
