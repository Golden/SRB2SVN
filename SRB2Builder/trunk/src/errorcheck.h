/* SRB2 Workbench -- A map editor for Sonic Robo Blast 2.
 * Copyright (C) 2009 Gregor Dick
 * http://workbench.srb2.org/
 *
 * Incorporates portions of Doom Builder, (C) 2003 Pascal vd Heiden.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 * USA.
 *
 * errorcheck.h: Header for errorcheck.c.
 *
 * AMDG.
 */

#ifndef __SRB2B_ERRORCHECK__
#define __SRB2B_ERRORCHECK__

#include <windows.h>

#include "general.h"
#include "map.h"
#include "CodeImp/ci_data_proto.h"


enum ENUM_SIDEDEF_ERROR_FLAGS
{
	SEF_FRONT_MISSING = 1,
	SEF_BACK_MISSING = 2,
	SEF_BACK_SUPERFLUOUS = 4
};


typedef struct _UNCLOSED_SECTOR_DESCRIPTOR
{
	int iLinedefSrc, iLinedefAdj;
	int iSideSrc;
} UNCLOSED_SECTOR_DESCRIPTOR;


int FindNextCoincidentVertex(MAP *lpmap, int iVertex);
BOOL* UsedVertexMatrix(MAP *lpmap);
BOOL* UsedSidedefMatrix(MAP *lpmap);
BOOL* UsedSectorMatrix(MAP *lpmap);
BYTE LinedefSidedefMismatches(MAP *lpmap, int iLinedef);
BYTE MissingTexFlags(MAP *lpmap, int iLinedef, LPCTSTR szSky);
int FindCrossingLinedef(MAP *lpmap, int iLinedef, int iLinedefBegin);
int FindUnclosedSectors(MAP *lpmap, UNCLOSED_SECTOR_DESCRIPTOR** lplpusd);
void FixCrossingLinedefs(MAP *lpmap, int iLinedef1, int iLinedef2);
void FixBrokenSectorFromLinedef(MAP *lpmap, int iLinedef, int iLinedefSide);

static __inline void FreeUsedMapObjectMatrix(BOOL *lpbUsed) { ProcHeapFree(lpbUsed); }
static __inline void FreeUnclosedSectorDescriptors(UNCLOSED_SECTOR_DESCRIPTOR *lpusd) { ProcHeapFree(lpusd); }


/* LineIsZeroLength
 *   Determines whether a line is of zero length.
 *
 * Parameters:
 *   MAP*	lpmap		Map.
 *   int	iLinedef	Index of linedef.
 *
 * Return value: BOOL
 *   TRUE if line is zero-length; FALSE otherwise.
 */
static __inline BOOL LineIsZeroLength(MAP *lpmap, int iLinedef)
{
	MAPLINEDEF *lpld = &lpmap->linedefs[iLinedef];

	/* The first check isn't strictly necessary, but is probably more likely in
	 * common scenarios and so we include it for efficiency.
	 */
	return lpld->v1 == lpld->v2 ||
		(lpmap->vertices[lpld->v1].x == lpmap->vertices[lpld->v2].x && lpmap->vertices[lpld->v1].y == lpmap->vertices[lpld->v2].y);
}


/* ThingInLevel
 *   Determines whether a thing lies within any sector.
 *
 * Parameters:
 *   MAP*	lpmap	Map.
 *   int	iThing	Index of thing.
 *
 * Return value: BOOL
 *   TRUE if thing is in the level; FALSE otherwise.
 */
static __inline BOOL ThingInLevel(MAP *lpmap, int iThing)
{
	return IntersectSector(lpmap->things[iThing].x, lpmap->things[iThing].y, lpmap, NULL, NULL) >= 0;
}


/* SectorHasNegativeHeight
 *   Determines whether a sector's ceiling is below its floor.
 *
 * Parameters:
 *   MAP*	lpmap		Map.
 *   int	iSector		Index of sector.
 *
 * Return value: BOOL
 *   TRUE if sector has negative height; FALSE otherwise.
 */
static __inline BOOL SectorHasNegativeHeight(MAP *lpmap, int iSector)
{
	return lpmap->sectors[iSector].hceiling < lpmap->sectors[iSector].hfloor;
}


#endif
