/* SRB2 Workbench -- A map editor for Sonic Robo Blast 2.
 * Copyright (C) 2009 Gregor Dick
 * http://workbench.srb2.org/
 *
 * Incorporates portions of Doom Builder, (C) 2003 Pascal vd Heiden.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 * USA.
 *
 * general.c: Assorted routines of varying usefulness that didn't fit elsewhere.
 *
 * AMDG.
 */

#include <windows.h>
#include <tchar.h>
#include <stdio.h>

#include "general.h"
#include "../res/resource.h"

#include "win/mdiframe.h"

typedef struct _QUEUE_NODE QUEUE_NODE;

struct _QUEUE_NODE
{
	void*		lpvData;
	QUEUE_NODE*	lpqnNext;
};

struct _QUEUE
{
	QUEUE_NODE*	lpqnFirst;
	QUEUE_NODE*	lpqnLast;
};


#define MBFST_INIT_BUFSIZE	256


/* Globals. */
TCHAR g_szAppName[] = TEXT("SRB2 Workbench");
HINSTANCE g_hInstance;
HANDLE g_hProcHeap;
BOOL g_bWinNT, g_bHasCC6;


/* Static function prototypes. */
static void QuitImmediately(int iExitCode);


/* Die
 *   Displays a message box with an error message, and then quits.
 *
 * Parameters:
 *   int		iMsgResource	Resource ID of error message.
 *
 * Return value:
 *   None, but note that it *does* return.
 */
void Die(int iMsgResource)
{
	if(IsDebuggerPresent())
		DebugBreak();
	else
	{
		TCHAR szErrorMsg[512];

		LoadString(g_hInstance, iMsgResource, szErrorMsg, NUM_ELEMENTS(szErrorMsg));

		/* szFormat should have a %d, an %s and an %s, in that order. */
		MessageBox(NULL, szErrorMsg, g_szAppName, MB_ICONERROR);

		/* Right, that's enough. Bye! */
		QuitImmediately(1);
	}
}


/* QuitImmediately
 *   Drops everything and quits.
 *
 * Parameters:
 *   int		iExitCode			Errorlevel.
 *
 * Return value:
 *   None, but note that it *does* return.
 */
static void QuitImmediately(int iExitCode)
{
	DestroyMainWindow();
	PostQuitMessage(iExitCode);
}


/* IncrementInteger
 *   Does exactly what it says on the tin.
 *
 * Parameters:
 *   HWND		hwndUnused	Unused.
 *   LPARAM		lParam		Pointer to integer to increment.
 *
 * Return value:
 *   Always TRUE.
 *
 * Notes:
 *   Useful for enumeration functions that count things.
 */
BOOL CALLBACK IncrementInteger(HWND hwnd, LPARAM lParam)
{
	UNREFERENCED_PARAMETER(hwnd);
	(*(int*)lParam)++;
	return TRUE;
}


/* EqualPaths
 *   Determines whether two paths are equivalent.
 *
 * Parameters:
 *   LPCTSTR	szPath1		First path.
 *   LPCTSTR	szPath2		Second path.
 *
 * Return value:
 *   TRUE if paths are equivalent; FALSE if not.
 *
 * Notes:
 *   Ignorant of links. See also PathsReferToSameFile.
 */
BOOL EqualPaths(LPCTSTR szPath1, LPCTSTR szPath2)
{
	unsigned int cch1, cch2;
	LPTSTR szFullPath1, szFullPath2;
	BOOL bEqual;

	/* First, make the buffers big enough to store the full pathname. */
	cch1 = GetFullPathName(szPath1, 0, TEXT(""), NULL);
	cch2 = GetFullPathName(szPath2, 0, TEXT(""), NULL);
	szFullPath1 = ProcHeapAlloc((cch1 + 1) * sizeof(TCHAR));
	szFullPath2 = ProcHeapAlloc((cch2 + 1) * sizeof(TCHAR));

	/* Generate comparable paths. */
	GetFullPathName(szPath1, cch1 + 1, szFullPath1, NULL);
	GetFullPathName(szPath2, cch2 + 1, szFullPath2, NULL);
	GetShortPathName(szFullPath1, szFullPath1, cch1 + 1);
	GetShortPathName(szFullPath2, szFullPath2, cch2 + 1);

	bEqual = !_tcsicmp(szFullPath1, szFullPath2);

	/* Clean up. */
	ProcHeapFree(szFullPath1);
	ProcHeapFree(szFullPath2);

	return bEqual;
}


/* PathsReferToSameFile
 *   Determines whether two paths refer to the same file.
 *
 * Parameters:
 *   LPCTSTR	szPath1		First path.
 *   LPCTSTR	szPath2		Second path.
 *
 * Return value: BOOL
 *   TRUE if paths refer to same file; FALSE if not.
 *
 * Notes:
 *   Unlike EqualPaths, PathsReferToSameFile takes hard links into account.
 */
BOOL PathsReferToSameFile(LPCTSTR szPath1, LPCTSTR szPath2)
{
	HANDLE hFile1, hFile2;
	BOOL bEqual;

	/* Trivial case: paths are equivalent. */
	if(EqualPaths(szPath1, szPath2)) return TRUE;

	/* To check whether they're both links, we have to open them and compare
	 * their IDs.
	 */
	hFile1 = CreateFile(szPath1, 0, FILE_SHARE_READ | FILE_SHARE_WRITE, NULL, OPEN_EXISTING, 0, NULL);

	/* If we couldn't open the first one, then they're different (since the
	 * paths are different.
	 */
	if(!hFile1) return FALSE;

	hFile2 = CreateFile(szPath2, 0, FILE_SHARE_READ | FILE_SHARE_WRITE, NULL, OPEN_EXISTING, 0, NULL);

	/* Assume not equal. */
	bEqual = FALSE;
	if(hFile2)
	{
		BY_HANDLE_FILE_INFORMATION byhfi1, byhfi2;

		GetFileInformationByHandle(hFile1, &byhfi1);
		GetFileInformationByHandle(hFile2, &byhfi2);

		/* Files are equal iff they have the same ID on the same volume. */
		if(byhfi1.dwVolumeSerialNumber == byhfi2.dwVolumeSerialNumber &&
			byhfi1.nFileIndexLow == byhfi2.nFileIndexLow &&
			byhfi1.nFileIndexHigh == byhfi2.nFileIndexHigh)
		{
			bEqual = TRUE;
		}

		CloseHandle(hFile2);
	}

	CloseHandle(hFile1);
	return bEqual;
}



/* IsStringIntA
 *   Determines whether a string is a valid integer.
 *
 * Parameters:
 *   LPCSTR		sz		String to check.
 *
 * Return value: BOOL
 *   TRUE if string is integer; FALSE if not.
 *
 * Notes:
 *   Range is not checked.
 */
BOOL IsStringIntA(LPCSTR sz)
{
	if(*sz == '-') sz++;
	while(*sz >= '0' && *sz <= '9') sz++;
	return !(*sz);
}



/* MessageBoxFromStringTable
 *   Displays a message box using a string from the table.
 *
 * Parameters:
 *   HWND	hwnd				Parent window.
 *   WORD	wResourceString		ID of string.
 *   UINT	uiType				As for MessageBox.
 *
 * Return value: int
 *   As for MessageBox.
 */
int MessageBoxFromStringTable(HWND hwnd, WORD wResourceString, UINT uiType)
{
	MSGBOXPARAMS mbp;
	DWORD cchBuffer = MBFST_INIT_BUFSIZE;
	LPTSTR szBuffer = ProcHeapAlloc(cchBuffer * sizeof(TCHAR));
	int iRet;

	/* Load the string. We don't just pass the ID to MessageBoxIndirect, since
	 * it has an (undocumented?!) limit on the length of the string.
	 */
	while((DWORD)LoadString(g_hInstance, wResourceString, szBuffer, cchBuffer) >= cchBuffer - 1)
	{
		ProcHeapFree(szBuffer);
		cchBuffer <<= 1;
		szBuffer = ProcHeapAlloc(cchBuffer * sizeof(TCHAR));
	}


	mbp.cbSize = sizeof(mbp);
	mbp.dwContextHelpId = 0;
	mbp.dwLanguageId = MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT);
	mbp.dwStyle = uiType;
	mbp.hInstance = g_hInstance;
	mbp.hwndOwner = hwnd;
	mbp.lpfnMsgBoxCallback = NULL;
	mbp.lpszCaption = g_szAppName;
	mbp.lpszIcon = NULL;
	mbp.lpszText = szBuffer;

	iRet = MessageBoxIndirect(&mbp);

	ProcHeapFree(szBuffer);

	return iRet;
}



/* Qsort[*]Comparison
 *   qsort callback function for comparing array entries. Various types.
 *
 * Parameters:
 *   const void*	lpv1, lpv2		Addresses of values to compare.
 *
 * Return value: int
 *   0 if *lpv1 == *lpv2; < 0 if *lpv1 < *lpv2; > 0 otherwise. Operators here
 *   are understood to refer to the objects referenced by the array
 */
int QsortIntegerComparison(const void *lpv1, const void *lpv2)
{
	int i1 = *((int*)lpv1), i2 = *((int*)lpv2);

	if(i1 < i2) return -1;
	if(i1 > i2) return 1;
	return 0;
}

int QsortUShortComparison(const void *lpv1, const void *lpv2)
{
	unsigned short un1 = *((unsigned short*)lpv1), un2 = *((unsigned short*)lpv2);

	if(un1 < un2) return -1;
	if(un1 > un2) return 1;
	return 0;
}


/* ListViewIntegerComparison
 *   Callback function for comparing LPARAMs. Useful for sorting ListView items.
 *
 * Parameters:
 *   LPARAM		lParam1, lParam2	Values to compare.
 *   LPARAM		lParamSort			An extra parameter. Unused.
 *
 * Return value: int
 *   0 if lParam1 == lParam2; < 0 if lParam1 < lParam2; > 0 otherwise.
 */
int CALLBACK ListViewIntegerComparison(LPARAM lParam1, LPARAM lParam2, LPARAM lParamSort)
{
	UNREFERENCED_PARAMETER(lParamSort);
	return lParam1 - lParam2;
}


/* InitialiseDynamicIntArray
 *   Initialises a dynamic integer array, allocating memory for an in initial
 *   buffer.
 *
 * Parameters:
 *   DYNAMICINTARRAY*	lpdiarray		Pointer to array structure.
 *   unsigned int		ciInitialSize	Number of ints for which memory must be
 *										allocated initially.
 *
 * Return value: None.
 *
 * Remarks:
 *   Call FreeDynamicIntArray to free the memory.
 */
void InitialiseDynamicIntArray(DYNAMICINTARRAY *lpdiarray, unsigned int ciInitialSize)
{
	lpdiarray->uiCount = 0;
	lpdiarray->uiBufSize = ciInitialSize;
	lpdiarray->lpiIndices = ProcHeapAlloc(ciInitialSize * sizeof(int));
}


/* FreeDynamicIntArray
 *   Frees the memory used by a dynamic integer array.
 *
 * Parameters:
 *   DYNAMICINTARRAY*	lpdiarray		Pointer to array structure.
 *
 * Return value: None.
 *
 * Remarks:
 *   Does not free the array structure itself: only the integer buffer.
 */
void FreeDynamicIntArray(DYNAMICINTARRAY *lpdiarray)
{
	ProcHeapFree(lpdiarray->lpiIndices);
}

/* AddToDynamicIntArray
 *   Adds the value to the end of a dynamic array of integers, allocating more
 *   memory if necessary.
 *
 * Parameters:
 *   DYNAMICINTARRAY*	lpdiarray	Pointer to array structure.
 *   int				iValue		Value to add.
 *
 * Return value: None.
 */
void AddToDynamicIntArray(DYNAMICINTARRAY *lpdiarray, int iValue)
{
	/* Do we need to allocate more space? */
	if(lpdiarray->uiCount == lpdiarray->uiBufSize)
	{
		/* Double the buffer length. */
		lpdiarray->uiBufSize <<= 1;

		/* Reallocate. */
		lpdiarray->lpiIndices = ProcHeapReAlloc(lpdiarray->lpiIndices, lpdiarray->uiBufSize * sizeof(int));
	}

	lpdiarray->lpiIndices[lpdiarray->uiCount++] = iValue;
}


/* SortDynamicIntArray
 *  Sorts a dynamic integer array in ascending order.
 *
 * Parameters:
 *   DYNAMICINTARRAY*	lpdiarray	Pointer to array structure.
 *
 * Return value: None.
 */
void SortDynamicIntArray(DYNAMICINTARRAY *lpdiarray)
{
	qsort(lpdiarray->lpiIndices, lpdiarray->uiCount, sizeof(int), QsortIntegerComparison);
}



/* LoadAndFormatFilterString
 *   Loads a string from the string table and replaces tabs with NULs. This gets
 *   around a limitation of the resource compiler.
 *
 * Parameters:
 *   USHORT		unStringID		ID of string resource.
 *   LPTSTR		szFilter		Buffer in which to return string.
 *   UINT		cchFilter		Size of buffer in characters, including space
 *								for terminators (both of them!).
 *
 * Return value: None.
 */
void LoadAndFormatFilterString(USHORT unStringID, LPTSTR szFilter, UINT cchFilter)
{
	TCHAR *szInFilter = szFilter;

	/* Get filter string from string table. Need TWO NULs, remember!
	 */
	LoadString(g_hInstance, unStringID, szFilter, cchFilter-1);
	szFilter[cchFilter - 1] = TEXT('\0');

	/* Replace tabs with NULs. The resource compiler doesn't like
	 * NULs embedded in strings, so we have to do it.
	 */
	do
	{
		if(*szInFilter == TEXT('\t'))
			*szInFilter = TEXT('\0');
	}
	while(*(++szInFilter));
}


/* Log2Floor
 *   Returns the integer part of the logarithm in base 2 of an integer.
 *
 * Parameters:
 *   unsigned int	ui	Number whose logarithm is to be taken.
 *
 * Return value: char
 *   Floor of the logarithm, or -1 if ui == 0.
 */
char Log2Floor(unsigned int ui)
{
	char c = 0;
	if(!ui) return -1;
	while(ui >>= 1) c++;
	return c;
}


/* Queue functions. These should be pretty self-explanatory. */

QUEUE* AllocateQueue(void)
{
	QUEUE *lpqueue = ProcHeapAlloc(sizeof(QUEUE));
	lpqueue->lpqnFirst = lpqueue->lpqnLast = NULL;

	return lpqueue;
}

void DestroyQueue(QUEUE *lpqueue)
{
	while(!QueueIsEmpty(lpqueue))
		DeQueue(lpqueue);

	ProcHeapFree(lpqueue);
}

BOOL QueueIsEmpty(QUEUE *lpqueue)
{
	return !lpqueue->lpqnFirst;
}

void* DeQueue(QUEUE *lpqueue)
{
	void *lpvData;
	QUEUE_NODE *lpqn = lpqueue->lpqnFirst;

	/* Empty queue? */
	if(!lpqn) return NULL;

	/* Remember the data before we free the container. */
	lpvData = lpqn->lpvData;

	/* Move the start pointer to the next element. */
	lpqueue->lpqnFirst = lpqn->lpqnNext;

	/* Free the container. */
	ProcHeapFree(lpqn);

	/* If the queue is now empty, we just got rid of the last element. */
	if(!lpqueue->lpqnFirst) lpqueue->lpqnLast = NULL;

	/* Finito! Return the data. */
	return lpvData;
}

void EnQueue(QUEUE *lpqueue, void *lpvData)
{
	QUEUE_NODE *lpqn = ProcHeapAlloc(sizeof(QUEUE_NODE));
	lpqn->lpvData = lpvData;
	lpqn->lpqnNext = NULL;

	/* If the queue is empty, the new node is the only one. */
	if(QueueIsEmpty(lpqueue))
		lpqueue->lpqnFirst = lpqn;
	/* Otherwise, just add it to the end. */
	else lpqueue->lpqnLast->lpqnNext = lpqn;

	lpqueue->lpqnLast = lpqn;
}


/* RandomInteger
 *   Returns a random integer between two bounds.
 *
 * Parameters:
 *   int	iMin, iMax	Bounds (inclusive).
 *
 * Return value: Random number.
 *
 * Remarks:
 *   We only get 30 bits of randomness if RAND_MAX is 32767 (as it always is as
 *   far as I'm aware), so this will be very broken if the range is too large.
 */
int RandomInteger(int iMin, int iMax)
{
	int iRaw = rand() + (RAND_MAX + 1) * rand();

	/* To make the distribution uniform, try again if our random number is
	 * in the little bit at the top beyond the multiples of the range.
	 */
	if(iRaw > (((RAND_MAX + 2) * RAND_MAX) / (iMax - iMin + 1)) * (iMax - iMin + 1))
		return RandomInteger(iMin, iMax);

	return iRaw % (iMax - iMin + 1) + iMin;
}


/* BufferSearch
 *   Searches one buffer for another.
 *
 * Parameters:
 *   const BYTE*	lpbyBuffer, lpvSearch	Buffer to search in and for, resp.
 *   DWORD			cbBuffer, cbSearch		Lengths of those buffers, resp.
 *
 * Return value: BOOL
 *   TRUE if match found; FALSE otherwise.
 */
BOOL BufferSearch(const BYTE *lpbyBuffer, const BYTE *lpbySearch, DWORD cbBuffer, DWORD cbSearch)
{
	DWORD cbOffset;

	if(cbSearch > cbBuffer)
		return FALSE;

	for(cbOffset = 0; cbOffset < cbBuffer - cbSearch; cbOffset++)
		if(memcmp(lpbyBuffer + cbOffset, lpbySearch, cbSearch) == 0)
			return TRUE;

	return FALSE;
}


/* InitGrowArray
 *   Initialises a GROWARRAY.
 *
 * Parameters:
 *   GROWARRAY*	lpgrowarray		The GROWARRAY to initialise.
 *   DWORD		cbElement		Size of an element.
 *   DWORD		cbAllocInitial	Initial number of elements to allocate memory
 *								for.
 *   DWORD		hHeap			Heap to use. NULL for process heap.
 *
 * Return value: None.
 */
void InitGrowArray(GROWARRAY *lpgrowarray, DWORD cbElement, DWORD cAllocInitial, HANDLE hHeap)
{
	lpgrowarray->cAllocedElements = cAllocInitial;
	lpgrowarray->cbElement = cbElement;
	lpgrowarray->cElements = 0;
	lpgrowarray->hHeap = hHeap ? hHeap : g_hProcHeap;
	lpgrowarray->lpvElements = HeapAlloc(lpgrowarray->hHeap, HEAP_GENERATE_EXCEPTIONS, cbElement * cAllocInitial);
}

/* DestroyGrowArray
 *   Destroys a GROWARRAY.
 *
 * Parameters:
 *   GROWARRAY*	lpgrowarray		The GROWARRAY to destroy.
 *
 * Return value: None.
 *
 * Remarks:
 *   Destroying the GROWARRAY is illegal (and unnecessary) after the heap has
 *   been destroyed.
 */
void DestroyGrowArray(GROWARRAY *lpgrowarray)
{
	HeapFree(lpgrowarray->hHeap, 0, lpgrowarray->lpvElements);
}


/* AddToGrowArray
 *   Adds a number of elements to a GROWARRAY, without initialising them.
 *
 * Parameters:
 *   GROWARRAY*	lpgrowarray				The GROWARRAY.
 *   DWORD		cAdditionalElements		Number of elements to add.
 *
 * Return value: None.
 */
void AddToGrowArray(GROWARRAY *lpgrowarray, DWORD cAdditionalElements)
{
	lpgrowarray->cElements += cAdditionalElements;

	if(lpgrowarray->cAllocedElements < lpgrowarray->cElements)
	{
		lpgrowarray->cAllocedElements = lpgrowarray->cElements * 2;
		lpgrowarray->lpvElements = HeapReAlloc(lpgrowarray->hHeap, HEAP_GENERATE_EXCEPTIONS, lpgrowarray->lpvElements, lpgrowarray->cAllocedElements * lpgrowarray->cbElement);
	}
}
