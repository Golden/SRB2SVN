/* SRB2 Workbench -- A map editor for Sonic Robo Blast 2.
 * Copyright (C) 2009 Gregor Dick
 * http://workbench.srb2.org/
 *
 * Incorporates portions of Doom Builder, (C) 2003 Pascal vd Heiden.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 * USA.
 *
 * mapconfig.c: Routines for interpreting game schemata, called "map configs"
 * for historical reasons. The structure for map configs is descended from that
 * used by Doom Builder.
 *
 * AMDG.
 */

#include <windows.h>
#include <tchar.h>
#include <stdio.h>

#include "general.h"
#include "config.h"
#include "options.h"
#include "mapconfig.h"
#include "map.h"
#include "openwads.h"
#include "cdlgwrapper.h"
#include "fof.h"

#include "../res/resource.h"

#include "win/mdiframe.h"


/* Macros. */
#define MAPCFGDIR		TEXT("conf/map/")
#define MAPCFGPATTERN	TEXT("*.cfg")

#define MAPCFG_SIGNATURE	TEXT("SRB2 Workbench Game Configuration")

#define ARROW_DEFAULT	0
#define CIRCLE_DEFAULT	0
#define ERROR_DEFAULT	0
#define HANGS_DEFAULT	0
#define HEIGHT_DEFAULT	64
#define WIDTH_DEFAULT	64
#define COLOUR_DEFAULT	0x808080
#define ZFACTOR_DEFAULT	16


/* We store all map configs as subsections of the following, since it's
 * convenient.
 */
static CONFIG *g_lpcfgMapConfigs;

LPTSTR g_szMapOptDir;


/* Static prototypes. */
static BOOL AddThingsByCategory(CONFIG *lpcfgCategorySS, void *lpv);
static BOOL InheritThingDetailsAndAddToFlatSection(CONFIG *lpcfgThingSS, void *lpv);
static BOOL LDTypeCategoryIterator(CONFIG *lpcfgLDCategory, void *lpv);
static BOOL AddLineTypeToFlatList(CONFIG *lpcfgLDType, void *lpv);
static BOOL AddSingleMapConfigToList(CONFIG *lpcfg, void *lpvData);
static void OutputIndividualEffectText(unsigned short unEffect, CONFIG *lpcfg, LPTSTR szBuffer, unsigned short cchBuffer);
static void UntaintMapConfig(CONFIG *lpcfgMap);
static void MapConfigIDToSubSecName(LPTSTR szMapConfigID);


typedef struct _ATBCDATA
{
	CONFIG *lpcfgFlatThings, *lpcfgThingFlags;
} ATBCDATA;

typedef struct _ALTTFLDATA
{
	CONFIG*	lpcfgFlatTypes;
	LPTSTR	szPrefix;
} ALTTFLDATA;

typedef struct _THINGDETAILS
{
	int		iWidth, iArrow, iError, iHeight, iHangs, iCircle, iColour, iZFactor;
	LPTSTR	szDeafText, szMultiText, szSpecText, szSprite;
} THINGDETAILS;

typedef struct _ITDAATFSDATA
{
	THINGDETAILS	td;
	LPCTSTR			szCategoryName;
	CONFIG			*lpcfgFlatThings;
} ITDAATFSDATA;

typedef struct _AMCTLDATA
{
	HWND				hwnd;
	ENUM_ADDMAPCONFIG	amcListType;
} AMCTLDATA;



/* LoadMapConfigs
 *   Loads all map configuration files from the appropriate directory.
 *
 * Parameters:
 *   None.
 *
 * Return value: int
 *   Number of configs loaded.
 */
int LoadMapConfigs(void)
{
	HANDLE hFind;
	WIN32_FIND_DATA fd;
	int iConfigs = 0;	/* Keep track of the number of map configs loaded. */
	LPTSTR szOldDir;
	unsigned int cchOldDir;
	TCHAR tc;

	/* Save wd -- we change it, since Find[First|Next]File return relative
	 * paths.
	 */
	cchOldDir = GetCurrentDirectory(1, &tc);
	szOldDir = ProcHeapAlloc(cchOldDir * sizeof(TCHAR));
	GetCurrentDirectory(cchOldDir, szOldDir);

	/* Change to the map config directory. */
	SetCurrentDirectory(g_szMapOptDir);
	SetCurrentDirectory(MAPCFGDIR);

	/* Initialise the container. */
	g_lpcfgMapConfigs = ConfigCreate();

	/* Find the first file we're interested in. */
	hFind = FindFirstFile(MAPCFGPATTERN, &fd);

	/* Did we find at least one file? */
	if(hFind != INVALID_HANDLE_VALUE)
	{
		CONFIG *lpcfg;

		/* Repeat for each map config file. */
		do
		{
			/* Load the file. */
			lpcfg = ConfigLoad(fd.cFileName);

			/* Was the file a syntactically-valid config? */
			if(lpcfg)
			{
				/* Check whether the type signature is correct. */
				int iLen;
				LPTSTR szType;
				CONFIG *lpcfgThingCats, *lpcfgLineTypes;
				ATBCDATA atbcdata;
				int cchGameID;

				/* Get the sections of things and line types, organised by
				 * category.
				 */
				lpcfgThingCats = ConfigGetSubsection(lpcfg, TEXT("thingtypes"));
				lpcfgLineTypes = ConfigGetSubsection(lpcfg, TEXT("linedeftypes"));

				/* Make sure everything exists that needs to and certain things
				 * don't that mustn't.
				 */
				if(ConfigNodeExists(lpcfg, FLAT_THING_SECTION) ||
					ConfigNodeExists(lpcfg, FLAT_LINE_SECTION) ||
					!ConfigGetSubsection(lpcfg, TEXT("maplumpnames")) ||
					!ConfigGetSubsection(lpcfg, TEXT("sectortypes")) ||
					ConfigGetStringLength(lpcfg, MAPCFG_GAME) <= 0 ||
					!lpcfgThingCats || !lpcfgLineTypes)
				{
					ConfigDestroy(lpcfg);
					continue;
				}

				/* FOF section validation. */
				if(!ValidateFOFConfig(lpcfg))
				{
					ConfigDestroy(lpcfg);
					continue;
				}

				iLen = ConfigGetStringLength(lpcfg, TEXT("type"));

				if(iLen <= 0)
				{
					ConfigDestroy(lpcfg);
					continue;
				}

				szType = ProcHeapAlloc((iLen + 1) * sizeof(TCHAR));
				ConfigGetString(lpcfg, TEXT("type"), szType, iLen + 1);

				if(_tcsicmp(szType, MAPCFG_SIGNATURE))
				{
					ConfigDestroy(lpcfg);
					ProcHeapFree(szType);
					continue;
				}

				ProcHeapFree(szType);


				/* Fix anything potentially dangerous. */
				UntaintMapConfig(lpcfg);


				/* Things and line types need some special attention. We store
				 * two copies of each: one in its category, and one in a flat
				 * subsection. Also, we fill in parental values for things.
				 */

				/* Create a flat section of things, so we don't have to look
				 * through all the categories for each thing.
				 */
				atbcdata.lpcfgFlatThings = ConfigAddSubsection(lpcfg, FLAT_THING_SECTION);
				atbcdata.lpcfgThingFlags = ConfigGetSubsection(lpcfg, TEXT("thingflags"));

				ConfigIterate(lpcfgThingCats, AddThingsByCategory, &atbcdata);

				/* Do the same for line types. */
				ConfigIterate(lpcfgLineTypes, LDTypeCategoryIterator, ConfigAddSubsection(lpcfg, FLAT_LINE_SECTION));


				/* Add the map config to the global config tree! Use the ID as
				 * the name.
				 */

				cchGameID = ConfigGetStringLength(lpcfg, MAPCFG_ID) + 1;

				if(cchGameID > 1)
				{
					LPTSTR szMapCfgID = ProcHeapAlloc(cchGameID * sizeof(TCHAR));
					ConfigGetString(lpcfg, MAPCFG_ID, szMapCfgID, cchGameID);

					/* Replace illegal characters in the ID. */
					MapConfigIDToSubSecName(szMapCfgID);

					ConfigSetSubsection(g_lpcfgMapConfigs, szMapCfgID, lpcfg);
					ProcHeapFree(szMapCfgID);

					iConfigs++;
				}
				else ConfigDestroy(lpcfg);

			}	/* if(lpcfg) */

		} while(FindNextFile(hFind, &fd));

		FindClose(hFind);
	}

	/* Change back to our old wd. */
	SetCurrentDirectory(szOldDir);
	ProcHeapFree(szOldDir);

	/* Return the number of map configs. */
	return iConfigs;
}




/* AddThingsByCategory
 *   Given a tree of things from the same category, sets their default
 *   properties as specified by the category, and then also adds a copy of each
 *   to a flat section.
 *
 * Parameters:
 *   CONFIG*	lpcfgCategorySS		The category's SUBSECTION CONTAINER!
 *   void*		lpv					(ATBCDATA*) Flat things ss ROOT, thing
 *									flags.
 *
 * Return value: BOOL
 *   TRUE if iteration should continue; FALSE otherwise.
 *
 * Remarks:
 *   Used as a callback by ConfigIterate.
 */
static BOOL AddThingsByCategory(CONFIG *lpcfgCategorySS, void *lpv)
{
	CONFIG *lpcfgCategoryRoot = lpcfgCategorySS->lpcfgSubsection;
	CONFIG *lpcfgFlatThings = ((ATBCDATA*)lpv)->lpcfgFlatThings;
	CONFIG *lpcfgThingFlags = ((ATBCDATA*)lpv)->lpcfgThingFlags;
	CONFIG *lpcfgValues;
	ITDAATFSDATA itdaatfsdata;
	unsigned int cchBuffer;

	/* Something other than a subsection in things section?? */
	if(lpcfgCategorySS->entrytype != CET_SUBSECTION) return TRUE;

	/* Get all the properties specified by the category. These are used as
	 * defaults when things in this category don't specify these values
	 * themselves.
	 */

	itdaatfsdata.td.iArrow		= ConfigNodeExists(lpcfgCategoryRoot, TEXT("arrow"))	? ConfigGetInteger(lpcfgCategoryRoot, TEXT("arrow"))	: ARROW_DEFAULT;
	itdaatfsdata.td.iCircle		= ConfigNodeExists(lpcfgCategoryRoot, TEXT("circle"))	? ConfigGetInteger(lpcfgCategoryRoot, TEXT("circle"))	: CIRCLE_DEFAULT;
	itdaatfsdata.td.iColour		= ConfigNodeExists(lpcfgCategoryRoot, TEXT("color"))	? ConfigGetInteger(lpcfgCategoryRoot, TEXT("color"))	: (signed)COLOUR_DEFAULT;
	itdaatfsdata.td.iError		= ConfigNodeExists(lpcfgCategoryRoot, TEXT("error"))	? ConfigGetInteger(lpcfgCategoryRoot, TEXT("error"))	: ERROR_DEFAULT;
	itdaatfsdata.td.iHangs		= ConfigNodeExists(lpcfgCategoryRoot, TEXT("hangs"))	? ConfigGetInteger(lpcfgCategoryRoot, TEXT("hangs"))	: HANGS_DEFAULT;
	itdaatfsdata.td.iHeight		= ConfigNodeExists(lpcfgCategoryRoot, TEXT("height"))	? ConfigGetInteger(lpcfgCategoryRoot, TEXT("height"))	: HEIGHT_DEFAULT;
	itdaatfsdata.td.iWidth		= ConfigNodeExists(lpcfgCategoryRoot, TEXT("width"))	? ConfigGetInteger(lpcfgCategoryRoot, TEXT("width"))	: WIDTH_DEFAULT;
	itdaatfsdata.td.iZFactor	= ConfigNodeExists(lpcfgCategoryRoot, TEXT("zfactor"))	? ConfigGetInteger(lpcfgCategoryRoot, TEXT("zfactor"))	: ZFACTOR_DEFAULT;

	/* Default string values. */

	if(ConfigNodeExists(lpcfgCategoryRoot, TEXT("deaftext")))
	{
		cchBuffer = ConfigGetStringLength(lpcfgCategoryRoot, TEXT("deaftext")) + 1;
		itdaatfsdata.td.szDeafText = ProcHeapAlloc(cchBuffer * sizeof(TCHAR));
		ConfigGetString(lpcfgCategoryRoot, TEXT("deaftext"), itdaatfsdata.td.szDeafText, cchBuffer);
	}
	else if(lpcfgThingFlags)
	{
		cchBuffer = ConfigGetStringLength(lpcfgThingFlags, TEXT("8")) + 1;
		itdaatfsdata.td.szDeafText = ProcHeapAlloc(cchBuffer * sizeof(TCHAR));
		ConfigGetString(lpcfgThingFlags, TEXT("8"), itdaatfsdata.td.szDeafText, cchBuffer);
	}
	else
	{
		itdaatfsdata.td.szDeafText = ProcHeapAlloc(sizeof(TCHAR));
		itdaatfsdata.td.szDeafText[0] = TEXT('\0');
	}

	if(ConfigNodeExists(lpcfgCategoryRoot, TEXT("multitext")))
	{
		cchBuffer = ConfigGetStringLength(lpcfgCategoryRoot, TEXT("multitext")) + 1;
		itdaatfsdata.td.szMultiText = ProcHeapAlloc(cchBuffer * sizeof(TCHAR));
		ConfigGetString(lpcfgCategoryRoot, TEXT("multitext"), itdaatfsdata.td.szMultiText, cchBuffer);
	}
	else if(lpcfgThingFlags)
	{
		cchBuffer = ConfigGetStringLength(lpcfgThingFlags, TEXT("16")) + 1;
		itdaatfsdata.td.szMultiText = ProcHeapAlloc(cchBuffer * sizeof(TCHAR));
		ConfigGetString(lpcfgThingFlags, TEXT("16"), itdaatfsdata.td.szMultiText, cchBuffer);
	}
	else
	{
		itdaatfsdata.td.szMultiText = ProcHeapAlloc(sizeof(TCHAR));
		itdaatfsdata.td.szMultiText[0] = TEXT('\0');
	}

	if(ConfigNodeExists(lpcfgCategoryRoot, TEXT("spectext")))
	{
		cchBuffer = ConfigGetStringLength(lpcfgCategoryRoot, TEXT("spectext")) + 1;
		itdaatfsdata.td.szSpecText = ProcHeapAlloc(cchBuffer * sizeof(TCHAR));
		ConfigGetString(lpcfgCategoryRoot, TEXT("spectext"), itdaatfsdata.td.szSpecText, cchBuffer);
	}
	else if(lpcfgThingFlags)
	{
		cchBuffer = ConfigGetStringLength(lpcfgThingFlags, TEXT("4")) + 1;
		itdaatfsdata.td.szSpecText = ProcHeapAlloc(cchBuffer * sizeof(TCHAR));
		ConfigGetString(lpcfgThingFlags, TEXT("4"), itdaatfsdata.td.szSpecText, cchBuffer);
	}
	else
	{
		itdaatfsdata.td.szSpecText = ProcHeapAlloc(sizeof(TCHAR));
		itdaatfsdata.td.szSpecText[0] = TEXT('\0');
	}

	if(ConfigNodeExists(lpcfgCategoryRoot, TEXT("sprite")))
	{
		cchBuffer = ConfigGetStringLength(lpcfgCategoryRoot, TEXT("sprite")) + 1;
		itdaatfsdata.td.szSprite = ProcHeapAlloc(cchBuffer * sizeof(TCHAR));
		ConfigGetString(lpcfgCategoryRoot, TEXT("sprite"), itdaatfsdata.td.szSprite, cchBuffer);
	}
	else
	{
		itdaatfsdata.td.szSprite = ProcHeapAlloc(sizeof(TCHAR));
		itdaatfsdata.td.szSprite[0] = TEXT('\0');
	}

	/* Now we iterate over every thing in the category, setting its missing
	 * fields and also adding a copy to the flat section.
	 */
	itdaatfsdata.szCategoryName = lpcfgCategorySS->szName;
	itdaatfsdata.lpcfgFlatThings = lpcfgFlatThings;
	lpcfgValues = ConfigGetSubsection(lpcfgCategoryRoot, TEXT("values"));
	if(lpcfgValues)
		ConfigIterate(lpcfgValues, InheritThingDetailsAndAddToFlatSection, &itdaatfsdata);

	ProcHeapFree(itdaatfsdata.td.szDeafText);
	ProcHeapFree(itdaatfsdata.td.szMultiText);
	ProcHeapFree(itdaatfsdata.td.szSpecText);
	ProcHeapFree(itdaatfsdata.td.szSprite);

	/* Keep going. */
	return TRUE;
}



/* InheritThingDetailsAndAddToFlatSection
 *   Sets a thing's default values where they're missing, and adds a copy of the
 *   thing to a flat section of things.
 *
 * Parameters:
 *   CONFIG*	lpcfgCategorySS		The thing's subsection *container* OR a
 *									string config entry!
 *   void*		lpv					Pointers to the defaults and the flat
 *									section.
 *
 * Return value: BOOL
 *   TRUE if iteration should continue; FALSE otherwise.
 *
 * Remarks:
 *   Used as a callback by ConfigIterate.
 */
static BOOL InheritThingDetailsAndAddToFlatSection(CONFIG *lpcfgThingSS, void *lpv)
{
	THINGDETAILS td = ((ITDAATFSDATA*)lpv)->td;
	CONFIG *lpcfgFlatThings = ((ITDAATFSDATA*)lpv)->lpcfgFlatThings;
	CONFIG *lpcfgThingRoot;
	LPTSTR szTitle, szName;
	WORD cchTitle, cchName;


	/* Determine whether this is a fully-fledged subsection, a simple string, or
	 * some garbage.
	 */
	switch(lpcfgThingSS->entrytype)
	{
	case CET_SUBSECTION:
		lpcfgThingRoot = lpcfgThingSS->lpcfgSubsection;
		break;
	case CET_STRING:
		/* Build a subsection from the string. The string value is the 'title'
		 * field.
		 */
		cchTitle = _tcslen(lpcfgThingSS->sz) + 1;
		szTitle = ProcHeapAlloc(cchTitle * sizeof(TCHAR));
		cchName = _tcslen(lpcfgThingSS->szName) + 1;
		szName = ProcHeapAlloc(cchName * sizeof(TCHAR));

		CopyMemory(szTitle, lpcfgThingSS->sz, cchTitle * sizeof(TCHAR));
		CopyMemory(szName, lpcfgThingSS->szName, cchName * sizeof(TCHAR));

		/* lpcfgThingSS isn't the category root, strictly, but it still fits the
		 * definition.
		 */
		lpcfgThingRoot = ConfigAddSubsection(lpcfgThingSS, szName);
		ConfigSetString(lpcfgThingRoot, TEXT("title"), szTitle);

		ProcHeapFree(szName);
		ProcHeapFree(szTitle);

		break;

	default:
		/* Neither a subsection nor a string. (??) */
		return TRUE;
	}


	/* Set the missing fields. */
	if(!ConfigNodeExists(lpcfgThingRoot, TEXT("arrow"))) ConfigSetInteger(lpcfgThingRoot, TEXT("arrow"), td.iArrow);
	if(!ConfigNodeExists(lpcfgThingRoot, TEXT("circle"))) ConfigSetInteger(lpcfgThingRoot, TEXT("circle"), td.iCircle);
	if(!ConfigNodeExists(lpcfgThingRoot, TEXT("color"))) ConfigSetInteger(lpcfgThingRoot, TEXT("color"), td.iColour);
	if(!ConfigNodeExists(lpcfgThingRoot, TEXT("error"))) ConfigSetInteger(lpcfgThingRoot, TEXT("error"), td.iError);
	if(!ConfigNodeExists(lpcfgThingRoot, TEXT("hangs"))) ConfigSetInteger(lpcfgThingRoot, TEXT("hangs"), td.iHangs);
	if(!ConfigNodeExists(lpcfgThingRoot, TEXT("height"))) ConfigSetInteger(lpcfgThingRoot, TEXT("height"), td.iHeight);
	if(!ConfigNodeExists(lpcfgThingRoot, TEXT("width"))) ConfigSetInteger(lpcfgThingRoot, TEXT("width"), td.iWidth);
	if(!ConfigNodeExists(lpcfgThingRoot, TEXT("zfactor"))) ConfigSetInteger(lpcfgThingRoot, TEXT("zfactor"), td.iZFactor);
	if(!ConfigNodeExists(lpcfgThingRoot, TEXT("deaftext"))) ConfigSetString(lpcfgThingRoot, TEXT("deaftext"), td.szDeafText);
	if(!ConfigNodeExists(lpcfgThingRoot, TEXT("multitext"))) ConfigSetString(lpcfgThingRoot, TEXT("multitext"), td.szMultiText);
	if(!ConfigNodeExists(lpcfgThingRoot, TEXT("spectext"))) ConfigSetString(lpcfgThingRoot, TEXT("spectext"), td.szSpecText);
	if(!ConfigNodeExists(lpcfgThingRoot, TEXT("sprite"))) ConfigSetString(lpcfgThingRoot, TEXT("sprite"), td.szSprite);

	/* Set the category name for cross-referencing from the flat section. */
	ConfigSetString(lpcfgThingRoot, TEXT("category"), ((ITDAATFSDATA*)lpv)->szCategoryName);

	/* Add a *copy* of the thing to the flat section. */
	ConfigSetSubsection(lpcfgFlatThings, lpcfgThingSS->szName, ConfigDuplicate(lpcfgThingRoot));

	/* Keep going. */
	return TRUE;
}


/* LDTypeCategoryIterator
 *   Adds all linedef types in a category to the flat list of linedef types.
 *
 * Parameters:
 *   CONFIG*	lpcfgLDType		A linedef type category subsection.
 *   void*		lpv				Pointer to the flat section.
 *
 * Return value: BOOL
 *   TRUE if iteration should continue; FALSE otherwise.
 *
 * Remarks:
 *   Used as a callback by ConfigIterate.
 */
static BOOL LDTypeCategoryIterator(CONFIG *lpcfgLDCategory, void *lpv)
{
	CONFIG *lpcfgValues;
	int cchPrefix;

	/* Sanity check. */
	if(lpcfgLDCategory->entrytype != CET_SUBSECTION)
		return FALSE;

	/* We should have a line-type category. It's only its values that we're
	 * interested in. Iterate over those.
	 */
	lpcfgValues = ConfigGetSubsection(lpcfgLDCategory->lpcfgSubsection, TEXT("values"));
	cchPrefix = ConfigGetStringLength(lpcfgLDCategory->lpcfgSubsection, TEXT("title"));

	if(lpcfgValues && cchPrefix > 0)
	{
		ALTTFLDATA alttfldata;

		alttfldata.lpcfgFlatTypes = (CONFIG*)lpv;
		alttfldata.szPrefix = ProcHeapAlloc((cchPrefix + 1) * sizeof(TCHAR));
		ConfigGetString(lpcfgLDCategory->lpcfgSubsection, TEXT("title"), alttfldata.szPrefix, cchPrefix + 1);

		ConfigIterate(lpcfgValues, AddLineTypeToFlatList, &alttfldata);

		ProcHeapFree(alttfldata.szPrefix);
	}

	/* Keep going. */
	return TRUE;
}


/* AddLineTypeToFlatList
 *   Adds a linedef type to the flat list of linedef types.
 *
 * Parameters:
 *   CONFIG*	lpcfgLDType		A string entry whose name is the linedef effect
 *								value.
 *   void*		lpv				Pointer to ALTTFLDATA with the flat section and
 *								prefix for the category.
 *
 * Return value: BOOL
 *   TRUE if iteration should continue; FALSE otherwise.
 *
 * Remarks:
 *   Used as a callback by ConfigIterate.
 */
static BOOL AddLineTypeToFlatList(CONFIG *lpcfgLDType, void *lpv)
{
	ALTTFLDATA *lpalttfldata = (ALTTFLDATA*)lpv;

	/* Build the display string from the prefix and the description. */
	unsigned int cchDisplayString = _tcslen(lpcfgLDType->sz) + _tcslen(lpalttfldata->szPrefix) + 3;
	LPTSTR szDisplayString = ProcHeapAlloc(cchDisplayString * sizeof(TCHAR));
	_sntprintf(szDisplayString, cchDisplayString, TEXT("%s: %s"), lpalttfldata->szPrefix, lpcfgLDType->sz);

	/* Add the linedef type to the flat section. */
	ConfigSetString(lpalttfldata->lpcfgFlatTypes, lpcfgLDType->szName, szDisplayString);

	ProcHeapFree(szDisplayString);

	/* Keep going. */
	return TRUE;
}



/* UnloadMapConfigs
 *   Frees all the map configs loaded at startup.
 *
 * Parameters:
 *   None.
 *
 * Return value: None.
 */
void UnloadMapConfigs(void)
{
	ConfigDestroy(g_lpcfgMapConfigs);
}



/* AddMapConfigsToList, AddSingleMapConfigToList
 *   Adds all loaded configs to a combo box or list box.
 *
 * Parameters:
 *   HWND				hwnd			Combo box or list box handle.
 *   ENUM_ADDMAPCONFIG	amcListType		Whether hwnd is a combo box or a list
 *										box.
 *
 * Return value: None.
 *
 * Remarks:
 *   The item data fields are set to pointers to the structures.
 */
void AddMapConfigsToList(HWND hwnd, ENUM_ADDMAPCONFIG amcListType)
{
	AMCTLDATA amctldata = {hwnd, amcListType};
	ConfigIterate(g_lpcfgMapConfigs, AddSingleMapConfigToList, (void*)&amctldata);
}


static BOOL AddSingleMapConfigToList(CONFIG *lpcfg, void *lpvData)
{
	AMCTLDATA *lpamctldata = (AMCTLDATA*)lpvData;
	LPTSTR szBuffer;
	int iIndex;
	int cchBuffer;

	/* Get title. */
	cchBuffer = ConfigGetStringLength(lpcfg->lpcfgSubsection, MAPCFG_GAME) + 1;
	szBuffer = ProcHeapAlloc(cchBuffer * sizeof(TCHAR));
	ConfigGetString(lpcfg->lpcfgSubsection, MAPCFG_GAME, szBuffer, cchBuffer);

	/* Add entry and store pointer to config. */
	iIndex = SendMessage(lpamctldata->hwnd, (lpamctldata->amcListType == AMC_COMBOBOX) ? CB_ADDSTRING : LB_ADDSTRING, 0, (LPARAM)szBuffer);
	SendMessage(lpamctldata->hwnd, (lpamctldata->amcListType == AMC_COMBOBOX) ? CB_SETITEMDATA : LB_SETITEMDATA, iIndex, (LPARAM)lpcfg->lpcfgSubsection);

	ProcHeapFree(szBuffer);

	/* Keep iterating. */
	return TRUE;
}



/* GetThingConfigInfo
 *   Gets config information about a particular type of thing.
 *
 * Parameters:
 *   CONFIG*			lpcfgThings		Root of flat things subsection.
 *   unsigned short		unType			Thing ID.
 *
 * Return value: CONFIG*
 *   Pointer to subsection for the specified thing-type, or NULL if none exists.
 */
CONFIG* GetThingConfigInfo(CONFIG *lpcfgThings, unsigned short unType)
{
	/* Enough space to represent an unsigned short. */
	TCHAR szTypeEntry[6];

	/* Format the effect number for the config entry. */
	_sntprintf(szTypeEntry, NUM_ELEMENTS(szTypeEntry), TEXT("%u"), unType);

	/* Is there an entry for this effect in the config? */
	if(ConfigNodeExists(lpcfgThings, szTypeEntry))
		return ConfigGetSubsection(lpcfgThings, szTypeEntry);
	else return NULL;
}






/* GetThingTypeDisplayText
 *   Builds the display string for a thing's type.
 *
 * Parameters:
 *   unsigned short		unType		Type specifier.
 *   CONFIG*			lpcfgThings	Config tree containing thing information.
 *   LPTSTR				szBuffer	Buffer to store string in.
 *   unsigned short		cchBuffer	Length of buffer, including terminator.
 *
 * Return value: None.
 */
void GetThingTypeDisplayText(unsigned short unType, CONFIG *lpcfgThings, LPTSTR szBuffer, unsigned short cchBuffer)
{
	LPTSTR szTypeText;
	CONFIG *lpcfgThingProps;

	/* We certainly don't need to be any longer than the buffer we return in. */
	szTypeText = ProcHeapAlloc(cchBuffer * sizeof(TCHAR));

	/* Is there an entry for this effect in the config? */
	if((lpcfgThingProps = GetThingConfigInfo(lpcfgThings, unType)))
		ConfigGetString(lpcfgThingProps, TEXT("title"), szTypeText, cchBuffer);
	else LoadString(g_hInstance, IDS_UNKNOWNTHING, szTypeText, cchBuffer);

	_sntprintf(szBuffer, cchBuffer - 1, TEXT("%u - %s"), unType, szTypeText);
	szBuffer[cchBuffer - 1] = '\0';

	ProcHeapFree(szTypeText);
}


/* GetThingDirectionDisplayText
 *   Builds the display string for a thing's direction.
 *
 * Parameters:
 *   unsigned short		nDirection	Direction as per wad-spec (i.e. degrees in
 *									the positive direction from the x-axis).
 *   LPTSTR				szBuffer	Buffer to store string in.
 *   unsigned short		cbBuffer	Length of buffer, including terminator.
 *
 * Return value: None.
 */
void GetThingDirectionDisplayText(short nDirection, LPTSTR szBuffer, unsigned short cbBuffer)
{
	TCHAR szDirString[16];
	UINT uiDirectionID;

	if(nDirection < 23 || nDirection > 337) uiDirectionID = IDS_EAST;
	else if(nDirection < 68) uiDirectionID = IDS_NORTHEAST;
	else if(nDirection < 113) uiDirectionID = IDS_NORTH;
	else if(nDirection < 158) uiDirectionID = IDS_NORTHWEST;
	else if(nDirection < 203) uiDirectionID = IDS_WEST;
	else if(nDirection < 248) uiDirectionID = IDS_SOUTHWEST;
	else if(nDirection < 293) uiDirectionID = IDS_SOUTH;
	else uiDirectionID = IDS_SOUTHEAST;

	LoadString(g_hInstance, uiDirectionID, szDirString, sizeof(szDirString) / sizeof(TCHAR));

	_sntprintf(szBuffer, cbBuffer - 1, TEXT("%s (%d)"), szDirString, nDirection);
	szBuffer[cbBuffer - 1] = '\0';
}



/* GetEffectDisplayText
 *   Builds the display string for a sector's effect.
 *
 * Parameters:
 *   unsigned short		unEffect	Effect specifier.
 *   CONFIG*			lpcfg		Config containing effect descriptions.
 *   LPTSTR				szBuffer	Buffer to store string in.
 *   unsigned short		cchBuffer	Length of buffer, including terminator.
 *   BOOL				bNybble		Whether effects are encoded by nybble.
 *
 * Return value: None.
 *
 * Remarks:
 *   This works for sectors and old-format linedefs.
 */
void GetEffectDisplayText(unsigned short unEffect, CONFIG *lpcfg, LPTSTR szBuffer, unsigned short cchBuffer, BOOL bNybble)
{
	/* Sanity check. */
	if(cchBuffer == 0) return;

	/* If we have multiple effects encoded, we need to get multiple strings --
	 * at least, potentially.
	 */
	if(bNybble)
	{
		int i;
		WORD cchInStr = 0;
		CONFIG *lpcfgEffectsByNybble[] =
		{
			ConfigGetSubsection(lpcfg, TEXT("nybble0")),
			ConfigGetSubsection(lpcfg, TEXT("nybble1")),
			ConfigGetSubsection(lpcfg, TEXT("nybble2")),
			ConfigGetSubsection(lpcfg, TEXT("nybble3"))
		};

		for(i = 0; i < (int)NUM_ELEMENTS(lpcfgEffectsByNybble) && cchInStr + 1 < cchBuffer; i++)
		{
			/* Extract the value from this nybble. */
			BYTE byEffectNybble = (unEffect >> (i << 2)) & 0xF;

			/* We ignore zero effects, except for the first time around. */
			if(byEffectNybble != 0 || i == 0)
			{
				const TCHAR szSeparator[] = TEXT("; ");

				/* If we're not at the very start of the string, add in a
				 * separator.
				 */
				if(cchInStr > 0)
				{
					_tcsncat(szBuffer + cchInStr, szSeparator, cchBuffer - cchInStr);
					cchInStr += _tcslen(szBuffer + cchInStr);
				}

				/* Output the effect text. */
				OutputIndividualEffectText(byEffectNybble, lpcfgEffectsByNybble[i], szBuffer + cchInStr, cchBuffer - cchInStr);
				cchInStr += _tcslen(szBuffer + cchInStr);
			}
		}
	}
	else
		OutputIndividualEffectText(unEffect, lpcfg, szBuffer, cchBuffer);
}

/* Helper routine for the above. */
static void OutputIndividualEffectText(unsigned short unEffect, CONFIG *lpcfg, LPTSTR szBuffer, unsigned short cchBuffer)
{
	TCHAR szEffectEntry[CCH_SIGNED_INT];
	LPTSTR szEffectText;

	/* We certainly don't need to be any longer than the buffer we return in. */
	szEffectText = ProcHeapAlloc(cchBuffer * sizeof(TCHAR));

	/* Format the effect number for the config entry. */
	_sntprintf(szEffectEntry, NUM_ELEMENTS(szEffectEntry), TEXT("%u"), unEffect);

	/* Is there an entry for this effect in the config? */
	if(ConfigNodeExists(lpcfg, szEffectEntry))
		ConfigGetString(lpcfg, szEffectEntry, szEffectText, cchBuffer);
	else LoadString(g_hInstance, IDS_UNKNOWNEFFECT, szEffectText, cchBuffer);

	_sntprintf(szBuffer, cchBuffer - 1, TEXT("%u - %s"), unEffect, szEffectText);
	szBuffer[cchBuffer - 1] = '\0';

	ProcHeapFree(szEffectText);
}



/* SetAllThingPropertiesFromType, SetThingPropertiesFromType
 *   Sets some fields of (a) thing(s), based on those specified for the thing
 *   type.
 *
 * Parameters:
 *   MAP*		lpmap				Pointer to map data.
 *   int		iThing				Index of thing.
 *   CONFIG*	lpcfgFlatThings		Flat thing config structure.
 *
 * Return value: None.
 */
void SetAllThingPropertiesFromType(MAP *lpmap, CONFIG *lpcfgFlatThings)
{
	int i;

	for(i = 0; i < lpmap->iThings; i++)
		SetThingPropertiesFromType(lpmap, i, lpcfgFlatThings);
}

void SetThingPropertiesFromType(MAP *lpmap, int iThing, CONFIG *lpcfgFlatThings)
{
	TCHAR szTypeEntry[6];

	/* Format the effect number for the config entry. */
	_sntprintf(szTypeEntry, NUM_ELEMENTS(szTypeEntry), TEXT("%u"), lpmap->things[iThing].thing);

	/* Is there an entry for this effect in the config? */
	if(ConfigNodeExists(lpcfgFlatThings, szTypeEntry))
	{
		CONFIG *lpcfgThing = ConfigGetSubsection(lpcfgFlatThings, szTypeEntry);
		lpmap->things[iThing].arrow = ConfigGetInteger(lpcfgThing, TEXT("arrow"));
		lpmap->things[iThing].color = ConfigGetInteger(lpcfgThing, TEXT("color"));
		lpmap->things[iThing].circle = ConfigGetInteger(lpcfgThing, TEXT("circle"));
	}
	else
	{
		lpmap->things[iThing].arrow = FALSE;
		lpmap->things[iThing].color = COLOUR_DEFAULT;
		lpmap->things[iThing].circle = FALSE;
	}
}


/* GetZFactor
 *   Retrieves the z-factor for a thing type.
 *
 * Parameters:
 *   CONFIG*			lpcfgFlatThings		Flat thing config structure.
 *   unsigned short		unType				Type of thing.
 *
 * Return value: WORD
 *   Z-factor.
 */
WORD GetZFactor(CONFIG *lpcfgFlatThings, unsigned short unType)
{
	CONFIG *lpcfgThing = GetThingConfigInfo(lpcfgFlatThings, unType);

	/* No entry in the config for this type of thing? */
	if(!lpcfgThing) return ZFACTOR_DEFAULT;

	return (WORD)ConfigGetInteger(lpcfgThing, TEXT("zfactor"));
}


/* GetIWadForConfig
 *   Gets the index of the IWAD for a map config, loading it first if necessary.
 *
 * Parameters:
 *   CONFIG*	lpcfgMap	Map configuration. *Not* the container.
 *
 * Return value: int
 *   Index of IWAD.
 */
int GetIWadForConfig(CONFIG *lpcfgMap)
{
	int iIWad = -1;
	CONFIG *lpcfgGame;
	int cchIWad;

	/* Game-config-specific options. */
	lpcfgGame = GetOptionsForGame(lpcfgMap);

	cchIWad = ConfigGetStringLength(lpcfgGame, GAMECFG_IWAD);

	/* Attempt to load IWAD if there's one specified. */
	if(cchIWad > 0)
	{
		LPTSTR szIWad;

		szIWad = ProcHeapAlloc((cchIWad + 1) * sizeof(TCHAR));
		ConfigGetString(lpcfgGame, GAMECFG_IWAD, szIWad, cchIWad + 1);

		iIWad = LoadWad(szIWad);

		ProcHeapFree(szIWad);
	}

	if(iIWad < 0 &&
		IDYES == MessageBoxFromStringTable(g_hwndMain, IDS_IWAD_QUERY, MB_ICONEXCLAMATION | MB_YESNO))
	{
		TCHAR szFilename[MAX_PATH] = TEXT("");

		/* Look for the IWAD. */
		if(BrowseForIWAD(szFilename, NUM_ELEMENTS(szFilename)))
		{
			/* If the user specified an IWAD, remember it and try to
			 * open it.
			 */
			ConfigSetString(lpcfgGame, GAMECFG_IWAD, szFilename);

			if((iIWad = LoadWad(szFilename)) < 0)
				MessageBoxFromStringTable(g_hwndMain, IDS_IWAD_FAIL, MB_ICONERROR);
		}
	}



	/* If cchGameID <= 0, the map config is malformed. */

	return iIWad;
}

/* GetDefaultMapConfig
 *   Returns the default map config.
 *
 * Parameters:
 *   None.
 *
 * Return value: CONFIG*
 *   Default map config.
 */
CONFIG *GetDefaultMapConfig(void)
{
	/* TODO: Actually implement this correctly rather than just taking the first
	 * one we find.
	 */
	return g_lpcfgMapConfigs->lpcfgLeft ?
		g_lpcfgMapConfigs->lpcfgLeft->lpcfgSubsection :
		g_lpcfgMapConfigs->lpcfgRight->lpcfgSubsection;
}


/* UntaintMapConfig
 *   Fixes anything potentially dangerous in a map config.
 *
 * Parameters:
 *   CONFIG*	lpcfgMap	Map config.
 *
 * Return value: None.
 */
static void UntaintMapConfig(CONFIG *lpcfgMap)
{
	CONFIG *lpcfgWiki = ConfigGetSubsection(lpcfgMap, MAPCFG_WIKI);

	if(lpcfgWiki)
	{
		LPCTSTR szFormatNames[] = {WIKI_LDEFFECT, WIKI_SECEFFECT, WIKI_THING};
		int i;

		/* Untaint each of the map component format strings. */
		for(i = 0; i < (int)(sizeof(szFormatNames)/sizeof(const char*)); i++)
		{
			/* Allocate twice as many characters as we need, which is (more
			 * than) enough to replace every possible errant %.
			 */
			UINT uiInitialLength = ConfigGetStringLength(lpcfgWiki, szFormatNames[i]);
			UINT cchBuffer = (uiInitialLength << 1) + 1;
			LPTSTR szFormat = ProcHeapAlloc(cchBuffer * sizeof(TCHAR));
			LPTSTR szFormatFinal = szFormat;
			LPTSTR szInitialFormat = szFormat + uiInitialLength;

			if(uiInitialLength > 0)
			{
				/* We're only allowed one %s. */
				BOOL bFoundFormatField = FALSE;

				/* Put the initial string at the *end* of the buffer. */
				ConfigGetString(lpcfgWiki, szFormatNames[i], szInitialFormat, uiInitialLength + 1);

				/* Copy the string to the beginning of the buffer, replacing
				 * anything that's unsafe.
				 */
				while(*szInitialFormat) switch(*szInitialFormat)
				{
				case TEXT('%'):
					/* We found a percentage sign. It's the next character that
					 * determines what we have to do.
					 */

					if(szInitialFormat[1] != TEXT('d') || bFoundFormatField)
						/* We actually want a % here. */
						*szFormat++ = TEXT('%');
					/* Otherwise, it's our format field, but don't allow it next
					 * time.
					 */
					else bFoundFormatField = TRUE;

					/* Fall through. */
				default:
					/* Just copy the character. */
					*szFormat++ = *szInitialFormat++;
				}
			}

			/* Terminate the string. */
			*szFormat = TEXT('\0');

			/* Write it back to the config. */
			ConfigSetString(lpcfgWiki, szFormatNames[i], szFormatFinal);

			/* Free the buffer. */
			ProcHeapFree(szFormatFinal);
		}
	}
}


/* MapConfigIDToSubSecName
 *   Converts a map config ID to the name used for the subsection in the global
 *   collection of map configs, by replacing illegal characters.
 *
 * Parameters:
 *   LPTSTR	szMapConfigID	ID to convert.
 *
 * Return value: None.
 */
static void MapConfigIDToSubSecName(LPTSTR szMapConfigID)
{
	while((szMapConfigID = _tcspbrk(szMapConfigID, TEXT(" \t\r\n={};")))) *szMapConfigID = '_';
}


/* GetMapConfigByID
 *   Retrieves the map config having a specified ID.
 *
 * Parameters:
 *   LPCTSTR	szMapConfigID	ID of map config.
 *
 * Return value: CONFIG*
 *   Pointer to matching map config, or NULL if not found.
 */
CONFIG* GetMapConfigByID(LPCTSTR szMapConfigID)
{
	UINT cchID = _tcslen(szMapConfigID) + 1;
	LPTSTR szCleanedID = ProcHeapAlloc(cchID * sizeof(TCHAR));
	CONFIG *lpcfgMap;

	/* Make a copy of the ID so we can convert it to the required format. */
	CopyMemory(szCleanedID, szMapConfigID, cchID * sizeof(TCHAR));

	MapConfigIDToSubSecName(szCleanedID);

	/* Look for a matching config. */
	lpcfgMap = ConfigGetSubsection(g_lpcfgMapConfigs, szCleanedID);

	ProcHeapFree(szCleanedID);

	return lpcfgMap;
}


/* BrowseForIWAD
 *   Displays an Open common dialogue to allow the user to choose an IWAD.
 *
 * Parameters:
 *   LPCTSTR	szFilename		Buffer used for initial filename and returned
 *								filename.
 *   DWORD		cchFilename		Size of buffer.
 *
 * Return value: int
 *   Nonzero if OKed; zero if cancelled.
 */
int BrowseForIWAD(LPTSTR szFilename, DWORD cchFilename)
{
	TCHAR szFilter[128];

	/* Get and tidy up the filter string. */
	LoadAndFormatFilterString(IDS_WADFILEFILTER, szFilter, NUM_ELEMENTS(szFilter));

	/* Look for the IWAD. */
	return CommDlgOpen(g_hwndMain, szFilename, cchFilename, NULL, szFilter, TEXT("wad"), szFilename, OFN_EXPLORER | OFN_FILEMUSTEXIST | OFN_HIDEREADONLY);
}


/* BrowseForBinary
 *   Displays an Open common dialogue to allow the user to choose a binary.
 *
 * Parameters:
 *   LPCTSTR	szFilename		Buffer used for initial filename and returned
 *								filename.
 *   DWORD		cchFilename		Size of buffer.
 *
 * Return value: int
 *   Nonzero if OKed; zero if cancelled.
 *
 * Remarks:
 *   Suitable for nodebuilder as well as game binaries.
 */
int BrowseForBinary(LPTSTR szFilename, DWORD cchFilename)
{
	TCHAR szFilter[128];

	/* Get and tidy up the filter string. */
	LoadAndFormatFilterString(IDS_BINARYFILTER, szFilter, NUM_ELEMENTS(szFilter));

	/* Look for the IWAD. */
	return CommDlgOpen(g_hwndMain, szFilename, cchFilename, NULL, szFilter, TEXT("exe"), szFilename, OFN_EXPLORER | OFN_FILEMUSTEXIST | OFN_HIDEREADONLY);
}


/* SetMapConfigDirectory
 *   Sets the directory from which map configurations are loaded.
 *
 * Parameters:
 *   LPCTSTR	szDir	Directory, either relative or absolute. NULL to use
 *						default.
 *
 * Return value: None.
 *
 * Remarks:
 *   This must be called before attempting to open any map config files. Call
 *   FreeMapConfigDirectoryName at shutdown to free memory.
 */
void SetMapConfigDirectory(LPCTSTR szDir)
{
	TCHAR szFullPath[MAX_PATH];

	if(szDir)
	{
		LPTSTR szFilePart;

		/* Expand the directory name we were given. */
		DWORD cch = GetFullPathName(szDir, sizeof(szFullPath)/sizeof(TCHAR), szFullPath, &szFilePart);

		/* Failed, so fall back to default. */
		if(cch == 0 || cch >= sizeof(szFullPath)/sizeof(TCHAR))
			szDir = NULL;
	}

	if(!szDir)
	{
		int i;

		/* Get and terminate path name. */
		GetModuleFileName(NULL, szFullPath, NUM_ELEMENTS(szFullPath) - 1);
		szFullPath[NUM_ELEMENTS(szFullPath) - 1] = TEXT('\0');

		/* Replace the filename with the name of the conf directory. */
		i = _tcslen(szFullPath);
		while(szFullPath[i-1] != '\\' && szFullPath[i-1] != '/') i--;
		_tcsncpy(&szFullPath[i], MAPCFGDIR, NUM_ELEMENTS(szFullPath) - i - 1);
	}

	/* Copy to the heap and save address in a global. */
	g_szMapOptDir = ProcHeapAlloc((_tcslen(szFullPath) + 1) * sizeof(TCHAR));
	_tcscpy(g_szMapOptDir, szFullPath);
}


/* FreeMapConfigDirectoryName
 *   Frees the map config directory name.
 *
 * Parameters:
 *   None.
 *
 * Return value: None.
 */
void FreeMapConfigDirectoryName(void)
{
	ProcHeapFree(g_szMapOptDir);
}
