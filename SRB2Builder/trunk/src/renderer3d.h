/* SRB2 Workbench -- A map editor for Sonic Robo Blast 2.
 * Copyright (C) 2011 Gregor Dick
 * http://workbench.srb2.org/
 *
 * Incorporates portions of Doom Builder, (C) 2003 Pascal vd Heiden.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 * USA.
 *
 * renderer.h: Header for renderer3d.c.
 *
 * AMDG.
 */

#ifndef __SRB2B_RENDERER3D__
#define __SRB2B_RENDERER3D__

#ifdef __cplusplus
extern "C" {
#endif

#include <windows.h>
#include <GL/gl.h>
#include <GL/glu.h>

#include "fof.h"

#include "win/mapwin.h"

typedef struct _MAPSURFACES MAPSURFACES;
typedef struct _BSP_TREE BSP_TREE;

MAPSURFACES* GenerateMapSurfaces(MAP *lpmap, HWND hwndMap, LPCTSTR szSky, BSP_TREE **lplpbsptree, CONFIG *lpcfgFOFTypes, FOF_FLAGS *lpfofflags, int iColourmap);
void FreeMapSurfaces(MAP *lpmap, MAPSURFACES *lpmapsurfaces, BSP_TREE *lpbsptree);
void RedrawMap3D(MAP *lpmap, MAPSURFACES *lpmapsurfaces, BSP_TREE *lpbsptree, MAPVIEW *lpmapview);
void Init3D(MAPVIEW *lpmapview, float x, float y, float z);
void Set3DProjection(MAPVIEW *lpmapview);
void Zoom3D(MAPVIEW *lpmapview, float fZoomDelta);
UINT PickObject(MAP *lpmap, MAPSURFACES *lpmapsurfaces, BSP_TREE *lpbsptree, MAPVIEW *lpmapview, short x, short y);
ENUM_EDITMODE GetEditorObjectTypeFromPickedName(UINT uiName);
WPARAM GetQuickEditTypeFromPickedName(UINT uiName);
int GetIndexFromPickedName(UINT uiName);
void UpdateLineSurfaces(MAP *lpmap, HWND hwndMap, MAPSURFACES *lpmapsurfaces, int iLinedef);
void UpdateSectorSurfaces(MAP *lpmap, HWND hwndMap, MAPSURFACES *lpmapsurfaces, int iSector);
void UpdateNeededLinedefSurfacesBySector(MAP *lpmap, MAPSURFACES *lpmapsurfaces, HWND hwndMap, LPCTSTR szSky, int iSector);
void SetNeededSurfacesForLinedef(MAP *lpmap, int iLinedef, LPCTSTR szSky, int iSideOfLine, MAPSURFACES *lpmapsurfaces, HWND hwndMap);
void UpdateFOFPlanes(MAPSURFACES *lpmapsurfaces, MAP *lpmap, HWND hwndMap, int iControlLinedef);
void UpdateFOFWalls(MAPSURFACES *lpmapsurfaces, MAP *lpmap, HWND hwndMap, int iControlLinedef);

#ifdef __cplusplus
}
#endif

#endif
