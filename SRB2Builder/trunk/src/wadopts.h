/* SRB2 Workbench -- A map editor for Sonic Robo Blast 2.
 * Copyright (C) 2009 Gregor Dick
 * http://workbench.srb2.org/
 *
 * Incorporates portions of Doom Builder, (C) 2003 Pascal vd Heiden.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 * USA.
 *
 * wadopts.h: Header for wadopts.c.
 *
 * AMDG.
 */

#ifndef __SRB2B_WADOPTS__
#define __SRB2B_WADOPTS__

#include <windows.h>

#include "config.h"


#define WADOPT_MAPCONFIG		TEXT("mapconfig")
#define WADOPT_NODEBUILD		TEXT("nodebuild")
#define WADOPT_BLOCKMAP			TEXT("buildblockmap")
#define WADOPT_DEFAULTTEX		TEXT("defaulttexture")
#define WADOPT_DEFAULTSEC		TEXT("defaultsector")

#define WADOPT_MAP_GAMETYPE		TEXT("gametype")
#define WADOPT_MAP_ADDWAD		TEXT("resourcewad")


enum ENUM_NODEBUILDERS
{
	NODEBUILD_DISABLED,
	NODEBUILD_INTERNAL,
	NODEBUILD_EXTERNAL
};


CONFIG* LoadWadOptions(LPCTSTR szWadFilename);
CONFIG* NewWadOptions(void);
void StoreMapCfgForWad(CONFIG *lpcfgWad, CONFIG *lpcfgMap);
CONFIG* GetMapOptionsFromWadOpt(CONFIG *lpcfgWadOpt, LPCTSTR szLumpname);
int WriteWadOptions(LPCTSTR szWadFilename, CONFIG *lpcfgWad);
CONFIG* GetDefaultMapConfigForWad(int iWad);


#endif
