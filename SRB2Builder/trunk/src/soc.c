/* SRB2 Workbench -- A map editor for Sonic Robo Blast 2.
 * Copyright (C) 2009 Gregor Dick
 * http://workbench.srb2.org/
 *
 * Incorporates portions of Doom Builder, (C) 2003 Pascal vd Heiden.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 * USA.
 *
 * cliputil.c: Routines for handling SOCs, including map headers.
 *
 * AMDG.
 */

#include <windows.h>
#include <stdio.h>

#include "general.h"
#include "soc.h"
#include "map.h"
#include "texture.h"
#include "editing.h"



typedef struct _DYNAMIC_SOC_BUFFER
{
	BYTE	*lpbyBuffer;
	DWORD	cchBuffer;
	DWORD	cchWritten;
} DYNAMIC_SOC_BUFFER;



#define DEFAULT_MUSICSLOT			1
#define DEFAULT_SKYNUM				1
#define DEFAULT_TYPEOFLEVEL			4099	/* SCR */
#define DEFAULT_WEATHER				0

#define INIT_HEADER_SOC_BUFSIZE		1024
#define CCH_SOC_LINE				128


#define FreeHeaderSOCSection		ProcHeapFree

static BOOL ExtractMapHeader(MAPHEADER *lpmapheader, LPCSTR sSOC, DWORD cchSOC, int iLevelNum, DWORD *lpcchOffsetStart, DWORD *lpcchOffsetEnd);
static void TerminateSOCLine(LPSTR szSOC, DWORD cchSOC, DWORD *lpcchOffset);
static __inline LPSTR PruneWSAndComments(LPSTR szSOCLine);
static __inline void MakeHeaderLumpname(LPSTR szLumpname, int iLevelNum);
static BYTE* GenerateHeaderSOCSection(MAPHEADER *lpmapheader, int iLevelNum, DWORD *lpcch);
static __inline void DynamicSOCWrite(DYNAMIC_SOC_BUFFER *lpdsb, LPSTR sz);



/* ExtractMapHeader
 *   Extracts a map header for a specific level from a SOC lump.
 *
 * Parameters:
 *   MAPHEADER*		lpmapheader			Map header. May be NULL if only the
 *										extent of the header is required.
 *   LPCSTR			sSOC				SOC lump.
 *   DWORD			cchSOC				Length of SOC lump.
 *   int			iLevelNum			Number of level whose header is to be
 *										extracted.
 *   DWORD*			lpcchOffsetStart	Offset of start of header in sSOC is
 *										returned here. May be NULL.
 *   DWORD*			lpcchOffsetEnd		Offset of first character following
 *										header in sSOC is returned here. May be
 *										NULL.
 *
 * Return value: BOOL
 *   TRUE if header is found; FALSE otherwise.
 *
 * Remarks:
 *   If the header runs right to the end of sSOC, *lpcchOffsetEnd will be set to
 *   cchSOC, i.e. just past the end of the buffer. Call FreeMapHeaderStrings on
 *   lpmapheader to free memory allocated by this function. Any pointers to
 *   existing memory associated with *lpmapheader may be clobbered without the
 *   memory being freed first. The offsets are not touched if a header is not
 *   found.
 */
static BOOL ExtractMapHeader(MAPHEADER *lpmapheader, LPCSTR sSOC, DWORD cchSOC, int iLevelNum, DWORD *lpcchOffsetStart, DWORD *lpcchOffsetEnd)
{
	LPSTR szSOC = ProcHeapAlloc((cchSOC + 1) * sizeof(char));
	BOOL bFoundHeader = FALSE;
	DWORD cchOffset = 0;

	/* Make a NUL-terminated version of the SOC. */
	CopyMemory(szSOC, sSOC, cchSOC);
	szSOC[cchSOC] = '\0';

	while(cchOffset < cchSOC)
	{
		LPSTR szInLine = &szSOC[cchOffset];
		LPSTR szFirstWord;
		DWORD cchLineStart = cchOffset;

		/* Terminate the line and advance beyond it. */
		TerminateSOCLine(szSOC, cchSOC, &cchOffset);

		/* Tidy the line. */
		szInLine = PruneWSAndComments(szInLine);

		szFirstWord = strtok(szInLine, " \t");

		/* Are we interested in the line at all? */
		if(szFirstWord && !_stricmp(szFirstWord, "LEVEL"))
		{
			LPSTR szSecondWord = strtok(NULL, " \t");

			/* Check whether this is for the right level. */
			if(szSecondWord && atoi(szSecondWord) == iLevelNum)
			{
				bFoundHeader = TRUE;
				if(lpcchOffsetStart) *lpcchOffsetStart = cchLineStart;

				/* No fields valid yet. */
				if(lpmapheader) lpmapheader->dwMask = 0;

				/* Modal parsing loop. */
				while((cchLineStart = cchOffset) < cchSOC)
				{
					LPSTR szVar, szValue;
					int iValue;
					DWORD cchValue;

					/* Terminate the line and advance beyond it. */
					szInLine = &szSOC[cchOffset];
					TerminateSOCLine(szSOC, cchSOC, &cchOffset);

					/* If we found a blank line, the header ends here. */
					if(!*szInLine) break;

					/* If we're only interested in the extent of the header, we
					 * don't actually care about the contents of the line.
					 */
					if(!lpmapheader) continue;

					/* Tidy the line. */
					szInLine = PruneWSAndComments(szInLine);

					/* No equals? No thanks! */
					if(!strchr(szInLine, '=')) continue;

					/* Get the variable name and its value. */
					szVar = strtok(szInLine, " \t=");
					szValue = strtok(NULL, "");

					/* Ignore pathological lines. */
					if(!szVar || !szValue) continue;

					/* Remove whitespace and equals sign from start of value. */
					while(*szValue == ' ' || *szValue == '\t' || *szValue == '=')
						szValue++;

					/* For ease of comparison. */
					_strupr(szVar);

					/* We won't need both of these, but never mind that. */
					iValue = atoi(szValue);
					cchValue = strlen(szValue) + 1;

					/* What follows is adapted from SRB2 itself. Where we
					 * allocate heap memory, it's freed by FreeMapHeaderStrings.
					 */
					if(!strcmp(szVar, "LEVELNAME"))
					{
						lpmapheader->szLevelName = ProcHeapAlloc(cchValue * sizeof(char));
						CopyMemory(lpmapheader->szLevelName, szValue, cchValue);
						lpmapheader->dwMask |= MHF_LEVELNAME;
					}
					else if(!strcmp(szVar, "SUBTITLE"))
					{
						lpmapheader->szSubtitle = ProcHeapAlloc(cchValue * sizeof(char));
						CopyMemory(lpmapheader->szSubtitle, szValue, cchValue);
						lpmapheader->dwMask |= MHF_SUBTITLE;
					}
					else if(!strcmp(szVar, "ACT"))
					{
						lpmapheader->iAct = iValue;
						lpmapheader->dwMask |= MHF_ACT;
					}
					else if(!strcmp(szVar, "TYPEOFLEVEL"))
					{
						lpmapheader->dwTypeOfLevel = (DWORD)iValue;
						lpmapheader->dwMask |= MHF_TYPEOFLEVEL;
					}
					else if(!strcmp(szVar, "NEXTLEVEL"))
					{
						lpmapheader->iNextLevel = iValue;
						lpmapheader->dwMask |= MHF_NEXTLEVEL;
					}
					else if(!strcmp(szVar, "MUSICSLOT"))
					{
						lpmapheader->iMusicSlot = iValue;
						lpmapheader->dwMask |= MHF_MUSICSLOT;
					}
					else if(!strcmp(szVar, "FORCECHARACTER"))
					{
						lpmapheader->iForceCharacter = iValue;
						lpmapheader->dwMask |= MHF_FORCECHARACTER;
					}
					else if(!strcmp(szVar, "WEATHER"))
					{
						lpmapheader->iWeather = iValue;
						lpmapheader->dwMask |= MHF_WEATHER;
					}
					else if(!strcmp(szVar, "SKYNUM"))
					{
						lpmapheader->iSkyNum = iValue;
						lpmapheader->dwMask |= MHF_SKYNUM;
					}
					else if(!strcmp(szVar, "INTERSCREEN"))
					{
						strncpy(lpmapheader->szInterScreen, szValue, NUM_ELEMENTS(lpmapheader->szInterScreen) - 1);
						lpmapheader->szInterScreen[NUM_ELEMENTS(lpmapheader->szInterScreen) - 1] = '\0';
						lpmapheader->dwMask |= MHF_INTERSCREEN;
					}
					else if(!strcmp(szVar, "SCRIPTNAME"))
					{
						lpmapheader->szScriptName = ProcHeapAlloc(cchValue * sizeof(char));
						CopyMemory(lpmapheader->szScriptName, szValue, cchValue);
						lpmapheader->dwMask |= MHF_SCRIPTNAME;
					}
					else if(!strcmp(szVar, "SCRIPTISLUMP"))
					{
						lpmapheader->bScriptIsLump = iValue;
						lpmapheader->dwMask |= MHF_SCRIPTISLUMP;
					}
					else if(!strcmp(szVar, "PRECUTSCENENUM"))
					{
						lpmapheader->iPreCutsceneNum = iValue;
						lpmapheader->dwMask |= MHF_PRECUTSCENENUM;
					}
					else if(!strcmp(szVar, "CUTSCENENUM"))
					{
						lpmapheader->iCutsceneNum = iValue;
						lpmapheader->dwMask |= MHF_CUTSCENENUM;
					}
					else if(!strcmp(szVar, "COUNTDOWN"))
					{
						lpmapheader->iCountdown = iValue;
						lpmapheader->dwMask |= MHF_COUNTDOWN;
					}
					else if(!strcmp(szVar, "NOZONE"))
					{
						lpmapheader->bNoZone = iValue;
						lpmapheader->dwMask |= MHF_NOZONE;
					}
					else if(!strcmp(szVar, "HIDDEN"))
					{
						lpmapheader->bHidden = iValue;
						lpmapheader->dwMask |= MHF_HIDDEN;
					}
					else if(!strcmp(szVar, "NOSSMUSIC"))
					{
						lpmapheader->bNoSSMusic = iValue;
						lpmapheader->dwMask |= MHF_NOSSMUSIC;
					}
					else if(!strcmp(szVar, "SPEEDMUSIC"))
					{
						lpmapheader->bSpeedMusic = iValue;
						lpmapheader->dwMask |= MHF_SPEEDMUSIC;
					}
					else if(!strcmp(szVar, "NORELOAD"))
					{
						lpmapheader->bNoReload = iValue;
						lpmapheader->dwMask |= MHF_NORELOAD;
					}
					else if(!strcmp(szVar, "TIMEATTACK"))
					{
						lpmapheader->bTimeAttack = iValue;
						lpmapheader->dwMask |= MHF_TIMEATTACK;
					}
					else if(!strcmp(szVar, "LEVELSELECT"))
					{
						lpmapheader->bLevelSelect = iValue;
						lpmapheader->dwMask |= MHF_LEVELSELECT;
					}
					else if(!strcmp(szVar, "RUNSOC"))
					{
						lpmapheader->szRunSOC = ProcHeapAlloc(cchValue * sizeof(char));
						CopyMemory(lpmapheader->szRunSOC, szValue, cchValue);
						lpmapheader->dwMask |= MHF_RUNSOC;
					}
				}	/* while(cchOffset < cchSOC) header modal loop */

				/* One we've parsed the header, we don't care about the rest of
				 * the lump.
				 */
				if(lpcchOffsetEnd) *lpcchOffsetEnd = cchLineStart;
				break;

			}	/* If-the-level-matches block */

		}	/* Is-this-a-level-header block */

	}	/* while(cchOffset < cchSOC) */

	ProcHeapFree(szSOC);

	return bFoundHeader;
}


/* TerminateSOCLine
 *   Terimnates a line from a SOC lump beginning from a specified offset.
 *
 * Parameters:
 *   LPSTR		szSOC			Beginning of SOC lump.
 *   DWORD		cchSOC			Length of SOC lump.
 *   DWORD*		lpcchOffset		Offset at which to begin reading. Will be set to
 *								position just past the end of the line.
 *
 * Return value: None.
 *
 * Remarks:
 *   The SOC lump is modified.
 */
static void TerminateSOCLine(LPSTR szSOC, DWORD cchSOC, DWORD *lpcchOffset)
{
	/* Find the end of the line. */
	while(szSOC[*lpcchOffset] != '\r' && szSOC[*lpcchOffset] != '\n' && szSOC[*lpcchOffset] != '\0')
		(*lpcchOffset)++;

	/* Zero out and skip CR before LF. */
	if(*lpcchOffset + 1 < cchSOC && szSOC[*lpcchOffset] == '\r' && szSOC[*lpcchOffset + 1] == '\n')
		szSOC[(*lpcchOffset)++] = '\0';

	/* Terminate the line. */
	szSOC[(*lpcchOffset)++] = '\0';
}


/* PruneWSAndComments
 *   Removes whitespace from the beginning and comments from the end of a SOC
 *   line.
 *
 * Parameters:
 *   LPSTR      szSOCLine               SOC line.
 *
 * Return value: LPSTR
 *   Pointer to beginning of the line, sans whitespace.
 */
static __inline LPSTR PruneWSAndComments(LPSTR szSOCLine)
{
	/* Ignore any end-line comments. */
	LPSTR szInLine = strchr(szSOCLine, '#');
	if(szInLine) *szInLine = '\0';

	/* Skip any whitespace. */
	szInLine = szSOCLine;
	while(*szInLine == ' ' || *szInLine == '\t') szInLine++;

	return szInLine;
}


/* GetMapHeader
 *   Loads a map header from a wad.
 *
 * Parameters:
 *   WAD*			lpwad				Wad structure.
 *   int			iLevelNum			Level number whose header is to be
 *										loaded.
 *   MAPHEADER*		lpmapheader			Structure into which header is to be
 *										loaded.
 *   DWORD*			lpcchOffsetStart	Offset of start of header in sSOC is
 *										returned here. May be NULL.
 *   DWORD*			lpcchOffsetEnd		Offset of first character following
 *										header in sSOC is returned here. May be
 *										NULL.
 *
 * Return value: ENUM_HEADER_TYPE
 *   HT_MAINCFG, HT_MAPxxD, HT_NONE as the header came from MAINCFG, a MAPxxD
 *   lump, or was not found, respectively. Call FreeMapHeaderStrings on
 *   lpmapheader to free memory allocated by this function. The offsets are not
 *   touched if a header is not found.
 */
ENUM_HEADER_TYPE GetMapHeader(WAD *lpwad, int iLevelNum, MAPHEADER *lpmapheader, DWORD *lpcchOffsetStart, DWORD *lpcchOffsetEnd)
{
	char szMAPxxD[CCH_LUMPNAME + 1];
	int i;

	const struct {char *szLumpname; ENUM_HEADER_TYPE ht;} headerlumpdesc[] = {{szMAPxxD, HT_MAPXXD}, {"MAINCFG", HT_MAINCFG}};

	MakeHeaderLumpname(szMAPxxD, iLevelNum);

	/* Look for MAPxxD and then MAINCFG if we don't find one. */
	for(i = 0; i < (int)NUM_ELEMENTS(headerlumpdesc); i++)
	{
		long iIndex = GetLumpIndex(lpwad, 0, -1, headerlumpdesc[i].szLumpname);
		DWORD cb;

		/* Found it? */
		if(iIndex >= 0 && (cb = GetLumpLength(lpwad, iIndex)) > 0)
		{
			PVOID sHeaderLump = ProcHeapAlloc(cb);
			BOOL bFound;

			GetLump(lpwad, iIndex, sHeaderLump, cb);
			bFound = ExtractMapHeader(lpmapheader, sHeaderLump, cb, iLevelNum, lpcchOffsetStart, lpcchOffsetEnd);
			ProcHeapFree(sHeaderLump);

			if(bFound) return headerlumpdesc[i].ht;
		}
	}

	/* Didn't find either if we get this far. */
	return HT_NONE;
}


/* FreeMapHeaderStrings
 *   Frees any dynamically-allocated strings in a map header, and marks the
 *   corresponding fields as unset.
 *
 * Parameters:
 *   MAPHEADER*		lpmapheader		Map header.
 *
 * Return value: None.
 */
void FreeMapHeaderStrings(MAPHEADER *lpmapheader)
{
	if((lpmapheader->dwMask & MHF_LEVELNAME) && lpmapheader->szLevelName) ProcHeapFree(lpmapheader->szLevelName);
	if((lpmapheader->dwMask & MHF_SCRIPTNAME) && lpmapheader->szScriptName) ProcHeapFree(lpmapheader->szScriptName);
	if((lpmapheader->dwMask & MHF_MAPCREDITS) && lpmapheader->szMapCredits) ProcHeapFree(lpmapheader->szMapCredits);

	lpmapheader->dwMask &= ~(MHF_LEVELNAME | MHF_SCRIPTNAME | MHF_MAPCREDITS);
}


/* SetMissingRequiredMapHeaderFields
 *   Fills in any fields in a map header which are required to be set for the
 *   editor but which are not set, using some sensible default values.
 *
 * Parameters:
 *   MAPHEADER*		lpmapheader		Map header.
 *   int			iLevelNum		Number of level to which the header
 *									corresponds.
 *
 * Return value: None.
 *
 * Remarks:
 *   We don't allocate any heap memory, so a call to this function does not
 *   introduce the need to call FreeMapHeaderStrings.
 */
void SetMissingRequiredMapHeaderFields(MAPHEADER *lpmapheader, int iLevelNum)
{
	if(!(lpmapheader->dwMask & MHF_HIDDEN)) lpmapheader->bHidden = FALSE;
	if(!(lpmapheader->dwMask & MHF_LEVELSELECT)) lpmapheader->bLevelSelect = TRUE;
	if(!(lpmapheader->dwMask & MHF_NORELOAD)) lpmapheader->bNoReload = FALSE;
	if(!(lpmapheader->dwMask & MHF_NOSSMUSIC)) lpmapheader->bNoSSMusic = FALSE;
	if(!(lpmapheader->dwMask & MHF_NOZONE)) lpmapheader->bNoZone = FALSE;
	if(!(lpmapheader->dwMask & MHF_TIMEOFDAY)) lpmapheader->bSpeedMusic = FALSE;
	if(!(lpmapheader->dwMask & MHF_TIMEATTACK)) lpmapheader->bTimeAttack = FALSE;
	if(!(lpmapheader->dwMask & MHF_TIMEOFDAY)) lpmapheader->bTimeOfDay = FALSE;
	if(!(lpmapheader->dwMask & MHF_ACT)) lpmapheader->iAct = 1;
	if(!(lpmapheader->dwMask & MHF_MUSICSLOT)) lpmapheader->iMusicSlot = DEFAULT_MUSICSLOT;
	if(!(lpmapheader->dwMask & MHF_NEXTLEVEL)) lpmapheader->iNextLevel = iLevelNum + 1;
	if(!(lpmapheader->dwMask & MHF_SKYNUM)) lpmapheader->iSkyNum = DEFAULT_SKYNUM;
	if(!(lpmapheader->dwMask & MHF_TYPEOFLEVEL)) lpmapheader->dwTypeOfLevel = DEFAULT_TYPEOFLEVEL;
	if(!(lpmapheader->dwMask & MHF_WEATHER)) lpmapheader->iWeather = DEFAULT_WEATHER;

	/* Set all the flags for fields we might have set. */
	lpmapheader->dwMask |= MHF_HIDDEN | MHF_LEVELSELECT | MHF_NORELOAD |
		MHF_NOSSMUSIC | MHF_NOZONE | MHF_SPEEDMUSIC | MHF_TIMEATTACK |
		MHF_TIMEOFDAY | MHF_ACT | MHF_MUSICSLOT | MHF_NEXTLEVEL | MHF_SKYNUM |
		MHF_TYPEOFLEVEL | MHF_WEATHER;
}


/* SetMapHeader
 *   Sets a map header in a wad.
 *
 * Parameters:
 *   WAD*			lpwad				Wad structure.
 *   int			iLevelNum			Level number.
 *   MAPHEADER*		lpmapheader			Map header.
 *
 * Return value: None.
 */
void SetMapHeader(WAD *lpwad, int iLevelNum, MAPHEADER *lpmapheader)
{
	/* Get the extent of the map header. */
	DWORD cchStart, cchEnd, cchOldSOC, cchNewSOCSection;
	ENUM_HEADER_TYPE ht = GetMapHeader(lpwad, iLevelNum, NULL, &cchStart, &cchEnd);
	long iIndex;

	BYTE *sOldSOC;
	BYTE *sNewSOCSection;
	BYTE *sNewSOC;
	char szLumpname[CCH_LUMPNAME + 1];

	switch(ht)
	{
	case HT_MAINCFG:
		strcpy(szLumpname, "MAINCFG");
		break;

	case HT_MAPXXD:
	case HT_NONE:
		MakeHeaderLumpname(szLumpname, iLevelNum);
		break;
	}

	/* Read the existing SOC lump if one exists. */
	if(ht != HT_NONE)
	{
		iIndex = GetLumpIndex(lpwad, -1, -1, szLumpname);
		sOldSOC = ProcHeapAlloc((cchOldSOC = (DWORD)GetLumpLength(lpwad, iIndex)));
		GetLump(lpwad, iIndex, sOldSOC, cchOldSOC);
	}
	else
	{
		/* No existing SOC, and create new lump at end. */
		cchOldSOC = cchStart = cchEnd = 0;
		sOldSOC = NULL;
		iIndex = CreateLump(lpwad, szLumpname, -1);
	}

	/* Generate the header SOC section. */
	sNewSOCSection = GenerateHeaderSOCSection(lpmapheader, iLevelNum, &cchNewSOCSection);

	/* Allocate a buffer for the replacement SOC. */
	sNewSOC = ProcHeapAlloc(cchOldSOC + cchNewSOCSection - (cchEnd - cchStart));

	/* Merge the old SOC, if we have one, with the new section. */
	if(cchStart > 0) CopyMemory(sNewSOC, sOldSOC, cchStart);
	CopyMemory(sNewSOC + cchStart, sNewSOCSection, cchNewSOCSection);
	if(cchOldSOC - cchEnd > 0) CopyMemory(sNewSOC + cchStart + cchNewSOCSection, sOldSOC + cchEnd, cchOldSOC - cchEnd);

	/* Set the lump's data in the wad. */
	SetLump(lpwad, iIndex, sNewSOC, cchStart + cchNewSOCSection + cchOldSOC - cchEnd);

	ProcHeapFree(sNewSOC);
	FreeHeaderSOCSection(sNewSOCSection);
	if(sOldSOC) ProcHeapFree(sOldSOC);
}


/* MakeHeaderLumpname
 *   Generates the MAPxxD lumpname from a level number.
 *
 * Parameters:
 *   LPSTR	szLumpName		Buffer for lumpname of length at least
 *							(CCH_LUMPNAME + 1).
 *   int	iLevelNum		Level number.
 *
 * Return value: None.
 */
static __inline void MakeHeaderLumpname(LPSTR szLumpname, int iLevelNum)
{
	/* Generate MAPxxD. */
	MapLumpnameFromNumber(iLevelNum, szLumpname);
	szLumpname[5] = 'D';
	szLumpname[6] = '\0';
}


/* Little helper macros for GenerateHeaderSOCSection. */
#define SOC_WRITE_INT(dwFlag, szVar, iValue) \
	if(lpmapheader->dwMask & (dwFlag)) \
	{ \
		_snprintf(szSOCLine, NUM_ELEMENTS(szSOCLine), szVar " = %d\n", (iValue)); \
		szSOCLine[NUM_ELEMENTS(szSOCLine) - 1] = '\0'; \
		DynamicSOCWrite(&dsbHeader, szSOCLine); \
	}

#define SOC_WRITE_STR(dwFlag, szVar, szValue) \
	if(lpmapheader->dwMask & (dwFlag)) \
	{ \
		DynamicSOCWrite(&dsbHeader, szVar " = "); \
		DynamicSOCWrite(&dsbHeader, (szValue)); \
		DynamicSOCWrite(&dsbHeader, "\n"); \
	}
/* Macros end. */

/* GenerateHeaderSOCSection
 *   Generates a level header SOC from a structure.
 *
 * Parameters:
 *   MAPHEADER*		lpmapheader		Map header structure.
 *   int			iLevelNum		Level number.
 *   DWORD*			lpcch			No. characters written is returned here.
 *
 * Return value: BYTE*
 *   Header SOC, not NUL-terminated.
 *
 * Remarks:
 *   Call FreeHeaderSOCSection on the returned pointer to free it.
 */
static BYTE* GenerateHeaderSOCSection(MAPHEADER *lpmapheader, int iLevelNum, DWORD *lpcch)
{
	/* Variable names are hardcoded into the macros above. */
	DYNAMIC_SOC_BUFFER dsbHeader;

	/* Big enough for var = integer; for strings we write them directly from the
	 * header structure.
	 */
	char szSOCLine[CCH_SOC_LINE];

	dsbHeader.cchBuffer = INIT_HEADER_SOC_BUFSIZE;
	dsbHeader.cchWritten = 0;
	dsbHeader.lpbyBuffer = ProcHeapAlloc(INIT_HEADER_SOC_BUFSIZE);

	/* Write the first line first. */
	_snprintf(szSOCLine, NUM_ELEMENTS(szSOCLine), "LEVEL %d\n", iLevelNum);
	szSOCLine[NUM_ELEMENTS(szSOCLine) - 1] = '\0';
	DynamicSOCWrite(&dsbHeader, szSOCLine);

	/* Write the lines. */
	SOC_WRITE_INT(MHF_HIDDEN,			"HIDDEN",			lpmapheader->bHidden);
	SOC_WRITE_INT(MHF_LEVELSELECT,		"LEVELSELECT",		lpmapheader->bLevelSelect);
	SOC_WRITE_INT(MHF_NORELOAD,			"NORELOAD",			lpmapheader->bNoReload);
	SOC_WRITE_INT(MHF_NOSSMUSIC,		"NOSSMUSIC",		lpmapheader->bNoSSMusic);
	SOC_WRITE_INT(MHF_NOZONE,			"NOZONE",			lpmapheader->bNoZone);
	SOC_WRITE_INT(MHF_SCRIPTISLUMP,		"SCRIPTISLUMP",		lpmapheader->bScriptIsLump);
	SOC_WRITE_INT(MHF_SPEEDMUSIC,		"SPEEDMUSIC",		lpmapheader->bSpeedMusic);
	SOC_WRITE_INT(MHF_TIMEATTACK,		"TIMEATTACK",		lpmapheader->bTimeAttack);
	SOC_WRITE_INT(MHF_TIMEOFDAY,		"TIMEOFDAY",		lpmapheader->bTimeOfDay);
	SOC_WRITE_INT(MHF_TYPEOFLEVEL,		"TYPEOFLEVEL",		lpmapheader->dwTypeOfLevel);
	SOC_WRITE_INT(MHF_ACT,				"ACT",				lpmapheader->iAct);
	SOC_WRITE_INT(MHF_COUNTDOWN,		"COUNTDOWN",		lpmapheader->iCountdown);
	SOC_WRITE_INT(MHF_CUTSCENENUM,		"CUTSCENENUM",		lpmapheader->iCutsceneNum);
	SOC_WRITE_INT(MHF_FORCECHARACTER,	"FORCECHARACTER",	lpmapheader->iForceCharacter);
	SOC_WRITE_INT(MHF_MUSICSLOT,		"MUSICSLOT",		lpmapheader->iMusicSlot);
	SOC_WRITE_INT(MHF_NEXTLEVEL,		"NEXTLEVEL",		lpmapheader->iNextLevel);
	SOC_WRITE_INT(MHF_PRECUTSCENENUM,	"PRECUTSCENENUM",	lpmapheader->iPreCutsceneNum);
	SOC_WRITE_INT(MHF_SKYNUM,			"SKYNUM",			lpmapheader->iSkyNum);
	SOC_WRITE_INT(MHF_WEATHER,			"WEATHER",			lpmapheader->iWeather);
	SOC_WRITE_STR(MHF_INTERSCREEN,		"INTERSCREEN",		lpmapheader->szInterScreen);
	SOC_WRITE_STR(MHF_LEVELNAME,		"LEVELNAME",		lpmapheader->szLevelName);
	SOC_WRITE_STR(MHF_MAPCREDITS,		"MAPCREDITS",		lpmapheader->szMapCredits);
	SOC_WRITE_STR(MHF_RUNSOC,			"RUNSOC",			lpmapheader->szRunSOC);
	SOC_WRITE_STR(MHF_SCRIPTNAME,		"SCRIPTNAME",		lpmapheader->szScriptName);
	SOC_WRITE_STR(MHF_SUBTITLE,			"SUBTITLE",			lpmapheader->szSubtitle);

	*lpcch = dsbHeader.cchWritten;
	return dsbHeader.lpbyBuffer;
}


/* DynamicSOCWrite
 *   Appends the contents of a buffer to a dynamic SOC.
 *
 * Parameters:
 *   DYNAMIC_SOC_BUFFER*	lpdsb		Dynamic SOC.
 *   LPSTR					sz			NUL-terminated string to append; the NUL
 *										is not copied.
 *
 * Return value: None.
 */
static __inline void DynamicSOCWrite(DYNAMIC_SOC_BUFFER *lpdsb, LPSTR sz)
{
	DWORD cch = strlen(sz);

	lpdsb->cchWritten += cch;

	/* Make sure the buffer's big enough. */
	if(lpdsb->cchBuffer < lpdsb->cchWritten)
	{
		do
		{
			lpdsb->cchBuffer <<= 1;
		} while(lpdsb->cchBuffer < lpdsb->cchWritten);

		lpdsb->lpbyBuffer = ProcHeapReAlloc(lpdsb->lpbyBuffer, lpdsb->cchBuffer);
	}

	CopyMemory(lpdsb->lpbyBuffer + lpdsb->cchWritten - cch, sz, cch);
}


/* ProcessTextureSOC
 *   Parses a texture SOC and adds the appropriate entries to the config.
 *
 * Parameters:
 *   LPCSTR				sSOC				SOC lump.
 *   DWORD				cchSOC				Length of SOC lump.
 *   CONFIG*			lpcfgTexEntries		Config to update.
 *   TEXTURENAMELIST*	lptnl				Name list of textures to update
 *											as necessary.
 *
 * Return value: None.
 */
void ProcessTextureSOC(LPCSTR sSOC, DWORD cchSOC, CONFIG *lpcfgTexEntries, TEXTURENAMELIST *lptnl)
{
	LPSTR szSOC = ProcHeapAlloc((cchSOC + 1) * sizeof(char));
	DWORD cchOffset = 0;
	TEXTURE_ENTRY_NEW *lptexentrynew = NULL;

	/* Make a NUL-terminated version of the SOC. */
	CopyMemory(szSOC, sSOC, cchSOC);
	szSOC[cchSOC] = '\0';

	while(cchOffset < cchSOC)
	{
		LPSTR szInLine = &szSOC[cchOffset];
		LPSTR szFirstWord, szSecondWord, szParameter;

		/* Terminate the line and advance beyond it. */
		TerminateSOCLine(szSOC, cchSOC, &cchOffset);

		/* Tidy the line. */
		szInLine = PruneWSAndComments(szInLine);

		szFirstWord = strtok(szInLine, " \t");
		if(!szFirstWord) continue;

		szSecondWord = strtok(NULL, " \t");
		if(!szSecondWord) continue;

		if(!_stricmp(szFirstWord, "TEXTURE"))
		{
			/* Start a new texture. */

			const short ALLOC_PATCHES_INIT = 2;

			TEXTURE_ENTRY_NEW *lptexentnewPrev;

#ifdef UNICODE
			WCHAR szTexName[TEXNAME_BUFFER_LENGTH];
			if(strlen(szSecondWord) >= TEXNAME_BUFFER_LENGTH)
				szSecondWord[TEXNAME_BUFFER_LENGTH - 1] = '\0';
			MultiByteToWideChar(CP_ACP, MB_PRECOMPOSED, szSecondWord, -1, szTexName, NUM_ELEMENTS(szTexName));
#else
			LPCSTR szTexName = szSecondWord;

			if(strlen(szTexName) >= TEXNAME_BUFFER_LENGTH)
				szTexName[TEXNAME_BUFFER_LENGTH - 1] = '\0';
#endif

			lptexentrynew = ProcHeapAlloc(sizeof(TEXTURE_ENTRY_NEW));
			FillTexNameBufferA(lptexentrynew->sTexName, szSecondWord);
			lptexentrynew->nNumPatches = 0;
			lptexentrynew->nNumAllocedPatches = ALLOC_PATCHES_INIT;
			lptexentrynew->lptpdn = ProcHeapAlloc(lptexentrynew->nNumAllocedPatches * sizeof(TEXTURE_PATCH_DESCRIPTOR_NEW));

			if((lptexentnewPrev = ConfigGetPointer(lpcfgTexEntries, szTexName)))
				TexEntryNewFree(lptexentnewPrev);
			ConfigSetPointer(lpcfgTexEntries, szTexName, lptexentrynew);

			AddTextureNameToList(lptexentrynew->sTexName, lptnl);

			continue;
		}
		
		if(!lptexentrynew)
			continue;
		
		if(!_stricmp(szFirstWord, "PATCH"))
		{
			/* Add a new patch to the current texture. */

			lptexentrynew->nNumPatches++;

			if(lptexentrynew->nNumPatches > lptexentrynew->nNumAllocedPatches)
			{
				lptexentrynew->nNumAllocedPatches <<= 1;
				lptexentrynew->lptpdn = ProcHeapReAlloc(lptexentrynew->lptpdn, lptexentrynew->nNumAllocedPatches * sizeof(TEXTURE_PATCH_DESCRIPTOR_NEW));
			}

			FillTexNameBufferA(lptexentrynew->lptpdn[lptexentrynew->nNumPatches - 1].sPatchName, szSecondWord);

			continue;
		}

		/* Beyond this point, we require an "=". */
		if(_stricmp(szSecondWord, "="))
			continue;

		szParameter = strtok(NULL, " \t");
		if(!szParameter) continue;

		if(!_stricmp(szFirstWord, "WIDTH"))
			lptexentrynew->cx = (short)atoi(szParameter);
		if(!_stricmp(szFirstWord, "HEIGHT"))
			lptexentrynew->cy = (short)atoi(szParameter);
		if(!_stricmp(szFirstWord, "X"))
			lptexentrynew->lptpdn[lptexentrynew->nNumPatches - 1].xOffset = (short)atoi(szParameter);
		if(!_stricmp(szFirstWord, "Y"))
			lptexentrynew->lptpdn[lptexentrynew->nNumPatches - 1].yOffset = (short)atoi(szParameter);
	}

	ProcHeapFree(szSOC);
}
