/* SRB2 Workbench -- A map editor for Sonic Robo Blast 2.
 * Copyright (C) 2009 Gregor Dick
 * http://workbench.srb2.org/
 *
 * Incorporates portions of Doom Builder, (C) 2003 Pascal vd Heiden.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 * USA.
 *
 * keyboard.c: Keyboard routines, mostly for handling accelerators.
 *
 * AMDG.
 */

#include <windows.h>
#include <stdlib.h>
#include <search.h>
#include <string.h>
#include <tchar.h>
#include <commctrl.h>

#include "general.h"
#include "keyboard.h"
#include "options.h"
#include "../res/resource.h"

#include "win/mdiframe.h"
#include "win/mapwin.h"



/* MinGW is missing these. */
#ifndef VK_OEM_COMMA
#define VK_OEM_COMMA 0xBC
#endif

#ifndef VK_OEM_PERIOD
#define VK_OEM_PERIOD 0xBC
#endif

#ifndef MAPVK_VK_TO_VSC
#define MAPVK_VK_TO_VSC 0
#endif



/* Maps keycodes to menu IDs. */
typedef struct _KEYMENUPAIR
{
	int		iKeyCode;
	USHORT	unMenuID;
	WORD	wModeFlags;
} KEYMENUPAIR;



/* Mapping tables of shortcut keys to menu commands. */

KEYMENUPAIR g_kmpShortcutsSortKey[SCK_MAX], g_kmpShortcutsSortMenu[SCK_MAX];
int g_iNumMenuShortcuts;
static short g_nVKToStringIndex[256] = {0};

enum ENUM_SHORTCUT_KEY_MODE_FLAGS
{
	SMF_2D  = 1,
	SMF_ISO = 2,
	SMF_3D  = 4
};

typedef struct _SHORTCUT_INFO
{
	USHORT	unMenuID;
	WORD	wModeFlags;
} SHORTCUT_INFO;

/* NOCOMMAND indicates that no menu command is associated with the key. */
static const SHORTCUT_INFO g_shortcutinfo[SCK_MAX] =
{
	{NOCOMMAND, SMF_2D | SMF_ISO},							/* SCK_EDITQUICKMOVE */
	{NOCOMMAND, SMF_2D | SMF_ISO},							/* SCK_ZOOMIN */
	{NOCOMMAND, SMF_2D | SMF_ISO},							/* SCK_ZOOMOUT */
	{IDM_VIEW_CENTRE, SMF_2D | SMF_ISO},					/* SCK_CENTREVIEW */
	{IDM_VIEW_MODE_MOVE, SMF_2D},							/* SCK_EDIT_MOVE */
	{IDM_VIEW_MODE_ANY, SMF_2D},			/* SCK_EDIT_ANY */
	{IDM_VIEW_MODE_LINES, SMF_2D},		/* SCK_EDITLINES */
	{IDM_VIEW_MODE_SECTORS, SMF_2D},		/* SCK_EDITSECTORS */
	{IDM_VIEW_MODE_VERTICES, SMF_2D},	/* SCK_EDITVERTICES */
	{IDM_VIEW_MODE_THINGS, SMF_2D},		/* SCK_EDITTHINGS */
	{IDM_VIEW_3D, SMF_2D | SMF_ISO | SMF_3D},				/* SCK_VIEW3D */
	{IDM_VIEW_2DVIEW, SMF_2D | SMF_ISO | SMF_3D},			/* SCK_VIEW2D */
	{IDM_VIEW_ISOMETRICVIEW, SMF_2D | SMF_ISO | SMF_3D},	/* SCK_VIEWISO */
	{IDM_EDIT_SNAPTOGRID, SMF_2D},		/* SCK_EDITSNAPTOGRID */
	{IDM_LINES_FLIPLINEDEFS, SMF_2D},	/* SCK_FLIPLINEDEFS */
	{IDM_LINES_FLIPSIDEDEFS, SMF_2D},	/* SCK_FLIPSIDEDEFS */
	{IDM_LINES_SPLITLINEDEFS, SMF_2D},	/* SCK_SPLITLINEDEFS */
	{IDM_SECTORS_JOIN, SMF_2D},			/* SCK_JOINSECTORS */
	{IDM_SECTORS_MERGE, SMF_2D},			/* SCK_MERGESECTORS */
	{IDM_EDIT_UNDO, SMF_2D},				/* SCK_UNDO */
	{IDM_EDIT_REDO, SMF_2D},				/* SCK_REDO */
	{IDM_EDIT_TRANSFORMSELECTION_FLIPHORIZONTALLY, SMF_2D},	/* SCK_FLIPHORIZ */
	{IDM_EDIT_TRANSFORMSELECTION_FLIPVERTICALLY, SMF_2D},		/* SCK_FLIPVERT */
	{IDM_EDIT_COPY, SMF_2D},				/* SCK_COPY */
	{IDM_EDIT_PASTE, SMF_2D | SMF_ISO | SMF_3D},			/* SCK_PASTE */
	{IDM_FILE_SAVEAS, SMF_2D | SMF_ISO | SMF_3D},			/* SCK_SAVEAS */
	{IDM_SECTORS_FLOORS_INCREASE, SMF_2D | SMF_ISO | SMF_3D},	/* SCK_INCFLOOR */
	{IDM_SECTORS_FLOORS_DECREASE, SMF_2D | SMF_ISO | SMF_3D},	/* SCK_DECFLOOR */
	{IDM_SECTORS_CEILINGS_INCREASE, SMF_2D | SMF_ISO | SMF_3D},	/* SCK_INCCEIL */
	{IDM_SECTORS_CEILINGS_DECREASE, SMF_2D | SMF_ISO | SMF_3D},	/* SCK_DECCEIL */
	{IDM_SECTORS_LIGHT_INCREASE, SMF_2D | SMF_ISO | SMF_3D},	/* SCK_INCLIGHT */
	{IDM_SECTORS_LIGHT_DECREASE, SMF_2D | SMF_ISO | SMF_3D},	/* SCK_DECLIGHT */
	{IDM_THINGS_HEIGHT_INCREASE, SMF_2D | SMF_ISO | SMF_3D},	/* SCK_INCTHINGZ */
	{IDM_THINGS_HEIGHT_DECREASE, SMF_2D | SMF_ISO | SMF_3D},	/* SCK_DECTHINGZ */
	{IDM_EDIT_TRANSFORM_SNAP, SMF_2D},	/* SCK_SNAPSELECTION */
	{IDM_SECTORS_FLOORS_GRADIENT, SMF_2D | SMF_ISO | SMF_3D},	/* SCK_GRADIENTFLOORS */
	{IDM_SECTORS_CEILINGS_GRADIENT, SMF_2D | SMF_ISO | SMF_3D},	/* SCK_GRADIENTCEILINGS */
	{IDM_SECTORS_LIGHT_GRADIENT, SMF_2D | SMF_ISO | SMF_3D},	/* SCK_GRADIENTBRIGHTNESS */
	{NOCOMMAND, SMF_2D},					/* SCK_INSERT */
	{IDM_INSERTRECT, SMF_2D},				/* SCK_DCKINSERT */
	{IDM_EDIT_CUT, SMF_2D},				/* SCK_EDITCUT */
	{IDM_EDIT_DELETE, SMF_2D},			/* SCK_EDITDELETE */
	{IDM_CANCEL, SMF_2D | SMF_ISO | SMF_3D},					/* SCK_CANCEL */
	{IDM_THINGS_HEIGHT_GRADIENT, SMF_2D | SMF_ISO | SMF_3D},	/* SCK_GRADIENTTHINGZ */
	{IDM_EDIT_TRANSFORMSELECTION_ROTATE, SMF_2D},	/* SCK_ROTATE */
	{IDM_EDIT_TRANSFORMSELECTION_RESIZE, SMF_2D},	/* SCK_RESIZE */
	{IDM_THINGS_ROTATE_ACW, SMF_2D | SMF_ISO | SMF_3D},		/* SCK_THINGROTACW */
	{IDM_THINGS_ROTATE_CW, SMF_2D | SMF_ISO | SMF_3D},		/* SCK_THINGROTCW */
	{IDM_EDIT_SELECT_ALL, SMF_2D},		/* SCK_SELECTALL */
	{IDM_EDIT_SELECT_NONE, SMF_2D},		/* SCK_SELECTNONE */
	{IDM_EDIT_SELECT_INVERTSELECTION, SMF_2D},	/* SCK_INVERTSELECTION */
	{IDM_EDIT_COPYSELECTIONPROPERTIES, SMF_2D},	/* SCK_COPYPROPS */
	{IDM_EDIT_PASTESELECTIONPROPERTIES, SMF_2D},	/* SCK_PASTEPROPS */
	{IDM_VIEW_GRID_INCREASE, SMF_2D},		/* SCK_GRIDINC */
	{IDM_VIEW_GRID_DECREASE, SMF_2D},		/* SCK_GRIDDEC */
	{IDM_EDIT_FIND, SMF_2D},				/* SCK_FIND */
	{IDM_EDIT_REPLACE, SMF_2D},			/* SCK_REPLACE */
	{IDM_FILE_NEW, SMF_2D | SMF_ISO | SMF_3D},				/* SCK_NEW */
	{IDM_FILE_OPEN, SMF_2D | SMF_ISO | SMF_3D},				/* SCK_OPEN */
	{IDM_FILE_SAVE, SMF_2D | SMF_ISO | SMF_3D},				/* SCK_SAVE */
	{IDM_EDIT_MAPOPTIONS, SMF_2D},		/* SCK_MAPOPTIONS */
	{IDM_THINGS_ROTATE_MOUSE, SMF_2D},	/* SCK_MOUSEROTATE */
	{IDM_VERTICES_STITCH, SMF_2D},		/* SCK_STITCHVERTICES */
	{NOCOMMAND, SMF_2D | SMF_ISO | SMF_3D},					/* SCK_SCROLL_UP */
	{NOCOMMAND, SMF_2D | SMF_ISO | SMF_3D},					/* SCK_SCROLL_DOWN */
	{NOCOMMAND, SMF_2D | SMF_ISO | SMF_3D},					/* SCK_SCROLL_LEFT */
	{NOCOMMAND, SMF_2D | SMF_ISO | SMF_3D},					/* SCK_SCROLL_RIGHT */
	{IDM_FILE_TEST, SMF_2D | SMF_ISO | SMF_3D},				/* SCK_TEST */
	{ID_QUICKTEST, SMF_2D | SMF_ISO | SMF_3D},				/* SCK_QUICKTEST */
	{IDM_SECTORS_IDENT_FIND, SMF_2D},		/* SCK_IDENTSECTORS */
	{IDM_VIEW_CENTRESEL, SMF_2D},			/* SCK_CENTREVIEWSEL */
	{IDM_LINES_MAKESINGLE, SMF_2D},		/* SCK_MAKESINGLE */
	{IDM_LINES_MAKEDOUBLE, SMF_2D},		/* SCK_MAKEDOUBLE */
	{IDM_SECTORS_REMOVEINTERIOR, SMF_2D},	/* SCK_REMOVEINTERIOR */
	{IDM_SECTORS_RESOLVE, SMF_2D},		/* SCK_RESOLVESEC */
	{NOCOMMAND, SMF_2D},					/* SCK_BACKTRACKDRAW */
	{IDM_EDIT_SELECT_TAGGINGDUAL, SMF_2D},	/* SCK_SELECTTAGDUAL */
	{IDM_EDITFOF, SMF_2D},				/* SCK_EDITFOF */
	{NOCOMMAND, SMF_2D},					/* SCK_QUICKDRAG */
	{NOCOMMAND, SMF_2D},					/* SCK_QUICKFOF */
	{IDM_TOOLS_OPTIONS, SMF_2D | SMF_ISO | SMF_3D},			/* SCK_OPTIONS */
	{IDM_TOOLS_ERRORCHECKER, SMF_2D},		/* SCK_ERRORCHECKER */
	{IDM_PROPERTIES, SMF_2D},				/* SCK_PROPERTIES */
	{IDM_EDIT_SNAPTOVERTICES, SMF_2D},	/* SCK_TOGGLEVXSNAP */
	{IDM_EDIT_SNAPTOLINES, SMF_2D},		/* SCK_TOGGLELINESNAP */
	{IDM_EDIT_AUTOSTITCH, SMF_2D},		/* SCK_TOGGLEAUTOSTITCH */
	{IDM_EDIT_MAPHEADER, SMF_2D | SMF_ISO | SMF_3D},			/* SCK_EDITHEADER */
	{IDM_LINES_FIXMISSINGTEXTURES, SMF_2D | SMF_ISO | SMF_3D},	/* SCK_FIXMISSTEX */
	{IDM_LINES_AUTOALIGNTEXTURES, SMF_2D | SMF_ISO | SMF_3D},	/* SCK_AUTOALIGN */
	{IDM_VERTICES_DELETEUNUSED, SMF_2D},	/* SCK_DELUNUSEDVX */
	{IDM_EDIT_LOCK, SMF_2D},				/* SCK_LOCKDRAGGING */
	{NOCOMMAND, SMF_ISO | SMF_3D},					/* SCK_3DROTATEACW */
	{NOCOMMAND, SMF_ISO | SMF_3D},					/* SCK_3DROTATECW */
	{NOCOMMAND, SMF_3D},					/* SCK_3DFORWARDS */
	{NOCOMMAND, SMF_3D},					/* SCK_3DBACKWARDS */
	{NOCOMMAND, SMF_3D},					/* SCK_3DSTRAFELEFT */
	{NOCOMMAND, SMF_3D},					/* SCK_3DSTRAFERIGHT */
	{NOCOMMAND, SMF_ISO | SMF_3D},			/* SCK_INCXOFFSET */
	{NOCOMMAND, SMF_ISO | SMF_3D},			/* SCK_DECXOFFSET */
	{NOCOMMAND, SMF_ISO | SMF_3D},			/* SCK_INCYOFFSET */
	{NOCOMMAND, SMF_ISO | SMF_3D},			/* SCK_DECYOFFSET */
	{NOCOMMAND, SMF_3D},					/* SCK_3DSTRAFEUP */
	{NOCOMMAND, SMF_3D},					/* SCK_3DSTRAFEDOWN */
	{IDM_LINES_CURVELINEDEFS, SMF_2D},
	/* _SCK_ */
};


static int CompareKMPKeyCodeSort(const void *lpvKMPLeft, const void *lpvKMPRight);
static int CompareKMPKeyCodeSearch(const void *lpvKMPLeft, const void *lpvKMPRight);
static int CompareKMPMenuID(const void *lpvKMPLeft, const void *lpvKMPRight);
static USHORT GetMenuIDFromKeycodeAndMode(int iKeyCode, WORD wMode);
static __inline BOOL IsExtendedShortcut(int iKeyCode);
static int GetVirtualKeyText(short nVKCode, LPTSTR szBuffer, int cchBuffer);


/* MakeShiftedKeyCode
 *   Maps a VK code to a character code with shift states.
 *
 * Parameters:
 *   int		iVirtKey	Virtual key code.
 *
 * Return value: int
 *   DB-esque key-code containing character code and shift states.
 */
int MakeShiftedKeyCode(int iVirtKey)
{
	switch(iVirtKey)
	{
	case VK_CONTROL:
		return SC_CTRL;
	case VK_MENU:
		return SC_ALT;
	case VK_SHIFT:
		return SC_SHIFT;
	default:
		return iVirtKey
			+ ((GetKeyState(VK_SHIFT) & 0x8000) ? SC_SHIFT : 0)
			+ ((GetKeyState(VK_CONTROL) & 0x8000) ? SC_CTRL : 0)
			+ ((GetKeyState(VK_MENU) & 0x8000) ? SC_ALT : 0);
	}
}


/* UpdateAcceleratorFromOptions
 *   Copies shortcut keys into our ad hoc accelerator table.
 *
 * Parameters:
 *   None.
 *
 * Return value: None.
 *
 * Notes:
 *   Call this after first loading the config file, and then every time the user
 *   changes the shortcut keys.
 */
void UpdateAcceleratorFromOptions(void)
{
	int i;

	/* Loop through all shortcut keys, copying into our mapping structure. */
	g_iNumMenuShortcuts = 0;
	for(i = 0; i < SCK_MAX; i++)
	{
		if(g_shortcutinfo[i].unMenuID != NOCOMMAND && g_iShortcutCodes[i] != 0)
		{
			g_kmpShortcutsSortKey[g_iNumMenuShortcuts].unMenuID = g_shortcutinfo[i].unMenuID;
			g_kmpShortcutsSortKey[g_iNumMenuShortcuts].wModeFlags = g_shortcutinfo[i].wModeFlags;
			g_kmpShortcutsSortKey[g_iNumMenuShortcuts].iKeyCode = g_iShortcutCodes[i];
			g_iNumMenuShortcuts++;
		}
	}

	/* Duplicate the list. */
	CopyMemory(g_kmpShortcutsSortMenu, g_kmpShortcutsSortKey, sizeof(g_kmpShortcutsSortKey));

	/* Sort the lists for efficient lookups by keycode and menu ID. */
	qsort(g_kmpShortcutsSortKey, g_iNumMenuShortcuts, sizeof(KEYMENUPAIR), CompareKMPKeyCodeSort);
	qsort(g_kmpShortcutsSortMenu, g_iNumMenuShortcuts, sizeof(KEYMENUPAIR), CompareKMPMenuID);
}

/* CompareKMPKeyCodeSort - qsort comparator for KEYMENUPAIRs (by
 * keycode).
 */
static int CompareKMPKeyCodeSort(const void *lpvKMPLeft, const void *lpvKMPRight)
{
	KEYMENUPAIR *lpkmpLeft = (KEYMENUPAIR*)lpvKMPLeft;
	KEYMENUPAIR *lpkmpRight = (KEYMENUPAIR*)lpvKMPRight;

	/* Sort first on code, then on mode. */
	int iCodeDiff = lpkmpLeft->iKeyCode - lpkmpRight->iKeyCode;

	if(iCodeDiff || lpkmpLeft->wModeFlags == (WORD)-1 || lpkmpRight->wModeFlags == (WORD)-1)
		return iCodeDiff;

	return (int)lpkmpLeft->wModeFlags - (int)lpkmpRight->wModeFlags;
}

/* CompareKMPKeyCodeSearch - bsearch comparator for KEYMENUPAIRs (by
 * keycode).
 */
static int CompareKMPKeyCodeSearch(const void *lpvKMPLeft, const void *lpvKMPRight)
{
	KEYMENUPAIR *lpkmpLeft = (KEYMENUPAIR*)lpvKMPLeft;
	KEYMENUPAIR *lpkmpRight = (KEYMENUPAIR*)lpvKMPRight;

	/* Sort first on code, then on mode. */
	int iCodeDiff = lpkmpLeft->iKeyCode - lpkmpRight->iKeyCode;

	if(iCodeDiff || lpkmpLeft->wModeFlags == (WORD)-1 || lpkmpRight->wModeFlags == (WORD)-1)
		return iCodeDiff;

	/* Any overlap in flags results in a match when searching. */
	if(lpkmpLeft->wModeFlags & lpkmpRight->wModeFlags)
		return 0;

	return (int)lpkmpLeft->wModeFlags - (int)lpkmpRight->wModeFlags;
}

/* CompareKeyMenuPairs - qsort, bsearch comparator for KEYMENUPAIRs (by
 * keycode).
 */
static int CompareKMPMenuID(const void *lpvKMPLeft, const void *lpvKMPRight)
{
	return ((KEYMENUPAIR*)lpvKMPLeft)->unMenuID - ((KEYMENUPAIR*)lpvKMPRight)->unMenuID;
}


/* GetMenuIDFromKeycode
 *   Retrieves the menu ID associated with a shortcut keycode.
 *
 * Parameters:
 *  int		iKeyCode	Shifted key code.
 *
 * Return value: USHORT
 *   ID of menu item, or NOCOMMAND if not found.
 */
USHORT GetMenuIDFromKeycode(int iKeyCode)
{
	return GetMenuIDFromKeycodeAndMode(iKeyCode, (WORD)-1);
}

static USHORT GetMenuIDFromKeycodeAndMode(int iKeyCode, WORD wMode)
{
	const KEYMENUPAIR kmpMatch = {iKeyCode, 0, wMode};
	KEYMENUPAIR *lpkmpMatch = bsearch(&kmpMatch, g_kmpShortcutsSortKey, g_iNumMenuShortcuts, sizeof(KEYMENUPAIR), CompareKMPKeyCodeSearch);
	return lpkmpMatch ? lpkmpMatch->unMenuID : NOCOMMAND;
}


/* GetKeycodeFromMenuID
 *   Retrieves the menu ID associated with a shortcut keycode.
 *
 * Parameters:
 *  int		iKeyCode	Shifted key code.
 *
 * Return value: USHORT
 *   ID of menu item, or NOCOMMAND if not found.
 */
int GetKeycodeFromMenuID(USHORT unMenuID)
{
	const KEYMENUPAIR kmpMatch = {0, unMenuID, (WORD)-1 /* Keycode and mode are ignored. */};
	KEYMENUPAIR *lpkmpMatch = bsearch(&kmpMatch, g_kmpShortcutsSortMenu, g_iNumMenuShortcuts, sizeof(KEYMENUPAIR), CompareKMPMenuID);
	return lpkmpMatch ? lpkmpMatch->iKeyCode : 0;
}


/* SetDefaultShortcuts
 *   Sets missing shortcuts to their default values.
 *
 * Parameters:
 *   None.
 *
 * Return value: None.
 */
void SetDefaultShortcuts(void)
{
	CONFIG *lpcfgShortcuts = ConfigGetSubsection(g_lpcfgMain, OPT_SHORTCUTS);

	if(!ConfigNodeExists(lpcfgShortcuts, TEXT("editquickmove")))
		ConfigSetInteger(lpcfgShortcuts, TEXT("editquickmove"),		VK_SPACE);

	if(!ConfigNodeExists(lpcfgShortcuts, TEXT("editcenterview")))
		ConfigSetInteger(lpcfgShortcuts, TEXT("editcenterview"),	VK_SPACE | SC_CTRL);

	if(!ConfigNodeExists(lpcfgShortcuts, TEXT("zoomin")))
		ConfigSetInteger(lpcfgShortcuts, TEXT("zoomin"),			MOUSE_SCROLL_UP);

	if(!ConfigNodeExists(lpcfgShortcuts, TEXT("zoomout")))
		ConfigSetInteger(lpcfgShortcuts, TEXT("zoomout"),			MOUSE_SCROLL_DOWN);

	if(!ConfigNodeExists(lpcfgShortcuts, TEXT("editmove")))
		ConfigSetInteger(lpcfgShortcuts, TEXT("editmove"),			'M');

	if(!ConfigNodeExists(lpcfgShortcuts, TEXT("editany")))
		ConfigSetInteger(lpcfgShortcuts, TEXT("editany"),			'Z');

	if(!ConfigNodeExists(lpcfgShortcuts, TEXT("editlines")))
		ConfigSetInteger(lpcfgShortcuts, TEXT("editlines"),			'L');

	if(!ConfigNodeExists(lpcfgShortcuts, TEXT("editsectors")))
		ConfigSetInteger(lpcfgShortcuts, TEXT("editsectors"),		'S');

	if(!ConfigNodeExists(lpcfgShortcuts, TEXT("editvertices")))
		ConfigSetInteger(lpcfgShortcuts, TEXT("editvertices"),		'V');

	if(!ConfigNodeExists(lpcfgShortcuts, TEXT("editthings")))
		ConfigSetInteger(lpcfgShortcuts, TEXT("editthings"),		'T');

	if(!ConfigNodeExists(lpcfgShortcuts, TEXT("view3d")))
		ConfigSetInteger(lpcfgShortcuts, TEXT("view3d"),			'3');

	if(!ConfigNodeExists(lpcfgShortcuts, TEXT("view2d")))
		ConfigSetInteger(lpcfgShortcuts, TEXT("view2d"),			'2');

	if(!ConfigNodeExists(lpcfgShortcuts, TEXT("viewiso")))
		ConfigSetInteger(lpcfgShortcuts, TEXT("viewiso"),			'I');

	if(!ConfigNodeExists(lpcfgShortcuts, TEXT("togglesnap")))
		ConfigSetInteger(lpcfgShortcuts, TEXT("togglesnap"),		VK_OEM_2);		/* / */

	if(!ConfigNodeExists(lpcfgShortcuts, TEXT("fliplinedefs")))
		ConfigSetInteger(lpcfgShortcuts, TEXT("fliplinedefs"),		'F');

	if(!ConfigNodeExists(lpcfgShortcuts, TEXT("splitlinedefs")))
		ConfigSetInteger(lpcfgShortcuts, TEXT("splitlinedefs"),		'X');

	if(!ConfigNodeExists(lpcfgShortcuts, TEXT("joinsector")))
		ConfigSetInteger(lpcfgShortcuts, TEXT("joinsector"),		'J');

	if(!ConfigNodeExists(lpcfgShortcuts, TEXT("mergesector")))
		ConfigSetInteger(lpcfgShortcuts, TEXT("mergesector"),		'J' | SC_CTRL);

	if(!ConfigNodeExists(lpcfgShortcuts, TEXT("editundo")))
		ConfigSetInteger(lpcfgShortcuts, TEXT("editundo"),			'Z' | SC_CTRL);

	if(!ConfigNodeExists(lpcfgShortcuts, TEXT("editredo")))
		ConfigSetInteger(lpcfgShortcuts, TEXT("editredo"),			'Y' | SC_CTRL);

	if(!ConfigNodeExists(lpcfgShortcuts, TEXT("editfliph")))
		ConfigSetInteger(lpcfgShortcuts, TEXT("editfliph"),			VK_OEM_COMMA);

	if(!ConfigNodeExists(lpcfgShortcuts, TEXT("editflipv")))
		ConfigSetInteger(lpcfgShortcuts, TEXT("editflipv"),			VK_OEM_PERIOD);

	if(!ConfigNodeExists(lpcfgShortcuts, TEXT("editcopy")))
		ConfigSetInteger(lpcfgShortcuts, TEXT("editcopy"),			'C' | SC_CTRL);

	if(!ConfigNodeExists(lpcfgShortcuts, TEXT("editpaste")))
		ConfigSetInteger(lpcfgShortcuts, TEXT("editpaste"),			'V' | SC_CTRL);

	if(!ConfigNodeExists(lpcfgShortcuts, TEXT("filesaveas")))
		ConfigSetInteger(lpcfgShortcuts, TEXT("filesaveas"),		VK_F12);

	if(!ConfigNodeExists(lpcfgShortcuts, TEXT("incfloor")))
		ConfigSetInteger(lpcfgShortcuts, TEXT("incfloor"),			VK_HOME);

	if(!ConfigNodeExists(lpcfgShortcuts, TEXT("decfloor")))
		ConfigSetInteger(lpcfgShortcuts, TEXT("decfloor"),			VK_END);

	if(!ConfigNodeExists(lpcfgShortcuts, TEXT("incceil")))
		ConfigSetInteger(lpcfgShortcuts, TEXT("incceil"),			VK_NEXT);

	if(!ConfigNodeExists(lpcfgShortcuts, TEXT("decceil")))
		ConfigSetInteger(lpcfgShortcuts, TEXT("decceil"),			VK_PRIOR);

	if(!ConfigNodeExists(lpcfgShortcuts, TEXT("inclight")))
		ConfigSetInteger(lpcfgShortcuts, TEXT("inclight"),			VK_NEXT | SC_CTRL);

	if(!ConfigNodeExists(lpcfgShortcuts, TEXT("declight")))
		ConfigSetInteger(lpcfgShortcuts, TEXT("declight"),			VK_PRIOR | SC_CTRL);

	if(!ConfigNodeExists(lpcfgShortcuts, TEXT("snapselection")))
		ConfigSetInteger(lpcfgShortcuts, TEXT("snapselection"),		VK_RETURN | SC_CTRL);

	if(!ConfigNodeExists(lpcfgShortcuts, TEXT("drawsector")))
		ConfigSetInteger(lpcfgShortcuts, TEXT("drawsector"),		VK_INSERT);

	if(!ConfigNodeExists(lpcfgShortcuts, TEXT("drawrect")))
		ConfigSetInteger(lpcfgShortcuts, TEXT("drawrect"),			VK_INSERT | SC_SHIFT);

	if(!ConfigNodeExists(lpcfgShortcuts, TEXT("editcut")))
		ConfigSetInteger(lpcfgShortcuts, TEXT("editcut"),			'X' | SC_CTRL);

	if(!ConfigNodeExists(lpcfgShortcuts, TEXT("editdelete")))
		ConfigSetInteger(lpcfgShortcuts, TEXT("editdelete"),		VK_DELETE);

	if(!ConfigNodeExists(lpcfgShortcuts, TEXT("cancel")))
		ConfigSetInteger(lpcfgShortcuts, TEXT("cancel"),			VK_ESCAPE);

	if(!ConfigNodeExists(lpcfgShortcuts, TEXT("editrotate")))
		ConfigSetInteger(lpcfgShortcuts, TEXT("editrotate"),		'R');

	if(!ConfigNodeExists(lpcfgShortcuts, TEXT("editresize")))
		ConfigSetInteger(lpcfgShortcuts, TEXT("editresize"),		'R' | SC_SHIFT);

	if(!ConfigNodeExists(lpcfgShortcuts, TEXT("thingrotateacw")))
		ConfigSetInteger(lpcfgShortcuts, TEXT("thingrotateacw"),	VK_OEM_COMMA | SC_CTRL);

	if(!ConfigNodeExists(lpcfgShortcuts, TEXT("thingrotatecw")))
		ConfigSetInteger(lpcfgShortcuts, TEXT("thingrotatecw"),		VK_OEM_PERIOD | SC_CTRL);

	if(!ConfigNodeExists(lpcfgShortcuts, TEXT("editselectall")))
		ConfigSetInteger(lpcfgShortcuts, TEXT("editselectall"),		'A' | SC_CTRL);

	if(!ConfigNodeExists(lpcfgShortcuts, TEXT("editselectnone")))
		ConfigSetInteger(lpcfgShortcuts, TEXT("editselectnone"),	'D' | SC_CTRL);

	if(!ConfigNodeExists(lpcfgShortcuts, TEXT("editselectinvert")))
		ConfigSetInteger(lpcfgShortcuts, TEXT("editselectinvert"),	'I' | SC_CTRL);

	if(!ConfigNodeExists(lpcfgShortcuts, TEXT("copyprops")))
		ConfigSetInteger(lpcfgShortcuts, TEXT("copyprops"),			'C' | SC_CTRL | SC_SHIFT);

	if(!ConfigNodeExists(lpcfgShortcuts, TEXT("pasteprops")))
		ConfigSetInteger(lpcfgShortcuts, TEXT("pasteprops"),		'V' | SC_CTRL | SC_SHIFT);

	if(!ConfigNodeExists(lpcfgShortcuts, TEXT("gridinc")))
		ConfigSetInteger(lpcfgShortcuts, TEXT("gridinc"),			VK_OEM_6);	/* ] */

	if(!ConfigNodeExists(lpcfgShortcuts, TEXT("griddec")))
		ConfigSetInteger(lpcfgShortcuts, TEXT("griddec"),			VK_OEM_4);	/* [ */

	if(!ConfigNodeExists(lpcfgShortcuts, TEXT("editfind")))
		ConfigSetInteger(lpcfgShortcuts, TEXT("editfind"),			'F' | SC_CTRL);

	if(!ConfigNodeExists(lpcfgShortcuts, TEXT("editreplace")))
		ConfigSetInteger(lpcfgShortcuts, TEXT("editreplace"),		'H' | SC_CTRL);

	if(!ConfigNodeExists(lpcfgShortcuts, TEXT("filenew")))
		ConfigSetInteger(lpcfgShortcuts, TEXT("filenew"),			'N' | SC_CTRL);

	if(!ConfigNodeExists(lpcfgShortcuts, TEXT("fileopen")))
		ConfigSetInteger(lpcfgShortcuts, TEXT("fileopen"),			'O' | SC_CTRL);

	if(!ConfigNodeExists(lpcfgShortcuts, TEXT("filesave")))
		ConfigSetInteger(lpcfgShortcuts, TEXT("filesave"),			'S' | SC_CTRL);

	if(!ConfigNodeExists(lpcfgShortcuts, TEXT("editoptions")))
		ConfigSetInteger(lpcfgShortcuts, TEXT("editoptions"),		VK_F2);

	if(!ConfigNodeExists(lpcfgShortcuts, TEXT("thingsmouserotate")))
		ConfigSetInteger(lpcfgShortcuts, TEXT("thingsmouserotate"),	'R' | SC_CTRL | SC_SHIFT);

	if(!ConfigNodeExists(lpcfgShortcuts, TEXT("scrollup")))
		ConfigSetInteger(lpcfgShortcuts, TEXT("scrollup"),			VK_UP);

	if(!ConfigNodeExists(lpcfgShortcuts, TEXT("scrolldown")))
		ConfigSetInteger(lpcfgShortcuts, TEXT("scrolldown"),		VK_DOWN);

	if(!ConfigNodeExists(lpcfgShortcuts, TEXT("scrollleft")))
		ConfigSetInteger(lpcfgShortcuts, TEXT("scrollleft"),		VK_LEFT);

	if(!ConfigNodeExists(lpcfgShortcuts, TEXT("scrollright")))
		ConfigSetInteger(lpcfgShortcuts, TEXT("scrollright"),		VK_RIGHT);

	if(!ConfigNodeExists(lpcfgShortcuts, TEXT("filetest")))
		ConfigSetInteger(lpcfgShortcuts, TEXT("filetest"),			VK_F8);

	if(!ConfigNodeExists(lpcfgShortcuts, TEXT("quicktest")))
		ConfigSetInteger(lpcfgShortcuts, TEXT("quicktest"),			VK_F8 | SC_SHIFT);

	if(!ConfigNodeExists(lpcfgShortcuts, TEXT("editcenterviewsel")))
		ConfigSetInteger(lpcfgShortcuts, TEXT("editcenterviewsel"),	VK_SPACE | SC_CTRL | SC_SHIFT);

	if(!ConfigNodeExists(lpcfgShortcuts, TEXT("backtrackdraw")))
		ConfigSetInteger(lpcfgShortcuts, TEXT("backtrackdraw"),		VK_BACK);

	if(!ConfigNodeExists(lpcfgShortcuts, TEXT("selecttagdual")))
		ConfigSetInteger(lpcfgShortcuts, TEXT("selecttagdual"),		'T' | SC_CTRL);

	if(!ConfigNodeExists(lpcfgShortcuts, TEXT("editfof")))
		ConfigSetInteger(lpcfgShortcuts, TEXT("editfof"),			'F' | SC_CTRL | SC_SHIFT);

	if(!ConfigNodeExists(lpcfgShortcuts, TEXT("quickdrag")))
		ConfigSetInteger(lpcfgShortcuts, TEXT("quickdrag"),			'M' | SC_CTRL);

	if(!ConfigNodeExists(lpcfgShortcuts, TEXT("quickfof")))
		ConfigSetInteger(lpcfgShortcuts, TEXT("quickfof"),			'G' | SC_CTRL | SC_SHIFT);

	if(!ConfigNodeExists(lpcfgShortcuts, TEXT("options")))
		ConfigSetInteger(lpcfgShortcuts, TEXT("options"),			VK_F5);

	if(!ConfigNodeExists(lpcfgShortcuts, TEXT("errorchecker")))
		ConfigSetInteger(lpcfgShortcuts, TEXT("errorchecker"),		VK_F4);

	if(!ConfigNodeExists(lpcfgShortcuts, TEXT("properties")))
		ConfigSetInteger(lpcfgShortcuts, TEXT("properties"),		VK_RETURN | SC_ALT);

	if(!ConfigNodeExists(lpcfgShortcuts, TEXT("editheader")))
		ConfigSetInteger(lpcfgShortcuts, TEXT("editheader"),		VK_F2 | SC_CTRL);

	if(!ConfigNodeExists(lpcfgShortcuts, TEXT("fixmisstex")))
		ConfigSetInteger(lpcfgShortcuts, TEXT("fixmisstex"),		VK_F3 | SC_CTRL);

	if(!ConfigNodeExists(lpcfgShortcuts, TEXT("autoalign")))
		ConfigSetInteger(lpcfgShortcuts, TEXT("autoalign"),			VK_F3);

	if(!ConfigNodeExists(lpcfgShortcuts, TEXT("editlock")))
		ConfigSetInteger(lpcfgShortcuts, TEXT("editlock"),			'L' | SC_CTRL);

	if(!ConfigNodeExists(lpcfgShortcuts, TEXT("3drotateacw")))
		ConfigSetInteger(lpcfgShortcuts, TEXT("3drotateacw"),		VK_RIGHT | SC_CTRL);

	if(!ConfigNodeExists(lpcfgShortcuts, TEXT("3drotatecw")))
		ConfigSetInteger(lpcfgShortcuts, TEXT("3drotatecw"),		VK_LEFT | SC_CTRL);

	if(!ConfigNodeExists(lpcfgShortcuts, TEXT("3dforwards")))
		ConfigSetInteger(lpcfgShortcuts, TEXT("3dforwards"),		'W');

	if(!ConfigNodeExists(lpcfgShortcuts, TEXT("3dbackwards")))
		ConfigSetInteger(lpcfgShortcuts, TEXT("3dbackwards"),		'S');

	if(!ConfigNodeExists(lpcfgShortcuts, TEXT("3dstrafeleft")))
		ConfigSetInteger(lpcfgShortcuts, TEXT("3dstrafeleft"),		'A');

	if(!ConfigNodeExists(lpcfgShortcuts, TEXT("3dstraferight")))
		ConfigSetInteger(lpcfgShortcuts, TEXT("3dstraferight"),		'D');

	if(!ConfigNodeExists(lpcfgShortcuts, TEXT("incxoffset")))
		ConfigSetInteger(lpcfgShortcuts, TEXT("incxoffset"),		VK_LEFT | SC_SHIFT);

	if(!ConfigNodeExists(lpcfgShortcuts, TEXT("decxoffset")))
		ConfigSetInteger(lpcfgShortcuts, TEXT("decxoffset"),		VK_RIGHT | SC_SHIFT);

	if(!ConfigNodeExists(lpcfgShortcuts, TEXT("incyoffset")))
		ConfigSetInteger(lpcfgShortcuts, TEXT("incyoffset"),		VK_DOWN | SC_SHIFT);

	if(!ConfigNodeExists(lpcfgShortcuts, TEXT("decyoffset")))
		ConfigSetInteger(lpcfgShortcuts, TEXT("decyoffset"),		VK_UP | SC_SHIFT);

	if(!ConfigNodeExists(lpcfgShortcuts, TEXT("3dstrafeup")))
		ConfigSetInteger(lpcfgShortcuts, TEXT("3dstrafeup"),		'R');

	if(!ConfigNodeExists(lpcfgShortcuts, TEXT("3dstrafedown")))
		ConfigSetInteger(lpcfgShortcuts, TEXT("3dstrafedown"),		'F');

	/* _SCK_ */
}


/* TranslateBespokeAccelerator
 *   Handles shortcut key translation and dispatch.
 *
 * Parameters:
 *   LPMSG	lpmsg	Window message.
 *
 * Return value: BOOL
 *   TRUE if a shortcut was dispatched; FALSE otherwise.
 */
BOOL TranslateBespokeAccelerator(LPMSG lpmsg)
{
	BOOL bIsChild = FALSE;

	if((lpmsg->message == WM_KEYDOWN || lpmsg->message == WM_SYSKEYDOWN) &&
		IsWindow(g_hwndClient) &&
		(g_hwndClient == lpmsg->hwnd ||
			(bIsChild = IsChild(g_hwndClient, lpmsg->hwnd)) ||
			(GetKeyState(VK_CONTROL) & 0x8000) ||
			(GetKeyState(VK_MENU) & 0x8000)))
	{
		USHORT unMenuID;

		if(bIsChild)
		{
			/* Child windows require special processing, because we need to
			 * perform extra checks for the rendering mode.
			 */
			WORD wModeFlags[] = {SMF_2D, SMF_ISO, SMF_3D};
			unMenuID = GetMenuIDFromKeycodeAndMode(MakeShiftedKeyCode(lpmsg->wParam), wModeFlags[GetMapWinRenderMode(lpmsg->hwnd)]);
		}
		else
			unMenuID = GetMenuIDFromKeycode(MakeShiftedKeyCode(lpmsg->wParam));

		/* Firstly, make sure it's a shortcut. Then, we dispatch it if a map
		 * window (or the MDI client) has the focus, or if Ctrl or Alt is
		 * depressed. Sounds arbitrary? Yup!
		 */
		if(unMenuID != NOCOMMAND)
		{
			SendMessage(g_hwndMain, WM_COMMAND, MAKEWPARAM(unMenuID, 1), 0);
			return TRUE;
		}
	}

	return FALSE;
}


/* GetShiftedShortcutText
 *   Generates shortcut key text for a shifted keycode.
 *
 * Parameters:
 *   int		iKeyCode	Shifted keycode.
 *   LPTSTR		szBuffer	Buffer in which to write shortcut string.
 *   UINT		cchBuffer	Length of buffer, including room for terminator.
 *
 * Return value: None.
 */
void GetShiftedShortcutText(int iKeyCode, LPTSTR szBuffer, UINT cchBuffer)
{
	struct {DWORD dwSCModifier; BYTE byVK;} modifiers[] = {{SC_CTRL, VK_CONTROL}, {SC_ALT, VK_MENU}, {SC_SHIFT, VK_SHIFT}};
	int i;

	for(i = 0; i < (int)NUM_ELEMENTS(modifiers); i++)
	{
		if(iKeyCode & modifiers[i].dwSCModifier)
		{
			UINT cchModifier;
			GetVirtualKeyText(modifiers[i].byVK, szBuffer, cchBuffer);
			cchModifier = _tcslen(szBuffer);
			cchBuffer -= cchModifier + 1;
			szBuffer += cchModifier;
			*szBuffer++ = TEXT('+');
			*szBuffer = TEXT('\0');
		}
	}

	/* Get the key itself. */
	GetVirtualKeyText(iKeyCode & 0xFFFF, szBuffer, cchBuffer);
}


/* IsExtendedShortcut
 *   Determines whether a shortcut uses an extended key.
 *
 * Parameters:
 *   int		iKeyCode	VK code or Shifted keycode.
 *
 * Return value: BOOL
 *   TRUE if the shortcut uses an extended key; FALSE otherwise.
 *
 * Remarks:
 *   Hat-tip to http://www.codeproject.com/KB/tips/getacceltext.aspx
 */
static __inline BOOL IsExtendedShortcut(int iKeyCode)
{
	switch(iKeyCode & SC_KEY)
	{
		case VK_INSERT: case VK_DELETE:
		case VK_HOME: case VK_END:
		case VK_NEXT: case VK_PRIOR:
		case VK_LEFT: case VK_RIGHT:
		case VK_UP: case VK_DOWN:
			return TRUE;
	}

	return FALSE;
}


/* ShiftedKeycodeToHotkey
 *   Converts a shifted keycode to a Hotkey control keycode.
 *
 * Parameters:
 *   int		iKeyCode	Shifted keycode.
 *
 * Return value: WORD
 *   Hotkey control keycode.
 *
 * Remarks:
 *   This makes no sense for mouse keycodes.
 */
WORD ShiftedKeycodeToHotkey(int iKeyCode)
{
	return MAKEWORD((iKeyCode & SC_KEY),
		((iKeyCode & SC_CTRL) ? HOTKEYF_CONTROL : 0)
		| ((iKeyCode & SC_SHIFT) ? HOTKEYF_SHIFT : 0)
		| ((iKeyCode & SC_ALT) ? HOTKEYF_ALT : 0)
		| (IsExtendedShortcut(iKeyCode) ? HOTKEYF_EXT : 0));
}


/* HotkeyToShiftedKeycode
 *   Converts a Hotkey control keycode to a shifted keycode.
 *
 * Parameters:
 *   WORD	wHotkey		Hotkey control keycode.
 *
 * Return value: int
 *   Shifted keycode.
 */
int HotkeyToShiftedKeycode(WORD wHotkey)
{
	BYTE byFlags = HIBYTE(wHotkey);
	return LOBYTE(wHotkey)
		| ((byFlags & HOTKEYF_CONTROL) ? SC_CTRL : 0)
		| ((byFlags & HOTKEYF_SHIFT) ? SC_SHIFT : 0)
		| ((byFlags & HOTKEYF_ALT) ? SC_ALT : 0);
}


/* InitialiseKeyboard
 *   Performs initialisation for keyboard routines.
 *
 * Parameters:
 *   None.
 *
 * Return value: None.
 */
void InitialiseKeyboard(void)
{
	/* Initialise the VK-to-string-ID table. */
#define VKSTRID(vkcode)	g_nVKToStringIndex[vkcode] = IDS_##vkcode
	VKSTRID(VK_CONTROL);
	VKSTRID(VK_SHIFT);
	VKSTRID(VK_MENU);
	VKSTRID(VK_BACK);
	VKSTRID(VK_RETURN);
	VKSTRID(VK_TAB);
	VKSTRID(VK_ESCAPE);
	VKSTRID(VK_SPACE);
	VKSTRID(VK_DELETE);
	VKSTRID(VK_PAUSE);
	VKSTRID(VK_CAPITAL);
	VKSTRID(VK_PRIOR);
	VKSTRID(VK_NEXT);
	VKSTRID(VK_END);
	VKSTRID(VK_HOME);
	VKSTRID(VK_LEFT);
	VKSTRID(VK_UP);
	VKSTRID(VK_RIGHT);
	VKSTRID(VK_DOWN);
	VKSTRID(VK_INSERT);
#undef VKSTRID
}


/* GetStringIDForVK
 *   Gets a string ID for a virtual keycode or mouse-wheel pseudo VK.
 *
 * Parameters:
 *   short		nVKCode		VK code.
 *
 * Return value: short
 *   String ID, or zero if none.
 */
short GetStringIDForVK(short nVKCode)
{
	switch(nVKCode)
	{
		case MOUSE_SCROLL_UP:	return IDS_SHORTCUTS_WUP;
		case MOUSE_SCROLL_DOWN:	return IDS_SHORTCUTS_WDOWN;
		default:				return g_nVKToStringIndex[nVKCode];
	}
}


/* GetVirtualKeyText
 *   Gets a string for a virtual keycode or mouse-wheel pseudo VK.
 *
 * Parameters:
 *   short		nVKCode		VK code.
 *   LPTSTR		szBuffer	Buffer in which to store string.
 *   int		cchBuffer	Size of buffer, including room for terminator.
 *
 * Return value: int
 *   Length of returned string, not including terminator.
 */
static int GetVirtualKeyText(short nVKCode, LPTSTR szBuffer, int cchBuffer)
{
	short nStrID = GetStringIDForVK(nVKCode);

	/* In case we have no translation, terminate the string. */
	if(cchBuffer > 0) *szBuffer = TEXT('\0');

	if(nStrID) return LoadString(g_hInstance, nStrID, szBuffer, cchBuffer);
	else
	{
		UINT uiScanCode = MapVirtualKey(nVKCode, MAPVK_VK_TO_VSC) << 16;

		/* Add extended bit if necessary. */
		if(IsExtendedShortcut(nVKCode))
			uiScanCode |= 0x100;

		return GetKeyNameText(uiScanCode, szBuffer, cchBuffer);
	}
}


/* GetShortcutModeFlags
 *   Retrieves mode flags for a shortcut key.
 *
 * Parameters:
 *   int	iShortcut	SCK_* code of shortcut.
 *
 * Return value: int
 *   Mode flags.
 *
 * Remarks:
 *   Callers outside of this module won't know the significance of the
 *   individual flags, but they can AND them against one another.
 */
WORD GetShortcutModeFlags(int iShortcut)
{
	return g_shortcutinfo[iShortcut].wModeFlags;
}
