/* SRB2 Workbench -- A map editor for Sonic Robo Blast 2.
 * Copyright (C) 2009 Gregor Dick
 * http://workbench.srb2.org/
 *
 * Incorporates portions of Doom Builder, (C) 2003 Pascal vd Heiden.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 * USA.
 *
 * texenum.h: Header for texenum.c.
 *
 * AMDG.
 */

#ifndef __SRB2B_TEXENUM__
#define __SRB2B_TEXENUM__

#include <windows.h>
#include <objbase.h>
#include <shldisp.h>

#include "general.h"
#include "texture.h"

typedef struct CTexEnum *LPTEXENUM;

LPTEXENUM CreateTexEnum(void);
void DestroyTexEnum(LPTEXENUM lptexenum);
void SetTexEnumList(LPTEXENUM lptexenum, TEXTURENAMELIST *lptnl);
IAutoComplete2* CreateAutoComplete(void);

#endif
