/* SRB2 Workbench -- A map editor for Sonic Robo Blast 2.
 * Copyright (C) 2009 Gregor Dick
 * http://workbench.srb2.org/
 *
 * Incorporates portions of Doom Builder, (C) 2003 Pascal vd Heiden.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 * USA.
 *
 * mapwin.h: Header for mapwin.c.
 *
 * AMDG.
 */

#ifndef __SRB2B_MAPWIN__
#define __SRB2B_MAPWIN__

#include <windows.h>
#include <GL/gl.h>
#include <GL/glu.h>

#include "../maptypes.h"
#include "../config.h"
#include "../selection.h"
#include "../texture.h"

#include "mdiframe.h"

/* Types. */

typedef enum _ENUM_EDITMODE
{
	EM_MOVE,
	EM_ANY,
	EM_VERTICES,
	EM_LINES,
	EM_SECTORS,
	EM_THINGS,
	EM_COUNT,
	EM_INVALID
} ENUM_EDITMODE;

typedef enum _ENUM_EDITSUBMODE
{
	ESM_NONE,
	ESM_DRAGGING,
	ESM_DRAWING,
	ESM_SELECTING,
	ESM_PASTING,
	ESM_MOVING,
	ESM_ROTATING,
	ESM_INSERTING,
	ESM_PREDRAG
} ENUM_EDITSUBMODE;

enum ENUM_FIND_FLAGS
{
	FFL_SELECT = 1,
	FFL_REPLACE = 2
};

/* Different sorts of drawing. */
typedef enum _ENUM_INSERT_MODE
{
	IM_PATH,
	IM_DCK,
	IM_VERTEX,
	IM_THING
} ENUM_INSERT_MODE;


/* Function prototypes. */
HWND CreateMapWindow(MAP *lpmap, LPSTR szLumpName, CONFIG *lpcfgMap, int iWadID, int iIWadID);
int RegisterMapWindowClass(void);
BOOL MapWinBelongsToWad(HWND hwnd, int iWadID);

BOOL GetTextureForMap(HWND hwnd, LPCTSTR szTexName, TEXTURE **lplptex, TEX_FORMAT tf);

#ifdef UNICODE
BOOL GetTextureForMapA(HWND hwnd, LPCSTR szTexNameA, TEXTURE **lplptex, TEX_FORMAT tf);
#else
#define GetTextureForMapA GetTextureForMap
#endif

void MakeUndoText(HWND hwndMap, LPTSTR szCaption, DWORD cchCaption);
void MakeRedoText(HWND hwndMap, LPTSTR szCaption, DWORD cchCaption);
void DeselectAllByHandle(HWND hwndMap);
int FindReplaceInt(HWND hwndMap, WORD wFindType, WORD wCompare, int iValue, int iReplace, BOOL bInSelection, BYTE byFlags);
int FindReplaceStr(HWND hwndMap, WORD wFindType, WORD wCompare, LPSTR szString, LPSTR szReplace, BOOL bInSelection, BYTE byFlags);
void GridProperties(HWND hwndMap);
void TexPreviewClicked(HWND hwndMap, char cNotifyID);
void LoadPopupMenus(void);
void DestroyPopupMenus(void);
BOOL LumpnameMatches(HWND hwndMap, LPCSTR szLumpname);
void RenameMapInWindow(HWND hwndMap, LPCSTR szNewName);
HWND FindMapWindowByLumpname(int iWad, LPCSTR szLumpName);
void ZoomToSelection(HWND hwndMap);
void ChangeMapConfigByHandle(HWND hwndMap, CONFIG *lpcfgMap);
void ExtSetSelection(HWND hwndMap, int *lpiIndices, int iCount, ENUM_EDITMODE emSelType);
BOOL CALLBACK SendOptionsChangedMessage(HWND hwnd, LPARAM lParam);
int GetMapWinRenderMode(HWND hwndMap);

/* Forces a map window to close without saving. */
static __inline void ForceMapWindowClose(HWND hwndMap) { SendMessage(g_hwndClient, WM_MDIDESTROY, (WPARAM)hwndMap, 0); }

/* Macros. */
#define MAPWINCLASS TEXT("SRB2WorkbenchMap")

#endif
