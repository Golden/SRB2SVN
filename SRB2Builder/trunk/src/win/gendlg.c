/* SRB2 Workbench -- A map editor for Sonic Robo Blast 2.
 * Copyright (C) 2009 Gregor Dick
 * http://workbench.srb2.org/
 *
 * Incorporates portions of Doom Builder, (C) 2003 Pascal vd Heiden.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 * USA.
 *
 * gendlg.c: Routines useful in dialogues, and even some entire dialogues that
 * don't belong anywhere else.
 *
 * AMDG.
 */

#include <windows.h>
#include <commctrl.h>
#include <uxtheme.h>
#include <tmschema.h>
#include <tchar.h>
#include <stdio.h>

#include "../general.h"
#include "../config.h"
#include "../options.h"
#include "../testing.h"

#include "../../res/resource.h"

#include "mdiframe.h"
#include "gendlg.h"
#include "editdlg.h"
#include "mapoptions.h"


#ifndef HTHEME
#define HTHEME HANDLE
#endif

#define LICENCE_BUFSIZE 1024

#define GRIDMIN			1
#define GRIDMAX			32768
#define GRIDOFFSETMIN	(-32768)
#define GRIDOFFSETMAX	32767
#define FIRSTGRIDLIST	4
#define LASTGRIDLIST	256


/* We have to load UxTheme on demand, so we need some typedefs for its
 * functions.
 */
typedef HTHEME (WINAPI *OPENTHEMEDATAFUNC) (HWND hwnd, LPCWSTR pszClassList);
typedef HRESULT (WINAPI *CLOSETHEMEDATAFUNC) (HTHEME hTheme);
typedef HRESULT (WINAPI *DRAWTHEMEBACKGROUNDFUNC) (HTHEME hTheme, HDC hdc, int iPartId, int iStateId, const RECT *pRect, const RECT *pClipRect);


/* Keeps track of the active modless dialogue so that we can call
 * IsDialogMessage on it.
 */
HWND g_hwndLastModelessDialogue = NULL;


static INT_PTR CALLBACK GridDlgProc(HWND hwndDlg, UINT uiMsg, WPARAM wParam, LPARAM lParam);
static INT_PTR CALLBACK TestDlgProc(HWND hwndDlg, UINT uiMsg, WPARAM wParam, LPARAM lParam);
static void PopulateSkinList(HWND hwndCombo);
static void PopulateDifficultyList(HWND hwndCombo);
static LRESULT WINAPI PropSheetCentreDlgProc(HWND hwndDlg, UINT uiMsg, WPARAM wParam, LPARAM lParam);



/* ListBoxSearchByItemData
 *   Searches a list box on its members' item data.
 *
 * Parameters:
 *   HWND		hwndListBox	Handle to list box.
 *   int		iItemData	Value to search for.
 *   BOOL		bSelect		Also select the match.
 *
 * Return value: int
 *   Index of first match, or negative if no match found.
 *
 * Remarks:
 *   Search ends after the first match.
 */
int ListBoxSearchByItemData(HWND hwndListBox, int iItemData, BOOL bSelect)
{
	int i, iCount;

	/* Find limit for looping through box. */
	iCount = SendMessage(hwndListBox, LB_GETCOUNT, 0, 0);

	for(i = 0; i < iCount; i++)
	{
		if(iItemData == SendMessage(hwndListBox, LB_GETITEMDATA, i, 0))
		{
			if(bSelect) SendMessage(hwndListBox, LB_SETCURSEL, i, 0);
			return i;
		}
	}

	/* No match. */
	if(bSelect) SendMessage(hwndListBox, LB_SETCURSEL, -1, 0);
	return -1;
}

/* ComboBoxSearchByItemData
 *   Searches a combo box on its members' item data.
 *
 * Parameters:
 *   HWND		hwndCombo	Handle to combo.
 *   int		iItemData	Value to search for.
 *   BOOL		bSelect		Also select the match.
 *
 * Return value: int
 *   Index of first match, or negative if no match found.
 *
 * Remarks:
 *   Search ends after the first match.
 */
int ComboBoxSearchByItemData(HWND hwndCombo, int iItemData, BOOL bSelect)
{
	int i, iCount;

	/* Find limit for looping through box. */
	iCount = SendMessage(hwndCombo, CB_GETCOUNT, 0, 0);

	for(i = 0; i < iCount; i++)
	{
		if(iItemData == SendMessage(hwndCombo, CB_GETITEMDATA, i, 0))
		{
			if(bSelect) SendMessage(hwndCombo, CB_SETCURSEL, i, 0);
			return i;
		}
	}

	/* No match. */
	if(bSelect) SendMessage(hwndCombo, CB_SETCURSEL, -1, 0);
	return -1;
}



/* BoundEditBox
 *   Bounds an edit-box's text between two ints.
 *
 * Parameters:
 *   HWND		hwndEditBox			Handle to edit box.
 *   int		iMin, iMax			Bounding range.
 *   BOOL		bPreserveEmpty		Leave empty boxes be?
 *   BOOL		bPreserveRelative	Allow strings beginning ++ or --.
 *
 * Return value: BOOL
 *   TRUE if box text was changed; FALSE otherwise.
 */
BOOL BoundEditBox(HWND hwndEditBox, int iMin, int iMax, BOOL bPreserveEmpty, BOOL bPreserveRelative)
{
	int i, j;
	TCHAR szNew[CCH_SIGNED_INT], szOld[CCH_SIGNED_INT];

	/* 12 is the most we'll ever need for an int: sign, ten digits, \0. */
	GetWindowText(hwndEditBox, szOld, NUM_ELEMENTS(szOld));

	/* Preserve empty boxes and relative strings, if desired. */
	if((bPreserveEmpty && *szOld == '\0') ||
		(bPreserveRelative && *szOld && STRING_RELATIVE(szOld)))
	{
		return FALSE;
	}

	i = _tcstol(szOld, NULL, 10);
	j = min(max(i, iMin), iMax);
	wsprintf(szNew, TEXT("%d"), j);

	/* Only set the text if we need to. This avoids spurious EN_CHANGE messages.
	 */
	if(_tcsncmp(szNew, szOld, NUM_ELEMENTS(szNew)) != 0 ||
		SendMessage(hwndEditBox, WM_GETTEXTLENGTH, 0, 0) >= CCH_SIGNED_INT)
	{
		SetWindowText(hwndEditBox, szNew);
		return TRUE;
	}

	return FALSE;
}


/* BoundEditBoxFloat
 *   Bounds an edit-box's text between two floats.
 *
 * Parameters:
 *   HWND		hwndEditBox			Handle to edit box.
 *   float		fMin, fMax			Bounding range.
 *   BOOL		bPreserveEmpty		Leave empty boxes be?
 *   BOOL		bPreserveRelative	Allow strings beginning ++ or --.
 *
 * Return value: BOOL
 *   TRUE if box text was changed; FALSE otherwise.
 */
BOOL BoundEditBoxFloat(HWND hwndEditBox, float fMin, float fMax, BOOL bPreserveEmpty, BOOL bPreserveRelative)
{
	float fRead, fBounded;
	LPTSTR szEnd;
	TCHAR sz[32];

	GetWindowText(hwndEditBox, sz, sizeof(sz)/sizeof(TCHAR));

	/* Preserve empty boxes and relative strings, if desired. */
	if((bPreserveEmpty && *sz == '\0') ||
		(bPreserveRelative && *sz && STRING_RELATIVE(sz)))
		return FALSE;

	fRead = (float)_tcstod(sz, &szEnd);
	fBounded = min(max(fRead, fMin), fMax);
	_sntprintf(sz, sizeof(sz)/sizeof(TCHAR), TEXT("%g"), fBounded);
	sz[sizeof(sz)/sizeof(TCHAR) - 1] = TEXT('\0');
	SetWindowText(hwndEditBox, sz);

	return *szEnd || fRead != fBounded;
}


/* GetDlgItemFloat
 *   Bounds an edit-box's text between two floats.
 *
 * Parameters:
 *   HWND	hwndDlg			Dialogue window handle.
 *   int	iDlgItem		Control ID.
 *   BOOL*	lpbTranslated	Set to whether successfully translated. May be NULL.
 *
 * Return value: float
 *   Value of text.
 */
float GetDlgItemFloat(HWND hwndDlg, int iDlgItem, BOOL *lpbTranslated)
{
	LPTSTR szEnd;
	TCHAR sz[32];
	float fRet;

	GetWindowText(GetDlgItem(hwndDlg, iDlgItem), sz, sizeof(sz)/sizeof(TCHAR));
	fRet = (float)_tcstod(sz, &szEnd);
	if(lpbTranslated) *lpbTranslated = (*szEnd == TEXT('\0'));

	return fRet;
}


/* Init3StateListView
 *   Configures a list view control to incorporate 3-state checkboxes.
 *
 * Parameters:
 *   HWND		hwndListView	Handle to list view.
 *
 * Return value: None.
 *
 * Remarks:
 *   The list view should not have the LVS_EX_CHECKBOXES style. To perform the
 *   state logic in response to clicks/keypresses, call ListView3StateClick or
 *   ListView3StateKeyDown, respectively.
 */
void Init3StateListView(HWND hwndListView)
{
	HIMAGELIST himl;
	HDC hdc = CreateCompatibleDC(NULL);
	HBITMAP hbmUnchecked, hbmIndeterminate, hbmChecked;
	int cxSmIcon, cySmIcon;
	RECT rc;
	HGDIOBJ hgdiobjOriginal;
	static const WCHAR ButtonClass[] = { 'B', 'U', 'T', 'T', 'O' , 'N', '\0'}; // L"BUTTON"

	/* For Visual Styles. */
	HMODULE hmodUxTheme;
	HTHEME hTheme = NULL;


	/* How big should the images be? */
	cxSmIcon = GetSystemMetrics(SM_CXMENUCHECK);
	cySmIcon = GetSystemMetrics(SM_CYMENUCHECK);

	/* Create the empty bitmaps. */
	hbmUnchecked = CreateBitmap(cxSmIcon, cySmIcon, 1, 32, NULL);
	hbmIndeterminate = CreateBitmap(cxSmIcon, cySmIcon, 1, 32, NULL);
	hbmChecked = CreateBitmap(cxSmIcon, cySmIcon, 1, 32, NULL);

	/* Bounding rectangle for the images. */
	rc.top = rc.left = 0;
	rc.right = cxSmIcon;
	rc.bottom = cySmIcon;

	/* Draw onto the bitmaps. How we do this depends on whether there's a visual
	 * style active.
	 */
	if((hmodUxTheme = LoadLibrary(TEXT("uxtheme.dll"))))
	{
		OPENTHEMEDATAFUNC lpfnOpenThemeData = (OPENTHEMEDATAFUNC)GetProcAddress(hmodUxTheme, "OpenThemeData");
		hTheme = lpfnOpenThemeData(hwndListView, ButtonClass);
	}

	hgdiobjOriginal = SelectObject(hdc, hbmUnchecked);

	if(hTheme)
	{
		DRAWTHEMEBACKGROUNDFUNC lpfnDrawThemeBackground = (DRAWTHEMEBACKGROUNDFUNC)GetProcAddress(hmodUxTheme, "DrawThemeBackground");
		CLOSETHEMEDATAFUNC lpfnCloseThemeData = (CLOSETHEMEDATAFUNC)GetProcAddress(hmodUxTheme, "CloseThemeData");

		lpfnDrawThemeBackground(hTheme, hdc, BP_CHECKBOX, CBS_UNCHECKEDNORMAL, &rc, NULL);
		SelectObject(hdc, hbmIndeterminate);
		lpfnDrawThemeBackground(hTheme, hdc, BP_CHECKBOX, CBS_MIXEDNORMAL, &rc, NULL);
		SelectObject(hdc, hbmChecked);
		lpfnDrawThemeBackground(hTheme, hdc, BP_CHECKBOX, CBS_CHECKEDNORMAL, &rc, NULL);

		lpfnCloseThemeData(hTheme);
	}
	else
	{
		DrawFrameControl(hdc, &rc, DFC_BUTTON, DFCS_BUTTONCHECK );
		SelectObject(hdc, hbmIndeterminate);
		DrawFrameControl(hdc, &rc, DFC_BUTTON, DFCS_BUTTON3STATE | DFCS_CHECKED);
		SelectObject(hdc, hbmChecked);
		DrawFrameControl(hdc, &rc, DFC_BUTTON, DFCS_BUTTONCHECK | DFCS_CHECKED);
	}

	SelectObject(hdc, hgdiobjOriginal);

	/* Create the image list. It'll be destroyed when the list view is
	 * destroyed.
	 */
	himl = ImageList_Create(cxSmIcon, cySmIcon, ILC_COLOR24, 3, 0);

	/* Add the images. */
	ImageList_SetImageCount(himl, 3);
	ImageList_Replace(himl, 0, hbmUnchecked, NULL);
	ImageList_Replace(himl, 1, hbmIndeterminate, NULL);
	ImageList_Replace(himl, 2, hbmChecked, NULL);

	/* Associate the image list with the control. */
	ListView_SetImageList(hwndListView, himl, LVSIL_STATE);


	/* Clean up. */
	if(hmodUxTheme) FreeLibrary(hmodUxTheme);

	DeleteObject(hbmUnchecked);
	DeleteObject(hbmIndeterminate);
	DeleteObject(hbmChecked);

	DeleteDC(hdc);
}



/* ListView3StateClick
 *   Performs state changes for a 3-state checkbox list view in response to a
 *   mouse click.
 *
 * Parameters:
 *   LPNMLISTVIEW	lpnmlistview	Notification info from NM_CLICK notification
 *									message.
 *
 * Return value: None.
 */
void ListView3StateClick(LPNMLISTVIEW lpnmlistview)
{
	LVHITTESTINFO lvhti;
	int iIndex;

	/* Did we click on a checkbox? */
	lvhti.pt = lpnmlistview->ptAction;
	iIndex = ListView_HitTest(lpnmlistview->hdr.hwndFrom, &lvhti);
	if(lvhti.flags & LVHT_ONITEMSTATEICON)
		ListView3StateToggleItem(lpnmlistview->hdr.hwndFrom, iIndex);
}


/* ListView3StateKeyDown
 *   Performs state changes for a 3-state checkbox list view in response to a
 *   key depression.
 *
 * Parameters:
 *   LPNMLVKEYDOWN	lpnmlvkeydown	Notification info from LVN_KEYDOWN
 *									notification message.
 *
 * Return value: None.
 */
void ListView3StateKeyDown(LPNMLVKEYDOWN lpnmlvkeydown)
{
	int iIndex = ListView_GetSelectionMark(lpnmlvkeydown->hdr.hwndFrom);

	if(iIndex >= 0 && lpnmlvkeydown->wVKey == VK_SPACE)
		ListView3StateToggleItem(lpnmlvkeydown->hdr.hwndFrom, iIndex);
}


/* ListView3StateToggleItem
 *   Deselects an item in a 3-state checkbox list view if it was selected (even
 *   only partially), and selects it otherwise.
 *
 * Parameters:
 *   HWND	hwndListView	Handle to list view control.
 *   int	iIndex			Index of item to toggle.
 *
 * Return value: None.
 */
void ListView3StateToggleItem(HWND hwndListView, int iIndex)
{
	/* If the checkbox is fully or partially selected, deselect it;
	 * otherwise select it.
	 */
	int iState = ListView3StateGetItemState(hwndListView, iIndex);
	iState = (iState != LV3_UNCHECKED) ? LV3_UNCHECKED : LV3_CHECKED;
	ListView3StateSetItemState(hwndListView, iIndex, iState);
}


/* ListView3StateSetItemState
 *   Sets the state of an item in a 3-state checkbox list view.
 *
 * Parameters:
 *   HWND	hwndListView	Handle to list view control.
 *   int	iIndex			Index of item.
 *   int	iState			State (see ENUM_LV_3STATE).
 *
 * Return value: None.
 */
void ListView3StateSetItemState(HWND hwndListView, int iIndex, int iState)
{
	ListView_SetItemState(hwndListView, iIndex, iState << 12, LVIS_STATEIMAGEMASK);
}


/* ListView3StateGetItemState
 *   Gets the state of an item in a 3-state checkbox list view.
 *
 * Parameters:
 *   HWND	hwndListView	Handle to list view control.
 *   int	iIndex			Index of item.
 *
 * Return value: int
 *   State. See ENUM_LV_3STATE for values.
 */
int ListView3StateGetItemState(HWND hwndListView, int iIndex)
{
	return ListView_GetItemState(hwndListView, iIndex, LVIS_STATEIMAGEMASK) >> 12;
}



/* AboutDlgProc
 *   Window procedure for the About dialogue box.
 *
 * Parameters:
 *   HWND		hwndDlg		Window handle for the dialogue.
 *   UINT		uiMessage	ID for the message.
 *   WPARAM		wParam		Message-specific.
 *   LPARAM		lParam		Message-specific.
 *
 * Return value: INT_PTR
 *   TRUE if message was processed; FALSE otherwise.
 */
INT_PTR CALLBACK AboutDlgProc(HWND hwndDlg, UINT uiMsg, WPARAM wParam, LPARAM lParam)
{
	static HICON s_hicon;

	UNREFERENCED_PARAMETER(lParam);

	switch(uiMsg)
	{
	case WM_INITDIALOG:
		{
			TCHAR szLicence[LICENCE_BUFSIZE];

			/* Centre in parent. */
			CentreWindowInParent(hwndDlg);

			/* Set the icon. */
			s_hicon = LoadImage(g_hInstance, MAKEINTRESOURCE(IDI_MAIN), IMAGE_ICON, 48, 48, LR_DEFAULTCOLOR);
			SendDlgItemMessage(hwndDlg, IDC_STATIC_ICON, STM_SETICON, (WPARAM)s_hicon, 0);

			/* The version is hard-coded since the file installation APIs are
			 * such a pain.
			 */

			/* Set the licence text. */
			LoadString(g_hInstance, IDS_LICENCE, szLicence, sizeof(szLicence) / sizeof(TCHAR));
			SendDlgItemMessage(hwndDlg, IDC_EDIT_LICENCE, WM_SETTEXT, 0, (LPARAM)szLicence);
		}

		/* Let system set the focus. */
		return TRUE;

	case WM_COMMAND:
		switch(LOWORD(wParam))
		{
		case IDOK:
		case IDCANCEL:
			EndDialog(hwndDlg, 0);
			return TRUE;
		}

		break;

	case WM_DESTROY:
		DestroyIcon(s_hicon);
		return TRUE;
	}

	/* Didn't process message. */
	return FALSE;
}


/* ShowGridDlg
 *   Displays the Grid Setup dialogue box.
 *
 * Parameters:
 *   HWND			hwndParent	Parent for dialogue.
 *   GRIDDLGDATA*	lpgriddd	Pointer to structure with initial values and in
 *								which to return new values.
 *
 * Return value: BOOL
 *   FALSE if cancelled; TRUE otherwise.
 */
BOOL ShowGridDlg(HWND hwndParent, GRIDDLGDATA *lpgriddd)
{
	return DialogBoxParam(g_hInstance, MAKEINTRESOURCE(IDD_GRID), hwndParent, GridDlgProc, (LPARAM)lpgriddd);
}


/* GridDlgProc
 *   Dialogue proc for Grid Setup dialogue box.
 *
 * Parameters:
 *   HWND	hwndDlg		Dialogue window handle.
 *   UINT	uiMsg		Message identifier.
 *   WPARAM wParam		Message-specific.
 *   LPARAM lParam		Message-specific.
 *
 * Return value: INT_PTR
 *   TRUE if the message was processed; FALSE otherwise.
 */
static INT_PTR CALLBACK GridDlgProc(HWND hwndDlg, UINT uiMsg, WPARAM wParam, LPARAM lParam)
{
	static GRIDDLGDATA *s_lpgriddd;

	switch(uiMsg)
	{
	case WM_INITDIALOG:
		{
			UDACCEL udaccel[2];
			int i;

			/* Centre in parent. */
			CentreWindowInParent(hwndDlg);

			/* Get the structure in which we return parameters, and which we use
			 * to set initial control states.
			 */
			s_lpgriddd = (GRIDDLGDATA*)lParam;

			/* Set up the spinner control. */

			udaccel[0].nInc = 1;
			udaccel[0].nSec = 0;
			udaccel[1].nInc = 8;
			udaccel[1].nSec = 2;

			SendDlgItemMessage(hwndDlg, IDC_SPIN_XOFFSET, UDM_SETRANGE32, GRIDOFFSETMIN, GRIDOFFSETMAX);
			SendDlgItemMessage(hwndDlg, IDC_SPIN_XOFFSET, UDM_SETACCEL, sizeof(udaccel)/sizeof(UDACCEL), (LPARAM)udaccel);

			SendDlgItemMessage(hwndDlg, IDC_SPIN_YOFFSET, UDM_SETRANGE32, GRIDOFFSETMIN, GRIDOFFSETMAX);
			SendDlgItemMessage(hwndDlg, IDC_SPIN_YOFFSET, UDM_SETACCEL, sizeof(udaccel)/sizeof(UDACCEL), (LPARAM)udaccel);


			/* Add some powers of two to the width/height combos. */
			for(i = FIRSTGRIDLIST; i <= LASTGRIDLIST; i <<= 1)
			{
				TCHAR szNum[16];	/* Big enough for an integer. */
				wsprintf(szNum, TEXT("%d"), i);
				SendDlgItemMessage(hwndDlg, IDC_COMBO_WIDTH, CB_ADDSTRING, 0, (LPARAM)szNum);
				SendDlgItemMessage(hwndDlg, IDC_COMBO_HEIGHT, CB_ADDSTRING, 0, (LPARAM)szNum);
			}


			/* Disable the thing/vertex radio buttons and checkboxes if
			 * necessary.
			 */
			if(s_lpgriddd->bShowGrid) CheckDlgButton(hwndDlg, IDC_CHECK_GRID, BST_CHECKED);
			if(s_lpgriddd->bShow64Grid) CheckDlgButton(hwndDlg, IDC_CHECK_GRID64, BST_CHECKED);
			if(s_lpgriddd->bShowAxes) CheckDlgButton(hwndDlg, IDC_CHECK_AXES, BST_CHECKED);

			/* Set dimensions. */
			SetDlgItemInt(hwndDlg, IDC_COMBO_WIDTH, s_lpgriddd->cx, FALSE);
			SetDlgItemInt(hwndDlg, IDC_COMBO_HEIGHT, s_lpgriddd->cy, FALSE);
			SetDlgItemInt(hwndDlg, IDC_EDIT_XOFFSET, s_lpgriddd->xOffset, TRUE);
			SetDlgItemInt(hwndDlg, IDC_EDIT_YOFFSET, s_lpgriddd->yOffset, TRUE);
		}

		/* Let the system set the focus. */
		return TRUE;

	case WM_COMMAND:
		switch(LOWORD(wParam))
		{
		case IDOK:
			/* Set the structure fields. */
			s_lpgriddd->bShowGrid = (IsDlgButtonChecked(hwndDlg, IDC_CHECK_GRID) == BST_CHECKED);
			s_lpgriddd->bShow64Grid = (IsDlgButtonChecked(hwndDlg, IDC_CHECK_GRID64) == BST_CHECKED);
			s_lpgriddd->bShowAxes = (IsDlgButtonChecked(hwndDlg, IDC_CHECK_AXES) == BST_CHECKED);

			s_lpgriddd->cx = min(GRIDMAX, max(GRIDMIN, GetDlgItemInt(hwndDlg, IDC_COMBO_WIDTH, NULL, FALSE)));
			s_lpgriddd->cy = min(GRIDMAX, max(GRIDMIN, GetDlgItemInt(hwndDlg, IDC_COMBO_HEIGHT, NULL, FALSE)));
			s_lpgriddd->xOffset = min(GRIDOFFSETMAX, max(GRIDOFFSETMIN, (int)GetDlgItemInt(hwndDlg, IDC_EDIT_XOFFSET, NULL, TRUE)));
			s_lpgriddd->yOffset = min(GRIDOFFSETMAX, max(GRIDOFFSETMIN, (int)GetDlgItemInt(hwndDlg, IDC_EDIT_YOFFSET, NULL, TRUE)));

			/* Signal that we OKed. */
			EndDialog(hwndDlg, TRUE);

			return TRUE;

		case IDCANCEL:
			/* Signal that we cancelled. */
			EndDialog(hwndDlg, FALSE);
			return TRUE;

		case IDC_COMBO_WIDTH:
		case IDC_COMBO_HEIGHT:
			if(HIWORD(wParam) == CBN_KILLFOCUS) BoundEditBox((HWND)lParam, GRIDMIN, GRIDMAX, FALSE, FALSE);
			return TRUE;

		case IDC_EDIT_XOFFSET:
		case IDC_EDIT_YOFFSET:
			if(HIWORD(wParam) == EN_KILLFOCUS) BoundEditBox((HWND)lParam, GRIDOFFSETMIN, GRIDOFFSETMAX, FALSE, FALSE);
			return TRUE;
		}

		break;
	}

	/* Didn't process message. */
	return FALSE;
}


/* TestDlg
 *   Displays the "Test Map" dialogue box.
 *
 * Parameters:
 *   HWND		hwndParent	Parent for dialogue.
 *   CONFIG*	lpcfgTest	Testing options config structure.
 *
 * Return value: BOOL
 *   FALSE if cancelled; TRUE otherwise.
 */
BOOL TestDlg(HWND hwndParent, CONFIG *lpcfgTest)
{
	return DialogBoxParam(g_hInstance, MAKEINTRESOURCE(IDD_TEST), hwndParent, TestDlgProc, (LPARAM)lpcfgTest);
}


/* TestDlgProc
 *   Dialogue proc for "Test Map" dialogue box.
 *
 * Parameters:
 *   HWND	hwndDlg		Dialogue window handle.
 *   UINT	uiMsg		Message identifier.
 *   WPARAM wParam		Message-specific.
 *   LPARAM lParam		Message-specific.
 *
 * Return value: INT_PTR
 *   TRUE if the message was processed; FALSE otherwise.
 */
static INT_PTR CALLBACK TestDlgProc(HWND hwndDlg, UINT uiMsg, WPARAM wParam, LPARAM lParam)
{
	static CONFIG *s_lpcfgTest;

	switch(uiMsg)
	{
	case WM_INITDIALOG:
		{
			int i;
			int cch;
			int iCount;
			char cGametype, cDifficulty;

			/* Centre in parent. */
			CentreWindowInParent(hwndDlg);

			/* Get the config in which we return parameters, and which we use
			 * to set initial control states.
			 */
			s_lpcfgTest = (CONFIG*)lParam;


			/* Fill the list boxes. */
			PopulateGametypeList(GetDlgItem(hwndDlg, IDC_COMBO_GAMETYPE));
			PopulateDifficultyList(GetDlgItem(hwndDlg, IDC_COMBO_DIFFICULTY));
			PopulateSkinList(GetDlgItem(hwndDlg, IDC_COMBO_SKIN));


			/* Select the various checkboxes and radio buttons. */

			CheckRadioButton(hwndDlg, IDC_RADIO_SOFTWARE, IDC_RADIO_OPENGL,
				ConfigGetInteger(s_lpcfgTest, TESTCFG_RENDERER) ?
					IDC_RADIO_OPENGL :
					IDC_RADIO_SOFTWARE);

			if(ConfigGetInteger(s_lpcfgTest, TESTCFG_WINDOWED))
				CheckDlgButton(hwndDlg, IDC_CHECK_WINDOWED, BST_CHECKED);

			if(ConfigGetInteger(s_lpcfgTest, TESTCFG_SFX))
				CheckDlgButton(hwndDlg, IDC_CHECK_SFX, BST_CHECKED);

			if(ConfigGetInteger(s_lpcfgTest, TESTCFG_MUSIC))
				CheckDlgButton(hwndDlg, IDC_CHECK_MUSIC, BST_CHECKED);
			else
			{
				/* Disable music type radio buttons if music is disabled. */
				EnableWindow(GetDlgItem(hwndDlg, IDC_RADIO_DIGMUSIC), FALSE);
				EnableWindow(GetDlgItem(hwndDlg, IDC_RADIO_MIDI), FALSE);
			}

			CheckRadioButton(hwndDlg, IDC_RADIO_DIGMUSIC, IDC_RADIO_MIDI,
				ConfigGetInteger(s_lpcfgTest, TESTCFG_MUSICTYPE) ?
					IDC_RADIO_MIDI :
					IDC_RADIO_DIGMUSIC);

			/* Now set the additional parameters. */
			cch = ConfigGetStringLength(s_lpcfgTest, TESTCFG_PARAMS) + 1;
			if(cch > 1)
			{
				LPTSTR szParams = ProcHeapAlloc(cch * sizeof(TCHAR));
				ConfigGetString(s_lpcfgTest, TESTCFG_PARAMS, szParams, cch);
				SetDlgItemText(hwndDlg, IDC_EDIT_PARAM, szParams);
				ProcHeapFree(szParams);
			}

			/* And the skin. Just set the text if specified; otherwise choose
			 * the first list item.
			 */
			cch = ConfigGetStringLength(s_lpcfgTest, TESTCFG_SKIN) + 1;
			if(cch > 1)
			{
				LPTSTR szSkin = ProcHeapAlloc(cch * sizeof(TCHAR));
				ConfigGetString(s_lpcfgTest, TESTCFG_SKIN, szSkin, cch);
				SetDlgItemText(hwndDlg, IDC_COMBO_SKIN, szSkin);
				ProcHeapFree(szSkin);
			}
			else
				SendDlgItemMessage(hwndDlg, IDC_COMBO_SKIN, CB_SETCURSEL, 0, 0);

			/* Find a matching gametype. */
			iCount = SendDlgItemMessage(hwndDlg, IDC_COMBO_GAMETYPE, CB_GETCOUNT, 0, 0);
			cGametype = (char)ConfigGetInteger(s_lpcfgTest, TESTCFG_GAMETYPE);
			for(i = 0; i < iCount; i++)
			{
				char cListGametype = (char)SendDlgItemMessage(hwndDlg, IDC_COMBO_GAMETYPE, CB_GETITEMDATA, i, 0);
				if(cListGametype == cGametype)
				{
					SendDlgItemMessage(hwndDlg, IDC_COMBO_GAMETYPE, CB_SETCURSEL, i, 0);
					break;
				}
			}

			/* Find a matching difficulty. */
			iCount = SendDlgItemMessage(hwndDlg, IDC_COMBO_DIFFICULTY, CB_GETCOUNT, 0, 0);
			cDifficulty = (char)ConfigGetInteger(s_lpcfgTest, TESTCFG_DIFFICULTY);
			for(i = 0; i < iCount; i++)
			{
				char cListDifficulty = (char)SendDlgItemMessage(hwndDlg, IDC_COMBO_DIFFICULTY, CB_GETITEMDATA, i, 0);
				if(cListDifficulty == cDifficulty)
				{
					SendDlgItemMessage(hwndDlg, IDC_COMBO_DIFFICULTY, CB_SETCURSEL, i, 0);
					break;
				}
			}
		}

		/* Let the system set the focus. */
		return TRUE;

	case WM_COMMAND:
		switch(LOWORD(wParam))
		{
		case IDOK:
			{
				int iSelection;
				int cch;
				LPTSTR szSkin, szParams;

				/* Gametype. */
				iSelection = SendDlgItemMessage(hwndDlg, IDC_COMBO_GAMETYPE, CB_GETCURSEL, 0, 0);
				if(iSelection != CB_ERR)
					ConfigSetInteger(s_lpcfgTest, TESTCFG_GAMETYPE, (char)SendDlgItemMessage(hwndDlg, IDC_COMBO_GAMETYPE, CB_GETITEMDATA, iSelection, 0));

				/* Difficulty. */
				iSelection = SendDlgItemMessage(hwndDlg, IDC_COMBO_DIFFICULTY, CB_GETCURSEL, 0, 0);
				if(iSelection != CB_ERR)
					ConfigSetInteger(s_lpcfgTest, TESTCFG_DIFFICULTY, (char)SendDlgItemMessage(hwndDlg, IDC_COMBO_DIFFICULTY, CB_GETITEMDATA, iSelection, 0));

				/* Skin. */
				cch = GetWindowTextLength(GetDlgItem(hwndDlg, IDC_COMBO_SKIN)) + 1;
				szSkin = ProcHeapAlloc(cch * sizeof(TCHAR));
				GetDlgItemText(hwndDlg, IDC_COMBO_SKIN, szSkin, cch);
				ConfigSetString(s_lpcfgTest, TESTCFG_SKIN, szSkin);
				ProcHeapFree(szSkin);

				/* Video options. */
				ConfigSetInteger(s_lpcfgTest, TESTCFG_RENDERER, IsDlgButtonChecked(hwndDlg, IDC_RADIO_OPENGL) != BST_UNCHECKED);
				ConfigSetInteger(s_lpcfgTest, TESTCFG_WINDOWED, IsDlgButtonChecked(hwndDlg, IDC_CHECK_WINDOWED) != BST_UNCHECKED);

				/* Audio options. */
				ConfigSetInteger(s_lpcfgTest, TESTCFG_SFX, IsDlgButtonChecked(hwndDlg, IDC_CHECK_SFX) != BST_UNCHECKED);
				ConfigSetInteger(s_lpcfgTest, TESTCFG_MUSIC, IsDlgButtonChecked(hwndDlg, IDC_CHECK_MUSIC) != BST_UNCHECKED);
				ConfigSetInteger(s_lpcfgTest, TESTCFG_MUSICTYPE, IsDlgButtonChecked(hwndDlg, IDC_RADIO_MIDI) != BST_UNCHECKED);

				/* Parameters. */
				cch = GetWindowTextLength(GetDlgItem(hwndDlg, IDC_EDIT_PARAM)) + 1;
				szParams = ProcHeapAlloc(cch * sizeof(TCHAR));
				GetDlgItemText(hwndDlg, IDC_EDIT_PARAM, szParams, cch);
				ConfigSetString(s_lpcfgTest, TESTCFG_PARAMS, szParams);
				ProcHeapFree(szParams);

				/* Signal that we OKed. */
				EndDialog(hwndDlg, TRUE);
			}

			return TRUE;

		case IDCANCEL:
			/* Signal that we cancelled. */
			EndDialog(hwndDlg, FALSE);
			return TRUE;

		case IDC_CHECK_MUSIC:
			{
				BOOL bEnabled = IsDlgButtonChecked(hwndDlg, IDC_CHECK_MUSIC);
				EnableWindow(GetDlgItem(hwndDlg, IDC_RADIO_DIGMUSIC), bEnabled);
				EnableWindow(GetDlgItem(hwndDlg, IDC_RADIO_MIDI), bEnabled);
			}

			return TRUE;
		}

		break;
	}

	/* Didn't process message. */
	return FALSE;
}


/* PopulateSkinList
 *   Fills a combobox with skins.
 *
 * Parameters:
 *   HWND	hwndCombo	Window handle of combobox.
 *
 * Return value: None.
 */
static void PopulateSkinList(HWND hwndCombo)
{
	USHORT unSkins[] = {IDS_SKIN_SONIC, IDS_SKIN_TAILS, IDS_SKIN_KNUCKLES};

	int i;

	for(i = 0; (unsigned int)i < sizeof(unSkins) / sizeof(USHORT); i++)
	{
		TCHAR szSkin[64];

		LoadString(g_hInstance, unSkins[i], szSkin, sizeof(szSkin) / sizeof(TCHAR));
		SendMessage(hwndCombo, CB_ADDSTRING, 0, (LPARAM)szSkin);
	}
}


/* PopulateDifficultyList
 *   Fills a combobox with difficulties.
 *
 * Parameters:
 *   HWND	hwndCombo	Window handle of combobox.
 *
 * Return value: None.
 */
static void PopulateDifficultyList(HWND hwndCombo)
{
	struct DIFFPAIR {char cDifficulty; USHORT unStringID;} diffpair[] =
	{
		{DIFF_EASY, IDS_DIFFICULTY_EASY},
		{DIFF_NORMAL, IDS_DIFFICULTY_NORMAL},
		{DIFF_HARD, IDS_DIFFICULTY_HARD},
		{DIFF_VERYHARD, IDS_DIFFICULTY_VERYHARD},
		{DIFF_ULTIMATE, IDS_DIFFICULTY_ULTIMATE}
	};

	int i;

	for(i = 0; (unsigned int)i < sizeof(diffpair) / sizeof(struct DIFFPAIR); i++)
	{
		TCHAR szDifficulty[64];
		int iIndex;

		LoadString(g_hInstance, diffpair[i].unStringID, szDifficulty, sizeof(szDifficulty) / sizeof(TCHAR));
		iIndex = SendMessage(hwndCombo, CB_ADDSTRING, 0, (LPARAM)szDifficulty);

		SendMessage(hwndCombo, CB_SETITEMDATA, iIndex, diffpair[i].cDifficulty);
	}
}


/* AddStringToListBoxCallback
 *   Adds a string to a listbox.
 *
 * Parameters:
 *   LPCTSTR	szString	String to add.
 *   void*		lpvWindow	(HWND) Window handle of listbox.
 *
 * Return value: None.
 *
 * Remarks:
 *   Intended for use with IterateMapLumpNames.
 */
void AddStringToListBoxCallback(LPCTSTR szString, void *lpvWindow)
{
	SendMessage((HWND)lpvWindow, LB_ADDSTRING, 0, (LPARAM)szString);
}


/* PropSheetCentreCallback
 *   A property sheet callback function that centres the dialogue in its parent
 *   when it is initialised.
 *
 * Parameters:
 *   HWND		hwndDlg		Handle to property sheet dialogue.
 *   UINT		uiMsg		Message ID.
 *   LPARAM		lParam		Message-specific.
 *
 * Return value: int
 *   Always zero.
 *
 * Remarks:
 *   Intended for use with PropertySheet with the PSP_USECALLBACK flag.
 */
int CALLBACK PropSheetCentreCallback(HWND hwndDlg, UINT uiMsg, LPARAM lParam)
{
	UNREFERENCED_PARAMETER(lParam);

	if(uiMsg == PSCB_INITIALIZED)
		SetWindowLong(hwndDlg, GWL_USERDATA, SetWindowLong(hwndDlg, DWL_DLGPROC, (LONG)PropSheetCentreDlgProc));

	return 0;
}

/* PropSheetCentreDlgProc -- a subclassing dialogue procedure to centre a
 * property sheet when it's created.
 */
static LRESULT WINAPI PropSheetCentreDlgProc(HWND hwndDlg, UINT uiMsg, WPARAM wParam, LPARAM lParam)
{
	if(uiMsg == DM_REPOSITION)
		CentreWindowInParent(hwndDlg);

	return CallWindowProc((WNDPROC)GetWindowLong(hwndDlg, GWL_USERDATA), hwndDlg, uiMsg, wParam, lParam);
}


/* CentreWindowInParent
 *   Centres a window within the client rectangle of its parent.
 *
 * Parameters:
 *   HWND	hwnd	Window to centre.
 *
 * Return value: None.
 */
void CentreWindowInParent(HWND hwnd)
{
	RECT rcWindow, rcParent;
	HWND hwndParent = GetParent(hwnd);

	GetWindowRect(hwnd, &rcWindow);
	GetClientRect(hwndParent, &rcParent);

	/* Convert to screen co-ordinates. */
	MapWindowPoints(hwndParent, HWND_DESKTOP, (LPPOINT)(LPVOID)&rcParent, 2);

	rcWindow.left = rcParent.left + ((rcParent.right  - rcParent.left - (rcWindow.right  - rcWindow.left)) >> 1);
	rcWindow.top  = rcParent.top  + ((rcParent.bottom - rcParent.top  - (rcWindow.bottom - rcWindow.top))  >> 1);

	SetWindowPos(hwnd, NULL, rcWindow.left, rcWindow.top, 0, 0, SWP_NOZORDER | SWP_NOSIZE);
}
