/* SRB2 Workbench -- A map editor for Sonic Robo Blast 2.
 * Copyright (C) 2009 Gregor Dick
 * http://workbench.srb2.org/
 *
 * Incorporates portions of Doom Builder, (C) 2003 Pascal vd Heiden.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 * USA.
 *
 * editdlg.c: Dialogue procedures for various dialogues presented in the course
 * of map-editing, including the property pages for sectors, linedefs etc.
 *
 * AMDG.
 */

#include <windows.h>
#include <commctrl.h>
#include <objbase.h>
#include <shldisp.h>

#include <tchar.h>
#include <stdio.h>

#include "../general.h"
#include "../map.h"
#include "../selection.h"
#include "../config.h"
#include "../options.h"
#include "../editing.h"
#include "../texture.h"
#include "../mapconfig.h"
#include "../preset.h"
#include "../wiki.h"
#include "../wadopts.h"
#include "../texenum.h"
#include "../../res/resource.h"

#include "editdlg.h"
#include "mapwin.h"
#include "mdiframe.h"
#include "infobar.h"
#include "gendlg.h"
#include "texbrowser.h"


#if _WIN32_WINNT < 0x0500
#define IDC_HAND MAKEINTRESOURCE(32649)
#endif

#define EDGESMIN 3
#define EDGESMAX 32767
#define THINGMIN 0
#define THINGMAX 65535
#define EDGESINIT 4
#define RADIUSMIN 0
#define RADIUSMAX 32767

#define NUMTHINGSMIN 3
#define NUMTHINGSMAX 32767
#define NUMTHINGSINIT 4

#define ANGLEMIN ((signed short)0x8000)
#define ANGLEMAX ((signed short)0x7FFF)

#define RESIZEFACTORMIN 10
#define RESIZEINIT 100
#define RESIZEFACTORMAX 1000

#define CURVERADIUSMIN	-32768
#define CURVERADIUSMAX	32768
#define CURVERADIUSINIT	128

#define SEGMENTSMIN		2
#define SEGMENTSMAX		256
#define SEGMENTSINIT	8


/* Types. */
typedef struct _TYPETREEDATA
{
	HWND		hwndTree;
	CONFIG		*lpcfgTypeToNode;
	HTREEITEM	htreeitemParent;
} TYPETREEDATA;

typedef struct _AFTLDATA
{
	HWND	hwndListView;
	WORD	wFlags;
	WORD	wFlagMask;
} AFTLDATA;

typedef struct _SECTOR_RELATIVE
{
	int iFloor, iCeil, iLight;
} SECTOR_RELATIVE;

typedef struct _LINEDEF_RELATIVE
{
	int xFront, yFront, xBack, yBack;
} LINEDEF_RELATIVE;

typedef struct _THING_RELATIVE
{
	int x, y, z, iAngle, iFlags;
} THING_RELATIVE;

typedef struct _FINDSELECTDLGDATA
{
	int		iSelection;
	CONFIG	*lpcfgMap;
	BOOL	bThing;
	int		iParam;
} FINDSELECTDLGDATA;


/* Static prototypes. */
static INT_PTR CALLBACK SectorsPropPageProc(HWND hwndPropPage, UINT uiMsg, WPARAM wParam, LPARAM lParam);
static INT_PTR CALLBACK LinesPropPageProc(HWND hwndPropPage, UINT uiMsg, WPARAM wParam, LPARAM lParam);
static void InitSectorsPropPageFields(HWND hwndPropPage, SECTORDISPLAYINFO *lpsdi, DWORD dwCheckFlags);
static void InitLinesPropPageFields(HWND hwndPropPage, LINEDEFDISPLAYINFO *lplddi, DWORD dwCheckFlags);
static void InitThingsPropPageFields(HWND hwndPropPage, THINGDISPLAYINFO *lpsdi, DWORD dwCheckFlags);
static BOOL AddSectorTypeToList(CONFIG *lpcfg, void *lpvWindow);
static BOOL AddSectorTypeToCombo(CONFIG *lpcfg, void *lpvWindow);
static BOOL AddFlagToList(CONFIG *lpcfg, void *lpvWindow);
static BOOL AddCategoryToTree(CONFIG *lpcfgCategory, void *lpv);
static BOOL AddTypeToTree(CONFIG *lpcfgType, void *lpv);
static void GetFlagsFromListView(HWND hwndListView, WORD *lpwFlagValues, WORD *lpwFlagMask);
static INT_PTR CALLBACK ThingsPropPageProc(HWND hwndPropPage, UINT uiMsg, WPARAM wParam, LPARAM lParam);
static INT_PTR CALLBACK InsertSectorDlgProc(HWND hwndDlg, UINT uiMsg, WPARAM wParam, LPARAM lParam);
static INT_PTR CALLBACK InsertThingDlgProc(HWND hwndDlg, UINT uiMsg, WPARAM wParam, LPARAM lParam);
static INT_PTR CALLBACK RotateDlgProc(HWND hwndDlg, UINT uiMsg, WPARAM wParam, LPARAM lParam);
static INT_PTR CALLBACK ResizeDlgProc(HWND hwndDlg, UINT uiMsg, WPARAM wParam, LPARAM lParam);
static INT_PTR CALLBACK SelectPresetTypesDlgProc(HWND hwndDlg, UINT uiMsg, WPARAM wParam, LPARAM lParam);
static INT_PTR CALLBACK AlignDlgProc(HWND hwndDlg, UINT uiMsg, WPARAM wParam, LPARAM lParam);
static INT_PTR CALLBACK MissingTexProc(HWND hwndDlg, UINT uiMsg, WPARAM wParam, LPARAM lParam);
static INT_PTR CALLBACK SelectSectorEffectDlgProc(HWND hwndDlg, UINT uiMsg, WPARAM wParam, LPARAM lParam);
static INT_PTR CALLBACK SelectThingLDEffectDlgProc(HWND hwndDlg, UINT uiMsg, WPARAM wParam, LPARAM lParam);
static void DoWikiPopup(MAPPPDATA *lpmapppdata, ENUM_WIKIQUERIES wikiquery, int iIndex);
static void DoTreeViewWikiRClick(HWND hwndTreeView, MAPPPDATA *lpmapppdata, ENUM_WIKIQUERIES wikiquery);
static void SetFlagText(HWND hwndList, WORD wFlag, LPCTSTR szText);
static void UpdateZControlsForThing(HWND hwndPropPage, MAPPPDATA *lpmapppdata, unsigned short unType);
static int CALLBACK EffectNumericalSort(LPARAM lParam1, LPARAM lParam2, LPARAM lParamUnused);
static INT_PTR CALLBACK CurveDlgProc(HWND hwndDlg, UINT uiMsg, WPARAM wParam, LPARAM lParam);


/* ShowMapObjectProperties
 *   Shows the Properties dialogue for the selection.
 *
 * Parameters:
 *   DWORD		dwPageFlags		Flags determining which pages to show.
 *   DWORD		dwStartPage		Flag indicating which page to show first. -1 if
 *								the caller doesn't care.
 *   LPCTSTR	szCaption		Window caption, sans TEXT(" Properties").
 *   MAPPPDATA*	lpmapppdata		Selection info and map data.
 *
 * Return value: BOOL
 *   TRUE if the user OKed to exit the dialogue; FALSE if cancelled.
 *
 * Remarks:
 *   All the processing is done by the property sheet pages. This function just
 *   shows the dialogue box.
 */
BOOL ShowMapObjectProperties(DWORD dwPageFlags, DWORD dwStartPage, LPCTSTR szCaption, MAPPPDATA *lpmapppdata)
{
	PROPSHEETHEADER		psh;
	PROPSHEETPAGE		psp[5];
	int					iNumPages, i;

	/* Show the first page initially, unless we were told otherwise. */
	short				nStartPage = 0;


	/* Check which pages were requested, and fill in the necessary info. */

	iNumPages = 0;

	if(dwPageFlags & MPPF_SECTOR)
	{
		psp[iNumPages].pfnDlgProc = SectorsPropPageProc;
		/* TODO: 1.1 */
		psp[iNumPages].pszTemplate = MAKEINTRESOURCE(IDD_PP_SECTORS);

		/* Is this the page to show first? */
		if(dwStartPage == MPPF_SECTOR) nStartPage = iNumPages;

		iNumPages++;
	}

	if(dwPageFlags & MPPF_LINE)
	{
		psp[iNumPages].pfnDlgProc = LinesPropPageProc;
		psp[iNumPages].pszTemplate = MAKEINTRESOURCE(IDD_PP_LINES);

		/* Is this the page to show first? */
		if(dwStartPage == MPPF_LINE) nStartPage = iNumPages;

		iNumPages++;
	}

	if(dwPageFlags & MPPF_THING)
	{
		psp[iNumPages].pfnDlgProc = ThingsPropPageProc;
		psp[iNumPages].pszTemplate = MAKEINTRESOURCE(IDD_PP_THINGS);

		/* Is this the page to show first? */
		if(dwStartPage == MPPF_THING) nStartPage = iNumPages;

		iNumPages++;
	}


	/* Fill in fields common to all pages. */
	for(i = 0; i < iNumPages; i++)
	{
		psp[i].dwSize = sizeof(PROPSHEETPAGE);
		psp[i].dwFlags = /*PSP_HASHELP */ 0;
		psp[i].lParam = (LONG)lpmapppdata;
		psp[i].hInstance = g_hInstance;
	}

	/* Properties affecting the whole dialogue. */
	psh.dwSize = sizeof(psh);
	psh.dwFlags = PSH_HASHELP | PSH_NOAPPLYNOW | PSH_PROPTITLE | PSH_PROPSHEETPAGE | PSH_USECALLBACK;
	psh.hwndParent = g_hwndMain;
	psh.nPages = iNumPages;
	psh.nStartPage = nStartPage;
	psh.ppsp = psp;
	psh.pszCaption = szCaption;
	psh.pfnCallback = PropSheetCentreCallback;

	/* Assume we cancelled. */
	lpmapppdata->bOKed = FALSE;

	/* Go! */
	PropertySheet(&psh);

	/* Did we close by clicking OK? */
	return lpmapppdata->bOKed;
}



/* SectorsPropPageProc
 *   Dialogue proc for sectors property page.
 *
 * Parameters:
 *   HWND	hwndPropPage	Property page window handle.
 *   UINT	uiMsg			Message identifier.
 *   WPARAM wParam			Message-specific.
 *   LPARAM lParam			Message-specific.
 *
 * Return value: INT_PTR
 *   TRUE if the message was processed; FALSE otherwise.
 */
static INT_PTR CALLBACK SectorsPropPageProc(HWND hwndPropPage, UINT uiMsg, WPARAM wParam, LPARAM lParam)
{
	static MAPPPDATA *s_lpmapppdata;
	static BOOL s_bInitComplete, s_bSelectingEffect = FALSE;
	static HBITMAP s_hbmCeil, s_hbmFloor;
	static int s_iLastEffectSelected = -1;
	static IAutoComplete2 *s_lpac2Ceil = NULL, *s_lpac2Floor = NULL;
	static LPTEXENUM s_lptexenumCeil = NULL, s_lptexenumFloor = NULL;
	static BOOL s_bNybble = FALSE;

	switch(uiMsg)
	{
	case WM_INITDIALOG:
		{
			SECTORDISPLAYINFO sdi;
			DWORD dwCheckFlags;
			CONFIG *lpcfgSecTypes;
			UDACCEL udaccelHeight[2], udaccelOther[1];
			int i, cxCol, cxColMax;
			short nMaxAdjCeil, nMinAdjCeil, nMaxAdjFloor, nMinAdjFloor;
			HDC hdcDlg;
			HWND hwndListEffect = GetDlgItem(hwndPropPage, IDC_LIST_EFFECT);
			RECT rcListEffect;
			struct { int iID; LPCTSTR szSubsection; } effectcombos[] =
			{
				{IDC_COMBO_EFFECT_N0, TEXT("nybble0")},
				{IDC_COMBO_EFFECT_N1, TEXT("nybble1")},
				{IDC_COMBO_EFFECT_N2, TEXT("nybble2")},
				{IDC_COMBO_EFFECT_N3, TEXT("nybble3")}
			};

			/* Stop interactions between controls until we're ready. */
			s_bInitComplete = FALSE;

			/* Get the map data. */
			s_lpmapppdata = (MAPPPDATA*)((PROPSHEETPAGE*)lParam)->lParam;

			/* Set the texture previews' bitmaps. */
			hdcDlg = GetDC(hwndPropPage);
			s_hbmCeil = CreateCompatibleBitmap(hdcDlg, CX_TEXPREVIEW, CY_TEXPREVIEW);
			s_hbmFloor = CreateCompatibleBitmap(hdcDlg, CX_TEXPREVIEW, CY_TEXPREVIEW);
			ReleaseDC(hwndPropPage, hdcDlg);
			SendDlgItemMessage(hwndPropPage, IDC_TEX_CEIL, STM_SETIMAGE, IMAGE_BITMAP, (LPARAM)s_hbmCeil);
			SendDlgItemMessage(hwndPropPage, IDC_TEX_FLOOR, STM_SETIMAGE, IMAGE_BITMAP, (LPARAM)s_hbmFloor);

			/* Set the previews to something sensible in case we never touch
			 * them again.
			 */
			SetTexturePreviewImage(s_lpmapppdata->hwndMap, GetDlgItem(hwndPropPage, IDC_TEX_CEIL), TEXT(""), TF_FLAT, s_hbmCeil, FALSE);
			SetTexturePreviewImage(s_lpmapppdata->hwndMap, GetDlgItem(hwndPropPage, IDC_TEX_FLOOR), TEXT(""), TF_FLAT, s_hbmFloor, FALSE);

			/* Subclass the texture preview controls. */
			{
				HWND hwndTexCeil = GetDlgItem(hwndPropPage, IDC_TEX_CEIL);
				HWND hwndTexFloor = GetDlgItem(hwndPropPage, IDC_TEX_FLOOR);

				/* Allocate memory for the preview windows. It's freed when
				 * they're destroyed.
				 */
				TPPDATA *lptppdCeil = ProcHeapAlloc(sizeof(TPPDATA));
				TPPDATA *lptppdFloor = ProcHeapAlloc(sizeof(TPPDATA));

				lptppdCeil->wndprocStatic = (WNDPROC)GetWindowLong(hwndTexCeil, GWL_WNDPROC);
				lptppdFloor->wndprocStatic = (WNDPROC)GetWindowLong(hwndTexFloor, GWL_WNDPROC);

				SetWindowLong(hwndTexCeil, GWL_USERDATA, (LONG)lptppdCeil);
				SetWindowLong(hwndTexFloor, GWL_USERDATA, (LONG)lptppdFloor);

				SetWindowLong(hwndTexCeil, GWL_WNDPROC, (LONG)TexPreviewProc);
				SetWindowLong(hwndTexFloor, GWL_WNDPROC, (LONG)TexPreviewProc);
			}

			if(ConfigGetInteger(g_lpcfgMain, OPT_AUTOCOMPLETETEX))
			{
				/* Create texture enumerators and shell autocomplete objects and
				 * associate them with the edit boxes.
				 */

				if((s_lptexenumCeil = CreateTexEnum()) && (s_lpac2Ceil = CreateAutoComplete()))
				{
					SetTexEnumList(s_lptexenumCeil, s_lpmapppdata->lptnlFlats);
					s_lpac2Ceil->lpVtbl->Init(s_lpac2Ceil, GetDlgItem(hwndPropPage, IDC_EDIT_TCEIL), (IUnknown*)s_lptexenumCeil, NULL, NULL);
				}

				if((s_lptexenumFloor = CreateTexEnum()) && (s_lpac2Floor = CreateAutoComplete()))
				{
					SetTexEnumList(s_lptexenumFloor, s_lpmapppdata->lptnlFlats);
					s_lpac2Floor->lpVtbl->Init(s_lpac2Floor, GetDlgItem(hwndPropPage, IDC_EDIT_TFLOOR), (IUnknown*)s_lptexenumFloor, NULL, NULL);
				}
			}

			/* Sector types from map configuration. */
			lpcfgSecTypes = ConfigGetSubsection(s_lpmapppdata->lpcfgMap, TEXT("sectortypes"));

			/* Do we have several effects packed into our value? */
			if((s_bNybble = ConfigGetInteger(s_lpmapppdata->lpcfgMap, TEXT("seceffectnybble"))))
			{
				/* Fill and show the effect combo boxes. */
				for(i = 0; i < (int)NUM_ELEMENTS(effectcombos); i++)
				{
					HWND hwndCombo = GetDlgItem(hwndPropPage, effectcombos[i].iID);

					/* If we're using numbers, disable text sort. */
					if(ConfigGetInteger(g_lpcfgMain, OPT_EFFECTNUMBERS))
						SetWindowLong(hwndCombo, GWL_STYLE, GetWindowLong(hwndCombo, GWL_STYLE) & ~CBS_SORT);

					ConfigIterate(ConfigGetSubsection(lpcfgSecTypes, effectcombos[i].szSubsection), AddSectorTypeToCombo, hwndCombo);
					ShowWindow(hwndCombo, SW_SHOW);
				}

				/* Disable the list-view. */
				EnableWindow(hwndListEffect, FALSE);
			}
			else
			{
				/* Fill the list of sector effects. */
				ConfigIterate(lpcfgSecTypes, AddSectorTypeToList, hwndListEffect);

				/* Show the list-view. */
				ShowWindow(hwndListEffect, SW_SHOW);

				/* Disable the combo boxes. */
				for(i = 0; i < (int)NUM_ELEMENTS(effectcombos); i++)
					EnableWindow(GetDlgItem(hwndPropPage, effectcombos[i].iID), FALSE);
			}

			/* Find info common to all selected sectors. */
			dwCheckFlags = CheckSectors(
							s_lpmapppdata->lpmap,
							s_lpmapppdata->lpselection->lpsellistSectors,
							lpcfgSecTypes,
							&sdi,
							s_bNybble);

			if(!s_bNybble)
			{
				/* Set up the list-view if we're using it. */

				LVCOLUMN lvcol;

				/* Create a column in the sector effect list view control. */
				lvcol.mask = LVCF_SUBITEM;
				lvcol.iSubItem = 0;
				ListView_InsertColumn(hwndListEffect, 0, &lvcol);

				/* Full row select. */
				ListView_SetExtendedListViewStyleEx(hwndListEffect,
					LVS_EX_FULLROWSELECT | LVS_EX_INFOTIP,
					LVS_EX_FULLROWSELECT | LVS_EX_INFOTIP);

				/* Make the column big enough, but not *too* big. */
				ListView_SetColumnWidth(hwndListEffect, 0, LVSCW_AUTOSIZE);
				cxCol = ListView_GetColumnWidth(hwndListEffect, 0);
				GetClientRect(hwndListEffect, &rcListEffect);
				cxColMax = rcListEffect.right - rcListEffect.left;
				if(cxCol > cxColMax)
					ListView_SetColumnWidth(hwndListEffect, 0, cxColMax);

				/* Sort the list by effect number if that's what the user wants. */
				if(ConfigGetInteger(g_lpcfgMain, OPT_EFFECTNUMBERS))
					ListView_SortItems(hwndListEffect, EffectNumericalSort, 0);
			}

			/* Set ranges and increments on the spinners. */

			udaccelHeight[0].nInc = 8;
			udaccelHeight[0].nSec = 0;
			udaccelHeight[1].nInc = 64;
			udaccelHeight[1].nSec = 2;

			udaccelOther[0].nInc = 1;
			udaccelOther[0].nSec = 0;

			SendDlgItemMessage(hwndPropPage, IDC_SPIN_TAG, UDM_SETRANGE32, 0, 65535);
			SendDlgItemMessage(hwndPropPage, IDC_SPIN_TAG, UDM_SETACCEL, 1, (LPARAM)udaccelOther);

			SendDlgItemMessage(hwndPropPage, IDC_SPIN_HCEIL, UDM_SETRANGE32, -32768, 32767);
			SendDlgItemMessage(hwndPropPage, IDC_SPIN_HCEIL, UDM_SETACCEL, 2, (LPARAM)udaccelHeight);

			SendDlgItemMessage(hwndPropPage, IDC_SPIN_HFLOOR, UDM_SETRANGE32, -32768, 32767);
			SendDlgItemMessage(hwndPropPage, IDC_SPIN_HFLOOR, UDM_SETACCEL, 2, (LPARAM)udaccelHeight);

			/* Add some brightnesses to the combo. */
			for(i=16; i>=0; i--)
			{
				TCHAR szNum[4];
				wsprintf(szNum, TEXT("%d"), min(16*i, 255));
				SendDlgItemMessage(hwndPropPage, IDC_COMBO_LIGHT, CB_ADDSTRING, FALSE, (LPARAM)szNum);
			}


			/* Set up the brightness slider. */
			SendDlgItemMessage(hwndPropPage, IDC_SLIDER_LIGHT, TBM_SETBUDDY, FALSE, (LPARAM)GetDlgItem(hwndPropPage, IDC_COMBO_LIGHT));
			SendDlgItemMessage(hwndPropPage, IDC_SLIDER_LIGHT, TBM_SETTICFREQ, 16, 0);
			SendDlgItemMessage(hwndPropPage, IDC_SLIDER_LIGHT, TBM_SETRANGE, FALSE, MAKELONG(0, 255));

			/* Set fields of the property page. */
			InitSectorsPropPageFields(hwndPropPage, &sdi, dwCheckFlags);

			/* Fill the adjacent sector info. */
			if(GetAdjacentSectorHeightRange(s_lpmapppdata->lpmap, &nMaxAdjCeil, &nMinAdjCeil, &nMaxAdjFloor, &nMinAdjFloor))
			{
				/* We had some adjacent sectors. */
				TCHAR szCeilRange[17], szFloorRange[17];

				if(nMinAdjCeil != nMaxAdjCeil)
					_sntprintf(szCeilRange, sizeof(szCeilRange) / sizeof(TCHAR), TEXT("[%d, %d]"), nMinAdjCeil, nMaxAdjCeil);
				else
					_sntprintf(szCeilRange, sizeof(szCeilRange) / sizeof(TCHAR), TEXT("%d"), nMinAdjCeil);

				if(nMinAdjFloor != nMaxAdjFloor)
					_sntprintf(szFloorRange, sizeof(szFloorRange) / sizeof(TCHAR), TEXT("[%d, %d]"), nMinAdjFloor, nMaxAdjFloor);
				else
					_sntprintf(szFloorRange, sizeof(szFloorRange) / sizeof(TCHAR), TEXT("%d"), nMinAdjFloor);

				SetDlgItemText(hwndPropPage, IDC_STATIC_CEILRANGE, szCeilRange);
				SetDlgItemText(hwndPropPage, IDC_STATIC_FLRRANGE, szFloorRange);
			}
			else
			{
				/* No adjacent sectors. */
				SetDlgItemText(hwndPropPage, IDC_STATIC_CEILRANGE, TEXT("-"));
				SetDlgItemText(hwndPropPage, IDC_STATIC_FLRRANGE, TEXT("-"));
			}
		}

		s_bInitComplete = TRUE;

		/* Let the system set the focus. */
		return TRUE;

	case WM_COMMAND:

		switch(LOWORD(wParam))
		{
		case IDC_BUTTON_NEXTTAG:
			SetDlgItemInt(hwndPropPage, IDC_EDIT_TAG, NextUnusedTag(s_lpmapppdata->lpmap), FALSE);
			return TRUE;

		case IDC_COMBO_LIGHT:
			switch(HIWORD(wParam))
			{
			case CBN_SELCHANGE:
				{
					/* Update the slider to reflect the combo box. */
					int iIndex, cchBuffer;
					LPTSTR sz;

					iIndex = SendDlgItemMessage(hwndPropPage, IDC_COMBO_LIGHT, CB_GETCURSEL, 0, 0);
					cchBuffer = SendDlgItemMessage(hwndPropPage, IDC_COMBO_LIGHT, CB_GETLBTEXTLEN, iIndex, 0) + 1;
					sz = ProcHeapAlloc(cchBuffer * sizeof(TCHAR));

					SendDlgItemMessage(hwndPropPage, IDC_COMBO_LIGHT, CB_GETLBTEXT, iIndex, (LPARAM)sz);
					SendDlgItemMessage(hwndPropPage, IDC_SLIDER_LIGHT, TBM_SETPOS, TRUE, _tcstol(sz, NULL, 10));

					ProcHeapFree(sz);
				}

				return TRUE;

			case CBN_EDITUPDATE:

				/* Update the slider to reflect the combo box. */
				SendDlgItemMessage(hwndPropPage, IDC_SLIDER_LIGHT, TBM_SETPOS, TRUE, min(255, GetDlgItemInt(hwndPropPage, IDC_COMBO_LIGHT, NULL, FALSE)));

				return TRUE;

			case CBN_KILLFOCUS:

				/* Validate. */
				BoundEditBox((HWND)lParam, 0, 255, TRUE, TRUE);

				return TRUE;
			}

			break;

		case IDC_COMBO_EFFECT_N0:
		case IDC_COMBO_EFFECT_N1:
		case IDC_COMBO_EFFECT_N2:
		case IDC_COMBO_EFFECT_N3:
			switch(HIWORD(wParam))
			{
			case CBN_SELCHANGE:
				{
					/* Update the effect edit box. */
					int iEffect = 0;
					int i;
					int iCombos[] =
					{
						IDC_COMBO_EFFECT_N0,
						IDC_COMBO_EFFECT_N1,
						IDC_COMBO_EFFECT_N2,
						IDC_COMBO_EFFECT_N3
					};

					/* Repeat for each combo. */
					for(i = 0; i < (int)NUM_ELEMENTS(iCombos); i++)
					{
						int iIndex = SendDlgItemMessage(hwndPropPage, iCombos[i], CB_GETCURSEL, 0, 0);
						if(iIndex >= 0)
							iEffect |= SendDlgItemMessage(hwndPropPage, iCombos[i], CB_GETITEMDATA, iIndex, 0) << (i << 2);
					}

					SetDlgItemInt(hwndPropPage, IDC_EDIT_EFFECT, iEffect, FALSE);
				}

				return TRUE;
			}

		case IDC_TEX_CEIL:
		case IDC_TEX_FLOOR:

			switch(HIWORD(wParam))
			{
			case BN_CLICKED:
				{
					TCHAR szTexName[TEXNAME_BUFFER_LENGTH];
					UINT uiEditBoxID = (LOWORD(wParam) == IDC_TEX_FLOOR) ? IDC_EDIT_TFLOOR : IDC_EDIT_TCEIL;

					/* Get the current flat so it's selected initially. */
					GetDlgItemText(hwndPropPage, uiEditBoxID, szTexName, TEXNAME_BUFFER_LENGTH);

					/* Show the texture browser. */
					if(SelectTexture(hwndPropPage, s_lpmapppdata->hwndMap, TF_FLAT, s_lpmapppdata->lphimlCacheFlats, s_lpmapppdata->lptnlFlats, s_lpmapppdata->lpcfgUsedFlats, 0, szTexName))
					{
						/* User didn't cancel. Update the edit-box. */
						SetDlgItemText(hwndPropPage, uiEditBoxID, szTexName);
					}
				}

				return TRUE;
			}

			break;

		case IDC_EDIT_EFFECT:
			switch(HIWORD(wParam))
			{
			case EN_CHANGE:
				{
					BOOL bTranslated;
					int iEffect = GetDlgItemInt(hwndPropPage, IDC_EDIT_EFFECT, &bTranslated, FALSE);

					/* Valid number? */
					if(bTranslated)
					{
						if(s_bNybble)
						{
							/* Update the combo boxes. */

							int iNybble, iInCombo;
							const int iCombos[] = {IDC_COMBO_EFFECT_N0, IDC_COMBO_EFFECT_N1, IDC_COMBO_EFFECT_N2, IDC_COMBO_EFFECT_N3};

							for(iNybble = 0; iNybble < (int)NUM_ELEMENTS(iCombos); iNybble++)
							{
								int iItems = SendDlgItemMessage(hwndPropPage, iCombos[iNybble], CB_GETCOUNT, 0, 0);
								BYTE byNybble = (iEffect >> (iNybble << 2)) & 0xF;

								/* Begin by deselecting. */
								SendDlgItemMessage(hwndPropPage, iCombos[iNybble], CB_SETCURSEL, -1, 0);

								for(iInCombo = 0; iInCombo < iItems; iInCombo++)
								{
									/* Do we match? */
									if(byNybble == SendDlgItemMessage(hwndPropPage, iCombos[iNybble], CB_GETITEMDATA, iInCombo, 0))
									{
										/* Select this item. */
										SendDlgItemMessage(hwndPropPage, iCombos[iNybble], CB_SETCURSEL, iInCombo, 0);
										break;
									}
								}
							}
						}
						else
						{
							/* We're using the listview. Select the appropriate
							 * item therein.
							 */

							LVFINDINFO lvfi;
							int iIndex;

							lvfi.flags = LVFI_PARAM;
							lvfi.lParam = iEffect;

							/* Don't cause the listview to set the text when we
							 * change the selection programmatically.
							 */
							s_bSelectingEffect = TRUE;

							/* Select corresponding listview item, or deselect if no
							 * match.
							 */
							iIndex = ListView_FindItem(GetDlgItem(hwndPropPage, IDC_LIST_EFFECT), -1, &lvfi);
							if(iIndex >= 0)
							{
								ListView_SetItemState(GetDlgItem(hwndPropPage, IDC_LIST_EFFECT), iIndex, LVIS_SELECTED, LVIS_SELECTED);
							}
							else if(s_iLastEffectSelected >= 0)
							{
								ListView_SetItemState(GetDlgItem(hwndPropPage, IDC_LIST_EFFECT), iIndex, 0, LVIS_SELECTED);
							}

							s_bSelectingEffect = FALSE;
						}
					}
				}

				return TRUE;

			case EN_KILLFOCUS:
				/* Validation. */
				BoundEditBox(GetDlgItem(hwndPropPage, IDC_EDIT_EFFECT), 0, 65535, TRUE, FALSE);

				return TRUE;
			}

			break;

		case IDC_EDIT_TFLOOR:
		case IDC_EDIT_TCEIL:
			switch(HIWORD(wParam))
			{
			case EN_CHANGE:
				{
					int iTextLength;
					LPTSTR szEditText;

					iTextLength = GetWindowTextLength((HWND)lParam);
					szEditText = ProcHeapAlloc((iTextLength + 1) * sizeof(TCHAR));
					GetWindowText((HWND)lParam, szEditText, iTextLength + 1);

					/* Update the preview image. */
					SetTexturePreviewImage
						(s_lpmapppdata->hwndMap,
						GetDlgItem(hwndPropPage, (LOWORD(wParam) == IDC_EDIT_TCEIL) ? IDC_TEX_CEIL : IDC_TEX_FLOOR),
						szEditText,
						TF_FLAT,
						(LOWORD(wParam) == IDC_EDIT_TCEIL) ? s_hbmCeil : s_hbmFloor,
						TRUE);

					ProcHeapFree(szEditText);
				}

				return TRUE;
			}

			break;

		case IDC_EDIT_HCEIL:
		case IDC_EDIT_HFLOOR:
			switch(HIWORD(wParam))
			{
			case EN_CHANGE:
				{
					BOOL bTranslatedCeil, bTranslatedFloor;
					int iSectorHeight;

					/* Set the 'sector height' static control's text. */
					iSectorHeight = GetDlgItemInt(hwndPropPage, IDC_EDIT_HCEIL, &bTranslatedCeil, TRUE) - GetDlgItemInt(hwndPropPage, IDC_EDIT_HFLOOR, &bTranslatedFloor, TRUE);

					if(bTranslatedCeil && bTranslatedFloor)
						SetDlgItemInt(hwndPropPage, IDC_STATIC_HEIGHT, iSectorHeight, TRUE);
					else
						SetDlgItemText(hwndPropPage, IDC_STATIC_HEIGHT, TEXT(""));
				}

				return TRUE;

			case EN_KILLFOCUS:
				/* Validation. */
				BoundEditBox(GetDlgItem(hwndPropPage, LOWORD(wParam)), -32768, 32767, TRUE, TRUE);

				return TRUE;
			}

		case IDC_EDIT_TAG:
			switch(HIWORD(wParam))
			{
			case EN_KILLFOCUS:
				/* Validation. */
				BoundEditBox(GetDlgItem(hwndPropPage, LOWORD(wParam)), -32768, 32767, TRUE, FALSE);

				return TRUE;
			}
		}

		/* Didn't process the message. */
		break;


	case WM_NOTIFY:

		switch(((LPNMHDR)lParam)->code)
		{
		case PSN_APPLY:
			{
				/* User OK-ed. Set the properties! */

				SECTORDISPLAYINFO sdi;
				SECTOR_RELATIVE secrelative = {0, 0, 0};
				DWORD dwPropertyFlags = 0, dwRelativeFlags = 0;
				TCHAR szBuffer[TEXNAME_BUFFER_LENGTH];

				/* We go through each field, checking whether it's non-empty.
				 * If so, copy its value and set the corresponding flag.
				 */

				/* Floor texture. */
				GetDlgItemText(hwndPropPage, IDC_EDIT_TFLOOR, szBuffer, sizeof(szBuffer)/sizeof(TCHAR));
				if(*szBuffer)
				{
					_tcscpy(sdi.szFloor, szBuffer);
					dwPropertyFlags |= SDIF_FLOORTEX;
				}

				/* Ceiling texture. */
				GetDlgItemText(hwndPropPage, IDC_EDIT_TCEIL, szBuffer, sizeof(szBuffer)/sizeof(TCHAR));
				if(*szBuffer)
				{
					_tcscpy(sdi.szCeiling, szBuffer);
					dwPropertyFlags |= SDIF_CEILINGTEX;
				}

				/* Floor height. */
				GetDlgItemText(hwndPropPage, IDC_EDIT_HFLOOR, szBuffer, sizeof(szBuffer)/sizeof(TCHAR));
				if(*szBuffer)
				{
					if(STRING_RELATIVE(szBuffer))
					{
						secrelative.iFloor = _tcstol(&szBuffer[1], NULL, 10);
						dwRelativeFlags |= SDIF_FLOOR;
					}
					else
					{
						sdi.nFloor = GetDlgItemInt(hwndPropPage, IDC_EDIT_HFLOOR, NULL, TRUE);
						dwPropertyFlags |= SDIF_FLOOR;
					}
				}

				/* Ceiling height. */
				GetDlgItemText(hwndPropPage, IDC_EDIT_HCEIL, szBuffer, sizeof(szBuffer)/sizeof(TCHAR));
				if(*szBuffer)
				{
					if(STRING_RELATIVE(szBuffer))
					{
						secrelative.iCeil = _tcstol(&szBuffer[1], NULL, 10);
						dwRelativeFlags |= SDIF_CEILING;
					}
					else
					{
						sdi.nCeiling = GetDlgItemInt(hwndPropPage, IDC_EDIT_HCEIL, NULL, TRUE);
						dwPropertyFlags |= SDIF_CEILING;
					}
				}

				/* Brightness. */
				GetDlgItemText(hwndPropPage, IDC_COMBO_LIGHT, szBuffer, sizeof(szBuffer)/sizeof(TCHAR));
				if(*szBuffer)
				{
					if(STRING_RELATIVE(szBuffer))
					{
						secrelative.iLight = _tcstol(&szBuffer[1], NULL, 10);
						dwRelativeFlags |= SDIF_BRIGHTNESS;
					}
					else
					{
						sdi.ucBrightness = GetDlgItemInt(hwndPropPage, IDC_COMBO_LIGHT, NULL, FALSE);
						dwPropertyFlags |= SDIF_BRIGHTNESS;
					}
				}

				/* Effect. */
				GetDlgItemText(hwndPropPage, IDC_EDIT_EFFECT, szBuffer, sizeof(szBuffer)/sizeof(TCHAR));
				if(*szBuffer)
				{
					sdi.unEffect = GetDlgItemInt(hwndPropPage, IDC_EDIT_EFFECT, NULL, FALSE);
					dwPropertyFlags |= SDIF_EFFECT;
				}

				/* Tag. */
				GetDlgItemText(hwndPropPage, IDC_EDIT_TAG, szBuffer, sizeof(szBuffer)/sizeof(TCHAR));
				if(*szBuffer)
				{
					sdi.unTag = GetDlgItemInt(hwndPropPage, IDC_EDIT_TAG, NULL, FALSE);
					dwPropertyFlags |= SDIF_TAG;
				}

				/* We're making changes. */
				s_lpmapppdata->bOKed = TRUE;

				/* Apply the relative properties. */
				if(dwRelativeFlags & SDIF_FLOOR)
					ApplyRelativeFloorHeightSelection(s_lpmapppdata->lpmap, s_lpmapppdata->lpselection->lpsellistSectors, secrelative.iFloor);

				if(dwRelativeFlags & SDIF_CEILING)
					ApplyRelativeCeilingHeightSelection(s_lpmapppdata->lpmap, s_lpmapppdata->lpselection->lpsellistSectors, secrelative.iCeil);

				if(dwRelativeFlags & SDIF_BRIGHTNESS)
					ApplyRelativeBrightnessSelection(s_lpmapppdata->lpmap, s_lpmapppdata->lpselection->lpsellistSectors, (short)secrelative.iLight);

				/* We've got all the properties now. Apply them. */
				ApplySectorPropertiesToSelection(s_lpmapppdata->lpmap, s_lpmapppdata->lpselection->lpsellistSectors, &sdi, dwPropertyFlags);

				/* Update the default textures if necessary. */
				if(SendDlgItemMessage(hwndPropPage, IDC_CHECK_SAVEDEFAULT, BM_GETCHECK, 0, 0) == BST_CHECKED)
				{
					CONFIG *lpcfgDefSec = ConfigGetSubsection(s_lpmapppdata->lpcfgWadOptMap, WADOPT_DEFAULTSEC);

					/* Set the values, using the ones we've already read
					 * from the controls.
					 */
					ConfigSetString(lpcfgDefSec, TEXT("tceiling"), sdi.szCeiling);
					ConfigSetString(lpcfgDefSec, TEXT("tfloor"), sdi.szFloor);
				}

				/* Finished! */
			}

			SetWindowLong(hwndPropPage, DWL_MSGRESULT, PSNRET_NOERROR);
			return TRUE;
		}

		/* Still WM_NOTIFY... */

		switch(((LPNMHDR)lParam)->idFrom)
		{
		case IDC_LIST_EFFECT:
			{
				LPNMLISTVIEW lpnmlistview = (LPNMLISTVIEW)lParam;

				switch(lpnmlistview->hdr.code)
				{
				case LVN_ITEMCHANGED:
					if((lpnmlistview->uChanged & LVIF_STATE) && (lpnmlistview->uNewState & LVIS_SELECTED))
					{
						/* Selection should always be visible. */
						ListView_EnsureVisible(lpnmlistview->hdr.hwndFrom, lpnmlistview->iItem, FALSE);

						/* Update the edit box unless the selection was only
						 * changed *because* the edit box changed.
						 */
						if(s_bInitComplete && !s_bSelectingEffect)
						{
							LVITEM lvitem;

							lvitem.mask = LVIF_PARAM;
							lvitem.iItem = lpnmlistview->iItem;
							ListView_GetItem(lpnmlistview->hdr.hwndFrom, &lvitem);

							SetDlgItemInt(hwndPropPage, IDC_EDIT_EFFECT, lvitem.lParam, FALSE);
						}

						/* Remember this item so that we can deselect it if
						 * necessary.
						 */
						s_iLastEffectSelected = lpnmlistview->iItem;
					}

					return TRUE;

				case NM_RCLICK:
					if(lpnmlistview->iItem >= 0)
					{
						/* Get the item data for the clicked item. */
						LVITEM lvitem;

						lvitem.mask = LVIF_PARAM;
						lvitem.iItem = lpnmlistview->iItem;
						ListView_GetItem(lpnmlistview->hdr.hwndFrom, &lvitem);

						DoWikiPopup(s_lpmapppdata, WIKIQUERY_SECEFFECT, lvitem.lParam);
					}

					return TRUE;
				}
			}

			/* Didn't process the message. */
			break;
		}

		/* Didn't process the message. */
		break;

	case WM_HSCROLL:
		/* We've only got the one slider. */

		SetDlgItemInt(hwndPropPage, IDC_COMBO_LIGHT, SendDlgItemMessage(hwndPropPage, IDC_SLIDER_LIGHT, TBM_GETPOS, 0, 0), FALSE);
		return TRUE;

	case WM_DESTROY:

		/* Destroy the preview bitmaps. */
		DeleteObject(s_hbmCeil);
		DeleteObject(s_hbmFloor);

		if(s_lpac2Ceil) s_lpac2Ceil->lpVtbl->Release(s_lpac2Ceil);
		if(s_lpac2Floor) s_lpac2Floor->lpVtbl->Release(s_lpac2Floor);

		if(s_lptexenumCeil) DestroyTexEnum(s_lptexenumCeil);
		if(s_lptexenumFloor) DestroyTexEnum(s_lptexenumFloor);

		return TRUE;
	}

	/* Didn't process message. */
	return FALSE;
}


/* LinesPropPageProc
 *   Dialogue proc for lines property page.
 *
 * Parameters:
 *   HWND	hwndPropPage	Property page window handle.
 *   UINT	uiMsg			Message identifier.
 *   WPARAM wParam			Message-specific.
 *   LPARAM lParam			Message-specific.
 *
 * Return value: INT_PTR
 *   TRUE if the message was processed; FALSE otherwise.
 */
static INT_PTR CALLBACK LinesPropPageProc(HWND hwndPropPage, UINT uiMsg, WPARAM wParam, LPARAM lParam)
{
	static MAPPPDATA *s_lpmapppdata;
	static CONFIG *s_lpcfgTypeToNode;
	static HBITMAP s_hbmTexPreviews[6];
	static BOOL s_bHasFrontSec, s_bHasBackSec;
	static BYTE s_byTexRequirementFlags;
	static IAutoComplete2 *s_lpac2[6] = {NULL};
	static LPTEXENUM s_lptexenum[6] = {NULL};

	switch(uiMsg)
	{
	case WM_INITDIALOG:
		{
			CONFIG *lpcfgLinedefFlags;
			CONFIG *lpcfgLDEffects;
			TYPETREEDATA ttd;
			LVCOLUMN lvcol;
			UDACCEL udaccelOffsets[2], udaccelTag[1];
			HWND hwndListFlags = GetDlgItem(hwndPropPage, IDC_LIST_FLAGS);
			DWORD dwCheckFlags;
			LINEDEFDISPLAYINFO lddi;
			int i;
			HDC hdcDlg;

			const int iOffsetSpinIDs[] = {IDC_SPIN_FRONT_OFFX, IDC_SPIN_FRONT_OFFY, IDC_SPIN_BACK_OFFX, IDC_SPIN_BACK_OFFY};
			const int iTexPreviewIDs[] = {	IDC_TEX_FRONT_UPPER, IDC_TEX_FRONT_MIDDLE, IDC_TEX_FRONT_LOWER,
											IDC_TEX_BACK_UPPER, IDC_TEX_BACK_MIDDLE, IDC_TEX_BACK_LOWER};
			const int iEditIDs[] = {	IDC_EDIT_FRONT_UPPER, IDC_EDIT_FRONT_MIDDLE, IDC_EDIT_FRONT_LOWER,
										IDC_EDIT_BACK_UPPER, IDC_EDIT_BACK_MIDDLE, IDC_EDIT_BACK_LOWER};

			/* Get the map data. */
			s_lpmapppdata = (MAPPPDATA*)((PROPSHEETPAGE*)lParam)->lParam;

			/* Initialise the flag list. */
			lvcol.mask = LVCF_SUBITEM;
			lvcol.iSubItem = 0;
			ListView_InsertColumn(hwndListFlags, 0, &lvcol);
			Init3StateListView(hwndListFlags);

			/* Get the possible linedef flag values. */
			lpcfgLinedefFlags = ConfigGetSubsection(s_lpmapppdata->lpcfgMap, TEXT("linedefflags"));

			/* Add them to the list, with their initial values. */
			if(lpcfgLinedefFlags)
			{
				AFTLDATA aftldata;
				aftldata.hwndListView = hwndListFlags;
				CheckLineFlags(s_lpmapppdata->lpmap, s_lpmapppdata->lpselection->lpsellistLinedefs, &aftldata.wFlags, &aftldata.wFlagMask);
				ConfigIterate(lpcfgLinedefFlags, AddFlagToList, &aftldata);
			}

			/* Make the column big enough. */
			ListView_SetColumnWidth(hwndListFlags, 0, LVSCW_AUTOSIZE);

			/* Create an empty effect-to-node lookup table. */
			s_lpcfgTypeToNode = ConfigCreate();

			/* Populate the effect tree and corresponding lookup table. */
			lpcfgLDEffects = ConfigGetSubsection(s_lpmapppdata->lpcfgMap, TEXT("linedeftypes"));
			ttd.hwndTree = GetDlgItem(hwndPropPage, IDC_TREE_EFFECT);
			ttd.lpcfgTypeToNode = s_lpcfgTypeToNode;
			ConfigIterate(lpcfgLDEffects, AddCategoryToTree, &ttd);

			/* Subclass the texture preview controls and create their preview
			 * bitmaps.
			 */
			hdcDlg = GetDC(hwndPropPage);
			for(i = 0; i < 6; i++)
			{
				HWND hwndTex = GetDlgItem(hwndPropPage, iTexPreviewIDs[i]);

				/* Allocate memory for the preview window. It's freed when the
				 * window's destroyed.
				 */
				TPPDATA *lptppd = ProcHeapAlloc(sizeof(TPPDATA));

				lptppd->wndprocStatic = (WNDPROC)GetWindowLong(hwndTex, GWL_WNDPROC);

				SetWindowLong(hwndTex, GWL_USERDATA, (LONG)lptppd);
				SetWindowLong(hwndTex, GWL_WNDPROC, (LONG)TexPreviewProc);

				/* Create preview bitmap. */
				s_hbmTexPreviews[i] = CreateCompatibleBitmap(hdcDlg, CX_TEXPREVIEW, CY_TEXPREVIEW);
				SendDlgItemMessage(hwndPropPage, iTexPreviewIDs[i], STM_SETIMAGE, IMAGE_BITMAP, (LPARAM)s_hbmTexPreviews[i]);

				/* Set the preview to something sensible in case we never touch
				 * it again.
				 */
				SetTexturePreviewImage(s_lpmapppdata->hwndMap, hwndTex, TEXT(""), TF_TEXTURE, s_hbmTexPreviews[i], FALSE);
			}

			ReleaseDC(hwndPropPage, hdcDlg);

			if(ConfigGetInteger(g_lpcfgMain, OPT_AUTOCOMPLETETEX))
			{
				/* Create texture enumerators and shell autocomplete objects and
				 * associate them with the edit boxes.
				 */
				for(i = 0; i < 6; i++)
				{
					if((s_lptexenum[i] = CreateTexEnum()) && (s_lpac2[i] = CreateAutoComplete()))
					{
						SetTexEnumList(s_lptexenum[i], s_lpmapppdata->lptnlTextures);
						s_lpac2[i]->lpVtbl->Init(s_lpac2[i], GetDlgItem(hwndPropPage, iEditIDs[i]), (IUnknown*)s_lptexenum[i], NULL, NULL);
					}
				}
			}

			/* Set up the spinner controls. */

			udaccelTag[0].nInc = 1;
			udaccelTag[0].nSec = 0;

			udaccelOffsets[0].nInc = 1;
			udaccelOffsets[0].nSec = 0;
			udaccelOffsets[1].nInc = 8;
			udaccelOffsets[1].nSec = 2;

			SendDlgItemMessage(hwndPropPage, IDC_SPIN_TAG, UDM_SETRANGE32, 0, 65535);
			SendDlgItemMessage(hwndPropPage, IDC_SPIN_TAG, UDM_SETACCEL, 1, (LPARAM)udaccelTag);

			/* This sets all the offset spinners' ranges and speeds. */
			for(i = 0; i < 4; i++)
			{
				SendDlgItemMessage(hwndPropPage, iOffsetSpinIDs[i], UDM_SETRANGE32, -32768, 32767);
				SendDlgItemMessage(hwndPropPage, iOffsetSpinIDs[i], UDM_SETACCEL, 2, (LPARAM)udaccelOffsets);
			}


			/* Initialise the controls to the values from the selection. */

			dwCheckFlags = CheckLines(
							s_lpmapppdata->lpmap,
							s_lpmapppdata->lpselection->lpsellistLinedefs,
							lpcfgLDEffects,
							&lddi);

			/* These will be recalculated every time the tab is activated, since
			 * the user might have made changes on the Sectors tab.
			 */
			s_byTexRequirementFlags = CheckTextureFlags(s_lpmapppdata->lpmap, s_lpmapppdata->lpselection->lpsellistLinedefs, s_lpmapppdata->szSky);

			s_bHasFrontSec = (dwCheckFlags & LDDIF_HASFRONT) && lddi.bHasFront;
			s_bHasBackSec = (dwCheckFlags & LDDIF_HASBACK) && lddi.bHasBack;

			InitLinesPropPageFields(hwndPropPage, &lddi, dwCheckFlags);
		}

		/* Let the system set the focus. */
		return TRUE;

	case WM_COMMAND:

		switch(LOWORD(wParam))
		{
		case IDC_BUTTON_LINE_NEXTTAG:
			SetDlgItemInt(hwndPropPage, IDC_EDIT_TAG, NextUnusedTag(s_lpmapppdata->lpmap), FALSE);
			return TRUE;

		case IDC_EDIT_EFFECT:
			switch(HIWORD(wParam))
			{
			case EN_CHANGE:
				{
					BOOL bTranslated;
					int iEffect = GetDlgItemInt(hwndPropPage, IDC_EDIT_EFFECT, &bTranslated, FALSE);

					/* Valid number? */
					if(bTranslated && iEffect >= 0)
					{
						/* Select corresponding tree item. */
						TCHAR szValue[CCH_SIGNED_INT];
						_sntprintf(szValue, NUM_ELEMENTS(szValue), TEXT("%d"), iEffect);

						if(ConfigNodeExists(s_lpcfgTypeToNode, szValue))
							TreeView_SelectItem(GetDlgItem(hwndPropPage, IDC_TREE_EFFECT), (HTREEITEM)ConfigGetInteger(s_lpcfgTypeToNode, szValue));
						else
							TreeView_SelectItem(GetDlgItem(hwndPropPage, IDC_TREE_EFFECT), NULL);
					}
				}

				return TRUE;

			case EN_KILLFOCUS:
				/* Validation. */
				BoundEditBox(GetDlgItem(hwndPropPage, IDC_EDIT_EFFECT), 0, 65535, TRUE, FALSE);

				return TRUE;
			}

			break;

		case IDC_EDIT_TAG:
		case IDC_EDIT_FRONT_SECTOR:
		case IDC_EDIT_BACK_SECTOR:
			if(HIWORD(wParam) == EN_KILLFOCUS)
			{
				/* Validation. */
				BoundEditBox((HWND)lParam, 0, 65535, TRUE, FALSE);
				return TRUE;
			}

			break;

		case IDC_EDIT_FRONT_OFFX:
		case IDC_EDIT_FRONT_OFFY:
		case IDC_EDIT_BACK_OFFX:
		case IDC_EDIT_BACK_OFFY:
			if(HIWORD(wParam) == EN_KILLFOCUS)
			{
				/* Validation. */
				BoundEditBox((HWND)lParam, -32768, 32767, TRUE, TRUE);
				return TRUE;
			}

			break;

		case IDC_TEX_FRONT_UPPER:
		case IDC_TEX_FRONT_MIDDLE:
		case IDC_TEX_FRONT_LOWER:
		case IDC_TEX_BACK_UPPER:
		case IDC_TEX_BACK_MIDDLE:
		case IDC_TEX_BACK_LOWER:

			switch(HIWORD(wParam))
			{
			case BN_CLICKED:
				{
					TCHAR szTexName[TEXNAME_BUFFER_LENGTH];
					UINT uiEditBoxID = 0;

					switch(LOWORD(wParam))
					{
						case IDC_TEX_FRONT_UPPER:	uiEditBoxID = IDC_EDIT_FRONT_UPPER;		break;
						case IDC_TEX_FRONT_MIDDLE:	uiEditBoxID = IDC_EDIT_FRONT_MIDDLE;	break;
						case IDC_TEX_FRONT_LOWER:	uiEditBoxID = IDC_EDIT_FRONT_LOWER;		break;
						case IDC_TEX_BACK_UPPER:	uiEditBoxID = IDC_EDIT_BACK_UPPER;		break;
						case IDC_TEX_BACK_MIDDLE:	uiEditBoxID = IDC_EDIT_BACK_MIDDLE;		break;
						case IDC_TEX_BACK_LOWER:	uiEditBoxID = IDC_EDIT_BACK_LOWER;		break;
					}

					/* Get the current flat so it's selected initially. */
					GetDlgItemText(hwndPropPage, uiEditBoxID, szTexName, TEXNAME_BUFFER_LENGTH);

					/* Show the texture browser. */
					if(SelectTexture(hwndPropPage, s_lpmapppdata->hwndMap, TF_TEXTURE, s_lpmapppdata->lphimlCacheTextures, s_lpmapppdata->lptnlTextures, s_lpmapppdata->lpcfgUsedTextures, 0, szTexName))
					{
						/* User didn't cancel. Update the edit-box. */
						SetDlgItemText(hwndPropPage, uiEditBoxID, szTexName);
					}
				}

				return TRUE;
			}

			break;

		case IDC_EDIT_FRONT_UPPER:
		case IDC_EDIT_FRONT_MIDDLE:
		case IDC_EDIT_FRONT_LOWER:
		case IDC_EDIT_BACK_UPPER:
		case IDC_EDIT_BACK_MIDDLE:
		case IDC_EDIT_BACK_LOWER:

			switch(HIWORD(wParam))
			{
			case EN_CHANGE:
				{
					UINT uiPreviewID = 0;
					HWND hwndPreview = NULL;
					HBITMAP hbmPreview = NULL;
					int iTextLength;
					LPTSTR szEditText;
					BOOL bTexRequired = FALSE;

					/* Get preview control handle and bitmap corresponding to
					 * the edit box.
					 */
					switch(LOWORD(wParam))
					{
					case IDC_EDIT_FRONT_UPPER:
						uiPreviewID = IDC_TEX_FRONT_UPPER;
						hbmPreview = s_hbmTexPreviews[0];
						bTexRequired = s_byTexRequirementFlags & LDTF_FRONTUPPER;
						break;
					case IDC_EDIT_FRONT_MIDDLE:
						uiPreviewID = IDC_TEX_FRONT_MIDDLE;
						hbmPreview = s_hbmTexPreviews[1];
						bTexRequired = s_byTexRequirementFlags & LDTF_FRONTMIDDLE;
						break;
					case IDC_EDIT_FRONT_LOWER:
						uiPreviewID = IDC_TEX_FRONT_LOWER;
						hbmPreview = s_hbmTexPreviews[2];
						bTexRequired = s_byTexRequirementFlags & LDTF_FRONTLOWER;
						break;
					case IDC_EDIT_BACK_UPPER:
						uiPreviewID = IDC_TEX_BACK_UPPER;
						hbmPreview = s_hbmTexPreviews[3];
						bTexRequired = s_byTexRequirementFlags & LDTF_BACKUPPER;
						break;
					case IDC_EDIT_BACK_MIDDLE:
						uiPreviewID = IDC_TEX_BACK_MIDDLE;
						hbmPreview = s_hbmTexPreviews[4];
						bTexRequired = s_byTexRequirementFlags & LDTF_BACKMIDDLE;
						break;
					case IDC_EDIT_BACK_LOWER:
						uiPreviewID = IDC_TEX_BACK_LOWER;
						hbmPreview = s_hbmTexPreviews[5];
						bTexRequired = s_byTexRequirementFlags & LDTF_BACKLOWER;
						break;
					}

					hwndPreview = GetDlgItem(hwndPropPage, uiPreviewID);


					/* Get the texture name. */
					iTextLength = GetWindowTextLength((HWND)lParam);
					szEditText = ProcHeapAlloc((iTextLength + 1) * sizeof(TCHAR));
					GetWindowText((HWND)lParam, szEditText, iTextLength + 1);

					/* Update the texture preview image. */
					SetTexturePreviewImage
						(s_lpmapppdata->hwndMap,
						 hwndPreview,
						 szEditText,
						 TF_TEXTURE,
						 hbmPreview,
						 bTexRequired);

					/* Free the buffer used to store the texture name. */
					ProcHeapFree(szEditText);
				}

				return TRUE;
			}

			break;

		case IDC_CHECK_FRONT_SAVEDEFAULT:
		case IDC_CHECK_BACK_SAVEDEFAULT:
			/* Enable/disable the opposing checkbox, as appropriate. */
			if(HIWORD(wParam) == BN_CLICKED)
				EnableWindow(GetDlgItem(hwndPropPage, LOWORD(wParam) == IDC_CHECK_BACK_SAVEDEFAULT ? IDC_CHECK_FRONT_SAVEDEFAULT : IDC_CHECK_BACK_SAVEDEFAULT), s_bHasBackSec && SendMessage((HWND)lParam, BM_GETCHECK, 0, 0) != BST_CHECKED);
			break;
		}

		/* Didn't process message. */
		break;

	case WM_NOTIFY:
		{
			LPNMHDR lpnmhdr = (LPNMHDR)lParam;

			/* Special case: notifications from the property sheet don't set an
			 * ID.
			 */
			switch(lpnmhdr->code)
			{
			case PSN_APPLY:
				{
					/* User OK-ed. Set the properties! */

					LINEDEFDISPLAYINFO lddi;
					LINEDEF_RELATIVE ldrelative = {0, 0, 0, 0};
					DWORD dwPropertyFlags = 0, dwRelativeFlags = 0;
					TCHAR szBuffer[TEXNAME_BUFFER_LENGTH];
					WORD wFlags, wFlagMask;

					/* We go through each field, checking whether it's non-empty.
					 * If so, copy its value and set the corresponding flag.
					 */

					/* Effect. */
					GetDlgItemText(hwndPropPage, IDC_EDIT_EFFECT, szBuffer, sizeof(szBuffer)/sizeof(TCHAR));
					if(*szBuffer)
					{
						lddi.unEffect = GetDlgItemInt(hwndPropPage, IDC_EDIT_EFFECT, NULL, FALSE);
						dwPropertyFlags |= LDDIF_EFFECT;
					}

					/* Tag. */
					GetDlgItemText(hwndPropPage, IDC_EDIT_TAG, szBuffer, sizeof(szBuffer)/sizeof(TCHAR));
					if(*szBuffer)
					{
						lddi.unTag = GetDlgItemInt(hwndPropPage, IDC_EDIT_TAG, NULL, FALSE);
						dwPropertyFlags |= LDDIF_TAG;
					}

					/* Flags. */
					GetFlagsFromListView(GetDlgItem(hwndPropPage, IDC_LIST_FLAGS), &wFlags, &wFlagMask);

					/* Front sector. */
					if(s_bHasFrontSec)
					{
						/* X Offset. */
						GetDlgItemText(hwndPropPage, IDC_EDIT_FRONT_OFFX, szBuffer, sizeof(szBuffer)/sizeof(TCHAR));
						if(*szBuffer)
						{
							if(STRING_RELATIVE(szBuffer))
							{
								ldrelative.xFront = _tcstol(&szBuffer[1], NULL, 10);
								dwRelativeFlags |= LDDIF_FRONTX;
							}
							else
							{
								lddi.nFrontX = GetDlgItemInt(hwndPropPage, IDC_EDIT_FRONT_OFFX, NULL, TRUE);
								dwPropertyFlags |= LDDIF_FRONTX;
							}
						}

						/* Y Offset. */
						GetDlgItemText(hwndPropPage, IDC_EDIT_FRONT_OFFY, szBuffer, sizeof(szBuffer)/sizeof(TCHAR));
						if(*szBuffer)
						{
							if(STRING_RELATIVE(szBuffer))
							{
								ldrelative.yFront = _tcstol(&szBuffer[1], NULL, 10);
								dwRelativeFlags |= LDDIF_FRONTY;
							}
							else
							{
								lddi.nFrontY = GetDlgItemInt(hwndPropPage, IDC_EDIT_FRONT_OFFY, NULL, TRUE);
								dwPropertyFlags |= LDDIF_FRONTY;
							}
						}

						/* Sector. */
						GetDlgItemText(hwndPropPage, IDC_EDIT_FRONT_SECTOR, szBuffer, sizeof(szBuffer)/sizeof(TCHAR));
						if(*szBuffer)
						{
							lddi.unFrontSector = GetDlgItemInt(hwndPropPage, IDC_EDIT_FRONT_SECTOR, NULL, FALSE);
							dwPropertyFlags |= LDDIF_FRONTSEC;
						}

						/* Upper texture. */
						GetDlgItemText(hwndPropPage, IDC_EDIT_FRONT_UPPER, szBuffer, sizeof(szBuffer)/sizeof(TCHAR));
						if(*szBuffer)
						{
							_tcscpy(lddi.szFrontUpper, szBuffer);
							dwPropertyFlags |= LDDIF_FRONTUPPER;
						}

						/* Middle texture. */
						GetDlgItemText(hwndPropPage, IDC_EDIT_FRONT_MIDDLE, szBuffer, sizeof(szBuffer)/sizeof(TCHAR));
						if(*szBuffer)
						{
							_tcscpy(lddi.szFrontMiddle, szBuffer);
							dwPropertyFlags |= LDDIF_FRONTMIDDLE;
						}

						/* Lower texture. */
						GetDlgItemText(hwndPropPage, IDC_EDIT_FRONT_LOWER, szBuffer, sizeof(szBuffer)/sizeof(TCHAR));
						if(*szBuffer)
						{
							_tcscpy(lddi.szFrontLower, szBuffer);
							dwPropertyFlags |= LDDIF_FRONTLOWER;
						}
					}

					/* Back sector. */
					if(s_bHasBackSec)
					{
						/* X Offset. */
						GetDlgItemText(hwndPropPage, IDC_EDIT_BACK_OFFX, szBuffer, sizeof(szBuffer)/sizeof(TCHAR));
						if(*szBuffer)
						{
							if(STRING_RELATIVE(szBuffer))
							{
								ldrelative.xBack = _tcstol(&szBuffer[1], NULL, 10);
								dwRelativeFlags |= LDDIF_BACKX;
							}
							else
							{
								lddi.nBackX = GetDlgItemInt(hwndPropPage, IDC_EDIT_BACK_OFFX, NULL, TRUE);
								dwPropertyFlags |= LDDIF_BACKX;
							}
						}

						/* Y Offset. */
						GetDlgItemText(hwndPropPage, IDC_EDIT_BACK_OFFY, szBuffer, sizeof(szBuffer)/sizeof(TCHAR));
						if(*szBuffer)
						{
							if(STRING_RELATIVE(szBuffer))
							{
								ldrelative.yBack = _tcstol(&szBuffer[1], NULL, 10);
								dwRelativeFlags |= LDDIF_BACKY;
							}
							else
							{
								lddi.nBackY = GetDlgItemInt(hwndPropPage, IDC_EDIT_BACK_OFFY, NULL, TRUE);
								dwPropertyFlags |= LDDIF_BACKY;
							}
						}

						/* Sector. */
						GetDlgItemText(hwndPropPage, IDC_EDIT_BACK_SECTOR, szBuffer, sizeof(szBuffer)/sizeof(TCHAR));
						if(*szBuffer)
						{
							lddi.unBackSector = GetDlgItemInt(hwndPropPage, IDC_EDIT_BACK_SECTOR, NULL, FALSE);
							dwPropertyFlags |= LDDIF_BACKSEC;
						}

						/* Upper texture. */
						GetDlgItemText(hwndPropPage, IDC_EDIT_BACK_UPPER, szBuffer, sizeof(szBuffer)/sizeof(TCHAR));
						if(*szBuffer)
						{
							_tcscpy(lddi.szBackUpper, szBuffer);
							dwPropertyFlags |= LDDIF_BACKUPPER;
						}

						/* Middle texture. */
						GetDlgItemText(hwndPropPage, IDC_EDIT_BACK_MIDDLE, szBuffer, sizeof(szBuffer)/sizeof(TCHAR));
						if(*szBuffer)
						{
							_tcscpy(lddi.szBackMiddle, szBuffer);
							dwPropertyFlags |= LDDIF_BACKMIDDLE;
						}

						/* Lower texture. */
						GetDlgItemText(hwndPropPage, IDC_EDIT_BACK_LOWER, szBuffer, sizeof(szBuffer)/sizeof(TCHAR));
						if(*szBuffer)
						{
							_tcscpy(lddi.szBackLower, szBuffer);
							dwPropertyFlags |= LDDIF_BACKLOWER;
						}
					}

					/* We're making changes. */
					s_lpmapppdata->bOKed = TRUE;

					/* Apply the relative properties. */
					if(dwRelativeFlags & LDDIF_FRONTX)
						ApplyRelativeFrontSidedefXSelection(s_lpmapppdata->lpmap, s_lpmapppdata->lpselection->lpsellistLinedefs, ldrelative.xFront);

					if(dwRelativeFlags & LDDIF_FRONTY)
						ApplyRelativeFrontSidedefYSelection(s_lpmapppdata->lpmap, s_lpmapppdata->lpselection->lpsellistLinedefs, ldrelative.yFront);

					if(dwRelativeFlags & LDDIF_BACKX)
						ApplyRelativeBackSidedefXSelection(s_lpmapppdata->lpmap, s_lpmapppdata->lpselection->lpsellistLinedefs, ldrelative.xBack);

					if(dwRelativeFlags & LDDIF_BACKY)
						ApplyRelativeBackSidedefYSelection(s_lpmapppdata->lpmap, s_lpmapppdata->lpselection->lpsellistLinedefs, ldrelative.yBack);

					/* We've got all the properties now. Apply them. */
					ApplyLinePropertiesToSelection(s_lpmapppdata->lpmap, s_lpmapppdata->lpselection->lpsellistLinedefs, &lddi, dwPropertyFlags);
					SetLineFlags(s_lpmapppdata->lpmap, s_lpmapppdata->lpselection->lpsellistLinedefs, wFlags, wFlagMask);


					/* Update the default textures if necessary. */
					if(SendDlgItemMessage(hwndPropPage, IDC_CHECK_FRONT_SAVEDEFAULT, BM_GETCHECK, 0, 0) == BST_CHECKED)
					{
						CONFIG *lpcfgDefTex = ConfigGetSubsection(s_lpmapppdata->lpcfgWadOptMap, WADOPT_DEFAULTTEX);

						/* Set the values, using the ones we've already read
						 * from the controls.
						 */

						if(!IsBlankTexture(lddi.szFrontUpper))
							ConfigSetString(lpcfgDefTex, TEXT("upper"), lddi.szFrontUpper);

						if(!IsBlankTexture(lddi.szFrontMiddle))
							ConfigSetString(lpcfgDefTex, TEXT("middle"), lddi.szFrontMiddle);

						if(!IsBlankTexture(lddi.szFrontLower))
							ConfigSetString(lpcfgDefTex, TEXT("lower"), lddi.szFrontLower);
					}
					else if(SendDlgItemMessage(hwndPropPage, IDC_CHECK_BACK_SAVEDEFAULT, BM_GETCHECK, 0, 0) == BST_CHECKED)
					{
						CONFIG *lpcfgDefTex = ConfigGetSubsection(s_lpmapppdata->lpcfgWadOptMap, WADOPT_DEFAULTTEX);

						/* Set the values, using the ones we've already read
						 * from the controls.
						 */
						if(!IsBlankTexture(lddi.szBackUpper))
							ConfigSetString(lpcfgDefTex, TEXT("upper"), lddi.szBackUpper);

						if(!IsBlankTexture(lddi.szBackMiddle))
							ConfigSetString(lpcfgDefTex, TEXT("middle"), lddi.szBackMiddle);

						if(!IsBlankTexture(lddi.szBackLower))
							ConfigSetString(lpcfgDefTex, TEXT("lower"), lddi.szBackLower);
					}
				}

				return TRUE;
			}

			/* Still WM_NOTIFY... */

			switch(lpnmhdr->idFrom)
			{
			case IDC_TREE_EFFECT:
				{
					LPNMTREEVIEW lpnmtreeview = (LPNMTREEVIEW)lParam;

					switch(lpnmhdr->code)
					{
					case TVN_SELCHANGED:
						/* Does the selected node correspond to an effect, and
						 * was it the direct result of user input (and not
						 * because the edit control changed)? If so, set the
						 * edit control.
						 */
						if((lpnmtreeview->action == TVC_BYKEYBOARD || lpnmtreeview->action == TVC_BYMOUSE) && lpnmtreeview->itemNew.lParam >= 0)
							SetDlgItemInt(hwndPropPage, IDC_EDIT_EFFECT, lpnmtreeview->itemNew.lParam, FALSE);

						return TRUE;

					case NM_RCLICK:
						DoTreeViewWikiRClick(lpnmhdr->hwndFrom, s_lpmapppdata, WIKIQUERY_LDEFFECT);
						return TRUE;
					}
				}

				break;

			case IDC_LIST_FLAGS:
				{
					switch(lpnmhdr->code)
					{
					case NM_DBLCLK:
					case NM_CLICK:
						/* Do checkbox processing. */
						ListView3StateClick((LPNMLISTVIEW)lParam);
						return TRUE;

					case LVN_KEYDOWN:
						/* Do checkbox processing. */
						ListView3StateKeyDown((LPNMLVKEYDOWN)lParam);
						return TRUE;
					}
				}

				break;
			}
		}

		/* Didn't process the message. */
		break;

	case WM_DESTROY:
		{
			int i;

			/* Clean up. */

			/* Free effect no. -> node lookup. */
			ConfigDestroy(s_lpcfgTypeToNode);

			/* Destroy preview bitmaps and autocomplete objects. */
			for(i = 0; i < 6; i++)
			{
				DeleteObject(s_hbmTexPreviews[i]);

				if(s_lpac2[i]) s_lpac2[i]->lpVtbl->Release(s_lpac2[i]);
				if(s_lptexenum[i]) DestroyTexEnum(s_lptexenum[i]);
			}
		}

		return TRUE;
	}

	/* Didn't process message. */
	return FALSE;
}


/* ThingsPropPageProc
 *   Dialogue proc for things property page.
 *
 * Parameters:
 *   HWND	hwndPropPage	Property page window handle.
 *   UINT	uiMsg			Message identifier.
 *   WPARAM wParam			Message-specific.
 *   LPARAM lParam			Message-specific.
 *
 * Return value: INT_PTR
 *   TRUE if the message was processed; FALSE otherwise.
 */
static INT_PTR CALLBACK ThingsPropPageProc(HWND hwndPropPage, UINT uiMsg, WPARAM wParam, LPARAM lParam)
{
	static MAPPPDATA *s_lpmapppdata;
	static CONFIG *s_lpcfgTypeToNode;
	static HBITMAP s_hbmSprite;
	static BOOL s_bInitComplete;

	switch(uiMsg)
	{
	case WM_INITDIALOG:
		{
			LVCOLUMNA lvcol;
			HWND hwndListFlags = GetDlgItem(hwndPropPage, IDC_LIST_FLAGS);
			CONFIG *lpcfgThingFlags;
			UDACCEL udaccel[2];
			TYPETREEDATA ttd;
			CONFIG *lpcfgThingEffects;
			HDC hdcDlg;
			DWORD dwCheckFlags;
			THINGDISPLAYINFO tdi;
			BOOL bNybble;

			/* Stop interactions between controls until we're ready. */
			s_bInitComplete = FALSE;

			/* Get the map data. */
			s_lpmapppdata = (MAPPPDATA*)((PROPSHEETPAGE*)lParam)->lParam;

			/* Are we using the extra field? */
			bNybble = ConfigGetInteger(s_lpmapppdata->lpcfgMap, MAPCFG_THINGEFFECTNYBBLE);

			/* Create the sprite bitmap. */
			hdcDlg = GetDC(hwndPropPage);
			s_hbmSprite = CreateCompatibleBitmap(hdcDlg, CX_TEXPREVIEW, CY_TEXPREVIEW);
			SendDlgItemMessage(hwndPropPage, IDC_TEX_SPRITE, STM_SETIMAGE, IMAGE_BITMAP, (LPARAM)s_hbmSprite);
			ReleaseDC(hwndPropPage, hdcDlg);

			/* Set the preview to something sensible in case we never touch it
			 * again.
			 */
			SetTexturePreviewImage(s_lpmapppdata->hwndMap, GetDlgItem(hwndPropPage, IDC_TEX_SPRITE), TEXT(""), TF_IMAGE, s_hbmSprite, FALSE);

			/* Initialise the flag list. */
			lvcol.mask = LVCF_SUBITEM;
			lvcol.iSubItem = 0;
			ListView_InsertColumn(hwndListFlags, 0, (VOID*)&lvcol);
			Init3StateListView(hwndListFlags);

			/* Get the possible thing flag values. */
			lpcfgThingFlags = ConfigGetSubsection(s_lpmapppdata->lpcfgMap, TEXT("thingflags"));

			/* Add them to the list, with their initial values. */
			if(lpcfgThingFlags)
			{
				AFTLDATA aftldata;
				aftldata.hwndListView = hwndListFlags;
				CheckThingFlags(s_lpmapppdata->lpmap, s_lpmapppdata->lpselection->lpsellistThings, &aftldata.wFlags, &aftldata.wFlagMask);
				ConfigIterate(lpcfgThingFlags, AddFlagToList, &aftldata);
			}

			/* Sort by flag value. */
			ListView_SortItems(hwndListFlags, ListViewIntegerComparison, 0);

			/* Make the column big enough. */
			ListView_SetColumnWidth(hwndListFlags, 0, LVSCW_AUTOSIZE);

			/* Set up the up-down acceleration structure. */
			udaccel[0].nInc = 1;
			udaccel[0].nSec = 0;
			udaccel[1].nInc = 8;
			udaccel[1].nSec = 2;

			SendDlgItemMessage(hwndPropPage, IDC_SPIN_ANGLE, UDM_SETRANGE32, -32768, 32767);
			SendDlgItemMessage(hwndPropPage, IDC_SPIN_ANGLE, UDM_SETACCEL, 2, (LPARAM)udaccel);

			SendDlgItemMessage(hwndPropPage, IDC_SPIN_X, UDM_SETRANGE32, -32768, 32767);
			SendDlgItemMessage(hwndPropPage, IDC_SPIN_X, UDM_SETACCEL, 2, (LPARAM)udaccel);

			SendDlgItemMessage(hwndPropPage, IDC_SPIN_Y, UDM_SETRANGE32, -32768, 32767);
			SendDlgItemMessage(hwndPropPage, IDC_SPIN_Y, UDM_SETACCEL, 2, (LPARAM)udaccel);

			SendDlgItemMessage(hwndPropPage, IDC_SPIN_PARAMETER, UDM_SETRANGE32, 0, 15);
			SendDlgItemMessage(hwndPropPage, IDC_SPIN_PARAMETER, UDM_SETACCEL, 2, (LPARAM)udaccel);

			SendDlgItemMessage(hwndPropPage, IDC_SPIN_Z, UDM_SETACCEL, 2, (LPARAM)udaccel);
			/* Z's range will be set when the thing type changes. */

			/* Disable the parameter controls if we're not using them. */
			if(!bNybble)
			{
				EnableWindow(GetDlgItem(hwndPropPage, IDC_EDIT_PARAMETER), FALSE);
				EnableWindow(GetDlgItem(hwndPropPage, IDC_SPIN_PARAMETER), FALSE);
			}


			/* Create an empty effect-to-node lookup table. */
			s_lpcfgTypeToNode = ConfigCreate();

			/* Populate the thing-type tree and corresponding lookup table. */
			lpcfgThingEffects = ConfigGetSubsection(s_lpmapppdata->lpcfgMap, TEXT("thingtypes"));
			ttd.hwndTree = GetDlgItem(hwndPropPage, IDC_TREE_THING);
			ttd.lpcfgTypeToNode = s_lpcfgTypeToNode;
			ConfigIterate(lpcfgThingEffects, AddCategoryToTree, &ttd);


			/* Set the angles associated with the radio buttons. */
			SetWindowLong(GetDlgItem(hwndPropPage, IDC_RADIO_E), GWL_USERDATA, 0);
			SetWindowLong(GetDlgItem(hwndPropPage, IDC_RADIO_NE), GWL_USERDATA, 45);
			SetWindowLong(GetDlgItem(hwndPropPage, IDC_RADIO_N), GWL_USERDATA, 90);
			SetWindowLong(GetDlgItem(hwndPropPage, IDC_RADIO_NW), GWL_USERDATA, 135);
			SetWindowLong(GetDlgItem(hwndPropPage, IDC_RADIO_W), GWL_USERDATA, 180);
			SetWindowLong(GetDlgItem(hwndPropPage, IDC_RADIO_SW), GWL_USERDATA, 225);
			SetWindowLong(GetDlgItem(hwndPropPage, IDC_RADIO_S), GWL_USERDATA, 270);
			SetWindowLong(GetDlgItem(hwndPropPage, IDC_RADIO_SE), GWL_USERDATA, 315);


			/* Find out which fields match in the selection. */
			dwCheckFlags = CheckThings(
				s_lpmapppdata->lpmap,
				s_lpmapppdata->lpselection->lpsellistThings,
				ConfigGetSubsection(s_lpmapppdata->lpcfgMap, FLAT_THING_SECTION),
				&tdi);

			/* Now set them. */
			InitThingsPropPageFields(hwndPropPage, &tdi, dwCheckFlags);

			/* Set the focus. */
			SendMessage(hwndPropPage, WM_NEXTDLGCTL, (WPARAM)GetDlgItem(hwndPropPage, IDC_EDIT_THING), MAKELPARAM(TRUE, 0));

			s_bInitComplete = TRUE;
		}

		/* We set the focus, so don't let the system do it again. */
		return FALSE;


	case WM_COMMAND:

		switch(LOWORD(wParam))
		{
		case IDC_EDIT_THING:
			switch(HIWORD(wParam))
			{
			case EN_CHANGE:
				{
					BOOL bTranslated;
					unsigned short unType = (unsigned short)GetDlgItemInt(hwndPropPage, IDC_EDIT_THING, &bTranslated, FALSE);
					TCHAR szSprite[TEXNAME_BUFFER_LENGTH] = TEXT("");
					TCHAR szDeafValue[CCH_SIGNED_INT];
					TCHAR szMultiValue[CCH_SIGNED_INT];
					TCHAR szSpecValue[CCH_SIGNED_INT];
					LPTSTR szDeafText, szMultiText, szSpecText;
					DWORD cchDeaf, cchMulti, cchSpec;
					CONFIG *lpcfgThingFlags = ConfigGetSubsection(s_lpmapppdata->lpcfgMap, TEXT("thingflags"));
					HWND hwndFlagList = GetDlgItem(hwndPropPage, IDC_LIST_FLAGS);

					/* Get the values for the deaf and multi flags so we can
					 * load the default text.
					 */
					_sntprintf(szDeafValue, NUM_ELEMENTS(szDeafValue), TEXT("%u"), TF_DEAF);
					_sntprintf(szMultiValue, NUM_ELEMENTS(szMultiValue), TEXT("%u"), TF_MULTI);
					_sntprintf(szMultiValue, NUM_ELEMENTS(szSpecValue), TEXT("%u"), TF_SPECIAL);

					cchDeaf = ConfigGetStringLength(lpcfgThingFlags, szDeafValue) + 1;
					cchMulti = ConfigGetStringLength(lpcfgThingFlags, szMultiValue) + 1;
					cchSpec = ConfigGetStringLength(lpcfgThingFlags, szSpecValue) + 1;
					szDeafText = ProcHeapAlloc(cchDeaf * sizeof(TCHAR));
					szMultiText = ProcHeapAlloc(cchMulti * sizeof(TCHAR));
					szSpecText = ProcHeapAlloc(cchSpec * sizeof(TCHAR));

					/* These will helpfully return empty strings even if the
					 * node doesn't exist.
					 */
					ConfigGetString(lpcfgThingFlags, szDeafValue, szDeafText, cchDeaf);
					ConfigGetString(lpcfgThingFlags, szMultiValue, szMultiText, cchMulti);
					ConfigGetString(lpcfgThingFlags, szSpecValue, szSpecText, cchSpec);

					/* Set the text in the control. */
					SetFlagText(hwndFlagList, TF_DEAF, szDeafText);
					SetFlagText(hwndFlagList, TF_MULTI, szMultiText);
					SetFlagText(hwndFlagList, TF_SPECIAL, szSpecText);

					ProcHeapFree(szDeafText);
					ProcHeapFree(szMultiText);
					ProcHeapFree(szSpecText);

					/* Valid number? */
					if(bTranslated)
					{
						TCHAR szValue[CCH_SIGNED_INT];

						_sntprintf(szValue, NUM_ELEMENTS(szValue), TEXT("%u"), unType);

						/* Update the Z controls based on the new thing. */
						UpdateZControlsForThing(hwndPropPage, s_lpmapppdata, unType);

						/* Do we have a tree node? */
						if(ConfigNodeExists(s_lpcfgTypeToNode, szValue))
						{
							CONFIG *lpcfgThing;

							/* Select corresponding tree item. */
							TreeView_SelectItem(GetDlgItem(hwndPropPage, IDC_TREE_THING), (HTREEITEM)ConfigGetInteger(s_lpcfgTypeToNode, szValue));

							lpcfgThing = GetThingConfigInfo(ConfigGetSubsection(s_lpmapppdata->lpcfgMap, FLAT_THING_SECTION), unType);
							if(lpcfgThing)
							{
								if(ConfigNodeExists(lpcfgThing, TEXT("sprite")))
									ConfigGetString(lpcfgThing, TEXT("sprite"), szSprite, TEXNAME_BUFFER_LENGTH);

								/* Set the per-thing deaf/multi text. */
								cchDeaf = ConfigGetStringLength(lpcfgThing, TEXT("deaftext")) + 1;
								cchMulti = ConfigGetStringLength(lpcfgThing, TEXT("multitext")) + 1;
								cchSpec = ConfigGetStringLength(lpcfgThing, TEXT("spectext")) + 1;
								szDeafText = ProcHeapAlloc(cchDeaf * sizeof(TCHAR));
								szMultiText = ProcHeapAlloc(cchMulti * sizeof(TCHAR));
								szSpecText = ProcHeapAlloc(cchSpec * sizeof(TCHAR));

								/* These will helpfully return empty strings even if the
								 * node doesn't exist.
								 */
								ConfigGetString(lpcfgThing, TEXT("deaftext"), szDeafText, cchDeaf);
								ConfigGetString(lpcfgThing, TEXT("multitext"), szMultiText, cchMulti);
								ConfigGetString(lpcfgThing, TEXT("spectext"), szSpecText, cchSpec);

								/* Set the text in the control. */
								SetFlagText(hwndFlagList, TF_DEAF, szDeafText);
								SetFlagText(hwndFlagList, TF_MULTI, szMultiText);
								SetFlagText(hwndFlagList, TF_SPECIAL, szSpecText);

								ProcHeapFree(szDeafText);
								ProcHeapFree(szMultiText);
								ProcHeapFree(szSpecText);
							}
						}
						else
							TreeView_SelectItem(GetDlgItem(hwndPropPage, IDC_TREE_THING), NULL);
					}

					/* Set or clear the preview image. */
					SetTexturePreviewImage(
						s_lpmapppdata->hwndMap,
						GetDlgItem(hwndPropPage, IDC_TEX_SPRITE),
						szSprite,
						TF_IMAGE,
						s_hbmSprite,
						TRUE);

					/* Adjust the colum-width in the flag-list. */
					ListView_SetColumnWidth(hwndFlagList, 0, LVSCW_AUTOSIZE);
				}

				return TRUE;

			case EN_KILLFOCUS:
				/* Validation. */
				BoundEditBox((HWND)lParam, 0, ConfigGetInteger(s_lpmapppdata->lpcfgMap, MAPCFG_THINGEFFECTNYBBLE) ? 4095 : 65535, TRUE, FALSE);

				return TRUE;
			}

			break;

		case IDC_EDIT_FLAGS:
			switch(HIWORD(wParam))
			{
			case EN_CHANGE:
				/* Clear the Z if the flags have been set. */
				if(s_bInitComplete && GetWindowTextLength((HWND)lParam) > 0)
					SetDlgItemText(hwndPropPage, IDC_EDIT_Z, TEXT(""));

				return TRUE;

			case EN_KILLFOCUS:
				/* Validation. */
				BoundEditBox((HWND)lParam, 0, 65535, TRUE, TRUE);

				return TRUE;
			}

			break;

		case IDC_EDIT_Z:
			switch(HIWORD(wParam))
			{
			case EN_CHANGE:
				/* Clear the flags if the Z has been set. */
				if(s_bInitComplete && GetWindowTextLength((HWND)lParam) > 0)
					SetDlgItemText(hwndPropPage, IDC_EDIT_FLAGS, TEXT(""));

				return TRUE;

			case EN_KILLFOCUS:
				{
					/* Validation. */

					int iMin, iMax;

					SendDlgItemMessage(hwndPropPage, IDC_SPIN_Z, UDM_GETRANGE32, (WPARAM)&iMin, (LPARAM)&iMax);
					BoundEditBox((HWND)lParam, iMin, iMax, TRUE, TRUE);
				}

				return TRUE;
			}

			break;

		case IDC_RADIO_E:
		case IDC_RADIO_NE:
		case IDC_RADIO_N:
		case IDC_RADIO_NW:
		case IDC_RADIO_W:
		case IDC_RADIO_SW:
		case IDC_RADIO_S:
		case IDC_RADIO_SE:
			/* If the radio button was clicked and is now set, set the angle. */
			if(HIWORD(wParam) == BN_CLICKED && SendMessage((HWND)lParam, BM_GETCHECK, 0, 0) == BST_CHECKED)
			{
				SetDlgItemInt(hwndPropPage, IDC_EDIT_ANGLE, GetWindowLong((HWND)lParam, GWL_USERDATA), FALSE);
				return TRUE;
			}

			break;

		case IDC_EDIT_ANGLE:
			switch(HIWORD(wParam))
			{
			case EN_CHANGE:
				{
					const int iRadioIDs[] = {IDC_RADIO_E, IDC_RADIO_NE, IDC_RADIO_N, IDC_RADIO_NW,
												IDC_RADIO_W, IDC_RADIO_SW, IDC_RADIO_S, IDC_RADIO_SE};
					size_t i;
					BOOL bValidNumber;
					int iAngle = GetDlgItemInt(hwndPropPage, LOWORD(wParam), &bValidNumber, TRUE);

					/* For each of the radio buttons, select it if we entered a
					 * valid angle and it matches that for this button, otherwise
					 * deselect it.
					 */
					for(i = 0; i < sizeof(iRadioIDs) / sizeof(int); i++)
						SendDlgItemMessage(hwndPropPage, iRadioIDs[i], BM_SETCHECK, bValidNumber && GetWindowLong(GetDlgItem(hwndPropPage, iRadioIDs[i]), GWL_USERDATA) == iAngle ? BST_CHECKED : BST_UNCHECKED, 0);
				}

				return TRUE;

			case EN_KILLFOCUS:
				/* Validation. */
				BoundEditBox((HWND)lParam, -32768, 32767, TRUE, TRUE);
				return TRUE;
			}

			break;

		case IDC_EDIT_X:
		case IDC_EDIT_Y:
			if(HIWORD(wParam) == EN_KILLFOCUS)
			{
				BoundEditBox((HWND)lParam, -32768, 32767, TRUE, TRUE);
				return TRUE;
			}

			break;

		case IDC_EDIT_PARAMETER:
			if(HIWORD(wParam) == EN_KILLFOCUS)
			{
				BoundEditBox((HWND)lParam, 0, 15, TRUE, FALSE);
				return TRUE;
			}

			break;

		case IDC_CHECK_ABSZ:
			/* If we've moved between relative and absolute Z, we need to update
			 * the Z controls.
			 */
			if(HIWORD(wParam) == BN_CLICKED)
			{
				UpdateZControlsForThing(hwndPropPage, s_lpmapppdata, GetDlgItemInt(hwndPropPage, IDC_EDIT_THING, NULL, FALSE));
				return TRUE;
			}

			break;
		}

		break;


	case WM_NOTIFY:
		{
			LPNMHDR lpnmhdr = (LPNMHDR)lParam;

			/* Special case: notifications from the property sheet don't set an
			 * ID.
			 */
			if(lpnmhdr->code == (unsigned)PSN_APPLY)
			{
				/* User OK-ed. Set the properties! */

				CONFIG *lpcfgFlatThings = ConfigGetSubsection(s_lpmapppdata->lpcfgMap, FLAT_THING_SECTION);
				THINGDISPLAYINFO tdi;
				THING_RELATIVE thingrelative = {0, 0, 0, 0, 0};
				DWORD dwPropertyFlags = 0, dwRelativeFlags = 0;
				TCHAR szBuffer[7];
				WORD wFlags, wFlagMask;

				/* We go through each field, checking whether it's non-empty.
				 * If so, copy its value and set the corresponding flag.
				 */

				/* Thing type. */
				GetDlgItemText(hwndPropPage, IDC_EDIT_THING, szBuffer, sizeof(szBuffer)/sizeof(TCHAR));
				if(*szBuffer)
				{
					tdi.unType = s_lpmapppdata->lpthingLast->thing = GetDlgItemInt(hwndPropPage, IDC_EDIT_THING, NULL, FALSE);
					dwPropertyFlags |= TDIF_TYPE;
				}

				/* Thing parameter. */
				GetDlgItemText(hwndPropPage, IDC_EDIT_PARAMETER, szBuffer, sizeof(szBuffer)/sizeof(TCHAR));
				if(*szBuffer)
				{
					tdi.byParam = s_lpmapppdata->lpthingLast->typeuppernybble = GetDlgItemInt(hwndPropPage, IDC_EDIT_PARAMETER, NULL, FALSE);
					dwPropertyFlags |= TDIF_PARAM;
				}

				/* Angle. */
				GetDlgItemText(hwndPropPage, IDC_EDIT_ANGLE, szBuffer, sizeof(szBuffer)/sizeof(TCHAR));
				if(*szBuffer)
				{
					if(STRING_RELATIVE(szBuffer))
					{
						thingrelative.iAngle = _tcstol(&szBuffer[1], NULL, 10);
						dwRelativeFlags |= TDIF_DIRECTION;
					}
					else
					{
						tdi.nDirection = s_lpmapppdata->lpthingLast->angle = GetDlgItemInt(hwndPropPage, IDC_EDIT_ANGLE, NULL, TRUE);
						dwPropertyFlags |= TDIF_DIRECTION;
					}
				}

				/* X. */
				GetDlgItemText(hwndPropPage, IDC_EDIT_X, szBuffer, sizeof(szBuffer)/sizeof(TCHAR));
				if(*szBuffer)
				{
					if(STRING_RELATIVE(szBuffer))
					{
						thingrelative.x = _tcstol(&szBuffer[1], NULL, 10);
						dwRelativeFlags |= TDIF_X;
					}
					else
					{
						tdi.x = GetDlgItemInt(hwndPropPage, IDC_EDIT_X, NULL, TRUE);
						dwPropertyFlags |= TDIF_X;
					}
				}

				/* Y. */
				GetDlgItemText(hwndPropPage, IDC_EDIT_Y, szBuffer, sizeof(szBuffer)/sizeof(TCHAR));
				if(*szBuffer)
				{
					if(STRING_RELATIVE(szBuffer))
					{
						thingrelative.y = _tcstol(&szBuffer[1], NULL, 10);
						dwRelativeFlags |= TDIF_Y;
					}
					else
					{
						tdi.y = GetDlgItemInt(hwndPropPage, IDC_EDIT_Y, NULL, TRUE);
						dwPropertyFlags |= TDIF_Y;
					}
				}

				/* Flags. */
				GetDlgItemText(hwndPropPage, IDC_EDIT_FLAGS, szBuffer, sizeof(szBuffer)/sizeof(TCHAR));
				if(*szBuffer)
				{
					/* Value for flags is specified, so just use that. */
					if(STRING_RELATIVE(szBuffer))
					{
						thingrelative.iFlags = _tcstol(&szBuffer[1], NULL, 10);
						dwRelativeFlags |= TDIF_FLAGS;
					}
					else
					{
						wFlags = GetDlgItemInt(hwndPropPage, IDC_EDIT_FLAGS, NULL, FALSE);
						dwPropertyFlags |= TDIF_FLAGS;
					}

					/* All flags are valid. */
					wFlagMask = (WORD)-1;
				}
				else
				{
					/* No direct value specified from flags, so construct it
					 * from the checkboxes.
					 */
					GetFlagsFromListView(GetDlgItem(hwndPropPage, IDC_LIST_FLAGS), &wFlags, &wFlagMask);
				}

				/* Update remembered flags, too. */
				s_lpmapppdata->lpthingLast->flag &= ~wFlagMask;
				s_lpmapppdata->lpthingLast->flag |= wFlags;


				/* Apply the relative properties. */
				if(dwRelativeFlags & TDIF_DIRECTION)
					ApplyRelativeAngleSelection(s_lpmapppdata->lpmap, s_lpmapppdata->lpselection->lpsellistThings, thingrelative.iAngle);

				if(dwRelativeFlags & TDIF_X)
					ApplyRelativeThingXSelection(s_lpmapppdata->lpmap, s_lpmapppdata->lpselection->lpsellistThings, thingrelative.x);

				if(dwRelativeFlags & TDIF_Y)
					ApplyRelativeThingYSelection(s_lpmapppdata->lpmap, s_lpmapppdata->lpselection->lpsellistThings, thingrelative.y);

				if(dwRelativeFlags & TDIF_FLAGS)
					ApplyRelativeThingFlagsSelection(s_lpmapppdata->lpmap, s_lpmapppdata->lpselection->lpsellistThings, thingrelative.iFlags);
				else
					SetThingFlags(s_lpmapppdata->lpmap, s_lpmapppdata->lpselection->lpsellistThings, wFlags, wFlagMask);

				/* We've got all the properties now. Apply them. */
				ApplyThingPropertiesToSelection(s_lpmapppdata->lpmap, s_lpmapppdata->lpselection->lpsellistThings, &tdi, dwPropertyFlags, lpcfgFlatThings);


				/* Z. This must be applied after flags! */
				GetDlgItemText(hwndPropPage, IDC_EDIT_Z, szBuffer, sizeof(szBuffer)/sizeof(TCHAR));
				if(*szBuffer)
				{
					if(STRING_RELATIVE(szBuffer))
					{
						thingrelative.z = _tcstol(&szBuffer[1], NULL, 10);
						ApplyRelativeThingZSelection(s_lpmapppdata->lpmap, s_lpmapppdata->lpselection->lpsellistThings, lpcfgFlatThings, thingrelative.z);
					}
					else
					{
						SetThingSelectionZ(
							s_lpmapppdata->lpmap,
							s_lpmapppdata->lpselection->lpsellistThings,
							lpcfgFlatThings,
							(WORD)GetDlgItemInt(hwndPropPage, IDC_EDIT_Z, NULL, FALSE),
							IsDlgButtonChecked(hwndPropPage, IDC_CHECK_ABSZ) != BST_UNCHECKED);
					}
				}

				/* We've made changes. */
				s_lpmapppdata->bOKed = TRUE;

				return TRUE;
			}

			/* Still WM_NOTIFY... */

			switch(lpnmhdr->idFrom)
			{
			case IDC_TREE_THING:
				{
					LPNMTREEVIEW lpnmtreeview = (LPNMTREEVIEW)lParam;

					switch(lpnmhdr->code)
					{
					case TVN_SELCHANGED:
						/* Does the selected node correspond to an effect, and
						 * was it the direct result of user input (and not
						 * because the edit control changed)? If so, set the
						 * edit control.
						 */
						if((lpnmtreeview->action == TVC_BYKEYBOARD || lpnmtreeview->action == TVC_BYMOUSE) && lpnmtreeview->itemNew.lParam >= 0)
							SetDlgItemInt(hwndPropPage, IDC_EDIT_THING, lpnmtreeview->itemNew.lParam, FALSE);

						return TRUE;

					case NM_RCLICK:
						DoTreeViewWikiRClick(lpnmhdr->hwndFrom, s_lpmapppdata, WIKIQUERY_THING);
						return TRUE;
					}
				}

				break;

			case IDC_LIST_FLAGS:
				{
					LPNMLISTVIEW lpnmlistview = ((LPNMLISTVIEW)lParam);

					switch(lpnmhdr->code)
					{
					case NM_DBLCLK:
					case NM_CLICK:
						/* Do checkbox processing. */
						ListView3StateClick(lpnmlistview);
						return TRUE;

					case LVN_KEYDOWN:
						/* Do checkbox processing. */
						ListView3StateKeyDown((LPNMLVKEYDOWN)lParam);
						return TRUE;

					case LVN_ITEMCHANGED:
						/* If user changed the flags, clear flags and Z edit
						 * boxes.
						 */
						if(s_bInitComplete && (lpnmlistview->uChanged & LVIF_STATE) && ((lpnmlistview->uNewState ^ lpnmlistview->uOldState) & LVIS_STATEIMAGEMASK))
						{
							SetDlgItemText(hwndPropPage, IDC_EDIT_FLAGS, TEXT(""));
							SetDlgItemText(hwndPropPage, IDC_EDIT_Z, TEXT(""));
						}

						return TRUE;
					}
				}

				break;
			}
		}

		break;

	case WM_DESTROY:

		/* Free effect no. -> node lookup. */
		ConfigDestroy(s_lpcfgTypeToNode);

		/* Free sprite bitmap. */
		DeleteObject(s_hbmSprite);

		return TRUE;
	}

	/* Didn't process message. */
	return FALSE;
}


/* InitSectorsPropPageFields
 *   Fills fields of the sector property page.
 *
 * Parameters:
 *   HWND				hwndPropPage	Property page window handle.
 *   SECTORDISPLAYINFO*	lpsdi			Data to fill in.
 *   DWORD				dwCheckFlags	Flags specifying which fields are valid.
 *
 * Return value: None.
 */
static void InitSectorsPropPageFields(HWND hwndPropPage, SECTORDISPLAYINFO *lpsdi, DWORD dwCheckFlags)
{
	/* Some are set to zero initially by their up-down controls, so we have to
	 * blank those if that's what we want. With the others, they're blank to
	 * start with.
	 */

	if(dwCheckFlags & SDIF_EFFECT)		SetDlgItemInt(hwndPropPage, IDC_EDIT_EFFECT, lpsdi->unEffect, FALSE);

	if(dwCheckFlags & SDIF_CEILING) SetDlgItemInt(hwndPropPage, IDC_EDIT_HCEIL, lpsdi->nCeiling, TRUE);
	else SetDlgItemText(hwndPropPage, IDC_EDIT_HCEIL, TEXT(""));

	if(dwCheckFlags & SDIF_FLOOR) SetDlgItemInt(hwndPropPage, IDC_EDIT_HFLOOR, lpsdi->nFloor, TRUE);
	else SetDlgItemText(hwndPropPage, IDC_EDIT_HFLOOR, TEXT(""));

	if(dwCheckFlags & SDIF_TAG) SetDlgItemInt(hwndPropPage, IDC_EDIT_TAG, lpsdi->unTag, FALSE);
	else SetDlgItemText(hwndPropPage, IDC_EDIT_TAG, TEXT(""));

	if(dwCheckFlags & SDIF_BRIGHTNESS)
	{
		SetDlgItemInt(hwndPropPage, IDC_COMBO_LIGHT, lpsdi->ucBrightness, FALSE);
		SendDlgItemMessage(hwndPropPage, IDC_SLIDER_LIGHT, TBM_SETPOS, TRUE, lpsdi->ucBrightness);
	}

	if(dwCheckFlags & SDIF_CEILINGTEX)	SetDlgItemText(hwndPropPage, IDC_EDIT_TCEIL, lpsdi->szCeiling);
	if(dwCheckFlags & SDIF_FLOORTEX)	SetDlgItemText(hwndPropPage, IDC_EDIT_TFLOOR, lpsdi->szFloor);
}


/* InitLinesPropPageFields
 *   Fills fields of the lines property page.
 *
 * Parameters:
 *   HWND					hwndPropPage	Property page window handle.
 *   LINEDEFDISPLAYINFO*	lplddi			Data to fill in.
 *   DWORD					dwCheckFlags	Flags specifying which fields are
 *											valid.
 *
 * Return value: None.
 */
static void InitLinesPropPageFields(HWND hwndPropPage, LINEDEFDISPLAYINFO *lplddi, DWORD dwCheckFlags)
{
	/* Some are set to zero initially by their up-down controls, so we have to
	 * blank those if that's what we want. With the others, they're blank to
	 * start with.
	 */

	if(dwCheckFlags & LDDIF_EFFECT)		SetDlgItemInt(hwndPropPage, IDC_EDIT_EFFECT, lplddi->unEffect, FALSE);

	if(dwCheckFlags & LDDIF_TAG) SetDlgItemInt(hwndPropPage, IDC_EDIT_TAG, lplddi->unTag, FALSE);
	else SetDlgItemText(hwndPropPage, IDC_EDIT_TAG, TEXT(""));

	if((dwCheckFlags & LDDIF_HASFRONT) && lplddi->bHasFront)
	{
		if(dwCheckFlags & LDDIF_FRONTSEC)	SetDlgItemInt(hwndPropPage, IDC_EDIT_FRONT_SECTOR, lplddi->unFrontSector, FALSE);

		if(dwCheckFlags & LDDIF_FRONTX) SetDlgItemInt(hwndPropPage, IDC_EDIT_FRONT_OFFX, lplddi->nFrontX, TRUE);
		else SetDlgItemText(hwndPropPage, IDC_EDIT_FRONT_OFFX, TEXT(""));

		if(dwCheckFlags & LDDIF_FRONTY) SetDlgItemInt(hwndPropPage, IDC_EDIT_FRONT_OFFY, lplddi->nFrontY, TRUE);
		else SetDlgItemText(hwndPropPage, IDC_EDIT_FRONT_OFFY, TEXT(""));

		if(dwCheckFlags & LDDIF_FRONTUPPER)	SetDlgItemText(hwndPropPage, IDC_EDIT_FRONT_UPPER, lplddi->szFrontUpper);
		if(dwCheckFlags & LDDIF_FRONTMIDDLE)	SetDlgItemText(hwndPropPage, IDC_EDIT_FRONT_MIDDLE, lplddi->szFrontMiddle);
		if(dwCheckFlags & LDDIF_FRONTLOWER)	SetDlgItemText(hwndPropPage, IDC_EDIT_FRONT_LOWER, lplddi->szFrontLower);
	}
	else
	{
		/* No front sector, so disable the front controls. */
		EnableWindow(GetDlgItem(hwndPropPage, IDC_GROUP_FRONT), FALSE);
		EnableWindow(GetDlgItem(hwndPropPage, IDC_STATIC_FRONT_OFFSET), FALSE);
		EnableWindow(GetDlgItem(hwndPropPage, IDC_STATIC_FRONT_SECTOR), FALSE);
		EnableWindow(GetDlgItem(hwndPropPage, IDC_STATIC_FRONT_UPPER), FALSE);
		EnableWindow(GetDlgItem(hwndPropPage, IDC_STATIC_FRONT_MIDDLE), FALSE);
		EnableWindow(GetDlgItem(hwndPropPage, IDC_STATIC_FRONT_LOWER), FALSE);
		EnableWindow(GetDlgItem(hwndPropPage, IDC_TEX_FRONT_UPPER), FALSE);
		EnableWindow(GetDlgItem(hwndPropPage, IDC_TEX_FRONT_MIDDLE), FALSE);
		EnableWindow(GetDlgItem(hwndPropPage, IDC_TEX_FRONT_LOWER), FALSE);
		EnableWindow(GetDlgItem(hwndPropPage, IDC_EDIT_FRONT_UPPER), FALSE);
		EnableWindow(GetDlgItem(hwndPropPage, IDC_EDIT_FRONT_MIDDLE), FALSE);
		EnableWindow(GetDlgItem(hwndPropPage, IDC_EDIT_FRONT_LOWER), FALSE);
		EnableWindow(GetDlgItem(hwndPropPage, IDC_EDIT_FRONT_OFFX), FALSE);
		EnableWindow(GetDlgItem(hwndPropPage, IDC_EDIT_FRONT_OFFY), FALSE);
		EnableWindow(GetDlgItem(hwndPropPage, IDC_SPIN_FRONT_OFFX), FALSE);
		EnableWindow(GetDlgItem(hwndPropPage, IDC_SPIN_FRONT_OFFY), FALSE);
		EnableWindow(GetDlgItem(hwndPropPage, IDC_EDIT_FRONT_SECTOR), FALSE);
		EnableWindow(GetDlgItem(hwndPropPage, IDC_BUTTON_FRONT_VISOFF), FALSE);
		EnableWindow(GetDlgItem(hwndPropPage, IDC_CHECK_FRONT_SAVEDEFAULT), FALSE);
	}

	if((dwCheckFlags & LDDIF_HASBACK) && lplddi->bHasBack)
	{
		if(dwCheckFlags & LDDIF_BACKSEC)	SetDlgItemInt(hwndPropPage, IDC_EDIT_BACK_SECTOR, lplddi->unBackSector, FALSE);

		if(dwCheckFlags & LDDIF_BACKX)		SetDlgItemInt(hwndPropPage, IDC_EDIT_BACK_OFFX, lplddi->nBackX, TRUE);
		else SetDlgItemText(hwndPropPage, IDC_EDIT_BACK_OFFX, TEXT(""));

		if(dwCheckFlags & LDDIF_BACKY)		SetDlgItemInt(hwndPropPage, IDC_EDIT_BACK_OFFY, lplddi->nBackY, TRUE);
		else SetDlgItemText(hwndPropPage, IDC_EDIT_BACK_OFFY, TEXT(""));

		if(dwCheckFlags & LDDIF_BACKUPPER)	SetDlgItemText(hwndPropPage, IDC_EDIT_BACK_UPPER, lplddi->szBackUpper);
		if(dwCheckFlags & LDDIF_BACKMIDDLE)	SetDlgItemText(hwndPropPage, IDC_EDIT_BACK_MIDDLE, lplddi->szBackMiddle);
		if(dwCheckFlags & LDDIF_BACKLOWER)	SetDlgItemText(hwndPropPage, IDC_EDIT_BACK_LOWER, lplddi->szBackLower);
	}
	else
	{
		/* No back sector, so disable the back controls. */
		EnableWindow(GetDlgItem(hwndPropPage, IDC_GROUP_BACK), FALSE);
		EnableWindow(GetDlgItem(hwndPropPage, IDC_STATIC_BACK_OFFSET), FALSE);
		EnableWindow(GetDlgItem(hwndPropPage, IDC_STATIC_BACK_SECTOR), FALSE);
		EnableWindow(GetDlgItem(hwndPropPage, IDC_STATIC_BACK_UPPER), FALSE);
		EnableWindow(GetDlgItem(hwndPropPage, IDC_STATIC_BACK_MIDDLE), FALSE);
		EnableWindow(GetDlgItem(hwndPropPage, IDC_STATIC_BACK_LOWER), FALSE);
		EnableWindow(GetDlgItem(hwndPropPage, IDC_TEX_BACK_UPPER), FALSE);
		EnableWindow(GetDlgItem(hwndPropPage, IDC_TEX_BACK_MIDDLE), FALSE);
		EnableWindow(GetDlgItem(hwndPropPage, IDC_TEX_BACK_LOWER), FALSE);
		EnableWindow(GetDlgItem(hwndPropPage, IDC_EDIT_BACK_UPPER), FALSE);
		EnableWindow(GetDlgItem(hwndPropPage, IDC_EDIT_BACK_MIDDLE), FALSE);
		EnableWindow(GetDlgItem(hwndPropPage, IDC_EDIT_BACK_LOWER), FALSE);
		EnableWindow(GetDlgItem(hwndPropPage, IDC_EDIT_BACK_OFFX), FALSE);
		EnableWindow(GetDlgItem(hwndPropPage, IDC_EDIT_BACK_OFFY), FALSE);
		EnableWindow(GetDlgItem(hwndPropPage, IDC_SPIN_BACK_OFFX), FALSE);
		EnableWindow(GetDlgItem(hwndPropPage, IDC_SPIN_BACK_OFFY), FALSE);
		EnableWindow(GetDlgItem(hwndPropPage, IDC_EDIT_BACK_SECTOR), FALSE);
		EnableWindow(GetDlgItem(hwndPropPage, IDC_BUTTON_BACK_VISOFF), FALSE);
		EnableWindow(GetDlgItem(hwndPropPage, IDC_CHECK_BACK_SAVEDEFAULT), FALSE);
	}
}


/* InitThingsPropPageFields
 *   Fills fields of the things property page.
 *
 * Parameters:
 *   HWND				hwndPropPage	Property page window handle.
 *   THINGDISPLAYINFO*	lptdi			Data to fill in.
 *   DWORD				dwCheckFlags	Flags specifying which fields are valid.
 *
 * Return value: None.
 */
static void InitThingsPropPageFields(HWND hwndPropPage, THINGDISPLAYINFO *lptdi, DWORD dwCheckFlags)
{
	/* Some are set to zero initially by their up-down controls, so we have to
	 * blank those if that's what we want. With the others, they're blank to
	 * start with.
	 */

	if(dwCheckFlags & TDIF_TYPE) SetDlgItemInt(hwndPropPage, IDC_EDIT_THING, lptdi->unType, FALSE);

	if(dwCheckFlags & TDIF_X) SetDlgItemInt(hwndPropPage, IDC_EDIT_X, lptdi->x, TRUE);
	else SetDlgItemText(hwndPropPage, IDC_EDIT_X, TEXT(""));

	if(dwCheckFlags & TDIF_Y) SetDlgItemInt(hwndPropPage, IDC_EDIT_Y, lptdi->y, TRUE);
	else SetDlgItemText(hwndPropPage, IDC_EDIT_Y, TEXT(""));

	if(dwCheckFlags & TDIF_Z) SetDlgItemInt(hwndPropPage, IDC_EDIT_Z, lptdi->z, FALSE);
	else SetDlgItemText(hwndPropPage, IDC_EDIT_Z, TEXT(""));

	if(dwCheckFlags & TDIF_FLAGS) SetDlgItemInt(hwndPropPage, IDC_EDIT_FLAGS, lptdi->unFlags, FALSE);

	if(dwCheckFlags & TDIF_DIRECTION) SetDlgItemInt(hwndPropPage, IDC_EDIT_ANGLE, lptdi->nDirection, TRUE);
	else SetDlgItemText(hwndPropPage, IDC_EDIT_ANGLE, TEXT(""));

	if(dwCheckFlags & TDIF_PARAM) SetDlgItemInt(hwndPropPage, IDC_EDIT_PARAMETER, lptdi->byParam, FALSE);
}


/* AddSectorTypeToList
 *   Adds a sector effect to a listview.
 *
 * Parameters:
 *   CONFIG		*lpcfg		Config node containing effect info.
 *   void*		lpvWindow	(HWND) Window handle of listview.
 *
 * Return value: BOOL
 *   Always TRUE.
 *
 * Remarks:
 *   Used as a callback function by ConfigIterate.
 */
static BOOL AddSectorTypeToList(CONFIG *lpcfg, void *lpvWindow)
{
	const TCHAR szSeparator[] = TEXT(" - ");
	BOOL bUseNumbers = ConfigGetInteger(g_lpcfgMain, OPT_EFFECTNUMBERS);
	HWND hwndListView = (HWND)lpvWindow;
	LVITEM lvitem;
	DWORD cchEffect;

	/* Add entry and store effect value. */
	lvitem.mask = LVIF_TEXT| LVIF_PARAM;
	lvitem.iItem = lvitem.iSubItem = 0;
	lvitem.lParam = _tcstol(lpcfg->szName, NULL, 10);

	/* If we're including numbers, we need more space. */
	cchEffect = _tcslen(lpcfg->sz) + 1;
	if(bUseNumbers) cchEffect += _tcslen(lpcfg->szName) + _tcslen(szSeparator);

	/* Build the string. */
	lvitem.pszText = ProcHeapAlloc(cchEffect * sizeof(TCHAR));
	_sntprintf(
		lvitem.pszText,
		cchEffect,
		TEXT("%s%s%s"),
		bUseNumbers ? lpcfg->szName : TEXT(""),
		bUseNumbers ? szSeparator : TEXT(""),
		lpcfg->sz);

	lvitem.pszText[cchEffect - 1] = TEXT('\0');

	ListView_InsertItem(hwndListView, &lvitem);

	ProcHeapFree(lvitem.pszText);

	/* Keep iterating. */
	return TRUE;
}


/* AddSectorTypeToCombo
 *   Adds a sector effect to a combo box.
 *
 * Parameters:
 *   CONFIG		*lpcfg		Config node containing effect info.
 *   void*		lpvWindow	(HWND) Window handle of combo box.
 *
 * Return value: BOOL
 *   Always TRUE.
 *
 * Remarks:
 *   Used as a callback function by ConfigIterate.
 */
static BOOL AddSectorTypeToCombo(CONFIG *lpcfg, void *lpvWindow)
{
	const TCHAR szSeparator[] = TEXT(" - ");
	BOOL bUseNumbers = ConfigGetInteger(g_lpcfgMain, OPT_EFFECTNUMBERS);
	HWND hwndCombo = (HWND)lpvWindow;
	DWORD cchEffect;
	LPTSTR szEffect;
	int iValue;
	int iIndex;

	/* Add entry and store effect value. */
	iValue = _tcstol(lpcfg->szName, NULL, 10);

	/* If we're including numbers, we need more space. */
	cchEffect = _tcslen(lpcfg->sz) + 1;
	if(bUseNumbers) cchEffect += _tcslen(lpcfg->sz) + _tcslen(szSeparator);

	/* Build the string. */
	szEffect = ProcHeapAlloc(cchEffect * sizeof(TCHAR));
	_sntprintf(
		szEffect,
		cchEffect,
		TEXT("%s%s%s"),
		bUseNumbers ? lpcfg->szName : TEXT(""),
		bUseNumbers ? szSeparator : TEXT(""),
		lpcfg->sz);

	szEffect[cchEffect - 1] = TEXT('\0');

	/* Add the item and set its item-data. */
	if(bUseNumbers)
	{
		/* Binary search, to find where to insert it. */
		int iLower = -1, iUpper = SendMessage(hwndCombo, CB_GETCOUNT, 0, 0);

		while(iLower < iUpper - 1)
		{
			int iMidIndex = (iUpper + iLower + 1) / 2;
			int iMidValue = SendMessage(hwndCombo, CB_GETITEMDATA, iMidIndex, 0);

			if(iValue <= iMidValue) iUpper = iMidIndex;
			else iLower = iMidIndex;
		}

		iIndex = SendMessage(hwndCombo, CB_INSERTSTRING, iLower + 1, (LPARAM)szEffect);
	}
	else
		iIndex = SendMessage(hwndCombo, CB_ADDSTRING, 0, (LPARAM)szEffect);

	SendMessage(hwndCombo, CB_SETITEMDATA, iIndex, iValue);

	ProcHeapFree(szEffect);

	/* Keep iterating. */
	return TRUE;
}



/* AddFlagToList
 *   Adds a linedef/thing flag to a list-view control.
 *
 * Parameters:
 *   CONFIG		*lpcfg		Config node containing flag info.
 *   void*		lpvData		(AFTLDATA*) Data including window handle of list
 *							and line selection flag values for initialisation.
 *
 * Return value: BOOL
 *   Always TRUE.
 *
 * Remarks:
 *   Used as a callback function by ConfigIterate.
 */
static BOOL AddFlagToList(CONFIG *lpcfg, void *lpvData)
{
	AFTLDATA *lpaftldata = (AFTLDATA*)lpvData;
	LVITEM lvitem;
	int iIndex, iState;

	/* Add entry and store effect value. */
	lvitem.mask = LVIF_TEXT | LVIF_PARAM;
	lvitem.iItem = lvitem.iSubItem = 0;
	lvitem.pszText = lpcfg->sz;
	lvitem.lParam = _tcstol(lpcfg->szName, NULL, 10);
	iIndex = ListView_InsertItem(lpaftldata->hwndListView, &lvitem);

	/* Determine the initial state. */
	if(!(lpaftldata->wFlagMask & lvitem.lParam))
		iState = LV3_INDETERMINATE;
	else if(lpaftldata->wFlags & lvitem.lParam)
		iState = LV3_CHECKED;
	else iState = LV3_UNCHECKED;

	ListView3StateSetItemState(lpaftldata->hwndListView, iIndex, iState);

	/* Keep iterating. */
	return TRUE;
}


LRESULT CALLBACK TexPreviewProc(HWND hwnd, UINT uiMsg, WPARAM wParam, LPARAM lParam)
{
	TPPDATA *lptppd = (TPPDATA*)GetWindowLong(hwnd, GWL_USERDATA);

	switch(uiMsg)
	{
	case WM_SETCURSOR:
		{
			HCURSOR hcursorHand = LoadCursor(NULL, IDC_HAND);

			/* Non-NT Windows don't support IDC_HAND. */
			if(hcursorHand) SetCursor(hcursorHand);
		}

		return 0;

	case WM_DESTROY:
		/* Restore default window class -- WM_DESTROY is apparently not the last
		 * message.
		 */
		SetWindowLong(hwnd, GWL_WNDPROC, (LONG)lptppd->wndprocStatic);

		/* Free subclassing data. */
		ProcHeapFree(lptppd);
		return 0;
	}

	/* The original wndproc is stored as our userdata. */
	return CallWindowProc(lptppd->wndprocStatic, hwnd, uiMsg, wParam, lParam);
}



/* AddCategoryToTree
 *   Creates a top-level node in a tree for a linedef/thing category, and adds
 *   all the associated types as children.
 *
 * Parameters:
 *   CONFIG*	lpcfgType	A linedef/thing type category subsection container.
 *   void*		lpv			(TYPETREEDATA*) Specifies handle of control and
 *							config used to create the effect-to-node lookup;
 *							parent field is ignored.
 *
 * Return value: BOOL
 *   TRUE if iteration should continue; FALSE otherwise.
 *
 * Remarks:
 *   Used as a callback by ConfigIterate.
 */
static BOOL AddCategoryToTree(CONFIG *lpcfgCategory, void *lpv)
{
	TYPETREEDATA *lpttd = (TYPETREEDATA*)lpv;
	CONFIG *lpcfgValues = ConfigGetSubsection(lpcfgCategory->lpcfgSubsection, TEXT("values"));
	int cchCategoryTitle = ConfigGetStringLength(lpcfgCategory->lpcfgSubsection, TEXT("title"));

	/* Sanity check. */
	if(lpcfgValues && cchCategoryTitle > 0)
	{
		TVINSERTSTRUCT tvis;

		/* Get the string for the parent node. */
		tvis.item.pszText = ProcHeapAlloc((cchCategoryTitle + 1) * sizeof(TCHAR));
		ConfigGetString(lpcfgCategory->lpcfgSubsection, TEXT("title"), tvis.item.pszText, cchCategoryTitle + 1);

		/* Set up some properties for the new node. */
		tvis.hParent = TVI_ROOT;
		tvis.hInsertAfter = TVI_SORT;
		tvis.item.mask = TVIF_TEXT | TVIF_PARAM;
		tvis.item.lParam = -1;		/* Doesn't correspond to an effect. */

		/* Create the parent node. */
		lpttd->htreeitemParent = TreeView_InsertItem(lpttd->hwndTree, &tvis);

		/* Finished with the title now. */
		ProcHeapFree(tvis.item.pszText);

		/* Add all the children. */
		ConfigIterate(lpcfgValues, AddTypeToTree, lpttd);

		/* Sort by effect number, if that's what the user wants. */
		if(ConfigGetInteger(g_lpcfgMain, OPT_EFFECTNUMBERS))
		{
			TVSORTCB tvsortcb;

			tvsortcb.hParent = lpttd->htreeitemParent;
			tvsortcb.lpfnCompare = EffectNumericalSort;
			TreeView_SortChildrenCB(lpttd->hwndTree, &tvsortcb, 0);
		}
	}

	/* Keep going. */
	return TRUE;
}


/* AddTypeToTree
 *   Adds a linedef effect type as a child of a tree node.
 *
 * Parameters:
 *   CONFIG*	lpcfgType		A linedef/thing type entry.
 *   void*		lpv				(TYPETREEDATA*) Specifies handle of control,
 *								config used to create the effect-to-node lookup
 *								and parent node.
 *
 * Return value: BOOL
 *   TRUE if iteration should continue; FALSE otherwise.
 *
 * Remarks:
 *   Used as a callback by ConfigIterate.
 */
static BOOL AddTypeToTree(CONFIG *lpcfgType, void *lpv)
{
	const TCHAR szSeparator[] = TEXT(" - ");
	BOOL bUseNumbers = ConfigGetInteger(g_lpcfgMain, OPT_EFFECTNUMBERS);
	TYPETREEDATA *lpttd = (TYPETREEDATA*)lpv;
	TVINSERTSTRUCT tvis;
	int cchTitle;
	HTREEITEM htreeitemNew;

	/* Get the display text. */
	switch(lpcfgType->entrytype)
	{
	case CET_STRING:
		/* Allocate space for the node. */
		cchTitle = _tcslen(lpcfgType->sz);
		if(bUseNumbers) cchTitle += _tcslen(lpcfgType->sz) + _tcslen(szSeparator);
		tvis.item.pszText = ProcHeapAlloc((cchTitle + 1) * sizeof(TCHAR));

		/* Build the string. */
		_sntprintf(
			tvis.item.pszText,
			cchTitle + 1,
			TEXT("%s%s%s"),
			bUseNumbers ? lpcfgType->szName : TEXT(""),
			bUseNumbers ? szSeparator : TEXT(""),
			lpcfgType->sz);

		tvis.item.pszText[cchTitle] = TEXT('\0');

		break;

	case CET_SUBSECTION:
		cchTitle = ConfigGetStringLength(lpcfgType->lpcfgSubsection, TEXT("title"));
		tvis.item.pszText = ProcHeapAlloc((cchTitle + 1) * sizeof(TCHAR));
		ConfigGetString(lpcfgType->lpcfgSubsection, TEXT("title"), tvis.item.pszText, cchTitle + 1);
		break;

	default:
		break;
	}

	/* Set up some properties for the new node. */
	tvis.hParent = lpttd->htreeitemParent;
	tvis.hInsertAfter = TVI_SORT;
	tvis.item.mask = TVIF_TEXT | TVIF_PARAM;
	tvis.item.lParam = _ttoi(lpcfgType->szName);

	/* Create the node. */
	htreeitemNew = TreeView_InsertItem(lpttd->hwndTree, &tvis);

	/* Add the node to the lookup table. */
	ConfigSetInteger(lpttd->lpcfgTypeToNode, lpcfgType->szName, (int)htreeitemNew);

	/* Clean up. */
	ProcHeapFree(tvis.item.pszText);

	return TRUE;
}


/* SetTexturePreviewImage
 *   Sets the image of a static control to that of the specified texture.
 *
 * Parameters:
 *   HWND		hwndMap		Handle to map window.
 *   HWND		hwndCtrl	Handle to static control.
 *   LPCTSTR	szTexName	Name of texture.
 *   TEX_FORMAT	tf			Texture format.
 *   HBITMAP	hbmPreview	Bitmap object used by the control.
 *   BOOL		bRequired	Whether the texture is required. Only used for
 *							TF_TEXTURE.
 *
 * Return value: None.
 */
void SetTexturePreviewImage(HWND hwndMap, HWND hwndCtrl, LPCTSTR szTexName, TEX_FORMAT tf, HBITMAP hbmPreview, BOOL bRequired)
{
	TEXTURE *lptex;
	BOOL bNeedFree = GetTextureForMap(hwndMap, szTexName, &lptex, tf);
	HDC hdcPreview;

	hdcPreview = CreateCompatibleDC(NULL);
	SelectObject(hdcPreview, hbmPreview);

	if(lptex)
	{
		StretchTextureToDC(lptex, hdcPreview, CX_TEXPREVIEW, CY_TEXPREVIEW, STT_MASKED);

		/* Free memory if necessary. */
		if(bNeedFree) DestroyTexture(lptex);
	}
	else
	{
		HBITMAP hbmSource;
		HDC		hdcSource;
		INT		iBitmapID;

		if(tf == TF_TEXTURE)
		{
			/* If it's a pseudo-texture, draw the blank image. */
			if(IsPseudoTexture(szTexName))
				iBitmapID = IDB_NOTEXTURE;

			/* If it's null, draw the blank image. Remember that in this case,
			 * we leave the sidedef alone -- we never actually *set* a texture
			 * to the null texture.
			 */
			else if(!*szTexName)
				iBitmapID = IDB_NOTEXTURE;

			/* If it's the blank texture, draw the blank image *unless* the
			 * texture is required, in which case use 'Missing Texture'.
			 */
			else if(IsBlankTexture(szTexName))
				iBitmapID = bRequired ? IDB_MISSINGTEXTURE : IDB_NOTEXTURE;

			else iBitmapID = IDB_UNKNOWNTEXTURE;
		}
		/* Handle pseudo-flats, and unknown flats. */
		else if(tf == TF_FLAT)
		{
			/* If it's null or a pseudoflat, draw blank. */
			iBitmapID = (IsPseudoFlat(szTexName) || !*szTexName) ? IDB_NOTEXTURE : IDB_UNKNOWNFLAT;
		}
		/* Finally, unknown images. */
		else iBitmapID = *szTexName ? IDB_UNKNOWNIMAGE : IDB_NOTEXTURE;

		hbmSource = LoadBitmap(g_hInstance, MAKEINTRESOURCE(iBitmapID));
		hdcSource = CreateCompatibleDC(NULL);
		SelectObject(hdcSource, hbmSource);

		/* We don't stretch resource bitmaps. */
		BitBlt(hdcPreview, 0, 0, CX_TEXPREVIEW, CY_TEXPREVIEW, hdcSource, 0, 0, SRCCOPY);

		DeleteDC(hdcSource);
		DeleteObject(hbmSource);
	}

	DeleteDC(hdcPreview);

	InvalidateRect(hwndCtrl, NULL, FALSE);
}


/* GetFlagsFromListView
 *   Obtains the values of flags from a 3-state checkbox list view, and also a
 *   mask indicating which of these flags are valid.
 *
 * Parameters:
 *   HWND	hwndListView	Handle to list control.
 *   WORD	*lpwFlagValues	Used to return values of shared flags.
 *   WORD	*lpwFlagMask	Used to return which of the flags in the
 *							above are valid.
 *
 * Return value: None.
 */
static void GetFlagsFromListView(HWND hwndListView, WORD *lpwFlagValues, WORD *lpwFlagMask)
{
	int iCountItems, i;

	/* Zero the flags and mask to begin with. */
	*lpwFlagMask = *lpwFlagValues = 0;

	/* How many flags in our list? */
	iCountItems = ListView_GetItemCount(hwndListView);

	/* Repeat for each flag. */
	for(i = 0; i < iCountItems; i++)
	{
		int iState = ListView3StateGetItemState(hwndListView, i);

		/* If the checkbox is on or off (but not mixed), we set the flag if
		 * necessary, and indicate in the mask that the bit is valid.
		 */
		if(iState != LV3_INDETERMINATE)
		{
			LVITEM lvitem;

			/* Get the flag value. */
			lvitem.iItem = i;
			lvitem.iSubItem = 0;
			lvitem.mask = LVIF_PARAM;
			ListView_GetItem(hwndListView, &lvitem);

			/* This bit is valid, so set the bit in the mask. */
			*lpwFlagMask |= (WORD)lvitem.lParam;

			/* If the checkbox is ticked, we also set the bit in the flags
			 * themselves. No need to reset the bit if the box is unticked,
			 * since we zeroed everything at the start.
			 */
			if(iState == LV3_CHECKED) *lpwFlagValues |= (WORD)lvitem.lParam;
		}
	}
}


/* InsertSectorDlg
 *   Dialogue proc for Insert Sector dialouge box.
 *
 * Parameters:
 *   HWND					hwndParent	Parent for dialogue.
 *   INSERTSECTORDLGDATA*	lpisdd		Pointer to structure in which to return
 *										the options specified by the user.
 *
 * Return value: BOOL
 *   FALSE if cancelled; TRUE otherwise.
 */
BOOL InsertSectorDlg(HWND hwndParent, INSERTSECTORDLGDATA *lpisdd)
{
	return DialogBoxParam(g_hInstance, MAKEINTRESOURCE(IDD_INSERTSECTOR), hwndParent, InsertSectorDlgProc, (LPARAM)lpisdd);
}



/* InsertSectorDlgProc
 *   Dialogue proc for Insert Sector dialouge box.
 *
 * Parameters:
 *   HWND	hwndDlg		Dialogue window handle.
 *   UINT	uiMsg		Message identifier.
 *   WPARAM wParam		Message-specific.
 *   LPARAM lParam		Message-specific.
 *
 * Return value: INT_PTR
 *   TRUE if the message was processed; FALSE otherwise.
 */
static INT_PTR CALLBACK InsertSectorDlgProc(HWND hwndDlg, UINT uiMsg, WPARAM wParam, LPARAM lParam)
{
	static INSERTSECTORDLGDATA *s_lpisdd;

	switch(uiMsg)
	{
	case WM_INITDIALOG:
		{
			UDACCEL udaccelEdges[1], udaccelRadius[2];

			/* Centre in parent. */
			CentreWindowInParent(hwndDlg);

			/* Get the structure to fill. Its radius and snapping fields are
			 * used to initialise the dialogue, but not the others.
			 */
			s_lpisdd = (INSERTSECTORDLGDATA*)lParam;

			/* Set up the spinner controls. */

			udaccelRadius[0].nInc = 8;
			udaccelRadius[0].nSec = 0;
			udaccelRadius[1].nInc = 64;
			udaccelRadius[1].nSec = 2;

			udaccelEdges[0].nInc = 1;
			udaccelEdges[0].nSec = 0;

			SendDlgItemMessage(hwndDlg, IDC_SPIN_EDGES, UDM_SETRANGE32, EDGESMIN, EDGESMAX);
			SendDlgItemMessage(hwndDlg, IDC_SPIN_EDGES, UDM_SETACCEL, 1, (LPARAM)udaccelEdges);

			SendDlgItemMessage(hwndDlg, IDC_SPIN_RADIUS, UDM_SETRANGE32, RADIUSMIN, RADIUSMAX);
			SendDlgItemMessage(hwndDlg, IDC_SPIN_RADIUS, UDM_SETACCEL, 2, (LPARAM)udaccelRadius);

			/* Initialise edges and radius editboxes. */
			SetDlgItemInt(hwndDlg, IDC_EDIT_EDGES, EDGESINIT, FALSE);
			SetDlgItemInt(hwndDlg, IDC_EDIT_RADIUS, s_lpisdd->nRadius, FALSE);


			/* Use the structure's snapping parameter to set the initial state
			 * of the checkbox.
			 */
			CheckDlgButton(hwndDlg, IDC_CHECK_SNAP, s_lpisdd->bSnap);

			/* Assume radius measured to edges. Good for squares. */
			CheckRadioButton(hwndDlg, IDC_RADIO_EDGES, IDC_RADIO_VERTICES, IDC_RADIO_EDGES);
		}

		/* Let the system set the focus. */
		return TRUE;

	case WM_COMMAND:
		switch(LOWORD(wParam))
		{
		case IDOK:
			/* Set the structure fields. */
			s_lpisdd->nEdges = GetDlgItemInt(hwndDlg, IDC_EDIT_EDGES, NULL, FALSE);
			s_lpisdd->nRadius = GetDlgItemInt(hwndDlg, IDC_EDIT_RADIUS, NULL, FALSE);
			s_lpisdd->bSnap = IsDlgButtonChecked(hwndDlg, IDC_CHECK_SNAP) != BST_UNCHECKED;
			s_lpisdd->cRadiusType = (IsDlgButtonChecked(hwndDlg, IDC_RADIO_EDGES) != BST_UNCHECKED) ? RT_TOEDGES : RT_TOVERTICES;

			/* Signal that we OKed. */
			EndDialog(hwndDlg, TRUE);

			return TRUE;

		case IDCANCEL:
			/* Signal that we cancelled. */
			EndDialog(hwndDlg, FALSE);

			return TRUE;

		case IDC_EDIT_EDGES:
			if(HIWORD(wParam) == EN_KILLFOCUS) BoundEditBox((HWND)lParam, EDGESMIN, EDGESMAX, FALSE, FALSE);
			return TRUE;

		case IDC_EDIT_RADIUS:
			if(HIWORD(wParam) == EN_KILLFOCUS) BoundEditBox((HWND)lParam, RADIUSMIN, RADIUSMAX, FALSE, FALSE);
			return TRUE;
		}

		break;
	}

	/* Didn't process message. */
	return FALSE;
}


/* InsertThingDlg
 *   Dialogue proc for Insert Things Radially dialouge box.
 *
 * Parameters:
 *   HWND					hwndParent	Parent for dialogue.
 *   INSERTTHINGDLGDATA*	lptsdd		Pointer to structure in which to return
 *										the options specified by the user.
 *
 * Return value: BOOL
 *   FALSE if cancelled; TRUE otherwise.
 */
BOOL InsertThingDlg(HWND hwndParent, INSERTTHINGDLGDATA *lpitdd)
{
	return DialogBoxParam(g_hInstance, MAKEINTRESOURCE(IDD_INSERTTHINGS), hwndParent, InsertThingDlgProc, (LPARAM)lpitdd);
}



/* InsertThingDlgProc
 *   Dialogue proc for Insert Thing dialouge box.
 *
 * Parameters:
 *   HWND	hwndDlg		Dialogue window handle.
 *   UINT	uiMsg		Message identifier.
 *   WPARAM wParam		Message-specific.
 *   LPARAM lParam		Message-specific.
 *
 * Return value: INT_PTR
 *   TRUE if the message was processed; FALSE otherwise.
 */
static INT_PTR CALLBACK InsertThingDlgProc(HWND hwndDlg, UINT uiMsg, WPARAM wParam, LPARAM lParam)
{
	static INSERTTHINGDLGDATA *s_lpitdd;

	switch(uiMsg)
	{
	case WM_INITDIALOG:
		{
			UDACCEL udaccelNum[1], udaccelRadius[2];

			/* Centre in parent. */
			CentreWindowInParent(hwndDlg);

			/* Get the structure to fill. Its radius and snapping fields are
			 * used to initialise the dialogue, but not the others.
			 */
			s_lpitdd = (INSERTTHINGDLGDATA*)lParam;

			/* Set up the spinner controls. */

			udaccelRadius[0].nInc = 8;
			udaccelRadius[0].nSec = 0;
			udaccelRadius[1].nInc = 64;
			udaccelRadius[1].nSec = 2;

			udaccelNum[0].nInc = 1;
			udaccelNum[0].nSec = 0;

			SendDlgItemMessage(hwndDlg, IDC_SPIN_NUMTHINGS, UDM_SETRANGE32, EDGESMIN, EDGESMAX);
			SendDlgItemMessage(hwndDlg, IDC_SPIN_NUMTHINGS, UDM_SETACCEL, NUM_ELEMENTS(udaccelNum), (LPARAM)udaccelNum);

			SendDlgItemMessage(hwndDlg, IDC_SPIN_RADIUS, UDM_SETRANGE32, RADIUSMIN, RADIUSMAX);
			SendDlgItemMessage(hwndDlg, IDC_SPIN_RADIUS, UDM_SETACCEL, NUM_ELEMENTS(udaccelRadius), (LPARAM)udaccelRadius);

			SendDlgItemMessage(hwndDlg, IDC_SPIN_THING, UDM_SETRANGE32, THINGMIN, THINGMAX);
			SendDlgItemMessage(hwndDlg, IDC_SPIN_THING, UDM_SETACCEL, NUM_ELEMENTS(udaccelNum), (LPARAM)udaccelNum);

			/* Initialise editboxes. */
			SetDlgItemInt(hwndDlg, IDC_EDIT_NUMTHINGS, NUMTHINGSINIT, FALSE);
			SetDlgItemInt(hwndDlg, IDC_EDIT_RADIUS, s_lpitdd->nRadius, FALSE);
			SetDlgItemInt(hwndDlg, IDC_EDIT_THING, s_lpitdd->uiThing, FALSE);


			/* Use the structure's snapping parameter to set the initial state
			 * of the checkbox.
			 */
			CheckDlgButton(hwndDlg, IDC_CHECK_SNAP, s_lpitdd->bSnap);
		}

		/* Let the system set the focus. */
		return TRUE;

	case WM_COMMAND:
		switch(LOWORD(wParam))
		{
		case IDOK:
			/* Set the structure fields. */
			s_lpitdd->nThings = GetDlgItemInt(hwndDlg, IDC_EDIT_NUMTHINGS, NULL, FALSE);
			s_lpitdd->nRadius = GetDlgItemInt(hwndDlg, IDC_EDIT_RADIUS, NULL, FALSE);
			s_lpitdd->uiThing = GetDlgItemInt(hwndDlg, IDC_EDIT_THING, NULL, FALSE);
			s_lpitdd->bSnap = IsDlgButtonChecked(hwndDlg, IDC_CHECK_SNAP) != BST_UNCHECKED;

			/* Signal that we OKed. */
			EndDialog(hwndDlg, TRUE);

			return TRUE;

		case IDCANCEL:
			/* Signal that we cancelled. */
			EndDialog(hwndDlg, FALSE);

			return TRUE;

		case IDC_BUTTON_SELECT:
			{
				int iValue = SelectThingDlg(hwndDlg, s_lpitdd->lpcfgMap, GetDlgItemInt(hwndDlg, IDC_EDIT_THING, NULL, FALSE));

				if(iValue >= 0)
					SetDlgItemInt(hwndDlg, IDC_EDIT_THING, iValue, FALSE);
			}

			return TRUE;

		case IDC_EDIT_NUMTHINGS:
			if(HIWORD(wParam) == EN_KILLFOCUS) BoundEditBox((HWND)lParam, NUMTHINGSMIN, NUMTHINGSMAX, FALSE, FALSE);
			return TRUE;

		case IDC_EDIT_RADIUS:
			if(HIWORD(wParam) == EN_KILLFOCUS) BoundEditBox((HWND)lParam, RADIUSMIN, RADIUSMAX, FALSE, FALSE);
			return TRUE;

		case IDC_EDIT_THING:
			if(HIWORD(wParam) == EN_KILLFOCUS) BoundEditBox((HWND)lParam, THINGMIN, THINGMAX, FALSE, FALSE);
			return TRUE;
		}

		break;
	}

	/* Didn't process message. */
	return FALSE;
}


/* RotateDlg
 *   Displays the Rotate dialouge box.
 *
 * Parameters:
 *   HWND				hwndParent	Parent for dialogue.
 *   ROTATEDLGDATA*		lprotdd		Pointer to structure in which to return the
 *									options specified by the user, and used to
 *									determine whether the rotate-about-selected-
 *									thing/vertex options should be enabled.
 *
 * Return value: BOOL
 *   FALSE if cancelled; TRUE otherwise.
 */
BOOL RotateDlg(HWND hwndParent, ROTATEDLGDATA *lprotdd)
{
	return DialogBoxParam(g_hInstance, MAKEINTRESOURCE(IDD_ROTATE), hwndParent, RotateDlgProc, (LPARAM)lprotdd);
}


/* RotateDlgProc
 *   Dialogue proc for Rotate dialouge box.
 *
 * Parameters:
 *   HWND	hwndDlg		Dialogue window handle.
 *   UINT	uiMsg		Message identifier.
 *   WPARAM wParam		Message-specific.
 *   LPARAM lParam		Message-specific.
 *
 * Return value: INT_PTR
 *   TRUE if the message was processed; FALSE otherwise.
 */
static INT_PTR CALLBACK RotateDlgProc(HWND hwndDlg, UINT uiMsg, WPARAM wParam, LPARAM lParam)
{
	static ROTATEDLGDATA *s_lprotdd;

	switch(uiMsg)
	{
	case WM_INITDIALOG:
		{
			UDACCEL udaccel[2];

			/* Centre in parent. */
			CentreWindowInParent(hwndDlg);

			/* Get the structure in which we return parameters, and from which
			 * we determine whether to enable some radio buttons.
			 */
			s_lprotdd = (ROTATEDLGDATA*)lParam;

			/* Set up the spinner control. */

			udaccel[0].nInc = 1;
			udaccel[0].nSec = 0;
			udaccel[1].nInc = 15;
			udaccel[1].nSec = 2;

			/* The range is enforced manually, but this is still necessary. */
			SendDlgItemMessage(hwndDlg, IDC_SPIN_ANGLE, UDM_SETRANGE32, ANGLEMIN, ANGLEMAX);
			SendDlgItemMessage(hwndDlg, IDC_SPIN_ANGLE, UDM_SETACCEL, sizeof(udaccel)/sizeof(UDACCEL), (LPARAM)udaccel);

			/* Initial angle. */
			SetDlgItemInt(hwndDlg, IDC_EDIT_ANGLE, 0, TRUE);

			/* Disable the thing/vertex radio buttons and checkboxes if
			 * necessary.
			 */
			EnableWindow(GetDlgItem(hwndDlg, IDC_RADIO_FIRSTVERTEX), s_lprotdd->bVerticesSelected);
			EnableWindow(GetDlgItem(hwndDlg, IDC_RADIO_FIRSTTHING), s_lprotdd->bThingsSelected);
			EnableWindow(GetDlgItem(hwndDlg, IDC_CHECK_THINGANGLE), s_lprotdd->bThingsSelected);

			/* Assume bounding box and thing-angle alteration. */
			CheckRadioButton(hwndDlg, IDC_RADIO_CENTREBB, IDC_RADIO_CENTREBB, IDC_RADIO_CENTREBB);
			CheckDlgButton(hwndDlg, IDC_CHECK_THINGANGLE, BST_CHECKED);
		}

		/* Let the system set the focus. */
		return TRUE;

	case WM_COMMAND:
		switch(LOWORD(wParam))
		{
		case IDOK:
			/* Set the structure fields. */
			s_lprotdd->fAngle = GetDlgItemFloat(hwndDlg, IDC_EDIT_ANGLE, NULL);
			s_lprotdd->bSnap = IsDlgButtonChecked(hwndDlg, IDC_CHECK_SNAP) != BST_UNCHECKED;
			s_lprotdd->bAlterThingAngles = IsDlgButtonChecked(hwndDlg, IDC_CHECK_THINGANGLE) != BST_UNCHECKED;

			if(IsDlgButtonChecked(hwndDlg, IDC_RADIO_CENTREBB) != BST_UNCHECKED)
				s_lprotdd->cCentreType = CT_BOUNDINGBOX;
			else if(IsDlgButtonChecked(hwndDlg, IDC_RADIO_FIRSTVERTEX) != BST_UNCHECKED)
				s_lprotdd->cCentreType = CT_VERTEX;
			else
				s_lprotdd->cCentreType = CT_THING;

			/* Signal that we OKed. */
			EndDialog(hwndDlg, TRUE);

			return TRUE;

		case IDCANCEL:
			/* Signal that we cancelled. */
			EndDialog(hwndDlg, FALSE);

			return TRUE;

		case IDC_EDIT_ANGLE:
			switch(HIWORD(wParam))
			{
			case EN_KILLFOCUS:
				BoundEditBoxFloat((HWND)lParam, ANGLEMIN, ANGLEMAX, FALSE, FALSE);
				return TRUE;

			case EN_CHANGE:
				/* Necessary to support fractional values properly. */
				SendDlgItemMessage(hwndDlg, IDC_SPIN_ANGLE, UDM_SETPOS32, 0, GetDlgItemInt(hwndDlg, IDC_EDIT_ANGLE, NULL, TRUE));
				return TRUE;
			}

			break;
		}

		break;

	case WM_NOTIFY:
		switch(((LPNMHDR)lParam)->idFrom)
		{
		case IDC_SPIN_ANGLE:
			{
				NMUPDOWN *lpnmud = (NMUPDOWN*)lParam;

				/* Update the edit control. */
				if(lpnmud->hdr.code == UDN_DELTAPOS)
				{
					SetDlgItemInt(hwndDlg, IDC_EDIT_ANGLE, max(ANGLEMIN, min(ANGLEMAX, lpnmud->iPos + lpnmud->iDelta)), TRUE);

					/* *Don't* allow the control to change itself, since we set
					 * its value ourselves in response to EN_CHANGE.
					 */
					SetWindowLong(hwndDlg, DWL_MSGRESULT, TRUE);

					return TRUE;
				}
			}
		}
	}

	/* Didn't process message. */
	return FALSE;
}


/* ResizeDlg
 *   Displays the Resize dialogue box.
 *
 * Parameters:
 *   HWND				hwndParent	Parent for dialogue.
 *   RESIZEDLGDATA*		lprszdd		Pointer to structure in which to return the
 *									options specified by the user, and used to
 *									determine which fixed-point options should
 *									be enabled.
 *
 * Return value: BOOL
 *   FALSE if cancelled; TRUE otherwise.
 */
BOOL ResizeDlg(HWND hwndParent, RESIZEDLGDATA *lprszdd)
{
	return DialogBoxParam(g_hInstance, MAKEINTRESOURCE(IDD_RESIZE), hwndParent, ResizeDlgProc, (LPARAM)lprszdd);
}


/* ResizeDlgProc
 *   Dialogue proc for Resize dialouge box.
 *
 * Parameters:
 *   HWND	hwndDlg		Dialogue window handle.
 *   UINT	uiMsg		Message identifier.
 *   WPARAM wParam		Message-specific.
 *   LPARAM lParam		Message-specific.
 *
 * Return value: INT_PTR
 *   TRUE if the message was processed; FALSE otherwise.
 */
static INT_PTR CALLBACK ResizeDlgProc(HWND hwndDlg, UINT uiMsg, WPARAM wParam, LPARAM lParam)
{
	static RESIZEDLGDATA *s_lprszdd;

	switch(uiMsg)
	{
	case WM_INITDIALOG:
		{
			UDACCEL udaccel[2];

			/* Centre in parent. */
			CentreWindowInParent(hwndDlg);

			/* Get the structure in which we return parameters, and from which
			 * we determine whether to enable some radio buttons.
			 */
			s_lprszdd = (RESIZEDLGDATA*)lParam;

			/* Set up the spinner control. */

			udaccel[0].nInc = 1;
			udaccel[0].nSec = 0;
			udaccel[1].nInc = 25;
			udaccel[1].nSec = 2;

			/* The range is enforced manually, but this is still necessary. */
			s_lprszdd->iMaxRatio = min(RESIZEFACTORMAX, s_lprszdd->iMaxRatio);
			SendDlgItemMessage(hwndDlg, IDC_SPIN_FACTOR, UDM_SETRANGE32, RESIZEFACTORMIN, s_lprszdd->iMaxRatio);
			SendDlgItemMessage(hwndDlg, IDC_SPIN_FACTOR, UDM_SETACCEL, sizeof(udaccel)/sizeof(UDACCEL), (LPARAM)udaccel);


			/* Disable the thing/vertex radio buttons and checkboxes if
			 * necessary.
			 */
			EnableWindow(GetDlgItem(hwndDlg, IDC_RADIO_FIRSTVERTEX), s_lprszdd->bVerticesSelected);
			EnableWindow(GetDlgItem(hwndDlg, IDC_RADIO_FIRSTTHING), s_lprszdd->bThingsSelected);

			/* Assume bounding box. */
			CheckRadioButton(hwndDlg, IDC_RADIO_CENTREBB, IDC_RADIO_FIRSTTHING, IDC_RADIO_CENTREBB);

			/* Set initial factor. */
			SetDlgItemInt(hwndDlg, IDC_EDIT_FACTOR, RESIZEINIT, FALSE);
		}

		/* Let the system set the focus. */
		return TRUE;

	case WM_COMMAND:
		switch(LOWORD(wParam))
		{
		case IDOK:
			/* Set the structure fields. */
			s_lprszdd->fFactor = GetDlgItemFloat(hwndDlg, IDC_EDIT_FACTOR, NULL);
			s_lprszdd->bSnap = IsDlgButtonChecked(hwndDlg, IDC_CHECK_SNAP) != BST_UNCHECKED;

			if(IsDlgButtonChecked(hwndDlg, IDC_RADIO_CENTREBB) != BST_UNCHECKED)
				s_lprszdd->cCentreType = CT_BOUNDINGBOX;
			else if(IsDlgButtonChecked(hwndDlg, IDC_RADIO_FIRSTVERTEX) != BST_UNCHECKED)
				s_lprszdd->cCentreType = CT_VERTEX;
			else
				s_lprszdd->cCentreType = CT_THING;

			/* Signal that we OKed. */
			EndDialog(hwndDlg, TRUE);

			return TRUE;

		case IDCANCEL:
			/* Signal that we cancelled. */
			EndDialog(hwndDlg, FALSE);

			return TRUE;

		case IDC_EDIT_FACTOR:
			switch(HIWORD(wParam))
			{
			case EN_KILLFOCUS:
				BoundEditBoxFloat((HWND)lParam, RESIZEFACTORMIN, (float)s_lprszdd->iMaxRatio, FALSE, FALSE);
				return TRUE;

			case EN_CHANGE:
				/* Necessary to support fractional values properly. */
				SendDlgItemMessage(hwndDlg, IDC_SPIN_FACTOR, UDM_SETPOS32, 0, GetDlgItemInt(hwndDlg, IDC_EDIT_FACTOR, NULL, FALSE));
				return TRUE;
			}

			break;
		}

		break;

	case WM_NOTIFY:
		switch(((LPNMHDR)lParam)->idFrom)
		{
		case IDC_SPIN_FACTOR:
			{
				NMUPDOWN *lpnmud = (NMUPDOWN*)lParam;

				/* Update the edit control. */
				if(lpnmud->hdr.code == UDN_DELTAPOS)
				{
					SetDlgItemInt(hwndDlg, IDC_EDIT_FACTOR, max(RESIZEFACTORMIN, min(s_lprszdd->iMaxRatio, lpnmud->iPos + lpnmud->iDelta)), FALSE);

					/* *Don't* allow the control to change itself, since we set
					 * its value ourselves in response to EN_CHANGE.
					 */
					SetWindowLong(hwndDlg, DWL_MSGRESULT, TRUE);

					return TRUE;
				}
			}
		}

		break;
	}

	/* Didn't process message. */
	return FALSE;
}


/* SelectPresetTypes
 *   Displays the Resize dialogue box.
 *
 * Parameters:
 *   HWND	hwndParent			Parent for dialogue.
 *   BYTE	byObjectTypeFlags	Flags specifying which checkboxes to enable. See
 *								ENUM_PRESET_OBJECT_FLAGS for values.
 *
 * Return value: BYTE
 *   Flags specifying which types were selected, or zero if cancelled.
 */
BYTE SelectPresetTypes(HWND hwndParent, BYTE byObjectFlags)
{
	return (BYTE)DialogBoxParam(g_hInstance, MAKEINTRESOURCE(IDD_PASTEMULTIPROP), hwndParent, SelectPresetTypesDlgProc, (LPARAM)byObjectFlags);
}

/* SelectPresetTypesDlgProc
 *   Dialogue proc for TEXT("Apply Properties") dialouge box.
 *
 * Parameters:
 *   HWND	hwndDlg		Dialogue window handle.
 *   UINT	uiMsg		Message identifier.
 *   WPARAM wParam		Message-specific.
 *   LPARAM lParam		Message-specific.
 *
 * Return value: INT_PTR
 *   TRUE if the message was processed; FALSE otherwise.
 */
static INT_PTR CALLBACK SelectPresetTypesDlgProc(HWND hwndDlg, UINT uiMsg, WPARAM wParam, LPARAM lParam)
{
	switch(uiMsg)
	{
	case WM_INITDIALOG:
		{
			/* Centre in parent. */
			CentreWindowInParent(hwndDlg);

			/* Disable non-applicable checkboxes. */
			if(!(lParam & POF_SECTOR)) EnableWindow(GetDlgItem(hwndDlg, IDC_CHECK_SECTORS), FALSE);
			else CheckDlgButton(hwndDlg, IDC_CHECK_SECTORS, BST_CHECKED);

			if(!(lParam & POF_LINE)) EnableWindow(GetDlgItem(hwndDlg, IDC_CHECK_LINES), FALSE);
			else CheckDlgButton(hwndDlg, IDC_CHECK_LINES, BST_CHECKED);

			if(!(lParam & POF_THING)) EnableWindow(GetDlgItem(hwndDlg, IDC_CHECK_THINGS), FALSE);
			else CheckDlgButton(hwndDlg, IDC_CHECK_THINGS, BST_CHECKED);
		}

		/* Let the system set the focus. */
		return TRUE;

	case WM_COMMAND:
		switch(LOWORD(wParam))
		{
		case IDOK:
			{
				BYTE byObjectFlags = 0;

				/* Set flags corresponding to selected checkboxes. */
				if(IsDlgButtonChecked(hwndDlg, IDC_CHECK_SECTORS) == BST_CHECKED) byObjectFlags |= POF_SECTOR;
				if(IsDlgButtonChecked(hwndDlg, IDC_CHECK_LINES) == BST_CHECKED) byObjectFlags |= POF_LINE;
				if(IsDlgButtonChecked(hwndDlg, IDC_CHECK_THINGS) == BST_CHECKED) byObjectFlags |= POF_THING;

				/* Signal that we OKed. */
				EndDialog(hwndDlg, byObjectFlags);
			}

			return TRUE;

		case IDCANCEL:
			/* Signal that we cancelled - equivalent to clearing all the checkboxes. */
			EndDialog(hwndDlg, 0);
			return TRUE;
		}

		break;
	}

	/* Didn't process message. */
	return FALSE;
}


/* AlignDlg
 *   Displays the TEXT("Align Textures") dialogue box.
 *
 * Parameters:
 *   HWND				hwndParent	Parent for dialogue.
 *   ALIGNDLGDATA*		lpaligndd	Pointer to structure in which to return the
 *									options specified by the user, and which
 *									specifies some controls' initial states.
 *
 * Return value: BOOL
 *   FALSE if cancelled; TRUE otherwise.
 */
BOOL AlignDlg(HWND hwndParent, ALIGNDLGDATA *lpaligndd)
{
	return DialogBoxParam(g_hInstance, MAKEINTRESOURCE(IDD_ALIGN), hwndParent, AlignDlgProc, (LPARAM)lpaligndd);
}


/* AlignDlgProc
 *   Dialogue proc for TEXT("Align Textures") dialouge box.
 *
 * Parameters:
 *   HWND	hwndDlg		Dialogue window handle.
 *   UINT	uiMsg		Message identifier.
 *   WPARAM wParam		Message-specific.
 *   LPARAM lParam		Message-specific.
 *
 * Return value: INT_PTR
 *   TRUE if the message was processed; FALSE otherwise.
 */
static INT_PTR CALLBACK AlignDlgProc(HWND hwndDlg, UINT uiMsg, WPARAM wParam, LPARAM lParam)
{
	static ALIGNDLGDATA *s_lpaligndd;

	switch(uiMsg)
	{
	case WM_INITDIALOG:
		{
			/* Centre in parent. */
			CentreWindowInParent(hwndDlg);

			/* Get the structure in which we return parameters, and from which
			 * we determine whether to enable and select some radio buttons and
			 * checkboxes.
			 */
			s_lpaligndd = (ALIGNDLGDATA*)lParam;

			/* Disable the thing/vertex radio buttons and checkboxes if
			 * necessary.
			 */
			EnableWindow(GetDlgItem(hwndDlg, IDC_RADIO_THISLINE), s_lpaligndd->bFromThisLine);
			EnableWindow(GetDlgItem(hwndDlg, IDC_CHECK_INSELECTION), s_lpaligndd->bInSelection);

			/* Assume aligning from any linedefs. */
			CheckRadioButton(hwndDlg, IDC_RADIO_ANYLINE, IDC_RADIO_THISLINE, IDC_RADIO_ANYLINE);

			/* All the texture checkboxes are selected by default. */
			CheckDlgButton(hwndDlg, IDC_CHECK_FRONTUPPER, BST_CHECKED);
			CheckDlgButton(hwndDlg, IDC_CHECK_FRONTMIDDLE, BST_CHECKED);
			CheckDlgButton(hwndDlg, IDC_CHECK_FRONTLOWER, BST_CHECKED);
			CheckDlgButton(hwndDlg, IDC_CHECK_BACKUPPER, BST_CHECKED);
			CheckDlgButton(hwndDlg, IDC_CHECK_BACKMIDDLE, BST_CHECKED);
			CheckDlgButton(hwndDlg, IDC_CHECK_BACKLOWER, BST_CHECKED);

			/* If we have a selection, assume we search only within it. */
			if(s_lpaligndd->bInSelection)
				CheckDlgButton(hwndDlg, IDC_CHECK_INSELECTION, BST_CHECKED);
		}

		/* Let the system set the focus. */
		return TRUE;

	case WM_COMMAND:
		switch(LOWORD(wParam))
		{
		case IDOK:
			/* Set the structure fields. */
			s_lpaligndd->bInSelection = IsDlgButtonChecked(hwndDlg, IDC_CHECK_INSELECTION) != BST_UNCHECKED;
			s_lpaligndd->bFromThisLine = IsDlgButtonChecked(hwndDlg, IDC_RADIO_THISLINE) != BST_UNCHECKED;

			/* Texture flags. */
			s_lpaligndd->byTexFlags = 0;

			if(IsDlgButtonChecked(hwndDlg, IDC_CHECK_FRONTUPPER) != BST_UNCHECKED)
				s_lpaligndd->byTexFlags |= LDTF_FRONTUPPER;

			if(IsDlgButtonChecked(hwndDlg, IDC_CHECK_FRONTMIDDLE) != BST_UNCHECKED)
				s_lpaligndd->byTexFlags |= LDTF_FRONTMIDDLE;

			if(IsDlgButtonChecked(hwndDlg, IDC_CHECK_FRONTLOWER) != BST_UNCHECKED)
				s_lpaligndd->byTexFlags |= LDTF_FRONTLOWER;

			if(IsDlgButtonChecked(hwndDlg, IDC_CHECK_BACKUPPER) != BST_UNCHECKED)
				s_lpaligndd->byTexFlags |= LDTF_BACKUPPER;

			if(IsDlgButtonChecked(hwndDlg, IDC_CHECK_BACKMIDDLE) != BST_UNCHECKED)
				s_lpaligndd->byTexFlags |= LDTF_BACKMIDDLE;

			if(IsDlgButtonChecked(hwndDlg, IDC_CHECK_BACKLOWER) != BST_UNCHECKED)
				s_lpaligndd->byTexFlags |= LDTF_BACKLOWER;

			/* Signal that we OKed. */
			EndDialog(hwndDlg, TRUE);

			return TRUE;

		case IDCANCEL:
			/* Signal that we cancelled. */
			EndDialog(hwndDlg, FALSE);

			return TRUE;
		}

		break;
	}

	/* Didn't process message. */
	return FALSE;
}


/* MissingTexDlg
 *   Displays the TEXT("Fix Missing Textures") dialogue box.
 *
 * Parameters:
 *   HWND				hwndParent	Parent for dialogue.
 *   MISSTEXDLGDATA*	lpmtdd		Pointer to structure in which to return the
 *									options specified by the user, and which
 *									specifies some controls' initial states.
 *
 * Return value: BOOL
 *   FALSE if cancelled; TRUE otherwise.
 */
BOOL MissingTexDlg(HWND hwndParent, MISSTEXDLGDATA *lpmtdd)
{
	return DialogBoxParam(g_hInstance, MAKEINTRESOURCE(IDD_MISSTEX), hwndParent, MissingTexProc, (LPARAM)lpmtdd);
}


/* MissingTexProc
 *   Dialogue proc for TEXT("Fix Missing Textures") dialouge box.
 *
 * Parameters:
 *   HWND	hwndDlg		Dialogue window handle.
 *   UINT	uiMsg		Message identifier.
 *   WPARAM wParam		Message-specific.
 *   LPARAM lParam		Message-specific.
 *
 * Return value: INT_PTR
 *   TRUE if the message was processed; FALSE otherwise.
 */
static INT_PTR CALLBACK MissingTexProc(HWND hwndDlg, UINT uiMsg, WPARAM wParam, LPARAM lParam)
{
	static MISSTEXDLGDATA *s_lpmtdd;
	static IAutoComplete2 *s_lpac2[6] = {NULL};
	static LPTEXENUM s_lptexenum[6] = {NULL};

	switch(uiMsg)
	{
	case WM_INITDIALOG:
		{
			/* Centre in parent. */
			CentreWindowInParent(hwndDlg);

			/* Get the structure in which we return parameters, and from which
			 * we determine whether to enable and select some radio buttons and
			 * checkboxes.
			 */
			s_lpmtdd = (MISSTEXDLGDATA*)lParam;

			/* Disable the thing/vertex radio buttons and checkboxes if
			 * necessary.
			 */
			EnableWindow(GetDlgItem(hwndDlg, IDC_CHECK_INSELECTION), s_lpmtdd->bInSelection);

			/* If we have a selection, assume we search only within it. */
			if(s_lpmtdd->bInSelection)
				CheckDlgButton(hwndDlg, IDC_CHECK_INSELECTION, BST_CHECKED);

			/* Get the default textures. */
			SetDlgItemText(hwndDlg, IDC_EDIT_UPPER, s_lpmtdd->szUpper);
			SetDlgItemText(hwndDlg, IDC_EDIT_MIDDLE, s_lpmtdd->szMiddle);
			SetDlgItemText(hwndDlg, IDC_EDIT_LOWER, s_lpmtdd->szLower);

			if(ConfigGetInteger(g_lpcfgMain, OPT_AUTOCOMPLETETEX))
			{
				int i;
				const int iEditIDs[] = {IDC_EDIT_UPPER, IDC_EDIT_MIDDLE, IDC_EDIT_LOWER};

				/* Create texture enumerators and shell autocomplete objects and
				 * associate them with the edit boxes.
				 */
				for(i = 0; i < 3; i++)
				{
					if((s_lptexenum[i] = CreateTexEnum()) && (s_lpac2[i] = CreateAutoComplete()))
					{
						SetTexEnumList(s_lptexenum[i], s_lpmtdd->lptnlAllTextures);
						s_lpac2[i]->lpVtbl->Init(s_lpac2[i], GetDlgItem(hwndDlg, iEditIDs[i]), (IUnknown*)s_lptexenum[i], NULL, NULL);
					}
				}
			}
		}

		/* Let the system set the focus. */
		return TRUE;

	case WM_COMMAND:
		switch(LOWORD(wParam))
		{
		case IDOK:
			/* Set the structure fields. */
			s_lpmtdd->bInSelection = IsDlgButtonChecked(hwndDlg, IDC_CHECK_INSELECTION) != BST_UNCHECKED;

			/* Texture names. */
			GetDlgItemText(hwndDlg, IDC_EDIT_UPPER, s_lpmtdd->szUpper, sizeof(s_lpmtdd->szUpper) / sizeof(TCHAR));
			GetDlgItemText(hwndDlg, IDC_EDIT_MIDDLE, s_lpmtdd->szMiddle, sizeof(s_lpmtdd->szMiddle) / sizeof(TCHAR));
			GetDlgItemText(hwndDlg, IDC_EDIT_LOWER, s_lpmtdd->szLower, sizeof(s_lpmtdd->szLower) / sizeof(TCHAR));

			/* Signal that we OKed. */
			EndDialog(hwndDlg, TRUE);

			return TRUE;

		case IDCANCEL:
			/* Signal that we cancelled. */
			EndDialog(hwndDlg, FALSE);

			return TRUE;

		case IDC_BUTTON_UPPER:
		case IDC_BUTTON_MIDDLE:
		case IDC_BUTTON_LOWER:
			{
				int iEditID;
				TCHAR szTexname[TEXNAME_BUFFER_LENGTH];

				if(LOWORD(wParam) == IDC_BUTTON_UPPER) iEditID = IDC_EDIT_UPPER;
				else if(LOWORD(wParam) == IDC_BUTTON_MIDDLE) iEditID = IDC_EDIT_MIDDLE;
				else iEditID = IDC_EDIT_LOWER;

				/* Get the initial texture name. */
				GetDlgItemText(hwndDlg, iEditID, szTexname, sizeof(szTexname)/sizeof(TCHAR));

				/* Show the dialogue, and check whether we OKed or cancelled. */
				if(SelectTexture(hwndDlg, s_lpmtdd->hwndMap, TF_TEXTURE, s_lpmtdd->lphimlCache, s_lpmtdd->lptnlAllTextures, s_lpmtdd->lpcfgUsedTextures, 0, szTexname))
					SetDlgItemText(hwndDlg, iEditID, szTexname);
			}

			return TRUE;
		}

		break;

	case WM_DESTROY:
		{
			int i;

			/* Destroy autocomplete objects. */
			for(i = 0; i < 3; i++)
			{
				if(s_lpac2[i]) s_lpac2[i]->lpVtbl->Release(s_lpac2[i]);
				if(s_lptexenum[i]) DestroyTexEnum(s_lptexenum[i]);
			}
		}

		break;
	}

	/* Didn't process message. */
	return FALSE;
}


/* SelectSectorEffectDlg
 *   Displays the Select Sector Effect dialogue, used by the Find/Replace
 *   dialogues.
 *
 * Parameters:
 *   HWND		hwndParent			Parent window.
 *   CONFIG*	lpcfgMap			Map configuration.
 *   int		iInitialSelection	Effect to select initially. Negative for
 *									none.
 *   int		iNybble				Index of nybble. Ignored if the specified
 *									map config doesn't use nybbles.
 *
 * Return value: int
 *   Value of selected sector effect, or negative if none selected or cancelled.
 */
int SelectSectorEffectDlg(HWND hwndParent, CONFIG *lpcfgMap, int iInitialSelection, int iNybble)
{
	FINDSELECTDLGDATA fsdd = {iInitialSelection, lpcfgMap, TRUE, iNybble};
	return DialogBoxParam(g_hInstance, MAKEINTRESOURCE(IDD_SELECT_SECEFFECT), hwndParent, SelectSectorEffectDlgProc, (LPARAM)&fsdd);
}


/* SelectSectorEffectDlgProc
 *   Dialogue proc for Select Sector effect dialogue, used by the Find/Replace
 *   dialogues.
 *
 * Parameters:
 *   HWND	hwndDlg		Dialogue window handle.
 *   UINT	uiMsg		Message identifier.
 *   WPARAM wParam		Message-specific.
 *   LPARAM lParam		Message-specific.
 *
 * Return value: INT_PTR
 *   TRUE if the message was processed; FALSE otherwise.
 */
static INT_PTR CALLBACK SelectSectorEffectDlgProc(HWND hwndDlg, UINT uiMsg, WPARAM wParam, LPARAM lParam)
{
	static FINDSELECTDLGDATA *s_lpfsdd;

	switch(uiMsg)
	{
	case WM_INITDIALOG:
		{
			CONFIG *lpcfgSecTypes;
			LVCOLUMN lvcol;
			LVFINDINFO lvfi;
			int iIndex;
			HWND hwndListEffect = GetDlgItem(hwndDlg, IDC_LIST_EFFECT);

			/* Centre in parent. */
			CentreWindowInParent(hwndDlg);

			/* Get the parameters. */
			s_lpfsdd = (FINDSELECTDLGDATA*)lParam;

			/* Sector types from map configuration. */
			lpcfgSecTypes = ConfigGetSubsection(s_lpfsdd->lpcfgMap, TEXT("sectortypes"));

			/* Fill the list of sector effects. */
			if(ConfigGetInteger(s_lpfsdd->lpcfgMap, TEXT("seceffectnybble")))
			{
				LPCTSTR szSubsections[] = {TEXT("nybble0"), TEXT("nybble1"), TEXT("nybble2"), TEXT("nybble3")};
				ConfigIterate(ConfigGetSubsection(lpcfgSecTypes, szSubsections[s_lpfsdd->iParam]), AddSectorTypeToList, GetDlgItem(hwndDlg, IDC_LIST_EFFECT));
			}
			else
				ConfigIterate(lpcfgSecTypes, AddSectorTypeToList, GetDlgItem(hwndDlg, IDC_LIST_EFFECT));

			/* Create a column in the sector effect list view control. */
			lvcol.mask = LVCF_SUBITEM;
			lvcol.iSubItem = 0;
			ListView_InsertColumn(hwndListEffect, 0, &lvcol);

			/* Full row select. */
			ListView_SetExtendedListViewStyleEx(hwndListEffect,
				LVS_EX_FULLROWSELECT | LVS_EX_INFOTIP,
				LVS_EX_FULLROWSELECT | LVS_EX_INFOTIP);

			/* Make the column big enough, but not *too* big. */
			ListView_SetColumnWidth(hwndListEffect, 0, LVSCW_AUTOSIZE_USEHEADER);

			/* Sort the list by effect number if that's what the user wants. */
			if(ConfigGetInteger(g_lpcfgMain, OPT_EFFECTNUMBERS))
				ListView_SortItems(hwndListEffect, EffectNumericalSort, 0);

			/* Select the specified effect, if it exists. */

			lvfi.flags = LVFI_PARAM;
			lvfi.lParam = s_lpfsdd->iSelection;

			/* Select corresponding listview item, or deselect if no
			 * match.
			 */
			iIndex = ListView_FindItem(hwndListEffect, -1, &lvfi);
			if(iIndex >= 0)
			{
				ListView_SetItemState(hwndListEffect, iIndex, LVIS_SELECTED, LVIS_SELECTED);
			}

			/* Focus on the list. */
			SendMessage(hwndDlg, WM_NEXTDLGCTL, (WPARAM)hwndListEffect, MAKELPARAM(TRUE, 0));
		}

		/* We set the focus. */
		return FALSE;

	case WM_NOTIFY:
		/* Double-clicking is equivalent to OKing. */
		if(((LPNMHDR)lParam)->idFrom == IDC_LIST_EFFECT && ((LPNMHDR)lParam)->code == (UINT)NM_DBLCLK)
		{
			SendMessage(hwndDlg, WM_COMMAND, IDOK, (LPARAM)GetDlgItem(hwndDlg, IDOK));
			return TRUE;
		}

		break;

	case WM_COMMAND:
		switch(LOWORD(wParam))
		{
		case IDOK:
			{
				/* Find whether we selected anything. */
				int iSelected = ListView_GetNextItem(GetDlgItem(hwndDlg, IDC_LIST_EFFECT), -1, LVIS_SELECTED);

				/* Assume nothing selected. */
				int iEffect = -1;

				if(iSelected >= 0)
				{
					LVITEM lvitem;
					lvitem.mask = LVIF_PARAM;
					lvitem.iItem = iSelected;
					lvitem.iSubItem = 0;

					ListView_GetItem(GetDlgItem(hwndDlg, IDC_LIST_EFFECT), &lvitem);
					iEffect = lvitem.lParam;
				}

				EndDialog(hwndDlg, iEffect);
			}

			return TRUE;

		case IDCANCEL:
			/* Signal that nothing was selected. */
			EndDialog(hwndDlg, -1);
			return TRUE;
		}

		break;
	}

	return FALSE;
}



/* SelectThingDlg
 *   Displays the Select Thing dialogue, used by the Find/Replace dialogues.
 *
 * Parameters:
 *   HWND		hwndParent			Parent window.
 *   CONFIG*	lpcfgMap			Map configuration.
 *   int		iInitialSelection	Item to select initially. Negative for
 *									none.
 *
 * Return value: int
 *   Value of selected thing, or negative if none selected or cancelled.
 */
int SelectThingDlg(HWND hwndParent, CONFIG *lpcfgMap, int iInitialSelection)
{
	/* TRUE for thing. */
	FINDSELECTDLGDATA fsdd = {iInitialSelection, lpcfgMap, TRUE, 0};
	return DialogBoxParam(g_hInstance, MAKEINTRESOURCE(IDD_SELECT_THINGLDEFFECT), hwndParent, SelectThingLDEffectDlgProc, (LPARAM)&fsdd);
}


/* SelectLDEffectDlg
 *   Displays the Select Linedef Effect dialogue, used by the Find/Replace
 *   dialogues.
 *
 * Parameters:
 *   HWND		hwndParent			Parent window.
 *   CONFIG*	lpcfgMap			Map configuration.
 *   int		iInitialSelection	Item to select initially. Negative for
 *									none.
 *
 * Return value: int
 *   Value of selected effect, or negative if none selected or cancelled.
 */
int SelectLDEffectDlg(HWND hwndParent, CONFIG *lpcfgMap, int iInitialSelection)
{
	/* FALSE for linedef effect, i.e. not thing. */
	FINDSELECTDLGDATA fsdd = {iInitialSelection, lpcfgMap, FALSE, 0};
	return DialogBoxParam(g_hInstance, MAKEINTRESOURCE(IDD_SELECT_THINGLDEFFECT), hwndParent, SelectThingLDEffectDlgProc, (LPARAM)&fsdd);
}


/* SelectThingLDEffectDlgProc
 *   Dialogue proc for Select Thing/LD Effect dialogue, used by the Find/Replace
 *   dialogues.
 *
 * Parameters:
 *   HWND	hwndDlg		Dialogue window handle.
 *   UINT	uiMsg		Message identifier.
 *   WPARAM wParam		Message-specific.
 *   LPARAM lParam		Message-specific.
 *
 * Return value: INT_PTR
 *   TRUE if the message was processed; FALSE otherwise.
 */
static INT_PTR CALLBACK SelectThingLDEffectDlgProc(HWND hwndDlg, UINT uiMsg, WPARAM wParam, LPARAM lParam)
{
	static FINDSELECTDLGDATA *s_lpfsdd;

	switch(uiMsg)
	{
	case WM_INITDIALOG:
		{
			TYPETREEDATA ttd;
			TCHAR szLabelText[64];
			CONFIG *lpcfgItems;
			TCHAR szSelectedValue[CCH_SIGNED_INT];

			/* Centre in parent. */
			CentreWindowInParent(hwndDlg);

			/* Get the parameters. */
			s_lpfsdd = (FINDSELECTDLGDATA*)lParam;

			/* Set the caption of the label depending on what we're choosing. */
			LoadString(g_hInstance, s_lpfsdd->bThing ? IDS_THINGTYPE : IDS_LINEDEFEFFECT, szLabelText, sizeof(szLabelText)/sizeof(TCHAR));
			SetDlgItemText(hwndDlg, IDC_STATIC_CAPTION, szLabelText);

			ttd.lpcfgTypeToNode = ConfigCreate();
			ttd.hwndTree = GetDlgItem(hwndDlg, IDC_TREE_EFFECT);

			/* Populate list of things/ld effects. */
			lpcfgItems = ConfigGetSubsection(s_lpfsdd->lpcfgMap, s_lpfsdd->bThing ? TEXT("thingtypes") : TEXT("linedeftypes"));
			ConfigIterate(lpcfgItems, AddCategoryToTree, &ttd);

			/* Select the specified item. */
			_sntprintf(szSelectedValue, NUM_ELEMENTS(szSelectedValue), TEXT("%d"), s_lpfsdd->iSelection);
			if(ConfigNodeExists(ttd.lpcfgTypeToNode, szSelectedValue))
				TreeView_SelectItem(GetDlgItem(hwndDlg, IDC_TREE_EFFECT), (HTREEITEM)ConfigGetInteger(ttd.lpcfgTypeToNode, szSelectedValue));
			else
				TreeView_SelectItem(GetDlgItem(hwndDlg, IDC_TREE_EFFECT), NULL);

			/* Don't need the lookup table any more. */
			ConfigDestroy(ttd.lpcfgTypeToNode);

			/* Focus on the tree. */
			SetFocus(GetDlgItem(hwndDlg, IDC_TREE_EFFECT));
		}

		/* We set the focus. */
		return FALSE;

	case WM_NOTIFY:
		if(((LPNMHDR)lParam)->idFrom == IDC_TREE_EFFECT && ((LPNMHDR)lParam)->code == (unsigned int)NM_DBLCLK)
		{
			/* We double-clicked in the tree. Find whether we actually
			 * double-clicked *on* anything.
			 */
			HWND hwndTV = GetDlgItem(hwndDlg, IDC_TREE_EFFECT);
			HTREEITEM hti = TreeView_GetSelection(hwndTV);

			/* Make sure we clicked on something and that it's not a category
			 * node.
			 */
			if(hti && TreeView_GetParent(hwndTV, hti))
			{
				/* Get the selected item's data. */
				TVITEM tvitem;
				tvitem.mask = TVIF_PARAM;
				tvitem.hItem = hti;
				TreeView_GetItem(hwndTV, &tvitem);
				EndDialog(hwndDlg, (int)tvitem.lParam);
			}

			return TRUE;
		}

		break;

	case WM_COMMAND:
		switch(LOWORD(wParam))
		{
		case IDOK:
			{
				/* Find whether we selected anything. */
				HWND hwndTV = GetDlgItem(hwndDlg, IDC_TREE_EFFECT);
				HTREEITEM hti = TreeView_GetSelection(hwndTV);

				/* Assume nothing selected. */
				int iEffect = -1;

				if(hti)
				{
					/* Get the selected item's data. */
					TVITEM tvitem;
					tvitem.mask = TVIF_PARAM;
					tvitem.hItem = hti;
					TreeView_GetItem(hwndTV, &tvitem);
					iEffect = (int)tvitem.lParam;
				}

				EndDialog(hwndDlg, iEffect);
			}

			return TRUE;

		case IDCANCEL:
			/* Signal that nothing was selected. */
			EndDialog(hwndDlg, -1);
			return TRUE;
		}

		break;
	}

	return FALSE;
}


/* DoWikiPopup
 *   Shows the Wiki context menu for an editing property page, and queries the
 *   Wiki appropriately if the user selects the menu item.
 *
 * Parameters:
 *   MAPPPDATA*			lpmapppdata	Property page data.
 *   ENUM_WIKIQUERIES	wikiquery	Query type.
 *   int				iIndex		Effect/thing type.
 *
 * Return value: None.
 */
static void DoWikiPopup(MAPPPDATA *lpmapppdata, ENUM_WIKIQUERIES wikiquery, int iIndex)
{
	POINT ptMouse;
	WORD wMenuID;

	GetCursorPos(&ptMouse);

	/* Show menu and see what was clicked. */
	wMenuID = (WORD)TrackPopupMenu(lpmapppdata->hmenuWikiPopup,
		TPM_NONOTIFY | TPM_RETURNCMD | TPM_TOPALIGN | TPM_RIGHTBUTTON,
		ptMouse.x, ptMouse.y,
		0,
		g_hwndMain,	/* Not used, but must belong to us. */
		NULL);

	if(wMenuID == IDM_POPUP_WIKI)
	{
		/* Query the Wiki. */
		WikiEffectQuery(lpmapppdata->lpcfgMap, wikiquery, iIndex);
	}
}


/* DoTreeViewWikiRClick
 *   Shows the Wiki context menu for the thing or LD effect tree views.
 *
 * Parameters:
 *   HWND				hwndTreeView		Tree view window handle.
 *   MAPPPDATA*			lpmapppdata			Property page data.
 *   ENUM_WIKIQUERIES	wikiquery			Query type.
 *
 * Return value: None.
 */
static void DoTreeViewWikiRClick(HWND hwndTreeView, MAPPPDATA *lpmapppdata, ENUM_WIKIQUERIES wikiquery)
{
	/* Get the drop highlight if we have one, or
	 * otherwise the selection.
	 */
	HTREEITEM hti = TreeView_GetDropHilight(hwndTreeView);
	if(!hti) hti = TreeView_GetSelection(hwndTreeView);

	/* Make sure there's a node highlighted/selected and
	 * that it's not a root node.
	 */
	if(hti && TreeView_GetParent(hwndTreeView, hti))
	{
		TVITEM tvitem;

		/* Get the effect code. */
		tvitem.mask = TVIF_PARAM;
		tvitem.hItem = hti;
		TreeView_GetItem(hwndTreeView, &tvitem);

		DoWikiPopup(lpmapppdata, wikiquery, (int)tvitem.lParam);
	}
}


/* SetFlagText
 *   Sets text for a flag item in a list-view.
 *
 * Parameters:
 *   HWND		hwndList	List-view control.
 *   WORD		wFlag		Value of flag whose text is to be set.
 *   LPCTSTR	szText		New text.
 *
 * Return value: None.
 */
static void SetFlagText(HWND hwndList, WORD wFlag, LPCTSTR szText)
{
	LVFINDINFO lvfi;
	LVITEM lvitem;

	/* Query parameters. */
	lvfi.flags = LVFI_PARAM;
	lvfi.lParam = wFlag;

	/* Find the item with the specified flag. */
	lvitem.iItem = ListView_FindItem(hwndList, -1, &lvfi);

	if(lvitem.iItem != -1)
	{
		/* Update the text. */
		lvitem.mask = LVIF_TEXT;
		lvitem.pszText = (LPTSTR)szText;
		lvitem.iSubItem = 0;
		ListView_SetItem(hwndList, &lvitem);
	}
}


/* UpdateZControlsForThing
 *   Updates Z controls as appropriate according to the selected thing-type.
 *
 * Parameters:
 *   HWND				hwndPropPage	"Things" property page.
 *   MAPPPDATA*			lpmapppdata		Context data.
 *   unsigned short		unType			Selected thing-type.
 *
 * Return value: None.
 */
static void UpdateZControlsForThing(HWND hwndPropPage, MAPPPDATA *lpmapppdata, unsigned short unType)
{
	int iMin = -32768, iMax = 32767;

	/* If it's not absolute, set the ranges depending on the Z-factor. Otherwise
	 * let it range over the whole short.
	 */
	if(!IsDlgButtonChecked(hwndPropPage, IDC_CHECK_ABSZ))
	{
		CONFIG *lpcfgFlatThings = ConfigGetSubsection(lpmapppdata->lpcfgMap, FLAT_THING_SECTION);
		WORD wZFactor = GetZFactor(lpcfgFlatThings, unType);

		iMin = 0;
		iMax = 0xFFFF / wZFactor;
	}

	SendDlgItemMessage(hwndPropPage, IDC_SPIN_Z, UDM_SETRANGE32, iMin, iMax);

	BoundEditBox(GetDlgItem(hwndPropPage, IDC_EDIT_Z), iMin, iMax, TRUE, TRUE);
}


/* A comparator for sorting effect lists. */
static int CALLBACK EffectNumericalSort(LPARAM lParam1, LPARAM lParam2, LPARAM lParamUnused)
{
	UNREFERENCED_PARAMETER(lParamUnused);
	return lParam1 - lParam2;
}


/* CurveDlg
 *   Displays the Curve Linedefs dialogue box.
 *
 * Parameters:
 *   HWND				hwndParent	Parent for dialogue.
 *   CURVEDLGDATA*		lpcurvedd	Pointer to structure in which to return the
 *									options specified by the user.
 *
 * Return value: BOOL
 *   FALSE if cancelled; TRUE otherwise.
 */
BOOL CurveDlg(HWND hwndParent, CURVEDLGDATA *lpcurvedd)
{
	return DialogBoxParam(g_hInstance, MAKEINTRESOURCE(IDD_CURVELINES), hwndParent, CurveDlgProc, (LPARAM)lpcurvedd);
}


/* CurveDlgProc
 *   Dialogue proc for Curve Linedefs dialouge box.
 *
 * Parameters:
 *   HWND	hwndDlg		Dialogue window handle.
 *   UINT	uiMsg		Message identifier.
 *   WPARAM wParam		Message-specific.
 *   LPARAM lParam		Message-specific.
 *
 * Return value: INT_PTR
 *   TRUE if the message was processed; FALSE otherwise.
 */
static INT_PTR CALLBACK CurveDlgProc(HWND hwndDlg, UINT uiMsg, WPARAM wParam, LPARAM lParam)
{
	static CURVEDLGDATA *s_lpcurvedd;

	switch(uiMsg)
	{
	case WM_INITDIALOG:
		{
			UDACCEL udaccel[2];

			/* Centre in parent. */
			CentreWindowInParent(hwndDlg);

			/* Get the structure in which we return parameters.
			 */
			s_lpcurvedd = (CURVEDLGDATA*)lParam;

			/* Set up the spinner controls. */

			udaccel[0].nInc = 1;
			udaccel[0].nSec = 0;
			udaccel[1].nInc = 8;
			udaccel[1].nSec = 2;

			SendDlgItemMessage(hwndDlg, IDC_SPIN_RADIUS, UDM_SETRANGE32, CURVERADIUSMIN, CURVERADIUSMAX);
			SendDlgItemMessage(hwndDlg, IDC_SPIN_RADIUS, UDM_SETACCEL, sizeof(udaccel)/sizeof(UDACCEL), (LPARAM)udaccel);

			SendDlgItemMessage(hwndDlg, IDC_SPIN_SEGMENTS, UDM_SETRANGE32, SEGMENTSMIN, SEGMENTSMAX);
			SendDlgItemMessage(hwndDlg, IDC_SPIN_SEGMENTS, UDM_SETACCEL, sizeof(udaccel)/sizeof(UDACCEL), (LPARAM)udaccel);

			CheckRadioButton(hwndDlg, IDC_RADIO_SPECIFYRADIUS, IDC_RADIO_SEMICIRCLE, IDC_RADIO_SPECIFYRADIUS);


			/* Set initial values. */
			SetDlgItemInt(hwndDlg, IDC_EDIT_RADIUS, CURVERADIUSINIT, TRUE);
			SetDlgItemInt(hwndDlg, IDC_EDIT_SEGMENTS, SEGMENTSINIT, FALSE);
		}

		/* Let the system set the focus. */
		return TRUE;

	case WM_COMMAND:
		switch(LOWORD(wParam))
		{
		case IDOK:
			/* Set the structure fields. */
			s_lpcurvedd->bCircle = IsDlgButtonChecked(hwndDlg, IDC_RADIO_SEMICIRCLE);
			s_lpcurvedd->iSegments = GetDlgItemInt(hwndDlg, IDC_EDIT_SEGMENTS, NULL, FALSE);
			if(!s_lpcurvedd->bCircle)
				s_lpcurvedd->iRadius = GetDlgItemInt(hwndDlg, IDC_EDIT_RADIUS, NULL, TRUE);

			/* Signal that we OKed. */
			EndDialog(hwndDlg, TRUE);

			return TRUE;

		case IDCANCEL:
			/* Signal that we cancelled. */
			EndDialog(hwndDlg, FALSE);

			return TRUE;

		case IDC_EDIT_RADIUS:
			switch(HIWORD(wParam))
			{
			case EN_KILLFOCUS:
				BoundEditBox((HWND)lParam, CURVERADIUSMIN, CURVERADIUSMAX, FALSE, FALSE);
				return TRUE;
			}

			break;

		case IDC_EDIT_SEGMENTS:
			switch(HIWORD(wParam))
			{
			case EN_KILLFOCUS:
				BoundEditBox((HWND)lParam, SEGMENTSMIN, SEGMENTSMAX, FALSE, FALSE);
				return TRUE;
			}

			break;

		case IDC_RADIO_SPECIFYRADIUS:
		case IDC_RADIO_SEMICIRCLE:
			/* Enable/disable the radius edit-box, as appropriate. */
			if(HIWORD(wParam) == BN_CLICKED)
				EnableWindow(GetDlgItem(hwndDlg, IDC_EDIT_RADIUS), (LOWORD(wParam) == IDC_RADIO_SPECIFYRADIUS));
			break;
		}

		break;
	}

	/* Didn't process message. */
	return FALSE;
}
