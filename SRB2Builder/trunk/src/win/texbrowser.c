/* SRB2 Workbench -- A map editor for Sonic Robo Blast 2.
 * Copyright (C) 2009 Gregor Dick
 * http://workbench.srb2.org/
 *
 * Incorporates portions of Doom Builder, (C) 2003 Pascal vd Heiden.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 * USA.
 *
 * texbrowser.c: Implements the texture browser dialogue.
 *
 * AMDG.
 */

#include <windows.h>
#include <commctrl.h>
#include <stdio.h>
#include <tchar.h>

#include "../general.h"
#include "../texture.h"
#include "../options.h"
#include "../renderer.h"

#include "../../res/resource.h"

#include "texbrowser.h"
#include "mapwin.h"
#include "gendlg.h"

/* TODO: Get rid of tiles, and this too. */
#if ((_WIN32_WINNT < 0x0501) || defined(__MINGW32__)) && !defined(__WINE__)
typedef struct _LVTILEINFO
{
	UINT cbSize;
	int iItem;
	UINT cColumns;
	PUINT puColumns;
} LVTILEINFO;

/* This is needed with MS SDK, too, if _WIN32_WINNT is small. */
#ifndef LVM_SETTILEINFO
#define LVM_SETTILEINFO (LVM_FIRST + 164)
#endif

#endif


/* Types. */
typedef struct _TEXBROWSERDATA
{
	HWND			hwndMap;
	TEX_FORMAT		tf;
	LPTSTR			szTexName;
	TEXTURENAMELIST *lptnlAll;
	CONFIG			*lpcfgUsed;
	HIMAGELIST		*lphimlCache;
	DWORD			dwFlags;
} TEXBROWSERDATA;

/* Static prototypes. */
static INT_PTR CALLBACK TexBrowserProc(HWND hwndDlg, UINT uiMsg, WPARAM wParam, LPARAM lParam);
static void BuildTextureImageList(HWND hwndList, HWND hwndMap, TEXTURENAMELIST *lptnl, TEX_FORMAT tf, HIMAGELIST himl);
static void FillTextureList(HWND hwndList, TEXTURENAMELIST *lptnl, CONFIG *lpcfgUsed, BOOL bAddNullEntry);
static LRESULT CALLBACK TexListSubclassProc(HWND hwndList, UINT uiMsg, WPARAM wParam, LPARAM lParam);
static void SelectTextureByName(HWND hwndList, LPTSTR szTexName);


/* SelectTexture
 *   Displays the texture browser and allows the user to select a texture.
 *
 * Parameters:
 *   HWND				hwndParent		Parent window for dialogue.
 *   HWND				hwndMap			Map window handle.
 *   TEX_FORMAT			tf				Texture format.
 *   HIMAGELIST*		lphimlCache		Cached image list.
 *   TEXTURENAMELIST*	lptnlAll		*Sorted* list of all texture names.
 *   CONFIG*			lpcfgUsed		Names of used textures.
 *   DWORD				dwFlags			Set SLT_FORCE_USED to only allow used
 *										textures to be shown or SLT_NO_NULL to
 *										force suppression of "-" entry.
 *   LPTSTR				szTexName		Name of texture to select initially, and
 *										buffer used to return selection. Must be
 *										at least TEXNAME_BUFFER_LENGTH long.
 *
 * Return value: BOOL
 *   FALSE if user cancelled; TRUE otherwise.
 */
BOOL SelectTexture(HWND hwndParent, HWND hwndMap, TEX_FORMAT tf, HIMAGELIST* lphimlCache, TEXTURENAMELIST *lptnlAll, CONFIG *lpcfgUsed, DWORD dwFlags, LPTSTR szTexName)
{
	TEXBROWSERDATA tbd = {hwndMap, tf, szTexName, lptnlAll, lpcfgUsed, lphimlCache, dwFlags};

	return (BOOL)DialogBoxParam(g_hInstance, MAKEINTRESOURCE(IDD_TEXBROWSER), hwndParent, TexBrowserProc, (LPARAM)&tbd);
}


/* TexBrowserProc
 *   Dialogue procedure for the texture browser.
 *
 * Parameters:
 *   HWND	hwndDlg		Handle to dialogue window.
 *   HWND	uiMsg		Window message ID.
 *   WPARAM	wParam		Message-specific.
 *   LPARAM lParam		Message-specific.
 *
 * Return value: INT_PTR
 *   TRUE if message was processed; FALSE otherwise.
 */
static INT_PTR CALLBACK TexBrowserProc(HWND hwndDlg, UINT uiMsg, WPARAM wParam, LPARAM lParam)
{
	static TEXBROWSERDATA *s_lptbd;
	static POINT s_ptListPadding;
	static POINT s_ptOKFromBR, s_ptCancelFromBR, s_ptHelpFromBR;

	switch(uiMsg)
	{
	case WM_INITDIALOG:
		{
			RECT rcDlg, rcDlgClient;
			RECT rcChild;
			POINT ptClientBR;
			LVCOLUMN lvcol;
			HWND hwndList = GetDlgItem(hwndDlg, IDC_LIST_TEXTURES);

			/* Centre in parent. */
			CentreWindowInParent(hwndDlg);

			/* Store parameter. */
			s_lptbd = (TEXBROWSERDATA*)lParam;

			/* Not only is the tile view not supported in old versions of the
			 * Common Controls, the LVM_SETVIEW message isn't, either.
			 *
			 * Icon is actually better. Allow user to change? Maybe only on XP?
			 * if(g_bHasCC6)
			 *	SendMessage(hwndList, LVM_SETVIEW, LV_VIEW_TILE, 0);
			 */

			lvcol.mask = LVCF_SUBITEM;
			lvcol.iSubItem = 0;
			ListView_InsertColumn(hwndList, 1, &lvcol);

			lvcol.mask = LVCF_SUBITEM;
			lvcol.iSubItem = 1;
			ListView_InsertColumn(hwndList, 2, &lvcol);

			/* Spacing includes icon dimensions. */
			ListView_SetIconSpacing(hwndList, 80, 96);

			/* Set the user's chosen background colour. */
			ListView_SetBkColor(hwndList, RGBQUADToCOLORREF(g_rgbqPalette[CLR_TEXBROWSER]));
			ListView_SetTextBkColor(hwndList, RGBQUADToCOLORREF(g_rgbqPalette[CLR_TEXBROWSER]));

			/* Choose white or black text using a weighted luminance comparison
			 * found in a comment on The Old New Thing:
			 * http://blogs.msdn.com/oldnewthing/archive/2004/05/26/142276.aspx
			 */
			ListView_SetTextColor(hwndList,
				(2 * g_rgbqPalette[CLR_TEXBROWSER].rgbRed + 5 * g_rgbqPalette[CLR_TEXBROWSER].rgbGreen + g_rgbqPalette[CLR_TEXBROWSER].rgbBlue > 0x400) ?
				RGB(0, 0, 0) :
				RGB(0xFF, 0xFF, 0xFF));

			/* If we don't already have an image list, make one. */
			if(!*s_lptbd->lphimlCache)
			{
				/* Create image-list. */
				*s_lptbd->lphimlCache = ImageList_Create(64, 64, ILC_COLOR24 | ILC_MASK, s_lptbd->lptnlAll->iEntries, 0);

				BuildTextureImageList(hwndList, s_lptbd->hwndMap, s_lptbd->lptnlAll, s_lptbd->tf, *s_lptbd->lphimlCache);
			}

			/* Set image list. */
			ListView_SetImageList(hwndList, *s_lptbd->lphimlCache, LVSIL_NORMAL);

			/* Fill the texture list and set the "used only" checkbox as appropriate. */
			if(s_lptbd->lpcfgUsed)
			{
				BOOL bUsedOnly = ConfigGetInteger(g_lpcfgMain, OPT_USEDONLY) || (s_lptbd->dwFlags & SLT_FORCE_USED);
				CheckDlgButton(hwndDlg, IDC_CHECK_USED, bUsedOnly ? BST_CHECKED : BST_UNCHECKED);

				/* Fill the texture list with either all the textures or only
				 * the used ones, as appropriate.
				 */
				FillTextureList(hwndList, s_lptbd->lptnlAll, bUsedOnly ? s_lptbd->lpcfgUsed : NULL, !(s_lptbd->dwFlags & SLT_NO_NULL) && (s_lptbd->tf == TF_TEXTURE));

				/* If we force used, disable the checkbox. */
				if(s_lptbd->dwFlags & SLT_FORCE_USED)
					EnableWindow(GetDlgItem(hwndDlg, IDC_CHECK_USED), FALSE);
			}
			else
			{
				EnableWindow(GetDlgItem(hwndDlg, IDC_CHECK_USED), FALSE);

				/* Fill the texture list with all the textures. */
				FillTextureList(hwndList, s_lptbd->lptnlAll, NULL, !(s_lptbd->dwFlags & SLT_NO_NULL) && (s_lptbd->tf == TF_TEXTURE));
			}

			GetWindowRect(hwndDlg, &rcDlg);
			GetClientRect(hwndDlg, &rcDlgClient);

			/* Get list-view padding. */
			GetWindowRect(hwndList, &rcChild);
			MapWindowPoints(NULL, hwndDlg, (LPPOINT)(LPVOID)&rcChild, 2);
			s_ptListPadding.x = rcDlgClient.right - rcChild.right + rcChild.left;
			s_ptListPadding.y = rcDlgClient.bottom - rcChild.bottom + rcChild.top;

			/* Bottom-right of client in screen co-ords. */
			ptClientBR.x = rcDlgClient.right;
			ptClientBR.y = rcDlgClient.bottom;
			ClientToScreen(hwndDlg, &ptClientBR);

			/* Get positions of the buttons relative to the bottom-right. */
			GetWindowRect(GetDlgItem(hwndDlg, IDOK), &rcChild);
			s_ptOKFromBR.x = ptClientBR.x - rcChild.left;
			s_ptOKFromBR.y = ptClientBR.y - rcChild.top;

			GetWindowRect(GetDlgItem(hwndDlg, IDCANCEL), &rcChild);
			s_ptCancelFromBR.x = ptClientBR.x - rcChild.left;
			s_ptCancelFromBR.y = ptClientBR.y - rcChild.top;

			GetWindowRect(GetDlgItem(hwndDlg, IDC_BUTTON_HELP), &rcChild);
			s_ptHelpFromBR.x = ptClientBR.x - rcChild.left;
			s_ptHelpFromBR.y = ptClientBR.y - rcChild.top;

			/* Select the specified texture. */
			SelectTextureByName(hwndList, s_lptbd->szTexName);

			/* Subclass the texture list so that we can eat TAB if desired. */
			SetWindowLong(hwndList, GWL_USERDATA, GetWindowLong(hwndList, GWL_WNDPROC));
			SetWindowLong(hwndList, GWL_WNDPROC, (LONG)TexListSubclassProc);
		}

		/* Let the system set the focus (to the list). */
		return TRUE;

	case WM_SIZE:
		{
			HDWP hdwp = BeginDeferWindowPos(4);
			RECT rcClient;
			int cx, cy;

			GetClientRect(hwndDlg, &rcClient);
			cx = rcClient.right;
			cy = rcClient.bottom;

			DeferWindowPos(hdwp, GetDlgItem(hwndDlg, IDC_LIST_TEXTURES), NULL, -1, -1, cx - s_ptListPadding.x, cy - s_ptListPadding.y, SWP_NOMOVE | SWP_NOZORDER);
			DeferWindowPos(hdwp, GetDlgItem(hwndDlg, IDOK), NULL, cx - s_ptOKFromBR.x, cy - s_ptOKFromBR.y, -1, -1, SWP_NOSIZE | SWP_NOZORDER);
			DeferWindowPos(hdwp, GetDlgItem(hwndDlg, IDCANCEL), NULL, cx - s_ptCancelFromBR.x, cy - s_ptCancelFromBR.y, -1, -1, SWP_NOSIZE | SWP_NOZORDER);
			DeferWindowPos(hdwp, GetDlgItem(hwndDlg, IDC_BUTTON_HELP), NULL, cx - s_ptHelpFromBR.x, cy - s_ptHelpFromBR.y, -1, -1, SWP_NOSIZE | SWP_NOZORDER);

			EndDeferWindowPos(hdwp);
		}

		return TRUE;

	case WM_COMMAND:
		switch(LOWORD(wParam))
		{
		case IDOK:
			{
				HWND hwndList = GetDlgItem(hwndDlg, IDC_LIST_TEXTURES);

				ListView_GetItemText(hwndList, ListView_GetNextItem(hwndList, -1, LVIS_SELECTED), 0, s_lptbd->szTexName, TEXNAME_BUFFER_LENGTH);
				EndDialog(hwndDlg, TRUE);
			}
			return TRUE;

		case IDCANCEL:
			EndDialog(hwndDlg, FALSE);
			return TRUE;

		case IDC_CHECK_USED:
			if(HIWORD(wParam) == BN_CLICKED)
			{
				HWND hwndList = GetDlgItem(hwndDlg, IDC_LIST_TEXTURES);
				int iSelected = ListView_GetNextItem(hwndList, -1, LVIS_SELECTED);
				TCHAR szTexName[TEXNAME_BUFFER_LENGTH];

				HCURSOR hcurLast = SetCursor(LoadCursor(NULL, IDC_WAIT));

				/* Remember the name of the selection. */
				if(iSelected >= 0)
					ListView_GetItemText(hwndList, iSelected, 0, szTexName, TEXNAME_BUFFER_LENGTH);

				ListView_DeleteAllItems(hwndList);
				FillTextureList(hwndList, s_lptbd->lptnlAll, IsDlgButtonChecked(hwndDlg, IDC_CHECK_USED) == BST_CHECKED ? s_lptbd->lpcfgUsed : NULL, !(s_lptbd->dwFlags & SLT_NO_NULL) && (s_lptbd->tf == TF_TEXTURE));

				/* Restore the selection. */
				if(iSelected >= 0)
					SelectTextureByName(hwndList, szTexName);

				SetCursor(hcurLast);
			}

			return TRUE;
		}

		break;

	case WM_NOTIFY:
		switch(wParam)
		{
		/* Texture list-view. */
		case IDC_LIST_TEXTURES:
			{
				NMLISTVIEW *lpnmlv = (NMLISTVIEW*)lParam;
				switch(lpnmlv->hdr.code)
				{
				case LVN_ITEMCHANGED:
					/* Deselection always precedes selection, so this is
					 * okay.
					 */
					EnableWindow(GetDlgItem(hwndDlg, IDOK), lpnmlv->uNewState & LVIS_SELECTED);
					return TRUE;

				case LVN_ITEMACTIVATE:
					SendMessage(hwndDlg, WM_COMMAND, MAKELONG(IDOK, BN_CLICKED), (LPARAM)GetDlgItem(hwndDlg, IDOK));
					return TRUE;
				}
			}

			break;
		}

		break;
	}

	/* Didn't process message. */
	return FALSE;
}


/* BuildTextureImageList
 *   Populates a texture- or flat-cache image-list.
 *
 * Parameters:
 *   HWND				hwndList	Handle to texture list-view.
 *   HWND				hwndMap		Map window handle.
 *   TEXTURENAMELIST*	lptnl		List of texture names.
 *   TEX_FORMAT			tf			Whether flats or textures.
 *   HIMAGELIST			himl		Image-list to populate.
 *
 * Return value: None.
 *
 * Remarks:
 *   The image-list is cleared first.
 */
static void BuildTextureImageList(HWND hwndList, HWND hwndMap, TEXTURENAMELIST *lptnl, TEX_FORMAT tf, HIMAGELIST himl)
{
	HBITMAP hbm, hbmMask;
	HDC hdc, hdcList;
	int i;

	ImageList_RemoveAll(himl);

	hdcList = GetDC(hwndList);
	hbm = CreateCompatibleBitmap(hdcList, CX_TEXPREVIEW, CY_TEXPREVIEW);
	ReleaseDC(hwndList, hdcList);

	hbmMask = CreateBitmap(CX_TEXPREVIEW, CY_TEXPREVIEW, 1, 1, NULL);
	hdc = CreateCompatibleDC(NULL);

	/* One extra for textures for "-". */
	ImageList_SetImageCount(himl, lptnl->iEntries + ((tf == TF_TEXTURE) ? 1 : 0));

	for(i = 0; i < lptnl->iEntries; i++)
	{
		TEXTURE *lptex;
		HBITMAP hbmOld;
		BOOL bNeedFree;

		/* Skip duplicate entries. */
		if(i > 0 && _tcscmp(lptnl->lpszTexNames[i-1], lptnl->lpszTexNames[i]) == 0) continue;

		/* Draw onto the texture preview bitmap first. */
		hbmOld = SelectObject(hdc, hbm);

		/* Get the texture and draw the preview. */
		bNeedFree = GetTextureForMap(hwndMap, lptnl->lpszTexNames[i], &lptex, tf);
		StretchTextureToDC(lptex, hdc, CX_TEXPREVIEW, CY_TEXPREVIEW, STT_TEXTURE);

		/* Now select the preview mask bitmap and draw the mask onto it. */
		SelectObject(hdc, hbmMask);
		StretchTextureToDC(lptex, hdc, CX_TEXPREVIEW, CY_TEXPREVIEW, STT_MASKONLY);

		if(bNeedFree) DestroyTexture(lptex);

		SelectObject(hdc, hbmOld);
		ImageList_Replace(himl, i, hbm, hbmMask);
	}

	/* Add transparent entry for "-" for textures. */
	if(tf == TF_TEXTURE)
	{
		RECT rc = {0, 0, CX_TEXPREVIEW, CY_TEXPREVIEW};
		HBITMAP hbmOld;

		hbmOld = SelectObject(hdc, hbm);
		FillRect(hdc, &rc, GetStockObject(WHITE_BRUSH));
		SelectObject(hdc, hbmOld);

		/* Stick it in last place. */
		ImageList_Replace(himl, lptnl->iEntries, hbm, hbm);
	}

	DeleteDC(hdc);
	DeleteObject(hbmMask);
	DeleteObject(hbm);
}


/* FillTextureList
 *   Populates a texture list-view.
 *
 * Parameters:
 *   HWND				hwndList		Handle to texture list-view.
 *   TEXTURENAMELIST*	lptnl			List of texture names.
 *   CONFIG*			lpcfgUsed		Used textures. If NULL, all textures are
 *										added.
 *   BOOL				bAddNullEntry	Whether to add "-" entry.
 *
 * Return value: None.
 */
static void FillTextureList(HWND hwndList, TEXTURENAMELIST *lptnl, CONFIG *lpcfgUsed, BOOL bAddNullEntry)
{
	int i, iNewItem;
	LVITEM lvitem;

	/* Common fields. */
	lvitem.mask = LVIF_TEXT | LVIF_IMAGE;
	lvitem.iItem = 0;
	lvitem.iSubItem = 0;

	/* Add "-" entry for textures. */
	if(bAddNullEntry)
	{
		/* "-" image is in last place. */
		lvitem.iImage = lptnl->iEntries;

		lvitem.cchTextMax = 2;
		lvitem.pszText = TEXT("-");
		iNewItem = ListView_InsertItem(hwndList, &lvitem);
	}

	lvitem.cchTextMax = TEXNAME_BUFFER_LENGTH;

	/* Don't redraw the list while we're adding items. */
	SendMessage(hwndList, WM_SETREDRAW, FALSE, 0);

	for(i = 0; i < lptnl->iEntries; i++)
	{
		TCHAR szDimensions[32];
		unsigned int uiCol = 1;

		/* Skip duplicate entries. */
		if(i > 0 && _tcscmp(lptnl->lpszTexNames[i-1], lptnl->lpszTexNames[i]) == 0) continue;

		/* Skip entries that aren't used, if we're only showing used. */
		if(lpcfgUsed && !ConfigNodeExists(lpcfgUsed, lptnl->lpszTexNames[i]))
			continue;

		/* Add the image. */

		/* Image list order is same as texture order. */
		lvitem.iImage = i;

		lvitem.pszText = lptnl->lpszTexNames[i];
		iNewItem = ListView_InsertItem(hwndList, &lvitem);

		/* TODO: Show the dimensions as a subitem. This was implemented, but it
		 * broke when you stated caching image lists. Fix it. Maybe by recording
		 * dimensions in BuildTextureImageList?
		 */
		_sntprintf(szDimensions, sizeof(szDimensions) / sizeof(TCHAR), TEXT("TODO"));
		ListView_SetItemText(hwndList, iNewItem, 1, szDimensions);
		if(g_bHasCC6)
		{
			LVTILEINFO lvti;
			lvti.cbSize = sizeof(lvti);
			lvti.iItem = iNewItem;
			lvti.cColumns = 1;
			lvti.puColumns = &uiCol;
			SendMessage(hwndList, LVM_SETTILEINFO, 0, (LPARAM)&lvti);
		}
	}

	/* Let it redraw now that we've populated it. */
	SendMessage(hwndList, WM_SETREDRAW, TRUE, 0);
}


/* TexListSubclassProc
 *   Window procedure to subclass texture list to eat TAB in order optionally
 *   to show all textures.
 *
 * Parameters:
 *   HWND	hwndList	Handle to list-view.
 *   HWND	uiMsg		Window message ID.
 *   WPARAM	wParam		Message-specific.
 *   LPARAM lParam		Message-specific.
 *
 * Return value: LRESULT
 *   Message-specific.
 */
static LRESULT CALLBACK TexListSubclassProc(HWND hwndList, UINT uiMsg, WPARAM wParam, LPARAM lParam)
{
	switch(uiMsg)
	{
	case WM_GETDLGCODE:
		{
			/* We eat TAB if the user wants it, if we're not already showing all
			 * textures and if we are able to do so.
			 */
			if(ConfigGetInteger(g_lpcfgMain, OPT_TEXBROWSERTAB) &&
				IsDlgButtonChecked(GetParent(hwndList), IDC_CHECK_USED) == BST_CHECKED &&
				IsWindowEnabled(GetDlgItem(GetParent(hwndList), IDC_CHECK_USED)))
				return DLGC_WANTTAB | CallWindowProc((WNDPROC)GetWindowLong(hwndList, GWL_USERDATA), hwndList, uiMsg, wParam, lParam);

			break;
		}

	case WM_KEYDOWN:
		if(wParam == VK_TAB)
		{
			/* Notice that the fact we got TAB means we want it, by virtue of
			 * the WM_GETDLGCODE logic.
			 */
			HWND hwndDlg = GetParent(hwndList);

			/* Simulate a click. This is okay, since the checkbox is always
			 * enabled.
			 */
			SendDlgItemMessage(hwndDlg, IDC_CHECK_USED, BM_CLICK, 0, 0);

			/* Simulating the click moved the focus. Move it back. */
			PostMessage(hwndDlg, WM_NEXTDLGCTL, (WPARAM)hwndList, TRUE);
		}

		break;
	}

	return CallWindowProc((WNDPROC)GetWindowLong(hwndList, GWL_USERDATA), hwndList, uiMsg, wParam, lParam);
}


/* SelectTextureByName
 *   Selects a texture in the list.
 *
 * Parameters:
 *   HWND		hwndList	Handle to list-view.
 *   LPTSTR		szTexName	Texture name.
 *
 * Return value: None.
 *
 * Remarks:
 *   The selection is ensured visible. If such a texture does not exist, nothing
 *   happens.
 */
static void SelectTextureByName(HWND hwndList, LPTSTR szTexName)
{
	LVFINDINFO lvfi;
	LVITEM lvi;

	lvfi.flags = LVFI_STRING;
	lvfi.psz = szTexName;
	lvi.iItem = ListView_FindItem(hwndList, -1, &lvfi);

	if(lvi.iItem >= 0)
	{
		lvi.mask = LVIF_STATE;
		lvi.iSubItem = 0;
		lvi.state = lvi.stateMask = LVIS_SELECTED;
		ListView_SetItem(hwndList, &lvi);
		ListView_EnsureVisible(hwndList, lvi.iItem, FALSE);
	}
}
