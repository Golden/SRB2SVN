/* SRB2 Workbench -- A map editor for Sonic Robo Blast 2.
 * Copyright (C) 2009 Gregor Dick
 * http://workbench.srb2.org/
 *
 * Incorporates portions of Doom Builder, (C) 2003 Pascal vd Heiden.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 * USA.
 *
 * texture.h: Header for texture.c.
 *
 * AMDG.
 */

#ifndef __SRB2B_TEXTURE__
#define __SRB2B_TEXTURE__

#include <windows.h>
#include <GL/gl.h>

#include "wad.h"


/* Size of PLAYPAL lump. */
#define CB_PLAYPAL 768

#define TEXNAME_BUFFER_LENGTH		9
#define TEXNAME_WAD_BUFFER_LENGTH	8

#define CX_TEXPREVIEW 64
#define CY_TEXPREVIEW 64

#define BLANK_TEXTURE	"-"

typedef enum _ENUM_STRETCHTEX_TYPES
{
	STT_TEXTURE,
	STT_MASKONLY,
	STT_MASKED
} ENUM_STRETCHTEX_TYPES;


/* Types. */

typedef struct _TEXTUREN TEXTUREN;
typedef struct _PNAMES PNAMES;

typedef struct _TEXTURE
{
	HBITMAP			hbitmap, hbitmapMask;
	unsigned short	cx, cy;
	short			xOffset, yOffset;
	LPVOID			lpvBits;
	GLuint			uiGLTexName;
} TEXTURE;


typedef enum _TEX_FORMAT
{
	TF_TEXTURE,
	TF_FLAT,
	TF_IMAGE
} TEX_FORMAT;


typedef struct _TEXCACHE
{
	char				szLumpName[9];
	TEXTURE				*lptexture;

	struct _TEXCACHE	*lptcNext;
} TEXCACHE;


typedef struct _TEXTURENAMELIST
{
	TCHAR		(*lpszTexNames)[TEXNAME_BUFFER_LENGTH];
	int			iEntries;
	int			cstrBuffer;		/* Number of strings the buffer can hold. */
} TEXTURENAMELIST;


typedef struct _TEXTURE_DIRECTORY
{
	/* Sorted name lists. */
	TEXTURENAMELIST		*lptnlFlats, *lptnlTextures;

	/* Texture info in the new format. */
	CONFIG				*lpcfgTextureNew;

	/* TEXTURE1 and TEXTURE2 from first wad containing such. */
	TEXTUREN			*lptexnTexture1, *lptexnTexture2;

	/* PNAMES from first wad containing such. */
	PNAMES				*lppnames;

	/* Wads to search for patches, flats and sprites, in order. */
	WAD					**lplpwad;
	int					iNumWads;
} TEXTURE_DIRECTORY;


typedef struct _TEXTURE_PATCH_DESCRIPTOR_NEW
{
	short		xOffset, yOffset;
	char		sPatchName[TEXNAME_WAD_BUFFER_LENGTH];
} TEXTURE_PATCH_DESCRIPTOR_NEW;


typedef struct _TEXTURE_ENTRY_NEW
{
	char		sTexName[TEXNAME_WAD_BUFFER_LENGTH];
	short		cx, cy;
	short		nNumPatches, nNumAllocedPatches;

	/* We allocate these dynamically now. */
	TEXTURE_PATCH_DESCRIPTOR_NEW*	lptpdn;
} TEXTURE_ENTRY_NEW;


/* Prototypes. */
TEXTURE* LoadTextureW(TEXTURE_DIRECTORY *lptexdir, LPCWSTR sTexName, TEX_FORMAT tf, RGBQUAD *lprgbq);
TEXTURE* LoadTextureA(TEXTURE_DIRECTORY *lptexdir, LPCSTR sTexName, TEX_FORMAT tf, RGBQUAD *lprgbq);
void DestroyTexture(TEXTURE *lptex);
TEXTURE* GetTextureFromCache(TEXCACHE *lptcHdr, LPCTSTR szTexName);
void PurgeTextureCache(TEXCACHE *lptcHdr);
TEXTURENAMELIST* CreateTextureNameList(void);
void DestroyTextureNameList(TEXTURENAMELIST *lptnl);
void AddAllTextureNamesToList(TEXTURENAMELIST *lptnl, WAD *lpwad, TEX_FORMAT tf);
void SortTextureNameList(TEXTURENAMELIST *lptnl);
LPCTSTR FindFirstTexNameMatch(LPCTSTR szTexName, TEXTURENAMELIST *lptnl);
void StretchTextureToDC(TEXTURE *lptex, HDC hdcTarget, int cx, int cy, ENUM_STRETCHTEX_TYPES stt);
void AddToTextureCache(TEXCACHE *lptcHdr, LPCTSTR szLumpName, TEXTURE *lptex);
void TexEntryNewFree(TEXTURE_ENTRY_NEW *lptexentrynew);
void CALLBACK AddTextureNameToList(const char *sTexName, void *lpvParam);
TEXTURE_DIRECTORY* CreateTextureDirectory(WAD **lplpwad, int iNumWads);
void DestroyTextureDirectory(TEXTURE_DIRECTORY *lptexdir);
TEXTURE* MakeSolidTexture(COLORREF cref);

#ifdef _UNICODE
BOOL IsPseudoTextureW(LPCWSTR sTexName);
BOOL IsPseudoFlatW(LPCWSTR sTexName);
BOOL IsBlankTextureW(LPCWSTR sTexName);
#endif

BOOL IsPseudoTextureA(LPCSTR sTexName);
BOOL IsPseudoFlatA(LPCSTR sTexName);
BOOL IsBlankTextureA(LPCSTR sTexName);

BOOL IsBonaFideTexture(LPCTSTR sTexName);
BOOL IsBonaFideTextureA(LPCSTR sTexName);

#ifdef _UNICODE
#define IsPseudoTexture IsPseudoTextureW
#define IsPseudoFlat IsPseudoFlatW
#define IsBlankTexture IsBlankTextureW
#else
#define IsPseudoTexture IsPseudoTextureA
#define IsPseudoFlat IsPseudoFlatA
#define IsBlankTexture IsBlankTextureA
#endif

#ifdef UNICODE
#define LoadTexture LoadTextureW
#else
#define LoadTexture LoadTextureA
#endif

#endif
