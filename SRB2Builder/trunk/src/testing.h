/* SRB2 Workbench -- A map editor for Sonic Robo Blast 2.
 * Copyright (C) 2009 Gregor Dick
 * http://workbench.srb2.org/
 *
 * Incorporates portions of Doom Builder, (C) 2003 Pascal vd Heiden.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 * USA.
 *
 * testing.h: Header for testing.c.
 *
 * AMDG.
 */

#ifndef __SRB2B_TESTING__
#define __SRB2B_TESTING__

#include <windows.h>

#include "config.h"


enum ENUM_GAMETYPES
{
	GT_COOP = 0,
	GT_MATCH = 1,
	GT_FULLRACE = 2,
	GT_TAG = 3,
	GT_CTF = 4,
	GT_SINGLEPLAYER = 6,
	GT_DEFAULT = 7,
	GT_TIMEONLYRACE = 43,
};


/* The values correspond to the command-line-option values for SRB2. */
enum ENUM_DIFFICULTIES
{
	DIFF_EASY,
	DIFF_NORMAL,
	DIFF_HARD,
	DIFF_VERYHARD,
	DIFF_ULTIMATE
};


typedef struct _TESTTHREADDATA
{
	HANDLE	hProcess;
	TCHAR	szFilename[MAX_PATH];
} TESTTHREADDATA;


HANDLE TestMapWithSRB2(LPCTSTR szWadFile, LPCSTR sMapLumpname, LPCTSTR szBinary, LPCTSTR szAddWadFile, CONFIG *lpcfgTest, CONFIG *lpcfgWadOptsMap);
BOOL TestInProgress(void);
void TestThreadProc(void *lpvTestData);


#endif
