/* SRB2 Workbench -- A map editor for Sonic Robo Blast 2.
 * Copyright (C) 2011 Gregor Dick
 * http://workbench.srb2.org/
 *
 * Incorporates portions of Doom Builder, (C) 2003 Pascal vd Heiden.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 * USA.
 *
 * renderer3d.c: 3D rendering, for both perspective and orthographic views.
 *
 * AMDG.
 */

#include <windows.h>

#include <GL/gl.h>
#include <GL/glu.h>

#include "general.h"
#include "maptypes.h"
#include "renderer.h"
#include "renderer3d.h"
#include "editing.h"
#include "fof.h"
#include "CodeImp/ci_math.h"

#include "win/mapwin.h"


/* MinGW needs this. */
#ifndef GL_BGRA_EXT
#define GL_BGRA_EXT 0x80E1
#endif

/* Some OpenGL 1.3 stuff that we need. */
#define GL_COMBINE		0x8570
#define GL_COMBINE_RGB	0x8571
#define GL_INTERPOLATE	0x8575
#define GL_CONSTANT		0x8576
#define GL_SRC2_RGB		0x8582


typedef struct _VERTEX3D
{
	float	x, y, z;
	float	s, t;
} VERTEX3D;

typedef struct _SURFACE3D
{
	TEXTURE		*lptex;
	short		cVertices;
	VERTEX3D	*lpvx3dVertices;
	DWORD		dwFlags;
	GLuint		uiName;
	UINT		uiColour;
} SURFACE3D;

enum ENUM_SURFACE_FLAGS
{
	SF_EXISTS	= 1,
	SF_CLAMP	= 2
};

enum ENUM_SURFACE_TYPES
{
	ST_NOTHING = 0,
	ST_FRONTUPPER = 1,
	ST_FRONTMIDDLE,
	ST_FRONTLOWER,
	ST_BACKUPPER,
	ST_BACKMIDDLE,
	ST_BACKLOWER,
	ST_FLOOR,
	ST_CEILING,
	ST_FOFTOP,
	ST_FOFBOTTOM,
	ST_FOFWALL
};

#define SURFACE_TYPE_SHIFT 24


typedef struct _LINESURFACES
{
	SURFACE3D	s3dUpper, s3dMiddle, s3dLower;
	int			iLastRenderedFrame;
} LINESURFACES;


typedef struct _FOF_SURFACE_ARRAY
{
	int		cSurfaces;
	SURFACE3D	s3dPlanes[1];
} FOF_SURFACE_ARRAY;


typedef struct _MAPQUAD MAPQUAD;

struct _MAPQUAD
{
	FPOINT	v1, v2, v3, v4;
	int		iSubsectorID;
	MAPQUAD	*lpmapquadNext;
};

struct _MAPSURFACES
{
	int					cWalls, cSubsectors;
	LINESURFACES		*lplinesurfaces;

	SURFACE3D			*lps3dFloorCeilPolys;
	MAPQUAD				**lplpmapquadSectorQuads;
	int					*lpiSSLastRenderedFrame;

	FOF_SURFACE_ARRAY	**lplpfsaFOFPlanesBySS;
	FOF_SURFACE_ARRAY	**lplpfsaFOFWallsByLinedef;
	FOF_LIST			*lpflFOFsByTag[65536];
};


/* Types for Seidel's algorithm. */

typedef enum _SEIDEL_NODETYPE
{
	SNT_TRAPEZOID,
	SNT_SPLITHORIZ,
	SNT_SPLITLINEDEF
} SEIDEL_NODETYPE;

typedef enum _SEIDEL_TRAPEZOID_FLAGS
{
	SNTF_GHOST = 1
} SEIDEL_TRAPEZOID_FLAGS;

typedef struct _SEIDEL_SPLITHORIZ
{
	short	y, nFract;
} SEIDEL_SPLITHORIZ;

typedef struct _SEIDEL_TRAPEZOID SEIDEL_TRAPEZOID;
typedef struct _SEIDEL_NODE SEIDEL_NODE;

typedef struct _SEIDEL_GHOST
{
	int					iLinedefSplit;
	SEIDEL_TRAPEZOID	*lptrapezoidLeft, *lptrapezoidRight;
} SEIDEL_GHOST;

struct _SEIDEL_TRAPEZOID
{
	int					iLinedefLeft, iLinedefRight;
	SEIDEL_SPLITHORIZ	splithorizTop, splithorizBottom;
	SEIDEL_TRAPEZOID	*lptrapezoidBelowLeft, *lptrapezoidBelowRight;
	BOOL				bUnboundedAbove;

	SEIDEL_TRAPEZOID	*lptrapezoidNext;
	SEIDEL_NODE			*lpnode;

	SEIDEL_GHOST		ghost;

	int					iSubsectorID;

	BYTE				byFlags;
};

struct _SEIDEL_NODE
{
	SEIDEL_NODETYPE		nodetype;

	union
	{
		int					iLinedef;
		SEIDEL_SPLITHORIZ	splithoriz;
		SEIDEL_TRAPEZOID	*lptrapezoid;
	};

	SEIDEL_NODE		*lpnodeLA;	/* Left or above. */
	SEIDEL_NODE		*lpnodeRB;	/* Right or below. */
};

typedef struct _BSP_NODE BSP_NODE;

struct _BSP_NODE
{
	HOMOG_POINT	hptPlaneNormal;
	BSP_NODE	*lpbspnodeFront, *lpbspnodeBack;
	GROWARRAY	growarrayLineSurfIndices;
	int			iSubsectorID;
};

struct _BSP_TREE
{
	BSP_NODE	*lpbspnodeRoot;
	HANDLE		hHeap;
	int			iSubsectors;
};

typedef struct _CLIPPING_PLANE CLIPPING_PLANE;

struct _CLIPPING_PLANE
{
	float x, y;
	HOMOG_POINT *lphptPlane;
	CLIPPING_PLANE *lpclipplaneNext;
};


#define PERSPECTIVE_NEAR_Z	50
#define PERSPECTIVE_FAR_Z	5000

#define BALANCE_LEVELS 4


static void UpdateWall(SURFACE3D *lps3d, HWND hwndMap, LPCSTR sTexNameA, MAPVERTEX *lpvxLeft, MAPVERTEX *lpvxRight, MAPSIDEDEF *lpsd, MAPLINEDEF *lpld, MAPSECTOR *lpsector, short zBottom, short zTop, UINT uiSurfaceType, int iNameIndex);
static void RenderSurface3D(MAPVIEW *lpmapview, SURFACE3D *lps3d);
static void BindTexture(TEXTURE *lptex);
static void TrapezoidDecomposition(MAP *lpmap, BSP_TREE *lpbsptree, MAPQUAD ***lplplpmapquadSectorQuads);
static __inline SEIDEL_NODE* SeidelGetTrapezoidForVertex(SEIDEL_NODE *lpnodeRoot, MAP *lpmap, int iVertex);
static SEIDEL_NODE* SeidelGetTrapezoidForVertexDirected(SEIDEL_NODE *lpnodeRoot, MAP *lpmap, int iVertex, int iDirectionVertex);

static BOOL IsTrapezoidDegenerateTop(MAP *lpmap, SEIDEL_TRAPEZOID *lptrapezoid);
#if 0
static BOOL IsTrapezoidDegenerateBottom(MAP *lpmap, SEIDEL_TRAPEZOID *lptrapezoid);
#endif

static void SeidelSplitTrapezoidHorizontally(SEIDEL_NODE *lpnodeOldTrapezoid, SEIDEL_SPLITHORIZ splithoriz, HANDLE hSeidelHeap, SEIDEL_TRAPEZOID **lplptrapezoidList, SEIDEL_NODE **lplpnodeUpperSplit, SEIDEL_NODE **lplpnodeLowerSplit);
static void SeidelDeGhostifyBelow(MAP *lpmap, SEIDEL_TRAPEZOID *lptrapezoid);

static void CreateFOFWallsForLinedef(MAP *lpmap, MAPSURFACES *lpmapsurfaces, HWND hwndMap, int iLinedef);
static void RenderBSPNode(MAP *lpmap, MAPVIEW *lpmapview, BSP_NODE *lpbspnode, CLIPPING_PLANE *lpclipplane, MAPSURFACES *lpmapsurfaces, int iFrame);
static __inline float SideOfPlane2D(HOMOG_POINT *lphptPlaneNormal, float x, float y);
static float AxialIntersectionWithPlane(HOMOG_POINT *lphptPlaneNormal, HOMOG_POINT *lphptOnLine, HOMOG_COORD hcoordLine);
static void SeidelMakeBSPBranch(MAP *lpmap, SEIDEL_NODE *lpseidelnode, BSP_NODE **lplpbspnode, short yTop, short yBottom, int *lpiNextSSID, HANDLE hHeap);
static __inline void AddHorizLDToBSPTree(MAP *lpmap, int iLinedef, BSP_TREE *lpbsptree);
static void AddHorizLDSegmentToBSPTree(MAP *lpmap, int iLinedef, float x1, float x2, short y, BSP_NODE *lpbspnode);
static void IntersectPlane2D(HOMOG_POINT *lphptPlane1, HOMOG_POINT *lphptPlane2, float *lpx, float *lpy);
static float ClipPlanes(CLIPPING_PLANE **lplpclipplaneIn, HOMOG_POINT *lphptNewPlane, CLIPPING_PLANE **lplpclipplaneReverse);
static void FreeClippingPlane(CLIPPING_PLANE *lpclipplane);
static CLIPPING_PLANE* GetFrustumClipPlanes(MAPVIEW *lpmapview);
static __inline UINT MakeSurfaceColour(BYTE byBrightness);
static void ResetModelviewMatrix(MAPVIEW *lpmapview);
static void DrawCrosshair(MAPVIEW *lpmapview);
static int Invert4x4Matrix(const double m[16], double invOut[16]);
static void MakeBalancedLindefIndexArray(MAP *lpmap, int *lpiShuffledLinedefs);
static void TransformVector(GLdouble fMatrix[16], const GLdouble fInVec[4], GLdouble fOutVec[4]);
static void GetFrustumClipCoordinates(MAPVIEW *lpmapview);
static BOOL LinedefInFrustum(MAP *lpmap, int iLinedef, MAPVIEW *lpmapview);



/* GenerateMapSurfaces
 *   Preprocesses a map to determine the surfaces that the renderer must draw.
 *
 * Parameters:
 *   MAP*				lpmap		Map to redraw.
 *   HWND				hwndMap		Map window handle.
 *   LPCTSTR			szSky		Name of sky texture. Empty string for none.
 *   BSP_TREE**			lplpbsptree	BSP tree, to be created from scratch.
 *
 * Return value: MAPSURFACES*
 *   Structure describing the surfaces.
 *
 * Remarks:
 *   Free the returned structure with FreeMapSurfaces.
 */
MAPSURFACES* GenerateMapSurfaces(MAP *lpmap, HWND hwndMap, LPCTSTR szSky, BSP_TREE **lplpbsptree, CONFIG *lpcfgFOFTypes, FOF_FLAGS *lpfofflags, int iColourmap)
{
	MAPSURFACES *lpmapsurfaces = ProcHeapAlloc(sizeof(MAPSURFACES));
	int iLinedef, iSector;
	TCHAR szTexName[TEXNAME_BUFFER_LENGTH];

	ZeroMemory(lpmapsurfaces, sizeof(MAPSURFACES));

	lpmapsurfaces->cWalls = 2 * lpmap->iLinedefs;
	lpmapsurfaces->lplinesurfaces = ProcHeapAlloc(lpmapsurfaces->cWalls * sizeof(LINESURFACES));
	ZeroMemory(lpmapsurfaces->lplinesurfaces, lpmapsurfaces->cWalls * sizeof(LINESURFACES));

	/* Allocate a pointer to a FOF array for each linedef (but don't actually
	 * allocate the FOF arrays themselves until we know that the respective
	 * linedef needs it).
	 */
	lpmapsurfaces->lplpfsaFOFWallsByLinedef = ProcHeapAlloc(lpmap->iLinedefs * sizeof(FOF_SURFACE_ARRAY*));
	ZeroMemory(lpmapsurfaces->lplpfsaFOFWallsByLinedef, lpmap->iLinedefs * sizeof(FOF_SURFACE_ARRAY*));

	/* Find FOFs. */
	if(lpcfgFOFTypes)
	{
		for(iLinedef = 0; iLinedef < lpmap->iLinedefs; iLinedef++)
		{
			MAPLINEDEF *lpld = &lpmap->linedefs[iLinedef];

			/* If this is the first FOF linedef we've seen for this tag, build
			 * the list for the ENTIRE tag.
			 */
			if((lpld->tag != 0 || lpld->effect != 0) &&
				!lpmapsurfaces->lpflFOFsByTag[lpld->tag] &&
				IsFOFLinedef(lpmap, iLinedef, lpcfgFOFTypes))
			{
				lpmapsurfaces->lpflFOFsByTag[lpld->tag] = AllocateFOFList();
				AddFOFsToListByTag(lpmap, lpld->tag, lpmapsurfaces->lpflFOFsByTag[lpld->tag], lpcfgFOFTypes, lpfofflags, iColourmap);
			}
		}
	}

	/* Add walls. */
	for(iLinedef = 0; iLinedef < lpmap->iLinedefs; iLinedef++)
	{
		SetNeededSurfacesForLinedef(lpmap, iLinedef, szSky, LS_FRONT, lpmapsurfaces, hwndMap);
		SetNeededSurfacesForLinedef(lpmap, iLinedef, szSky, LS_BACK, lpmapsurfaces, hwndMap);
	}

	/* Add FOF walls. */
	for(iLinedef = 0; iLinedef < lpmap->iLinedefs; iLinedef++)
		CreateFOFWallsForLinedef(lpmap, lpmapsurfaces, hwndMap, iLinedef);

	/* Initialise BSP tree. Complete guess at the size of the heap. */
	*lplpbsptree = ProcHeapAlloc(sizeof(BSP_TREE));
	(*lplpbsptree)->hHeap = HeapCreate(HEAP_GENERATE_EXCEPTIONS | HEAP_NO_SERIALIZE, lpmap->iLinedefs * sizeof(BSP_NODE) * 4, 0);
	(*lplpbsptree)->iSubsectors = 0;

	/* Get trapezoidation. */
	TrapezoidDecomposition(lpmap, *lplpbsptree, &lpmapsurfaces->lplpmapquadSectorQuads);

	/* Allocate space for floors and ceilings. */
	lpmapsurfaces->cSubsectors = (*lplpbsptree)->iSubsectors;
	lpmapsurfaces->lps3dFloorCeilPolys = ProcHeapAlloc(lpmapsurfaces->cSubsectors * sizeof(SURFACE3D) * 2);
	lpmapsurfaces->lpiSSLastRenderedFrame = ProcHeapAlloc(lpmapsurfaces->cSubsectors * sizeof(int));
	ZeroMemory(lpmapsurfaces->lps3dFloorCeilPolys, lpmapsurfaces->cSubsectors * sizeof(SURFACE3D) * 2);
	ZeroMemory(lpmapsurfaces->lpiSSLastRenderedFrame, lpmapsurfaces->cSubsectors * sizeof(int));

	/* Allocate memory for FOF planes per subsector as we did above for walls
	 * per linedef.
	 */
	lpmapsurfaces->lplpfsaFOFPlanesBySS = ProcHeapAlloc(lpmapsurfaces->cSubsectors * sizeof(FOF_SURFACE_ARRAY*));
	ZeroMemory(lpmapsurfaces->lplpfsaFOFPlanesBySS, lpmapsurfaces->cSubsectors * sizeof(FOF_SURFACE_ARRAY*));

	/* Create the polys. */
	for(iSector = 0; iSector < lpmap->iSectors; iSector++)
	{
		MAPSECTOR *lpsector = &lpmap->sectors[iSector];

		/* Skip sectors that we don't draw. */
		if(lpsector->hceiling > lpsector->hfloor)
		{
			TEXTURE *lptexFloor, *lptexCeil;
			MAPQUAD *lpmapquadCurrent;
			FOF_LIST *lpfoflist = lpmapsurfaces->lpflFOFsByTag[lpsector->tag];

			wsprintf(szTexName, TEXT("%.8hs"), lpsector->tfloor);
			GetTextureForMap(hwndMap, szTexName, &lptexFloor, TF_FLAT);
			wsprintf(szTexName, TEXT("%.8hs"), lpsector->tceiling);
			GetTextureForMap(hwndMap, szTexName, &lptexCeil, TF_FLAT);

			for(lpmapquadCurrent = lpmapsurfaces->lplpmapquadSectorQuads[iSector];
				lpmapquadCurrent;
				lpmapquadCurrent = lpmapquadCurrent->lpmapquadNext)
			{
				/* Floor. */

				SURFACE3D *lps3d = lpmapsurfaces->lps3dFloorCeilPolys + lpmapquadCurrent->iSubsectorID * 2;

				lps3d->cVertices = 4;
				lps3d->dwFlags = SF_EXISTS;
				lps3d->lptex = lptexFloor;
				lps3d->lpvx3dVertices = ProcHeapAlloc(lps3d->cVertices * sizeof(VERTEX3D));
				lps3d->uiName = (ST_FLOOR << SURFACE_TYPE_SHIFT) | iSector;
				lps3d->uiColour = MakeSurfaceColour((BYTE)lpsector->brightness);

				/* Preserve orientation. */
				lps3d->lpvx3dVertices[0].x = lpmapquadCurrent->v1.x;
				lps3d->lpvx3dVertices[0].y = lpmapquadCurrent->v1.y;
				lps3d->lpvx3dVertices[0].z = lpsector->hfloor;
				lps3d->lpvx3dVertices[0].s = lptexFloor ? lpmapquadCurrent->v1.x / lptexFloor->cx : 0.0f;
				lps3d->lpvx3dVertices[0].t = lptexFloor ? lpmapquadCurrent->v1.y / lptexFloor->cy : 0.0f;

				lps3d->lpvx3dVertices[1].x = lpmapquadCurrent->v2.x;
				lps3d->lpvx3dVertices[1].y = lpmapquadCurrent->v2.y;
				lps3d->lpvx3dVertices[1].z = lpsector->hfloor;
				lps3d->lpvx3dVertices[1].s = lptexFloor ? lpmapquadCurrent->v2.x / lptexFloor->cx : 0.0f;
				lps3d->lpvx3dVertices[1].t = lptexFloor ? lpmapquadCurrent->v2.y / lptexFloor->cy : 0.0f;

				lps3d->lpvx3dVertices[2].x = lpmapquadCurrent->v3.x;
				lps3d->lpvx3dVertices[2].y = lpmapquadCurrent->v3.y;
				lps3d->lpvx3dVertices[2].z = lpsector->hfloor;
				lps3d->lpvx3dVertices[2].s = lptexFloor ? lpmapquadCurrent->v3.x / lptexFloor->cx : 0.0f;
				lps3d->lpvx3dVertices[2].t = lptexFloor ? lpmapquadCurrent->v3.y / lptexFloor->cy : 0.0f;

				lps3d->lpvx3dVertices[3].x = lpmapquadCurrent->v4.x;
				lps3d->lpvx3dVertices[3].y = lpmapquadCurrent->v4.y;
				lps3d->lpvx3dVertices[3].z = lpsector->hfloor;
				lps3d->lpvx3dVertices[3].s = lptexFloor ? lpmapquadCurrent->v4.x / lptexFloor->cx : 0.0f;
				lps3d->lpvx3dVertices[3].t = lptexFloor ? lpmapquadCurrent->v4.y / lptexFloor->cy : 0.0f;

				/* The ceiling follows the floor immediately in the array. */
				lps3d++;

				/* Ceiling. Yes, copy-paste... */

				lps3d->cVertices = 4;
				lps3d->dwFlags = SF_EXISTS;
				lps3d->lptex = lptexCeil;
				lps3d->lpvx3dVertices = ProcHeapAlloc(lps3d->cVertices * sizeof(VERTEX3D));
				lps3d->uiName = (ST_CEILING << SURFACE_TYPE_SHIFT) | iSector;
				lps3d->uiColour = MakeSurfaceColour((BYTE)lpsector->brightness);

				/* Reverse orientation. */
				lps3d->lpvx3dVertices[0].x = lpmapquadCurrent->v1.x;
				lps3d->lpvx3dVertices[0].y = lpmapquadCurrent->v1.y;
				lps3d->lpvx3dVertices[0].z = lpsector->hceiling;
				lps3d->lpvx3dVertices[0].s = lptexCeil ? lpmapquadCurrent->v1.x / lptexCeil->cx : 0.0f;
				lps3d->lpvx3dVertices[0].t = lptexCeil ? lpmapquadCurrent->v1.y / lptexCeil->cy : 0.0f;

				lps3d->lpvx3dVertices[1].x = lpmapquadCurrent->v4.x;
				lps3d->lpvx3dVertices[1].y = lpmapquadCurrent->v4.y;
				lps3d->lpvx3dVertices[1].z = lpsector->hceiling;
				lps3d->lpvx3dVertices[1].s = lptexCeil ? lpmapquadCurrent->v4.x / lptexCeil->cx : 0.0f;
				lps3d->lpvx3dVertices[1].t = lptexCeil ? lpmapquadCurrent->v4.y / lptexCeil->cy : 0.0f;

				lps3d->lpvx3dVertices[2].x = lpmapquadCurrent->v3.x;
				lps3d->lpvx3dVertices[2].y = lpmapquadCurrent->v3.y;
				lps3d->lpvx3dVertices[2].z = lpsector->hceiling;
				lps3d->lpvx3dVertices[2].s = lptexCeil ? lpmapquadCurrent->v3.x / lptexCeil->cx : 0.0f;
				lps3d->lpvx3dVertices[2].t = lptexCeil ? lpmapquadCurrent->v3.y / lptexCeil->cy : 0.0f;

				lps3d->lpvx3dVertices[3].x = lpmapquadCurrent->v2.x;
				lps3d->lpvx3dVertices[3].y = lpmapquadCurrent->v2.y;
				lps3d->lpvx3dVertices[3].z = lpsector->hceiling;
				lps3d->lpvx3dVertices[3].s = lptexCeil ? lpmapquadCurrent->v2.x / lptexCeil->cx : 0.0f;
				lps3d->lpvx3dVertices[3].t = lptexCeil ? lpmapquadCurrent->v2.y / lptexCeil->cy : 0.0f;

				if(lpfoflist)
				{
					FOF_LIST_NODE *lpfln;
					int iFOFinSS, cFOFs;
					FOF_SURFACE_ARRAY *lpfsa;

					/* Floor and ceiling polys of target sector, which we'll
					 * copy and then adjust for the FOFs.
					 */
					SURFACE3D *lps3dTargetFlr = lps3d - 1;
					SURFACE3D *lps3dTargetCeil = lps3d;

					/* Allocate memory for the planes. The structure itself has
					 * room for one.
					 */
					cFOFs = CountFOFsInList(lpfoflist);
					lpfsa = lpmapsurfaces->lplpfsaFOFPlanesBySS[lpmapquadCurrent->iSubsectorID] =
						ProcHeapAlloc(sizeof(FOF_SURFACE_ARRAY) + (cFOFs * 2 - 1) * sizeof(SURFACE3D));
					lpfsa->cSurfaces = cFOFs * 2;

					/* Create planes for each FOF. */
					for(lpfln = lpfoflist->flnHeader.lpflnNext, iFOFinSS = 0; lpfln->lpflnNext; lpfln = lpfln->lpflnNext, iFOFinSS++)
					{
						SURFACE3D *lps3dFOF = &lpfsa->s3dPlanes[iFOFinSS * 2];
						MAPSECTOR *lpsectorControl = &lpmap->sectors[lpfln->fof.iControlSector];
						int iSurfVx;

						/* Bottom. */
						lps3dFOF->uiName = (ST_FOFBOTTOM << SURFACE_TYPE_SHIFT) | lpfln->fof.iLinedef;
						lps3dFOF->uiColour = MakeSurfaceColour((BYTE)lpsectorControl->brightness);
						lps3dFOF->dwFlags = SF_EXISTS;
						lps3dFOF->cVertices = 4;
						lps3dFOF->lpvx3dVertices = ProcHeapAlloc(lps3dFOF->cVertices * sizeof(VERTEX3D));
						CopyMemory(lps3dFOF->lpvx3dVertices, lps3dTargetCeil->lpvx3dVertices, lps3dFOF->cVertices * sizeof(VERTEX3D));
						for(iSurfVx = 0; iSurfVx < 4; iSurfVx++)
							lps3dFOF->lpvx3dVertices[iSurfVx].z = lpsectorControl->hfloor;

						wsprintf(szTexName, TEXT("%.8hs"), lpsectorControl->tfloor);
						GetTextureForMap(hwndMap, szTexName, &lps3dFOF->lptex, TF_FLAT);

						lps3dFOF++;

						/* Top. */
						lps3dFOF->uiName = (ST_FOFTOP << SURFACE_TYPE_SHIFT) | lpfln->fof.iLinedef;
						lps3dFOF->uiColour = MakeSurfaceColour((BYTE)lpsectorControl->brightness);
						lps3dFOF->dwFlags = SF_EXISTS;
						lps3dFOF->cVertices = 4;
						lps3dFOF->lpvx3dVertices = ProcHeapAlloc(lps3dFOF->cVertices * sizeof(VERTEX3D));
						CopyMemory(lps3dFOF->lpvx3dVertices, lps3dTargetFlr->lpvx3dVertices, lps3dFOF->cVertices * sizeof(VERTEX3D));
						for(iSurfVx = 0; iSurfVx < 4; iSurfVx++)
							lps3dFOF->lpvx3dVertices[iSurfVx].z = lpsectorControl->hceiling;

						wsprintf(szTexName, TEXT("%.8hs"), lpsectorControl->tceiling);
						GetTextureForMap(hwndMap, szTexName, &lps3dFOF->lptex, TF_FLAT);
					}
				}
			}
		}
	}

	return lpmapsurfaces;
}



/* UpdateWall
 *   Updates or creates a surface for a wall.
 *
 * Parameters:
 *   SURFACE3D*		lps3d					Structure in which to create
 *											surface.
 *   HWND			hwndMap					Map window.
 *   LPCSTR			sTexNameA				Texture name.
 *   MAPVERTEX		*lpvxLeft, *lpvxRight	Vertices as viewed.
 *   MAPSIDEDEF*	lpsd					Defining sidedef.
 *   MAPLINEDEF*	lpld					Linedef. For a FOF, this is the
 *											control linedef.
 *   MAPSECTOR*		lpsector				Facing sector for real walls;
 *											control sector for FOF walls.
 *   short			zBottom, zTop			z co-ordinates.
 *   UINT			uiSurfaceType			One of ENUM_SURFACE_TYPES specifying
 *											which sort of surface this is.
 *   int			iNameIndex				Index of linedef, sector or FOF.
 *
 * Return value: None
 */
static void UpdateWall(SURFACE3D *lps3d, HWND hwndMap, LPCSTR sTexNameA, MAPVERTEX *lpvxLeft, MAPVERTEX *lpvxRight, MAPSIDEDEF *lpsd, MAPLINEDEF *lpld, MAPSECTOR *lpsector, short zBottom, short zTop, UINT uiSurfaceType, int iNameIndex)
{
	TCHAR szTexName[TEXNAME_BUFFER_LENGTH];
	TEXTURE *lptex;
	BOOL bPegTop = FALSE;
	float fYExtraOffset = 0.0f;
	short cxOffset = lpsd->tx;
	short cyOffset = lpsd->ty;

	wsprintf(szTexName, TEXT("%.8hs"), sTexNameA);
	GetTextureForMap(hwndMap, szTexName, &lptex, TF_TEXTURE);
	lps3d->lptex = lptex;
	
	if(!(lps3d->dwFlags & SF_EXISTS))
	{
		lps3d->cVertices = 4;
		lps3d->lpvx3dVertices = ProcHeapAlloc(lps3d->cVertices * sizeof(VERTEX3D));
		lps3d->dwFlags = SF_EXISTS;
		lps3d->uiName = (uiSurfaceType << SURFACE_TYPE_SHIFT) | iNameIndex;
	}

	/* Lighting. */
	lps3d->uiColour = MakeSurfaceColour((BYTE)lpsector->brightness);

	/* Peg (etc.) according to surface type and linedef flags. */
	switch(uiSurfaceType)
	{
	case ST_FRONTLOWER:
	case ST_BACKLOWER:
	case ST_FOFWALL:
		if((lpld->flags & LDF_LOWERUNPEGGED) && lptex)
		{
			/* Lower-unpegging of lower textures is weird. Don't set bPegTop
			 * (since we peg from the top of the sector).
			 */
			fYExtraOffset = (float)(zTop - lpsector->hceiling) / lptex->cy;
		}
		else
			bPegTop = TRUE;

		break;

	case ST_FRONTMIDDLE:
	case ST_BACKMIDDLE:
		bPegTop = !(lpld->flags & LDF_LOWERUNPEGGED);

		/* Double-sided middle textures don't repeat. */
		if(lptex && (lpld->flags & LDF_DOUBLESIDED))
		{
			zBottom += cyOffset;
			cyOffset = 0;
			zTop = zBottom + lptex->cy;
			lps3d->dwFlags |= SF_CLAMP;
		}

		break;

	case ST_FRONTUPPER:
	case ST_BACKUPPER:
		bPegTop = lpld->flags & LDF_UPPERUNPEGGED;
		break;
	}

	/* Correct for pegging. */
	if(bPegTop && lptex)
		fYExtraOffset += (float)(zBottom - zTop) / lptex->cy;

	/* Make offset positive. */
	fYExtraOffset -= floorf(fYExtraOffset);


	/* Vertices. */

	lps3d->lpvx3dVertices[0].x = lpvxLeft->x;
	lps3d->lpvx3dVertices[0].y = lpvxLeft->y;
	lps3d->lpvx3dVertices[0].z = zBottom;

	lps3d->lpvx3dVertices[1].x = lpvxRight->x;
	lps3d->lpvx3dVertices[1].y = lpvxRight->y;
	lps3d->lpvx3dVertices[1].z = zBottom;

	lps3d->lpvx3dVertices[2].x = lpvxRight->x;
	lps3d->lpvx3dVertices[2].y = lpvxRight->y;
	lps3d->lpvx3dVertices[2].z = zTop;

	lps3d->lpvx3dVertices[3].x = lpvxLeft->x;
	lps3d->lpvx3dVertices[3].y = lpvxLeft->y;
	lps3d->lpvx3dVertices[3].z = zTop;

	/* Texture co-ordinates. */

	lps3d->lpvx3dVertices[0].s = lptex ? (float)cxOffset / lptex->cx : 0.0f;
	lps3d->lpvx3dVertices[0].t = fYExtraOffset + (lptex ? (float)cyOffset / lptex->cy : 0.0f);

	lps3d->lpvx3dVertices[1].s = lps3d->lpvx3dVertices[0].s + (lptex ? (float)distancei(lpvxLeft->x, lpvxLeft->y, lpvxRight->x, lpvxRight->y) / lptex->cx : 0.0f);
	lps3d->lpvx3dVertices[1].t = lps3d->lpvx3dVertices[0].t;
	lps3d->lpvx3dVertices[2].s = lps3d->lpvx3dVertices[1].s;
	lps3d->lpvx3dVertices[2].t = lps3d->lpvx3dVertices[0].t + (lptex ? (float)(lps3d->lpvx3dVertices[2].z - lps3d->lpvx3dVertices[0].z) / lptex->cy : 0.0f);
	lps3d->lpvx3dVertices[3].s = lps3d->lpvx3dVertices[0].s;
	lps3d->lpvx3dVertices[3].t = lps3d->lpvx3dVertices[2].t;
}


/* FreeMapSurfaces
 *   Frees a map surfaces structure.
 *
 * Parameters:
 *   MAP			*lpmap			Map.
 *   MAPSURFACES	*lpmapsurfaces	Structure to free.
 *   BSP_TREE		*lpbsptree		Associated BSP tree.
 *
 * Return value: None
 */
void FreeMapSurfaces(MAP *lpmap, MAPSURFACES *lpmapsurfaces, BSP_TREE *lpbsptree)
{
	int i;

	for(i = 0; i < lpbsptree->iSubsectors * 2; i++)
		if(lpmapsurfaces->lps3dFloorCeilPolys[i].lpvx3dVertices)
			ProcHeapFree(lpmapsurfaces->lps3dFloorCeilPolys[i].lpvx3dVertices);

	for(i = 0; i < lpbsptree->iSubsectors; i++)
		if(lpmapsurfaces->lplpfsaFOFPlanesBySS[i])
			ProcHeapFree(lpmapsurfaces->lplpfsaFOFPlanesBySS[i]);

	/* Free trapezoidation. */
	for(i = 0; i < lpmap->iSectors; i++)
	{
		MAPQUAD *lpmapquadCurrent = lpmapsurfaces->lplpmapquadSectorQuads[i];
		MAPQUAD *lpmapquadNext;

		while(lpmapquadCurrent)
		{
			lpmapquadNext = lpmapquadCurrent->lpmapquadNext;
			ProcHeapFree(lpmapquadCurrent);
			lpmapquadCurrent = lpmapquadNext;
		}
	}

	ProcHeapFree(lpmapsurfaces->lplpmapquadSectorQuads);

	ProcHeapFree(lpmapsurfaces->lplinesurfaces);
	ProcHeapFree(lpmapsurfaces->lps3dFloorCeilPolys);
	ProcHeapFree(lpmapsurfaces->lpiSSLastRenderedFrame);
	ProcHeapFree(lpmapsurfaces->lplpfsaFOFPlanesBySS);
	ProcHeapFree(lpmapsurfaces);

	HeapDestroy(lpbsptree->hHeap);
	ProcHeapFree(lpbsptree);
}


/* RedrawMap3D
 *   Redraws the map to the backbuffer, in either isometric or perspective
 *   views.
 *
 * Parameters:
 *   MAPVIEW*	lpmapview		Parameters affecting what to draw.
 *
 * Return value: None
 */
void RedrawMap3D(MAP *lpmap, MAPSURFACES *lpmapsurfaces, BSP_TREE *lpbsptree, MAPVIEW *lpmapview)
{
	if(!lpbsptree || !lpmapsurfaces)
		return;

	GL_CLEAR_COLOUR_FROM_INDEX(CLR_BACKGROUND);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	/* A bit of an accidental design difference between iso and perspective: we
	 * accumulate transformations in iso mode, but in perspective we store state
	 * and reconstruct the matrix every frame. A uniform approach would be
	 * nicer.
	 */
	if(lpmapview->rendermode == RM_3D)
		ResetModelviewMatrix(lpmapview);
	
	if(!lpmapview->bPicking)
	{
		Set3DProjection(lpmapview);

		if(lpmapview->nAngleDelta && !lpmapview->bCentreDirty)
		{
			glTranslated(lpmapview->xCentre, lpmapview->yCentre, 0);
			glRotatef((GLfloat)lpmapview->nAngleDelta, 0, 0, 1);
			glTranslated(-lpmapview->xCentre, -lpmapview->yCentre, 0);

			lpmapview->nAngleDelta = 0;
		}
	}

	RenderBSPNode(lpmap, lpmapview, lpbsptree->lpbspnodeRoot, GetFrustumClipPlanes(lpmapview), lpmapsurfaces, lpmapview->iFrame);

	if(lpmapview->rendermode == RM_3D && !lpmapview->bPicking)
		DrawCrosshair(lpmapview);

	lpmapview->iFrame++;
}

static void RenderBSPNode(MAP *lpmap, MAPVIEW *lpmapview, BSP_NODE *lpbspnode, CLIPPING_PLANE *lpclipplane, MAPSURFACES *lpmapsurfaces, int iFrame)
{
	CLIPPING_PLANE *lpclipplaneReverse = NULL;
	float fSide = ClipPlanes(&lpclipplane, &lpbspnode->hptPlaneNormal, &lpclipplaneReverse);
	CLIPPING_PLANE *lpclipplaneFront = (fSide >= 0) ? lpclipplane : lpclipplaneReverse;
	CLIPPING_PLANE *lpclipplaneBack = (fSide >= 0) ? lpclipplaneReverse : lpclipplane;
	float fViewSide = (lpmapview->rendermode == RM_3D) ? SideOfPlane2D(&lpbspnode->hptPlaneNormal, lpmapview->x, lpmapview->y) : 0.0f;
	struct {CLIPPING_PLANE *lpclipplane; BSP_NODE *lpbspnode;} clipnodepair[] =
	{
		{lpclipplaneBack, lpbspnode->lpbspnodeBack},
		{lpclipplaneFront, lpbspnode->lpbspnodeFront},
	};
	int i;
	
	for(i = 0; i < (int)NUM_ELEMENTS(clipnodepair); i++)
	{
		/* If on the front, render back first. */
		int iIndex = (fViewSide >= 0.0f) ? i : 1 - i;

		if(clipnodepair[iIndex].lpclipplane)
		{
			if(clipnodepair[iIndex].lpbspnode)
				RenderBSPNode(lpmap, lpmapview, clipnodepair[iIndex].lpbspnode, clipnodepair[iIndex].lpclipplane, lpmapsurfaces, iFrame);
			else
				FreeClippingPlane(clipnodepair[iIndex].lpclipplane);
		}
	}

	if(lpbspnode->growarrayLineSurfIndices.lpvElements)
	{
		int iSurfaceSets;
		int *lpiSurfaceSets = lpbspnode->growarrayLineSurfIndices.lpvElements;

		for(iSurfaceSets = 0; iSurfaceSets < (int)lpbspnode->growarrayLineSurfIndices.cElements; iSurfaceSets++)
		{
			LINESURFACES *lplinesurfaces = &lpmapsurfaces->lplinesurfaces[lpiSurfaceSets[iSurfaceSets]];
			int iLinedef = lpiSurfaceSets[iSurfaceSets] >> 1;
			int iSideOfLine = (lpiSurfaceSets[iSurfaceSets] & 1) ? LS_BACK : LS_FRONT;
			
			if(!LinedefInFrustum(lpmap, iLinedef, lpmapview))
				continue;

			if(lplinesurfaces->iLastRenderedFrame != iFrame)
			{
				/* In perspective mode we can cull back-facing surfaces. */
				FPOINT fptView = {lpmapview->x, lpmapview->y};
				if(lpmapview->rendermode != RM_3D || SideOfLinedef(lpmap, iLinedef, &fptView) == iSideOfLine)
				{
					/* Draw surfaces. */
					if(lplinesurfaces->s3dLower.dwFlags & SF_EXISTS) RenderSurface3D(lpmapview, &lplinesurfaces->s3dLower);
					if(lplinesurfaces->s3dMiddle.dwFlags & SF_EXISTS) RenderSurface3D(lpmapview, &lplinesurfaces->s3dMiddle);
					if(lplinesurfaces->s3dUpper.dwFlags & SF_EXISTS) RenderSurface3D(lpmapview, &lplinesurfaces->s3dUpper);
				}

				/* FOF walls. Hacky check for already-rendered-ness. */
				if(lpmapsurfaces->lplinesurfaces[lpiSurfaceSets[iSurfaceSets] ^ 1].iLastRenderedFrame != iFrame)
				{
					FOF_SURFACE_ARRAY *lpfsa = lpmapsurfaces->lplpfsaFOFWallsByLinedef[lpiSurfaceSets[iSurfaceSets] / 2];

					if(lpfsa)
					{
						int iFOFWall;

						for(iFOFWall = 0; iFOFWall < lpfsa->cSurfaces; iFOFWall++)
							if(lpfsa->s3dPlanes[iFOFWall].dwFlags & SF_EXISTS)
								RenderSurface3D(lpmapview, &lpfsa->s3dPlanes[iFOFWall]);
					}
				}

				lplinesurfaces->iLastRenderedFrame = iFrame;
			}
		}
	}

	if(lpbspnode->iSubsectorID >= 0 && lpmapsurfaces->lpiSSLastRenderedFrame[lpbspnode->iSubsectorID] != iFrame)
	{
		FOF_SURFACE_ARRAY *lpfsa;

		/* Floor. */
		SURFACE3D *lps3d = &lpmapsurfaces->lps3dFloorCeilPolys[lpbspnode->iSubsectorID * 2];
		if(lps3d->dwFlags & SF_EXISTS) RenderSurface3D(lpmapview, lps3d);

		/* Ceiling. */
		lps3d++;
		if(lps3d->dwFlags & SF_EXISTS) RenderSurface3D(lpmapview, lps3d);

		lpmapsurfaces->lpiSSLastRenderedFrame[lpbspnode->iSubsectorID] = iFrame;

		/* FOF planes. */
		if((lpfsa = lpmapsurfaces->lplpfsaFOFPlanesBySS[lpbspnode->iSubsectorID]))
		{
			int iFOFPlane;
			for(iFOFPlane = 0; iFOFPlane < lpfsa->cSurfaces; iFOFPlane++)
				RenderSurface3D(lpmapview, &lpfsa->s3dPlanes[iFOFPlane]);
		}
	}
}


/* Init3D
 *   Does initialisation for 3D rendering.
 *
 * Parameters:
 *   MAPVIEW*	lpmapview	Map view data.
 *
 * Return value: None
 */
void Init3D(MAPVIEW *lpmapview, float x, float y, float z)
{
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_CULL_FACE);
	glEnable(GL_TEXTURE_2D);
	glEnable(GL_BLEND);
	glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);

	lpmapview->fXAngle = lpmapview->fYAngle = 0;
	lpmapview->xCentre = lpmapview->x = x;
	lpmapview->yCentre = lpmapview->y = y;
	lpmapview->z = z + (lpmapview->rendermode == RM_3D) ? 128.0f : 0.0f;

	ResetModelviewMatrix(lpmapview);

	lpmapview->xLeft = 0;
	lpmapview->yTop = 0;
	lpmapview->bCentreDirty = TRUE;
}


/* RenderSurface3D
 *   Renders a surface.
 *
 * Parameters:
 *   MAPVIEW	*lpmapview	Renderer state.
 *   SURFACE3D	*lps3d		Surface to render.
 *
 * Return value: None
 */
static void RenderSurface3D(MAPVIEW *lpmapview, SURFACE3D *lps3d)
{
	static DWORD dwLastFlags = 0;
	VERTEX3D *lpvx3d;

	if(lpmapview->bPicking)
	{
		glColor4ubv((GLubyte*)&lps3d->uiName);
	}
	else
	{
		/* We might change our minds in a moment. */
		glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);

		if(lpmapview->uiHighlightName && lpmapview->uiHighlightName == lps3d->uiName)
		{
			float fInterpolationFactor[] = {0.5f, 0.5f, 0.5f, 0.5f};

			glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_COMBINE);
			glTexEnvi(GL_TEXTURE_ENV, GL_COMBINE_RGB, GL_INTERPOLATE);
			glTexEnvi(GL_TEXTURE_ENV, GL_SRC2_RGB, GL_CONSTANT);
			glTexEnvfv(GL_TEXTURE_ENV, GL_TEXTURE_ENV_COLOR, fInterpolationFactor);

			GL_COLOUR_FROM_INDEX(CLR_LINEHIGHLIGHT);
		}
		else
		{
			if(lps3d->lptex)
				glColor4ubv((GLubyte*)&lps3d->uiColour);
			else
				glColor4ub(255, 0, 0, 255);
		}

		BindTexture(lps3d->lptex);

		if((dwLastFlags ^ lps3d->dwFlags) & SF_CLAMP)
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, (lps3d->dwFlags & SF_CLAMP) ? GL_CLAMP : GL_REPEAT);
	}

	dwLastFlags = lps3d->dwFlags;

	glBegin(GL_TRIANGLE_FAN);
		for(lpvx3d = lps3d->lpvx3dVertices; (lpvx3d - lps3d->lpvx3dVertices) < lps3d->cVertices; lpvx3d++)
		{
			glTexCoord2f(lpvx3d->s, lpvx3d->t);
			glVertex3f(lpvx3d->x, lpvx3d->y, lpvx3d->z);
		}
	glEnd();
}


/* BindTexture
 *   Selects the specified texture for rendering.
 *
 * Parameters:
 *   TEXTURE	*lptex	Texture to select. NULL for no texture.
 *
 * Return value: None
 */
static void BindTexture(TEXTURE *lptex)
{
	if(lptex)
	{
		/* Create the GL texture if necessary. */
		if(!lptex->uiGLTexName)
		{
			glGenTextures(1, &lptex->uiGLTexName);
			glBindTexture(GL_TEXTURE_2D, lptex->uiGLTexName);
			glTexImage2D(GL_TEXTURE_2D, 0, 4, lptex->cx, lptex->cy, 0, GL_BGRA_EXT, GL_UNSIGNED_BYTE, lptex->lpvBits);

			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		}
		else
			glBindTexture(GL_TEXTURE_2D, lptex->uiGLTexName);
	}
	else
		glBindTexture(GL_TEXTURE_2D, 0);
}


#ifdef REPORT_BSP_STATS
static int BSPMaxDepth(BSP_NODE *lpbspnode)
{
	if(!lpbspnode) return 0;
	else
	{
		int i1 = BSPMaxDepth(lpbspnode->lpbspnodeFront);
		int i2 = BSPMaxDepth(lpbspnode->lpbspnodeBack);
		return 1 + max(i1, i2);
	}
}
#endif


/* TrapezoidDecomposition
 *   Decomposes all sectors into trapezoids.
 *
 * Parameters:
 *   MAP		*lpmap						Map.
 *   BSP_TREE	*lpbsptree					BSP tree. The caller should
 *											initialise the heap; we'll create
 *											the tree itself.
 *   MAPQUAD	***lplplpmapquadSectorQuads	Used to return array of trapezoids
 *											by sector.
 *
 * Return value: None
 */
static void TrapezoidDecomposition(MAP *lpmap, BSP_TREE *lpbsptree, MAPQUAD ***lplplpmapquadSectorQuads)
{
	/* We use Seidel's algorithm to decompose the map into trapezoids.
	 *
	 * Seidel, R. A simple and fast incremental randomized algorithm for
	 * computing trapezoidal decompositions and for triangulating polygons.
	 * Comput. Geom. 1 (1991), no. 1, 51-64.
	 */

	SEIDEL_NODE *lpnodeRoot;
	HANDLE hSeidelHeap;
	int *lpiShuffledLinedefs;
	BYTE *lpbyUsedVertices;
	int iLinedefIndex;
	SEIDEL_TRAPEZOID *lptrapezoidList;

	/* Create a heap for all the junk that we allocate, so that we can free the
	 * lot in one go. The size is an estimate; the heap is growable.
	 */
	hSeidelHeap = HeapCreate(HEAP_GENERATE_EXCEPTIONS | HEAP_NO_SERIALIZE, lpmap->iLinedefs * (2 * sizeof(SEIDEL_TRAPEZOID) + 4 * sizeof(SEIDEL_NODE)), 0);

	/* We start off with an infinite trapezoid consisting of the whole plane. */
	lpnodeRoot = HeapAlloc(hSeidelHeap, 0, sizeof(SEIDEL_NODE));
	lpnodeRoot->nodetype = SNT_TRAPEZOID;
	lpnodeRoot->lptrapezoid = HeapAlloc(hSeidelHeap, 0, sizeof(SEIDEL_TRAPEZOID));
	lpnodeRoot->lptrapezoid->lpnode = lpnodeRoot;
	lpnodeRoot->lptrapezoid->iLinedefLeft = -1;
	lpnodeRoot->lptrapezoid->iLinedefRight = -1;
	lpnodeRoot->lptrapezoid->byFlags = 0;
	lpnodeRoot->lptrapezoid->bUnboundedAbove = TRUE;
	lpnodeRoot->lptrapezoid->iSubsectorID = -1;
	lpnodeRoot->lptrapezoid->lptrapezoidBelowLeft = NULL;
	lpnodeRoot->lptrapezoid->lptrapezoidBelowRight = NULL;
	lpnodeRoot->lptrapezoid->lptrapezoidNext = NULL;

	lptrapezoidList = lpnodeRoot->lptrapezoid;

	/* Used to track whether each vertex has been used as an endpoint of a
	 * linedef already considered.
	 */
	lpbyUsedVertices = ProcHeapAlloc(lpmap->iVertices * sizeof(BYTE));
	ZeroMemory(lpbyUsedVertices, lpmap->iVertices * sizeof(BYTE));

	lpiShuffledLinedefs = ProcHeapAlloc(lpmap->iLinedefs * sizeof(int));


	/* Try to order the linedefs in a helpful way for splitting. */
	MakeBalancedLindefIndexArray(lpmap, lpiShuffledLinedefs);


	/* Add all the linedefs one by one, in the order just generated,
	 * splitting the trapezoids as we go.
	 */
	for(iLinedefIndex = 0; iLinedefIndex < lpmap->iLinedefs; iLinedefIndex++)
	{
		MAPLINEDEF *lpld = &lpmap->linedefs[lpiShuffledLinedefs[iLinedefIndex]];
		SEIDEL_NODE *lpnodeUpperTrapezoid, *lpnodeLowerTrapezoid;
		int iLowerVertex = lpld->v2, iUpperVertex = lpld->v1;
		MAPVERTEX *lpvxUpper = &lpmap->vertices[iUpperVertex];
		MAPVERTEX *lpvxLower = &lpmap->vertices[iLowerVertex];
		SEIDEL_TRAPEZOID *lptrapezoidCurrent;
		BOOL bFinishThreading;
		SEIDEL_TRAPEZOID *lptrapezoidPrevLeft, *lptrapezoidPrevRight;

		/* Skip horizontal linedefs. */
		if(lpvxUpper->y == lpvxLower->y)
			continue;

		/* Switch orientation if necessary. */
		if(lpvxUpper->y < lpvxLower->y)
		{
			int iIntermediate = iLowerVertex;
			iLowerVertex = iUpperVertex;
			iUpperVertex = iIntermediate;
			lpvxUpper = &lpmap->vertices[iUpperVertex];
			lpvxLower = &lpmap->vertices[iLowerVertex];
		}

		/* Make a horizontal split if we haven't already used this vertex. */
		if(!lpbyUsedVertices[iUpperVertex])
		{
			SEIDEL_SPLITHORIZ splithoriz = {lpvxUpper->y, lpvxUpper->x};
			SEIDEL_NODE *lpnodeUpperSplit, *lpnodeLowerSplit;

			/* Mark this vertex as visited and find the trapezoid that it lies in. */
			lpbyUsedVertices[iUpperVertex] = TRUE;
			SeidelSplitTrapezoidHorizontally(SeidelGetTrapezoidForVertex(lpnodeRoot, lpmap, iUpperVertex), splithoriz, hSeidelHeap, &lptrapezoidList, &lpnodeUpperSplit, &lpnodeLowerSplit);

			/* The trapezoid where the linedef starts. */
			lpnodeUpperTrapezoid = lpnodeLowerSplit;
		}
		else
			lpnodeUpperTrapezoid = SeidelGetTrapezoidForVertexDirected(lpnodeRoot, lpmap, iUpperVertex, iLowerVertex);

		/* Now do the same for the lower vertex. */
		if(!lpbyUsedVertices[iLowerVertex])
		{
			SEIDEL_SPLITHORIZ splithoriz = {lpvxLower->y, lpvxLower->x};
			SEIDEL_NODE *lpnodeUpperSplit, *lpnodeLowerSplit;

			/* Mark this vertex as visited and find the trapezoid that it lies in. */
			lpbyUsedVertices[iLowerVertex] = TRUE;
			SeidelSplitTrapezoidHorizontally(SeidelGetTrapezoidForVertex(lpnodeRoot, lpmap, iLowerVertex), splithoriz, hSeidelHeap, &lptrapezoidList, &lpnodeUpperSplit, &lpnodeLowerSplit);

			/* The trapezoid where the linedef starts. */
			lpnodeLowerTrapezoid = lpnodeUpperSplit;

			/* If the line lay within a single trapezoid (before we started
			 * splitting stuff), we now have to adjust for the fact that we've
			 * just split the trapezoid we found last time.
			 */
			if(lpnodeUpperTrapezoid->nodetype != SNT_TRAPEZOID)
				lpnodeUpperTrapezoid = lpnodeLowerTrapezoid;
		}
		else
			lpnodeLowerTrapezoid = SeidelGetTrapezoidForVertexDirected(lpnodeRoot, lpmap, iLowerVertex, iUpperVertex);

		/* Thread the linedef through the trapezoids. */
		lptrapezoidCurrent = lpnodeUpperTrapezoid->lptrapezoid;
		lptrapezoidPrevLeft = lptrapezoidPrevRight = NULL;
		do
		{
			SEIDEL_TRAPEZOID *lptrapezoidNext;
			BOOL bNextIsLeft;

			/* Determine whether this is the last iteration in the threading.
			 * We have to do the check now, because we might mess with these
			 * structures. The second condition is for robustness, and will
			 * never occur if there aren't crossing linedefs.
			 */
			bFinishThreading = (lptrapezoidCurrent == lpnodeLowerTrapezoid->lptrapezoid) ||
				(lptrapezoidCurrent->lptrapezoidBelowLeft && (lptrapezoidCurrent->splithorizBottom.y < lpnodeLowerTrapezoid->lptrapezoid->splithorizBottom.y || (lptrapezoidCurrent->splithorizBottom.y == lpnodeLowerTrapezoid->lptrapezoid->splithorizBottom.y && lptrapezoidCurrent->splithorizBottom.nFract <= lpnodeLowerTrapezoid->lptrapezoid->splithorizBottom.nFract)));

			/* Resolve the trapezoids below. */
			SeidelDeGhostifyBelow(lpmap, lptrapezoidCurrent);

			/* Find the trapezoid below us that the linedef intersects. */
			if(lptrapezoidCurrent == lpnodeLowerTrapezoid->lptrapezoid)
			{
				/* Last one, so nothing follows. */
				lptrapezoidNext = NULL;
			}
			else if(!lptrapezoidCurrent->lptrapezoidBelowRight)
			{
				/* Just one below, so easy. */
				lptrapezoidNext = lptrapezoidCurrent->lptrapezoidBelowLeft;
			}
			else
			{
				/* Two below, so work out which one. The linedef between the two
				 * below must have a vertex on the horizontal split at the
				 * bottom of the current trapezoid. Determine the side of our
				 * linedef on which this vertex lies.
				 */

				MAPVERTEX *lpvxBelow = &lpmap->vertices[lpmap->linedefs[lptrapezoidCurrent->lptrapezoidBelowLeft->iLinedefRight].v1];
				int iSide, iLeftSide;

				/* If it's not v1, it must be v2. */
				if(lpvxBelow->y != lptrapezoidCurrent->splithorizBottom.y)
					lpvxBelow = &lpmap->vertices[lpmap->linedefs[lptrapezoidCurrent->lptrapezoidBelowLeft->iLinedefRight].v2];

				iSide = SideOfLinedefVertex(lpmap, lpiShuffledLinedefs[iLinedefIndex], lpvxBelow);
				iLeftSide = LeftSideOfLinedef(lpmap, lpiShuffledLinedefs[iLinedefIndex]);

				lptrapezoidNext = (iSide == iLeftSide) ? lptrapezoidCurrent->lptrapezoidBelowRight : lptrapezoidCurrent->lptrapezoidBelowLeft;
			}

			bNextIsLeft = (lptrapezoidNext == lptrapezoidCurrent->lptrapezoidBelowLeft);

			/* Turn the old trapezoid node into a linedef-split node. We'll fill
			 * in the details later.
			 */
			lptrapezoidCurrent->lpnode->nodetype = SNT_SPLITLINEDEF;
			lptrapezoidCurrent->lpnode->iLinedef = lpiShuffledLinedefs[iLinedefIndex];
			lptrapezoidCurrent->lpnode->lpnodeLA = HeapAlloc(hSeidelHeap, 0, sizeof(SEIDEL_NODE));
			lptrapezoidCurrent->lpnode->lpnodeLA->nodetype = SNT_TRAPEZOID;
			lptrapezoidCurrent->lpnode->lpnodeRB = HeapAlloc(hSeidelHeap, 0, sizeof(SEIDEL_NODE));
			lptrapezoidCurrent->lpnode->lpnodeRB->nodetype = SNT_TRAPEZOID;

			/* Merge if necessary. Do lefts agree? */
			if(lptrapezoidPrevLeft &&
				lptrapezoidCurrent->iLinedefLeft == lptrapezoidPrevLeft->iLinedefLeft)
			{
				/* Shrink to right and stretch above-left. */
				lptrapezoidCurrent->iLinedefLeft = lpiShuffledLinedefs[iLinedefIndex];
				lptrapezoidPrevLeft->splithorizBottom = lptrapezoidCurrent->splithorizBottom;

				/* Update the tree. */
				lptrapezoidCurrent->lpnode->lpnodeLA = lptrapezoidPrevLeft->lpnode;
				lptrapezoidCurrent->lpnode->lpnodeRB->lptrapezoid = lptrapezoidCurrent;
				lptrapezoidCurrent->lpnode = lptrapezoidCurrent->lpnode->lpnodeRB;

				/* Correct adjacency references. */
				lptrapezoidPrevLeft->lptrapezoidBelowLeft = lptrapezoidCurrent->lptrapezoidBelowLeft;
				lptrapezoidPrevLeft->lptrapezoidBelowRight = bNextIsLeft ? NULL : lptrapezoidNext;
				if(lptrapezoidNext)
				{
					/* Everything else stays the same! */
					if(!bNextIsLeft)
						lptrapezoidCurrent->lptrapezoidBelowRight = NULL;
				}
				else
				{
					if(lptrapezoidCurrent->lptrapezoidBelowRight)
					{
						lptrapezoidCurrent->lptrapezoidBelowLeft = lptrapezoidCurrent->lptrapezoidBelowRight;
						lptrapezoidCurrent->lptrapezoidBelowRight = NULL;
					}
				}

				/* Update pointers to the previous trapezoids for next time
				 * around. lptrapezoidPrevLeft remains unchanged.
				 */
				lptrapezoidPrevRight = lptrapezoidCurrent;
			}
			/* Lefts don't agree, but maybe the rights do? */
			else if(lptrapezoidPrevRight &&
				lptrapezoidCurrent->iLinedefRight == lptrapezoidPrevRight->iLinedefRight)
			{
				/* Shrink to left and stretch above-right. */
				lptrapezoidCurrent->iLinedefRight = lpiShuffledLinedefs[iLinedefIndex];
				lptrapezoidPrevRight->splithorizBottom = lptrapezoidCurrent->splithorizBottom;

				/* Update the tree. */
				lptrapezoidCurrent->lpnode->lpnodeRB = lptrapezoidPrevRight->lpnode;
				lptrapezoidCurrent->lpnode->lpnodeLA->lptrapezoid = lptrapezoidCurrent;
				lptrapezoidCurrent->lpnode = lptrapezoidCurrent->lpnode->lpnodeLA;

				/* Correct adjacency references. */
				if(lptrapezoidNext)
				{
					lptrapezoidPrevRight->lptrapezoidBelowLeft = bNextIsLeft ? lptrapezoidNext : lptrapezoidCurrent->lptrapezoidBelowRight;
					lptrapezoidPrevRight->lptrapezoidBelowRight = bNextIsLeft ? lptrapezoidCurrent->lptrapezoidBelowRight : NULL;
					if(bNextIsLeft)
						lptrapezoidCurrent->lptrapezoidBelowRight = NULL;
				}
				else
				{
					lptrapezoidPrevRight->lptrapezoidBelowLeft = lptrapezoidCurrent->lptrapezoidBelowRight ? lptrapezoidCurrent->lptrapezoidBelowRight : lptrapezoidCurrent->lptrapezoidBelowLeft;
					lptrapezoidPrevRight->lptrapezoidBelowRight = NULL;
					lptrapezoidCurrent->lptrapezoidBelowRight = NULL;
				}

				/* Update pointers to the previous trapezoids for next time
				 * around. lptrapezoidPrevRight remains unchanged.
				 */
				lptrapezoidPrevLeft = lptrapezoidCurrent;
			}
			/* No agreement, so no merging; create new trapezoids instead. */
			else
			{
				/* Create two new trapezoids. */
				SEIDEL_TRAPEZOID *lptrapezoidNewLeft = HeapAlloc(hSeidelHeap, 0, sizeof(SEIDEL_TRAPEZOID));
				SEIDEL_TRAPEZOID *lptrapezoidNewRight = HeapAlloc(hSeidelHeap, 0, sizeof(SEIDEL_TRAPEZOID));

				/* Add to list. */
				lptrapezoidNewLeft->lptrapezoidNext = lptrapezoidList;
				lptrapezoidList = lptrapezoidNewLeft;
				lptrapezoidNewRight->lptrapezoidNext = lptrapezoidList;
				lptrapezoidList = lptrapezoidNewRight;

				lptrapezoidNewLeft->lpnode = lptrapezoidCurrent->lpnode->lpnodeLA;
				lptrapezoidNewLeft->lpnode->lptrapezoid = lptrapezoidNewLeft;
				lptrapezoidNewRight->lpnode = lptrapezoidCurrent->lpnode->lpnodeRB;
				lptrapezoidNewRight->lpnode->lptrapezoid = lptrapezoidNewRight;

				/* Ghostify the old trapezoid. */
				lptrapezoidCurrent->byFlags |= SNTF_GHOST;
				lptrapezoidCurrent->ghost.iLinedefSplit = lpiShuffledLinedefs[iLinedefIndex];
				lptrapezoidCurrent->ghost.lptrapezoidLeft = lptrapezoidNewLeft;
				lptrapezoidCurrent->ghost.lptrapezoidRight = lptrapezoidNewRight;

				/* Set trapezoids' properties. */

				lptrapezoidNewLeft->byFlags = lptrapezoidNewRight->byFlags = 0;
				lptrapezoidNewLeft->iSubsectorID = lptrapezoidNewRight->iSubsectorID = -1;
				lptrapezoidNewLeft->bUnboundedAbove = lptrapezoidNewRight->bUnboundedAbove = lptrapezoidCurrent->bUnboundedAbove;

				lptrapezoidNewLeft->splithorizTop = lptrapezoidNewRight->splithorizTop = lptrapezoidCurrent->splithorizTop;
				lptrapezoidNewLeft->splithorizBottom = lptrapezoidNewRight->splithorizBottom = lptrapezoidCurrent->splithorizBottom;

				lptrapezoidNewLeft->iLinedefLeft = lptrapezoidCurrent->iLinedefLeft;
				lptrapezoidNewRight->iLinedefRight = lptrapezoidCurrent->iLinedefRight;
				lptrapezoidNewLeft->iLinedefRight = lptrapezoidNewRight->iLinedefLeft = lpiShuffledLinedefs[iLinedefIndex];

				lptrapezoidNewLeft->lptrapezoidBelowLeft = lptrapezoidCurrent->lptrapezoidBelowLeft;
				lptrapezoidNewLeft->lptrapezoidBelowRight = bNextIsLeft ? NULL : lptrapezoidNext;

				if(lptrapezoidNext)
				{
					lptrapezoidNewRight->lptrapezoidBelowLeft = bNextIsLeft ? lptrapezoidNext : lptrapezoidCurrent->lptrapezoidBelowRight;
					lptrapezoidNewRight->lptrapezoidBelowRight = bNextIsLeft ? lptrapezoidCurrent->lptrapezoidBelowRight : NULL;
				}
				else
				{
					lptrapezoidNewRight->lptrapezoidBelowLeft = lptrapezoidCurrent->lptrapezoidBelowRight ? lptrapezoidCurrent->lptrapezoidBelowRight : lptrapezoidCurrent->lptrapezoidBelowLeft;
					lptrapezoidNewRight->lptrapezoidBelowRight = NULL;
				}

				/* Update pointers to the previous trapezoids for next time
				 * around.
				 */
				lptrapezoidPrevLeft = lptrapezoidNewLeft;
				lptrapezoidPrevRight = lptrapezoidNewRight;
			}

			lptrapezoidCurrent = lptrapezoidNext;
		} while(!bFinishThreading && lptrapezoidCurrent);
	}

	/* Make BSP tree. */
	lpbsptree->iSubsectors = 0;
	SeidelMakeBSPBranch(lpmap, lpnodeRoot, &lpbsptree->lpbspnodeRoot, 32767, -32768, &lpbsptree->iSubsectors, lpbsptree->hHeap);

#ifdef REPORT_BSP_STATS
	{
		TCHAR sz[1000];
		wsprintf(sz, TEXT("SS: %d  MaxDepth: %d"), lpbsptree->iSubsectors, BSPMaxDepth(lpbsptree->lpbspnodeRoot));
		MessageBox(g_hwndMain, sz, g_szAppName, 0);
	}
#endif

	/* Add horizontal linedefs to BSP tree. */
	if(lpbsptree->lpbspnodeRoot)
	{
		for(iLinedefIndex = 0; iLinedefIndex < lpmap->iLinedefs; iLinedefIndex++)
		{
			MAPLINEDEF *lpld = &lpmap->linedefs[iLinedefIndex];
			MAPVERTEX *lpvx1 = &lpmap->vertices[lpld->v1];
			MAPVERTEX *lpvx2 = &lpmap->vertices[lpld->v2];

			if(lpvx1->y == lpvx2->y)
				AddHorizLDToBSPTree(lpmap, iLinedefIndex, lpbsptree);
		}
	}

	/* Okay, we've split up the map. Time to co-ordinatise the trapezoids. */
	*lplplpmapquadSectorQuads = ProcHeapAlloc(lpmap->iSectors * sizeof(MAPQUAD*));
	ZeroMemory(*lplplpmapquadSectorQuads, lpmap->iSectors * sizeof(MAPQUAD*));
	for(; lptrapezoidList; lptrapezoidList = lptrapezoidList->lptrapezoidNext)
	{
		MAPLINEDEF *lplinedefLeft;
		int iSidedef;

		/* Ignore ghosts. */
		if(lptrapezoidList->byFlags & SNTF_GHOST)
			continue;

		/* Ignore infinite trapezoids. */
		if(lptrapezoidList->iLinedefLeft < 0 || lptrapezoidList->iLinedefRight < 0 || lptrapezoidList->bUnboundedAbove)
			continue;

		/* Ignore infnitessimal trapezoids. */
		if(lptrapezoidList->splithorizBottom.y == lptrapezoidList->splithorizTop.y)
			continue;

		lplinedefLeft = &lpmap->linedefs[lptrapezoidList->iLinedefLeft];
		iSidedef = LeftSideOfLinedef(lpmap, lptrapezoidList->iLinedefLeft) == LS_FRONT ? lplinedefLeft->s2 : lplinedefLeft->s1;

		/* Ignore trapezoids not part of a sector. */
		if(SidedefExists(lpmap, iSidedef))
		{
			MAPLINEDEF *lplinedefRight = &lpmap->linedefs[lptrapezoidList->iLinedefRight];
			MAPSIDEDEF *lpsidedef = &lpmap->sidedefs[iSidedef];
			MAPQUAD *lpmapquad;
			float fInverseGradient;
			float fXIntercept;

			/* Sanity check. */
			if(lpsidedef->sector < 0 || lpsidedef->sector >= lpmap->iSectors)
				continue;

			/* Create quad and add it to the list. */
			lpmapquad = ProcHeapAlloc(sizeof(MAPQUAD));
			lpmapquad->lpmapquadNext = (*lplplpmapquadSectorQuads)[lpsidedef->sector];
			(*lplplpmapquadSectorQuads)[lpsidedef->sector] = lpmapquad;

			/* Propagate the ID assigned at BSP time. */
			lpmapquad->iSubsectorID = lptrapezoidList->iSubsectorID;

			/* Set the vertices so that the front faces upwards. */

			/* Left line first. */
			fInverseGradient = (float)(lpmap->vertices[lplinedefLeft->v2].x - lpmap->vertices[lplinedefLeft->v1].x) / (lpmap->vertices[lplinedefLeft->v2].y - lpmap->vertices[lplinedefLeft->v1].y);
			fXIntercept = lpmap->vertices[lplinedefLeft->v1].x - lpmap->vertices[lplinedefLeft->v1].y * fInverseGradient;

			lpmapquad->v1.y = lptrapezoidList->splithorizTop.y;
			lpmapquad->v1.x = fXIntercept + lpmapquad->v1.y * fInverseGradient;
			lpmapquad->v2.y = lptrapezoidList->splithorizBottom.y;
			lpmapquad->v2.x = fXIntercept + lpmapquad->v2.y * fInverseGradient;

			/* Right line. */
			fInverseGradient = (float)(lpmap->vertices[lplinedefRight->v2].x - lpmap->vertices[lplinedefRight->v1].x) / (lpmap->vertices[lplinedefRight->v2].y - lpmap->vertices[lplinedefRight->v1].y);
			fXIntercept = lpmap->vertices[lplinedefRight->v1].x - lpmap->vertices[lplinedefRight->v1].y * fInverseGradient;

			lpmapquad->v3.y = lptrapezoidList->splithorizBottom.y;
			lpmapquad->v3.x = fXIntercept + lpmapquad->v3.y * fInverseGradient;
			lpmapquad->v4.y = lptrapezoidList->splithorizTop.y;
			lpmapquad->v4.x = fXIntercept + lpmapquad->v4.y * fInverseGradient;
		}
	}
	

	ProcHeapFree(lpiShuffledLinedefs);
	ProcHeapFree(lpbyUsedVertices);

	HeapDestroy(hSeidelHeap);
}


/* SeidelGetTrapezoidForVertex/SeidelGetTrapezoidForVertexDirected
 *   Finds the node representing the trapezoid in which the specified vertex
 *   lies.
 *
 * Parameters:
 *   SEIDEL_NODE*	lpnodeRoot			Root of point-query tree.
 *   MAP*			lpmap				Map.
 *   int			iVertex				Vertex to locate.
 *   int			iDirectionVertex	Vertex towards which to err when
 *										resolving ambiguities caused by
 *										vertices lying on a split. Negative (or
 *										call SeidelGetTrapezoidForVertex) for
 *										none.
 *
 * Return value: SEIDEL_NODE*
 *   Trapezoid node.
 */
static __inline SEIDEL_NODE* SeidelGetTrapezoidForVertex(SEIDEL_NODE *lpnodeRoot, MAP *lpmap, int iVertex)
{
	return SeidelGetTrapezoidForVertexDirected(lpnodeRoot, lpmap, iVertex, -1);
}

static SEIDEL_NODE* SeidelGetTrapezoidForVertexDirected(SEIDEL_NODE *lpnodeRoot, MAP *lpmap, int iVertex, int iDirectionVertex)
{
	MAPVERTEX *lpvx = &lpmap->vertices[iVertex];
	MAPVERTEX *lpvxDirection = iDirectionVertex >= 0 ? &lpmap->vertices[iDirectionVertex] : NULL;

	while(lpnodeRoot->nodetype != SNT_TRAPEZOID)
	{
		if(lpnodeRoot->nodetype == SNT_SPLITHORIZ)
		{
			/* "If the vertex is below the split; or if the vertex is on the split and the direction vertex is below the split." */
			if(lpvx->y < lpnodeRoot->splithoriz.y || (lpvx->y == lpnodeRoot->splithoriz.y && (lpvx->x < lpnodeRoot->splithoriz.nFract || (lpvx->x == lpnodeRoot->splithoriz.nFract && lpvxDirection && (lpvxDirection->y < lpnodeRoot->splithoriz.y || (lpvxDirection->y == lpnodeRoot->splithoriz.y && lpvxDirection->x < lpnodeRoot->splithoriz.nFract))))))
				lpnodeRoot = lpnodeRoot->lpnodeRB;
			else
				lpnodeRoot = lpnodeRoot->lpnodeLA;
		}
		else
		{
			/* Linedef split. */

			/* We check on which side of the line the vertex lies, unless it's
			 * actually *on* the linedef, in which case we check the direction
			 * vertex instead.
			 */
			MAPLINEDEF *lpldSplit = &lpmap->linedefs[lpnodeRoot->iLinedef];
			MAPVERTEX *lpvxCheck = (lpvxDirection && (lpldSplit->v1 == iVertex || lpldSplit->v2 == iVertex)) ? lpvxDirection : lpvx;

			if(SideOfLinedefVertex(lpmap, lpnodeRoot->iLinedef, lpvxCheck) == LeftSideOfLinedef(lpmap, lpnodeRoot->iLinedef))
				lpnodeRoot = lpnodeRoot->lpnodeLA;
			else
				lpnodeRoot = lpnodeRoot->lpnodeRB;
		}
	}

	return lpnodeRoot;
}



/* IsTrapezoidDegenerateTop/IsTrapezoidDegenerateBottom
 *   Determines whether a trapezoid has a zero-length top or bottom edge.
 *
 * Parameters:
 *   MAP*				lpmap			Map.
 *   SEIDEL_TRAPEZOID*	lptrapezoid		Trapezoid.
 *
 * Return value: BOOL
 *   TRUE iff degenerate.
 */
static BOOL IsTrapezoidDegenerateTop(MAP *lpmap, SEIDEL_TRAPEZOID *lptrapezoid)
{
	/* Since we're assuming lines don't cross, degeneracy occurs only at
	 * vertices.
	 */
	MAPLINEDEF *lpldLeft = &lpmap->linedefs[lptrapezoid->iLinedefLeft];
	MAPLINEDEF *lpldRight = &lpmap->linedefs[lptrapezoid->iLinedefRight];
	MAPVERTEX *lpvxLeft1 = &lpmap->vertices[lpldLeft->v1];
	MAPVERTEX *lpvxLeft2 = &lpmap->vertices[lpldLeft->v2];
	MAPVERTEX *lpvxRight1 = &lpmap->vertices[lpldRight->v1];
	MAPVERTEX *lpvxRight2 = &lpmap->vertices[lpldRight->v2];

	if((lpvxLeft1->y != lptrapezoid->splithorizTop.y || lpvxLeft1->x != lptrapezoid->splithorizTop.nFract) && (lpvxLeft2->y != lptrapezoid->splithorizTop.y || lpvxLeft2->x != lptrapezoid->splithorizTop.nFract))
		return FALSE;

	return (lpvxRight1->y == lptrapezoid->splithorizTop.y && lpvxRight1->x == lptrapezoid->splithorizTop.nFract) || (lpvxRight2->y == lptrapezoid->splithorizTop.y && lpvxRight2->x == lptrapezoid->splithorizTop.nFract);
}

#if 0
static BOOL IsTrapezoidDegenerateBottom(MAP *lpmap, SEIDEL_TRAPEZOID *lptrapezoid)
{
	/* Since we're assuming lines don't cross, degeneracy occurs only at
	 * vertices.
	 */
	MAPLINEDEF *lpldLeft = &lpmap->linedefs[lptrapezoid->iLinedefLeft];
	MAPLINEDEF *lpldRight = &lpmap->linedefs[lptrapezoid->iLinedefRight];
	MAPVERTEX *lpvxLeft1 = &lpmap->vertices[lpldLeft->v1];
	MAPVERTEX *lpvxLeft2 = &lpmap->vertices[lpldLeft->v2];
	MAPVERTEX *lpvxRight1 = &lpmap->vertices[lpldRight->v1];
	MAPVERTEX *lpvxRight2 = &lpmap->vertices[lpldRight->v2];

	if((lpvxLeft1->y != lptrapezoid->splithorizBottom.y || lpvxLeft1->x != lptrapezoid->splithorizBottom.nFract) && (lpvxLeft2->y != lptrapezoid->splithorizBottom.y || lpvxLeft2->x != lptrapezoid->splithorizBottom.nFract))
		return FALSE;

	return (lpvxRight1->y == lptrapezoid->splithorizBottom.y && lpvxRight1->x == lptrapezoid->splithorizBottom.nFract) || (lpvxRight2->y == lptrapezoid->splithorizBottom.y && lpvxRight2->x == lptrapezoid->splithorizBottom.nFract);
}
#endif


/* SeidelSplitTrapezoidHorizontally
 *   Splits a trapezoid in two along a horizontal line.
 *
 * Parameters:
 *   SEIDEL_NODE*		lpnodeOldTrapezpoid		Node of trapezoid to slpit.
 *   SEIDEL_SPLITHORIZ	splithoriz				Co-ordinates of split.
 *   HANDLE				hSeidelHeap				Heap from which to allocate new
 *												trapezoid.
 *   SEIDEL_TRAPEZOID**	lplptrapezoidList		List of trapezoids to be kept
 *												up-to-date.
 *   SEIDEL_NODE**		lplpnodeUpperSplit		Returns node of upper trapezoid.
 *   SEIDEL_NODE**		lplpnodeLowerSplit		Returns node of lower trapezoid.
 *
 * Return value: None.
 */
static void SeidelSplitTrapezoidHorizontally(SEIDEL_NODE *lpnodeOldTrapezoid, SEIDEL_SPLITHORIZ splithoriz, HANDLE hSeidelHeap, SEIDEL_TRAPEZOID **lplptrapezoidList, SEIDEL_NODE **lplpnodeUpperSplit, SEIDEL_NODE **lplpnodeLowerSplit)
{
	SEIDEL_TRAPEZOID *lptrapezoidNew = HeapAlloc(hSeidelHeap, 0, sizeof(SEIDEL_TRAPEZOID));

	/* Add the new trapezoid to the list. */
	lptrapezoidNew->lptrapezoidNext = *lplptrapezoidList;
	*lplptrapezoidList = lptrapezoidNew;

	/* Set the boundaries resulting from the split. */
	lptrapezoidNew->splithorizBottom = lpnodeOldTrapezoid->lptrapezoid->splithorizBottom;
	lptrapezoidNew->splithorizTop = splithoriz;
	lpnodeOldTrapezoid->lptrapezoid->splithorizBottom = splithoriz;
	lptrapezoidNew->byFlags = 0;
	lptrapezoidNew->bUnboundedAbove = FALSE;
	lptrapezoidNew->iSubsectorID = -1;
	lptrapezoidNew->iLinedefLeft = lpnodeOldTrapezoid->lptrapezoid->iLinedefLeft;
	lptrapezoidNew->iLinedefRight = lpnodeOldTrapezoid->lptrapezoid->iLinedefRight;

	/* Internal references now. */
	lptrapezoidNew->lptrapezoidBelowLeft = lpnodeOldTrapezoid->lptrapezoid->lptrapezoidBelowLeft;
	lptrapezoidNew->lptrapezoidBelowRight = lpnodeOldTrapezoid->lptrapezoid->lptrapezoidBelowRight;
	lpnodeOldTrapezoid->lptrapezoid->lptrapezoidBelowLeft = lptrapezoidNew;
	lpnodeOldTrapezoid->lptrapezoid->lptrapezoidBelowRight = NULL;

	/* Stick the trapezoids in the tree. */
	*lplpnodeUpperSplit = HeapAlloc(hSeidelHeap, 0, sizeof(SEIDEL_NODE));
	*lplpnodeLowerSplit = HeapAlloc(hSeidelHeap, 0, sizeof(SEIDEL_NODE));

	/* The node for the upper trapezoid just looks like the node for the
	 * original.
	 */
	CopyMemory(*lplpnodeUpperSplit, lpnodeOldTrapezoid, sizeof(SEIDEL_NODE));
	(*lplpnodeUpperSplit)->lptrapezoid->lpnode = *lplpnodeUpperSplit;

	/* New node for lower trapezoid. */
	(*lplpnodeLowerSplit)->nodetype = SNT_TRAPEZOID;
	(*lplpnodeLowerSplit)->lptrapezoid = lptrapezoidNew;
	lptrapezoidNew->lpnode = *lplpnodeLowerSplit;

	/* The original trapezoid becomes a horizontal split. */
	lpnodeOldTrapezoid->nodetype = SNT_SPLITHORIZ;
	lpnodeOldTrapezoid->splithoriz = splithoriz;
	lpnodeOldTrapezoid->lpnodeLA = *lplpnodeUpperSplit;
	lpnodeOldTrapezoid->lpnodeRB = *lplpnodeLowerSplit;
}


/* SeidelDeGhostifyBelow
 *   Resolves ghost references in nodes below a trapezoid to actual trapezoids.
 *
 * Parameters:
 *   MAP*				lpmap			Map.
 *   SEIDEL_TRAPEZOID*	lptrapezoid		Trapezoid whose below-nodes are to be
 *										resolved.
 *
 * Return value: None.
 */
static void SeidelDeGhostifyBelow(MAP *lpmap, SEIDEL_TRAPEZOID *lptrapezoid)
{
	while((lptrapezoid->lptrapezoidBelowLeft && (lptrapezoid->lptrapezoidBelowLeft->byFlags & SNTF_GHOST)) ||
		(lptrapezoid->lptrapezoidBelowRight && (lptrapezoid->lptrapezoidBelowRight->byFlags & SNTF_GHOST)))
	{
		if(lptrapezoid->lptrapezoidBelowRight && (lptrapezoid->lptrapezoidBelowRight->byFlags & SNTF_GHOST))
		{
			/* Simple case 1: right is a ghost, so it must be the left of the
			 * split (unless it's degenerate).
			 */
			lptrapezoid->lptrapezoidBelowRight = IsTrapezoidDegenerateTop(lpmap, lptrapezoid->lptrapezoidBelowRight->ghost.lptrapezoidLeft) ?
				lptrapezoid->lptrapezoidBelowRight->ghost.lptrapezoidRight :
				lptrapezoid->lptrapezoidBelowRight->ghost.lptrapezoidLeft;
		}
		else if(lptrapezoid->lptrapezoidBelowRight)
		{
			/* Simple case 2: left is a ghost and we already have a right
			 * trapezoid, so the left must be the right of the split (unless
			 * it's degenerate).
			 */
			lptrapezoid->lptrapezoidBelowLeft = IsTrapezoidDegenerateTop(lpmap, lptrapezoid->lptrapezoidBelowLeft->ghost.lptrapezoidRight) ?
				lptrapezoid->lptrapezoidBelowLeft->ghost.lptrapezoidLeft :
				lptrapezoid->lptrapezoidBelowLeft->ghost.lptrapezoidRight;
		}
		else
		{
			/* Less simple case: left is a ghost and we don't have a right, so
			 * the split can result in three possibilities.
			 */

			float fSplitIntercept, fLeftIntercept = 0.0f, fRightIntercept = 0.0f;

			/* Find the points of intersection between the various linedefs and
			 * the horizontal boundary.
			 */
			fSplitIntercept = LinedefXAtY(lpmap, lptrapezoid->lptrapezoidBelowLeft->ghost.iLinedefSplit, (float)lptrapezoid->splithorizBottom.y);
			if(lptrapezoid->iLinedefLeft >= 0) fLeftIntercept = LinedefXAtY(lpmap, lptrapezoid->iLinedefLeft, (float)lptrapezoid->splithorizBottom.y);
			if(lptrapezoid->iLinedefRight >= 0) fRightIntercept = LinedefXAtY(lpmap, lptrapezoid->iLinedefRight, (float)lptrapezoid->splithorizBottom.y);

			/* Case 1: split to the right of both lines. */
			if(lptrapezoid->iLinedefRight >= 0 && fSplitIntercept > fRightIntercept)
				lptrapezoid->lptrapezoidBelowLeft = lptrapezoid->lptrapezoidBelowLeft->ghost.lptrapezoidLeft;
			/* Case 2: split to the left of both lines. */
			else if(lptrapezoid->iLinedefLeft >= 0 && fSplitIntercept < fLeftIntercept)
				lptrapezoid->lptrapezoidBelowLeft = lptrapezoid->lptrapezoidBelowLeft->ghost.lptrapezoidRight;
			/* Case 3: split is in between the lines. */
			else
			{
				/* Watch the order! Don't clobber lptrapezoidBelowLeft too soon. */
				lptrapezoid->lptrapezoidBelowRight = lptrapezoid->lptrapezoidBelowLeft->ghost.lptrapezoidRight;
				lptrapezoid->lptrapezoidBelowLeft = lptrapezoid->lptrapezoidBelowLeft->ghost.lptrapezoidLeft;
			}
		}
	}
}


static void SeidelMakeBSPBranch(MAP *lpmap, SEIDEL_NODE *lpseidelnode, BSP_NODE **lplpbspnode, short yTop, short yBottom, int *lpiNextSSID, HANDLE hHeap)
{
	/* Create and initialise the node. */
	*lplpbspnode = HeapAlloc(hHeap, HEAP_GENERATE_EXCEPTIONS, sizeof(BSP_NODE));
	(*lplpbspnode)->growarrayLineSurfIndices.lpvElements = NULL;
	(*lplpbspnode)->iSubsectorID = -1;

	/* Clip to range. This avoids infinitessimals and unnecessary splits from
	 * multiply-referenced trapezoids.
	 */
	while(lpseidelnode->nodetype == SNT_SPLITHORIZ &&
		(lpseidelnode->splithoriz.y >= yTop || lpseidelnode->splithoriz.y <= yBottom))
	{
		lpseidelnode = (lpseidelnode->splithoriz.y >= yTop) ?
			lpseidelnode->lpnodeRB :
			lpseidelnode->lpnodeLA;
	}

	/* Set the BSP node according to the type of Seidel node. */
	switch(lpseidelnode->nodetype)
	{
	case SNT_SPLITHORIZ:
		/* Set the plane for the split. */
		(*lplpbspnode)->hptPlaneNormal.x = 0;
		(*lplpbspnode)->hptPlaneNormal.y = -1;
		(*lplpbspnode)->hptPlaneNormal.z = 0;
		(*lplpbspnode)->hptPlaneNormal.w = lpseidelnode->splithoriz.y;

		/* Recur across the split. */
		SeidelMakeBSPBranch(
			lpmap,
			lpseidelnode->lpnodeLA,
			&(*lplpbspnode)->lpbspnodeBack,
			yTop,
			lpseidelnode->splithoriz.y,
			lpiNextSSID,
			hHeap);
		SeidelMakeBSPBranch(
			lpmap,
			lpseidelnode->lpnodeRB,
			&(*lplpbspnode)->lpbspnodeFront,
			lpseidelnode->splithoriz.y,
			yBottom,
			lpiNextSSID,
			hHeap);

		break;

	case SNT_SPLITLINEDEF:
		{
			MAPLINEDEF *lpld = &lpmap->linedefs[lpseidelnode->iLinedef];
			BOOL bFaceRight = (lpmap->vertices[lpld->v2].y > lpmap->vertices[lpld->v1].y);

			/* Set the plane for the split. */
			(*lplpbspnode)->hptPlaneNormal.x = (float)(lpmap->vertices[lpld->v2].y - lpmap->vertices[lpld->v1].y);
			(*lplpbspnode)->hptPlaneNormal.y = (float)(lpmap->vertices[lpld->v1].x - lpmap->vertices[lpld->v2].x);
			(*lplpbspnode)->hptPlaneNormal.z = 0;
			(*lplpbspnode)->hptPlaneNormal.w = -(*lplpbspnode)->hptPlaneNormal.x * lpmap->vertices[lpld->v1].x - (*lplpbspnode)->hptPlaneNormal.y * lpmap->vertices[lpld->v1].y;

			/* Recur across split. */
			SeidelMakeBSPBranch(
				lpmap,
				bFaceRight ? lpseidelnode->lpnodeLA : lpseidelnode->lpnodeRB,
				&(*lplpbspnode)->lpbspnodeBack,
				yTop,
				yBottom,
				lpiNextSSID,
				hHeap);
			SeidelMakeBSPBranch(
				lpmap,
				bFaceRight ? lpseidelnode->lpnodeRB : lpseidelnode->lpnodeLA,
				&(*lplpbspnode)->lpbspnodeFront,
				yTop,
				yBottom,
				lpiNextSSID,
				hHeap);
		}

		break;

	case SNT_TRAPEZOID:
		if(lpseidelnode->lptrapezoid->iLinedefLeft >= 0 &&
			lpseidelnode->lptrapezoid->iLinedefRight >= 0)
		{
			MAPLINEDEF *lpldLeft = &lpmap->linedefs[lpseidelnode->lptrapezoid->iLinedefLeft];
			MAPLINEDEF *lpldRight = &lpmap->linedefs[lpseidelnode->lptrapezoid->iLinedefRight];
			MAPVERTEX *lpvxLeft1 = &lpmap->vertices[lpldLeft->v1];
			MAPVERTEX *lpvxLeft2 = &lpmap->vertices[lpldLeft->v2];
			MAPVERTEX *lpvxRight1 = &lpmap->vertices[lpldRight->v1];
			MAPVERTEX *lpvxRight2 = &lpmap->vertices[lpldRight->v2];

			BOOL bLeftFront = (lpvxLeft2->y > lpvxLeft1->y);
			BOOL bRightFront = (lpvxRight2->y < lpvxRight1->y);

			int iLSLeft = bLeftFront ? LS_FRONT : LS_BACK;
			int iLSRight = bRightFront ? LS_FRONT : LS_BACK;

			/* Add the necessary surfaces for this trapezoid to the array. */
			InitGrowArray(&(*lplpbspnode)->growarrayLineSurfIndices, sizeof(int), 2, hHeap);
			AddToGrowArray(&(*lplpbspnode)->growarrayLineSurfIndices, 2);
			((int*)(*lplpbspnode)->growarrayLineSurfIndices.lpvElements)[0] = lpseidelnode->lptrapezoid->iLinedefLeft * 2 + iLSLeft;
			((int*)(*lplpbspnode)->growarrayLineSurfIndices.lpvElements)[1] = lpseidelnode->lptrapezoid->iLinedefRight * 2 + iLSRight;

			/* Assign an ID to the subsector described by this trapezoid. */
			if(lpseidelnode->lptrapezoid->iSubsectorID < 0)
				lpseidelnode->lptrapezoid->iSubsectorID = (*lpiNextSSID)++;

			(*lplpbspnode)->iSubsectorID = lpseidelnode->lptrapezoid->iSubsectorID;
		}

		/* We're a leaf. */
		(*lplpbspnode)->lpbspnodeBack = (*lplpbspnode)->lpbspnodeFront = NULL;

		break;
	}

	/* Collapse fully degenerate nodes. */
	if(!(*lplpbspnode)->growarrayLineSurfIndices.lpvElements &&
		!(*lplpbspnode)->lpbspnodeBack && !(*lplpbspnode)->lpbspnodeFront)
	{
		HeapFree(hHeap, HEAP_GENERATE_EXCEPTIONS | HEAP_NO_SERIALIZE, *lplpbspnode);
		*lplpbspnode = NULL;
	}
}


static __inline void AddHorizLDToBSPTree(MAP *lpmap, int iLinedef, BSP_TREE *lpbsptree)
{
	MAPLINEDEF *lpld = &lpmap->linedefs[iLinedef];
	AddHorizLDSegmentToBSPTree(
		lpmap,
		iLinedef,
		lpmap->vertices[lpld->v1].x,
		lpmap->vertices[lpld->v2].x,
		lpmap->vertices[lpld->v1].y,
		lpbsptree->lpbspnodeRoot);
}


static void AddHorizLDSegmentToBSPTree(MAP *lpmap, int iLinedef, float x1, float x2, short y, BSP_NODE *lpbspnode)
{
	MAPVERTEX *lpvx1 = &lpmap->vertices[lpmap->linedefs[iLinedef].v1];
	HOMOG_POINT hptOnLine = {lpvx1->x, lpvx1->y, 0, 1};

	if(x1 == x2)
		return;

	while(lpbspnode)
	{
		/* Does this node have children? */
		if(lpbspnode->lpbspnodeFront || lpbspnode->lpbspnodeBack)
		{
			float fSide1 = SideOfPlane2D(&lpbspnode->hptPlaneNormal, x1, y);
			float fSide2 = SideOfPlane2D(&lpbspnode->hptPlaneNormal, x2, y);

			if(fSide1 * fSide2 >= 0 && !(fSide1 == 0 && fSide2 == 0))
			{
				/* Same side. */
				lpbspnode = (fSide1 > 0 || fSide2 > 0) ? lpbspnode->lpbspnodeFront : lpbspnode->lpbspnodeBack;
			}
			else
			{
				/* Different sides, so we need to bifurcate, with appropriate
				 * adjustments to the segments.
				 */

				float x1New, x2New;
				
				if(lpbspnode->hptPlaneNormal.x)
				{
					x1New = x2New = AxialIntersectionWithPlane(&lpbspnode->hptPlaneNormal, &hptOnLine, HCOORD_X);
				}
				else
				{
					/* The linedef lies in the plane, so don't split. */
					x1New = x1;
					x2New = x2;
				}

				/* One side, by a recursive call... */
				AddHorizLDSegmentToBSPTree(lpmap, iLinedef, x1New, x2, y, (fSide2 > 0) ? lpbspnode->lpbspnodeFront : lpbspnode->lpbspnodeBack);

				/* ...and the other side, by looping around again. */
				lpbspnode = (fSide2 > 0) ? lpbspnode->lpbspnodeBack : lpbspnode->lpbspnodeFront;
				x2 = x2New;
			}
		}
		else
		{
			/* We've found the leaf. Stick the segment of the linedef in it. */

			if(lpbspnode->growarrayLineSurfIndices.lpvElements)
			{
				AddToGrowArray(&lpbspnode->growarrayLineSurfIndices, 2);
				((int*)lpbspnode->growarrayLineSurfIndices.lpvElements)[lpbspnode->growarrayLineSurfIndices.cElements - 2] = 2 * iLinedef;
				((int*)lpbspnode->growarrayLineSurfIndices.lpvElements)[lpbspnode->growarrayLineSurfIndices.cElements - 1] = 2 * iLinedef + 1;
			}

			return;
		}
	}
}


static __inline float SideOfPlane2D(HOMOG_POINT *lphptPlaneNormal, float x, float y)
{
	return lphptPlaneNormal->x * x + lphptPlaneNormal->y * y + lphptPlaneNormal->w;
}

static float AxialIntersectionWithPlane(HOMOG_POINT *lphptPlaneNormal, HOMOG_POINT *lphptOnLine, HOMOG_COORD hcoordLine)
{
	float fAccum = 0;
	float fDenom = 0;

	if(hcoordLine == HCOORD_X) fDenom = -lphptPlaneNormal->x;
	else fAccum += lphptPlaneNormal->x * lphptOnLine->x;

	if(hcoordLine == HCOORD_Y) fDenom = -lphptPlaneNormal->y;
	else fAccum += lphptPlaneNormal->y * lphptOnLine->y;

	if(hcoordLine == HCOORD_Z) fDenom = -lphptPlaneNormal->z;
	else fAccum += lphptPlaneNormal->z * lphptOnLine->z;

	fAccum += lphptPlaneNormal->w * lphptOnLine->w;

	if(!fDenom)
		return 0;

	return fAccum / fDenom;
}


void SetNeededSurfacesForLinedef(MAP *lpmap, int iLinedef, LPCTSTR szSky, int iSideOfLine, MAPSURFACES *lpmapsurfaces, HWND hwndMap)
{
	MAPLINEDEF *lpld = &lpmap->linedefs[iLinedef];
	MAPSECTOR *lpsecFront = NULL, *lpsecBack = NULL;
	MAPSIDEDEF *lpsdFront = NULL, *lpsdBack = NULL;
	MAPVERTEX *lpvx1 = &lpmap->vertices[lpld->v1];
	MAPVERTEX *lpvx2 = &lpmap->vertices[lpld->v2];
	LINESURFACES *lplinesurfaces = lpmapsurfaces->lplinesurfaces + 2 * iLinedef;

	/* This will tell us which parts of the wall need drawn. */
	BYTE bySurfaces = RequiredTextures(lpmap, iLinedef, szSky);

	if(iSideOfLine == LS_BACK)
		lplinesurfaces++;

	/* Get pointers to front and back sectors. */
	if(SidedefExists(lpmap, lpld->s1))
	{
		lpsdFront = &lpmap->sidedefs[lpld->s1];
		lpsecFront = &lpmap->sectors[lpsdFront->sector];
	}

	if(SidedefExists(lpmap, lpld->s2))
	{
		lpsdBack = &lpmap->sidedefs[lpld->s2];
		lpsecBack = &lpmap->sectors[lpsdBack->sector];
	}

	/* Render each component as needed, taking care with the order of
	 * vertices. We are slightly naughty in that we rely on the fact that
	 * GetTextureForMap will cache the textures, and so we hold on to the
	 * pointers. But this is just a hacked-together renderer, right?
	 */

	if(iSideOfLine == LS_FRONT)
	{
		if(bySurfaces & LDTF_FRONTLOWER)
			UpdateWall(&lplinesurfaces->s3dLower, hwndMap, lpsdFront->lower, lpvx1, lpvx2, lpsdFront, lpld, lpsecFront, lpsecFront->hfloor, lpsecBack->hfloor, ST_FRONTLOWER, iLinedef);
		else
			lplinesurfaces->s3dLower.dwFlags &= ~SF_EXISTS;

		if((lpsdFront && IsBonaFideTextureA(lpsdFront->middle)) || (bySurfaces & LDTF_FRONTMIDDLE))
		{
			if(lpsecBack && (lpld->flags & LDF_DOUBLESIDED))
				UpdateWall(&lplinesurfaces->s3dMiddle, hwndMap, lpsdFront->middle, lpvx1, lpvx2, lpsdFront, lpld, lpsecFront, max(lpsecFront->hfloor, lpsecBack->hfloor), min(lpsecFront->hceiling, lpsecBack->hceiling), ST_FRONTMIDDLE, iLinedef);
			else
				UpdateWall(&lplinesurfaces->s3dMiddle, hwndMap, lpsdFront->middle, lpvx1, lpvx2, lpsdFront, lpld, lpsecFront, lpsecFront->hfloor, lpsecFront->hceiling, ST_FRONTMIDDLE, iLinedef);
		}
		else
			lplinesurfaces->s3dMiddle.dwFlags &= ~SF_EXISTS;

		if(bySurfaces & LDTF_FRONTUPPER)
			UpdateWall(&lplinesurfaces->s3dUpper, hwndMap, lpsdFront->upper, lpvx1, lpvx2, lpsdFront, lpld, lpsecFront, lpsecBack->hceiling, lpsecFront->hceiling, ST_FRONTUPPER, iLinedef);
		else
			lplinesurfaces->s3dUpper.dwFlags &= ~SF_EXISTS;
	}
	else /* Back. */
	{
		if(bySurfaces & LDTF_BACKLOWER)
			UpdateWall(&lplinesurfaces->s3dLower, hwndMap, lpsdBack->lower, lpvx2, lpvx1, lpsdBack, lpld, lpsecBack, lpsecBack->hfloor, lpsecFront->hfloor, ST_BACKLOWER, iLinedef);
		else
			lplinesurfaces->s3dLower.dwFlags &= ~SF_EXISTS;

		if((lpsdBack && IsBonaFideTextureA(lpsdBack->middle)) || (bySurfaces & LDTF_BACKMIDDLE))
		{
			if(lpsecFront && (lpld->flags & LDF_DOUBLESIDED))
				UpdateWall(&lplinesurfaces->s3dMiddle, hwndMap, lpsdBack->middle, lpvx2, lpvx1, lpsdBack, lpld, lpsecBack, max(lpsecFront->hfloor, lpsecBack->hfloor), min(lpsecFront->hceiling, lpsecBack->hceiling), ST_BACKMIDDLE, iLinedef);
			else
				UpdateWall(&lplinesurfaces->s3dMiddle, hwndMap, lpsdBack->middle, lpvx2, lpvx1, lpsdBack, lpld, lpsecBack, lpsecBack->hfloor, lpsecBack->hceiling, ST_BACKMIDDLE, iLinedef);
		}
		else
			lplinesurfaces->s3dMiddle.dwFlags &= ~SF_EXISTS;

		if(bySurfaces & LDTF_BACKUPPER)
			UpdateWall(&lplinesurfaces->s3dUpper, hwndMap, lpsdBack->upper, lpvx2, lpvx1, lpsdBack, lpld, lpsecBack, lpsecFront->hceiling, lpsecBack->hceiling, ST_BACKUPPER, iLinedef);
		else
			lplinesurfaces->s3dUpper.dwFlags &= ~SF_EXISTS;
	}
}

static void CreateFOFWallsForLinedef(MAP *lpmap, MAPSURFACES *lpmapsurfaces, HWND hwndMap, int iLinedef)
{
	int iFOFTags[2];
	int cFOFTags, iFOFTagIndex;
	MAPLINEDEF *lpld = &lpmap->linedefs[iLinedef];
	MAPVERTEX *lpvx1 = &lpmap->vertices[lpld->v1];
	MAPVERTEX *lpvx2 = &lpmap->vertices[lpld->v2];
	MAPSECTOR *lpsecFront = NULL, *lpsecBack = NULL;
	MAPSIDEDEF *lpsdFront = NULL, *lpsdBack = NULL;

	/* Get pointers to front and back sectors. */
	if(SidedefExists(lpmap, lpld->s1))
	{
		lpsdFront = &lpmap->sidedefs[lpld->s1];
		lpsecFront = &lpmap->sectors[lpsdFront->sector];
	}

	if(SidedefExists(lpmap, lpld->s2))
	{
		lpsdBack = &lpmap->sidedefs[lpld->s2];
		lpsecBack = &lpmap->sectors[lpsdBack->sector];
	}

	cFOFTags = 0;
	if(lpsecFront)
		iFOFTags[cFOFTags++] = lpsecFront->tag;
	if(lpsecBack)
		iFOFTags[cFOFTags++] = lpsecBack->tag;

	for(iFOFTagIndex = 0; iFOFTagIndex < cFOFTags; iFOFTagIndex++)
	{
		FOF_LIST *lpfoflist = lpmapsurfaces->lpflFOFsByTag[iFOFTags[iFOFTagIndex]];
		
		if(lpfoflist)
		{
			/* Allocate memory for the planes. The structure itself has
			 * room for one.
			 */
			int cFOFs = CountFOFsInList(lpfoflist);
			FOF_SURFACE_ARRAY *lpfsa;
			FOF_LIST_NODE *lpfln;
			int iFOFInList;
			MAPVERTEX *lpvxFOFLeft = (iFOFTagIndex == 0) ? lpvx2 : lpvx1;
			MAPVERTEX *lpvxFOFRight = (iFOFTagIndex == 0) ? lpvx1 : lpvx2;

			if((lpfsa = lpmapsurfaces->lplpfsaFOFWallsByLinedef[iLinedef]))
			{
				/* There are some FOFs at this line already, so add some
				 * more.
				 */
				cFOFs += lpfsa->cSurfaces;
				iFOFInList = lpfsa->cSurfaces;
				lpfsa = lpmapsurfaces->lplpfsaFOFWallsByLinedef[iLinedef] =
					ProcHeapReAlloc(lpfsa, sizeof(FOF_SURFACE_ARRAY) + (cFOFs - 1) * sizeof(SURFACE3D));
				ZeroMemory(lpfsa->s3dPlanes + iFOFInList, (cFOFs - lpfsa->cSurfaces) * sizeof(SURFACE3D));
			}
			else
			{
				iFOFInList = 0;
				lpfsa = lpmapsurfaces->lplpfsaFOFWallsByLinedef[iLinedef] =
					ProcHeapAlloc(sizeof(FOF_SURFACE_ARRAY) + (cFOFs - 1) * sizeof(SURFACE3D));
				ZeroMemory(lpfsa->s3dPlanes, cFOFs * sizeof(SURFACE3D));
			}

			lpfsa->cSurfaces = cFOFs;

			/* Create walls for each FOF. */
			for(lpfln = lpfoflist->flnHeader.lpflnNext;
				lpfln->lpflnNext;
				lpfln = lpfln->lpflnNext, iFOFInList++)
			{
				if(lpfln->fof.iControlSector >= 0)
				{
					MAPLINEDEF *lpldControl = &lpmap->linedefs[lpfln->fof.iLinedef];
					MAPSECTOR *lpsecControl = &lpmap->sectors[lpfln->fof.iControlSector];
					MAPSIDEDEF *lpsdControl = &lpmap->sidedefs[lpldControl->s1];

					UpdateWall(&lpfsa->s3dPlanes[iFOFInList], hwndMap, lpsdControl->middle, lpvxFOFLeft, lpvxFOFRight, lpsdControl, lpldControl, lpsecControl, lpsecControl->hfloor, lpsecControl->hceiling, ST_FOFWALL, lpfln->fof.iLinedef);
				}
			}
		}
	}
}

static void IntersectPlane2D(HOMOG_POINT *lphptPlane1, HOMOG_POINT *lphptPlane2, float *lpx, float *lpy)
{
	HOMOG_POINT hptCopy;

	/* Swop so that plane 2 has non-zero x coeff. */
	if(lphptPlane2->x == 0)
	{
		HOMOG_POINT *lphptIntermediate = lphptPlane1;

		if(lphptPlane1->x == 0)
			return;

		lphptPlane1 = lphptPlane2;
		lphptPlane2 = lphptIntermediate;
	}

	hptCopy = *lphptPlane1;
	lphptPlane1 = &hptCopy;

	/* Echelonise. */
	if(lphptPlane1->x != 0)
	{
		lphptPlane1->y -= lphptPlane2->y * lphptPlane1->x / lphptPlane2->x;
		lphptPlane1->w -= lphptPlane2->w * lphptPlane1->x / lphptPlane2->x;
		lphptPlane1->x = 0;
	}

	if(lphptPlane1->y != 0)
	{
		*lpy = -lphptPlane1->w/lphptPlane1->y;
		if(lphptPlane2->x != 0)
			*lpx = (-lphptPlane2->w - lphptPlane2->y * *lpy) / lphptPlane2->x;
	}
}


static float ClipPlanes(CLIPPING_PLANE **lplpclipplaneIn, HOMOG_POINT *lphptNewPlane, CLIPPING_PLANE **lplpclipplaneReverse)
{
	CLIPPING_PLANE *lpclipplane = *lplpclipplaneIn;
	CLIPPING_PLANE *lpclipplaneFirst = lpclipplane;
	CLIPPING_PLANE *lpclipplaneSecondStretch;
	CLIPPING_PLANE *lpclipplaneNewFS, *lpclipplaneNewSS;
	CLIPPING_PLANE *lpclipplaneTargetFS, *lpclipplaneTargetSS;
	float fThisSide, fLastSide = 0;

	/* Indicate no reverse, until we find otherwise. */
	*lplpclipplaneReverse = NULL;

	/* Skip over vertices on side we first encounter. */
	do
	{
		fThisSide = SideOfPlane2D(lphptNewPlane, lpclipplane->x, lpclipplane->y);

		if(fThisSide * fLastSide < 0) break;

		/* Preserve last result. */
		if(fThisSide != 0)
			fLastSide = fThisSide;

		lpclipplane = lpclipplane->lpclipplaneNext;
	}
	while(lpclipplane != lpclipplaneFirst);

	lpclipplaneSecondStretch = lpclipplane;

	/* All on one side? */
	if(lpclipplane == lpclipplaneFirst)
	{
		/* Find a point on an unambiguous side of the plane. */
		do
		{
			fThisSide = SideOfPlane2D(lphptNewPlane, lpclipplane->x, lpclipplane->y);
			lpclipplane = lpclipplane->lpclipplaneNext;
		}
		while(fThisSide == 0 && lpclipplane != lpclipplaneFirst);

		return fThisSide;
	}

	/* Skip over vertices on other side. */
	do
	{
		lpclipplane = lpclipplane->lpclipplaneNext;

		/* Preserve last result. */
		if(fThisSide != 0)
			fLastSide = fThisSide;

		fThisSide = SideOfPlane2D(lphptNewPlane, lpclipplane->x, lpclipplane->y);
	}
	while(fThisSide * fLastSide > 0);
	/* This inequality is strict, since we must now be hitting the plane
	 * *having crossed it once already*.
	 */

	if(lpclipplane == lpclipplaneSecondStretch)
	{
		/* We got back to where we started, which can only happen if two points
		 * are on, or very nearly on, the plane -- in which case we'll just say
		 * we needn't split the region.
		 */

		return SideOfPlane2D(lphptNewPlane, lpclipplane->x, lpclipplane->y);
	}

	/* Make the new plane sets. */
	lpclipplaneNewFS = ProcHeapAlloc(sizeof(CLIPPING_PLANE));
	lpclipplaneNewFS->lphptPlane = lphptNewPlane;
	lpclipplaneNewSS = ProcHeapAlloc(sizeof(CLIPPING_PLANE));
	lpclipplaneNewSS->lphptPlane = lphptNewPlane;

	lpclipplaneTargetFS = ProcHeapAlloc(sizeof(CLIPPING_PLANE));
	*lpclipplaneTargetFS = *lpclipplane;
	lpclipplaneTargetSS = ProcHeapAlloc(sizeof(CLIPPING_PLANE));
	*lpclipplaneTargetSS = *lpclipplaneSecondStretch;

	/* First set. */
	IntersectPlane2D(lphptNewPlane, lpclipplaneSecondStretch->lphptPlane, &lpclipplaneSecondStretch->x, &lpclipplaneSecondStretch->y);
	lpclipplaneSecondStretch->lpclipplaneNext = lpclipplaneNewFS;
	IntersectPlane2D(lphptNewPlane, lpclipplaneTargetFS->lphptPlane, &lpclipplaneNewFS->x, &lpclipplaneNewFS->y);
	lpclipplaneNewFS->lpclipplaneNext = lpclipplaneTargetFS;

	/* Second set. */
	IntersectPlane2D(lphptNewPlane, lpclipplane->lphptPlane, &lpclipplane->x, &lpclipplane->y);
	lpclipplane->lpclipplaneNext = lpclipplaneNewSS;
	IntersectPlane2D(lphptNewPlane, lpclipplaneTargetSS->lphptPlane, &lpclipplaneNewSS->x, &lpclipplaneNewSS->y);
	lpclipplaneNewSS->lpclipplaneNext = lpclipplaneTargetSS;

	*lplpclipplaneIn = lpclipplaneNewFS;
	*lplpclipplaneReverse = lpclipplaneNewSS;

	lpclipplane = lpclipplaneTargetFS;
	do
	{
		fThisSide = SideOfPlane2D(lphptNewPlane, lpclipplane->x, lpclipplane->y);
		lpclipplane = lpclipplane->lpclipplaneNext;
	}
	while(fThisSide == 0 && lpclipplane != lpclipplaneTargetFS);

	return fThisSide;
}


static void FreeClippingPlane(CLIPPING_PLANE *lpclipplane)
{
	CLIPPING_PLANE *lpclipplaneFirst = lpclipplane;

	do
	{
		CLIPPING_PLANE *lpclipplaneNext = lpclipplane->lpclipplaneNext;
		ProcHeapFree(lpclipplane);
		lpclipplane = lpclipplaneNext;
	}
	while(lpclipplane != lpclipplaneFirst);
}


/* GetFrustumClipPlanes
 *   Retrieves the clipping planes in object co-ordinates as determined by the
 *   viewing frustum.
 *
 * Parameters:
 *   None.
 *
 * Return value: CLIPPING_PLANE*
 *   Clipping planes.
 */
static CLIPPING_PLANE* GetFrustumClipPlanes(MAPVIEW *lpmapview)
{
	GLdouble	fModelview[16];
	GLdouble	fMVInverse[16];

	FPOINT3D	fptVertices[8];
	int			iNearFar, iVertex;
	int			iHullIndices[8], cHullVertices, iNextVertex;

	CLIPPING_PLANE *lpclipplane = NULL, *lpclipplaneFirst = NULL;

	/* Get the current modelview matrix and invert it. */
	glGetDoublev(GL_MODELVIEW_MATRIX, fModelview);
	Invert4x4Matrix(fModelview, fMVInverse);

	/* Get world co-ordinates of the frustum's vertices projected on to the
	 * plane z = 0.
	 */
	for(iNearFar = 0; iNearFar < 2; iNearFar++)
	{
		for(iVertex = 0; iVertex < 4; iVertex++)
		{
			FPOINT3D *lpfpt = &lpmapview->fptFrustumClipVertices[iNearFar][iVertex];
			float w = (float)(fMVInverse[3] * lpfpt->x + fMVInverse[7] * lpfpt->y + fMVInverse[11] * lpfpt->z + fMVInverse[15]);
			fptVertices[iNearFar * 4 + iVertex].x = (float)(fMVInverse[0] * lpfpt->x + fMVInverse[4] * lpfpt->y + fMVInverse[8] * lpfpt->z + fMVInverse[12]) / w;
			fptVertices[iNearFar * 4 + iVertex].y = (float)(fMVInverse[1] * lpfpt->x + fMVInverse[5] * lpfpt->y + fMVInverse[9] * lpfpt->z + fMVInverse[13]) / w;
			fptVertices[iNearFar * 4 + iVertex].z = 0;
		}
	}

	/* Find the convex hull of the projected vertices using Jarvis's March. */

	/* Start at leftmost vertex. */
	iNextVertex = 0;
	for(iVertex = 1; iVertex < 8; iVertex++)
		if(fptVertices[iVertex].x < fptVertices[iNextVertex].x)
			iNextVertex = iVertex;

	/* Loop until we come all the way around. */
	cHullVertices = 0;
	do
	{
		int iVertexRover;

		/* We definitely want this point. */
		iHullIndices[cHullVertices] = iNextVertex;

		/* Find the next point. */
		iNextVertex = 0;
		for(iVertexRover = 1; iVertexRover < 8; iVertexRover++)
		{
			/* Is this point better? */
			if((fptVertices[iNextVertex].x == fptVertices[iHullIndices[cHullVertices]].x &&
				fptVertices[iNextVertex].y == fptVertices[iHullIndices[cHullVertices]].y) ||
				side_of_lineff(
					fptVertices[iHullIndices[cHullVertices]].x, fptVertices[iHullIndices[cHullVertices]].y,
					fptVertices[iNextVertex].x, fptVertices[iNextVertex].y,
					fptVertices[iVertexRover].x, fptVertices[iVertexRover].y) > 0)
			{
				iNextVertex = iVertexRover;
			}
		}

		cHullVertices++;
	} while(iNextVertex != iHullIndices[0]);


	/* Make the plane structures. */
	for(iVertex = 0; iVertex < cHullVertices; iVertex++)
	{
		int iNextVertex = (iVertex + 1) % cHullVertices;

		if(iVertex == 0)
			lpclipplaneFirst = lpclipplane = ProcHeapAlloc(sizeof(CLIPPING_PLANE));
		else
		{
			lpclipplane->lpclipplaneNext = ProcHeapAlloc(sizeof(CLIPPING_PLANE));
			lpclipplane = lpclipplane->lpclipplaneNext;
		}

		lpclipplane->x = fptVertices[iHullIndices[iNextVertex]].x;
		lpclipplane->y = fptVertices[iHullIndices[iNextVertex]].y;

		lpmapview->hptFrustumHullPlanes[iVertex].x = lpclipplane->y - fptVertices[iHullIndices[iVertex]].y;
		lpmapview->hptFrustumHullPlanes[iVertex].y = fptVertices[iHullIndices[iVertex]].x - lpclipplane->x;
		lpmapview->hptFrustumHullPlanes[iVertex].z = 0;
		lpmapview->hptFrustumHullPlanes[iVertex].w = -(lpmapview->hptFrustumHullPlanes[iVertex].x * lpclipplane->x + lpmapview->hptFrustumHullPlanes[iVertex].y * lpclipplane->y);

		lpclipplane->lphptPlane = lpmapview->hptFrustumHullPlanes + iVertex;
	}

	lpmapview->cFrustumHullPlanes = cHullVertices;

	lpclipplane->lpclipplaneNext = lpclipplaneFirst;

	return lpclipplane;
}

static void DoIsoRenderProjection(MAPVIEW *lpmapview)
{
	const int iDepth = 10000;
	const float fLeft = lpmapview->xLeft -(float)lpmapview->cxDrawSurface/2 / lpmapview->fZoom;
	const float fRight = lpmapview->xLeft + (float)lpmapview->cxDrawSurface/2 / lpmapview->fZoom;
	const float fBottom = lpmapview->yTop - (float)lpmapview->cyDrawSurface/2 / lpmapview->fZoom;
	const float fTop = lpmapview->yTop + (float)lpmapview->cyDrawSurface/2 / lpmapview->fZoom;
	const float fDepth = iDepth / lpmapview->fZoom;


	/* Get frustum clip coordinates. */
	if(!lpmapview->bPicking)
	{
		lpmapview->fptFrustumClipVertices[0][0].x = fLeft;
		lpmapview->fptFrustumClipVertices[0][0].y = fBottom;
		lpmapview->fptFrustumClipVertices[0][0].z = -fDepth;

		lpmapview->fptFrustumClipVertices[0][1].x = fRight;
		lpmapview->fptFrustumClipVertices[0][1].y = fBottom;
		lpmapview->fptFrustumClipVertices[0][1].z = -fDepth;

		lpmapview->fptFrustumClipVertices[0][2].x = fRight;
		lpmapview->fptFrustumClipVertices[0][2].y = fTop;
		lpmapview->fptFrustumClipVertices[0][2].z = -fDepth;

		lpmapview->fptFrustumClipVertices[0][3].x = fLeft;
		lpmapview->fptFrustumClipVertices[0][3].y = fTop;
		lpmapview->fptFrustumClipVertices[0][3].z = -fDepth;


		lpmapview->fptFrustumClipVertices[1][0].x = fLeft;
		lpmapview->fptFrustumClipVertices[1][0].y = fBottom;
		lpmapview->fptFrustumClipVertices[1][0].z = fDepth;

		lpmapview->fptFrustumClipVertices[1][1].x = fRight;
		lpmapview->fptFrustumClipVertices[1][1].y = fBottom;
		lpmapview->fptFrustumClipVertices[1][1].z = fDepth;

		lpmapview->fptFrustumClipVertices[1][2].x = fRight;
		lpmapview->fptFrustumClipVertices[1][2].y = fTop;
		lpmapview->fptFrustumClipVertices[1][2].z = fDepth;

		lpmapview->fptFrustumClipVertices[1][3].x = fLeft;
		lpmapview->fptFrustumClipVertices[1][3].y = fTop;
		lpmapview->fptFrustumClipVertices[1][3].z = fDepth;
	}


	glOrtho(fLeft, fRight, fBottom, fTop, -fDepth, fDepth);
}

static __inline void DoPerspectiveRenderProjection(MAPVIEW *lpmapview)
{
	float fHalfHeight = PERSPECTIVE_NEAR_Z * tanf((float)lpmapview->iFOV / 2 * PI/180);
	float fHalfWidth = fHalfHeight * (float)lpmapview->cxDrawSurface/lpmapview->cyDrawSurface;
	float fFarScale = (GLdouble)PERSPECTIVE_FAR_Z / PERSPECTIVE_NEAR_Z;


	/* Get frustum clip coordinates. */
	if(!lpmapview->bPicking)
	{
		lpmapview->fptFrustumClipVertices[0][0].x = -fHalfWidth;
		lpmapview->fptFrustumClipVertices[0][0].y = -fHalfHeight;
		lpmapview->fptFrustumClipVertices[0][0].z = -PERSPECTIVE_NEAR_Z;

		lpmapview->fptFrustumClipVertices[0][1].x = fHalfWidth;
		lpmapview->fptFrustumClipVertices[0][1].y = -fHalfHeight;
		lpmapview->fptFrustumClipVertices[0][1].z = -PERSPECTIVE_NEAR_Z;

		lpmapview->fptFrustumClipVertices[0][2].x = fHalfWidth;
		lpmapview->fptFrustumClipVertices[0][2].y = fHalfHeight;
		lpmapview->fptFrustumClipVertices[0][2].z = -PERSPECTIVE_NEAR_Z;

		lpmapview->fptFrustumClipVertices[0][3].x = -fHalfWidth;
		lpmapview->fptFrustumClipVertices[0][3].y = fHalfHeight;
		lpmapview->fptFrustumClipVertices[0][3].z = -PERSPECTIVE_NEAR_Z;


		lpmapview->fptFrustumClipVertices[1][0].x = -fHalfWidth * fFarScale;
		lpmapview->fptFrustumClipVertices[1][0].y = -fHalfHeight * fFarScale;
		lpmapview->fptFrustumClipVertices[1][0].z = -PERSPECTIVE_FAR_Z;

		lpmapview->fptFrustumClipVertices[1][1].x = fHalfWidth * fFarScale;
		lpmapview->fptFrustumClipVertices[1][1].y = -fHalfHeight * fFarScale;
		lpmapview->fptFrustumClipVertices[1][1].z = -PERSPECTIVE_FAR_Z;

		lpmapview->fptFrustumClipVertices[1][2].x = fHalfWidth * fFarScale;
		lpmapview->fptFrustumClipVertices[1][2].y = fHalfHeight * fFarScale;
		lpmapview->fptFrustumClipVertices[1][2].z = -PERSPECTIVE_FAR_Z;

		lpmapview->fptFrustumClipVertices[1][3].x = -fHalfWidth * fFarScale;
		lpmapview->fptFrustumClipVertices[1][3].y = fHalfHeight * fFarScale;
		lpmapview->fptFrustumClipVertices[1][3].z = -PERSPECTIVE_FAR_Z;
	}


	glFrustum(-fHalfWidth, fHalfWidth, -fHalfHeight, fHalfHeight, PERSPECTIVE_NEAR_Z, PERSPECTIVE_FAR_Z);
}

static void SetPickingProjection(MAPVIEW *lpmapview, short x, short y)
{
	GLint iViewport[] = {0, 0, lpmapview->cxDrawSurface, lpmapview->cyDrawSurface};

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPickMatrix(x, y, 1, 1, iViewport);
	
	if(lpmapview->rendermode == RM_ISOMETRIC)
		DoIsoRenderProjection(lpmapview);
	else
		DoPerspectiveRenderProjection(lpmapview);

	GetFrustumClipCoordinates(lpmapview);

	glMatrixMode(GL_MODELVIEW);
}

static void SetIsometricProjection(MAPVIEW *lpmapview)
{
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	DoIsoRenderProjection(lpmapview);
	glMatrixMode(GL_MODELVIEW);
}

static void SetPerspectiveProjection(MAPVIEW *lpmapview)
{
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	DoPerspectiveRenderProjection(lpmapview);
	glMatrixMode(GL_MODELVIEW);
}

void Set3DProjection(MAPVIEW *lpmapview)
{
	if(lpmapview->rendermode == RM_ISOMETRIC)
		SetIsometricProjection(lpmapview);
	else
		SetPerspectiveProjection(lpmapview);
}


void Zoom3D(MAPVIEW *lpmapview, float fZoomDelta)
{
	UNREFERENCED_PARAMETER(lpmapview);

	SetZoom(lpmapview, lpmapview->fZoom * fZoomDelta);
	SetIsometricProjection(lpmapview);

	lpmapview->bCentreDirty = TRUE;
}

UINT PickObject(MAP *lpmap, MAPSURFACES *lpmapsurfaces, BSP_TREE *lpbsptree, MAPVIEW *lpmapview, short x, short y)
{
	UINT uiName = 0;

	y = lpmapview->cyDrawSurface - y - 1;

	/* Enter picking mode. */
	lpmapview->bPicking = TRUE;

	glDisable(GL_TEXTURE_2D);
	glDisable(GL_BLEND);

	/* Save projection matrix. */
	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glMatrixMode(GL_MODELVIEW);

	/* Set special picking projection matrix. */
	SetPickingProjection(lpmapview, x, y);

	/* Render scene. */
	RedrawMap3D(lpmap, lpmapsurfaces, lpbsptree, lpmapview);

	/* The name is encoded in the colour of the pixel. */
	glFlush();
	glReadPixels(x, y, 1, 1, GL_RGBA, GL_UNSIGNED_BYTE, &uiName);

	glEnable(GL_TEXTURE_2D);
	glEnable(GL_BLEND);
	lpmapview->bPicking = FALSE;

	/* Restore projection matrix. */
	glMatrixMode(GL_PROJECTION);
	glPopMatrix();
	glMatrixMode(GL_MODELVIEW);

	return uiName;
}


ENUM_EDITMODE GetEditorObjectTypeFromPickedName(UINT uiName)
{
	switch((uiName >> SURFACE_TYPE_SHIFT) & 0xff)
	{
	case ST_FRONTLOWER: case ST_FRONTMIDDLE: case ST_FRONTUPPER:
	case ST_BACKLOWER: case ST_BACKMIDDLE: case ST_BACKUPPER:
		return EM_LINES;

	case ST_FLOOR: case ST_CEILING:
		return EM_SECTORS;
	}

	return EM_MOVE;
}

WPARAM GetQuickEditTypeFromPickedName(UINT uiName)
{
	switch((uiName >> SURFACE_TYPE_SHIFT) & 0xff)
	{
		case ST_FRONTLOWER:		return TP_FRONTLOWER;
		case ST_FRONTMIDDLE:	return TP_FRONTMIDDLE;
		case ST_FRONTUPPER:		return TP_FRONTUPPER;
		
		case ST_BACKLOWER:		return TP_BACKLOWER;
		case ST_BACKMIDDLE:		return TP_BACKMIDDLE;
		case ST_BACKUPPER:		return TP_BACKUPPER;

		case ST_FLOOR:			return TP_FLOOR;
		case ST_CEILING:		return TP_CEILING;

		case ST_FOFTOP:			return TP_FOFTOP;
		case ST_FOFBOTTOM:		return TP_FOFBOTTOM;
		case ST_FOFWALL:		return TP_FOFWALL;
	}

	return (WPARAM)-1;
}


int GetIndexFromPickedName(UINT uiName)
{
	return (int)(uiName & ((1 << SURFACE_TYPE_SHIFT) - 1));
}


void UpdateLineSurfaces(MAP *lpmap, HWND hwndMap, MAPSURFACES *lpmapsurfaces, int iLinedef)
{
	MAPLINEDEF *lpld = &lpmap->linedefs[iLinedef];
	int i;

	struct
	{
		unsigned short	unSidedef;
		LINESURFACES	*lplinesurfaces;
	} sdsurfacepairs[] =
	{
		{lpld->s1, &lpmapsurfaces->lplinesurfaces[iLinedef * 2]},
		{lpld->s2, &lpmapsurfaces->lplinesurfaces[iLinedef * 2 + 1]}
	};

	/* All we need to do is update the textures, as the lines aren't allowed to
	 * move.
	 */
	for(i = 0; i < (int)NUM_ELEMENTS(sdsurfacepairs); i++)
	{
		if(SidedefExists(lpmap, sdsurfacepairs[i].unSidedef))
		{
			MAPSIDEDEF *lpsd = &lpmap->sidedefs[sdsurfacepairs[i].unSidedef];
			TCHAR szTexName[TEXNAME_BUFFER_LENGTH];

			wsprintf(szTexName, TEXT("%.8hs"), lpsd->lower);
			GetTextureForMap(hwndMap, szTexName, &sdsurfacepairs[i].lplinesurfaces->s3dLower.lptex, TF_TEXTURE);

			wsprintf(szTexName, TEXT("%.8hs"), lpsd->middle);
			GetTextureForMap(hwndMap, szTexName, &sdsurfacepairs[i].lplinesurfaces->s3dMiddle.lptex, TF_TEXTURE);

			wsprintf(szTexName, TEXT("%.8hs"), lpsd->lower);
			GetTextureForMap(hwndMap, szTexName, &sdsurfacepairs[i].lplinesurfaces->s3dUpper.lptex, TF_TEXTURE);
		}
	}
}


void UpdateSectorSurfaces(MAP *lpmap, HWND hwndMap, MAPSURFACES *lpmapsurfaces, int iSector)
{
	MAPSECTOR *lpsector = &lpmap->sectors[iSector];
	MAPQUAD *lpmapquad;

	struct
	{
		LPSTR	sTexNameA;
		short	z;
	} texheightpairs[] =
	{
		{lpsector->tfloor, lpsector->hfloor},
		{lpsector->tceiling, lpsector->hceiling}
	};

	/* Loop through subsectors. */
	for(lpmapquad = lpmapsurfaces->lplpmapquadSectorQuads[iSector];
		lpmapquad;
		lpmapquad = lpmapquad->lpmapquadNext)
	{
		TCHAR szTexName[TEXNAME_BUFFER_LENGTH];
		SURFACE3D *lps3d = &lpmapsurfaces->lps3dFloorCeilPolys[lpmapquad->iSubsectorID * 2];
		int iFloorCeil;

		for(iFloorCeil = 0; iFloorCeil < (int)NUM_ELEMENTS(texheightpairs); iFloorCeil++)
		{
			int iVertex;

			/* Update texture. */
			wsprintf(szTexName, TEXT("%.8hs"), texheightpairs[iFloorCeil].sTexNameA);
			GetTextureForMap(hwndMap, szTexName, &lps3d->lptex, TF_FLAT);

			/* Update vertices. */
			for(iVertex = 0; iVertex < lps3d->cVertices; iVertex++)
				lps3d->lpvx3dVertices[iVertex].z = texheightpairs[iFloorCeil].z;

			/* Colour. */
			lps3d->uiColour = MakeSurfaceColour((BYTE)lpsector->brightness);

			lps3d++;
		}
	}
}


void UpdateNeededLinedefSurfacesBySector(MAP *lpmap, MAPSURFACES *lpmapsurfaces, HWND hwndMap, LPCTSTR szSky, int iSector)
{
	int iLinedef;

	/* Update all affected linedefs. */
	for(iLinedef = 0; iLinedef < lpmap->iLinedefs; iLinedef++)
	{
		MAPLINEDEF *lpld = &lpmap->linedefs[iLinedef];
		if((SidedefExists(lpmap, lpld->s1) && lpmap->sidedefs[lpld->s1].sector == iSector) ||
			(SidedefExists(lpmap, lpld->s1) && lpmap->sidedefs[lpld->s2].sector == iSector))
		{
			SetNeededSurfacesForLinedef(lpmap, iLinedef, szSky, LS_FRONT, lpmapsurfaces, hwndMap);
			SetNeededSurfacesForLinedef(lpmap, iLinedef, szSky, LS_BACK,  lpmapsurfaces, hwndMap);
		}
	}
}


static __inline UINT MakeSurfaceColour(BYTE byBrightness)
{
	return byBrightness | (byBrightness << 8) | (byBrightness << 16) | (255 << 24);
}


static void ResetModelviewMatrix(MAPVIEW *lpmapview)
{
	glLoadIdentity();

	if(lpmapview->rendermode == RM_ISOMETRIC)
	{
		gluLookAt(lpmapview->x + 1.0, lpmapview->y - 1.0, lpmapview->z + 1.0,
				lpmapview->x, lpmapview->y, lpmapview->z,
				0.0, 0.0, 1.0);
	}
	else
	{
		gluLookAt(
				lpmapview->x, lpmapview->y, lpmapview->z,
				lpmapview->x + cosf(lpmapview->fXAngle) * cosf(lpmapview->fYAngle),
				lpmapview->y + sinf(lpmapview->fXAngle) * cosf(lpmapview->fYAngle),
				lpmapview->z + sinf(lpmapview->fYAngle),
				0.0, 0.0, 1.0);
	}
}


static void DrawCrosshair(MAPVIEW *lpmapview)
{
	float fHalfLength = PERSPECTIVE_NEAR_Z * tanf((PI/180) * lpmapview->iFOV / 2) / 20;

	glPushMatrix();
	glLoadIdentity();

	glDisable(GL_DEPTH_TEST);
	glDisable(GL_TEXTURE_2D);
	
	glColor4f(1.0f, 1.0f, 1.0f, 0.5f);

	glBegin(GL_LINES);
		glVertex3f(0, fHalfLength, -PERSPECTIVE_NEAR_Z);
		glVertex3f(0, -fHalfLength, -PERSPECTIVE_NEAR_Z);
		glVertex3f(fHalfLength, 0, -PERSPECTIVE_NEAR_Z);
		glVertex3f(-fHalfLength, 0, -PERSPECTIVE_NEAR_Z);
	glEnd();

	glEnable(GL_TEXTURE_2D);
	glEnable(GL_DEPTH_TEST);

	glPopMatrix();
}


void UpdateFOFPlanes(MAPSURFACES *lpmapsurfaces, MAP *lpmap, HWND hwndMap, int iControlLinedef)
{
	int iSubsector;
	MAPSECTOR *lpsectorControl;
	MAPSIDEDEF *lpsdControl;
	MAPLINEDEF *lpldControl = &lpmap->linedefs[iControlLinedef];

	if(!SidedefExists(lpmap, lpldControl->s1))
		return;

	lpsdControl = &lpmap->sidedefs[lpldControl->s1];

	if(lpsdControl->sector < 0)
		return;

	lpsectorControl = &lpmap->sectors[lpsdControl->sector];

	for(iSubsector = 0; iSubsector < lpmapsurfaces->cSubsectors; iSubsector++)
	{
		FOF_SURFACE_ARRAY *lpfsa = lpmapsurfaces->lplpfsaFOFPlanesBySS[iSubsector];
		int iSurface;
		
		if(lpfsa)
		{
			for(iSurface = 0; iSurface < lpfsa->cSurfaces; iSurface++)
			{
				SURFACE3D *lps3d = &lpfsa->s3dPlanes[iSurface];
				if(GetIndexFromPickedName(lps3d->uiName) == iControlLinedef)
				{
					TCHAR szTexName[TEXNAME_BUFFER_LENGTH];
					int iVertex;

					wsprintf(szTexName, TEXT("%.8hs"), (iSurface & 1) ? lpsectorControl->tceiling : lpsectorControl->tfloor);
					GetTextureForMap(hwndMap, szTexName, &lps3d->lptex, TF_FLAT);

					for(iVertex = 0; iVertex < lps3d->cVertices; iVertex++)
						lps3d->lpvx3dVertices->z = (float)((iSurface & 1) ? lpsectorControl->hceiling : lpsectorControl->hfloor);
				}
			}
		}
	}
}


void UpdateFOFWalls(MAPSURFACES *lpmapsurfaces, MAP *lpmap, HWND hwndMap, int iControlLinedef)
{
	int iLinedef;
	MAPSECTOR *lpsectorControl;
	MAPSIDEDEF *lpsdControl;
	MAPLINEDEF *lpldControl = &lpmap->linedefs[iControlLinedef];

	if(!SidedefExists(lpmap, lpldControl->s1))
		return;

	lpsdControl = &lpmap->sidedefs[lpldControl->s1];

	if(lpsdControl->sector < 0)
		return;

	lpsectorControl = &lpmap->sectors[lpsdControl->sector];

	for(iLinedef = 0; iLinedef < lpmap->iLinedefs; iLinedef++)
	{
		FOF_SURFACE_ARRAY *lpfsa = lpmapsurfaces->lplpfsaFOFWallsByLinedef[iLinedef];
		int iSurface;
		
		if(lpfsa)
		{
			for(iSurface = 0; iSurface < lpfsa->cSurfaces; iSurface++)
			{
				SURFACE3D *lps3d = &lpfsa->s3dPlanes[iSurface];
				if(GetIndexFromPickedName(lps3d->uiName) == iControlLinedef)
				{
					TCHAR szTexName[TEXNAME_BUFFER_LENGTH];

					wsprintf(szTexName, TEXT("%.8hs"), lpsdControl->middle);
					GetTextureForMap(hwndMap, szTexName, &lps3d->lptex, TF_TEXTURE);

					lps3d->lpvx3dVertices[0].z = (float)lpsectorControl->hfloor;
					lps3d->lpvx3dVertices[1].z = (float)lpsectorControl->hfloor;
					lps3d->lpvx3dVertices[2].z = (float)lpsectorControl->hceiling;
					lps3d->lpvx3dVertices[3].z = (float)lpsectorControl->hceiling;
				}
			}
		}
	}
}


/* Invert4x4Matrix
 *   Inverts a 4x4 matrix.
 *
 * Parameters:
 *   const double[16]	m		In.
 *   double				invOut	Out.
 *
 * Return value: int
 *   1 on success, 0 on failure.
 *
 * Remarks:
 *   This is taken almost straight from Mesa <http://www.mesa3d.org/>, which is
 *   MIT-licensed. Although this bit might be SGI Free B. Mesa is copyright
 *   1999-2007 Brian Paul; this code is labelled as being contributed by David
 *   Moore. I think that should be sufficient waffle.
 */
static int Invert4x4Matrix(const double m[16], double invOut[16])
{
    double inv[16], det;
    int i;

    inv[0] = m[5]  * m[10] * m[15] - 
             m[5]  * m[11] * m[14] - 
             m[9]  * m[6]  * m[15] + 
             m[9]  * m[7]  * m[14] +
             m[13] * m[6]  * m[11] - 
             m[13] * m[7]  * m[10];

    inv[4] = -m[4]  * m[10] * m[15] + 
              m[4]  * m[11] * m[14] + 
              m[8]  * m[6]  * m[15] - 
              m[8]  * m[7]  * m[14] - 
              m[12] * m[6]  * m[11] + 
              m[12] * m[7]  * m[10];

    inv[8] = m[4]  * m[9] * m[15] - 
             m[4]  * m[11] * m[13] - 
             m[8]  * m[5] * m[15] + 
             m[8]  * m[7] * m[13] + 
             m[12] * m[5] * m[11] - 
             m[12] * m[7] * m[9];

    inv[12] = -m[4]  * m[9] * m[14] + 
               m[4]  * m[10] * m[13] +
               m[8]  * m[5] * m[14] - 
               m[8]  * m[6] * m[13] - 
               m[12] * m[5] * m[10] + 
               m[12] * m[6] * m[9];

    inv[1] = -m[1]  * m[10] * m[15] + 
              m[1]  * m[11] * m[14] + 
              m[9]  * m[2] * m[15] - 
              m[9]  * m[3] * m[14] - 
              m[13] * m[2] * m[11] + 
              m[13] * m[3] * m[10];

    inv[5] = m[0]  * m[10] * m[15] - 
             m[0]  * m[11] * m[14] - 
             m[8]  * m[2] * m[15] + 
             m[8]  * m[3] * m[14] + 
             m[12] * m[2] * m[11] - 
             m[12] * m[3] * m[10];

    inv[9] = -m[0]  * m[9] * m[15] + 
              m[0]  * m[11] * m[13] + 
              m[8]  * m[1] * m[15] - 
              m[8]  * m[3] * m[13] - 
              m[12] * m[1] * m[11] + 
              m[12] * m[3] * m[9];

    inv[13] = m[0]  * m[9] * m[14] - 
              m[0]  * m[10] * m[13] - 
              m[8]  * m[1] * m[14] + 
              m[8]  * m[2] * m[13] + 
              m[12] * m[1] * m[10] - 
              m[12] * m[2] * m[9];

    inv[2] = m[1]  * m[6] * m[15] - 
             m[1]  * m[7] * m[14] - 
             m[5]  * m[2] * m[15] + 
             m[5]  * m[3] * m[14] + 
             m[13] * m[2] * m[7] - 
             m[13] * m[3] * m[6];

    inv[6] = -m[0]  * m[6] * m[15] + 
              m[0]  * m[7] * m[14] + 
              m[4]  * m[2] * m[15] - 
              m[4]  * m[3] * m[14] - 
              m[12] * m[2] * m[7] + 
              m[12] * m[3] * m[6];

    inv[10] = m[0]  * m[5] * m[15] - 
              m[0]  * m[7] * m[13] - 
              m[4]  * m[1] * m[15] + 
              m[4]  * m[3] * m[13] + 
              m[12] * m[1] * m[7] - 
              m[12] * m[3] * m[5];

    inv[14] = -m[0]  * m[5] * m[14] + 
               m[0]  * m[6] * m[13] + 
               m[4]  * m[1] * m[14] - 
               m[4]  * m[2] * m[13] - 
               m[12] * m[1] * m[6] + 
               m[12] * m[2] * m[5];

    inv[3] = -m[1] * m[6] * m[11] + 
              m[1] * m[7] * m[10] + 
              m[5] * m[2] * m[11] - 
              m[5] * m[3] * m[10] - 
              m[9] * m[2] * m[7] + 
              m[9] * m[3] * m[6];

    inv[7] = m[0] * m[6] * m[11] - 
             m[0] * m[7] * m[10] - 
             m[4] * m[2] * m[11] + 
             m[4] * m[3] * m[10] + 
             m[8] * m[2] * m[7] - 
             m[8] * m[3] * m[6];

    inv[11] = -m[0] * m[5] * m[11] + 
               m[0] * m[7] * m[9] + 
               m[4] * m[1] * m[11] - 
               m[4] * m[3] * m[9] - 
               m[8] * m[1] * m[7] + 
               m[8] * m[3] * m[5];

    inv[15] = m[0] * m[5] * m[10] - 
              m[0] * m[6] * m[9] - 
              m[4] * m[1] * m[10] + 
              m[4] * m[2] * m[9] + 
              m[8] * m[1] * m[6] - 
              m[8] * m[2] * m[5];

    det = m[0] * inv[0] + m[1] * inv[4] + m[2] * inv[8] + m[3] * inv[12];

    if (det == 0)
        return 0;

    det = 1.0 / det;

    for (i = 0; i < 16; i++)
        invOut[i] = inv[i] * det;

    return 1;
}


/* MakeBalancedLindefIndexArray
 *   Heuristically sorts linedefs for BSP splitting.
 *
 * Parameters:
 *   MAP*	lpmap					Map.
 *   int*	lpiShuffledLinedefs		Array to populate. It better be big enough.
 *
 * Return value: None.
 *
 * Remarks:
 *   Tweak BALANCE_LEVELS to determine how hard to work. The heuristic is
 *   pretty naive: it doesn't adjust for where we *actually* split, and neither
 *   does it take into account that every linedef causes *two* horiz splits.
 */
static void MakeBalancedLindefIndexArray(MAP *lpmap, int *lpiShuffledLinedefs)
{
	int iIdealSplits[(1 << BALANCE_LEVELS) - 1];
	int *lpyVertices = ProcHeapAlloc(lpmap->iVertices * sizeof(int));
	int i, j;
	int cBalanceLines = min((int)NUM_ELEMENTS(iIdealSplits), lpmap->iLinedefs);

	/* Prepare array unsorted. */
	for(i = 0; i < lpmap->iLinedefs; i++)
		lpiShuffledLinedefs[i] = i;

	/* Sort vertex y co-ordinates, bearing in mind that Seidel will always
	 * split by y.
	 */
	for(i = 0; i < lpmap->iVertices; i++)
		lpyVertices[i] = lpmap->vertices[i].y;
	qsort(lpyVertices, lpmap->iVertices, sizeof(int), QsortIntegerComparison);

	/* Where would we split in an ideal world? */
	for(i = 0; i < BALANCE_LEVELS; i++)
	{
		for(j = 0; j <= i; j++)
			iIdealSplits[(1 << i) - 1 + j] = lpyVertices[lpmap->iVertices/(1 << (i+1)) * (2*j + 1)];
	}

	/* Get as close to the ideal as we can. */
	for(i = 0; i < cBalanceLines; i++)
	{
		int iError = INT_MAX;

		for(j = i; j < lpmap->iLinedefs; j++)
		{
			int iNewError;
			MAPLINEDEF *lpld = &lpmap->linedefs[j];
			MAPVERTEX *lpvx1 = &lpmap->vertices[lpld->v1];
			MAPVERTEX *lpvx2 = &lpmap->vertices[lpld->v2];

			/* Note the effect of the short circuit on the assignment. */
			if((iNewError = abs(lpvx1->y - iIdealSplits[i])) < iError ||
				(iNewError = abs(lpvx2->y - iIdealSplits[i])) < iError)
			{
				int iIntermediate = lpiShuffledLinedefs[i];
				lpiShuffledLinedefs[i] = lpiShuffledLinedefs[j];
				lpiShuffledLinedefs[j] = iIntermediate;
				iError = iNewError;
			}
		}
	}

	ProcHeapFree(lpyVertices);
}


static void TransformVector(GLdouble fMatrix[16], const GLdouble fInVec[4], GLdouble fOutVec[4])
{
	fOutVec[0] = fMatrix[0] * fInVec[0] + fMatrix[4] * fInVec[1] + fMatrix[8] * fInVec[2] + fMatrix[12] * fInVec[3];
	fOutVec[1] = fMatrix[1] * fInVec[0] + fMatrix[5] * fInVec[1] + fMatrix[9] * fInVec[2] + fMatrix[13] * fInVec[3];
	fOutVec[2] = fMatrix[2] * fInVec[0] + fMatrix[6] * fInVec[1] + fMatrix[10] * fInVec[2] + fMatrix[14] * fInVec[3];
	fOutVec[3] = fMatrix[3] * fInVec[0] + fMatrix[7] * fInVec[1] + fMatrix[11] * fInVec[2] + fMatrix[15] * fInVec[3];
}


static void GetFrustumClipCoordinates(MAPVIEW *lpmapview)
{
	GLdouble fProj[16], fProjInverse[16];
	GLdouble fVecIn[4], fVecOut[4];
	GLdouble fPlusMinus[2] = {-1.0, 1.0};

	int i, j, k;

	glGetDoublev(GL_PROJECTION_MATRIX, fProj);
	Invert4x4Matrix(fProj, fProjInverse);

	/* This doesn't change. */
	fVecIn[3] = 1.0;

	for(i = 0; i < 2; i++)
	{
		fVecIn[2] = fPlusMinus[i];
		for(j = 0; j < 2; j++)
		{
			fVecIn[1] = fPlusMinus[j];
			for(k = 0; k < 2; k++)
			{
				fVecIn[0] = fPlusMinus[k];

				TransformVector(fProjInverse, fVecIn, fVecOut);
				lpmapview->fptFrustumClipVertices[i][2*j+k].x = (GLfloat)(fVecOut[0] / fVecOut[3]);
				lpmapview->fptFrustumClipVertices[i][2*j+k].y = (GLfloat)(fVecOut[1] / fVecOut[3]);
				lpmapview->fptFrustumClipVertices[i][2*j+k].z = (GLfloat)(fVecOut[2] / fVecOut[3]);
			}
		}
	}
}


static BOOL LinedefInFrustum(MAP *lpmap, int iLinedef, MAPVIEW *lpmapview)
{
	MAPLINEDEF *lpld = &lpmap->linedefs[iLinedef];
	MAPVERTEX *lpvx1 = &lpmap->vertices[lpld->v1];
	MAPVERTEX *lpvx2 = &lpmap->vertices[lpld->v2];
	int i;

	for(i = 0; i < lpmapview->cFrustumHullPlanes; i++)
	{
		if(SideOfPlane2D(&lpmapview->hptFrustumHullPlanes[i], (float)lpvx1->x, (float)lpvx1->y) < 0 &&
			SideOfPlane2D(&lpmapview->hptFrustumHullPlanes[i], (float)lpvx2->x, (float)lpvx2->y) < 0)
			return FALSE;
	}

	return TRUE;
}
