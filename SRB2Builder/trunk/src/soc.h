/* SRB2 Workbench -- A map editor for Sonic Robo Blast 2.
 * Copyright (C) 2009 Gregor Dick
 * http://workbench.srb2.org/
 *
 * Incorporates portions of Doom Builder, (C) 2003 Pascal vd Heiden.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 * USA.
 *
 * soc.h: Header for soc.c.
 *
 * AMDG.
 */

#ifndef __SRB2B_SOC__
#define __SRB2B_SOC__

#include <windows.h>

#include "wad.h"
#include "texture.h"

typedef struct _MAPHEADER
{
	DWORD		dwMask;					/* Which fields are valid? */

	LPSTR		szLevelName;
	LPSTR		szMapCredits;
	int			iAct;
	int			iMusicSlot;
	int			iSkyNum;
	BOOL		bTimeOfDay;
	int			iWeather;
	DWORD		dwTypeOfLevel;
	int			iNextLevel;
	BOOL		bHidden;
	BOOL		bLevelSelect;
	BOOL		bTimeAttack;
	int			iCutsceneNum;
	int			iPreCutsceneNum;
	int			iForceCharacter;
	int			iCountdown;
	BOOL		bNoZone;
	char		szInterScreen[CCH_LUMPNAME + 1];
	BOOL		bNoReload;
	BOOL		bNoSSMusic;
	LPSTR		szScriptName;
	BOOL		bScriptIsLump;
	LPSTR		szSubtitle;
	LPSTR		szRunSOC;
	BOOL		bSpeedMusic;
} MAPHEADER;


/* The following flags are used both to indicate which fields in a map header
 * are valid and which functions are supported by a particular version of SRB2.
 * In the latter case, only those in MHF_SUPPORTCHECK are considered; others
 * are always supported.
 */
enum ENUM_MAPHEADER_FLAGS
{
	MHF_LEVELNAME			= 0x00000001,
	MHF_MAPCREDITS			= 0x00000002,
	MHF_ACT					= 0x00000004,
	MHF_MUSICSLOT			= 0x00000008,
	MHF_SKYNUM				= 0x00000010,
	MHF_TIMEOFDAY			= 0x00000020,
	MHF_WEATHER				= 0x00000040,
	MHF_TYPEOFLEVEL			= 0x00000080,
	MHF_NEXTLEVEL			= 0x00000100,
	MHF_HIDDEN				= 0x00000200,
	MHF_LEVELSELECT			= 0x00000400,
	MHF_TIMEATTACK			= 0x00000800,
	MHF_CUTSCENENUM			= 0x00001000,
	MHF_PRECUTSCENENUM		= 0x00002000,
	MHF_FORCECHARACTER		= 0x00004000,
	MHF_COUNTDOWN			= 0x00008000,
	MHF_NOZONE				= 0x00010000,
	MHF_INTERSCREEN			= 0x00020000,
	MHF_NORELOAD			= 0x00040000,
	MHF_NOSSMUSIC			= 0x00080000,
	MHF_SCRIPTNAME			= 0x00100000,
	MHF_SCRIPTISLUMP		= 0x00200000,
	MHF_RUNSOC				= 0x00400000,
	MHF_SUBTITLE			= 0x00800000,
	MHF_SPEEDMUSIC			= 0x01000000
};


typedef enum _ENUM_HEADER_TYPE
{
	HT_MAINCFG,
	HT_MAPXXD,
	HT_NONE
} ENUM_HEADER_TYPE;



#define FORCECHAR_NONE		255



ENUM_HEADER_TYPE GetMapHeader(WAD *lpwad, int iLevelNum, MAPHEADER *lpmapheader, DWORD *lpcchOffsetStart, DWORD *lpcchOffsetEnd);
void FreeMapHeaderStrings(MAPHEADER *lpmapheader);
void SetMissingRequiredMapHeaderFields(MAPHEADER *lpmapheader, int iLevelNum);
void SetMapHeader(WAD *lpwad, int iLevelNum, MAPHEADER *lpmapheader);
void ProcessTextureSOC(LPCSTR sSOC, DWORD cchSOC, CONFIG *lpcfgTexEntries, TEXTURENAMELIST *lptnl);


#endif
