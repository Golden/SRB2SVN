/* SRB2 Workbench -- A map editor for Sonic Robo Blast 2.
 * Copyright (C) 2009 Gregor Dick
 * http://workbench.srb2.org/
 *
 * Incorporates portions of Doom Builder, (C) 2003 Pascal vd Heiden.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 * USA.
 *
 * maptypes.h: Types representing records in map data structures (and one or two
 * others). Adapted from Doom Builder.
 *
 * AMDG.
 */

#ifndef __SRB2B_MAPTYPES__
#define __SRB2B_MAPTYPES__

/* Sizes of entries in the respective lumps. */
#define LINEDEFRECORDSIZE	14
#define SECTORRECORDSIZE	26
#define THINGRECORDSIZE		10
#define SIDEDEFRECORDSIZE	30
#define VERTEXRECORDSIZE	4

typedef struct _FPOINT
{
	float x;
	float y;
} FPOINT;

typedef struct _FPOINT3D
{
	float x;
	float y;
	float z;
} FPOINT3D;

typedef struct _FRECT
{
	float left;
	float top;
	float right;
	float bottom;
} FRECT;



/* The first parts of these structures is exactly the lump structure. */
typedef struct _MAPTHING
{
	short x;
	short y;
	short angle;
	unsigned short thing;
	unsigned short flag;

	/* Pseudo-lump-fields. These aren't part of the lump structure, but are
	 * encoded into fields above just-in-time.
	 */
	BYTE	typeuppernybble;

	/* Fields beyond here are used by the editor only. */

	/* Fields beyond here are not compared when testing equality in undo frames.
	 */
#define THING_UNDO_IGNORE_FROM	z
	int		z;
	short	category;
	int		color;
	int		image;
	int		size;
	int		height;
	BOOL	hangs;
	BYTE	selected;
	int		sector;
	BOOL	circle;
	BYTE	highlight;
	BOOL	arrow;
} MAPTHING;

typedef struct _MAPLINEDEF
{
	unsigned short v1;
	unsigned short v2;
	short flags;
	short effect;
	short tag;
	unsigned short s1;
	unsigned short s2;

	/* Fields beyond here are used by the editor only. */
	DWORD	editflags;

	/* Fields beyond here are not compared when testing equality in undo frames.
	 */
#define LINEDEF_UNDO_IGNORE_FROM	selected
	BYTE	selected;
	BYTE	highlight;
} MAPLINEDEF;

typedef struct _MAPSIDEDEF
{
	short	tx;
	short	ty;
	char	upper[8];
	char	lower[8];
	char	middle[8];
	short	sector;
} MAPSIDEDEF;

typedef struct _MAPVERTEX
{
	short	x;
	short	y;

	/* Fields beyond here are used by the editor only. */
	BYTE	editflags;

	/* Fields beyond here are not compared when testing equality in undo frames.
	 */
#define VERTEX_UNDO_IGNORE_FROM	selected
	BYTE	selected;
	BYTE	highlight;
} MAPVERTEX;

typedef struct _MAPSECTOR
{
	short	hfloor;
	short	hceiling;
	char	tfloor[8];
	char	tceiling[8];
	short	brightness;
	short	special;
	short	tag;

	/* Fields beyond here are used by the editor only. */
	int		editflags;

	/* Fields beyond here are not compared when testing equality in undo frames.
	 */
#define SECTOR_UNDO_IGNORE_FROM	selected
	BYTE	selected;
	BYTE	highlight;
} MAPSECTOR;

typedef struct _MAP
{
	MAPSECTOR	*sectors;
	MAPLINEDEF	*linedefs;
	MAPVERTEX	*vertices;
	MAPSIDEDEF	*sidedefs;
	MAPTHING	*things;
	int			iSectors, iLinedefs;
	int			iVertices, iSidedefs;
	int			iThings;
} MAP;

#endif
