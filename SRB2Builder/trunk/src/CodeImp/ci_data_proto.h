/* SRB2 Workbench -- A map editor for Sonic Robo Blast 2.
 * Copyright (C) 2009 Gregor Dick
 * http://workbench.srb2.org/
 *
 * Incorporates portions of Doom Builder, (C) 2003 Pascal vd Heiden.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 * USA.
 *
 * ci_data_proto.h: Prototypes for ci_data.c. Adapted from Doom Builder.
 *
 * AMDG.
 */


#ifndef __SRB2B_CI_DATA_PROTO__
#define __SRB2B_CI_DATA_PROTO__

#ifdef __cplusplus
extern "C" {
#endif

#include "../editing.h"

int __fastcall NearestLinedef(MAP *lpmap, MAPOBJGRID *lpmog, int x, int y, int *lpiDist);
int NearestConditionedLinedef(MAP *lpmap, MAPOBJGRID *lpmog, int x, int y, int *lpiDist, BOOL (*fnCondition)(MAP *lpmap, int iLinedef, void *lpvParam), void *lpvParam);
int NearestConditionedVertex(MAP *lpmap, MAPOBJGRID *lpmog, int x, int y, int *lpiDist, BOOL (*fnCondition)(MAP *lpmap, int iVertex, void *lpvParam), void *lpvParam);
int NearestOtherVertex(MAP *lpmap, int vx, int *lpiDist);
int NearestConditionedVertexToLinedef(MAP *lpmap, MAPOBJGRID *lpmog, int iLinedef, int *lpiDist, BOOL (*fnCondition)(MAP *lpmap, int iIndex, void *lpvParam), void *lpvParam);
int NearestThing(MAP *lpmap, int x, int y, int *lpiDist);

BOOL LinedefNoInvalidSD(MAP *lpmap, int iLinedef, void *lpvParam);
BOOL LinedefUnlabelled(MAP *lpmap, int iLinedef, void *lpvParam);
BOOL LinedefNotAttachedToVertex(MAP *lpmap, int iLinedef, void *lpvParam);

BOOL VertexUnlabelled(MAP *lpmap, int iVertex, void *lpvParam);
BOOL VertexUnlabelledAndNotAttToLine(MAP *lpmap, int iVertex, void *lpviLinedef);
BOOL VertexNotDragging(MAP *lpmap, int iVertex, void *lpvParam);
BOOL VertexOnSegment(MAP *lpmap, int iVertex, void *lpvLineSegment);

int IntersectSector(int x, int y, MAP *lpmap, BOOL (*fnCondition)(MAP *lpmap, int iLinedef, void *lpvParam), void *lpvParam);
int IntersectSectorFloat(float x, float y, MAP *lpmap, BOOL (*fnCondition)(MAP *lpmap, int iLinedef, void *lpvParam), void *lpvParam);

void ResetSelections(MAPTHING* things, int numthings, MAPLINEDEF* linedefs, int numlinedefs, MAPVERTEX* vertices, int numvertices, MAPSECTOR* sectors, int numsectors);

BOOL PointInSidedefs(MAP *lpmap, float x, float y, int* sideslist, int numsides, int *lpiSidesToLines);
BOOL PointInLinedefs(MAP *lpmap, float x, float y, int* lineslist, int numlines);

int LinedefIntersectingSegment(MAP* lpmap, float x1, float y1, float x2, float y2, BOOL bAdmitCoincidence);


static __inline int InLDRectOpen(MAPLINEDEF* linedefs, MAPVERTEX* vertices, int ld, float x, float y)
{
	int x1 = (int)(vertices[linedefs[ld].v1].x * 10 + 0.5);
	int y1 = (int)(vertices[linedefs[ld].v1].y * 10 + 0.5);
	int x2 = (int)(vertices[linedefs[ld].v2].x * 10 + 0.5);
	int y2 = (int)(vertices[linedefs[ld].v2].y * 10 + 0.5);
	int t;
	int X=(int)(x*10 + 0.5), Y=(int)(y*10 + 0.5);

	if(x1 > x2) {t=x1; x1=x2; x2=t;}
	if(y1 > y2) {t=y1; y1=y2; y2=t;}

	return (X >= x1 && X <= x2 && Y > y1 && Y < y2) || (X > x1 && X < x2 && Y >= y1 && Y <= y2);
}

static __inline int InLDRectClosed(MAPLINEDEF* linedefs, MAPVERTEX* vertices, int ld, float x, float y)
{
	int x1 = (int)(vertices[linedefs[ld].v1].x * 10 + 0.5);
	int y1 = (int)(vertices[linedefs[ld].v1].y * 10 + 0.5);
	int x2 = (int)(vertices[linedefs[ld].v2].x * 10 + 0.5);
	int y2 = (int)(vertices[linedefs[ld].v2].y * 10 + 0.5);
	int t;
	int X=(int)(x*10 + 0.5), Y=(int)(y*10 + 0.5);

	if(x1 > x2) {t=x1; x1=x2; x2=t;}
	if(y1 > y2) {t=y1; y1=y2; y2=t;}

	return (X >= x1 && X <= x2 && Y >= y1 && Y <= y2) || (X >= x1 && X <= x2 && Y >= y1 && Y <= y2);
}

// NearestVertex: Returns the nearest vertex index
//----------------------------------------------------------------------------
static __inline int NearestVertex(MAP *lpmap, MAPOBJGRID *lpmog, int x, int y, int *lpiDist)
{
	return NearestConditionedVertex(lpmap, lpmog, x, y, lpiDist, NULL, NULL);
}

#ifdef __cplusplus
}
#endif

#endif
