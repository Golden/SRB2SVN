/* SRB2 Workbench -- A map editor for Sonic Robo Blast 2.
 * Copyright (C) 2009 Gregor Dick
 * http://workbench.srb2.org/
 *
 * Incorporates portions of Doom Builder, (C) 2003 Pascal vd Heiden.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 * USA.
 *
 * find.h: Header for find.c.
 *
 * AMDG.
 */

#ifndef __SRB2B_FIND__
#define __SRB2B_FIND__

#include <windows.h>

#include "map.h"
#include "config.h"

/* Types. */
enum ENUM_FIND_TYPES
{
	FT_VX,
	FT_LINE, FT_LINEEFFECT, FT_LINEFLAGS, FT_LINETAG, FT_LINETEX,
	FT_SEC, FT_SECCEIL, FT_SECFLR,
	FT_SECEFFECT, FT_SECEFFECTA, FT_SECEFFECTB, FT_SECEFFECTC, FT_SECEFFECTD,
	FT_SECTAG, FT_SECCEILFLAT, FT_SECFLRFLAT, FT_SECLIGHT, FT_SECHEIGHT,
	FT_SIDEDEF,
	FT_THING, FT_THINGANGLE, FT_THINGFLAGS, FT_THINGTYPE, FT_THINGZOFF,
	FT_THINGZABS
};

enum ENUM_FIND_CONDITIONS
{
	FC_EQUAL, FC_LESS, FC_GREATER, FC_LESSEQ, FC_GREATEREQ, FC_NOTEQUAL, FC_AND
};

enum ENUM_FINDMASK_SIDEDEFS
{
	FM_SD_FRONT = 1,
	FM_SD_BACK = 2
};

enum ENUM_FINDMASK_TEXTURE
{
	FM_TEX_FRONTUPPER = 1,
	FM_TEX_FRONTMIDDLE = 2,
	FM_TEX_FRONTLOWER = 4,
	FM_TEX_BACKUPPER = 8,
	FM_TEX_BACKMIDDLE = 16,
	FM_TEX_BACKLOWER = 32
};


/* Prototypes. */
BYTE MapObjectSatisfiesFindConditionInt(MAP *lpmap, WORD wType, WORD wCompare, int iIndex, int iValue, CONFIG *lpcfgFlatThings);
BYTE MapObjectSatisfiesFindConditionStr(MAP *lpmap, WORD wType, WORD wCompare, int iIndex, LPSTR szString);
void ReplaceInt(MAP *lpmap, WORD wType, int iIndex, int iValue, CONFIG *lpcfgFlatThings);
void ReplaceStr(MAP *lpmap, WORD wType, int iIndex, LPSTR szString, BYTE byMatchFlags);


#endif
