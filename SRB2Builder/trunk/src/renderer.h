/* SRB2 Workbench -- A map editor for Sonic Robo Blast 2.
 * Copyright (C) 2009 Gregor Dick
 * http://workbench.srb2.org/
 *
 * Incorporates portions of Doom Builder, (C) 2003 Pascal vd Heiden.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 * USA.
 *
 * renderer.h: Header for renderer.c.
 *
 * AMDG.
 */

#ifndef __SRB2B_RENDERER__
#define __SRB2B_RENDERER__

#ifdef __cplusplus
extern "C" {
#endif

#include <windows.h>
#include <GL/gl.h>
#include <GL/glu.h>

#include "map.h"
#include "selection.h"
#include "win/mapwin.h"


typedef enum _ENUM_RENDER_MODE
{
	RM_2D,
	RM_ISOMETRIC,
	RM_3D
} ENUM_RENDER_MODE;

typedef struct _HOMOG_POINT
{
	float x, y, z, w;
} HOMOG_POINT;

typedef enum _HOMOG_COORD
{
	HCOORD_X,
	HCOORD_Y,
	HCOORD_Z,
	HCOORD_W
} HOMOG_COORD;


/* Types. */
typedef struct _MAPVIEW
{
	ENUM_RENDER_MODE	rendermode, prevrendermode;

	short				cxDrawSurface, cyDrawSurface;
	float				xLeft, yTop;
	float				fZoom;
	int					iIndicatorSize;
	float				fVertexSize;
	float				fThingSize;
	unsigned short		cxGrid, cyGrid;
	short				xGridOffset, yGridOffset;
	BOOL				bShowGrid, bShow64Grid;
	BOOL				bShowAxes;
	GLuint				uiThingTex;
	int					iVxSnapDist, iLineSnapDist;
	DWORD				dwLineColourFlags;

	/* 3D stuff. */
	BOOL				bNeedResize;
	int					iFrame;
	short				nAngleDelta;
	GLdouble			xCentre, yCentre;
	GLfloat				zCentreDepth;
	BOOL				bCentreDirty;
	BOOL				bPicking;
	float				fXAngle, fYAngle;
	float				x, y, z;
	int					iFOV;
	GLuint				uiHighlightName;
	FPOINT3D			fptFrustumClipVertices[2][4];
	HOMOG_POINT			hptFrustumHullPlanes[8];
	int					cFrustumHullPlanes;
} MAPVIEW;

typedef struct _MAP_RENDERINFO
{
	ENUM_EDITMODE editmode;
	ENUM_EDITSUBMODE submode;
	ENUM_INSERT_MODE insertmode;
	MAPVIEW *lpmapview;
	SELECTION *lpselection;
	short nHighlightTag;
	DWORD dwFlags;

	union
	{
		POINT ptSrc;
		FPOINT fptSrc;
	};

	union
	{
		POINT ptDest;
		FPOINT fptDest;
	};

	POINTS ptsCross;
	ENUM_EDITMODE	emHighlightType;
	int iAxisTransfer, iATLine;
} MAP_RENDERINFO;


/* Constants. */
#define RENDERER_FONT_HEIGHT_PX		9
#define RENDERER_FONT_WIDTH_PX		5
#define RENDERER_FONT_FACE			TEXT("Tahoma")
#define RENDERER_FONT_NUM_GLYPHS	96
#define RENDERER_FONT_FIRST_GLYPH	32

#define RENDERER_PRINTF_BUF_SIZE	64


/* Palette indices. Adapted from Doom Builder. */
enum ENUM_PALETTECOLORS
{
	CLR_BACKGROUND,
	CLR_VERTEX,
	CLR_VERTEXSELECTED,
	CLR_VERTEXHIGHLIGHT,
	CLR_LINE,
	CLR_LINEDOUBLE,
	CLR_LINESPECIAL,
	CLR_LINESPECIALDOUBLE,
	CLR_LINESELECTED,
	CLR_LINEHIGHLIGHT,
	CLR_SECTORTAG,
	CLR_THINGUNKNOWN,
	CLR_THINGSELECTED,
	CLR_THINGHIGHLIGHT,
	CLR_GRID,
	CLR_GRID64,
	CLR_LINEBLOCKSOUND,
	CLR_MAPBOUNDARY,
	CLR_AXES,
	CLR_ZEROHEIGHTLINE,
	CLR_FOFSECTOR,
	CLR_TEXBROWSER,
	CLR_LASSO,
	CLR_NIGHTS,
	CLR_BLACK,
	CLR_MAX
};


enum RENDERER_FLAGS
{
	RF_CROSS = 1,
	RF_NIGHTS = 2
};


enum ENUM_LINE_COLOUR_FLAGS
{
	LCF_ZEROHEIGHT		= 1,
	LCF_FAUXIMPASSABLE	= 2,
	LCF_COLOURFOFLINES	= 4,

	/* Mask for the above three, which are passed directly to the renderer. */
	LCF_DIRECTPASS_MASK	= 7,

	/* This is used for preferences but not directly by the renderer. */
	LCF_TAGBRETHREN		= 8,

	/* These two are not chosen directly by the user, but are set by the caller
	 * depending on the mode and whether LCF_TAGBRETHREN is set.
	 */
	LCF_TAGSECTORS		= 64,
	LCF_TAGLINES		= 128
};


enum ENUM_THINGRENDERFLAGS
{
	TRF_DIMMED = 1
};



/* Globals. */
extern RGBQUAD g_rgbqPalette[];

/* Prototypes. */
BOOL InitialiseRenderer(void);
void ShutdownRenderer(void);
void LoadUserPalette(void);
void SaveUserPalette(void);
void RedrawMap(MAP *lpmap, MAP_RENDERINFO *lpmri);
void SetZoom(MAPVIEW *lpmapview, float fZoom);
void Init2D(void);
BOOL InitGL(HDC hdc);
void RendererMakeDCCurrent(HDC hdc);
GLuint LoadThingCircleTexture(GLvoid);
GLvoid ReSizeGLScene(MAPVIEW *lpmapview);
void __fastcall Render_LineF(float x1, float y1, float x2, float y2, BYTE byColour);
void __fastcall Render_SquareF(float xCentre, float yCentre, float fHalfSize, BYTE byColour);
void __fastcall Render_Rect(float x1, float y1, float x2, float y2, BYTE byColour);
void __fastcall Render_OutlineRect(float x1, float y1, float x2, float y2, BYTE byColour);
void __fastcall Render_ThingPalettedF(float xCentre, float yCentre, int iAngle, BYTE byPalColour, float fSize, float fZoom, GLuint uiThingTex);
void __fastcall Render_ThingF(float xCentre, float yCentre, int iAngle, int iColour, float fSize, float fZoom, GLuint uiThingTex);

void __fastcall Render_BoxF(int xCentre, int yCentre, int iRadius, BYTE byColour);
void __fastcall Render_DiscF(float xCentre, float yCentre, float fRadius, int iColour, GLuint uiThingTex);
void __fastcall Render_CirclePalettedF(float x, float y, float fRadius, BYTE byPalColour);
void __fastcall Render_CircleF(float xCentre, float yCentre, float fRadius, int iColour);
void __fastcall Render_BitmapF(BYTE* bitmap, int width, int height, int sx, int sy, int sw, int sh, int tx, int ty, BYTE c1, BYTE c2);
void __fastcall Render_ScaledBitmapF(BYTE* bitmap, int width, int height, int sx, int sy, int sw, int sh, int tx, int ty, BYTE c1, BYTE c2);

void ZoomToRect(MAPVIEW *lpmapview, RECT *lprc, int iBorderPercentage);
void CentreViewAt(MAPVIEW *lpmapview, int x, int y);

void Render_Printf(MAPVIEW *lpmapview, float x, float y, BYTE byColour, LPCTSTR szFormat, ...);

void __fastcall Render_LinedefLineF(int x1, int y1, int x2, int y2, BYTE byColour, int iIndicatorLength);
void __fastcall Render_LineLengthF(MAPVIEW *lpmapview, int x1, int y1, int x2, int y2, BYTE byColour);


/* RGBQUADToCOLORREF, COLORREFToRGBQUAD
 *   Convert between RGBQUADs and COLORREFs.
 */
static __inline COLORREF RGBQUADToCOLORREF(RGBQUAD rgbqColour)
{
	return RGB(rgbqColour.rgbRed, rgbqColour.rgbGreen, rgbqColour.rgbBlue);
}

static __inline RGBQUAD COLORREFToRGBQUAD(COLORREF rgbColour)
{
	RGBQUAD rgbq = {GetBValue(rgbColour), GetGValue(rgbColour), GetRValue(rgbColour), 0};
	return rgbq;
}

#define GL_COLOUR_FROM_INDEX(byColour) (glColor4ub(g_rgbqPalette[byColour].rgbRed, g_rgbqPalette[byColour].rgbGreen, g_rgbqPalette[byColour].rgbBlue, 255))
#define GL_CLEAR_COLOUR_FROM_INDEX(byColour) (glClearColor(g_rgbqPalette[byColour].rgbRed / 255.0f, g_rgbqPalette[byColour].rgbGreen / 255.0f, g_rgbqPalette[byColour].rgbBlue / 255.0f, 0.0f))


#ifdef __cplusplus
}
#endif

#endif
