/* SRB2 Workbench -- A map editor for Sonic Robo Blast 2.
 * Copyright (C) 2009 Gregor Dick
 * http://workbench.srb2.org/
 *
 * Incorporates portions of Doom Builder, (C) 2003 Pascal vd Heiden.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 * USA.
 *
 * fof.h: Header for fof.c.
 *
 * AMDG.
 */

#ifndef __SRB2B_FOF__
#define __SRB2B_FOF__

#include <windows.h>
#include <ctype.h>

#include "map.h"
#include "texture.h"
#include "config.h"
#include "editing.h"


typedef struct _FOF
{
	int			iLinedef;

	/* Cache the index of the control sector. This can, of course, be obtained
	 * from iLinedef.
	 */
	int			iControlSector;

	/* Flags and effect number. The former is redundant but helpful to have
	 * cached, except in the case of a custom FOF where it cannot be inferred.
	 */
	DWORD		dwFlags;
	short		nType;

	/* Heights. */
	short		nFloorHeight, nCeilingHeight;

	/* Texture names. */
	char		szCeiling[TEXNAME_BUFFER_LENGTH];
	char		szFloor[TEXNAME_BUFFER_LENGTH];
	char		szSidedefs[TEXNAME_BUFFER_LENGTH];

	/* Brightness. */
	short		nBrightness;

	/* Alpha for transulcent FOFs. Only significant when the FF_TRANSLUCENT bit
	 * is set in dwFlags.
	 */
	BYTE		byAlpha;

	/* Colourmap proerties. Only significant when the FEF_COLOURMAP bit is set
	 * in dwEditFlags.
	 */
	int			iColourmapLinedef;
	COLORREF	crefColourmap;
	BYTE		byColourmapAlpha;

	/* Flags used by the editor. */
	DWORD	dwEditFlags;
} FOF;


typedef struct _FOF_LIST_NODE FOF_LIST_NODE;

struct _FOF_LIST_NODE
{
	FOF				fof;
	FOF_LIST_NODE	*lpflnNext, *lpflnPrev;
};


typedef struct _FOF_LIST
{
	FOF_LIST_NODE flnHeader, flnFooter;
} FOF_LIST;


typedef struct _FOF_FLAGS
{
	DWORD	dwMask;
	DWORD	dwExists;
	DWORD	dwTranslucent;
} FOF_FLAGS;


typedef enum _ENUM_CSEC_DIRECTION
{
	CSD_LEFT,
	CSD_RIGHT,
	CSD_UP,
	CSD_DOWN
} ENUM_CSEC_DIRECTION;

typedef struct _CONTROLSECPOS
{
	int						xCentre, yCentre;
	ENUM_CSEC_DIRECTION		csd;
} CONTROLSECPOS;


/* FOF flag flags! For FOF_FLAGS::dwMask. */
enum ENUM_FOF_FLAG_MASK
{
	FFM_EXISTS		= 1,
	FFM_TRANSLUCENT	= 2
};

enum ENUM_FOF_EDIT_FLAGS
{
	FEF_DELETE		= 1,
	FEF_COLOURMAP	= 2,
	FEF_TRANSLUCENT	= 4
};


#define FOFCFG_FF_EXISTS		TEXT("FF_EXISTS")
#define FOFCFG_FF_TRANSLUCENT	TEXT("FF_TRANSLUCENT")

#define FOFCFG_EFFECT_CUSTOM	TEXT("custom")
#define FOFCFG_EFFECT_FLAGS		TEXT("flags")
#define FOFCFG_EFFECT_AIRBOB	TEXT("airbob")
#define FOFCFG_EFFECT_RISING	TEXT("rising")

#define FOFCFG_FLAG_VALUE		TEXT("value")
#define FOFCFG_FLAG_TEXT		TEXT("text")

#define FOFCFG_DEFAULT			TEXT("default")
#define FOFCFG_COLOURMAP		TEXT("colourmap")
#define FOFCFG_DEFAULTCUSTOM	TEXT("defaultcustom")


#define DEFAULT_FOF_HEIGHT		128

#define COLOURMAP_ALPHA_MIN		0
#define COLOURMAP_ALPHA_MAX		35
#define TRANS_ALPHA_MIN			0
#define TRANS_ALPHA_MAX			255

#define CCH_COLOURMAP_COLOUR	6

#define DEFAULT_COLOURMAP_COLOUR	0xFFFFFF
#define DEFAULT_COLOURMAP_ALPHA		COLOURMAP_ALPHA_MAX



BOOL GetFOFProperties(MAP *lpmap, int iLinedef, CONFIG *lpcfgFOFTypes, FOF_FLAGS *lpff, FOF *lpfof, int iColourmap);
void GetSpecialFOFFlagValues(CONFIG *lpcfgFOFFlags, FOF_FLAGS *lpff);
FOF_LIST* AllocateFOFList(void);
void DestroyFOFList(FOF_LIST *lpfl);
FOF_LIST_NODE* AddNodeToFOFList(FOF_LIST *lpfl);
void RemoveNodeFromFOFList(FOF_LIST_NODE *lpfln);
void AddFOFsToListByTag(MAP *lpmap, short nTag, FOF_LIST *lpfl, CONFIG *lpcfgFOFTypes, FOF_FLAGS *lpff, int iColourmap);
BOOL ApplyFOFListToMap(MAP *lpmap, short nTag, FOF_LIST *lpfl, CONFIG *lpcfgFOFTypes, FOF_FLAGS *lpff, CONTROLSECPOS *lpcsp, DRAW_OPERATION *lpdrawop, CONFIG *lpcfgWadOptMap, LPCTSTR szSky, int iColourmap);
CONFIG* GetFOFEffectConfig(CONFIG *lpcfgFOFTypes, short nFOFType);
void GetDefaultFOFProperties(FOF *lpfof, CONFIG *lpcfgFOFTypes);
void GenerateColourmapString(LPTSTR szBuffer, COLORREF crefColour);
COLORREF GetColourmapFromString(LPCTSTR szColour);
int CountFOFsInList(FOF_LIST *lpfl);
FOF_LIST_NODE* AddNewDefaultFOFToList(FOF_LIST *lpfl, CONFIG *lpcfgMap, CONFIG *lpcfgWadOptMap, short nContainingFloor, short nContainingBrightness);
BOOL ApplyFOFToMap(MAP *lpmap, short nTag, FOF *lpfof, CONFIG *lpcfgFOFTypes, FOF_FLAGS *lpff, CONTROLSECPOS *lpcsp, DRAW_OPERATION *lpdrawop, CONFIG *lpcfgWadOptMap, LPCTSTR szSky, int iColourmap);
BOOL IsValidColourmap(LPCTSTR szColourmap);
BOOL ValidateFOFConfig(CONFIG *lpcfgMap);


static __inline BOOL IsFOFLinedef(MAP *lpmap, int iLinedef, CONFIG *lpcfgFOFTypes)
{
	return GetFOFProperties(lpmap, iLinedef, lpcfgFOFTypes, NULL, NULL, -1);
}

static __inline BOOL IsFOFListEmpty(FOF_LIST *lpfl)
{
	return !(lpfl->flnHeader.lpflnNext->lpflnNext);
}

static __inline BYTE ColourmapAlphaFromCharacter(char cAlpha)
{
	char cReadAlpha = isdigit(cAlpha) ? cAlpha - '0' + 'Z' - 'A' + 1: cAlpha - 'A';
	return min(COLOURMAP_ALPHA_MAX, max(COLOURMAP_ALPHA_MIN, cReadAlpha));
}

static __inline char ColourmapCharacterFromAlpha(BYTE byAlpha)
{
	if(byAlpha > COLOURMAP_ALPHA_MAX) return '0';
	return (byAlpha <= 'Z' - 'A') ? byAlpha + 'A' : byAlpha - ('Z' - 'A' + 1) + '0';
}

static __inline COLORREF ColourmapColourToCOLORREF(DWORD dwColour)
{
	return ((dwColour & 0xFF) << 16) | (dwColour & 0xFF00) | ((dwColour & 0xFF0000) >> 16);
}

/* It's its own inverse. */
#define COLORREFToColourmapColour(dwColour) ((DWORD)ColourmapColourToCOLORREF((COLORREF)dwColour))


#endif
