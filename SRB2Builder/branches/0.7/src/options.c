/* SRB2 Workbench -- A map editor for Sonic Robo Blast 2.
 * Copyright (C) 2009 Gregor Dick
 * http://workbench.srb2.org/
 *
 * Incorporates portions of Doom Builder, (C) 2003 Pascal vd Heiden.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 * USA.
 *
 * options.c: Routines for storing and retrieving the user's preferences.
 *
 * AMDG.
 */

#include <windows.h>
#include <string.h>
#include <tchar.h>
#include <shlobj.h>
#include <shlwapi.h>

#include "general.h"
#include "config.h"
#include "options.h"
#include "keyboard.h"
#include "editing.h"
#include "mapconfig.h"
#include "testing.h"
#include "wiki.h"
#include "cdlgwrapper.h"

#include "DockWnd/DockWnd.h"

#include "../res/resource.h"

/* Macros. */
#define OPTFILE_SIGNATURE	TEXT("SRB2 Workbench Configuration")
#define OPTDIR				TEXT("SRB2 Workbench")
#define OPTFILENAME			TEXT("workbench.cfg")

/* Default values that are used multiple times. */
#define OPT_DEFAULT_DEFTEXTURE	TEXT("GFZROCK")

/* The field-names for docking states. */
#define DOCKSTATECFG_DOCKTYPE		TEXT("docktype")
#define DOCKSTATECFG_VISIBLE		TEXT("visible")
#define DOCKSTATECFG_XFLOAT		TEXT("xfloat")
#define DOCKSTATECFG_YFLOAT		TEXT("yfloat")
#define DOCKSTATECFG_CXFLOAT		TEXT("cxfloat")
#define DOCKSTATECFG_CYFLOAT		TEXT("cyfloat")
#define DOCKSTATECFG_DOCKSIZE		TEXT("docksize")
#define DOCKSTATECFG_DISCRIMINANT	TEXT("discriminant")

/* (RR,GG,BB) |-> 0x00RRGGBB. */
#define SRB2BCOLOUR(r,g,b)	((BYTE)(b) | (((WORD)((BYTE)(g)) << 8)) | (((DWORD)(BYTE)(r)) << 16))


/* Globals. */
CONFIG *g_lpcfgMain = NULL;
int g_iShortcutCodes[SCK_MAX];
RENDEREROPTIONS g_rendopts;

static TCHAR g_szOptDir[MAX_PATH];


/* Static prototypes. */
static void LoadShortcutKeys(void);
static void SetShortcutKeys(void);
static void LoadRendererOptions(void);
static void SetRendererOptions(void);
static void SetMissingOptions(void);
static void SetDefaultPalette(void);
static void SetDefaultTextures(void);
static void SetDefaultSector(void);
static void SetDefaultNodebuilderOptions(void);
static void SetDefaultWikiOptions(void);
static void SetDefaultWindowOptions(void);
static void UntaintOptions(void);
static void RemoveStaleMRUEntries(void);


/* LoadMainConfigurationFile
 *   Loads the main settings file and reads the settings into their appropriate
 *   places.
 *
 * Parameters:
 *   None.
 *
 * Return value: BOOL
 *   TRUE on success; FALSE on failure.
 *
 * Remarks:
 *   We also initialise the global containing the options directory here.
 */
BOOL LoadMainConfigurationFile(void)
{
	LPTSTR szType;
	int iLen;
	BOOL bRet;

	/* Save working directory. */
	TCHAR tc;
	DWORD cchOldDir = GetCurrentDirectory(1, &tc);
	LPTSTR szOldDir = ProcHeapAlloc(cchOldDir * sizeof(TCHAR));
	GetCurrentDirectory(cchOldDir, szOldDir);

	/* Initialise the global variable to contain the config directory. */
	SHGetSpecialFolderPath(NULL, g_szOptDir, CSIDL_APPDATA, TRUE);
	PathAppend(g_szOptDir, OPTDIR);

	/* If the directory doesn't exist, try to create it. */
	if(GetFileAttributes(g_szOptDir) == INVALID_FILE_ATTRIBUTES)
		CreateDirectory(g_szOptDir, NULL);

	/* Load file. */
	bRet = SetCurrentDirectory(g_szOptDir);
	g_lpcfgMain = ConfigLoad(OPTFILENAME);

	/* Change back to our old wd. */
	SetCurrentDirectory(szOldDir);
	ProcHeapFree(szOldDir);

	/* Bad options directory? */
	if(!bRet) return FALSE;

	if(!g_lpcfgMain)
	{
		/* Make a new config file. */
		g_lpcfgMain = ConfigCreate();
		ConfigSetString(g_lpcfgMain, OPT_TYPE, OPTFILE_SIGNATURE);

		/* Defaults are set later. */
	}
	else
	{
		iLen = ConfigGetStringLength(g_lpcfgMain, OPT_TYPE);

		if(iLen <= 0)
		{
			UnloadMainConfigurationFile();
			return FALSE;
		}

		szType = ProcHeapAlloc((iLen + 1) * sizeof(TCHAR));
		ConfigGetString(g_lpcfgMain, OPT_TYPE, szType, iLen + 1);

		if(_tcsicmp(szType, OPTFILE_SIGNATURE))
		{
			UnloadMainConfigurationFile();
			ProcHeapFree(szType);
			return FALSE;
		}

		ProcHeapFree(szType);
	}

	/* Set any missing options. */
	SetMissingOptions();

	/* Fix anything dangerous. */
	UntaintOptions();

	/* Expunge any stale MRU entries. */
	RemoveStaleMRUEntries();

	/* Copies from tree into other storage, for efficiency. */
	LoadShortcutKeys();
	LoadRendererOptions();

	return TRUE;
}


/* UnloadMainConfigurationFile
 *   Cleans up the settings structures.
 *
 * Parameters:
 *   None.
 *
 * Return value: None.
 */
void UnloadMainConfigurationFile(void)
{
	if(g_lpcfgMain)
	{
		TCHAR tc;
		DWORD cchOldDir;
		LPTSTR szOldDir;

		/* Copy settings back into the tree. */
		SetShortcutKeys();
		SetRendererOptions();

		/* Save working directory. */
		cchOldDir = GetCurrentDirectory(1, &tc);
		szOldDir = ProcHeapAlloc(cchOldDir * sizeof(TCHAR));
		GetCurrentDirectory(cchOldDir, szOldDir);

		/* Save. */
		SetCurrentDirectory(g_szOptDir);
		if(ConfigWrite(g_lpcfgMain, OPTFILENAME))
			MessageBoxFromStringTable(NULL, IDS_ERROR_WRITECONFIG, MB_ICONERROR);

		/* Change back to our old wd. */
		SetCurrentDirectory(szOldDir);
		ProcHeapFree(szOldDir);

		ConfigDestroy(g_lpcfgMain);
	}
}


/* LoadShortcutKeys, SetShortcutKeys
 *   Copies shortcut key settings between the tree and the array.
 *
 * Parameters:
 *   None.
 *
 * Return value: None.
 */
static void LoadShortcutKeys(void)
{
	CONFIG *lpcfgShortcuts = ConfigGetSubsection(g_lpcfgMain, OPT_SHORTCUTS);

	g_iShortcutCodes[SCK_EDITQUICKMOVE] = ConfigGetInteger(lpcfgShortcuts, TEXT("editquickmove"));
	g_iShortcutCodes[SCK_ZOOMIN] = ConfigGetInteger(lpcfgShortcuts, TEXT("zoomin"));
	g_iShortcutCodes[SCK_ZOOMOUT] = ConfigGetInteger(lpcfgShortcuts, TEXT("zoomout"));
	g_iShortcutCodes[SCK_CENTREVIEW] = ConfigGetInteger(lpcfgShortcuts, TEXT("editcenterview"));
	g_iShortcutCodes[SCK_EDITMOVE] = ConfigGetInteger(lpcfgShortcuts, TEXT("editmove"));
	g_iShortcutCodes[SCK_EDITANY] = ConfigGetInteger(lpcfgShortcuts, TEXT("editany"));
	g_iShortcutCodes[SCK_EDITLINES] = ConfigGetInteger(lpcfgShortcuts, TEXT("editlines"));
	g_iShortcutCodes[SCK_EDITSECTORS] = ConfigGetInteger(lpcfgShortcuts, TEXT("editsectors"));
	g_iShortcutCodes[SCK_EDITVERTICES] = ConfigGetInteger(lpcfgShortcuts, TEXT("editvertices"));
	g_iShortcutCodes[SCK_EDITTHINGS] = ConfigGetInteger(lpcfgShortcuts, TEXT("editthings"));
	g_iShortcutCodes[SCK_EDIT3D] = ConfigGetInteger(lpcfgShortcuts, TEXT("edit3d"));
	g_iShortcutCodes[SCK_EDITSNAPTOGRID] = ConfigGetInteger(lpcfgShortcuts, TEXT("togglesnap"));
	g_iShortcutCodes[SCK_FLIPLINEDEFS] = ConfigGetInteger(lpcfgShortcuts, TEXT("fliplinedefs"));
	g_iShortcutCodes[SCK_FLIPSIDEDEFS] = ConfigGetInteger(lpcfgShortcuts, TEXT("flipsidedefs"));
	g_iShortcutCodes[SCK_SPLITLINEDEFS] = ConfigGetInteger(lpcfgShortcuts, TEXT("splitlinedefs"));
	g_iShortcutCodes[SCK_JOINSECTORS] = ConfigGetInteger(lpcfgShortcuts, TEXT("joinsector"));
	g_iShortcutCodes[SCK_MERGESECTORS] = ConfigGetInteger(lpcfgShortcuts, TEXT("mergesector"));
	g_iShortcutCodes[SCK_UNDO] = ConfigGetInteger(lpcfgShortcuts, TEXT("editundo"));
	g_iShortcutCodes[SCK_REDO] = ConfigGetInteger(lpcfgShortcuts, TEXT("editredo"));
	g_iShortcutCodes[SCK_FLIPHORIZ] = ConfigGetInteger(lpcfgShortcuts, TEXT("editfliph"));
	g_iShortcutCodes[SCK_FLIPVERT] = ConfigGetInteger(lpcfgShortcuts, TEXT("editflipv"));
	g_iShortcutCodes[SCK_COPY] = ConfigGetInteger(lpcfgShortcuts, TEXT("editcopy"));
	g_iShortcutCodes[SCK_PASTE] = ConfigGetInteger(lpcfgShortcuts, TEXT("editpaste"));
	g_iShortcutCodes[SCK_SAVEAS] = ConfigGetInteger(lpcfgShortcuts, TEXT("filesaveas"));
	g_iShortcutCodes[SCK_INCFLOOR] = ConfigGetInteger(lpcfgShortcuts, TEXT("incfloor"));
	g_iShortcutCodes[SCK_DECFLOOR] = ConfigGetInteger(lpcfgShortcuts, TEXT("decfloor"));
	g_iShortcutCodes[SCK_INCCEIL] = ConfigGetInteger(lpcfgShortcuts, TEXT("incceil"));
	g_iShortcutCodes[SCK_DECCEIL] = ConfigGetInteger(lpcfgShortcuts, TEXT("decceil"));
	g_iShortcutCodes[SCK_INCLIGHT] = ConfigGetInteger(lpcfgShortcuts, TEXT("inclight"));
	g_iShortcutCodes[SCK_DECLIGHT] = ConfigGetInteger(lpcfgShortcuts, TEXT("declight"));
	g_iShortcutCodes[SCK_INCTHINGZ] = ConfigGetInteger(lpcfgShortcuts, TEXT("incthingz"));
	g_iShortcutCodes[SCK_DECTHINGZ] = ConfigGetInteger(lpcfgShortcuts, TEXT("decthingz"));
	g_iShortcutCodes[SCK_SNAPSELECTION] = ConfigGetInteger(lpcfgShortcuts, TEXT("snapselection"));
	g_iShortcutCodes[SCK_GRADIENTFLOORS] = ConfigGetInteger(lpcfgShortcuts, TEXT("gradientfloors"));
	g_iShortcutCodes[SCK_GRADIENTCEILINGS] = ConfigGetInteger(lpcfgShortcuts, TEXT("gradientceilings"));
	g_iShortcutCodes[SCK_GRADIENTBRIGHTNESS] = ConfigGetInteger(lpcfgShortcuts, TEXT("gradientbrightness"));
	g_iShortcutCodes[SCK_INSERT] = ConfigGetInteger(lpcfgShortcuts, TEXT("drawsector"));
	g_iShortcutCodes[SCK_DCKINSERT] = ConfigGetInteger(lpcfgShortcuts, TEXT("drawrect"));
	g_iShortcutCodes[SCK_EDITCUT] = ConfigGetInteger(lpcfgShortcuts, TEXT("editcut"));
	g_iShortcutCodes[SCK_EDITDELETE] = ConfigGetInteger(lpcfgShortcuts, TEXT("editdelete"));
	g_iShortcutCodes[SCK_CANCEL] = ConfigGetInteger(lpcfgShortcuts, TEXT("cancel"));
	g_iShortcutCodes[SCK_GRADIENTTHINGZ] = ConfigGetInteger(lpcfgShortcuts, TEXT("gradientthingz"));
	g_iShortcutCodes[SCK_ROTATE] = ConfigGetInteger(lpcfgShortcuts, TEXT("editrotate"));
	g_iShortcutCodes[SCK_RESIZE] = ConfigGetInteger(lpcfgShortcuts, TEXT("editresize"));
	g_iShortcutCodes[SCK_THINGROTACW] = ConfigGetInteger(lpcfgShortcuts, TEXT("thingrotateacw"));
	g_iShortcutCodes[SCK_THINGROTCW] = ConfigGetInteger(lpcfgShortcuts, TEXT("thingrotatecw"));
	g_iShortcutCodes[SCK_SELECTALL] = ConfigGetInteger(lpcfgShortcuts, TEXT("editselectall"));
	g_iShortcutCodes[SCK_SELECTNONE] = ConfigGetInteger(lpcfgShortcuts, TEXT("editselectnone"));
	g_iShortcutCodes[SCK_INVERTSELECTION] = ConfigGetInteger(lpcfgShortcuts, TEXT("editselectinvert"));
	g_iShortcutCodes[SCK_COPYPROPS] = ConfigGetInteger(lpcfgShortcuts, TEXT("copyprops"));
	g_iShortcutCodes[SCK_PASTEPROPS] = ConfigGetInteger(lpcfgShortcuts, TEXT("pasteprops"));
	g_iShortcutCodes[SCK_GRIDINC] = ConfigGetInteger(lpcfgShortcuts, TEXT("gridinc"));
	g_iShortcutCodes[SCK_GRIDDEC] = ConfigGetInteger(lpcfgShortcuts, TEXT("griddec"));
	g_iShortcutCodes[SCK_FIND] = ConfigGetInteger(lpcfgShortcuts, TEXT("editfind"));
	g_iShortcutCodes[SCK_REPLACE] = ConfigGetInteger(lpcfgShortcuts, TEXT("editreplace"));
	g_iShortcutCodes[SCK_NEW] = ConfigGetInteger(lpcfgShortcuts, TEXT("filenew"));
	g_iShortcutCodes[SCK_OPEN] = ConfigGetInteger(lpcfgShortcuts, TEXT("fileopen"));
	g_iShortcutCodes[SCK_SAVE] = ConfigGetInteger(lpcfgShortcuts, TEXT("filesave"));
	g_iShortcutCodes[SCK_MAPOPTIONS] = ConfigGetInteger(lpcfgShortcuts, TEXT("editoptions"));
	g_iShortcutCodes[SCK_MOUSEROTATE] = ConfigGetInteger(lpcfgShortcuts, TEXT("thingsmouserotate"));
	g_iShortcutCodes[SCK_STITCHVERTICES] = ConfigGetInteger(lpcfgShortcuts, TEXT("stitchvertices"));
	g_iShortcutCodes[SCK_SCROLL_UP] = ConfigGetInteger(lpcfgShortcuts, TEXT("scrollup"));
	g_iShortcutCodes[SCK_SCROLL_DOWN] = ConfigGetInteger(lpcfgShortcuts, TEXT("scrolldown"));
	g_iShortcutCodes[SCK_SCROLL_LEFT] = ConfigGetInteger(lpcfgShortcuts, TEXT("scrollleft"));
	g_iShortcutCodes[SCK_SCROLL_RIGHT] = ConfigGetInteger(lpcfgShortcuts, TEXT("scrollright"));
	g_iShortcutCodes[SCK_TEST] = ConfigGetInteger(lpcfgShortcuts, TEXT("filetest"));
	g_iShortcutCodes[SCK_QUICKTEST] = ConfigGetInteger(lpcfgShortcuts, TEXT("quicktest"));
	g_iShortcutCodes[SCK_IDENTSECTORS] = ConfigGetInteger(lpcfgShortcuts, TEXT("sectorsident"));
	g_iShortcutCodes[SCK_CENTREVIEWSEL] = ConfigGetInteger(lpcfgShortcuts, TEXT("editcenterviewsel"));
	g_iShortcutCodes[SCK_MAKESINGLE] = ConfigGetInteger(lpcfgShortcuts, TEXT("linesmakesingle"));
	g_iShortcutCodes[SCK_MAKEDOUBLE] = ConfigGetInteger(lpcfgShortcuts, TEXT("linesmakedouble"));
	g_iShortcutCodes[SCK_REMOVEINTERIOR] = ConfigGetInteger(lpcfgShortcuts, TEXT("sectorsremoveinterior"));
	g_iShortcutCodes[SCK_RESOLVESEC] = ConfigGetInteger(lpcfgShortcuts, TEXT("sectorsresolve"));
	g_iShortcutCodes[SCK_BACKTRACKDRAW] = ConfigGetInteger(lpcfgShortcuts, TEXT("backtrackdraw"));
	g_iShortcutCodes[SCK_SELECTTAGDUAL] = ConfigGetInteger(lpcfgShortcuts, TEXT("selecttagdual"));
	g_iShortcutCodes[SCK_EDITFOF] = ConfigGetInteger(lpcfgShortcuts, TEXT("editfof"));
	g_iShortcutCodes[SCK_QUICKDRAG] = ConfigGetInteger(lpcfgShortcuts, TEXT("quickdrag"));
	g_iShortcutCodes[SCK_QUICKFOF] = ConfigGetInteger(lpcfgShortcuts, TEXT("quickfof"));
	g_iShortcutCodes[SCK_OPTIONS] = ConfigGetInteger(lpcfgShortcuts, TEXT("options"));
	g_iShortcutCodes[SCK_ERRORCHECKER] = ConfigGetInteger(lpcfgShortcuts, TEXT("errorchecker"));
	g_iShortcutCodes[SCK_PROPERTIES] = ConfigGetInteger(lpcfgShortcuts, TEXT("properties"));
	g_iShortcutCodes[SCK_TOGGLEVXSNAP] = ConfigGetInteger(lpcfgShortcuts, TEXT("togglevxsnap"));
	g_iShortcutCodes[SCK_TOGGLELINESNAP] = ConfigGetInteger(lpcfgShortcuts, TEXT("togglelinesnap"));
	g_iShortcutCodes[SCK_TOGGLEAUTOSTITCH] = ConfigGetInteger(lpcfgShortcuts, TEXT("toggleautostitch"));
	g_iShortcutCodes[SCK_EDITHEADER] = ConfigGetInteger(lpcfgShortcuts, TEXT("editheader"));
	g_iShortcutCodes[SCK_FIXMISSTEX] = ConfigGetInteger(lpcfgShortcuts, TEXT("fixmisstex"));
	g_iShortcutCodes[SCK_AUTOALIGN] = ConfigGetInteger(lpcfgShortcuts, TEXT("autoalign"));
	g_iShortcutCodes[SCK_DELUNUSEDVX] = ConfigGetInteger(lpcfgShortcuts, TEXT("delunusedvx"));
	g_iShortcutCodes[SCK_LOCKDRAGGING] = ConfigGetInteger(lpcfgShortcuts, TEXT("editlock"));
	/* _SCK_ */

	/* Create the accelerator table. */
	UpdateAcceleratorFromOptions();
}

static void SetShortcutKeys(void)
{
	CONFIG *lpcfgShortcuts = ConfigGetSubsection(g_lpcfgMain, OPT_SHORTCUTS);

	ConfigSetInteger(lpcfgShortcuts, TEXT("editquickmove"), g_iShortcutCodes[SCK_EDITQUICKMOVE]);
	ConfigSetInteger(lpcfgShortcuts, TEXT("zoomin"), g_iShortcutCodes[SCK_ZOOMIN]);
	ConfigSetInteger(lpcfgShortcuts, TEXT("zoomout"), g_iShortcutCodes[SCK_ZOOMOUT]);
	ConfigSetInteger(lpcfgShortcuts, TEXT("editcenterview"), g_iShortcutCodes[SCK_CENTREVIEW]);
	ConfigSetInteger(lpcfgShortcuts, TEXT("editmove"), g_iShortcutCodes[SCK_EDITMOVE]);
	ConfigSetInteger(lpcfgShortcuts, TEXT("editany"), g_iShortcutCodes[SCK_EDITANY]);
	ConfigSetInteger(lpcfgShortcuts, TEXT("editlines"), g_iShortcutCodes[SCK_EDITLINES]);
	ConfigSetInteger(lpcfgShortcuts, TEXT("editsectors"), g_iShortcutCodes[SCK_EDITSECTORS]);
	ConfigSetInteger(lpcfgShortcuts, TEXT("editvertices"), g_iShortcutCodes[SCK_EDITVERTICES]);
	ConfigSetInteger(lpcfgShortcuts, TEXT("editthings"), g_iShortcutCodes[SCK_EDITTHINGS]);
	ConfigSetInteger(lpcfgShortcuts, TEXT("edit3d"), g_iShortcutCodes[SCK_EDIT3D]);
	ConfigSetInteger(lpcfgShortcuts, TEXT("togglesnap"), g_iShortcutCodes[SCK_EDITSNAPTOGRID]);
	ConfigSetInteger(lpcfgShortcuts, TEXT("fliplinedefs"), g_iShortcutCodes[SCK_FLIPLINEDEFS]);
	ConfigSetInteger(lpcfgShortcuts, TEXT("flipsidedefs"), g_iShortcutCodes[SCK_FLIPSIDEDEFS]);
	ConfigSetInteger(lpcfgShortcuts, TEXT("splitlinedefs"), g_iShortcutCodes[SCK_SPLITLINEDEFS]);
	ConfigSetInteger(lpcfgShortcuts, TEXT("joinsector"), g_iShortcutCodes[SCK_JOINSECTORS]);
	ConfigSetInteger(lpcfgShortcuts, TEXT("mergesector"), g_iShortcutCodes[SCK_MERGESECTORS]);
	ConfigSetInteger(lpcfgShortcuts, TEXT("editundo"), g_iShortcutCodes[SCK_UNDO]);
	ConfigSetInteger(lpcfgShortcuts, TEXT("editredo"), g_iShortcutCodes[SCK_REDO]);
	ConfigSetInteger(lpcfgShortcuts, TEXT("editfliph"), g_iShortcutCodes[SCK_FLIPHORIZ]);
	ConfigSetInteger(lpcfgShortcuts, TEXT("editflipv"), g_iShortcutCodes[SCK_FLIPVERT]);
	ConfigSetInteger(lpcfgShortcuts, TEXT("editcopy"), g_iShortcutCodes[SCK_COPY]);
	ConfigSetInteger(lpcfgShortcuts, TEXT("editpaste"), g_iShortcutCodes[SCK_PASTE]);
	ConfigSetInteger(lpcfgShortcuts, TEXT("filesaveas"), g_iShortcutCodes[SCK_SAVEAS]);
	ConfigSetInteger(lpcfgShortcuts, TEXT("incfloor"), g_iShortcutCodes[SCK_INCFLOOR]);
	ConfigSetInteger(lpcfgShortcuts, TEXT("decfloor"), g_iShortcutCodes[SCK_DECFLOOR]);
	ConfigSetInteger(lpcfgShortcuts, TEXT("incceil"), g_iShortcutCodes[SCK_INCCEIL]);
	ConfigSetInteger(lpcfgShortcuts, TEXT("decceil"), g_iShortcutCodes[SCK_DECCEIL]);
	ConfigSetInteger(lpcfgShortcuts, TEXT("inclight"), g_iShortcutCodes[SCK_INCLIGHT]);
	ConfigSetInteger(lpcfgShortcuts, TEXT("declight"), g_iShortcutCodes[SCK_DECLIGHT]);
	ConfigSetInteger(lpcfgShortcuts, TEXT("incthingz"), g_iShortcutCodes[SCK_INCTHINGZ]);
	ConfigSetInteger(lpcfgShortcuts, TEXT("decthingz"), g_iShortcutCodes[SCK_DECTHINGZ]);
	ConfigSetInteger(lpcfgShortcuts, TEXT("snapselection"), g_iShortcutCodes[SCK_SNAPSELECTION]);
	ConfigSetInteger(lpcfgShortcuts, TEXT("gradientfloors"), g_iShortcutCodes[SCK_GRADIENTFLOORS]);
	ConfigSetInteger(lpcfgShortcuts, TEXT("gradientceilings"), g_iShortcutCodes[SCK_GRADIENTCEILINGS]);
	ConfigSetInteger(lpcfgShortcuts, TEXT("gradientbrightness"), g_iShortcutCodes[SCK_GRADIENTBRIGHTNESS]);
	ConfigSetInteger(lpcfgShortcuts, TEXT("drawsector"), g_iShortcutCodes[SCK_INSERT]);
	ConfigSetInteger(lpcfgShortcuts, TEXT("drawrect"), g_iShortcutCodes[SCK_DCKINSERT]);
	ConfigSetInteger(lpcfgShortcuts, TEXT("editcut"), g_iShortcutCodes[SCK_EDITCUT]);
	ConfigSetInteger(lpcfgShortcuts, TEXT("editdelete"), g_iShortcutCodes[SCK_EDITDELETE]);
	ConfigSetInteger(lpcfgShortcuts, TEXT("cancel"), g_iShortcutCodes[SCK_CANCEL]);
	ConfigSetInteger(lpcfgShortcuts, TEXT("gradientthingz"), g_iShortcutCodes[SCK_GRADIENTTHINGZ]);
	ConfigSetInteger(lpcfgShortcuts, TEXT("editrotate"), g_iShortcutCodes[SCK_ROTATE]);
	ConfigSetInteger(lpcfgShortcuts, TEXT("editresize"), g_iShortcutCodes[SCK_RESIZE]);
	ConfigSetInteger(lpcfgShortcuts, TEXT("thingrotateacw"), g_iShortcutCodes[SCK_THINGROTACW]);
	ConfigSetInteger(lpcfgShortcuts, TEXT("thingrotatecw"), g_iShortcutCodes[SCK_THINGROTCW]);
	ConfigSetInteger(lpcfgShortcuts, TEXT("editselectall"), g_iShortcutCodes[SCK_SELECTALL]);
	ConfigSetInteger(lpcfgShortcuts, TEXT("editselectnone"), g_iShortcutCodes[SCK_SELECTNONE]);
	ConfigSetInteger(lpcfgShortcuts, TEXT("editselectinvert"), g_iShortcutCodes[SCK_INVERTSELECTION]);
	ConfigSetInteger(lpcfgShortcuts, TEXT("copyprops"), g_iShortcutCodes[SCK_COPYPROPS]);
	ConfigSetInteger(lpcfgShortcuts, TEXT("pasteprops"), g_iShortcutCodes[SCK_PASTEPROPS]);
	ConfigSetInteger(lpcfgShortcuts, TEXT("gridinc"), g_iShortcutCodes[SCK_GRIDINC]);
	ConfigSetInteger(lpcfgShortcuts, TEXT("griddec"), g_iShortcutCodes[SCK_GRIDDEC]);
	ConfigSetInteger(lpcfgShortcuts, TEXT("editfind"), g_iShortcutCodes[SCK_FIND]);
	ConfigSetInteger(lpcfgShortcuts, TEXT("editreplace"), g_iShortcutCodes[SCK_REPLACE]);
	ConfigSetInteger(lpcfgShortcuts, TEXT("filenew"), g_iShortcutCodes[SCK_NEW]);
	ConfigSetInteger(lpcfgShortcuts, TEXT("fileopen"), g_iShortcutCodes[SCK_OPEN]);
	ConfigSetInteger(lpcfgShortcuts, TEXT("filesave"), g_iShortcutCodes[SCK_SAVE]);
	ConfigSetInteger(lpcfgShortcuts, TEXT("editoptions"), g_iShortcutCodes[SCK_MAPOPTIONS]);
	ConfigSetInteger(lpcfgShortcuts, TEXT("thingsmouserotate"), g_iShortcutCodes[SCK_MOUSEROTATE]);
	ConfigSetInteger(lpcfgShortcuts, TEXT("stitchvertices"), g_iShortcutCodes[SCK_STITCHVERTICES]);
	ConfigSetInteger(lpcfgShortcuts, TEXT("scrollup"), g_iShortcutCodes[SCK_SCROLL_UP]);
	ConfigSetInteger(lpcfgShortcuts, TEXT("scrolldown"), g_iShortcutCodes[SCK_SCROLL_DOWN]);
	ConfigSetInteger(lpcfgShortcuts, TEXT("scrollleft"), g_iShortcutCodes[SCK_SCROLL_LEFT]);
	ConfigSetInteger(lpcfgShortcuts, TEXT("scrollright"), g_iShortcutCodes[SCK_SCROLL_RIGHT]);
	ConfigSetInteger(lpcfgShortcuts, TEXT("filetest"), g_iShortcutCodes[SCK_TEST]);
	ConfigSetInteger(lpcfgShortcuts, TEXT("quicktest"), g_iShortcutCodes[SCK_QUICKTEST]);
	ConfigSetInteger(lpcfgShortcuts, TEXT("sectorsident"), g_iShortcutCodes[SCK_IDENTSECTORS]);
	ConfigSetInteger(lpcfgShortcuts, TEXT("editcenterviewsel"), g_iShortcutCodes[SCK_CENTREVIEWSEL]);
	ConfigSetInteger(lpcfgShortcuts, TEXT("linesmakesingle"), g_iShortcutCodes[SCK_MAKESINGLE]);
	ConfigSetInteger(lpcfgShortcuts, TEXT("linesmakedouble"), g_iShortcutCodes[SCK_MAKEDOUBLE]);
	ConfigSetInteger(lpcfgShortcuts, TEXT("sectorsremoveinterior"), g_iShortcutCodes[SCK_REMOVEINTERIOR]);
	ConfigSetInteger(lpcfgShortcuts, TEXT("sectorsresolve"), g_iShortcutCodes[SCK_RESOLVESEC]);
	ConfigSetInteger(lpcfgShortcuts, TEXT("backtrackdraw"), g_iShortcutCodes[SCK_BACKTRACKDRAW]);
	ConfigSetInteger(lpcfgShortcuts, TEXT("selecttagdual"), g_iShortcutCodes[SCK_SELECTTAGDUAL]);
	ConfigSetInteger(lpcfgShortcuts, TEXT("editfof"), g_iShortcutCodes[SCK_EDITFOF]);
	ConfigSetInteger(lpcfgShortcuts, TEXT("quickdrag"), g_iShortcutCodes[SCK_QUICKDRAG]);
	ConfigSetInteger(lpcfgShortcuts, TEXT("quickfof"), g_iShortcutCodes[SCK_QUICKFOF]);
	ConfigSetInteger(lpcfgShortcuts, TEXT("options"), g_iShortcutCodes[SCK_OPTIONS]);
	ConfigSetInteger(lpcfgShortcuts, TEXT("errorchecker"), g_iShortcutCodes[SCK_ERRORCHECKER]);
	ConfigSetInteger(lpcfgShortcuts, TEXT("properties"), g_iShortcutCodes[SCK_PROPERTIES]);
	ConfigSetInteger(lpcfgShortcuts, TEXT("togglevxsnap"), g_iShortcutCodes[SCK_TOGGLEVXSNAP]);
	ConfigSetInteger(lpcfgShortcuts, TEXT("togglelinesnap"), g_iShortcutCodes[SCK_TOGGLELINESNAP]);
	ConfigSetInteger(lpcfgShortcuts, TEXT("toggleautostitch"), g_iShortcutCodes[SCK_TOGGLEAUTOSTITCH]);
	ConfigSetInteger(lpcfgShortcuts, TEXT("editheader"), g_iShortcutCodes[SCK_EDITHEADER]);
	ConfigSetInteger(lpcfgShortcuts, TEXT("fixmisstex"), g_iShortcutCodes[SCK_FIXMISSTEX]);
	ConfigSetInteger(lpcfgShortcuts, TEXT("autoalign"), g_iShortcutCodes[SCK_AUTOALIGN]);
	ConfigSetInteger(lpcfgShortcuts, TEXT("delunusedvx"), g_iShortcutCodes[SCK_DELUNUSEDVX]);
	ConfigSetInteger(lpcfgShortcuts, TEXT("editlock"), g_iShortcutCodes[SCK_LOCKDRAGGING]);
	/* _SCK_ */
}


/* LoadRendererOptions, SetRendererOptions
 *   Copies renderer settings between the tree and the structure.
 *
 * Parameters:
 *   None.
 *
 * Return value: None.
 */
static void LoadRendererOptions(void)
{
	g_rendopts.iIndicatorSize = ConfigGetInteger(g_lpcfgMain, OPT_INDICATORSIZE);
	g_rendopts.iVertexSize = ConfigGetInteger(g_lpcfgMain, OPT_VERTEXSIZE);
	g_rendopts.bVerticesInLinesMode = ConfigGetInteger(g_lpcfgMain, OPT_LINEMODEVERTICES);
	g_rendopts.bVerticesInSectorsMode = ConfigGetInteger(g_lpcfgMain, OPT_SECMODEVERTICES);
}

static void SetRendererOptions(void)
{
	ConfigSetInteger(g_lpcfgMain, OPT_INDICATORSIZE, g_rendopts.iIndicatorSize);
	ConfigSetInteger(g_lpcfgMain, OPT_VERTEXSIZE, g_rendopts.iVertexSize);
	ConfigSetInteger(g_lpcfgMain, OPT_LINEMODEVERTICES, g_rendopts.bVerticesInLinesMode);
	ConfigSetInteger(g_lpcfgMain, OPT_SECMODEVERTICES, g_rendopts.bVerticesInSectorsMode);
}


/* MRUAdd
 *   Adds a file to the top of the MRU list.
 *
 * Parameters:
 *   LPCTSTR	szFilename	Filename to add.
 *
 * Return value: None.
 *
 * Remarks:
 *   The filename need not be the full path: it is expanded by the function. If
 *   the file is already in the list, it is moved to first place, and not
 *   duplicated.
 */
void MRUAdd(LPCTSTR szFilename)
{
	LPTSTR szFullFilename;
	UINT cchFullFilename;
	LPCTSTR szIndices[MRUENTRIES] = {TEXT("1"), TEXT("2"), TEXT("3"), TEXT("4"), TEXT("5"), TEXT("6"), TEXT("7"), TEXT("8"), TEXT("9"), TEXT("10")};
	CONFIG *lpcfgRecent = ConfigGetSubsection(g_lpcfgMain, OPT_RECENT);
	int i, iExistingPos;

	cchFullFilename = GetFullPathName(szFilename, 0, TEXT(""), NULL);
	szFullFilename = ProcHeapAlloc(cchFullFilename * sizeof(TCHAR));
	GetFullPathName(szFilename, cchFullFilename, szFullFilename, NULL);

	/* Check whether we're already in the list, and at which point. Note that we
	 * don't need to check the last position, as it being there is equivalent to
	 * it not being there at all.
	 */
	for(iExistingPos = 0; iExistingPos < (int)(NUM_ELEMENTS(szIndices) - 1) && ConfigNodeExists(lpcfgRecent, szIndices[iExistingPos]); iExistingPos++)
	{
		/* Get the filename to move. */
		UINT cchExistingFilename = ConfigGetStringLength(lpcfgRecent, szIndices[iExistingPos]) + 1;
		LPTSTR szExistingFilename = ProcHeapAlloc(cchExistingFilename * sizeof(TCHAR));
		ConfigGetString(lpcfgRecent, szIndices[iExistingPos], szExistingFilename, cchExistingFilename);

		/* Does this match with the file we're adding? */
		if(PathsReferToSameFile(szFullFilename, szExistingFilename))
		{
			/* The file was already in the MRU. */
			ProcHeapFree(szExistingFilename);
			break;
		}

		ProcHeapFree(szExistingFilename);
	}

	/* Move all the items down one. */
	for(i = iExistingPos; i > 0; i--)
	{
		/* Get the filename to move. */
		UINT cchExistingFilename = ConfigGetStringLength(lpcfgRecent, szIndices[i - 1]) + 1;
		LPTSTR szExistingFilename = ProcHeapAlloc(cchExistingFilename * sizeof(TCHAR));
		ConfigGetString(lpcfgRecent, szIndices[i - 1], szExistingFilename, cchExistingFilename);

		/* Move it down. */
		ConfigSetString(lpcfgRecent, szIndices[i], szExistingFilename);

		ProcHeapFree(szExistingFilename);
	}

	/* Add the new item. */
	ConfigSetString(lpcfgRecent, szIndices[0], szFullFilename);

	ProcHeapFree(szFullFilename);
}


/* SetMissingOptions
 *   Sets missing items in the config file to sensible defaults.
 *
 * Parameters: None.
 *
 * Return value: None.
 */
static void SetMissingOptions(void)
{
	/* Make sure we have a recent files section. */
	if(!ConfigGetSubsection(g_lpcfgMain, OPT_RECENT))
		MRUClear();

	if(!ConfigGetSubsection(g_lpcfgMain, OPT_PALETTE)) ConfigSetSubsection(g_lpcfgMain, OPT_PALETTE, ConfigCreate());
	if(!ConfigGetSubsection(g_lpcfgMain, OPT_SHORTCUTS)) ConfigSetSubsection(g_lpcfgMain, OPT_SHORTCUTS, ConfigCreate());
	if(!ConfigGetSubsection(g_lpcfgMain, OPT_GAMECONFIGS)) ConfigSetSubsection(g_lpcfgMain, OPT_GAMECONFIGS, ConfigCreate());
	if(!ConfigGetSubsection(g_lpcfgMain, OPT_DEFAULTTEX)) ConfigSetSubsection(g_lpcfgMain, OPT_DEFAULTTEX, ConfigCreate());
	if(!ConfigGetSubsection(g_lpcfgMain, OPT_DEFAULTSEC)) ConfigSetSubsection(g_lpcfgMain, OPT_DEFAULTSEC, ConfigCreate());
	if(!ConfigGetSubsection(g_lpcfgMain, OPT_WINDOW)) ConfigSetSubsection(g_lpcfgMain, OPT_WINDOW, ConfigCreate());
	if(!ConfigGetSubsection(g_lpcfgMain, OPT_NODEBUILDER)) ConfigSetSubsection(g_lpcfgMain, OPT_NODEBUILDER, ConfigCreate());
	if(!ConfigGetSubsection(g_lpcfgMain, OPT_WIKI)) ConfigSetSubsection(g_lpcfgMain, OPT_WIKI, ConfigCreate());

	SetDefaultPalette();
	SetDefaultShortcuts();
	SetDefaultTextures();
	SetDefaultSector();
	SetDefaultNodebuilderOptions();
	SetDefaultWikiOptions();
	SetDefaultWindowOptions();

	if(!ConfigNodeExists(g_lpcfgMain, OPT_VERTEXSIZE)) ConfigSetInteger(g_lpcfgMain, OPT_VERTEXSIZE, 4);
	if(!ConfigNodeExists(g_lpcfgMain, OPT_VERTEXSNAPDISTANCE)) ConfigSetInteger(g_lpcfgMain, OPT_VERTEXSNAPDISTANCE, 8);
	if(!ConfigNodeExists(g_lpcfgMain, OPT_LINESNAPDISTANCE)) ConfigSetInteger(g_lpcfgMain, OPT_LINESNAPDISTANCE, 6);
	if(!ConfigNodeExists(g_lpcfgMain, OPT_ZOOMMOUSE)) ConfigSetInteger(g_lpcfgMain, OPT_ZOOMMOUSE, TRUE);
	if(!ConfigNodeExists(g_lpcfgMain, OPT_ZOOMSPEED)) ConfigSetInteger(g_lpcfgMain, OPT_ZOOMSPEED, 200);
	if(!ConfigNodeExists(g_lpcfgMain, OPT_NOTHINGDESELECTS)) ConfigSetInteger(g_lpcfgMain, OPT_NOTHINGDESELECTS, TRUE);
	if(!ConfigNodeExists(g_lpcfgMain, OPT_DEFAULTSNAP)) ConfigSetInteger(g_lpcfgMain, OPT_DEFAULTSNAP, SF_RECTANGLE | SF_VERTICES | SF_LINES);
	if(!ConfigNodeExists(g_lpcfgMain, OPT_DETAILSANY)) ConfigSetInteger(g_lpcfgMain, OPT_DETAILSANY, TRUE);
	if(!ConfigNodeExists(g_lpcfgMain, OPT_DRAWAXES)) ConfigSetInteger(g_lpcfgMain, OPT_DRAWAXES, TRUE);
	if(!ConfigNodeExists(g_lpcfgMain, OPT_GRIDSHOW)) ConfigSetInteger(g_lpcfgMain, OPT_GRIDSHOW, TRUE);
	if(!ConfigNodeExists(g_lpcfgMain, OPT_INDICATORSIZE)) ConfigSetInteger(g_lpcfgMain, OPT_INDICATORSIZE, 5);
	if(!ConfigNodeExists(g_lpcfgMain, OPT_LINESPLITDISTANCE)) ConfigSetInteger(g_lpcfgMain, OPT_LINESPLITDISTANCE, 2);
	if(!ConfigNodeExists(g_lpcfgMain, OPT_DEFAULTSTITCH)) ConfigSetInteger(g_lpcfgMain, OPT_DEFAULTSTITCH, TRUE);
	if(!ConfigNodeExists(g_lpcfgMain, OPT_AUTOSTITCHDISTANCE)) ConfigSetInteger(g_lpcfgMain, OPT_AUTOSTITCHDISTANCE, 2);
	if(!ConfigNodeExists(g_lpcfgMain, OPT_CONTEXTMENUS)) ConfigSetInteger(g_lpcfgMain, OPT_CONTEXTMENUS, TRUE);
	if(!ConfigNodeExists(g_lpcfgMain, OPT_CXGRID)) ConfigSetInteger(g_lpcfgMain, OPT_CXGRID, 32);
	if(!ConfigNodeExists(g_lpcfgMain, OPT_CYGRID)) ConfigSetInteger(g_lpcfgMain, OPT_CYGRID, 32);
	if(!ConfigNodeExists(g_lpcfgMain, OPT_XOFFGRID)) ConfigSetInteger(g_lpcfgMain, OPT_XOFFGRID, 0);
	if(!ConfigNodeExists(g_lpcfgMain, OPT_YOFFGRID)) ConfigSetInteger(g_lpcfgMain, OPT_YOFFGRID, 0);
	if(!ConfigNodeExists(g_lpcfgMain, OPT_SCROLLSPEED)) ConfigSetInteger(g_lpcfgMain, OPT_SCROLLSPEED, 9);
	if(!ConfigNodeExists(g_lpcfgMain, OPT_USEDONLY)) ConfigSetInteger(g_lpcfgMain, OPT_USEDONLY, TRUE);
	if(!ConfigNodeExists(g_lpcfgMain, OPT_PERSISTENTOPENWAD)) ConfigSetInteger(g_lpcfgMain, OPT_PERSISTENTOPENWAD, TRUE);
	if(!ConfigNodeExists(g_lpcfgMain, OPT_AUTOCOMPLETETEX)) ConfigSetInteger(g_lpcfgMain, OPT_AUTOCOMPLETETEX, TRUE);
	if(!ConfigNodeExists(g_lpcfgMain, OPT_UNDOSTITCH)) ConfigSetInteger(g_lpcfgMain, OPT_UNDOSTITCH, TRUE);
	if(!ConfigNodeExists(g_lpcfgMain, OPT_CONTEXTCROSS)) ConfigSetInteger(g_lpcfgMain, OPT_CONTEXTCROSS, TRUE);
	if(!ConfigNodeExists(g_lpcfgMain, OPT_LINECOLOURFLAGS)) ConfigSetInteger(g_lpcfgMain, OPT_LINECOLOURFLAGS, LCF_FAUXIMPASSABLE | LCF_ZEROHEIGHT);
	if(!ConfigNodeExists(g_lpcfgMain, OPT_REMEMBERWINDOWPOS)) ConfigSetInteger(g_lpcfgMain, OPT_REMEMBERWINDOWPOS, TRUE);
	if(!ConfigNodeExists(g_lpcfgMain, OPT_LINEMODEVERTICES)) ConfigSetInteger(g_lpcfgMain, OPT_LINEMODEVERTICES, TRUE);
	if(!ConfigNodeExists(g_lpcfgMain, OPT_DRAGTHRESHOLD)) ConfigSetInteger(g_lpcfgMain, OPT_DRAGTHRESHOLD, 2);
}


/* SetDefaultPalette
 *   Sets default palette colours.
 *
 * Parameters: None.
 *
 * Return value: None.
 */
static void SetDefaultPalette(void)
{
	CONFIG *lpcfgPal = ConfigGetSubsection(g_lpcfgMain, OPT_PALETTE);

	if(!ConfigNodeExists(lpcfgPal, TEXT("CLR_BACKGROUND")))
		ConfigSetInteger(lpcfgPal, TEXT("CLR_BACKGROUND"),		SRB2BCOLOUR(0x00, 0x00, 0x00));

	if(!ConfigNodeExists(lpcfgPal, TEXT("CLR_VERTEX")))
		ConfigSetInteger(lpcfgPal, TEXT("CLR_VERTEX"),			SRB2BCOLOUR(0x00, 0x99, 0xFF));

	if(!ConfigNodeExists(lpcfgPal, TEXT("CLR_VERTEXSELECTED")))
		ConfigSetInteger(lpcfgPal, TEXT("CLR_VERTEXSELECTED"),	SRB2BCOLOUR(0xFF, 0x33, 0x11));

	if(!ConfigNodeExists(lpcfgPal, TEXT("CLR_VERTEXHIGHLIGHT")))
		ConfigSetInteger(lpcfgPal, TEXT("CLR_VERTEXHIGHLIGHT"),	SRB2BCOLOUR(0xFF, 0x99, 0x00));

	if(!ConfigNodeExists(lpcfgPal, TEXT("CLR_LINE")))
		ConfigSetInteger(lpcfgPal, TEXT("CLR_LINE"),				SRB2BCOLOUR(0xFF, 0xFF, 0xFF));

	if(!ConfigNodeExists(lpcfgPal, TEXT("CLR_LINEDOUBLE")))
		ConfigSetInteger(lpcfgPal, TEXT("CLR_LINEDOUBLE"),		SRB2BCOLOUR(0xAA, 0xAA, 0xAA));

	if(!ConfigNodeExists(lpcfgPal, TEXT("CLR_LINESPECIAL")))
		ConfigSetInteger(lpcfgPal, TEXT("CLR_LINESPECIAL"),		SRB2BCOLOUR(0xAA, 0xFF, 0xAA));

	if(!ConfigNodeExists(lpcfgPal, TEXT("CLR_LINESPECIALDOUBLE")))
		ConfigSetInteger(lpcfgPal, TEXT("CLR_LINESPECIALDOUBLE"),	SRB2BCOLOUR(0x80, 0xAA, 0x80));

	if(!ConfigNodeExists(lpcfgPal, TEXT("CLR_LINESELECTED")))
		ConfigSetInteger(lpcfgPal, TEXT("CLR_LINESELECTED"),		SRB2BCOLOUR(0xFF, 0x00, 0x00));

	if(!ConfigNodeExists(lpcfgPal, TEXT("CLR_LINEHIGHLIGHT")))
		ConfigSetInteger(lpcfgPal, TEXT("CLR_LINEHIGHLIGHT"),		SRB2BCOLOUR(0xFF, 0x99, 0x00));

	if(!ConfigNodeExists(lpcfgPal, TEXT("CLR_LINEDRAG")))
		ConfigSetInteger(lpcfgPal, TEXT("CLR_LINEDRAG"),			SRB2BCOLOUR(0xAA, 0x99, 0x33));

	if(!ConfigNodeExists(lpcfgPal, TEXT("CLR_SECTORTAG")))
		ConfigSetInteger(lpcfgPal, TEXT("CLR_SECTORTAG"),			SRB2BCOLOUR(0xFF, 0xFF, 0x80));

	if(!ConfigNodeExists(lpcfgPal, TEXT("CLR_THINGUNKNOWN")))
		ConfigSetInteger(lpcfgPal, TEXT("CLR_THINGUNKNOWN"),		SRB2BCOLOUR(0x80, 0x80, 0x80));

	if(!ConfigNodeExists(lpcfgPal, TEXT("CLR_THINGSELECTED")))
		ConfigSetInteger(lpcfgPal, TEXT("CLR_THINGSELECTED"),		SRB2BCOLOUR(0xFF, 0x00, 0x00));

	if(!ConfigNodeExists(lpcfgPal, TEXT("CLR_THINGHIGHLIGHT")))
		ConfigSetInteger(lpcfgPal, TEXT("CLR_THINGHIGHLIGHT"),	SRB2BCOLOUR(0xFF, 0x99, 0x00));

	if(!ConfigNodeExists(lpcfgPal, TEXT("CLR_GRID")))
		ConfigSetInteger(lpcfgPal, TEXT("CLR_GRID"),				SRB2BCOLOUR(0x00, 0x00, 0x36));

	if(!ConfigNodeExists(lpcfgPal, TEXT("CLR_GRID64")))
		ConfigSetInteger(lpcfgPal, TEXT("CLR_GRID64"),			SRB2BCOLOUR(0x00, 0x00, 0x5E));

	if(!ConfigNodeExists(lpcfgPal, TEXT("CLR_LINEBLOCKSOUND")))
		ConfigSetInteger(lpcfgPal, TEXT("CLR_LINEBLOCKSOUND"),	SRB2BCOLOUR(0x69, 0x67, 0x98));

	if(!ConfigNodeExists(lpcfgPal, TEXT("CLR_MAPBOUNDARY")))
		ConfigSetInteger(lpcfgPal, TEXT("CLR_MAPBOUNDARY"),		SRB2BCOLOUR(0xFF, 0x00, 0x00));

	if(!ConfigNodeExists(lpcfgPal, TEXT("CLR_AXES")))
		ConfigSetInteger(lpcfgPal, TEXT("CLR_AXES"),				SRB2BCOLOUR(0x56, 0x00, 0x73));

	if(!ConfigNodeExists(lpcfgPal, TEXT("CLR_ZEROHEIGHTLINE")))
		ConfigSetInteger(lpcfgPal, TEXT("CLR_ZEROHEIGHTLINE"),	SRB2BCOLOUR(0x55, 0x55, 0x55));

	if(!ConfigNodeExists(lpcfgPal, TEXT("CLR_FOFSECTOR")))
		ConfigSetInteger(lpcfgPal, TEXT("CLR_FOFSECTOR"),			SRB2BCOLOUR(0xAA, 0xFF, 0xAA));

	if(!ConfigNodeExists(lpcfgPal, TEXT("CLR_TEXBROWSER")))
		ConfigSetInteger(lpcfgPal, TEXT("CLR_TEXBROWSER"),			SRB2BCOLOUR(0xFF, 0xFF, 0xFF));

	if(!ConfigNodeExists(lpcfgPal, TEXT("CLR_LASSO")))
		ConfigSetInteger(lpcfgPal, TEXT("CLR_LASSO"),		SRB2BCOLOUR(0xFF, 0x99, 0x00));

	if(!ConfigNodeExists(lpcfgPal, TEXT("CLR_NIGHTS")))
		ConfigSetInteger(lpcfgPal, TEXT("CLR_NIGHTS"),		SRB2BCOLOUR(0xFF, 0xFF, 0x00));
}


/* SetDefaultTextures
 *   Sets default default default (!) textures.
 *
 * Parameters: None.
 *
 * Return value: None.
 */
static void SetDefaultTextures(void)
{
	CONFIG *lpcfgDefTextures = ConfigGetSubsection(g_lpcfgMain, OPT_DEFAULTTEX);

	if(!ConfigNodeExists(lpcfgDefTextures, TEXT("upper"))) ConfigSetString(lpcfgDefTextures, TEXT("upper"), OPT_DEFAULT_DEFTEXTURE);
	if(!ConfigNodeExists(lpcfgDefTextures, TEXT("middle"))) ConfigSetString(lpcfgDefTextures, TEXT("middle"), OPT_DEFAULT_DEFTEXTURE);
	if(!ConfigNodeExists(lpcfgDefTextures, TEXT("lower"))) ConfigSetString(lpcfgDefTextures, TEXT("lower"), OPT_DEFAULT_DEFTEXTURE);
}


/* SetDefaultSector
 *   Sets default default default (!) sector properties.
 *
 * Parameters: None.
 *
 * Return value: None.
 */
static void SetDefaultSector(void)
{
	CONFIG *lpcfgDefSec = ConfigGetSubsection(g_lpcfgMain, OPT_DEFAULTSEC);

	if(!ConfigNodeExists(lpcfgDefSec, TEXT("tfloor"))) ConfigSetString(lpcfgDefSec, TEXT("tfloor"), TEXT("FLOOR0_6"));
	if(!ConfigNodeExists(lpcfgDefSec, TEXT("tceiling"))) ConfigSetString(lpcfgDefSec, TEXT("tceiling"), TEXT("FLOOR0_3"));

	if(!ConfigNodeExists(lpcfgDefSec, TEXT("hfloor"))) ConfigSetInteger(lpcfgDefSec, TEXT("hfloor"), 128);
	if(!ConfigNodeExists(lpcfgDefSec, TEXT("hceiling"))) ConfigSetInteger(lpcfgDefSec, TEXT("hceiling"), 512);
	if(!ConfigNodeExists(lpcfgDefSec, TEXT("brightness"))) ConfigSetInteger(lpcfgDefSec, TEXT("brightness"), 255);
}


/* SetDefaultNodebuilderOptions
 *   Sets default nodebuilder options.
 *
 * Parameters: None.
 *
 * Return value: None.
 */
static void SetDefaultNodebuilderOptions(void)
{
	CONFIG *lpcfgNB = ConfigGetSubsection(g_lpcfgMain, OPT_NODEBUILDER);

	if(!ConfigNodeExists(lpcfgNB, NBCFG_BINARY))
	{
		TCHAR szZenNode[MAX_PATH];
		int i;

		/* If ZenNode is in the same directory as ourselves, default to it. */

		GetModuleFileName(NULL, szZenNode, NUM_ELEMENTS(szZenNode) - 1);
		szZenNode[NUM_ELEMENTS(szZenNode) - 1] = TEXT('\0');

		/* Remove binary name. */
		i = _tcslen(szZenNode) - 1;
		while(szZenNode[i] != TEXT('\\') && szZenNode[i] != TEXT('/')) i--;
		szZenNode[i] = TEXT('\0');

		PathAppend(szZenNode, TEXT("ZenNode.exe"));

		if(GetFileAttributes(szZenNode) != INVALID_FILE_ATTRIBUTES)
			ConfigSetString(lpcfgNB, NBCFG_BINARY, szZenNode);
		else ConfigSetString(lpcfgNB, NBCFG_BINARY, TEXT(""));
	}

	if(!ConfigNodeExists(lpcfgNB, NBCFG_ARGS)) ConfigSetString(lpcfgNB, NBCFG_ARGS, TEXT("\"%F\" %L -o \"%F\""));
}


/* SetDefaultWikiOptions
 *   Sets default Wiki options.
 *
 * Parameters: None.
 *
 * Return value: None.
 */
static void SetDefaultWikiOptions(void)
{
	CONFIG *lpcfgWiki = ConfigGetSubsection(g_lpcfgMain, OPT_WIKI);

	if(!ConfigNodeExists(lpcfgWiki, WIKICFG_QUERYURLFMT)) ConfigSetString(lpcfgWiki, WIKICFG_QUERYURLFMT, TEXT("http://wiki.srb2.org/wiki/Special:Search?search=%s&go=Go"));
	if(!ConfigNodeExists(lpcfgWiki, WIKICFG_MAINPAGE)) ConfigSetString(lpcfgWiki, WIKICFG_MAINPAGE, TEXT("Main_Page"));
}


/* SetDefaultWindowOptions
 *   Sets default options for the window's position and state.
 *
 * Parameters: None.
 *
 * Return value: None.
 */
static void SetDefaultWindowOptions(void)
{
	CONFIG *lpcfgWindow = ConfigGetSubsection(g_lpcfgMain, OPT_WINDOW);

	if(!ConfigNodeExists(lpcfgWindow, WINCFG_STATE)) ConfigSetInteger(lpcfgWindow, WINCFG_STATE, SW_SHOWNORMAL);
}


/* GetOptionsForGame
 *   Gets the subsection of the main options for the specified game config.
 *
 * Parameters:
 *   CONFIG*	lpcfgMap	Game configuration.
 *
 * Return value: CONFIG*
 *   Subsection of the main options for lpcfgMap.
 */
CONFIG* GetOptionsForGame(CONFIG *lpcfgMap)
{
	UINT cchGameID = ConfigGetStringLength(lpcfgMap, MAPCFG_ID) + 1;
	LPTSTR szGameID = ProcHeapAlloc(cchGameID * sizeof(TCHAR));
	CONFIG *lpcfgGameOptions;

	/* Get the ID for this game. */
	ConfigGetString(lpcfgMap, MAPCFG_ID, szGameID, cchGameID);

	/* Get the options if they exist, or create them if they don't. */
	lpcfgGameOptions = ConfigGetOrAddSubsection(
		ConfigGetSubsection(g_lpcfgMain, OPT_GAMECONFIGS),
		szGameID);
	ProcHeapFree(szGameID);

	return lpcfgGameOptions;
}


/* GetTestingOptions
 *   Gets the testing options for a particular game configuration.
 *
 * Parameters:
 *   CONFIG*	lpcfgMainGame	Subsection of the main options for the game.
 *
 * Return value: CONFIG*
 *   Testing options.
 */
CONFIG* GetTestingOptions(CONFIG *lpcfgMainGame)
{
	CONFIG *lpcfgTesting = ConfigGetOrAddSubsection(lpcfgMainGame, TEXT("testing"));

	/* Set some defaults if necessary. */
	if(!ConfigNodeExists(lpcfgTesting, TESTCFG_SFX)) ConfigSetInteger(lpcfgTesting, TESTCFG_SFX, TRUE);
	if(!ConfigNodeExists(lpcfgTesting, TESTCFG_MUSIC)) ConfigSetInteger(lpcfgTesting, TESTCFG_MUSIC, TRUE);
	if(!ConfigNodeExists(lpcfgTesting, TESTCFG_DIFFICULTY)) ConfigSetInteger(lpcfgTesting, TESTCFG_DIFFICULTY, DIFF_NORMAL);
	if(!ConfigNodeExists(lpcfgTesting, TESTCFG_GAMETYPE)) ConfigSetInteger(lpcfgTesting, TESTCFG_GAMETYPE, GT_DEFAULT);
	if(!ConfigNodeExists(lpcfgTesting, TESTCFG_SKIN)) ConfigSetString(lpcfgTesting, TESTCFG_SKIN, TEXT("Sonic"));

	return lpcfgTesting;
}


/* UntaintOptions
 *   Fixes up potentially dangerous options to prevent monkey business by the
 *   user.
 *
 * Parameters:
 *   None.
 *
 * Return value: None.
 */
static void UntaintOptions(void)
{
	UntaintWikiOptions();
}


/* RemoveStaleMRUEntries
 *   Removes MRU entries for files that don't exist.
 *
 * Parameters:
 *   None.
 *
 * Return value: None.
 */
static void RemoveStaleMRUEntries(void)
{
	CONFIG *lpcfgMRU = ConfigGetSubsection(g_lpcfgMain, OPT_RECENT);
	LPCTSTR szIndices[MRUENTRIES] = {TEXT("1"), TEXT("2"), TEXT("3"), TEXT("4"), TEXT("5"), TEXT("6"), TEXT("7"), TEXT("8"), TEXT("9"), TEXT("10")};
	int i, iNextPos = 0;
	LPTSTR szFilename;
	int cchFilename;

	for(i = 0; i < (int)NUM_ELEMENTS(szIndices) && ConfigNodeExists(lpcfgMRU, szIndices[i]); i++)
	{

		cchFilename = ConfigGetStringLength(lpcfgMRU, szIndices[i]);
		if(cchFilename == 0) continue;

		szFilename = ProcHeapAlloc((cchFilename + 1) * sizeof(TCHAR));
		ConfigGetString(lpcfgMRU, szIndices[i], szFilename, cchFilename + 1);

		/* If the file exists, write it to its new position if necessary and
		 * move to the next slot.
		 */
		if(GetFileAttributes(szFilename) != 0xFFFFFFFF)
		{
			if(iNextPos < i)
				ConfigSetString(lpcfgMRU, szIndices[iNextPos], szFilename);

			iNextPos++;
		}

		ProcHeapFree(szFilename);
	}

	/* Delete the tail. */
	for(i = iNextPos; i < (int)NUM_ELEMENTS(szIndices) && ConfigNodeExists(lpcfgMRU, szIndices[i]); i++)
		ConfigDeleteNode(lpcfgMRU, szIndices[i]);
}


/* SetDockWndPosOptions
 *   Copies docking state information into the main config.
 *
 * Parameters:
 *   LPDOCKSAVESTATE	lpdss		Docking state info.
 *   LPCTSTR			szSection	Name of subsection (of window info) into
 *									which to store docking state.
 *
 * Return value: None.
 */
void SetDockWndPosOptions(LPDOCKSAVESTATE lpdss, LPCTSTR szSection)
{
	/* Get/create the subsection. */
	CONFIG *lpcfgDockState = ConfigGetOrAddSubsection(ConfigGetOrAddSubsection(g_lpcfgMain, OPT_WINDOW), szSection);

	/* Set all the fields. */
	ConfigSetInteger(lpcfgDockState, DOCKSTATECFG_DOCKTYPE, lpdss->dsdt);
	ConfigSetInteger(lpcfgDockState, DOCKSTATECFG_VISIBLE, lpdss->bVisible);
	ConfigSetInteger(lpcfgDockState, DOCKSTATECFG_XFLOAT, lpdss->xFloat);
	ConfigSetInteger(lpcfgDockState, DOCKSTATECFG_YFLOAT, lpdss->yFloat);
	ConfigSetInteger(lpcfgDockState, DOCKSTATECFG_CXFLOAT, lpdss->cxFrame);
	ConfigSetInteger(lpcfgDockState, DOCKSTATECFG_CYFLOAT, lpdss->cyFrame);
	ConfigSetInteger(lpcfgDockState, DOCKSTATECFG_DOCKSIZE, lpdss->nDockedSize);
	ConfigSetInteger(lpcfgDockState, DOCKSTATECFG_DISCRIMINANT, lpdss->iDiscriminant);
}


/* GetDockWndPosOptions
 *   Retrieves docking state information from the main config.
 *
 * Parameters:
 *   LPDOCKSAVESTATE	lpdss		Docking state info.
 *   LPCTSTR			szSection	Name of subsection (of window info) from
 *									which to retrieve docking state.
 *
 * Return value: BOOL
 *   TRUE if such information was found in the config; FALSE otherwise.
 */
BOOL GetDockWndPosOptions(LPDOCKSAVESTATE lpdss, LPCTSTR szSection)
{
	CONFIG *lpcfgWindow;
	CONFIG *lpcfgDockState;

	/* Attempt to get the subsection. */
	if((lpcfgWindow = ConfigGetSubsection(g_lpcfgMain, OPT_WINDOW)) && (lpcfgDockState = ConfigGetSubsection(lpcfgWindow, szSection)))
	{
		/* Found subsection. Get all the fields. */
		lpdss->dsdt = ConfigGetInteger(lpcfgDockState, DOCKSTATECFG_DOCKTYPE);
		lpdss->bVisible = ConfigGetInteger(lpcfgDockState, DOCKSTATECFG_VISIBLE);
		lpdss->xFloat = ConfigGetInteger(lpcfgDockState, DOCKSTATECFG_XFLOAT);
		lpdss->yFloat = ConfigGetInteger(lpcfgDockState, DOCKSTATECFG_YFLOAT);
		lpdss->cxFrame = ConfigGetInteger(lpcfgDockState, DOCKSTATECFG_CXFLOAT);
		lpdss->cyFrame = ConfigGetInteger(lpcfgDockState, DOCKSTATECFG_CYFLOAT);
		lpdss->nDockedSize = ConfigGetInteger(lpcfgDockState, DOCKSTATECFG_DOCKSIZE);
		lpdss->iDiscriminant = ConfigGetInteger(lpcfgDockState, DOCKSTATECFG_DISCRIMINANT);

		/* Found settings. */
		return TRUE;
	}

	/* No settings found. */
	return FALSE;
}


/* EnsureHaveNodebuilder
 *   Prompts the user to set a nodebuilder if one is not already set.
 *
 * Parameters:
 *   None.
 *
 * Return value: BOOL
 *   TRUE if a nodebuilder is set by the time we finish; FALSE otherwise.
 *
 * Remarks:
 *   Call this when it's time to build some nodes. If it's called at other
 *   times the UI probably won't make much sense.
 */
BOOL EnsureHaveNodebuilder(void)
{
	CONFIG *lpcfgNB = ConfigGetSubsection(g_lpcfgMain, OPT_NODEBUILDER);

	/* If we already have a nodebuilder, we can succeed immediately. */
	if(ConfigGetStringLength(lpcfgNB, NBCFG_BINARY) > 0)
		return TRUE;

	if(IDYES == MessageBoxFromStringTable(g_hwndClient, IDS_NODEBUILDER_QUERY, MB_ICONEXCLAMATION | MB_YESNO))
	{
		TCHAR szBinary[MAX_PATH] = TEXT("");
		TCHAR szFilter[128];

		/* Get and tidy up the filter string. */
		LoadAndFormatFilterString(IDS_BINARYFILTER, szFilter, NUM_ELEMENTS(szFilter));

		/* Browse for nodebuilder binary. */
		if(CommDlgOpen(g_hwndMain, szBinary, NUM_ELEMENTS(szBinary), NULL, szFilter, TEXT("exe"), szBinary, OFN_EXPLORER | OFN_FILEMUSTEXIST | OFN_HIDEREADONLY))
		{
			ConfigSetString(lpcfgNB, NBCFG_BINARY, szBinary);
			return TRUE;
		}
	}

	return FALSE;
}
