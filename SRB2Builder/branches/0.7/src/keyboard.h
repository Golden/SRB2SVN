/* SRB2 Workbench -- A map editor for Sonic Robo Blast 2.
 * Copyright (C) 2009 Gregor Dick
 * http://workbench.srb2.org/
 *
 * Incorporates portions of Doom Builder, (C) 2003 Pascal vd Heiden.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 * USA.
 *
 * keyboard.h: Header for keyboard.c.
 *
 * AMDG.
 */

#ifndef __SRB2B_KEYBOARD__
#define __SRB2B_KEYBOARD__

#include <windows.h>

#include "options.h"


#define NOCOMMAND	0xFFFF

/* Shift masks used for shortcut keys. */
#define SC_SHIFT	0x10000
#define SC_CTRL		0x20000
#define SC_ALT		0x40000
#define SC_KEY		0xFFFF


int MakeShiftedKeyCode(int iVirtKey);
void UpdateAcceleratorFromOptions(void);
USHORT GetMenuIDFromKeycode(int iKeyCode);
int GetKeycodeFromMenuID(USHORT unMenuID);
void SetDefaultShortcuts(void);
BOOL TranslateBespokeAccelerator(LPMSG lpmsg);
void GetShiftedShortcutText(int iKeyCode, LPTSTR szBuffer, UINT cchBuffer);
WORD ShiftedKeycodeToHotkey(int iKeyCode);
int HotkeyToShiftedKeycode(WORD wHotkey);
void InitialiseKeyboard(void);
short GetStringIDForVK(short nVKCode);


#endif
