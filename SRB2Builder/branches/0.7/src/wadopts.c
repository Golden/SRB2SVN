/* SRB2 Workbench -- A map editor for Sonic Robo Blast 2.
 * Copyright (C) 2009 Gregor Dick
 * http://workbench.srb2.org/
 *
 * Incorporates portions of Doom Builder, (C) 2003 Pascal vd Heiden.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 * USA.
 *
 * wadopts.c: Routines for handling per-wad preferences. The structure of the
 * configs is a cousin of Doom Builder's.
 *
 * AMDG.
 */

#include <windows.h>
#include <string.h>
#include <tchar.h>

#include "general.h"
#include "config.h"
#include "mapconfig.h"
#include "wadopts.h"
#include "texture.h"
#include "options.h"
#include "testing.h"
#include "openwads.h"

#define WADOPT_EXTENSION		TEXT(".wbs")
#define WADOPT_SIGNATURE		TEXT("SRB2 Workbench Wad Options")
#define WADOPT_SIGNATURE_KEY	TEXT("type")


static int LoadWadOptionsFilename(LPCTSTR szWadFilename, LPTSTR szBuffer, UINT cchBuffer);
static void SetMissingWadOptions(CONFIG *lpcfgWadOpts);
static void SetMissingMapOptions(CONFIG *lpcfgMapOptions);


/* LoadWadOptionsFilename
 *   Generates a wad options filename from a wad filename.
 *
 * Parameters:
 *   LPCTSTR	szWadFilename	Filename of wad.
 *   LPTSTR		szBuffer		Buffer in which to return options filename.
 *   UINT		cchBuffer		Size of szBuffer in characters, including room
 *								for terminator.
 *
 * Return value: int
 *   Length of written string, including terminator, or the size of buffer in
 *   characters required to store the entire filename if szBuffer is NULL.
 */
static int LoadWadOptionsFilename(LPCTSTR szWadFilename, LPTSTR szBuffer, UINT cchBuffer)
{
	int iWadFilenameLength = _tcslen(szWadFilename), iExtOffset;
	LPCTSTR szInName = &szWadFilename[iWadFilenameLength];
	UINT cchRequired;

	/* First, determine whether the file has an extension already. */
	while(szInName > szWadFilename && *szInName != TEXT('\\') && *szInName != TEXT('.') && *szInName != TEXT('/'))
		szInName--;

	/* Whither the extension? */
	if(*szInName == TEXT('.'))
		iExtOffset = iWadFilenameLength - _tcslen(szInName);
	else
		iExtOffset = iWadFilenameLength;

	cchRequired = iExtOffset + _tcslen(WADOPT_EXTENSION) + 1;

	/* If we didn't pass in a buffer, just return the number of characters
	 * required.
	 */
	if(!szBuffer)
		return cchRequired;

	/* Do we not even have enough room to start adding the extension? */
	if(cchBuffer - iExtOffset < 2)
		return iWadFilenameLength + 1;

	/* Copy as much as we can. */
	_tcsncpy(szBuffer, szWadFilename, cchBuffer - 1);

	/* Set the extension. */
	_tcsncpy(&szBuffer[iExtOffset], WADOPT_EXTENSION, cchBuffer - iExtOffset - 1);

	/* Ensure terminated. */
	szBuffer[cchBuffer - 1] = TEXT('\0');

	/* Return the number of characters copied. */
	return min(cchBuffer, cchRequired);
}


/* LoadWadOptions
 *   Retrieves the wad options for a wadfile, or creates a new one if no options
 *   file exists.
 *
 * Parameters:
 *   LPCTSTR	szWadFilename	Filename of wad.
 *
 * Return value: CONFIG*
 *   Wad options config structure.
 */
CONFIG *LoadWadOptions(LPCTSTR szWadFilename)
{
	UINT cchRequired = LoadWadOptionsFilename(szWadFilename, NULL, 0);
	LPTSTR szOptionsFilename = ProcHeapAlloc(cchRequired * sizeof(TCHAR));
	CONFIG *lpcfgWadOptions;

	/* Generate the filename for the options. */
	LoadWadOptionsFilename(szWadFilename, szOptionsFilename, cchRequired);

	/* Try to load it. */
	lpcfgWadOptions = ConfigLoad(szOptionsFilename);
	ProcHeapFree(szOptionsFilename);

	/* If we found it, make sure it's valid. */
	if(lpcfgWadOptions && ConfigNodeExists(lpcfgWadOptions, WADOPT_SIGNATURE_KEY))
	{
		UINT cchSignature = ConfigGetStringLength(lpcfgWadOptions, WADOPT_SIGNATURE_KEY) + 1;
		LPTSTR szSignature = ProcHeapAlloc(cchSignature * sizeof(TCHAR));
		BOOL bMatch;

		ConfigGetString(lpcfgWadOptions, WADOPT_SIGNATURE_KEY, szSignature, cchSignature);

		/* If the signature's okay, it passes the validation test. */
		bMatch = !_tcscmp(szSignature, WADOPT_SIGNATURE);

		ProcHeapFree(szSignature);

		if(bMatch)
		{
			SetMissingWadOptions(lpcfgWadOptions);
			return lpcfgWadOptions;
		}
	}

	/* If the file didn't exist or it was invalid, make a new one. */
	return NewWadOptions();
}


/* NewWadOptions
 *   Creates a new wad options config structure.
 *
 * Parameters: None.
 *
 * Return value: CONFIG*
 *   New wad options config structure.
 */
CONFIG* NewWadOptions(void)
{
	CONFIG *lpcfgWadOpts = ConfigCreate();

	/* Set the signature. */
	ConfigSetString(lpcfgWadOpts, WADOPT_SIGNATURE_KEY, WADOPT_SIGNATURE);

	SetMissingWadOptions(lpcfgWadOpts);

	return lpcfgWadOpts;
}


/* SetMissingWadOptions
 *   Fills missing items in a wad options config applying to the whole map (i.e.
 *   not in individual map sections).
 *
 * Parameters:
 *   CONFIG*	lpcfgWadOpts	Wad options.
 *
 * Return value: None.
 */
static void SetMissingWadOptions(CONFIG *lpcfgWadOpts)
{
	/* Default to using the external nodebuilder. */
	if(!ConfigNodeExists(lpcfgWadOpts, WADOPT_NODEBUILD))
		ConfigSetInteger(lpcfgWadOpts, WADOPT_NODEBUILD, NODEBUILD_EXTERNAL);

	/* Build the blockmap when using the internal nodebuilder. */
	if(!ConfigNodeExists(lpcfgWadOpts, WADOPT_BLOCKMAP))
		ConfigSetInteger(lpcfgWadOpts, WADOPT_BLOCKMAP, TRUE);
}


/* StoreMapCfgForWad
 *   Stores the specified map configuration in the wad options.
 *
 * Parameters:
 *   CONFIG*	lpcfgWadOpt		Wad options.
 *   CONFIG*	lpcfgMap		Map configuration.
 *
 * Return value: None.
 */
void StoreMapCfgForWad(CONFIG *lpcfgWadOpt, CONFIG *lpcfgMap)
{
	UINT cchGameID = ConfigGetStringLength(lpcfgMap, MAPCFG_ID) + 1;
	LPTSTR szGameID = ProcHeapAlloc(cchGameID * sizeof(TCHAR));

	/* Read string from map config and store it in wad options. */
	ConfigGetString(lpcfgMap, MAPCFG_ID, szGameID, cchGameID);
	ConfigSetString(lpcfgWadOpt, WADOPT_MAPCONFIG, szGameID);

	ProcHeapFree(szGameID);
}


/* GetMapOptionsFromWadOpt
 *   Gets options for a particular map from wad options, creating them if
 *   necessary.
 *
 * Parameters:
 *   CONFIG*	lpcfgWadOpt		Wad options.
 *   LPCTSTR	szLumpname		Map lumpname.
 *
 * Return value: CONFIG*
 *   Subsection root of map options.
 */
CONFIG* GetMapOptionsFromWadOpt(CONFIG *lpcfgWadOpt, LPCTSTR szLumpname)
{
	CONFIG *lpcfgMapOpt;

	if(!ConfigNodeExists(lpcfgWadOpt, szLumpname))
	{
		/* Create a new config and add it to the parent. */
		lpcfgMapOpt = ConfigCreate();
		ConfigSetSubsection(lpcfgWadOpt, szLumpname, lpcfgMapOpt);
	}
	else lpcfgMapOpt = ConfigGetSubsection(lpcfgWadOpt, szLumpname);

	/* Fill in anything that's missing. */
	SetMissingMapOptions(lpcfgMapOpt);

	return lpcfgMapOpt;
}


/* WriteWadOptions
 *   Writes the wad options for a wadfile.
 *
 * Parameters:
 *   LPCTSTR	szWadFilename	Filename of wad.
 *   CONFIG*	lpcfgWad		Wad options to write.
 *
 * Return value: int
 *   Zero on success; nonzero on error.
 */
int WriteWadOptions(LPCTSTR szWadFilename, CONFIG *lpcfgWad)
{
	UINT cchRequired = LoadWadOptionsFilename(szWadFilename, NULL, 0);
	LPTSTR szOptionsFilename = ProcHeapAlloc(cchRequired * sizeof(TCHAR));
	int iRet;

	/* Generate the filename for the options. */
	LoadWadOptionsFilename(szWadFilename, szOptionsFilename, cchRequired);

	/* Try to write the config. */
	iRet = ConfigWrite(lpcfgWad, szOptionsFilename);
	ProcHeapFree(szOptionsFilename);

	/* Finished. */
	return iRet;
}


/* SetMissingMapOptions
 *   Fills in any missing items from a map options config.
 *
 * Parameters:
 *   CONFIG*	lpcfgMapOptions		Map options config.
 *
 * Return value: None.
 *
 * Remarks:
 *   If the entries are there but of the wrong type, they aren't corrected.
 */
static void SetMissingMapOptions(CONFIG *lpcfgMapOptions)
{
	CHAR szTexName[TEXNAME_BUFFER_LENGTH];
	CONFIG *lpcfgMapDefTex, *lpcfgMapDefSec;
	CONFIG *lpcfgDefDefTex, *lpcfgDefDefSec;

	/* Get the subsections for textures and sectors, or make them if they don't
	 * exist.
	 */
	if(!(lpcfgMapDefTex = ConfigGetSubsection(lpcfgMapOptions, WADOPT_DEFAULTTEX)))
		lpcfgMapDefTex = ConfigAddSubsection(lpcfgMapOptions, WADOPT_DEFAULTTEX);

	if(!(lpcfgMapDefSec = ConfigGetSubsection(lpcfgMapOptions, WADOPT_DEFAULTSEC)))
		lpcfgMapDefSec = ConfigAddSubsection(lpcfgMapOptions, WADOPT_DEFAULTSEC);


	/* Default defaults from main config. */

	/* Default to single-player. */
	if(!ConfigNodeExists(lpcfgMapOptions, WADOPT_MAP_GAMETYPE))
		ConfigSetInteger(lpcfgMapOptions, WADOPT_MAP_GAMETYPE, GT_SINGLEPLAYER);

	/* Some sections. */
	lpcfgDefDefTex = ConfigGetSubsection(g_lpcfgMain, OPT_DEFAULTTEX);
	lpcfgDefDefSec = ConfigGetSubsection(g_lpcfgMain, OPT_DEFAULTSEC);


	/* Set the default textures. */
	if(!ConfigNodeExists(lpcfgMapDefTex, TEXT("upper")))
	{
		ConfigGetStringA(lpcfgDefDefTex, TEXT("upper"), szTexName, TEXNAME_BUFFER_LENGTH);
		ConfigSetStringA(lpcfgMapDefTex, TEXT("upper"), szTexName);
	}

	if(!ConfigNodeExists(lpcfgMapDefTex, TEXT("middle")))
	{
		ConfigGetStringA(lpcfgDefDefTex, TEXT("middle"), szTexName, TEXNAME_BUFFER_LENGTH);
		ConfigSetStringA(lpcfgMapDefTex, TEXT("middle"), szTexName);
	}

	if(!ConfigNodeExists(lpcfgMapDefTex, TEXT("lower")))
	{
		ConfigGetStringA(lpcfgDefDefTex, TEXT("lower"), szTexName, TEXNAME_BUFFER_LENGTH);
		ConfigSetStringA(lpcfgMapDefTex, TEXT("lower"), szTexName);
	}


	/* Set the default sector options. */
	if(!ConfigNodeExists(lpcfgMapDefSec, TEXT("tceiling")))
	{
		ConfigGetStringA(lpcfgDefDefSec, TEXT("tceiling"), szTexName, TEXNAME_BUFFER_LENGTH);
		ConfigSetStringA(lpcfgMapDefSec, TEXT("tceiling"), szTexName);
	}

	if(!ConfigNodeExists(lpcfgMapDefSec, TEXT("tfloor")))
	{
		ConfigGetStringA(lpcfgDefDefSec, TEXT("tfloor"), szTexName, TEXNAME_BUFFER_LENGTH);
		ConfigSetStringA(lpcfgMapDefSec, TEXT("tfloor"), szTexName);
	}

	if(!ConfigNodeExists(lpcfgMapDefSec, TEXT("hceiling")))
		ConfigSetInteger(lpcfgMapDefSec, TEXT("hceiling"), ConfigGetInteger(lpcfgDefDefSec, TEXT("hceiling")));

	if(!ConfigNodeExists(lpcfgMapDefSec, TEXT("hfloor")))
		ConfigSetInteger(lpcfgMapDefSec, TEXT("hfloor"), ConfigGetInteger(lpcfgDefDefSec, TEXT("hfloor")));

	if(!ConfigNodeExists(lpcfgMapDefSec, TEXT("brightness")))
		ConfigSetInteger(lpcfgMapDefSec, TEXT("brightness"), ConfigGetInteger(lpcfgDefDefSec, TEXT("brightness")));
}


/* GetDefaultMapConfigForWad
 *   Gets the default map config for a wad, or returns the default default
 *   config if none is specified.
 *
 * Parameters:
 *   int	iWad	Index of wad.
 *
 * Return value: CONFIG*
 *   Default map config.
 */
CONFIG* GetDefaultMapConfigForWad(int iWad)
{
	CONFIG *lpcfgWadOpt = GetWadOptions(iWad);
	CONFIG *lpcfgMap = NULL;

	/* Do we specify a default at all? */
	if(ConfigNodeExists(lpcfgWadOpt, WADOPT_MAPCONFIG))
	{
		/* Get the default ID. */
		UINT cchID = ConfigGetStringLength(lpcfgWadOpt, WADOPT_MAPCONFIG) + 1;
		LPTSTR szID = ProcHeapAlloc(cchID * sizeof(TCHAR));
		ConfigGetString(lpcfgWadOpt, WADOPT_MAPCONFIG, szID, cchID);

		/* Get the corresponding map config. */
		lpcfgMap = GetMapConfigByID(szID);

		ProcHeapFree(szID);
	}

	/* Return the matching config if we have one; otherwise return the default
	 * default.
	 */
	return lpcfgMap ? lpcfgMap : GetDefaultMapConfig();
}
