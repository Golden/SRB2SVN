/* SRB2 Workbench -- A map editor for Sonic Robo Blast 2.
 * Copyright (C) 2009 Gregor Dick
 * http://workbench.srb2.org/
 *
 * Incorporates portions of Doom Builder, (C) 2003 Pascal vd Heiden.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 * USA.
 *
 * editing.h: Header for editing.c.
 *
 * AMDG.
 */

#ifndef __SRB2B_EDITING__
#define __SRB2B_EDITING__

#include "general.h"
#include "map.h"
#include "config.h"
#include "renderer.h"
#include "selection.h"
#include "win/infobar.h"


/* Types. */
typedef struct _MAPOBJGRID MAPOBJGRID;

typedef struct _DRAW_OPERATION
{
	int				*lpiNewVertices;
	int				*lpiNewLines;

	int				iNewVertexCount;
	int				iNewLineCount;

	unsigned int	ciVertexBuffer;
	unsigned int	ciLineBuffer;

	DYNAMICINTARRAY	diarrayLDStateStack, diarrayVxStateStack;

	MAPOBJGRID		*lpmogVertices;
	MAPOBJGRID		*lpmogLinedefs;
} DRAW_OPERATION;

typedef struct _LOOPLIST LOOPLIST;

typedef struct _LINESEGMENT
{
	short x1, y1;
	short x2, y2;
} LINESEGMENT;


enum ENUM_VERTEX_EDIT_FLAGS
{
	VEF_NEW = 1,
	VEF_USED = 2,
	VEF_LABELLED = 4,
	VEF_DRAGGING = 8
};

enum ENUM_LINEDEF_EDIT_FLAGS
{
	LEF_NEW = 1,
	LEF_INVALIDSIDEDEFS = 2,	/* For new lds which haven't been set yet. */
	LEF_LOOPFRONT = 4,
	LEF_LOOPBACK = 8,
	LEF_ENCLOSEDFRONT = 16,
	LEF_ENCLOSEDBACK = 32,
	LEF_DELETE = 64,
	LEF_LENGTHCHANGING = 128,

	/* Values are important! They correspond to the LDTF_* flags << 8. */
	LEF_VISITED_FRONTUPPER = 0x100,
	LEF_VISITED_FRONTMIDDLE = 0x200,
	LEF_VISITED_FRONTLOWER = 0x400,
	LEF_VISITED_BACKUPPER = 0x800,
	LEF_VISITED_BACKMIDDLE = 0x1000,
	LEF_VISITED_BACKLOWER = 0x2000,

	LEF_2PASSFRONT = 0x4000,
	LEF_2PASSBACK = 0x8000,
	LEF_NEWSECSETFRONT = 0x10000,
	LEF_NEWSECSETBACK = 0x20000,

	LEF_NEWLOOPFRONT = 0x40000,
	LEF_NEWLOOPBACK = 0x80000,

	LEF_RSFRONT = 0x100000,
	LEF_RSBACK = 0x200000,

	LEF_NOGRAPH = 0x400000,

	LEF_LABELLED = 0x80000000		/* A generic label. */
};

#define LEF_VISITED_FRONT	(LEF_VISITED_FRONTUPPER | LEF_VISITED_FRONTMIDDLE | LEF_VISITED_FRONTLOWER)
#define LEF_VISITED_BACK	(LEF_VISITED_BACKUPPER | LEF_VISITED_BACKMIDDLE | LEF_VISITED_BACKLOWER)
#define LEF_VISITED			(LEF_VISITED_FRONT | LEF_VISITED_BACK)
#define LEF_RECALCSECTOR	(LEF_RSFRONT | LEF_RSBACK)

enum ENUM_SECTOREDITFLAGS
{
	SED_HASFOF = 1
};

enum ENUM_SNAP_FLAGS
{
	SF_RECTANGLE	= 1,
	SF_45			= 2,
	SF_VERTICES		= 4,
	SF_LINES		= 8
};

/* These can be used as indices into arrays. */
enum ENUM_LINE_SIDE
{
	LS_FRONT, LS_BACK
};

/* These can be used as indices into arrays. */
enum ENUM_SDTEX
{
	SDT_UPPER, SDT_MIDDLE, SDT_LOWER
};

/* Values are important! They correspond to the LEF_VISITED flags >> 8. */
enum ENUM_LDTEX_FLAGS
{
	LDTF_FRONTUPPER		= 0x01,
	LDTF_FRONTMIDDLE	= 0x02,
	LDTF_FRONTLOWER		= 0x04,
	LDTF_BACKUPPER		= 0x08,
	LDTF_BACKMIDDLE		= 0x10,
	LDTF_BACKLOWER		= 0x20
};

#define LDTF_FRONT (LDTF_FRONTUPPER | LDTF_FRONTMIDDLE | LDTF_FRONTLOWER)
#define LDTF_BACK (LDTF_BACKUPPER | LDTF_BACKMIDDLE | LDTF_BACKLOWER)
#define LDTF_ALL (LDTF_FRONT | LDTF_BACK)

/* Flags have precedence. One lassoed ld beats lots of selected lds when it
 * comes to determining sector states.
 */
enum ENUM_SELFLAGS
{
	SLF_SELECTED = 1,
	SLF_LASSOING = 2
};

typedef enum _ENUM_LDLOOKUP_TYPES
{
	LDLT_VXTOVX, LDLT_VXTOLD
} ENUM_LDLOOKUP_TYPES;

enum ENUM_LABELRSLINESLOOPS_FLAGS
{
	LRL_ALLOWPARTIAL = 1
};

typedef enum _ENUM_RADIUS_TYPE
{
	RT_TOEDGES,
	RT_TOVERTICES
} ENUM_RADIUS_TYPE;

enum ENUM_FNOIMOG_FLAGS
{
	FNF_OBJISRECT = 1,		/* If set, *lpvSourceObject is a RECT; otherwise,
							 * it's an int* pointing to the index of the object.
							 */
};

/* Flags for StitchMultipleVertices. */
#define SMVF_NODELETE	0
#define SMVF_DELETE		1	/* Delete source vertices after stitching. */


/* Are we interested in this object at all? */
typedef BOOL (*LPFNOBJ_CONDITION)(MAP *lpmap, int iObject, void *lpvParam);
/* How far from this object to that cell? lpvObject is either a RECT or an
 * int* pointing at the index.
 */
typedef int (*LPFNOBJ_CELL_METRIC)(MAP *lpmap, MAPOBJGRID *lpmog, short nCol, short nRow, void *lpvObject);
/* Finds nearest object in a cell to an object. */
typedef int (*LPFNNEAREST_IN_CELL)(MAP *lpmap, MAPOBJGRID *lpmog, short nCol, short nRow, void *lpvObject, int *lpiDist, LPFNOBJ_CONDITION lpfnObjCondition, void *lpvParam);
/* Find extent of an object. */
typedef void (*LPFNOBJ_REP_RECT)(MAP *lpmap, int iObject, RECT *lprc);


/* Prototypes. */
int RemoveUnusedVertices(MAP *lpmap, BYTE byRequiredFlags);
void GetLinedefDisplayInfo(MAP *lpmap, CONFIG *lpcfgLinedefFlat, int iIndex, LINEDEFDISPLAYINFO *lplddi);
void GetSectorDisplayInfo(MAP *lpmap, CONFIG *lpcfgSectors, int iIndex, SECTORDISPLAYINFO *lpsdi, BOOL bNybble);
void GetThingDisplayInfo(MAP *lpmap, CONFIG *lpcfgThings, int iIndex, THINGDISPLAYINFO *lptdi);
void GetVertexDisplayInfo(MAP *lpmap, int iIndex, VERTEXDISPLAYINFO *lpvdi);
DWORD CheckLines(MAP *lpmap, SELECTION_LIST *lpsellist, CONFIG *lpcfgLinedefTypesFlat, LINEDEFDISPLAYINFO *lpsdi);
DWORD CheckSectors(MAP *lpmap, SELECTION_LIST *lpsellist, CONFIG *lpcfgSecTypes, SECTORDISPLAYINFO *lpsdi, BOOL bNybble);
DWORD CheckThings(MAP *lpmap, SELECTION_LIST *lpsellist, CONFIG *lpcfgFlatThings, THINGDISPLAYINFO *lptdi);
DWORD CheckVertices(MAP *lpmap, SELECTION_LIST *lpsellist, VERTEXDISPLAYINFO *lptdi);
void CheckLineFlags(MAP *lpmap, SELECTION_LIST *lpsellist, WORD *lpwFlagValues, WORD *lpwFlagMask);
void SetLineFlags(MAP *lpmap, SELECTION_LIST *lpsellist, WORD wFlagValues, WORD wFlagMask);
void CheckThingFlags(MAP *lpmap, SELECTION_LIST *lpsellist, WORD *lpwFlagValues, WORD *lpwFlagMask);
void SetThingFlags(MAP *lpmap, SELECTION_LIST *lpsellist, WORD wFlagValues, WORD wFlagMask);
BYTE CheckTextureFlags(MAP *lpmap, SELECTION_LIST *lpsellist, LPCTSTR szSky);
unsigned short NextUnusedTag(MAP *lpmap);
BOOL GetAdjacentSectorHeightRange(MAP *lpmap, short *lpnMaxCeil, short *lpnMinCeil, short *lpnMaxFloor, short *lpnMinFloor);
void GetMapRect(MAP *lpmap, RECT *lprc);
void GetSelectionRect(MAP *lpmap, SELECTION *lpselection, RECT *lprc);
void ApplySectorPropertiesToSelection(MAP *lpmap, SELECTION_LIST *lpsellist, SECTORDISPLAYINFO *lpsdi, DWORD dwFlags);
void ApplySectorProperties(MAP *lpmap, int iIndex, SECTORDISPLAYINFO *lpsdi, DWORD dwFlags);
void ApplyLinePropertiesToSelection(MAP *lpmap, SELECTION_LIST *lpsellist, LINEDEFDISPLAYINFO *lplddi, DWORD dwFlags);
void ApplyLineProperties(MAP *lpmap, int iIndex, LINEDEFDISPLAYINFO *lplddi, DWORD dwFlags);
void ApplyThingPropertiesToSelection(MAP *lpmap, SELECTION_LIST *lpsellist, THINGDISPLAYINFO *lptdi, DWORD dwFlags, CONFIG *lpcfgFlatThings);
void ApplyThingProperties(MAP *lpmap, int iIndex, THINGDISPLAYINFO *lptdi, DWORD dwFlags);
int AddVertex(MAP *lpmap, short x, short y);
int AddThing(MAP *lpmap, short x, short y);
int AddLinedef(MAP *lpmap, int iVertexStart, int iVertexEnd);
int AddSector(MAP *lpmap);
int AddSidedef(MAP *lpmap, int iSector);
void BeginDrawOperation(DRAW_OPERATION *lpdrawop, MAP *lpmap);
void EndDrawOperation(MAP *lpmap, DRAW_OPERATION *lpdrawop, CONFIG *lpcfgWadOptMap, LPCTSTR szSky);
BOOL DrawToNewVertex(MAP *lpmap, DRAW_OPERATION *lpdrawop, CONFIG *lpcfgWadOptMap, short x, short y, BOOL bStitch);
void FlipLinedef(MAP *lpmap, int iLinedef);
void FlipSelectedLinedefs(MAP *lpmap, SELECTION_LIST* lpsellist);
void ExchangeSelectedSidedefs(MAP *lpmap, SELECTION_LIST* lpsellist);
int SplitLinedefAtPoint(MAP *lpmap, int iLinedef, short x, short y);
void BisectSelectedLinedefs(MAP *lpmap, SELECTION_LIST* lpsellist);
int SplitLinedef(MAP *lpmap, int iLinedef, int iVertex);
int GetVertexFromPosition(MAP *lpmap, short x, short y);
int FindLinedefBetweenVertices(MAP *lpmap, int iVertexA, int iVertexB);
int FindLinedefBetweenVerticesDirected(MAP *lpmap, int iVertex1, int iVertex2);
void Snap(short *lpx, short *lpy, unsigned short cxSnap, unsigned short cySnap, short xOffset, short yOffset);
void LineSegmentIntersection(short xA1, short yA1, short xA2, short yA2, short xB1, short yB1, short xB2, short yB2, short *lpxRet, short *lpyRet);
void DeleteSector(MAP *lpmap, int iSector);
void DeleteLinedef(MAP *lpmap, int iLinedef);
void DeleteSidedef(MAP *lpmap, int iSidedef);
void DeleteVertex(MAP *lpmap, int iVertex);
void DeleteThing(MAP *lpmap, int iThing);
void JoinSelectedSectors(MAP *lpmap, SELECTION_LIST *lpsellist, BOOL bMerge);
void LabelSelectedLinesRS(MAP *lpmap);
void LabelRSLinesEnclosure(MAP *lpmap, LOOPLIST *lplooplist);
LOOPLIST* LabelRSLinesLoops(MAP *lpmap, BYTE byFlags);
void LabelLoopedLinesRS(MAP *lpmap, LOOPLIST *lplooplist);
void DestroyLoopList(LOOPLIST *lplooplist);
void ClearDraggingFlags(MAP *lpmap);
void ClearLineFlags(MAP *lpmap, DWORD dwFlags);
void CorrectRSSectorReferences(MAP *lpmap);
void FlipVertexAboutVerticalAxis(MAP *lpmap, int iVertex, short xAxis);
void FlipThingAboutVerticalAxis(MAP *lpmap, int iThing, short xAxis);
void FlipVertexAboutHorizontalAxis(MAP *lpmap, int iVertex, short yAxis);
void FlipThingAboutHorizontalAxis(MAP *lpmap, int iThing, short yAxis);
void SetThingSelectionZ(MAP *lpmap, SELECTION_LIST *lpsellist, CONFIG *lpcfgFlatThings, WORD z, BOOL bAbsolute);
void StitchMultipleVertices(MAP *lpmap, const int *lpiSrcVertices, unsigned int uiNumSrcVertices, int iTargetVertex, DWORD dwFlags);

static __inline void StitchVertices(MAP *lpmap, int iSrc, int iTarget)
{
	StitchMultipleVertices(lpmap, &iSrc, 1, iTarget, SMVF_DELETE);
}

static __inline void ReplaceVertexReferences(MAP *lpmap, int iSrc, int iTarget)
{
	StitchMultipleVertices(lpmap, &iSrc, 1, iTarget, SMVF_NODELETE);
}

BOOL StitchDraggedVertices(MAP *lpmap, SELECTION_LIST *lpsellistVertices);
void DeleteZeroLengthLinedefs(MAP *lpmap);
BYTE RequiredTextures(MAP *lpmap, int iLinedef, LPCTSTR szSky);
int GetThingZ(MAP *lpmap, int iThing, CONFIG *lpcfgFlatThings, BOOL bAbsolute);
__inline void SetThingZ(MAP *lpmap, int iThing, CONFIG *lpcfgFlatThings, WORD z, BOOL bAbsolute);
void ApplyRelativeCeilingHeightSelection(MAP *lpmap, SELECTION_LIST *lpsellistSectors, int iDelta);
void ApplyRelativeFloorHeightSelection(MAP *lpmap, SELECTION_LIST *lpsellistSectors, int iDelta);
void ApplyRelativeBrightnessSelection(MAP *lpmap, SELECTION_LIST *lpsellistSectors, short nDelta);
void ApplyRelativeThingZSelection(MAP *lpmap, SELECTION_LIST *lpselllistThings, CONFIG *lpcfgFlatThings, int iDelta);
void ApplyRelativeAngleSelection(MAP *lpmap, SELECTION_LIST *lpselllistThings, int iDelta);
void ApplyRelativeThingXSelection(MAP *lpmap, SELECTION_LIST *lpselllistThings, int iDelta);
void ApplyRelativeThingYSelection(MAP *lpmap, SELECTION_LIST *lpselllistThings, int iDelta);
void ApplyRelativeThingFlagsSelection(MAP *lpmap, SELECTION_LIST *lpselllistThings, int iDelta);
void ApplyRelativeFrontSidedefXSelection(MAP *lpmap, SELECTION_LIST *lpselllistLines, int iDelta);
void ApplyRelativeFrontSidedefYSelection(MAP *lpmap, SELECTION_LIST *lpselllistLines, int iDelta);
void ApplyRelativeBackSidedefXSelection(MAP *lpmap, SELECTION_LIST *lpselllistLines, int iDelta);
void ApplyRelativeBackSidedefYSelection(MAP *lpmap, SELECTION_LIST *lpselllistLines, int iDelta);
void GradientSelectedCeilings(MAP *lpmap, SELECTION_LIST *lpsellistSectors);
void GradientSelectedFloors(MAP *lpmap, SELECTION_LIST *lpsellistSectors);
void GradientSelectedBrightnesses(MAP *lpmap, SELECTION_LIST *lpsellistSectors);
void GradientSelectedThingZ(MAP *lpmap, SELECTION_LIST *lpsellistThings, CONFIG *lpcfgFlatThings);
void RotateVertexAboutPoint(MAP *lpmap, int iVertex, short x, short y, float fAngle);
void RotateThingAboutPoint(MAP *lpmap, int iThing, short x, short y, float fAngle);
void RotateThingDirection(MAP *lpmap, int iThing, int iAngle);
void DilateVertexWrtFixedPoint(MAP *lpmap, int iVertex, short x, short y, float fFactor);
void DilateThingWrtFixedPoint(MAP *lpmap, int iThing, short x, short y, float fFactor);
void RotateThingToPoint(MAP *lpmap, int iThing, short x, short y);
void LabelChangingLines(MAP *lpmap, SELECTION_LIST *lpsellistVertices);
void LabelDraggingVertices(MAP *lpmap, SELECTION_LIST *lpsellistVertices);
void AutoalignTexturesFromLinedef(MAP *lpmap, HWND hwndMap, int iStartLinedef, BYTE byTexFlags, BOOL bInSelection);
void AutoalignAllTextures(MAP *lpmap, HWND hwndMap, BYTE byTexFlags, BOOL bInSelection);
void FixMissingTextures(MAP *lpmap, LPCTSTR szUpper, LPCTSTR szMiddle, LPCTSTR szLower, LPCTSTR szSky, BOOL bInSelection);
int FindIdenticalSectorSet(MAP *lpmap, int iStartSector, BOOL bInSelection, DYNAMICINTARRAY *lpdiarray);
int MergeIdenticalSectors(MAP *lpmap, BOOL bInSelection);
CONFIG* CreateUsedTexturesConfig(MAP *lpmap);
CONFIG* CreateUsedFlatsConfig(MAP *lpmap);
void MakeSelectedLinesSingleSided(MAP *lpmap, SELECTION_LIST* lpsellist);
void MakeSelectedLinesDoubleSided(MAP *lpmap, SELECTION_LIST* lpsellist, CONFIG *lpcfgWadOptMap);
void RemoveSelectedSectorInteriors(MAP *lpmap, SELECTION_LIST* lpsellist);
void ResolveSelectedSectors(MAP *lpmap, SELECTION_LIST* lpsellist);
int FindNearestAdjacentLinedef(MAP *lpmap, DYNAMICINTARRAY *lpldlookup, int iLinedef, int iLinedefSide, int iWhichVertex, int *lpiLookupIndex, double *lpdAngle, BOOL (*fnCondition)(MAPLINEDEF *lpld, void *lpvParam), void *lpvParam);
DYNAMICINTARRAY *BuildLDLookupTable(MAP *lpmap, int iLookupType);
void DestroyLDLookupTable(MAP *lpmap, DYNAMICINTARRAY *lpldlookup);
DYNAMICINTARRAY* DeepCopyLDLookupTable(MAP *lpmap, DYNAMICINTARRAY *lpldlookupSrc);
void SetLinedefSidedef(MAP *lpmap, int iLinedef, int iSidedef, int iLinedefSide);
int FindLineSideSector(MAP *lpmap, int iLinedef, int iLinedefSide);
void FixMissingTexturesLinedef(MAP *lpmap, int iLinedef, LPCTSTR szUpper, LPCTSTR szMiddle, LPCTSTR szLower, LPCTSTR szSky);
void CorrectRSLinesSidedness(MAP *lpmap);
BOOL NearestPointOnSnappableLine(MAP *lpmap, int xSrc, int ySrc, int *lpxLine, int *lpyLine);
BOOL IsRectangleUnobstructed(MAP *lpmap, short xLeft, short xRight, short yTop, short yBottom);
void CreatePolygonalSector(MAP *lpmap, short xCentre, short yCentre, short nRadius, short nEdges, ENUM_RADIUS_TYPE radiustype, DRAW_OPERATION *lpdrawop, CONFIG *lpcfgWadOptMap, LPCTSTR szSky, BOOL bSnapToGrid, short cxGrid, short cyGrid, short xGridOffset, short yGridOffset);
void CreateRectangularSector(MAP *lpmap, LPRECT lprc, DRAW_OPERATION *lpdrawop, CONFIG *lpcfgWadOptMap, LPCTSTR szSky);
void DestroyMapObjGrid(MAPOBJGRID *lpmog);
int FindNearestObjectInMOG(MAP *lpmap,
							 MAPOBJGRID *lpmog,
							 void *lpvSourceObject,
							 LPFNOBJ_CELL_METRIC lpfnObjCellMetric,
							 LPFNOBJ_REP_RECT lpfnObjRepRect,
							 LPFNNEAREST_IN_CELL lpfnNearestInCell,
							 LPFNOBJ_CONDITION lpfnObjCondition,
							 void *lpvConditionParam,
							 DWORD dwFlags);
MAPOBJGRID* CreateVertexMOG(MAP *lpmap);
MAPOBJGRID* CreateConditionedVertexMOG(MAP *lpmap, BOOL (*lpfnCondition)(MAP* lpmap, int iVertex, void *lpvParam), void* lpvParam);
MAPOBJGRID* CreateLinedefMOG(MAP *lpmap);
int PointToCellMetric(MAP *lpmap, MAPOBJGRID *lpmog, short nCol, short nRow, void *lpvPoint);
int LinedefToCellMetric(MAP *lpmap, MAPOBJGRID *lpmog, short nCol, short nRow, void *lpvLinedef);
int NearestConditionedVertexInCell(MAP *lpmap, MAPOBJGRID *lpmog, short nCol, short nRow, void *lpvRect, int *lpiDist, LPFNOBJ_CONDITION lpfnObjCondition, void *lpvParam);
int NearestConditionedVertexInCellToLinedef(MAP *lpmap, MAPOBJGRID *lpmog, short nCol, short nRow, void *lpvLinedef, int *lpiDist, LPFNOBJ_CONDITION lpfnObjCondition, void *lpvParam);
int NearestConditionedLinedefInCell(MAP *lpmap, MAPOBJGRID *lpmog, short nCol, short nRow, void *lpvRect, int *lpiDist, LPFNOBJ_CONDITION lpfnObjCondition, void *lpvParam);
void GetLinedefBounds(MAP *lpmap, int iLinedef, RECT *lprc);
void RecalcDrawOpMOGs(MAP *lpmap, DRAW_OPERATION *lpdrawop);

#ifdef _UNICODE
void FillTexNameBufferW(LPSTR sTexBuf, LPCWSTR szTexName);
#define FillTexNameBuffer FillTexNameBufferW
#else
#define FillTexNameBuffer FillTexNameBufferA
#endif
void FillTexNameBufferA(LPSTR sTexBuf, LPCSTR szTexName);


#ifdef _DEBUG
BOOL VerifyEditFlagIntegrity(MAP *lpmap);
#endif


static __inline void PushDrawOpState(DRAW_OPERATION *lpdrawop)
{
	AddToDynamicIntArray(&lpdrawop->diarrayLDStateStack, lpdrawop->iNewLineCount);
	AddToDynamicIntArray(&lpdrawop->diarrayVxStateStack, lpdrawop->iNewVertexCount);
}


static __inline void PopDrawOpState(DRAW_OPERATION *lpdrawop)
{
	if(lpdrawop->diarrayLDStateStack.uiCount > 0)
	{
		lpdrawop->iNewLineCount = lpdrawop->diarrayLDStateStack.lpiIndices[--lpdrawop->diarrayLDStateStack.uiCount];
		lpdrawop->iNewVertexCount = lpdrawop->diarrayVxStateStack.lpiIndices[--lpdrawop->diarrayVxStateStack.uiCount];
	}
}


#endif
