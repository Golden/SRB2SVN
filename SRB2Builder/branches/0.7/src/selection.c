/* SRB2 Workbench -- A map editor for Sonic Robo Blast 2.
 * Copyright (C) 2009 Gregor Dick
 * http://workbench.srb2.org/
 *
 * Incorporates portions of Doom Builder, (C) 2003 Pascal vd Heiden.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 * USA.
 *
 * selection.c: Routines for manipulating selections, i.e. dynamically-sized
 * arrays with unique elements.
 *
 * AMDG.
 */

#include <windows.h>

#include "general.h"
#include "selection.h"


/* AllocateSelectionList
 *   Allocates a selection list structure, initialising it.
 *
 * Parameters:
 *   int	iInitListSize		Initial length list.
 *
 * Return value: SELECTION_LIST*
 *   Pointer to new structure.
 *
 * Notes:
 *   Call DestroySelectionList to free the returned pointer.
 */
SELECTION_LIST* AllocateSelectionList(int iInitListSize)
{
	SELECTION_LIST *lpsellist;

	/* Allocate structure. */
	lpsellist = ProcHeapAlloc(sizeof(SELECTION_LIST));

	/* Avoid the pathological case. */
	if(iInitListSize == 0)
		iInitListSize = 1;

	lpsellist->ciBufferLength = iInitListSize;
	lpsellist->iDataCount = 0;
	lpsellist->lpiIndices = ProcHeapAlloc(iInitListSize * sizeof(int));

	return lpsellist;
}


/* DestroySelectionList
 *   Destroys a selection list structure, including its contents.
 *
 * Parameters:
 *   SELECTION_LIST*	lpsellist	Pointer to selection list to free.
 *
 * Return value: None.
 */
void DestroySelectionList(SELECTION_LIST *lpsellist)
{
	/* Free list. */
	ProcHeapFree(lpsellist->lpiIndices);

	/* Free structure. */
	ProcHeapFree(lpsellist);
}



/* AddToSelectionList
 *   Adds an item to a selection list.
 *
 * Parameters:
 *   SELECTION_LIST*	lpsellist	Pointer to selection hash.
 *   int				iValue		New value.
 *
 * Return value: None.
 */
void AddToSelectionList(SELECTION_LIST *lpsellist, int iValue)
{
	/* Remove the item if it already exists. */
	RemoveFromSelectionList(lpsellist, iValue);

	AddToSelectionListWithRepetition(lpsellist, iValue);
}


/* AddToSelectionListWithRepetition
 *   Adds an item to a selection list, but doesn't check for duplicates.
 *
 * Parameters:
 *   SELECTION_LIST*	lpsellist	Pointer to selection hash.
 *   int				iValue		New value.
 *
 * Return value: None.
 */
void AddToSelectionListWithRepetition(SELECTION_LIST *lpsellist, int iValue)
{
	/* List full? */
	if(lpsellist->iDataCount == lpsellist->ciBufferLength)
	{
		/* Increase size. */
		lpsellist->ciBufferLength <<= 1;
		lpsellist->lpiIndices = ProcHeapReAlloc(lpsellist->lpiIndices, (lpsellist->ciBufferLength) * sizeof(int));
	}

	/* Add the new item to the end of the list and increment count. */
	lpsellist->lpiIndices[lpsellist->iDataCount++] = iValue;
}


/* RemoveFromSelectionList
 *   Removes an item from a selection list.
 *
 * Parameters:
 *   SELECTION_LIST*	lpsellist	Pointer to selection list.
 *   int				iValue		Value to remove.
 *
 * Return value: BOOL
 *   TRUE if value was found (and removed); FALSE otherwise.
 */
BOOL RemoveFromSelectionList(SELECTION_LIST *lpsellist, int iValue)
{
	int *lpiIndices;
	int i;

	/* Save some dereferencing in the loop. */
	lpiIndices = lpsellist->lpiIndices;

	/* Find whether entry's already there. */
	i = 0;
	while(i < lpsellist->iDataCount && lpiIndices[i] != iValue) i++;

	/* Did the entry already exist? */
	if(i < lpsellist->iDataCount)
	{
		if(i < lpsellist->iDataCount - 1)
		{
			/* Remove it by copying the rest of the list down on top of it. Must
			 * use MoveMemory, since the regions overlap.
			 */
			MoveMemory(&lpiIndices[i], &lpiIndices[i+1], (lpsellist->iDataCount - i - 1) * sizeof(int));
		}

		lpsellist->iDataCount--;

		return TRUE;
	}

	return FALSE;
}


/* ExistsInSelectionList
 *  Determines whether an item exists in a selection list.
 *
 * Parameters:
 *   SELECTION_LIST*	lpsellist	Pointer to selection list.
 *   int				iValue		Value to search for.
 *
 * Return value: BOOL
 *   TRUE if value was found; FALSE otherwise.
 */
BOOL ExistsInSelectionList(SELECTION_LIST *lpsellist, int iValue)
{
	int *lpiIndices;
	int i;

	/* Save some dereferencing in the loop. */
	lpiIndices = lpsellist->lpiIndices;

	/* Find whether entry's already there. */
	for(i = 0; i < lpsellist->iDataCount; i++)
		if(lpiIndices[i] == iValue) return TRUE;

	return FALSE;
}


/* SortSelectionList
 *  Sorts a selection list in ascending order.
 *
 * Parameters:
 *   SELECTION_LIST*	lpsellist	Pointer to selection list.
 *
 * Return value: None.
 */
void SortSelectionList(SELECTION_LIST *lpsellist)
{
	qsort(lpsellist->lpiIndices, lpsellist->iDataCount, sizeof(int), QsortIntegerComparison);
}

#if 0
/* DuplicateSelectionList
 *   Makes a deep copy of a selection list structure.
 *
 * Parameters:
 *   SELECTION_LIST*	lpsellist	List to duplicate.
 *
 * Return value: SELECTION_LIST*
 *   Pointer to new structure.
 *
 * Notes:
 *   Call DestroySelectionList as normal to free the returned pointer.
 */
SELECTION_LIST* DuplicateSelectionList(SELECTION_LIST* lpsellist)
{
	SELECTION_LIST *lpsellistDup;

	/* Allocate structure. */
	lpsellistDup = ProcHeapAlloc(sizeof(SELECTION_LIST));

	lpsellistDup->ciBufferLength = lpsellist->ciBufferLength;
	lpsellistDup->iDataCount = lpsellist->iDataCount;
	lpsellistDup->lpiIndices = ProcHeapAlloc(lpsellist->ciBufferLength * sizeof(int));
	CopyMemory(lpsellistDup->lpiIndices, lpsellist->lpiIndices, lpsellist->iDataCount * sizeof(int));

	return lpsellistDup;
}
#endif
