/* SRB2 Workbench -- A map editor for Sonic Robo Blast 2.
 * Copyright (C) 2009 Gregor Dick
 * http://workbench.srb2.org/
 *
 * Incorporates portions of Doom Builder, (C) 2003 Pascal vd Heiden.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 * USA.
 *
 * optionsdlg.c: Implements the Options dialogue.
 *
 * AMDG.
 */

#include <windows.h>
#include <commctrl.h>
#include <shlwapi.h>

#include "../general.h"
#include "../config.h"
#include "../options.h"
#include "../renderer.h"
#include "../cdlgwrapper.h"
#include "../keyboard.h"
#include "../wiki.h"
#include "../mapconfig.h"

#include "gendlg.h"
#include "optionsdlg.h"
#include "mdiframe.h"

#include "../../res/resource.h"


#define OPTDLG_NUMPAGES			6
#define CCH_COLOUR_DESCRIPTION	64
#define CCH_SHORTCUT_COLHDR		64
#define CCH_SHORTCUT_MOUSECOMBO	32
#define CCH_SHORTCUT_FUNCTION	64
#define CCH_SHORTCUT_SHORTCUT	64
#define CX_SWATCH_MAX			64

#ifndef SHACF_FILESYS_ONLY
#define SHACF_FILESYS_ONLY		0x10
#endif


const struct SCKSTRINGID {int iShortcutID, iStringID;} g_sckstringids[] =
{
	{SCK_EDITQUICKMOVE, IDS_SCK_EDITQUICKMOVE},
	{SCK_ZOOMIN, IDS_SCK_ZOOMIN},
	{SCK_ZOOMOUT, IDS_SCK_ZOOMOUT},
	{SCK_CENTREVIEW, IDS_SCK_CENTREVIEW},
	{SCK_EDITMOVE, IDS_SCK_EDITMOVE},
	{SCK_EDITANY, IDS_SCK_EDITANY},
	{SCK_EDITLINES, IDS_SCK_EDITLINES},
	{SCK_EDITSECTORS, IDS_SCK_EDITSECTORS},
	{SCK_EDITVERTICES, IDS_SCK_EDITVERTICES},
	{SCK_EDITTHINGS, IDS_SCK_EDITTHINGS},
	/*{SCK_EDIT3D, IDS_SCK_EDIT3D},*/
	{SCK_EDITSNAPTOGRID, IDS_SCK_EDITSNAPTOGRID},
	{SCK_FLIPLINEDEFS, IDS_SCK_FLIPLINEDEFS},
	{SCK_FLIPSIDEDEFS, IDS_SCK_FLIPSIDEDEFS},
	{SCK_SPLITLINEDEFS, IDS_SCK_SPLITLINEDEFS},
	{SCK_JOINSECTORS, IDS_SCK_JOINSECTORS},
	{SCK_MERGESECTORS, IDS_SCK_MERGESECTORS},
	{SCK_UNDO, IDS_SCK_UNDO},
	{SCK_REDO, IDS_SCK_REDO},
	{SCK_FLIPHORIZ, IDS_SCK_FLIPHORIZ},
	{SCK_FLIPVERT, IDS_SCK_FLIPVERT},
	{SCK_COPY, IDS_SCK_COPY},
	{SCK_PASTE, IDS_SCK_PASTE},
	{SCK_SAVEAS, IDS_SCK_SAVEAS},
	{SCK_INCFLOOR, IDS_SCK_INCFLOOR},
	{SCK_DECFLOOR, IDS_SCK_DECFLOOR},
	{SCK_INCCEIL, IDS_SCK_INCCEIL},
	{SCK_DECCEIL, IDS_SCK_DECCEIL},
	{SCK_INCLIGHT, IDS_SCK_INCLIGHT},
	{SCK_DECLIGHT, IDS_SCK_DECLIGHT},
	{SCK_INCTHINGZ, IDS_SCK_INCTHINGZ},
	{SCK_DECTHINGZ, IDS_SCK_DECTHINGZ},
	{SCK_SNAPSELECTION, IDS_SCK_SNAPSELECTION},
	{SCK_GRADIENTFLOORS, IDS_SCK_GRADIENTFLOORS},
	{SCK_GRADIENTCEILINGS, IDS_SCK_GRADIENTCEILINGS},
	{SCK_GRADIENTBRIGHTNESS, IDS_SCK_GRADIENTBRIGHTNESS},
	{SCK_INSERT, IDS_SCK_INSERT},
	{SCK_DCKINSERT, IDS_SCK_DCKINSERT},
	{SCK_EDITCUT, IDS_SCK_EDITCUT},
	{SCK_EDITDELETE, IDS_SCK_EDITDELETE},
	{SCK_CANCEL, IDS_SCK_CANCEL},
	{SCK_GRADIENTTHINGZ, IDS_SCK_GRADIENTTHINGZ},
	{SCK_ROTATE, IDS_SCK_ROTATE},
	{SCK_RESIZE, IDS_SCK_RESIZE},
	{SCK_THINGROTACW, IDS_SCK_THINGROTACW},
	{SCK_THINGROTCW, IDS_SCK_THINGROTCW},
	{SCK_SELECTALL, IDS_SCK_SELECTALL},
	{SCK_SELECTNONE, IDS_SCK_SELECTNONE},
	{SCK_INVERTSELECTION, IDS_SCK_INVERTSELECTION},
	{SCK_COPYPROPS, IDS_SCK_COPYPROPS},
	{SCK_PASTEPROPS, IDS_SCK_PASTEPROPS},
	{SCK_GRIDINC, IDS_SCK_GRIDINC},
	{SCK_GRIDDEC, IDS_SCK_GRIDDEC},
	{SCK_FIND, IDS_SCK_FIND},
	{SCK_REPLACE, IDS_SCK_REPLACE},
	{SCK_NEW, IDS_SCK_NEW},
	{SCK_OPEN, IDS_SCK_OPEN},
	{SCK_SAVE, IDS_SCK_SAVE},
	{SCK_MAPOPTIONS, IDS_SCK_MAPOPTIONS},
	{SCK_MOUSEROTATE, IDS_SCK_MOUSEROTATE},
	{SCK_STITCHVERTICES, IDS_SCK_STITCHVERTICES},
	{SCK_SCROLL_UP, IDS_SCK_SCROLL_UP},
	{SCK_SCROLL_DOWN, IDS_SCK_SCROLL_DOWN},
	{SCK_SCROLL_LEFT, IDS_SCK_SCROLL_LEFT},
	{SCK_SCROLL_RIGHT, IDS_SCK_SCROLL_RIGHT},
	{SCK_TEST, IDS_SCK_TEST},
	{SCK_QUICKTEST, IDS_SCK_QUICKTEST},
	{SCK_IDENTSECTORS, IDS_SCK_IDENTSECTORS},
	{SCK_CENTREVIEWSEL, IDS_SCK_CENTREVIEWSEL},
	{SCK_MAKESINGLE, IDS_SCK_MAKESINGLE},
	{SCK_MAKEDOUBLE, IDS_SCK_MAKEDOUBLE},
	{SCK_REMOVEINTERIOR, IDS_SCK_REMOVEINTERIOR},
	{SCK_RESOLVESEC, IDS_SCK_RESOLVESEC},
	{SCK_BACKTRACKDRAW, IDS_SCK_BACKTRACKDRAW},
	{SCK_SELECTTAGDUAL, IDS_SCK_SELECTTAGDUAL},
	{SCK_EDITFOF, IDS_SCK_EDITFOF},
	{SCK_QUICKDRAG, IDS_SCK_QUICKDRAG},
	{SCK_QUICKFOF, IDS_SCK_QUICKFOF},
	{SCK_OPTIONS, IDS_SCK_OPTIONS},
	{SCK_ERRORCHECKER, IDS_SCK_ERRORCHECKER},
	{SCK_PROPERTIES, IDS_SCK_PROPERTIES},
	{SCK_TOGGLEVXSNAP, IDS_SCK_TOGGLEVXSNAP},
	{SCK_TOGGLELINESNAP, IDS_SCK_TOGGLELINESNAP},
	{SCK_TOGGLEAUTOSTITCH, IDS_SCK_TOGGLEAUTOSTITCH},
	{SCK_EDITHEADER, IDS_SCK_EDITHEADER},
	{SCK_FIXMISSTEX, IDS_SCK_FIXMISSTEX},
	{SCK_AUTOALIGN, IDS_SCK_AUTOALIGN},
	{SCK_DELUNUSEDVX, IDS_SCK_DELUNUSEDVX},
	{SCK_LOCKDRAGGING, IDS_SCK_LOCKDRAGGING},
};
/* _SCK_ */

const struct SPECIALSHORTCUTS {int iStringID, iKeyCode;} g_specialshortcuts[] =
{
	{IDS_SHORTCUTS_WUP, MOUSE_SCROLL_UP},
	{IDS_SHORTCUTS_WDOWN, MOUSE_SCROLL_DOWN},
	{IDS_SHORTCUTS_ENTER, VK_RETURN},
	{IDS_SHORTCUTS_BACKSPACE, VK_BACK},
	{IDS_SHORTCUTS_TAB, VK_TAB},
	{IDS_SHORTCUTS_ESCAPE, VK_ESCAPE},
	{IDS_SHORTCUTS_SPACE, VK_SPACE},
	{IDS_SHORTCUTS_DELETE, VK_DELETE}
};



static INT_PTR CALLBACK OptionsGeneralPropProc(HWND hwndPropPage, UINT uiMsg, WPARAM wParam, LPARAM lParam);
static INT_PTR CALLBACK OptionsAppearancePropProc(HWND hwndPropPage, UINT uiMsg, WPARAM wParam, LPARAM lParam);
static INT_PTR CALLBACK OptionsDefaultsPropProc(HWND hwndPropPage, UINT uiMsg, WPARAM wParam, LPARAM lParam);
static INT_PTR CALLBACK OptionsGameConfigsPropProc(HWND hwndPropPage, UINT uiMsg, WPARAM wParam, LPARAM lParam);
static INT_PTR CALLBACK OptionsShortcutsPropProc(HWND hwndPropPage, UINT uiMsg, WPARAM wParam, LPARAM lParam);
static INT_PTR CALLBACK OptionsAdvancedPropProc(HWND hwndPropPage, UINT uiMsg, WPARAM wParam, LPARAM lParam);
static void UpdateSwatch(HWND hwndColourList, int iListIndex, RGBQUAD *lprgbqPalette);
static void ChooseRendererColour(HWND hwndPropPage, HWND hwndColourList, int iListIndex, RGBQUAD *lprgbqPalette);
static void SetShortcutControlsFromList(HWND hwndPropPage, HWND hwndShortcutList, int iIndex, int *lpiShortcutCodes);
static void EnableAppropriateShortcutControls(HWND hwndPropPage);
static void DisableAllShortcutControls(HWND hwndPropPage);
static int GetKeycodeFromControls(HWND hwndPropPage);
static void UpdateAssignmentControls(HWND hwndPropPage, int *lpiShortcutCodes);
static void SetListItemShortcutText(HWND hwndShortcutList, int iIndex, int iShortcutID, int *lpiShortcutCodes);



/* ShowOptions
 *   Shows the Options dialogue.
 *
 * Parameters:
 *   None.
 *
 * Return value: BOOL
 *   TRUE if the user OKed to exit the dialogue; FALSE if cancelled.
 */
BOOL ShowOptions(void)
{
	PROPSHEETHEADER		psh;
	PROPSHEETPAGE		psp[OPTDLG_NUMPAGES];
	int					i;
	BOOL				bOptionsChanged;


	/* Build the info for the individual pages. */

	psp[0].pfnDlgProc = OptionsGeneralPropProc;
	psp[0].pszTemplate = MAKEINTRESOURCE(IDD_PP_OPT_GENERAL);

	psp[1].pfnDlgProc = OptionsAppearancePropProc;
	psp[1].pszTemplate = MAKEINTRESOURCE(IDD_PP_OPT_APPEARANCE);

	psp[2].pfnDlgProc = OptionsDefaultsPropProc;
	psp[2].pszTemplate = MAKEINTRESOURCE(IDD_PP_OPT_DEFAULTS);

	psp[3].pfnDlgProc = OptionsGameConfigsPropProc;
	psp[3].pszTemplate = MAKEINTRESOURCE(IDD_PP_OPT_GAMECONFIGS);

	psp[4].pfnDlgProc = OptionsShortcutsPropProc;
	psp[4].pszTemplate = MAKEINTRESOURCE(IDD_PP_OPT_SHORTCUTS);

	psp[5].pfnDlgProc = OptionsAdvancedPropProc;
	psp[5].pszTemplate = MAKEINTRESOURCE(IDD_PP_OPT_ADVANCED);


	/* Fill in fields common to all pages. */
	for(i = 0; i < OPTDLG_NUMPAGES; i++)
	{
		psp[i].dwSize = sizeof(PROPSHEETPAGE);
		psp[i].dwFlags = /*PSP_HASHELP */ 0;
		psp[i].lParam = (LONG)&bOptionsChanged;
		psp[i].hInstance = g_hInstance;
	}

	/* Properties affecting the whole dialogue. */
	psh.dwSize = sizeof(psh);
	psh.dwFlags = PSH_HASHELP | PSH_NOAPPLYNOW | PSH_PROPSHEETPAGE | PSH_USECALLBACK;
	psh.hwndParent = g_hwndMain;
	psh.nPages = OPTDLG_NUMPAGES;
	psh.nStartPage = 0;
	psh.ppsp = psp;
	psh.pszCaption = MAKEINTRESOURCE(IDS_OPTCAPTION);
	psh.hInstance = g_hInstance;
	psh.pfnCallback = PropSheetCentreCallback;

	/* Assume we cancelled. */
	bOptionsChanged = FALSE;

	/* Go! */
	PropertySheet(&psh);

	/* Did we change any options? */
	return bOptionsChanged;
}



/* OptionsGeneralPropProc
 *   Dialogue proc for General page of Options dialogue.
 *
 * Parameters:
 *   HWND	hwndPropPage	Property page window handle.
 *   UINT	uiMsg			Message identifier.
 *   WPARAM wParam			Message-specific.
 *   LPARAM lParam			Message-specific.
 *
 * Return value: INT_PTR
 *   TRUE if the message was processed; FALSE otherwise.
 */
static INT_PTR CALLBACK OptionsGeneralPropProc(HWND hwndPropPage, UINT uiMsg, WPARAM wParam, LPARAM lParam)
{
	static BOOL *s_lpbOptionsChanged = NULL;

	UNREFERENCED_PARAMETER(wParam);

	switch(uiMsg)
	{
	case WM_INITDIALOG:
		{
			UDACCEL udaccel[2];

			/* Get address of flag in which to indicate whether we changed
			 * anything.
			 */
			s_lpbOptionsChanged = (BOOL*)((PROPSHEETPAGE*)lParam)->lParam;

			/* Set up the spinners. */

			udaccel[0].nInc = 1;
			udaccel[0].nSec = 0;
			udaccel[1].nInc = 8;
			udaccel[1].nSec = 2;

			SendDlgItemMessage(hwndPropPage, IDC_SPIN_AUTOSTITCHDISTANCE, UDM_SETRANGE32, OPT_MIN_AUTOSTITCHDISTANCE, OPT_MAX_AUTOSTITCHDISTANCE);
			SendDlgItemMessage(hwndPropPage, IDC_SPIN_AUTOSTITCHDISTANCE, UDM_SETACCEL, NUM_ELEMENTS(udaccel), (LPARAM)udaccel);

			SendDlgItemMessage(hwndPropPage, IDC_SPIN_LINESPLITDISTANCE, UDM_SETRANGE32, OPT_MIN_LINESPLITDISTANCE, OPT_MAX_LINESPLITDISTANCE);
			SendDlgItemMessage(hwndPropPage, IDC_SPIN_LINESPLITDISTANCE, UDM_SETACCEL, NUM_ELEMENTS(udaccel), (LPARAM)udaccel);

			SendDlgItemMessage(hwndPropPage, IDC_SPIN_VERTEXSNAPDISTANCE, UDM_SETRANGE32, OPT_MIN_VERTEXSNAPDISTANCE, OPT_MAX_VERTEXSNAPDISTANCE);
			SendDlgItemMessage(hwndPropPage, IDC_SPIN_VERTEXSNAPDISTANCE, UDM_SETACCEL, NUM_ELEMENTS(udaccel), (LPARAM)udaccel);

			SendDlgItemMessage(hwndPropPage, IDC_SPIN_SCROLLSPEED, UDM_SETRANGE32, OPT_MIN_SCROLLSPEED, OPT_MAX_SCROLLSPEED);
			SendDlgItemMessage(hwndPropPage, IDC_SPIN_SCROLLSPEED, UDM_SETACCEL, NUM_ELEMENTS(udaccel), (LPARAM)udaccel);

			SendDlgItemMessage(hwndPropPage, IDC_SPIN_ZOOMSPEED, UDM_SETRANGE32, OPT_MIN_ZOOMSPEED, OPT_MAX_ZOOMSPEED);
			SendDlgItemMessage(hwndPropPage, IDC_SPIN_ZOOMSPEED, UDM_SETACCEL, NUM_ELEMENTS(udaccel), (LPARAM)udaccel);

			SendDlgItemMessage(hwndPropPage, IDC_SPIN_DRAGTHRESHOLD, UDM_SETRANGE32, OPT_MIN_DRAGTHRESHOLD, OPT_MAX_DRAGTHRESHOLD);
			SendDlgItemMessage(hwndPropPage, IDC_SPIN_DRAGTHRESHOLD, UDM_SETACCEL, NUM_ELEMENTS(udaccel), (LPARAM)udaccel);


			/* Initialise the edit boxes. */
			SetDlgItemInt(hwndPropPage, IDC_EDIT_AUTOSTITCHDISTANCE, ConfigGetInteger(g_lpcfgMain, OPT_AUTOSTITCHDISTANCE), FALSE);
			SetDlgItemInt(hwndPropPage, IDC_EDIT_LINESPLITDISTANCE, ConfigGetInteger(g_lpcfgMain, OPT_LINESPLITDISTANCE), FALSE);
			SetDlgItemInt(hwndPropPage, IDC_EDIT_VERTEXSNAPDISTANCE, ConfigGetInteger(g_lpcfgMain, OPT_VERTEXSNAPDISTANCE), FALSE);
			SetDlgItemInt(hwndPropPage, IDC_EDIT_LINESNAPDISTANCE, ConfigGetInteger(g_lpcfgMain, OPT_LINESNAPDISTANCE), FALSE);
			SetDlgItemInt(hwndPropPage, IDC_EDIT_SCROLLSPEED, ConfigGetInteger(g_lpcfgMain, OPT_SCROLLSPEED), FALSE);
			SetDlgItemInt(hwndPropPage, IDC_EDIT_ZOOMSPEED, ConfigGetInteger(g_lpcfgMain, OPT_ZOOMSPEED), FALSE);
			SetDlgItemInt(hwndPropPage, IDC_EDIT_DRAGTHRESHOLD, ConfigGetInteger(g_lpcfgMain, OPT_DRAGTHRESHOLD), FALSE);


			/* Initialise checkboxes and radio buttons. */
			CheckDlgButton(hwndPropPage, IDC_CHECK_AUTOCOMPLETETEX, ConfigGetInteger(g_lpcfgMain, OPT_AUTOCOMPLETETEX) ? BST_CHECKED : BST_UNCHECKED);
			CheckDlgButton(hwndPropPage, IDC_CHECK_CONTEXTCROSS, ConfigGetInteger(g_lpcfgMain, OPT_CONTEXTCROSS) ? BST_CHECKED : BST_UNCHECKED);
			CheckDlgButton(hwndPropPage, IDC_CHECK_DETAILSANY, ConfigGetInteger(g_lpcfgMain, OPT_DETAILSANY) ? BST_CHECKED : BST_UNCHECKED);
			CheckDlgButton(hwndPropPage, IDC_CHECK_NEWSECTORDIALOGUE, ConfigGetInteger(g_lpcfgMain, OPT_NEWSECTORDIALOGUE) ? BST_CHECKED : BST_UNCHECKED);
			CheckDlgButton(hwndPropPage, IDC_CHECK_NEWTHINGDIALOGUE, ConfigGetInteger(g_lpcfgMain, OPT_NEWTHINGDIALOGUE) ? BST_CHECKED : BST_UNCHECKED);
			CheckDlgButton(hwndPropPage, IDC_CHECK_NOTHINGDESELECTS, ConfigGetInteger(g_lpcfgMain, OPT_NOTHINGDESELECTS) ? BST_CHECKED : BST_UNCHECKED);
			CheckDlgButton(hwndPropPage, IDC_CHECK_TEXBROWSERTAB, ConfigGetInteger(g_lpcfgMain, OPT_TEXBROWSERTAB) ? BST_CHECKED : BST_UNCHECKED);
			CheckDlgButton(hwndPropPage, IDC_CHECK_USEDONLY, ConfigGetInteger(g_lpcfgMain, OPT_USEDONLY) ? BST_CHECKED : BST_UNCHECKED);
			CheckDlgButton(hwndPropPage, IDC_CHECK_ZOOMMOUSE, ConfigGetInteger(g_lpcfgMain, OPT_ZOOMMOUSE) ? BST_CHECKED : BST_UNCHECKED);
			CheckDlgButton(hwndPropPage, IDC_CHECK_REMEMBERWINDOWPOS, ConfigGetInteger(g_lpcfgMain, OPT_REMEMBERWINDOWPOS) ? BST_CHECKED : BST_UNCHECKED);
			CheckDlgButton(hwndPropPage, IDC_CHECK_EFFECTNUMBERS, ConfigGetInteger(g_lpcfgMain, OPT_EFFECTNUMBERS) ? BST_CHECKED : BST_UNCHECKED);
			CheckRadioButton(hwndPropPage, IDC_RADIO_ADDITIVESELECT, IDC_RADIO_EXCLUSIVESELECT, ConfigGetInteger(g_lpcfgMain, OPT_ADDITIVESELECT) ?  IDC_RADIO_ADDITIVESELECT: IDC_RADIO_EXCLUSIVESELECT);
			CheckRadioButton(hwndPropPage, IDC_RADIO_CONTEXTMENUS, IDC_RADIO_NOCONTEXT, ConfigGetInteger(g_lpcfgMain, OPT_CONTEXTMENUS) ?  IDC_RADIO_CONTEXTMENUS: IDC_RADIO_NOCONTEXT);
		}

		/* Let the system set the focus. */
		return TRUE;

	case WM_NOTIFY:
		switch(((LPNMHDR)lParam)->code)
		{
		case PSN_APPLY:

			/* Inform the caller that we applied. */
			*s_lpbOptionsChanged = TRUE;

			/* Get values from the edit boxes. */
			ConfigSetInteger(g_lpcfgMain, OPT_AUTOSTITCHDISTANCE, GetDlgItemInt(hwndPropPage, IDC_EDIT_AUTOSTITCHDISTANCE, NULL, FALSE));
			ConfigSetInteger(g_lpcfgMain, OPT_LINESPLITDISTANCE, GetDlgItemInt(hwndPropPage, IDC_EDIT_LINESPLITDISTANCE, NULL, FALSE));
			ConfigSetInteger(g_lpcfgMain, OPT_VERTEXSNAPDISTANCE, GetDlgItemInt(hwndPropPage, IDC_EDIT_VERTEXSNAPDISTANCE, NULL, FALSE));
			ConfigSetInteger(g_lpcfgMain, OPT_LINESNAPDISTANCE, GetDlgItemInt(hwndPropPage, IDC_EDIT_LINESNAPDISTANCE, NULL, FALSE));
			ConfigSetInteger(g_lpcfgMain, OPT_SCROLLSPEED, GetDlgItemInt(hwndPropPage, IDC_EDIT_SCROLLSPEED, NULL, FALSE));
			ConfigSetInteger(g_lpcfgMain, OPT_ZOOMSPEED, GetDlgItemInt(hwndPropPage, IDC_EDIT_ZOOMSPEED, NULL, FALSE));
			ConfigSetInteger(g_lpcfgMain, OPT_DRAGTHRESHOLD, GetDlgItemInt(hwndPropPage, IDC_EDIT_DRAGTHRESHOLD, NULL, FALSE));


			/* Get states of checkboxes and radio buttons. */
			ConfigSetInteger(g_lpcfgMain, OPT_AUTOCOMPLETETEX, IsDlgButtonChecked(hwndPropPage, IDC_CHECK_AUTOCOMPLETETEX) == BST_CHECKED);
			ConfigSetInteger(g_lpcfgMain, OPT_DETAILSANY, IsDlgButtonChecked(hwndPropPage, IDC_CHECK_DETAILSANY) == BST_CHECKED);
			ConfigSetInteger(g_lpcfgMain, OPT_CONTEXTCROSS, IsDlgButtonChecked(hwndPropPage, IDC_CHECK_CONTEXTCROSS) == BST_CHECKED);
			ConfigSetInteger(g_lpcfgMain, OPT_NEWSECTORDIALOGUE, IsDlgButtonChecked(hwndPropPage, IDC_CHECK_NEWSECTORDIALOGUE) == BST_CHECKED);
			ConfigSetInteger(g_lpcfgMain, OPT_NEWTHINGDIALOGUE, IsDlgButtonChecked(hwndPropPage, IDC_CHECK_NEWTHINGDIALOGUE) == BST_CHECKED);
			ConfigSetInteger(g_lpcfgMain, OPT_NOTHINGDESELECTS, IsDlgButtonChecked(hwndPropPage, IDC_CHECK_NOTHINGDESELECTS) == BST_CHECKED);
			ConfigSetInteger(g_lpcfgMain, OPT_TEXBROWSERTAB, IsDlgButtonChecked(hwndPropPage, IDC_CHECK_TEXBROWSERTAB) == BST_CHECKED);
			ConfigSetInteger(g_lpcfgMain, OPT_USEDONLY, IsDlgButtonChecked(hwndPropPage, IDC_CHECK_USEDONLY) == BST_CHECKED);
			ConfigSetInteger(g_lpcfgMain, OPT_ZOOMMOUSE, IsDlgButtonChecked(hwndPropPage, IDC_CHECK_ZOOMMOUSE) == BST_CHECKED);
			ConfigSetInteger(g_lpcfgMain, OPT_REMEMBERWINDOWPOS, IsDlgButtonChecked(hwndPropPage, IDC_CHECK_REMEMBERWINDOWPOS) == BST_CHECKED);
			ConfigSetInteger(g_lpcfgMain, OPT_EFFECTNUMBERS, IsDlgButtonChecked(hwndPropPage, IDC_CHECK_EFFECTNUMBERS) == BST_CHECKED);
			ConfigSetInteger(g_lpcfgMain, OPT_ADDITIVESELECT, IsDlgButtonChecked(hwndPropPage, IDC_RADIO_ADDITIVESELECT) == BST_CHECKED);
			ConfigSetInteger(g_lpcfgMain, OPT_CONTEXTMENUS, IsDlgButtonChecked(hwndPropPage, IDC_RADIO_CONTEXTMENUS) == BST_CHECKED);

			SetWindowLong(hwndPropPage, DWL_MSGRESULT, PSNRET_NOERROR);
			return TRUE;

		case PSN_KILLACTIVE:

			/* Bound the values of the edit boxes. */
			BoundEditBox(GetDlgItem(hwndPropPage, IDC_EDIT_AUTOSTITCHDISTANCE), OPT_MIN_AUTOSTITCHDISTANCE, OPT_MAX_AUTOSTITCHDISTANCE, FALSE, FALSE);
			BoundEditBox(GetDlgItem(hwndPropPage, IDC_EDIT_LINESPLITDISTANCE), OPT_MIN_LINESPLITDISTANCE, OPT_MAX_LINESPLITDISTANCE, FALSE, FALSE);
			BoundEditBox(GetDlgItem(hwndPropPage, IDC_EDIT_VERTEXSNAPDISTANCE), OPT_MIN_VERTEXSNAPDISTANCE, OPT_MAX_VERTEXSNAPDISTANCE, FALSE, FALSE);
			BoundEditBox(GetDlgItem(hwndPropPage, IDC_EDIT_LINESNAPDISTANCE), OPT_MIN_VERTEXSNAPDISTANCE, OPT_MAX_LINESNAPDISTANCE, FALSE, FALSE);
			BoundEditBox(GetDlgItem(hwndPropPage, IDC_EDIT_SCROLLSPEED), OPT_MIN_SCROLLSPEED, OPT_MAX_SCROLLSPEED, FALSE, FALSE);
			BoundEditBox(GetDlgItem(hwndPropPage, IDC_EDIT_ZOOMSPEED), OPT_MIN_ZOOMSPEED, OPT_MAX_ZOOMSPEED, FALSE, FALSE);
			BoundEditBox(GetDlgItem(hwndPropPage, IDC_EDIT_DRAGTHRESHOLD), OPT_MIN_DRAGTHRESHOLD, OPT_MAX_DRAGTHRESHOLD, FALSE, FALSE);

			SetWindowLong(hwndPropPage, DWL_MSGRESULT, FALSE);
			return TRUE;
		}
	}

	/* Didn't process message. */
	return FALSE;
}


/* OptionsAppearancePropProc
 *   Dialogue proc for the Appearance page of Options dialogue.
 *
 * Parameters:
 *   HWND	hwndPropPage	Property page window handle.
 *   UINT	uiMsg			Message identifier.
 *   WPARAM wParam			Message-specific.
 *   LPARAM lParam			Message-specific.
 *
 * Return value: INT_PTR
 *   TRUE if the message was processed; FALSE otherwise.
 */
static INT_PTR CALLBACK OptionsAppearancePropProc(HWND hwndPropPage, UINT uiMsg, WPARAM wParam, LPARAM lParam)
{
	static BOOL *s_lpbOptionsChanged = NULL;
	static RGBQUAD s_rgbqPalette[CLR_MAX];

	UNREFERENCED_PARAMETER(wParam);

	switch(uiMsg)
	{
	case WM_INITDIALOG:
		{
			UDACCEL udaccel[2];
			LVCOLUMN lvcol;
			HWND hwndColourList = GetDlgItem(hwndPropPage, IDC_LIST_COLOURS);
			HIMAGELIST himl;
			int i;
			int cxSwatch, cySwatch, cxText;
			RECT rcItem, rcList;
			DWORD dwLineColourFlags;

			const struct {int iColourID, iStringID;} colourentries[] =
			{
				{CLR_BACKGROUND,		IDS_CLR_BACKGROUND},
				{CLR_VERTEX,			IDS_CLR_VERTEX},
				{CLR_VERTEXSELECTED,	IDS_CLR_VERTEXSELECTED},
				{CLR_VERTEXHIGHLIGHT,	IDS_CLR_VERTEXHIGHLIGHT},
				{CLR_LINE,				IDS_CLR_LINE},
				{CLR_LINEDOUBLE,		IDS_CLR_LINEDOUBLE},
				{CLR_LINESPECIAL,		IDS_CLR_LINESPECIAL},
				{CLR_LINESPECIALDOUBLE,	IDS_CLR_LINESPECIALDOUBLE},
				{CLR_LINESELECTED,		IDS_CLR_LINESELECTED},
				{CLR_LINEHIGHLIGHT,		IDS_CLR_LINEHIGHLIGHT},
				{CLR_SECTORTAG,			IDS_CLR_SECTORTAG},
				{CLR_THINGUNKNOWN,		IDS_CLR_THINGUNKNOWN},
				{CLR_THINGSELECTED,		IDS_CLR_THINGSELECTED},
				{CLR_THINGHIGHLIGHT,	IDS_CLR_THINGHIGHLIGHT},
				{CLR_GRID,				IDS_CLR_GRID},
				{CLR_GRID64,			IDS_CLR_GRID64},
				{CLR_LINEBLOCKSOUND,	IDS_CLR_LINEBLOCKSOUND},
				{CLR_MAPBOUNDARY,		IDS_CLR_MAPBOUNDARY},
				{CLR_AXES,				IDS_CLR_AXES},
				{CLR_ZEROHEIGHTLINE,	IDS_CLR_ZEROHEIGHTLINE},
				{CLR_TEXBROWSER,		IDS_CLR_TEXBROWSER},
				{CLR_LASSO,				IDS_CLR_LASSO},
				{CLR_NIGHTS,			IDS_CLR_NIGHTS},
			};


			/* Get address of flag in which to indicate whether we changed
			 * anything.
			 */
			s_lpbOptionsChanged = (BOOL*)((PROPSHEETPAGE*)lParam)->lParam;

			/* Set up the spinners. */

			udaccel[0].nInc = 1;
			udaccel[0].nSec = 0;
			udaccel[1].nInc = 8;
			udaccel[1].nSec = 2;

			SendDlgItemMessage(hwndPropPage, IDC_SPIN_INDICATORSIZE, UDM_SETRANGE32, OPT_MIN_INDICATORSIZE, OPT_MAX_INDICATORSIZE);
			SendDlgItemMessage(hwndPropPage, IDC_SPIN_INDICATORSIZE, UDM_SETACCEL, NUM_ELEMENTS(udaccel), (LPARAM)udaccel);

			SendDlgItemMessage(hwndPropPage, IDC_SPIN_VERTEXSIZE, UDM_SETRANGE32, OPT_MIN_VERTEXSIZE, OPT_MAX_VERTEXSIZE);
			SendDlgItemMessage(hwndPropPage, IDC_SPIN_VERTEXSIZE, UDM_SETACCEL, NUM_ELEMENTS(udaccel), (LPARAM)udaccel);


			/* Set up the colour list. */

			/* Create the column in the list view control. */
			lvcol.mask = LVCF_SUBITEM;

			lvcol.iSubItem = 0;
			ListView_InsertColumn(hwndColourList, 0, &lvcol);

			/* Set the required extended styles. */
			ListView_SetExtendedListViewStyleEx(hwndColourList, LVS_EX_FULLROWSELECT, LVS_EX_FULLROWSELECT);

			/* Make a local copy of the palette. */
			CopyMemory(s_rgbqPalette, g_rgbqPalette, sizeof(s_rgbqPalette));

			for(i = 0; i < (int)NUM_ELEMENTS(colourentries); i++)
			{
				TCHAR szColour[CCH_COLOUR_DESCRIPTION];
				LVITEM lvitem;

				LoadString(g_hInstance, colourentries[i].iStringID, szColour, NUM_ELEMENTS(szColour));

				lvitem.mask = LVIF_PARAM | LVIF_TEXT;
				lvitem.iSubItem = 0;
				lvitem.iItem = i;
				lvitem.lParam = colourentries[i].iColourID;
				lvitem.pszText = szColour;

				ListView_InsertItem(hwndColourList, &lvitem);
			}

			/* The list sorts itself, so colourentries is no longer very useful.
			 */

			/* Make the column big enough for the text. */
			ListView_SetColumnWidth(hwndColourList, 0, LVSCW_AUTOSIZE);
			cxText = ListView_GetColumnWidth(hwndColourList, 0);
			GetClientRect(hwndColourList, &rcList);
			cxSwatch = min(CX_SWATCH_MAX, rcList.right - rcList.left - cxText);

			/* Create the image-list. Observe that this has to be done after the
			 * list-view itself has been filled, as we need to know the
			 * dimensions. It'll be destroyed when the control's destroyed.
			 */
			ListView_GetItemRect(hwndColourList, 0, &rcItem, LVIR_BOUNDS);
			cySwatch = rcItem.bottom - rcItem.top - 2;
			himl = ImageList_Create(cxSwatch, cySwatch, ILC_COLOR24, 0, 0);
			ImageList_SetImageCount(himl, NUM_ELEMENTS(colourentries));
			ListView_SetImageList(hwndColourList, himl, LVSIL_SMALL);

			for(i = 0; i < (int)NUM_ELEMENTS(colourentries); i++)
			{
				LVITEM lvitem;

				UpdateSwatch(hwndColourList, i, s_rgbqPalette);

				lvitem.mask = LVIF_IMAGE;
				lvitem.iSubItem = 0;
				lvitem.iItem = i;
				lvitem.iImage = i;
				ListView_SetItem(hwndColourList, &lvitem);
			}

			/* Make the column big enough for the text *and* swatch. */
			ListView_SetColumnWidth(hwndColourList, 0, LVSCW_AUTOSIZE_USEHEADER);


			/* Initialise the edit boxes. */
			SetDlgItemInt(hwndPropPage, IDC_EDIT_INDICATORSIZE, g_rendopts.iIndicatorSize, FALSE);
			SetDlgItemInt(hwndPropPage, IDC_EDIT_VERTEXSIZE, g_rendopts.iVertexSize, FALSE);


			/* Initialise checkboxes. */
			CheckDlgButton(hwndPropPage, IDC_CHECK_SECMODEVERTICES, g_rendopts.bVerticesInSectorsMode ? BST_CHECKED : BST_UNCHECKED);
			CheckDlgButton(hwndPropPage, IDC_CHECK_LINEMODEVERTICES, g_rendopts.bVerticesInLinesMode ? BST_CHECKED : BST_UNCHECKED);
			dwLineColourFlags = ConfigGetInteger(g_lpcfgMain, OPT_LINECOLOURFLAGS);
			CheckDlgButton(hwndPropPage, IDC_CHECK_ZEROHEIGHT, (dwLineColourFlags & LCF_ZEROHEIGHT) ? BST_CHECKED : BST_UNCHECKED);
			CheckDlgButton(hwndPropPage, IDC_CHECK_FAUXIMPASSABLE, (dwLineColourFlags & LCF_FAUXIMPASSABLE) ? BST_CHECKED : BST_UNCHECKED);
			CheckDlgButton(hwndPropPage, IDC_CHECK_TAGBRETHREN, (dwLineColourFlags & LCF_TAGBRETHREN) ? BST_CHECKED : BST_UNCHECKED);
		}

		/* Let the system set the focus. */
		return TRUE;

	case WM_NOTIFY:
		switch(((LPNMHDR)lParam)->idFrom)
		{
		case IDC_LIST_COLOURS:
			{
				LPNMLISTVIEW lpnmlv = (LPNMLISTVIEW)lParam;

				switch(lpnmlv->hdr.code)
				{
				case NM_DBLCLK:
					ChooseRendererColour(hwndPropPage, lpnmlv->hdr.hwndFrom, lpnmlv->iItem, s_rgbqPalette);
					return TRUE;
				}
			}

			break;

		default:
			/* Property sheets don't have IDs. */
			switch(((LPNMHDR)lParam)->code)
			{
			case PSN_APPLY:
				{
					DWORD dwLineColourFlags = 0;

					/* Inform the caller that we applied. */
					*s_lpbOptionsChanged = TRUE;

					/* Get values from the edit boxes. */
					g_rendopts.iIndicatorSize = (GetDlgItemInt(hwndPropPage, IDC_EDIT_INDICATORSIZE, NULL, FALSE));
					g_rendopts.iVertexSize = (GetDlgItemInt(hwndPropPage, IDC_EDIT_VERTEXSIZE, NULL, FALSE));

					/* Get states of checkboxes and radio buttons. */
					g_rendopts.bVerticesInSectorsMode = (IsDlgButtonChecked(hwndPropPage, IDC_CHECK_SECMODEVERTICES) == BST_CHECKED);
					g_rendopts.bVerticesInLinesMode = (IsDlgButtonChecked(hwndPropPage, IDC_CHECK_LINEMODEVERTICES) == BST_CHECKED);

					if(IsDlgButtonChecked(hwndPropPage, IDC_CHECK_ZEROHEIGHT))
						dwLineColourFlags |= LCF_ZEROHEIGHT;

					if(IsDlgButtonChecked(hwndPropPage, IDC_CHECK_FAUXIMPASSABLE))
						dwLineColourFlags |= LCF_FAUXIMPASSABLE;

					if(IsDlgButtonChecked(hwndPropPage, IDC_CHECK_TAGBRETHREN))
						dwLineColourFlags |= LCF_TAGBRETHREN;

					ConfigSetInteger(g_lpcfgMain, OPT_LINECOLOURFLAGS, (int)dwLineColourFlags);

					/* Update the palette from our copy. */
					CopyMemory(g_rgbqPalette, s_rgbqPalette, sizeof(s_rgbqPalette));

					SetWindowLong(hwndPropPage, DWL_MSGRESULT, PSNRET_NOERROR);
				}

				return TRUE;

			case PSN_KILLACTIVE:

				/* Bound the values of the edit boxes. */
				BoundEditBox(GetDlgItem(hwndPropPage, IDC_EDIT_INDICATORSIZE), OPT_MIN_INDICATORSIZE, OPT_MAX_INDICATORSIZE, FALSE, FALSE);
				BoundEditBox(GetDlgItem(hwndPropPage, IDC_EDIT_VERTEXSIZE), OPT_MIN_VERTEXSIZE, OPT_MAX_VERTEXSIZE, FALSE, FALSE);

				SetWindowLong(hwndPropPage, DWL_MSGRESULT, FALSE);
				return TRUE;
			}
		}
	}

	/* Didn't process message. */
	return FALSE;
}


/* OptionsDefaultsPropProc
 *   Dialogue proc for the Defaults page of Options dialogue.
 *
 * Parameters:
 *   HWND	hwndPropPage	Property page window handle.
 *   UINT	uiMsg			Message identifier.
 *   WPARAM wParam			Message-specific.
 *   LPARAM lParam			Message-specific.
 *
 * Return value: INT_PTR
 *   TRUE if the message was processed; FALSE otherwise.
 */
static INT_PTR CALLBACK OptionsDefaultsPropProc(HWND hwndPropPage, UINT uiMsg, WPARAM wParam, LPARAM lParam)
{
	static BOOL *s_lpbOptionsChanged = NULL;

	switch(uiMsg)
	{
	case WM_INITDIALOG:
		{
			TCHAR szTexName[TEXNAME_BUFFER_LENGTH];
			CONFIG *lpcfgDefTex, *lpcfgDefSec;
			UDACCEL udaccelHeight[2], udaccelBrightness[2];

			/* Get address of flag in which to indicate whether we changed
			 * anything.
			 */
			s_lpbOptionsChanged = (BOOL*)((PROPSHEETPAGE*)lParam)->lParam;

			/* Get subsections of config. */
			lpcfgDefTex = ConfigGetSubsection(g_lpcfgMain, OPT_DEFAULTTEX);
			lpcfgDefSec = ConfigGetSubsection(g_lpcfgMain, OPT_DEFAULTSEC);


			/* Set up the edit boxes. */

			ConfigGetString(lpcfgDefTex, TEXT("upper"), szTexName, TEXNAME_BUFFER_LENGTH);
			SetDlgItemText(hwndPropPage, IDC_EDIT_UPPER, szTexName);

			ConfigGetString(lpcfgDefTex, TEXT("middle"), szTexName, TEXNAME_BUFFER_LENGTH);
			SetDlgItemText(hwndPropPage, IDC_EDIT_MIDDLE, szTexName);

			ConfigGetString(lpcfgDefTex, TEXT("lower"), szTexName, TEXNAME_BUFFER_LENGTH);
			SetDlgItemText(hwndPropPage, IDC_EDIT_LOWER, szTexName);

			ConfigGetString(lpcfgDefSec, TEXT("tceiling"), szTexName, TEXNAME_BUFFER_LENGTH);
			SetDlgItemText(hwndPropPage, IDC_EDIT_TCEIL, szTexName);

			ConfigGetString(lpcfgDefSec, TEXT("tfloor"), szTexName, TEXNAME_BUFFER_LENGTH);
			SetDlgItemText(hwndPropPage, IDC_EDIT_TFLOOR, szTexName);

			SetDlgItemInt(hwndPropPage, IDC_EDIT_HCEIL, ConfigGetInteger(lpcfgDefSec, TEXT("hceiling")), TRUE);
			SetDlgItemInt(hwndPropPage, IDC_EDIT_HFLOOR, ConfigGetInteger(lpcfgDefSec, TEXT("hfloor")), TRUE);
			SetDlgItemInt(hwndPropPage, IDC_EDIT_BRIGHTNESS, ConfigGetInteger(lpcfgDefSec, TEXT("brightness")), FALSE);


			/* Set up the spinners. */

			udaccelHeight[0].nInc = 8;
			udaccelHeight[0].nSec = 0;
			udaccelHeight[1].nInc = 64;
			udaccelHeight[1].nSec = 2;

			udaccelBrightness[0].nInc = 1;
			udaccelBrightness[0].nSec = 0;
			udaccelBrightness[1].nInc = 8;
			udaccelBrightness[1].nSec = 2;

			SendDlgItemMessage(hwndPropPage, IDC_SPIN_BRIGHTNESS, UDM_SETRANGE32, 0, 255);
			SendDlgItemMessage(hwndPropPage, IDC_SPIN_BRIGHTNESS, UDM_SETACCEL, NUM_ELEMENTS(udaccelBrightness), (LPARAM)udaccelBrightness);

			SendDlgItemMessage(hwndPropPage, IDC_SPIN_HCEIL, UDM_SETRANGE32, -32768, 32767);
			SendDlgItemMessage(hwndPropPage, IDC_SPIN_HCEIL, UDM_SETACCEL, NUM_ELEMENTS(udaccelHeight), (LPARAM)udaccelHeight);

			SendDlgItemMessage(hwndPropPage, IDC_SPIN_HFLOOR, UDM_SETRANGE32, -32768, 32767);
			SendDlgItemMessage(hwndPropPage, IDC_SPIN_HFLOOR, UDM_SETACCEL, NUM_ELEMENTS(udaccelHeight), (LPARAM)udaccelHeight);
		}

		/* Let the system set the focus. */
		return TRUE;

	case WM_COMMAND:

		switch(LOWORD(wParam))
		{
		case IDC_EDIT_HCEIL:
		case IDC_EDIT_HFLOOR:
			switch(HIWORD(wParam))
			{
			case EN_KILLFOCUS:
				/* Validation. */
				BoundEditBox((HWND)lParam, -32768, 32767, FALSE, FALSE);

				return TRUE;
			}

		case IDC_EDIT_BRIGHTNESS:
			switch(HIWORD(wParam))
			{
			case EN_KILLFOCUS:
				/* Validation. */
				BoundEditBox((HWND)lParam, 0, 255, FALSE, FALSE);

				return TRUE;
			}
		}

		/* Didn't process message. */
		break;

	case WM_NOTIFY:
		switch(((LPNMHDR)lParam)->code)
		{
		case PSN_APPLY:
			{
				TCHAR szTexName[TEXNAME_BUFFER_LENGTH];

				/* Get subsections of config. */
				CONFIG *lpcfgDefTex = ConfigGetSubsection(g_lpcfgMain, OPT_DEFAULTTEX);
				CONFIG *lpcfgDefSec = ConfigGetSubsection(g_lpcfgMain, OPT_DEFAULTSEC);

				/* Inform the caller that we applied. */
				*s_lpbOptionsChanged = TRUE;

				/* Set the values! */
				GetDlgItemText(hwndPropPage, IDC_EDIT_UPPER, szTexName, NUM_ELEMENTS(szTexName));
				ConfigSetString(lpcfgDefTex, TEXT("upper"), szTexName);

				GetDlgItemText(hwndPropPage, IDC_EDIT_MIDDLE, szTexName, NUM_ELEMENTS(szTexName));
				ConfigSetString(lpcfgDefTex, TEXT("middle"), szTexName);

				GetDlgItemText(hwndPropPage, IDC_EDIT_LOWER, szTexName, NUM_ELEMENTS(szTexName));
				ConfigSetString(lpcfgDefTex, TEXT("lower"), szTexName);

				GetDlgItemText(hwndPropPage, IDC_EDIT_TCEIL, szTexName, NUM_ELEMENTS(szTexName));
				ConfigSetString(lpcfgDefSec, TEXT("tceiling"), szTexName);

				GetDlgItemText(hwndPropPage, IDC_EDIT_TFLOOR, szTexName, NUM_ELEMENTS(szTexName));
				ConfigSetString(lpcfgDefSec, TEXT("tfloor"), szTexName);

				ConfigSetInteger(lpcfgDefSec, TEXT("hceiling"), GetDlgItemInt(hwndPropPage, IDC_EDIT_HCEIL, NULL, TRUE));
				ConfigSetInteger(lpcfgDefSec, TEXT("hfloor"), GetDlgItemInt(hwndPropPage, IDC_EDIT_HFLOOR, NULL, TRUE));
				ConfigSetInteger(lpcfgDefSec, TEXT("brightness"), GetDlgItemInt(hwndPropPage, IDC_EDIT_BRIGHTNESS, NULL, FALSE));

				SetWindowLong(hwndPropPage, DWL_MSGRESULT, PSNRET_NOERROR);
			}

			return TRUE;

		case PSN_KILLACTIVE:

			/* Bound the values of the edit boxes. */
			BoundEditBox(GetDlgItem(hwndPropPage, IDC_EDIT_HCEIL), -32768, 32767, FALSE, FALSE);
			BoundEditBox(GetDlgItem(hwndPropPage, IDC_EDIT_HFLOOR), -32768, 32767, FALSE, FALSE);
			BoundEditBox(GetDlgItem(hwndPropPage, IDC_EDIT_BRIGHTNESS), 0, 255, FALSE, FALSE);

			SetWindowLong(hwndPropPage, DWL_MSGRESULT, FALSE);
			return TRUE;
		}

		/* Didn't process message. */
		break;
	}

	/* Didn't process message. */
	return FALSE;
}


/* OptionsGameConfigsPropProc
 *   Dialogue proc for the Game Configurations page of Options dialogue.
 *
 * Parameters:
 *   HWND	hwndPropPage	Property page window handle.
 *   UINT	uiMsg			Message identifier.
 *   WPARAM wParam			Message-specific.
 *   LPARAM lParam			Message-specific.
 *
 * Return value: INT_PTR
 *   TRUE if the message was processed; FALSE otherwise.
 */
static INT_PTR CALLBACK OptionsGameConfigsPropProc(HWND hwndPropPage, UINT uiMsg, WPARAM wParam, LPARAM lParam)
{
	static BOOL *s_lpbOptionsChanged = NULL;
	static CONFIG *s_lpcfgGameConfigs = NULL;
	static CONFIG *s_lpcfgCurrentConfig = NULL;

	switch(uiMsg)
	{
	case WM_INITDIALOG:
		/* Get address of flag in which to indicate whether we changed
		 * anything.
		 */
		s_lpbOptionsChanged = (BOOL*)((PROPSHEETPAGE*)lParam)->lParam;

		/* Make a local copy of the game configs subsection. Remember, this only
		 * contains entries for those configs who have had their options set
		 * already -- we'll need to add any new ones.
		 */
		s_lpcfgGameConfigs = ConfigDuplicate(ConfigGetSubsection(g_lpcfgMain, OPT_GAMECONFIGS));

		/* Populate the listbox. */
		AddMapConfigsToList(GetDlgItem(hwndPropPage, IDC_LIST_GAMECONFIGS), AMC_LISTBOX);

		/* Enable autocomplete on the binary and IWAD edit boxes. */
		SHAutoComplete(GetDlgItem(hwndPropPage, IDC_EDIT_BINARY), SHACF_FILESYS_ONLY);
		SHAutoComplete(GetDlgItem(hwndPropPage, IDC_EDIT_IWAD), SHACF_FILESYS_ONLY);

		/* Let the system set the focus. */
		return TRUE;

	case WM_COMMAND:

		switch(LOWORD(wParam))
		{
		case IDC_BUTTON_BROWSEBINARY:
			{
				TCHAR szFilename[MAX_PATH];
				GetDlgItemText(hwndPropPage, IDC_EDIT_BINARY, szFilename, NUM_ELEMENTS(szFilename));
				if(BrowseForBinary(szFilename, NUM_ELEMENTS(szFilename)))
					SetDlgItemText(hwndPropPage, IDC_EDIT_BINARY, szFilename);
			}

			return TRUE;

		case IDC_BUTTON_BROWSEIWAD:
			{
				TCHAR szFilename[MAX_PATH];
				GetDlgItemText(hwndPropPage, IDC_EDIT_IWAD, szFilename, NUM_ELEMENTS(szFilename));
				if(BrowseForIWAD(szFilename, NUM_ELEMENTS(szFilename)))
					SetDlgItemText(hwndPropPage, IDC_EDIT_IWAD, szFilename);
			}

			return TRUE;

		case IDC_EDIT_BINARY:
		case IDC_EDIT_IWAD:
			switch(HIWORD(wParam))
			{
			case EN_CHANGE:
				if(s_lpcfgCurrentConfig)
				{
					/* Update the config. */
					DWORD cchBuffer = GetWindowTextLength((HWND)lParam) + 1;
					LPTSTR szBuffer = (LPTSTR)ProcHeapAlloc(cchBuffer * sizeof(TCHAR));

					GetWindowText((HWND)lParam, szBuffer, cchBuffer);
					ConfigSetString(s_lpcfgCurrentConfig, (LOWORD(wParam) == IDC_EDIT_BINARY) ? GAMECFG_BINARY : GAMECFG_IWAD, szBuffer);

					ProcHeapFree(szBuffer);
				}

				return TRUE;
			}

			break;

		case IDC_LIST_GAMECONFIGS:
			if(HIWORD(wParam) == LBN_SELCHANGE)
			{
				int iListIndex = SendMessage((HWND)lParam, LB_GETCURSEL, 0, 0);

				if(iListIndex >= 0)
				{
					CONFIG *lpcfgMap = (CONFIG*)SendMessage((HWND)lParam, LB_GETITEMDATA, iListIndex, 0);
					DWORD cchBuffer = ConfigGetStringLength(lpcfgMap, MAPCFG_ID) + 1;
					LPTSTR szBuffer = (LPTSTR)ProcHeapAlloc(cchBuffer * sizeof(TCHAR));

					/* Get the ID for this game. */
					ConfigGetString(lpcfgMap, MAPCFG_ID, szBuffer, cchBuffer);

					/* Get the options if they exist, or create them if they don't. */
					s_lpcfgCurrentConfig = ConfigGetOrAddSubsection(s_lpcfgGameConfigs, szBuffer);
					ProcHeapFree(szBuffer);

					/* Enable controls. */
					EnableWindow(GetDlgItem(hwndPropPage, IDC_STATIC_BINARY), TRUE);
					EnableWindow(GetDlgItem(hwndPropPage, IDC_STATIC_IWAD), TRUE);
					EnableWindow(GetDlgItem(hwndPropPage, IDC_EDIT_BINARY), TRUE);
					EnableWindow(GetDlgItem(hwndPropPage, IDC_EDIT_IWAD), TRUE);
					EnableWindow(GetDlgItem(hwndPropPage, IDC_BUTTON_BROWSEBINARY), TRUE);
					EnableWindow(GetDlgItem(hwndPropPage, IDC_BUTTON_BROWSEIWAD), TRUE);

					/* Allocate a buffer big enough to hold either the IWAD or
					 * binary from the config.
					 */
					cchBuffer = max(ConfigGetStringLength(s_lpcfgCurrentConfig, GAMECFG_IWAD), ConfigGetStringLength(s_lpcfgCurrentConfig, GAMECFG_BINARY)) + 1;
					szBuffer = (LPTSTR)ProcHeapAlloc(cchBuffer * sizeof(TCHAR));

					/* Update the edit boxes. */

					if(!ConfigGetString(s_lpcfgCurrentConfig, GAMECFG_IWAD, szBuffer, cchBuffer))
						*szBuffer = TEXT('\0');
					SetDlgItemText(hwndPropPage, IDC_EDIT_IWAD, szBuffer);

					if(!ConfigGetString(s_lpcfgCurrentConfig, GAMECFG_BINARY, szBuffer, cchBuffer))
						*szBuffer = TEXT('\0');
					SetDlgItemText(hwndPropPage, IDC_EDIT_BINARY, szBuffer);

					ProcHeapFree(szBuffer);
				}
				else
				{
					/* Nothing selected. Disable controls. Not really necessary
					 * with the way we're configured, but no harm done.
					 */
					EnableWindow(GetDlgItem(hwndPropPage, IDC_STATIC_BINARY), FALSE);
					EnableWindow(GetDlgItem(hwndPropPage, IDC_STATIC_IWAD), FALSE);
					EnableWindow(GetDlgItem(hwndPropPage, IDC_EDIT_BINARY), FALSE);
					EnableWindow(GetDlgItem(hwndPropPage, IDC_EDIT_IWAD), FALSE);
					EnableWindow(GetDlgItem(hwndPropPage, IDC_BUTTON_BROWSEBINARY), FALSE);
					EnableWindow(GetDlgItem(hwndPropPage, IDC_BUTTON_BROWSEIWAD), FALSE);
				}

				return TRUE;
			}

			break;
		}

		/* Didn't process message. */
		break;

	case WM_NOTIFY:
		switch(((LPNMHDR)lParam)->code)
		{
		case PSN_APPLY:
			{
				/* Update the global options. */
				ConfigSetSubsection(g_lpcfgMain, OPT_GAMECONFIGS, s_lpcfgGameConfigs);

				/* The memory we allocated for our local config is now owned by
				 * the main tree. Make another local copy for ourselves.
				 */
				s_lpcfgGameConfigs = ConfigDuplicate(s_lpcfgGameConfigs);

				/* Inform the caller that we applied. */
				*s_lpbOptionsChanged = TRUE;

				SetWindowLong(hwndPropPage, DWL_MSGRESULT, PSNRET_NOERROR);
			}

			return TRUE;
		}

		/* Didn't process message. */
		break;

	case WM_DESTROY:
		if(s_lpcfgGameConfigs) ConfigDestroy(s_lpcfgGameConfigs);
		return TRUE;
	}

	/* Didn't process message. */
	return FALSE;
}



/* OptionsShortcutsPropProc
 *   Dialogue proc for the Shortcuts page of Options dialogue.
 *
 * Parameters:
 *   HWND	hwndPropPage	Property page window handle.
 *   UINT	uiMsg			Message identifier.
 *   WPARAM wParam			Message-specific.
 *   LPARAM lParam			Message-specific.
 *
 * Return value: INT_PTR
 *   TRUE if the message was processed; FALSE otherwise.
 */
static INT_PTR CALLBACK OptionsShortcutsPropProc(HWND hwndPropPage, UINT uiMsg, WPARAM wParam, LPARAM lParam)
{
	static BOOL *s_lpbOptionsChanged = NULL;
	static int s_iShortcutCodes[SCK_MAX];

	UNREFERENCED_PARAMETER(wParam);

	switch(uiMsg)
	{
	case WM_INITDIALOG:
		{
			LVCOLUMN lvcol;
			TCHAR szColHeader[CCH_SHORTCUT_COLHDR];
			HWND hwndShortcutList = GetDlgItem(hwndPropPage, IDC_LIST_SHORTCUTS);
			int i;

			/* Get address of flag in which to indicate whether we changed
			 * anything.
			 */
			s_lpbOptionsChanged = (BOOL*)((PROPSHEETPAGE*)lParam)->lParam;

			/* Make local copy of shortcut keys. */
			CopyMemory(s_iShortcutCodes, g_iShortcutCodes, sizeof(s_iShortcutCodes));

			/* Create the columns in the list view control. */
			lvcol.pszText = szColHeader;
			lvcol.mask = LVCF_SUBITEM | LVCF_TEXT;

			lvcol.iSubItem = 0;
			LoadString(g_hInstance, IDS_SHORTCUTS_COL_FUNCTION, szColHeader, NUM_ELEMENTS(szColHeader));
			ListView_InsertColumn(hwndShortcutList, 0, &lvcol);

			lvcol.iSubItem = 1;
			LoadString(g_hInstance, IDS_SHORTCUTS_COL_SHORTCUT, szColHeader, NUM_ELEMENTS(szColHeader));
			ListView_InsertColumn(hwndShortcutList, 1, &lvcol);

			/* Full row select. */
			ListView_SetExtendedListViewStyleEx(hwndShortcutList, LVS_EX_FULLROWSELECT, LVS_EX_FULLROWSELECT);

			/* Populate the list. */
			for(i = 0; i < (int)NUM_ELEMENTS(g_sckstringids); i++)
			{
				TCHAR szFunction[CCH_SHORTCUT_FUNCTION];
				LVITEM lvitem;
				int iIndex;

				/* Get string to insert for this function. */
				LoadString(g_hInstance, g_sckstringids[i].iStringID, szFunction, NUM_ELEMENTS(szFunction));

				lvitem.mask = LVIF_TEXT | LVIF_PARAM;
				lvitem.iItem = i;
				lvitem.iSubItem = 0;
				lvitem.pszText = szFunction;
				lvitem.lParam = g_sckstringids[i].iShortcutID;
				iIndex = ListView_InsertItem(hwndShortcutList, &lvitem);

				SetListItemShortcutText(hwndShortcutList, iIndex, g_sckstringids[i].iShortcutID, s_iShortcutCodes);
			}

			/* Autosize the columns. */
			ListView_SetColumnWidth(hwndShortcutList, 0, LVSCW_AUTOSIZE_USEHEADER);
			ListView_SetColumnWidth(hwndShortcutList, 1, LVSCW_AUTOSIZE_USEHEADER);


			/* Populate the "special shortcut" combo-box. */
			for(i = 0; i < (int)NUM_ELEMENTS(g_specialshortcuts); i++)
			{
				TCHAR szCombo[CCH_SHORTCUT_MOUSECOMBO];
				int iIndex;

				LoadString(g_hInstance, g_specialshortcuts[i].iStringID, szCombo, NUM_ELEMENTS(szCombo));
				iIndex = SendDlgItemMessage(hwndPropPage, IDC_COMBO_SPECSHORTCUT, CB_ADDSTRING, 0, (LPARAM)szCombo);
				SendDlgItemMessage(hwndPropPage, IDC_COMBO_SPECSHORTCUT, CB_SETITEMDATA, iIndex, g_specialshortcuts[i].iKeyCode);
			}

			SendDlgItemMessage(hwndPropPage, IDC_COMBO_SPECSHORTCUT, CB_SETCURSEL, 0, 0);

			/* Disable them all to start with, since nothing's selected. */
			DisableAllShortcutControls(hwndPropPage);
		}

		/* Let the system set the focus. */
		return TRUE;

	case WM_COMMAND:
		switch(LOWORD(wParam))
		{
		case IDC_RADIO_NOSHORTCUT:
		case IDC_RADIO_STDSHORTCUT:
		case IDC_RADIO_SPECSHORTCUT:
		case IDC_HOTKEY:
		case IDC_CHECK_SPECCTRL:
		case IDC_CHECK_SPECALT:
		case IDC_CHECK_SPECSHIFT:
		case IDC_COMBO_SPECSHORTCUT:

			/* Update if the hotkey control changed, if one of the checkboxes or
			 * radio buttons was clicked or if the combo box changed.
			 */
			if(HIWORD(wParam) == ((LOWORD(wParam) == IDC_HOTKEY) ? EN_CHANGE : ((LOWORD(wParam) == IDC_COMBO_SPECSHORTCUT) ? CBN_SELCHANGE : BN_CLICKED)))
			{
				EnableAppropriateShortcutControls(hwndPropPage);
				UpdateAssignmentControls(hwndPropPage, s_iShortcutCodes);

				return TRUE;
			}

			break;

		case IDC_BUTTON_ASSIGN:
			{
				HWND hwndShortcutList = GetDlgItem(hwndPropPage, IDC_LIST_SHORTCUTS);
				LVITEM lvitem;

				lvitem.mask = LVIF_PARAM;
				lvitem.iItem = ListView_GetNextItem(hwndShortcutList, -1, LVIS_SELECTED);
				lvitem.iSubItem = 0;
				ListView_GetItem(hwndShortcutList, &lvitem);

				s_iShortcutCodes[lvitem.lParam] = GetKeycodeFromControls(hwndPropPage);
				SetListItemShortcutText(hwndShortcutList, lvitem.iItem, lvitem.lParam, s_iShortcutCodes);
				UpdateAssignmentControls(hwndPropPage, s_iShortcutCodes);
			}

			return TRUE;
		}

		break;

	case WM_NOTIFY:
		switch(((LPNMHDR)lParam)->idFrom)
		{
		case IDC_LIST_SHORTCUTS:
			{
				LPNMLISTVIEW lpnmlv = (LPNMLISTVIEW)lParam;

				switch(lpnmlv->hdr.code)
				{
				case LVN_ITEMCHANGED:
					if(lpnmlv->uChanged & LVIF_STATE)
					{
						int iIndex = ListView_GetNextItem(lpnmlv->hdr.hwndFrom, -1, LVIS_SELECTED);

						if(iIndex >= 0)
						{
							/* Enable the radio buttons and static controls. */
							EnableWindow(GetDlgItem(hwndPropPage, IDC_RADIO_NOSHORTCUT), TRUE);
							EnableWindow(GetDlgItem(hwndPropPage, IDC_RADIO_STDSHORTCUT), TRUE);
							EnableWindow(GetDlgItem(hwndPropPage, IDC_RADIO_SPECSHORTCUT), TRUE);
							EnableWindow(GetDlgItem(hwndPropPage, IDC_STATIC_ASSIGNEDTOLABEL), TRUE);
							EnableWindow(GetDlgItem(hwndPropPage, IDC_STATIC_ASSIGNEDTO), TRUE);

							SetShortcutControlsFromList(hwndPropPage, lpnmlv->hdr.hwndFrom, iIndex, s_iShortcutCodes);
							EnableAppropriateShortcutControls(hwndPropPage);
							UpdateAssignmentControls(hwndPropPage, s_iShortcutCodes);
						}
						else
						{
							/* Disable the whole lot. */
							DisableAllShortcutControls(hwndPropPage);
						}

						return TRUE;
					}
				}
			}

			break;

		default:
			/* Property sheets don't have IDs. */
			switch(((LPNMHDR)lParam)->code)
			{
			case PSN_APPLY:

				/* Inform the caller that we applied. */
				*s_lpbOptionsChanged = TRUE;

				/* Copy our local shortcuts array back into the global array. */
				CopyMemory(g_iShortcutCodes, s_iShortcutCodes, sizeof(s_iShortcutCodes));

				SetWindowLong(hwndPropPage, DWL_MSGRESULT, PSNRET_NOERROR);
				return TRUE;
			}
		}
	}

	/* Didn't process message. */
	return FALSE;
}


/* OptionsAdvancedPropProc
 *   Dialogue proc for the Advanced page of Options dialogue.
 *
 * Parameters:
 *   HWND	hwndPropPage	Property page window handle.
 *   UINT	uiMsg			Message identifier.
 *   WPARAM wParam			Message-specific.
 *   LPARAM lParam			Message-specific.
 *
 * Return value: INT_PTR
 *   TRUE if the message was processed; FALSE otherwise.
 */
static INT_PTR CALLBACK OptionsAdvancedPropProc(HWND hwndPropPage, UINT uiMsg, WPARAM wParam, LPARAM lParam)
{
	static BOOL *s_lpbOptionsChanged = NULL;

	UNREFERENCED_PARAMETER(wParam);

	switch(uiMsg)
	{
	case WM_INITDIALOG:
		{
			/* Get Wiki and nodebuilder sections. */
			CONFIG *lpcfgWiki = ConfigGetSubsection(g_lpcfgMain, OPT_WIKI);
			CONFIG *lpcfgNodebuilder = ConfigGetSubsection(g_lpcfgMain, OPT_NODEBUILDER);
			DWORD cchBuffer;
			LPTSTR szBuffer;

			/* Get address of flag in which to indicate whether we changed
			 * anything.
			 */
			s_lpbOptionsChanged = (BOOL*)((PROPSHEETPAGE*)lParam)->lParam;

			/* Enable autocomplete for nodebuilder binary. */
			SHAutoComplete(GetDlgItem(hwndPropPage, IDC_EDIT_NBBINARY), SHACF_FILESYS_ONLY);

			/* Initialise checkboxes and radio buttons. */
			CheckDlgButton(hwndPropPage, IDC_CHECK_PERSISTENTOPENWAD, ConfigGetInteger(g_lpcfgMain, OPT_PERSISTENTOPENWAD) ? BST_CHECKED : BST_UNCHECKED);
			CheckDlgButton(hwndPropPage, IDC_CHECK_UNDOSTITCH, ConfigGetInteger(g_lpcfgMain, OPT_UNDOSTITCH) ? BST_CHECKED : BST_UNCHECKED);
			CheckRadioButton(hwndPropPage, IDC_RADIO_NEWSECINTERIOR, IDC_RADIO_NEWSECFRONT, ConfigGetInteger(g_lpcfgMain, OPT_NEWSECFRONT) ?  IDC_RADIO_NEWSECFRONT: IDC_RADIO_NEWSECINTERIOR);

			/* Initialise edit boxes. */

			cchBuffer = ConfigGetStringLength(lpcfgWiki, WIKICFG_QUERYURLFMT) + 1;
			szBuffer = (LPTSTR)ProcHeapAlloc(cchBuffer * sizeof(TCHAR));
			ConfigGetString(lpcfgWiki, WIKICFG_QUERYURLFMT, szBuffer, cchBuffer);
			SetDlgItemText(hwndPropPage, IDC_EDIT_WIKIQUERY, szBuffer);
			ProcHeapFree(szBuffer);

			cchBuffer = ConfigGetStringLength(lpcfgWiki, WIKICFG_MAINPAGE) + 1;
			szBuffer = (LPTSTR)ProcHeapAlloc(cchBuffer * sizeof(TCHAR));
			ConfigGetString(lpcfgWiki, WIKICFG_MAINPAGE, szBuffer, cchBuffer);
			SetDlgItemText(hwndPropPage, IDC_EDIT_WIKIMAIN, szBuffer);
			ProcHeapFree(szBuffer);

			cchBuffer = ConfigGetStringLength(lpcfgNodebuilder, NBCFG_BINARY) + 1;
			szBuffer = (LPTSTR)ProcHeapAlloc(cchBuffer * sizeof(TCHAR));
			ConfigGetString(lpcfgNodebuilder, NBCFG_BINARY, szBuffer, cchBuffer);
			SetDlgItemText(hwndPropPage, IDC_EDIT_NBBINARY, szBuffer);
			ProcHeapFree(szBuffer);

			cchBuffer = ConfigGetStringLength(lpcfgNodebuilder, NBCFG_ARGS) + 1;
			szBuffer = (LPTSTR)ProcHeapAlloc(cchBuffer * sizeof(TCHAR));
			ConfigGetString(lpcfgNodebuilder, NBCFG_ARGS, szBuffer, cchBuffer);
			SetDlgItemText(hwndPropPage, IDC_EDIT_NBPARAMS, szBuffer);
			ProcHeapFree(szBuffer);
		}

		/* Let the system set the focus. */
		return TRUE;

	case WM_COMMAND:
		switch(LOWORD(wParam))
		{
		case IDC_BUTTON_BROWSE:
			{
				TCHAR szBinary[MAX_PATH];
				TCHAR szFilter[128];

				/* Get and tidy up the filter string. */
				LoadAndFormatFilterString(IDS_BINARYFILTER, szFilter, NUM_ELEMENTS(szFilter));

				/* Get current filename. */
				GetDlgItemText(hwndPropPage, IDC_EDIT_NBBINARY, szBinary, NUM_ELEMENTS(szBinary));

				/* Browse for nodebuilder binary. */
				if(CommDlgOpen(hwndPropPage, szBinary, NUM_ELEMENTS(szBinary), NULL, szFilter, TEXT("exe"), szBinary, OFN_EXPLORER | OFN_FILEMUSTEXIST | OFN_HIDEREADONLY))
					SetDlgItemText(hwndPropPage, IDC_EDIT_NBBINARY, szBinary);

				return 0;
			}
		}

		/* Didn't process WM_COMMAND. */
		break;

	case WM_NOTIFY:
		switch(((LPNMHDR)lParam)->code)
		{
		case PSN_APPLY:
			{
				/* Get Wiki and nodebuilder sections. */
				CONFIG *lpcfgWiki = ConfigGetSubsection(g_lpcfgMain, OPT_WIKI);
				CONFIG *lpcfgNodebuilder = ConfigGetSubsection(g_lpcfgMain, OPT_NODEBUILDER);
				DWORD cchBuffer;
				LPTSTR szBuffer;

				/* Inform the caller that we applied. */
				*s_lpbOptionsChanged = TRUE;

				/* Get states of checkboxes and radio buttons. */
				ConfigSetInteger(g_lpcfgMain, OPT_PERSISTENTOPENWAD, IsDlgButtonChecked(hwndPropPage, IDC_CHECK_PERSISTENTOPENWAD) == BST_CHECKED);
				ConfigSetInteger(g_lpcfgMain, OPT_UNDOSTITCH, IsDlgButtonChecked(hwndPropPage, IDC_CHECK_UNDOSTITCH) == BST_CHECKED);
				ConfigSetInteger(g_lpcfgMain, OPT_NEWSECFRONT, IsDlgButtonChecked(hwndPropPage, IDC_RADIO_NEWSECFRONT) == BST_CHECKED);

				cchBuffer = GetWindowTextLength(GetDlgItem(hwndPropPage, IDC_EDIT_WIKIQUERY)) + 1;
				szBuffer = (LPTSTR)ProcHeapAlloc(cchBuffer * sizeof(TCHAR));
				GetDlgItemText(hwndPropPage, IDC_EDIT_WIKIQUERY, szBuffer, cchBuffer);
				ConfigSetString(lpcfgWiki, WIKICFG_QUERYURLFMT, szBuffer);
				ProcHeapFree(szBuffer);

				cchBuffer = GetWindowTextLength(GetDlgItem(hwndPropPage, IDC_EDIT_WIKIMAIN)) + 1;
				szBuffer = (LPTSTR)ProcHeapAlloc(cchBuffer * sizeof(TCHAR));
				GetDlgItemText(hwndPropPage, IDC_EDIT_WIKIMAIN, szBuffer, cchBuffer);
				ConfigSetString(lpcfgWiki, WIKICFG_MAINPAGE, szBuffer);
				ProcHeapFree(szBuffer);

				cchBuffer = GetWindowTextLength(GetDlgItem(hwndPropPage, IDC_EDIT_NBBINARY)) + 1;
				szBuffer = (LPTSTR)ProcHeapAlloc(cchBuffer * sizeof(TCHAR));
				GetDlgItemText(hwndPropPage, IDC_EDIT_NBBINARY, szBuffer, cchBuffer);
				ConfigSetString(lpcfgNodebuilder, NBCFG_BINARY, szBuffer);
				ProcHeapFree(szBuffer);

				cchBuffer = GetWindowTextLength(GetDlgItem(hwndPropPage, IDC_EDIT_NBPARAMS)) + 1;
				szBuffer = (LPTSTR)ProcHeapAlloc(cchBuffer * sizeof(TCHAR));
				GetDlgItemText(hwndPropPage, IDC_EDIT_NBPARAMS, szBuffer, cchBuffer);
				ConfigSetString(lpcfgNodebuilder, NBCFG_ARGS, szBuffer);
				ProcHeapFree(szBuffer);

				/* No nefariousness, please. */
				UntaintWikiOptions();

				SetWindowLong(hwndPropPage, DWL_MSGRESULT, PSNRET_NOERROR);
			}

			return TRUE;
		}
	}

	/* Didn't process message. */
	return FALSE;
}


/* UpdateSwatch
 *   Updates a colour swatch in the palette list.
 *
 * Parameters:
 *   HWND		hwndColourList	Palette list-view.
 *   int		iListIndex		List index of colour to update.
 *   RGBQUAD*	lprgbqPalette	Palette.
 *
 * Return value: None.
 */
static void UpdateSwatch(HWND hwndColourList, int iListIndex, RGBQUAD *lprgbqPalette)
{
	HBITMAP hbmSwatch, hbmOld;
	HIMAGELIST himl;
	HDC hdc, hdcList;
	HBRUSH hbrush;
	LVITEM lvitem;
	RGBQUAD rgbqColour;
	RECT rcFill;

	/* Find the swatch size. */
	himl = ListView_GetImageList(hwndColourList, LVSIL_SMALL);
	rcFill.left = rcFill.top = 0;
	ImageList_GetIconSize(himl, (int*)&rcFill.right, (int*)&rcFill.bottom);

	/* Get the palette colour. */
	lvitem.mask = LVIF_PARAM;
	lvitem.iItem = iListIndex;
	lvitem.iSubItem = 0;
	ListView_GetItem(hwndColourList, &lvitem);
	rgbqColour = lprgbqPalette[lvitem.lParam];

	/* Jump through the GDI hoops necessary to fill the bitmap. */
	hdc = CreateCompatibleDC(NULL);
	hdcList = GetDC(hwndColourList);
	hbmSwatch = CreateCompatibleBitmap(hdcList, rcFill.right, rcFill.bottom);
	hbrush = CreateSolidBrush(RGBQUADToCOLORREF(rgbqColour));
	hbmOld = (HBITMAP)SelectObject(hdc, hbmSwatch);

	FillRect(hdc, &rcFill, hbrush);

	SelectObject(hdc, hbmOld);
	DeleteObject(hbrush);
	ReleaseDC(hwndColourList, hdcList);
	DeleteDC(hdc);

	/* Update the image-list. */
	ImageList_Replace(himl, iListIndex, hbmSwatch, NULL);

	DeleteObject(hbmSwatch);

	/* Make sure the item is repainted. */
	ListView_RedrawItems(hwndColourList, iListIndex, iListIndex);
}


/* ChooseRendererColour
 *   Prompts the user to choose a specific renderer colour.
 *
 * Parameters:
 *   HWND		hwndColourList	Palette list-view.
 *   int		iListIndex		List index of colour to choose.
 *   RGBQUAD*	lprgbqPalette	Palette, which will be updated if the user
 *								chooses a colour.
 *
 * Return value: None.
 */
static void ChooseRendererColour(HWND hwndPropPage, HWND hwndColourList, int iListIndex, RGBQUAD *lprgbqPalette)
{
	/* Custom colours. */
	static COLORREF s_rgbCustom[16] = {0};

	LVITEM lvitem;
	COLORREF rgb;

	lvitem.mask = LVIF_PARAM;
	lvitem.iItem = iListIndex;
	ListView_GetItem(hwndColourList, &lvitem);

	rgb = RGBQUADToCOLORREF(lprgbqPalette[lvitem.lParam]);

	/* Show the Colour dialogue, and if the user OKs, update the palette. */
	if(CommDlgColour(hwndPropPage, &rgb, s_rgbCustom))
	{
		lprgbqPalette[lvitem.lParam] = COLORREFToRGBQUAD(rgb);
		UpdateSwatch(hwndColourList, iListIndex, lprgbqPalette);
	}
}


/* SetShortcutControlsFromList
 *   Sets the RHS controls according to a list item.
 *
 * Parameters:
 *   HWND		hwndPropPage		Property page handle.
 *   HWND		hwndColourList		Shortcuts list-view.
 *   int		iIndex				List index of shortcut.
 *   int*		lpiShortcutCodes	Array of assigned shortcut codes.
 *
 * Return value: None.
 *
 * Remarks:
 *   Call EnableAppropriateShortcutControls after this function to tidy up the
 *   UI.
 */
static void SetShortcutControlsFromList(HWND hwndPropPage, HWND hwndShortcutList, int iIndex, int *lpiShortcutCodes)
{
	LVITEM lvitem;
	int iShortcutCode;
	int iRadioButton;

	/* Find out which function is selected. */
	lvitem.iItem = iIndex;
	lvitem.iSubItem = 0;
	lvitem.mask = LVIF_PARAM;
	ListView_GetItem(hwndShortcutList, &lvitem);

	/* Get the current code. */
	iShortcutCode = lpiShortcutCodes[lvitem.lParam];

	if(iShortcutCode == 0)
		iRadioButton = IDC_RADIO_NOSHORTCUT;
	else
	{
		/* There's a shortcut assigned. Determine whether it's a special
		 * shortcut.
		 */

		BOOL bSpecial = FALSE;
		int i;

		for(i = 0; i < (int)NUM_ELEMENTS(g_specialshortcuts) && !bSpecial; i++)
			if((g_specialshortcuts[i].iKeyCode & SC_KEY) == (iShortcutCode & SC_KEY))
				bSpecial = TRUE;

		/* Set the appropriate controls depending on whether we're special. */
		if(bSpecial)
		{
			int iCount;

			iRadioButton = IDC_RADIO_SPECSHORTCUT;

			/* Select the combo box item. */
			iCount = SendDlgItemMessage(hwndPropPage, IDC_COMBO_SPECSHORTCUT, CB_GETCOUNT, 0, 0);
			for(i = 0; i < iCount; i++)
			{
				int iListKeyCode = SendDlgItemMessage(hwndPropPage, IDC_COMBO_SPECSHORTCUT, CB_GETITEMDATA, i, 0);
				if(iListKeyCode == (iShortcutCode & SC_KEY))
				{
					SendDlgItemMessage(hwndPropPage, IDC_COMBO_SPECSHORTCUT, CB_SETCURSEL, i, 0);
					break;
				}
			}

			/* Set the checkbox states. */
			CheckDlgButton(hwndPropPage, IDC_CHECK_SPECCTRL, iShortcutCode & SC_CTRL);
			CheckDlgButton(hwndPropPage, IDC_CHECK_SPECSHIFT, iShortcutCode & SC_SHIFT);
			CheckDlgButton(hwndPropPage, IDC_CHECK_SPECALT, iShortcutCode & SC_ALT);
		}
		else
		{
			iRadioButton = IDC_RADIO_STDSHORTCUT;
			SendDlgItemMessage(hwndPropPage, IDC_HOTKEY, HKM_SETHOTKEY, ShiftedKeycodeToHotkey(iShortcutCode), 0);
		}
	}

	CheckRadioButton(hwndPropPage, IDC_RADIO_NOSHORTCUT, IDC_RADIO_SPECSHORTCUT, iRadioButton);
}


/* EnableAppropriateShortcutControls
 *   Enables/disables RHS shortcut controls according to which type of shortcut
 *   has been selected.
 *
 * Parameters:
 *   HWND		hwndPropPage		Property page handle.
 *
 * Return value: None.
 *
 * Remarks:
 *   A shortcut must be selected -- don't call this if there's nothing selected
 *   in the list.
 */
static void EnableAppropriateShortcutControls(HWND hwndPropPage)
{
	/* Get the ID of the selected radio button. */
	int iRadioButton = IsDlgButtonChecked(hwndPropPage, IDC_RADIO_SPECSHORTCUT) ? IDC_RADIO_SPECSHORTCUT :
		(IsDlgButtonChecked(hwndPropPage, IDC_RADIO_STDSHORTCUT) ? IDC_RADIO_STDSHORTCUT : IDC_RADIO_NOSHORTCUT);

	/* Enable/disable the controls accordingly. */
	EnableWindow(GetDlgItem(hwndPropPage, IDC_HOTKEY), (iRadioButton == IDC_RADIO_STDSHORTCUT));
	EnableWindow(GetDlgItem(hwndPropPage, IDC_CHECK_SPECCTRL), (iRadioButton == IDC_RADIO_SPECSHORTCUT));
	EnableWindow(GetDlgItem(hwndPropPage, IDC_CHECK_SPECALT), (iRadioButton == IDC_RADIO_SPECSHORTCUT));
	EnableWindow(GetDlgItem(hwndPropPage, IDC_CHECK_SPECSHIFT), (iRadioButton == IDC_RADIO_SPECSHORTCUT));
	EnableWindow(GetDlgItem(hwndPropPage, IDC_COMBO_SPECSHORTCUT), (iRadioButton == IDC_RADIO_SPECSHORTCUT));
}


/* DisableAllShortcutControls
 *   Disables all RHS shortcut controls.
 *
 * Parameters:
 *   HWND		hwndPropPage		Property page handle.
 *
 * Return value: None.
 */
static void DisableAllShortcutControls(HWND hwndPropPage)
{
	EnableWindow(GetDlgItem(hwndPropPage, IDC_RADIO_NOSHORTCUT), FALSE);
	EnableWindow(GetDlgItem(hwndPropPage, IDC_RADIO_STDSHORTCUT), FALSE);
	EnableWindow(GetDlgItem(hwndPropPage, IDC_RADIO_SPECSHORTCUT), FALSE);
	EnableWindow(GetDlgItem(hwndPropPage, IDC_HOTKEY), FALSE);
	EnableWindow(GetDlgItem(hwndPropPage, IDC_CHECK_SPECCTRL), FALSE);
	EnableWindow(GetDlgItem(hwndPropPage, IDC_CHECK_SPECALT), FALSE);
	EnableWindow(GetDlgItem(hwndPropPage, IDC_CHECK_SPECSHIFT), FALSE);
	EnableWindow(GetDlgItem(hwndPropPage, IDC_COMBO_SPECSHORTCUT), FALSE);
	EnableWindow(GetDlgItem(hwndPropPage, IDC_STATIC_ASSIGNEDTOLABEL), FALSE);
	EnableWindow(GetDlgItem(hwndPropPage, IDC_STATIC_ASSIGNEDTO), FALSE);
	EnableWindow(GetDlgItem(hwndPropPage, IDC_BUTTON_ASSIGN), FALSE);
}


/* GetKeycodeFromControls
 *   Gets the shifted keycode specified by the RHS controls.
 *
 * Parameters:
 *   HWND		hwndPropPage		Property page handle.
 *
 * Return value: int
 *   Shifted keycode.
 *
 * Remarks:
 *   A shortcut must be selected -- don't call this if there's nothing selected
 *   in the list.
 */
static int GetKeycodeFromControls(HWND hwndPropPage)
{
	/* Consider the ID of the selected radio button. */
	switch(IsDlgButtonChecked(hwndPropPage, IDC_RADIO_SPECSHORTCUT) ? IDC_RADIO_SPECSHORTCUT :
		(IsDlgButtonChecked(hwndPropPage, IDC_RADIO_STDSHORTCUT) ? IDC_RADIO_STDSHORTCUT : IDC_RADIO_NOSHORTCUT))
	{
	default:	/* default clause not needed, but keeps the compiler happy. */
	case IDC_RADIO_NOSHORTCUT:
		return 0;

	case IDC_RADIO_STDSHORTCUT:
		{
			WORD wHotkey = (WORD)SendDlgItemMessage(hwndPropPage, IDC_HOTKEY, HKM_GETHOTKEY, 0, 0);
			return wHotkey ? HotkeyToShiftedKeycode(wHotkey) : 0;
		}

	case IDC_RADIO_SPECSHORTCUT:
		{
			/* Get virtual key code from combo box. */
			int iIndex = SendDlgItemMessage(hwndPropPage, IDC_COMBO_SPECSHORTCUT, CB_GETCURSEL, 0, 0);
			int iKeyCode = SendDlgItemMessage(hwndPropPage, IDC_COMBO_SPECSHORTCUT, CB_GETITEMDATA, iIndex, 0);

			/* Add shift mask. */
			return iKeyCode
				| (IsDlgButtonChecked(hwndPropPage, IDC_CHECK_SPECCTRL) ? SC_CTRL : 0)
				| (IsDlgButtonChecked(hwndPropPage, IDC_CHECK_SPECSHIFT) ? SC_SHIFT : 0)
				| (IsDlgButtonChecked(hwndPropPage, IDC_CHECK_SPECALT) ? SC_ALT : 0);
		}
	}
}


/* UpdateAssignmentControls
 *   Determines whether the specified shortcut is already assigned to someone,
 *   and sets control states accordingly.
 *
 * Parameters:
 *   HWND		hwndPropPage		Property page handle.
 *   int*		lpiShortcutCodes	Array of assigned shortcut codes.
 *
 * Return value: None.
 *
 * Remarks:
 *   A shortcut must be selected -- don't call this if there's nothing selected
 *   in the list.
 */
static void UpdateAssignmentControls(HWND hwndPropPage, int *lpiShortcutCodes)
{
	int iKeyCode = GetKeycodeFromControls(hwndPropPage), iMatch = -1;

	if(iKeyCode != 0)
	{
		int i;

		/* Check each function in the list and see if it's assigned to anyone
		 * (including ourselves).
		 */
		for(i = 0; i < (int)NUM_ELEMENTS(g_sckstringids); i++)
		{
			if(lpiShortcutCodes[g_sckstringids[i].iShortcutID] == iKeyCode)
			{
				iMatch = i;
				break;
			}
		}
	}

	if(iMatch == -1 || iKeyCode == 0)
	{
		/* Not assigned. */
		SetDlgItemText(hwndPropPage, IDC_STATIC_ASSIGNEDTO, TEXT(""));
		EnableWindow(GetDlgItem(hwndPropPage, IDC_BUTTON_ASSIGN), TRUE);
	}
	else
	{
		/* Already assigned. */
		TCHAR szFunction[CCH_SHORTCUT_FUNCTION];
		LoadString(g_hInstance, g_sckstringids[iMatch].iStringID, szFunction, NUM_ELEMENTS(szFunction));
		SetDlgItemText(hwndPropPage, IDC_STATIC_ASSIGNEDTO, szFunction);
		EnableWindow(GetDlgItem(hwndPropPage, IDC_BUTTON_ASSIGN), FALSE);
	}
}


/* SetListItemShortcutText
 *   Updates the shortcut text in a list entry from the array of assigned
 *   shortcuts.
 *
 * Parameters:
 *   HWND		hwndShortcutList	Shortcut list-view.
 *   int		iIndex				Index of item in list.
 *   int		iShortcutID			Associated shortcut ID.
 *   int*		lpiShortcutCodes	Array of assigned shortcut codes.
 *
 * Return value: None.
 *
 * Remarks:
 *   A shortcut must be selected -- don't call this if there's nothing selected
 *   in the list.
 */
static void SetListItemShortcutText(HWND hwndShortcutList, int iIndex, int iShortcutID, int *lpiShortcutCodes)
{
	TCHAR szShortcut[CCH_SHORTCUT_SHORTCUT];

	if(lpiShortcutCodes[iShortcutID] != 0)
		GetShiftedShortcutText(lpiShortcutCodes[iShortcutID], szShortcut, NUM_ELEMENTS(szShortcut));
	else *szShortcut = TEXT('\0');

	ListView_SetItemText(hwndShortcutList, iIndex, 1, szShortcut);
}
