/* SRB2 Workbench -- A map editor for Sonic Robo Blast 2.
 * Copyright (C) 2009 Gregor Dick
 * http://workbench.srb2.org/
 *
 * Incorporates portions of Doom Builder, (C) 2003 Pascal vd Heiden.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 * USA.
 *
 * toolbar.c: Implements the toolbar's logic.
 *
 * AMDG.
 */

#include <windows.h>
#include <commctrl.h>
#include <tchar.h>
#include <stdio.h>

#include "../general.h"
#include "../../res/resource.h"

#include "../DockWnd/DockWnd.h"

#include "toolbar.h"
#include "mdiframe.h"


static DOCKINFO *g_lpdiToolbar;

#define CX_TBIMAGES		16
#define CY_TBIMAGES		16

#define TB_MASK_COLOUR RGB(0xFF, 0, 0xFF)

enum ENUM_TB_IMAGE_OFFSETS
{
	TBIO_ANYMODE = 0,
	TBIO_SECTORSMODE,
	TBIO_LINESMODE,
	TBIO_VERTICESMODE,
	TBIO_THINGSMODE,

	TBIO_POINTER,
	TBIO_PAN,
	TBIO_DRAWLINES,
	TBIO_DRAWRECT,
	TBIO_INSERTVERTEX,
	TBIO_INSERTTHING,

	TBIO_FIND,
	TBIO_REPLACE,

	TBIO_TEST,

	TBIO_FLIPHORIZ,
	TBIO_FLIPVERT,
	TBIO_ROTATE,
	TBIO_RESIZE,

	TBIO_SNAPTOGRID,
	TBIO_SNAPTOVERTICES,
	TBIO_SNAPTOLINES,

	TBIO_CENTREVIEW,

	TBIO_FLIPLINES,
	TBIO_SPLITLINES,

	TBIO_JOINSECTORS,
	TBIO_MERGESECTORS
};


/* CreateToolbarWindow
 *   Creates the toolbar tool window with its docking container.
 *
 * Parameters: None.
 *
 * Return value: BOOL
 *   TRUE on success; FALSE on error.
 *
 * Remarks:
 *   g_hwndMain isn't set yet!
 */
BOOL CreateToolbarWindow(void)
{
	TCHAR		szCaption[64];
	HWND		hwndTB;
	int			iImagesOffset;
	HBITMAP		hbmButtons;
	HIMAGELIST	himl = ImageList_Create(CX_TBIMAGES, CY_TBIMAGES, ILC_COLOR24 | ILC_MASK, 0, 0);
	SIZE		sizeToolbar;

	/* The toolbar is initially attached to the parent. */
	hwndTB = CreateWindowEx(0,
							TOOLBARCLASSNAME,
							NULL,
							WS_CHILD | WS_CLIPCHILDREN,
							0, 0, 0, 0,
							g_dwc.hwndContainer,
							NULL,
							g_hInstance,
							NULL);

	SendMessage(hwndTB, TB_BUTTONSTRUCTSIZE, sizeof(TBBUTTON), 0);

	SendMessage(hwndTB, TB_SETSTYLE, 0, TBSTYLE_FLAT | TBSTYLE_TRANSPARENT | TBSTYLE_WRAPABLE | TBSTYLE_TOOLTIPS | WS_CLIPSIBLINGS | CCS_NOPARENTALIGN | CCS_NORESIZE);
	SendMessage(hwndTB, TB_SETEXTENDEDSTYLE, 0, TBSTYLE_EX_DRAWDDARROWS);

	/* Associate the (empty) image list with the toolbar. */
	SendMessage(hwndTB, TB_SETIMAGELIST, 0, (LPARAM)himl);

	/* Add the system's images. */
	SendMessage(hwndTB, TB_LOADIMAGES, IDB_STD_SMALL_COLOR, (LPARAM)HINST_COMMCTRL);

	/* Add my images. */
	hbmButtons = LoadBitmap(g_hInstance, MAKEINTRESOURCE(IDB_TOOLBAR));
	iImagesOffset = ImageList_AddMasked(himl, hbmButtons, TB_MASK_COLOUR);
	DeleteObject(hbmButtons);

	{
		TBBUTTON tbb[] =
		{
			{STD_FILENEW, IDM_FILE_NEW, TBSTATE_ENABLED, TBSTYLE_BUTTON, {0}, 0, 0},
			{STD_FILEOPEN, IDM_FILE_OPEN, TBSTATE_ENABLED, TBSTYLE_BUTTON, {0}, 0, 0},
			{STD_FILESAVE, IDM_FILE_SAVE, TBSTATE_ENABLED, TBSTYLE_BUTTON, {0}, 0, 0},

			{0, 0, 0, TBSTYLE_SEP, {0}, 0, 0},

			{STD_CUT, IDM_EDIT_CUT, TBSTATE_ENABLED, TBSTYLE_BUTTON, {0}, 0, 0},
			{STD_COPY, IDM_EDIT_COPY, TBSTATE_ENABLED, TBSTYLE_BUTTON, {0}, 0, 0},
			{STD_PASTE, IDM_EDIT_PASTE, TBSTATE_ENABLED, TBSTYLE_BUTTON, {0}, 0, 0},

			{0, 0, 0, TBSTYLE_SEP, {0}, 0, 0},

			{STD_UNDO, IDM_EDIT_UNDO, TBSTATE_ENABLED, TBSTYLE_BUTTON, {0}, 0, 0},
			{STD_REDOW, IDM_EDIT_REDO, TBSTATE_ENABLED, TBSTYLE_BUTTON, {0}, 0, 0},

			{0, 0, 0, TBSTYLE_SEP, {0}, 0, 0},

			{iImagesOffset + TBIO_TEST, IDM_FILE_TEST, TBSTATE_ENABLED, TBSTYLE_BUTTON, {0}, 0, 0},

			{0, 0, 0, TBSTYLE_SEP, {0}, 0, 0},

			{STD_PROPERTIES, IDM_PROPERTIES, TBSTATE_ENABLED, TBSTYLE_BUTTON, {0}, 0, 0},

			{0, 0, 0, TBSTYLE_SEP, {0}, 0, 0},

			{iImagesOffset + TBIO_FIND, IDM_EDIT_FIND, TBSTATE_ENABLED, TBSTYLE_BUTTON, {0}, 0, 0},
			{iImagesOffset + TBIO_REPLACE, IDM_EDIT_REPLACE, TBSTATE_ENABLED, TBSTYLE_BUTTON, {0}, 0, 0},

			{0, 0, 0, TBSTYLE_SEP, {0}, 0, 0},

			{iImagesOffset + TBIO_ANYMODE, IDM_VIEW_MODE_ANY, TBSTATE_ENABLED, TBSTYLE_BUTTON | TBSTYLE_CHECKGROUP, {0}, 0, 0},
			{iImagesOffset + TBIO_LINESMODE, IDM_VIEW_MODE_LINES, TBSTATE_ENABLED, TBSTYLE_BUTTON | TBSTYLE_CHECKGROUP, {0}, 0, 0},
			{iImagesOffset + TBIO_SECTORSMODE, IDM_VIEW_MODE_SECTORS, TBSTATE_ENABLED, TBSTYLE_BUTTON | TBSTYLE_CHECKGROUP, {0}, 0, 0},
			{iImagesOffset + TBIO_VERTICESMODE, IDM_VIEW_MODE_VERTICES, TBSTATE_ENABLED, TBSTYLE_BUTTON | TBSTYLE_CHECKGROUP, {0}, 0, 0},
			{iImagesOffset + TBIO_THINGSMODE, IDM_VIEW_MODE_THINGS, TBSTATE_ENABLED, TBSTYLE_BUTTON | TBSTYLE_CHECKGROUP, {0}, 0, 0},

			{0, 0, 0, TBSTYLE_SEP, {0}, 0, 0},

			{iImagesOffset + TBIO_POINTER, IDM_CANCEL, TBSTATE_ENABLED, TBSTYLE_BUTTON | TBSTYLE_CHECKGROUP, {0}, 0, 0},
			{iImagesOffset + TBIO_PAN, IDM_VIEW_MODE_MOVE, TBSTATE_ENABLED, TBSTYLE_BUTTON | TBSTYLE_CHECKGROUP, {0}, 0, 0},
			{iImagesOffset + TBIO_DRAWLINES, IDM_INSERTLINES, TBSTATE_ENABLED, TBSTYLE_BUTTON | TBSTYLE_CHECKGROUP, {0}, 0, 0},
			{iImagesOffset + TBIO_DRAWRECT, IDM_INSERTRECT, TBSTATE_ENABLED, TBSTYLE_BUTTON | TBSTYLE_CHECKGROUP, {0}, 0, 0},
			{iImagesOffset + TBIO_INSERTVERTEX, IDM_INSERTVERTEX, TBSTATE_ENABLED, TBSTYLE_BUTTON | TBSTYLE_CHECKGROUP, {0}, 0, 0},
			{iImagesOffset + TBIO_INSERTTHING, IDM_INSERTTHING, TBSTATE_ENABLED, TBSTYLE_BUTTON | TBSTYLE_CHECKGROUP, {0}, 0, 0},

			{0, 0, 0, TBSTYLE_SEP, {0}, 0, 0},

			{iImagesOffset + TBIO_FLIPHORIZ, IDM_EDIT_TRANSFORMSELECTION_FLIPHORIZONTALLY, TBSTATE_ENABLED, TBSTYLE_BUTTON, {0}, 0, 0},
			{iImagesOffset + TBIO_FLIPVERT, IDM_EDIT_TRANSFORMSELECTION_FLIPVERTICALLY, TBSTATE_ENABLED, TBSTYLE_BUTTON, {0}, 0, 0},
			{iImagesOffset + TBIO_ROTATE, IDM_EDIT_TRANSFORMSELECTION_ROTATE, TBSTATE_ENABLED, TBSTYLE_BUTTON, {0}, 0, 0},
			{iImagesOffset + TBIO_RESIZE, IDM_EDIT_TRANSFORMSELECTION_RESIZE, TBSTATE_ENABLED, TBSTYLE_BUTTON, {0}, 0, 0},

			{0, 0, 0, TBSTYLE_SEP, {0}, 0, 0},

			{iImagesOffset + TBIO_SNAPTOGRID, IDM_EDIT_SNAPTOGRID, TBSTATE_ENABLED, TBSTYLE_BUTTON | TBSTYLE_CHECK, {0}, 0, 0},
			{iImagesOffset + TBIO_SNAPTOVERTICES, IDM_EDIT_SNAPTOVERTICES, TBSTATE_ENABLED, TBSTYLE_BUTTON | TBSTYLE_CHECK, {0}, 0, 0},
			{iImagesOffset + TBIO_SNAPTOLINES, IDM_EDIT_SNAPTOLINES, TBSTATE_ENABLED, TBSTYLE_BUTTON | TBSTYLE_CHECK, {0}, 0, 0},

			{0, 0, 0, TBSTYLE_SEP, {0}, 0, 0},

			{iImagesOffset + TBIO_CENTREVIEW, IDM_VIEW_CENTRE, TBSTATE_ENABLED, TBSTYLE_BUTTON, {0}, 0, 0},

			{0, 0, 0, TBSTYLE_SEP, {0}, 0, 0},

			{iImagesOffset + TBIO_FLIPLINES, IDM_LINES_FLIPLINEDEFS, TBSTATE_ENABLED, TBSTYLE_BUTTON, {0}, 0, 0},
			{iImagesOffset + TBIO_SPLITLINES, IDM_LINES_SPLITLINEDEFS, TBSTATE_ENABLED, TBSTYLE_BUTTON, {0}, 0, 0},
			{iImagesOffset + TBIO_JOINSECTORS, IDM_SECTORS_JOIN, TBSTATE_ENABLED, TBSTYLE_BUTTON, {0}, 0, 0},
			{iImagesOffset + TBIO_MERGESECTORS, IDM_SECTORS_MERGE, TBSTATE_ENABLED, TBSTYLE_BUTTON, {0}, 0, 0},
		};

		SendMessage(hwndTB, TB_ADDBUTTONS, NUM_ELEMENTS(tbb), (LPARAM)tbb);
	}

	/* Allocate a docking structure. This'll be freed automatically by the lib.
	 */
	g_lpdiToolbar = DockingAlloc((char)DWS_DOCKED_TOP, &g_dwc);

	/* We only actually use this for width. */
	SendMessage(hwndTB, TB_GETMAXSIZE, 0, (LPARAM)&sizeToolbar);

	g_lpdiToolbar->cxFloating = sizeToolbar.cx;
	g_lpdiToolbar->cyFloating = g_lpdiToolbar->nDockedSize = HIWORD(SendMessage(hwndTB, TB_GETBUTTONSIZE, 0, 0)) + HIWORD(SendMessage(hwndTB, TB_GETPADDING, 0, 0));

	/* Destroy the toolbar when the docking container is destroyed. Also, hide
	 * the docking container if the user clicks the Close button, rather than
	 * destroying the window.
	 */
	g_lpdiToolbar->dwStyle |= DWS_NODESTROY | DWS_DESTROYFOCUSWIN | DWS_NORESIZE;

	LoadString(g_hInstance, IDS_TOOLBAR_CAPTION, szCaption, NUM_ELEMENTS(szCaption));
	DockingCreateFrame(g_lpdiToolbar, szCaption);

	/* Move the toolbar from the parent to the docking container. The MDI frame
	 * will continue to get notification messages, though.
	 */
	g_lpdiToolbar->focusWindow = hwndTB;
	SetParent(hwndTB, g_lpdiToolbar->hwnd);
	ShowWindow(hwndTB, SW_SHOW);

	/* Initially disable buttons that require a map. */
	DisableNoWindowTBButtons();

	DockingShowFrame(g_lpdiToolbar);

	return TRUE;
}


/* DestroyToolbarWindow
 *   Destroys the toolbar window.
 *
 * Parameters:
 *   None.
 *
 * Return value: None.
 *
 * Remarks:
 *   This destroys the docking container, which in turn destroys the toolbar
 *    control.
 */
void DestroyToolbarWindow(void)
{
	if(IsWindow(g_lpdiToolbar->hwnd))
	{
		HIMAGELIST himl;
		HWND hwndTB = g_lpdiToolbar->focusWindow;

		himl = (HIMAGELIST)SendMessage(hwndTB, TB_GETIMAGELIST, 0, 0);
		DestroyWindow(g_lpdiToolbar->hwnd);
		ImageList_Destroy(himl);
	}
}


/* GetToolbarWindow
 *   Retrieves the handle to the toolbar control.
 *
 * Parameters:
 *   None.
 *
 * Return value: HWND
 *   Toolbar window handle.
 */
HWND GetToolbarWindow(void)
{
	return g_lpdiToolbar->focusWindow;
}


/* DisableNoWindowTBButtons
 *   Disables all toolbar buttons that should not be used while there is no map
 *   open.
 *
 * Parameters:
 *   None.
 *
 * Return value: None.
 */
void DisableNoWindowTBButtons(void)
{
	int i;

	const HWND hwndTB = g_lpdiToolbar->focusWindow;
	const int iButtonIDs[] =
	{
		IDM_FILE_SAVE, IDM_EDIT_CUT, IDM_EDIT_COPY, IDM_EDIT_PASTE,
		IDM_EDIT_UNDO, IDM_EDIT_REDO, IDM_PROPERTIES, IDM_EDIT_FIND,
		IDM_EDIT_REPLACE, IDM_VIEW_MODE_ANY, IDM_VIEW_MODE_LINES,
		IDM_VIEW_MODE_SECTORS, IDM_VIEW_MODE_VERTICES, IDM_VIEW_MODE_THINGS,
		IDM_CANCEL, IDM_VIEW_MODE_MOVE, IDM_INSERTLINES, IDM_INSERTRECT,
		IDM_INSERTVERTEX, IDM_INSERTTHING, IDM_FILE_TEST,
		IDM_EDIT_TRANSFORMSELECTION_FLIPHORIZONTALLY,
		IDM_EDIT_TRANSFORMSELECTION_FLIPVERTICALLY,
		IDM_EDIT_TRANSFORMSELECTION_ROTATE, IDM_EDIT_TRANSFORMSELECTION_RESIZE,
		IDM_EDIT_SNAPTOGRID, IDM_EDIT_SNAPTOVERTICES, IDM_EDIT_SNAPTOLINES,
		IDM_VIEW_CENTRE, IDM_LINES_FLIPLINEDEFS, IDM_LINES_SPLITLINEDEFS,
		IDM_SECTORS_JOIN, IDM_SECTORS_MERGE
	};


	for(i = 0; i < (int)NUM_ELEMENTS(iButtonIDs); i++)
		SendMessage(hwndTB, TB_ENABLEBUTTON, iButtonIDs[i], MAKELONG(FALSE, 0));
}


/* EnableWindowTBButtons
 *   Enables all toolbar buttons that should always be enabled while there is a
 *   map open.
 *
 * Parameters:
 *   None.
 *
 * Return value: None.
 */
void EnableWindowTBButtons(void)
{
	int i;

	const HWND hwndTB = g_lpdiToolbar->focusWindow;
	const int iButtonIDs[] =
	{
		IDM_FILE_SAVE, IDM_EDIT_FIND, IDM_EDIT_REPLACE, IDM_VIEW_MODE_ANY,
		IDM_VIEW_MODE_LINES, IDM_VIEW_MODE_SECTORS, IDM_VIEW_MODE_VERTICES,
		IDM_VIEW_MODE_THINGS, IDM_CANCEL, IDM_VIEW_MODE_MOVE, IDM_FILE_TEST,
		IDM_EDIT_SNAPTOGRID, IDM_EDIT_SNAPTOVERTICES, IDM_EDIT_SNAPTOLINES,
		IDM_VIEW_CENTRE
	};


	for(i = 0; i < (int)NUM_ELEMENTS(iButtonIDs); i++)
		SendMessage(hwndTB, TB_ENABLEBUTTON, iButtonIDs[i], MAKELONG(TRUE, 0));
}


/* IsToolbarVisible: Returns whether the toolbar is visible. */
BOOL IsToolbarVisible(void) { return !(g_lpdiToolbar->dwStyle & DWS_HIDDEN); }

/* ToggleToolbarVisibility: Toggles toolbar visibility. */
void ToggleToolbarVisibility(void)
{
	if(IsToolbarVisible()) SendMessage(g_lpdiToolbar->hwnd, WM_CLOSE, 0, 0);
	else DockingShowFrame(g_lpdiToolbar);
}

/* Get/SetToolbarState: Gets/sets toolbar state for saving across sessions. */
void GetToolbarState(LPDOCKSAVESTATE lpdss) { DockingSavePlacement(g_lpdiToolbar, lpdss); }
void SetToolbarState(LPDOCKSAVESTATE lpdss) { DockingLoadPlacement(g_lpdiToolbar, lpdss); }


/* GetToolbarDI
 * Gets the DOCKINFO structure for the toolbar.
 *
 * Parameters:
 *   None.
 *
 * Return value: LPDOCKINFO
 *   Pointer to DOCKINFO structure for the toolbar.
 *
 * Remarks:
 *   Routines outside this file should not be messing with the structure, but
 *   sometimes it's necessary for aggregating DI structures across docking
 *   windows and passing them back into another routine in here.
 */
LPDOCKINFO GetToolbarDI(void)
{
	return g_lpdiToolbar;
}
