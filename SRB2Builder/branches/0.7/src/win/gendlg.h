/* SRB2 Workbench -- A map editor for Sonic Robo Blast 2.
 * Copyright (C) 2009 Gregor Dick
 * http://workbench.srb2.org/
 *
 * Incorporates portions of Doom Builder, (C) 2003 Pascal vd Heiden.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 * USA.
 *
 * gendlg.h: Header for gendlg.c.
 *
 * AMDG.
 */

#ifndef __SRB2B_GENDLG__
#define __SRB2B_GENDLG__

#include <windows.h>
#include <commctrl.h>

#include "../config.h"


enum ENUM_LV_3STATE
{
	LV3_UNCHECKED = 1,
	LV3_INDETERMINATE,
	LV3_CHECKED
};

typedef struct _GRIDDLGDATA
{
	BOOL bShowGrid, bShowAxes, bShow64Grid;
	unsigned short cx, cy;
	short xOffset, yOffset;
} GRIDDLGDATA;


extern HWND g_hwndLastModelessDialogue;


/* SetDlgItemANSIText
 *   Sets a control's text from an ANSI string.
 *
 * Parameters:
 *   HWND		hwndDlg		Dialogue handle.
 *   int		iControlID	Control ID.
 *   LPCSTR		sz			ANSI string.
 *
 * Return value: BOOL
 *   Return value of SetDlgItemTextA.
 */
#ifdef UNICODE
static __inline BOOL SetDlgItemANSIText(HWND hwndDlg, int iControlID, LPCSTR sz)
{
	DWORD cch = strlen(sz) + 1;
	LPWSTR szW = ProcHeapAlloc(cch * sizeof(WCHAR));
	BOOL bRet;

	MultiByteToWideChar(CP_ACP, MB_PRECOMPOSED, sz, -1, szW, cch);
	bRet = SetDlgItemTextW(hwndDlg, iControlID, szW);

	ProcHeapFree(szW);

	return bRet;
}
#else
#define SetDlgItemANSIText SetDlgItemTextA
#endif


/* GetDlgItemANSIText
 *   Gets a control's text in ANSI, with the possible loss of information that
 *   that implies.
 *
 * Parameters:
 *   HWND		hwndDlg		Dialogue handle.
 *   int		iControlID	Control ID.
 *   LPSTR		sz			Buffer in which to place ANSI string.
 *   DWORD		cch			Length of buffer, including room for terminator.
 *
 * Return value: UINT
 *   Return value of GetDlgItemTextA.
 */
#ifdef UNICODE
static __inline UINT GetDlgItemANSIText(HWND hwndDlg, int iControlID, LPSTR sz, DWORD cch)
{
	LPWSTR szW = ProcHeapAlloc(cch * sizeof(WCHAR));
	UINT uiRet;

	uiRet = GetDlgItemTextW(hwndDlg, iControlID, szW, cch);
	WideCharToMultiByte(CP_ACP, 0, szW, -1, sz, cch, NULL, NULL);

	ProcHeapFree(szW);

	return uiRet;
}
#else
#define GetDlgItemANSIText GetDlgItemTextA
#endif



int ListBoxSearchByItemData(HWND hwndListBox, int iItemData, BOOL bSelect);
int ComboBoxSearchByItemData(HWND hwndCombo, int iItemData, BOOL bSelect);
BOOL BoundEditBox(HWND hwndEditBox, int iMin, int iMax, BOOL bPreserveEmpty, BOOL bPreserveRelative);
BOOL BoundEditBoxFloat(HWND hwndEditBox, float fMin, float fMax, BOOL bPreserveEmpty, BOOL bPreserveRelative);
float GetDlgItemFloat(HWND hwndDlg, int iDlgItem, BOOL *lpbTranslated);
void Init3StateListView(HWND hwndListView);
void ListView3StateClick(LPNMLISTVIEW lpnmlistview);
void ListView3StateKeyDown(LPNMLVKEYDOWN lpnmlvkeydown);
void ListView3StateToggleItem(HWND hwndListView, int iIndex);
void ListView3StateSetItemState(HWND hwndListView, int iIndex, int iState);
int ListView3StateGetItemState(HWND hwndListView, int iIndex);
INT_PTR CALLBACK AboutDlgProc(HWND hwndDlg, UINT uiMsg, WPARAM wParam, LPARAM lParam);
BOOL ShowGridDlg(HWND hwndParent, GRIDDLGDATA *lpgriddd);
BOOL TestDlg(HWND hwndParent, CONFIG *lpcfgTest);
void AddStringToListBoxCallback(LPCTSTR szString, void *lpvWindow);
int CALLBACK PropSheetCentreCallback(HWND hwndDlg, UINT uiMsg, LPARAM lParam);
void CentreWindowInParent(HWND hwnd);

#endif
