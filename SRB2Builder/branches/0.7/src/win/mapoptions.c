/* SRB2 Workbench -- A map editor for Sonic Robo Blast 2.
 * Copyright (C) 2009 Gregor Dick
 * http://workbench.srb2.org/
 *
 * Incorporates portions of Doom Builder, (C) 2003 Pascal vd Heiden.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 * USA.
 *
 * mapoptions.c: UI for settings affecting an entire map, e.g. the lumpname.
 *
 * AMDG.
 */

#include <windows.h>
#include <commctrl.h>
#include <objbase.h>
#include <shldisp.h>
#include <shlwapi.h>

#include "../general.h"
#include "../config.h"
#include "../mapconfig.h"
#include "../testing.h"
#include "../cdlgwrapper.h"
#include "../texture.h"
#include "../wadopts.h"
#include "../texenum.h"
#include "../options.h"
#include "../openwads.h"

#include "mdiframe.h"
#include "mapoptions.h"
#include "editdlg.h"
#include "texbrowser.h"
#include "gendlg.h"
#include "wadlist.h"

#include "../../res/resource.h"


#define MAPOPT_NUMPAGES 3

#ifndef SHACF_FILESYS_ONLY
#define SHACF_FILESYS_ONLY		0x10
#endif


static INT_PTR CALLBACK MapOptCfgPropProc(HWND hwndPropPage, UINT uiMsg, WPARAM wParam, LPARAM lParam);
static INT_PTR CALLBACK MapOptDefaultsPropProc(HWND hwndPropPage, UINT uiMsg, WPARAM wParam, LPARAM lParam);
static INT_PTR CALLBACK MapOptNodesPropProc(HWND hwndPropPage, UINT uiMsg, WPARAM wParam, LPARAM lParam);
static INT_PTR CALLBACK NewMapDlgProc(HWND hwndDlg, UINT uiMsg, WPARAM wParam, LPARAM lParam);
static BOOL AddListedWadToList(int iWad, void *lpvList);
static void UpdateNewMapControlStates(HWND hwndDlg);



/* ShowMapProperties
 *   Shows the Properties dialogue for the selection.
 *
 * Parameters:
 *   MAPPOPTPDATA*	lpmapoptppdata		Current properties.
 *
 * Return value: BOOL
 *   TRUE if the user OKed to exit the dialogue; FALSE if cancelled.
 *
 * Remarks:
 *   All the processing is done by the property sheet pages. This function just
 *   shows the dialogue box.
 */
BOOL ShowMapProperties(MAPOPTPPDATA *lpmapoptppdata)
{
	PROPSHEETHEADER		psh;
	PROPSHEETPAGE		psp[MAPOPT_NUMPAGES];
	int					i;


	/* Build the info for the individual pages. */

	psp[0].pfnDlgProc = MapOptCfgPropProc;
	psp[0].pszTemplate = MAKEINTRESOURCE(IDD_PP_MAP_CFG);

	psp[1].pfnDlgProc = MapOptDefaultsPropProc;
	psp[1].pszTemplate = MAKEINTRESOURCE(IDD_PP_MAP_DEFAULTS);

	psp[2].pfnDlgProc = MapOptNodesPropProc;
	psp[2].pszTemplate = MAKEINTRESOURCE(IDD_PP_MAP_NODES);


	/* Fill in fields common to all pages. */
	for(i = 0; i < MAPOPT_NUMPAGES; i++)
	{
		psp[i].dwSize = sizeof(PROPSHEETPAGE);
		psp[i].dwFlags = /*PSP_HASHELP */ 0;
		psp[i].lParam = (LONG)lpmapoptppdata;
		psp[i].hInstance = g_hInstance;
	}

	/* Properties affecting the whole dialogue. */
	psh.dwSize = sizeof(psh);
	psh.dwFlags = PSH_HASHELP | PSH_NOAPPLYNOW | PSH_PROPTITLE | PSH_PROPSHEETPAGE | PSH_USECALLBACK;
	psh.hwndParent = g_hwndMain;
	psh.nPages = MAPOPT_NUMPAGES;
	psh.nStartPage = 0;
	psh.ppsp = psp;
	psh.pszCaption = MAKEINTRESOURCE(IDS_MAPOPTCAPTION);
	psh.hInstance = g_hInstance;
	psh.pfnCallback = PropSheetCentreCallback;

	/* Assume we OKed. */
	lpmapoptppdata->bOKed = TRUE;

	/* Go! */
	PropertySheet(&psh);

	/* Did we close by clicking OK? */
	return lpmapoptppdata->bOKed;
}



/* MapOptCfgPropProc
 *   Dialogue proc for Configuration page of Map Properties dialogue.
 *
 * Parameters:
 *   HWND	hwndPropPage	Property page window handle.
 *   UINT	uiMsg			Message identifier.
 *   WPARAM wParam			Message-specific.
 *   LPARAM lParam			Message-specific.
 *
 * Return value: INT_PTR
 *   TRUE if the message was processed; FALSE otherwise.
 */
static INT_PTR CALLBACK MapOptCfgPropProc(HWND hwndPropPage, UINT uiMsg, WPARAM wParam, LPARAM lParam)
{
	static MAPOPTPPDATA *s_lpmapoptppdata;

	switch(uiMsg)
	{
	case WM_INITDIALOG:
		{
			HWND hwndMapConfigs = GetDlgItem(hwndPropPage, IDC_COMBO_CONFIG);
			HWND hwndGametypes = GetDlgItem(hwndPropPage, IDC_COMBO_TESTMODE);
			int iCount, i;
			TCHAR szLumpName[9];
			char cGametype;
			DWORD cchAddWad;

			/* Get parameters. */
			s_lpmapoptppdata = (MAPOPTPPDATA*)((PROPSHEETPAGE*)lParam)->lParam;

			/* Fill the map config list and select the current one. */
			AddMapConfigsToList(hwndMapConfigs, AMC_COMBOBOX);

			/* Find a matching config. */
			iCount = SendMessage(hwndMapConfigs, CB_GETCOUNT, 0, 0);
			for(i = 0; i < iCount; i++)
			{
				CONFIG *lpcfg = (CONFIG*)SendMessage(hwndMapConfigs, CB_GETITEMDATA, i, 0);
				if(lpcfg == s_lpmapoptppdata->lpcfgMap)
				{
					SendMessage(hwndMapConfigs, CB_SETCURSEL, i, 0);
					break;
				}
			}

			/* Get the lumpname. */
#ifdef _UNICODE
			MultiByteToWideChar(CP_ACP, MB_PRECOMPOSED, s_lpmapoptppdata->szLumpName, -1, szLumpName, sizeof(szLumpName)/sizeof(WCHAR));
#else
			strcpy(szLumpName, s_lpmapoptppdata->szLumpName);
#endif
			SetWindowText(GetDlgItem(hwndPropPage, IDC_EDIT_LUMPNAME), szLumpName);


			/* Fill the list of gametypes. */
			PopulateGametypeList(hwndGametypes);

			/* Find a matching gametype. */
			iCount = SendMessage(hwndGametypes, CB_GETCOUNT, 0, 0);
			cGametype = (char)ConfigGetInteger(s_lpmapoptppdata->lpcfgWadOptMap, WADOPT_MAP_GAMETYPE);
			for(i = 0; i < iCount; i++)
			{
				char cListGametype = (char)SendMessage(hwndGametypes, CB_GETITEMDATA, i, 0);
				if(cListGametype == cGametype)
				{
					SendMessage(hwndGametypes, CB_SETCURSEL, i, 0);
					break;
				}
			}

			/* Enable autocomplete on the external-wad edit box. */
			SHAutoComplete(GetDlgItem(hwndPropPage, IDC_EDIT_EXTWAD), SHACF_FILESYS_ONLY);

			/* Set the external wad filename. */
			cchAddWad = ConfigGetStringLength(s_lpmapoptppdata->lpcfgWadOptMap, WADOPT_MAP_ADDWAD) + 1;
			if(cchAddWad > 1)
			{
				LPTSTR szAddWad = ProcHeapAlloc(cchAddWad * sizeof(TCHAR));
				ConfigGetString(s_lpmapoptppdata->lpcfgWadOptMap, WADOPT_MAP_ADDWAD, szAddWad, cchAddWad);
				SetDlgItemText(hwndPropPage, IDC_EDIT_EXTWAD, szAddWad);
				ProcHeapFree(szAddWad);
			}
		}

		/* Let the system set the focus. */
		return TRUE;

	case WM_COMMAND:
		switch(LOWORD(wParam))
		{
		case IDC_BUTTON_BROWSE:
			{
				TCHAR szFilename[MAX_PATH];
				TCHAR szFilter[128];

				/* As initial filename, use that currently specified. */
				GetDlgItemText(hwndPropPage, IDC_EDIT_EXTWAD, szFilename, sizeof(szFilename)/sizeof(TCHAR));

				/* Load the filter from the string table. */
				LoadAndFormatFilterString(IDS_WADFILEFILTER, szFilter, sizeof(szFilter)/sizeof(TCHAR));

				/* Show the Open dialogue. */
				if(CommDlgOpen(hwndPropPage, szFilename, sizeof(szFilename)/sizeof(TCHAR), NULL, szFilter, TEXT("wad"), szFilename, OFN_EXPLORER | OFN_FILEMUSTEXIST | OFN_HIDEREADONLY))
				{
					/* If the user didn't cancel, update the filename. */
					SetDlgItemText(hwndPropPage, IDC_EDIT_EXTWAD, szFilename);
				}
			}

			return TRUE;
		}

		/* Didn't process message. */
		break;

	case WM_NOTIFY:
		switch(((LPNMHDR)lParam)->code)
		{
		case PSN_APPLY:
			{
				int iSelection;
				TCHAR szLumpnameT[CCH_LUMPNAME + 1];
				DWORD cchAddWad;

				/* Assume we don't fail any validation. This may change. */
				SetWindowLong(hwndPropPage, DWL_MSGRESULT, PSNRET_NOERROR);

				/* Map configuration. */
				iSelection = SendDlgItemMessage(hwndPropPage, IDC_COMBO_CONFIG, CB_GETCURSEL, 0, 0);
				if(iSelection != CB_ERR)
					s_lpmapoptppdata->lpcfgMap = (CONFIG*)SendDlgItemMessage(hwndPropPage, IDC_COMBO_CONFIG, CB_GETITEMDATA, iSelection, 0);
				else
					s_lpmapoptppdata->lpcfgMap = NULL;

				/* Gametype. */
				iSelection = SendDlgItemMessage(hwndPropPage, IDC_COMBO_TESTMODE, CB_GETCURSEL, 0, 0);
				if(iSelection != CB_ERR)
					ConfigSetInteger(s_lpmapoptppdata->lpcfgWadOptMap, WADOPT_MAP_GAMETYPE, SendDlgItemMessage(hwndPropPage, IDC_COMBO_TESTMODE, CB_GETITEMDATA, iSelection, 0));

				/* Lumpname. */
				GetDlgItemText(hwndPropPage, IDC_EDIT_LUMPNAME, szLumpnameT, NUM_ELEMENTS(szLumpnameT));
#ifdef UNICODE
				WideCharToMultiByte(CP_ACP, 0, szLumpnameT, -1, s_lpmapoptppdata->szLumpName, NUM_ELEMENTS(s_lpmapoptppdata->szLumpName), NULL, NULL);
#else
				strcpy(s_lpmapoptppdata->szLumpName, szLumpnameT);
#endif

				/* Additional wad. */
				cchAddWad = GetWindowTextLength(GetDlgItem(hwndPropPage, IDC_EDIT_EXTWAD)) + 1;
				if(cchAddWad > 1)
				{
					LPTSTR szAddWad = ProcHeapAlloc(cchAddWad * sizeof(TCHAR));
					GetDlgItemText(hwndPropPage, IDC_EDIT_EXTWAD, szAddWad, cchAddWad);
					ConfigSetString(s_lpmapoptppdata->lpcfgWadOptMap, WADOPT_MAP_ADDWAD, szAddWad);
					ProcHeapFree(szAddWad);
				}
				else ConfigSetString(s_lpmapoptppdata->lpcfgWadOptMap, WADOPT_MAP_ADDWAD, TEXT(""));
			}

			return TRUE;

		case PSN_KILLACTIVE:
			{
				/* Validate the lumpname. Silently truncate it. */
				TCHAR szLumpname[CCH_LUMPNAME + 1];

#ifdef _UNICODE
				CHAR szLumpnameA[CCH_LUMPNAME + 1];
#else
				LPSTR szLumpnameA = szLumpname;
#endif

				/* Assume failure. */
				BOOL bValid = FALSE;

				GetDlgItemText(hwndPropPage, IDC_EDIT_LUMPNAME, szLumpname, NUM_ELEMENTS(szLumpname));

#ifdef _UNICODE
				WideCharToMultiByte(CP_ACP, 0, szLumpname, -1, szLumpnameA, NUM_ELEMENTS(szLumpnameA), NULL, NULL);
#endif

				if(!IsMapLumpnameA(szLumpnameA))
					MessageBoxFromStringTable(hwndPropPage, IDS_ERROR_MAPLUMPNAME, MB_ICONERROR);
				else if(strcmp(s_lpmapoptppdata->szLumpName, szLumpnameA) != 0 && GetLumpIndex(s_lpmapoptppdata->lpwad, 0, -1, szLumpnameA) >= 0)
					MessageBoxFromStringTable(hwndPropPage, IDS_ERROR_MAPEXISTS, MB_ICONERROR);
				else
					bValid = TRUE;

				SetWindowLong(hwndPropPage, DWL_MSGRESULT, !bValid);
			}

			return TRUE;

		case PSN_QUERYCANCEL:
			/* Allow the cancel, and signal that we did so. */
			SetWindowLong(hwndPropPage, DWL_MSGRESULT, FALSE);
			s_lpmapoptppdata->bOKed = FALSE;
			return TRUE;
		}

		/* Didn't process WM_NOTIFY message. */
		break;
	}

	return FALSE;
}


/* PopulateGametypeList
 *   Fills a combobox with gametypes.
 *
 * Parameters:
 *   HWND	hwndCombo	Window handle of combobox.
 *
 * Return value: None.
 */
void PopulateGametypeList(HWND hwndCombo)
{
	struct GTPAIR {char cGametype; USHORT unStringID;} gtpair[] =
	{
		{GT_DEFAULT, IDS_GT_DEFAULT},
		{GT_COOP, IDS_GT_COOP},
		{GT_FULLRACE, IDS_GT_FULLRACE},
		{GT_TIMEONLYRACE, IDS_GT_TIMEONLYRACE},
		{GT_MATCH, IDS_GT_MATCH},
		{GT_TAG, IDS_GT_TAG},
		{GT_CTF, IDS_GT_CTF},
		{GT_SINGLEPLAYER, IDS_GT_SINGLEPLAYER}
	};

	int i;

	for(i = 0; (unsigned int)i < sizeof(gtpair) / sizeof(struct GTPAIR); i++)
	{
		TCHAR szGametype[64];
		int iIndex;

		LoadString(g_hInstance, gtpair[i].unStringID, szGametype, sizeof(szGametype) / sizeof(TCHAR));
		iIndex = SendMessage(hwndCombo, CB_ADDSTRING, 0, (LPARAM)szGametype);

		SendMessage(hwndCombo, CB_SETITEMDATA, iIndex, gtpair[i].cGametype);
	}
}


/* MapOptDefaultsPropProc
 *   Dialogue proc for Defaults page of Map Properties dialogue.
 *
 * Parameters:
 *   HWND	hwndPropPage	Property page window handle.
 *   UINT	uiMsg			Message identifier.
 *   WPARAM wParam			Message-specific.
 *   LPARAM lParam			Message-specific.
 *
 * Return value: INT_PTR
 *   TRUE if the message was processed; FALSE otherwise.
 */
static INT_PTR CALLBACK MapOptDefaultsPropProc(HWND hwndPropPage, UINT uiMsg, WPARAM wParam, LPARAM lParam)
{
	static MAPOPTPPDATA *s_lpmapoptppdata;

	static TPINFO s_tpinfo[] =
	{
		{IDC_TEX_UPPER,		IDC_EDIT_UPPER,		TF_TEXTURE,	NULL,	NULL,	NULL},
		{IDC_TEX_MIDDLE,	IDC_EDIT_MIDDLE,	TF_TEXTURE,	NULL,	NULL,	NULL},
		{IDC_TEX_LOWER,		IDC_EDIT_LOWER,		TF_TEXTURE,	NULL,	NULL,	NULL},
		{IDC_TEX_CEIL,		IDC_EDIT_TCEIL,		TF_FLAT,	NULL,	NULL,	NULL},
		{IDC_TEX_FLOOR,		IDC_EDIT_TFLOOR,	TF_FLAT,	NULL,	NULL,	NULL}
	};

	switch(uiMsg)
	{
	case WM_INITDIALOG:
		{
			HDC hdcDlg;
			int i;
			TCHAR szTexName[TEXNAME_BUFFER_LENGTH];
			CONFIG *lpcfgMapDefTex, *lpcfgMapDefSec;
			UDACCEL udaccelHeight[2], udaccelBrightness[2];
			BOOL bAutoComplete = ConfigGetInteger(g_lpcfgMain, OPT_AUTOCOMPLETETEX);

			/* Get parameters. */
			s_lpmapoptppdata = (MAPOPTPPDATA*)((PROPSHEETPAGE*)lParam)->lParam;

			/* Get subsections of config. */
			lpcfgMapDefTex = ConfigGetSubsection(s_lpmapoptppdata->lpcfgWadOptMap, WADOPT_DEFAULTTEX);
			lpcfgMapDefSec = ConfigGetSubsection(s_lpmapoptppdata->lpcfgWadOptMap, WADOPT_DEFAULTSEC);

			/* Subclass the texture preview controls, create their preview
			 * bitmaps and initialise autocomplete.
			 */
			hdcDlg = GetDC(hwndPropPage);
			for(i = 0; i < (int)NUM_ELEMENTS(s_tpinfo); i++)
			{
				HWND hwndTex = GetDlgItem(hwndPropPage, s_tpinfo[i].iTPID);

				/* Allocate memory for the preview window. It's freed when the
				 * window's destroyed.
				 */
				TPPDATA *lptppd = ProcHeapAlloc(sizeof(TPPDATA));

				lptppd->wndprocStatic = (WNDPROC)GetWindowLong(hwndTex, GWL_WNDPROC);

				SetWindowLong(hwndTex, GWL_USERDATA, (LONG)lptppd);
				SetWindowLong(hwndTex, GWL_WNDPROC, (LONG)TexPreviewProc);

				/* Create preview bitmap. */
				s_tpinfo[i].hbitmap = CreateCompatibleBitmap(hdcDlg, CX_TEXPREVIEW, CY_TEXPREVIEW);
				SendDlgItemMessage(hwndPropPage, s_tpinfo[i].iTPID, STM_SETIMAGE, IMAGE_BITMAP, (LPARAM)s_tpinfo[i].hbitmap);

				/* Set the preview to something sensible in case we never touch
				 * it again.
				 */
				SetTexturePreviewImage(s_lpmapoptppdata->hwndMap, hwndTex, TEXT(""), TF_TEXTURE, s_tpinfo[i].hbitmap, FALSE);

				if(bAutoComplete)
				{
					if((s_tpinfo[i].lptexenum = CreateTexEnum()) && (s_tpinfo[i].lpac2 = CreateAutoComplete()))
					{
						SetTexEnumList(s_tpinfo[i].lptexenum, s_tpinfo[i].tf == TF_TEXTURE ? s_lpmapoptppdata->lptnlTextures : s_lpmapoptppdata->lptnlFlats);
						s_tpinfo[i].lpac2->lpVtbl->Init(s_tpinfo[i].lpac2, GetDlgItem(hwndPropPage, s_tpinfo[i].iEditID), (IUnknown*)s_tpinfo[i].lptexenum, NULL, NULL);
					}
				}
			}

			ReleaseDC(hwndPropPage, hdcDlg);


			/* Set up the edit boxes. */

			ConfigGetString(lpcfgMapDefTex, TEXT("upper"), szTexName, TEXNAME_BUFFER_LENGTH);
			SetDlgItemText(hwndPropPage, IDC_EDIT_UPPER, szTexName);

			ConfigGetString(lpcfgMapDefTex, TEXT("middle"), szTexName, TEXNAME_BUFFER_LENGTH);
			SetDlgItemText(hwndPropPage, IDC_EDIT_MIDDLE, szTexName);

			ConfigGetString(lpcfgMapDefTex, TEXT("lower"), szTexName, TEXNAME_BUFFER_LENGTH);
			SetDlgItemText(hwndPropPage, IDC_EDIT_LOWER, szTexName);

			ConfigGetString(lpcfgMapDefSec, TEXT("tceiling"), szTexName, TEXNAME_BUFFER_LENGTH);
			SetDlgItemText(hwndPropPage, IDC_EDIT_TCEIL, szTexName);

			ConfigGetString(lpcfgMapDefSec, TEXT("tfloor"), szTexName, TEXNAME_BUFFER_LENGTH);
			SetDlgItemText(hwndPropPage, IDC_EDIT_TFLOOR, szTexName);

			SetDlgItemInt(hwndPropPage, IDC_EDIT_HCEIL, ConfigGetInteger(lpcfgMapDefSec, TEXT("hceiling")), TRUE);
			SetDlgItemInt(hwndPropPage, IDC_EDIT_HFLOOR, ConfigGetInteger(lpcfgMapDefSec, TEXT("hfloor")), TRUE);
			SetDlgItemInt(hwndPropPage, IDC_EDIT_BRIGHTNESS, ConfigGetInteger(lpcfgMapDefSec, TEXT("brightness")), FALSE);


			/* Set up the spinners. */

			udaccelHeight[0].nInc = 8;
			udaccelHeight[0].nSec = 0;
			udaccelHeight[1].nInc = 64;
			udaccelHeight[1].nSec = 2;

			udaccelBrightness[0].nInc = 1;
			udaccelBrightness[0].nSec = 0;
			udaccelBrightness[1].nInc = 8;
			udaccelBrightness[1].nSec = 2;

			SendDlgItemMessage(hwndPropPage, IDC_SPIN_BRIGHTNESS, UDM_SETRANGE32, 0, 255);
			SendDlgItemMessage(hwndPropPage, IDC_SPIN_BRIGHTNESS, UDM_SETACCEL, NUM_ELEMENTS(udaccelBrightness), (LPARAM)udaccelBrightness);

			SendDlgItemMessage(hwndPropPage, IDC_SPIN_HCEIL, UDM_SETRANGE32, -32768, 32767);
			SendDlgItemMessage(hwndPropPage, IDC_SPIN_HCEIL, UDM_SETACCEL, NUM_ELEMENTS(udaccelHeight), (LPARAM)udaccelHeight);

			SendDlgItemMessage(hwndPropPage, IDC_SPIN_HFLOOR, UDM_SETRANGE32, -32768, 32767);
			SendDlgItemMessage(hwndPropPage, IDC_SPIN_HFLOOR, UDM_SETACCEL, NUM_ELEMENTS(udaccelHeight), (LPARAM)udaccelHeight);
		}

		/* Let the system set the focus. */
		return TRUE;

	case WM_COMMAND:

		switch(LOWORD(wParam))
		{
		case IDC_TEX_UPPER:
		case IDC_TEX_MIDDLE:
		case IDC_TEX_LOWER:
		case IDC_TEX_CEIL:
		case IDC_TEX_FLOOR:

			switch(HIWORD(wParam))
			{
			case BN_CLICKED:
				{
					TCHAR szTexName[TEXNAME_BUFFER_LENGTH];
					int iPreviewIndex;
					BOOL bTexOKed;

					/* Find the corresponding edit box. */
					for(iPreviewIndex = 0; s_tpinfo[iPreviewIndex].iTPID != LOWORD(wParam); iPreviewIndex++);

					/* Get the current flat so it's selected initially. */
					GetDlgItemText(hwndPropPage, s_tpinfo[iPreviewIndex].iEditID, szTexName, TEXNAME_BUFFER_LENGTH);

					/* Show the texture browser. */
					if(s_tpinfo[iPreviewIndex].tf == TF_TEXTURE)
						bTexOKed = SelectTexture(hwndPropPage, s_lpmapoptppdata->hwndMap, TF_TEXTURE, s_lpmapoptppdata->lphimlCacheTextures, s_lpmapoptppdata->lptnlTextures, s_lpmapoptppdata->lpcfgTexUsed, 0, szTexName);
					else
						bTexOKed = SelectTexture(hwndPropPage, s_lpmapoptppdata->hwndMap, TF_FLAT, s_lpmapoptppdata->lphimlCacheFlats, s_lpmapoptppdata->lptnlFlats, s_lpmapoptppdata->lpcfgFlatsUsed, 0, szTexName);

					if(bTexOKed)
					{
						/* User didn't cancel. Update the edit-box. */
						SetDlgItemText(hwndPropPage, s_tpinfo[iPreviewIndex].iEditID, szTexName);
					}

					return TRUE;
				}
			}

			break;

		case IDC_EDIT_UPPER:
		case IDC_EDIT_MIDDLE:
		case IDC_EDIT_LOWER:
		case IDC_EDIT_TCEIL:
		case IDC_EDIT_TFLOOR:

			switch(HIWORD(wParam))
			{
			case EN_CHANGE:
				{
					TCHAR szEditText[TEXNAME_BUFFER_LENGTH];
					int iPreviewIndex;

					/* Find the corresponding preview control. */
					for(iPreviewIndex = 0; s_tpinfo[iPreviewIndex].iEditID != LOWORD(wParam); iPreviewIndex++);

					/* Get the texture name. */
					GetWindowText((HWND)lParam, szEditText, TEXNAME_BUFFER_LENGTH);

					/* Update the texture preview image. */
					SetTexturePreviewImage
						(s_lpmapoptppdata->hwndMap,
						 GetDlgItem(hwndPropPage, s_tpinfo[iPreviewIndex].iTPID),
						 szEditText,
						 s_tpinfo[iPreviewIndex].tf,
						 s_tpinfo[iPreviewIndex].hbitmap,
						 FALSE);

					return TRUE;
				}
			}

			break;

		case IDC_EDIT_HCEIL:
		case IDC_EDIT_HFLOOR:
			switch(HIWORD(wParam))
			{
			case EN_KILLFOCUS:
				/* Validation. */
				BoundEditBox((HWND)lParam, -32768, 32767, FALSE, FALSE);

				return TRUE;
			}

		case IDC_EDIT_BRIGHTNESS:
			switch(HIWORD(wParam))
			{
			case EN_KILLFOCUS:
				/* Validation. */
				BoundEditBox((HWND)lParam, 0, 255, FALSE, FALSE);

				return TRUE;
			}
		}

		/* Didn't process message. */
		break;

	case WM_NOTIFY:
		switch(((LPNMHDR)lParam)->code)
		{
		case PSN_APPLY:
			{
				TCHAR szTexName[TEXNAME_BUFFER_LENGTH];

				/* Get subsections of config. */
				CONFIG *lpcfgMapDefTex = ConfigGetSubsection(s_lpmapoptppdata->lpcfgWadOptMap, WADOPT_DEFAULTTEX);
				CONFIG *lpcfgMapDefSec = ConfigGetSubsection(s_lpmapoptppdata->lpcfgWadOptMap, WADOPT_DEFAULTSEC);

				/* Set the values! */
				GetDlgItemText(hwndPropPage, IDC_EDIT_UPPER, szTexName, NUM_ELEMENTS(szTexName));
				ConfigSetString(lpcfgMapDefTex, TEXT("upper"), szTexName);

				GetDlgItemText(hwndPropPage, IDC_EDIT_MIDDLE, szTexName, NUM_ELEMENTS(szTexName));
				ConfigSetString(lpcfgMapDefTex, TEXT("middle"), szTexName);

				GetDlgItemText(hwndPropPage, IDC_EDIT_LOWER, szTexName, NUM_ELEMENTS(szTexName));
				ConfigSetString(lpcfgMapDefTex, TEXT("lower"), szTexName);

				GetDlgItemText(hwndPropPage, IDC_EDIT_TCEIL, szTexName, NUM_ELEMENTS(szTexName));
				ConfigSetString(lpcfgMapDefSec, TEXT("tceiling"), szTexName);

				GetDlgItemText(hwndPropPage, IDC_EDIT_TFLOOR, szTexName, NUM_ELEMENTS(szTexName));
				ConfigSetString(lpcfgMapDefSec, TEXT("tfloor"), szTexName);

				ConfigSetInteger(lpcfgMapDefSec, TEXT("hceiling"), GetDlgItemInt(hwndPropPage, IDC_EDIT_HCEIL, NULL, TRUE));
				ConfigSetInteger(lpcfgMapDefSec, TEXT("hfloor"), GetDlgItemInt(hwndPropPage, IDC_EDIT_HFLOOR, NULL, TRUE));
				ConfigSetInteger(lpcfgMapDefSec, TEXT("brightness"), GetDlgItemInt(hwndPropPage, IDC_EDIT_BRIGHTNESS, NULL, FALSE));
			}

			return TRUE;

		case PSN_KILLACTIVE:

			/* Bound the values of the edit boxes. */
			BoundEditBox(GetDlgItem(hwndPropPage, IDC_EDIT_HCEIL), -32768, 32767, FALSE, FALSE);
			BoundEditBox(GetDlgItem(hwndPropPage, IDC_EDIT_HFLOOR), -32768, 32767, FALSE, FALSE);
			BoundEditBox(GetDlgItem(hwndPropPage, IDC_EDIT_BRIGHTNESS), 0, 255, FALSE, FALSE);

			SetWindowLong(hwndPropPage, DWL_MSGRESULT, FALSE);
			return TRUE;

		case PSN_QUERYCANCEL:
			/* Allow the cancel, and signal that we did so. */
			SetWindowLong(hwndPropPage, DWL_MSGRESULT, FALSE);
			s_lpmapoptppdata->bOKed = FALSE;
			return TRUE;
		}

		/* Didn't process message. */
		break;

	case WM_DESTROY:
		{
			int i;

			/* Destroy preview bitmaps and clean up autocomplete. */
			for(i = 0; i < (int)NUM_ELEMENTS(s_tpinfo); i++)
			{
				DeleteObject(s_tpinfo[i].hbitmap);
				if(s_tpinfo[i].lpac2) s_tpinfo[i].lpac2->lpVtbl->Release(s_tpinfo[i].lpac2);
				if(s_tpinfo[i].lptexenum) DestroyTexEnum(s_tpinfo[i].lptexenum);
			}
		}
	}

	/* Didn't process message. */
	return FALSE;
}


/* MapOptNodesPropProc
 *   Dialogue proc for Nodes page of Map Properties dialogue.
 *
 * Parameters:
 *   HWND	hwndPropPage	Property page window handle.
 *   UINT	uiMsg			Message identifier.
 *   WPARAM wParam			Message-specific.
 *   LPARAM lParam			Message-specific.
 *
 * Return value: INT_PTR
 *   TRUE if the message was processed; FALSE otherwise.
 */
static INT_PTR CALLBACK MapOptNodesPropProc(HWND hwndPropPage, UINT uiMsg, WPARAM wParam, LPARAM lParam)
{
	static MAPOPTPPDATA *s_lpmapoptppdata;

	switch(uiMsg)
	{
	case WM_INITDIALOG:
		{
			/* Get parameters. */
			s_lpmapoptppdata = (MAPOPTPPDATA*)((PROPSHEETPAGE*)lParam)->lParam;

			/* Internal nodebuilder doesn't exist yet. */
			EnableWindow(GetDlgItem(hwndPropPage, IDC_RADIO_NODES_INTERNAL), FALSE);
			EnableWindow(GetDlgItem(hwndPropPage, IDC_CHECK_BLOCKMAP), FALSE);

			/* Set states. */
			switch(ConfigGetInteger(s_lpmapoptppdata->lpcfgWadOpt, WADOPT_NODEBUILD))
			{
			case NODEBUILD_INTERNAL:
				CheckDlgButton(hwndPropPage, IDC_RADIO_NODES_INTERNAL, BST_CHECKED);
				break;
			case NODEBUILD_EXTERNAL:
				CheckDlgButton(hwndPropPage, IDC_RADIO_NODES_EXTERNAL, BST_CHECKED);
				break;
			case NODEBUILD_DISABLED:
				CheckDlgButton(hwndPropPage, IDC_RADIO_NODES_DONTBUILD, BST_CHECKED);
				break;
			}

			CheckDlgButton(hwndPropPage, IDC_CHECK_BLOCKMAP, ConfigGetInteger(s_lpmapoptppdata->lpcfgWadOpt, WADOPT_BLOCKMAP) ? BST_CHECKED : BST_UNCHECKED);
		}

		/* Let the system set the focus. */
		return TRUE;

	case WM_COMMAND:
		switch(LOWORD(wParam))
		{
		case IDC_RADIO_NODES_INTERNAL:
			EnableWindow(GetDlgItem(hwndPropPage, IDC_CHECK_BLOCKMAP), TRUE);
			return TRUE;

		case IDC_RADIO_NODES_EXTERNAL:
		case IDC_RADIO_NODES_DONTBUILD:
			EnableWindow(GetDlgItem(hwndPropPage, IDC_CHECK_BLOCKMAP), FALSE);
			return TRUE;
		}

		/* Didn't process message. */
		break;

	case WM_NOTIFY:
		switch(((LPNMHDR)lParam)->code)
		{
		case PSN_APPLY:
			{
				int iNodebuildType = NODEBUILD_DISABLED;

				/* We can't fail. */
				SetWindowLong(hwndPropPage, DWL_MSGRESULT, PSNRET_NOERROR);

				if(IsDlgButtonChecked(hwndPropPage, IDC_RADIO_NODES_INTERNAL) == BST_CHECKED)
					iNodebuildType = NODEBUILD_INTERNAL;
				else if(IsDlgButtonChecked(hwndPropPage, IDC_RADIO_NODES_EXTERNAL) == BST_CHECKED)
					iNodebuildType = NODEBUILD_EXTERNAL;

				ConfigSetInteger(s_lpmapoptppdata->lpcfgWadOpt, WADOPT_NODEBUILD, iNodebuildType);

				ConfigSetInteger(s_lpmapoptppdata->lpcfgWadOpt, WADOPT_BLOCKMAP, IsDlgButtonChecked(hwndPropPage, IDC_CHECK_BLOCKMAP) == BST_CHECKED);
			}

			return TRUE;

		case PSN_QUERYCANCEL:
			/* Allow the cancel, and signal that we did so. */
			SetWindowLong(hwndPropPage, DWL_MSGRESULT, FALSE);
			s_lpmapoptppdata->bOKed = FALSE;
			return TRUE;
		}

		/* Didn't process WM_NOTIFY message. */
		break;
	}

	return FALSE;
}


/* ShowNewMapDialogue
 *   Shows the New Map dialogue.
 *
 * Parameters:
 *   NEWMAPDLGDATA*		lpnmdd	Structure used for initialisation and return.
 *
 * Return value: BOOL
 *   TRUE if the user OKed to exit the dialogue; FALSE if cancelled.
 *
 * Remarks:
 *   The config and lumpname fields of the structure are used for
 *   initialisation.
 */
BOOL ShowNewMapDialogue(NEWMAPDLGDATA *lpnmdd)
{
	return DialogBoxParam(g_hInstance, MAKEINTRESOURCE(IDD_NEWMAP), g_hwndMain, NewMapDlgProc, (LPARAM)lpnmdd);
}


/* NewMapDlgProc
 *   Dialogue proc for New Map dialogue.
 *
 * Parameters:
 *   HWND	hwndDlg		Dialogue window handle.
 *   UINT	uiMsg		Message identifier.
 *   WPARAM wParam		Message-specific.
 *   LPARAM lParam		Message-specific.
 *
 * Return value: INT_PTR
 *   TRUE if the message was processed; FALSE otherwise.
 */
static INT_PTR CALLBACK NewMapDlgProc(HWND hwndDlg, UINT uiMsg, WPARAM wParam, LPARAM lParam)
{
	static NEWMAPDLGDATA *s_lpnmdd;

	switch(uiMsg)
	{
	case WM_INITDIALOG:
		{
			HWND hwndMapConfigs = GetDlgItem(hwndDlg, IDC_COMBO_CONFIG);
			HWND hwndWadList = GetDlgItem(hwndDlg, IDC_LIST_OPENWADS);
			int iCount, i;

			CentreWindowInParent(hwndDlg);

			/* Get the structure for initialisation and returning. */
			s_lpnmdd = (NEWMAPDLGDATA*)lParam;

			/* Fill the map config list and select the current one. */
			AddMapConfigsToList(hwndMapConfigs, AMC_COMBOBOX);

			/* Find a matching config. */
			iCount = SendMessage(hwndMapConfigs, CB_GETCOUNT, 0, 0);
			for(i = 0; i < iCount; i++)
			{
				CONFIG *lpcfg = (CONFIG*)SendMessage(hwndMapConfigs, CB_GETITEMDATA, i, 0);
				if(lpcfg == s_lpnmdd->lpcfgMap)
				{
					SendMessage(hwndMapConfigs, CB_SETCURSEL, i, 0);
					break;
				}
			}

			/* Set lumpname. */
			SetDlgItemANSIText(hwndDlg, IDC_EDIT_LUMPNAME, s_lpnmdd->szLumpName);

			/* Prepare and populate the list of open wads. */
			ListView_SetImageList(hwndWadList, GetWadListImageList(), LVSIL_SMALL);
			EnumOpenWads(AddListedWadToList, (void*)hwndWadList);

			/* If there are no open wads, disable the option to add the new map
			 * to an open wad.
			 */
			if(ListView_GetItemCount(hwndWadList) == 0)
				EnableWindow(GetDlgItem(hwndDlg, IDC_RADIO_ADDTOEXISTING), FALSE);

			/* Assume new wad. */
			CheckRadioButton(hwndDlg, IDC_RADIO_CREATENEW, IDC_RADIO_ADDTOEXISTING, IDC_RADIO_CREATENEW);

			/* Enable/disable controls as necessary. */
			UpdateNewMapControlStates(hwndDlg);
		}

		/* Let the system set the focus. */
		return TRUE;

	case WM_NOTIFY:
		if(((LPNMHDR)lParam)->idFrom == IDC_LIST_OPENWADS)
		{
			LPNMLISTVIEW lpnmlistview = (LPNMLISTVIEW)lParam;

			switch(lpnmlistview->hdr.code)
			{
			case LVN_ITEMCHANGED:
				if(lpnmlistview->uChanged & LVIF_STATE)
				{
					/* Was the item selected or deselected? */
					if((lpnmlistview->uNewState | lpnmlistview->uOldState) & LVIS_SELECTED)
					{
						UpdateNewMapControlStates(hwndDlg);
						return TRUE;
					}
				}
			}
		}

		/* Didn't process message. */
		break;

	case WM_COMMAND:
		switch((LOWORD(wParam)))
		{
		case IDOK:
			{
				char szLumpname[CCH_LUMPNAME + 1];

				/* Get the map config. */
				s_lpnmdd->lpcfgMap = (CONFIG*)SendDlgItemMessage(hwndDlg, IDC_COMBO_CONFIG, CB_GETITEMDATA, SendDlgItemMessage(hwndDlg, IDC_COMBO_CONFIG, CB_GETCURSEL, 0, 0), 0);

				/* Determine whether we're adding to an existing wad, and if so,
				 * get its index.
				 */
				if(IsDlgButtonChecked(hwndDlg, IDC_RADIO_ADDTOEXISTING))
				{
					HWND hwndWadList = GetDlgItem(hwndDlg, IDC_LIST_OPENWADS);
					LVITEM lvitem;

					/* Get the selected item's info. */
					lvitem.mask = LVIF_PARAM;
					lvitem.iItem = ListView_GetNextItem(hwndWadList, -1, LVIS_SELECTED);
					lvitem.iSubItem = 0;
					ListView_GetItem(hwndWadList, &lvitem);

					s_lpnmdd->iWad = lvitem.lParam;
				}
				else s_lpnmdd->iWad = -1;

				GetDlgItemANSIText(hwndDlg, IDC_EDIT_LUMPNAME, szLumpname, NUM_ELEMENTS(szLumpname));

				/* Validate the lumpname. */
				if(!IsMapLumpnameA(szLumpname))
					MessageBoxFromStringTable(hwndDlg, IDS_ERROR_MAPLUMPNAME, MB_ICONERROR);
				else if(s_lpnmdd->iWad >= 0 && GetLumpIndex(GetWad(s_lpnmdd->iWad), 0, -1, szLumpname) >= 0)
					MessageBoxFromStringTable(hwndDlg, IDS_ERROR_MAPEXISTS, MB_ICONERROR);
				else
				{
					/* Lumpname is valid. Copy it and end the dialogue. */
					strcpy(s_lpnmdd->szLumpName, szLumpname);
					EndDialog(hwndDlg, TRUE);
				}
			}

			return TRUE;

		case IDCANCEL:
			EndDialog(hwndDlg, FALSE);
			return TRUE;

		case IDC_RADIO_CREATENEW:
		case IDC_RADIO_ADDTOEXISTING:
			if(HIWORD(wParam) == BN_CLICKED)
			{
				UpdateNewMapControlStates(hwndDlg);
				return TRUE;
			}

			break;
		}

		break;
	}

	/* Didn't process message. */
	return FALSE;
}


/* AddListedWadToList
 *   If a wad is in the wad-list tree-view, add it to to a particular list-view.
 *
 * Parameters:
 *   int	iWad		Index of wad.
 *   void*	lpvList		(HWND) List-view window handle.
 *
 * Return value: BOOL
 *   Always TRUE.
 *
 * Remarks:
 *   Intended for use with EnumOpenWads.
 */
static BOOL AddListedWadToList(int iWad, void *lpvList)
{
	HWND hwndList = (HWND)lpvList;

	/* If the wad is in the wad-list, add it to *this* list. */
	if(WadIsInList(iWad))
	{
		LVITEM lvitem;

		lvitem.mask = LVIF_TEXT | LVIF_IMAGE | LVIF_PARAM;
		lvitem.iItem = lvitem.iSubItem = 0;
		lvitem.lParam = iWad;
		lvitem.iImage = WLI_WAD;
		lvitem.cchTextMax = GetWadTitle(iWad, NULL, 0);
		lvitem.pszText = (LPTSTR)ProcHeapAlloc(lvitem.cchTextMax * sizeof(TCHAR));
		GetWadTitle(iWad, lvitem.pszText, lvitem.cchTextMax);

		ListView_InsertItem(hwndList, &lvitem);

		ProcHeapFree(lvitem.pszText);
	}

	return TRUE;
}


/* UpdateNewMapControlStates
 *   Enables/disables controls on the New Map dialogue as necessary.
 *
 * Parameters:
 *   HWND	hwndDlg		Dialogue window handle.
 *
 * Return value: None.
 */
static void UpdateNewMapControlStates(HWND hwndDlg)
{
	HWND hwndWadList = GetDlgItem(hwndDlg, IDC_LIST_OPENWADS);

	/* Handle add-to-new vs add-to-existing. */
	if(IsDlgButtonChecked(hwndDlg, IDC_RADIO_ADDTOEXISTING))
	{
		EnableWindow(hwndWadList, TRUE);

		/* Enable OK button iff we have a selected wad. */
		EnableWindow(GetDlgItem(hwndDlg, IDOK), (ListView_GetNextItem(hwndWadList, -1, LVIS_SELECTED) != -1));
	}
	else
	{
		EnableWindow(hwndWadList, FALSE);
		EnableWindow(GetDlgItem(hwndDlg, IDOK), TRUE);
	}
}
