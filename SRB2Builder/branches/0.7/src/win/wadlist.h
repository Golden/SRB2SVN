/* SRB2 Workbench -- A map editor for Sonic Robo Blast 2.
 * Copyright (C) 2009 Gregor Dick
 * http://workbench.srb2.org/
 *
 * Incorporates portions of Doom Builder, (C) 2003 Pascal vd Heiden.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 * USA.
 *
 * wadlist.h: Header for wadlist.c.
 *
 * AMDG.
 */

#ifndef __SRB2B_WADLIST__
#define __SRB2B_WADLIST__

#include <windows.h>
#include <commctrl.h>

#include "../DockWnd/DockWnd.h"

enum ENUM_WADLISTICONS
{
	WLI_WAD,
	WLI_MAP,
	WLI_MAX
};


/* Function prototypes. */
BOOL CreateWadListWindow(void);
void DestroyWadListWindow(void);
void AddToWadList(int iWad);
void RemoveFromWadList(int iWad);
void UpdateWadTitleInList(int iWad);
int RenameMapInWadList(int iWad, LPCSTR szOldName, LPCSTR szNewName);
void AddMapToTree(int iWad, LPCSTR szLumpname);
BOOL WadIsInList(int iWad);
HIMAGELIST GetWadListImageList(void);
BOOL IsWadListVisible(void);
void ToggleWadListVisibility(void);
void GetWadListState(LPDOCKSAVESTATE lpdss);
void SetWadListState(LPDOCKSAVESTATE lpdss);
LPDOCKINFO GetWadListDI(void);

#endif
