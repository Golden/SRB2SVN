/* SRB2 Workbench -- A map editor for Sonic Robo Blast 2.
 * Copyright (C) 2009 Gregor Dick
 * http://workbench.srb2.org/
 *
 * Incorporates portions of Doom Builder, (C) 2003 Pascal vd Heiden.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 * USA.
 *
 * config.h: Header for config.c.
 *
 * AMDG.
 */

#ifndef __SRB2B_CONFIG__
#define __SRB2B_CONFIG__


/* Macros. */


/* Types. */

typedef enum _CONFIG_ENTRY_TYPE
{
	CET_SUBSECTION,
	CET_STRING,
	CET_INT,
	CET_FLOAT,
	CET_NULL
} CONFIG_ENTRY_TYPE;

/* Config-file-entry binary tree. */
typedef struct _CONFIG
{
	LPTSTR				szName;

	CONFIG_ENTRY_TYPE	entrytype;
	union
	{
		struct _CONFIG *lpcfgSubsection;
		LPTSTR			sz;
		int				i;
		float			f;
	};

	struct _CONFIG	*lpcfgLeft, *lpcfgRight;
} CONFIG;


/* Prototypes. */
CONFIG* ConfigLoad(LPCTSTR szFilename);
void ConfigDestroy(CONFIG *lpcfg);

#ifdef UNICODE
void ConfigSetStringA(CONFIG *lpcfgRoot, LPCWSTR szName, LPCSTR szValue);
#else
#define ConfigSetStringA ConfigSetString
#endif

void ConfigSetString(CONFIG *lpcfgRoot, LPCTSTR szName, LPCTSTR szValue);

void ConfigSetFloat(CONFIG *lpcfgRoot, LPCTSTR szName, float f);
void ConfigSetInteger(CONFIG *lpcfgRoot, LPCTSTR szName, int i);

#ifdef UNICODE
void ConfigSetAtomA(CONFIG *lpcfgRoot, LPCSTR szName);
#else
#define ConfigSetAtomA ConfigSetAtom
#endif

void ConfigSetAtom(CONFIG *lpcfgRoot, LPCTSTR szName);

void ConfigSetSubsection(CONFIG *lpcfgRoot, LPCTSTR szName, CONFIG *lpcfgSS);
CONFIG* ConfigAddSubsection(CONFIG *lpcfgRoot, LPCTSTR szName);
BOOL ConfigNodeExists(CONFIG *lpcfgRoot, LPCTSTR szName);
int ConfigGetInteger(CONFIG *lpcfgRoot, LPCTSTR szName);
float ConfigGetFloat(CONFIG *lpcfgRoot, LPCTSTR szName);
int ConfigGetStringLength(CONFIG *lpcfgRoot, LPCTSTR szName);

#ifdef UNICODE
BOOL ConfigGetStringA(CONFIG *lpcfgRoot, LPCWSTR szName, LPSTR szBuffer, unsigned int cchBuffer);
#else
#define ConfigGetStringA ConfigGetString
#endif

BOOL ConfigGetString(CONFIG *lpcfgRoot, LPCTSTR szName, LPTSTR szBuffer, unsigned int cchBuffer);

CONFIG* ConfigGetSubsection(CONFIG *lpcfgRoot, LPCTSTR szName);
CONFIG* ConfigCreate(void);
CONFIG* ConfigDuplicate(CONFIG *lpcfg);
BOOL ConfigIterate(CONFIG *lpcfgRoot, BOOL (*lpfnCallback)(CONFIG*, void*), void *lpvParam);
int ConfigWrite(CONFIG *lpcfg, LPCTSTR szFilename);
void ConfigDeleteNode(CONFIG *lpcfgRoot, LPCTSTR szName);


/* Inline functions. */

static __inline CONFIG* ConfigGetOrAddSubsection(CONFIG *lpcfgRoot, LPCTSTR szName)
{
	CONFIG *lpcfgSubsection = ConfigGetSubsection(lpcfgRoot, szName);
	if(lpcfgSubsection) return lpcfgSubsection;
	return ConfigAddSubsection(lpcfgRoot, szName);
}

static __inline BOOL ConfigIsEmpty(CONFIG *lpcfg) { return !(lpcfg->lpcfgLeft || lpcfg->lpcfgRight); }

#endif
