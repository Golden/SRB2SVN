# Work-around to allow Dev-C++ to pass compiler flags to windres

all-before: res/SRB2Builder.res

res/SRB2Builder.res: res/SRB2Builder.rc
	$(WINDRES) -i res/SRB2Builder.rc --input-format=rc -o res/SRB2Builder.res -O coff --include-dir ./res --preprocessor '$(CC) -E -xc-header -DRC_INVOKED $(CFLAGS)'

clean-custom:
	${RM} res/SRB2Builder.res
