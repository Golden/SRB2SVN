#include <windows.h>
#include <string.h>

#include "general.h"
#include "config.h"
#include "mapconfig.h"
#include "wadopts.h"
#include "texture.h"
#include "options.h"
#include "testing.h"

#define WADOPT_EXTENSION		TEXT(".sbs")
#define WADOPT_SIGNATURE		"SRB2 Builder Wad Options"
#define WADOPT_SIGNATURE_KEY	"type"


static int GetWadOptionsFilename(LPCTSTR szWadFilename, LPTSTR szBuffer, UINT cchBuffer);
static void SetMissingMapOptions(CONFIG *lpcfgMapOptions);


/* GetWadOptionsFilename
 *   Generates a wad options filename from a wad filename.
 *
 * Parameters:
 *   LPCTSTR	szWadFilename	Filename of wad.
 *   LPTSTR		szBuffer		Buffer in which to return options filename.
 *   UINT		cchBuffer		Size of szBuffer in characters, including room
 *								for terminator.
 *
 * Return value: int
 *   Number of characters written to the buffer, including terminator, or the
 *   number of such required to store the entire filename if szBuffer is NULL.
 */
static int GetWadOptionsFilename(LPCTSTR szWadFilename, LPTSTR szBuffer, UINT cchBuffer)
{
	int iWadFilenameLength = lstrlen(szWadFilename), iExtOffset;
	LPCTSTR szInName = &szWadFilename[iWadFilenameLength];
	UINT cchRequired;

	/* First, determine whether the file has an extension already. */
	while(szInName > szWadFilename && *szInName != TEXT('\\') && *szInName != TEXT('.') && *szInName != TEXT('/'))
		szInName--;

	/* Whither the extension? */
	if(*szInName == TEXT('.'))
		iExtOffset = iWadFilenameLength - lstrlen(szInName);
	else
		iExtOffset = iWadFilenameLength;

	cchRequired = iExtOffset + lstrlen(WADOPT_EXTENSION) + 1;

	/* If we didn't pass in a buffer, just return the number of characters
	 * required.
	 */
	if(!szBuffer)
		return cchRequired;

	/* Do we not even have enough room to start adding the extension? */
	if(cchBuffer - iExtOffset < 2)
		return iWadFilenameLength + 1;

	/* Copy as much as we can. */
	lstrcpyn(szBuffer, szWadFilename, cchBuffer);

	/* Set the extension. */
	lstrcpyn(&szBuffer[iExtOffset], WADOPT_EXTENSION, cchBuffer - iExtOffset);

	/* Return the number of characters copied. */
	return min(cchBuffer, cchRequired);
}


/* GetWadOptions
 *   Retrieves the wad options for a wadfile, or creates a new one if no options
 *   file exists.
 *
 * Parameters:
 *   LPCTSTR	szWadFilename	Filename of wad.
 *
 * Return value: CONFIG*
 *   Wad options config structure.
 */
CONFIG *GetWadOptions(LPCTSTR szWadFilename)
{
	UINT cchRequired = GetWadOptionsFilename(szWadFilename, NULL, 0);
	LPTSTR szOptionsFilename = ProcHeapAlloc(cchRequired * sizeof(TCHAR));
	CONFIG *lpcfgWadOptions;

	/* Generate the filename for the options. */
	GetWadOptionsFilename(szWadFilename, szOptionsFilename, cchRequired);

	/* Try to load it. */
	lpcfgWadOptions = ConfigLoad(szOptionsFilename);
	ProcHeapFree(szOptionsFilename);

	/* If we found it, make sure it's valid. */
	if(lpcfgWadOptions && ConfigNodeExists(lpcfgWadOptions, WADOPT_SIGNATURE_KEY))
	{
		UINT cchSignature = ConfigGetStringLength(lpcfgWadOptions, WADOPT_SIGNATURE_KEY) + 1;
		LPSTR szSignature = ProcHeapAlloc(cchSignature);
		BOOL bMatch;

		ConfigGetStringA(lpcfgWadOptions, WADOPT_SIGNATURE_KEY, szSignature, cchSignature);

		/* If the signature's okay, it passes the validation test. */
		bMatch = !lstrcmpA(szSignature, WADOPT_SIGNATURE);

		ProcHeapFree(szSignature);

		if(bMatch)
			return lpcfgWadOptions;
	}

	/* If the file didn't exist or it was invalid, make a new one. */
	return NewWadOptions();
}


/* NewWadOptions
 *   Creates a new wad options config structure.
 *
 * Parameters: None.
 *
 * Return value: CONFIG*
 *   New wad options config structure.
 */
CONFIG* NewWadOptions(void)
{
	CONFIG *lpcfgWadOpts = ConfigCreate();

	/* Set the signature. */
	ConfigSetStringA(lpcfgWadOpts, WADOPT_SIGNATURE_KEY, WADOPT_SIGNATURE);

	return lpcfgWadOpts;
}


/* StoreMapCfgForWad
 *   Stores the specified map configuration in the wad options.
 *
 * Parameters:
 *   CONFIG*	lpcfgWadOpt		Wad options.
 *   CONFIG*	lpcfgMap		Map configuration.
 *
 * Return value: None.
 */
void StoreMapCfgForWad(CONFIG *lpcfgWadOpt, CONFIG *lpcfgMap)
{
	UINT cchGameID = ConfigGetStringLength(lpcfgMap, MAPCFG_ID) + 1;
	LPSTR szGameID = ProcHeapAlloc(cchGameID);

	/* Read string from map config and store it in wad options. */
	ConfigGetStringA(lpcfgMap, MAPCFG_ID, szGameID, cchGameID);
	ConfigSetStringA(lpcfgWadOpt, WADOPT_MAPCONFIG, szGameID);

	ProcHeapFree(szGameID);
}


/* GetMapOptionsFromWadOpt
 *   Gets options for a particular map from wad options, creating them if
 *   necessary.
 *
 * Parameters:
 *   CONFIG*	lpcfgWadOpt		Wad options.
 *   LPCSTR		szLumpname		Map lumpname.
 *
 * Return value: CONFIG*
 *   Subsection root of map options.
 */
CONFIG* GetMapOptionsFromWadOpt(CONFIG *lpcfgWadOpt, LPCSTR szLumpname)
{
	CONFIG *lpcfgMapOpt;

	if(!ConfigNodeExists(lpcfgWadOpt, szLumpname))
	{
		/* Create a new config and add it to the parent. */
		lpcfgMapOpt = ConfigCreate();
		ConfigSetSubsection(lpcfgWadOpt, szLumpname, lpcfgMapOpt);
	}
	else lpcfgMapOpt = ConfigGetSubsection(lpcfgWadOpt, szLumpname);

	/* Fill in anything that's missing. */
	SetMissingMapOptions(lpcfgMapOpt);

	return lpcfgMapOpt;
}


/* WriteWadOptions
 *   Writes the wad options for a wadfile.
 *
 * Parameters:
 *   LPCTSTR	szWadFilename	Filename of wad.
 *   CONFIG*	lpcfgWad		Wad options to write.
 *
 * Return value: int
 *   Zero on success; nonzero on error.
 */
int WriteWadOptions(LPCTSTR szWadFilename, CONFIG *lpcfgWad)
{
	UINT cchRequired = GetWadOptionsFilename(szWadFilename, NULL, 0);
	LPTSTR szOptionsFilename = ProcHeapAlloc(cchRequired * sizeof(TCHAR));
	int iRet;

	/* Generate the filename for the options. */
	GetWadOptionsFilename(szWadFilename, szOptionsFilename, cchRequired);

	/* Try to write the config. */
	iRet = ConfigWrite(lpcfgWad, szOptionsFilename);
	ProcHeapFree(szOptionsFilename);

	/* Finished. */
	return iRet;
}


/* SetMissingMapOptions
 *   Fills in any missing items from a map options config.
 *
 * Parameters:
 *   CONFIG*	lpcfgMapOptions		Map options config.
 *
 * Return value: None.
 *
 * Remarks:
 *   If the entries are there but of the wrong type, they aren't corrected.
 */
static void SetMissingMapOptions(CONFIG *lpcfgMapOptions)
{
	CHAR szTexName[TEXNAME_BUFFER_LENGTH];
	CONFIG *lpcfgMapDefTex, *lpcfgMapDefSec;
	CONFIG *lpcfgDefDefTex, *lpcfgDefDefSec;

	/* Get the subsections for textures and sectors, or make them if they don't
	 * exist.
	 */
	if(!(lpcfgMapDefTex = ConfigGetSubsection(lpcfgMapOptions, OPT_DEFAULTTEX)))
		lpcfgMapDefTex = ConfigAddSubsection(lpcfgMapOptions, OPT_DEFAULTTEX);

	if(!(lpcfgMapDefSec = ConfigGetSubsection(lpcfgMapOptions, OPT_DEFAULTSEC)))
		lpcfgMapDefSec = ConfigAddSubsection(lpcfgMapOptions, OPT_DEFAULTSEC);


	/* Default defaults from main config. */

	/* Default to single-player. */
	if(!ConfigNodeExists(lpcfgMapOptions, "gametype"))
		ConfigSetInteger(lpcfgMapOptions, "gametype", GT_SINGLEPLAYER);

	/* Some sections. */
	lpcfgDefDefTex = ConfigGetSubsection(g_lpcfgMain, OPT_DEFAULTTEX);
	lpcfgDefDefSec = ConfigGetSubsection(g_lpcfgMain, OPT_DEFAULTSEC);


	/* Set the default textures. */
	if(!ConfigNodeExists(lpcfgMapDefTex, "upper"))
	{
		ConfigGetStringA(lpcfgDefDefTex, "upper", szTexName, TEXNAME_BUFFER_LENGTH);
		ConfigSetStringA(lpcfgMapDefTex, "upper", szTexName);
	}

	if(!ConfigNodeExists(lpcfgMapDefTex, "middle"))
	{
		ConfigGetStringA(lpcfgDefDefTex, "middle", szTexName, TEXNAME_BUFFER_LENGTH);
		ConfigSetStringA(lpcfgMapDefTex, "middle", szTexName);
	}

	if(!ConfigNodeExists(lpcfgMapDefTex, "lower"))
	{
		ConfigGetStringA(lpcfgDefDefTex, "lower", szTexName, TEXNAME_BUFFER_LENGTH);
		ConfigSetStringA(lpcfgMapDefTex, "lower", szTexName);
	}


	/* Set the default sector options. */
	if(!ConfigNodeExists(lpcfgMapDefSec, "tceiling"))
	{
		ConfigGetStringA(lpcfgDefDefSec, "tceiling", szTexName, TEXNAME_BUFFER_LENGTH);
		ConfigSetStringA(lpcfgMapDefSec, "tceiling", szTexName);
	}

	if(!ConfigNodeExists(lpcfgMapDefSec, "tfloor"))
	{
		ConfigGetStringA(lpcfgDefDefSec, "tfloor", szTexName, TEXNAME_BUFFER_LENGTH);
		ConfigSetStringA(lpcfgMapDefSec, "tfloor", szTexName);
	}

	if(!ConfigNodeExists(lpcfgMapDefSec, "hceiling"))
		ConfigSetInteger(lpcfgMapDefSec, "hceiling", ConfigGetInteger(lpcfgDefDefSec, "hceiling"));

	if(!ConfigNodeExists(lpcfgMapDefSec, "hfloor"))
		ConfigSetInteger(lpcfgMapDefSec, "hfloor", ConfigGetInteger(lpcfgDefDefSec, "hfloor"));

	if(!ConfigNodeExists(lpcfgMapDefSec, "brightness"))
		ConfigSetInteger(lpcfgMapDefSec, "brightness", ConfigGetInteger(lpcfgDefDefSec, "brightness"));
}
