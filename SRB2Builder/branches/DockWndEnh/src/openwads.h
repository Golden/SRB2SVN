#ifndef __SRB2B_OPENWADS__
#define __SRB2B_OPENWADS__

#include "wad.h"

/* Types. */
typedef struct _OPENWAD OPENWAD;

struct _OPENWAD
{
	WAD				*lpwad;
	unsigned int	uiReferenceCount;
	int				iID;
	LPTSTR			szFilename;
	OPENWAD	*lpowNext;
};


/* Macros. */
#define SaveWad(iWad) (SaveWadAs(iWad, NULL))


/* Function prototypes. */
int LoadWad(LPCTSTR szPath);
void EnumChildWindowsByWad(int iID, WNDENUMPROC lpEnumFunc, LPARAM lParam);
int ReleaseWad(int iID);
void InitOpenWadsList(void);
int NewWad(void);
WAD* GetWad(int iWad);
void IncrementWadReferenceCount(int iWad);
int GetWadFilename(int iWad, LPTSTR szFilename, WORD cchBuffer);
int SaveWadAs(int iWad, LPTSTR szFileName);
BOOL WadIsNewByID(int iWad);

#endif	/* __SRB2B_OPENWADS__ */
