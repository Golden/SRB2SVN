#ifndef __SRB2B_MAP__
#define __SRB2B_MAP__

#include "maptypes.h"
#include "config.h"
#include "wad.h"

/* Function prototypes. */
MAP *AllocateMapStructure(void);
void DestroyMapStructure(MAP *lpmap);
int MapLoad(MAP *lpmap, int iWad, LPCSTR szLumpname);
int MapLoadForPreview(MAP *lpmap, int iWad, LPCSTR szLumpname);
void UpdateMap(MAP *lpmap, int iWad, LPCSTR szLumpname, CONFIG *lpcfgMap);
void UpdateMapToWadStructure(MAP *lpmap, WAD *lpwad, LPCSTR szLumpname, CONFIG *lpcfgMap);
void IterateMapLumpNames(int iWad, void (*fnCallback)(LPCTSTR, void*), void *lpvParam);


/* Macros. */
#define SECEFFECT_NYBBLE0(x) ((x) & 0xF)
#define SECEFFECT_NYBBLE1(x) (((x) & 0xF0) >> 4)
#define SECEFFECT_NYBBLE2(x) (((x) & 0xF00) >> 8)
#define SECEFFECT_NYBBLE3(x) (((x) & 0xF000) >> 12)

#define NYBBLE0_TO_SECEFFECTMASK(x) ((x) & 0xF)
#define NYBBLE1_TO_SECEFFECTMASK(x) (((x) & 0xF) << 4)
#define NYBBLE2_TO_SECEFFECTMASK(x) (((x) & 0xF) << 8)
#define NYBBLE3_TO_SECEFFECTMASK(x) (((x) & 0xF) << 12)


/* Inline functions. */
static __inline BOOL LinedefBelongsToSector(MAP *lpmap, int iLinedefIndex, int iSectorIndex)
{
	int s1 = lpmap->linedefs[iLinedefIndex].s1, s2 = lpmap->linedefs[iLinedefIndex].s2;
	if(s1 >= 0 && lpmap->sidedefs[s1].sector == iSectorIndex) return TRUE;
	if(s2 >= 0 && lpmap->sidedefs[s2].sector == iSectorIndex) return TRUE;
	return FALSE;
}


#endif
