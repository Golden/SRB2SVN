#include <windows.h>

#include "keyboard.h"
#include "options.h"
#include "../res/resource.h"


#define NOCOMMAND 0xFFFF

/* Shift masks used for shortcut keys. */
#define SC_SHIFT	0x10000
#define SC_CTRL		0x20000
#define SC_ALT		0x40000
#define SC_KEY		0xFFFF

/* MinGW is missing these two. */
#ifndef VK_OEM_COMMA
#define VK_OEM_COMMA 0xBC
#endif

#ifndef VK_OEM_PERIOD
#define VK_OEM_PERIOD 0xBC
#endif


/* Globals. */
HACCEL g_hAccel = NULL;

/* Mapping table of shortcut keys to menu commands. */
/* NOCOMMAND indicates that no menu command is associated with the key. */
static USHORT g_unShortcutsToMenu[SCK_MAX] =
{
	NOCOMMAND,					/* SCK_EDITQUICKMOVE */
	NOCOMMAND,					/* SCK_ZOOMIN */
	NOCOMMAND,					/* SCK_ZOOMOUT */
	IDM_VIEW_CENTRE,			/* SCK_CENTREVIEW */
	IDM_VIEW_MODE_MOVE,			/* SCK_EDIT_MOVE */
	IDM_VIEW_MODE_ANY,			/* SCK_EDIT_ANY */
	IDM_VIEW_MODE_LINES,		/* SCK_EDITLINES */
	IDM_VIEW_MODE_SECTORS,		/* SCK_EDITSECTORS */
	IDM_VIEW_MODE_VERTICES,		/* SCK_EDITVERTICES */
	IDM_VIEW_MODE_THINGS,		/* SCK_EDITTHINGS */
	IDM_VIEW_3D,				/* SCK_EDIT3D */
	IDM_EDIT_SNAPTOGRID,		/* SCK_EDITSNAPTOGRID */
	IDM_LINES_FLIPLINEDEFS,		/* SCK_FLIPLINEDEFS */
	IDM_LINES_FLIPSIDEDEFS,		/* SCK_FLIPSIDEDEFS */
	IDM_LINES_SPLITLINEDEFS,	/* SCK_SPLITLINEDEFS */
	IDM_SECTORS_JOIN,			/* SCK_JOINSECTORS */
	IDM_SECTORS_MERGE,			/* SCK_MERGESECTORS */
	IDM_EDIT_UNDO,				/* SCK_UNDO */
	IDM_EDIT_REDO,				/* SCK_REDO */
	IDM_EDIT_TRANSFORMSELECTION_FLIPHORIZONTALLY,	/* SCK_FLIPHORIZ */
	IDM_EDIT_TRANSFORMSELECTION_FLIPVERTICALLY,		/* SCK_FLIPVERT */
	IDM_EDIT_COPY,				/* SCK_COPY */
	IDM_EDIT_PASTE,				/* SCK_PASTE */
	IDM_FILE_SAVEAS,			/* SCK_SAVEAS */
	IDM_SECTORS_FLOORS_INCREASE,	/* SCK_INCFLOOR */
	IDM_SECTORS_FLOORS_DECREASE,	/* SCK_DECFLOOR */
	IDM_SECTORS_CEILINGS_INCREASE,	/* SCK_INCCEIL */
	IDM_SECTORS_CEILINGS_DECREASE,	/* SCK_DECCEIL */
	IDM_SECTORS_LIGHT_INCREASE,	/* SCK_INCLIGHT */
	IDM_SECTORS_LIGHT_DECREASE,	/* SCK_DECLIGHT */
	IDM_THINGS_HEIGHT_INCREASE,	/* SCK_INCTHINGZ */
	IDM_THINGS_HEIGHT_DECREASE,	/* SCK_DECTHINGZ */
	IDM_EDIT_TRANSFORM_SNAP,	/* SCK_SNAPSELECTION */
	IDM_SECTORS_FLOORS_GRADIENT,	/* SCK_GRADIENTFLOORS */
	IDM_SECTORS_CEILINGS_GRADIENT,	/* SCK_GRADIENTCEILINGS */
	IDM_SECTORS_LIGHT_GRADIENT,	/* SCK_GRADIENTBRIGHTNESS */
	NOCOMMAND,					/* SCK_DRAWSECTOR */
	IDM_EDIT_CUT,				/* SCK_EDITCUT */
	IDM_EDIT_DELETE,			/* SCK_EDITDELETE */
	NOCOMMAND,					/* SCK_CANCEL */
	IDM_THINGS_HEIGHT_GRADIENT,	/* SCK_GRADIENTTHINGZ */
	IDM_EDIT_TRANSFORMSELECTION_ROTATE,	/* SCK_ROTATE */
	IDM_EDIT_TRANSFORMSELECTION_RESIZE,	/* SCK_RESIZE */
	IDM_THINGS_ROTATE_ACW,		/* SCK_THINGROTACW */
	IDM_THINGS_ROTATE_CW,		/* SCK_THINGROTCW */
	IDM_EDIT_SELECT_ALL,		/* SCK_SELECTALL */
	IDM_EDIT_SELECT_NONE,		/* SCK_SELECTNONE */
	IDM_EDIT_SELECT_INVERTSELECTION,	/* SCK_INVERTSELECTION */
	IDM_EDIT_COPYSELECTIONPROPERTIES,	/* SCK_COPYPROPS */
	IDM_EDIT_PASTESELECTIONPROPERTIES,	/* SCK_PASTEPROPS */
	IDM_VIEW_GRID_INCREASE,		/* SCK_GRIDINC */
	IDM_VIEW_GRID_DECREASE,		/* SCK_GRIDDEC */
	IDM_EDIT_FIND,				/* SCK_FIND */
	IDM_EDIT_REPLACE,			/* SCK_REPLACE */
	IDM_FILE_NEW,				/* SCK_NEW */
	IDM_FILE_OPEN,				/* SCK_OPEN */
	IDM_FILE_SAVE,				/* SCK_SAVE */
	IDM_EDIT_MAPOPTIONS,		/* SCK_MAPOPTIONS */
	IDM_THINGS_ROTATE_MOUSE,	/* SCK_MOUSEROTATE */
	IDM_VERTICES_STITCH,		/* SCK_STITCHVERTICES */
	NOCOMMAND,					/* SCK_SCROLL_UP */
	NOCOMMAND,					/* SCK_SCROLL_DOWN */
	NOCOMMAND,					/* SCK_SCROLL_LEFT */
	NOCOMMAND,					/* SCK_SCROLL_RIGHT */
	IDM_FILE_TEST,				/* SCK_TEST */
	ID_QUICKTEST,				/* SCK_QUICKTEST */
	IDM_SECTORS_IDENT,			/* SCK_IDENTSECTORS */
	/* _SCK_ */
};


/* MakeShiftedKeyCode
 *   Maps a VK code to a character code with shift states.
 *
 * Parameters:
 *   int		iVirtKey	Virtual key code.
 *
 * Return value: int
 *   DB-esque key-code containing character code and shift states.
 */
int MakeShiftedKeyCode(int iVirtKey)
{
	/* TODO: Does VB actually use VK codes, or chars? If the latter, be
	 * careful: you're processing mousewheel events too!
	 */
	return iVirtKey
		+ ((GetKeyState(VK_SHIFT) & 0x8000) ? 0x10000 : 0)
		+ ((GetKeyState(VK_CONTROL) & 0x8000) ? 0x20000 : 0)
		+ ((GetKeyState(VK_MENU) & 0x8000) ? 0x40000 : 0);
}


/* UpdateAcceleratorFromOptions
 *   Copies shortcut keys into accelerator table.
 *
 * Parameters:
 *   None.
 *
 * Return value: None.
 *
 * Notes:
 *   Call this after first loading the config file, and then every time the user
 *   changes the shortcut keys.
 */
void UpdateAcceleratorFromOptions(void)
{
	ACCEL accel[SCK_MAX];
	int i, iAccelCount;

	/* We recreate the table here, so destroy it if it exists already. */
	if(g_hAccel) DestroyCustomAccelerator();

	/* First entry is dummy until something overwrites it. */
	accel[0].cmd = (unsigned short)-1;
	accel[0].fVirt = 0;
	accel[0].key = 0;

	/* Loop through all shortcut keys, copying into accel struct. */
	iAccelCount = 0;
	for(i=0; i<SCK_MAX; i++)
	{
		if(g_unShortcutsToMenu[i] != NOCOMMAND && g_iShortcutCodes[i] != 0)
		{
			accel[iAccelCount].cmd = g_unShortcutsToMenu[i];

			/* Build shift-mask. */
			accel[iAccelCount].fVirt = 0;
			if(g_iShortcutCodes[i] & SC_SHIFT) accel[iAccelCount].fVirt |= FSHIFT;
			if(g_iShortcutCodes[i] & SC_CTRL) accel[iAccelCount].fVirt |= FCONTROL;
			if(g_iShortcutCodes[i] & SC_ALT) accel[iAccelCount].fVirt |= FALT;

			/* TODO: Work out whether this is okay: */
			accel[iAccelCount].fVirt |= FVIRTKEY;

			accel[iAccelCount].key = g_iShortcutCodes[i] & SC_KEY;

			iAccelCount++;
		}
	}

	g_hAccel = CreateAcceleratorTable(accel, max(iAccelCount, 1));
}


/* SetDefaultShortcuts
 *   Sets missing shortcuts to their default values.
 *
 * Parameters:
 *   None.
 *
 * Return value: None.
 */
void SetDefaultShortcuts(void)
{
	CONFIG *lpcfgShortcuts = ConfigGetSubsection(g_lpcfgMain, OPT_SHORTCUTS);

	if(!ConfigNodeExists(lpcfgShortcuts, "editquickmove"))
		ConfigSetInteger(lpcfgShortcuts, "editquickmove",		VK_SPACE);

	if(!ConfigNodeExists(lpcfgShortcuts, "zoomin"))
		ConfigSetInteger(lpcfgShortcuts, "zoomin",				MOUSE_SCROLL_UP);

	if(!ConfigNodeExists(lpcfgShortcuts, "zoomout"))
		ConfigSetInteger(lpcfgShortcuts, "zoomout",				MOUSE_SCROLL_DOWN);

	if(!ConfigNodeExists(lpcfgShortcuts, "editmove"))
		ConfigSetInteger(lpcfgShortcuts, "editmove",			'M');

	if(!ConfigNodeExists(lpcfgShortcuts, "editlines"))
		ConfigSetInteger(lpcfgShortcuts, "editlines",			'L');

	if(!ConfigNodeExists(lpcfgShortcuts, "editsectors"))
		ConfigSetInteger(lpcfgShortcuts, "editsectors",			'S');

	if(!ConfigNodeExists(lpcfgShortcuts, "editvertices"))
		ConfigSetInteger(lpcfgShortcuts, "editvertices",		'V');

	if(!ConfigNodeExists(lpcfgShortcuts, "editthings"))
		ConfigSetInteger(lpcfgShortcuts, "editthings",			'T');

	if(!ConfigNodeExists(lpcfgShortcuts, "edit3d"))
		ConfigSetInteger(lpcfgShortcuts, "edit3d",				'W');

	if(!ConfigNodeExists(lpcfgShortcuts, "togglesnap"))
		ConfigSetInteger(lpcfgShortcuts, "togglesnap",			VK_OEM_2);		/* / */

	if(!ConfigNodeExists(lpcfgShortcuts, "fliplinedefs"))
		ConfigSetInteger(lpcfgShortcuts, "fliplinedefs",		'F');

	if(!ConfigNodeExists(lpcfgShortcuts, "splitlinedefs"))
		ConfigSetInteger(lpcfgShortcuts, "splitlinedefs",		'X');

	if(!ConfigNodeExists(lpcfgShortcuts, "joinsector"))
		ConfigSetInteger(lpcfgShortcuts, "joinsector",			'J');

	if(!ConfigNodeExists(lpcfgShortcuts, "mergesector"))
		ConfigSetInteger(lpcfgShortcuts, "mergesector",			'J' | SC_CTRL);

	if(!ConfigNodeExists(lpcfgShortcuts, "editundo"))
		ConfigSetInteger(lpcfgShortcuts, "editundo",			'Z' | SC_CTRL);

	if(!ConfigNodeExists(lpcfgShortcuts, "editredo"))
		ConfigSetInteger(lpcfgShortcuts, "editredo",			'Y' | SC_CTRL);

	if(!ConfigNodeExists(lpcfgShortcuts, "editfliph"))
		ConfigSetInteger(lpcfgShortcuts, "editfliph",			VK_OEM_COMMA);

	if(!ConfigNodeExists(lpcfgShortcuts, "editflipv"))
		ConfigSetInteger(lpcfgShortcuts, "editflipv",			VK_OEM_PERIOD);

	if(!ConfigNodeExists(lpcfgShortcuts, "editcopy"))
		ConfigSetInteger(lpcfgShortcuts, "editcopy",			'C' | SC_CTRL);

	if(!ConfigNodeExists(lpcfgShortcuts, "editpaste"))
		ConfigSetInteger(lpcfgShortcuts, "editpaste",			'V' | SC_CTRL);

	if(!ConfigNodeExists(lpcfgShortcuts, "filesaveas"))
		ConfigSetInteger(lpcfgShortcuts, "filesaveas",			VK_F12);

	if(!ConfigNodeExists(lpcfgShortcuts, "incfloor"))
		ConfigSetInteger(lpcfgShortcuts, "incfloor",			VK_HOME);

	if(!ConfigNodeExists(lpcfgShortcuts, "decfloor"))
		ConfigSetInteger(lpcfgShortcuts, "decfloor",			VK_END);

	if(!ConfigNodeExists(lpcfgShortcuts, "incceil"))
		ConfigSetInteger(lpcfgShortcuts, "incceil",				VK_NEXT);

	if(!ConfigNodeExists(lpcfgShortcuts, "decceil"))
		ConfigSetInteger(lpcfgShortcuts, "decceil",				VK_PRIOR);

	if(!ConfigNodeExists(lpcfgShortcuts, "inclight"))
		ConfigSetInteger(lpcfgShortcuts, "inclight",			VK_NEXT | SC_CTRL);

	if(!ConfigNodeExists(lpcfgShortcuts, "declight"))
		ConfigSetInteger(lpcfgShortcuts, "declight",			VK_PRIOR | SC_CTRL);

	if(!ConfigNodeExists(lpcfgShortcuts, "snapselection"))
		ConfigSetInteger(lpcfgShortcuts, "snapselection",		VK_RETURN | SC_CTRL);

	if(!ConfigNodeExists(lpcfgShortcuts, "drawsector"))
		ConfigSetInteger(lpcfgShortcuts, "drawsector",			VK_INSERT);

	if(!ConfigNodeExists(lpcfgShortcuts, "editcut"))
		ConfigSetInteger(lpcfgShortcuts, "editcut",				'X' | SC_CTRL);

	if(!ConfigNodeExists(lpcfgShortcuts, "editdelete"))
		ConfigSetInteger(lpcfgShortcuts, "editdelete",			VK_DELETE);

	if(!ConfigNodeExists(lpcfgShortcuts, "cancel"))
		ConfigSetInteger(lpcfgShortcuts, "cancel",				VK_ESCAPE);

	if(!ConfigNodeExists(lpcfgShortcuts, "editrotate"))
		ConfigSetInteger(lpcfgShortcuts, "editrotate",			'R');

	if(!ConfigNodeExists(lpcfgShortcuts, "editresize"))
		ConfigSetInteger(lpcfgShortcuts, "editresize",			'R' | SC_SHIFT);

	if(!ConfigNodeExists(lpcfgShortcuts, "thingrotateacw"))
		ConfigSetInteger(lpcfgShortcuts, "thingrotateacw",		VK_OEM_COMMA | SC_CTRL);

	if(!ConfigNodeExists(lpcfgShortcuts, "thingrotatecw"))
		ConfigSetInteger(lpcfgShortcuts, "thingrotatecw",		VK_OEM_PERIOD | SC_CTRL);

	if(!ConfigNodeExists(lpcfgShortcuts, "editselectall"))
		ConfigSetInteger(lpcfgShortcuts, "editselectall",		'A' | SC_CTRL);

	if(!ConfigNodeExists(lpcfgShortcuts, "editselectnone"))
		ConfigSetInteger(lpcfgShortcuts, "editselectnone",		'D' | SC_CTRL);

	if(!ConfigNodeExists(lpcfgShortcuts, "editselectinvert"))
		ConfigSetInteger(lpcfgShortcuts, "editselectinvert",	'I' | SC_CTRL);

	if(!ConfigNodeExists(lpcfgShortcuts, "copyprops"))
		ConfigSetInteger(lpcfgShortcuts, "copyprops",			'C' | SC_CTRL | SC_SHIFT);

	if(!ConfigNodeExists(lpcfgShortcuts, "pasteprops"))
		ConfigSetInteger(lpcfgShortcuts, "pasteprops",			'V' | SC_CTRL | SC_SHIFT);

	if(!ConfigNodeExists(lpcfgShortcuts, "gridinc"))
		ConfigSetInteger(lpcfgShortcuts, "gridinc",				VK_OEM_6);	/* ] */

	if(!ConfigNodeExists(lpcfgShortcuts, "griddec"))
		ConfigSetInteger(lpcfgShortcuts, "griddec",				VK_OEM_4);	/* [ */

	if(!ConfigNodeExists(lpcfgShortcuts, "editfind"))
		ConfigSetInteger(lpcfgShortcuts, "editfind",			'F' | SC_CTRL);

	if(!ConfigNodeExists(lpcfgShortcuts, "editreplace"))
		ConfigSetInteger(lpcfgShortcuts, "editreplace",			'H' | SC_CTRL);

	if(!ConfigNodeExists(lpcfgShortcuts, "filenew"))
		ConfigSetInteger(lpcfgShortcuts, "filenew",				'N' | SC_CTRL);

	if(!ConfigNodeExists(lpcfgShortcuts, "fileopen"))
		ConfigSetInteger(lpcfgShortcuts, "fileopen",			'O' | SC_CTRL);

	if(!ConfigNodeExists(lpcfgShortcuts, "filesave"))
		ConfigSetInteger(lpcfgShortcuts, "filesave",			'S' | SC_CTRL);

	if(!ConfigNodeExists(lpcfgShortcuts, "editoptions"))
		ConfigSetInteger(lpcfgShortcuts, "editoptions",			VK_F2);

	if(!ConfigNodeExists(lpcfgShortcuts, "thingsmouserotate"))
		ConfigSetInteger(lpcfgShortcuts, "thingsmouserotate",	'R' | SC_CTRL | SC_SHIFT);

	if(!ConfigNodeExists(lpcfgShortcuts, "scrollup"))
		ConfigSetInteger(lpcfgShortcuts, "scrollup",			VK_UP);

	if(!ConfigNodeExists(lpcfgShortcuts, "scrolldown"))
		ConfigSetInteger(lpcfgShortcuts, "scrolldown",			VK_DOWN);

	if(!ConfigNodeExists(lpcfgShortcuts, "scrollleft"))
		ConfigSetInteger(lpcfgShortcuts, "scrollleft",			VK_LEFT);

	if(!ConfigNodeExists(lpcfgShortcuts, "scrollright"))
		ConfigSetInteger(lpcfgShortcuts, "scrollright",			VK_RIGHT);

	if(!ConfigNodeExists(lpcfgShortcuts, "filetest"))
		ConfigSetInteger(lpcfgShortcuts, "filetest",			VK_F8);

	if(!ConfigNodeExists(lpcfgShortcuts, "quicktest"))
		ConfigSetInteger(lpcfgShortcuts, "quicktest",			VK_F8 | SC_SHIFT);

	/* _SCK_ */
}
