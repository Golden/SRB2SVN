#include <windows.h>
#include <commdlg.h>

#include "cdlgwrapper.h"
#include "general.h"


/* CommDlgOpen
 *   Displays the Open common dialogue box.
 *
 * Parameters:
 *   HWND		hwnd				Parent window.
 *   LPSTR		szFileNameReturn	Filename is returned here.
 *   UINT		cbFileNameReturn	Length of filename buffer.
 *   LPCTSTR	szTitle				Title of dialogue.
 *   LPCTSTR	szFilter			Type filter string.
 *   LPCTSTR	szDefExt			Default extension, without the dot.
 *   LPCTSTR	szInitFileName		Initial filename.
 *   int		iFlags				Additional flags for the comdlg32 library
 *									call. See its documentation.
 *
 * Return value: int
 *   Nonzero if the user selects a file and OKs; zero if cancelled or on error.
 *
 * Remarks:
 *   Even if the user cancels, szFileNameReturn may be clobbered.
 */
int CommDlgOpen(HWND hwnd, LPTSTR szFileNameReturn, UINT cbFileNameReturn, LPCTSTR szTitle, LPCTSTR szFilter, LPCTSTR szDefExt, LPCTSTR szInitFilename, int iFlags)
{
	OPENFILENAME ofn;

	lstrcpyn(szFileNameReturn, szInitFilename ? szInitFilename : TEXT(""), cbFileNameReturn);

	ofn.Flags = iFlags;
	ofn.hwndOwner = hwnd;
	ofn.lpstrFilter = szFilter;
	ofn.nFilterIndex = 1;
	ofn.lpstrDefExt = szDefExt;
	ofn.lpstrFile = szFileNameReturn;
	ofn.nMaxFile = cbFileNameReturn;
	ofn.lpfnHook = NULL;
	ofn.lpstrCustomFilter = NULL;
	ofn.lpstrFileTitle = NULL;
	ofn.lpstrInitialDir = NULL;
	ofn.lpTemplateName = NULL;
	ofn.lpstrTitle = szTitle;
	ofn.lStructSize = sizeof(ofn);

	return GetOpenFileName(&ofn);
}


/* CommDlgSave
 *   Displays the Save As common dialogue box.
 *
 * Parameters:
 *   HWND		hwnd				Parent window.
 *   LPSTR		szFileNameReturn	Filename is returned here.
 *   UINT		cbFileNameReturn	Length of filename buffer.
 *   LPCTSTR	szTitle				Title of dialogue.
 *   LPCTSTR	szFilter			Type filter string.
 *   LPCTSTR	szDefExt			Default extension, without the dot.
 *   LPCTSTR	szInitFileName		Initial filename.
 *   int		iFlags				Additional flags for the comdlg32 library
 *									call. See its documentation.
 *
 * Return value: int
 *   Nonzero if the user selects a file and OKs; zero if cancelled or on error.
 */
int CommDlgSave(HWND hwnd, LPTSTR szFileNameReturn, UINT cbFileNameReturn, LPCTSTR szTitle, LPCTSTR szFilter, LPCTSTR szDefExt, LPCTSTR szInitFilename, int iFlags)
{
	OPENFILENAME ofn;

	lstrcpyn(szFileNameReturn, szInitFilename ? szInitFilename : TEXT(""), cbFileNameReturn);

	ofn.Flags = iFlags;
	ofn.hwndOwner = hwnd;
	ofn.lpstrFilter = szFilter;
	ofn.nFilterIndex = 1;
	ofn.lpstrDefExt = szDefExt;
	ofn.lpstrFile = szFileNameReturn;
	ofn.nMaxFile = cbFileNameReturn;
	ofn.lpfnHook = NULL;
	ofn.lpstrCustomFilter = NULL;
	ofn.lpstrFileTitle = NULL;
	ofn.lpstrInitialDir = NULL;
	ofn.lpTemplateName = NULL;
	ofn.lpstrTitle = szTitle;
	ofn.lStructSize = sizeof(ofn);

	return GetSaveFileName(&ofn);
}
