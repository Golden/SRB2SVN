#include <windows.h>
#include <commctrl.h>
#include <tchar.h>
#include <stdio.h>

#include "../general.h"
#include "../../res/resource.h"

#include "../../DockWnd/DockWnd.h"

#include "toolbar.h"
#include "mdiframe.h"


static HWND g_hwndToolbar;


/* CreateToolbarWindow
 *   Creates the toolbar tool window with its docking container.
 *
 * Parameters:
 *   HWND	hwndParent	Parent window (i.e. MDI frame window).
 *
 * Return value: BOOL
 *   TRUE on success; FALSE on error.
 *
 * Remarks:
 *   We have to pass the frame window handle since this is called before the
 *   global var is set.
 */
BOOL CreateToolbarWindow(HWND hwndParent)
{
	DOCKINFO *lpdi;
	TCHAR	szCaption[64];
	HWND	hwndTB;

	TBBUTTON tbb[] =
	{
		{STD_FILENEW, IDM_FILE_NEW, TBSTATE_ENABLED, TBSTYLE_BUTTON, {0}, 0, 0},
		{STD_FILEOPEN, IDM_FILE_OPEN, TBSTATE_ENABLED, TBSTYLE_BUTTON, {0}, 0, 0},
		{STD_FILESAVE, IDM_FILE_SAVE, TBSTATE_ENABLED, TBSTYLE_BUTTON, {0}, 0, 0},

		{0, 0, 0, TBSTYLE_SEP, {0}, 0, 0},

		{STD_CUT, IDM_EDIT_CUT, TBSTATE_ENABLED, TBSTYLE_BUTTON, {0}, 0, 0},
		{STD_COPY, IDM_EDIT_COPY, TBSTATE_ENABLED, TBSTYLE_BUTTON, {0}, 0, 0},
		{STD_PASTE, IDM_EDIT_PASTE, TBSTATE_ENABLED, TBSTYLE_BUTTON, {0}, 0, 0},

		{0, 0, 0, TBSTYLE_SEP, {0}, 0, 0},

		{STD_UNDO, IDM_EDIT_UNDO, TBSTATE_ENABLED, TBSTYLE_DROPDOWN, {0}, 0, 0},
		{STD_REDOW, IDM_EDIT_REDO, TBSTATE_ENABLED, TBSTYLE_DROPDOWN, {0}, 0, 0},

		{0, 0, 0, TBSTYLE_SEP, {0}, 0, 0},

		{STD_FIND, IDM_EDIT_FIND, TBSTATE_ENABLED, TBSTYLE_BUTTON, {0}, 0, 0},
		{STD_REPLACE, IDM_EDIT_REPLACE, TBSTATE_ENABLED, TBSTYLE_BUTTON, {0}, 0, 0}
	};

	/* The toolbar is initially attached to the parent. */
	hwndTB = CreateToolbarEx(hwndParent,
							WS_CHILD | TBSTYLE_FLAT | TBSTYLE_TRANSPARENT | TBSTYLE_WRAPABLE | TBSTYLE_TOOLTIPS | WS_CLIPSIBLINGS | WS_CLIPCHILDREN | CCS_NOPARENTALIGN | CCS_NORESIZE,
							IDC_TOOLBAR,
							0,
							HINST_COMMCTRL,
							IDB_STD_SMALL_COLOR,
							tbb,
							sizeof(tbb) / sizeof(TBBUTTON),
							0, 0, 0, 0,
							sizeof(TBBUTTON));

	/* Show arrows for drop-down buttons. */
	SendMessage(hwndTB, TB_SETEXTENDEDSTYLE, 0, TBSTYLE_EX_DRAWDDARROWS);

	/* Allocate a docking structure. This'll be freed automatically by the lib.
	 */
	lpdi = DockingAlloc((char)DWS_DOCKED_TOP);

	lpdi->cyFloating = lpdi->nDockedSize = HIWORD(SendMessage(hwndTB, TB_GETBUTTONSIZE, 0, 0)) + HIWORD(SendMessage(hwndTB, TB_GETPADDING, 0, 0));

	/* Destroy the toolbar when the docking container is destroyed. Also, hide
	 * the docking container if the user clicks the Close button, rather than
	 * destroying the window.
	 */
	lpdi->dwStyle |= DWS_NODESTROY | DWS_DESTROYFOCUSWIN | DWS_NORESIZE;

	LoadString(g_hInstance, IDS_TOOLBAR_CAPTION, szCaption, sizeof(szCaption) / sizeof(TCHAR));
	DockingCreateFrame(lpdi, hwndParent, szCaption);
	g_hwndToolbar = lpdi->hwnd;

	/* Move the toolbar from the parent to the docking container. The MDI frame
	 * will continue to get notification messages, though.
	 */
	lpdi->focusWindow = hwndTB;
	SetParent(hwndTB, lpdi->hwnd);
	ShowWindow(hwndTB, SW_SHOW);

	DockingShowFrame(lpdi);

	return TRUE;
}


/* DestroyToolbarWindow
 *   Destroys the toolbar window.
 *
 * Parameters:
 *   None.
 *
 * Return value: None.
 *
 * Remarks:
 *   This destroys the docking container, which in turn destroys the toolbar
 *    control.
 */
void DestroyToolbarWindow(void)
{
	if(IsWindow(g_hwndToolbar)) DestroyWindow(g_hwndToolbar);
}
