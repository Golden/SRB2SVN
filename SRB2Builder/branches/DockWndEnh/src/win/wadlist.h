#ifndef __SRB2B_WADLIST__
#define __SRB2B_WADLIST__

/* Function prototypes. */
BOOL CreateWadListWindow(HWND hwndParent);
void DestroyWadListWindow(void);
void AddToWadList(int iWad);
void RemoveFromWadList(int iWad);

#endif
