#ifndef __SRB2B_FINDWIN__
#define __SRB2B_FINDWIN__


/* Prototypes. */
void ShowFindDlg(HWND hwndMap, BOOL bUseNybbledSectorEffects);
BOOL ShowReplaceDlg(HWND hwndMap, BOOL bUseNybbledSectorEffects);


#endif
