#ifndef __SRB2B_MAPOPTIONS__
#define __SRB2B_MAPOPTIONS__

#include <windows.h>

#include "../config.h"

typedef struct _MAPOPTPPDATA
{
	CONFIG	*lpcfgMap;
	char	szLumpName[9];
	char	cGametype;
	TCHAR	szAddWadName[MAX_PATH];
	BOOL	bOKed;
} MAPOPTPPDATA;


BOOL ShowMapProperties(MAPOPTPPDATA *lpmapoptppdata);
void PopulateGametypeList(HWND hwndCombo);


#endif
