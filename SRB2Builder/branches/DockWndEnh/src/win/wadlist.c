#include <windows.h>

#include "../general.h"
#include "../wad.h"
#include "../openwads.h"
#include "../map.h"
#include "../../res/resource.h"

#include "wadlist.h"
#include "mdiframe.h"
#include "gendlg.h"

#include "../../DockWnd/DockWnd.h"


static HWND g_hwndWadlist;
static HWND g_hwndTreeViewWads;

static BOOL CALLBACK WadListProc(HWND hwndDlg, UINT uiMsg, WPARAM wParam, LPARAM lParam);
static void AddMapNameToWadListCallback(LPCTSTR szLumpname, void *lpvParent);
static HTREEITEM FindWadInList(int iWad);


/* CreateWadListWindow
 *   Creates the wad-list tool window.
 *
 * Parameters:
 *   HWND	hwndParent	Parent window (i.e. MDI frame window).
 *
 * Return value: BOOL
 *   TRUE on success; FALSE on error.
 *
 * Remarks:
 *   We have to pass the frame window handle since this is called before the
 *   global var is set.
 */
BOOL CreateWadListWindow(HWND hwndParent)
{
	DOCKINFO *lpdi;
	TCHAR	szCaption[64];

	/* Allocate a docking structure. This'll be freed automatically by the lib.
	 */
	lpdi = DockingAlloc((char)DWS_DOCKED_LEFT);

	/* Destroy the dialogue when the docking container is destroyed. Also, hide
	 * the docking container if the user clicks the Close button, rather than
	 * destroying the window.
	 */
	lpdi->dwStyle |= DWS_NODESTROY | DWS_DESTROYFOCUSWIN;

	/* This window'll be destroyed by the lib, too. */
	LoadString(g_hInstance, IDS_WADLIST_CAPTION, szCaption, sizeof(szCaption) / sizeof(TCHAR));
	DockingCreateFrame(lpdi, hwndParent, szCaption);

	/* Now that we've got a frame, we can create the window that will appear
	 * inside it.
	 */
	g_hwndWadlist = lpdi->hwnd;

	lpdi->focusWindow = CreateDialog(g_hInstance, MAKEINTRESOURCE(IDD_WADLIST), lpdi->hwnd, WadListProc);

	/* Get a handle to the tree-view, since we use it quite a bit. */
	g_hwndTreeViewWads = GetDlgItem(lpdi->focusWindow, IDC_WADLIST_TREE);

	DockingShowFrame(lpdi);

	/* TODO: Hide it if the user has turned it off. */

	return TRUE;
}


/* WadListProc
 *   Dialogue procedure for the wad-list dialogue.
 *
 * Parameters:
 *   HWND	hwndDlg		Handle to dialogue window.
 *   HWND	uiMsg		Window message ID.
 *   WPARAM	wParam		Message-specific.
 *   LPARAM lParam		Message-specific.
 *
 * Return value: BOOL
 *   TRUE if message was processed; FALSE otherwise.
 *
 * Remarks:
 *   This is the procedure for the window *inside* the docking container.
 */
static BOOL CALLBACK WadListProc(HWND hwndDlg, UINT uiMsg, WPARAM wParam, LPARAM lParam)
{
	UNREFERENCED_PARAMETER(wParam);
	switch(uiMsg)
	{
	case WM_INITDIALOG:
		/* Let the system set the focus. */
		return TRUE;

	case WM_SIZE:
		/* Resize the tree window to fit the dialogue. */
		SetWindowPos(GetDlgItem(hwndDlg, IDC_WADLIST_TREE), NULL, 0, 0, LOWORD(lParam) - 9, HIWORD(lParam) - 9, SWP_NOMOVE|SWP_NOZORDER);
		return TRUE;

	case WM_NCHITTEST:
		{
			/* Get default HT result. */
			LRESULT lrHT = DefWindowProc(hwndDlg, uiMsg, wParam, lParam);

			/* If we're in the client are, fall through to the parent. */
			SetWindowLong(hwndDlg, DWL_MSGRESULT, (lrHT == HTCLIENT) ? HTTRANSPARENT : lrHT);
		}

		return TRUE;
	}

	return FALSE;
}



/* DestroyWadListWindow
 *   Destroys the wad-list window.
 *
 * Parameters:
 *   None.
 *
 * Return value: None.
 *
 * Remarks:
 *   This destroys the docking container, which in turn destroys the dialogue.
 */
void DestroyWadListWindow(void)
{
	if(IsWindow(g_hwndWadlist)) DestroyWindow(g_hwndWadlist);
}


/* AddToWadList
 *   Adds a wad to the list if it's not already there.
 *
 * Parameters:
 *   int	iWad	Index of wad.
 *
 * Return value: None.
 */
void AddToWadList(int iWad)
{
	int cchFilename, cchFileTitle;
	LPTSTR szFilename;
	TVINSERTSTRUCT tvis;
	HTREEITEM htiWad;
	
	/* If the wad's already on the list, stop. */
	if(FindWadInList(iWad)) return;

	/* Otherwise, add it. */

	/* Get file title. */
	cchFilename = GetWadFilename(iWad, NULL, 0);
	szFilename = ProcHeapAlloc(cchFilename * sizeof(TCHAR));
	GetWadFilename(iWad, szFilename, cchFilename);
	cchFileTitle = GetFileTitle(szFilename, NULL, 0);
	tvis.item.pszText = (LPTSTR)ProcHeapAlloc(cchFileTitle * sizeof(TCHAR));
	GetFileTitle(szFilename, tvis.item.pszText, cchFileTitle);
	ProcHeapFree(szFilename);

	/* The item parameter is the wad ID. */
	tvis.item.lParam = iWad;
	tvis.item.mask = TVIF_PARAM | TVIF_TEXT;

	tvis.hInsertAfter = TVI_SORT;
	tvis.hParent = TVI_ROOT;

	/* Add the wad node. */
	htiWad = TreeView_InsertItem(g_hwndTreeViewWads, &tvis);
	ProcHeapFree(tvis.item.pszText);

	/* Add the map lumps. */
	IterateMapLumpNames(iWad, AddMapNameToWadListCallback, (void*)htiWad);
}


/* AddMapNameToWadListCallback
 *   Adds a map to the wad list.
 *
 * Parameters:
 *   LPCTSTR	szLumpname	String to add.
 *   void*		lpvParent	(HTREEITEM) Parent node.
 *
 * Return value: None.
 *
 * Remarks:
 *   Intended for use with IterateMapLumpNames.
 */
static void AddMapNameToWadListCallback(LPCTSTR szLumpname, void *lpvParent)
{
	TVINSERTSTRUCT tvis;

	tvis.item.mask = TVIF_TEXT;

	/* This doesn't get changed, so we don't violate constness. */
	tvis.item.pszText = (LPTSTR)szLumpname;

	tvis.hInsertAfter = TVI_SORT;
	tvis.hParent = (HTREEITEM)lpvParent;

	/* Add it. */
	TreeView_InsertItem(g_hwndTreeViewWads, &tvis);
}


/* FindWadInList
 *   Finds the node corresponding to a wad in the list of wads.
 *
 * Parameters:
 *   int	iWad	Index of wad.
 *
 * Return value: HTREEITEM
 *   Handle to node, or NULL if not found.
 */
static HTREEITEM FindWadInList(int iWad)
{
	TVITEM tvitem;

	/* We determine this by checking the item data, which is the wad ID. */
	tvitem.mask = TVIF_HANDLE | TVIF_PARAM;

	/* Get the first root node. */
	tvitem.hItem = TreeView_GetRoot(g_hwndTreeViewWads);

	/* Check all the top-level nodes until we find a match or run out. */
	while(tvitem.hItem &&
		(TreeView_GetItem(g_hwndTreeViewWads, &tvitem), tvitem.lParam != iWad))
	{
		/* Advance to the next node. */
		tvitem.hItem = TreeView_GetNextSibling(g_hwndTreeViewWads, tvitem.hItem);
	}

	return tvitem.hItem;
}


/* RemoveFromWadList
 *   Removes a wad from the list.
 *
 * Parameters:
 *   int	iWad	Index of wad.
 *
 * Return value: None.
 *
 * Remarks:
 *   If the wad isn't in the list, does nothing.
 */
void RemoveFromWadList(int iWad)
{
	HTREEITEM hti = FindWadInList(iWad);

	if(hti) TreeView_DeleteItem(g_hwndTreeViewWads, hti);
}
