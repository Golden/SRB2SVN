#ifndef __SRB2B_MAPWIN__
#define __SRB2B_MAPWIN__

#include <windows.h>
#include <GL/gl.h>
#include <GL/glu.h>

#include "../maptypes.h"
#include "../config.h"
#include "../selection.h"
#include "../texture.h"

#include "../CodeImp/ci_const.h"

/* Types. */

enum ENUM_FIND_FLAGS
{
	FFL_SELECT = 1,
	FFL_REPLACE = 2
};


/* Function prototypes. */
HWND CreateMapWindow(MAP *lpmap, LPSTR szLumpName, CONFIG *lpcfgMap, CONFIG *lpcfgWadOpts, int iWadID, int iIWadID);
int RegisterMapWindowClass(void);
BOOL MapWinBelongsToWad(HWND hwnd, int iWadID);
BOOL GetTextureForMapW(HWND hwnd, LPCWSTR szTexNameW, TEXTURE **lplptex, TEX_FORMAT tf);
BOOL GetTextureForMapA(HWND hwnd, LPCSTR szTexName, TEXTURE **lplptex, TEX_FORMAT tf);
void MakeUndoText(HWND hwndMap, LPTSTR szCaption, DWORD cchCaption);
void MakeRedoText(HWND hwndMap, LPTSTR szCaption, DWORD cchCaption);
void DeselectAllByHandle(HWND hwndMap);
int FindReplaceInt(HWND hwndMap, WORD wFindType, WORD wCompare, int iValue, int iReplace, BOOL bInSelection, BYTE byFlags);
int FindReplaceStr(HWND hwndMap, WORD wFindType, WORD wCompare, LPSTR szString, LPSTR szReplace, BOOL bInSelection, BYTE byFlags);
void GridProperties(HWND hwndMap);
void TexPreviewClicked(HWND hwndMap, char cNotifyID);
void LoadPopupMenus(void);
void DestroyPopupMenus(void);

/* Macros. */
#define MAPWINCLASS TEXT("SRB2BuilderMap")

#endif
