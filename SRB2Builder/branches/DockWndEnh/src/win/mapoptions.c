#include <windows.h>
#include <commctrl.h>

#include "../general.h"
#include "../config.h"
#include "../mapconfig.h"
#include "../testing.h"
#include "../cdlgwrapper.h"

#include "mdiframe.h"
#include "mapoptions.h"

#include "../../res/resource.h"


static BOOL CALLBACK MapOptCfgPropProc(HWND hwndPropPage, UINT uiMsg, WPARAM wParam, LPARAM lParam);



/* ShowMapProperties
 *   Shows the Properties dialogue for the selection.
 *
 * Parameters:
 *   MAPPOPTPDATA*	lpmapoptppdata		Current properties.
 *
 * Return value: BOOL
 *   TRUE if the user OKed to exit the dialogue; FALSE if cancelled.
 *
 * Remarks:
 *   All the processing is done by the property sheet pages. This function just
 *   shows the dialogue box.
 */
BOOL ShowMapProperties(MAPOPTPPDATA *lpmapoptppdata)
{
	PROPSHEETHEADER		psh;
	PROPSHEETPAGE		psp[3];
	int					i;


	/* Build the info for the individual pages. */

	psp[0].pfnDlgProc = MapOptCfgPropProc;
	psp[0].pszTemplate = MAKEINTRESOURCE(IDD_PP_MAP_CFG);


	/* Fill in fields common to all pages. */
	for(i = 0; i < 3; i++)
	{
		psp[i].dwSize = sizeof(PROPSHEETPAGE);
		psp[i].dwFlags = /*PSP_HASHELP */ 0;
		psp[i].lParam = (LONG)lpmapoptppdata;
		psp[i].hInstance = g_hInstance;
	}

	/* Properties affecting the whole dialogue. */
	psh.dwSize = sizeof(psh);
	psh.dwFlags = PSH_HASHELP | PSH_NOAPPLYNOW | PSH_PROPTITLE | PSH_PROPSHEETPAGE;
	psh.hwndParent = g_hwndMain;
	psh.nPages = 1;
	psh.nStartPage = 0;
	psh.ppsp = psp;
	psh.pszCaption = MAKEINTRESOURCE(IDS_MAPOPTCAPTION);
	psh.hInstance = g_hInstance;

	/* Assume we cancelled. */
	lpmapoptppdata->bOKed = FALSE;

	/* Go! */
	PropertySheet(&psh);

	/* Did we close by clicking OK? */
	return lpmapoptppdata->bOKed;
}



/* MapOptCfgPropProc
 *   Dialogue proc for Configuration page of Map Properties dialogue.
 *
 * Parameters:
 *   HWND	hwndPropPage	Property page window handle.
 *   UINT	uiMsg			Message identifier.
 *   WPARAM wParam			Message-specific.
 *   LPARAM lParam			Message-specific.
 *
 * Return value: BOOL
 *   TRUE if the message was processed; FALSE otherwise.
 */
static BOOL CALLBACK MapOptCfgPropProc(HWND hwndPropPage, UINT uiMsg, WPARAM wParam, LPARAM lParam)
{
	static MAPOPTPPDATA *s_lpmapoptppdata;
	
	UNREFERENCED_PARAMETER(wParam);

	switch(uiMsg)
	{
	case WM_INITDIALOG:
		{
			HWND hwndMapConfigs = GetDlgItem(hwndPropPage, IDC_COMBO_CONFIG);
			HWND hwndGametypes = GetDlgItem(hwndPropPage, IDC_COMBO_TESTMODE);
			int iCount, i;
			TCHAR szLumpName[9];

			/* Get parameters. */
			s_lpmapoptppdata = (MAPOPTPPDATA*)((PROPSHEETPAGE*)lParam)->lParam;

			/* Fill the map config list and select the current one. */
			AddMapConfigsToComboBox(hwndMapConfigs);

			/* Find a matching config. */
			iCount = SendMessage(hwndMapConfigs, CB_GETCOUNT, 0, 0);
			for(i = 0; i < iCount; i++)
			{
				CONFIG *lpcfg = (CONFIG*)SendMessage(hwndMapConfigs, CB_GETITEMDATA, i, 0);
				if(lpcfg == s_lpmapoptppdata->lpcfgMap)
				{
					SendMessage(hwndMapConfigs, CB_SETCURSEL, i, 0);
					break;
				}
			}

			/* Get the lumpname. */
#ifdef _UNICODE
			MultiByteToWideChar(CP_ACP, MB_PRECOMPOSED, s_lpmapoptppdata->szLumpName, -1, szLumpName, sizeof(szLumpName)/sizeof(WCHAR));
#else
			lstrcpyA(szLumpName, s_lpmapoptppdata->szLumpName);
#endif
			SetWindowText(GetDlgItem(hwndPropPage, IDC_EDIT_LUMPNAME), szLumpName);

			
			/* Fill the list of gametypes. */
			PopulateGametypeList(hwndGametypes);

			/* Find a matching gametype. */
			iCount = SendMessage(hwndGametypes, CB_GETCOUNT, 0, 0);
			for(i = 0; i < iCount; i++)
			{
				char cGametype = (char)SendMessage(hwndGametypes, CB_GETITEMDATA, i, 0);
				if(cGametype == s_lpmapoptppdata->cGametype)
				{
					SendMessage(hwndGametypes, CB_SETCURSEL, i, 0);
					break;
				}
			}

			/* Set the external wad filename. */
			SetDlgItemText(hwndPropPage, IDC_EDIT_EXTWAD, s_lpmapoptppdata->szAddWadName);
		}
			
		/* Let the system set the focus. */
		return TRUE;

	case WM_COMMAND:
		switch(LOWORD(wParam))
		{
		case IDC_BUTTON_BROWSE:
			{
				TCHAR szFilename[MAX_PATH];
				TCHAR szFilter[128];

				/* As initial filename, use that currently specified. */
				GetDlgItemText(hwndPropPage, IDC_EDIT_EXTWAD, szFilename, sizeof(szFilename)/sizeof(TCHAR));

				/* Load the filter from the string table. */
				LoadAndFormatFilterString(IDS_WADFILEFILTER, szFilter, sizeof(szFilter)/sizeof(TCHAR));

				/* Show the Open dialogue. */
				if(CommDlgOpen(hwndPropPage, szFilename, sizeof(szFilename)/sizeof(TCHAR), NULL, szFilter, TEXT("wad"), szFilename, OFN_EXPLORER | OFN_FILEMUSTEXIST | OFN_HIDEREADONLY))
				{
					/* If the user didn't cancel, update the filename. */
					SetDlgItemText(hwndPropPage, IDC_EDIT_EXTWAD, szFilename);
				}
			}

			return TRUE;
		}

		/* Didn't process message. */
		break;

	case WM_NOTIFY:
		switch(((LPNMHDR)lParam)->code)
		{
		case PSN_APPLY:
			{
				int iSelection;

				/* Assume we don't fail any validation. This may change. */
				SetWindowLong(hwndPropPage, DWL_MSGRESULT, PSNRET_NOERROR);

				/* We didn't cancel. */
				s_lpmapoptppdata->bOKed = TRUE;

				/* Map configuration. */
				iSelection = SendDlgItemMessage(hwndPropPage, IDC_COMBO_CONFIG, CB_GETCURSEL, 0, 0);
				if(iSelection != CB_ERR)
					s_lpmapoptppdata->lpcfgMap = (CONFIG*)SendDlgItemMessage(hwndPropPage, IDC_COMBO_CONFIG, CB_GETITEMDATA, iSelection, 0);
				else
					s_lpmapoptppdata->lpcfgMap = NULL;

				/* Gametype. */
				iSelection = SendDlgItemMessage(hwndPropPage, IDC_COMBO_TESTMODE, CB_GETCURSEL, 0, 0);
				if(iSelection != CB_ERR)
					s_lpmapoptppdata->cGametype = (char)SendDlgItemMessage(hwndPropPage, IDC_COMBO_TESTMODE, CB_GETITEMDATA, iSelection, 0);

				/* TODO: Validate lumpname. */

				/* Lumpname. */
				GetDlgItemText(hwndPropPage, IDC_EDIT_LUMPNAME, s_lpmapoptppdata->szLumpName, sizeof(s_lpmapoptppdata->szLumpName));

				/* Additional wad. */
				GetDlgItemText(hwndPropPage, IDC_EDIT_EXTWAD, s_lpmapoptppdata->szAddWadName, sizeof(s_lpmapoptppdata->szAddWadName));
			}

			return TRUE;

		case PSN_QUERYCANCEL:
			/* Allow the cancel, and signal that we did so.. */
			SetWindowLong(hwndPropPage, DWL_MSGRESULT, FALSE);
			s_lpmapoptppdata->bOKed = FALSE;
			return TRUE;
		}

		/* Didn't process WM_NOTIFY message. */
		break;
	}

	return FALSE;
}


/* PopulateGametypeList
 *   Fills a combobox with gametypes.
 *
 * Parameters:
 *   HWND	hwndCombo	Window handle of combobox.
 *
 * Return value: None.
 */
void PopulateGametypeList(HWND hwndCombo)
{
	struct GTPAIR {char cGametype; USHORT unStringID;} gtpair[] =
	{
		{GT_DEFAULT, IDS_GT_DEFAULT},
		{GT_COOP, IDS_GT_COOP},
		{GT_FULLRACE, IDS_GT_FULLRACE},
		{GT_TIMEONLYRACE, IDS_GT_TIMEONLYRACE},
		{GT_MATCH, IDS_GT_MATCH},
		{GT_TAG, IDS_GT_TAG},
		{GT_CTF, IDS_GT_CTF},
		{GT_SINGLEPLAYER, IDS_GT_SINGLEPLAYER}
	};

	int i;

	for(i = 0; (unsigned int)i < sizeof(gtpair) / sizeof(struct GTPAIR); i++)
	{
		TCHAR szGametype[64];
		int iIndex;

		LoadString(g_hInstance, gtpair[i].unStringID, szGametype, sizeof(szGametype) / sizeof(TCHAR));
		iIndex = SendMessage(hwndCombo, CB_ADDSTRING, 0, (LPARAM)szGametype);

		SendMessage(hwndCombo, CB_SETITEMDATA, iIndex, gtpair[i].cGametype);
	}
}
