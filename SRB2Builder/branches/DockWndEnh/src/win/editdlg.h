#ifndef __SRB2B_EDITDLG__
#define __SRB2B_EDITDLG__

#include <windows.h>

#include "../map.h"
#include "../selection.h"
#include "../config.h"

#include "mapwin.h"


/* Macros. */
#define STRING_RELATIVE(sz) ((*(sz) == '+' && (sz)[1] == '+') || (*(sz) == '-' && (sz)[1] == '-'))


/* Types. */
typedef struct _MAPPPDATA
{
	HWND				hwndMap;
	MAP					*lpmap;
	SELECTION			*lpselection;
	CONFIG				*lpcfgMap;
	TEXTURENAMELIST		*lptnlFlats, *lptnlTextures;
	HIMAGELIST			*lphimlCacheFlats, *lphimlCacheTextures;
	BOOL				bOKed;
} MAPPPDATA;

enum ENUM_MAPPPFLAGS
{
	MPPF_SECTOR = 1,
	MPPF_LINE = 2,
	MPPF_THING = 4,
	MPPF_VERTEX = 8
};


typedef struct _INSERTSECTORDLGDATA
{
	short	nRadius;
	short	nEdges;
	BOOL	bSnap;
	char	cRadiusType;
} INSERTSECTORDLGDATA;

enum ENUM_RADIUS_TYPE
{
	RT_TOEDGES,
	RT_TOVERTICES
};


typedef struct _ROTATEDLGDATA
{
	int		iAngle;
	BOOL	bSnap;
	char	cCentreType;
	BOOL	bAlterThingAngles;
	BOOL	bThingsSelected;
	BOOL	bVerticesSelected;
} ROTATEDLGDATA;

enum ENUM_CENTRE_TYPE
{
	CT_BOUNDINGBOX, CT_THING, CT_VERTEX
};


typedef struct _RESIZEDLGDATA
{
	unsigned short	unFactor;
	BOOL			bSnap;
	char			cCentreType;
	BOOL			bThingsSelected;
	BOOL			bVerticesSelected;
} RESIZEDLGDATA;


typedef struct _ALIGNDLGDATA
{
	/* On the way in, these specify whether the options should be *enabled*; on
	 * the way out, they specify whether they were *selected*.
	 */
	BOOL	bFromThisLine;
	BOOL	bInSelection;

	/* This is used for returning only. */
	BYTE	byTexFlags;
} ALIGNDLGDATA;


typedef struct _MISSTEXDLGDATA
{
	/* On the way in, this specifies whether the option should be *enabled*; on
	 * the way out, it specifies whether it was *selected*.
	 */
	BOOL	bInSelection;

	/* Texture names. */
	TCHAR	szUpper[TEXNAME_BUFFER_LENGTH];
	TCHAR	szMiddle[TEXNAME_BUFFER_LENGTH];
	TCHAR	szLower[TEXNAME_BUFFER_LENGTH];

	/* These are needed to show the texture selection dialogue. */
	HWND			hwndMap;
	HIMAGELIST		*lphimlCache;
	TEXTURENAMELIST	*lptnlAllTextures;
} MISSTEXDLGDATA;


typedef struct _TPPDATA
{
	WNDPROC	wndprocStatic;
} TPPDATA;



/* Prototypes. */
BOOL ShowMapObjectProperties(DWORD dwPageFlags, DWORD dwStartPage, LPCTSTR szCaption, MAPPPDATA *lpmapppdata);
LRESULT CALLBACK TexPreviewProc(HWND hwnd, UINT uiMsg, WPARAM wParam, LPARAM lParam);
BOOL InsertSectorDlg(HWND hwndParent, INSERTSECTORDLGDATA *lpisdd);
BOOL RotateDlg(HWND hwndParent, ROTATEDLGDATA *lprotdd);
BOOL ResizeDlg(HWND hwndParent, RESIZEDLGDATA *lprszdd);
BYTE SelectPresetTypes(HWND hwndParent, BYTE byObjectFlags);
BOOL AlignDlg(HWND hwndParent, ALIGNDLGDATA *lpaligndd);
BOOL MissingTexDlg(HWND hwndParent, MISSTEXDLGDATA *lpmtdd);


#endif

