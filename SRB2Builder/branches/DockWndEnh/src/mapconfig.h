#ifndef __SRB2B_MAPCONFIG__
#define __SRB2B_MAPCONFIG__

#include <windows.h>

#include "config.h"
#include "map.h"

#define MAPCFG_GAME			"game"
#define MAPCFG_ID			"id"

#define FLAT_THING_SECTION "__things"

int LoadMapConfigs(void);
void UnloadMapConfigs(void);
void AddMapConfigsToComboBox(HWND hwndCombo);
CONFIG* GetThingConfigInfo(CONFIG *lpcfgThings, unsigned short unType);
void GetThingTypeDisplayText(unsigned short unType, CONFIG *lpcfgThings, LPTSTR szBuffer, unsigned short cchBuffer);
void GetThingDirectionDisplayText(short nDirection, LPTSTR szBuffer, unsigned short cbBuffer);
void GetEffectDisplayText(unsigned short unEffect, CONFIG *lpcfgSectors, LPTSTR szBuffer, unsigned short cchBuffer);
void SetAllThingPropertiesFromType(MAP *lpmap, CONFIG *lpcfgFlatThings);
WORD GetZFactor(CONFIG *lpcfgFlatThings, unsigned short unType);
void SetThingPropertiesFromType(MAP *lpmap, int iThing, CONFIG *lpcfgFlatThings);
int GetIWadForConfig(CONFIG *lpcfgMap);
CONFIG *GetDefaultMapConfig(void);

#endif
