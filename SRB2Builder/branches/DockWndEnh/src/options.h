#ifndef __SRB2B_OPTIONS__
#define __SRB2B_OPTIONS__

#include "config.h"

/* Macros. */
#define OPTDIR TEXT("conf/")

#define OPT_RECENT		"recent"
#define OPT_PALETTE		"palette"
#define OPT_SHORTCUTS	"shortcuts"
#define OPT_GAMECONFIGS	"gameconfigs"
#define OPT_DEFAULTTEX	"defaulttexture"
#define OPT_DEFAULTSEC	"defaultsector"
#define OPT_WINDOWPOS	"window"

#define TESTCFG_RENDERER	"renderer"
#define TESTCFG_WINDOWED	"windowed"
#define TESTCFG_SFX			"sfx"
#define TESTCFG_MUSIC		"music"
#define TESTCFG_MUSICTYPE	"musictype"
#define TESTCFG_PARAMS		"params"
#define TESTCFG_SKIN		"skin"
#define TESTCFG_GAMETYPE	"gametype"
#define TESTCFG_DIFFICULTY	"difficulty"

#define MRUENTRIES 10

/* The wheel is treated like a keypress. */
#define MOUSE_SCROLL_UP 4008
#define MOUSE_SCROLL_DOWN 4009


/* Types. */

/* Every time you update this table, alter the corresponding menu code map! */
typedef enum _SHORTCUTCODES
{
	SCK_EDITQUICKMOVE,
	SCK_ZOOMIN,
	SCK_ZOOMOUT,
	SCK_CENTREVIEW,
	SCK_EDITMOVE,
	SCK_EDITANY,
	SCK_EDITLINES,
	SCK_EDITSECTORS,
	SCK_EDITVERTICES,
	SCK_EDITTHINGS,
	SCK_EDIT3D,
	SCK_EDITSNAPTOGRID,
	SCK_FLIPLINEDEFS,
	SCK_FLIPSIDEDEFS,
	SCK_SPLITLINEDEFS,
	SCK_JOINSECTORS,
	SCK_MERGESECTORS,
	SCK_UNDO,
	SCK_REDO,
	SCK_FLIPHORIZ,
	SCK_FLIPVERT,
	SCK_COPY,
	SCK_PASTE,
	SCK_SAVEAS,
	SCK_INCFLOOR,
	SCK_DECFLOOR,
	SCK_INCCEIL,
	SCK_DECCEIL,
	SCK_INCLIGHT,
	SCK_DECLIGHT,
	SCK_INCTHINGZ,
	SCK_DECTHINGZ,
	SCK_SNAPSELECTION,
	SCK_GRADIENTFLOORS,
	SCK_GRADIENTCEILINGS,
	SCK_GRADIENTBRIGHTNESS,
	SCK_DRAWSECTOR,
	SCK_EDITCUT,
	SCK_EDITDELETE,
	SCK_CANCEL,
	SCK_GRADIENTTHINGZ,
	SCK_ROTATE,
	SCK_RESIZE,
	SCK_THINGROTACW,
	SCK_THINGROTCW,
	SCK_SELECTALL,
	SCK_SELECTNONE,
	SCK_INVERTSELECTION,
	SCK_COPYPROPS,
	SCK_PASTEPROPS,
	SCK_GRIDINC,
	SCK_GRIDDEC,
	SCK_FIND,
	SCK_REPLACE,
	SCK_NEW,
	SCK_OPEN,
	SCK_SAVE,
	SCK_MAPOPTIONS,
	SCK_MOUSEROTATE,
	SCK_STITCHVERTICES,
	SCK_SCROLL_UP,
	SCK_SCROLL_DOWN,
	SCK_SCROLL_LEFT,
	SCK_SCROLL_RIGHT,
	SCK_TEST,
	SCK_QUICKTEST,
	SCK_IDENTSECTORS,
	SCK_MAX
	/* _SCK_ */
} SHORTCUTCODES;
/* Every time you update this table, alter the corresponding menu code map! */


typedef struct _RENDEREROPTIONS
{
	int		iIndicatorSize;
	int		iVertexSize;
	BOOL	bVerticesInLinesMode;
	BOOL	bVerticesInSectorsMode;
} RENDEREROPTIONS;



/* extern globals. */
extern CONFIG *g_lpcfgMain;
extern int g_iShortcutCodes[SCK_MAX];
extern RENDEREROPTIONS g_rendopts;

/* Prototypes. */
BOOL LoadMainConfigurationFile(void);
void UnloadMainConfigurationFile(void);
void MRUAdd(LPCTSTR szFilename);
CONFIG* GetOptionsForGame(CONFIG *lpcfgMap);
CONFIG* GetTestingOptions(CONFIG *lpcfgMainGame);

/* Inline functions. */
static __inline void MRUClear(void)
{
	ConfigSetSubsection(g_lpcfgMain, OPT_RECENT, ConfigCreate());
}

#endif
