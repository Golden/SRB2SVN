#ifndef __SRB2B_CDLGWRAPPER__
#define __SRB2B_CDLGWRAPPER__

int CommDlgOpen(HWND hwnd, LPTSTR szFileNameReturn, UINT cbFileNameReturn, LPCTSTR szTitle, LPCTSTR szFilter, LPCTSTR szDefExt, LPCTSTR szInitFilename, int iFlags);
int CommDlgSave(HWND hwnd, LPTSTR szFileNameReturn, UINT cbFileNameReturn, LPCTSTR szTitle, LPCTSTR szFilter, LPCTSTR szDefExt, LPCTSTR szInitFilename, int iFlags);

#endif
