#include <windows.h>

#include "general.h"
#include "cliputil.h"

#include "win/mdiframe.h"


/* CopyBufferToClipboard
 *   Copies data from a buffer to the clipboard.
 *
 * Parameters:
 *   void*	lpvBuffer	Buffer.
 *   DWORD	cbBuffer	Size of buffer, in bytes.
 *   UINT	uiFormat	Clipboard format ID.
 *
 * Return value: int
 *   Zero on success; non-zero on error.
 */
int CopyBufferToClipboard(void *lpvBuffer, DWORD cbBuffer, UINT uiFormat)
{
	GLOBALHANDLE	hglobalClipboard;
	void			*lpvClipboard;

	/* Allocate a shared global block for the clipboard's use. */
	hglobalClipboard = GlobalAlloc(GMEM_MOVEABLE, cbBuffer);

	/* Out of memory? */
	if(!hglobalClipboard) return 1;

	/* Copy data into the clipboard's memory. */
	lpvClipboard = GlobalLock(hglobalClipboard);
	CopyMemory(lpvClipboard, lpvBuffer, cbBuffer);
	GlobalUnlock(hglobalClipboard);

	/* Give the clipboard its memory. */
	if(!OpenClipboard(g_hwndMain)) return 2;
	EmptyClipboard();
	SetClipboardData(uiFormat, hglobalClipboard);
	CloseClipboard();

	/* Finished! We don't free the memory we allocated, since the clipboard's
	 * still using it.
	 */
	return 0;
}


/* GetBufferFromClipboard
 *   Copies clipboard data into a buffer.
 *
 * Parameters:
 *   UINT	uiFormat	Clipboard data format.
 *
 * Return value: void*
 *   Pointer to the buffer containing the clipboard data if successful; NULL on
 *   error (including there being no matching data on the clipboard).
 *
 * Remarks:
 *   The caller is responsible for freeing the returned buffer.
 */
void* GetBufferFromClipboard(UINT uiFormat)
{
	GLOBALHANDLE hglobalClipboard;
	void *lpvBuffer = NULL;

	if(!OpenClipboard(g_hwndMain)) return NULL;
	hglobalClipboard = GetClipboardData(uiFormat);

	/* Highligly unlikely that GetClipboardData failed, but still possible. */
	if(hglobalClipboard)
	{
		DWORD	cbBuffer = GlobalSize(hglobalClipboard);
		void	*lpvClipboard = GlobalLock(hglobalClipboard);

		lpvBuffer = ProcHeapAlloc(cbBuffer);

		CopyMemory(lpvBuffer, lpvClipboard, cbBuffer);

		GlobalUnlock(hglobalClipboard);
	}

	CloseClipboard();

	return lpvBuffer;
}

