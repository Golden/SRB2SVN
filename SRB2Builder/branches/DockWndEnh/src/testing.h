#ifndef __SRB2B_TESTING__
#define __SRB2B_TESTING__

#include <windows.h>

#include "config.h"


enum ENUM_GAMETYPES
{
	GT_COOP = 0,
	GT_MATCH = 1,
	GT_FULLRACE = 2,
	GT_TAG = 3,
	GT_CTF = 4,
	GT_SINGLEPLAYER = 6,
	GT_DEFAULT = 7,
	GT_TIMEONLYRACE = 43,
};


/* The values correspond to the command-line-option values for SRB2. */
enum ENUM_DIFFICULTIES
{
	DIFF_EASY,
	DIFF_NORMAL,
	DIFF_HARD,
	DIFF_VERYHARD,
	DIFF_ULTIMATE
};


typedef struct _TESTTHREADDATA
{
	HANDLE	hProcess;
	TCHAR	szFilename[MAX_PATH];
} TESTTHREADDATA;


HANDLE TestMapWithSRB2(LPCTSTR szWadFile, LPCSTR sMapLumpname, LPCTSTR szBinary, LPCTSTR szAddWadFile, CONFIG *lpcfgTest, CONFIG *lpcfgWadOptsMap);
BOOL TestInProgress(void);
void TestThreadProc(void *lpvTestData);


#endif
