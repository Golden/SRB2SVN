#ifndef __SRB2B_WADOPTS__
#define __SRB2B_WADOPTS__

#include <windows.h>

#include "config.h"


#define WADOPT_MAPCONFIG		"mapconfig"


CONFIG *GetWadOptions(LPCTSTR szWadFilename);
CONFIG* NewWadOptions(void);
void StoreMapCfgForWad(CONFIG *lpcfgWad, CONFIG *lpcfgMap);
CONFIG* GetMapOptionsFromWadOpt(CONFIG *lpcfgWadOpt, LPCSTR szLumpname);
int WriteWadOptions(LPCTSTR szWadFilename, CONFIG *lpcfgWad);


#endif

