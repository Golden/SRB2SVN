/* DockWnd.h */

#ifndef DOCKWINDOW_INCLUDED
#define DOCKWINDOW_INCLUDED

#ifdef __cplusplus
extern "C" {
#endif

typedef struct _DOCKINFO DOCKINFO;
typedef DOCKINFO* LPDOCKINFO;

typedef LRESULT (CALLBACK *DockMsgFunc)(struct _DOCKINFO *, UINT, WPARAM, LPARAM);
typedef VOID (CALLBACK *DockResizeFunc)(LPDOCKINFO, LPRECT);
typedef VOID (CALLBACK *DockDestroyFunc)(LPDOCKINFO);
typedef DWORD (CALLBACK *DockCloseFunc)(LPDOCKINFO);

#ifdef __GNUC__
#define ATTRPACK __attribute__ ((packed))
#else
#define ATTRPACK
#endif

#ifdef _MSC_VER
#pragma pack(1)
#endif

typedef struct {
	UINT count;
	HWND container;
} ATTRPACK DOCKCOUNTPARAMS, *LPDOCKCOUNTPARAMS;

typedef struct {
	HDWP   hdwp;
	LPRECT rect;
	BYTE   which;
} ATTRPACK DOCKLAYOUTPARAMS;

typedef struct {
	HWND   container; /* Handle to container window. */
	HWND   hwnd;      /* Handle to window which received WM_NCACTIVATE or WM_ENABLE (can
                       * be the container, or some Docking Frame window).
                       */
	WPARAM wParam;    /* WPARAM of the WM_NCACTIVATE/WM_ENABLE message. */
	LPARAM lParam;    /* LPARAM of the WM_NCACTIVATE/WM_ENABLE message. */
} ATTRPACK DOCKPARAMS, *LPDOCKPARAMS;

struct ATTRPACK _DOCKINFO
{
	/* For application's use */
	LPVOID          userdata;

	/* Various bits, see below. */
	DWORD           dwStyle;

	/* XY position (relative to screen) of Docking Frame window when floating. */
	int             xpos;
	int             ypos;
	/* Width/Height of client area of Docking Frame window when floating. */
	long            cxFloating;
	long            cyFloating;

	/* Width or height of window when docked. */
	long            nDockedSize;

	/* HWND of the window to set the focus whenever the user completes an operation with
	 * the Docking Frame. If NULL, no focus is set. This is also the window that is resized
	 * to fill the client area if no DockResize callback is specified.
	 */
	HWND            focusWindow;

	/* Callback for messages received by the Docking frame but not handled by it.
	 * NULL if not needed.
	 */
	DockMsgFunc     DockMsg;

	/* Callback to resize the contents of the Docking Frame's client area. */
	DockResizeFunc  DockResize;

	/* Callback to query if the app wants to allow the Docking Frame to be closed. */
	DockCloseFunc   DockClose;

	/* Callback to notify the app that this DockInfo is no longer in service. */
	DockDestroyFunc DockDestroy;

	/* The following fields are initialized/used by the DockWnd library, so do not modify these. */

	/* Handle to the Docking Frame (ie, tool window). */
	HWND            hwnd;

	/* Handle to the container (ie, owner) window. */
	HWND            container;

	/* Docking Frame window width/height when floating. */
	int             nFrameWidth;
	int             nFrameHeight;

	/* Indicates to which side of the container window the Docking Frame is docked (if any). */
	char            uDockedState;
};

#if 0
typedef struct _DILIST
{
	LPDOCKINFO	lpdi;

	struct _DILIST	*lpdilNext;
} ATTRPACK DILIST, *LPDILIST;

typedef struct _DRLIST
{
	DILIST		dilHdr;
	LPDILIST	lpdilLast;

	struct _DRLIST *lpdrlNext;
} ATTRPACK DRLIST, *LPDRLIST;

// Maintains information about DockWnds belonging to a specific parent window.
typedef struct _DOCKCONTEXT
{
	HWND		hwndContainer;

	// Lists of rows/columns of docked windows.
	DRLIST		drlWindows[5];	// 5 == edgesof(rectangle) + 1 for floating.
	LPDRLIST	lpdrlWindowsLast[5];
} ATTRPACK DOCKCONTEXT, *LPDOCKCONTEXT;
#endif

#ifdef _MSC_VER
#pragma pack()
#endif

#undef ATTRPACK

//	DOCKINFO dwStyle
#define DWS_ALLOW_DOCKLEFT      0x01 // Allow the Docking Frame to be docked to the left side of container window.
#define DWS_ALLOW_DOCKRIGHT     0x02
#define DWS_ALLOW_DOCKTOP       0x04
#define DWS_ALLOW_DOCKBOTTOM    0x08
#define DWS_DRAWGRIPPERDOCKED   0x00000010	// Draw a gripper when docked.
#define DWS_DRAWGRIPPERFLOATING 0x00000020	// Draw a gripper when floating.
#define DWS_KEEPORIGSTATE       0x00000040	// Force the Docking Frame to always stay docked or floating as it was originally created.
#define DWS_NORESIZE            0x00000080	// Prevent user resizing the Docking Frame.
#define DWS_DONTSAVEPOS         0x00000100	// Don't save changed position/size. This is primarily set only by the docking library.
#define DWS_NODESTROY           0x00000200	// Hides the Docking Frame window instead of destroying it.
#define DWS_NODISABLE           0x00000400	// Does not disable the Docking Frame window when DockingEnable() is called, and the window is floating.
#define DWS_FREEFLOAT           0x00000800	// Docking Frame window can float behind the container.
#define DWS_DESTROYFOCUSWIN     0x00001000	// Destroy focus window when cleaning up.

#define DWS_HIDDEN              0x80000000	// Used only by docking library.
#define DWS_ALLOW_DOCKALL       (DWS_ALLOW_DOCKLEFT|DWS_ALLOW_DOCKBOTTOM|DWS_ALLOW_DOCKRIGHT|DWS_ALLOW_DOCKTOP)

// DOCKINFO uDockedState
#define DWS_FLOATING            0x80
#define DWS_DOCKED_LEFT         DWS_ALLOW_DOCKLEFT
#define DWS_DOCKED_RIGHT        DWS_ALLOW_DOCKRIGHT
#define DWS_DOCKED_TOP          DWS_ALLOW_DOCKTOP
#define DWS_DOCKED_BOTTOM       DWS_ALLOW_DOCKBOTTOM

// Gripper width
#define CX_GRIPPER              7

// Function declarations
extern VOID WINAPI			DockingUnInitialize(VOID);
extern ULONG WINAPI			DockingInitialize(HINSTANCE);
extern UINT WINAPI			DockingCountFrames(HWND, UINT);
extern VOID WINAPI			DockingArrangeWindows(HWND, HDWP, RECT *);
extern LPDOCKINFO WINAPI	DockingAlloc(char);
extern VOID WINAPI			DockingFree(LPDOCKINFO);

#if 0
extern LPDOCKCONTEXT WINAPI DockContextAlloc(HWND);
extern VOID WINAPI			DockingContextFree(LPDOCKCONTEXT);
#endif

extern HWND WINAPI			DockingCreateFrame(LPDOCKINFO, HWND, LPCTSTR);
extern VOID WINAPI			DockingShowFrame(LPDOCKINFO);
extern LRESULT WINAPI		DockingEnable(LPDOCKPARAMS);
extern LRESULT WINAPI		DockingActivate(LPDOCKPARAMS);
extern VOID WINAPI			DockingLoadPlacement(LPDOCKINFO, HKEY);
extern DWORD WINAPI			DockingSavePlacement(LPDOCKINFO, HKEY);
extern VOID WINAPI			DockingUpdateLayout(HWND);
extern VOID WINAPI			DockingRedrawFrame(LPDOCKINFO);
extern LPDOCKINFO WINAPI	DockingIsToolWindow(HWND, HWND);
extern VOID WINAPI			DockingDestroyFreeFloat(HWND);

#ifdef __cplusplus
}
#endif

#endif
