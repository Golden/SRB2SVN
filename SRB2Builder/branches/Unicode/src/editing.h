#ifndef __SRB2B_EDITING__
#define __SRB2B_EDITING__

#include "general.h"
#include "map.h"
#include "config.h"
#include "renderer.h"
#include "selection.h"
#include "win/infobar.h"


/* Types. */
typedef struct _DRAW_OPERATION
{
	int				*lpiNewVertices;
	int				*lpiNewLines;

	int				iNewVertexCount;
	int				iNewLineCount;

	unsigned int	ciVertexBuffer;
	unsigned int	ciLineBuffer;
} DRAW_OPERATION;

typedef struct _LOOPLIST LOOPLIST;

typedef struct _LINESEGMENT
{
	short x1, y1;
	short x2, y2;
} LINESEGMENT;


enum ENUM_VERTEX_EDIT_FLAGS
{
	VEF_NEW = 1,
	VEF_USED = 2,
	VEF_LABELLED = 4,
	VEF_DRAGGING = 8
};

enum ENUM_LINEDEF_EDIT_FLAGS
{
	LEF_NEW = 1,
	LEF_INVALIDSIDEDEFS = 2,	/* For new lds which haven't been set yet. */
	LEF_LOOPFRONT = 4,
	LEF_LOOPBACK = 8,
	LEF_ENCLOSEDFRONT = 16,
	LEF_ENCLOSEDBACK = 32,
	LEF_RECALCSECTOR = 64,
	LEF_LENGTHCHANGING = 128,

	/* Values are important! They correspond to the LDTF_* flags << 8. */
	LEF_VISITED_FRONTUPPER = 0x100,
	LEF_VISITED_FRONTMIDDLE = 0x200,
	LEF_VISITED_FRONTLOWER = 0x400,
	LEF_VISITED_BACKUPPER = 0x800,
	LEF_VISITED_BACKMIDDLE = 0x1000,
	LEF_VISITED_BACKLOWER = 0x2000,

	LEF_2PASSFRONT = 0x4000,
	LEF_2PASSBACK = 0x8000,
	LEF_NEWSECSETFRONT = 0x10000,
	LEF_NEWSECSETBACK = 0x20000,

	LEF_NEWLOOPFRONT = 0x40000,
	LEF_NEWLOOPBACK = 0x80000,

	LEF_LABELLED = 0x80000000		/* A generic label. */
};

#define LEF_VISITED_FRONT (LEF_VISITED_FRONTUPPER | LEF_VISITED_FRONTMIDDLE | LEF_VISITED_FRONTLOWER)
#define LEF_VISITED_BACK (LEF_VISITED_BACKUPPER | LEF_VISITED_BACKMIDDLE | LEF_VISITED_BACKLOWER)
#define LEF_VISITED (LEF_VISITED_FRONT | LEF_VISITED_BACK)

enum ENUM_SNAP_FLAGS
{
	SF_RECTANGLE	= 1,
	SF_45			= 2,
	SF_VERTICES		= 4
};

/* These can be used as indices into arrays. */
enum ENUM_LINE_SIDE
{
	LS_FRONT, LS_BACK
};

/* These can be used as indices into arrays. */
enum ENUM_SDTEX
{
	SDT_UPPER, SDT_MIDDLE, SDT_LOWER
};

/* Values are important! They correspond to the LEF_VISITED flags >> 8. */
enum ENUM_LDTEX_FLAGS
{
	LDTF_FRONTUPPER		= 0x01,
	LDTF_FRONTMIDDLE	= 0x02,
	LDTF_FRONTLOWER		= 0x04,
	LDTF_BACKUPPER		= 0x08,
	LDTF_BACKMIDDLE		= 0x10,
	LDTF_BACKLOWER		= 0x20
};

#define LDTF_FRONT (LDTF_FRONTUPPER | LDTF_FRONTMIDDLE | LDTF_FRONTLOWER)
#define LDTF_BACK (LDTF_BACKUPPER | LDTF_BACKMIDDLE | LDTF_BACKLOWER)
#define LDTF_ALL (LDTF_FRONT | LDTF_BACK)

/* Flags have precedence. One lassoed ld beats lots of selected lds when it
 * comes to determining sector states.
 */
enum ENUM_SELFLAGS
{
	SLF_SELECTED = 1,
	SLF_LASSOING = 2
};


/* Prototypes. */
int RemoveUnusedVertices(MAP *lpmap, BYTE byRequiredFlags);
void GetLinedefDisplayInfo(MAP *lpmap, CONFIG *lpcfgLinedefFlat, int iIndex, LINEDEFDISPLAYINFO *lplddi);
void GetSectorDisplayInfo(MAP *lpmap, CONFIG *lpcfgSectors, int iIndex, SECTORDISPLAYINFO *lpsdi);
void GetThingDisplayInfo(MAP *lpmap, CONFIG *lpcfgThings, int iIndex, THINGDISPLAYINFO *lptdi);
void GetVertexDisplayInfo(MAP *lpmap, int iIndex, VERTEXDISPLAYINFO *lpvdi);
DWORD CheckLines(MAP *lpmap, SELECTION_LIST *lpsellist, CONFIG *lpcfgLinedefTypesFlat, LINEDEFDISPLAYINFO *lpsdi);
DWORD CheckSectors(MAP *lpmap, SELECTION_LIST *lpsellist, CONFIG *lpcfgSecTypes, SECTORDISPLAYINFO *lpsdi);
DWORD CheckThings(MAP *lpmap, SELECTION_LIST *lpsellist, CONFIG *lpcfgFlatThings, THINGDISPLAYINFO *lptdi);
DWORD CheckVertices(MAP *lpmap, SELECTION_LIST *lpsellist, VERTEXDISPLAYINFO *lptdi);
void CheckLineFlags(MAP *lpmap, SELECTION_LIST *lpsellist, WORD *lpwFlagValues, WORD *lpwFlagMask);
void SetLineFlags(MAP *lpmap, SELECTION_LIST *lpsellist, WORD wFlagValues, WORD wFlagMask);
void CheckThingFlags(MAP *lpmap, SELECTION_LIST *lpsellist, WORD *lpwFlagValues, WORD *lpwFlagMask);
void SetThingFlags(MAP *lpmap, SELECTION_LIST *lpsellist, WORD wFlagValues, WORD wFlagMask);
BYTE CheckTextureFlags(MAP *lpmap, SELECTION_LIST *lpsellist);
unsigned short NextUnusedTag(MAP *lpmap);
BOOL GetAdjacentSectorHeightRange(MAP *lpmap, short *lpnMaxCeil, short *lpnMinCeil, short *lpnMaxFloor, short *lpnMinFloor);
void GetMapRect(MAP *lpmap, RECT *lprc);
void GetSelectionRect(MAP *lpmap, SELECTION *lpselection, RECT *lprc);
void ApplySectorPropertiesToSelection(MAP *lpmap, SELECTION_LIST *lpsellist, SECTORDISPLAYINFO *lpsdi, DWORD dwFlags);
void ApplySectorProperties(MAP *lpmap, int iIndex, SECTORDISPLAYINFO *lpsdi, DWORD dwFlags);
void ApplyLinePropertiesToSelection(MAP *lpmap, SELECTION_LIST *lpsellist, LINEDEFDISPLAYINFO *lplddi, DWORD dwFlags);
void ApplyLineProperties(MAP *lpmap, int iIndex, LINEDEFDISPLAYINFO *lplddi, DWORD dwFlags);
void ApplyThingPropertiesToSelection(MAP *lpmap, SELECTION_LIST *lpsellist, THINGDISPLAYINFO *lptdi, DWORD dwFlags, CONFIG *lpcfgFlatThings);
void ApplyThingProperties(MAP *lpmap, int iIndex, THINGDISPLAYINFO *lptdi, DWORD dwFlags);
int AddVertex(MAP *lpmap, short x, short y);
int AddThing(MAP *lpmap, short x, short y);
int AddLinedef(MAP *lpmap, int iVertexStart, int iVertexEnd);
int AddSector(MAP *lpmap);
int AddSidedef(MAP *lpmap, int iSector);
void BeginDrawOperation(DRAW_OPERATION *lpdrawop);
void EndDrawOperation(MAP *lpmap, DRAW_OPERATION *lpdrawop, CONFIG *lpcfgWadOptMap);
BOOL DrawToNewVertex(MAP *lpmap, DRAW_OPERATION *lpdrawop, CONFIG *lpcfgWadOptsMap, short x, short y, BOOL bStitch);
void FlipLinedef(MAP *lpmap, int iLinedef);
void FlipSelectedLinedefs(MAP *lpmap, SELECTION_LIST* lpsellist);
void ExchangeSelectedSidedefs(MAP *lpmap, SELECTION_LIST* lpsellist);
int SplitLinedefAtPoint(MAP *lpmap, int iLinedef, short x, short y);
void BisectSelectedLinedefs(MAP *lpmap, SELECTION_LIST* lpsellist);
int SplitLinedef(MAP *lpmap, int iLinedef, int iVertex);
int GetVertexFromPosition(MAP *lpmap, short x, short y);
int FindLinedefBetweenVertices(MAP *lpmap, int iVertexA, int iVertexB);
int FindLinedefBetweenVerticesDirected(MAP *lpmap, int iVertex1, int iVertex2);
void Snap(short *lpx, short *lpy, unsigned short cxSnap, unsigned short cySnap, short xOffset, short yOffset);
BOOL VerticesReachable(MAP *lpmap, int iVertex1, int iVertex2);
void DeleteSector(MAP *lpmap, int iSector);
void DeleteLinedef(MAP *lpmap, int iLinedef);
void DeleteSidedef(MAP *lpmap, int iSidedef);
void DeleteVertex(MAP *lpmap, int iVertex);
void DeleteThing(MAP *lpmap, int iThing);
void JoinSelectedSectors(MAP *lpmap, SELECTION_LIST *lpsellist, BOOL bMerge);
void LabelSelectedLinesRS(MAP *lpmap);
void LabelRSLinesEnclosure(MAP *lpmap, LOOPLIST *lplooplist);
LOOPLIST* LabelRSLinesLoops(MAP *lpmap);
void LabelLoopedLinesRS(MAP *lpmap, LOOPLIST *lplooplist);
void DestroyLoopList(LOOPLIST *lplooplist);
void ClearDraggingFlags(MAP *lpmap);
void CorrectDraggedSectorReferences(MAP *lpmap);
void FlipVertexAboutVerticalAxis(MAP *lpmap, int iVertex, short xAxis);
void FlipThingAboutVerticalAxis(MAP *lpmap, int iThing, short xAxis);
void FlipVertexAboutHorizontalAxis(MAP *lpmap, int iVertex, short yAxis);
void FlipThingAboutHorizontalAxis(MAP *lpmap, int iThing, short yAxis);
void SetThingSelectionZ(MAP *lpmap, SELECTION_LIST *lpsellist, CONFIG *lpcfgFlatThings, WORD z, BOOL bAbsolute);
void StitchMultipleVertices(MAP *lpmap, const int *lpiSrcVertices, unsigned int uiNumSrcVertices, int iTargetVertex);

static __inline void StitchVertices(MAP *lpmap, int iSrc, int iTarget)
{
	StitchMultipleVertices(lpmap, &iSrc, 1, iTarget);
}

void StitchDraggedVertices(MAP *lpmap, SELECTION_LIST *lpsellistVertices);
void DeleteZeroLengthLinedefs(MAP *lpmap);
BYTE RequiredTextures(MAP *lpmap, int iLinedef);
int GetThingZ(MAP *lpmap, int iThing, CONFIG *lpcfgFlatThings, BOOL bAbsolute);
__inline void SetThingZ(MAP *lpmap, int iThing, CONFIG *lpcfgFlatThings, WORD z, BOOL bAbsolute);
void ApplyRelativeCeilingHeightSelection(MAP *lpmap, SELECTION_LIST *lpsellistSectors, int iDelta);
void ApplyRelativeFloorHeightSelection(MAP *lpmap, SELECTION_LIST *lpsellistSectors, int iDelta);
void ApplyRelativeBrightnessSelection(MAP *lpmap, SELECTION_LIST *lpsellistSectors, short nDelta);
void ApplyRelativeThingZSelection(MAP *lpmap, SELECTION_LIST *lpselllistThings, CONFIG *lpcfgFlatThings, int iDelta);
void ApplyRelativeAngleSelection(MAP *lpmap, SELECTION_LIST *lpselllistThings, int iDelta);
void ApplyRelativeThingXSelection(MAP *lpmap, SELECTION_LIST *lpselllistThings, int iDelta);
void ApplyRelativeThingYSelection(MAP *lpmap, SELECTION_LIST *lpselllistThings, int iDelta);
void ApplyRelativeFrontSidedefXSelection(MAP *lpmap, SELECTION_LIST *lpselllistLines, int iDelta);
void ApplyRelativeFrontSidedefYSelection(MAP *lpmap, SELECTION_LIST *lpselllistLines, int iDelta);
void ApplyRelativeBackSidedefXSelection(MAP *lpmap, SELECTION_LIST *lpselllistLines, int iDelta);
void ApplyRelativeBackSidedefYSelection(MAP *lpmap, SELECTION_LIST *lpselllistLines, int iDelta);
void GradientSelectedCeilings(MAP *lpmap, SELECTION_LIST *lpsellistSectors);
void GradientSelectedFloors(MAP *lpmap, SELECTION_LIST *lpsellistSectors);
void GradientSelectedBrightnesses(MAP *lpmap, SELECTION_LIST *lpsellistSectors);
void GradientSelectedThingZ(MAP *lpmap, SELECTION_LIST *lpsellistThings, CONFIG *lpcfgFlatThings);
void RotateVertexAboutPoint(MAP *lpmap, int iVertex, short x, short y, float fAngle);
void RotateThingAboutPoint(MAP *lpmap, int iThing, short x, short y, float fAngle);
void RotateThingDirection(MAP *lpmap, int iThing, int iAngle);
void DilateVertexWrtFixedPoint(MAP *lpmap, int iVertex, short x, short y, float fFactor);
void DilateThingWrtFixedPoint(MAP *lpmap, int iThing, short x, short y, float fFactor);
void RotateThingToPoint(MAP *lpmap, int iThing, short x, short y);
void LabelChangingLines(MAP *lpmap, SELECTION_LIST *lpsellistVertices);
void LabelDraggingVertices(MAP *lpmap, SELECTION_LIST *lpsellistVertices);
void RequiredSnapping(MAP *lpmap, short *lpx, short *lpy, MAPVIEW *lpmapview, BYTE bySnapFlags, ENUM_EDITSUBMODE submode, int iLastVertex);
void AutoalignTexturesFromLinedef(MAP *lpmap, HWND hwndMap, int iStartLinedef, BYTE byTexFlags, BOOL bInSelection);
void AutoalignAllTextures(MAP *lpmap, HWND hwndMap, BYTE byTexFlags, BOOL bInSelection);
void FixMissingTextures(MAP *lpmap, LPTSTR szUpper, LPTSTR szMiddle, LPTSTR szLower, BOOL bInSelection);
int FindIdenticalSectorSet(MAP *lpmap, int iStartSector, BOOL bInSelection, DYNAMICINTARRAY *lpdiarray);
int MergeIdenticalSectors(MAP *lpmap, BOOL bInSelection);
CONFIG* CreateUsedTexturesConfig(MAP *lpmap);
CONFIG* CreateUsedFlatsConfig(MAP *lpmap);
void MakeSelectedLinesSingleSided(MAP *lpmap, SELECTION_LIST* lpsellist);
void MakeSelectedLinesDoubleSided(MAP *lpmap, SELECTION_LIST* lpsellist, CONFIG *lpcfgWadOptMap);
void RemoveSelectedSectorInteriors(MAP *lpmap, SELECTION_LIST* lpsellist);

#ifdef _DEBUG
BOOL VerifyEditFlagIntegrity(MAP *lpmap);
#endif

#endif
