#ifndef __SRB2B_SELECTION__
#define __SRB2B_SELECTION__

/* Types. */
typedef struct _SELECTION_LIST
{
	int ciBufferLength;
	int iDataCount;
	int *lpiIndices;
} SELECTION_LIST;

typedef struct _SELECTION
{
	SELECTION_LIST *lpsellistSectors, *lpsellistLinedefs, *lpsellistVertices, *lpsellistThings;
} SELECTION;


/* Function prototypes. */
SELECTION_LIST* AllocateSelectionList(int iInitListSize);
void DestroySelectionList(SELECTION_LIST *lpsellist);
void AddToSelectionList(SELECTION_LIST *lpsellist, int iValue);
BOOL RemoveFromSelectionList(SELECTION_LIST *lpsellist, int iValue);
BOOL ExistsInSelectionList(SELECTION_LIST *lpsellist, int iValue);
void SortSelectionList(SELECTION_LIST *lpsellist);

/* Inline functions. */

/* ClearSelectionList
 *   Removes all items from a selection list.
 *
 * Parameters:
 *   SELECTION_LIST*	lpsellist	Pointer to selection list to empty.
 *
 * Return value: None.
 */
static __inline void ClearSelectionList(SELECTION_LIST *lpsellist)
{
	lpsellist->iDataCount = 0;
}


#endif
