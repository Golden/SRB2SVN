#ifndef __SRB2B_RENDERER__
#define __SRB2B_RENDERER__

#ifdef __cplusplus
extern "C" {
#endif

#include <windows.h>
#include <GL/gl.h>
#include <GL/glu.h>

#include "map.h"
#include "selection.h"
#include "win/mapwin.h"

/* Types. */
typedef struct _MAPVIEW
{
	short	cxDrawSurface, cyDrawSurface;
	float	xLeft, yTop;
	float	fZoom;
	int		iIndicatorSize;
	float	fVertexSize;
	float	fThingSize;
	unsigned short	cxGrid, cyGrid;
	short	xGridOffset, yGridOffset;
	BOOL	bShowGrid, bShow64Grid;
	BOOL	bShowAxes;
	GLuint	uiThingTex;
	int		iVxSnapDist;
} MAPVIEW;

typedef struct _MAP_RENDERINFO
{
	ENUM_EDITMODE editmode;
	ENUM_EDITSUBMODE submode;
	ENUM_INSERT_MODE insertmode;
	MAPVIEW *lpmapview;
	SELECTION *lpselection;
	short nHighlightTag;

	union
	{
		POINT ptSrc;
		FPOINT fptSrc;
	};
	
	union
	{
		POINT ptDest;
		FPOINT fptDest;
	};
} MAP_RENDERINFO;


/* Constants. */
#define PALETTE_16COLORS_OFFSET 32
#define PALETTE_16COLORSDIMMED_OFFSET 48

#define RENDERER_FONT_HEIGHT_PX		9
#define RENDERER_FONT_WIDTH_PX		5
#define RENDERER_FONT_FACE			TEXT("Tahoma")
#define RENDERER_FONT_NUM_GLYPHS	96
#define RENDERER_FONT_FIRST_GLYPH	32

#define RENDERER_PRINTF_BUF_SIZE	64



/* Globals. */
extern RGBQUAD g_rgbqPalette[];

/* Prototypes. */
BOOL InitialiseRenderer(void);
void ShutdownRenderer(void);
void LoadUserPalette(void);
void RedrawMap(MAP *lpmap, MAP_RENDERINFO *lpmri);
void SetZoom(MAPVIEW *lpmapview, float fZoom);
BOOL InitGL(HDC hdc);
void RendererMakeDCCurrent(HDC hdc);
GLuint LoadThingCircleTexture(GLvoid);
GLvoid ReSizeGLScene(GLsizei cx, GLsizei cy);
void __fastcall Render_LineF(float x1, float y1, float x2, float y2, BYTE byColour);
void __fastcall Render_SquareF(float xCentre, float yCentre, float fHalfSize, BYTE byColour);
void __fastcall Render_Rect(float x1, float y1, float x2, float y2, BYTE byColour);
void __fastcall Render_OutlineRect(float x1, float y1, float x2, float y2, BYTE byColour);
void __fastcall Render_ThingPalettedF(float xCentre, float yCentre, int iAngle, BYTE byPalColour, float fSize, float fZoom, GLuint uiThingTex);
void __fastcall Render_ThingF(float xCentre, float yCentre, int iAngle, int iColour, float fSize, float fZoom, GLuint uiThingTex);

void __fastcall Render_BoxF(int xCentre, int yCentre, int iRadius, BYTE byColour);
void __fastcall Render_CircleF(float xCentre, float yCentre, float fRadius, int iColour, GLuint uiThingTex);
void __fastcall Render_BitmapF(BYTE* bitmap, int width, int height, int sx, int sy, int sw, int sh, int tx, int ty, BYTE c1, BYTE c2);
void __fastcall Render_ScaledBitmapF(BYTE* bitmap, int width, int height, int sx, int sy, int sw, int sh, int tx, int ty, BYTE c1, BYTE c2);

void ZoomToRect(MAPVIEW *lpmapview, RECT *lprc, int iBorderPercentage);
void CentreViewAt(MAPVIEW *lpmapview, int x, int y);

void Render_Printf(float x, float y, BYTE byColour, LPCTSTR szFormat, ...);

int DimmedColour(int iColour);


#ifdef __cplusplus
}
#endif

#endif
