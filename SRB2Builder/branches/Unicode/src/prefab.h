#ifndef __SRB2B_PREFAB__
#define __SRB2B_PREFAB__

#include "map.h"


#define FreePrefab(lpprefab) ProcHeapFree(lpprefab)

typedef struct _PREFAB PREFAB;

PREFAB* CreatePrefabFromSelection(MAP *lpmap);
void InsertPrefab(MAP *lpmap, PREFAB *lpprefab);
void RegisterPrefabClipboardFormat(void);
BOOL ClipboardContainsPrefab(void);
int CopyPrefabToClipboard(PREFAB *lpprefab);
PREFAB* PastePrefabFromClipboard(void);

#endif
