#ifndef __SRB2B_WAD__
#define __SRB2B_WAD__

#include <windows.h>

#include "general.h"
#include "config.h"


typedef struct _WAD WAD;


#define CCH_LUMPNAME			8


WAD *CreateWad(void);
void FreeWad(WAD *lpwad);
WAD *OpenWad(LPCTSTR szFileName);
long CreateLump(WAD *lpwad, LPCSTR sLumpName, long iLumpIndex);
BOOL SetLump(WAD *lpwad, long iLumpIndex, const BYTE *lpbyData, long cb);
long GetLumpIndex(WAD *lpwad, long iFirstLump, long iLastLump, LPCSTR sLumpName);
long GetLumpCount(WAD *lpwad);
BOOL GetLumpNameA(WAD *lpwad, long iLumpIndex, LPSTR sLumpName);
long GetLumpLength(WAD *lpwad, long iLumpIndex);
long GetLump(WAD *lpwad, long iLumpIndex, BYTE *lpbyBuffer, long cbBuffer);
BOOL DeleteMultipleLumps(WAD *lpwad, long iFirstLump, long iLastLump);
BOOL WriteWad(WAD *lpwad, LPCTSTR szFileName, CONFIG *lpcfgChangedMaps);
WAD* DuplicateWadToMemory(WAD *lpwad);
void SetLumpName(WAD *lpwad, long iLumpIndex, LPCSTR sLumpName);


#ifdef UNICODE
static __inline BOOL GetLumpName(WAD *lpwad, long iLumpIndex, LPWSTR sLumpName)
{
	char sLumpNameA[CCH_LUMPNAME];

	if(GetLumpNameA(lpwad, iLumpIndex, sLumpNameA))
	{
		MultiByteToWideChar(CP_ACP, MB_PRECOMPOSED, sLumpNameA, NUM_ELEMENTS(sLumpNameA), sLumpName, CCH_LUMPNAME);
		return TRUE;
	}

	return FALSE;
}
#else
#define GetLumpName GetLumpNameA
#endif


#endif
