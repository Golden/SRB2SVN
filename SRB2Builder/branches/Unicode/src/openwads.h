#ifndef __SRB2B_OPENWADS__
#define __SRB2B_OPENWADS__

#include "wad.h"
#include "config.h"

/* Types. */
typedef struct _OPENWAD OPENWAD;

struct _OPENWAD
{
	WAD				*lpwad;
	unsigned int	uiReferenceCount;
	int				iID;
	LPTSTR			szFilename;
	BOOL			bNonMapDataChanged;
	CONFIG			*lpcfgWad;
	OPENWAD			*lpowNext;
};


/* Function prototypes. */
int LoadWad(LPCTSTR szPath);
void EnumChildWindowsByWad(int iID, WNDENUMPROC lpEnumFunc, LPARAM lParam);
int CountWadWindows(int iWadID);
int ReleaseWad(int iID);
void InitOpenWadsList(void);
int NewWad(void);
WAD* GetWad(int iWad);
void IncrementWadReferenceCount(int iWad);
int GetWadFilename(int iWad, LPTSTR szFilename, WORD cchBuffer);
int SaveWadAs(int iWad, LPTSTR szFileName, CONFIG *lpcfgChangedMaps);
BOOL WadIsNewByID(int iWad);
int BuildNodesExternal(LPCTSTR szFilename, LPCTSTR szLumpname);
BOOL BuildNodesExternalFromConfig(CONFIG *lpcfgNode, void *lpvFilename);
int SetWadNonMapModified(int iWad);
void DeleteMapFromOpenWad(int iWad, LPCSTR szLumpname);
CONFIG* GetWadOptions(int iWad);
BOOL CloseWad(int iWad);


/* Inline functions. */
static __inline int SaveWad(int iWad, CONFIG *lpcfgChangedMaps)
{
	return SaveWadAs(iWad, NULL, lpcfgChangedMaps);
}

#endif	/* __SRB2B_OPENWADS__ */
