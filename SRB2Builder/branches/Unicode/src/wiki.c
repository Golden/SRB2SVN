#include <windows.h>
#include <tchar.h>
#include <stdio.h>

#include "general.h"
#include "config.h"
#include "options.h"
#include "wiki.h"
#include "mapconfig.h"

#include "win/mdiframe.h"

#include "../res/resource.h"


static void WikiQuery(LPCTSTR szQuery);


/* WikiQuery
 *   Opens the Wiki at the URL specified in the config file, replacing the
 *   format field in the format string with the supplied query.
 *
 * Parameters:
 *   LPCTSTR	szQuery		Query string.
 *
 * Return value: None.
 */
static void WikiQuery(LPCTSTR szQuery)
{
	CONFIG *lpcfgWiki = ConfigGetSubsection(g_lpcfgMain, OPT_WIKI);

	UINT cchQueryFmt = ConfigGetStringLength(lpcfgWiki, WIKICFG_QUERYURLFMT);
	LPTSTR szQueryFmt = ProcHeapAlloc((cchQueryFmt + 1) * sizeof(TCHAR));

	/* Make a string big enough for the expanded URL. */
	UINT cchURL = cchQueryFmt + _tcslen(szQuery) + 1;
	LPTSTR szURL = ProcHeapAlloc(cchURL * sizeof(TCHAR));

	/* Get the format string. */
	ConfigGetString(lpcfgWiki, WIKICFG_QUERYURLFMT, szQueryFmt, cchQueryFmt + 1);

	/* Expand the query placeholder. */
	_sntprintf(szURL, cchURL, szQueryFmt, szQuery);

	/* Attempt to open the URL. */
	if(!(*szURL) || (int)ShellExecute(NULL, NULL, szURL, NULL, NULL, SW_SHOW) <= 32)
		MessageBoxFromStringTable(g_hwndMain, IDS_ERROR_WIKI, MB_ICONERROR);

	/* Free buffers. */
	ProcHeapFree(szQueryFmt);
	ProcHeapFree(szURL);
}


/* WikiMainPage
 *   Opens the Wiki's main page.
 *
 * Parameters:
 *   None.
 *
 * Return value: None.
 */
void WikiMainPage(void)
{
	CONFIG *lpcfgWiki = ConfigGetSubsection(g_lpcfgMain, OPT_WIKI);
	UINT cchBuffer = ConfigGetStringLength(lpcfgWiki, WIKICFG_MAINPAGE) + 1;
	LPTSTR szMainPage = ProcHeapAlloc(cchBuffer * sizeof(TCHAR));
	ConfigGetString(lpcfgWiki, WIKICFG_MAINPAGE, szMainPage, cchBuffer);
	WikiQuery(szMainPage);
	ProcHeapFree(szMainPage);
}


/* WikiEffectQuery
 *   Searches the Wiki for a Linedef, Sector or Thing type.
 *
 * Parameters:
 *   CONFIG*			lpcfgMap		Map config.
 *   ENUM_WIKIQUERIES	wikiquery		Query type: ld, sec, thing.
 *   int				iIndex			Effect value.
 *
 * Return value: None.
 */
void WikiEffectQuery(CONFIG *lpcfgMap, ENUM_WIKIQUERIES wikiquery, int iIndex)
{
	CONFIG *lpcfgWiki = ConfigGetSubsection(lpcfgMap, MAPCFG_WIKI);

	if(lpcfgWiki)
	{
		LPCTSTR szFormatName = NULL;
		DWORD cchFormat, cchQuery;
		LPTSTR szFormat;
		LPTSTR szQuery;

		/* What sort of thing are we looking up? */
		switch(wikiquery)
		{
			case WIKIQUERY_LDEFFECT:	szFormatName = WIKI_LDEFFECT;	break;
			case WIKIQUERY_SECEFFECT:	szFormatName = WIKI_SECEFFECT;	break;
			case WIKIQUERY_THING:		szFormatName = WIKI_THING;		break;
		}

		/* Get the appropriate format string. */
		cchFormat = ConfigGetStringLength(lpcfgWiki, szFormatName) + 1;
		if(cchFormat == 0) return;
		szFormat = ProcHeapAlloc(cchFormat * sizeof(TCHAR));
		ConfigGetString(lpcfgWiki, szFormatName, szFormat, cchFormat);

		/* Fill out the format field with the index of the item we're interested
		 * in.
		 */
		cchQuery = cchFormat + 11;
		szQuery = ProcHeapAlloc(cchQuery * sizeof(TCHAR));
		_sntprintf(szQuery, cchQuery, szFormat, iIndex);
		
		WikiQuery(szQuery);

		/* Done! Tidy up. */
		ProcHeapFree(szFormat);
		ProcHeapFree(szQuery);
	}
}
