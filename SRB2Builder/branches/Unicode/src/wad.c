#include <windows.h>
#include <string.h>

#include "general.h"
#include "wad.h"
#include "config.h"
#include "openwads.h"


/* Macros. */

#define INIT_NUM_LUMPS			16
#define INIT_LUMPBUFFER_SIZE	16384
#define WADDIR_OFFSET_OFFSET	8

#define IWAD_MAGIC				('I' | ('W' << 8) | ('A' << 16) | ('D' << 24))
#define PWAD_MAGIC				('P' | ('W' << 8) | ('A' << 16) | ('D' << 24))

#define WAD_TEMP_PREFIX			TEXT("wad")

#define DeleteLump(lpwad, i)	(DeleteMultipleLumps(lpwad, i, i))


/* Types. */

/* Lumps can either be in memory, if we've messed with them, or unchanged on
 * disc.
 */
typedef enum _ENUM_LUMP_TYPE
{
	LT_MEMORY,
	LT_FILE
} ENUM_LUMP_TYPE;

/* This structure is used to represent lumps. */
typedef struct _LUMP
{
	CHAR			sLumpname[8];
	ENUM_LUMP_TYPE	lumptype;
	long			cb;

	union
	{
		BYTE		*lpbyData;
		long		iFileOffset;
	};
} LUMP;

/* The main wad structure. */
struct _WAD
{
	LUMP	*lplumps;
	HANDLE	hFile;
	long	iLumpCount, iLumpsAllocated;
};

/* The twelve bytes that begin every wadfile. */
typedef struct _WADFILEHEADER
{
	DWORD	dwMagic;
	long	iLumps;
	long	iDirOffset;
} WADFILEHEADER;

/* The directory of all lumps in a wad file. */
typedef struct _WADDIRECTORY
{
	long	iLumpOffset;
	long	cb;
	char	sName[CCH_LUMPNAME];
} WADDIRECTORY;



/* Static prototypes. */
static __inline void SetLumpFromFile(WAD *lpwad, long iLumpIndex, long iOffset, long cb);



/* CreateWad
 *   Allocates a new wad structure.
 *
 * Parameters: None.
 *
 * Return value: WAD*
 *   Pointer to the new structure, or NULL on error.
 *
 * Remarks:
 *   To load a wad from disc, use OpenWad.
 */
WAD *CreateWad(void)
{
	WAD *lpwad = (WAD*)ProcHeapAlloc(sizeof(WAD));

	/* No file associated with this wad. */
	lpwad->hFile = INVALID_HANDLE_VALUE;

	/* We're empty. */
	lpwad->iLumpCount = 0;

	/* Allocate enough room for INIT_NUM_LUMPS lumps. */
	lpwad->iLumpsAllocated = INIT_NUM_LUMPS;
	lpwad->lplumps = ProcHeapAlloc(lpwad->iLumpsAllocated * sizeof(LUMP));

	return lpwad;
}


/* FreeWAD
 *   Frees a wad structure.
 *
 * Parameters:
 *   WAD*	lpwad		Pointer to wad structure to free.
 *
 * Return value: None.
 *
 * Remarks:
 *   Frees all data associated with the wad and closes files.
 */
void FreeWad(WAD *lpwad)
{
	long i;

	/* Free all in-memory lump data. */
	for(i = 0; i < lpwad->iLumpCount; i++)
		if(lpwad->lplumps[i].lumptype == LT_MEMORY)
			ProcHeapFree(lpwad->lplumps[i].lpbyData);

	/* Free lump metadata. */
	ProcHeapFree(lpwad->lplumps);

	/* Close file. */
	if(lpwad->hFile != INVALID_HANDLE_VALUE)
		CloseHandle(lpwad->hFile);

	/* Free wad structure. */
	ProcHeapFree(lpwad);
}


/* OpenWad
 *   Opens a wad from disc.
 *
 * Parameters:
 *   LPCTSTR	szFileName	Filename.
 *
 * Return value: WAD*
 *   Pointer to new wad structure on success; NULL on error.
 *
 * Remarks:
 *   The file is kept open for GENERIC_READ access.
 */
WAD *OpenWad(LPCTSTR szFileName)
{
	WAD *lpwad = CreateWad();
	WADFILEHEADER wadfileheader;
	DWORD dwBytesRead;
	DWORD cbDirectory;
	WADDIRECTORY *lpwaddirectory = NULL;
	long i;

	/* Open the file. */
	lpwad->hFile = CreateFile(szFileName, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_ARCHIVE, NULL);

	if(lpwad->hFile == INVALID_HANDLE_VALUE)
	{
		FreeWad(lpwad);
		return NULL;
	}

	/* Read the header. */
	ReadFile(lpwad->hFile, &wadfileheader, sizeof(wadfileheader), &dwBytesRead, NULL);

	/* Sanity check. */
	if(dwBytesRead != sizeof(wadfileheader) ||
		(wadfileheader.dwMagic != IWAD_MAGIC && wadfileheader.dwMagic != PWAD_MAGIC) ||
		wadfileheader.iLumps < 0)
	{
		FreeWad(lpwad);
		return NULL;
	}

	/* Seek to the directory. */
	if(SetFilePointer(lpwad->hFile, wadfileheader.iDirOffset, NULL, FILE_BEGIN) == 0xFFFFFFFF)
	{
		FreeWad(lpwad);
		return NULL;
	}

	/* Get the directory. */
	cbDirectory = wadfileheader.iLumps * sizeof(WADDIRECTORY);
	lpwaddirectory = (WADDIRECTORY*)ProcHeapAlloc(cbDirectory);
	ReadFile(lpwad->hFile, lpwaddirectory, cbDirectory, &dwBytesRead, NULL);

	/* Directory too short? */
	if(dwBytesRead != cbDirectory)
	{
		ProcHeapFree(lpwaddirectory);
		FreeWad(lpwad);
		return NULL;
	}

	/* Build the array of lump metadata. */
	for(i = 0; i < wadfileheader.iLumps; i++)
	{
		/* Create new lump at end. */
		long iNewLump = CreateLump(lpwad, lpwaddirectory[i].sName, -1);

		/* It's a LT_FILE. */
		SetLumpFromFile(lpwad, iNewLump, lpwaddirectory[i].iLumpOffset, lpwaddirectory[i].cb);
	}

	/* We're finished! Clean up. */
	ProcHeapFree(lpwaddirectory);

	return lpwad;
}


/* CreateLump
 *   Creates a new, empty lump.
 *
 * Parameters:
 *   WAD*		lpwad		Wad structure.
 *   LPCSTR		sLumpName	Lump name.
 *   long		iLumpIndex	Index of new lump, or negative to create at end.
 *
 * Return value: long
 *   Index of new lump.
 *
 * Remarks:
 *   Only CCH_LUMPNAME characters of sLumpName are used. It need not be zero-
 *   padded, however.
 */
long CreateLump(WAD *lpwad, LPCSTR sLumpName, long iLumpIndex)
{
	/* If iLumpIndex is negative, we make the new lump at the end. */
	if(iLumpIndex < 0) iLumpIndex = lpwad->iLumpCount;

	/* We have an extra lump now. */
	lpwad->iLumpCount++;

	/* We might need to allocate more memory for the meta-data. */
	if(lpwad->iLumpCount > lpwad->iLumpsAllocated)
	{
		lpwad->iLumpsAllocated <<= 1;
		lpwad->lplumps = ProcHeapReAlloc(lpwad->lplumps, lpwad->iLumpsAllocated * sizeof(LUMP));
	}

	/* Move all the following lumps down a spot. */
	if(iLumpIndex < lpwad->iLumpCount - 1)
		MoveMemory(&lpwad->lplumps[iLumpIndex + 1], &lpwad->lplumps[iLumpIndex], (lpwad->iLumpCount - 1 - iLumpIndex) * sizeof(LUMP));

	/* Set the lump's name. */
	SetLumpName(lpwad, iLumpIndex, sLumpName);

	/* Assume it's an empty memory lump. */
	lpwad->lplumps[iLumpIndex].lumptype = LT_MEMORY;
	lpwad->lplumps[iLumpIndex].lpbyData = NULL;
	lpwad->lplumps[iLumpIndex].cb = 0;

	/* Finished! Return the index of the new lump. */
	return iLumpIndex;
}


/* SetLumpFromFile
 *   Sets a lump to point to within the loaded file.
 *
 * Parameters:
 *   WAD*		lpwad		Wad structure.
 *   long		iLumpIndex	Index of lump.
 *   long		iOffset		Offset into file.
 *   long		cb			Length of lump.
 *
 * Return value: None.
 *
 * Remarks:
 *   This should only ever be needed when loading a wad from a file.
 */
static __inline void SetLumpFromFile(WAD *lpwad, long iLumpIndex, long iOffset, long cb)
{
	lpwad->lplumps[iLumpIndex].lumptype = LT_FILE;
	lpwad->lplumps[iLumpIndex].iFileOffset = iOffset;
	lpwad->lplumps[iLumpIndex].cb = cb;
}


/* SetLump
 *   Sets a lump's data from a buffer.
 *
 * Parameters:
 *   WAD*			lpwad		Wad structure.
 *   long			iLumpIndex	Index of lump.
 *   const BYTE*	lpbyData	Data for lump.
 *   long			cb			Length of lump.
 *
 * Return value: None.
 *
 * Remarks:
 *   A new buffer is created and the data from the one passed in copied into it.
 */
BOOL SetLump(WAD *lpwad, long iLumpIndex, const BYTE *lpbyData, long cb)
{
	if(iLumpIndex < 0 || iLumpIndex >= lpwad->iLumpCount) return FALSE;

	/* Set the type and size. */
	lpwad->lplumps[iLumpIndex].lumptype = LT_MEMORY;
	lpwad->lplumps[iLumpIndex].cb = cb;

	/* Make a buffer into which to copy the data, and copy it. */
	lpwad->lplumps[iLumpIndex].lpbyData = (BYTE*)ProcHeapAlloc(cb);
	CopyMemory(lpwad->lplumps[iLumpIndex].lpbyData, lpbyData, cb);

	return TRUE;
}


/* GetLumpIndex
 *   Finds the index of a lump by name.
 *
 * Parameters:
 *   WAD*		lpwad		Wad structure.
 *   long		iFirstLump	Index of lump to begin searching from.
 *   long		iLastLump	Index of lump to search to, or < 0 to search to end.
 *   LPCSTR		sLumpName	Name of lump to search for.
 *
 * Return value: long
 *   Index of lump, or negative if no matches found.
 */
long GetLumpIndex(WAD *lpwad, long iFirstLump, long iLastLump, LPCSTR sLumpName)
{
	long i;

	/* If the iLastLump is negative, search to the end of the wad. */
	if(iLastLump < 0) iLastLump = lpwad->iLumpCount - 1;

	/* Find the lump. */
	for(i = iFirstLump; i <= iLastLump; i++)
		if(strncmp(sLumpName, lpwad->lplumps[i].sLumpname, CCH_LUMPNAME) == 0)
			return i;

	/* Not found! */
	return -1;
}


/* GetLumpCount
 *   Returns the number of lumps in a wad.
 *
 * Parameters:
 *   WAD*		lpwad		Wad structure.
 *
 * Return value: long
 *   Number of lumps.
 */
long GetLumpCount(WAD *lpwad)
{
	return lpwad->iLumpCount;
}


/* GetLumpName
 *   Gets the name of a lump given its index.
 *
 * Parameters:
 *   WAD*	lpwad		Wad structure.
 *   long	iLumpIndex	Index of lump.
 *   LPSTR	sLumpName	Buffer in which to return lumpname.
 *
 * Return value: BOOL
 *   TRUE if successful; FALSE if not.
 *
 * Remarks:
 *   sLumpName must be at least CCH_LUMPNAME characters long. It will not
 *   necessarily be NULL-terminated.
 */
BOOL GetLumpNameA(WAD *lpwad, long iLumpIndex, LPSTR sLumpName)
{
	if(iLumpIndex < lpwad->iLumpCount && iLumpIndex >= 0)
	{
		CopyMemory(sLumpName, lpwad->lplumps[iLumpIndex].sLumpname, CCH_LUMPNAME);
		return TRUE;
	}

	/* Bad index. */
	return FALSE;
}


/* GetLumpLength
 *   Returns the length of a lump.
 *
 * Parameters:
 *   WAD*		lpwad		Wad structure.
 *   long		iLumpIndex	Index of lump.
 *
 * Return value: long
 *   Lenth of lump, or negative if no such lump exists (i.e. bad index).
 */
long GetLumpLength(WAD *lpwad, long iLumpIndex)
{
	if(iLumpIndex < lpwad->iLumpCount && iLumpIndex >= 0)
		return lpwad->lplumps[iLumpIndex].cb;

	/* Bad index. */
	return -1;
}


/* GetLump
 *   Gets a lump's data.
 *
 * Parameters:
 *   WAD*		lpwad		Wad structure.
 *   long		iLumpIndex	Index of lump.
 *   BYTE*		lpbyBuffer	Buffer into which to copy the lump's data.
 *   lpng		cbBuffer	Length of the buffer.
 *
 * Return value: long
 *   Number of bytes copied into the buffer.
 */
long GetLump(WAD *lpwad, long iLumpIndex, BYTE *lpbyBuffer, long cbBuffer)
{
	long cbCopied;
	LUMP *lplump;

	/* Bad index? */
	if(iLumpIndex >= lpwad->iLumpCount || iLumpIndex < 0) return -1;

	/* Get a pointer to our lump. */
	lplump = &lpwad->lplumps[iLumpIndex];

	/* We can copy no more than the length of the lump or the size of the
	 * buffer.
	 */
	cbCopied = min(cbBuffer, lplump->cb);

	/* Are we in memory or in the file? */
	switch(lplump->lumptype)
	{
	case LT_MEMORY:
		/* Copy the data from memory into the buffer. */
		CopyMemory(lpbyBuffer, lplump->lpbyData, cbCopied);
		break;

	case LT_FILE:
		/* Seek to where we want to be, and the read from the file to the
		 * buffer.
		 */
		SetFilePointer(lpwad->hFile, lplump->iFileOffset, NULL, FILE_BEGIN);
		ReadFile(lpwad->hFile, lpbyBuffer, cbCopied, &cbCopied, NULL);
		break;
	}

	/* Return the number of bytes copied into the buffer. */
	return cbCopied;
}


/* DeleteMultipleLumps
 *   Deletes multiple contiguous lumps.
 *
 * Parameters:
 *   WAD*	lpwad		Wad structure.
 *   long	iFirstLump	Index of first lump to delete.
 *   long	iLastLump	Index of last lump to delete.
 *
 * Return value: BOOL
 *   TRUE if successful; FALSE if not.
 */
BOOL DeleteMultipleLumps(WAD *lpwad, long iFirstLump, long iLastLump)
{
	long i;

	/* Bad index? */
	if(iFirstLump >= lpwad->iLumpCount || iLastLump >= lpwad->iLumpCount || iFirstLump < 0 || iLastLump < 0)
		return FALSE;

	/* Free the memory for each memory lump. */
	for(i = iFirstLump; i <= iLastLump; i++)
		if(lpwad->lplumps[i].lumptype == LT_MEMORY && lpwad->lplumps[i].lpbyData)
			ProcHeapFree(lpwad->lplumps[i].lpbyData);

	/* Move the lumps below it, if there be any, up into the vacated space. */
	if(iLastLump < lpwad->iLumpCount)
		MoveMemory(&lpwad->lplumps[iFirstLump], &lpwad->lplumps[iLastLump + 1], (lpwad->iLumpCount - iLastLump) * sizeof(LUMP));

	/* Reduce lump count. */
	lpwad->iLumpCount -= iLastLump - iFirstLump + 1;

	/* Success. */
	return TRUE;
}


/* WriteWad
 *   Writes a wad to disc.
 *
 * Parameters:
 *   WAD*		lpwad				Wad structure.
 *   LPCTSTR	szFileName			Filename.
 *   CONFIG*	lpcfgChangedMaps	Config containing changed lumpnames on which
 *									to invoke the external nodebuilder. May be
 *									NULL if not required.
 *
 * Return value: BOOL
 *   TRUE if successful; FALSE if not.
 *
 * Remarks:
 *   Adjusts the wad structure to refer to the new file.
 */
BOOL WriteWad(WAD *lpwad, LPCTSTR szFileName, CONFIG *lpcfgChangedMaps)
{
	LPTSTR szTempPath, szTempFileName;
	DWORD cchTempPath, cb;
	HANDLE hTempFile;
	DWORD dwMagic;
	WADDIRECTORY *lpwaddirectory;
	long i, iOffset;
	BYTE *lpbyBuffer;
	long cbBuffer;

	/* If there's no filename, we can't go on. */
	if(!szFileName) return FALSE;

	/* Get the temporary file directory. */
	cchTempPath = GetTempPath(0, NULL);
	szTempPath = ProcHeapAlloc((cchTempPath + 1) * sizeof(TCHAR));
	GetTempPath(cchTempPath, szTempPath);

	/* Create a temporary file. */
	szTempFileName = ProcHeapAlloc(MAX_PATH * sizeof(TCHAR));
	GetTempFileName(szTempPath, WAD_TEMP_PREFIX, 0, szTempFileName);
	hTempFile = CreateFile(szTempFileName, GENERIC_WRITE, 0, NULL, OPEN_ALWAYS, FILE_ATTRIBUTE_ARCHIVE, NULL);

	if(!hTempFile)
	{
		ProcHeapFree(szTempFileName);
		return FALSE;
	}

	/* Don't need the temp *path* any more. Still need the filename, though. */
	ProcHeapFree(szTempPath);

	/* If we have an existing file, get the magic number from there; otherwise,
	 * assume we're a PWAD.
	 */
	if(lpwad->hFile != INVALID_HANDLE_VALUE)
	{
		SetFilePointer(lpwad->hFile, 0, NULL, FILE_BEGIN);

		if(!ReadFile(lpwad->hFile, &dwMagic, sizeof(DWORD), &cb, NULL))
		{
			ProcHeapFree(szTempFileName);
			CloseHandle(hTempFile);
			return FALSE;
		}
	}
	else dwMagic = PWAD_MAGIC;

	/* Write the magic number and number of lumps. */
	if(!WriteFile(hTempFile, &dwMagic, sizeof(DWORD), &cb, NULL) ||
		!WriteFile(hTempFile, &lpwad->iLumpCount, sizeof(long), &cb, NULL))
	{
		ProcHeapFree(szTempFileName);
		CloseHandle(hTempFile);
		return FALSE;
	}

	/* Skip the directory offset for now: we'll write that when we know it. */
	SetFilePointer(hTempFile, sizeof(long), NULL, FILE_CURRENT);

	/* Create a directory structure, which we'll fill as we go. */
	lpwaddirectory = (WADDIRECTORY*)ProcHeapAlloc(lpwad->iLumpCount * sizeof(WADDIRECTORY));

	/* We begin writing lumps immediately after the header. */
	iOffset = sizeof(WADFILEHEADER);

	/* Create a buffer for reading lumps. */
	cbBuffer = INIT_LUMPBUFFER_SIZE;
	lpbyBuffer = (BYTE*)ProcHeapAlloc(cbBuffer);

	/* Write each lump in turn. */
	for(i = 0; i < lpwad->iLumpCount; i++)
	{
		LUMP *lplump = &lpwad->lplumps[i];

		/* Fill its directory entry. */
		lpwaddirectory[i].iLumpOffset = iOffset;
		lpwaddirectory[i].cb = lplump->cb;
		CopyMemory(lpwaddirectory[i].sName, lplump->sLumpname, CCH_LUMPNAME);

		/* Don't write zero-length lumps. */
		if(lplump->cb > 0)
		{
			/* Determine which sort of lump we're dealing with so we know how to
			 * write it.
			 */
			switch(lplump->lumptype)
			{
			case LT_MEMORY:
				/* If the lump's already in memory, we can write it straight
				 * away.
				 */
				if(!WriteFile(hTempFile, lplump->lpbyData, lplump->cb, &cb, NULL))
				{
					ProcHeapFree(szTempFileName);
					ProcHeapFree(lpbyBuffer);
					CloseHandle(hTempFile);
					return FALSE;
				}

				break;

			case LT_FILE:
				/* If the lump is still in the file, we read it into memory,
				 * extending the buffer first if necessary.
				 */
				if(cbBuffer < lplump->cb)
				{
					cbBuffer = lplump->cb;
					ProcHeapFree(lpbyBuffer);
					lpbyBuffer = (BYTE*)ProcHeapAlloc(cbBuffer);
				}

				/* Read it from the old file and write it to the new. */

				SetFilePointer(lpwad->hFile, lplump->iFileOffset, NULL, FILE_BEGIN);
				if(!ReadFile(lpwad->hFile, lpbyBuffer, lplump->cb, &cb, NULL) ||
					!WriteFile(hTempFile, lpbyBuffer, lplump->cb, &cb, NULL))
				{
					ProcHeapFree(szTempFileName);
					ProcHeapFree(lpbyBuffer);
					CloseHandle(hTempFile);
					return FALSE;
				}
			}

			/* Advance the offset for next time. */
			iOffset += lplump->cb;
		}
	}

	/* We're finished with the buffer now. */
	ProcHeapFree(lpbyBuffer);

	/* Write the directory. */
	if(!WriteFile(hTempFile, lpwaddirectory, lpwad->iLumpCount * sizeof(WADDIRECTORY), &cb, NULL))
	{
		ProcHeapFree(szTempFileName);
		CloseHandle(hTempFile);
		return FALSE;
	}

	/* Write the directory offset. */
	SetFilePointer(hTempFile, WADDIR_OFFSET_OFFSET, NULL, FILE_BEGIN);
	if(!WriteFile(hTempFile, &iOffset, sizeof(long), &cb, NULL))
	{
		ProcHeapFree(szTempFileName);
		CloseHandle(hTempFile);
		return FALSE;
	}

	/* We're now substantially finished. All that remains is housework. Also, we
	 * can't fail after this point, so we can do things that would break the
	 * state if we did them and subsequently failed.
	 */

	/* Update stored offsets to refer to new file. */
	for(i = 0; i < lpwad->iLumpCount; i++)
		if(lpwad->lplumps[i].lumptype == LT_FILE)
			lpwad->lplumps[i].iFileOffset = lpwaddirectory[i].iLumpOffset;

	ProcHeapFree(lpwaddirectory);

	/* Finished writing to the temp file. */
	CloseHandle(hTempFile);

	/* If we had a previous file, close it and delete it. */
	if(lpwad->hFile != INVALID_HANDLE_VALUE)
	{
		CloseHandle(lpwad->hFile);

		/* In case we fail before opening the file again. */
		lpwad->hFile = INVALID_HANDLE_VALUE;
	}

	/* If the new file already exists, we need to delete it. If it doesn't
	 * exist, then this isn't going to do any harm. So why check?
	 */
	DeleteFile(szFileName);

	/* Move the temporary file to the new location. */
	if(!MoveFile(szTempFileName, szFileName)) return FALSE;
	ProcHeapFree(szTempFileName);

	/* Build nodes if desired. */
	if(lpcfgChangedMaps)
		ConfigIterate(lpcfgChangedMaps, BuildNodesExternalFromConfig, (void*)szFileName);

	/* Reopen the file for reading from its new location. */
	lpwad->hFile = CreateFile(szFileName, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_ARCHIVE, NULL);

	/* Finished! One last check. */
	return (lpwad->hFile != INVALID_HANDLE_VALUE);
}


/* DuplicateWadToMemory
 *   Creates a new wad with identical contents to the specified wad. All the new
 *   wad's lumps are stored in memory.
 *
 * Parameters:
 *   WAD*		lpwad		Existing wad.
 *
 * Return value: WAD*
 *   New wad.
 *
 * Remarks:
 *   The new wad is not associated with the file (if any) of the old.
 */
WAD* DuplicateWadToMemory(WAD *lpwad)
{
	WAD *lpwadNew = CreateWad();
	long iLumps = GetLumpCount(lpwad);
	long i;

	/* Repeat for each lump. */
	for(i = 0; i < iLumps; i++)
	{
		CHAR sLumpName[CCH_LUMPNAME];
		long cbLump = GetLumpLength(lpwad, i);

		/* Create the lump in the new wad. */
		GetLumpNameA(lpwad, i, sLumpName);
		CreateLump(lpwadNew, sLumpName, i);

		if(cbLump > 0)
		{
			BYTE *lpbyLump = ProcHeapAlloc(cbLump);

			/* Get the lump from the source wad. */
			GetLump(lpwad, i, lpbyLump, cbLump);

			/* Set it as a memory lump in the new wad. */
			SetLump(lpwadNew, i, lpbyLump, cbLump);

			ProcHeapFree(lpbyLump);
		}
	}
	
	return lpwadNew;
}


/* SetLumpName
 *   Sets a lump's name.
 *
 * Parameters:
 *   WAD*		lpwad		Wad structure.
 *   long		iLumpIndex	Index of lump.
 *   LPCSTR		sLumpName	Lump name.
 *
 * Return value: None.
 */
void SetLumpName(WAD *lpwad, long iLumpIndex, LPCSTR sLumpName)
{
	LPSTR sInName;

	/* Set the lump's name. */
	ZeroMemory(lpwad->lplumps[iLumpIndex].sLumpname, CCH_LUMPNAME);
	strncpy(lpwad->lplumps[iLumpIndex].sLumpname, sLumpName, CCH_LUMPNAME);

	/* Zero-pad the lumpname. */
	sInName = lpwad->lplumps[iLumpIndex].sLumpname;
	while(*sInName) sInName++;
	while(sInName - lpwad->lplumps[iLumpIndex].sLumpname < CCH_LUMPNAME) *sInName++ = '\0';
}
