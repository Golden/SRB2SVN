#ifndef __SRB2B_WADLIST__
#define __SRB2B_WADLIST__

/* Function prototypes. */
BOOL CreateWadListWindow(HWND hwndParent);
void DestroyWadListWindow(void);
void AddToWadList(int iWad, CONFIG *lpcfgMap);
void RemoveFromWadList(int iWad);
void UpdateWadTitleInList(int iWad);
int RenameMapInWadList(int iWad, LPCSTR szOldName, LPCSTR szNewName);
void AddMapToTree(int iWad, LPCSTR szLumpname);

#endif
