#ifndef __SRB2B_TOOLBAR__
#define __SRB2B_TOOLBAR__

BOOL CreateToolbarWindow(HWND hwndParent);
void DestroyToolbarWindow(void);
HWND GetToolbarWindow(void);

#endif
