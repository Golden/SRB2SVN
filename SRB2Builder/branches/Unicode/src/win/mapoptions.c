#include <windows.h>
#include <commctrl.h>

#include "../general.h"
#include "../config.h"
#include "../mapconfig.h"
#include "../testing.h"
#include "../cdlgwrapper.h"
#include "../texture.h"
#include "../wadopts.h"

#include "mdiframe.h"
#include "mapoptions.h"
#include "editdlg.h"
#include "texbrowser.h"
#include "gendlg.h"

#include "../../res/resource.h"


#define MAPOPT_NUMPAGES 2


static BOOL CALLBACK MapOptCfgPropProc(HWND hwndPropPage, UINT uiMsg, WPARAM wParam, LPARAM lParam);
static BOOL CALLBACK MapOptDefaultsPropProc(HWND hwndPropPage, UINT uiMsg, WPARAM wParam, LPARAM lParam);



/* ShowMapProperties
 *   Shows the Properties dialogue for the selection.
 *
 * Parameters:
 *   MAPPOPTPDATA*	lpmapoptppdata		Current properties.
 *
 * Return value: BOOL
 *   TRUE if the user OKed to exit the dialogue; FALSE if cancelled.
 *
 * Remarks:
 *   All the processing is done by the property sheet pages. This function just
 *   shows the dialogue box.
 */
BOOL ShowMapProperties(MAPOPTPPDATA *lpmapoptppdata)
{
	PROPSHEETHEADER		psh;
	PROPSHEETPAGE		psp[MAPOPT_NUMPAGES];
	int					i;


	/* Build the info for the individual pages. */

	psp[0].pfnDlgProc = MapOptCfgPropProc;
	psp[0].pszTemplate = MAKEINTRESOURCE(IDD_PP_MAP_CFG);

	psp[1].pfnDlgProc = MapOptDefaultsPropProc;
	psp[1].pszTemplate = MAKEINTRESOURCE(IDD_PP_MAP_DEFAULTS);


	/* Fill in fields common to all pages. */
	for(i = 0; i < MAPOPT_NUMPAGES; i++)
	{
		psp[i].dwSize = sizeof(PROPSHEETPAGE);
		psp[i].dwFlags = /*PSP_HASHELP */ 0;
		psp[i].lParam = (LONG)lpmapoptppdata;
		psp[i].hInstance = g_hInstance;
	}

	/* Properties affecting the whole dialogue. */
	psh.dwSize = sizeof(psh);
	psh.dwFlags = PSH_HASHELP | PSH_NOAPPLYNOW | PSH_PROPTITLE | PSH_PROPSHEETPAGE | PSH_USECALLBACK;
	psh.hwndParent = g_hwndMain;
	psh.nPages = MAPOPT_NUMPAGES;
	psh.nStartPage = 0;
	psh.ppsp = psp;
	psh.pszCaption = MAKEINTRESOURCE(IDS_MAPOPTCAPTION);
	psh.hInstance = g_hInstance;
	psh.pfnCallback = PropSheetCentreCallback;

	/* Assume we cancelled. */
	lpmapoptppdata->bOKed = FALSE;

	/* Go! */
	PropertySheet(&psh);

	/* Did we close by clicking OK? */
	return lpmapoptppdata->bOKed;
}



/* MapOptCfgPropProc
 *   Dialogue proc for Configuration page of Map Properties dialogue.
 *
 * Parameters:
 *   HWND	hwndPropPage	Property page window handle.
 *   UINT	uiMsg			Message identifier.
 *   WPARAM wParam			Message-specific.
 *   LPARAM lParam			Message-specific.
 *
 * Return value: BOOL
 *   TRUE if the message was processed; FALSE otherwise.
 */
static BOOL CALLBACK MapOptCfgPropProc(HWND hwndPropPage, UINT uiMsg, WPARAM wParam, LPARAM lParam)
{
	static MAPOPTPPDATA *s_lpmapoptppdata;
	
	UNREFERENCED_PARAMETER(wParam);

	switch(uiMsg)
	{
	case WM_INITDIALOG:
		{
			HWND hwndMapConfigs = GetDlgItem(hwndPropPage, IDC_COMBO_CONFIG);
			HWND hwndGametypes = GetDlgItem(hwndPropPage, IDC_COMBO_TESTMODE);
			int iCount, i;
			TCHAR szLumpName[9];
			char cGametype;

			/* Get parameters. */
			s_lpmapoptppdata = (MAPOPTPPDATA*)((PROPSHEETPAGE*)lParam)->lParam;

			/* Fill the map config list and select the current one. */
			AddMapConfigsToComboBox(hwndMapConfigs);

			/* Find a matching config. */
			iCount = SendMessage(hwndMapConfigs, CB_GETCOUNT, 0, 0);
			for(i = 0; i < iCount; i++)
			{
				CONFIG *lpcfg = (CONFIG*)SendMessage(hwndMapConfigs, CB_GETITEMDATA, i, 0);
				if(lpcfg == s_lpmapoptppdata->lpcfgMap)
				{
					SendMessage(hwndMapConfigs, CB_SETCURSEL, i, 0);
					break;
				}
			}

			/* Get the lumpname. */
#ifdef _UNICODE
			MultiByteToWideChar(CP_ACP, MB_PRECOMPOSED, s_lpmapoptppdata->szLumpName, -1, szLumpName, sizeof(szLumpName)/sizeof(WCHAR));
#else
			strcpy(szLumpName, s_lpmapoptppdata->szLumpName);
#endif
			SetWindowText(GetDlgItem(hwndPropPage, IDC_EDIT_LUMPNAME), szLumpName);

			
			/* Fill the list of gametypes. */
			PopulateGametypeList(hwndGametypes);

			/* Find a matching gametype. */
			iCount = SendMessage(hwndGametypes, CB_GETCOUNT, 0, 0);
			cGametype = (char)ConfigGetInteger(s_lpmapoptppdata->lpcfgWadOptMap, TEXT("gametype"));
			for(i = 0; i < iCount; i++)
			{
				char cListGametype = (char)SendMessage(hwndGametypes, CB_GETITEMDATA, i, 0);
				if(cListGametype == cGametype)
				{
					SendMessage(hwndGametypes, CB_SETCURSEL, i, 0);
					break;
				}
			}

			/* Set the external wad filename. */
			SetDlgItemText(hwndPropPage, IDC_EDIT_EXTWAD, s_lpmapoptppdata->szAddWadName);
		}
			
		/* Let the system set the focus. */
		return TRUE;

	case WM_COMMAND:
		switch(LOWORD(wParam))
		{
		case IDC_BUTTON_BROWSE:
			{
				TCHAR szFilename[MAX_PATH];
				TCHAR szFilter[128];

				/* As initial filename, use that currently specified. */
				GetDlgItemText(hwndPropPage, IDC_EDIT_EXTWAD, szFilename, sizeof(szFilename)/sizeof(TCHAR));

				/* Load the filter from the string table. */
				LoadAndFormatFilterString(IDS_WADFILEFILTER, szFilter, sizeof(szFilter)/sizeof(TCHAR));

				/* Show the Open dialogue. */
				if(CommDlgOpen(hwndPropPage, szFilename, sizeof(szFilename)/sizeof(TCHAR), NULL, szFilter, TEXT("wad"), szFilename, OFN_EXPLORER | OFN_FILEMUSTEXIST | OFN_HIDEREADONLY))
				{
					/* If the user didn't cancel, update the filename. */
					SetDlgItemText(hwndPropPage, IDC_EDIT_EXTWAD, szFilename);
				}
			}

			return TRUE;
		}

		/* Didn't process message. */
		break;

	case WM_NOTIFY:
		switch(((LPNMHDR)lParam)->code)
		{
		case PSN_APPLY:
			{
				int iSelection;
				TCHAR szLumpnameT[CCH_LUMPNAME + 1];

				/* Assume we don't fail any validation. This may change. */
				SetWindowLong(hwndPropPage, DWL_MSGRESULT, PSNRET_NOERROR);

				/* We didn't cancel. */
				s_lpmapoptppdata->bOKed = TRUE;

				/* Map configuration. */
				iSelection = SendDlgItemMessage(hwndPropPage, IDC_COMBO_CONFIG, CB_GETCURSEL, 0, 0);
				if(iSelection != CB_ERR)
					s_lpmapoptppdata->lpcfgMap = (CONFIG*)SendDlgItemMessage(hwndPropPage, IDC_COMBO_CONFIG, CB_GETITEMDATA, iSelection, 0);
				else
					s_lpmapoptppdata->lpcfgMap = NULL;

				/* Gametype. */
				iSelection = SendDlgItemMessage(hwndPropPage, IDC_COMBO_TESTMODE, CB_GETCURSEL, 0, 0);
				if(iSelection != CB_ERR)
					ConfigSetInteger(s_lpmapoptppdata->lpcfgWadOptMap, TEXT("gametype"), SendDlgItemMessage(hwndPropPage, IDC_COMBO_TESTMODE, CB_GETITEMDATA, iSelection, 0));

				/* Lumpname. */
				GetDlgItemText(hwndPropPage, IDC_EDIT_LUMPNAME, szLumpnameT, NUM_ELEMENTS(szLumpnameT));
#ifdef UNICODE
				WideCharToMultiByte(CP_ACP, 0, szLumpnameT, -1, s_lpmapoptppdata->szLumpName, NUM_ELEMENTS(s_lpmapoptppdata->szLumpName), NULL, NULL);
#else
				strcpy(s_lpmapoptppdata->szLumpName, szLumpnameT);
#endif

				/* Additional wad. */
				GetDlgItemText(hwndPropPage, IDC_EDIT_EXTWAD, s_lpmapoptppdata->szAddWadName, sizeof(s_lpmapoptppdata->szAddWadName) / sizeof(TCHAR));
			}

			return TRUE;

		case PSN_KILLACTIVE:
			{
				/* Validate the lumpname. Silently truncate it. */
				TCHAR szLumpname[CCH_LUMPNAME + 1];

#ifdef _UNICODE
				CHAR szLumpnameA[CCH_LUMPNAME + 1];
#else
				LPSTR szLumpnameA = szLumpname;
#endif

				/* Assume failure. */
				BOOL bValid = FALSE;

				GetDlgItemText(hwndPropPage, IDC_EDIT_LUMPNAME, szLumpname, NUM_ELEMENTS(szLumpname));

#ifdef _UNICODE
				WideCharToMultiByte(CP_ACP, 0, szLumpname, -1, szLumpnameA, NUM_ELEMENTS(szLumpnameA), NULL, NULL);
#endif
				
				if(!IsMapLumpnameA(szLumpnameA))
					MessageBoxFromStringTable(hwndPropPage, IDS_ERROR_MAPLUMPNAME, MB_ICONERROR);
				else if(strcmp(s_lpmapoptppdata->szLumpName, szLumpnameA) != 0 && GetLumpIndex(s_lpmapoptppdata->lpwad, 0, -1, szLumpnameA) >= 0)
					MessageBoxFromStringTable(hwndPropPage, IDS_ERROR_MAPEXISTS, MB_ICONERROR);
				else
					bValid = TRUE;
				
				SetWindowLong(hwndPropPage, DWL_MSGRESULT, !bValid);
			}

			return TRUE;

		case PSN_QUERYCANCEL:
			/* Allow the cancel, and signal that we did so. */
			SetWindowLong(hwndPropPage, DWL_MSGRESULT, FALSE);

			/* TODO: Why is this necessary?? */
			s_lpmapoptppdata->bOKed = FALSE;
			return TRUE;
		}

		/* Didn't process WM_NOTIFY message. */
		break;
	}

	return FALSE;
}


/* PopulateGametypeList
 *   Fills a combobox with gametypes.
 *
 * Parameters:
 *   HWND	hwndCombo	Window handle of combobox.
 *
 * Return value: None.
 */
void PopulateGametypeList(HWND hwndCombo)
{
	struct GTPAIR {char cGametype; USHORT unStringID;} gtpair[] =
	{
		{GT_DEFAULT, IDS_GT_DEFAULT},
		{GT_COOP, IDS_GT_COOP},
		{GT_FULLRACE, IDS_GT_FULLRACE},
		{GT_TIMEONLYRACE, IDS_GT_TIMEONLYRACE},
		{GT_MATCH, IDS_GT_MATCH},
		{GT_TAG, IDS_GT_TAG},
		{GT_CTF, IDS_GT_CTF},
		{GT_SINGLEPLAYER, IDS_GT_SINGLEPLAYER}
	};

	int i;

	for(i = 0; (unsigned int)i < sizeof(gtpair) / sizeof(struct GTPAIR); i++)
	{
		TCHAR szGametype[64];
		int iIndex;

		LoadString(g_hInstance, gtpair[i].unStringID, szGametype, sizeof(szGametype) / sizeof(TCHAR));
		iIndex = SendMessage(hwndCombo, CB_ADDSTRING, 0, (LPARAM)szGametype);

		SendMessage(hwndCombo, CB_SETITEMDATA, iIndex, gtpair[i].cGametype);
	}
}


/* MapOptDefaultsPropProc
 *   Dialogue proc for Defaults page of Map Properties dialogue.
 *
 * Parameters:
 *   HWND	hwndPropPage	Property page window handle.
 *   UINT	uiMsg			Message identifier.
 *   WPARAM wParam			Message-specific.
 *   LPARAM lParam			Message-specific.
 *
 * Return value: BOOL
 *   TRUE if the message was processed; FALSE otherwise.
 */
static BOOL CALLBACK MapOptDefaultsPropProc(HWND hwndPropPage, UINT uiMsg, WPARAM wParam, LPARAM lParam)
{
	static MAPOPTPPDATA *s_lpmapoptppdata;

	static TPINFO s_tpinfo[] =
	{
		{IDC_TEX_UPPER,		IDC_EDIT_UPPER,		TF_TEXTURE,	NULL},
		{IDC_TEX_MIDDLE,	IDC_EDIT_MIDDLE,	TF_TEXTURE,	NULL},
		{IDC_TEX_LOWER,		IDC_EDIT_LOWER,		TF_TEXTURE,	NULL},
		{IDC_TEX_CEIL,		IDC_EDIT_TCEIL,		TF_FLAT,	NULL},
		{IDC_TEX_FLOOR,		IDC_EDIT_TFLOOR,	TF_FLAT,	NULL}
	};

	switch(uiMsg)
	{
	case WM_INITDIALOG:
		{
			HDC hdcDlg;
			int i;
			TCHAR szTexName[TEXNAME_BUFFER_LENGTH];
			CONFIG *lpcfgMapDefTex, *lpcfgMapDefSec;
			UDACCEL udaccelHeight[2], udaccelBrightness[2];

			/* Get parameters. */
			s_lpmapoptppdata = (MAPOPTPPDATA*)((PROPSHEETPAGE*)lParam)->lParam;

			/* Get subsections of config. */
			lpcfgMapDefTex = ConfigGetSubsection(s_lpmapoptppdata->lpcfgWadOptMap, WADOPT_DEFAULTTEX);
			lpcfgMapDefSec = ConfigGetSubsection(s_lpmapoptppdata->lpcfgWadOptMap, WADOPT_DEFAULTSEC);

			/* Subclass the texture preview controls and create their preview
			 * bitmaps.
			 */
			hdcDlg = GetDC(hwndPropPage);
			for(i = 0; i < (int)NUM_ELEMENTS(s_tpinfo); i++)
			{
				HWND hwndTex = GetDlgItem(hwndPropPage, s_tpinfo[i].iTPID);

				/* Allocate memory for the preview window. It's freed when the
				 * window's destroyed.
				 */
				TPPDATA *lptppd = ProcHeapAlloc(sizeof(TPPDATA));

				lptppd->wndprocStatic = (WNDPROC)GetWindowLong(hwndTex, GWL_WNDPROC);

				SetWindowLong(hwndTex, GWL_USERDATA, (LONG)lptppd);
				SetWindowLong(hwndTex, GWL_WNDPROC, (LONG)TexPreviewProc);

				/* Create preview bitmap. */
				s_tpinfo[i].hbitmap = CreateCompatibleBitmap(hdcDlg, CX_TEXPREVIEW, CY_TEXPREVIEW);
				SendDlgItemMessage(hwndPropPage, s_tpinfo[i].iTPID, STM_SETIMAGE, IMAGE_BITMAP, (LPARAM)s_tpinfo[i].hbitmap);

				/* Set the preview to something sensible in case we never touch
				 * it again.
				 */
				SetTexturePreviewImage(s_lpmapoptppdata->hwndMap, hwndTex, TEXT(""), TF_TEXTURE, s_tpinfo[i].hbitmap, FALSE);
			}

			ReleaseDC(hwndPropPage, hdcDlg);


			/* Set up the edit boxes. */

			ConfigGetString(lpcfgMapDefTex, TEXT("upper"), szTexName, TEXNAME_BUFFER_LENGTH);
			SetDlgItemText(hwndPropPage, IDC_EDIT_UPPER, szTexName);

			ConfigGetString(lpcfgMapDefTex, TEXT("middle"), szTexName, TEXNAME_BUFFER_LENGTH);
			SetDlgItemText(hwndPropPage, IDC_EDIT_MIDDLE, szTexName);

			ConfigGetString(lpcfgMapDefTex, TEXT("lower"), szTexName, TEXNAME_BUFFER_LENGTH);
			SetDlgItemText(hwndPropPage, IDC_EDIT_LOWER, szTexName);

			ConfigGetString(lpcfgMapDefSec, TEXT("tceiling"), szTexName, TEXNAME_BUFFER_LENGTH);
			SetDlgItemText(hwndPropPage, IDC_EDIT_TCEIL, szTexName);

			ConfigGetString(lpcfgMapDefSec, TEXT("tfloor"), szTexName, TEXNAME_BUFFER_LENGTH);
			SetDlgItemText(hwndPropPage, IDC_EDIT_TFLOOR, szTexName);

			SetDlgItemInt(hwndPropPage, IDC_EDIT_HCEIL, ConfigGetInteger(lpcfgMapDefSec, TEXT("hceiling")), TRUE);
			SetDlgItemInt(hwndPropPage, IDC_EDIT_HFLOOR, ConfigGetInteger(lpcfgMapDefSec, TEXT("hfloor")), TRUE);
			SetDlgItemInt(hwndPropPage, IDC_EDIT_BRIGHTNESS, ConfigGetInteger(lpcfgMapDefSec, TEXT("brightness")), TRUE);


			/* Set up the spinners. */

			udaccelHeight[0].nInc = 8;
			udaccelHeight[0].nSec = 0;
			udaccelHeight[1].nInc = 64;
			udaccelHeight[1].nSec = 2;

			udaccelBrightness[0].nInc = 1;
			udaccelBrightness[0].nSec = 0;
			udaccelBrightness[1].nInc = 8;
			udaccelBrightness[1].nSec = 2;

			SendDlgItemMessage(hwndPropPage, IDC_SPIN_BRIGHTNESS, UDM_SETRANGE32, 0, 255);
			SendDlgItemMessage(hwndPropPage, IDC_SPIN_BRIGHTNESS, UDM_SETACCEL, NUM_ELEMENTS(udaccelBrightness), (LPARAM)udaccelBrightness);

			SendDlgItemMessage(hwndPropPage, IDC_SPIN_HCEIL, UDM_SETRANGE32, -32768, 32767);
			SendDlgItemMessage(hwndPropPage, IDC_SPIN_HCEIL, UDM_SETACCEL, NUM_ELEMENTS(udaccelHeight), (LPARAM)udaccelHeight);

			SendDlgItemMessage(hwndPropPage, IDC_SPIN_HFLOOR, UDM_SETRANGE32, -32768, 32767);
			SendDlgItemMessage(hwndPropPage, IDC_SPIN_HFLOOR, UDM_SETACCEL, NUM_ELEMENTS(udaccelHeight), (LPARAM)udaccelHeight);
		}

		/* Let the system set the focus. */
		return TRUE;

	case WM_COMMAND:

		switch(LOWORD(wParam))
		{
		case IDC_TEX_UPPER:
		case IDC_TEX_MIDDLE:
		case IDC_TEX_LOWER:
		case IDC_TEX_CEIL:
		case IDC_TEX_FLOOR:

			switch(HIWORD(wParam))
			{
			case BN_CLICKED:
				{
					TCHAR szTexName[TEXNAME_BUFFER_LENGTH];
					int iPreviewIndex;
					BOOL bTexOKed;

					/* Find the corresponding edit box. */
					for(iPreviewIndex = 0; s_tpinfo[iPreviewIndex].iTPID != LOWORD(wParam); iPreviewIndex++);

					/* Get the current flat so it's selected initially. */
					GetDlgItemText(hwndPropPage, s_tpinfo[iPreviewIndex].iEditID, szTexName, TEXNAME_BUFFER_LENGTH);

					/* Show the texture browser. */
					if(s_tpinfo[iPreviewIndex].tf == TF_TEXTURE)
						bTexOKed = SelectTexture(hwndPropPage, s_lpmapoptppdata->hwndMap, TF_TEXTURE, s_lpmapoptppdata->lphimlCacheTextures, s_lpmapoptppdata->lptnlTextures, s_lpmapoptppdata->lpcfgTexUsed, szTexName);
					else
						bTexOKed = SelectTexture(hwndPropPage, s_lpmapoptppdata->hwndMap, TF_FLAT, s_lpmapoptppdata->lphimlCacheFlats, s_lpmapoptppdata->lptnlFlats, s_lpmapoptppdata->lpcfgFlatsUsed, szTexName);

					if(bTexOKed)
					{
						/* User didn't cancel. Update the edit-box. */
						SetDlgItemText(hwndPropPage, s_tpinfo[iPreviewIndex].iEditID, szTexName);
					}

					return TRUE;
				}
			}

			break;

		case IDC_EDIT_UPPER:
		case IDC_EDIT_MIDDLE:
		case IDC_EDIT_LOWER:
		case IDC_EDIT_TCEIL:
		case IDC_EDIT_TFLOOR:

			switch(HIWORD(wParam))
			{
			case EN_CHANGE:
				{
					TCHAR szEditText[TEXNAME_BUFFER_LENGTH];
					int iPreviewIndex;

					/* Find the corresponding preview control. */
					for(iPreviewIndex = 0; s_tpinfo[iPreviewIndex].iEditID != LOWORD(wParam); iPreviewIndex++);

					/* Get the texture name. */
					GetWindowText((HWND)lParam, szEditText, TEXNAME_BUFFER_LENGTH);

					/* Update the texture preview image. */
					SetTexturePreviewImage
						(s_lpmapoptppdata->hwndMap,
						 GetDlgItem(hwndPropPage, s_tpinfo[iPreviewIndex].iTPID),
						 szEditText,
						 s_tpinfo[iPreviewIndex].tf,
						 s_tpinfo[iPreviewIndex].hbitmap,
						 FALSE);

					return TRUE;
				}
			}

			break;

		case IDC_EDIT_HCEIL:
		case IDC_EDIT_HFLOOR:
			switch(HIWORD(wParam))
			{
			case EN_KILLFOCUS:
				/* Validation. */
				BoundEditBox((HWND)lParam, -32768, 32767, FALSE, FALSE);

				return TRUE;
			}

		case IDC_EDIT_BRIGHTNESS:
			switch(HIWORD(wParam))
			{
			case EN_KILLFOCUS:
				/* Validation. */
				BoundEditBox((HWND)lParam, 0, 255, FALSE, FALSE);

				return TRUE;
			}
		}

		/* Didn't process message. */
		break;

	case WM_NOTIFY:
		switch(((LPNMHDR)lParam)->code)
		{
		case PSN_APPLY:
			{
				TCHAR szTexName[TEXNAME_BUFFER_LENGTH];

				/* Get subsections of config. */
				CONFIG *lpcfgMapDefTex = ConfigGetSubsection(s_lpmapoptppdata->lpcfgWadOptMap, WADOPT_DEFAULTTEX);
				CONFIG *lpcfgMapDefSec = ConfigGetSubsection(s_lpmapoptppdata->lpcfgWadOptMap, WADOPT_DEFAULTSEC);

				/* Set the values! */
				GetDlgItemText(hwndPropPage, IDC_EDIT_UPPER, szTexName, NUM_ELEMENTS(szTexName));
				ConfigSetString(lpcfgMapDefTex, TEXT("upper"), szTexName);				

				GetDlgItemText(hwndPropPage, IDC_EDIT_MIDDLE, szTexName, NUM_ELEMENTS(szTexName));
				ConfigSetString(lpcfgMapDefTex, TEXT("middle"), szTexName);

				GetDlgItemText(hwndPropPage, IDC_EDIT_LOWER, szTexName, NUM_ELEMENTS(szTexName));
				ConfigSetString(lpcfgMapDefTex, TEXT("lower"), szTexName);

				GetDlgItemText(hwndPropPage, IDC_EDIT_TCEIL, szTexName, NUM_ELEMENTS(szTexName));
				ConfigSetString(lpcfgMapDefSec, TEXT("tceiling"), szTexName);

				GetDlgItemText(hwndPropPage, IDC_EDIT_TFLOOR, szTexName, NUM_ELEMENTS(szTexName));
				ConfigSetString(lpcfgMapDefSec, TEXT("tfloor"), szTexName);

				ConfigSetInteger(lpcfgMapDefSec, TEXT("hceiling"), GetDlgItemInt(hwndPropPage, IDC_EDIT_HCEIL, NULL, TRUE));
				ConfigSetInteger(lpcfgMapDefSec, TEXT("hfloor"), GetDlgItemInt(hwndPropPage, IDC_EDIT_HFLOOR, NULL, TRUE));
				ConfigSetInteger(lpcfgMapDefSec, TEXT("brightness"), GetDlgItemInt(hwndPropPage, IDC_EDIT_BRIGHTNESS, NULL, FALSE));
			}

			return TRUE;
		}

		/* Didn't process message. */
		break;

	case WM_DESTROY:
		{
			int i;

			/* Destroy preview bitmaps. */
			for(i = 0; i < (int)NUM_ELEMENTS(s_tpinfo); i++)
				DeleteObject(s_tpinfo[i].hbitmap);
		}
	}

	/* Didn't process message. */
	return FALSE;
}
