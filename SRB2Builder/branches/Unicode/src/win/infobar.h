#ifndef __SRB2B_INFOBAR__
#define __SRB2B_INFOBAR__

#include <windows.h>

#include "../texture.h"

#include "../CodeImp/ci_const.h"

/* Types. */

typedef struct _LINEDEFDISPLAYINFO
{
	int				iIndex;
	TCHAR			szEffect[96];
	unsigned short	unEffect;		/* For speed in checking, and prop-page. */
	int				cxVector, cyVector;
	unsigned short	unTag;

	TCHAR			szFrontUpper[9], szFrontMiddle[9], szFrontLower[9];
	TCHAR			szBackUpper[9], szBackMiddle[9], szBackLower[9];

	/* Texture bitmaps. */
	TEXTURE			*lptexFrontUpper, *lptexFrontMiddle, *lptexFrontLower;
	TEXTURE			*lptexBackUpper, *lptexBackMiddle, *lptexBackLower;
	DWORD			dwDestroyTexFlags;
	DWORD			dwPseudoTexFlags;
	DWORD			dwBlankTexFlags;

	unsigned short	unFrontSector, unBackSector;
	int				iFrontHeight, iBackHeight;
	short			nFrontX, nBackX;
	short			nFrontY, nBackY;

	BOOL			bHasFront, bHasBack;		/* Sidedefs. */
	BOOL			bHasFrontSec, bHasBackSec;	/* Sectors. */
} LINEDEFDISPLAYINFO;

enum ENUM_LDDI_FLAGS
{
	LDDIF_INDEX			= 0x000001,
	LDDIF_EFFECT		= 0x000002,
	LDDIF_VECTOR		= 0x000004,
	LDDIF_TAG			= 0x000008,
	LDDIF_LENGTH		= 0x000010,
	LDDIF_FRONTSEC		= 0x000020,
	LDDIF_BACKSEC		= 0x000040,
	LDDIF_FRONTHEIGHT	= 0x000080,
	LDDIF_BACKHEIGHT	= 0x000100,
	LDDIF_FRONTX		= 0x000200,
	LDDIF_BACKX			= 0x000400,
	LDDIF_FRONTY		= 0x000400,
	LDDIF_BACKY			= 0x001000,
	LDDIF_FRONTSD		= 0x002000,
	LDDIF_FRONTUPPER	= 0x004000,
	LDDIF_FRONTMIDDLE	= 0x008000,
	LDDIF_FRONTLOWER	= 0x010000,
	LDDIF_BACKSD		= 0x020000,
	LDDIF_BACKUPPER		= 0x040000,
	LDDIF_BACKMIDDLE	= 0x080000,
	LDDIF_BACKLOWER		= 0x100000,
	LDDIF_HASFRONT		= 0x200000,
	LDDIF_HASBACK		= 0x400000,
	LDDIF_ALL			= 0x7FFFFF
};

typedef struct _SECTORDISPLAYINFO
{
	int				iIndex;
	TCHAR			szEffect[96];
	unsigned short	unEffect;		/* For speed in checking, and prop-page. */
	unsigned short	unTag;
	short			nCeiling, nFloor;
	unsigned char	ucBrightness;

	TCHAR			szCeiling[9], szFloor[9];
	TEXTURE			*lptexCeiling, *lptexFloor;
	DWORD			dwDestroyTexFlags;
} SECTORDISPLAYINFO;

enum ENUM_SDI_FLAGS
{
	SDIF_INDEX		= 0x0001,
	SDIF_EFFECT		= 0x0002,
	SDIF_CEILING	= 0x0004,
	SDIF_FLOOR		= 0x0008,
	SDIF_HEIGHT		= 0x0010,
	SDIF_TAG		= 0x0020,
	SDIF_BRIGHTNESS	= 0x0040,
	SDIF_FLOORTEX	= 0x0080,
	SDIF_CEILINGTEX	= 0x0100,
	SDIF_ALL		= 0x01FF
};

typedef struct _THINGDISPLAYINFO
{
	int				iIndex;
	TCHAR			szType[96];
	unsigned short	unType;			/* For speed in checking, and prop-page. */
	short			x, y;
	unsigned short	z;
	unsigned short	unFlags;
	TCHAR			szDirection[32];
	short			nDirection;		/* For speed in checking, and prop-page. */

	TCHAR			szSprite[9];
	TEXTURE			*lptexSprite;
	DWORD			dwDestroyTexFlags;
} THINGDISPLAYINFO;


enum ENUM_TDI_FLAGS
{
	TDIF_INDEX		= 0x0001,
	TDIF_TYPE		= 0x0002,
	TDIF_X			= 0x0004,
	TDIF_Y			= 0x0008,
	TDIF_Z			= 0x0010,
	TDIF_FLAGS		= 0x0020,
	TDIF_DIRECTION	= 0x0040,
	TDIF_SPRITE		= 0x0080,
	TDIF_ALL		= 0x00FF
};

typedef struct _VERTEXDISPLAYINFO
{
	int				iIndex;
	short			x, y;
} VERTEXDISPLAYINFO;

enum ENUM_VDI_FLAGS
{
	VDIF_INDEX		= 0x0001,
	VDIF_COORDS		= 0x0002,
	VDIF_ALL		= 0x0003
};


typedef enum _INFOBAR_PANEL_FLAGS
{
	IBPF_LINEDEF	= 0x0001,
	IBPF_SECTOR		= 0x0002,
	IBPF_VERTEX		= 0x0004,
	IBPF_THING		= 0x0008,
	IBPF_ALLINFO	= 0x000F,
	IBPF_SIDEDEF	= 0x0010,
	IBPF_FLAT		= 0x0020,
	IBPF_SPRITE		= 0x0040,
	IBPF_ALLTEX		= 0x0070,
	IBPF_ALL		= 0x007F
} INFOBAR_PANEL_FLAGS;


enum ENUM_TEXPREVIEWS
{
	TP_FRONTUPPER,
	TP_FRONTMIDDLE,
	TP_FRONTLOWER,
	TP_BACKUPPER,
	TP_BACKMIDDLE,
	TP_BACKLOWER,
	TP_CEILING,
	TP_FLOOR,
	TP_SPRITE
};




/* Function prototypes. */
BOOL CreateInfoBarWindow(HWND hwndParent);
void DestroyInfoBarWindow(void);
void ShowLinesInfo(LINEDEFDISPLAYINFO *lplddi, DWORD dwDisplayFlags, BYTE byTexRequirementFlags);
void ShowSectorInfo(SECTORDISPLAYINFO *lpsdi, DWORD dwDisplayFlags);
void ShowThingInfo(THINGDISPLAYINFO *lptdi, DWORD dwDisplayFlags);
void ShowVertexInfo(VERTEXDISPLAYINFO *lpvdi, DWORD dwDisplayFlags);
void ResetSidedefPreviews(void);
void ResetFlatPreviews(void);
void ResetSpritePreview(void);
void InfoBarShowPanels(DWORD dwFlags, BOOL bShow);
void InfoBarTakeControl(HWND hwnd, ENUM_EDITMODE editmode, RGBQUAD *lprgbqPal);
void InfoBarEnableDetailsPanels(void);
void InfoBarDisableDetailsPanels(void);


/* Macros. */
#define WM_INFOBAR_INITTEXBITMAPS	WM_APP
#define WM_INFOBAR_SETTEXPREVIEW	(WM_APP+1)

#endif

