#include <windows.h>
#include <commctrl.h>
#include <stdio.h>
#include <tchar.h>

#ifndef ID_FILE_MRU_FILE1
#define ID_FILE_MRU_FILE1 0xE110
#endif

#include "../general.h"
#include "../map.h"
#include "../../res/resource.h"
#include "../maptypes.h"
#include "../mapconfig.h"
#include "../openwads.h"
#include "../cdlgwrapper.h"
#include "../config.h"
#include "../options.h"
#include "../wadopts.h"
#include "../wiki.h"
#include "../keyboard.h"

#include "mdiframe.h"
#include "mapwin.h"
#include "wadlist.h"
#include "infobar.h"
#include "mapselect.h"
#include "toolbar.h"
#include "gendlg.h"

#include "../../DockWnd/DockWnd.h"

#include "../../res/resource.h"

/* Positions of Window sub-menus. */

#define IDM_FIRST_CHILD 51000

/* Classname. */
#define FRAMEWINCLASS TEXT("SRB2BuilderFrame")

/* We have one or two digits, an ampersand and a space at the start of each MRU
 * entry.
 */
#define MRU_LEADER_CHARS 4

/* Number of characters (inc. terminator) for "Recent Files" string. */
#define RECENTPLACEHOLDER_BUFLEN 64

/* Causes the MDI client to take the focus. */
#define WM_TAKEFOCUS	WM_APP


typedef struct _ACT_MAPWIN_DATA
{
	LPCSTR	szMapname;
	BOOL	bFound;
} ACT_MAPWIN_DATA;


/* Static Function prototypes. */
static LRESULT CALLBACK FrameWndProc(HWND hwnd, UINT uiMessage, WPARAM wParam, LPARAM lParam);
static BOOL CloseAllChildWindows(void);
static void CloseMainWindow(void);
static void CreateStatusBar(HWND hwndParent);
static void MRUBuildMenu(void);
static __inline int GetFileMenuPos(void);
static BOOL CALLBACK ActivateMapWindowEnumProc(HWND hwndMap, LPARAM lParam);
static BOOL CALLBACK ActivateMapWindowEnumProc(HWND hwndMap, LPARAM lParam);


/* Globals. */
HWND g_hwndMain, g_hwndClient;			/* Window handles. */
HWND g_hwndStatusBar;

HMENU g_hmenuNodoc, g_hmenuMap;				/* Different main menus. */
HMENU g_hmenuNodocWin;						/* Window submenu of no-doc menu. */

/* All top-level submenus of map menu, to counteract MDI's interference. */
HMENU g_hmenuMapSubmenus[MAPMENUCOUNT];



/* CreateMainWindow
 *   Creates the MDI frame window.
 *
 * Parameters:
 *   int		iCmdShow			Constant determining initial window state.
 *
 * Return value: int
 *   Zero on success; nonzero on error.
 *
 * Remarks:
 *   The main config file must be loaded before calling this function.
 */

int CreateMainWindow(int iCmdShow)
{
	WNDCLASSEX wndclassex;
	const TCHAR szClassName[] = FRAMEWINCLASS;
	MENUITEMINFO mi;
	int i;

	/* Set up and register the window class. */

	wndclassex.cbClsExtra = 0;
	wndclassex.cbSize = sizeof(wndclassex);
	wndclassex.cbWndExtra = 0;
	wndclassex.hbrBackground = (HBRUSH)(COLOR_APPWORKSPACE + 1);
	wndclassex.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclassex.hIcon = LoadIcon(g_hInstance, MAKEINTRESOURCE(IDI_MAIN));
	
	/* For the small icon, ask the system what size it should be. */
	wndclassex.hIconSm = LoadImage(g_hInstance, MAKEINTRESOURCE(IDI_MAIN), IMAGE_ICON, GetSystemMetrics(SM_CXSMICON), GetSystemMetrics(SM_CYSMICON), LR_DEFAULTCOLOR | LR_SHARED);

	wndclassex.hInstance = g_hInstance;
	wndclassex.lpfnWndProc = FrameWndProc;
	wndclassex.lpszClassName = szClassName;
	wndclassex.lpszMenuName = NULL;
	wndclassex.style = CS_VREDRAW | CS_HREDRAW;

	/* The only reason this should fail is if we try to register a Unicode class
	 * on 9x.
	 */
	if(!RegisterClassEx(&wndclassex)) return 1;

	/* Get menu handles. */
	g_hmenuMap = LoadMenu(g_hInstance, MAKEINTRESOURCE(IDM_MAP));
	g_hmenuNodoc = LoadMenu(g_hInstance, MAKEINTRESOURCE(IDM_NODOC));
	g_hmenuNodocWin = GetSubMenu(g_hmenuNodoc, WINDOWMENUPOSNODOC);

	UpdateMenuShortcutKeys(g_hmenuMap);
	UpdateMenuShortcutKeys(g_hmenuNodoc);

	/* Get submenus of map menu. We don't just call GetSubMenu when we need
	 * these later, since the MDI messes with the indexing by adding the child's
	 * system menu.
	 */
	for(i = 0; i < MAPMENUCOUNT; i++)
		g_hmenuMapSubmenus[i] = GetSubMenu(g_hmenuMap, i);

	/* Assign some IDs to top-level menus. */
	mi.cbSize = sizeof(mi);
	mi.fMask = MIIM_ID;

	mi.wID = IDM_FILE;		SetMenuItemInfo(g_hmenuNodoc, FILEMENUPOSNODOC, TRUE, &mi);
	mi.wID = IDM_HELP;		SetMenuItemInfo(g_hmenuNodoc, HELPMENUPOSNODOC, TRUE, &mi);
	
	mi.wID = IDM_FILE;		SetMenuItemInfo(g_hmenuMap, FILEMENUPOS, TRUE, &mi);
	mi.wID = IDM_EDIT;		SetMenuItemInfo(g_hmenuMap, EDITMENUPOS, TRUE, &mi);
	mi.wID = IDM_VIEW;		SetMenuItemInfo(g_hmenuMap, VIEWMENUPOS, TRUE, &mi);
	mi.wID = IDM_SECTORS;	SetMenuItemInfo(g_hmenuMap, SECTORSMENUPOS, TRUE, &mi);
	mi.wID = IDM_LINES;		SetMenuItemInfo(g_hmenuMap, LINESMENUPOS, TRUE, &mi);
	mi.wID = IDM_VERTICES;	SetMenuItemInfo(g_hmenuMap, VERTICESMENUPOS, TRUE, &mi);
	mi.wID = IDM_THINGS;	SetMenuItemInfo(g_hmenuMap, THINGSMENUPOS, TRUE, &mi);
	mi.wID = IDM_WINDOW;	SetMenuItemInfo(g_hmenuMap, WINDOWMENUPOS, TRUE, &mi);
	mi.wID = IDM_HELP;		SetMenuItemInfo(g_hmenuMap, HELPMENUPOS, TRUE, &mi);

#ifdef _DEBUG
	mi.wID = IDM_DEBUG;		SetMenuItemInfo(g_hmenuMap, DEBUGMENUPOS, TRUE, &mi);
#endif

#ifdef _DEBUG
	{
		/* Append the Debug menu to the menu bar. */
		TCHAR szCaption[] = TEXT("&Debug");

		mi.dwTypeData = szCaption;
		mi.fMask = MIIM_TYPE | MIIM_SUBMENU;
		mi.fType = MFT_STRING;
		mi.hSubMenu = GetSubMenu(LoadMenu(g_hInstance, MAKEINTRESOURCE(IDM_MAP_DEBUG)), 0);
		
		InsertMenuItem(g_hmenuMap, -1, TRUE, &mi);
	}
#endif

	/* Create the window. */

	g_hwndMain = CreateWindowEx(	0,							/* Extended styles. */
									FRAMEWINCLASS,				/* Class. */
									g_szAppName,				/* Window name. */
																/* Window styles. */
									WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN,
																/* Position and size. */
									CW_USEDEFAULT, CW_USEDEFAULT, CW_USEDEFAULT,
										CW_USEDEFAULT,
									NULL,						/* Parent. */
																/* Menu. */
									g_hmenuNodoc,
									g_hInstance,				/* Instance handle. */
									NULL);						/* Extra data. */

	/* Just in case creating the window failed: */
	if(!g_hwndMain) return 2;


	/* Show the window as the system wants us to. */
	ShowWindow(g_hwndMain, iCmdShow);

	/* Success! */
	return 0;
}


/* FrameWndProc
 *   MDI Frame Window procedure. Called by the message loop.
 *
 * Parameters:
 *   HWND		hwnd		Window handle for message.
 *   UINT		uiMessage	ID for the message.
 *   WPARAM		wParam		Message-specific.
 *   LPARAM		lParam		Message-specific.
 *
 * Return value: LRESULT
 *   Message-specific.
 */

static LRESULT CALLBACK FrameWndProc(HWND hwnd, UINT uiMessage, WPARAM wParam, LPARAM lParam)
{
	CLIENTCREATESTRUCT clientcreate;
	HWND hwndChild;

	switch(uiMessage)
	{
	case WM_CREATE:

		/* Create the MDI client window -- that's the bit where the child windows
		 * appear.
		 */
		clientcreate.hWindowMenu  = g_hmenuNodocWin;
		clientcreate.idFirstChild = IDM_FIRST_CHILD;

		g_hwndClient = CreateWindowEx(WS_EX_CLIENTEDGE, TEXT ("MDICLIENT"), NULL,
									WS_CHILD | WS_CLIPCHILDREN | WS_VISIBLE,
									0, 0, 0, 0, hwnd, (HMENU) 1, g_hInstance,
									(LPSTR)&clientcreate);

		/* Create status bar. */
		CreateStatusBar(hwnd);

		/* Create tool windows. */
		CreateWadListWindow(hwnd);
		CreateInfoBarWindow(hwnd);
		CreateToolbarWindow(hwnd);

		/* Load context menus. */
		LoadPopupMenus();


		/* We can process WM_DROPFILES. */
		DragAcceptFiles(hwnd, TRUE);

		return 0;

	/* The docking library needs to process this message. */
	case WM_NCACTIVATE:
		{
			DOCKPARAMS	dockParams;

			dockParams.container = dockParams.hwnd = hwnd;
			dockParams.wParam = wParam;
			dockParams.lParam = lParam;
			return(DockingActivate(&dockParams));
		}

	/* The docking library needs to process this message. */
	case WM_ENABLE:
		{
			DOCKPARAMS	dockParams;

			dockParams.container = dockParams.hwnd = hwnd;
			dockParams.wParam = wParam;
			dockParams.lParam = lParam;
			return(DockingEnable(&dockParams));
		}

	case WM_SIZE:

		{
			int		cySBHeight;
			RECT	rcStatusBar;
			HDWP	hdwp;
			RECT	rect;

			// Do the default handling of this message
			//DefFrameProc(hwnd, MainWindow, msg, wParam, lParam);

			/* Resize the status bar. */
			SendMessage(g_hwndStatusBar, WM_SIZE, 0, 0);

			/* Get the status bar's height. */
			GetWindowRect(g_hwndStatusBar, &rcStatusBar);
			cySBHeight = rcStatusBar.bottom - rcStatusBar.top;

			/* Set the area where Docking Frame windows are allowed. */
			rect.left = rect.top = 0;
			rect.right = LOWORD(lParam);
			rect.bottom = HIWORD(lParam) - cySBHeight;

			/* Allocate enough space for all Docking Frames which are actually
			 * docked.
			 */
			hdwp = BeginDeferWindowPos(DockingCountFrames(hwnd, 1) + 1);

			/* Position the docked Docking Frame windows for this container
			 * window. rect will be modified to contain the "inner" client
			 * rectangle, where we can position an MDI client.
			 */
			DockingArrangeWindows(hwnd, hdwp, &rect);

			/* Here we resize our MDI client window so that it fits into the area
			 * described by "rect". Do not let it extend outside of this
			 * area or it (and the client windows inside of it) will be obscured
			 * by docked toolwindows (or vice versa).
			 */
			DeferWindowPos(hdwp, g_hwndClient, 0, rect.left, rect.top, rect.right - rect.left, rect.bottom - rect.top, SWP_NOACTIVATE|SWP_NOZORDER);

			EndDeferWindowPos(hdwp);

			/* Resize the MDI client window. */
			/*if(wParam != SIZE_MINIMIZED)
				MoveWindow(g_hwndClient, 0, 0, LOWORD(lParam), HIWORD(lParam) - cySBHeight, FALSE);*/

			/* DefFrameProc doesn't get this message, because we handle resizing the
			 * MDI client window ourselves.
			 */
			return 0;
		}


	/* Handle menu commands. */
	case WM_COMMAND:

		/* MRU first, since it's a range of values. */
		if(LOWORD(wParam) >= ID_FILE_MRU_FILE1 && LOWORD(wParam) < ID_FILE_MRU_FILE1 + MRUENTRIES)
		{
			/* Allocate space for the filename. */
			LPTSTR szIndices[MRUENTRIES] = {TEXT("1"), TEXT("2"), TEXT("3"), TEXT("4"), TEXT("5"), TEXT("6"), TEXT("7"), TEXT("8"), TEXT("9"), TEXT("10")};
			CONFIG *lpcfgRecent = ConfigGetSubsection(g_lpcfgMain, OPT_RECENT);
			UINT cchFilename = ConfigGetStringLength(lpcfgRecent, szIndices[LOWORD(wParam) - ID_FILE_MRU_FILE1]) + 1;
			LPTSTR szFilename = ProcHeapAlloc(cchFilename * sizeof(TCHAR));

			/* Get the filename. */
			ConfigGetString(lpcfgRecent, szIndices[LOWORD(wParam) - ID_FILE_MRU_FILE1], szFilename, cchFilename);

			/* Open the file. */
			OpenWadForEditing(szFilename);

			/* Free the buffer. */
			ProcHeapFree(szFilename);

			/* Finished processing WM_COMMAND. */
			return 0;
		}

		switch(LOWORD(wParam))
		{
		/* File menu. ------------------------------------------------------- */
		case IDM_FILE_NEW:
			{
				CONFIG *lpcfgMap = GetDefaultMapConfig();
				MAP *lpmap = AllocateMapStructure();
				int iIWad = GetIWadForConfig(lpcfgMap);
				int iWad = NewWad();

				/* TODO: Display the map properties dialogue to allow the user
				 * to choose the map config.
				 */

				/* Create an empty MAP01 so that it shows up in the wad-list,
				 * and then add the wad to the list.
				 */
				UpdateMapToWadStructure(lpmap, GetWad(iWad), "MAP01", lpcfgMap);
				AddToWadList(iWad, lpcfgMap);

				/* Create and show a window for the map. */
				ShowWindow(CreateMapWindow(lpmap, "MAP01", lpcfgMap, iWad, iIWad), SW_SHOW);
			}
			return 0;

		case IDM_FILE_OPEN:
			{
				TCHAR szFileName[MAX_PATH];
				TCHAR szFilter[128];

				/* Load the filter from the string table. */
				LoadAndFormatFilterString(IDS_WADFILEFILTER, szFilter, sizeof(szFilter)/sizeof(TCHAR));

				/* Make sure the user doesn't cancel. */
				if(CommDlgOpen(hwnd, szFileName, sizeof(szFileName)/sizeof(TCHAR), NULL, szFilter, TEXT("wad"), NULL, OFN_EXPLORER | OFN_FILEMUSTEXIST | OFN_HIDEREADONLY))
				{
					OpenWadForEditing(szFileName);
				}
			}

			return 0;

		case IDM_FILE_EXIT:
			/* Attempt to close the main window, and hence exit. The user'll get
			 * the chance to cancel if there are any unsaved documents.
			 */
			SendMessage(hwnd, WM_CLOSE, 0, 0);

			return 0;

		/* Window menu. ----------------------------------------------------- */
		case IDM_WINDOW_CLOSE:
			/* Close active child window. */
			hwndChild = (HWND)SendMessage(g_hwndClient, WM_MDIGETACTIVE, 0, 0);
			if(IsWindow(hwndChild)) SendMessage(hwndChild, WM_CLOSE, wParam, lParam);
			return 0;

		case IDM_WINDOW_CLOSEALL:
			/* Close all active child windows. The user may cancel. */
			CloseAllChildWindows();
			return 0;

		case IDM_WINDOW_TILEHORIZONTALLY:
			SendMessage(g_hwndClient, WM_MDITILE, MDITILE_HORIZONTAL, 0);
			return 0;

		case IDM_WINDOW_TILEVERTICALLY:
			SendMessage(g_hwndClient, WM_MDITILE, MDITILE_VERTICAL, 0);
			return 0;

		case IDM_WINDOW_CASCADE:
			SendMessage(g_hwndClient, WM_MDICASCADE, 0, 0);
			return 0;

		case IDM_WINDOW_ARRANGEICONS:
			SendMessage(g_hwndClient, WM_MDIICONARRANGE, 0, 0);
			return 0;

		/* Help menu. ------------------------------------------------------- */
		case IDM_HELP_WIKI:
			WikiMainPage();
			return 0;

		case IDM_HELP_ABOUT:
			DialogBox(g_hInstance, MAKEINTRESOURCE(IDD_ABOUT), hwnd, AboutDlgProc);
			return 0;

		default:
			/* Pass to active child window if we don't know what to do with it. */
			hwndChild = (HWND)SendMessage(g_hwndClient, WM_MDIGETACTIVE, 0, 0);
			if(IsWindow(hwndChild)) SendMessage(hwndChild, WM_COMMAND, wParam, lParam);

			/* Let DefFrameProc have a shot at it, too. */
			break;
		}

		/* Delegate to DefFrameProc. Necessary! */
		break;

	case WM_NOTIFY:
		{
			LPNMHDR lpnmhdr = (LPNMHDR)lParam;
			switch(lpnmhdr->idFrom)
			{
			case IDC_STATUSBAR:
				if(lpnmhdr->code == (unsigned int)NM_CLICK)
				{
					switch(((LPNMMOUSE)lpnmhdr)->dwItemSpec)
					{
					case SBP_GRID:
						/* Configure the grid for the active child. */
						hwndChild = (HWND)SendMessage(g_hwndClient, WM_MDIGETACTIVE, 0, 0);
						if(IsWindow(hwndChild)) GridProperties(hwndChild);
						return 0;
					}
				}

				break;

			default:
				/* Tooltips don't play nicely with IDs, so we process them here.
				 */
				switch(lpnmhdr->code)
				{
				case TTN_GETDISPINFO:
					{
						LPTOOLTIPTEXT lpttt = (LPTOOLTIPTEXT)lParam;
						lpttt->hinst = g_hInstance;

						/* Set the string table ID. TODO: Move to toolbar.c? */
						switch(lpttt->hdr.idFrom)
						{
							case IDM_FILE_NEW:				lpttt->lpszText = MAKEINTRESOURCE(IDS_TT_NEW); break;
							case IDM_FILE_OPEN:				lpttt->lpszText = MAKEINTRESOURCE(IDS_TT_OPEN); break;
							case IDM_FILE_SAVE:				lpttt->lpszText = MAKEINTRESOURCE(IDS_TT_SAVE); break;
							case IDM_EDIT_CUT:				lpttt->lpszText = MAKEINTRESOURCE(IDS_TT_CUT); break;
							case IDM_EDIT_COPY:				lpttt->lpszText = MAKEINTRESOURCE(IDS_TT_COPY); break;
							case IDM_EDIT_PASTE:			lpttt->lpszText = MAKEINTRESOURCE(IDS_TT_PASTE); break;
							case IDM_EDIT_FIND:				lpttt->lpszText = MAKEINTRESOURCE(IDS_TT_FIND); break;
							case IDM_EDIT_REPLACE:			lpttt->lpszText = MAKEINTRESOURCE(IDS_TT_REPLACE); break;
							case IDM_PROPERTIES:			lpttt->lpszText = MAKEINTRESOURCE(IDS_TT_PROPERTIES); break;
							case IDM_VIEW_MODE_ANY:			lpttt->lpszText = MAKEINTRESOURCE(IDS_TT_ANYMODE); break;
							case IDM_VIEW_MODE_LINES:		lpttt->lpszText = MAKEINTRESOURCE(IDS_TT_LINESMODE); break;
							case IDM_VIEW_MODE_SECTORS:		lpttt->lpszText = MAKEINTRESOURCE(IDS_TT_SECTORSMODE); break;
							case IDM_VIEW_MODE_VERTICES:	lpttt->lpszText = MAKEINTRESOURCE(IDS_TT_VERTICESMODE); break;
							case IDM_VIEW_MODE_THINGS:		lpttt->lpszText = MAKEINTRESOURCE(IDS_TT_THINGSMODE); break;
							case IDM_CANCEL:				lpttt->lpszText = MAKEINTRESOURCE(IDS_TT_POINTER); break;
							case IDM_VIEW_MODE_MOVE:		lpttt->lpszText = MAKEINTRESOURCE(IDS_TT_MOVE); break;
							case IDM_INSERTLINES:			lpttt->lpszText = MAKEINTRESOURCE(IDS_TT_INSERTLINES); break;
							case IDM_INSERTRECT:			lpttt->lpszText = MAKEINTRESOURCE(IDS_TT_INSERTRECT); break;
							case IDM_INSERTVERTEX:			lpttt->lpszText = MAKEINTRESOURCE(IDS_TT_INSERTVERTEX); break;
							case IDM_INSERTTHING:			lpttt->lpszText = MAKEINTRESOURCE(IDS_TT_INSERTTHING); break;

							/* Undo and redo need special handling. */
							case IDM_EDIT_UNDO:
							case IDM_EDIT_REDO:
								{
									HWND hwndChild = (HWND)SendMessage(g_hwndClient, WM_MDIGETACTIVE, 0, 0);
									if(IsWindow(hwndChild))
									{
										lpttt->hinst = NULL;

										/* Build the text for undo or redo, as
										 * appropriate.
										 */
										(lpttt->hdr.idFrom == IDM_EDIT_UNDO ? MakeUndoText : MakeRedoText)(hwndChild, lpttt->szText, sizeof(lpttt->szText) / sizeof(TCHAR));
									}
									else
									{
										/* Default to a simple string if we've no
										 * child window.
										 */
										lpttt->lpszText = MAKEINTRESOURCE(lpttt->hdr.idFrom == IDM_EDIT_UNDO ? IDS_TT_UNDO : IDS_TT_REDO);
									}
								}

								break;
						}

						return 0;
					}

					break;
				}

				break;
			}
		}

		break;

	case WM_TAKEFOCUS:
		SetFocus(g_hwndClient);
		return 0;

	case WM_INITMENUPOPUP:
		/* MRU? */

		if(!HIWORD(lParam) && LOWORD(lParam) == GetFileMenuPos())
		{
			MRUBuildMenu();
			return 0;
		}

		/* Fall through. */

	case WM_EXITMENULOOP:
	
		/* Pass to active child window. */
		hwndChild = (HWND)SendMessage(g_hwndClient, WM_MDIGETACTIVE, 0, 0);
		if(IsWindow(hwndChild))
			return SendMessage(hwndChild, uiMessage, wParam, lParam);

		break;


	case WM_DROPFILES:

		{
			LPTSTR szFilename;
			int iBufLenTC, iNewSize, iNumFiles;
			int i;

			/* How many files? */
			iNumFiles = DragQueryFile((HDROP)wParam, 0xFFFFFFFF, NULL, 0);

			/* Allocate some room to begin with. We realloc if we need more. */
			iBufLenTC = 256;
			szFilename = ProcHeapAlloc(iBufLenTC * sizeof(TCHAR));

			/* Loop for each file. */
			for(i=0; i<iNumFiles; i++)
			{
				/* Get the size of buffer we need, and allocate more space if
				 * necessary.
				 */
				iNewSize = DragQueryFile((HANDLE)wParam, i, NULL, 0) + 1;
				if(iNewSize > iBufLenTC)
				{
					iBufLenTC = iNewSize;
					ProcHeapFree(szFilename);
					szFilename = ProcHeapAlloc(iBufLenTC * sizeof(TCHAR));
				}

				/* Get filename. */
				DragQueryFile((HANDLE)wParam, i, szFilename, iNewSize);

				/* Open the file. */
				OpenWadForEditing(szFilename);
			}

			ProcHeapFree(szFilename);

			DragFinish((HDROP)wParam);

			return 0;
		}


	/* User requested exit. */
	case WM_QUERYENDSESSION:
	case WM_CLOSE:
		/* Attempt to close all wads, closing all children in the process, and
		 * then close the main window if successful.
		 */
		if(CloseAllChildWindows())
		{
			/* Bow out. */
			CloseMainWindow();

			if(LOWORD(wParam) == WM_QUERYENDSESSION)
				return TRUE;
		}

		return 0;

	case WM_DESTROY:

		/* Free context menus. */
		LoadPopupMenus();

		/* Child windows have been destroyed by the time we get here. */
		PostQuitMessage(0);
		break;
	}

	return DefFrameProc(hwnd, g_hwndClient, uiMessage, wParam, lParam);
}


/* CloseAllChildWindows
 *   Attempts to close all MDI child windows.
 *
 * Parameters:
 *   None
 *
 * Return value:
 *   BOOL		TRUE if successful; FALSE otherwise (user may cancel).
 *
 * Remarks:
 *   Doesn't actually close wads -- only map windows.
 */
static BOOL CloseAllChildWindows(void)
{
	/* Attempt to close all child windows. */
	EnumChildWindows(g_hwndClient, CloseEnumProc, 0);

	/* Are there any still remaining? If so, the user cancelled the operation.
	 */
	return !GetWindow(g_hwndClient, GW_CHILD);
}


/* CloseEnumProc
 *   Called once for each child window when attempting to close them all.
 *   Used by EnumChildWindows.
 *
 * Parameters:
 *   HWND	hwnd	Handle of child window.
 *	 LPARAM	lParam	Unused.
 *
 * Return value:
 *   BOOL		TRUE if enum should continue; FALSE otherwise (user may cancel).
 *
 * Remarks:
 *   Based closely on C. Petzold, "Programming Windows, 5ed.", MS Press.
 *   Also used by routines that close all windows belonging to a certain wad.
 */
BOOL CALLBACK CloseEnumProc(HWND hwnd, LPARAM lParam)
{
     UNREFERENCED_PARAMETER(lParam);
     if(GetWindow(hwnd, GW_OWNER))
          return TRUE;

     if(!SendMessage(hwnd, WM_CLOSE, 0, 0))
          return TRUE;

     SendMessage(GetParent(hwnd), WM_MDIDESTROY, (WPARAM)hwnd, 0);

     return TRUE;
}



/* CloseMainWindow
 *   Closes the MDI frame window immediately, cleaning up as it goes.
 *
 * Parameters:
 *   None
 *
 * Return value:
 *   None
 *
 * Remarks:
 *   This will eventually exit the app.
 */
static void CloseMainWindow(void)
{
	/* Close the tool windows. */
	DestroyInfoBarWindow();
	DestroyWadListWindow();

	/* Destroy the status bar. */
	DestroyWindow(g_hwndStatusBar);

	/* Destroy *unattached* menus. */
	DestroyMenu(g_hmenuMap);

	DestroyMainWindow();
}


/* DestroyMainWindow
 *   Destroys the MDI frame window immediately.
 *
 * Parameters:
 *   None
 *
 * Return value:
 *   None
 *
 * Remarks:
 *   This should only be called after cleanup, or when we're bombing.
 */
void DestroyMainWindow(void)
{
	if(g_hwndMain) DestroyWindow(g_hwndMain);
}


/* OpenWadForEditing
 *   Loads a wad if necessary, and sets up the UI.
 *
 * Parameters:
 *   LPCTRSTR		szFilename		Filename of wad.
 *
 * Return value: BOOL
 *   TRUE on success; FALSE on error.
 *
 * Remarks:
 *   Called when the user selects a file from a comdlg, with command line args
 *   at startup or on a drag-and-drop. Adds the file to the MRU.
 */
BOOL OpenWadForEditing(LPCTSTR szFilename)
{
	/* This gets the wad's ID, opening it if necessary. */
	int iWad = LoadWad(szFilename);
	char szLumpName[CCH_LUMPNAME + 1];
	CONFIG *lpcfgMap;

	/* Get the wad's options, creating new ones if necessary. */
	CONFIG *lpcfgWad = GetWadOptions(iWad);

	if(iWad < 0)
	{
		MessageBoxFromStringTable(g_hwndMain, IDS_BADWAD, MB_ICONERROR);
		return FALSE;
	}

	/* Let the user select which map to open from the wad. Make sure he doesn't
	 * cancel.
	 */
	if(SelectMap(iWad, szLumpName, &lpcfgMap, lpcfgWad))
	{
		ENUM_OMIW_RETURN omiwret;

		/* New map? */
		if(*szLumpName == '\0')
		{
			WAD *lpwad = GetWad(iWad);

			if(GetFirstFreeMapLumpname(lpwad, szLumpName) == 0)
			{
				/* Create a dummy map structure and add it to the wad. */
				MAP *lpmap = AllocateMapStructure();
				UpdateMapToWadStructure(lpmap, lpwad, szLumpName, GetDefaultMapConfigForWad(iWad));
				DestroyMapStructure(lpmap);

				/* Add new node to the tree. If the tree for this wad doesn't
				 * exist yet, this fails, but that's okay, because in that case,
				 * the map'll be added when the tree is populated anyway.
				 */
				AddMapToTree(iWad, szLumpName);
			}
			else
			{
				MessageBoxFromStringTable(g_hwndMain, IDS_ERROR_NOFREEMAP, MB_ICONERROR);
				ReleaseWad(iWad);
				return FALSE;
			}
		}

		omiwret = OpenMapInWindow(iWad, szLumpName, lpcfgMap);
		
		switch(omiwret)
		{
		case OMIWRET_FAILED:
			ReleaseWad(iWad);
			return FALSE;

		case OMIWRET_EXISTINGWINDOW:
			/* If we used an existing window, we're also using its reference. */
			ReleaseWad(iWad);
			break;

		default:
			break;
		}
		
		/* Save the name of the map config in the wad options so we can load it
		 * next time the wad is opened.
		 */
		StoreMapCfgForWad(lpcfgWad, lpcfgMap);

		/* Add to wad list. */
		AddToWadList(iWad, lpcfgMap);

		/* Add to the MRU. */
		MRUAdd(szFilename);
	}

	/* Even if we didn't open a map because the user told us not to, we succeed.
	 * The wad was still opened, remember.
	 */
	return TRUE;
}


/* OpenMapInWindow
 *   Opens a map in a new child window, or activates an existing window in which
 *   that map is open.
 *
 * Parameters:
 *   int	iWad			Wad ID for which to list maps.
 *   LPSTR	szLumpName		Map lumpname.
 *   CONFIG	*lplpcfgMap		Map config to use.
 *
 * Return value: ENUM_OMIW_RETURN
 *   OMIWRET_FAILED on error; OMIWRET_NEWWINDOW if the map was opened in a new
 *   window; OMIWRET_EXISTINGWINDOW if there was an existing window.
 */
ENUM_OMIW_RETURN OpenMapInWindow(int iWad, LPSTR szLumpName, CONFIG *lpcfgMap)
{
	int iIWad;
	MAP *lpmap;
	ACT_MAPWIN_DATA amd;

	/* If the map's already open, just activate its window. */
	amd.szMapname = szLumpName;
	amd.bFound = FALSE;
	EnumChildWindowsByWad(iWad, ActivateMapWindowEnumProc, (LPARAM)&amd);
	if(amd.bFound) return OMIWRET_EXISTINGWINDOW;

	/* Load the specified map. */
	lpmap = AllocateMapStructure();
	if(MapLoad(lpmap, iWad, szLumpName) < 0)
	{
		MessageBoxFromStringTable(g_hwndMain, IDS_BADMAP, MB_ICONERROR);
		DestroyMapStructure(lpmap);
		return OMIWRET_FAILED;
	}

	/* Load the IWAD. */
	iIWad = GetIWadForConfig(lpcfgMap);

	/* Open a new window. */
	ShowWindow(CreateMapWindow(lpmap, szLumpName, lpcfgMap, iWad, iIWad), SW_SHOW);
	return OMIWRET_NEWWINDOW;
}


/* CreateStatusBar
 *   Creates the status bar for the main window.
 *
 * Parameters:
 *   HWND	hwndParent	Handle to main window.
 *
 * Return value: HWND
 *   Status bar's window handle.
 *
 * Remarks:
 *   We haven't set the global yet, which is why we pass the frame window's
 *   handle. The bar is initially in simple mode.
 */
static void CreateStatusBar(HWND hwndParent)
{
	g_hwndStatusBar = CreateStatusWindow(WS_CHILD | WS_VISIBLE | SBARS_SIZEGRIP, g_szAppName, hwndParent, IDC_STATUSBAR);

	/* Simple mode. */
	StatusBarNoWindow();
}


/* StatusBarNoWindow
 *   Sets the status bar to correspond to having no child windows.
 *
 * Parameters:
 *   None.
 *
 * Return value: None.
 */
void StatusBarNoWindow(void)
{
	/* Simple mode. */
	SendMessage(g_hwndStatusBar, SB_SIMPLE, TRUE, 0);
	SendMessage(g_hwndStatusBar, SB_SETTEXT, 255, (LPARAM)g_szAppName);
}


/* MRUBuildMenu
 *   Builds the File menu's MRU list.
 *
 * Parameters: None.
 *
 * Return value: None.
 */
static void MRUBuildMenu(void)
{
	CONFIG *lpcfgRecent = ConfigGetSubsection(g_lpcfgMain, OPT_RECENT);
	LPTSTR szIndices[MRUENTRIES] = {TEXT("1"), TEXT("2"), TEXT("3"), TEXT("4"), TEXT("5"), TEXT("6"), TEXT("7"), TEXT("8"), TEXT("9"), TEXT("10")};
	int i;
	MENUITEMINFO mii;
	HMENU hmenuFile = GetSubMenu(GetMenu(g_hwndMain), GetFileMenuPos());
	int iMRUPos;
	int iCount, iMRUCount;

	/* Find MRU position. */
	iCount = GetMenuItemCount(hmenuFile);
	for(iMRUPos = 0;
		iMRUPos < iCount && (int)GetMenuItemID(hmenuFile, iMRUPos) != ID_FILE_MRU_FILE1;
		iMRUPos++);

	/* Find how many MRU items we have. */
	for(iMRUCount = 1;
		iMRUCount < MRUENTRIES && (int)GetMenuItemID(hmenuFile, iMRUPos + iMRUCount) == ID_FILE_MRU_FILE1 + iMRUCount;
		iMRUCount++);

	/* Fill in the common members. */
	mii.cbSize = sizeof(mii);
	mii.fMask = MIIM_TYPE | MIIM_ID;
	mii.fType = MFT_STRING;

	for(i = 0; i < (int)NUM_ELEMENTS(szIndices) && ConfigNodeExists(lpcfgRecent, szIndices[i]); i++)
	{
		/* Get the filename. */
		UINT cchExistingFilename = ConfigGetStringLength(lpcfgRecent, szIndices[i]) + 1 + MRU_LEADER_CHARS;
		LPTSTR szExistingFilename = ProcHeapAlloc(cchExistingFilename * sizeof(TCHAR));
		int iPrefixLen;

		/* Build the prefix for the MRU entry. */
		if(i < 9)
			_sntprintf(szExistingFilename, MRU_LEADER_CHARS + 1,
				TEXT("&%d "),
				(i + 1) % 10);
		else
			_sntprintf(szExistingFilename, MRU_LEADER_CHARS + 1,
				TEXT("%d%s%d "),
				(i + 1) / 10,
				i < 10 ? "&" : "",
				(i + 1) % 10);

		/* Terminate the string in case _sntprintf didn't. */
		szExistingFilename[MRU_LEADER_CHARS] = TEXT('\0');

		/* Append the filename. */
		iPrefixLen = _tcslen(szExistingFilename);
		ConfigGetString(lpcfgRecent, szIndices[i], szExistingFilename + iPrefixLen, cchExistingFilename - iPrefixLen);

		/* Properties for this menu item. */
		mii.dwTypeData = szExistingFilename;
		mii.wID = ID_FILE_MRU_FILE1 + i;

		/* If the position in the menu is not an MRU item, make it so; otherwise
		 * we just need to set the text.
		 */
		if(iMRUPos + i >= iCount || (int)GetMenuItemID(hmenuFile, iMRUPos + i) != ID_FILE_MRU_FILE1 + i)
			InsertMenuItem(hmenuFile, iMRUPos + i, TRUE, &mii);
		else SetMenuItemInfo(hmenuFile, iMRUPos + i, TRUE, &mii);

		ProcHeapFree(szExistingFilename);
	}

	/* We always have the first item, but if there's no item in the MRU, we set
	 * it to the disabled "Recent Files" item.
	 */
	if(i == 0)
	{
		TCHAR szRecentPlaceholder[RECENTPLACEHOLDER_BUFLEN];
		LoadString(g_hInstance, IDS_RECENTFILES, szRecentPlaceholder, sizeof(szRecentPlaceholder) / sizeof(TCHAR));

		/* Disable it and set its text. */
		mii.fMask = MIIM_STATE | MIIM_TYPE;
		mii.fState = MFS_GRAYED;
		mii.dwTypeData = szRecentPlaceholder;
		SetMenuItemInfo(hmenuFile, iMRUPos, TRUE, &mii);

		/* Don't want to delete it. */
		i++;
	}
	else
	{
		/* The first item might still be disabled from last time. */
		mii.fMask = MIIM_STATE;
		mii.fState = MFS_ENABLED;
		SetMenuItemInfo(hmenuFile, iMRUPos, TRUE, &mii);
	}

	/* Delete the rest of them. */
	for(; i < iMRUCount; i++)
		DeleteMenu(hmenuFile, iMRUPos + i, MF_BYPOSITION);
}


/* GetFileMenuPos
 *   Gets the position of the File menu, taking the MDI control menu into
 *   account.
 *
 * Parameters: None.
 *
 * Return value: int
 *   Position of the File menu.
 */
static __inline int GetFileMenuPos(void)
{
	/* Get active child window. */
	HWND hwndChild = (HWND)SendMessage(g_hwndClient, WM_MDIGETACTIVE, 0, 0);
	
	return FILEMENUPOS + (IsWindow(hwndChild) && IsZoomed(hwndChild)) ? 1 : 0;
}


/* ActivateMapWindowEnumProc
 *   If the specified map window is editing the specified map, activates it.
 *
 * Parameters:
 *   HWND	hwndMap		Map window handle.
 *   LPARAM	lParam		(ACT_MAPWIN_DATA*) Lumpname to check, and has a flag
 *						that is set if we matched (but not touched otherwise).
 *
 * Return value: BOOL
 *   TRUE to keep looking; FALSE to stop.
 *
 * Remarks:
 *   Intended for use with EnumChildWindowsByWad.
 */
static BOOL CALLBACK ActivateMapWindowEnumProc(HWND hwndMap, LPARAM lParam)
{
	ACT_MAPWIN_DATA *lpamd = (ACT_MAPWIN_DATA*)lParam;

	if(LumpnameMatches(hwndMap, lpamd->szMapname))
	{
		/* We matched, so activate, set the flag and stop looking. */
		SendMessage(g_hwndClient, WM_MDIACTIVATE, (WPARAM)hwndMap, 0);
		lpamd->bFound = TRUE;
		return FALSE;
	}

	/* Keep looking. */
	return TRUE;
}


/* DeferTakeFocus
 *   Posts a WM_TAKEFOCUS to the frame window, causing the client to take the
 *   focus eventually.
 *
 * Parameters:
 *   None.
 *
 * Return value: None.
 *
 * Remarks:
 *   This is only necessary because tree views steal the focus after a click.
 */
void DeferTakeFocus(void)
{
	PostMessage(g_hwndMain, WM_TAKEFOCUS, 0, 0);
}


/* UpdateMenuShortcutKeys
 *   Recursively updates a menu's items to include shortcut key text.
 *
 * Parameters:
 *   HMENU	hmenu	Menu handle.
 *
 * Return value: None.
 */
void UpdateMenuShortcutKeys(HMENU hmenu)
{
	const int iCount = GetMenuItemCount(hmenu);
	int i;
	MENUITEMINFO mii;
	UINT cchBuffer = 64;
	LPTSTR szBuffer = (LPTSTR)ProcHeapAlloc(cchBuffer * sizeof(TCHAR));

	mii.cbSize = sizeof(mii);
	mii.fMask = MIIM_TYPE | MIIM_SUBMENU | MIIM_ID;

	for(i = 0; i < iCount; i++)
	{
		/* Is the buffer big enough? */
		mii.dwTypeData = NULL;
		GetMenuItemInfo(hmenu, i, TRUE, &mii);
		mii.cch++;
		if(mii.cch > cchBuffer)
		{
			/* Buffer too small; make it bigger. */
			mii.cch = cchBuffer = mii.cch << 1;
			szBuffer = (LPTSTR)ProcHeapReAlloc(szBuffer, cchBuffer);
		}
		
		/* Actually get the string this time. */
		mii.dwTypeData = szBuffer;
		GetMenuItemInfo(hmenu, i, TRUE, &mii);

		/* If this item is a submenu, recur. */
		if(mii.hSubMenu)
			UpdateMenuShortcutKeys(mii.hSubMenu);
		else
		{
			/* Not a submenu, so try to update the string. */
			int iKeyCode = GetKeycodeFromMenuID(mii.wID);

			if(iKeyCode != 0)
			{
				TCHAR szShortcutKey[64];
				LPTSTR szInString = szBuffer;
				UINT cchMenuText, cchMenuTail;

				/* Generate shortcut key text. */
				GetShiftedShortcutText(iKeyCode, szShortcutKey, NUM_ELEMENTS(szShortcutKey));

				/* Append to menu text, expanding buffer if necessary. The 2
				 * characters are the tab and the zero terminator.
				 */
				cchMenuTail = (UINT)(_tcslen(szShortcutKey) + 2);
				cchMenuText = (UINT)(_tcslen(mii.dwTypeData) + cchMenuTail);
				if(cchMenuText > cchBuffer)
				{
					/* Buffer too small; make it bigger. */
					cchBuffer = cchMenuText << 1;
					szBuffer = (LPTSTR)ProcHeapReAlloc(szBuffer, cchBuffer);
				}

				while(*szInString && *szInString != TEXT('\t')) szInString++;
				_sntprintf(szInString, cchMenuTail, TEXT("\t%s"), szShortcutKey);

				/* Update the menu item. */
				SetMenuItemInfo(hmenu, i, TRUE, &mii);
			}
		}
	}

	ProcHeapFree(szBuffer);
}
