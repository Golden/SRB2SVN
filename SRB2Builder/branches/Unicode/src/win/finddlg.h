#ifndef __SRB2B_FINDWIN__
#define __SRB2B_FINDWIN__

#include "../texture.h"

/* Prototypes. */
void ShowFindDlg(HWND hwndMap, BOOL bUseNybbledSectorEffects, TEXTURENAMELIST *lptnlFlatsAll, TEXTURENAMELIST *lptnlTexAll, CONFIG *lpcfgFlatsUsed, CONFIG *lpcfgTexUsed, HIMAGELIST *lphimlFlatCache, HIMAGELIST *lphimlTexCache, CONFIG *lpcfgMap);
BOOL ShowReplaceDlg(HWND hwndMap, BOOL bUseNybbledSectorEffects, TEXTURENAMELIST *lptnlFlatsAll, TEXTURENAMELIST *lptnlTexAll, CONFIG *lpcfgFlatsUsed, CONFIG *lpcfgTexUsed, HIMAGELIST *lphimlFlatCache, HIMAGELIST *lphimlTexCache, CONFIG *lpcfgMap);


#endif
