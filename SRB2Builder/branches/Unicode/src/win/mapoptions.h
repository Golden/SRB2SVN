#ifndef __SRB2B_MAPOPTIONS__
#define __SRB2B_MAPOPTIONS__

#include <windows.h>
#include <commctrl.h>

#include "../config.h"
#include "../texture.h"

typedef struct _MAPOPTPPDATA
{
	CONFIG			*lpcfgMap, *lpcfgWadOptMap;
	char			szLumpName[9];
	TCHAR			szAddWadName[MAX_PATH];
	BOOL			bOKed;
	HWND			hwndMap;
	HIMAGELIST		*lphimlCacheTextures, *lphimlCacheFlats;
	TEXTURENAMELIST	*lptnlTextures, *lptnlFlats;
	CONFIG			*lpcfgTexUsed, *lpcfgFlatsUsed;
	WAD				*lpwad;
} MAPOPTPPDATA;


BOOL ShowMapProperties(MAPOPTPPDATA *lpmapoptppdata);
void PopulateGametypeList(HWND hwndCombo);


#endif
