#include <windows.h>
#include <string.h>
#include <tchar.h>

#include "../general.h"
#include "../../res/resource.h"
#include "../openwads.h"
#include "../map.h"
#include "../config.h"
#include "../mapconfig.h"
#include "../wad.h"
#include "../wadopts.h"

#include "mapselect.h"
#include "mdiframe.h"
#include "gendlg.h"


#define CCH_NEWMAP 64


/* Types. */

typedef struct _MAPSELECTDLGDATA
{
	int		iWad;
	char	szLumpName[9];
	CONFIG	*lpcfgMap;		/* Map config *CONTAINER* */
	CONFIG	*lpcfgWad;		/* Wad options. */
} MAPSELECTDLGDATA;

typedef enum _ENUM_MAPLIST_MAPTYPES
{
	MAPLIST_EXISTINGMAP,
	MAPLIST_NEWMAP
} ENUM_MAPLIST_MAPTYPES;


/* Static prototypes. */
static BOOL CALLBACK MapSelectProc(HWND hwndDlg, UINT uiMsg, WPARAM wParam, LPARAM lParam);
static void GetListBoxSelLumpnameA(HWND hwndLB, LPSTR szBuffer);
static void SetMapSelectTitle(HWND hwndDlg, int iWad);
static void AddMapNameToListBoxCallback(LPCTSTR szString, void *lpvWindow);



/* SelectMap
 *   Displays the Select Map dialogue.
 *
 * Parameters:
 *   int	iWad			Wad ID for which to list maps.
 *   LPSTR	szLumpName		Buffer to return lump name into. Must be big enough.
 *   CONFIG	**lplpcfgMap	Pointer to specified map config returned here.
 *   CONFIG *lpcfgWad		Wad options.
 *
 * Return value: BOOL
 *   TRUE if user selected a map; FALSE if cancelled.
 */
BOOL SelectMap(int iWad, LPSTR szLumpName, CONFIG **lplpcfgMap, CONFIG *lpcfgWad)
{
	MAPSELECTDLGDATA msdd;

	/* Specify parameters for the dialogue box. */
	msdd.iWad = iWad;
	msdd.lpcfgWad = lpcfgWad;

	/* Display the dialogue box. */
	if(IDCANCEL != DialogBoxParam(g_hInstance, MAKEINTRESOURCE(IDD_MAPSELECT), g_hwndMain, MapSelectProc, (LPARAM)&msdd))
	{
		/* Copy the lumpname and config address, and finish. */
		strcpy(szLumpName, msdd.szLumpName);
		*lplpcfgMap = msdd.lpcfgMap;
		return TRUE;	/* User clicked OK. */
	}

	/* User cancelled. */
	return FALSE;
}



static BOOL CALLBACK MapSelectProc(HWND hwndDlg, UINT uiMsg, WPARAM wParam, LPARAM lParam)
{
	/* There's only ever one of us at a time, so this is okay. */
	static MAPSELECTDLGDATA *s_lpmsdd;
	static HWND s_hwndMapList, s_hwndOK, s_hwndMapConfigs;
	static HBITMAP s_hbmPreview;
	static RECT s_rcPreview;

	switch(uiMsg)
	{
	case WM_INITDIALOG:
		{
			HWND hwndPreview = GetDlgItem(hwndDlg, IDC_PREVIEW);
			HDC hdc;
			TCHAR szNewMap[CCH_NEWMAP];
			int iIndex;

			/* Centre in parent. */
			CentreWindowInParent(hwndDlg);

			/* Get the parameters. */
			s_lpmsdd = (MAPSELECTDLGDATA*)lParam;

			/* Store some things for convenience. */
			s_hwndMapList = GetDlgItem(hwndDlg, IDC_LISTMAPS);
			s_hwndOK = GetDlgItem(hwndDlg, IDOK);
			s_hwndMapConfigs = GetDlgItem(hwndDlg, IDC_COMBOMAPCONFIG);

			/* Create the monochrome preview bitmap. */
			GetClientRect(GetDlgItem(hwndDlg, IDC_PREVIEWSIZE), &s_rcPreview);
			s_hbmPreview = CreateBitmap(s_rcPreview.right - s_rcPreview.left, s_rcPreview.bottom - s_rcPreview.top, 1, 1, NULL);

			/* Clear the preview bitmap. */
			hdc = CreateCompatibleDC(NULL);
			SelectObject(hdc, s_hbmPreview);
			FillRect(hdc, &s_rcPreview, GetStockObject(BLACK_BRUSH));
			DeleteDC(hdc);

			/* Assign the bitmap to the preview static control. */
			SendMessage(hwndPreview, STM_SETIMAGE, IMAGE_BITMAP, (LPARAM)s_hbmPreview);

			/* Fill the map config list. */
			AddMapConfigsToComboBox(s_hwndMapConfigs);

			/* Select an item in the config list. Begin by assuming the first.
			 */
			SendMessage(s_hwndMapConfigs, CB_SETCURSEL, 0, 0);

			/* Do we have a config specified in the wad options? */
			if(ConfigNodeExists(s_lpmsdd->lpcfgWad, WADOPT_MAPCONFIG))
			{
				int i;
				int iCount = SendMessage(s_hwndMapConfigs, CB_GETCOUNT, 0, 0);
				UINT cchConfig = ConfigGetStringLength(s_lpmsdd->lpcfgWad, WADOPT_MAPCONFIG) + 1;
				LPTSTR szConfig = ProcHeapAlloc(cchConfig * sizeof(TCHAR));

				/* Get the desired map config string. */
				ConfigGetString(s_lpmsdd->lpcfgWad, WADOPT_MAPCONFIG, szConfig, cchConfig);

				/* Look for a match. Start at 1 because we assumed 0. */
				for(i = 1; i < iCount; i++)
				{
					CONFIG *lpcfgGame = (CONFIG*)SendMessage(s_hwndMapConfigs, CB_GETITEMDATA, i, 0);
					UINT cchGameID = ConfigGetStringLength(lpcfgGame, MAPCFG_ID) + 1;

					if(cchGameID > 1)
					{
						LPTSTR szGameID = ProcHeapAlloc(cchGameID * sizeof(TCHAR));

						/* Get the ID for this item in the combo box. */
						ConfigGetString(lpcfgGame, MAPCFG_ID, szGameID, cchGameID);

						/* Match? */
						if(!_tcscmp(szGameID, szConfig))
						{
							SendMessage(s_hwndMapConfigs, CB_SETCURSEL, i, 0);
							ProcHeapFree(szGameID);
							break;
						}

						ProcHeapFree(szGameID);
					}
				}

				ProcHeapFree(szConfig);
			}

			/* Fill the list of maps. */
			IterateMapLumpNames(s_lpmsdd->iWad, AddMapNameToListBoxCallback, s_hwndMapList);

			/* Add the "new map" entry. */
			LoadString(g_hInstance, IDS_NEWMAP, szNewMap, NUM_ELEMENTS(szNewMap));
			iIndex = SendMessage(s_hwndMapList, LB_INSERTSTRING, -1, (LPARAM)szNewMap);
			SendMessage(s_hwndMapList, LB_SETITEMDATA, iIndex, MAPLIST_NEWMAP);

			/* Set the caption to include the name of the wad. */
			SetMapSelectTitle(hwndDlg, s_lpmsdd->iWad);

			/* If list isn't empty, select a map and enable the OK button. */
			if(SendMessage(s_hwndMapList, LB_GETCOUNT, 0, 0) > 0)
			{
				/* Select the first map in the list. */
				SendMessage(s_hwndMapList, LB_SETCURSEL, 0, 0);
				SendMessage(hwndDlg, WM_COMMAND, MAKEWPARAM(IDC_LISTMAPS, LBN_SELCHANGE), (LPARAM)s_hwndMapList);

				EnableWindow(s_hwndOK, TRUE);
			}

			return TRUE;		/* Let the system set the focus. */
		}

	/* Notification messages. */
	case WM_COMMAND:
		switch(HIWORD(wParam))
		{
		case BN_CLICKED:
			/* One of the buttons was clicked. */

			switch(LOWORD(wParam))
			{
			case IDOK:
				{
					int iConfigIndex, iMapIndex;

					/* Return the selected map config. */
					iConfigIndex = SendMessage(s_hwndMapConfigs, CB_GETCURSEL, 0, 0);
					s_lpmsdd->lpcfgMap = (CONFIG*)SendMessage(s_hwndMapConfigs, CB_GETITEMDATA, iConfigIndex, 0);

					iMapIndex = SendMessage(s_hwndMapList, LB_GETCURSEL, 0, 0);

					/* Are we dealing with a new map or an existing one? */
					if(SendMessage(s_hwndMapList, LB_GETITEMDATA, iMapIndex, 0) == MAPLIST_EXISTINGMAP)
					{
						/* Return the selected lump name. */
						GetListBoxSelLumpnameA(s_hwndMapList, s_lpmsdd->szLumpName);
					}
					else
					{
						/* New map, which we signal with an empty string. */
						*s_lpmsdd->szLumpName = '\0';
					}

					EndDialog(hwndDlg, IDOK);
				}

				return TRUE;

			case IDCANCEL:
				EndDialog(hwndDlg, IDCANCEL);
				return TRUE;
			}

			break;

		case LBN_DBLCLK:
			/* An item in the map-list was double-clicked. Simulate OK press. */
			SendMessage(hwndDlg, WM_COMMAND, MAKEWPARAM(IDOK, BN_CLICKED), (LPARAM)GetDlgItem(hwndDlg, IDOK));
			return TRUE;

		case LBN_SELCHANGE:
			{
				/* List selection changed. Draw the preview. */

				char szLumpname[9];
				HDC hdc;
				ENUM_MAPLIST_MAPTYPES mlmt;
				int iIndex = SendMessage(s_hwndMapList, LB_GETCURSEL, 0, 0);

				/* Sanity check. */
				if(iIndex < 0) return TRUE;

				hdc = CreateCompatibleDC(NULL);
				SelectObject(hdc, s_hbmPreview);
				SelectObject(hdc, GetStockObject(WHITE_PEN));

				/* Clear. Scale back to pixels first. */
				SetMapMode(hdc, MM_TEXT);
				FillRect(hdc, &s_rcPreview, GetStockObject(BLACK_BRUSH));

				/* Is this a new map or an existing one? */
				mlmt = SendMessage(s_hwndMapList, LB_GETITEMDATA, iIndex, 0);

				/* We only load the necessary bits of the map. */
				if(mlmt == MAPLIST_EXISTINGMAP)
				{
					MAP *lpmap = AllocateMapStructure();
					GetListBoxSelLumpnameA(s_hwndMapList, szLumpname);

					if(MapLoadForPreview(lpmap, s_lpmsdd->iWad, szLumpname) >= 0)
					{
						
						int i;
						short x, y;
						short xMax=-32768, yMax=-32768;
						short xMin=32767, yMin=32767;
						short cxViewport = (short)(s_rcPreview.right - s_rcPreview.left);
						short cyViewport = (short)(s_rcPreview.bottom - s_rcPreview.top);
						unsigned short cxWin, cyWin;

						/* Find necessary extent. */
						for(i=0; i < lpmap->iVertices; i++)
						{
							x = lpmap->vertices[i].x;
							y = lpmap->vertices[i].y;

							if(x > xMax) xMax = x;
							if(y > yMax) yMax = y;

							if(x < xMin) xMin = x;
							if(y < yMin) yMin = y;
						}

						cxWin = xMax - xMin;
						cyWin = yMax - yMin;

						if((int)cxWin * cyViewport < (int)cyWin * cxViewport)
							cxWin = (int)(((double)cxViewport/(double)cyViewport) * cyWin);
						else
							cyWin = (int)(((double)cyViewport/(double)cxViewport) * cxWin);

						/* Scale. */
						SetMapMode(hdc, MM_ANISOTROPIC);
						SetWindowExtEx(hdc, (int)(1.1 * cxWin), (int)(1.1 * cyWin), NULL);
						SetWindowOrgEx(hdc, (xMax + xMin) / 2, (yMax + yMin) / 2, NULL);
						SetViewportExtEx(hdc, cxViewport, -cyViewport, NULL);
						SetViewportOrgEx(hdc, cxViewport >> 1, cyViewport >> 1, NULL);

						for(i=0; i < lpmap->iLinedefs; i++)
						{
							MoveToEx(hdc, lpmap->vertices[lpmap->linedefs[i].v1].x, lpmap->vertices[lpmap->linedefs[i].v1].y, NULL);
							LineTo(hdc, lpmap->vertices[lpmap->linedefs[i].v2].x, lpmap->vertices[lpmap->linedefs[i].v2].y);
						}
					}

					DestroyMapStructure(lpmap);
				}

				DeleteDC(hdc);
				InvalidateRect(GetDlgItem(hwndDlg, IDC_PREVIEW), NULL, FALSE);
			}

		}

		/* Didn't process the WM_COMMAND if we got here. */
		break;


	case WM_DESTROY:
		/* Unassign the preview bitmap. */
		SendMessage(GetDlgItem(hwndDlg, IDC_PREVIEW), STM_SETIMAGE, IMAGE_BITMAP, (LPARAM)NULL);

		/* Destroy the bitmap. */
		DeleteObject(s_hbmPreview);

		/* Message was processed. */
		return TRUE;
	}

	/* Didn't process the message. */
	return FALSE;
}


static void GetListBoxSelLumpnameA(HWND hwndLB, LPSTR szBuffer)
{
	int iIndex = SendMessage(hwndLB, LB_GETCURSEL, 0, 0);
	UINT cchLBItem = SendMessage(hwndLB, LB_GETTEXTLEN, iIndex, 0);
	LPTSTR szBufferT = ProcHeapAlloc((cchLBItem + 1) * sizeof(TCHAR));

	/* Get text of selected item. */
	SendMessage(hwndLB, LB_GETTEXT, iIndex, (LPARAM)szBufferT);

	/* Copy from szBufferT to the caller's buffer, converting to ANSI if
	 * necessary.
	 */
#ifdef _UNICODE
	WideCharToMultiByte(CP_ACP, 0, szBufferT, -1, szBuffer, CCH_LUMPNAME + 1, NULL, NULL);
#else
	strncpy(szBuffer, szBufferT, CCH_LUMPNAME + 1);
	szBuffer[CCH_LUMPNAME] = '\0';
#endif

	ProcHeapFree(szBufferT);
}




/* SetMapSelectTitle
 *   Updates the map-select dialogue's caption.
 *
 * Parameters:
 *   HWND		hwndDlg		Window handle.
 *   int		iWad		Wad ID.
 *
 * Return value: None.
 */
static void SetMapSelectTitle(HWND hwndDlg, int iWad)
{
	TCHAR	szBaseText[64];
	LPTSTR	szPath, szTitle, szFileTitle;
	WORD	cb;

	/* Get the base text (e.g. "Select Map"). */
	LoadString(g_hInstance, IDS_MAPSELECTCAPTION, szBaseText, sizeof(szBaseText) / sizeof(TCHAR));

	/* Allocate buffer for full path, and get it. */
	cb = GetWadFilename(iWad, NULL, 0);
	szPath = (LPTSTR)ProcHeapAlloc(cb * sizeof(TCHAR));
	GetWadFilename(iWad, szPath, cb);

	/* Allocate buffer for file title, and get it. */
	cb = GetFileTitle(szPath, NULL, 0);
	szFileTitle = (LPTSTR)ProcHeapAlloc(cb * sizeof(TCHAR));
	GetFileTitle(szPath, szFileTitle, cb);
	ProcHeapFree(szPath);

	/* Buffer for final caption text. */
	szTitle = (LPTSTR)ProcHeapAlloc((cb + _tcslen(szBaseText) + 3) * sizeof(TCHAR));

	/* Compose the parts. */
	wsprintf(szTitle, TEXT("%s - %s"), szBaseText, szFileTitle);

	SetWindowText(hwndDlg, szTitle);

	ProcHeapFree(szFileTitle);
	ProcHeapFree(szTitle);
}


/* AddMapNameToListBoxCallback
 *   Adds an existing map to the map listbox.
 *
 * Parameters:
 *   LPCTSTR	szString	String to add.
 *   void*		lpvWindow	(HWND) Window handle of listbox.
 *
 * Return value: None.
 *
 * Remarks:
 *   Intended for use with IterateMapLumpNames.
 */
static void AddMapNameToListBoxCallback(LPCTSTR szString, void *lpvWindow)
{
	int iIndex = SendMessage((HWND)lpvWindow, LB_ADDSTRING, 0, (LPARAM)szString);
	SendMessage((HWND)lpvWindow, LB_SETITEMDATA, iIndex, MAPLIST_EXISTINGMAP);
}
