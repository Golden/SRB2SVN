#include <windows.h>
#include <string.h>
#include <tchar.h>

#include "../general.h"
#include "../wad.h"
#include "../openwads.h"
#include "../map.h"
#include "../mapconfig.h"
#include "../wadopts.h"
#include "../../res/resource.h"

#include "wadlist.h"
#include "mdiframe.h"
#include "gendlg.h"
#include "mapwin.h"

#include "../../DockWnd/DockWnd.h"


#define CX_WADLISTICON	16
#define CY_WADLISTICON	16

#define WLIMASKCOLOUR RGB(255, 0, 255)

enum ENUM_WADLISTICONS
{
	WLI_WAD,
	WLI_MAP,
	WLI_MAX
};


typedef struct _WADLIST_WAD_DATA
{
	int		iWad;
	CONFIG*	lpcfgMap;
} WADLIST_WAD_DATA;


typedef enum _ENUM_WADLISTPOPUP
{
	WLP_MAP,
	WLP_WAD
} ENUM_WADLISTPOPUP;


static HWND g_hwndWadlist;
static HWND g_hwndTreeViewWads;

static BOOL CALLBACK WadListProc(HWND hwndDlg, UINT uiMsg, WPARAM wParam, LPARAM lParam);
static void AddMapNameToWadListCallback(LPCTSTR szLumpname, void *lpvParent);
static HTREEITEM FindWadInList(int iWad);
static void UpdateWadTitleInListWithHTI(int iWad, HTREEITEM hti);
static void OpenMapFromTreeNodes(HTREEITEM htiMapNode, HTREEITEM htiParent);
static HTREEITEM AddMapToTreeReturnHTI(int iWad, LPCSTR szLumpname);


/* CreateWadListWindow
 *   Creates the wad-list tool window.
 *
 * Parameters:
 *   HWND	hwndParent	Parent window (i.e. MDI frame window).
 *
 * Return value: BOOL
 *   TRUE on success; FALSE on error.
 *
 * Remarks:
 *   We have to pass the frame window handle since this is called before the
 *   global var is set.
 */
BOOL CreateWadListWindow(HWND hwndParent)
{
	DOCKINFO *lpdi;
	TCHAR	szCaption[64];

	/* Allocate a docking structure. This'll be freed automatically by the lib.
	 */
	lpdi = DockingAlloc((char)DWS_DOCKED_LEFT);

	/* Destroy the dialogue when the docking container is destroyed. Also, hide
	 * the docking container if the user clicks the Close button, rather than
	 * destroying the window.
	 */
	lpdi->dwStyle |= DWS_NODESTROY | DWS_DESTROYFOCUSWIN;

	/* This window'll be destroyed by the lib, too. */
	LoadString(g_hInstance, IDS_WADLIST_CAPTION, szCaption, sizeof(szCaption) / sizeof(TCHAR));
	DockingCreateFrame(lpdi, hwndParent, szCaption);

	/* Now that we've got a frame, we can create the window that will appear
	 * inside it.
	 */
	g_hwndWadlist = lpdi->hwnd;

	lpdi->focusWindow = CreateDialog(g_hInstance, MAKEINTRESOURCE(IDD_WADLIST), lpdi->hwnd, WadListProc);

	/* Get a handle to the tree-view, since we use it quite a bit. */
	g_hwndTreeViewWads = GetDlgItem(lpdi->focusWindow, IDC_WADLIST_TREE);

	DockingShowFrame(lpdi);

	/* TODO: Hide it if the user has turned it off. */

	return TRUE;
}


/* WadListProc
 *   Dialogue procedure for the wad-list dialogue.
 *
 * Parameters:
 *   HWND	hwndDlg		Handle to dialogue window.
 *   HWND	uiMsg		Window message ID.
 *   WPARAM	wParam		Message-specific.
 *   LPARAM lParam		Message-specific.
 *
 * Return value: BOOL
 *   TRUE if message was processed; FALSE otherwise.
 *
 * Remarks:
 *   This is the procedure for the window *inside* the docking container.
 */
static BOOL CALLBACK WadListProc(HWND hwndDlg, UINT uiMsg, WPARAM wParam, LPARAM lParam)
{
	UNREFERENCED_PARAMETER(wParam);

	switch(uiMsg)
	{
	case WM_INITDIALOG:
		{
			/* Create an image list for the wad-list's icons. */
			HIMAGELIST himl = ImageList_Create(CX_WADLISTICON, CY_WADLISTICON, ILC_COLOR24 | ILC_MASK, 0, 0);
			const int cxSMIcon = GetSystemMetrics(SM_CXSMICON);
			const int cySMIcon = GetSystemMetrics(SM_CYSMICON);

			/* Load the icons from the resource. */
			HICON hiconWad = LoadImage(g_hInstance, MAKEINTRESOURCE(IDI_WAD), IMAGE_ICON, cxSMIcon, cySMIcon, LR_DEFAULTCOLOR);
			HICON hiconMap = LoadImage(g_hInstance, MAKEINTRESOURCE(IDI_MAP), IMAGE_ICON, cxSMIcon, cySMIcon, LR_DEFAULTCOLOR);

			ImageList_SetImageCount(himl, WLI_MAX);
			ImageList_ReplaceIcon(himl, WLI_WAD, hiconWad);
			ImageList_ReplaceIcon(himl, WLI_MAP, hiconMap);

			DestroyIcon(hiconWad);
			DestroyIcon(hiconMap);

			/* Add them to the tree. */
			TreeView_SetImageList(GetDlgItem(hwndDlg, IDC_WADLIST_TREE), himl, TVSIL_NORMAL);
		}

		/* Let the system set the focus. */
		return TRUE;

	case WM_ACTIVATE:
		if(wParam == WA_INACTIVE)
			g_hwndLastModelessDialogue = NULL;
		else g_hwndLastModelessDialogue = hwndDlg;

		/* Allow default processing. */
		return FALSE;

	case WM_SIZE:
		/* Resize the tree window to fit the dialogue. */
		SetWindowPos(GetDlgItem(hwndDlg, IDC_WADLIST_TREE), NULL, 0, 0, LOWORD(lParam) - 9, HIWORD(lParam) - 9, SWP_NOMOVE|SWP_NOZORDER);
		return TRUE;

	case WM_NCHITTEST:
		{
			/* Get default HT result. */
			LRESULT lrHT = DefWindowProc(hwndDlg, uiMsg, wParam, lParam);

			/* If we're in the client are, fall through to the parent. */
			SetWindowLong(hwndDlg, DWL_MSGRESULT, (lrHT == HTCLIENT) ? HTTRANSPARENT : lrHT);
		}

		return TRUE;

	case WM_NOTIFY:
		{
			/* Notification messages from common controls. */
			LPNMHDR lpnmhdr = (LPNMHDR)lParam;

			if(lpnmhdr->idFrom == IDC_WADLIST_TREE)
			{
				/* Message from the wad-list treeview. */
				
				switch(lpnmhdr->code)
				{
				case NM_RETURN:
				case NM_DBLCLK:
					{
						HTREEITEM hti, htiParent;

						/* We double-clicked or pressed enter. We're only
						 * interested if we have a map node item, though. Make
						 * sure we something's selected, and then make sure it's
						 * a map node.
						 */
						if((hti = TreeView_GetSelection(g_hwndTreeViewWads)) &&
							(htiParent = TreeView_GetParent(g_hwndTreeViewWads, hti)))
						{
							OpenMapFromTreeNodes(hti, htiParent);
							return TRUE;
						}
					}

					break;

				case NM_RCLICK:
					{
						HTREEITEM hti;

						hti = TreeView_GetDropHilight(g_hwndTreeViewWads);
						if(!hti) hti = TreeView_GetSelection(g_hwndTreeViewWads);

						/* Is anything selected? */
						if(hti)
						{
							ENUM_WADLISTPOPUP wlpMenuIndex;
							HMENU hmenuPopup = LoadMenu(g_hInstance, MAKEINTRESOURCE(IDM_WADLISTPOPUP));
							WORD wID;
							POINT ptMouse;
							HTREEITEM htiParent;
							WADLIST_WAD_DATA *lpwwd;
							TVITEM tvitem;

							/* Really select this node. */
							TreeView_SelectItem(g_hwndTreeViewWads, hti);

							/* Are we a map or a wad? */
							if((htiParent = TreeView_GetParent(g_hwndTreeViewWads, hti)))
								wlpMenuIndex = WLP_MAP;
							else
								wlpMenuIndex = WLP_WAD;

							/* Get data for the wad. */
							tvitem.hItem = htiParent ? htiParent : hti;
							tvitem.mask = TVIF_PARAM;
							TreeView_GetItem(g_hwndTreeViewWads, &tvitem);
							lpwwd = (WADLIST_WAD_DATA*)tvitem.lParam;

							GetCursorPos(&ptMouse);
							wID = TrackPopupMenu(GetSubMenu(hmenuPopup, wlpMenuIndex),
								TPM_NONOTIFY | TPM_RETURNCMD | TPM_TOPALIGN | TPM_RIGHTBUTTON,
								ptMouse.x, ptMouse.y,
								0, g_hwndMain, NULL);

							DestroyMenu(hmenuPopup);

							/* Which menu item was chosen? */
							switch(wID)
							{
							case IDM_POPUP_WLCLOSEWAD:
								CloseWad(lpwwd->iWad);
								break;

							case IDM_POPUP_WLOPENMAP:
								OpenMapFromTreeNodes(hti, htiParent);
								break;

							case IDM_POPUP_WLRENAMEMAP:
								TreeView_EditLabel(g_hwndTreeViewWads, hti);
								break;

							case IDM_POPUP_WLDELETEMAP:
								if(IDYES == MessageBoxFromStringTable(g_hwndMain, IDS_QUERY_DELETEMAP, MB_ICONEXCLAMATION | MB_YESNO))
								{
									TVITEM tviMap;
									TCHAR szLumpnameT[CCH_LUMPNAME + 1];
#ifdef _UNICODE
									CHAR szLumpnameA[CCH_LUMPNAME + 1];
#else
									LPSTR szLumpnameA = szLumpnameT;
#endif

									/* Get the map's name. */
									tviMap.hItem = hti;
									tviMap.mask = TVIF_HANDLE | TVIF_TEXT;
									tviMap.pszText = szLumpnameT;
									tviMap.cchTextMax = NUM_ELEMENTS(szLumpnameT);
									TreeView_GetItem(g_hwndTreeViewWads, &tviMap);

#ifdef _UNICODE
									WideCharToMultiByte(CP_ACP, 0, szLumpnameT, -1, szLumpnameA, NUM_ELEMENTS(szLumpnameA), NULL, NULL);
#endif

									/* Delete the node from the tree-view. */
									TreeView_DeleteItem(g_hwndTreeViewWads, hti);

									/* Close any open windows for the map, and
									 * delete it.
									 */
									DeleteMapFromOpenWad(lpwwd->iWad, szLumpnameA);
								}

								break;

							case IDM_POPUP_WLADDMAP:
								{
									CHAR szLumpnameA[CCH_LUMPNAME + 1];
									WAD *lpwad = GetWad(lpwwd->iWad);

									if(GetFirstFreeMapLumpname(lpwad, szLumpnameA) == 0)
									{
										HTREEITEM htiNewMap;

										/* Create a dummy map structure and add
										 * it to the wad.
										 */
										MAP *lpmap = AllocateMapStructure();
										UpdateMapToWadStructure(lpmap, lpwad, szLumpnameA, GetDefaultMapConfigForWad(lpwwd->iWad));
										DestroyMapStructure(lpmap);

										/* Add a new node to the tree and begin
										 * a renaming operation.
										 */
										htiNewMap = AddMapToTreeReturnHTI(lpwwd->iWad, szLumpnameA);
										TreeView_Expand(g_hwndTreeViewWads, hti, TVE_EXPAND);
										TreeView_EditLabel(g_hwndTreeViewWads, htiNewMap);

										/* Mark as modified. */
										SetWadNonMapModified(lpwwd->iWad);
									}
									else MessageBoxFromStringTable(g_hwndMain, IDS_ERROR_NOFREEMAP, MB_ICONERROR);
								}

								break;
							}

							return TRUE;
						}
					}

					break;

				case TVN_BEGINLABELEDIT:
					/* Make sure we're allowed to edit this node. */
					SetWindowLong(hwndDlg, DWL_MSGRESULT, !TreeView_GetParent(g_hwndTreeViewWads, ((LPNMTVDISPINFO)lParam)->item.hItem));
					return TRUE;

				case TVN_ENDLABELEDIT:
					{
						LPNMTVDISPINFO lptvdi = (LPNMTVDISPINFO)lParam;

						/* Assume rejection. */
						BOOL bValid = FALSE;

						/* Make sure we didn't cancel. */
						if(lptvdi->item.pszText)
						{
							WADLIST_WAD_DATA *lpwwd;
							TVITEM tvitem, tviParent;
							TCHAR szOldNameT[CCH_LUMPNAME + 1];
#ifdef _UNICODE
							CHAR szNewNameA[CCH_LUMPNAME + 1];
							CHAR szOldNameA[CCH_LUMPNAME + 1];
							WideCharToMultiByte(CP_ACP, 0, lptvdi->item.pszText, -1, szNewNameA, NUM_ELEMENTS(szNewNameA), NULL, NULL);
#else
							LPSTR szNewNameA = lptvdi->item.pszText, szOldNameA = szOldNameT;
#endif

							/* Get the current map name. */
							tvitem.hItem = lptvdi->item.hItem;
							tvitem.mask = TVIF_HANDLE | TVIF_TEXT;
							tvitem.pszText = szOldNameT;
							tvitem.cchTextMax = NUM_ELEMENTS(szOldNameT);
							TreeView_GetItem(g_hwndTreeViewWads, &tvitem);

#ifdef _UNICODE
							WideCharToMultiByte(CP_ACP, 0, szOldNameT, -1, szOldNameA, NUM_ELEMENTS(szOldNameA), NULL, NULL);
#endif

							/* Get the data for the wad. */
							tviParent.hItem = TreeView_GetParent(g_hwndTreeViewWads, lptvdi->item.hItem);
							tviParent.mask = TVIF_HANDLE | TVIF_PARAM;
							TreeView_GetItem(g_hwndTreeViewWads, &tviParent);
							lpwwd = (WADLIST_WAD_DATA*)tviParent.lParam;

							/* Make sure the new name is valid? */
							if(!IsMapLumpname(lptvdi->item.pszText))
								MessageBoxFromStringTable(g_hwndMain, IDS_ERROR_MAPLUMPNAME, MB_ICONERROR);
							else if(strcmp(szOldNameA, szNewNameA) != 0 && GetLumpIndex(GetWad(lpwwd->iWad), 0, -1, szNewNameA) >= 0)
								MessageBoxFromStringTable(g_hwndMain, IDS_ERROR_MAPEXISTS, MB_ICONERROR);
							else
							{
								/* Valid and unique name, so go ahead and
								 * rename.
								 */

								/* If we have an open window for this map, update it. */
								HWND hwndMap = FindMapWindowByLumpname(lpwwd->iWad, szOldNameA);
								if(hwndMap) RenameMapInWindow(hwndMap, szNewNameA);

								/* Finally rename the map in the wad! */
								RenameMapInWad(lpwwd->iWad, szOldNameA, szNewNameA);

								/* Update the tree node's text. Although the
								 * control does this for us anyway, we do it now
								 * so that we can sort the list before we hand
								 * back to the control itself.
								 */
								_tcscpy(tvitem.pszText, lptvdi->item.pszText);
								tvitem.mask = TVIF_HANDLE | TVIF_TEXT;
								TreeView_SetItem(g_hwndTreeViewWads, &tvitem);

								bValid = TRUE;
								SetWadNonMapModified(lpwwd->iWad);

								/* Sort the list. */
								TreeView_SortChildren(g_hwndTreeViewWads, tviParent.hItem, FALSE);
							}
						}

						/* Set the return value for the message. */
						SetWindowLong(hwndDlg, DWL_MSGRESULT, bValid);
					}

					return TRUE;
				}
			}
		}

		break;

	case WM_DESTROY:
		/* Destroy the image list. */
		ImageList_Destroy(TreeView_GetImageList(g_hwndTreeViewWads, TVSIL_NORMAL));
		return TRUE;
	}

	return FALSE;
}



/* DestroyWadListWindow
 *   Destroys the wad-list window.
 *
 * Parameters:
 *   None.
 *
 * Return value: None.
 *
 * Remarks:
 *   This destroys the docking container, which in turn destroys the dialogue.
 */
void DestroyWadListWindow(void)
{
	if(IsWindow(g_hwndWadlist)) DestroyWindow(g_hwndWadlist);
}


/* AddToWadList
 *   Adds a wad to the list if it's not already there.
 *
 * Parameters:
 *   int		iWad		Index of wad.
 *   CONFIG*	lpcfgMap	Map configuration to be used when opening maps from
 *							this wad.
 *
 * Return value: None.
 */
void AddToWadList(int iWad, CONFIG *lpcfgMap)
{
	TVINSERTSTRUCT tvis;
	HTREEITEM htiWad;
	WADLIST_WAD_DATA *lpwwd = ProcHeapAlloc(sizeof(WADLIST_WAD_DATA));
	
	/* If the wad's already on the list, stop. */
	if(FindWadInList(iWad)) return;

	/* Otherwise, add it. */

	/* The item parameter is the structure we allocated. It's freed when the
	 * node is removed from the list.
	 */
	lpwwd->iWad = iWad;
	lpwwd->lpcfgMap = lpcfgMap;
	tvis.item.lParam = (LPARAM)lpwwd;
	tvis.item.iImage = tvis.item.iSelectedImage = WLI_WAD;
	tvis.item.mask = TVIF_PARAM | TVIF_IMAGE | TVIF_SELECTEDIMAGE;

	tvis.hInsertAfter = TVI_SORT;
	tvis.hParent = TVI_ROOT;

	/* Add the wad node. */
	htiWad = TreeView_InsertItem(g_hwndTreeViewWads, &tvis);

	/* Set its text. */
	UpdateWadTitleInListWithHTI(iWad, htiWad);

	/* Add the map lumps. */
	IterateMapLumpNames(iWad, AddMapNameToWadListCallback, (void*)htiWad);
}


/* AddMapNameToWadListCallback
 *   Adds a map to the wad list.
 *
 * Parameters:
 *   LPCTSTR	szLumpname	String to add.
 *   void*		lpvParent	(HTREEITEM) Parent node.
 *
 * Return value: None.
 *
 * Remarks:
 *   Intended for use with IterateMapLumpNames.
 */
static void AddMapNameToWadListCallback(LPCTSTR szLumpname, void *lpvParent)
{
	TVINSERTSTRUCT tvis;

	tvis.item.mask = TVIF_TEXT | TVIF_IMAGE | TVIF_SELECTEDIMAGE;

	/* This doesn't get changed, so we don't violate constness. */
	tvis.item.pszText = (LPTSTR)szLumpname;
	tvis.item.iImage = tvis.item.iSelectedImage = WLI_MAP;

	tvis.hInsertAfter = TVI_SORT;
	tvis.hParent = (HTREEITEM)lpvParent;

	/* Add it. */
	TreeView_InsertItem(g_hwndTreeViewWads, &tvis);
}


/* FindWadInList
 *   Finds the node corresponding to a wad in the list of wads.
 *
 * Parameters:
 *   int	iWad	Index of wad.
 *
 * Return value: HTREEITEM
 *   Handle to node, or NULL if not found.
 */
static HTREEITEM FindWadInList(int iWad)
{
	TVITEM tvitem;

	/* We determine this by checking the item data, which is the wad ID. */
	tvitem.mask = TVIF_HANDLE | TVIF_PARAM;

	/* Get the first root node. */
	tvitem.hItem = TreeView_GetRoot(g_hwndTreeViewWads);

	/* Check all the top-level nodes until we find a match or run out. */
	while(tvitem.hItem &&
		(TreeView_GetItem(g_hwndTreeViewWads, &tvitem), !tvitem.lParam || ((WADLIST_WAD_DATA*)(tvitem.lParam))->iWad != iWad ))
	{
		/* Advance to the next node. */
		tvitem.hItem = TreeView_GetNextSibling(g_hwndTreeViewWads, tvitem.hItem);
	}

	return tvitem.hItem;
}


/* RemoveFromWadList
 *   Removes a wad from the list.
 *
 * Parameters:
 *   int	iWad	Index of wad.
 *
 * Return value: None.
 *
 * Remarks:
 *   If the wad isn't in the list, does nothing.
 */
void RemoveFromWadList(int iWad)
{
	HTREEITEM hti = FindWadInList(iWad);

	if(hti)
	{
		TVITEM tvitem;

		/* Get the information for this item. */
		tvitem.hItem = hti;
		tvitem.mask = TVIF_PARAM;
		TreeView_GetItem(g_hwndTreeViewWads, &tvitem);

		/* Free the data associated with the node. */
		ProcHeapFree((WADLIST_WAD_DATA*)tvitem.lParam);

		TreeView_DeleteItem(g_hwndTreeViewWads, hti);
	}
}


/* UpdateWadTitleInList, UpdateWadTitleInListWithHTI
 *   Updates a wad's title string in the wad-list.
 *
 * Parameters:
 *   int		iWad	Index of wad.
 *    and, in the case of UpdateWadTitleInListWithHTI: 
 *   HTREEITEM	hti		Handle to wad node.
 *
 * Return value: None.
 *
 * Remarks:
 *   If the wad isn't in the list, does nothing.
 */
void UpdateWadTitleInList(int iWad)
{
	HTREEITEM hti;

	/* Try to find the item, and set its text if it exists. */
	hti = FindWadInList(iWad);
	if(hti)
		UpdateWadTitleInListWithHTI(iWad, hti);
}

static void UpdateWadTitleInListWithHTI(int iWad, HTREEITEM hti)
{
	TVITEM tvitem;
	int cchFilename, cchFileTitle;
	LPTSTR szFilename;

	tvitem.hItem = hti;

	/* Get file title. */
	cchFilename = GetWadFilename(iWad, NULL, 0);
	if(cchFilename > 0)
	{
		/* We have a filename, so get it. */
		szFilename = ProcHeapAlloc(cchFilename * sizeof(TCHAR));
		GetWadFilename(iWad, szFilename, cchFilename);
		cchFileTitle = GetFileTitle(szFilename, NULL, 0);
		tvitem.pszText = (LPTSTR)ProcHeapAlloc(cchFileTitle * sizeof(TCHAR));
		GetFileTitle(szFilename, tvitem.pszText, cchFileTitle);
		ProcHeapFree(szFilename);
	}
	else
	{
		/* No filename. Get "Untitled" string. */
		tvitem.pszText = (LPTSTR)ProcHeapAlloc(UNTITLED_BUFLEN * sizeof(TCHAR));
		LoadString(g_hInstance, IDS_UNTITLED, tvitem.pszText, UNTITLED_BUFLEN);
	}

	/* Set the text. */
	tvitem.mask = TVIF_HANDLE | TVIF_TEXT;
	TreeView_SetItem(g_hwndTreeViewWads, &tvitem);

	ProcHeapFree(tvitem.pszText);
}


/* OpenMapFromTreeNodes
 *   Opens a map corresponding to a wad-list tree-node.
 *
 * Parameters:
 *   HTREEITEM	htiMapNode, htiParent		The node and its parent.
 *
 * Return value: None.
 */
static void OpenMapFromTreeNodes(HTREEITEM htiMapNode, HTREEITEM htiParent)
{
	TCHAR szMapname[CCH_LUMPNAME + 1];
#ifdef _UNICODE
	char szMapnameA[CCH_LUMPNAME + 1];
#else
	char *szMapnameA = szMapname;
#endif
	TVITEM tvitem;
	WADLIST_WAD_DATA *lpwwd;

	/* Get the selected item's text. */
	tvitem.hItem = htiMapNode;
	tvitem.mask = TVIF_TEXT;
	tvitem.cchTextMax = sizeof(szMapname)/sizeof(TCHAR);
	tvitem.pszText = szMapname;
	TreeView_GetItem(g_hwndTreeViewWads, &tvitem);

	/* Convert the map name to ANSI if necessary. */
#ifdef _UNICODE
	WideCharToMultiByte(CP_ACP, 0, szMapname, -1, szMapnameA, sizeof(szMapnameA), NULL, NULL);
#endif

	/* Determine which map config to use. It's stored in the data pointed at by
	 * our parent's parameter.
	 */
	tvitem.hItem = htiParent;
	tvitem.mask = TVIF_PARAM;
	TreeView_GetItem(g_hwndTreeViewWads, &tvitem);
	lpwwd = (WADLIST_WAD_DATA*)tvitem.lParam;

	/* Attempt to open the map. */
	if(OpenMapInWindow(lpwwd->iWad, szMapnameA, lpwwd->lpcfgMap) == OMIWRET_NEWWINDOW)
		IncrementWadReferenceCount(lpwwd->iWad);

	/* Take the focus *after* we've finished. */
	DeferTakeFocus();
}


/* RenameMapInWadList
 *   Adjusts the text of a map's node in the wad-list.
 *
 * Parameters:
 *   int		iWad		Index of wad.
 *   LPCSTR		szOldName	Current name of map.
 *   LPCSTR		szNewName	New name for map.
 *
 * Return value: int
 *   Zero on success, or non-zero if specified map does not exist.
 *
 * Remarks:
 *   No validation is carried out on the new name.
 */
int RenameMapInWadList(int iWad, LPCSTR szOldName, LPCSTR szNewName)
{
	HTREEITEM htiWad;
	TVITEM tvitem;
	TCHAR szNodeName[CCH_LUMPNAME + 1];

	/* Unicodify the lumpnames if necessary. */
#ifdef _UNICODE
	WCHAR szOldNameT[CCH_LUMPNAME + 1], szNewNameT[CCH_LUMPNAME + 1];
	MultiByteToWideChar(CP_ACP, MB_PRECOMPOSED, szOldName, -1, szOldNameT, NUM_ELEMENTS(szOldNameT));
	MultiByteToWideChar(CP_ACP, MB_PRECOMPOSED, szNewName, -1, szNewNameT, NUM_ELEMENTS(szNewNameT));
#else
	LPCSTR szOldNameT = szOldName, szNewNameT = szNewName;
#endif

	htiWad = FindWadInList(iWad);

	/* Bad wad? */
	if(!htiWad) return 1;

	/* Enumerate the wad's maps, retrieving their lumpnames. */
	tvitem.mask = TVIF_HANDLE | TVIF_TEXT;
	tvitem.pszText = szNodeName;
	tvitem.cchTextMax = NUM_ELEMENTS(szNodeName);

	/* Get first map node. */
	tvitem.hItem = TreeView_GetChild(g_hwndTreeViewWads, htiWad);

	/* Check all the map nodes until we find a match or run out. */
	while(tvitem.hItem &&
		(TreeView_GetItem(g_hwndTreeViewWads, &tvitem), _tcscmp(tvitem.pszText, szOldNameT) != 0))
	{
		/* Advance to the next node. */
		tvitem.hItem = TreeView_GetNextSibling(g_hwndTreeViewWads, tvitem.hItem);
	}

	/* Map doesn't exist? */
	if(!tvitem.hItem) return 2;

	/* Set the new name. */
	_tcscpy(tvitem.pszText, szNewNameT);
	TreeView_SetItem(g_hwndTreeViewWads, &tvitem);

	TreeView_SortChildren(g_hwndTreeViewWads, htiWad, FALSE);

	/* Non-map data has changed. */
	SetWadNonMapModified(iWad);

	/* Success! */
	return 0;
}


/* AddMapToTree, AddMapToTreeReturnHTI
 *   Adds a map node to the tree.
 *
 * Parameters:
 *   int		iWad		Index of wad.
 *   LPCSTR		szLumpname	Map name.
 *
 * Return value: None, or HTREEITEM, respectively.
 *   N/A, or handle to new tree node (NULL on error), resp.
 *
 * Remarks:
 *   This isn't actually used when initially populating the list of a wad's
 *   maps, as that can be done more efficiently; instead, it's intended for
 *   adding new maps.
 */
void AddMapToTree(int iWad, LPCSTR szLumpname)
{
	AddMapToTreeReturnHTI(iWad, szLumpname);
}

static HTREEITEM AddMapToTreeReturnHTI(int iWad, LPCSTR szLumpname)
{
#ifdef _UNICODE
	WCHAR szLumpnameT[CCH_LUMPNAME + 1];
#else
	/* Constness isn't violated. */
	LPSTR szLumpnameT = (LPSTR)szLumpname;
#endif
	TVINSERTSTRUCT tvis;

	/* Add a new node to the tree. */
	tvis.hParent = FindWadInList(iWad);

	if(!tvis.hParent) return NULL;

	tvis.hInsertAfter = TVI_SORT;
	tvis.item.mask = TVIF_TEXT | TVIF_IMAGE | TVIF_SELECTEDIMAGE;
	tvis.item.pszText = szLumpnameT;
	tvis.item.iImage = tvis.item.iSelectedImage = WLI_MAP;

#ifdef _UNICODE
	MultiByteToWideChar(CP_ACP, MB_PRECOMPOSED, szLumpname, -1, szLumpnameT, NUM_ELEMENTS(szLumpnameT));
#endif

	return TreeView_InsertItem(g_hwndTreeViewWads, &tvis);
}
