#include <windows.h>
#include <commctrl.h>
#include <tchar.h>
#include <stdio.h>

#include "../general.h"
#include "../../res/resource.h"

#include "../../DockWnd/DockWnd.h"

#include "toolbar.h"
#include "mdiframe.h"


static DOCKINFO *g_lpdiToolbar;

#define TOOLBAR_IMAGES	11
#define CX_TBIMAGES		16
#define CY_TBIMAGES		16

#define TB_MASK_COLOUR RGB(0xFF, 0, 0xFF)

enum ENUM_TB_IMAGE_OFFSETS
{
	TBIO_ANYMODE = 0,
	TBIO_SECTORSMODE,
	TBIO_LINESMODE,
	TBIO_VERTICESMODE,
	TBIO_THINGSMODE,
	TBIO_POINTER,
	TBIO_PAN,
	TBIO_DRAWLINES,
	TBIO_DRAWRECT,
	TBIO_INSERTVERTEX,
	TBIO_INSERTTHING
};


/* CreateToolbarWindow
 *   Creates the toolbar tool window with its docking container.
 *
 * Parameters:
 *   HWND	hwndParent	Parent window (i.e. MDI frame window).
 *
 * Return value: BOOL
 *   TRUE on success; FALSE on error.
 *
 * Remarks:
 *   We have to pass the frame window handle since this is called before the
 *   global var is set.
 */
BOOL CreateToolbarWindow(HWND hwndParent)
{
	TCHAR		szCaption[64];
	HWND		hwndTB;
	int			iImagesOffset;
	HBITMAP		hbmButtons;
	HIMAGELIST	himl = ImageList_Create(CX_TBIMAGES, CY_TBIMAGES, ILC_COLOR24 | ILC_MASK, 0, 0);

	/* The toolbar is initially attached to the parent. */
	hwndTB = CreateWindowEx(0,
							TOOLBARCLASSNAME,
							NULL,
							WS_CHILD | WS_CLIPCHILDREN,
							0, 0, 0, 0,
							hwndParent,
							NULL,
							g_hInstance,
							NULL);

	SendMessage(hwndTB, TB_BUTTONSTRUCTSIZE, sizeof(TBBUTTON), 0);

	SendMessage(hwndTB, TB_SETSTYLE, 0, TBSTYLE_FLAT | TBSTYLE_TRANSPARENT | TBSTYLE_WRAPABLE | TBSTYLE_TOOLTIPS | WS_CLIPSIBLINGS | CCS_NOPARENTALIGN | CCS_NORESIZE);
	SendMessage(hwndTB, TB_SETEXTENDEDSTYLE, 0, TBSTYLE_EX_DRAWDDARROWS);

	/* Associate the (empty) image list with the toolbar. */
	SendMessage(hwndTB, TB_SETIMAGELIST, 0, (LPARAM)himl);

	/* Add the system's images. */
	SendMessage(hwndTB, TB_LOADIMAGES, IDB_STD_SMALL_COLOR, (LPARAM)HINST_COMMCTRL);

	/* Add my images. */
	hbmButtons = LoadBitmap(g_hInstance, MAKEINTRESOURCE(IDB_TOOLBAR));
	iImagesOffset = ImageList_AddMasked(himl, hbmButtons, TB_MASK_COLOUR);
	DeleteObject(hbmButtons);

	{
		TBBUTTON tbb[] =
		{
			{STD_FILENEW, IDM_FILE_NEW, TBSTATE_ENABLED, TBSTYLE_BUTTON, {0}, 0, 0},
			{STD_FILEOPEN, IDM_FILE_OPEN, TBSTATE_ENABLED, TBSTYLE_BUTTON, {0}, 0, 0},
			{STD_FILESAVE, IDM_FILE_SAVE, TBSTATE_ENABLED, TBSTYLE_BUTTON, {0}, 0, 0},

			{0, 0, 0, TBSTYLE_SEP, {0}, 0, 0},

			{STD_CUT, IDM_EDIT_CUT, TBSTATE_ENABLED, TBSTYLE_BUTTON, {0}, 0, 0},
			{STD_COPY, IDM_EDIT_COPY, TBSTATE_ENABLED, TBSTYLE_BUTTON, {0}, 0, 0},
			{STD_PASTE, IDM_EDIT_PASTE, TBSTATE_ENABLED, TBSTYLE_BUTTON, {0}, 0, 0},

			{0, 0, 0, TBSTYLE_SEP, {0}, 0, 0},

			{STD_UNDO, IDM_EDIT_UNDO, TBSTATE_ENABLED, TBSTYLE_DROPDOWN, {0}, 0, 0},
			{STD_REDOW, IDM_EDIT_REDO, TBSTATE_ENABLED, TBSTYLE_DROPDOWN, {0}, 0, 0},

			{0, 0, 0, TBSTYLE_SEP, {0}, 0, 0},

			{STD_PROPERTIES, IDM_PROPERTIES, TBSTATE_ENABLED, TBSTYLE_BUTTON, {0}, 0, 0},

			{0, 0, 0, TBSTYLE_SEP, {0}, 0, 0},

			{STD_FIND, IDM_EDIT_FIND, TBSTATE_ENABLED, TBSTYLE_BUTTON, {0}, 0, 0},
			{STD_REPLACE, IDM_EDIT_REPLACE, TBSTATE_ENABLED, TBSTYLE_BUTTON, {0}, 0, 0},

			{0, 0, 0, TBSTYLE_SEP, {0}, 0, 0},

			{iImagesOffset + TBIO_ANYMODE, IDM_VIEW_MODE_ANY, TBSTATE_ENABLED, TBSTYLE_BUTTON | TBSTYLE_CHECKGROUP, {0}, 0, 0},
			{iImagesOffset + TBIO_LINESMODE, IDM_VIEW_MODE_LINES, TBSTATE_ENABLED, TBSTYLE_BUTTON | TBSTYLE_CHECKGROUP, {0}, 0, 0},
			{iImagesOffset + TBIO_SECTORSMODE, IDM_VIEW_MODE_SECTORS, TBSTATE_ENABLED, TBSTYLE_BUTTON | TBSTYLE_CHECKGROUP, {0}, 0, 0},
			{iImagesOffset + TBIO_VERTICESMODE, IDM_VIEW_MODE_VERTICES, TBSTATE_ENABLED, TBSTYLE_BUTTON | TBSTYLE_CHECKGROUP, {0}, 0, 0},
			{iImagesOffset + TBIO_THINGSMODE, IDM_VIEW_MODE_THINGS, TBSTATE_ENABLED, TBSTYLE_BUTTON | TBSTYLE_CHECKGROUP, {0}, 0, 0},

			{0, 0, 0, TBSTYLE_SEP, {0}, 0, 0},

			{iImagesOffset + TBIO_POINTER, IDM_CANCEL, TBSTATE_ENABLED, TBSTYLE_BUTTON | TBSTYLE_CHECKGROUP, {0}, 0, 0},
			{iImagesOffset + TBIO_PAN, IDM_VIEW_MODE_MOVE, TBSTATE_ENABLED, TBSTYLE_BUTTON | TBSTYLE_CHECKGROUP, {0}, 0, 0},
			{iImagesOffset + TBIO_DRAWLINES, IDM_INSERTLINES, TBSTATE_ENABLED, TBSTYLE_BUTTON | TBSTYLE_CHECKGROUP, {0}, 0, 0},
			{iImagesOffset + TBIO_DRAWRECT, IDM_INSERTRECT, TBSTATE_ENABLED, TBSTYLE_BUTTON | TBSTYLE_CHECKGROUP, {0}, 0, 0},
			{iImagesOffset + TBIO_INSERTVERTEX, IDM_INSERTVERTEX, TBSTATE_ENABLED, TBSTYLE_BUTTON | TBSTYLE_CHECKGROUP, {0}, 0, 0},
			{iImagesOffset + TBIO_INSERTTHING, IDM_INSERTTHING, TBSTATE_ENABLED, TBSTYLE_BUTTON | TBSTYLE_CHECKGROUP, {0}, 0, 0},
		};

		SendMessage(hwndTB, TB_ADDBUTTONS, NUM_ELEMENTS(tbb), (LPARAM)tbb);
	}

	/* Show arrows for drop-down buttons. */
	//SendMessage(hwndTB, TB_SETEXTENDEDSTYLE, 0, TBSTYLE_EX_DRAWDDARROWS);

	/* Allocate a docking structure. This'll be freed automatically by the lib.
	 */
	g_lpdiToolbar = DockingAlloc((char)DWS_DOCKED_TOP);

	g_lpdiToolbar->cyFloating = g_lpdiToolbar->nDockedSize = HIWORD(SendMessage(hwndTB, TB_GETBUTTONSIZE, 0, 0)) + HIWORD(SendMessage(hwndTB, TB_GETPADDING, 0, 0));

	/* Destroy the toolbar when the docking container is destroyed. Also, hide
	 * the docking container if the user clicks the Close button, rather than
	 * destroying the window.
	 */
	g_lpdiToolbar->dwStyle |= DWS_NODESTROY | DWS_DESTROYFOCUSWIN;

	LoadString(g_hInstance, IDS_TOOLBAR_CAPTION, szCaption, NUM_ELEMENTS(szCaption));
	DockingCreateFrame(g_lpdiToolbar, hwndParent, szCaption);

	/* Move the toolbar from the parent to the docking container. The MDI frame
	 * will continue to get notification messages, though.
	 */
	g_lpdiToolbar->focusWindow = hwndTB;
	SetParent(hwndTB, g_lpdiToolbar->hwnd);
	ShowWindow(hwndTB, SW_SHOW);

	DockingShowFrame(g_lpdiToolbar);

	return TRUE;
}


/* DestroyToolbarWindow
 *   Destroys the toolbar window.
 *
 * Parameters:
 *   None.
 *
 * Return value: None.
 *
 * Remarks:
 *   This destroys the docking container, which in turn destroys the toolbar
 *    control.
 */
void DestroyToolbarWindow(void)
{
	if(IsWindow(g_lpdiToolbar->hwnd))
	{
		HIMAGELIST himl;
		HWND hwndTB = g_lpdiToolbar->focusWindow;

		himl = (HIMAGELIST)SendMessage(hwndTB, TB_GETIMAGELIST, 0, 0);
		DestroyWindow(g_lpdiToolbar->hwnd);
		ImageList_Destroy(himl);
	}
}


/* GetToolbarWindow
 *   Retrieves the handle to the toolbar control.
 *
 * Parameters:
 *   None.
 *
 * Return value: HWND
 *   Toolbar window handle.
 */
HWND GetToolbarWindow(void)
{
	return g_lpdiToolbar->focusWindow;
}
