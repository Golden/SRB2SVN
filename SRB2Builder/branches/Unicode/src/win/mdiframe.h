#ifndef __SRB2B_MDIFRAME__
#define __SRB2B_MDIFRAME__

#include <windows.h>

#include "../config.h"


/* Number of characters (inc. terminator) for "Untitled" string. */
#define UNTITLED_BUFLEN 32


/* Types. */
enum ENUM_CHILD_IDS
{
	IDC_STATUSBAR,
	IDC_TOOLBAR,
};

typedef enum _ENUM_OMIW_RETURN
{
	OMIWRET_NEWWINDOW,
	OMIWRET_EXISTINGWINDOW,
	OMIWRET_FAILED
} ENUM_OMIW_RETURN;

/* Status bar panels. */
enum ENUM_SBPANELS
{
	SBP_MODE = 0,
	SBP_SECTORS,
	SBP_LINEDEFS,
	SBP_SIDEDEFS,
	SBP_VERTICES,
	SBP_THINGS,
	SBP_GRID,
	SBP_ZOOM,
	SBP_COORD,
	SBP_MAPCONFIG,
	SBP_LAST
};

/* Menu positions for the no-document menu bar. */
enum ENUM_NODOC_MENUPOS
{
	FILEMENUPOSNODOC = 0,
	WINDOWMENUPOSNODOC = 0,		/* Dummy. */
	HELPMENUPOSNODOC = 1
};

/* Menu positions for the map-window menu bar. */
enum ENUM_MAP_MENUPOS
{
	FILEMENUPOS = 0,
	EDITMENUPOS,
	VIEWMENUPOS,
	LINESMENUPOS,
	SECTORSMENUPOS,
	VERTICESMENUPOS,
	THINGSMENUPOS,
	WINDOWMENUPOS,
	HELPMENUPOS,
#ifdef _DEBUG
	DEBUGMENUPOS,
#endif
	MAPMENUCOUNT
};


/* Corresponding flags. */
#define SBPF_MODE		(1 << SBP_MODE)
#define SBPF_SECTORS	(1 << SBP_SECTORS)
#define SBPF_LINEDEFS	(1 << SBP_LINEDEFS)
#define SBPF_SIDEDEFS	(1 << SBP_SIDEDEFS)
#define SBPF_VERTICES	(1 << SBP_VERTICES)
#define SBPF_THINGS		(1 << SBP_THINGS)
#define SBPF_GRID		(1 << SBP_GRID)
#define SBPF_ZOOM		(1 << SBP_ZOOM)
#define SBPF_COORD		(1 << SBP_COORD)
#define SBPF_MAPCONFIG	(1 << SBP_MAPCONFIG)


/* Globals. */
extern HWND g_hwndMain, g_hwndClient;
extern HMENU g_hmenuNodoc, g_hmenuMap;
extern HMENU g_hmenuNodocWin, g_hmenuMapSubmenus[];
extern HWND g_hwndStatusBar;


/* Function prototypes. */
int CreateMainWindow(int iCmdShow);
void DestroyMainWindow(void);
BOOL CALLBACK CloseEnumProc(HWND hwnd, LPARAM lParam);
BOOL OpenWadForEditing(LPCTSTR szFilename);
ENUM_OMIW_RETURN OpenMapInWindow(int iWad, LPSTR szLumpName, CONFIG *lpcfgMap);
void StatusBarNoWindow(void);
void DeferTakeFocus(void);
void UpdateMenuShortcutKeys(HMENU hmenu);


#endif
