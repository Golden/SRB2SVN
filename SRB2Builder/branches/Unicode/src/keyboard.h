#ifndef __SRB2B_KEYBOARD__
#define __SRB2B_KEYBOARD__

#include <windows.h>


#define NOCOMMAND 0xFFFF


int MakeShiftedKeyCode(int iVirtKey);
void UpdateAcceleratorFromOptions(void);
USHORT GetMenuIDFromKeycode(int iKeyCode);
int GetKeycodeFromMenuID(USHORT unMenuID);
void SetDefaultShortcuts(void);
BOOL TranslateBespokeAccelerator(LPMSG lpmsg);
void GetShiftedShortcutText(int iKeyCode, LPTSTR szBuffer, UINT cchBuffer);


#endif
