#ifndef __SRB2B_GENERAL__
#define __SRB2B_GENERAL__

#include <windows.h>
#include <math.h>


/* Types. */
typedef struct _DYNAMICINTARRAY
{
	unsigned int uiBufSize, uiCount;
	int *lpiIndices;
} DYNAMICINTARRAY;



/* Globals. */
extern HINSTANCE g_hInstance;
extern TCHAR g_szAppName[];
extern HANDLE g_hProcHeap;
extern CRITICAL_SECTION g_cs;
BOOL g_bWinNT, g_bHasCC6;


/* Function prototypes. */
void Die(LPCTSTR szFile, int iLine, int iMsgResource);
BOOL CALLBACK IncrementInteger(HWND hwndUnused, LPARAM lParam);
BOOL EqualPaths(LPCTSTR szPath1, LPCTSTR szPath2);
BOOL PathsReferToSameFile(LPCTSTR szPath1, LPCTSTR szPath2);
BOOL IsStringIntA(LPCSTR sz);
int MessageBoxFromStringTable(HWND hwnd, WORD wResourceString, UINT uiType);
int QsortIntegerComparison(const void *lpv1, const void *lpv2);
int QsortUShortComparison(const void *lpv1, const void *lpv2);
int CALLBACK ListViewIntegerComparison(LPARAM lParam1, LPARAM lParam2, LPARAM lParamSort);
void InitialiseDynamicIntArray(DYNAMICINTARRAY *lpdiarray, unsigned int ciInitialSize);
void FreeDynamicIntArray(DYNAMICINTARRAY *lpdiarray);
void AddToDynamicIntArray(DYNAMICINTARRAY *lpdiarray, int iValue);
void SortDynamicIntArray(DYNAMICINTARRAY *lpdiarray);
void LoadAndFormatFilterString(USHORT unStringID, LPTSTR szFilter, UINT cchFilter);
char Log2Floor(unsigned int ui);


/* Inlined functions. */
static __inline void ClearDynamicIntArray(DYNAMICINTARRAY *lpdiarray)
{
	lpdiarray->uiCount = 0;
}


/* Macros. */
#define DIE(x) Die(TEXT(__FILE__), __LINE__, (x))
#define NUM_ELEMENTS(arr) (sizeof(arr)/sizeof(arr[0]))

/* ProcHeapAlloc, ProcHeapReAlloc, ProcHeapFree
 *   Wrappers for Heap* functions to alloc/free from the process heap.
 */
static __inline LPVOID ProcHeapAlloc(DWORD cb) { return HeapAlloc(g_hProcHeap, HEAP_GENERATE_EXCEPTIONS, cb); }
static __inline LPVOID ProcHeapReAlloc(LPVOID lpv, DWORD cb) { return HeapReAlloc(g_hProcHeap, HEAP_GENERATE_EXCEPTIONS, lpv, cb); }
static __inline BOOL ProcHeapFree(LPVOID lpv) { return HeapFree(g_hProcHeap, 0, lpv); }

#define ROUND(f) (int)(floor((f) + 0.5))

#ifndef INT_MAX
#define INT_MAX 0x7FFFFFFF
#endif

#endif
