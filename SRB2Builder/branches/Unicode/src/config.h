#ifndef __SRB2B_CONFIG__
#define __SRB2B_CONFIG__


/* Macros. */


/* Types. */

typedef enum _CONFIG_ENTRY_TYPE
{
	CET_SUBSECTION,
	CET_STRING,
	CET_INT,
	CET_FLOAT,
	CET_NULL
} CONFIG_ENTRY_TYPE;

/* Config-file-entry binary tree. */
typedef struct _CONFIG
{
	LPTSTR				szName;

	CONFIG_ENTRY_TYPE	entrytype;
	union
	{
		struct _CONFIG *lpcfgSubsection;
		LPTSTR			sz;
		int				i;
		float			f;
	};

	struct _CONFIG	*lpcfgLeft, *lpcfgRight;
} CONFIG;


/* Prototypes. */
CONFIG* ConfigLoad(LPCTSTR szFilename);
void ConfigDestroy(CONFIG *lpcfg);

#ifdef UNICODE
void ConfigSetStringA(CONFIG *lpcfgRoot, LPCWSTR szName, LPCSTR szValue);
#else
#define ConfigSetStringA ConfigSetString
#endif

void ConfigSetString(CONFIG *lpcfgRoot, LPCTSTR szName, LPCTSTR szValue);

void ConfigSetFloat(CONFIG *lpcfgRoot, LPCTSTR szName, float f);
void ConfigSetInteger(CONFIG *lpcfgRoot, LPCTSTR szName, int i);

#ifdef UNICODE
void ConfigSetAtomA(CONFIG *lpcfgRoot, LPCSTR szName);
#else
#define ConfigSetAtomA ConfigSetAtom
#endif

void ConfigSetAtom(CONFIG *lpcfgRoot, LPCTSTR szName);

void ConfigSetSubsection(CONFIG *lpcfgRoot, LPCTSTR szName, CONFIG *lpcfgSS);
CONFIG* ConfigAddSubsection(CONFIG *lpcfgRoot, LPCTSTR szName);
BOOL ConfigNodeExists(CONFIG *lpcfgRoot, LPCTSTR szName);
int ConfigGetInteger(CONFIG *lpcfgRoot, LPCTSTR szName);
float ConfigGetFloat(CONFIG *lpcfgRoot, LPCTSTR szName);
int ConfigGetStringLength(CONFIG *lpcfgRoot, LPCTSTR szName);

#ifdef UNICODE
BOOL ConfigGetStringA(CONFIG *lpcfgRoot, LPCWSTR szName, LPSTR szBuffer, unsigned int cchBuffer);
#else
#define ConfigGetStringA ConfigGetString
#endif

BOOL ConfigGetString(CONFIG *lpcfgRoot, LPCTSTR szName, LPTSTR szBuffer, unsigned int cchBuffer);

CONFIG* ConfigGetSubsection(CONFIG *lpcfgRoot, LPCTSTR szName);
CONFIG* ConfigCreate(void);
CONFIG* ConfigDuplicate(CONFIG *lpcfg);
BOOL ConfigIterate(CONFIG *lpcfgRoot, BOOL (*lpfnCallback)(CONFIG*, void*), void *lpvParam);
int ConfigWrite(CONFIG *lpcfg, LPCTSTR szFilename);
void ConfigDeleteNode(CONFIG *lpcfgRoot, LPCTSTR szName);


/* Inline functions. */

static __inline CONFIG* ConfigGetOrAddSubsection(CONFIG *lpcfgRoot, LPCTSTR szName)
{
	CONFIG *lpcfgSubsection = ConfigGetSubsection(lpcfgRoot, szName);
	if(lpcfgSubsection) return lpcfgSubsection;
	return ConfigAddSubsection(lpcfgRoot, szName);
}

static __inline BOOL ConfigIsEmpty(CONFIG *lpcfg) { return !(lpcfg->lpcfgLeft || lpcfg->lpcfgRight); }

#endif
