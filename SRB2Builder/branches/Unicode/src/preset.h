#ifndef __SRB2B_PRESET__
#define __SRB2B_PRESET__

#include <windows.h>

#include "map.h"
#include "selection.h"

enum ENUM_SECTOR_PRESET_FLAGS
{
	SPF_EFFECT		= 0x0001,
	SPF_CEILING		= 0x0002,
	SPF_FLOOR		= 0x0004,
	SPF_TAG			= 0x0008,
	SPF_BRIGHTNESS	= 0x0010,
	SPF_FLOORTEX	= 0x0020,
	SPF_CEILINGTEX	= 0x0040,
	SPF_ALL			= 0x007F
};

enum ENUM_LINE_PRESET_FLAGS
{
	LPF_EFFECT		= 0x0001,
	LPF_FLAGS		= 0x0002,
	LPF_TAG			= 0x0004,

	LPF_FRONTUPPER	= 0x0008,
	LPF_FRONTMIDDLE	= 0x0010,
	LPF_FRONTLOWER	= 0x0020,
	LPF_FRONTX		= 0x0040,
	LPF_FRONTY		= 0x0080,
	LPF_ALLFRONT	= 0x00F8,

	LPF_BACKUPPER	= 0x0100,
	LPF_BACKMIDDLE	= 0x0200,
	LPF_BACKLOWER	= 0x0400,
	LPF_BACKX		= 0x0800,
	LPF_BACKY		= 0x1000,
	LPF_ALLBACK		= 0x1F00,

	LPF_ALL			= 0x1FFF
};

enum ENUM_THING_PRESET_FLAGS
{
	TPF_TYPE		= 0x0001,
	TPF_ANGLE		= 0x0002,
	TPF_FLAGS		= 0x0004,
	TPF_ALL			= 0x0007
};


typedef struct _PRESET
{
	BYTE	byObjectTypeFlags;
	DWORD	dwSectorFlags, dwLineFlags, dwThingFlags;

	MAPSECTOR	sec;
	MAPLINEDEF	ld;
	MAPSIDEDEF	sdFront, sdBack;
	MAPTHING	thing;
} PRESET;

enum ENUM_PRESET_OBJECT_FLAGS
{
	POF_SECTOR	= 0x01,
	POF_LINE	= 0x02,
	POF_THING	= 0x04
};


DWORD CreateSectorPresetFromSelection(MAP *lpmap, SELECTION_LIST *lpsellistSectors, MAPSECTOR *lpsectorPreset);
DWORD CreateLinePresetFromSelection(MAP *lpmap, SELECTION_LIST *lpsellistLinedefs, MAPLINEDEF *lpldPreset, MAPSIDEDEF *lpsdFrontPreset, MAPSIDEDEF *lpsdBackPreset);
DWORD CreateThingPresetFromSelection(MAP *lpmap, SELECTION_LIST *lpsellistThings, MAPTHING *lpthingPreset);
void RegisterPresetClipboardFormat(void);
BOOL ClipboardContainsPreset(void);
int CopyPresetToClipboard(PRESET *lppreset);
PRESET* PastePresetFromClipboard(void);
void ApplySectorPresetToSelection(MAP *lpmap, SELECTION_LIST *lpsellistSectors, MAPSECTOR *lpsectorPreset, DWORD dwFlags);
void ApplyLinePresetToSelection(MAP *lpmap, SELECTION_LIST *lpsellistLinedefs, MAPLINEDEF *lpldPreset, MAPSIDEDEF *lpsdFrontPreset, MAPSIDEDEF *lpsdBackPreset, DWORD dwFlags);
void ApplyThingPresetToSelection(MAP *lpmap, SELECTION_LIST *lpsellistThings, MAPTHING *lpthingPreset, DWORD dwFlags);

#endif
