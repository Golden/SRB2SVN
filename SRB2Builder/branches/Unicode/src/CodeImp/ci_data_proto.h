#ifndef __SRB2B_CI_DATA_PROTO__
#define __SRB2B_CI_DATA_PROTO__

#ifdef __cplusplus
extern "C" {
#endif

int __fastcall NearestLinedef(int x, int y, MAP *lpmap, int *lpdist);
int NearestConditionedLinedef(int x, int y, MAP *lpmap, int *lpdist, BOOL (*fnCondition)(MAPLINEDEF *lpld, void *lpvParam), void *lpvParam);
int NearestVertex(MAP *lpmap, int x, int y, int *lpdist);
int NearestConditionedVertex(MAP *lpmap, int x, int y, int *lpdist, BOOL (*fnCondition)(MAPVERTEX *lpxv, void *lpvParam), void *lpvParam);
int NearestOtherVertex(MAP *lpmap, int vx, int *lpdist);
int NearestConditionedVertexToLinedef(MAP *lpmap, int iLinedef, int *lpdist, BOOL (*fnCondition)(MAPVERTEX *lpxv, void *lpvParam), void *lpvParam);
int NearestThing(int x, int y, MAPTHING* things, int numthings, int *lpdist, int filterthings, THINGFILTERS* filter);

int __fastcall ThingFiltered(MAPTHING* thing, int filterthings, THINGFILTERS* filter);
void __fastcall ReallocIntP(int** lplpintarray, int oldlength, int newlength);
int FurthestSectorVertex(MAP *lpmap, int line, int sector);

BOOL LinedefSelected(MAPLINEDEF *lpld, void *lpvParam);
BOOL LinedefUnselected(MAPLINEDEF *lpld, void *lpvParam);
BOOL LinedefNoInvalidSD(MAPLINEDEF *lpld, void *lpvParam);
BOOL LinedefDragged(MAPLINEDEF *lpld, void *lpvParam);
BOOL LinedefNotAttachedToVertex(MAPLINEDEF *lpld, void *lpvParam);

BOOL VertexUnlabelled(MAPVERTEX *lpvx, void *lpvParam);
BOOL VertexNotDragging(MAPVERTEX *lpvx, void *lpvParam);
BOOL VertexOnSegment(MAPVERTEX *lpvx, void *lpvLineSegment);

int IntersectSector(int x, int y, MAP *lpmap, BOOL (*fnCondition)(MAPLINEDEF *lpld, void *lpvParam), void *lpvParam);
int IntersectSectorFloat(float x, float y, MAP *lpmap, BOOL (*fnCondition)(MAPLINEDEF *lpld, void *lpvParam), void *lpvParam);

void ResetSelections(MAPTHING* things, int numthings, MAPLINEDEF* linedefs, int numlinedefs, MAPVERTEX* vertices, int numvertices, MAPSECTOR* sectors, int numsectors);

BOOL PointInSidedefs(MAP *lpmap, float x, float y, int* sideslist, int numsides);
BOOL PointInLinedefs(MAP *lpmap, float x, float y, int* lineslist, int numlines);

int CrossingLinedef(MAPLINEDEF* linedefs, MAPVERTEX* vertices, int tl, int begin);
int LinedefIntersectingSegmentAtPoint(MAPLINEDEF* linedefs, MAPVERTEX* vertices, float x1, float y1, float x2, float y2, int begin);


static __inline int InLDRectOpen(MAPLINEDEF* linedefs, MAPVERTEX* vertices, int ld, float x, float y)
{
	int x1 = (int)(vertices[linedefs[ld].v1].x * 10 + 0.5);
	int y1 = (int)(vertices[linedefs[ld].v1].y * 10 + 0.5);
	int x2 = (int)(vertices[linedefs[ld].v2].x * 10 + 0.5);
	int y2 = (int)(vertices[linedefs[ld].v2].y * 10 + 0.5);
	int t;
	int X=(int)(x*10 + 0.5), Y=(int)(y*10 + 0.5);

	if(x1 > x2) {t=x1; x1=x2; x2=t;}
	if(y1 > y2) {t=y1; y1=y2; y2=t;}

	return (X >= x1 && X <= x2 && Y > y1 && Y < y2) || (X > x1 && X < x2 && Y >= y1 && Y <= y2);
}

static __inline int InLDRectClosed(MAPLINEDEF* linedefs, MAPVERTEX* vertices, int ld, float x, float y)
{
	int x1 = (int)(vertices[linedefs[ld].v1].x * 10 + 0.5);
	int y1 = (int)(vertices[linedefs[ld].v1].y * 10 + 0.5);
	int x2 = (int)(vertices[linedefs[ld].v2].x * 10 + 0.5);
	int y2 = (int)(vertices[linedefs[ld].v2].y * 10 + 0.5);
	int t;
	int X=(int)(x*10 + 0.5), Y=(int)(y*10 + 0.5);

	if(x1 > x2) {t=x1; x1=x2; x2=t;}
	if(y1 > y2) {t=y1; y1=y2; y2=t;}

	return (X >= x1 && X <= x2 && Y >= y1 && Y <= y2) || (X >= x1 && X <= x2 && Y >= y1 && Y <= y2);
}

#ifdef __cplusplus
}
#endif

#endif
