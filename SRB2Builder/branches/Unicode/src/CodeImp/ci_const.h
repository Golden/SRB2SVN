/*
'    Doom Builder
'    Copyright (c) 2003 Pascal vd Heiden, www.codeimp.com
'    This program is released under GNU General Public License
'
'    This program is distributed in the hope that it will be useful,
'    but WITHOUT ANY WARRANTY; without even the implied warranty of
'    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
'    GNU General Public License for more details.
*/

#ifndef __SRB2DB_CI_CONST__
#define __SRB2DB_CI_CONST__


// Palette organization
enum ENUM_PALETTECOLORS
{
	CLR_BACKGROUND,
	CLR_VERTEX,
	CLR_VERTEXSELECTED,
	CLR_VERTEXHIGHLIGHT,
	CLR_LINE,
	CLR_LINEDOUBLE,
	CLR_LINESPECIAL,
	CLR_LINESPECIALDOUBLE,
	CLR_LINESELECTED,
	CLR_LINEHIGHLIGHT,
	CLR_LINEDRAG,
	CLR_THINGTAG,
	CLR_SECTORTAG,
	CLR_THINGUNKNOWN,
	CLR_THINGSELECTED,
	CLR_THINGHIGHLIGHT,
	CLR_MULTISELECT,
	CLR_GRID,
	CLR_GRID64,
	CLR_LINEBLOCKSOUND,
	CLR_MAPBOUNDARY,
	CLR_AXES,
	CLR_ZEROHEIGHTLINE,
	CLR_FOFSECTOR,
	CLR_BLACK,
	CLR_MAX
};


// Standard linedef flags
enum ENUM_LINEDEFFLAGS
{
	LDF_IMPASSABLE = 1,
	LDF_BLOCKMONSTER = 2,
	LDF_TWOSIDED = 4,
	LDF_UPPERUNPEGGED = 8,
	LDF_LOWERUNPEGGED = 16,
	LDF_SECRET = 32,
	LDF_BLOCKSOUND = 64,
	LDF_HIDDEN = 128,
	LDF_SHOWN = 256
};

// Sector editing flags
enum ENUM_SECTOREDITFLAGS
{
	SED_HASFOF = 1
};

#endif

