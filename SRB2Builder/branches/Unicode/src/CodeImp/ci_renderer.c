/*
'    Doom Builder
'    Copyright (c) 2003 Pascal vd Heiden, www.codeimp.com
'    This program is released under GNU General Public License
'
'    This program is distributed in the hope that it will be useful,
'    but WITHOUT ANY WARRANTY; without even the implied warranty of
'    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
'    GNU General Public License for more details.
*/


// Definitions
#define WIN32_LEAN_AND_MEAN

// Includes
#include <windows.h>
#include <stdio.h>

// Glue between DB's renderer and SRB2B.
#include "../renderer.h"
#include "../maptypes.h"

#include "ci_const.h"
#include "ci_math.h"
#include "ci_renderer.h"


// sng: Returns 1 for positive, 0 for 0, -1 for negative
//----------------------------------------------------------------------------
static __inline int sgn(int s) { if(s) return (int)(s / fabs(s)); else return 0; }



// Render_LinedefLine: Renders a linedef line
//----------------------------------------------------------------------------
void __fastcall Render_LinedefLineF(int x1, int y1, int x2, int y2, BYTE c, int sl)
{
	float ix2;
	float iy2;

	// Render linedef line
	Render_LineF((float)x1, (float)y1, (float)x2, (float)y2, c);

	// Render indicator?
	if(sl)
	{
		// Middle of indicator line
		float lx = (float)(x2 - x1) * 0.5f;
		float ly = (float)(-y2 + y1) * 0.5f;

		// Indicator line begin coordinates
		float ix1 = x1 + lx;
		float iy1 = -y1 + ly;

		// Normalize slope and calculate coordinates
		float len = (float)sqrt(lx * lx + ly * ly);

		if(len)
		{
			iy2 = iy1 + (lx / len) * sl;
			ix2 = ix1 - (ly / len) * sl;
		}
		else
		{
			ix2 = ix1;
			iy2 = iy1;
		}

		// Render indicator line
		Render_LineF(ix1, -iy1, ix2, -iy2, c);
	}
}



// Render_LineLength: Renders a single line's length, in its middle.
//----------------------------------------------------------------------------
void __fastcall Render_LineLengthF(int x1, int y1, int x2, int y2, BYTE byColour, float fZoom)
{
	float cx, cy;
	float x, y;
	int iLength;

	// Half the lengths of the line.
	cx = (x2 - x1) / 2.0f;
	cy = (y2 - y1) / 2.0f;

	// Length of line
	iLength = (int)floor(0.5f + distancei(x1, y1, x2, y2));

	if(iLength == 0) return;

	// Start offset for text
	x = x1 + (cx - (((RENDERER_FONT_WIDTH_PX / 2.0f) * (float)ceil(log10(iLength)) / fZoom) / 2));
	y = y1 + cy;

	/* Draw the text on top of a background rectangle. */
	Render_Rect(x, y - 1/fZoom, x + RENDERER_FONT_WIDTH_PX * (float)ceil(log10(iLength)) / fZoom, y + (RENDERER_FONT_HEIGHT_PX - 1) / fZoom, CLR_BACKGROUND);
	Render_Printf(x, y, byColour, TEXT("%d"), iLength);
}
