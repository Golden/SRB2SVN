#include <windows.h>

#include "general.h"
#include "map.h"
#include "maptypes.h"
#include "prefab.h"
#include "selection.h"
#include "editing.h"
#include "cliputil.h"

#include "win/mdiframe.h"


/* Macros. */
#define PREFAB_MAGIC			MAKELONG(MAKEWORD('P', 'R'), MAKEWORD('F', 'B'))
#define PREFAB_VERSION			0
#define PREFAB_CLIPBOARDFORMAT	TEXT("SRB2 Builder Prefab")


/* Types. */
struct _PREFAB
{
	DWORD		dwMagicNumber;
	WORD		wVersion;

	int			iSectors, iLinedefs, iSidedefs, iVertices, iThings;
	DWORD		cbSectorOffset, cbLinedefOffset, cbSidedefOffset, cbVertexOffset, cbThingOffset;

	/* The map structures follow the PREFAB structure itself, at offsets from
	 * the beginning of the prefab described by cb*.
	 */
};


/* Globals. */
static UINT		g_uiPrefabClipboardFormat;


/* Static prototypes. */
static __inline DWORD GetPrefabSize(PREFAB *lpprefab);


/* CreatePrefabFromSelection
 *   Creates a prefab structure from the current selection.
 *
 * Parameters:
 *   MAP*			lpmap			Map data.
 *   SELECTION*		lpselection		Selection.
 *
 * Return value: PREFAB*
 *   Pointer to new prefab.
 *
 * Remarks:
 *   Call FreePrefab to free the memory allocated.
 */
PREFAB* CreatePrefabFromSelection(MAP *lpmap)
{
	PREFAB *lpprefab;
	PREFAB prefab;

	MAPSECTOR *lpsectors;
	MAPLINEDEF *lplinedefs;
	MAPSIDEDEF *lpsidedefs;
	MAPVERTEX *lpvertices;
	MAPTHING *lpthings;

	/* Index translation arrays. */
	int *lpiNewSectorIndices, *lpiNewLinedefIndices, *lpiNewSidedefIndices;
	int *lpiNewVertexIndices;

	LOOPLIST *lplooplist;

	int i, iCopiedThings;


	/* Do enclosure labelling so we know which sectors to include. */
	LabelSelectedLinesRS(lpmap);
	lplooplist = LabelRSLinesLoops(lpmap);
	LabelRSLinesEnclosure(lpmap, lplooplist);
	DestroyLoopList(lplooplist);

	/* Label the used vertices, which are not necessarily selected. */
	for(i = 0; i < lpmap->iLinedefs; i++)
	{
		if(lpmap->linedefs[i].selected)
		{
			lpmap->vertices[lpmap->linedefs[i].v1].editflags |= VEF_USED;
			lpmap->vertices[lpmap->linedefs[i].v2].editflags |= VEF_USED;
		}
	}

	/* Allocate the index-mapping tables. */
	lpiNewSectorIndices = ProcHeapAlloc(lpmap->iSectors * sizeof(int));
	lpiNewLinedefIndices = ProcHeapAlloc(lpmap->iLinedefs * sizeof(int));
	lpiNewSidedefIndices = ProcHeapAlloc(lpmap->iSidedefs * sizeof(int));
	lpiNewVertexIndices = ProcHeapAlloc(lpmap->iVertices * sizeof(int));
	for(i = 0; i < lpmap->iSectors; i++) lpiNewSectorIndices[i] = -1;
	for(i = 0; i < lpmap->iLinedefs; i++) lpiNewLinedefIndices[i] = -1;
	for(i = 0; i < lpmap->iSidedefs; i++) lpiNewSidedefIndices[i] = -1;
	for(i = 0; i < lpmap->iVertices; i++) lpiNewVertexIndices[i] = -1;


	/* Set up the initial prefab stuff. */
	prefab.dwMagicNumber = PREFAB_MAGIC;
	prefab.wVersion = PREFAB_VERSION;

	/* Find selection counts. */
	prefab.iSectors = prefab.iLinedefs = prefab.iVertices = prefab.iThings = prefab.iSidedefs = 0;

	/* Count linedefs, sidedefs and sectors. */
	for(i = 0; i < lpmap->iLinedefs; i++)
	{
		if(lpmap->linedefs[i].selected)
		{
			lpiNewLinedefIndices[i] = prefab.iLinedefs++;

			if(SidedefExists(lpmap, lpmap->linedefs[i].s1) && lpiNewSidedefIndices[lpmap->linedefs[i].s1] < 0)
			{
				lpiNewSidedefIndices[lpmap->linedefs[i].s1] = prefab.iSidedefs++;
				if((lpmap->linedefs[i].editflags & LEF_ENCLOSEDFRONT) && lpiNewSectorIndices[lpmap->sidedefs[lpmap->linedefs[i].s1].sector] < 0)
				{
					lpiNewSectorIndices[lpmap->sidedefs[lpmap->linedefs[i].s1].sector] = prefab.iSectors++;
				}
			}

			if(SidedefExists(lpmap, lpmap->linedefs[i].s2) && lpiNewSidedefIndices[lpmap->linedefs[i].s2] < 0)
			{
				lpiNewSidedefIndices[lpmap->linedefs[i].s2] = prefab.iSidedefs++;
				if((lpmap->linedefs[i].editflags & LEF_ENCLOSEDBACK) && lpiNewSectorIndices[lpmap->sidedefs[lpmap->linedefs[i].s2].sector] < 0)
				{
					lpiNewSectorIndices[lpmap->sidedefs[lpmap->linedefs[i].s2].sector] = prefab.iSectors++;
				}
			}
		}
	}

	/* Count vertices and things. */
	for(i = 0; i < lpmap->iVertices; i++)
		if(lpmap->vertices[i].selected || (lpmap->vertices[i].editflags & VEF_USED))
			lpiNewVertexIndices[i] = prefab.iVertices++;

	for(i = 0; i < lpmap->iThings; i++) if(lpmap->things[i].selected) prefab.iThings++;


	/* We know enough to allocate our memory now. Map it out first, and then do
	 * so.
	 */
	prefab.cbSectorOffset = sizeof(PREFAB);
	prefab.cbLinedefOffset = prefab.cbSectorOffset + prefab.iSectors * sizeof(MAPSECTOR);
	prefab.cbSidedefOffset = prefab.cbLinedefOffset + prefab.iLinedefs * sizeof(MAPLINEDEF);
	prefab.cbVertexOffset = prefab.cbSidedefOffset + prefab.iSidedefs * sizeof(MAPSIDEDEF);
	prefab.cbThingOffset = prefab.cbVertexOffset + prefab.iVertices * sizeof(MAPVERTEX);

	lpprefab = ProcHeapAlloc(GetPrefabSize(&prefab));


	/* Copy the stack prefab structure into our heap one. */
	*lpprefab = prefab;

	/* Get pointers to the individual sections. */
	lpsectors = (MAPSECTOR*)((BYTE*)lpprefab + lpprefab->cbSectorOffset);
	lplinedefs = (MAPLINEDEF*)((BYTE*)lpprefab + lpprefab->cbLinedefOffset);
	lpsidedefs = (MAPSIDEDEF*)((BYTE*)lpprefab + lpprefab->cbSidedefOffset);
	lpvertices = (MAPVERTEX*)((BYTE*)lpprefab + lpprefab->cbVertexOffset);
	lpthings = (MAPTHING*)((BYTE*)lpprefab + lpprefab->cbThingOffset);


	/* Begin by making shallow copies of all the objects we're interested in. We
	 * go back and correct references later, using the maps that we established
	 * above.
	 */

	/* Copy objects into prefab. */
	for(i = 0; i < lpmap->iSectors; i++)
		if(lpiNewSectorIndices[i] >= 0)
			lpsectors[lpiNewSectorIndices[i]] = lpmap->sectors[i];

	for(i = 0; i < lpmap->iLinedefs; i++)
		if(lpiNewLinedefIndices[i] >= 0)
			lplinedefs[lpiNewLinedefIndices[i]] = lpmap->linedefs[i];

	/* Note that we really do want >= 0 here. */
	for(i = 0; i < lpmap->iSidedefs; i++)
		if(lpiNewSidedefIndices[i] >= 0 && SidedefExists(lpmap, lpiNewSidedefIndices[i]))
			lpsidedefs[lpiNewSidedefIndices[i]] = lpmap->sidedefs[i];

	for(i = 0; i < lpmap->iVertices; i++)
		if(lpiNewVertexIndices[i] >= 0)
			lpvertices[lpiNewVertexIndices[i]] = lpmap->vertices[i];

	iCopiedThings = 0;
	for(i = 0; i < lpmap->iThings; i++)
		if(lpmap->things[i].selected)
			lpthings[iCopiedThings++] = lpmap->things[i];


	/* Correct the inter-object references and clear highlight flags. */
	for(i = 0; i < lpprefab->iLinedefs; i++)
	{
		if(SidedefExists(lpmap, lplinedefs[i].s1)) lplinedefs[i].s1 = lpiNewSidedefIndices[lplinedefs[i].s1];
		if(SidedefExists(lpmap, lplinedefs[i].s2)) lplinedefs[i].s2 = lpiNewSidedefIndices[lplinedefs[i].s2];
		lplinedefs[i].v1 = lpiNewVertexIndices[lplinedefs[i].v1];
		lplinedefs[i].v2 = lpiNewVertexIndices[lplinedefs[i].v2];
		lplinedefs[i].highlight = 0;
	}

	for(i = 0; i < lpprefab->iSidedefs; i++)
	{
		lpsidedefs[i].sector = lpiNewSectorIndices[lpsidedefs[i].sector];
		lpsidedefs[i].linedef = lpiNewLinedefIndices[lpsidedefs[i].linedef];
	}

	for(i = 0;  i < lpprefab->iVertices; i++)
	{
		lpvertices[i].highlight = 0;

		/* The vertices weren't necessarily selected before, but they ought to
		 * be.
		 */
		lpvertices[i].selected = SLF_SELECTED;
	}

	for(i = 0;  i < lpprefab->iThings; i++) lpthings[i].highlight = 0;



	/* Clean up. */

	/* Clear the flags we set. */
	ClearDraggingFlags(lpmap);
	for(i = 0; i < lpmap->iVertices; i++) lpmap->vertices[i].editflags &= ~VEF_USED;
	for(i = 0; i < lpprefab->iVertices; i++) lpvertices[i].editflags &= ~VEF_USED;

	/* Free memory. */
	ProcHeapFree(lpiNewSectorIndices);
	ProcHeapFree(lpiNewLinedefIndices);
	ProcHeapFree(lpiNewSidedefIndices);
	ProcHeapFree(lpiNewVertexIndices);

	/* Finished! */
	return lpprefab;
}


/* InsertPrefab
 *   Inserts a prefab into a map.
 *
 * Parameters:
 *   MAP*		lpmap		Map data.
 *   PREFAB*	lpprefab	Prefab.
 *
 * Return value: None.
 *
 * Remarks:
 *   The position at which the prefab is to be inserted is not specified. There
 *   would be little point in doing so, as there is no sensible way to determine
 *   where it ought to go before it's part of the map, and inserting a prefab is
 *   always followed by the user moving it about in any case. The only caveat is
 *   that the caller should not allow a redraw to occur before moving the prefab
 *   to the mouse position.
 */
void InsertPrefab(MAP *lpmap, PREFAB *lpprefab)
{
	int i;

	/* Firstly, we append the prefab's objects to the map's arrays. */
	CopyMemory(&lpmap->sectors[lpmap->iSectors], (BYTE*)lpprefab + lpprefab->cbSectorOffset, lpprefab->iSectors * sizeof(MAPSECTOR));
	CopyMemory(&lpmap->linedefs[lpmap->iLinedefs], (BYTE*)lpprefab + lpprefab->cbLinedefOffset, lpprefab->iLinedefs * sizeof(MAPLINEDEF));
	CopyMemory(&lpmap->sidedefs[lpmap->iSidedefs], (BYTE*)lpprefab + lpprefab->cbSidedefOffset, lpprefab->iSidedefs * sizeof(MAPSIDEDEF));
	CopyMemory(&lpmap->vertices[lpmap->iVertices], (BYTE*)lpprefab + lpprefab->cbVertexOffset, lpprefab->iVertices * sizeof(MAPVERTEX));
	CopyMemory(&lpmap->things[lpmap->iThings], (BYTE*)lpprefab + lpprefab->cbThingOffset, lpprefab->iThings * sizeof(MAPTHING));

	/* Now we correct the inter-object references. Note that none of the new
	 * objects refers to an existing object, and vice versa.
	 */
	for(i = lpmap->iLinedefs; i < lpmap->iLinedefs + lpprefab->iLinedefs; i++)
	{
		/* Notice that we can't use SidedefExists here. */
		if(lpmap->linedefs[i].s1 != INVALID_SIDEDEF) lpmap->linedefs[i].s1 += lpmap->iSidedefs;
		if(lpmap->linedefs[i].s2 != INVALID_SIDEDEF) lpmap->linedefs[i].s2 += lpmap->iSidedefs;
		lpmap->linedefs[i].v1 += lpmap->iVertices;
		lpmap->linedefs[i].v2 += lpmap->iVertices;
	}

	for(i = lpmap->iSidedefs; i < lpmap->iSidedefs + lpprefab->iSidedefs; i++)
	{
		lpmap->sidedefs[i].sector += lpmap->iSectors;
		lpmap->sidedefs[i].linedef += lpmap->iLinedefs;
	}

	/* Increase map object counts. */
	lpmap->iSectors += lpprefab->iSectors;
	lpmap->iLinedefs += lpprefab->iLinedefs;
	lpmap->iSidedefs += lpprefab->iSidedefs;
	lpmap->iVertices += lpprefab->iVertices;
	lpmap->iThings += lpprefab->iThings;
}


/* RegisterPrefabClipboardFormat
 *   Registers the clipboard format for prefabs.
 *
 * Parameters:
 *   None.
 *
 * Return value: None.
 *
 * Remakrs:
 *   This function must be called before any of the other clipboard functions.
 */
void RegisterPrefabClipboardFormat(void)
{
	g_uiPrefabClipboardFormat = RegisterClipboardFormat(PREFAB_CLIPBOARDFORMAT);
}


/* ClipboardContainsPrefab
 *   Determines whether the clipboard contains a prefab.
 *
 * Parameters:
 *   None.
 *
 * Return value: BOOL
 *   TRUE if the clipboard contains a prefab; FALSE otherwise.
 */
BOOL ClipboardContainsPrefab(void)
{
	return IsClipboardFormatAvailable(g_uiPrefabClipboardFormat);
}


/* CopyPrefabToClipboard
 *   Copies a prefab to the clipboard.
 *
 * Parameters:
 *   PREFAB*	lpprefab	Prefab.
 *
 * Return value: int
 *   Zero on success; non-zero on error.
 */
int CopyPrefabToClipboard(PREFAB *lpprefab)
{
	return CopyBufferToClipboard(lpprefab, GetPrefabSize(lpprefab), g_uiPrefabClipboardFormat);
}


/* PastePrefabFromClipboard
 *   Pastes a prefab from the clipboard.
 *
 * Parameters:
 *   None.
 *
 * Return value: PREFAB*
 *   Pointer to the prefab from the clipboard if successful; NULL on error
 *   (including there being no prefab on the clipboard).
 *
 * Remarks:
 *   The caller is responsible for freeing the returned prefab.
 */
PREFAB* PastePrefabFromClipboard(void)
{
	return (PREFAB*)GetBufferFromClipboard(g_uiPrefabClipboardFormat);
}


/* GetPrefabSize
 *   Gets the size of a prefab in memory.
 *
 * Parameters:
 *   PREFAB*	lpprefab	Prefab.
 *
 * Return value: DWORD
 *   Size of the prefab, in bytes.
 *
 * Remarks:
 *   This function only reads the prefab stub, so it's safe to call this as a
 *   way of determining how much memory to allocate for the entire prefab after
 *   the stub has been read/generated.
 */
static __inline DWORD GetPrefabSize(PREFAB *lpprefab)
{
	return lpprefab->cbThingOffset + lpprefab->iThings * sizeof(MAPTHING);
}
