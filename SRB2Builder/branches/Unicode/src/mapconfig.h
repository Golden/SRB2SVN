#ifndef __SRB2B_MAPCONFIG__
#define __SRB2B_MAPCONFIG__

#include <windows.h>

#include "config.h"
#include "map.h"


#define MAPCFG_GAME			TEXT("game")
#define MAPCFG_ID			TEXT("id")

#define FLAT_THING_SECTION	TEXT("__things")
#define FLAT_LINE_SECTION	TEXT("__ldtypesflat")

#define MAPCFG_WIKI			TEXT("wiki")
#define WIKI_LDEFFECT		TEXT("ldeffect")
#define WIKI_SECEFFECT		TEXT("seceffect")
#define WIKI_THING			TEXT("thing")


int LoadMapConfigs(void);
void UnloadMapConfigs(void);
void AddMapConfigsToComboBox(HWND hwndCombo);
CONFIG* GetThingConfigInfo(CONFIG *lpcfgThings, unsigned short unType);
void GetThingTypeDisplayText(unsigned short unType, CONFIG *lpcfgThings, LPTSTR szBuffer, unsigned short cchBuffer);
void GetThingDirectionDisplayText(short nDirection, LPTSTR szBuffer, unsigned short cbBuffer);
void GetEffectDisplayText(unsigned short unEffect, CONFIG *lpcfgSectors, LPTSTR szBuffer, unsigned short cchBuffer);
void SetAllThingPropertiesFromType(MAP *lpmap, CONFIG *lpcfgFlatThings);
WORD GetZFactor(CONFIG *lpcfgFlatThings, unsigned short unType);
void SetThingPropertiesFromType(MAP *lpmap, int iThing, CONFIG *lpcfgFlatThings);
int GetIWadForConfig(CONFIG *lpcfgMap);
CONFIG *GetDefaultMapConfig(void);
CONFIG* GetMapConfigByID(LPCTSTR szMapConfigID);

#endif
