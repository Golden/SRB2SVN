#ifndef __SRB2B_WADOPTS__
#define __SRB2B_WADOPTS__

#include <windows.h>

#include "config.h"


#define WADOPT_MAPCONFIG		TEXT("mapconfig")
#define WADOPT_EXTNODEBUILD		TEXT("extnodebuild")
#define WADOPT_DEFAULTTEX		TEXT("defaulttexture")
#define WADOPT_DEFAULTSEC		TEXT("defaultsector")


CONFIG* LoadWadOptions(LPCTSTR szWadFilename);
CONFIG* NewWadOptions(void);
void StoreMapCfgForWad(CONFIG *lpcfgWad, CONFIG *lpcfgMap);
CONFIG* GetMapOptionsFromWadOpt(CONFIG *lpcfgWadOpt, LPCTSTR szLumpname);
int WriteWadOptions(LPCTSTR szWadFilename, CONFIG *lpcfgWad);
CONFIG* GetDefaultMapConfigForWad(int iWad);


#endif

