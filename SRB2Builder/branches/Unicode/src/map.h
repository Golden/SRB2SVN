#ifndef __SRB2B_MAP__
#define __SRB2B_MAP__

#include "maptypes.h"
#include "config.h"
#include "wad.h"
#include "like.h"

/* Function prototypes. */
MAP *AllocateMapStructure(void);
void DestroyMapStructure(MAP *lpmap);
int MapLoad(MAP *lpmap, int iWad, LPCSTR szLumpname);
int MapLoadForPreview(MAP *lpmap, int iWad, LPCSTR szLumpname);
void UpdateMap(MAP *lpmap, int iWad, LPCSTR szLumpname, CONFIG *lpcfgMap);
void UpdateMapToWadStructure(MAP *lpmap, WAD *lpwad, LPCSTR szLumpname, CONFIG *lpcfgMap);
void IterateMapLumpNames(int iWad, void (*fnCallback)(LPCTSTR, void*), void *lpvParam);
int RenameMapInWad(int iWad, LPCSTR sOldName, LPCSTR sNewName);
int MapNumberFromLumpname(LPCSTR sLumpname);
int MapLumpnameFromNumber(int iMapNumber, LPSTR szLumpname);
int GetFirstFreeMapLumpname(WAD *lpwad, LPSTR szLumpname);
int DeleteMap(WAD *lpwad, LPCSTR szLumpname, CONFIG *lpcfgMap);


/* Macros. */
#define SECEFFECT_NYBBLE0(x) ((x) & 0xF)
#define SECEFFECT_NYBBLE1(x) (((x) & 0xF0) >> 4)
#define SECEFFECT_NYBBLE2(x) (((x) & 0xF00) >> 8)
#define SECEFFECT_NYBBLE3(x) (((x) & 0xF000) >> 12)

#define NYBBLE0_TO_SECEFFECTMASK(x) ((x) & 0xF)
#define NYBBLE1_TO_SECEFFECTMASK(x) (((x) & 0xF) << 4)
#define NYBBLE2_TO_SECEFFECTMASK(x) (((x) & 0xF) << 8)
#define NYBBLE3_TO_SECEFFECTMASK(x) (((x) & 0xF) << 12)

#define INVALID_SIDEDEF ((unsigned short)65535)
#define MAPNUMBER_MIN 1
#define MAPNUMBER_MAX 1035

#define MAX_LINEDEFS	65536
#define MAX_SIDEDEFS	65536
#define MAX_SECTORS		65536
#define MAX_VERTICES	65536
#define MAX_THINGS		65536


/* Inline functions. */

static __inline BOOL LinedefBelongsToSector(MAP *lpmap, int iLinedefIndex, int iSectorIndex);
static __inline BOOL SidedefExists(MAP *lpmap, unsigned short unSidedef);
#ifdef UNICODE
static __inline BOOL IsMapLumpnameW(LPCWSTR szLumpName);
#endif
static __inline BOOL IsMapLumpnameA(LPCSTR szLumpName);


static __inline BOOL LinedefBelongsToSector(MAP *lpmap, int iLinedefIndex, int iSectorIndex)
{
	int s1 = lpmap->linedefs[iLinedefIndex].s1, s2 = lpmap->linedefs[iLinedefIndex].s2;
	if(s1 != INVALID_SIDEDEF && lpmap->sidedefs[s1].sector == iSectorIndex) return TRUE;
	if(s2 != INVALID_SIDEDEF && lpmap->sidedefs[s2].sector == iSectorIndex) return TRUE;
	return FALSE;
}

static __inline BOOL SidedefExists(MAP *lpmap, unsigned short unSidedef)
{
	return unSidedef != INVALID_SIDEDEF && unSidedef < lpmap->iSidedefs;
}


#ifdef UNICODE

static __inline BOOL IsMapLumpnameW(LPCWSTR szLumpName)
{
	CHAR szLumpNameA[CCH_LUMPNAME + 1];
	WideCharToMultiByte(CP_ACP, 0, szLumpName, -1, szLumpNameA, NUM_ELEMENTS(szLumpNameA), NULL, NULL);
	return IsMapLumpnameA(szLumpNameA);
}

#define IsMapLumpname IsMapLumpnameW

#else

#define IsMapLumpname IsMapLumpnameA

#endif


static __inline BOOL IsMapLumpnameA(LPCSTR szLumpName)
{
	return LikeA(szLumpName, "MAP##") || LikeA(szLumpName, "MAP[A-Z][0-9A-Z]") || LikeA(szLumpName, "E#M#");
}


#endif
