#ifndef __SRB2B_WIKI__
#define __SRB2B_WIKI__

#include <windows.h>


typedef enum _ENUM_WIKIQUERIES
{
	WIKIQUERY_LDEFFECT,
	WIKIQUERY_SECEFFECT,
	WIKIQUERY_THING
} ENUM_WIKIQUERIES;


void WikiMainPage(void);
void WikiEffectQuery(CONFIG *lpcfgMap, ENUM_WIKIQUERIES wikiquery, int iIndex);


#endif
