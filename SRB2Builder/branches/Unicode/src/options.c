#include <windows.h>
#include <string.h>
#include <tchar.h>

#include "general.h"
#include "config.h"
#include "options.h"
#include "keyboard.h"
#include "editing.h"
#include "mapconfig.h"
#include "testing.h"

#include "../res/resource.h"

/* Macros. */
#define OPTFILE_SIGNATURE	TEXT("SRB2 Builder Configuration")
#define OPTFILENAME			TEXT("builder.cfg")

/* Default values that are used multiple times. */
#define OPT_DEFAULT_DEFTEXTURE	TEXT("GFZROCK")


/* Globals. */
CONFIG *g_lpcfgMain = NULL;
int g_iShortcutCodes[SCK_MAX];
RENDEREROPTIONS g_rendopts;
LPTSTR g_szOptDir;


/* Static prototypes. */
static void LoadShortcutKeys(void);
static void SetShortcutKeys(void);
static void LoadRendererOptions(void);
static void SetRendererOptions(void);
static void SetMissingOptions(void);
static void SetDefaultPalette(void);
static void SetDefaultTextures(void);
static void SetDefaultSector(void);
static void SetDefaultNodebuilderOptions(void);
static void SetDefaultWikiOptions(void);
static void UntaintOptions(void);
static void RemoveStaleMRUEntries(void);


/* LoadMainConfigurationFile
 *   Loads the main settings file and reads the settings into their appropriate
 *   places.
 *
 * Parameters:
 *   None.
 *
 * Return value: BOOL
 *   TRUE on success; FALSE on failure.
 */
BOOL LoadMainConfigurationFile(void)
{
	LPTSTR szType;
	int iLen;
	BOOL bRet;

	/* Save working directory. */
	TCHAR tc;
	DWORD cchOldDir = GetCurrentDirectory(1, &tc);
	LPTSTR szOldDir = ProcHeapAlloc(cchOldDir * sizeof(TCHAR));
	GetCurrentDirectory(cchOldDir, szOldDir);

	/* Load file. */
	bRet = SetCurrentDirectory(g_szOptDir);
	g_lpcfgMain = ConfigLoad(OPTFILENAME);

	/* Change back to our old wd. */
	SetCurrentDirectory(szOldDir);
	ProcHeapFree(szOldDir);

	/* Bad options directory? */
	if(!bRet) return FALSE;

	if(!g_lpcfgMain)
	{
		/* Make a new config file. */
		g_lpcfgMain = ConfigCreate();
		ConfigSetString(g_lpcfgMain, TEXT("type"), OPTFILE_SIGNATURE);

		/* Defaults are set later. */
	}
	else
	{
		iLen = ConfigGetStringLength(g_lpcfgMain, TEXT("type"));

		if(iLen <= 0)
		{
			UnloadMainConfigurationFile();
			return FALSE;
		}

		szType = ProcHeapAlloc((iLen + 1) * sizeof(TCHAR));
		ConfigGetString(g_lpcfgMain, TEXT("type"), szType, iLen + 1);

		if(_tcsicmp(szType, OPTFILE_SIGNATURE))
		{
			UnloadMainConfigurationFile();
			ProcHeapFree(szType);
			return FALSE;
		}

		ProcHeapFree(szType);
	}

	/* Set any missing options. */
	SetMissingOptions();

	/* Fix anything dangerous. */
	UntaintOptions();

	/* Expunge any stale MRU entries. */
	RemoveStaleMRUEntries();

	/* Copies from tree into other storage, for efficiency. */
	LoadShortcutKeys();
	LoadRendererOptions();

	return TRUE;
}


/* UnloadMainConfigurationFile
 *   Cleans up the settings structures.
 *
 * Parameters:
 *   None.
 *
 * Return value: None.
 */
void UnloadMainConfigurationFile(void)
{
	if(g_lpcfgMain)
	{
		TCHAR tc;
		DWORD cchOldDir;
		LPTSTR szOldDir;

		/* Copy settings back into the tree. */
		SetShortcutKeys();
		SetRendererOptions();

		/* Save working directory. */
		cchOldDir = GetCurrentDirectory(1, &tc);
		szOldDir = ProcHeapAlloc(cchOldDir * sizeof(TCHAR));
		GetCurrentDirectory(cchOldDir, szOldDir);

		/* Save. */
		SetCurrentDirectory(g_szOptDir);
		if(ConfigWrite(g_lpcfgMain, OPTFILENAME))
			MessageBoxFromStringTable(NULL, IDS_ERROR_WRITECONFIG, MB_ICONERROR);

		/* Change back to our old wd. */
		SetCurrentDirectory(szOldDir);
		ProcHeapFree(szOldDir);

		ConfigDestroy(g_lpcfgMain);
	}
}


/* LoadShortcutKeys, SetShortcutKeys
 *   Copies shortcut key settings between the tree and the array.
 *
 * Parameters:
 *   None.
 *
 * Return value: None.
 */
static void LoadShortcutKeys(void)
{
	CONFIG *lpcfgShortcuts = ConfigGetSubsection(g_lpcfgMain, OPT_SHORTCUTS);

	g_iShortcutCodes[SCK_EDITQUICKMOVE] = ConfigGetInteger(lpcfgShortcuts, TEXT("editquickmove"));
	g_iShortcutCodes[SCK_ZOOMIN] = ConfigGetInteger(lpcfgShortcuts, TEXT("zoomin"));
	g_iShortcutCodes[SCK_ZOOMOUT] = ConfigGetInteger(lpcfgShortcuts, TEXT("zoomout"));
	g_iShortcutCodes[SCK_CENTREVIEW] = ConfigGetInteger(lpcfgShortcuts, TEXT("editcenterview"));
	g_iShortcutCodes[SCK_EDITMOVE] = ConfigGetInteger(lpcfgShortcuts, TEXT("editmove"));
	g_iShortcutCodes[SCK_EDITANY] = ConfigGetInteger(lpcfgShortcuts, TEXT("editany"));
	g_iShortcutCodes[SCK_EDITLINES] = ConfigGetInteger(lpcfgShortcuts, TEXT("editlines"));
	g_iShortcutCodes[SCK_EDITSECTORS] = ConfigGetInteger(lpcfgShortcuts, TEXT("editsectors"));
	g_iShortcutCodes[SCK_EDITVERTICES] = ConfigGetInteger(lpcfgShortcuts, TEXT("editvertices"));
	g_iShortcutCodes[SCK_EDITTHINGS] = ConfigGetInteger(lpcfgShortcuts, TEXT("editthings"));
	g_iShortcutCodes[SCK_EDIT3D] = ConfigGetInteger(lpcfgShortcuts, TEXT("edit3d"));
	g_iShortcutCodes[SCK_EDITSNAPTOGRID] = ConfigGetInteger(lpcfgShortcuts, TEXT("togglesnap"));
	g_iShortcutCodes[SCK_FLIPLINEDEFS] = ConfigGetInteger(lpcfgShortcuts, TEXT("fliplinedefs"));
	g_iShortcutCodes[SCK_FLIPSIDEDEFS] = ConfigGetInteger(lpcfgShortcuts, TEXT("flipsidedefs"));
	g_iShortcutCodes[SCK_SPLITLINEDEFS] = ConfigGetInteger(lpcfgShortcuts, TEXT("splitlinedefs"));
	g_iShortcutCodes[SCK_JOINSECTORS] = ConfigGetInteger(lpcfgShortcuts, TEXT("joinsector"));
	g_iShortcutCodes[SCK_MERGESECTORS] = ConfigGetInteger(lpcfgShortcuts, TEXT("mergesector"));
	g_iShortcutCodes[SCK_UNDO] = ConfigGetInteger(lpcfgShortcuts, TEXT("editundo"));
	g_iShortcutCodes[SCK_REDO] = ConfigGetInteger(lpcfgShortcuts, TEXT("editredo"));
	g_iShortcutCodes[SCK_FLIPHORIZ] = ConfigGetInteger(lpcfgShortcuts, TEXT("editfliph"));
	g_iShortcutCodes[SCK_FLIPVERT] = ConfigGetInteger(lpcfgShortcuts, TEXT("editflipv"));
	g_iShortcutCodes[SCK_COPY] = ConfigGetInteger(lpcfgShortcuts, TEXT("editcopy"));
	g_iShortcutCodes[SCK_PASTE] = ConfigGetInteger(lpcfgShortcuts, TEXT("editpaste"));
	g_iShortcutCodes[SCK_SAVEAS] = ConfigGetInteger(lpcfgShortcuts, TEXT("filesaveas"));
	g_iShortcutCodes[SCK_INCFLOOR] = ConfigGetInteger(lpcfgShortcuts, TEXT("incfloor"));
	g_iShortcutCodes[SCK_DECFLOOR] = ConfigGetInteger(lpcfgShortcuts, TEXT("decfloor"));
	g_iShortcutCodes[SCK_INCCEIL] = ConfigGetInteger(lpcfgShortcuts, TEXT("incceil"));
	g_iShortcutCodes[SCK_DECCEIL] = ConfigGetInteger(lpcfgShortcuts, TEXT("decceil"));
	g_iShortcutCodes[SCK_INCLIGHT] = ConfigGetInteger(lpcfgShortcuts, TEXT("inclight"));
	g_iShortcutCodes[SCK_DECLIGHT] = ConfigGetInteger(lpcfgShortcuts, TEXT("declight"));
	g_iShortcutCodes[SCK_INCTHINGZ] = ConfigGetInteger(lpcfgShortcuts, TEXT("incthingz"));
	g_iShortcutCodes[SCK_DECTHINGZ] = ConfigGetInteger(lpcfgShortcuts, TEXT("decthingz"));
	g_iShortcutCodes[SCK_SNAPSELECTION] = ConfigGetInteger(lpcfgShortcuts, TEXT("snapselection"));
	g_iShortcutCodes[SCK_GRADIENTFLOORS] = ConfigGetInteger(lpcfgShortcuts, TEXT("gradientfloors"));
	g_iShortcutCodes[SCK_GRADIENTCEILINGS] = ConfigGetInteger(lpcfgShortcuts, TEXT("gradientceilings"));
	g_iShortcutCodes[SCK_GRADIENTBRIGHTNESS] = ConfigGetInteger(lpcfgShortcuts, TEXT("gradientbrightness"));
	g_iShortcutCodes[SCK_INSERT] = ConfigGetInteger(lpcfgShortcuts, TEXT("drawsector"));
	g_iShortcutCodes[SCK_DCKINSERT] = ConfigGetInteger(lpcfgShortcuts, TEXT("drawrect"));
	g_iShortcutCodes[SCK_EDITCUT] = ConfigGetInteger(lpcfgShortcuts, TEXT("editcut"));
	g_iShortcutCodes[SCK_EDITDELETE] = ConfigGetInteger(lpcfgShortcuts, TEXT("editdelete"));
	g_iShortcutCodes[SCK_CANCEL] = ConfigGetInteger(lpcfgShortcuts, TEXT("cancel"));
	g_iShortcutCodes[SCK_GRADIENTTHINGZ] = ConfigGetInteger(lpcfgShortcuts, TEXT("gradientthingz"));
	g_iShortcutCodes[SCK_ROTATE] = ConfigGetInteger(lpcfgShortcuts, TEXT("editrotate"));
	g_iShortcutCodes[SCK_RESIZE] = ConfigGetInteger(lpcfgShortcuts, TEXT("editresize"));
	g_iShortcutCodes[SCK_THINGROTACW] = ConfigGetInteger(lpcfgShortcuts, TEXT("thingrotateacw"));
	g_iShortcutCodes[SCK_THINGROTCW] = ConfigGetInteger(lpcfgShortcuts, TEXT("thingrotatecw"));
	g_iShortcutCodes[SCK_SELECTALL] = ConfigGetInteger(lpcfgShortcuts, TEXT("editselectall"));
	g_iShortcutCodes[SCK_SELECTNONE] = ConfigGetInteger(lpcfgShortcuts, TEXT("editselectnone"));
	g_iShortcutCodes[SCK_INVERTSELECTION] = ConfigGetInteger(lpcfgShortcuts, TEXT("editselectinvert"));
	g_iShortcutCodes[SCK_COPYPROPS] = ConfigGetInteger(lpcfgShortcuts, TEXT("copyprops"));
	g_iShortcutCodes[SCK_PASTEPROPS] = ConfigGetInteger(lpcfgShortcuts, TEXT("pasteprops"));
	g_iShortcutCodes[SCK_GRIDINC] = ConfigGetInteger(lpcfgShortcuts, TEXT("gridinc"));
	g_iShortcutCodes[SCK_GRIDDEC] = ConfigGetInteger(lpcfgShortcuts, TEXT("griddec"));
	g_iShortcutCodes[SCK_FIND] = ConfigGetInteger(lpcfgShortcuts, TEXT("editfind"));
	g_iShortcutCodes[SCK_REPLACE] = ConfigGetInteger(lpcfgShortcuts, TEXT("editreplace"));
	g_iShortcutCodes[SCK_NEW] = ConfigGetInteger(lpcfgShortcuts, TEXT("filenew"));
	g_iShortcutCodes[SCK_OPEN] = ConfigGetInteger(lpcfgShortcuts, TEXT("fileopen"));
	g_iShortcutCodes[SCK_SAVE] = ConfigGetInteger(lpcfgShortcuts, TEXT("filesave"));
	g_iShortcutCodes[SCK_MAPOPTIONS] = ConfigGetInteger(lpcfgShortcuts, TEXT("editoptions"));
	g_iShortcutCodes[SCK_MOUSEROTATE] = ConfigGetInteger(lpcfgShortcuts, TEXT("thingsmouserotate"));
	g_iShortcutCodes[SCK_STITCHVERTICES] = ConfigGetInteger(lpcfgShortcuts, TEXT("stitchvertices"));
	g_iShortcutCodes[SCK_SCROLL_UP] = ConfigGetInteger(lpcfgShortcuts, TEXT("scrollup"));
	g_iShortcutCodes[SCK_SCROLL_DOWN] = ConfigGetInteger(lpcfgShortcuts, TEXT("scrolldown"));
	g_iShortcutCodes[SCK_SCROLL_LEFT] = ConfigGetInteger(lpcfgShortcuts, TEXT("scrollleft"));
	g_iShortcutCodes[SCK_SCROLL_RIGHT] = ConfigGetInteger(lpcfgShortcuts, TEXT("scrollright"));
	g_iShortcutCodes[SCK_TEST] = ConfigGetInteger(lpcfgShortcuts, TEXT("filetest"));
	g_iShortcutCodes[SCK_QUICKTEST] = ConfigGetInteger(lpcfgShortcuts, TEXT("quicktest"));
	g_iShortcutCodes[SCK_IDENTSECTORS] = ConfigGetInteger(lpcfgShortcuts, TEXT("sectorsident"));
	g_iShortcutCodes[SCK_CENTREVIEWSEL] = ConfigGetInteger(lpcfgShortcuts, TEXT("editcenterviewsel"));
	g_iShortcutCodes[SCK_MAKESINGLE] = ConfigGetInteger(lpcfgShortcuts, TEXT("linesmakesingle"));
	g_iShortcutCodes[SCK_MAKEDOUBLE] = ConfigGetInteger(lpcfgShortcuts, TEXT("linesmakedouble"));
	g_iShortcutCodes[SCK_REMOVEINTERIOR] = ConfigGetInteger(lpcfgShortcuts, TEXT("sectorsremoveinterior"));
	/* _SCK_ */

	/* Create the accelerator table. */
	UpdateAcceleratorFromOptions();
}

static void SetShortcutKeys(void)
{
	CONFIG *lpcfgShortcuts = ConfigGetSubsection(g_lpcfgMain, OPT_SHORTCUTS);

	ConfigSetInteger(lpcfgShortcuts, TEXT("editquickmove"), g_iShortcutCodes[SCK_EDITQUICKMOVE]);
	ConfigSetInteger(lpcfgShortcuts, TEXT("zoomin"), g_iShortcutCodes[SCK_ZOOMIN]);
	ConfigSetInteger(lpcfgShortcuts, TEXT("zoomout"), g_iShortcutCodes[SCK_ZOOMOUT]);
	ConfigSetInteger(lpcfgShortcuts, TEXT("editmove"), g_iShortcutCodes[SCK_EDITMOVE]);
	ConfigSetInteger(lpcfgShortcuts, TEXT("editlines"), g_iShortcutCodes[SCK_EDITLINES]);
	ConfigSetInteger(lpcfgShortcuts, TEXT("editsectors"), g_iShortcutCodes[SCK_EDITSECTORS]);
	ConfigSetInteger(lpcfgShortcuts, TEXT("editvertices"), g_iShortcutCodes[SCK_EDITVERTICES]);
	ConfigSetInteger(lpcfgShortcuts, TEXT("editthings"), g_iShortcutCodes[SCK_EDITTHINGS]);
	ConfigSetInteger(lpcfgShortcuts, TEXT("edit3d"), g_iShortcutCodes[SCK_EDIT3D]);
	ConfigSetInteger(lpcfgShortcuts, TEXT("togglesnap"), g_iShortcutCodes[SCK_EDITSNAPTOGRID]);
	ConfigSetInteger(lpcfgShortcuts, TEXT("fliplinedefs"), g_iShortcutCodes[SCK_FLIPLINEDEFS]);
	ConfigSetInteger(lpcfgShortcuts, TEXT("flipsidedefs"), g_iShortcutCodes[SCK_FLIPSIDEDEFS]);
	ConfigSetInteger(lpcfgShortcuts, TEXT("splitlinedefs"), g_iShortcutCodes[SCK_SPLITLINEDEFS]);
	ConfigSetInteger(lpcfgShortcuts, TEXT("joinsector"), g_iShortcutCodes[SCK_JOINSECTORS]);
	ConfigSetInteger(lpcfgShortcuts, TEXT("mergesector"), g_iShortcutCodes[SCK_MERGESECTORS]);
	ConfigSetInteger(lpcfgShortcuts, TEXT("editundo"), g_iShortcutCodes[SCK_UNDO]);
	ConfigSetInteger(lpcfgShortcuts, TEXT("editredo"), g_iShortcutCodes[SCK_REDO]);
	ConfigSetInteger(lpcfgShortcuts, TEXT("editfliph"), g_iShortcutCodes[SCK_FLIPHORIZ]);
	ConfigSetInteger(lpcfgShortcuts, TEXT("editflipv"), g_iShortcutCodes[SCK_FLIPVERT]);
	ConfigSetInteger(lpcfgShortcuts, TEXT("editcopy"), g_iShortcutCodes[SCK_COPY]);
	ConfigSetInteger(lpcfgShortcuts, TEXT("editpaste"), g_iShortcutCodes[SCK_PASTE]);
	ConfigSetInteger(lpcfgShortcuts, TEXT("filesaveas"), g_iShortcutCodes[SCK_SAVEAS]);
	ConfigSetInteger(lpcfgShortcuts, TEXT("incfloor"), g_iShortcutCodes[SCK_INCFLOOR]);
	ConfigSetInteger(lpcfgShortcuts, TEXT("decfloor"), g_iShortcutCodes[SCK_DECFLOOR]);
	ConfigSetInteger(lpcfgShortcuts, TEXT("incceil"), g_iShortcutCodes[SCK_INCCEIL]);
	ConfigSetInteger(lpcfgShortcuts, TEXT("decceil"), g_iShortcutCodes[SCK_DECCEIL]);
	ConfigSetInteger(lpcfgShortcuts, TEXT("inclight"), g_iShortcutCodes[SCK_INCLIGHT]);
	ConfigSetInteger(lpcfgShortcuts, TEXT("declight"), g_iShortcutCodes[SCK_DECLIGHT]);
	ConfigSetInteger(lpcfgShortcuts, TEXT("incthingz"), g_iShortcutCodes[SCK_INCTHINGZ]);
	ConfigSetInteger(lpcfgShortcuts, TEXT("decthingz"), g_iShortcutCodes[SCK_DECTHINGZ]);
	ConfigSetInteger(lpcfgShortcuts, TEXT("snapselection"), g_iShortcutCodes[SCK_SNAPSELECTION]);
	ConfigSetInteger(lpcfgShortcuts, TEXT("gradientfloors"), g_iShortcutCodes[SCK_GRADIENTFLOORS]);
	ConfigSetInteger(lpcfgShortcuts, TEXT("gradientceilings"), g_iShortcutCodes[SCK_GRADIENTCEILINGS]);
	ConfigSetInteger(lpcfgShortcuts, TEXT("gradientbrightness"), g_iShortcutCodes[SCK_GRADIENTBRIGHTNESS]);
	ConfigSetInteger(lpcfgShortcuts, TEXT("drawsector"), g_iShortcutCodes[SCK_INSERT]);
	ConfigSetInteger(lpcfgShortcuts, TEXT("drawrect"), g_iShortcutCodes[SCK_DCKINSERT]);
	ConfigSetInteger(lpcfgShortcuts, TEXT("editcut"), g_iShortcutCodes[SCK_EDITCUT]);
	ConfigSetInteger(lpcfgShortcuts, TEXT("editdelete"), g_iShortcutCodes[SCK_EDITDELETE]);
	ConfigSetInteger(lpcfgShortcuts, TEXT("cancel"), g_iShortcutCodes[SCK_CANCEL]);
	ConfigSetInteger(lpcfgShortcuts, TEXT("gradientthingz"), g_iShortcutCodes[SCK_GRADIENTTHINGZ]);
	ConfigSetInteger(lpcfgShortcuts, TEXT("editrotate"), g_iShortcutCodes[SCK_ROTATE]);
	ConfigSetInteger(lpcfgShortcuts, TEXT("editresize"), g_iShortcutCodes[SCK_RESIZE]);
	ConfigSetInteger(lpcfgShortcuts, TEXT("thingrotateacw"), g_iShortcutCodes[SCK_THINGROTACW]);
	ConfigSetInteger(lpcfgShortcuts, TEXT("thingrotatecw"), g_iShortcutCodes[SCK_THINGROTCW]);
	ConfigSetInteger(lpcfgShortcuts, TEXT("editselectall"), g_iShortcutCodes[SCK_SELECTALL]);
	ConfigSetInteger(lpcfgShortcuts, TEXT("editselectnone"), g_iShortcutCodes[SCK_SELECTNONE]);
	ConfigSetInteger(lpcfgShortcuts, TEXT("editselectinvert"), g_iShortcutCodes[SCK_INVERTSELECTION]);
	ConfigSetInteger(lpcfgShortcuts, TEXT("copyprops"), g_iShortcutCodes[SCK_COPYPROPS]);
	ConfigSetInteger(lpcfgShortcuts, TEXT("pasteprops"), g_iShortcutCodes[SCK_PASTEPROPS]);
	ConfigSetInteger(lpcfgShortcuts, TEXT("gridinc"), g_iShortcutCodes[SCK_GRIDINC]);
	ConfigSetInteger(lpcfgShortcuts, TEXT("griddec"), g_iShortcutCodes[SCK_GRIDDEC]);
	ConfigSetInteger(lpcfgShortcuts, TEXT("editfind"), g_iShortcutCodes[SCK_FIND]);
	ConfigSetInteger(lpcfgShortcuts, TEXT("editreplace"), g_iShortcutCodes[SCK_REPLACE]);
	ConfigSetInteger(lpcfgShortcuts, TEXT("filenew"), g_iShortcutCodes[SCK_NEW]);
	ConfigSetInteger(lpcfgShortcuts, TEXT("fileopen"), g_iShortcutCodes[SCK_OPEN]);
	ConfigSetInteger(lpcfgShortcuts, TEXT("filesave"), g_iShortcutCodes[SCK_SAVE]);
	ConfigSetInteger(lpcfgShortcuts, TEXT("editoptions"), g_iShortcutCodes[SCK_MAPOPTIONS]);
	ConfigSetInteger(lpcfgShortcuts, TEXT("thingsmouserotate"), g_iShortcutCodes[SCK_MOUSEROTATE]);
	ConfigSetInteger(lpcfgShortcuts, TEXT("stitchvertices"), g_iShortcutCodes[SCK_STITCHVERTICES]);
	ConfigSetInteger(lpcfgShortcuts, TEXT("scrollup"), g_iShortcutCodes[SCK_SCROLL_UP]);
	ConfigSetInteger(lpcfgShortcuts, TEXT("scrolldown"), g_iShortcutCodes[SCK_SCROLL_DOWN]);
	ConfigSetInteger(lpcfgShortcuts, TEXT("scrollleft"), g_iShortcutCodes[SCK_SCROLL_LEFT]);
	ConfigSetInteger(lpcfgShortcuts, TEXT("scrollright"), g_iShortcutCodes[SCK_SCROLL_RIGHT]);
	ConfigSetInteger(lpcfgShortcuts, TEXT("filetest"), g_iShortcutCodes[SCK_TEST]);
	ConfigSetInteger(lpcfgShortcuts, TEXT("quicktest"), g_iShortcutCodes[SCK_QUICKTEST]);
	ConfigSetInteger(lpcfgShortcuts, TEXT("sectorsident"), g_iShortcutCodes[SCK_IDENTSECTORS]);
	ConfigSetInteger(lpcfgShortcuts, TEXT("editcenterviewsel"), g_iShortcutCodes[SCK_CENTREVIEWSEL]);
	ConfigSetInteger(lpcfgShortcuts, TEXT("linesmakesingle"), g_iShortcutCodes[SCK_MAKESINGLE]);
	ConfigSetInteger(lpcfgShortcuts, TEXT("linesmakedouble"), g_iShortcutCodes[SCK_MAKEDOUBLE]);
	ConfigSetInteger(lpcfgShortcuts, TEXT("sectorsremoveinterior"), g_iShortcutCodes[SCK_REMOVEINTERIOR]);
	/* _SCK_ */
}


/* LoadRendererOptions, SetRendererOptions
 *   Copies renderer settings between the tree and the structure.
 *
 * Parameters:
 *   None.
 *
 * Return value: None.
 */
static void LoadRendererOptions(void)
{
	g_rendopts.iIndicatorSize = ConfigGetInteger(g_lpcfgMain, TEXT("indicatorsize"));
	g_rendopts.iVertexSize = ConfigGetInteger(g_lpcfgMain, TEXT("vertexsize"));
	g_rendopts.bVerticesInLinesMode = ConfigGetInteger(g_lpcfgMain, TEXT("linemodevertices"));
	g_rendopts.bVerticesInSectorsMode = ConfigGetInteger(g_lpcfgMain, TEXT("secmodevertices"));
}

static void SetRendererOptions(void)
{
	ConfigSetInteger(g_lpcfgMain, TEXT("indicatorsize"), g_rendopts.iIndicatorSize);
	ConfigSetInteger(g_lpcfgMain, TEXT("vertexsize"), g_rendopts.iVertexSize);
	ConfigSetInteger(g_lpcfgMain, TEXT("linemodevertices"), g_rendopts.bVerticesInLinesMode);
	ConfigSetInteger(g_lpcfgMain, TEXT("secmodevertices"), g_rendopts.bVerticesInSectorsMode);
}


/* MRUAdd
 *   Adds a file to the top of the MRU list.
 *
 * Parameters:
 *   LPCTSTR	szFilename	Filename to add.
 *
 * Return value: None.
 *
 * Remarks:
 *   The filename need not be the full path: it is expanded by the function. If
 *   the file is already in the list, it is moved to first place, and not
 *   duplicated.
 */
void MRUAdd(LPCTSTR szFilename)
{
	LPTSTR szFullFilename;
	UINT cchFullFilename;
	LPCTSTR szIndices[MRUENTRIES] = {TEXT("1"), TEXT("2"), TEXT("3"), TEXT("4"), TEXT("5"), TEXT("6"), TEXT("7"), TEXT("8"), TEXT("9"), TEXT("10")};
	CONFIG *lpcfgRecent = ConfigGetSubsection(g_lpcfgMain, OPT_RECENT);
	int i, iExistingPos;

	cchFullFilename = GetFullPathName(szFilename, 0, TEXT(""), NULL);
	szFullFilename = ProcHeapAlloc(cchFullFilename * sizeof(TCHAR));
	GetFullPathName(szFilename, cchFullFilename, szFullFilename, NULL);

	/* Check whether we're already in the list, and at which point. Note that we
	 * don't need to check the last position, as it being there is equivalent to
	 * it not being there at all.
	 */
	for(iExistingPos = 0; iExistingPos < (int)(NUM_ELEMENTS(szIndices) - 1) && ConfigNodeExists(lpcfgRecent, szIndices[iExistingPos]); iExistingPos++)
	{
		/* Get the filename to move. */
		UINT cchExistingFilename = ConfigGetStringLength(lpcfgRecent, szIndices[iExistingPos]) + 1;
		LPTSTR szExistingFilename = ProcHeapAlloc(cchExistingFilename * sizeof(TCHAR));
		ConfigGetString(lpcfgRecent, szIndices[iExistingPos], szExistingFilename, cchExistingFilename);

		/* Does this match with the file we're adding? */
		if(PathsReferToSameFile(szFullFilename, szExistingFilename))
		{
			/* The file was already in the MRU. */
			ProcHeapFree(szExistingFilename);
			break;
		}

		ProcHeapFree(szExistingFilename);
	}

	/* Move all the items down one. */
	for(i = iExistingPos; i > 0; i--)
	{
		/* Get the filename to move. */
		UINT cchExistingFilename = ConfigGetStringLength(lpcfgRecent, szIndices[i - 1]) + 1;
		LPTSTR szExistingFilename = ProcHeapAlloc(cchExistingFilename * sizeof(TCHAR));
		ConfigGetString(lpcfgRecent, szIndices[i - 1], szExistingFilename, cchExistingFilename);

		/* Move it down. */
		ConfigSetString(lpcfgRecent, szIndices[i], szExistingFilename);

		ProcHeapFree(szExistingFilename);
	}

	/* Add the new item. */
	ConfigSetString(lpcfgRecent, szIndices[0], szFullFilename);

	ProcHeapFree(szFullFilename);
}


/* SetMissingOptions
 *   Sets missing items in the config file to sensible defaults.
 *
 * Parameters: None.
 *
 * Return value: None.
 */
static void SetMissingOptions(void)
{
	/* Make sure we have a recent files section. */
	if(!ConfigGetSubsection(g_lpcfgMain, OPT_RECENT))
		MRUClear();

	if(!ConfigGetSubsection(g_lpcfgMain, OPT_PALETTE)) ConfigSetSubsection(g_lpcfgMain, OPT_PALETTE, ConfigCreate());
	if(!ConfigGetSubsection(g_lpcfgMain, OPT_SHORTCUTS)) ConfigSetSubsection(g_lpcfgMain, OPT_SHORTCUTS, ConfigCreate());
	if(!ConfigGetSubsection(g_lpcfgMain, OPT_GAMECONFIGS)) ConfigSetSubsection(g_lpcfgMain, OPT_GAMECONFIGS, ConfigCreate());
	if(!ConfigGetSubsection(g_lpcfgMain, OPT_DEFAULTTEX)) ConfigSetSubsection(g_lpcfgMain, OPT_DEFAULTTEX, ConfigCreate());
	if(!ConfigGetSubsection(g_lpcfgMain, OPT_DEFAULTSEC)) ConfigSetSubsection(g_lpcfgMain, OPT_DEFAULTSEC, ConfigCreate());
	if(!ConfigGetSubsection(g_lpcfgMain, OPT_WINDOWPOS)) ConfigSetSubsection(g_lpcfgMain, OPT_WINDOWPOS, ConfigCreate());
	if(!ConfigGetSubsection(g_lpcfgMain, OPT_NODEBUILDER)) ConfigSetSubsection(g_lpcfgMain, OPT_NODEBUILDER, ConfigCreate());
	if(!ConfigGetSubsection(g_lpcfgMain, OPT_WIKI)) ConfigSetSubsection(g_lpcfgMain, OPT_WIKI, ConfigCreate());

	SetDefaultPalette();
	SetDefaultShortcuts();
	SetDefaultTextures();
	SetDefaultSector();
	SetDefaultNodebuilderOptions();
	SetDefaultWikiOptions();

	if(!ConfigNodeExists(g_lpcfgMain, TEXT("vertexsize"))) ConfigSetInteger(g_lpcfgMain, TEXT("vertexsize"), 4);
	if(!ConfigNodeExists(g_lpcfgMain, TEXT("vertexsnapdistance"))) ConfigSetInteger(g_lpcfgMain, TEXT("vertexsnapdistance"), 8);
	if(!ConfigNodeExists(g_lpcfgMain, TEXT("zoommouse"))) ConfigSetInteger(g_lpcfgMain, TEXT("zoommouse"), TRUE);
	if(!ConfigNodeExists(g_lpcfgMain, TEXT("zoomspeed"))) ConfigSetInteger(g_lpcfgMain, TEXT("zoomspeed"), 200);
	if(!ConfigNodeExists(g_lpcfgMain, TEXT("nothingdeselects"))) ConfigSetInteger(g_lpcfgMain, TEXT("nothingdeselects"), TRUE);
	if(!ConfigNodeExists(g_lpcfgMain, TEXT("defaultsnap"))) ConfigSetInteger(g_lpcfgMain, TEXT("defaultsnap"), SF_RECTANGLE | SF_VERTICES);
	if(!ConfigNodeExists(g_lpcfgMain, TEXT("detailsany"))) ConfigSetInteger(g_lpcfgMain, TEXT("detailsany"), TRUE);
	if(!ConfigNodeExists(g_lpcfgMain, TEXT("drawaxes"))) ConfigSetInteger(g_lpcfgMain, TEXT("drawaxes"), TRUE);
	if(!ConfigNodeExists(g_lpcfgMain, TEXT("gridshow"))) ConfigSetInteger(g_lpcfgMain, TEXT("gridshow"), TRUE);
	if(!ConfigNodeExists(g_lpcfgMain, TEXT("grid64show"))) ConfigSetInteger(g_lpcfgMain, TEXT("grid64show"), TRUE);
	if(!ConfigNodeExists(g_lpcfgMain, TEXT("indicatorsize"))) ConfigSetInteger(g_lpcfgMain, TEXT("indicatorsize"), 5);
	if(!ConfigNodeExists(g_lpcfgMain, TEXT("linesplitdistance"))) ConfigSetInteger(g_lpcfgMain, TEXT("linesplitdistance"), 2);
	if(!ConfigNodeExists(g_lpcfgMain, TEXT("defaultstitch"))) ConfigSetInteger(g_lpcfgMain, TEXT("defaultstitch"), TRUE);
	if(!ConfigNodeExists(g_lpcfgMain, TEXT("autostitchdistance"))) ConfigSetInteger(g_lpcfgMain, TEXT("autostitchdistance"), 2);
	if(!ConfigNodeExists(g_lpcfgMain, TEXT("contextmenus"))) ConfigSetInteger(g_lpcfgMain, TEXT("contextmenus"), TRUE);
	if(!ConfigNodeExists(g_lpcfgMain, TEXT("defaultgrid"))) ConfigSetInteger(g_lpcfgMain, TEXT("defaultgrid"), 32);
	if(!ConfigNodeExists(g_lpcfgMain, TEXT("scrollspeed"))) ConfigSetInteger(g_lpcfgMain, TEXT("scrollspeed"), 9);
	if(!ConfigNodeExists(g_lpcfgMain, TEXT("usedonly"))) ConfigSetInteger(g_lpcfgMain, TEXT("usedonly"), TRUE);
	if(!ConfigNodeExists(g_lpcfgMain, TEXT("persistentopenwad"))) ConfigSetInteger(g_lpcfgMain, TEXT("persistentopenwad"), TRUE);
}


/* SetDefaultPalette
 *   Sets default palette colours.
 *
 * Parameters: None.
 *
 * Return value: None.
 */
static void SetDefaultPalette(void)
{
	CONFIG *lpcfgPal = ConfigGetSubsection(g_lpcfgMain, OPT_PALETTE);

	if(!ConfigNodeExists(lpcfgPal, TEXT("CLR_BACKGROUND")))
		ConfigSetInteger(lpcfgPal, TEXT("CLR_BACKGROUND"),		RGB(0x00, 0x00, 0x00));

	if(!ConfigNodeExists(lpcfgPal, TEXT("CLR_VERTEX")))
		ConfigSetInteger(lpcfgPal, TEXT("CLR_VERTEX"),			RGB(0x00, 0x99, 0xFF));

	if(!ConfigNodeExists(lpcfgPal, TEXT("CLR_VERTEXSELECTED")))
		ConfigSetInteger(lpcfgPal, TEXT("CLR_VERTEXSELECTED"),	RGB(0xFF, 0x33, 0x11));

	if(!ConfigNodeExists(lpcfgPal, TEXT("CLR_VERTEXHIGHLIGHT")))
		ConfigSetInteger(lpcfgPal, TEXT("CLR_VERTEXHIGHLIGHT"),	RGB(0xFF, 0x99, 0x00));

	if(!ConfigNodeExists(lpcfgPal, TEXT("CLR_LINE")))
		ConfigSetInteger(lpcfgPal, TEXT("CLR_LINE"),				RGB(0xFF, 0xFF, 0xFF));

	if(!ConfigNodeExists(lpcfgPal, TEXT("CLR_LINEDOUBLE")))
		ConfigSetInteger(lpcfgPal, TEXT("CLR_LINEDOUBLE"),		RGB(0xAA, 0xAA, 0xAA));

	if(!ConfigNodeExists(lpcfgPal, TEXT("CLR_LINESPECIAL")))
		ConfigSetInteger(lpcfgPal, TEXT("CLR_LINESPECIAL"),		RGB(0xAA, 0xFF, 0xAA));

	if(!ConfigNodeExists(lpcfgPal, TEXT("CLR_LINESPECIALDOUBLE")))
		ConfigSetInteger(lpcfgPal, TEXT("CLR_LINESPECIALDOUBLE"),	RGB(0x80, 0xAA, 0x80));

	if(!ConfigNodeExists(lpcfgPal, TEXT("CLR_LINESELECTED")))
		ConfigSetInteger(lpcfgPal, TEXT("CLR_LINESELECTED"),		RGB(0xFF, 0x00, 0x00));

	if(!ConfigNodeExists(lpcfgPal, TEXT("CLR_LINEHIGHLIGHT")))
		ConfigSetInteger(lpcfgPal, TEXT("CLR_LINEHIGHLIGHT"),		RGB(0xFF, 0x99, 0x00));

	if(!ConfigNodeExists(lpcfgPal, TEXT("CLR_LINEDRAG")))
		ConfigSetInteger(lpcfgPal, TEXT("CLR_LINEDRAG"),			RGB(0xAA, 0x99, 0x33));

	if(!ConfigNodeExists(lpcfgPal, TEXT("CLR_SECTORTAG")))
		ConfigSetInteger(lpcfgPal, TEXT("CLR_SECTORTAG"),			RGB(0xFF, 0xFF, 0x80));

	if(!ConfigNodeExists(lpcfgPal, TEXT("CLR_THINGUNKNOWN")))
		ConfigSetInteger(lpcfgPal, TEXT("CLR_THINGUNKNOWN"),		RGB(0x80, 0x80, 0x80));

	if(!ConfigNodeExists(lpcfgPal, TEXT("CLR_THINGSELECTED")))
		ConfigSetInteger(lpcfgPal, TEXT("CLR_THINGSELECTED"),		RGB(0xFF, 0x00, 0x00));

	if(!ConfigNodeExists(lpcfgPal, TEXT("CLR_THINGHIGHLIGHT")))
		ConfigSetInteger(lpcfgPal, TEXT("CLR_THINGHIGHLIGHT"),	RGB(0xFF, 0x99, 0x00));

	if(!ConfigNodeExists(lpcfgPal, TEXT("CLR_GRID")))
		ConfigSetInteger(lpcfgPal, TEXT("CLR_GRID"),				RGB(0x00, 0x00, 0x36));

	if(!ConfigNodeExists(lpcfgPal, TEXT("CLR_GRID64")))
		ConfigSetInteger(lpcfgPal, TEXT("CLR_GRID64"),			RGB(0x00, 0x00, 0x5E));

	if(!ConfigNodeExists(lpcfgPal, TEXT("CLR_LINEBLOCKSOUND")))
		ConfigSetInteger(lpcfgPal, TEXT("CLR_LINEBLOCKSOUND"),	RGB(0x69, 0x67, 0x98));

	if(!ConfigNodeExists(lpcfgPal, TEXT("CLR_MAPBOUNDARY")))
		ConfigSetInteger(lpcfgPal, TEXT("CLR_MAPBOUNDARY"),		RGB(0xFF, 0x00, 0x00));

	if(!ConfigNodeExists(lpcfgPal, TEXT("CLR_AXES")))
		ConfigSetInteger(lpcfgPal, TEXT("CLR_AXES"),				RGB(0x56, 0x00, 0x73));

	if(!ConfigNodeExists(lpcfgPal, TEXT("CLR_ZEROHEIGHTLINE")))
		ConfigSetInteger(lpcfgPal, TEXT("CLR_ZEROHEIGHTLINE"),	RGB(0x55, 0x55, 0x55));

	if(!ConfigNodeExists(lpcfgPal, TEXT("CLR_FOFSECTOR")))
		ConfigSetInteger(lpcfgPal, TEXT("CLR_FOFSECTOR"),			RGB(0xAA, 0xFF, 0xAA));
}


/* SetDefaultTextures
 *   Sets default default default (!) textures.
 *
 * Parameters: None.
 *
 * Return value: None.
 */
static void SetDefaultTextures(void)
{
	CONFIG *lpcfgDefTextures = ConfigGetSubsection(g_lpcfgMain, OPT_DEFAULTTEX);

	if(!ConfigNodeExists(lpcfgDefTextures, TEXT("upper"))) ConfigSetString(lpcfgDefTextures, TEXT("upper"), OPT_DEFAULT_DEFTEXTURE);
	if(!ConfigNodeExists(lpcfgDefTextures, TEXT("middle"))) ConfigSetString(lpcfgDefTextures, TEXT("middle"), OPT_DEFAULT_DEFTEXTURE);
	if(!ConfigNodeExists(lpcfgDefTextures, TEXT("lower"))) ConfigSetString(lpcfgDefTextures, TEXT("lower"), OPT_DEFAULT_DEFTEXTURE);
}


/* SetDefaultSector
 *   Sets default default default (!) sector properties.
 *
 * Parameters: None.
 *
 * Return value: None.
 */
static void SetDefaultSector(void)
{
	CONFIG *lpcfgDefSec = ConfigGetSubsection(g_lpcfgMain, OPT_DEFAULTSEC);

	if(!ConfigNodeExists(lpcfgDefSec, TEXT("tfloor"))) ConfigSetString(lpcfgDefSec, TEXT("tfloor"), TEXT("FLOOR0_6"));
	if(!ConfigNodeExists(lpcfgDefSec, TEXT("tceiling"))) ConfigSetString(lpcfgDefSec, TEXT("tceiling"), TEXT("FLOOR0_3"));

	if(!ConfigNodeExists(lpcfgDefSec, TEXT("hfloor"))) ConfigSetInteger(lpcfgDefSec, TEXT("hfloor"), 128);
	if(!ConfigNodeExists(lpcfgDefSec, TEXT("hceiling"))) ConfigSetInteger(lpcfgDefSec, TEXT("hceiling"), 512);
	if(!ConfigNodeExists(lpcfgDefSec, TEXT("brightness"))) ConfigSetInteger(lpcfgDefSec, TEXT("brightness"), 255);
}


/* SetDefaultNodebuilderOptions
 *   Sets default nodebuilder options.
 *
 * Parameters: None.
 *
 * Return value: None.
 */
static void SetDefaultNodebuilderOptions(void)
{
	CONFIG *lpcfgNB = ConfigGetSubsection(g_lpcfgMain, OPT_NODEBUILDER);

	if(!ConfigNodeExists(lpcfgNB, NBCFG_BINARY)) ConfigSetString(lpcfgNB, NBCFG_BINARY, TEXT(""));
	if(!ConfigNodeExists(lpcfgNB, NBCFG_ARGS)) ConfigSetString(lpcfgNB, NBCFG_ARGS, TEXT("\"%F\" %L -o \"%F\""));
}


/* SetDefaultWikiOptions
 *   Sets default Wiki options.
 *
 * Parameters: None.
 *
 * Return value: None.
 */
static void SetDefaultWikiOptions(void)
{
	CONFIG *lpcfgWiki = ConfigGetSubsection(g_lpcfgMain, OPT_WIKI);

	if(!ConfigNodeExists(lpcfgWiki, WIKICFG_QUERYURLFMT)) ConfigSetString(lpcfgWiki, WIKICFG_QUERYURLFMT, TEXT("http://srb2wiki.sepwich.com/wiki/Special:Search?search=%s&go=Go"));
	if(!ConfigNodeExists(lpcfgWiki, WIKICFG_MAINPAGE)) ConfigSetString(lpcfgWiki, WIKICFG_MAINPAGE, TEXT("Main_Page"));
}


/* GetOptionsForGame
 *   Gets the subsection of the main options for the specified game config.
 *
 * Parameters:
 *   CONFIG*	lpcfgMap	Game configuration.
 *
 * Return value: CONFIG*
 *   Subsection of the main options for lpcfgMap.
 */
CONFIG* GetOptionsForGame(CONFIG *lpcfgMap)
{
	UINT cchGameID = ConfigGetStringLength(lpcfgMap, MAPCFG_ID) + 1;
	LPTSTR szGameID = ProcHeapAlloc(cchGameID * sizeof(TCHAR));
	CONFIG *lpcfgGameOptions;

	/* Get the ID for this game. */
	ConfigGetString(lpcfgMap, MAPCFG_ID, szGameID, cchGameID);

	/* Get the options if they exist, or create them if they don't. */
	lpcfgGameOptions = ConfigGetOrAddSubsection(
		ConfigGetSubsection(g_lpcfgMain, OPT_GAMECONFIGS),
		szGameID);
	ProcHeapFree(szGameID);

	return lpcfgGameOptions;
}


/* GetTestingOptions
 *   Gets the testing options for a particular game configuration.
 *
 * Parameters:
 *   CONFIG*	lpcfgMainGame	Subsection of the main options for the game.
 *
 * Return value: CONFIG*
 *   Testing options.
 */
CONFIG* GetTestingOptions(CONFIG *lpcfgMainGame)
{
	CONFIG *lpcfgTesting = ConfigGetOrAddSubsection(lpcfgMainGame, TEXT("testing"));

	/* Set some defaults if necessary. */
	if(!ConfigNodeExists(lpcfgTesting, TESTCFG_SFX)) ConfigSetInteger(lpcfgTesting, TESTCFG_SFX, TRUE);
	if(!ConfigNodeExists(lpcfgTesting, TESTCFG_MUSIC)) ConfigSetInteger(lpcfgTesting, TESTCFG_MUSIC, TRUE);
	if(!ConfigNodeExists(lpcfgTesting, TESTCFG_DIFFICULTY)) ConfigSetInteger(lpcfgTesting, TESTCFG_DIFFICULTY, DIFF_NORMAL);
	if(!ConfigNodeExists(lpcfgTesting, TESTCFG_GAMETYPE)) ConfigSetInteger(lpcfgTesting, TESTCFG_GAMETYPE, GT_DEFAULT);
	if(!ConfigNodeExists(lpcfgTesting, TESTCFG_SKIN)) ConfigSetString(lpcfgTesting, TESTCFG_SKIN, TEXT("Sonic"));

	return lpcfgTesting;
}


/* SetOptionsDirectory
 *   Sets the directory from which options, both main and map configurations,
 *   are loaded.
 *
 * Parameters:
 *   LPCTSTR	szDir	Directory, either relative or absolute. NULL to use
 *						default.
 *
 * Return value: None.
 *
 * Remarks:
 *   This must be called before attempting to open any config files. Call
 *   FreeOptionsDirectoryName at shutdown to free memory.
 */
void SetOptionsDirectory(LPCTSTR szDir)
{
	TCHAR szFullPath[MAX_PATH];

	if(szDir)
	{
		LPTSTR szFilePart;

		/* Expand the directory name we were given. */
		DWORD cch = GetFullPathName(szDir, sizeof(szFullPath)/sizeof(TCHAR), szFullPath, &szFilePart);
		
		/* Failed, so fall back to default. */
		if(cch == 0 || cch >= sizeof(szFullPath)/sizeof(TCHAR))
			szDir = NULL;
	}

	if(!szDir)
	{
		int i;

		/* Get and terminate path name. */
		GetModuleFileName(NULL, szFullPath, NUM_ELEMENTS(szFullPath) - 1);
		szFullPath[NUM_ELEMENTS(szFullPath) - 1] = TEXT('\0');

		/* Replace the filename with the name of the conf directory. */
		i = _tcslen(szFullPath);
		while(szFullPath[i-1] != '\\' && szFullPath[i-1] != '/') i--;
		_tcsncpy(&szFullPath[i], OPTDIR, NUM_ELEMENTS(szFullPath) - i - 1);
	}
	
	/* Copy to the heap and save address in a global. */
	g_szOptDir = ProcHeapAlloc((_tcslen(szFullPath) + 1) * sizeof(TCHAR));
	_tcscpy(g_szOptDir, szFullPath);
}


/* FreeOptionsDirectoryName
 *   Frees the options directory name.
 *
 * Parameters:
 *   None.
 *
 * Return value: None.
 */
void FreeOptionsDirectoryName(void)
{
	ProcHeapFree(g_szOptDir);
}


/* UntaintOptions
 *   Fixes up potentially dangerous options to prevent monkey business by the
 *   user.
 *
 * Parameters:
 *   None.
 *
 * Return value: None.
 */
static void UntaintOptions(void)
{
	CONFIG *lpcfgWiki = ConfigGetSubsection(g_lpcfgMain, OPT_WIKI);

	/* Allocate twice as many characters as we need, which is (more than) enough
	 * to replace every possible errant %.
	 */
	UINT uiInitialLength = ConfigGetStringLength(lpcfgWiki, WIKICFG_QUERYURLFMT);
	UINT cchBuffer = (uiInitialLength << 1) + 1;
	LPTSTR szWikiURLFormat = ProcHeapAlloc(cchBuffer * sizeof(TCHAR));
	LPTSTR szWikiURLFmtFinal = szWikiURLFormat;
	LPTSTR szInitialFormat = szWikiURLFormat + uiInitialLength;

	/* We're only allowed one %s. */
	BOOL bFoundFormatField = FALSE;

	/* Put the initial string at the *end* of the buffer. */
	ConfigGetString(lpcfgWiki, WIKICFG_QUERYURLFMT, szInitialFormat, uiInitialLength + 1);

	/* Make sure that it begins http:// */
	if(_tcsncmp(szInitialFormat, TEXT("http://"), 7) == 0)
	{
		/* Copy the string to the beginning of the buffer, replacing anything
		 * that's unsafe. The lengths of the strings are such that we never
		 * overwrite a character in the original string until we're finished
		 * with it.
		 *
		 * A quick word on the syntax of the format string: Only %s means
		 * anything special, and that's allowed only once. We don't even do %%:
		 * that sequence is translated to *two* % signs in the output; it's not
		 * an escape sequence.
		 */
		while(*szInitialFormat) switch(*szInitialFormat)
		{
		case TEXT('%'):
			/* We found a percentage sign. It's the next character that determines
			 * what we have to do.
			 */

			if(szInitialFormat[1] != TEXT('s') || bFoundFormatField)
				/* We actually want a % here. */
				*szWikiURLFormat++ = TEXT('%');
			/* Otherwise, it's our format field, but don't allow it next time. */
			else bFoundFormatField = TRUE;

			/* Fall through. */
		default:
			/* Just copy the character. */
			*szWikiURLFormat++ = *szInitialFormat++;
		}
	}

	/* Terminate the string. */
	*szWikiURLFormat = TEXT('\0');

	/* Write it back to the config. */
	ConfigSetString(lpcfgWiki, WIKICFG_QUERYURLFMT, szWikiURLFmtFinal);

	/* Free the buffer. */
	ProcHeapFree(szWikiURLFmtFinal);
}


/* RemoveStaleMRUEntries
 *   Removes MRU entries for files that don't exist.
 *
 * Parameters:
 *   None.
 *
 * Return value: None.
 */
static void RemoveStaleMRUEntries(void)
{
	CONFIG *lpcfgMRU = ConfigGetSubsection(g_lpcfgMain, OPT_RECENT);
	LPCTSTR szIndices[MRUENTRIES] = {TEXT("1"), TEXT("2"), TEXT("3"), TEXT("4"), TEXT("5"), TEXT("6"), TEXT("7"), TEXT("8"), TEXT("9"), TEXT("10")};
	int i, iNextPos = 0;
	LPTSTR szFilename;
	int cchFilename;

	for(i = 0; i < (int)NUM_ELEMENTS(szIndices) && ConfigNodeExists(lpcfgMRU, szIndices[i]); i++)
	{
		
		cchFilename = ConfigGetStringLength(lpcfgMRU, szIndices[i]);
		if(cchFilename == 0) continue;

		szFilename = ProcHeapAlloc((cchFilename + 1) * sizeof(TCHAR));
		ConfigGetString(lpcfgMRU, szIndices[i], szFilename, cchFilename + 1);

		/* If the file exists, write it to its new position if necessary and
		 * move to the next slot.
		 */
		if(GetFileAttributes(szFilename) != 0xFFFFFFFF)
		{
			if(iNextPos < i)
				ConfigSetString(lpcfgMRU, szIndices[iNextPos], szFilename);

			iNextPos++;
		}

		ProcHeapFree(szFilename);
	}

	/* Delete the tail. */
	for(i = iNextPos; i < (int)NUM_ELEMENTS(szIndices) && ConfigNodeExists(lpcfgMRU, szIndices[i]); i++)
		ConfigDeleteNode(lpcfgMRU, szIndices[i]);
}
