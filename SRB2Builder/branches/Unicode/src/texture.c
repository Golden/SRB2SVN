#include <windows.h>
#include <search.h>
#include <stdio.h>
#include <tchar.h>

#include "general.h"
#include "texture.h"
#include "openwads.h"
#include "wad.h"

#include "../res/resource.h"


/* Macros. */
#define COLUMN_END				0xFF
#define TNL_BUFSIZEINCREMENT	64


/* Types. */
typedef struct _ADDTOTEXCACHEDATA
{
	TEX_FORMAT	tf;
	WAD			*lpwad;
	TEXCACHE	*lptcHdr;
	RGBQUAD		*lprgbq;
} ADDTOTEXCACHEDATA;


typedef struct _TEXTURE_PATCH_DESCRIPTOR
{
	short		xOffset, yOffset;
	short		nPatchNum;
	short		nStepDir, nColourmap;
} TEXTURE_PATCH_DESCRIPTOR;


typedef struct _TEXTURE_ENTRY
{
	char		sTexName[8];
	short		nReserved1, nReserved2;
	short		cx, cy;
	short		nReserved3, nReserved4;
	short		nNumPatches;

	/* There are variably many of these. Declared thus for convenience. */
	TEXTURE_PATCH_DESCRIPTOR	tpd[1];
} TEXTURE_ENTRY;


typedef struct _DOOMPICTURE_HEADER
{
	short	cx, cy;
	short	xOffset, yOffset;

	/* There are variably many of these. Declared thus for convenience. */
	int		iDataOffsets[1];
} DOOMPICTURE_HEADER;


typedef struct _DOOMPICTURE_POST
{
	BYTE	byRowStart;
	BYTE	byNumPixels;
	BYTE	byReserved;

	/* There are variably many of these. Declared thus for convenience. */
	BYTE	byPixels[1];
} DOOMPICTURE_POST;


typedef struct _PNAMES
{
	int			iNumPatches;

	/* There are variably many of these. Declared thus for convenience. */
	char		lpsPatchName[1][8];
} PNAMES;


static void EnumTextures(WAD *lpwad, TEX_FORMAT tf, void (CALLBACK *f)(const char *sTexName, void *lpvParam), void *lpvParam);
static void DoomPictureToDIB(BYTE *lpbyDIB, DOOMPICTURE_HEADER *lpdp, int cbImage, short cxDIB, short cyDIB, short xOffset, short yOffset);
static int CompareTextureNames(LPCTSTR sz1, LPCTSTR sz2);
static int CompareTextureNameHeads(LPCTSTR sz1, LPCTSTR sz2);


/* LoadTexture
 *   Loads a texture from a wad.
 *
 * Parameters:
 *   WAD*		lpwad				Wad from which to load the texture.
 *   LPCSTR		szTexName			Name of texture's lump.
 *   TEX_FORMAT	tf					Specifies whether flat or texture.
 *   RGBQUAD*	lprgbq				256-colour palette.
 *
 * Return value: TEXTURE*
 *   Pointer to newly-allocated texture.
 *
 * Remarks:
 *   The caller must call DestroyTexture to free the memory!
 */
TEXTURE* LoadTextureW(WAD *lpwad, LPCWSTR sTexName, TEX_FORMAT tf, RGBQUAD *lprgbq)
{
	WCHAR szTexNameW[TEXNAME_BUFFER_LENGTH];
	char szTexNameA[TEXNAME_BUFFER_LENGTH];

	/* Terminate. */
	CopyMemory(szTexNameW, sTexName, sizeof(WCHAR) * (TEXNAME_BUFFER_LENGTH - 1));
	szTexNameW[TEXNAME_BUFFER_LENGTH - 1] = L'\0';

	WideCharToMultiByte(CP_ACP, 0, szTexNameW, NUM_ELEMENTS(szTexNameW), szTexNameA, NUM_ELEMENTS(szTexNameA), NULL, NULL);
	return LoadTextureA(lpwad, szTexNameA, tf, lprgbq);
}

TEXTURE* LoadTextureA(WAD *lpwad, LPCSTR sTexName, TEX_FORMAT tf, RGBQUAD *lprgbq)
{
	TEXTURE *lptex = NULL;
	BYTE *lpbyBits = NULL;

	switch(tf)
	{
	case TF_FLAT:
		{
			int	cb;
			long iFStart, iFEnd, iFlat;

			/* TODO: Read the section from the config (?). */
			iFStart = GetLumpIndex(lpwad, 0, -1, "F_START");
			if(iFStart < 0) break;
			iFEnd = GetLumpIndex(lpwad, iFStart, -1, "F_END");
			if(iFEnd < 0) break;

			iFlat = GetLumpIndex(lpwad, iFStart, iFEnd, sTexName);
			if(iFlat < 0) break;

			cb = GetLumpLength(lpwad, iFlat);
			if(cb <= 0) break;

			lpbyBits = ProcHeapAlloc(cb);
			GetLump(lpwad, iFlat, lpbyBits, cb);

			lptex = ProcHeapAlloc(sizeof(TEXTURE));

			/* There's probably a nice, integer way of doing this. */
			lptex->cx = lptex->cy = (int)sqrt(cb);

			lptex->xOffset = lptex->yOffset = 0;
		}

		/* Pointer to texture will be returned. */
		break;

	case TF_TEXTURE:
		{
			BYTE *lpbyTexture1, *lpbyTexture2;
			TEXTURE_ENTRY *lpte;
			PNAMES *lppnames;
			DOOMPICTURE_HEADER *lpdpPatch;
			int cbTexture1, cbTexture2, cbPnames, cbPatch;
			int cbTextureBitmap;
			int iTexCount, i;
			long iTexture1, iTexture2, iPNames;
			long iPStart, iPEnd;

			/* At least one of TEXTURE1 and TEXTURE2 must exist. */
			iTexture1 = GetLumpIndex(lpwad, 0, -1, "TEXTURE1");
			iTexture2 = GetLumpIndex(lpwad, 0, -1, "TEXTURE2");
			if(iTexture1 < 0 && iTexture2 < 0) return NULL;

			cbTexture1 = iTexture1 >= 0 ? GetLumpLength(lpwad, iTexture1) : 0;
			cbTexture2 = iTexture2 >= 0 ? GetLumpLength(lpwad, iTexture2) : 0;

			/* Make sure we have at least one of them. */
			if(max(cbTexture1, 0) + max(cbTexture2, 0) == 0) return NULL;

			/* PNAMES must exist, too. */
			iPNames = GetLumpIndex(lpwad, 0, -1, "PNAMES");
			if(iPNames < 0 || (cbPnames = GetLumpLength(lpwad, iPNames)) <= 0)
				return NULL;

			/* And P_START and P_END! */
			iPStart = GetLumpIndex(lpwad, 0, -1, "P_START");
			if(iPStart < 0 || (iPEnd = GetLumpIndex(lpwad, iPStart, -1, "P_END")) < 0)
				return NULL;

			/* Allocate room for and load each of them. */
			if(cbTexture1 > 0)
			{
				lpbyTexture1 = ProcHeapAlloc(cbTexture1);
				GetLump(lpwad, iTexture1, lpbyTexture1, cbTexture1);
			} else lpbyTexture1 = NULL;

			if(cbTexture2 > 0)
			{
				lpbyTexture2 = ProcHeapAlloc(cbTexture2);
				GetLump(lpwad, iTexture2, lpbyTexture2, cbTexture2);
			} else lpbyTexture2 = NULL;

			lppnames = ProcHeapAlloc(cbPnames);
			GetLump(lpwad, iPNames, (BYTE*)lppnames, cbPnames);


			/* Find the specified texture in either TEXTURE1 or TEXTURE2. */

			/* Assume we don't find it. */
			lpte = NULL;

			if(lpbyTexture1)
			{
				iTexCount = *((int*)lpbyTexture1);

				for(i=0; i<iTexCount; i++)
				{
					/* TEXTUREn has a table of offsets into itself, after a long
					 * indicating the no. entries.
					 */
					if(strncmp(&lpbyTexture1[((int*)lpbyTexture1)[1 + i]], sTexName, TEXNAME_WAD_BUFFER_LENGTH) == 0)
					{
						/* Found it! */
						lpte = (TEXTURE_ENTRY*)&lpbyTexture1[((int*)lpbyTexture1)[1 + i]];
						break;
					}
				}
			}

			/* Try TEXTURE2 if TEXTURE1 failed. */
			if(!lpte && lpbyTexture2)
			{
				iTexCount = *((int*)lpbyTexture2);

				for(i=0; i<iTexCount; i++)
				{
					/* TEXTUREn has a table of offsets into itself, after a long
					 * indicating the no. entries.
					 */
					if(strncmp(&lpbyTexture2[((int*)lpbyTexture2)[1 + i]], sTexName, TEXNAME_WAD_BUFFER_LENGTH) == 0)
					{
						/* Found it! */
						lpte = (TEXTURE_ENTRY*)&lpbyTexture2[((int*)lpbyTexture2)[1 + i]];
						break;
					}
				}
			}

			/* No match? Or bad texture descriptor? */
			if(!lpte || lpte->cx <= 0 || lpte->cy <= 0)
			{
				if(lpbyTexture1) ProcHeapFree(lpbyTexture1);
				if(lpbyTexture2) ProcHeapFree(lpbyTexture2);
				ProcHeapFree(lppnames);

				break;
			}

			/* From now on, we won't fail, even if we can't find a patch or it's
			 * bad. We just won't draw it.
			 */

			/* Allocate memory for the texture. */
			lptex = ProcHeapAlloc(sizeof(TEXTURE));

			/* DIBs must have widths of a multiple of 4. */
			lptex->cx = ((lpte->cx - 1) & ~3) + 4;
			lptex->cy = lpte->cy;
			cbTextureBitmap = lptex->cx * lptex->cy;
			lptex->xOffset = lptex->yOffset = 0;

			lpbyBits = ProcHeapAlloc(cbTextureBitmap);

			/* Clear the texture bitmap. */
			ZeroMemory(lpbyBits, cbTextureBitmap);

			/* Loop for each patch in the texture. */
			for(i=0; i < lpte->nNumPatches; i++)
			{
				long iPatch;

				/* Make sure patch is in range. And yes, there may be long many
				 * patches, but a texture can only reference short many.
				 */
				if(lpte->tpd[i].nPatchNum >= lppnames->iNumPatches) continue;

				iPatch = GetLumpIndex(lpwad, iPStart, iPEnd, lppnames->lpsPatchName[lpte->tpd[i].nPatchNum]);
				cbPatch = GetLumpLength(lpwad, iPatch);

				/* Patch not found? */
				if(cbPatch <= 0) continue;

				lpdpPatch = ProcHeapAlloc(cbPatch);

				/* Load patch. */
				GetLump(lpwad, iPatch, (BYTE*)lpdpPatch, cbPatch);

				/* Blit! */
				DoomPictureToDIB(lpbyBits, lpdpPatch, cbPatch, lptex->cx, lptex->cy, lpte->tpd[i].xOffset, lpte->tpd[i].yOffset);

				/* Free patch. */
				ProcHeapFree(lpdpPatch);
			}

			/* Hooray! Finished blitting. Free memory and return. */

			if(lpbyTexture1) ProcHeapFree(lpbyTexture1);
			if(lpbyTexture2) ProcHeapFree(lpbyTexture2);
			ProcHeapFree(lppnames);
		}

		/* Pointer to texture will be returned. */
		break;

	case TF_IMAGE:
		{
			int cbImage, cbTextureBitmap;
			DOOMPICTURE_HEADER *lpdp;
			long iImage;

			iImage = GetLumpIndex(lpwad, 0, -1, sTexName);
			cbImage = GetLumpLength(lpwad, iImage);

			/* Patch not found? */
			if(cbImage <= 0) return NULL;

			lpdp = ProcHeapAlloc(cbImage);

			/* Load image. */
			GetLump(lpwad, iImage, (BYTE*)lpdp, cbImage);

			/* Allocate memory for the texture. */
			lptex = ProcHeapAlloc(sizeof(TEXTURE));

			/* DIBs must have widths of a multiple of 4. */
			lptex->cx = ((lpdp->cx - 1) & ~3) + 4;
			lptex->cy = lpdp->cy;
			cbTextureBitmap = lptex->cx * lptex->cy;
			lptex->xOffset = lptex->yOffset = 0;

			lpbyBits = ProcHeapAlloc(cbTextureBitmap);

			/* Clear the texture bitmap. */
			ZeroMemory(lpbyBits, cbTextureBitmap);

			/* Blit! */
			DoomPictureToDIB(lpbyBits, lpdp, cbImage, lptex->cx, lptex->cy, 0, 0);

			/* Free Doom-format image. */
			ProcHeapFree(lpdp);
		}

		/* Pointer to texture will be returned. */
		break;
	}

	if(lptex)
	{
		/* Create a DDB from the bits. */

		HDC hdc;
		BITMAPINFO *lpbmiDI;
		BITMAPINFOHEADER bmih;
		BITMAP bm;

		/* The BITMAPINFO structure requires a tail of 256-1 palette
		 * entries.
		 */
		lpbmiDI = ProcHeapAlloc(sizeof(BITMAPINFO) + 255 * sizeof(RGBQUAD));

		lpbmiDI->bmiHeader.biBitCount = 8;		/* 8bpp. */
		lpbmiDI->bmiHeader.biClrImportant = 0;	/* All are important. */
		lpbmiDI->bmiHeader.biClrUsed = 256;
		lpbmiDI->bmiHeader.biCompression = BI_RGB;
		lpbmiDI->bmiHeader.biPlanes = 1;
		lpbmiDI->bmiHeader.biSize = sizeof(BITMAPINFOHEADER);
		lpbmiDI->bmiHeader.biSizeImage = 0;
		lpbmiDI->bmiHeader.biWidth = lptex->cx;
		lpbmiDI->bmiHeader.biHeight = -lptex->cy;

		bmih.biBitCount = 32;		/* 32bpp. */
		bmih.biClrImportant = 0;	/* All are important. */
		bmih.biClrUsed = 0;
		bmih.biCompression = BI_RGB;
		bmih.biPlanes = 1;
		bmih.biSize = sizeof(BITMAPINFOHEADER);
		bmih.biSizeImage = 0;
		bmih.biWidth = lptex->cx;
		bmih.biHeight = lptex->cy;

		/* Copy the palette data. */
		CopyMemory(lpbmiDI->bmiColors, lprgbq, (CB_PLAYPAL / 3) * sizeof(RGBQUAD));

		hdc = GetDC(NULL);
		lptex->hbitmap = CreateDIBitmap(hdc, &bmih, CBM_INIT, lpbyBits, lpbmiDI, DIB_RGB_COLORS);
		GetObject(lptex->hbitmap, sizeof(bm), &bm);
		ReleaseDC(NULL, hdc);

		ProcHeapFree(lpbmiDI);
		ProcHeapFree(lpbyBits);
	}

	return lptex;
}



/* DestroyTexture
 *   Frees a texture.
 *
 * Parameters:
 *   TEXTURE*	lptex	Texture to free.
 *
 * Return value: None.
 *
 * Remarks:
 *   The texture would normally have been allocated by LoadTexture.
 */
void DestroyTexture(TEXTURE *lptex)
{
	DeleteObject(lptex->hbitmap);
	ProcHeapFree(lptex);
}


/* AddToTextureCache
 *   Adds a texture to a texture cache.
 *
 * Parameters:
 *   TEXCACHE*	lptcHdr		Header of texture cache.
 *   LPCTSTR		szLumpName	Name of texture.
 *   TEXTURE*	lptex		Texture to add.
 *
 * Return value: None.
 *
 * Remarks:
 *   No copying is done -- the pointer itself is used in the cache.
 */
void AddToTextureCache(TEXCACHE *lptcHdr, LPCTSTR szLumpName, TEXTURE *lptex)
{
	TEXCACHE *lptcNew = ProcHeapAlloc(sizeof(TEXCACHE));

	/* Fill data. */
	lptcNew->lptexture = lptex;

#ifdef UNICODE
	WideCharToMultiByte(CP_ACP, 0, szLumpName, -1, lptcNew->szLumpName, NUM_ELEMENTS(lptcNew->szLumpName), NULL, NULL);
#else
	strcpy(lptcNew->szLumpName, szLumpName);
#endif

	/* Add to the beginning of the list. This must be done atomically, or Bad
	 * Things may happen.
	 */
	EnterCriticalSection(&g_cs);
		lptcNew->lptcNext = lptcHdr->lptcNext;
		lptcHdr->lptcNext = lptcNew;
	LeaveCriticalSection(&g_cs);
}


/* EnumTextures
 *   Enumerates the names of all textures or flats in a wad.
 *
 * Parameters:
 *   WAD*			lpwad		Wad to enumerate lumps from.
 *   TEX_FORMAT		tf			Enumerate either flats or textures.
 *   BOOL (CALLBACK *)(const char*, void*)	f
 *								Callback function. The strings passed to it are
 *								not necessarily NULL-terminated.
 *   void*			lpvParam	Parameter to callback.
 *
 * Return value: None.
 */
static void EnumTextures(WAD *lpwad, TEX_FORMAT tf, void (CALLBACK *f)(const char *sTexName, void *lpvParam), void *lpvParam)
{
	switch(tf)
	{
	case TF_FLAT:
		{
			long iFStart, iFEnd, i;

			/* Find F_START and F_END. If either is missing, give up. */
			iFStart = GetLumpIndex(lpwad, 0, -1, "F_START");
			if(iFStart < 0 || (iFEnd = GetLumpIndex(lpwad, iFStart, -1, "F_END")) < 0)
				break;

			/* Loop through all the intervening lumps and call our function. */
			for(i = iFStart + 1; i < iFEnd; i++)
			{
				char sTexName[CCH_LUMPNAME];
				GetLumpNameA(lpwad, i, sTexName);
				f(sTexName, lpvParam);
			}
		}

		break;

	case TF_TEXTURE:
		{
			char sLumpName[CCH_LUMPNAME];
			int cbTexture1, cbTexture2;
			int iTexCount;
			int i;
			long iTexture1, iTexture2;

			iTexture1 = GetLumpIndex(lpwad, 0, -1, "TEXTURE1");
			iTexture2 = GetLumpIndex(lpwad, 0, -1, "TEXTURE2");
			if(iTexture1 < 0 && iTexture2 < 0) break;

			cbTexture1 = iTexture1 >= 0 ? GetLumpLength(lpwad, iTexture1) : 0;
			cbTexture2 = iTexture2 >= 0 ? GetLumpLength(lpwad, iTexture2) : 0;

			if(cbTexture1 > 0)
			{
				BYTE *lpbyTexture1 = ProcHeapAlloc(cbTexture1);

				GetLump(lpwad, iTexture1, lpbyTexture1, cbTexture1);

				iTexCount = *((int*)lpbyTexture1);

				for(i=0; i<iTexCount; i++)
				{
					/* TEXTUREn has a table of offsets into itself, after a long
					 * indicating the no. entries.
					 */
					CopyMemory(sLumpName, &lpbyTexture1[((int*)lpbyTexture1)[1 + i]], CCH_LUMPNAME);
					f(sLumpName, lpvParam);
				}

				ProcHeapFree(lpbyTexture1);
			}

			if(cbTexture2 > 0)
			{
				BYTE *lpbyTexture2 = ProcHeapAlloc(cbTexture2);

				GetLump(lpwad, iTexture2, lpbyTexture2, cbTexture2);

				iTexCount = *((int*)lpbyTexture2);

				for(i=0; i<iTexCount; i++)
				{
					/* TEXTUREn has a table of offsets into itself, after a long
					 * indicating the no. entries.
					 */
					CopyMemory(sLumpName, &lpbyTexture2[((int*)lpbyTexture2)[1 + i]], CCH_LUMPNAME);
					f(sLumpName, lpvParam);
				}

				ProcHeapFree(lpbyTexture2);
			}
		}

		break;


	default:
        break;
	}
}


/* GetTextureFromCache
 *   Searches a cache for a particular texture.
 *
 * Parameters:
 *   TEXCACHE*	lptcHdr		Cache header node.
 *   LPCTSTR	szTexName	Name of texture to search for.
 *
 * Return value: TEXTURE*
 *   Pointer to texture, or NULL if not found.
 *
 * Remarks:
 *   The cache texture itself is returned, so the caller must not destroy it.
 *   This represents a disparity with LoadTexture, which can sometimes make the
 *   calling code messy (only freeing if it wasn't in the cache), but the
 *   inefficiency of copying the texture isn't worthwhile.
 */
TEXTURE* GetTextureFromCache(TEXCACHE *lptcHdr, LPCTSTR szTexName)
{
	TEXCACHE *lptcRover = lptcHdr;
	TEXCACHE *lptcPrev = lptcHdr;

#ifdef UNICODE
	char szTexNameA[TEXNAME_BUFFER_LENGTH];
	WideCharToMultiByte(CP_ACP, 0, szTexName, -1, szTexNameA, NUM_ELEMENTS(szTexNameA), NULL, NULL);
#else
	LPCSTR szTexNameA = szTexName;
#endif

	while((lptcRover = lptcRover->lptcNext))
	{
		if(strcmp(lptcRover->szLumpName, szTexNameA) == 0)
		{
			/* Move match to front of list. */
			lptcPrev->lptcNext = lptcRover->lptcNext;
			lptcRover->lptcNext = lptcHdr->lptcNext;
			lptcHdr->lptcNext = lptcRover;

			return lptcRover->lptexture;
		}

		lptcPrev = lptcPrev->lptcNext;
	}

	return NULL;
}



/* PurgeTextureCache
 *   Frees all memory used by a texture cache.
 *
 * Parameters:
 *   TEXCACHE*	lptcHdr		Cache header node.
 *
 * Return value: None.
 */
void PurgeTextureCache(TEXCACHE *lptcHdr)
{
	/* Skip header. */
	TEXCACHE *lptcNext = lptcHdr->lptcNext;

	/* The caller expects this. */
	lptcHdr->lptcNext = NULL;

	while((lptcHdr = lptcNext))
	{
		lptcNext = lptcHdr->lptcNext;
		DestroyTexture(lptcHdr->lptexture);
		ProcHeapFree(lptcHdr);
	}
}



/* DoomPictureToDIB
 *   Converts a column-major Doom picture into a top-down DIB.
 *
 * Parameters:
 *   BYTE*			lpbyDIB				Buffer to store DIB in.
 *   DOOMPICTURE*	*lpdp				Doom picture structure.
 *   int			cbImage				Length of buffer pointed to by lpdp.
 *   short			cxDIB, cyDIB		Dimensions of buffer.
 *   short			xOffset, yOffset	Offset within DIB at which to blit.
 *
 * Return value: None.
 */
static void DoomPictureToDIB(BYTE *lpbyDIB, DOOMPICTURE_HEADER *lpdp, int cbImage, short cxDIB, short cyDIB, short xOffset, short yOffset)
{
	/* Part of offset into the data common to the whole patch. */
	int iPatchOffset = yOffset * cxDIB + xOffset;
	int cbTextureBitmap = cxDIB * cyDIB;
	int i;

	/* Blit patch to texture. Loop for each column. */
	int iColLimit = min(lpdp->cx, cxDIB - xOffset);

	for(i = max(0, -xOffset); i < iColLimit; i++)
	{
		/* Part of offset into the data common to this column. */
		int iColOffset = iPatchOffset + i;

		/* Find this column's first post. */
		DOOMPICTURE_POST *lppost = (DOOMPICTURE_POST*)&((BYTE*)lpdp)[lpdp->iDataOffsets[i]];

		/* Loop until we get to the column-end magic number. */
		while(lppost->byRowStart != COLUMN_END)
		{
			int iPostOffset = iColOffset + lppost->byRowStart * cxDIB;
			int j, iRowLimit;

			/* Clipping. */
			iRowLimit = min(lppost->byNumPixels, cyDIB - yOffset);
			for(j = max(0, -yOffset); j < iRowLimit; j++)
			{
				/* Skies are very strange. They seem to have another post
				 * following the normal one, which purports to have full height,
				 * but only actually lasts a byte before we see what looks like
				 * a COLUMN_END marker (even though it's in the middle of a
				 * post) whereafter you can read synchronously again. The offset
				 * directory also resyncs there. So, as a workaround, just check
				 * some buffer boundaries.
				 */
				if(iPostOffset + j * cxDIB >= cbTextureBitmap) continue;

				/* TODO: If this proves to be right, move this one into
				 * iRowLimit.
				 */
				if(lpdp->iDataOffsets[i] + j >= cbImage) break;

				lpbyDIB[iPostOffset + j * cxDIB] = lppost->byPixels[j];
			}

			/* Move to next post. Skip the unused byte at end. */
			lppost = (DOOMPICTURE_POST*)&lppost->byPixels[lppost->byNumPixels + 1];
		}
	}
}


/* CreateTextureNameList
 *   Creates a new texture name list structure.
 *
 * Parameters:
 *   None.
 *
 * Return value: TEXTURENAMELIST*
 *   Pointer to new structure.
 *
 * Remarks:
 *   The caller should call DestroyTextureNameList to free the memory.
 */
TEXTURENAMELIST* CreateTextureNameList(void)
{
	TEXTURENAMELIST *lptnl = ProcHeapAlloc(sizeof(TEXTURENAMELIST));

	lptnl->iEntries = 0;
	lptnl->cstrBuffer = TNL_BUFSIZEINCREMENT;
	lptnl->szNames = ProcHeapAlloc(lptnl->cstrBuffer * sizeof(TCHAR) * TEXNAME_BUFFER_LENGTH);

	return lptnl;
}


/* DestroyTextureNameList
 *   Destroys a texture name list structure.
 *
 * Parameters:
 *   TEXTURENAMELIST*	lptnl	List to destroy.
 *
 * Return value: None.
 */
void DestroyTextureNameList(TEXTURENAMELIST *lptnl)
{
	ProcHeapFree(lptnl->szNames);
	ProcHeapFree(lptnl);
}


/* AddTextureNameToList
 *   Adds a texture name to a texture name list.
 *
 * Parameters:
 *   const char*	sTexName		Name of texture. Not NULL-terminated.
 *   void*			lpvParam		(TEXTURENAMELIST*) List to add to.
 *
 * Return value: None.
 *
 * Remarks:
 *   The list is extended if necessary. This is intended to be used as a
 *   callback by EnumTextures.
 */
static void CALLBACK AddTextureNameToList(const char *sTexName, void *lpvParam)
{
	TEXTURENAMELIST *lptnl = (TEXTURENAMELIST*)lpvParam;
	CHAR szTexNameA[TEXNAME_BUFFER_LENGTH];
	TCHAR szTexNameT[TEXNAME_BUFFER_LENGTH];

	if(lptnl->iEntries == lptnl->cstrBuffer)
	{
		/* No more room in buffer; make it bigger. */
		lptnl->cstrBuffer += TNL_BUFSIZEINCREMENT;
		lptnl->szNames = ProcHeapReAlloc(lptnl->szNames, lptnl->cstrBuffer * sizeof(TCHAR) * TEXNAME_BUFFER_LENGTH);
	}

	/* Terminate the string. */
	strncpy(szTexNameA, sTexName, TEXNAME_WAD_BUFFER_LENGTH);
	szTexNameA[TEXNAME_WAD_BUFFER_LENGTH] = '\0';

	/* Convert string from ANSI if necessary. */
	wsprintf(szTexNameT, TEXT("%hs"), szTexNameA);

	/* Add to list. */
	_tcscpy(&lptnl->szNames[TEXNAME_BUFFER_LENGTH * lptnl->iEntries++], szTexNameT);
}


/* AddAllTextureNamesToList
 *   Adds all texture names from a wad to a texture name list.
 *
 * Parameters:
 *   TEXTURENAMELIST*	lptnl	List to add to.
 *   WAD*				lpwad	Wad from which to add textures.
 *   TEX_FORMAT			tf		Flats or textures?
 *
 * Return value: None.
 */
void AddAllTextureNamesToList(TEXTURENAMELIST *lptnl, WAD *lpwad, TEX_FORMAT tf)
{
	EnumTextures(lpwad, tf, AddTextureNameToList, lptnl);
}


/* SortTextureNameList, CompareTextureNames
 *   Sorts a list of texture names.
 *
 * Parameters:
 *   TEXTURENAMELIST*	lptnl	List to sort.
 *
 * Return value: None.
 */
void SortTextureNameList(TEXTURENAMELIST *lptnl)
{
	qsort(lptnl->szNames, lptnl->iEntries, TEXNAME_BUFFER_LENGTH * sizeof(TCHAR), (int (*)(const void*, const void*))CompareTextureNames);
}

static int CompareTextureNames(LPCTSTR sz1, LPCTSTR sz2)
{
	return _tcscmp(sz1, sz2);
}

LPCTSTR FindFirstTexNameMatch(LPCTSTR szTexName, TEXTURENAMELIST *lptnl)
{
	return _lfind(szTexName, lptnl->szNames, &lptnl->iEntries, TEXNAME_BUFFER_LENGTH * sizeof(TCHAR), (int (*)(const void*, const void*))CompareTextureNameHeads);
}

static int CompareTextureNameHeads(LPCTSTR sz1, LPCTSTR sz2)
{
	int iLen = min(_tcslen(sz1), _tcslen(sz2));
	return _tcsncmp(sz1, sz2, iLen);
}


/* StretchTextureToDC
 *   Blits a texture to a DC, shrinking it if necessary.
 *
 * Parameters:
 *   TEXTURE*	lptex		Texture to blit.
 *   HDC		hdcTarget	Target device context.
 *   int		cx, cy		Dimensions of destination.
 *
 * Return value: None.
 */
void StretchTextureToDC(TEXTURE *lptex, HDC hdcTarget, int cx, int cy)
{
	int xStretch, yStretch, cxStretch, cyStretch;
	HDC hdcSource;
	HBITMAP hbmSource, hbmOld;

	/* Clear first. TODO: Not sure whether resource loading really belongs
	 * here...
	 */
	hbmSource = LoadBitmap(g_hInstance, MAKEINTRESOURCE(IDB_NOTEXTURE));
	hdcSource = CreateCompatibleDC(NULL);
	hbmOld = SelectObject(hdcSource, hbmSource);

	BitBlt(hdcTarget, 0, 0, CX_TEXPREVIEW, CY_TEXPREVIEW, hdcSource, 0, 0, SRCCOPY);

	SelectObject(hdcSource, hbmOld);
	DeleteObject(hbmSource);

	/* Keep the DC around for later. */

	if(lptex->cx <= cx && lptex->cy <= cy)
	{
		/* Texture fits within preview control */

		xStretch = (cx - lptex->cx) >> 1;
		yStretch = (cy - lptex->cy) >> 1;
		cxStretch = lptex->cx;
		cyStretch = lptex->cy;
	}
	else
	{
		/* Preview control and texture are in different ratios.
		 * Centre the latter in the former.
		 */

		if(lptex->cx * cy > lptex->cy * cx)
		{
			/* Texture is wider than it is high, with respect to the ratio of
			 * the preview control.
			 */

			cxStretch = cx;
			cyStretch = (lptex->cy * cy) / lptex->cx;
			xStretch = 0;
			yStretch = (cy - cyStretch) >> 1;
		}
		else
		{
			/* Texture is at least as high as it is wide, with respect to the
			 * ratio of the preview control.
			 */

			cyStretch = cy;
			cxStretch = (lptex->cx * cx) / lptex->cy;
			yStretch = 0;
			xStretch = (cx - cxStretch) >> 1;
		}
	}

	/* Just lose the intermediate pixels. */
	SetStretchBltMode(hdcTarget, COLORONCOLOR);

	/* Blit! We already have a DC to blit from. */
	SelectObject(hdcSource, lptex->hbitmap);
	StretchBlt(	hdcTarget,
				xStretch, yStretch, cxStretch, cyStretch,
				hdcSource, 0, 0, lptex->cx, lptex->cy,
				SRCCOPY);

	DeleteDC(hdcSource);
}


/* IsPseudoTexture, IsPseudoFlat, IsBlankTexture
 *   Determines whether a texture that does not actually exist should
 *   nonetheless not be reported as missing, or whether it corresponds to the
 *   blank texture.
 *
 * Parameters:
 *   LPCTSTR	sTexName	Name of texture. Need not be NUL-terminated.
 *
 * Return value: BOOL
 *   TRUE if pseudo-texture; FALSE if not.
 *
 * Remarks:
 *   Pseudo-textures are useful for things like F_SKY1 and colourmaps. The blank
 *   texture is the one that's normally "-".
 */

#ifdef _UNICODE
BOOL IsPseudoTextureW(LPCWSTR sTexName)
{
	WCHAR szTexNameW[TEXNAME_BUFFER_LENGTH];
	CHAR szTexNameA[TEXNAME_BUFFER_LENGTH];

	/* First make a NUL-terminated Unicode string. */
	wcsncpy(szTexNameW, sTexName, TEXNAME_WAD_BUFFER_LENGTH);

	/* Make it ANSI. */
	WideCharToMultiByte(CP_ACP, 0, szTexNameW, -1, szTexNameA, sizeof(szTexNameA), NULL, NULL);

	return IsPseudoTextureA(szTexNameA);
}
#endif

BOOL IsPseudoTextureA(LPCSTR sTexName)
{
	UNREFERENCED_PARAMETER(sTexName);
	/* TODO: Check for pseudo-textures from config. */
	return FALSE;
}

#ifdef _UNICODE
BOOL IsPseudoFlatW(LPCWSTR sTexName)
{
	WCHAR szTexNameW[TEXNAME_BUFFER_LENGTH];
	CHAR szTexNameA[TEXNAME_BUFFER_LENGTH];

	/* First make a NUL-terminated Unicode string. */
	wcsncpy(szTexNameW, sTexName, TEXNAME_WAD_BUFFER_LENGTH);

	/* Make it ANSI. */
	WideCharToMultiByte(CP_ACP, 0, szTexNameW, -1, szTexNameA, sizeof(szTexNameA), NULL, NULL);

	return IsPseudoFlatA(szTexNameA);
}
#endif

BOOL IsPseudoFlatA(LPCSTR sTexName)
{
	UNREFERENCED_PARAMETER(sTexName);
	/* TODO: Check for pseudo-flats from config. */
	return FALSE;
}

#ifdef _UNICODE
BOOL IsBlankTextureW(LPCWSTR sTexName)
{
	WCHAR szTexNameW[TEXNAME_BUFFER_LENGTH];
	CHAR szTexNameA[TEXNAME_BUFFER_LENGTH];

	/* First make a NUL-terminated Unicode string. */
	wcsncpy(szTexNameW, sTexName, TEXNAME_WAD_BUFFER_LENGTH);

	/* Make it ANSI. */
	WideCharToMultiByte(CP_ACP, 0, szTexNameW, -1, szTexNameA, sizeof(szTexNameA), NULL, NULL);

	return IsBlankTextureA(szTexNameA);
}
#endif

BOOL IsBlankTextureA(LPCSTR sTexName)
{
	if(strncmp(sTexName, "-", TEXNAME_WAD_BUFFER_LENGTH) == 0) return TRUE;

	/* TODO: Get blank texture string from config. */

	return FALSE;
}


/* IsBonaFideTexture
 *   Determines whether a texture is a real texture.
 *
 * Parameters:
 *   LPCTSTR	sTexName	Name of texture. Need not be terminated.
 *
 * Return value: BOOL
 *   TRUE if a real texture; FALSE if not.
 */
BOOL IsBonaFideTexture(LPCTSTR sTexName)
{
	return *sTexName && !IsPseudoTexture(sTexName) && !IsBlankTexture(sTexName);
}

BOOL IsBonaFideTextureA(LPCSTR sTexName)
{
	return *sTexName && !IsPseudoTextureA(sTexName) && !IsBlankTextureA(sTexName);
}
