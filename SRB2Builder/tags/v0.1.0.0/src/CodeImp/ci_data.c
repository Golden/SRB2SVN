/*
'    Doom Builder
'    Copyright (c) 2003 Pascal vd Heiden, www.codeimp.com
'    This program is released under GNU General Public License
'
'    This program is distributed in the hope that it will be useful,
'    but WITHOUT ANY WARRANTY; without even the implied warranty of
'    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
'    GNU General Public License for more details.
*/


// Definitions
#define WIN32_LEAN_AND_MEAN

// Includes
#include <windows.h>
#include <stdlib.h>

#include "../general.h"
#include "../maptypes.h"
#include "../editing.h"
#include "ci_map.h"
#include "ci_const.h"
#include "ci_poly.h"
#include "ci_math.h"
#include "ci_data_proto.h"


static __inline int InRect(float x1rect, float y1rect, float x2rect, float y2rect, float x, float y);

// This reallocates an int array and preserves any contents
//----------------------------------------------------------------------------
void __fastcall ReallocIntP(int **lplpintarray, int oldlength, int newlength)
{
	// Allocate new memory
	int* newarray = ProcHeapAlloc(newlength * sizeof(int));
	
	// Check if an old list is given
	if(*lplpintarray != NULL)
	{
		// Copy contents
		CopyMemory(newarray, *lplpintarray, oldlength * sizeof(int));
		
		// Kill the old list
		ProcHeapFree(*lplpintarray);
	}
	
	// Apply the new list
	*lplpintarray = newarray;
}


// ThingFiltered: Returns 1 if a given thing must be shown
//----------------------------------------------------------------------------
int __fastcall ThingFiltered(MAPTHING* thing, int filterthings, THINGFILTERS* filter)
{
	// Check if filtering
	if(filterthings)
	{
		// Test category
		if( (filter->category == -1) || (thing->category == filter->category) )
		{
			// Check filter mode
			switch(filter->filtermode)
			{
				case 0: return ((thing->flag & filter->flags) != 0) || (thing->flag == 0);
				case 1: return ((thing->flag & filter->flags) == filter->flags);
				case 2: return (thing->flag == filter->flags);
				default: return 0;
			}
		}
		else
		{
			// Wrong category
			return 0;
		}
	}
	else
	{
		// Always shown
		return 1;
	}
}


// CountSectorSidedefs: Returns the number of sidedefs referring to a given sector
//-----------------------------------------------------------------------------
static __inline int APIENTRY CountSectorSidedefs(MAPSIDEDEF* sidedefs, int numsidedefs, int sector)
{
	int sd;
	int count = 0;
	
	// Count all sidedefs that refer to the givens ector
	for(sd = 0; sd < numsidedefs; sd++) { if(sidedefs[sd].sector == sector) count++; }
	
	// Return result
	return count;
}


// CountVertexLinedefs: Returns the number of linedefs referring to a given vertex
//-----------------------------------------------------------------------------
static __inline int APIENTRY CountVertexLinedefs(MAPLINEDEF* linedefs, int numlinedefs, int vertex)
{
	int count = 0;
	int ld;
	
	// Count all lines that refer to the given vertex
	for(ld = 0; ld < numlinedefs; ld++)	{ if((linedefs[ld].v1 == vertex) || (linedefs[ld].v2 == vertex)) count++; }
	
	// Return result
	return count;
}


// ResetSelections: Sets all selected properties to 0
//-----------------------------------------------------------------------------
void ResetSelections(MAPTHING* things, int numthings, MAPLINEDEF* linedefs, int numlinedefs,
							  MAPVERTEX* vertices, int numvertices, MAPSECTOR* sectors, int numsectors)
{
	int i;
	
	// Reset all selected properties
	if(numthings) for(i = 0; i < numthings; i++) things[i].selected = 0;
	if(numlinedefs) for(i = 0; i < numlinedefs; i++) linedefs[i].selected = 0;
	if(numvertices) for(i = 0; i < numvertices; i++) vertices[i].selected = 0;
	if(numsectors) for(i = 0; i < numsectors; i++) sectors[i].selected = 0;
}


// NearestVertex: Returns the nearest vertex index
//----------------------------------------------------------------------------
int NearestVertex(MAP *lpmap, int x, int y, int *lpdist)
{
	int foundvertex = -1;
	int founddistance = INT_MAX;
	int d, v;
	
	// Correct the Y axis (IGD - Don't think this is needed.)
	// y = -y;
	
	// Go for all vertices
	for(v = 0; v < lpmap->iVertices; v++)
	{
		// Calculate distance
		d = (int)distancei(lpmap->vertices[v].x, lpmap->vertices[v].y, x, y);
		
		// Check if closer
		if(d < founddistance)
		{
			// Found a closer match
			foundvertex = v;
			founddistance = d;
		}
	}
	
	// Return result
	*lpdist = founddistance;
	return foundvertex;
}


// NearestConditionedVertex: Returns the nearest vertex index that satisfies a
// given condition.
//----------------------------------------------------------------------------
int NearestConditionedVertex(MAP *lpmap, int x, int y, int *lpdist, BOOL (*fnCondition)(MAPVERTEX *lpxv, void *lpvParam), void *lpvParam)
{
	int foundvertex = -1;
	int founddistance = INT_MAX;
	int d, v;
	
	// Correct the Y axis (IGD - Don't think this is needed.)
	// y = -y;
	
	// Go for all vertices
	for(v = 0; v < lpmap->iVertices; v++)
	{
		if(fnCondition(&lpmap->vertices[v], lpvParam))
		{
			// Calculate distance
			d = (int)distancei(lpmap->vertices[v].x, lpmap->vertices[v].y, x, y);
			
			// Check if closer
			if(d < founddistance)
			{
				// Found a closer match
				foundvertex = v;
				founddistance = d;
			}
		}
	}
	
	// Return result
	*lpdist = founddistance;
	return foundvertex;
}




// NearestOtherVertex: Returns the vertex index nearest to given vertex index
//----------------------------------------------------------------------------
int NearestOtherVertex(MAP *lpmap, int vx, int *lpdist)
{
	int foundvertex = -1;
	int founddistance = INT_MAX;
	int d, v;
	int x = (int)lpmap->vertices[vx].x;
	int y = (int)lpmap->vertices[vx].y;
	
	// Go for all vertices before vx
	for(v = 0; v < vx; v++)
	{
		// Calculate distance
		d = (int)distancei(lpmap->vertices[v].x, lpmap->vertices[v].y, x, y);
		
		// Check if closer
		if(d < founddistance)
		{
			// Found a closer match
			foundvertex = v;
			founddistance = d;
		}
	}
	
	// Go for all vertices after vx
	for(v = vx + 1; v < lpmap->iVertices; v++)
	{
		// Calculate distance
		d = (int)distancei(lpmap->vertices[v].x, lpmap->vertices[v].y, x, y);
		
		// Check if closer
		if(d < founddistance)
		{
			// Found a closer match
			foundvertex = v;
			founddistance = d;
		}
	}
	
	// Return result
	*lpdist = founddistance;
	return foundvertex;
}


// NearestSelectedVertex: Returns the nearest selected vertex index
//----------------------------------------------------------------------------
static __inline int APIENTRY NearestSelectedVertex(int x, int y, MAPVERTEX* vertices, int numvertices, int *lpdist)
{
	int foundvertex = -1;
	int founddistance = INT_MAX;
	int d, v;
	
	// Correct the Y axis (IGD - Don't think this is needed.)
	// y = -y;
	
	// Go for all vertices
	for(v = 0; v < numvertices; v++)
	{
		// Check if selected
		if(vertices[v].selected)
		{
			// Calculate distance
			d = (int)distancei(vertices[v].x, vertices[v].y, x, y);
			
			// Check if closer
			if(d < founddistance)
			{
				// Found a closer match
				foundvertex = v;
				founddistance = d;
			}
		}
	}
	
	// Return result
	*lpdist = founddistance;
	return foundvertex;
}


// NearestUnselectedVertex: Returns the nearest unselected vertex index
//----------------------------------------------------------------------------
static __inline int APIENTRY NearestUnselectedVertex(int x, int y, MAPVERTEX* vertices, int numvertices, int *lpdist)
{
	int foundvertex = -1;
	int founddistance = INT_MAX;
	float fx = (float)x, fy = (float)y;
	int d, v;
	
	// Correct the Y axis (IGD - Don't think this is needed.)
	// y = -y;
	
	// Go for all vertices
	for(v = 0; v < numvertices; v++)
	{
		// Check if not selected
		if(vertices[v].selected == 0)
		{
			// Calculate distance
			d = (int)distancef(vertices[v].x, vertices[v].y, fx, fy);
			
			// Check if closer
			if(d < founddistance)
			{
				// Found a closer match
				foundvertex = v;
				founddistance = d;
			}
		}
	}
	
	// Return result
	*lpdist = founddistance;
	return foundvertex;
}


// OverlappingUnselectedVertex: Returns the overlapping unselected vertex index
//----------------------------------------------------------------------------
static __inline int APIENTRY OverlappingUnselectedVertex(MAPVERTEX* vertices, int numvertices, int tv)
{
	float fx = vertices[tv].x;
	float fy = vertices[tv].y;
	int v;
	
	// Go for all vertices
	for(v = 0; v < numvertices; v++)
	{
		// Check if not selected
		if(vertices[v].selected == 0)
		{
			// Check if overlapping
			if((vertices[v].x == fx) && (vertices[v].y == fy))
			{
				// if this is not the same vertex as the source,
				// then this is an overlapping vertex
				if(v != tv) return v;
			}
		}
	}
	
	// Return -1 (nothing found)
	return -1;
}


// OverlappingUnselectedLinedef: Returns the overlapping unselected linedef index
//----------------------------------------------------------------------------
static __inline int APIENTRY OverlappingUnselectedLinedef(MAPLINEDEF* linedefs, int numlinedefs, int tl)
{
	float tlv1 = (float)linedefs[tl].v1;
	float tlv2 = (float)linedefs[tl].v2;
	int ld;
	
	// Go for all linedefs
	for(ld = 0; ld < numlinedefs; ld++)
	{
		// Check if not selected
		if(linedefs[ld].selected == 0)
		{
			// Check if overlapping
			if(((linedefs[ld].v1 == tlv1) && (linedefs[ld].v2 == tlv2)) ||
			   ((linedefs[ld].v1 == tlv2) && (linedefs[ld].v2 == tlv1)))
			{
				// if this is not the same linedef as the source,
				// then this is an overlapping linedef
				if(ld != tl) return ld;
			}
		}
	}
	
	// Return -1 (nothing found)
	return -1;
}


// NearestConditionedVertexToLinedef: Returns the index of the vertex nearest to
// a linedef, where the vertex satisfies a given condition.
//----------------------------------------------------------------------------
int NearestConditionedVertexToLinedef(MAP *lpmap, int iLinedef, int *lpdist, BOOL (*fnCondition)(MAPVERTEX *lpxv, void *lpvParam), void *lpvParam)
{
	int foundvertex = -1;
	int founddistance = INT_MAX;
	int d, v;
	int x1 = lpmap->vertices[lpmap->linedefs[iLinedef].v1].x;
	int y1 = lpmap->vertices[lpmap->linedefs[iLinedef].v1].y;
	int x2 = lpmap->vertices[lpmap->linedefs[iLinedef].v2].x;
	int y2 = lpmap->vertices[lpmap->linedefs[iLinedef].v2].y;
	
	// Correct the Y axis (IGD - Don't think this is needed.)
	// y = -y;
	
	// Go for all vertices
	for(v = 0; v < lpmap->iVertices; v++)
	{
		if(fnCondition(&lpmap->vertices[v], lpvParam))
		{
			// Calculate distance
			d = (int)distance_to_line((float)x1, (float)y1, (float)x2, (float)y2, (float)lpmap->vertices[v].x, (float)lpmap->vertices[v].y);
			
			// Check if closer
			if(d < founddistance)
			{
				// Found a closer match
				foundvertex = v;
				founddistance = d;
			}
		}
	}
	
	// Return result
	*lpdist = founddistance;
	return foundvertex;
}





// CrossingLinedef: Returns the crossing linedef index, looking backwards from begin.
//----------------------------------------------------------------------------
int CrossingLinedef(MAPLINEDEF* linedefs, MAPVERTEX* vertices, int tl, int begin)
{
	// To ensure that they aren't congruent.
	int tlv1 = linedefs[tl].v1;
	int tlv2 = linedefs[tl].v2;
	int ld;

	float m = 0, M = 0;		// Gradients.
	float numadd = 0;		// Add to numerator.
	float x, y;				// Point of intersection.
	float x1, y1, x2, y2;	// Co-ord of linedef tl's vertices.
	float X1, Y1, X2, Y2;	// Co-ord of linedef ld's vertices.

	// Calculate gradient etc. of the top ld.
	x1 = vertices[linedefs[tl].v1].x;
	y1 = vertices[linedefs[tl].v1].y;
	x2 = vertices[linedefs[tl].v2].x;
	y2 = vertices[linedefs[tl].v2].y;
	
	if(x1 != x2)
	{
		m = (y2 - y1) / (x2 - x1);
		numadd = x1 * m - y1;
	}
	
	// Go for all linedefs.
	for(ld = begin; ld >= 0; ld--)
	{
		// Calculate gradient of this ld.
		X1 = vertices[linedefs[ld].v1].x;
		Y1 = vertices[linedefs[ld].v1].y;
		X2 = vertices[linedefs[ld].v2].x;
		Y2 = vertices[linedefs[ld].v2].y;
		
		if(X2 != X1) M = (Y2 - Y1) / (X2 - X1);

		if(M != m || x1 == x2 || X1 == X2)
		{
			if(X1 != X2 && x1 != x2)
			{
				x = (numadd + Y1 - X1 * M) / (m-M);
				y = m * x - numadd;
			}
			else if(x1 == x2 && X1 == X2)	// Both vertical.
			{
				if(x1 != X1) continue;		// Parallel!
				else						// Coincident, but not necessarily congruent.
				{
					// Handle this seperately. It's easier.
					if((y1 < Y1 && y1 > Y2) || (y1 > Y1 && y1 < Y2)) return ld;
					else continue;
				}
			}
			else if(x1 == x2)				// X1 != X2
			{
				x = x1;
				y = M * (x - X1) + Y1;
			}
			else							// X1 == X2 && x1 != x2
			{
				x = X1;
				y = m * x - numadd;
			}
		}
		else		// Parallel or co-incident, and neither vertical.
		{
			// Parallel?
			if(y1 - m * x1 != Y1 - m * X1) continue;
			else
			{
				// We only need to test one co-ordinate.
				if((y1 < Y1 && y1 > Y2) || (y1 > Y1 && y1 < Y2)) return ld;
				else continue;
			}

		}


		// Check if in range (hence crossing)
		if(InLDRectOpen(linedefs, vertices, tl, x, y) && InLDRectOpen(linedefs, vertices, ld, x, y))
		{
			// But not congruent...
			if( !(((linedefs[ld].v1 == tlv1) && (linedefs[ld].v2 == tlv2)) ||
				  ((linedefs[ld].v1 == tlv2) && (linedefs[ld].v2 == tlv1))))
			{
				// if this is not the same linedef as the source,
				// then this is a crossed linedef
				if(ld != tl) return ld;
			}
		}
	}
	
	// Return -1 (nothing found)
	return -1;
}


// CrossingLinedef: Returns the index of a linedef intersecting the specified
// line segment *at a point*, looking backwards from begin.
//----------------------------------------------------------------------------
int LinedefIntersectingSegmentAtPoint(MAPLINEDEF* linedefs, MAPVERTEX* vertices, float x1, float y1, float x2, float y2, int begin)
{
	// To ensure that they aren't congruent.
	int ld;

	float m = 0, M = 0;		// Gradients.
	float numadd = 0;		// Add to numerator.
	float x, y;				// Point of intersection.
	float X1, Y1, X2, Y2;	// Co-ord of linedef ld's vertices.
	
	if(x1 != x2)
	{
		m = (y2 - y1) / (x2 - x1);
		numadd = x1 * m - y1;
	}
	
	// Go for all linedefs.
	for(ld = begin; ld >= 0; ld--)
	{
		// Calculate gradient of this ld.
		X1 = vertices[linedefs[ld].v1].x;
		Y1 = vertices[linedefs[ld].v1].y;
		X2 = vertices[linedefs[ld].v2].x;
		Y2 = vertices[linedefs[ld].v2].y;
		
		if(X2 != X1) M = (Y2 - Y1) / (X2 - X1);

		if(M != m || x1 == x2 || X1 == X2)
		{
			if(X1 != X2 && x1 != x2)
			{
				x = (numadd + Y1 - X1 * M) / (m-M);
				y = m * x - numadd;
			}
			else if(x1 == x2 && X1 == X2)	// Both vertical.
			{
				// They definitely don't intersect a point.
				continue;
			}
			else if(x1 == x2)				// X1 != X2
			{
				x = x1;
				y = M * (x - X1) + Y1;
			}
			else							// X1 == X2 && x1 != x2
			{
				x = X1;
				y = m * x - numadd;
			}
		}
		else		// Parallel or co-incident, and neither vertical.
		{
			// They definitely don't intersect a point.
			continue;
		}


		// Check if in range (hence crossing)
		if(InRect(x1, y1, x2, y2, x, y) && InRect(X1, Y1, X2, Y2, x, y))
		{
			// But not congruent... [Check isn't necessary, is it?]
			//if( !(x1 == X1 && x2 == X2 && y1 == Y1 && y2 == Y2) ||
			//	  (x1 == X2 && x2 == X1 && y1 == Y2 && y2 == Y1))
			//{
				// this is not the same linedef as the source,
				// so this is a crossed linedef
				return ld;
			//}
		}
	}
	
	// Return -1 (nothing found)
	return -1;
}

static __inline int InRect(float x1rect, float y1rect, float x2rect, float y2rect, float x, float y)
{
	int x1 = (int)(x1rect * 10 + 0.5);
	int y1 = (int)(y1rect * 10 + 0.5);
	int x2 = (int)(x2rect * 10 + 0.5);
	int y2 = (int)(y2rect * 10 + 0.5);
	int t;
	int X=(int)(x*10 + 0.5), Y=(int)(y*10 + 0.5);

	if(x1 > x2) {t=x1; x1=x2; x2=t;}
	if(y1 > y2) {t=y1; y1=y2; y2=t;}

	return (X >= x1 && X <= x2 && Y > y1 && Y < y2) || (X > x1 && X < x2 && Y >= y1 && Y <= y2);
}


// NearestThing: Returns the nearest thing index
//----------------------------------------------------------------------------
int NearestThing(int x, int y, MAPTHING* things, int numthings, int *lpdist, int filterthings, THINGFILTERS* filter)
{
	int foundthing = -1;
	int founddistance = INT_MAX;
	int d, th;
	
	// Correct the Y axis (IGD - Don't think this is needed.)
	// y = -y;
	
	// Go for all things
	for(th = 0; th < numthings; th++)
	{
		// Check if any of the thing flags match any of the filter flags
		if(ThingFiltered(&things[th], filterthings, filter) || (things[th].selected != 0))
		{
			// Calculate distance
			d = (int)distancei(things[th].x, things[th].y, x, y);
			
			// Check if closer
			if(d < founddistance)
			{
				// Found a closer match
				foundthing = th;
				founddistance = d;
			}
		}
	}
	
	// Return result
	*lpdist = founddistance;
	return foundthing;
}


// NearestSelectedThing: Returns the nearest selected thing index
//----------------------------------------------------------------------------
static __inline int APIENTRY NearestSelectedThing(int x, int y, MAPTHING* things, int numthings, int *lpdist)
{
	int foundthing = -1;
	int founddistance = INT_MAX;
	int d, th;
	
	// Correct the Y axis (IGD - Don't think this is needed.)
	// y = -y;
	
	// Go for all vertices
	for(th = 0; th < numthings; th++)
	{
		// Check if selected or all things are allowed
		if(things[th].selected)
		{
			// Calculate distance
			d = (int)distancei(things[th].x, things[th].y, x, y);
			
			// Check if closer
			if(d < founddistance)
			{
				// Found a closer match
				foundthing = th;
				founddistance = d;
			}
		}
	}
	
	// Return result
	*lpdist = founddistance;
	return foundthing;
}


// NearestUnselectedThing: Returns the nearest unselected thing index
//----------------------------------------------------------------------------
static __inline int APIENTRY NearestUnselectedThing(int x, int y, MAPTHING* things, int numthings, int *lpdist, int filterthings, THINGFILTERS* filter)
{
	int foundthing = -1;
	int founddistance = INT_MAX;
	int d, th;
	
	// Correct the Y axis (IGD - Don't think this is needed.)
	// y = -y;
	
	// Go for all vertices
	for(th = 0; th < numthings; th++)
	{
		// Check if selected or all things are allowed
		if(things[th].selected == 0)
		{
			// Check if any of the thing flags match any of the filter flags
			if(ThingFiltered(&things[th], filterthings, filter))
			{
				// Calculate distance
				d = (int)distancei(things[th].x, things[th].y, x, y);
				
				// Check if closer
				if(d < founddistance)
				{
					// Found a closer match
					foundthing = th;
					founddistance = d;
				}
			}
		}
	}
	
	// Return result
	*lpdist = founddistance;
	return foundthing;
}


// NearestLinedef: Returns the nearest linedef index
//----------------------------------------------------------------------------
int __fastcall NearestLinedef(int x, int y, MAPVERTEX* vertices, MAPLINEDEF* linedefs, int numlinedefs, int *lpdist)
{
	int foundlinedef = -1;
	float founddistance = 10000000;
	float d;
	int l;
	
	// Correct the Y axis (IGD - Don't think this is needed.)
	// y = -y;
	
	// Go for all linedefs
	for(l = 0; l < numlinedefs; l++)
	{
		// Get linedef vertices
		MAPVERTEX* v1 = &vertices[linedefs[l].v1];
		MAPVERTEX* v2 = &vertices[linedefs[l].v2];
		
		// Get shortest distance to linedef
		d = distance_to_line(v1->x, v1->y, v2->x, v2->y, (float)x, (float)y);
		
		// Check if closer but 'within' range
		if(d < founddistance)
		{
			// Found a closer match
			foundlinedef = l;
			founddistance = d;
		}
	}
	
	// Return result
	*lpdist = (int)founddistance;
	return foundlinedef;
}

// Floating-point version of the above.
static __inline int NearestLinedefFloat(float x, float y, MAPVERTEX* vertices, MAPLINEDEF* linedefs, int numlinedefs, int *lpdist)
{
	int foundlinedef = -1;
	float founddistance = 1000000;
	float d;
	int l;
	MAPVERTEX *v1, *v2;
	
	// Correct the Y axis (IGD - Don't think this is needed.)
	// y = -y;
	
	// Go for all linedefs
	for(l = 0; l < numlinedefs; l++)
	{
		// Get linedef vertices
		v1 = &vertices[linedefs[l].v1];
		v2 = &vertices[linedefs[l].v2];
		
		// Get shortest distance to linedef
		d = distance_to_line(v1->x, v1->y, v2->x, v2->y, x, y);
		
		// Check if closer but 'within' range
		if(d < founddistance)
		{
			// Found a closer match
			foundlinedef = l;
			founddistance = d;
		}
	}
	
	// Return result
	*lpdist = (int)founddistance;
	return foundlinedef;
}


// NearestSelectedLinedef: Returns the nearest selected linedef index
//----------------------------------------------------------------------------
static __inline int NearestSelectedLinedef(int x, int y, MAPVERTEX* vertices, MAPLINEDEF* linedefs, int* selected, int numselectedlinedefs, int *lpdist, int maxdist)
{
	int foundlinedef = -1;
	float founddistance = 1000000;
	float d;
	float fx = (float)x, fy = (float)y;
	int l, s;
	MAPVERTEX *v1, *v2;
	
	// Correct the Y axis (IGD - Don't think this is needed.)
	// y = -y;
	
	// Go for all linedefs
	for(s = 0; s < numselectedlinedefs; s++)
	{
		// Get the linedef
		l = selected[s];
		
		// Get linedef vertices
		v1 = &vertices[linedefs[l].v1];
		v2 = &vertices[linedefs[l].v2];
		
		// Check if point is near the line
		if(point_near_line(v1->x, v1->y, v2->x, v2->y, fx, fy, maxdist))
		{
			// Get shortest distance to linedef
			d = distance_to_line(v1->x, v1->y, v2->x, v2->y, fx, fy);
			
			// Check if closer but 'within' range
			if(d < founddistance)
			{
				// Found a closer match
				foundlinedef = l;
				founddistance = d;
			}
		}
	}
	
	// Return result
	*lpdist = (int)founddistance;
	return foundlinedef;
}


// NearestConditionedLinedef: Returns the nearest linedef index satisfying a
// particular condition.
//----------------------------------------------------------------------------
int NearestConditionedLinedef(int x, int y, MAPVERTEX* vertices, MAPLINEDEF* linedefs, int numlinedefs, int *lpdist, BOOL (*fnCondition)(MAPLINEDEF *lpld, void *lpvParam), void *lpvParam)
{
	int foundlinedef = -1;
	float founddistance = 1000000;
	float fx = (float)x, fy = (float)y;
	float d;
	int l;
	
	// Correct the Y axis (IGD - Don't think this is needed.)
	// y = -y;
	
	// Go for all linedefs
	for(l = 0; l < numlinedefs; l++)
	{
		// Check if satisfies condition
		if(fnCondition(&linedefs[l], lpvParam))
		{
			// Get linedef vertices
			MAPVERTEX* v1 = &vertices[linedefs[l].v1];
			MAPVERTEX* v2 = &vertices[linedefs[l].v2];
			
			// Check if point is near the line
			if(point_near_line(v1->x, v1->y, v2->x, v2->y, fx, fy, ENDLESS_DISTANCE))
			{
				// Get shortest distance to linedef
				d = distance_to_line(v1->x, v1->y, v2->x, v2->y, fx, fy);
				
				// Check if closer but 'within' range
				if(d < founddistance)
				{
					// Found a closer match
					foundlinedef = l;
					founddistance = d;
				}
			}
		}
	}
	
	// Return result
	*lpdist = (int)founddistance;
	return foundlinedef;
}

// Flaoting-point version of the above.
static __inline int NearestConditionedLinedefFloat(float x, float y, MAPVERTEX* vertices, MAPLINEDEF* linedefs, int numlinedefs, int *lpdist, BOOL (*fnCondition)(MAPLINEDEF *lpld, void *lpvParam), void *lpvParam)
{
	int foundlinedef = -1;
	float founddistance = 1000000;
	float d;
	int l;
		
	// Go for all linedefs
	for(l = 0; l < numlinedefs; l++)
	{
		// Check if satisfies condition
		if(fnCondition(&linedefs[l], lpvParam))
		{
			// Get linedef vertices
			MAPVERTEX* v1 = &vertices[linedefs[l].v1];
			MAPVERTEX* v2 = &vertices[linedefs[l].v2];
			
			// Check if point is near the line
			if(point_near_line(v1->x, v1->y, v2->x, v2->y, x, y, ENDLESS_DISTANCE))
			{
				// Get shortest distance to linedef
				d = distance_to_line(v1->x, v1->y, v2->x, v2->y, x, y);
				
				// Check if closer but 'within' range
				if(d < founddistance)
				{
					// Found a closer match
					foundlinedef = l;
					founddistance = d;
				}
			}
		}
	}
	
	// Return result
	*lpdist = (int)founddistance;
	return foundlinedef;
}


// LinedefSelected: Returns whether a linedef is selected.
//----------------------------------------------------------------------------
BOOL LinedefSelected(MAPLINEDEF *lpld, void *lpvParam)
{
	UNREFERENCED_PARAMETER(lpvParam);
	return lpld->selected;
}

// LinedefUnselected: Returns whether a linedef is UNselected.
//----------------------------------------------------------------------------
BOOL LinedefUnselected(MAPLINEDEF *lpld, void *lpvParam)
{
	UNREFERENCED_PARAMETER(lpvParam);
	return lpld->selected == 0;
}

// LinedefNoInvalidSD: Returns whether a linedef has valid sidedefs.
//----------------------------------------------------------------------------
BOOL LinedefNoInvalidSD(MAPLINEDEF *lpld, void *lpvParam)
{
	UNREFERENCED_PARAMETER(lpvParam);
	return !(lpld->editflags & LEF_INVALIDSIDEDEFS);
}

// LinedefDragged: Returns whether a linedef has been dragged.
//----------------------------------------------------------------------------
BOOL LinedefDragged(MAPLINEDEF *lpld, void *lpvParam)
{
	UNREFERENCED_PARAMETER(lpvParam);
	return (lpld->editflags & LEF_RECALCSECTOR);
}

// LinedefNotAttachedToVertex: Returns whether a linedef is attached to a
// given vertex.
//----------------------------------------------------------------------------
BOOL LinedefNotAttachedToVertex(MAPLINEDEF *lpld, void *lpvParam)
{
	return (lpld->v1 != (int)lpvParam && lpld->v2 != (int)lpvParam);
}



// VertexUnlabelled: Returns whether a vertex is unlabelled.
//----------------------------------------------------------------------------
BOOL VertexUnlabelled(MAPVERTEX *lpvx, void *lpvParam)
{
	UNREFERENCED_PARAMETER(lpvParam);
	return !(lpvx->editflags & VEF_LABELLED);
}



// IntersectSector: Returns the intersecting sector index
//----------------------------------------------------------------------------
int IntersectSector(int x, int y, MAPVERTEX* vertices, MAPLINEDEF* linedefs, MAPSIDEDEF* sidedefs, int numlinedefs, BOOL (*fnCondition)(MAPLINEDEF *lpld, void *lpvParam), void *lpvParam)
{
	int nld;
	
	// Perform additional check if necessary
	if(fnCondition)
	{
		// Get the nearest linedef with valid sidedefs
		int lddist = 0;
		nld = NearestConditionedLinedef(x, y, vertices, linedefs, numlinedefs, &lddist, fnCondition, lpvParam);
	}
	else
	{
		// Lets get the nearest linedef
		int lddist = 0;
		nld = NearestLinedef(x, y, vertices, linedefs, numlinedefs, &lddist);
	}
	
	// Only continue if any linedefs found
	if(nld > -1)
	{
		// Get the vertices
		MAPVERTEX* v1 = &vertices[linedefs[nld].v1];
		MAPVERTEX* v2 = &vertices[linedefs[nld].v2];

		// Correct the Y axis (IGD - Don't think this is needed.)
		// y = -y;
		
		// Check the side of the line
		if(side_of_lineii((int)v1->x, (int)v1->y, (int)v2->x, (int)v2->y, x, y) < 0)
		{
			// Front side (sidedef 1)
			// Return the referenced sector number if a sidedef is referenced
			if(linedefs[nld].s1 > -1) return sidedefs[linedefs[nld].s1].sector; else return -1;
		}
		else
		{
			// Back side (sidedef 2)
			// Return the referenced sector number if a sidedef is referenced
			if(linedefs[nld].s2 > -1) return sidedefs[linedefs[nld].s2].sector; else return -1;
		}
	}
	else
	{
		// Nothing intersected
		return -1;
	}
}

// IntersectSectorFloat: Returns the intersecting sector index from a floating
//                       point co-ordinate. For lovers of one-fracunit-wide
//                       sectors.
//----------------------------------------------------------------------------
int IntersectSectorFloat(float x, float y, MAPVERTEX* vertices, MAPLINEDEF* linedefs, MAPSIDEDEF* sidedefs, int numlinedefs, BOOL (*fnCondition)(MAPLINEDEF *lpld, void *lpvParam), void *lpvParam)
{
	int nld;
	
	// Perform additional check if necessary
	if(fnCondition)
	{
		// Get the nearest linedef with valid sidedefs
		int lddist = 0;
		nld = NearestConditionedLinedefFloat(x, y, vertices, linedefs, numlinedefs, &lddist, fnCondition, lpvParam);
	}
	else
	{
		// Let's get the nearest linedef
		int lddist = 0;
		nld = NearestLinedefFloat(x, y, vertices, linedefs, numlinedefs, &lddist);
	}
	
	// Only continue if any linedefs found
	if(nld > -1)
	{
		// Get the vertices
		MAPVERTEX* v1 = &vertices[linedefs[nld].v1];
		MAPVERTEX* v2 = &vertices[linedefs[nld].v2];

		// Correct the Y axis (IGD - Don't think this is needed.)
		// y = -y;
		
		// Check the side of the line
		if(side_of_lineii((int)v1->x, (int)v1->y, (int)v2->x, (int)v2->y, (int)x, (int)y) < 0)
		{
			// Front side (sidedef 1)
			// Return the referenced sector number if a sidedef is referenced
			if(linedefs[nld].s1 > -1) return sidedefs[linedefs[nld].s1].sector; else return -1;
		}
		else
		{
			// Back side (sidedef 2)
			// Return the referenced sector number if a sidedef is referenced
			if(linedefs[nld].s2 > -1) return sidedefs[linedefs[nld].s2].sector; else return -1;
		}
	}
	else
	{
		// Nothing intersected
		return -1;
	}
}


// LinedefBetweenVertices: Returns the first next linedefs that is between the given vertices
//----------------------------------------------------------------------------
static __inline int LinedefBetweenVertices(MAPLINEDEF* linedefs, int numlinedefs, int startlinedef, int vertex1, int vertex2, int excludeline)
{
	int ld;

	// Go for all linedefs
	for(ld = startlinedef; ld < numlinedefs; ld++)
	{
		// Check linedef vertices
		if(((linedefs[ld].v1 == vertex1) && (linedefs[ld].v2 == vertex2)) ||
		   ((linedefs[ld].v2 == vertex1) && (linedefs[ld].v1 == vertex2))) if(ld != excludeline) return ld;
	}
	
	// None found
	return -1;
}


// SetAllThingSectors: Sets the sector property on all Things
//----------------------------------------------------------------------------
static __inline void SetAllThingSectors(MAPTHING* things, int numthings, MAPVERTEX* vertices, MAPLINEDEF* linedefs, int numlinedefs, MAPSIDEDEF* sidedefs)
{
	int th;

	// Go for all things
	for(th = 0; th < numthings; th++)
	{
		// Set the thing sector
		things[th].sector = IntersectSector(things[th].x, -things[th].y, vertices, linedefs, sidedefs, numlinedefs, NULL, NULL);
	}
}

// FurthestSectorVertex: Finds the furthest vertex from a line in sector
//----------------------------------------------------------------------------
static __inline int FurthestSectorVertex(MAPVERTEX* vertices, MAPLINEDEF* linedefs, MAPSIDEDEF* sidedefs, int numlinedefs, int line, int sector)
{ return FurthestSectorVertexF(vertices, linedefs, sidedefs, numlinedefs, line, sector); }
int FurthestSectorVertexF(MAPVERTEX* vertices, MAPLINEDEF* linedefs, MAPSIDEDEF* sidedefs, int numlinedefs, int line, int sector)
{
	int furthestvertex = -1;
	int furthestdistance = -1;
	int dx, dy, vx, vy, d;
	MAPVERTEX* v;
	int s1, s2;
	int ld;
	
	// Calculate line slope and coordinates
	vx = vertices[linedefs[line].v1].x;
	vy = vertices[linedefs[line].v1].y;
	dx = vx - vertices[linedefs[line].v2].x;
	dy = vy - vertices[linedefs[line].v2].y;
	
	// Go for all linedefs
	for(ld = 0; ld < numlinedefs; ld++)
	{
		// Check if this line is with the given sector
		if(linedefs[ld].s1 > -1) s1 = sidedefs[linedefs[ld].s1].sector; else s1 = -1;
		if(linedefs[ld].s2 > -1) s2 = sidedefs[linedefs[ld].s2].sector; else s2 = -1;
		if((s1 == sector) || (s2 == sector))
		{
			// Test first vertex
			v = vertices + linedefs[ld].v1;
			d = abs(((vy - v->y) * dx) - ((vx - v->x) * dy));
			if(d > furthestdistance)
			{
				furthestdistance = d;
				furthestvertex = linedefs[ld].v1;
			}
			
			// Test second vertex
			v = vertices + linedefs[ld].v2;
			d = abs(((vy - v->y) * dx) - ((vx - v->x) * dy));
			if(d > furthestdistance)
			{
				furthestdistance = d;
				furthestvertex = linedefs[ld].v2;
			}
		}
	}
	
	// Return result
	return furthestvertex;
}



// pointinsidedefs: This tests if a point lies within or on the edge of
// a polygon of sidedefs
//----------------------------------------------------------------------------
BOOL PointInSidedefs(MAP *lpmap, float x, float y, int* sideslist, int numsides)
{
	int *lpiLines = ProcHeapAlloc(numsides * sizeof(int));
	int i;
	BOOL bRet;

	for(i = 0; i < numsides; i++)
		lpiLines[i] = lpmap->sidedefs[sideslist[i]].linedef;

	bRet = PointInLinedefs(lpmap, x, y, lpiLines, numsides);

	ProcHeapFree(lpiLines);

	return bRet;
}

BOOL PointInLinedefs(MAP *lpmap, float x, float y, int* lineslist, int numlines)
{
	int inside = 0;
	int i;
	
	// Go for all linedefs
	for(i = 0; i < numlines; i++)
	{
		// Get the line vertices
		int v1 = lpmap->linedefs[lineslist[i]].v1;
		int v2 = lpmap->linedefs[lineslist[i]].v2;
		int x1, y1, x2, y2;
		
		// Get coordinates
		x1 = lpmap->vertices[v1].x;
		y1 = lpmap->vertices[v1].y;
		x2 = lpmap->vertices[v2].x;
		y2 = lpmap->vertices[v2].y;

		// If the given point is one of the polygon vertices,
		// then the point is always considered on the edge of the polygon
		if((x == x1 && y == y1) || (x == x2 && y == y2)) return TRUE;
		
		// Check if the linedef crosses the point's horizontal axis
		if(((y1 < y) && (y2 >= y)) || ((y2 < y) && (y1 >= y)))
		{
			// Check if the linedef is at the right of the point
			if(x <= (float)((x2 - x1) * (y - y1)) / (y2 - y1) + x1) inside++;
		}
	}
	
	// Return result
	return (inside & 1);
}
