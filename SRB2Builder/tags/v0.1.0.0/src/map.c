#include <windows.h>

#include "general.h"
#include "maptypes.h"
#include "openwads.h"
#include "editing.h"

#include "../wad/Wad.h"
#include "editing.h"

#include "CodeImp/ci_map.h"


/* AllocateMapStructure
 *   Allocates memory for map structures.
 *
 * Parameters:
 *   None
 *
 * Return value: MAP*
 *   Pointer to the memory allocated.
 *
 * Notes:
 *   The system raises an exception if we run out of memory, so we don't need to
 *   check the return values. Call DestroyMapStructure to free the memory.
 */
MAP *AllocateMapStructure(void)
{
	MAP *lpmap;

	lpmap = ProcHeapAlloc(sizeof(MAP));
	lpmap->sectors = ProcHeapAlloc(65536 * sizeof(MAPSECTOR));
	lpmap->linedefs = ProcHeapAlloc(65536 * sizeof(MAPLINEDEF));
	lpmap->vertices = ProcHeapAlloc(65536 * sizeof(MAPVERTEX));
	lpmap->sidedefs = ProcHeapAlloc(65536 * sizeof(MAPSIDEDEF));
	lpmap->things = ProcHeapAlloc(65536 * sizeof(MAPTHING));
	lpmap->iLinedefs = lpmap->iSectors = lpmap->iSidedefs = lpmap->iVertices = lpmap->iThings = 0;

	return lpmap;
}


/* DestroyMapStructure
 *   Frees memory associated with a map.
 *
 * Parameters:
 *   MAP*		Pointer to map structure to free.
 *
 * Return value:
 *   None
 *
 * Notes:
 *   Frees memory pointed to by the members, as well as the struct's own memory.
 */
void DestroyMapStructure(MAP *lpmap)
{
	ProcHeapFree(lpmap->sectors);
	ProcHeapFree(lpmap->linedefs);
	ProcHeapFree(lpmap->vertices);
	ProcHeapFree(lpmap->sidedefs);
	ProcHeapFree(lpmap->things);
	ProcHeapFree(lpmap);
}


/* MapLoad
 *   Loads a map from a wad into a map structure in memory.
 *
 * Parameters:
 *   MAP*			lpmap		Structure to load map into.
 *   int			iWad		ID of wad.
 *   LPCSTR			szLumpname	Name of map marker lump.
 *
 * Return value: int
 *   Non-negative on success; negative on error.
 */
int MapLoad(MAP *lpmap, int iWad, LPCSTR szLumpname)
{
	WAD *lpwad = GetWad(iWad);
	int i, cbBuf;
	BYTE *lpbBuffer;

	/* Bad wad ID. */
	if(!lpwad) return -1;

	/* Make sure map exists. */
	if(GetLumpLength(lpwad, szLumpname, NULL) < 0) return -2;

	/* Get sizes of each lump. Scaled to counts later. */
	lpmap->iLinedefs = GetLumpLength(lpwad, "LINEDEFS", szLumpname);
	lpmap->iSectors = GetLumpLength(lpwad, "SECTORS", szLumpname);
	lpmap->iThings = GetLumpLength(lpwad, "THINGS", szLumpname);
	lpmap->iSidedefs = GetLumpLength(lpwad, "SIDEDEFS", szLumpname);
	lpmap->iVertices = GetLumpLength(lpwad, "VERTEXES", szLumpname);

	/* Missing lumps? */
	if(lpmap->iLinedefs < 0 || lpmap->iSectors < 0 || lpmap->iSidedefs < 0 || lpmap->iVertices < 0 || lpmap->iThings < 0)
		return -3;

	/* Find maximum length for our buffer. */
	cbBuf = lpmap->iLinedefs;
	if(cbBuf < lpmap->iSectors)		cbBuf = lpmap->iSectors;
	if(cbBuf < lpmap->iThings)		cbBuf = lpmap->iThings;
	if(cbBuf < lpmap->iSidedefs)	cbBuf = lpmap->iSidedefs;
	if(cbBuf < lpmap->iVertices)	cbBuf = lpmap->iVertices;

	/* Allocate memory for buffer. */
	lpbBuffer = ProcHeapAlloc(cbBuf);


	/* Load linedefs. ------------------------------------------------------- */
	GetLump(lpwad, "LINEDEFS", szLumpname, lpbBuffer, lpmap->iLinedefs);
	
	/* Scale the byte count to record numbers. */
	lpmap->iLinedefs /= LINEDEFRECORDSIZE;

	/* We have data in our structure that doesn't come from the file. */
	ZeroMemory(lpmap->linedefs, lpmap->iLinedefs * sizeof(MAPLINEDEF));

	for(i=0; i < lpmap->iLinedefs; i++)
		CopyMemory(&lpmap->linedefs[i], lpbBuffer + i * LINEDEFRECORDSIZE, LINEDEFRECORDSIZE);


	/* Load sectors. -------------------------------------------------------- */
	GetLump(lpwad, "SECTORS", szLumpname, lpbBuffer, lpmap->iSectors);
	
	/* Scale the byte count to record numbers. */
	lpmap->iSectors /= SECTORRECORDSIZE;

	/* We have data in our structure that doesn't come from the file. */
	ZeroMemory(lpmap->sectors, lpmap->iSectors * sizeof(MAPSECTOR));

	for(i=0; i < lpmap->iSectors; i++)
		CopyMemory(&lpmap->sectors[i], lpbBuffer + i * SECTORRECORDSIZE, SECTORRECORDSIZE);


	/* Load things. --------------------------------------------------------- */
	GetLump(lpwad, "THINGS", szLumpname, lpbBuffer, lpmap->iThings);
	
	/* Scale the byte count to record numbers. */
	lpmap->iThings /= THINGRECORDSIZE;

	/* We have data in our structure that doesn't come from the file. */
	ZeroMemory(lpmap->things, lpmap->iThings * sizeof(MAPTHING));

	for(i=0; i < lpmap->iThings; i++)
		CopyMemory(&lpmap->things[i], lpbBuffer + i * THINGRECORDSIZE, THINGRECORDSIZE);


	/* Load sidedefs. ------------------------------------------------------- */
	GetLump(lpwad, "SIDEDEFS", szLumpname, lpbBuffer, lpmap->iSidedefs);
	
	/* Scale the byte count to record numbers. */
	lpmap->iSidedefs /= SIDEDEFRECORDSIZE;

	/* We have data in our structure that doesn't come from the file. */
	ZeroMemory(lpmap->sidedefs, lpmap->iSidedefs * sizeof(MAPSIDEDEF));

	for(i=0; i < lpmap->iSidedefs; i++)
		CopyMemory(&lpmap->sidedefs[i], lpbBuffer + i * SIDEDEFRECORDSIZE, SIDEDEFRECORDSIZE);


	/* Load vertices. ------------------------------------------------------- */
	GetLump(lpwad, "VERTEXES", szLumpname, lpbBuffer, lpmap->iVertices);
	
	/* Scale the byte count to record numbers. */
	lpmap->iVertices /= VERTEXRECORDSIZE;

	/* We have data in our structure that doesn't come from the file. */
	ZeroMemory(lpmap->vertices, lpmap->iVertices * sizeof(MAPVERTEX));

	/* Copy the data. */
	for(i=0; i < lpmap->iVertices; i++)
		CopyMemory(&lpmap->vertices[i], lpbBuffer + i * VERTEXRECORDSIZE, VERTEXRECORDSIZE);

	/* Finished with our buffer now. */
	ProcHeapFree(lpbBuffer);


	/* The map's loaded now, but we still have to set some fields in the
	 * structures that we use for our own purposes.
	 */

	/* Set all (used) sidedefs' linedef references. */
	for(i = 0; i < lpmap->iLinedefs; i++)
	{
		if(lpmap->linedefs[i].s1 >= 0) lpmap->sidedefs[lpmap->linedefs[i].s1].linedef = i;
		if(lpmap->linedefs[i].s2 >= 0) lpmap->sidedefs[lpmap->linedefs[i].s2].linedef = i;
	}

	/* TODO: Validate map. As it is, invalid maps cause crashes. */

	/* Nodebuilding adds vertices that we don't want. */
	RemoveUnusedVertices(lpmap, 0);
	DeleteZeroLengthLinedefs(lpmap);

	/* Success! */
	return 0;
}



/* MapLoadForPreview
 *   Loads only linedefs and vertices, and doesn't do any cleaning.
 *
 * Parameters:
 *   MAP*			lpmap		Structure to load map into.
 *   int			iWad		ID of wad.
 *   LPCSTR			szLumpname	Name of map marker lump.
 *
 * Return value: int
 *   Non-negative on success; negative on error.
 *
 * Remarks:
 *   Quicker than MapLoad, so useful for the preview in the map selection
 *   dialogue. All the other fields will be invalid, though.
 */
int MapLoadForPreview(MAP *lpmap, int iWad, LPCSTR szLumpname)
{
	WAD *lpwad = GetWad(iWad);
	int i, cbBuf;
	BYTE *lpbBuffer;

	/* Bad wad ID. */
	if(!lpwad) return -1;

	/* Make sure map exists. */
	if(GetLumpLength(lpwad, szLumpname, NULL) < 0) return -2;

	/* Get sizes of each lump. Scaled to counts later. */
	lpmap->iLinedefs = GetLumpLength(lpwad, "LINEDEFS", szLumpname);
	lpmap->iSectors = GetLumpLength(lpwad, "SECTORS", szLumpname);
	lpmap->iThings = GetLumpLength(lpwad, "THINGS", szLumpname);
	lpmap->iSidedefs = GetLumpLength(lpwad, "SIDEDEFS", szLumpname);
	lpmap->iVertices = GetLumpLength(lpwad, "VERTEXES", szLumpname);

	/* Missing lumps? */
	if(lpmap->iLinedefs < 0 || lpmap->iSectors < 0 || lpmap->iSidedefs < 0 || lpmap->iVertices < 0 || lpmap->iThings < 0)
		return -3;

	/* Find maximum length for our buffer. */
	cbBuf = lpmap->iLinedefs;
	if(cbBuf < lpmap->iVertices)	cbBuf = lpmap->iVertices;

	/* Allocate memory for buffer. */
	lpbBuffer = ProcHeapAlloc(cbBuf);


	/* Load linedefs. ------------------------------------------------------- */
	GetLump(lpwad, "LINEDEFS", szLumpname, lpbBuffer, lpmap->iLinedefs);
	
	/* Scale the byte count to record numbers. */
	lpmap->iLinedefs /= LINEDEFRECORDSIZE;

	for(i=0; i < lpmap->iLinedefs; i++)
		CopyMemory(&lpmap->linedefs[i], lpbBuffer + i * LINEDEFRECORDSIZE, LINEDEFRECORDSIZE);


	/* Load vertices. ------------------------------------------------------- */
	GetLump(lpwad, "VERTEXES", szLumpname, lpbBuffer, lpmap->iVertices);
	
	/* Scale the byte count to record numbers. */
	lpmap->iVertices /= VERTEXRECORDSIZE;

	/* Copy the data. */
	for(i=0; i < lpmap->iVertices; i++)
		CopyMemory(&lpmap->vertices[i], lpbBuffer + i * VERTEXRECORDSIZE, VERTEXRECORDSIZE);



	/* Finished with our buffer now. */
	ProcHeapFree(lpbBuffer);

	/* TODO: Validate map. As it is, invalid maps cause crashes. */

	/* Success! */
	return 0;
}




/* UpdateMap
 *   Copies a map to a wad in memory.
 *
 * Parameters:
 *   MAP*			lpmap		Structure to save.
 *   int			iWad		ID of wad.
 *   LPCSTR			szLumpname	Name of map marker lump.
 *
 * Return value: None.
 */
void UpdateMap(MAP *lpmap, int iWad, LPCSTR szLumpname)
{
	WAD *lpwad = GetWad(iWad);
	BYTE *lpbyBuffer = NULL;
	unsigned int cbBuffer;
	int i;

	/* No map with this name yet? */
	if(GetLumpLength(lpwad, szLumpname, NULL) < 0)
	{
		/* Create the marker lump at the end of the wad. */
		CreateLump(lpwad, szLumpname, NULL, NULL, TRUE);
	}

	/* Loop through each type of item, building the appropriate structures for
	 * the wad.
	 */


	/* Linedefs. **************************************************************/
	cbBuffer = lpmap->iLinedefs * LINEDEFRECORDSIZE;

	if(cbBuffer > 0)
	{
		lpbyBuffer = ProcHeapAlloc(cbBuffer);
		for(i = 0; i < lpmap->iLinedefs; i++)
			CopyMemory(lpbyBuffer + i * LINEDEFRECORDSIZE, &lpmap->linedefs[i], LINEDEFRECORDSIZE);
	}

	SetLump(lpwad, "LINEDEFS", szLumpname, lpbyBuffer, cbBuffer);
	
	if(cbBuffer > 0) ProcHeapFree(lpbyBuffer);


	/* Sectors. ***************************************************************/
	cbBuffer = lpmap->iSectors * SECTORRECORDSIZE;

	if(cbBuffer > 0)
	{
		lpbyBuffer = ProcHeapAlloc(cbBuffer);
		for(i = 0; i < lpmap->iSectors; i++)
			CopyMemory(lpbyBuffer + i * SECTORRECORDSIZE, &lpmap->sectors[i], SECTORRECORDSIZE);
	}

	SetLump(lpwad, "SECTORS", szLumpname, lpbyBuffer, cbBuffer);
	
	if(cbBuffer > 0) ProcHeapFree(lpbyBuffer);


	/* Things. ****************************************************************/
	cbBuffer = lpmap->iThings * THINGRECORDSIZE;

	if(cbBuffer > 0)
	{
		lpbyBuffer = ProcHeapAlloc(cbBuffer);
		for(i = 0; i < lpmap->iThings; i++)
			CopyMemory(lpbyBuffer + i * THINGRECORDSIZE, &lpmap->things[i], THINGRECORDSIZE);
	}
	
	SetLump(lpwad, "THINGS", szLumpname, lpbyBuffer, cbBuffer);
	
	if(cbBuffer > 0) ProcHeapFree(lpbyBuffer);


	/* Sidedefs. **************************************************************/
	cbBuffer = lpmap->iSidedefs * SIDEDEFRECORDSIZE;

	if(cbBuffer > 0)
	{
		lpbyBuffer = ProcHeapAlloc(cbBuffer);
		for(i = 0; i < lpmap->iSidedefs; i++)
			CopyMemory(lpbyBuffer + i * SIDEDEFRECORDSIZE, &lpmap->sidedefs[i], SIDEDEFRECORDSIZE);
	}

	SetLump(lpwad, "SIDEDEFS", szLumpname, lpbyBuffer, cbBuffer);
	
	if(cbBuffer > 0) ProcHeapFree(lpbyBuffer);


	/* Vertices. **************************************************************/
	cbBuffer = lpmap->iVertices * VERTEXRECORDSIZE;

	if(cbBuffer > 0)
	{
		lpbyBuffer = ProcHeapAlloc(cbBuffer);
		for(i = 0; i < lpmap->iVertices; i++)
			CopyMemory(lpbyBuffer + i * VERTEXRECORDSIZE, &lpmap->vertices[i], VERTEXRECORDSIZE);
	}

	SetLump(lpwad, "VERTEXES", szLumpname, lpbyBuffer, cbBuffer);
	
	if(cbBuffer > 0) ProcHeapFree(lpbyBuffer);
}
