#include <windows.h>
#include <commctrl.h>

#include "../general.h"
#include "../map.h"
#include "../../res/resource.h"
#include "../maptypes.h"
#include "../mapconfig.h"
#include "../openwads.h"
#include "../cdlgwrapper.h"
#include "../config.h"
#include "../options.h"

#include "mdiframe.h"
#include "mapwin.h"
#include "wadlist.h"
#include "infobar.h"
#include "mapselect.h"
#include "toolbar.h"
#include "gendlg.h"

#include "../../DockWnd/DockWnd.h"

/* Positions of Window sub-menus. */
#define NODOC_WINMENU_POS	0
#define MAP_WINMENU_POS		7

/* Classname. */
#define FRAMEWINCLASS TEXT("SRB2BuilderFrame")

/* IDs of child controls. */
enum {IDC_STATUSBAR};

/* Static Function prototypes. */
static LRESULT CALLBACK FrameWndProc(HWND hwnd, UINT uiMessage, WPARAM wParam, LPARAM lParam);
static BOOL CloseAllChildWindows(void);
static void CloseMainWindow(void);
static void CreateStatusBar(HWND hwndParent);


/* Globals. */
HWND g_hwndMain, g_hwndClient;			/* Window handles. */
HWND g_hwndStatusBar;

HMENU g_hmenuNodoc, g_hmenuMap;			/* Different main menus. */
HMENU g_hmenuNodocWin, g_hmenuMapWin;	/* Different Window sub-menus. */



/* CreateMainWindow
 *   Creates the MDI frame window.
 *
 * Parameters:
 *   int		iCmdShow			Constant determining initial window state.
 *
 * Return value: int
 *   Zero on success; nonzero on error.
 */

int CreateMainWindow(int iCmdShow)
{
	WNDCLASSEX wndclassex;
	const TCHAR szClassName[] = FRAMEWINCLASS;
	
	/* Set up and register the window class. */

	wndclassex.cbClsExtra = 0;
	wndclassex.cbSize = sizeof(wndclassex);
	wndclassex.cbWndExtra = 0;
	wndclassex.hbrBackground = (HBRUSH)(COLOR_APPWORKSPACE + 1);
	wndclassex.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclassex.hIcon = LoadIcon(g_hInstance, MAKEINTRESOURCE(IDI_MAIN));

	/* For the small icon, ask the system what size it should be. */
	wndclassex.hIconSm = LoadImage(g_hInstance, MAKEINTRESOURCE(IDI_MAIN), IMAGE_ICON, GetSystemMetrics(SM_CXSMICON), GetSystemMetrics(SM_CYSMICON), LR_DEFAULTCOLOR);

	wndclassex.hInstance = g_hInstance;
	wndclassex.lpfnWndProc = FrameWndProc;
	wndclassex.lpszClassName = szClassName;
	wndclassex.lpszMenuName = NULL;
	wndclassex.style = CS_VREDRAW | CS_HREDRAW;

	/* The only reason this should fail is if we try to register a Unicode class
	 * on 9x.
	 */
	if(!RegisterClassEx(&wndclassex)) return 1;

	/* Get menu handles. */
	g_hmenuMap = LoadMenu(g_hInstance, MAKEINTRESOURCE(IDM_MAP));
	g_hmenuNodoc = LoadMenu(g_hInstance, MAKEINTRESOURCE(IDM_NODOC));
	g_hmenuMapWin = GetSubMenu(g_hmenuMap, MAP_WINMENU_POS);
	g_hmenuNodocWin = GetSubMenu(g_hmenuNodoc, NODOC_WINMENU_POS);

	/* Create the window. */

	g_hwndMain = CreateWindowEx(	0,							/* Extended styles. */
									FRAMEWINCLASS,				/* Class. */
									g_szAppName,				/* Window name. */
																/* Window styles. */
									WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN,
																/* Position and size. */
									CW_USEDEFAULT, CW_USEDEFAULT, CW_USEDEFAULT,
										CW_USEDEFAULT,
									NULL,						/* Parent. */
																/* Menu. */
									g_hmenuNodoc,
									g_hInstance,				/* Instance handle. */
									NULL);						/* Extra data. */

	/* Just in case creating the window failed: */
	if(!g_hwndMain) return 2;


	/* Show the window as the system wants us to. */
	ShowWindow(g_hwndMain, iCmdShow);

	/* Success! */
	return 0;
}


/* FrameWndProc
 *   MDI Frame Window procedure. Called by the message loop.
 *
 * Parameters:
 *   HWND		hwnd		Window handle for message.
 *   UINT		uiMessage	ID for the message.
 *   WPARAM		wParam		Message-specific.
 *   LPARAM		lParam		Message-specific.
 *
 * Return value: LRESULT
 *   Message-specific.
 */

static LRESULT CALLBACK FrameWndProc(HWND hwnd, UINT uiMessage, WPARAM wParam, LPARAM lParam)
{
	CLIENTCREATESTRUCT clientcreate;
	HWND hwndChild;

	switch(uiMessage)
	{
	case WM_CREATE:
     
		/* Create the MDI client window -- that's the bit where the child windows
		 * appear.
		 */
		clientcreate.hWindowMenu  = g_hmenuNodocWin;
		clientcreate.idFirstChild = IDM_FIRST_CHILD;
          
		g_hwndClient = CreateWindowEx(WS_EX_CLIENTEDGE, TEXT ("MDICLIENT"), NULL,
									WS_CHILD | WS_CLIPCHILDREN | WS_VISIBLE,   
									0, 0, 0, 0, hwnd, (HMENU) 1, g_hInstance,
									(LPSTR)&clientcreate);

		/* Create status bar. */
		CreateStatusBar(hwnd);

		/* Create tool windows. */
		CreateWadListWindow(hwnd);
		CreateInfoBarWindow(hwnd);
		CreateToolbarWindow(hwnd);


		/* We can process WM_DROPFILES. */
		DragAcceptFiles(hwnd, TRUE);
		
		return 0;

	/* The docking library needs to process this message. */
	case WM_NCACTIVATE:
		{
			DOCKPARAMS	dockParams;

			dockParams.container = dockParams.hwnd = hwnd;
			dockParams.wParam = wParam;
			dockParams.lParam = lParam;
			return(DockingActivate(&dockParams));
		}

	/* The docking library needs to process this message. */
	case WM_ENABLE:
		{
			DOCKPARAMS	dockParams;

			dockParams.container = dockParams.hwnd = hwnd;
			dockParams.wParam = wParam;
			dockParams.lParam = lParam;
			return(DockingEnable(&dockParams));
		}

	case WM_SIZE:

		{
			int		cySBHeight;
			RECT	rcStatusBar;
			HDWP	hdwp;
			RECT	rect;

			// Do the default handling of this message
			//DefFrameProc(hwnd, MainWindow, msg, wParam, lParam);

			/* Resize the status bar. */
			SendMessage(g_hwndStatusBar, WM_SIZE, 0, 0);

			/* Get the status bar's height. */
			GetWindowRect(g_hwndStatusBar, &rcStatusBar);
			cySBHeight = rcStatusBar.bottom - rcStatusBar.top;

			/* Set the area where Docking Frame windows are allowed. */
			rect.left = rect.top = 0;
			rect.right = LOWORD(lParam);
			rect.bottom = HIWORD(lParam) - cySBHeight;

			/* Allocate enough space for all Docking Frames which are actually
			 * docked.
			 */
			hdwp = BeginDeferWindowPos(DockingCountFrames(hwnd, 1) + 1);

			/* Position the docked Docking Frame windows for this container
			 * window. rect will be modified to contain the "inner" client
			 * rectangle, where we can position an MDI client.
			 */
			DockingArrangeWindows(hwnd, hdwp, &rect);

			/* Here we resize our MDI client window so that it fits into the area
			 * described by "rect". Do not let it extend outside of this
			 * area or it (and the client windows inside of it) will be obscured
			 * by docked toolwindows (or vice versa).
			 */
			DeferWindowPos(hdwp, g_hwndClient, 0, rect.left, rect.top, rect.right - rect.left, rect.bottom - rect.top, SWP_NOACTIVATE|SWP_NOZORDER);

			EndDeferWindowPos(hdwp);

			/* Resize the MDI client window. */
			/*if(wParam != SIZE_MINIMIZED)
				MoveWindow(g_hwndClient, 0, 0, LOWORD(lParam), HIWORD(lParam) - cySBHeight, FALSE);*/

			/* DefFrameProc doesn't get this message, because we handle resizing the
			 * MDI client window ourselves.
			 */
			return 0;
		}


	/* Handle menu commands. */
	case WM_COMMAND:

		switch(LOWORD(wParam))
		{
		/* File menu. ------------------------------------------------------- */
		case IDM_FILE_NEW:
			{
				CONFIG *lpcfgMapContainer = GetDefaultMapConfigContainer();
				int iIWad = GetIWadForConfig(lpcfgMapContainer);

				/* TODO: Display the map properties dialogue to allow the user
				 * to choose the map config.
				 */
				ShowWindow(CreateMapWindow(AllocateMapStructure(), "MAP01", lpcfgMapContainer->lpcfgSubsection, NewWad(), iIWad), SW_SHOW);
			}
			return 0;

		case IDM_FILE_OPEN:
			{
				TCHAR szFileName[MAX_PATH];
				
				/* Make sure the user doesn't cancel. */
				if(CommDlgOpen(hwnd, szFileName, sizeof(szFileName)/sizeof(TCHAR), NULL, TEXT("Wad files (*.wad, *.srb)\0*.wad;*.srb\0All files (*.*)\0*.*\0"), TEXT("wad"), NULL, OFN_EXPLORER | OFN_FILEMUSTEXIST | OFN_HIDEREADONLY))
				{
					OpenWadForEditing(szFileName);
				}
			}

			return 0;

		case IDM_FILE_EXIT:
			/* Attempt to close the main window, and hence exit. The user'll get
			 * the chance to cancel if there are any unsaved documents.
			 */

#ifdef BUGTRAP
			/* Test SEH if BugTrap is being used. */
			if((GetKeyState(VK_CONTROL) & 0x8000) && (GetKeyState(VK_SHIFT) & 0x8000) &&
				IDYES == MessageBox(hwnd, TEXT("Crash?"), g_szAppName, MB_ICONEXCLAMATION | MB_YESNO))
			{
				int *lp = 0;
				*lp = 0;
			}
			else
#endif
				SendMessage(hwnd, WM_CLOSE, 0, 0);

			return 0;

		/* Window menu. ----------------------------------------------------- */
		case IDM_WINDOW_CLOSE:
			/* Close active child window. */
			hwndChild = (HWND)SendMessage(g_hwndClient, WM_MDIGETACTIVE, 0, 0);
			if(IsWindow(hwndChild)) SendMessage(hwndChild, WM_CLOSE, wParam, lParam);
			return 0;

		case IDM_WINDOW_CLOSEALL:
			/* Close all active child windows. The user may cancel. */
			CloseAllChildWindows();
			return 0;

		case IDM_WINDOW_TILEHORIZONTALLY:
			SendMessage(g_hwndClient, WM_MDITILE, MDITILE_HORIZONTAL, 0);
			return 0;

		case IDM_WINDOW_TILEVERTICALLY:
			SendMessage(g_hwndClient, WM_MDITILE, MDITILE_VERTICAL, 0);
			return 0;

		case IDM_WINDOW_CASCADE:
			SendMessage(g_hwndClient, WM_MDICASCADE, 0, 0);
			return 0;

		case IDM_WINDOW_ARRANGEICONS:
			SendMessage(g_hwndClient, WM_MDIICONARRANGE, 0, 0);
			return 0;

		/* Help menu. ------------------------------------------------------- */
		case IDM_HELP_ABOUT:
			DialogBox(g_hInstance, MAKEINTRESOURCE(IDD_ABOUT), hwnd, AboutDlgProc);
			return 0;

		default:
			/* Pass to active child window if we don't know what to do with it. */
			hwndChild = (HWND)SendMessage(g_hwndClient, WM_MDIGETACTIVE, 0, 0);
			if(IsWindow(hwndChild)) SendMessage(hwndChild, WM_COMMAND, wParam, lParam);

			/* Let DefFrameProc have a shot at it, too. */
			break;
		}
		
		/* Delegate to DefFrameProc. Necessary! */
		break;


	case WM_INITMENUPOPUP:
		/* Pass to active child window. */
		hwndChild = (HWND)SendMessage(g_hwndClient, WM_MDIGETACTIVE, 0, 0);
		if(IsWindow(hwndChild))
			return SendMessage(hwndChild, WM_INITMENUPOPUP, wParam, lParam);

		break;


	case WM_DROPFILES:

		{
			LPTSTR szFilename;
			int iBufLenTC, iNewSize, iNumFiles;
			int i;
		
			/* How many files? */
			iNumFiles = DragQueryFile((HDROP)wParam, 0xFFFFFFFF, NULL, 0);

			/* Allocate some room to begin with. We realloc if we need more. */
			iBufLenTC = 256;
			szFilename = ProcHeapAlloc(iBufLenTC * sizeof(TCHAR));

			/* Loop for each file. */
			for(i=0; i<iNumFiles; i++)
			{
				/* Get the size of buffer we need, and allocate more space if
				 * necessary.
				 */
				iNewSize = DragQueryFile((HANDLE)wParam, i, NULL, 0) + 1;
				if(iNewSize > iBufLenTC)
				{
					iBufLenTC = iNewSize;
					ProcHeapFree(szFilename);
					szFilename = ProcHeapAlloc(iBufLenTC * sizeof(TCHAR));
				}

				/* Get filename. */
				DragQueryFile((HANDLE)wParam, i, szFilename, iNewSize);

				/* Open the file. */
				OpenWadForEditing(szFilename);
			}

			ProcHeapFree(szFilename);

			DragFinish((HDROP)wParam);

			return 0;
		}


	/* User requested exit. */
	case WM_QUERYENDSESSION:
	case WM_CLOSE:
		/* Attempt to close all wads, closing all children in the process, and
		 * then close the main window if successful.
		 */
		if(CloseAllChildWindows())
		{
			/* Bow out. */
			CloseMainWindow();

			if(LOWORD(wParam) == WM_QUERYENDSESSION)
				return TRUE;
		}

		return 0;

	case WM_DESTROY:
		/* Child windows have been destroyed by the time we get here. */
		PostQuitMessage(0);
		break;
	}

	return DefFrameProc(hwnd, g_hwndClient, uiMessage, wParam, lParam);
}


/* CloseAllChildWindows
 *   Attempts to close all MDI child windows.
 *
 * Parameters:
 *   None
 *
 * Return value:
 *   BOOL		TRUE if successful; FALSE otherwise (user may cancel).
 *
 * Remarks:
 *   Doesn't actually close wads -- only map windows.
 */
static BOOL CloseAllChildWindows(void)
{
	/* Attempt to close all child windows. */
	EnumChildWindows(g_hwndClient, CloseEnumProc, 0);

	/* Are there any still remaining? If so, the user cancelled the operation.
	 */
	return !GetWindow(g_hwndClient, GW_CHILD);

}


/* CloseEnumProc
 *   Called once for each child window when attempting to close them all.
 *   Used by EnumChildWindows.
 *
 * Parameters:
 *   HWND	hwnd	Handle of child window.
 *	 LPARAM	lParam	Unused.
 *
 * Return value:
 *   BOOL		TRUE if enum should continue; FALSE otherwise (user may cancel).
 *
 * Remarks:
 *   Based closely on C. Petzold, "Programming Windows, 5ed.", MS Press.
 *   Also used by routines that close all windows belonging to a certain wad.
 */
BOOL CALLBACK CloseEnumProc(HWND hwnd, LPARAM lParam)
{
     UNREFERENCED_PARAMETER(lParam);
     if(GetWindow(hwnd, GW_OWNER))
          return TRUE;
     
     if(!SendMessage(hwnd, WM_CLOSE, 0, 0))
          return TRUE;
     
     SendMessage(GetParent(hwnd), WM_MDIDESTROY, (WPARAM)hwnd, 0);

     return TRUE;
}



/* CloseMainWindow
 *   Closes the MDI frame window immediately, cleaning up as it goes.
 *
 * Parameters:
 *   None
 *
 * Return value:
 *   None
 *
 * Remarks:
 *   This will eventually exit the app.
 */
static void CloseMainWindow(void)
{
	/* Close the tool windows. */
	DestroyInfoBarWindow();
	DestroyWadListWindow();

	/* Destroy the status bar. */
	DestroyWindow(g_hwndStatusBar);

	/* Destroy *unattached* menus. */
	DestroyMenu(g_hmenuMap);

	DestroyMainWindow();
}


/* DestroyMainWindow
 *   Destroys the MDI frame window immediately.
 *
 * Parameters:
 *   None
 *
 * Return value:
 *   None
 *
 * Remarks:
 *   This should only be called after cleanup, or when we're bombing.
 */
void DestroyMainWindow(void)
{
	if(g_hwndMain) DestroyWindow(g_hwndMain);
}


/* OpenWadForEditing
 *   Loads a wad if necessary, and sets up the UI.
 *
 * Parameters:
 *   LPCTRSTR		szFilename		Filename of wad.
 *
 * Return value: BOOL
 *   TRUE on success; FALSE on error.
 *
 * Remarks:
 *   Called when the user selects a file from a comdlg, with command line args
 *   at startup or on a drag-and-drop.
 */
BOOL OpenWadForEditing(LPCTSTR szFilename)
{
	/* This gets the wad's ID, opening it if necessary. */
	int iWad = LoadWad(szFilename);
	MAP *lpmap;
	char szLumpName[9];
	CONFIG *lpcfgMap;

	if(iWad < 0)
	{
		MessageBoxFromStringTable(g_hwndMain, IDS_BADWAD, MB_ICONERROR);
		return FALSE;
	}

	/* Let the user select which map to open from the wad. Make sure he doesn't
	 * cancel.
	*/
	if(SelectMap(iWad, szLumpName, &lpcfgMap))
	{
		int iIWad;

		/* Load the specified map. */
		lpmap = AllocateMapStructure();
		if(MapLoad(lpmap, iWad, szLumpName) < 0)
		{
			MessageBoxFromStringTable(g_hwndMain, IDS_BADMAP, MB_ICONERROR);
			return FALSE;
		}

		/* Load the IWAD. */
		iIWad = GetIWadForConfig(lpcfgMap);

		ShowWindow(CreateMapWindow(lpmap, szLumpName, lpcfgMap->lpcfgSubsection, iWad, iIWad), SW_SHOW);
	}

	/* Even if we didn't open a map because the user told us not to, we succeed.
	 * The wad was still opened, remember.
	 */
	return TRUE;
}


/* CreateStatusBar
 *   Creates the status bar for the main window.
 *
 * Parameters:
 *   HWND	hwndParent	Handle to main window.
 *
 * Return value: HWND
 *   Status bar's window handle.
 *
 * Remarks:
 *   We haven't set the global yet, which is why we pass the frame window's
 *   handle. The bar is initially in simple mode.
 */
static void CreateStatusBar(HWND hwndParent)
{
	g_hwndStatusBar = CreateStatusWindow(WS_CHILD | WS_VISIBLE | SBARS_SIZEGRIP, g_szAppName, hwndParent, IDC_STATUSBAR);

	/* Simple mode. */
	StatusBarNoWindow();
}


/* StatusBarNoWindow
 *   Sets the status bar to correspond to having no child windows.
 *
 * Parameters:
 *   None.
 *
 * Return value: None.
 */
void StatusBarNoWindow(void)
{
	/* Simple mode. */
	SendMessage(g_hwndStatusBar, SB_SIMPLE, TRUE, 0);
	SendMessage(g_hwndStatusBar, SB_SETTEXT, 255, (LPARAM)g_szAppName);
}
