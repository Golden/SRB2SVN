#ifndef __SRB2B_GENDLG__
#define __SRB2B_GENDLG__

#include <commctrl.h>

LRESULT CALLBACK TransDlgProc(HWND hwndDlg, UINT uiMsg, WPARAM wParam, LPARAM lParam);
int ListBoxSearchByItemData(HWND hwndListBox, int iItemData, BOOL bSelect);
BOOL BoundEditBox(HWND hwndEditBox, int iMin, int iMax, BOOL bPreserveEmpty, BOOL bPreserveRelative);
void Init3StateListView(HWND hwndListView);
void ListView3StateClick(LPNMLISTVIEW lpnmlistview);
void ListView3StateKeyDown(LPNMLVKEYDOWN lpnmlvkeydown);
void ListView3StateToggleItem(HWND hwndListView, int iIndex);
void ListView3StateSetItemState(HWND hwndListView, int iIndex, int iState);
int ListView3StateGetItemState(HWND hwndListView, int iIndex);
BOOL CALLBACK AboutDlgProc(HWND hwndDlg, UINT uiMsg, WPARAM wParam, LPARAM lParam);

enum ENUM_LV_3STATE
{
	LV3_UNCHECKED = 1,
	LV3_INDETERMINATE,
	LV3_CHECKED
};

#endif
