#ifndef __SRB2B_EDITDLG__
#define __SRB2B_EDITDLG__

#include <windows.h>

#include "../map.h"
#include "../selection.h"
#include "../config.h"

#include "mapwin.h"


/* Macros. */
#define STRING_RELATIVE(sz) ((*(sz) == '+' && (sz)[1] == '+') || (*(sz) == '-' && (sz)[1] == '-'))


/* Types. */
typedef struct _MAPPPDATA
{
	HWND				hwndMap;
	MAP					*lpmap;
	SELECTION			*lpselection;
	CONFIG				*lpcfgMap;
	TEXTURENAMELIST		*lptnlFlats, *lptnlTextures;
	HIMAGELIST			*lphimlCacheFlats, *lphimlCacheTextures;
	BOOL				bOKed;
} MAPPPDATA;

enum ENUM_MAPPPFLAGS
{
	MPPF_SECTOR = 1,
	MPPF_LINE = 2,
	MPPF_THING = 4,
	MPPF_VERTEX = 8
};


/* Prototypes. */
BOOL ShowMapObjectProperties(DWORD dwPageFlags, DWORD dwStartPage, LPCTSTR szCaption, MAPPPDATA *lpmapppdata);


#endif

