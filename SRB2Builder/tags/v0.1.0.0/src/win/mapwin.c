#include <windows.h>
#include <commctrl.h>
#include <stdio.h>
#include <tchar.h>
#include <math.h>

#include "../general.h"
#include "../../res/resource.h"
#include "../maptypes.h"
#include "../map.h"
#include "../editing.h"
#include "../config.h"
#include "../openwads.h"
#include "../options.h"
#include "../keyboard.h"
#include "../renderer.h"
#include "../selection.h"
#include "../mapconfig.h"
#include "../texture.h"
#include "../undo.h"
#include "../prefab.h"
#include "../cdlgwrapper.h"

#include "mdiframe.h"
#include "mapwin.h"
#include "infobar.h"
#include "editdlg.h"

#include "../CodeImp/ci_const.h"
#include "../CodeImp/ci_data_proto.h"
#include "../CodeImp/ci_map.h"

/* Macros. */
#define XMOUSE_WIN_TO_MAP(n, lpmwd) ((int)floor((n)/(lpmwd)->mapview.fZoom + lpmwd->mapview.xLeft + 0.5))
#define YMOUSE_WIN_TO_MAP(n, lpmwd) ((int)floor((-n)/(lpmwd)->mapview.fZoom + lpmwd->mapview.yTop + 0.5))

/* The wheel is treated like a keypress. */
#define MOUSE_SCROLL_UP 4008
#define MOUSE_SCROLL_DOWN 4009

/* In WadAuthor-selection-mode, there's a threshold for picking between linedefs
 * and sectors. It's measured in pixels. Similarly vertices.
 * Must be <= HOVER_THRESHOLD, or funny things start happening.
 */
#define LINEDEF_THRESHOLD	16
#define VERTEX_THRESHOLD	4

/* We set a maximum distance for hovering near objects. Even if that vertex
 * half-way across the map is in fact nearest, it feels weird when it lights up.
 * Measured in pixels.
 */
#define HOVER_THRESHOLD		16


/* Indices of caching events in window data. */
#define HCACHE_FLAT			0
#define HCACHE_TEX			1


/* Initial size of selection hash buckets. */
#define INIT_SELLIST_SIZE	10

/* ID constants. */
#define IDT_ANIM_TIMER 1

/* Menu positions. */
#define EDITMENUPOS 1
#define VIEWMENUPOS 2

/* Size of buffer for undo/redo strings. */
#define UNDOCAPTIONBUFSIZE 128

#define SAVECHANGES_BUFSIZE 1024

/* Custom messages. */
#define WM_UPDATEWAD	WM_APP
#define WM_UPDATETITLE	(WM_APP+1)

/* Number of characters (inc. terminator) for "Untitled" string. */
#define UNTITLED_BUFLEN 32


#ifdef _UNICODE
#define IsPseudoTexture IsPseudoTextureW
#define IsBlankTexture IsPseudoTextureW
#else
#define IsPseudoTexture IsPseudoTextureA
#define IsBlankTexture IsBlankTextureA
#endif


typedef struct _HIGHLIGHTOBJ
{
	ENUM_EDITMODE	emType;
	int			iIndex;
} HIGHLIGHTOBJ;


typedef struct _MAPWINDATA
{
	HWND				hwnd;		/* Just for convenience. */
	int					iWadID, iIWadID, iAdditionalWadID;
	WAD					*lpwadIWad;	/* For efficiency. */
	MAP					*lpmap;
	char				szLumpName[9];
	ENUM_EDITMODE		editmode, prevmode;
	ENUM_EDITSUBMODE	submode;
	HDC					hdc;
	HGLRC				hrc;
	HCURSOR				hCursor;
	short				xMouseLast, yMouseLast;
	MAPVIEW				mapview;
	BOOL				bWantRedraw;
	HIGHLIGHTOBJ		highlightobj;
	HIGHLIGHTOBJ		hobjDrag;
	SELECTION			selection, selectionAux;
	CONFIG				*lpcfgMap;
	TEXCACHE			tcFlatsHdr, tcTexturesHdr;
	TEXTURENAMELIST		*lptnlTextures, *lptnlFlats;
	RGBQUAD				rgbqPalette[CB_PLAYPAL / 3];
	DRAW_OPERATION		drawop;
	BYTE				bySnapFlags, bySnapToggleMask;
	short				xDragLast, yDragLast;
	BOOL				bDeselectAfterEdit;
	UNDOSTACK			*lpundostackUndo, *lpundostackRedo;
	BOOL				bMapChanged;
	LOOPLIST			*lplooplistRS;
	BOOL				bStitchMode;
	MAPTHING			thingLast;
	HIMAGELIST			himlCacheFlats, himlCacheTextures;
} MAPWINDATA;

/* Status bar panels. */
enum _ENUM_SBPANELS
{
	SBP_MODE = 0,
	SBP_SECTORS,
	SBP_LINEDEFS,
	SBP_SIDEDEFS,
	SBP_VERTICES,
	SBP_THINGS,
	SBP_GRID,
	SBP_ZOOM,
	SBP_COORD,
	SBP_MAPCONFIG,
	SBP_LAST
};

/* Corresponding flags. */
#define SBPF_MODE		(1 << SBP_MODE)
#define SBPF_SECTORS	(1 << SBP_SECTORS)
#define SBPF_LINEDEFS	(1 << SBP_LINEDEFS)
#define SBPF_SIDEDEFS	(1 << SBP_SIDEDEFS)
#define SBPF_VERTICES	(1 << SBP_VERTICES)
#define SBPF_THINGS		(1 << SBP_THINGS)
#define SBPF_GRID		(1 << SBP_GRID)
#define SBPF_ZOOM		(1 << SBP_ZOOM)
#define SBPF_COORD		(1 << SBP_COORD)
#define SBPF_MAPCONFIG	(1 << SBP_MAPCONFIG)



/* Static function declarations. */
static LRESULT CALLBACK MapWndProc(HWND hwnd, UINT uiMessage, WPARAM wParam, LPARAM lParam);
static void DestroyMapWindowData(HWND hwnd);
static BOOL MapKeyDown(MAPWINDATA *lpmwd, int iKeyCode);
static BOOL MapKeyUp(MAPWINDATA *lpmwd, int iKeyCode);
static void RedrawMapWindow(MAPWINDATA *lpmwd);
static void ChangeMode(MAPWINDATA *lpmwd, USHORT unModeMenuID);
static void StatusBarMapWindow(void);
static void UpdateStatusBar(MAPWINDATA *lpmwd, DWORD dwFlags);
static BOOL UpdateHighlight(MAPWINDATA *lpmwd, int xMouse, int yMouse);
static void GetNearestObject(MAPWINDATA *lpmwd, int xMap, int yMap, ENUM_EDITMODE mode, HIGHLIGHTOBJ *lphighlightobj);
static void ClearHighlight(MAPWINDATA *lpmwd);
static void SetHighlight(MAPWINDATA *lpmwd, HIGHLIGHTOBJ *lphightlightobj);
static LRESULT MouseMove(MAPWINDATA *lpmwd, WORD xMouse, WORD yMouse);
static LRESULT LButtonDown(MAPWINDATA *lpmwd, WORD xMouse, WORD yMouse, WORD wFlags);
static LRESULT LButtonUp(MAPWINDATA *lpmwd, WORD xMouse, WORD yMouse, WORD wFlags);
static LRESULT RButtonUp(MAPWINDATA *lpmwd, WORD xMouse, WORD yMouse, WORD wFlags);
static LRESULT RButtonDown(MAPWINDATA *lpmwd, WORD xMouse, WORD yMouse, WORD wFlags);
static BOOL ObjectSelected(MAPWINDATA *lpmwd, HIGHLIGHTOBJ *lphighlightobj);
static void AddObjectToSelection(MAPWINDATA *lpmwd, HIGHLIGHTOBJ *lphighlightobj);
static BOOL RemoveObjectFromSelection(MAPWINDATA *lpmwd, HIGHLIGHTOBJ *lphighlightobj);
static void UpdateMapWindowTitle(MAPWINDATA *lpmwd);
static void BuildSectorTexturePreviews(MAPWINDATA *lpmwd, SECTORDISPLAYINFO *lpsdi, DWORD dwDisplayFlags);
static void DestroySectorTexturePreviews(SECTORDISPLAYINFO *lpsdi, DWORD dwDisplayFlags);
static void BuildThingSpritePreview(MAPWINDATA *lpmwd, THINGDISPLAYINFO *lptdi, DWORD dwDisplayFlags);
static void DestroyThingSpritePreview(THINGDISPLAYINFO *lptdi, DWORD dwDisplayFlags);
static void BuildSidedefTexturePreviews(MAPWINDATA *lpmwd, LINEDEFDISPLAYINFO *lplddi, DWORD dwDisplayFlags);
static void DestroySidedefTexturePreviews(LINEDEFDISPLAYINFO *lplddi, DWORD dwDisplayFlags);

#ifdef _UNICODE
static BOOL IsPseudoTextureW(LPCWSTR szTexName);
static BOOL IsBlankTextureW(LPCWSTR szTexName);
#endif

static BOOL IsPseudoTextureA(LPCSTR szTexName);
static BOOL IsBlankTextureA(LPCSTR szTexName);
static void MapWinUpdateInfoBar(MAPWINDATA *lpmwd);
static void ShowLinesSelectionInfo(MAPWINDATA *lpmwd);
static void ShowSectorSelectionInfo(MAPWINDATA *lpmwd);
static void ShowThingsSelectionInfo(MAPWINDATA *lpmwd);
static void ShowVerticesSelectionInfo(MAPWINDATA *lpmwd);
static void ShowSelectionProperties(MAPWINDATA *lpmwd);
static void SelectVerticesFromLines(MAPWINDATA *lpmwd);
static void SelectLinesFromVertices(MAPWINDATA *lpmwd);
static void SelectSectorsFromLines(MAPWINDATA *lpmwd);
static void DeselectAll(MAPWINDATA *lpmwd);
static void DeselectAllBruteForce(MAPWINDATA *lpmwd);
static void DeselectAllLines(MAPWINDATA *lpmwd);
static void DeselectAllSectors(MAPWINDATA *lpmwd);
static void DeselectAllThings(MAPWINDATA *lpmwd);
static void DeselectAllVertices(MAPWINDATA *lpmwd);
static void DeselectLinesFromPartialSectors(MAPWINDATA *lpmwd);
static void DoInsertOperation(MAPWINDATA *lpmwd, short xMap, short yMap);
static __inline int SelectionCount(MAPWINDATA *lpmwd);
static void CentreAuxSelectionAtPoint(MAPWINDATA *lpmwd, short xCentre, short yCentre);
static void FindAuxSelectionBoundaries(MAPWINDATA *lpmwd, short *lpxMin, short *lpxMax, short *lpyMin, short *lpyMax);
static int InsertThing(MAPWINDATA *lpmwd, unsigned short xMap, unsigned short yMap);
static BOOL CALLBACK SendUpdateWadMessage(HWND hwnd, LPARAM lParam);
static BOOL CALLBACK SendUpdateTitleMessage(HWND hwnd, LPARAM lParam);
static void EndDragOperation(MAPWINDATA *lpmwd);
static void RebuildSelection(MAPWINDATA *lpmwd);
static void CreateUndo(MAPWINDATA *lpmwd, int iStringIndex);
static void ClearRedoStack(MAPWINDATA *lpmwd);
static void PerformUndo(MAPWINDATA *lpmwd);
static void PerformRedo(MAPWINDATA *lpmwd);
static void WithdrawUndo(MAPWINDATA *lpmwd);
static void WithdrawRedo(MAPWINDATA *lpmwd);
static void FlipSelectionHorizontally(MAPWINDATA *lpmwd);
static void FlipSelectionVertically(MAPWINDATA *lpmwd);
static void CreateAuxiliarySelection(MAPWINDATA *lpmwd);
static void CleanAuxiliarySelection(MAPWINDATA *lpmwd);
static void MapWindowSave(MAPWINDATA *lpmwd);
static void MapWindowSaveAs(MAPWINDATA *lpmwd);
static void SnapAuxVerticesToGrid(MAPWINDATA *lpmwd);
static void SnapSelectedThingsToGrid(MAPWINDATA *lpmwd);
static void StartPan(MAPWINDATA *lpmwd);
static void EndPan(MAPWINDATA *lpmwd);
static void DeleteSelection(MAPWINDATA *lpmwd);


/* RegisterMapWindowClass
 *   Register's the map window class.
 *
 * Parameters:
 *   None
 *
 * Return value: int
 *   Zero on success; nonzero on error.
 *
 * Notes:
 *   Only needs to be called at startup.
 */
int RegisterMapWindowClass(void)
{
	WNDCLASSEX wndclassex;
	const TCHAR szClassName[] = MAPWINCLASS;
	
	/* Set up and register the window class. */

	wndclassex.cbClsExtra = 0;
	wndclassex.cbSize = sizeof(wndclassex);
	wndclassex.cbWndExtra = sizeof(struct MAP*);
	wndclassex.hbrBackground = (HBRUSH)GetStockObject(WHITE_BRUSH);
	wndclassex.hCursor = NULL;
	wndclassex.hIcon = LoadIcon(NULL, IDI_APPLICATION);

	/* For the small icon, ask the system what size it should be. */
	wndclassex.hIconSm = LoadImage(NULL, IDI_APPLICATION, IMAGE_ICON, GetSystemMetrics(SM_CXSMICON), GetSystemMetrics(SM_CYSMICON), LR_DEFAULTCOLOR);

	wndclassex.hInstance = g_hInstance;
	wndclassex.lpfnWndProc = MapWndProc;
	wndclassex.lpszClassName = szClassName;
	wndclassex.lpszMenuName = NULL;
	wndclassex.style = CS_VREDRAW | CS_HREDRAW | CS_OWNDC;

	/* The only reason this should fail is if we try to register a Unicode class
	 * on 9x.
	 */
	if(!RegisterClassEx(&wndclassex)) return 1;
	return 0;
}


/* CreateMapWindow
 *   Creates a map editor window.
 *
 * Parameters:
 *   MAP*	lpmap		Pointer to map data for window.
 *   LPSTR	szLumpName	Name of map-lump.
 *   CONFIG *lpcfgMap	Map configuration.
 *   int	iWadID		ID of wad structure this window belongs to.
 *   int	iIWadID		ID of IWAD for the selected config.
 *
 * Return value: HWND
 *   Window handle, or NULL on error.
 */
HWND CreateMapWindow(MAP *lpmap, LPSTR szLumpName, CONFIG *lpcfgMap, int iWadID, int iIWadID)
{
	const TCHAR szClassName[] = MAPWINCLASS;
	MAPWINDATA *lpmwd;
	PIXELFORMATDESCRIPTOR pfd;

	/* Allocate storage for this window's data. */
	lpmwd = ProcHeapAlloc(sizeof(MAPWINDATA));

	lpmwd->iWadID = iWadID;
	lpmwd->iIWadID = iIWadID;
	lpmwd->iAdditionalWadID = -1;	/* TODO. */

	/* Map is unchanged initially. */
	lpmwd->bMapChanged = FALSE;
	
	/* We use this rather a lot, so cache it. */
	lpmwd->lpwadIWad = iIWadID >= 0 ? GetWad(iIWadID) : NULL;

	lpmwd->lpmap = lpmap;

	/* Get lumpname. Note that this is always ANSI. */
	lstrcpynA(lpmwd->szLumpName, szLumpName, 9);

	/* Store map config. */
	lpmwd->lpcfgMap = lpcfgMap;

	lpmwd->submode = ESM_NONE;
	lpmwd->editmode = EM_ANY;
	lpmwd->prevmode = EM_ANY;
	
	lpmwd->mapview.cxDrawSurface = lpmwd->mapview.cyDrawSurface = -1;
	
	/* We don't set the view position until we've created the window, since we
	 * need to know its dimensions.
	 */

	lpmwd->mapview.cxGrid = lpmwd->mapview.cyGrid = ConfigGetInteger(g_lpcfgMain, "defaultgrid");
	lpmwd->mapview.bShowGrid = ConfigGetInteger(g_lpcfgMain, "gridshow");
	lpmwd->mapview.xGridOffset = lpmwd->mapview.yGridOffset = 0;

	lpmwd->bStitchMode = ConfigGetInteger(g_lpcfgMain, "defaultstitch");

	/* No object highlighted initially. */
	lpmwd->highlightobj.emType = EM_MOVE;

	lpmwd->hCursor = LoadCursor(NULL, IDC_ARROW);
	
	/* Mouse is assumed to be outside window. */
	lpmwd->xMouseLast = lpmwd->yMouseLast = -1;

	/* Snap to grid. */
	lpmwd->bySnapFlags = SF_RECTANGLE;
	lpmwd->bySnapToggleMask = 0;

	/* Set default values for last thing. */
	lpmwd->thingLast.angle = 0;
	lpmwd->thingLast.flag = ConfigGetInteger(lpcfgMap, "defaultthingflags");
	lpmwd->thingLast.thing = ConfigGetInteger(lpcfgMap, "defaultthing");


	/* Load palette. */
	if(lpmwd->iIWadID >= 0 && GetLumpLength(lpmwd->lpwadIWad, "PLAYPAL", NULL) >= CB_PLAYPAL)
	{
		BYTE	lpbyPlaypal[CB_PLAYPAL];
		int		i;

		/* Get the palette from the IWAD. */
		GetLump(lpmwd->lpwadIWad, "PLAYPAL", NULL, lpbyPlaypal, CB_PLAYPAL);

		/* Convert it to an array of RGBQUADs for the API. */
		for(i = 0; i < CB_PLAYPAL / 3; i++)
		{
			lpmwd->rgbqPalette[i].rgbRed = lpbyPlaypal[3*i];
			lpmwd->rgbqPalette[i].rgbGreen = lpbyPlaypal[3*i + 1];
			lpmwd->rgbqPalette[i].rgbBlue = lpbyPlaypal[3*i + 2];
		}
	}
	else
	{
		/* No palette? Zero it. */
		FillMemory(lpmwd->rgbqPalette, sizeof(lpmwd->rgbqPalette), 0);
	}

	/* Initialise all things' colour flags. */
	SetAllThingPropertiesFromType(lpmap, ConfigGetSubsection(lpcfgMap, FLAT_THING_SECTION));

	/* Get a list of all texture names. */
	lpmwd->lptnlFlats = CreateTextureNameList();
	lpmwd->lptnlTextures = CreateTextureNameList();
	if(lpmwd->iWadID >= 0)
	{
		AddAllTextureNamesToList(lpmwd->lptnlFlats, lpmwd->lpwadIWad, TF_FLAT);
		AddAllTextureNamesToList(lpmwd->lptnlTextures, lpmwd->lpwadIWad, TF_TEXTURE);
	}
	if(lpmwd->iAdditionalWadID >= 0)
	{
		WAD *lpwadAdd = GetWad(lpmwd->iAdditionalWadID);

		AddAllTextureNamesToList(lpmwd->lptnlFlats, lpwadAdd, TF_FLAT);
		AddAllTextureNamesToList(lpmwd->lptnlTextures, lpwadAdd, TF_TEXTURE);
	}
	/* TODO: Also add textures from file itself? */

	SortTextureNameList(lpmwd->lptnlFlats);
	SortTextureNameList(lpmwd->lptnlTextures);

	/* Texture caching: both the image list and our own bitmaps. */
	/* TODO: Use the image list for both? */
	lpmwd->tcFlatsHdr.lptcNext = NULL;
	lpmwd->tcTexturesHdr.lptcNext = NULL;
	lpmwd->himlCacheFlats = NULL;
	lpmwd->himlCacheTextures = NULL;

	lpmwd->hwnd = CreateMDIWindow(szClassName, TEXT("Map"), 0, CW_USEDEFAULT, CW_USEDEFAULT, CW_USEDEFAULT, CW_USEDEFAULT, g_hwndClient, g_hInstance, (LPARAM)lpmwd);
	UpdateMapWindowTitle(lpmwd);

	/* Now that we've got the window, we can get its DC. */
	lpmwd->hdc = GetDC(lpmwd->hwnd);

	/* Set the view now that we know the dimensions. */
	ResetMapView(lpmap, &lpmwd->mapview);

	/* Initialise selection structures. */
	lpmwd->selection.lpsellistLinedefs = AllocateSelectionList(INIT_SELLIST_SIZE);
	lpmwd->selection.lpsellistVertices = AllocateSelectionList(INIT_SELLIST_SIZE);
	lpmwd->selection.lpsellistSectors = AllocateSelectionList(INIT_SELLIST_SIZE);
	lpmwd->selection.lpsellistThings = AllocateSelectionList(INIT_SELLIST_SIZE);

	/* No undo structures to start with. */
	lpmwd->lpundostackUndo = NULL;
	lpmwd->lpundostackRedo = NULL;

	/* Set the pixel format. */
	ZeroMemory(&pfd, sizeof(pfd));
	pfd.nSize = sizeof(pfd);
    pfd.nVersion = 1;
    pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
    pfd.iPixelType = PFD_TYPE_RGBA;
    pfd.cColorBits = 32;
    pfd.cDepthBits = 16;
	SetPixelFormat(lpmwd->hdc, ChoosePixelFormat(lpmwd->hdc, &pfd), &pfd);

	/* Get a resource context and make it current. */
	lpmwd->hrc = wglCreateContext(lpmwd->hdc);
	wglMakeCurrent(lpmwd->hdc, lpmwd->hrc);
	InitGL();
	lpmwd->mapview.uiThingTex = LoadThingCircleTexture();
	ReSizeGLScene(lpmwd->mapview.cxDrawSurface, lpmwd->mapview.cyDrawSurface);

	/* Need to be set up in order to do this. */
	ChangeMode(lpmwd, IDM_VIEW_MODE_ANY);	/* Sets menus etc. */

	return lpmwd->hwnd;
}



/* MapWndProc
 *   Map editor child window procedure. Called by the message loop.
 *
 * Parameters:
 *   HWND		hwnd		Window handle for message.
 *   UINT		uiMessage	ID for the message.
 *   WPARAM		wParam		Message-specific.
 *   LPARAM		lParam		Message-specific.
 *
 * Return value: LRESULT
 *   Message-specific.
 */

static LRESULT CALLBACK MapWndProc(HWND hwnd, UINT uiMessage, WPARAM wParam, LPARAM lParam)
{
	MAPWINDATA *lpmwd;

	switch(uiMessage)
	{
	case WM_CREATE:
		{
			/* Retrieve the window-specific structure, and store it. */
			lpmwd = (MAPWINDATA*)((MDICREATESTRUCT*)((CREATESTRUCT*)lParam)->lpCreateParams)->lParam;
			SetWindowLong(hwnd, GWL_USERDATA, (LONG)lpmwd);

			/* Create an animation timer for this window. */
			SetTimer(hwnd, IDT_ANIM_TIMER, 10, NULL);
		}

		return 0;

	case WM_MDIACTIVATE:
		/* Are we now active? */
		if(lParam == (LPARAM)hwnd)
		{
			lpmwd = (MAPWINDATA*)GetWindowLong(hwnd, GWL_USERDATA);

			/* Set our menu and status bar.. */
			SendMessage(g_hwndClient, WM_MDISETMENU, (WPARAM)g_hmenuMap, (LPARAM)g_hmenuMapWin);
			StatusBarMapWindow();
			UpdateStatusBar(lpmwd, 0xFFFFFFFF);		/* Update all cells. */
		}
		else	/* We're not active any more. */
		{
			/* Set the 'no active window' menu and status bar. */
			SendMessage(g_hwndClient, WM_MDISETMENU, (WPARAM)g_hmenuNodoc, (LPARAM)g_hmenuNodocWin);
			StatusBarNoWindow();
		}

		DrawMenuBar(g_hwndMain);

		return 0;

	case WM_UPDATEWAD:
		/* Update the wad structure with the changes made to the map. Necessary
		 * immediately prior to saving, and may be necessary if another map
		 * window triggers a Save As, which is why it's done in response to a
		 * window message.
		 */
		lpmwd = (MAPWINDATA*)GetWindowLong(hwnd, GWL_USERDATA);
		UpdateMap(lpmwd->lpmap, lpmwd->iWadID, lpmwd->szLumpName);
		return 0;

	case WM_UPDATETITLE:
		/* Update the windows title with lumpname and filename. Necessary
		 * immediately after a Save As, and is necessary when another map window
		 * triggers such, which is why it's done in response to a window
		 * message.
		 */
		lpmwd = (MAPWINDATA*)GetWindowLong(hwnd, GWL_USERDATA);
		UpdateMapWindowTitle(lpmwd);
		return 0;


	case WM_INITMENUPOPUP:
		/* Check items as necessary before the menu's displayed. */

		/* Make sure it's not the system menu. */
		if(!HIWORD(lParam))
		{
			lpmwd = (MAPWINDATA*)GetWindowLong(hwnd, GWL_USERDATA);

			/* Switch on menu position. We subtract one when maximised due to
			 * the MDI system menu. TODO: Replace this with an ID check. We can
			 * assign IDs to pop-up menus at runtime, which'll allow us to
			 * process sub-menus, e.g. gradients.
			 */
			switch(LOWORD(lParam) - (IsZoomed(lpmwd->hwnd) ? 1 : 0))
			{
			case EDITMENUPOS:
				{
					MENUITEMINFO mii;
					TCHAR szCaption[UNDOCAPTIONBUFSIZE];
					int iPrefixLength;

					/* Check some options as appropriate. */
					CheckMenuItem((HMENU)wParam, IDM_EDIT_SNAPTOGRID, lpmwd->bySnapFlags & SF_RECTANGLE ? MF_CHECKED : MF_UNCHECKED);
					CheckMenuItem((HMENU)wParam, IDM_EDIT_AUTOSTITCH, lpmwd->bStitchMode & SF_RECTANGLE ? MF_CHECKED : MF_UNCHECKED);

					/* Set undo/redo strings. */

					mii.cbSize = sizeof(mii);
					mii.fMask = MIIM_TYPE;
					mii.fType = MFT_STRING;
					mii.dwTypeData = szCaption;

					/* Concatenate undo prefix and description, and set string. */
					LoadString(g_hInstance, IDS_UNDOPREFIX, szCaption, sizeof(szCaption) / sizeof(TCHAR));
					if(lpmwd->lpundostackUndo)
					{
						iPrefixLength = lstrlen(szCaption);
						LoadString(g_hInstance, GetUndoTopStringIndex(lpmwd->lpundostackUndo), &szCaption[iPrefixLength], sizeof(szCaption) / sizeof(TCHAR) - iPrefixLength);
					}
					SetMenuItemInfo((HMENU)wParam, IDM_EDIT_UNDO, FALSE, &mii);

					/* Concatenate redo prefix and description, and set string. */
					LoadString(g_hInstance, IDS_REDOPREFIX, szCaption, sizeof(szCaption) / sizeof(TCHAR));
					if(lpmwd->lpundostackRedo)
					{
						iPrefixLength = lstrlen(szCaption);
						LoadString(g_hInstance, GetUndoTopStringIndex(lpmwd->lpundostackRedo), &szCaption[iPrefixLength], sizeof(szCaption) / sizeof(TCHAR) - iPrefixLength);
					}
					SetMenuItemInfo((HMENU)wParam, IDM_EDIT_REDO, FALSE, &mii);


					/* Enable/disable undo and redo. */
					EnableMenuItem((HMENU)wParam, IDM_EDIT_UNDO, lpmwd->lpundostackUndo ? MF_ENABLED : MF_GRAYED);
					EnableMenuItem((HMENU)wParam, IDM_EDIT_REDO, lpmwd->lpundostackRedo ? MF_ENABLED : MF_GRAYED);
				}

				return 0;
			}
		}

		/* Didn't do anything. Fall through. */
		break;

	/* Menu commands. */
	case WM_COMMAND:

		lpmwd = (MAPWINDATA*)GetWindowLong(hwnd, GWL_USERDATA);

		switch(LOWORD(wParam))
		{
		/* File menu. *********************************************************/

		/* Save. */
		case IDM_FILE_SAVE:
			MapWindowSave(lpmwd);
			return 0;

		/* Save As. */
		case IDM_FILE_SAVEAS:
			MapWindowSaveAs(lpmwd);
			return 0;


		/* Edit menu. *********************************************************/

		/* Copy. */
		case IDM_EDIT_CUT:
			if(SelectionCount(lpmwd) > 0)
			{
				PREFAB *lpprefab;

				lpprefab = CreatePrefabFromSelection(lpmwd->lpmap);
				if(CopyPrefabToClipboard(lpprefab) != 0)
					MessageBoxFromStringTable(g_hwndMain, IDS_ERROR_CLIPBOARD, MB_ICONEXCLAMATION);

				FreePrefab(lpprefab);

				CreateUndo(lpmwd, IDS_UNDO_CUT);
				ClearRedoStack(lpmwd);
				DeleteSelection(lpmwd);

				lpmwd->bMapChanged = TRUE;
				lpmwd->bWantRedraw = TRUE;
				MapWinUpdateInfoBar(lpmwd);
				UpdateStatusBar(lpmwd, SBPF_LINEDEFS | SBPF_SIDEDEFS | SBPF_VERTICES | SBPF_SECTORS | SBPF_THINGS);
			}

			return 0;

		/* Copy. */
		case IDM_EDIT_COPY:
			if(SelectionCount(lpmwd) > 0)
			{
				PREFAB *lpprefab;

				lpprefab = CreatePrefabFromSelection(lpmwd->lpmap);
				if(CopyPrefabToClipboard(lpprefab) != 0)
					MessageBoxFromStringTable(g_hwndMain, IDS_ERROR_CLIPBOARD, MB_ICONEXCLAMATION);

				FreePrefab(lpprefab);
			}

			return 0;

		/* Paste. */
		case IDM_EDIT_PASTE:
			/* If the user presses the accelerator, he circumvents the disabling
			 * of menu items and toolbar buttons, so we need to check the
			 * clipboard again.
			 */
			if(ClipboardContainsPrefab())
			{
				PREFAB *lpprefab;

				DeselectAll(lpmwd);

				if((lpprefab = PastePrefabFromClipboard()))
				{
					/* Don't clear the redo stack yet. */
					CreateUndo(lpmwd, IDS_UNDO_PASTE);

					InsertPrefab(lpmwd->lpmap, lpprefab);
					RebuildSelection(lpmwd);
					CreateAuxiliarySelection(lpmwd);
					
					/* Find loops. We don't do the extra check of any linedefs we
					 * loop, though, since we don't really exist yet and so aren't
					 * affecting any other lines.
					 */
					lpmwd->lplooplistRS = LabelRSLinesLoops(lpmwd->lpmap);

					/* TODO: What if the mouse is outside the map? */
					lpmwd->xDragLast = XMOUSE_WIN_TO_MAP(lpmwd->xMouseLast, lpmwd);
					lpmwd->yDragLast = YMOUSE_WIN_TO_MAP(lpmwd->yMouseLast, lpmwd);

					/* Centre the prefab on the mouse position. */
					CentreAuxSelectionAtPoint(lpmwd, lpmwd->xDragLast, lpmwd->yDragLast);

					lpmwd->submode = ESM_PASTING;
					lpmwd->bWantRedraw = TRUE;

					FreePrefab(lpprefab);
				}
				else
				{
					/* Paste failed. */
					MessageBoxFromStringTable(g_hwndMain, IDS_ERROR_CLIPBOARD, MB_ICONEXCLAMATION);
				}
			}

			return 0;

		/* Delete. */
		case IDM_EDIT_DELETE:

			CreateUndo(lpmwd, IDS_UNDO_DELETE);
			ClearRedoStack(lpmwd);
			DeleteSelection(lpmwd);

			lpmwd->bMapChanged = TRUE;
			lpmwd->bWantRedraw = TRUE;
			MapWinUpdateInfoBar(lpmwd);
			UpdateStatusBar(lpmwd, SBPF_LINEDEFS | SBPF_SIDEDEFS | SBPF_VERTICES | SBPF_SECTORS | SBPF_THINGS);

			return 0;

		/* Undo. */
		case IDM_EDIT_UNDO:

			/* Make sure we've got something to undo. */
			if(lpmwd->lpundostackUndo)
			{
				PerformUndo(lpmwd);
				lpmwd->bWantRedraw = TRUE;

				/* The selection is now garbage, both because of nonsense in the
				 * stored selection fields and that numbering might have
				 * changed. Same goes for highlighting.
				 */
				DeselectAllBruteForce(lpmwd);
				ClearHighlight(lpmwd);

				/* Update highlight and info bar. */
				UpdateHighlight(lpmwd, lpmwd->xMouseLast, lpmwd->yMouseLast);
				MapWinUpdateInfoBar(lpmwd);

				/* Update all cells. */
				UpdateStatusBar(lpmwd, 0xFFFFFFFF);
			}

			return 0;

		/* Redo. */
		case IDM_EDIT_REDO:

			/* Make sure we've got something to redo. */
			if(lpmwd->lpundostackRedo)
			{
				PerformRedo(lpmwd);
				lpmwd->bWantRedraw = TRUE;

				/* The selection is now garbage, both because of nonsense in the
				 * stored selection fields and that numbering might have
				 * changed. Same goes for highlighting.
				 */
				DeselectAllBruteForce(lpmwd);
				ClearHighlight(lpmwd);

				/* Update highlight and info bar. */
				UpdateHighlight(lpmwd, lpmwd->xMouseLast, lpmwd->yMouseLast);
				MapWinUpdateInfoBar(lpmwd);

				/* Update all cells. */
				UpdateStatusBar(lpmwd, 0xFFFFFFFF);
			}

			return 0;

		/* Snap to grid. */
		case IDM_EDIT_SNAPTOGRID:

			lpmwd->bySnapFlags ^= SF_RECTANGLE;

			/* If we're drawing or dragging, this could alter the position of
			 * the object under the mouse. Redraw.
			 */
			lpmwd->bWantRedraw = TRUE;

			return 0;

		/* Autostitch. */
		case IDM_EDIT_AUTOSTITCH:

			lpmwd->bStitchMode = !lpmwd->bStitchMode;

			/* TODO: If you start visibly snapping to vertices while drawing,
			 * set the redraw flag.
			 */

			return 0;

		/* Flip horizontally. */
		case IDM_EDIT_TRANSFORMSELECTION_FLIPHORIZONTALLY:

			/* If we're pasting or dragging, we already have an aux. selection.
			 */
			if(lpmwd->editmode != ESM_PASTING && lpmwd->editmode != ESM_DRAGGING)
				CreateAuxiliarySelection(lpmwd);

			if(lpmwd->selectionAux.lpsellistVertices->iDataCount + lpmwd->selectionAux.lpsellistThings->iDataCount >= 1)
			{
				CreateUndo(lpmwd, IDS_UNDO_FLIPHORIZ);
				ClearRedoStack(lpmwd);

				FlipSelectionHorizontally(lpmwd);

				lpmwd->bMapChanged = TRUE;
				lpmwd->bWantRedraw = TRUE;
			}

			if(lpmwd->editmode != ESM_PASTING && lpmwd->editmode != ESM_DRAGGING)
				CleanAuxiliarySelection(lpmwd);

			return 0;

		/* Flip vertically. */
		case IDM_EDIT_TRANSFORMSELECTION_FLIPVERTICALLY:

			/* If we're pasting or dragging, we already have an aux. selection.
			 */
			if(lpmwd->editmode != ESM_PASTING && lpmwd->editmode != ESM_DRAGGING)
				CreateAuxiliarySelection(lpmwd);

			if(lpmwd->selectionAux.lpsellistVertices->iDataCount + lpmwd->selectionAux.lpsellistThings->iDataCount >= 1)
			{
				CreateUndo(lpmwd, IDS_UNDO_FLIPVERT);
				ClearRedoStack(lpmwd);

				FlipSelectionVertically(lpmwd);

				lpmwd->bMapChanged = TRUE;
				lpmwd->bWantRedraw = TRUE;
			}

			if(lpmwd->editmode != ESM_PASTING && lpmwd->editmode != ESM_DRAGGING)
				CleanAuxiliarySelection(lpmwd);

			return 0;



		/* View menu. *********************************************************/

		/* Mode change (other than 3D). */
		case IDM_VIEW_MODE_MOVE:
		case IDM_VIEW_MODE_ANY:
		case IDM_VIEW_MODE_LINES:
		case IDM_VIEW_MODE_SECTORS:
		case IDM_VIEW_MODE_VERTICES:
		case IDM_VIEW_MODE_THINGS:
			ChangeMode(lpmwd, LOWORD(wParam));
			UpdateStatusBar(lpmwd, SBPF_MODE);
			lpmwd->bWantRedraw = TRUE;
			return 0;

		case IDM_VIEW_3D:
			return 0;

		case IDM_VIEW_CENTRE:
			ResetMapView(lpmwd->lpmap, &lpmwd->mapview);
			lpmwd->bWantRedraw = TRUE;
			return 0;

		/* Lines menu. ********************************************************/
		case IDM_LINES_FLIPLINEDEFS:
			
			CreateUndo(lpmwd, IDS_UNDO_FLIPLINEDEFS);
			ClearRedoStack(lpmwd);
			lpmwd->bMapChanged = TRUE;

			FlipSelectedLinedefs(lpmwd->lpmap, lpmwd->selection.lpsellistLinedefs);
			lpmwd->bWantRedraw = TRUE;

			return 0;

		case IDM_LINES_FLIPSIDEDEFS:

			CreateUndo(lpmwd, IDS_UNDO_FLIPSIDEDEFS);
			ClearRedoStack(lpmwd);
			lpmwd->bMapChanged = TRUE;

			ExchangeSelectedSidedefs(lpmwd->lpmap, lpmwd->selection.lpsellistLinedefs);
			lpmwd->bWantRedraw = TRUE;
			return 0;

		case IDM_LINES_SPLITLINEDEFS:

			CreateUndo(lpmwd, IDS_UNDO_SPLITLINEDEFS);
			ClearRedoStack(lpmwd);
			lpmwd->bMapChanged = TRUE;

			BisectSelectedLinedefs(lpmwd->lpmap, lpmwd->selection.lpsellistLinedefs);
			/* TODO: Move the selection rebuilding from the fn to here. */
			lpmwd->bWantRedraw = TRUE;

			MapWinUpdateInfoBar(lpmwd);
			UpdateStatusBar(lpmwd, SBPF_LINEDEFS | SBPF_SIDEDEFS | SBPF_VERTICES);

			return 0;

		/* Sectors menu. ******************************************************/
		case IDM_SECTORS_JOIN:

			CreateUndo(lpmwd, IDS_UNDO_JOINSECTORS);
			ClearRedoStack(lpmwd);
			lpmwd->bMapChanged = TRUE;

			JoinSelectedSectors(lpmwd->lpmap, lpmwd->selection.lpsellistSectors, FALSE);
			RebuildSelection(lpmwd);
			lpmwd->bWantRedraw = TRUE;

			MapWinUpdateInfoBar(lpmwd);
			UpdateStatusBar(lpmwd, SBPF_SECTORS);

			return 0;

		case IDM_SECTORS_MERGE:

			CreateUndo(lpmwd, IDS_UNDO_MERGESECTORS);
			ClearRedoStack(lpmwd);
			lpmwd->bMapChanged = TRUE;

			JoinSelectedSectors(lpmwd->lpmap, lpmwd->selection.lpsellistSectors, TRUE);
			RebuildSelection(lpmwd);
			lpmwd->bWantRedraw = TRUE;

			MapWinUpdateInfoBar(lpmwd);
			UpdateStatusBar(lpmwd, SBPF_LINEDEFS | SBPF_SIDEDEFS | SBPF_VERTICES | SBPF_SECTORS);

			return 0;

		case IDM_SECTORS_FLOORS_INCREASE:

			CreateUndo(lpmwd, IDS_UNDO_INCREASEFLOOR);
			ClearRedoStack(lpmwd);
			lpmwd->bMapChanged = TRUE;

			ApplyRelativeFloorHeightSelection(lpmwd->lpmap, lpmwd->selection.lpsellistSectors, 8);

			/* Might change line colours. */
			lpmwd->bWantRedraw = TRUE;

			MapWinUpdateInfoBar(lpmwd);

			return 0;

		case IDM_SECTORS_FLOORS_DECREASE:

			CreateUndo(lpmwd, IDS_UNDO_DECREASEFLOOR);
			ClearRedoStack(lpmwd);
			lpmwd->bMapChanged = TRUE;

			ApplyRelativeFloorHeightSelection(lpmwd->lpmap, lpmwd->selection.lpsellistSectors, -8);

			/* Might change line colours. */
			lpmwd->bWantRedraw = TRUE;

			MapWinUpdateInfoBar(lpmwd);

			return 0;

		case IDM_SECTORS_FLOORS_GRADIENT:

			CreateUndo(lpmwd, IDS_UNDO_GRADIENTFLOOR);
			ClearRedoStack(lpmwd);
			lpmwd->bMapChanged = TRUE;

			GradientSelectedFloors(lpmwd->lpmap, lpmwd->selection.lpsellistSectors);

			/* Might change line colours. */
			lpmwd->bWantRedraw = TRUE;

			return 0;

		case IDM_SECTORS_CEILINGS_INCREASE:

			CreateUndo(lpmwd, IDS_UNDO_INCREASECEIL);
			ClearRedoStack(lpmwd);
			lpmwd->bMapChanged = TRUE;

			ApplyRelativeCeilingHeightSelection(lpmwd->lpmap, lpmwd->selection.lpsellistSectors, 8);

			/* Might change line colours. */
			lpmwd->bWantRedraw = TRUE;

			MapWinUpdateInfoBar(lpmwd);

			return 0;

		case IDM_SECTORS_CEILINGS_DECREASE:

			CreateUndo(lpmwd, IDS_UNDO_DECREASECEIL);
			ClearRedoStack(lpmwd);
			lpmwd->bMapChanged = TRUE;

			ApplyRelativeCeilingHeightSelection(lpmwd->lpmap, lpmwd->selection.lpsellistSectors, -8);

			/* Might change line colours. */
			lpmwd->bWantRedraw = TRUE;

			MapWinUpdateInfoBar(lpmwd);

			return 0;

		case IDM_SECTORS_CEILINGS_GRADIENT:

			CreateUndo(lpmwd, IDS_UNDO_GRADIENTCEIL);
			ClearRedoStack(lpmwd);
			lpmwd->bMapChanged = TRUE;

			GradientSelectedCeilings(lpmwd->lpmap, lpmwd->selection.lpsellistSectors);

			/* Might change line colours. */
			lpmwd->bWantRedraw = TRUE;

			return 0;

		case IDM_SECTORS_LIGHT_INCREASE:

			CreateUndo(lpmwd, IDS_UNDO_INCREASELIGHT);
			ClearRedoStack(lpmwd);
			lpmwd->bMapChanged = TRUE;

			ApplyRelativeBrightnessSelection(lpmwd->lpmap, lpmwd->selection.lpsellistSectors, 16);

			MapWinUpdateInfoBar(lpmwd);

			return 0;

		case IDM_SECTORS_LIGHT_DECREASE:

			CreateUndo(lpmwd, IDS_UNDO_DECREASELIGHT);
			ClearRedoStack(lpmwd);
			lpmwd->bMapChanged = TRUE;

			ApplyRelativeBrightnessSelection(lpmwd->lpmap, lpmwd->selection.lpsellistSectors, -16);

			MapWinUpdateInfoBar(lpmwd);

			return 0;

		case IDM_SECTORS_LIGHT_GRADIENT:

			CreateUndo(lpmwd, IDS_UNDO_GRADIENTLIGHT);
			ClearRedoStack(lpmwd);
			lpmwd->bMapChanged = TRUE;

			GradientSelectedBrightnesses(lpmwd->lpmap, lpmwd->selection.lpsellistSectors);

			/* Might change line colours. */
			lpmwd->bWantRedraw = TRUE;

			return 0;

		case IDM_EDIT_TRANSFORM_SNAP:

			CreateUndo(lpmwd, IDS_UNDO_SNAP);
			ClearRedoStack(lpmwd);
			lpmwd->bMapChanged = TRUE;

			/* Vertices first. Select them from lines. */
			CreateAuxiliarySelection(lpmwd);
			SnapAuxVerticesToGrid(lpmwd);
			CleanAuxiliarySelection(lpmwd);

			/* Now things. */
			SnapSelectedThingsToGrid(lpmwd);

			lpmwd->bWantRedraw = TRUE;
			MapWinUpdateInfoBar(lpmwd);
			UpdateStatusBar(lpmwd, SBPF_LINEDEFS | SBPF_SIDEDEFS | SBPF_VERTICES);

			return 0;

		case IDM_THINGS_HEIGHT_INCREASE:

			CreateUndo(lpmwd, IDS_UNDO_INCREASETHINGZ);
			ClearRedoStack(lpmwd);
			lpmwd->bMapChanged = TRUE;

			ApplyRelativeThingZSelection(lpmwd->lpmap, lpmwd->selection.lpsellistThings, ConfigGetSubsection(lpmwd->lpcfgMap, FLAT_THING_SECTION), 8);

			MapWinUpdateInfoBar(lpmwd);

			return 0;

		case IDM_THINGS_HEIGHT_DECREASE:

			CreateUndo(lpmwd, IDS_UNDO_DECREASETHINGZ);
			ClearRedoStack(lpmwd);
			lpmwd->bMapChanged = TRUE;

			ApplyRelativeThingZSelection(lpmwd->lpmap, lpmwd->selection.lpsellistThings, ConfigGetSubsection(lpmwd->lpcfgMap, FLAT_THING_SECTION), -8);

			MapWinUpdateInfoBar(lpmwd);

			return 0;
		}

		/* No-one else needs to see it any more. */
		return 0;


	case WM_MOUSEWHEEL:
		/* We pass wheel events as keypresses so they can act as shortcuts. */
		lpmwd = (MAPWINDATA*)GetWindowLong(hwnd, GWL_USERDATA);
		MapKeyDown(lpmwd, MakeShiftedKeyCode((short)HIWORD(wParam) > 0 ? MOUSE_SCROLL_UP : MOUSE_SCROLL_DOWN));
		return 0;

	case WM_KEYDOWN:
		/* Most of our keypresses are handled using accelerators, but there are
		 * some exceptions.
		 */
		lpmwd = (MAPWINDATA*)GetWindowLong(hwnd, GWL_USERDATA);
		MapKeyDown(lpmwd, MakeShiftedKeyCode(wParam));
		return 0;

	case WM_KEYUP:
		lpmwd = (MAPWINDATA*)GetWindowLong(hwnd, GWL_USERDATA);
		MapKeyUp(lpmwd, MakeShiftedKeyCode(wParam));
		return 0;

	case WM_MOUSEMOVE:
		return MouseMove((MAPWINDATA*)GetWindowLong(hwnd, GWL_USERDATA), LOWORD(lParam), HIWORD(lParam));

	case WM_LBUTTONDOWN:
		return LButtonDown((MAPWINDATA*)GetWindowLong(hwnd, GWL_USERDATA), LOWORD(lParam), HIWORD(lParam), (WORD)wParam);

	case WM_LBUTTONUP:
		return LButtonUp((MAPWINDATA*)GetWindowLong(hwnd, GWL_USERDATA), LOWORD(lParam), HIWORD(lParam), (WORD)wParam);

	case WM_RBUTTONUP:
		return RButtonUp((MAPWINDATA*)GetWindowLong(hwnd, GWL_USERDATA), LOWORD(lParam), HIWORD(lParam), (WORD)wParam);

	case WM_RBUTTONDOWN:
		return RButtonDown((MAPWINDATA*)GetWindowLong(hwnd, GWL_USERDATA), LOWORD(lParam), HIWORD(lParam), (WORD)wParam);

	case WM_MOUSELEAVE:
		/* Generated by TrackMouseEvent. */

		lpmwd = (MAPWINDATA*)GetWindowLong(hwnd, GWL_USERDATA);
		lpmwd->xMouseLast = lpmwd->yMouseLast = -1;

		/* Clear the highlight if necessary. */
		if(UpdateHighlight(lpmwd, -1, -1))
			MapWinUpdateInfoBar(lpmwd);

		return 0;

	case WM_SIZE:

		if(wParam != SIZE_MINIMIZED)
		{
			lpmwd = (MAPWINDATA*)GetWindowLong(hwnd, GWL_USERDATA);

			/* Store the width and the height for later use. */
			lpmwd->mapview.cxDrawSurface = (LOWORD(lParam) + 3) & ~3;
			lpmwd->mapview.cyDrawSurface = HIWORD(lParam);

			wglMakeCurrent(lpmwd->hdc, lpmwd->hrc);
			ReSizeGLScene(lpmwd->mapview.cxDrawSurface, lpmwd->mapview.cyDrawSurface);
		}

		/* DefMDIChildProc needs to process this message too. */
		break;

	case WM_ERASEBKGND:
		return 1;	/* Stop flickering. */

	case WM_TIMER:

		/* Is this the redraw timer? */
		if(wParam == IDT_ANIM_TIMER)
		{
			lpmwd = (MAPWINDATA*)GetWindowLong(hwnd, GWL_USERDATA);
			if(lpmwd->bWantRedraw)
			{
				/* Force a repaint. */
				InvalidateRect(hwnd, NULL, FALSE);
				lpmwd->bWantRedraw = FALSE;
			}
		}

		return 0;

	case WM_PAINT:
		{
			PAINTSTRUCT ps;

			lpmwd = (MAPWINDATA*)GetWindowLong(hwnd, GWL_USERDATA);

			BeginPaint(hwnd, &ps);
			RedrawMapWindow(lpmwd);
			EndPaint(hwnd, &ps);
		}

		return 0;

	case WM_CLOSE:

		lpmwd = (MAPWINDATA*)GetWindowLong(hwnd, GWL_USERDATA);

		/* If we're unsaved, display the customary message box. */
		if(lpmwd->bMapChanged)
		{
			TCHAR szSaveChanges[SAVECHANGES_BUFSIZE];
			int iHeadLength;
			int iMBRet;

			/* Cheap trick: use the window title to report the filename and
			 * lumpname in the message box.
			 */
			GetWindowText(hwnd, szSaveChanges, sizeof(szSaveChanges) / sizeof(TCHAR));
			iHeadLength = lstrlen(szSaveChanges);
			LoadString(g_hInstance, IDS_SAVECHANGES, szSaveChanges + iHeadLength, sizeof(szSaveChanges) / sizeof(TCHAR) - iHeadLength);

			iMBRet = MessageBox(g_hwndMain, szSaveChanges, g_szAppName, MB_ICONEXCLAMATION | MB_YESNOCANCEL);

			switch(iMBRet)
			{
			case IDYES:
				/* User selected to save. */
				MapWindowSave(lpmwd);
				break;
			case IDCANCEL:
				/* Don't close the window. */
				return 0;
			}
		}

		break;		/* Allow DefMDIChildProc to destroy us. */

	case WM_DESTROY:

		lpmwd = (MAPWINDATA*)GetWindowLong(hwnd, GWL_USERDATA);

		/* Stop the animation timer. */
		KillTimer(hwnd, IDT_ANIM_TIMER);

		/* Free data. */
		DestroyMapWindowData(hwnd);

		return 0;
	}

	return DefMDIChildProc(hwnd, uiMessage, wParam, lParam);
}


/* DestroyMapWindowData
 *   Cleans up a map window's data.
 *
 * Parameters:
 *   HWND		hwnd		Window handle.
 *
 * Return value:
 *   None.
 */
static void DestroyMapWindowData(HWND hwnd)
{
	MAPWINDATA *lpmwd = (MAPWINDATA*)GetWindowLong(hwnd, GWL_USERDATA);

	/* Renderer clean-up. */
	if(lpmwd->hrc)
	{
		/* Try to release DC and RC. */
		wglMakeCurrent(NULL,NULL);

		/* Try to delete the RC. */
		wglDeleteContext(lpmwd->hrc);
	}

	if(lpmwd->hdc) ReleaseDC(hwnd, lpmwd->hdc);

	/* Release the wad references. */
	ReleaseWad(lpmwd->iWadID);
	if(lpmwd->iIWadID) ReleaseWad(lpmwd->iIWadID);

	/* Clean up undo memory. */
	if(lpmwd->lpundostackUndo) FreeUndoStack(lpmwd->lpundostackUndo);
	if(lpmwd->lpundostackRedo) FreeUndoStack(lpmwd->lpundostackRedo);

	/* Destroy selection structures. */
	DestroySelectionList(lpmwd->selection.lpsellistLinedefs);
	DestroySelectionList(lpmwd->selection.lpsellistSectors);
	DestroySelectionList(lpmwd->selection.lpsellistVertices);
	DestroySelectionList(lpmwd->selection.lpsellistThings);

	/* Clean the texture caches. */
	PurgeTextureCache(&lpmwd->tcFlatsHdr);
	PurgeTextureCache(&lpmwd->tcTexturesHdr);
	if(lpmwd->himlCacheFlats) ImageList_Destroy(lpmwd->himlCacheFlats);
	if(lpmwd->himlCacheTextures) ImageList_Destroy(lpmwd->himlCacheTextures);

	/* Free the list of texture names. */
	DestroyTextureNameList(lpmwd->lptnlFlats);
	DestroyTextureNameList(lpmwd->lptnlTextures);

	/* Destroy map structure. */
	DestroyMapStructure(lpmwd->lpmap);

	/* Destroy window structure. */
	HeapFree(g_hProcHeap, 0, lpmwd);
}

/* MapWinBelongsToWad
 *   Determines whether a map window belongs to a particular wad.
 *
 * Parameters:
 *   HWND		hwnd		Window handle.
 *   int		iWadID		ID of wad.
 *
 * Return value: BOOL
 *   TRUE if window belongs to wad; FALSE otherwise.
 */
BOOL MapWinBelongsToWad(HWND hwnd, int iWadID)
{
	return ((MAPWINDATA*)GetWindowLong(hwnd, GWL_USERDATA))->iWadID == iWadID;
}


/* MapKeyDown
 *   Performs certain keypress operations for a map window.
 *
 * Parameters:
 *   MAPWINDATA *lpmwd		Window data.
 *   int		iKeyCode	Key-code containing VK and shift states.
 *
 * Return value: BOOL
 *   TRUE if something done in response to keypress; FALSE otherwise.
 *
 * Notes:
 *   Most keypresses are handled by accelerators, but certain ones -- e.g. those
 *   that have separate up/down responses -- are handled separately.
 */
static BOOL MapKeyDown(MAPWINDATA *lpmwd, int iKeyCode)
{
	if(iKeyCode == g_iShortcutCodes[SCK_EDITQUICKMOVE])
	{
		if(lpmwd->editmode != EM_MOVE)
		{
			StartPan(lpmwd);

			return TRUE;
		}
	}
	else if(iKeyCode == g_iShortcutCodes[SCK_ZOOMIN] || iKeyCode == g_iShortcutCodes[SCK_ZOOMOUT])
	{
		float fNewZoom;
		float cxDelta, cyDelta;
		BOOL bMouseInside;
		POINT ptCursor;

		/* Determine whether the mouse is over our map. */
		GetCursorPos(&ptCursor);
		bMouseInside = (SendMessage(lpmwd->hwnd, WM_NCHITTEST, 0, MAKELPARAM(ptCursor.x, ptCursor.y)) == HTCLIENT);

		/* Zoom in or out, according to request. */
		fNewZoom = lpmwd->mapview.fZoom * (1 + (((iKeyCode == g_iShortcutCodes[SCK_ZOOMIN]) ? 1 : -1) * ConfigGetInteger(g_lpcfgMain, "zoomspeed") / 1000.0f));

		/* Bounds. */
		if(fNewZoom > 100) fNewZoom = 100;
		else if(fNewZoom < 0.01f) fNewZoom = 0.01f;

		if(ConfigGetInteger(g_lpcfgMain, "zoommouse") && bMouseInside)
		{
			/* Fracunit x-origin difference = 
			 *		(new fracunit width - old fracunit width) *
			 *		mouse pos as proportion of width.
			 *
			 * Similarly height.
			 */
			cxDelta = (((lpmwd->mapview.cxDrawSurface / fNewZoom) - (lpmwd->mapview.cxDrawSurface / lpmwd->mapview.fZoom)) * ((float)lpmwd->xMouseLast / lpmwd->mapview.cxDrawSurface));
			cyDelta = (((lpmwd->mapview.cyDrawSurface / fNewZoom) - (lpmwd->mapview.cyDrawSurface / lpmwd->mapview.fZoom)) * ((float)lpmwd->yMouseLast / lpmwd->mapview.cyDrawSurface));
		}
		else
		{
			cxDelta = (((lpmwd->mapview.cxDrawSurface / fNewZoom) - (lpmwd->mapview.cxDrawSurface / lpmwd->mapview.fZoom)) / 2);
			cyDelta = (((lpmwd->mapview.cyDrawSurface / fNewZoom) - (lpmwd->mapview.cyDrawSurface / lpmwd->mapview.fZoom)) / 2);
		}

		lpmwd->mapview.xLeft -= cxDelta;
		lpmwd->mapview.yTop += cyDelta;
		SetZoom(&lpmwd->mapview, fNewZoom);

		/* Redraw map next frame. */
		lpmwd->bWantRedraw = TRUE;

		UpdateStatusBar(lpmwd, SBPF_ZOOM);
	}
	else if(iKeyCode == g_iShortcutCodes[SCK_DRAWSECTOR])
	{
		/* TODO: This should maybe have a menu item. If so, it should be handled
		 * as a WM_COMMAND.
		 */

		/* Begin insertion, of a type proper to the mode. */
		switch(lpmwd->editmode)
		{
		case EM_ANY:
		case EM_LINES:
		case EM_SECTORS:
		case EM_VERTICES:
			/* This is a prelude to ESM_DRAWING. Also, note that the map
			 * hasn't changed yet!
			 */
			DeselectAll(lpmwd);
			UpdateHighlight(lpmwd, -1, -1);
			lpmwd->submode = ESM_INSERTING;
			BeginDrawOperation(&lpmwd->drawop);

			break;
			
		default:
            break;
		}

		lpmwd->bWantRedraw = TRUE;
	}

	return FALSE;
}



/* MapKeyUp
 *   Performs certain keyrelease operations for a map window.
 *
 * Parameters:
 *   MAPWINDATA*	lpmwd		Window data.
 *   int			iKeyCode	Key-code containing VK and shift states.
 *
 * Return value: BOOL
 *   TRUE if something done in response to keyrelease; FALSE otherwise.
 *
 * Notes:
 *   Most keypresses are handled by accelerators, but certain ones -- e.g. those
 *   that have separate up/down responses -- are handled separately.
 */
static BOOL MapKeyUp(MAPWINDATA *lpmwd, int iKeyCode)
{
	if(iKeyCode == g_iShortcutCodes[SCK_EDITQUICKMOVE])
	{
		if(lpmwd->editmode != EM_MOVE)
		{
			EndPan(lpmwd);

			return TRUE;
		}
	}

	return FALSE;
}


/* RedrawMapWindow
 *   Performs certain keypress operations for a map window.
 *
 * Parameters:
 *   MAPWINDATA*	lpmwd		Window data.
 *
 * Return value: None.
 */
static void RedrawMapWindow(MAPWINDATA *lpmwd)
{
	DRAWOP_RENDERINFO dori;

	/* If we're in a drawing operation, set up the extra info. */
	if(lpmwd->submode == ESM_DRAWING)
	{
		/* We must have a previous vertex in ESM_DRAWING. */
		int iVertexPrev = lpmwd->drawop.lpiNewVertices[lpmwd->drawop.iNewVertexCount-1];

		dori.ptSrc.x = lpmwd->lpmap->vertices[iVertexPrev].x;
		dori.ptSrc.y = lpmwd->lpmap->vertices[iVertexPrev].y;
	}

	if(lpmwd->submode == ESM_DRAWING || lpmwd->submode == ESM_INSERTING)
	{
		short x, y;

		/* We draw the floating vertex at the mouse position. The drawing line
		 * will also be drawn *to* here if necessary.
		 */
		x = XMOUSE_WIN_TO_MAP(lpmwd->xMouseLast, lpmwd);
		y = YMOUSE_WIN_TO_MAP(lpmwd->yMouseLast, lpmwd);

		/* Do snapping on the point if necessary. */
		if((lpmwd->bySnapFlags ^ lpmwd->bySnapToggleMask) & SF_RECTANGLE)
			Snap(&x, &y, lpmwd->mapview.cxGrid, lpmwd->mapview.cyGrid);

		/* We needed these intermediates since Snap expects shorts, while POINTs
		 * have ints.
		 */
		dori.ptDest.x = x;
		dori.ptDest.y = y;
	}

	wglMakeCurrent(lpmwd->hdc, lpmwd->hrc);
	
	/* Draw onto backbuffer. */
	RedrawMap(lpmwd->lpmap, lpmwd->editmode, lpmwd->submode, &lpmwd->mapview, &dori);
	
	glFlush();
	SwapBuffers(lpmwd->hdc);
}


static void ChangeMode(MAPWINDATA *lpmwd, USHORT unModeMenuID)
{
	/* TODO: Do all the menu stuff on WM_INITMENUPOPUP. */

	static USHORT s_unModeToMenu[7] =
	{
		IDM_VIEW_MODE_MOVE, IDM_VIEW_MODE_ANY,
		IDM_VIEW_MODE_VERTICES, IDM_VIEW_MODE_LINES,
		IDM_VIEW_MODE_SECTORS, IDM_VIEW_MODE_THINGS,
		IDM_VIEW_3D
	};

	HMENU hmenuView;
	POINT pt;
	

	/* Get handle to View menu. */
	hmenuView = GetSubMenu(g_hmenuMap, VIEWMENUPOS);


	/* Uncheck old modes if we're not about to move. */
	if(unModeMenuID != IDM_VIEW_MODE_MOVE)
	{
		CheckMenuItem(hmenuView, s_unModeToMenu[lpmwd->editmode], MF_BYCOMMAND | MF_UNCHECKED);
		CheckMenuItem(hmenuView, s_unModeToMenu[lpmwd->prevmode], MF_BYCOMMAND | MF_UNCHECKED);
	}


	if(unModeMenuID == IDM_VIEW_MODE_MOVE && lpmwd->editmode == EM_MOVE)
	{
		/* Restore previous mode. */
		ChangeMode(lpmwd, s_unModeToMenu[lpmwd->prevmode]);
		return;
	}
	else
	{
		lpmwd->prevmode = lpmwd->editmode;
	}
	
	/* Do things necessary on leaving old mode. */
	switch(lpmwd->editmode)
	{
	case EM_MOVE:
		lpmwd->hCursor = LoadCursor(NULL, IDC_ARROW);
		break;
	case EM_ANY:
		break;
	case EM_LINES:
		break;
	case EM_SECTORS:
		break;
	case EM_VERTICES:
		break;
	case EM_THINGS:
		break;
	default:
        break;
	}


	switch(unModeMenuID)
	{
	case IDM_VIEW_MODE_MOVE:
		lpmwd->editmode = EM_MOVE;
		lpmwd->hCursor = LoadCursor(g_hInstance, MAKEINTRESOURCE(IDC_GRABOPEN));
		break;

	case IDM_VIEW_MODE_ANY:
		lpmwd->editmode = EM_ANY;
		break;

	case IDM_VIEW_MODE_LINES:
		SelectLinesFromVertices(lpmwd);
		SelectSectorsFromLines(lpmwd);
		DeselectAllThings(lpmwd);
		DeselectAllVertices(lpmwd);
		lpmwd->editmode = EM_LINES;
		break;

	case IDM_VIEW_MODE_SECTORS:
		SelectLinesFromVertices(lpmwd);
		SelectSectorsFromLines(lpmwd);
		DeselectAllThings(lpmwd);
		DeselectAllVertices(lpmwd);
		DeselectLinesFromPartialSectors(lpmwd);
		lpmwd->editmode = EM_SECTORS;
		break;

	case IDM_VIEW_MODE_THINGS:
		DeselectAllLines(lpmwd);
		DeselectAllSectors(lpmwd);
		DeselectAllVertices(lpmwd);
		lpmwd->editmode = EM_THINGS;
		break;

	case IDM_VIEW_MODE_VERTICES:
		SelectVerticesFromLines(lpmwd);
		DeselectAllLines(lpmwd);
		DeselectAllSectors(lpmwd);
		DeselectAllThings(lpmwd);
		lpmwd->editmode = EM_VERTICES;
		break;
	}

	/* Check new mode. */
	CheckMenuItem(hmenuView, unModeMenuID, MF_BYCOMMAND | MF_CHECKED);

	/* Update highlight if mouse is in window. */
	if(lpmwd->xMouseLast >= 0 && lpmwd->yMouseLast >= 0)
		UpdateHighlight(lpmwd, lpmwd->xMouseLast, lpmwd->yMouseLast);

	/* Set up info bar. */
	SetInfoBarMode(lpmwd->editmode);
	MapWinUpdateInfoBar(lpmwd);

	/* Update cursor if we're in the client area. */
	pt.x = lpmwd->xMouseLast;
	pt.y = lpmwd->yMouseLast;
	ClientToScreen(lpmwd->hwnd, &pt);
	if(SendMessage(lpmwd->hwnd, WM_NCHITTEST, 0, MAKELPARAM(pt.x, pt.y)) == HTCLIENT)
		SetCursor(lpmwd->hCursor);
}



/* StatusBarMapWindow
 *   Sets the status bar to correspond to the active map window.
 *
 * Parameters:
 *   None.
 *
 * Return value: None.
 */
static void StatusBarMapWindow(void)
{
	INT lpiWidths[SBP_LAST] = {96, 84, 84, 84, 84, 84, 84, 84, 96, -1};
	int i;

	/* Calculate RHS from widths. Loop from second to second-to-last: The first
	 * and last are okay.
	 */
	for(i = 1; i < SBP_LAST - 1; i++) lpiWidths[i] += lpiWidths[i-1];

	SendMessage(g_hwndStatusBar, SB_SETPARTS, SBP_LAST, (LPARAM)lpiWidths);

	/* Not simple mode. */
	SendMessage(g_hwndStatusBar, SB_SIMPLE, FALSE, 0);
}


/* UpdateStatusBar
 *   Updates the status bar's panels' text.
 *
 * Parameters:
 *   MAPWINDATA*	lpmwd	Pointer to data for active window.
 *   DWORD			dwFlags	Flags specifiying which panels to update.
 *
 * Return value: None.
 *
 * Remarks:
 *   Each bit of the flag corresponds to the equally-numbered constant specified
 *   in _ENUM_SBPANELS.
 */
static void UpdateStatusBar(MAPWINDATA *lpmwd, DWORD dwFlags)
{
	TCHAR szBuffer[256], szFromStringTable[32];
	MAP *lpmap = lpmwd->lpmap;

	/* Check for each field, and update if asked. */
	if(dwFlags & SBPF_MODE)
	{
		DWORD dwStringID = 0;

		switch(lpmwd->editmode)
		{
			case EM_ANY:		dwStringID = IDS_SB_ANYMODE;		break;
			case EM_SECTORS:	dwStringID = IDS_SB_SECTORSMODE;	break;
			case EM_LINES:		dwStringID = IDS_SB_LINESMODE;		break;
			case EM_VERTICES:	dwStringID = IDS_SB_VERTICESMODE;	break;
			case EM_THINGS:		dwStringID = IDS_SB_THINGSMODE;		break;
			default:												break;
		}

		LoadString(g_hInstance, dwStringID, szFromStringTable, sizeof(szFromStringTable) / sizeof(TCHAR));
		_sntprintf(szBuffer, sizeof(szBuffer)/sizeof(TCHAR) - 1, TEXT("\t%s"), szFromStringTable);
		szBuffer[255] = '\0';
		SendMessage(g_hwndStatusBar, SB_SETTEXT, SBP_MODE, (LPARAM)szBuffer);
	}

	if(dwFlags & SBPF_SECTORS)
	{
		LoadString(g_hInstance, IDS_SB_SECTORS, szFromStringTable, sizeof(szFromStringTable) / sizeof(TCHAR));
		_sntprintf(szBuffer, sizeof(szBuffer)/sizeof(TCHAR) - 1, TEXT("\t%d %s"), lpmap->iSectors, szFromStringTable);
		szBuffer[255] = '\0';
		SendMessage(g_hwndStatusBar, SB_SETTEXT, SBP_SECTORS, (LPARAM)szBuffer);
	}

	if(dwFlags & SBPF_LINEDEFS)
	{
		LoadString(g_hInstance, IDS_SB_LINEDEFS, szFromStringTable, sizeof(szFromStringTable) / sizeof(TCHAR));
		_sntprintf(szBuffer, sizeof(szBuffer)/sizeof(TCHAR) - 1, TEXT("\t%d %s"), lpmap->iLinedefs, szFromStringTable);
		szBuffer[255] = '\0';
		SendMessage(g_hwndStatusBar, SB_SETTEXT, SBP_LINEDEFS, (LPARAM)szBuffer);
	}

	if(dwFlags & SBPF_SIDEDEFS)
	{
		LoadString(g_hInstance, IDS_SB_SIDEDEFS, szFromStringTable, sizeof(szFromStringTable) / sizeof(TCHAR));
		_sntprintf(szBuffer, sizeof(szBuffer)/sizeof(TCHAR) - 1, TEXT("\t%d %s"), lpmap->iSidedefs, szFromStringTable);
		szBuffer[255] = '\0';
		SendMessage(g_hwndStatusBar, SB_SETTEXT, SBP_SIDEDEFS, (LPARAM)szBuffer);
	}

	if(dwFlags & SBPF_VERTICES)
	{
		LoadString(g_hInstance, IDS_SB_VERTICES, szFromStringTable, sizeof(szFromStringTable) / sizeof(TCHAR));
		_sntprintf(szBuffer, sizeof(szBuffer)/sizeof(TCHAR) - 1, TEXT("\t%d %s"), lpmap->iVertices, szFromStringTable);
		szBuffer[255] = '\0';
		SendMessage(g_hwndStatusBar, SB_SETTEXT, SBP_VERTICES, (LPARAM)szBuffer);
	}

	if(dwFlags & SBPF_THINGS)
	{
		LoadString(g_hInstance, IDS_SB_THINGS, szFromStringTable, sizeof(szFromStringTable) / sizeof(TCHAR));
		_sntprintf(szBuffer, sizeof(szBuffer)/sizeof(TCHAR) - 1, TEXT("\t%d %s"), lpmap->iThings, szFromStringTable);
		szBuffer[255] = '\0';
		SendMessage(g_hwndStatusBar, SB_SETTEXT, SBP_THINGS, (LPARAM)szBuffer);
	}

	if(dwFlags & SBPF_COORD)
	{
		_sntprintf(szBuffer, sizeof(szBuffer)/sizeof(TCHAR) - 1, TEXT("\t(%d, %d)"), XMOUSE_WIN_TO_MAP(lpmwd->xMouseLast, lpmwd), YMOUSE_WIN_TO_MAP(lpmwd->yMouseLast, lpmwd));
		szBuffer[255] = '\0';
		SendMessage(g_hwndStatusBar, SB_SETTEXT, SBP_COORD, (LPARAM)szBuffer);
	}

	if(dwFlags & SBPF_GRID)
	{
		LoadString(g_hInstance, IDS_SB_GRID, szFromStringTable, sizeof(szFromStringTable) / sizeof(TCHAR));

		/* If the grid is square, don't duplicate the dimension. */
		if(lpmwd->mapview.cxGrid == lpmwd->mapview.cyGrid)
			_sntprintf(szBuffer, sizeof(szBuffer)/sizeof(TCHAR) - 1, TEXT("\t%s: %d"), szFromStringTable, lpmwd->mapview.cxGrid);
		else
			_sntprintf(szBuffer, sizeof(szBuffer)/sizeof(TCHAR) - 1, TEXT("\t%s: (%d, %d)"), szFromStringTable, lpmwd->mapview.cxGrid, lpmwd->mapview.cyGrid);

		szBuffer[255] = '\0';

		SendMessage(g_hwndStatusBar, SB_SETTEXT, SBP_GRID, (LPARAM)szBuffer);
	}

	if(dwFlags & SBPF_ZOOM)
	{
		LoadString(g_hInstance, IDS_SB_ZOOM, szFromStringTable, sizeof(szFromStringTable) / sizeof(TCHAR));

		/* If the grid is square, don't duplicate the dimension. */
		_sntprintf(szBuffer, sizeof(szBuffer)/sizeof(TCHAR) - 1, TEXT("\t%s: %d%%"), szFromStringTable, ROUND(lpmwd->mapview.fZoom * 100));

		szBuffer[255] = '\0';

		SendMessage(g_hwndStatusBar, SB_SETTEXT, SBP_ZOOM, (LPARAM)szBuffer);
	}

	if(dwFlags & SBPF_MAPCONFIG)
	{
		ConfigGetString(lpmwd->lpcfgMap, "game", szBuffer, sizeof(szBuffer) / sizeof(TCHAR));
		SendMessage(g_hwndStatusBar, SB_SETTEXT, SBP_MAPCONFIG, (LPARAM)szBuffer);
	}
}


/* UpdateHighlight, ClearHighlight
 *   Updates highlighted object for a window, given mouse co-ordinates.
 *
 * Parameters:
 *   MAPWINDATA*	lpmwd			Pointer to data for window.
 *   int			xMouse, yMouse	Mouse co-ordinates.
 *
 * Return value: BOOL
 *   True if highlighted object was changed; FALSE otherwise.
 *
 * Remarks:
 *   If any of the mouse co-ordinates are negative, clear the highlight.
 *   ClearHighlight calls UpdateHighlight with xMouse, yMouse == -1.
 */
static BOOL UpdateHighlight(MAPWINDATA *lpmwd, int xMouse, int yMouse)
{
	HIGHLIGHTOBJ highlightobj;

	if(xMouse >= 0 && yMouse >= 0)
	{
		int xMap, yMap;

		xMap = XMOUSE_WIN_TO_MAP(xMouse, lpmwd);
		yMap = YMOUSE_WIN_TO_MAP(yMouse, lpmwd);

		/* Find the nearest object for the mode. */
		GetNearestObject(lpmwd, xMap, yMap, lpmwd->editmode, &highlightobj);
	}
	else
	{
		/* Just clear the highlight. */
		highlightobj.emType = EM_MOVE;
	}

	/* Has the highlight changed? */
	if(highlightobj.emType != lpmwd->highlightobj.emType || (highlightobj.emType != EM_MOVE && highlightobj.iIndex != lpmwd->highlightobj.iIndex))
	{
		/* If so, redraw with new highlight. */
		ClearHighlight(lpmwd);
		SetHighlight(lpmwd, &highlightobj);
		lpmwd->bWantRedraw = TRUE;

		/* Highlight changed. */
		return TRUE;
	}

	/* Highlight didn't change. */
	return FALSE;
}


/* GetNearestObject
 *   Gets index and type of object nearest to a point on the map, according to
 *   mode.
 *
 * Parameters:
 *   MAPWINDATA*	lpmwd			Pointer to data for window.
 *   int			xMap, yMap		Map co-ordinates to find obj nearest to.
 *   ENUM_EDITMODE	mode			Editing mode, specifying types of obj.
 *   HIGHLIGHTOBJ*	lphighlightobj	Nearest object is returned here.
 *
 * Return value: None.
 *
 * Remarks:
 *   It's possible that nothing will be found, in which case the emType field of
 *   lphighlightobj will be set to EM_MOVE.
 */
static void GetNearestObject(MAPWINDATA *lpmwd, int xMap, int yMap, ENUM_EDITMODE mode, HIGHLIGHTOBJ *lphighlightobj)
{
	MAP *lpmap = lpmwd->lpmap;

	BOOL bWantLinedef = (mode == EM_LINES) || (mode == EM_ANY);
	BOOL bWantSector = (mode == EM_SECTORS) || (mode == EM_ANY);
	BOOL bWantThing = (mode == EM_THINGS) || (mode == EM_ANY);
	BOOL bWantVertex = (mode == EM_VERTICES) || (mode == EM_ANY);

	int iDistLinedef, iDistThing, iDistVertex;
	int iIndexLinedef = 0, iIndexSector = 0, iIndexThing = 0, iIndexVertex = 0;

	int iMinDist = INT_MAX;


	/* Begin by assuming we don't find anything. */
	lphighlightobj->emType = EM_MOVE;


	/* Find nearest objects. If we don't find any at each stage, indicate that
	 * we're no longer looking for objects of that type.
	 */

	if((bWantLinedef || bWantSector)
		&& -1 == (iIndexLinedef = NearestLinedef(xMap, yMap, lpmap->vertices, lpmap->linedefs, lpmap->iLinedefs, &iDistLinedef)))
	{
		bWantLinedef = bWantSector = FALSE;
	}

	if(bWantThing && -1 == (iIndexThing = NearestThing(xMap, yMap, lpmap->things, lpmap->iThings, &iDistThing, FALSE, NULL)))
		bWantThing = FALSE;

	if(bWantVertex && -1 == (iIndexVertex = NearestVertex(lpmap, xMap, yMap, &iDistVertex)))
		bWantVertex = FALSE;


	/* For sectors, we also check that we're actually *in* a sector. If we are,
	 * get its index.
	 */
	if(bWantSector)
	{
		iIndexSector = IntersectSector(xMap, yMap, lpmap->vertices, lpmap->linedefs, lpmap->sidedefs, lpmap->iLinedefs, NULL, NULL);
		if(iIndexSector < 0) bWantSector = FALSE;
	}


	/* If we're within VERTEX_THRESHOLD of a vertex, it takes precedence over
	 * linedefs and sectors.
	 */
	if(bWantVertex && iDistVertex * lpmwd->mapview.fZoom <= VERTEX_THRESHOLD)
		bWantLinedef = bWantSector = FALSE;

	/* Turn off one of these. */
	if(bWantLinedef && bWantSector)
	{
		/* Are we near enough to the linedef to choose it in preference to the
		 * sector?
		 */
		if(iDistLinedef * lpmwd->mapview.fZoom <= LINEDEF_THRESHOLD)
			bWantSector = FALSE;		/* Pick linedef. */
		else bWantLinedef = FALSE;		/* Pick sector. */
	}

	if(bWantLinedef && iDistLinedef < iMinDist)
	{
		iMinDist = iDistLinedef;
		lphighlightobj->emType = EM_LINES;
		lphighlightobj->iIndex = iIndexLinedef;
	}

	if(bWantThing && iDistThing < iMinDist)
	{
		iMinDist = iDistThing;
		lphighlightobj->emType = EM_THINGS;
		lphighlightobj->iIndex = iIndexThing;
	}

	if(bWantVertex && iDistVertex < iMinDist)
	{
		iMinDist = iDistVertex;
		lphighlightobj->emType = EM_VERTICES;
		lphighlightobj->iIndex = iIndexVertex;
	}

	/* We've checked everything except sectors now. These must respect the hover
	 * threshold.
	 */
	if(iMinDist * lpmwd->mapview.fZoom > HOVER_THRESHOLD)
		lphighlightobj->emType = EM_MOVE;

	/* With sectors, pick one if it's nearest *or* if we haven't found anything
	 * else yet. This assumes we're interested at all, of course.
	 */
	if(bWantSector && (iDistLinedef < iMinDist || lphighlightobj->emType == EM_MOVE))
	{
		iMinDist = iDistLinedef;
		lphighlightobj->emType = EM_SECTORS;
		lphighlightobj->iIndex = iIndexSector;
	}
}


/* ClearHighlight
 *   Sets a window's highlighted object to nothing.
 *
 * Parameters:
 *   MAPWINDATA*	lpmwd			Pointer to data for window.
 *
 * Return value: None.
 */
static void ClearHighlight(MAPWINDATA *lpmwd)
{
	int i, iLinedefs;
	MAP *lpmap = lpmwd->lpmap;
	int iIndex = lpmwd->highlightobj.iIndex;

	/* Clear the map-data highlight flag. */
	switch(lpmwd->highlightobj.emType)
	{
	case EM_SECTORS:

		/* For sectors, we actually highlight the linedefs. */
		iLinedefs = lpmwd->lpmap->iLinedefs;
		for(i=0; i<iLinedefs; i++)
		{
			if(LinedefBelongsToSector(lpmap, i, iIndex))
				lpmap->linedefs[i].highlight = 0;
		}

		break;

	case EM_LINES:
		lpmap->linedefs[iIndex].highlight = 0;
		break;
	case EM_VERTICES:
		lpmap->vertices[iIndex].highlight = 0;
		break;
	case EM_THINGS:
		lpmap->things[iIndex].highlight = 0;
		break;
	default:
        break;
	}

	/* Move mode indicates no higlight. There's *kind* of a logic to it... */
	lpmwd->highlightobj.emType = EM_MOVE;
}


/* SetHighlight
 *   Sets a window's highlighted object.
 *
 * Parameters:
 *   MAPWINDATA*	lpmwd			Pointer to data for window.
 *   HIGHLIGHTOBJ*	lphighlightobj	Pointer to object data.
 *
 * Return value: None.
 */
static void SetHighlight(MAPWINDATA *lpmwd, HIGHLIGHTOBJ *lphightlightobj)
{
	int i, iLinedefs;
	MAP *lpmap = lpmwd->lpmap;
	int iIndex;

	/* Store the details in the window data. */
	lpmwd->highlightobj = *lphightlightobj;
	iIndex = lphightlightobj->iIndex;

	/* Set the highlight flag in the map data. */
	switch(lphightlightobj->emType)
	{
	case EM_SECTORS:
		
		/* For sectors, we actually highlight the linedefs. */
		iLinedefs = lpmwd->lpmap->iLinedefs;
		for(i=0; i<iLinedefs; i++)
		{
			if(LinedefBelongsToSector(lpmap, i, iIndex))
				lpmap->linedefs[i].highlight = 1;
		}

		break;

	case EM_LINES:
		lpmap->linedefs[iIndex].highlight = 1;
		break;
	case EM_VERTICES:
		lpmap->vertices[iIndex].highlight = 1;
		break;
	case EM_THINGS:
		lpmap->things[iIndex].highlight = 1;
		break;
	default:
        break;
	}
}


/* MouseMove
 *   Perform map window's WM_MOUSEMOVE processing.
 *
 * Parameters:
 *   MAPWINDATA*	lpmwd			Pointer to data for window.
 *   WORD			xMouse, yMouse	Mouse co-ordinates.
 *
 * Return value: LRESULT
 *   Value window procedure should return.
 */
static LRESULT MouseMove(MAPWINDATA *lpmwd, WORD xMouse, WORD yMouse)
{
	/* Not all WM_MOUSEMOVE messages are genuine. */
	if(lpmwd->xMouseLast == xMouse && lpmwd->yMouseLast == yMouse)
		return 0;

	/* Set the selected cursor. This is fast if it's already selected. */
	SetCursor(lpmwd->hCursor);

	/* Depending on the submode, we process mouse moves differently. */
	switch(lpmwd->submode)
	{
	case ESM_NONE:
		/* Update the highlight (only redraws if necessary). */
		if(UpdateHighlight(lpmwd, xMouse, yMouse))
		{
			/* If the highlight changed, update the info-bar. */
			MapWinUpdateInfoBar(lpmwd);
		}

		break;

	case ESM_MOVING:
		if(lpmwd->xMouseLast >= 0 && lpmwd->yMouseLast >= 0)
		{
			/* Pan the map. */
			lpmwd->mapview.xLeft += (lpmwd->xMouseLast - xMouse) / lpmwd->mapview.fZoom;
			lpmwd->mapview.yTop -= (lpmwd->yMouseLast - yMouse) / lpmwd->mapview.fZoom;

			lpmwd->bWantRedraw = TRUE;
		}

		break;

	case ESM_PREDRAG:
		{
			/* TODO: Move to its own function? */

			/* Create an undo marker, but don't clear the redo stack, since the
			 * user might cancel.
			 */
			CreateUndo(lpmwd, IDS_UNDO_DRAG);

			/* Convert to a selection of things and vertices in an extra
			 * structure, so as not to interfere with the real selection.
			 */
			CreateAuxiliarySelection(lpmwd);

			/* Mark all selected lines as dragged. */
			LabelSelectedLinesRS(lpmwd->lpmap);

			/* Find loops. */
			lpmwd->lplooplistRS = LabelRSLinesLoops(lpmwd->lpmap);

			/* Any lines inside any of our loops in their initial positions also
			 * need to be marked RS.
			 */
			LabelLoopedLinesRS(lpmwd->lpmap, lpmwd->lplooplistRS);

			/* Enter the dragging state. */
			lpmwd->submode = ESM_DRAGGING;

			/* TODO: Capture mouse? */
		}

		/* Fall through. */

	case ESM_PASTING:
	case ESM_DRAGGING:
		{
			short xDragNew = XMOUSE_WIN_TO_MAP(xMouse, lpmwd);
			short yDragNew = YMOUSE_WIN_TO_MAP(yMouse, lpmwd);
			int i;

			/* Do the snapping. */
			if((lpmwd->bySnapFlags ^ lpmwd->bySnapToggleMask) & SF_RECTANGLE)
			{
				short xUnsnapped = 0, yUnsnapped = 0;
				short xSnapped = 0, ySnapped = 0;

				/* Find the unsnapped opsition for the first selected item. */
				if(lpmwd->selectionAux.lpsellistVertices->iDataCount > 0)
				{
					xUnsnapped = lpmwd->lpmap->vertices[lpmwd->selectionAux.lpsellistVertices->lpiIndices[0]].x + xDragNew - lpmwd->xDragLast;
					yUnsnapped = lpmwd->lpmap->vertices[lpmwd->selectionAux.lpsellistVertices->lpiIndices[0]].y + yDragNew - lpmwd->yDragLast;
				}
				else if(lpmwd->selectionAux.lpsellistThings->iDataCount > 0)
				{
					xUnsnapped = lpmwd->lpmap->things[lpmwd->selectionAux.lpsellistThings->lpiIndices[0]].x + xDragNew - lpmwd->xDragLast;
					yUnsnapped = lpmwd->lpmap->things[lpmwd->selectionAux.lpsellistThings->lpiIndices[0]].y + yDragNew - lpmwd->yDragLast;
				}

				/* Calculate the corresponding snapped position. */
				xSnapped = xUnsnapped;
				ySnapped = yUnsnapped;
				Snap(&xSnapped, &ySnapped, lpmwd->mapview.cxGrid, lpmwd->mapview.cyGrid);

				/* Find the difference between the two. */
				xDragNew += xSnapped - xUnsnapped;
				yDragNew += ySnapped - yUnsnapped;
			}

			/* If we moved far enough to require moving stuff (i.e. far enough
			 * so that snapping doesn't keep us in place), update positions.
			 */
			if(xDragNew - lpmwd->xDragLast != 0 || yDragNew - lpmwd->yDragLast != 0)
			{
				/* Displace all selected vertices and things by the appropriate
				 * amount.
				 */
				for(i = 0; i < lpmwd->selectionAux.lpsellistVertices->iDataCount; i++)
				{
					lpmwd->lpmap->vertices[lpmwd->selectionAux.lpsellistVertices->lpiIndices[i]].x += xDragNew - lpmwd->xDragLast;
					lpmwd->lpmap->vertices[lpmwd->selectionAux.lpsellistVertices->lpiIndices[i]].y += yDragNew - lpmwd->yDragLast;
				}

				for(i = 0; i < lpmwd->selectionAux.lpsellistThings->iDataCount; i++)
				{
					lpmwd->lpmap->things[lpmwd->selectionAux.lpsellistThings->lpiIndices[i]].x += xDragNew - lpmwd->xDragLast;
					lpmwd->lpmap->things[lpmwd->selectionAux.lpsellistThings->lpiIndices[i]].y += yDragNew - lpmwd->yDragLast;
				}

				/* Update stored co-ordinates. */
				lpmwd->xDragLast = xDragNew;
				lpmwd->yDragLast = yDragNew;

				lpmwd->bWantRedraw = TRUE;
			}
		}

		break;

	case ESM_DRAWING:
	case ESM_INSERTING:
		/* Always redraw when drawing. */
		lpmwd->bWantRedraw = TRUE;
		break;		

	default:
        break;
	}

	/* Was the mouse not previously inside the window? */
	if(lpmwd->xMouseLast < 0 || lpmwd->yMouseLast < 0)
	{
		/* If so, track the pointer until it leaves. */
		TRACKMOUSEEVENT tme;

		tme.cbSize = sizeof(tme);
		tme.dwFlags = TME_LEAVE;
		tme.hwndTrack = lpmwd->hwnd;
		
		_TrackMouseEvent(&tme);
	}

	/* Save the cursor position. */
	lpmwd->xMouseLast = xMouse;
	lpmwd->yMouseLast = yMouse;

	UpdateStatusBar(lpmwd, SBPF_COORD);

	/* Make window proc return 0. */
	return 0;
}


/* LButtonDown
 *   Perform map window's WM_LBUTTONDOWN processing.
 *
 * Parameters:
 *   MAPWINDATA*	lpmwd			Pointer to data for window.
 *   WORD			xMouse, yMouse	Mouse co-ordinates.
 *   WORD			wFlags			Flags corresponding to WM_LBUTTONDOWN's
 *									wParam.
 *
 * Return value: LRESULT
 *   Value window procedure should return.
 */
static LRESULT LButtonDown(MAPWINDATA *lpmwd, WORD xMouse, WORD yMouse, WORD wFlags)
{
	UNREFERENCED_PARAMETER(xMouse);
	UNREFERENCED_PARAMETER(yMouse);
	UNREFERENCED_PARAMETER(wFlags);
	switch(lpmwd->submode)
	{
	case ESM_NONE:
		/* Normal processing if we're not doing anything special. */

		if(lpmwd->editmode == EM_MOVE)
		{
			StartPan(lpmwd);
		}
		else
		{
			/* Select/deselect an object if we're pointing at one. */
			if(lpmwd->highlightobj.emType != EM_MOVE)
			{
				/* If object isn't selected, add it to selection; remove it,
				 * otherwise.
				 */
				if(!ObjectSelected(lpmwd, &lpmwd->highlightobj))
					AddObjectToSelection(lpmwd, &lpmwd->highlightobj);
				else
					RemoveObjectFromSelection(lpmwd, &lpmwd->highlightobj);

				/* The info-bar will be updated when the highlight changes. */

				lpmwd->bWantRedraw = TRUE;
			}
			/* Not pointing at anything. */
			else if(ConfigGetInteger(g_lpcfgMain, "nothingdeselects"))
			{
				DeselectAll(lpmwd);
				MapWinUpdateInfoBar(lpmwd);
				lpmwd->bWantRedraw = TRUE;
			}
		}

		break;

	case ESM_MOVING:
		/* Stop panning. */
		EndPan(lpmwd);
		break;

	case ESM_PASTING:
		
		/* Always deselect following a prefab insertion. */
		lpmwd->bDeselectAfterEdit = TRUE;

		/* This also clears the redo stack. */
		EndDragOperation(lpmwd);

		break;

	default:
		break;
	}

	return 0;
}


/* LButtonUp
 *   Perform map window's WM_LBUTTONUP processing.
 *
 * Parameters:
 *   MAPWINDATA*	lpmwd			Pointer to data for window.
 *   WORD			xMouse, yMouse	Mouse co-ordinates.
 *   WORD			wFlags			Flags corresponding to WM_LBUTTONUP's
 *									wParam.
 *
 * Return value: LRESULT
 *   Value window procedure should return.
 */
static LRESULT LButtonUp(MAPWINDATA *lpmwd, WORD xMouse, WORD yMouse, WORD wFlags)
{
	UNREFERENCED_PARAMETER(wFlags);
	switch(lpmwd->submode)
	{
	case ESM_MOVING:
		/* Stop panning. */
		EndPan(lpmwd);
		break;

	case ESM_INSERTING:
		{
			short x = (short)XMOUSE_WIN_TO_MAP(xMouse, lpmwd);
			short y = (short)YMOUSE_WIN_TO_MAP(yMouse, lpmwd);

			if((lpmwd->bySnapFlags ^ lpmwd->bySnapToggleMask) & SF_RECTANGLE)
				Snap(&x, &y, lpmwd->mapview.cxGrid, lpmwd->mapview.cyGrid);

			/* Undo is handled in here. */
			DoInsertOperation(lpmwd, x, y);
			UpdateHighlight(lpmwd, lpmwd->xMouseLast, lpmwd->yMouseLast);
			lpmwd->bWantRedraw = TRUE;
		}

		break;

	case ESM_DRAWING:
		{
			short x = (short)XMOUSE_WIN_TO_MAP(xMouse, lpmwd);
			short y = (short)YMOUSE_WIN_TO_MAP(yMouse, lpmwd);

			if((lpmwd->bySnapFlags ^ lpmwd->bySnapToggleMask) & SF_RECTANGLE)
				Snap(&x, &y, lpmwd->mapview.cxGrid, lpmwd->mapview.cyGrid);

			/* Draw the next vertex. */
			if(!DrawToNewVertex(lpmwd->lpmap, &lpmwd->drawop, x, y, lpmwd->bStitchMode))
			{
				/* Drawing this vertex finishes the drawing operation. */
				EndDrawOperation(lpmwd->lpmap, &lpmwd->drawop);
				lpmwd->submode = ESM_NONE;

				ClearRedoStack(lpmwd);
				lpmwd->bMapChanged = TRUE;

				UpdateStatusBar(lpmwd, SBPF_LINEDEFS | SBPF_SIDEDEFS | SBPF_VERTICES | SBPF_SECTORS);
			}

			lpmwd->bWantRedraw = TRUE;
		}

		break;
	
	default:
        break;
	}

	return 0;
}



/* RButtonUp
 *   Perform map window's WM_RBUTTONUP processing.
 *
 * Parameters:
 *   MAPWINDATA*	lpmwd			Pointer to data for window.
 *   WORD			xMouse, yMouse	Mouse co-ordinates.
 *   WORD			wFlags			Flags corresponding to WM_RBUTTONUP's
 *									wParam.
 *
 * Return value: LRESULT
 *   Value window procedure should return.
 */
static LRESULT RButtonUp(MAPWINDATA *lpmwd, WORD xMouse, WORD yMouse, WORD wFlags)
{
	UNREFERENCED_PARAMETER(wFlags);
	switch(lpmwd->submode)
	{
	case ESM_INSERTING:		
		{
			short x = (short)XMOUSE_WIN_TO_MAP(xMouse, lpmwd);
			short y = (short)YMOUSE_WIN_TO_MAP(yMouse, lpmwd);

			if((lpmwd->bySnapFlags ^ lpmwd->bySnapToggleMask) & SF_RECTANGLE)
				Snap(&x, &y, lpmwd->mapview.cxGrid, lpmwd->mapview.cyGrid);

			/* Undo is handled in here. */
			DoInsertOperation(lpmwd, x, y);
			UpdateHighlight(lpmwd, lpmwd->xMouseLast, lpmwd->yMouseLast);
			lpmwd->bWantRedraw = TRUE;
		}

		break;

	case ESM_PREDRAG:
		/* If we get a mouse-up while still in this state, then the mouse didn't
		 * move (otherwise we'd've moved into ESM_DRAGGING). Hence, we should
		 * show the selection's properties.
		 */

		if(lpmwd->selection.lpsellistLinedefs->iDataCount > 0 ||
			lpmwd->selection.lpsellistSectors->iDataCount > 0 ||
			lpmwd->selection.lpsellistThings->iDataCount > 0 ||
			lpmwd->selection.lpsellistVertices->iDataCount > 0)
		{
			/* Selection properties. Display a context menu if the user likes
			 * that sort of thing; otherwise go straight to properties dialogue.
			 */
			if(ConfigGetInteger(g_lpcfgMain, "contextmenus"))
			{
				/* TODO: Context-menus. These depend on the mode *and* the
				 * nature of the selected objects, if indeed there are any.
				 */
			}
			else
			{
				/* Undo is all handled in here. */
				ShowSelectionProperties(lpmwd);
			}
		}

		/* Were the objects selected before we clicked on them? If not, deselect
		 * them.
		 */
		if(lpmwd->bDeselectAfterEdit) DeselectAll(lpmwd);

		/* Restore normal behaviour. */
		lpmwd->submode = ESM_NONE;
		lpmwd->bWantRedraw = TRUE;

		break;

	case ESM_DRAGGING:

		/* This also clears the redo stack. */
		EndDragOperation(lpmwd);

		break;
		
	/* Nothing happens in ESM_NONE: We're never pointing at anything, since that
	 * would have triggered a move to ESM_PREDRAG at the mouse-down stage.
	 */
	
	default:
	    break;
	}

	return 0;
}


/* RButtonDown
 *   Perform map window's WM_RBUTTONDOWN processing.
 *
 * Parameters:
 *   MAPWINDATA*	lpmwd			Pointer to data for window.
 *   WORD			xMouse, yMouse	Mouse co-ordinates.
 *   WORD			wFlags			Flags corresponding to WM_RBUTTONDOWN's
 *									wParam.
 *
 * Return value: LRESULT
 *   Value window procedure should return.
 */
static LRESULT RButtonDown(MAPWINDATA *lpmwd, WORD xMouse, WORD yMouse, WORD wFlags)
{
	UNREFERENCED_PARAMETER(xMouse);
	UNREFERENCED_PARAMETER(yMouse);
	UNREFERENCED_PARAMETER(wFlags);
	switch(lpmwd->submode)
	{
	case ESM_NONE:
		/* Not pointing at anything? */
		if(lpmwd->highlightobj.emType == EM_MOVE)
		{
			/* Begin insertion, of a type proper to the mode. */
			switch(lpmwd->editmode)
			{
			case EM_ANY:
			case EM_LINES:
			case EM_SECTORS:
			case EM_VERTICES:
				/* This is a prelude to ESM_DRAWING. Also, note that the map
				 * hasn't changed yet!
				 */
				DeselectAll(lpmwd);
				lpmwd->submode = ESM_INSERTING;
				BeginDrawOperation(&lpmwd->drawop);

				break;

			case EM_THINGS:
				DeselectAll(lpmwd);
				lpmwd->submode = ESM_INSERTING;
				break;
				
			default:
                break;
			}

			lpmwd->bWantRedraw = TRUE;
		}
		else
		{
			/* We are pointing at something, so enter into the I'm-either-going-
			 * to-drag-you-or-alter-your-properties state, after altering the
			 * selection as necessary.
			 */

			/* If the object was already selected, great; otherwise clear the
			 * existing selection and then select the new object.
			 */
			if(!ObjectSelected(lpmwd, &lpmwd->highlightobj))
			{
				DeselectAll(lpmwd);				
				AddObjectToSelection(lpmwd, &lpmwd->highlightobj);
				lpmwd->bDeselectAfterEdit = TRUE;
			}
			else
				lpmwd->bDeselectAfterEdit = FALSE;

			/* Clear the highlight, but save what we're pointing at first. */
			lpmwd->hobjDrag = lpmwd->highlightobj;
			if(UpdateHighlight(lpmwd, -1, -1))
				MapWinUpdateInfoBar(lpmwd);

			/* Store the current map co-ordinates of the mouse. These are reset
			 * with every mouse move.
			 */
			lpmwd->xDragLast = XMOUSE_WIN_TO_MAP(lpmwd->xMouseLast, lpmwd);
			lpmwd->yDragLast = YMOUSE_WIN_TO_MAP(lpmwd->yMouseLast, lpmwd);

			/* Set the state and redraw. */
			lpmwd->submode = ESM_PREDRAG;
			lpmwd->bWantRedraw = TRUE;
		}

		break;
	
	default:
        break;
	}

	return 0;
}


/* ObjectSelected
 *   Determines whether a certain object is selected.
 *
 * Parameters:
 *   MAPWINDATA*	lpmwd			Pointer to data for window.
 *   HIGHLIGHTOBJ*	lphighlightobj	Type and index of object.
 *
 * Return value: BOOL
 *   True if the object is selected; FALSE otherwise.
 */
static BOOL ObjectSelected(MAPWINDATA *lpmwd, HIGHLIGHTOBJ *lphighlightobj)
{
	switch(lphighlightobj->emType)
	{
	case EM_SECTORS:
		return ExistsInSelectionList(lpmwd->selection.lpsellistSectors, lphighlightobj->iIndex);
	case EM_LINES:
		return ExistsInSelectionList(lpmwd->selection.lpsellistLinedefs, lphighlightobj->iIndex);
	case EM_VERTICES:
		return ExistsInSelectionList(lpmwd->selection.lpsellistVertices, lphighlightobj->iIndex);
	case EM_THINGS:
		return ExistsInSelectionList(lpmwd->selection.lpsellistThings, lphighlightobj->iIndex);	
	default:
        break;
	}

	return FALSE;
}



/* AddObjectToSelection
 *   Selects an object.
 *
 * Parameters:
 *   MAPWINDATA*	lpmwd			Pointer to data for window.
 *   HIGHLIGHTOBJ*	lphighlightobj	Type and index of object.
 *
 * Return value: None.
 *
 * Remarks:
 *   The selection fields in the map data are also updated.
 */
static void AddObjectToSelection(MAPWINDATA *lpmwd, HIGHLIGHTOBJ *lphighlightobj)
{
	MAP *lpmap = lpmwd->lpmap;

	switch(lphighlightobj->emType)
	{
	case EM_SECTORS:
		{
			int i;

			AddToSelectionList(lpmwd->selection.lpsellistSectors, lphighlightobj->iIndex);
			lpmap->sectors[lphighlightobj->iIndex].selected = 1;

			/* Select and mark linedefs for this sector, but don't trigger any
			 * of the effects that would occur if the user selected them
			 * manually.
			 */
			for(i = 0; i < lpmap->iLinedefs; i++)
			{
				if((lpmap->linedefs[i].s1 >= 0 && lpmap->sidedefs[lpmap->linedefs[i].s1].sector == lphighlightobj->iIndex) ||
					(lpmap->linedefs[i].s2 >= 0 && lpmap->sidedefs[lpmap->linedefs[i].s2].sector == lphighlightobj->iIndex))
				{
					AddToSelectionList(lpmwd->selection.lpsellistLinedefs, i);
					lpmap->linedefs[i].selected = 1;
				}
			}
		}

		break;

	case EM_LINES:

		{
			int iFrontSec, iBackSec;
			int i;
			BOOL bAddFront, bAddBack;
			
			if(!lpmap->linedefs[lphighlightobj->iIndex].selected)
			{
				AddToSelectionList(lpmwd->selection.lpsellistLinedefs, lphighlightobj->iIndex);
				lpmap->linedefs[lphighlightobj->iIndex].selected = 1;
			}

			/* Did adding this linedef cause a complete sector to be selected?
			 * This takes a bit of working out, but if it did, add it. We need
			 * to check both front and back sectors. Cf. SelectSectorsFromLines,
			 * which rebuilds the whole lot; this is more efficient, though.
			 */
			iFrontSec = lpmap->linedefs[lphighlightobj->iIndex].s1 >= 0 ? lpmap->sidedefs[lpmap->linedefs[lphighlightobj->iIndex].s1].sector : -1;
			iBackSec = lpmap->linedefs[lphighlightobj->iIndex].s2 >= 0 ? lpmap->sidedefs[lpmap->linedefs[lphighlightobj->iIndex].s2].sector : -1;

			/* Assume that we add them, if they exist. */
			bAddFront = (iFrontSec >= 0);
			bAddBack = (iBackSec >= 0);

			/* Look through all sidedefs attached to linedefs; if we find an
			 * unselected sd referencing one of our sectors, we don't add it.
			 */
			for(i = 0; (bAddFront || bAddBack) && i < lpmap->iLinedefs; i++)
			{
				MAPLINEDEF *lpld = &lpmap->linedefs[i];
				if(!lpld->selected)
				{
					if(lpld->s1 >= 0)
					{
						int iSector = lpmap->sidedefs[lpld->s1].sector;
						if(iSector == iFrontSec) bAddFront = FALSE;
						if(iSector == iBackSec) bAddBack = FALSE;
					}

					if(lpld->s2 >= 0)
					{
						int iSector = lpmap->sidedefs[lpld->s2].sector;
						if(iSector == iFrontSec) bAddFront = FALSE;
						if(iSector == iBackSec) bAddBack = FALSE;
					}
				}
			}

			/* Add the sectors if we should. */
			if(bAddFront)
			{
				AddToSelectionList(lpmwd->selection.lpsellistSectors, iFrontSec);
				lpmap->sectors[iFrontSec].selected = 1;
			}

			if(bAddBack)
			{
				AddToSelectionList(lpmwd->selection.lpsellistSectors, iBackSec);
				lpmap->sectors[iBackSec].selected = 1;
			}
		}

		break;

	case EM_VERTICES:

		AddToSelectionList(lpmwd->selection.lpsellistVertices, lphighlightobj->iIndex);
		lpmap->vertices[lphighlightobj->iIndex].selected = 1;
		break;

	case EM_THINGS:

		AddToSelectionList(lpmwd->selection.lpsellistThings, lphighlightobj->iIndex);
		lpmap->things[lphighlightobj->iIndex].selected = 1;
		break;
		
	default:
        break;
	}
}



/* RemoveObjectFromSelection
 *   Deselects an object.
 *
 * Parameters:
 *   MAPWINDATA*	lpmwd			Pointer to data for window.
 *   HIGHLIGHTOBJ*	lphighlightobj	Type and index of object.
 *
 * Return value: BOOL
 *   TRUE if object was indeed previously selected; FALSE otherwise.
 *
 * Remarks:
 *   The selection fields in the map data are also updated.
 */
static BOOL RemoveObjectFromSelection(MAPWINDATA *lpmwd, HIGHLIGHTOBJ *lphighlightobj)
{
	BOOL bRet = FALSE;
	MAP *lpmap = lpmwd->lpmap;
	int i;

	switch(lphighlightobj->emType)
	{
	case EM_SECTORS:

		bRet = RemoveFromSelectionList(lpmwd->selection.lpsellistSectors, lphighlightobj->iIndex);
		lpmap->sectors[lphighlightobj->iIndex].selected = 0;

		/* Unmark linedefs for this sector. */
		for(i = 0; i < lpmap->iLinedefs; i++)
		{
			/* This is not particularly transparent. We deselect the linedef iff
			 * it belongs to us and the sector on its opposite side is
			 * unselected.
			 */
			if(((lpmap->linedefs[i].s1 >= 0 && lpmap->sidedefs[lpmap->linedefs[i].s1].sector == lphighlightobj->iIndex) &&
				!(lpmap->linedefs[i].s2 >= 0 && ExistsInSelectionList(lpmwd->selection.lpsellistSectors, lpmap->sidedefs[lpmap->linedefs[i].s2].sector)))
			|| ((lpmap->linedefs[i].s2 >= 0 && lpmap->sidedefs[lpmap->linedefs[i].s2].sector == lphighlightobj->iIndex) &&
				!(lpmap->linedefs[i].s1 >= 0 && ExistsInSelectionList(lpmwd->selection.lpsellistSectors, lpmap->sidedefs[lpmap->linedefs[i].s1].sector))))
			{
				RemoveFromSelectionList(lpmwd->selection.lpsellistLinedefs, i);
				lpmap->linedefs[i].selected = 0;
			}
		}

		break;

	case EM_LINES:
		RemoveFromSelectionList(lpmwd->selection.lpsellistLinedefs, lphighlightobj->iIndex);
		lpmap->linedefs[lphighlightobj->iIndex].selected = 0;

		/* Remove any sectors that might have become unselected. We don't
		 * actually check whether they *were* selected: it wouldn't be any
		 * quicker.
		 */
		if(lpmap->linedefs[lphighlightobj->iIndex].s1 >= 0)
		{
			int iSector = lpmap->sidedefs[lpmap->linedefs[lphighlightobj->iIndex].s1].sector;
			if(iSector >= 0) RemoveFromSelectionList(lpmwd->selection.lpsellistSectors, iSector);
		}

		if(lpmap->linedefs[lphighlightobj->iIndex].s2 >= 0)
		{
			int iSector = lpmap->sidedefs[lpmap->linedefs[lphighlightobj->iIndex].s2].sector;
			if(iSector >= 0) RemoveFromSelectionList(lpmwd->selection.lpsellistSectors, iSector);
		}

		break;
	case EM_VERTICES:
		RemoveFromSelectionList(lpmwd->selection.lpsellistVertices, lphighlightobj->iIndex);
		lpmap->vertices[lphighlightobj->iIndex].selected = 0;
		break;
	case EM_THINGS:
		RemoveFromSelectionList(lpmwd->selection.lpsellistThings, lphighlightobj->iIndex);
		lpmap->things[lphighlightobj->iIndex].selected = 0;
		break;
	default:
        break;
	}

	return bRet;
}


/* SelectVerticesFromLines
 *   Selects those vertices belonging to selected lines.
 *
 * Parameters:
 *   MAPWINDATA*	lpmwd	Pointer to map window data.
 *
 * Return value: None.
 *
 * Remarks:
 *   Doesn't deselect any vertices that are already selected.
 */
static void SelectVerticesFromLines(MAPWINDATA *lpmwd)
{
	int i;

	for(i = 0; i < lpmwd->lpmap->iLinedefs; i++)
	{
		MAPLINEDEF *lpld = &lpmwd->lpmap->linedefs[i];
		
		if(lpld->selected)
		{
			AddToSelectionList(lpmwd->selection.lpsellistVertices, lpld->v1);
			lpmwd->lpmap->vertices[lpld->v1].selected = 1;
			AddToSelectionList(lpmwd->selection.lpsellistVertices, lpld->v2);
			lpmwd->lpmap->vertices[lpld->v2].selected = 1;
		}
	}
}


/* SelectLinesFromVertices
 *   Selects those lines whose vertices are both selected.
 *
 * Parameters:
 *   MAPWINDATA*	lpmwd	Pointer to map window data.
 *
 * Return value: None.
 *
 * Remarks:
 *   Doesn't deselect any lines that are already selected. Doesn't trigger
 *   selection of any sectors that should become selected.
 */
static void SelectLinesFromVertices(MAPWINDATA *lpmwd)
{
	int i;

	for(i = 0; i < lpmwd->lpmap->iLinedefs; i++)
	{
		MAPLINEDEF *lpld = &lpmwd->lpmap->linedefs[i];
		
		if(lpmwd->lpmap->vertices[lpld->v1].selected && lpmwd->lpmap->vertices[lpld->v2].selected)
		{
			AddToSelectionList(lpmwd->selection.lpsellistLinedefs, i);
			lpld->selected = 1;
		}
	}
}


/* SelectSectorsFromLines
 *   Selects those sectors whose linedefs are all selected.
 *
 * Parameters:
 *   MAPWINDATA*	lpmwd	Pointer to map window data.
 *
 * Return value: None.
 *
 * Remarks:
 *   Doesn't deselect any sectors that are already selected.
 */
static void SelectSectorsFromLines(MAPWINDATA *lpmwd)
{
	MAP *lpmap = lpmwd->lpmap;
	int i;
	char *lpcSelectSector;

	/* Sanity check. */
	if(lpmap->iSectors == 0) return;

	/* Begin by indicating that it's possible to select all of the sectors. */
	lpcSelectSector = (char*)ProcHeapAlloc(lpmap->iSectors * sizeof(char));
	ZeroMemory(lpcSelectSector, lpmap->iSectors * sizeof(char));

	/* Consider all linedefs. */
	for(i = 0; i < lpmap->iLinedefs; i++)
	{
		MAPLINEDEF *lpld = &lpmap->linedefs[i];
		int iFrontSec = lpld->s1 >= 0 ? lpmwd->lpmap->sidedefs[lpld->s1].sector : -1;	
		int iBackSec = lpld->s2 >= 0 ? lpmwd->lpmap->sidedefs[lpld->s2].sector : -1;

		if(!lpld->selected)
		{
			/* We definitely don't want to select any sectors belonging to this
			 * line.
			 */
			if(iFrontSec >= 0) lpcSelectSector[iFrontSec] = -1;
			if(iBackSec >= 0) lpcSelectSector[iBackSec] = -1;
		}
		else
		{
			/* Unless we already know we can never select this sector, assume
			 * that we can select it.
			 */
			if(iFrontSec >= 0 && lpcSelectSector[iFrontSec] >= 0) lpcSelectSector[iFrontSec] = 1;
			if(iBackSec >= 0 && lpcSelectSector[iBackSec] >= 0) lpcSelectSector[iBackSec] = 1;
		}
	}

	/* Select all the sectors that we ought to. */
	for(i = 0; i < lpmap->iSectors; i++)
	{
		if(lpcSelectSector[i] > 0 && !lpmap->sectors[i].selected)
		{
			AddToSelectionList(lpmwd->selection.lpsellistSectors, i);
			lpmap->sectors[i].selected = 1;
		}
	}

	ProcHeapFree(lpcSelectSector);
}


/* DeselectAll
 *   Deselects all map objects.
 *
 * Parameters:
 *   MAPWINDATA*	lpmwd	Pointer to map window data.
 *
 * Return value: None.
 */
static void DeselectAll(MAPWINDATA *lpmwd)
{
	if(lpmwd->editmode == EM_ANY || lpmwd->editmode == EM_SECTORS || lpmwd->editmode == EM_LINES)
	{
		DeselectAllLines(lpmwd);
		DeselectAllSectors(lpmwd);
	}

	if(lpmwd->editmode == EM_ANY || lpmwd->editmode == EM_THINGS)
		DeselectAllThings(lpmwd);

	if(lpmwd->editmode == EM_ANY || lpmwd->editmode == EM_VERTICES)
		DeselectAllVertices(lpmwd);
}


/* DeselectAllBruteForce
 *   Deselects everything without trying to be clever. Useful for when the
 *   selection can be thrown out of sync, e.g. after an undo.
 *
 * Parameters:
 *   MAPWINDATA*	lpmwd	Pointer to map window data.
 *
 * Return value: None.
 */
static void DeselectAllBruteForce(MAPWINDATA *lpmwd)
{
	ClearSelectionList(lpmwd->selection.lpsellistSectors);
	ClearSelectionList(lpmwd->selection.lpsellistLinedefs);
	ClearSelectionList(lpmwd->selection.lpsellistThings);
	ClearSelectionList(lpmwd->selection.lpsellistVertices);

	ResetSelections(NULL, 0, NULL, 0, NULL, 0, lpmwd->lpmap->sectors, lpmwd->lpmap->iSectors);
	ResetSelections(NULL, 0, lpmwd->lpmap->linedefs, lpmwd->lpmap->iLinedefs, NULL, 0, NULL, 0);
	ResetSelections(lpmwd->lpmap->things, lpmwd->lpmap->iThings, NULL, 0, NULL, 0, NULL, 0);
	ResetSelections(NULL, 0, NULL, 0, lpmwd->lpmap->vertices, lpmwd->lpmap->iVertices, NULL, 0);
}


/* DeselectAllLines etc.
 *   Deselects all map objects of a specified type. Doesn't do any associated
 *   deselections, e.g. between lines and sectors.
 *
 * Parameters:
 *   MAPWINDATA*	lpmwd	Pointer to map window data.
 *
 * Return value: None.
 */
static void DeselectAllLines(MAPWINDATA *lpmwd)
{
	ClearSelectionList(lpmwd->selection.lpsellistSectors);
	ResetSelections(NULL, 0, NULL, 0, NULL, 0, lpmwd->lpmap->sectors, lpmwd->lpmap->iSectors);
}

static void DeselectAllSectors(MAPWINDATA *lpmwd)
{
	ClearSelectionList(lpmwd->selection.lpsellistLinedefs);
	ResetSelections(NULL, 0, lpmwd->lpmap->linedefs, lpmwd->lpmap->iLinedefs, NULL, 0, NULL, 0);
}

static void DeselectAllThings(MAPWINDATA *lpmwd)
{
	ClearSelectionList(lpmwd->selection.lpsellistThings);
	ResetSelections(lpmwd->lpmap->things, lpmwd->lpmap->iThings, NULL, 0, NULL, 0, NULL, 0);
}

static void DeselectAllVertices(MAPWINDATA *lpmwd)
{
	ClearSelectionList(lpmwd->selection.lpsellistVertices);
	ResetSelections(NULL, 0, NULL, 0, lpmwd->lpmap->vertices, lpmwd->lpmap->iVertices, NULL, 0);
}


/* DeselectLinesFromPartialSectors
 *   Deselects any lines that do not belong to a selected sector.
 *
 * Parameters:
 *   MAPWINDATA*	lpmwd	Pointer to map window data.
 *
 * Return value: None.
 */
static void DeselectLinesFromPartialSectors(MAPWINDATA *lpmwd)
{
	int i;

	for(i = 0; i < lpmwd->lpmap->iLinedefs; i++)
	{
		MAPLINEDEF *lpld = &lpmwd->lpmap->linedefs[i];
		
		if(lpld->selected)
		{
			int iFrontSec = lpld->s1 >= 0 ? lpmwd->lpmap->sidedefs[lpld->s1].sector : -1;	
			int iBackSec = lpld->s2 >= 0 ? lpmwd->lpmap->sidedefs[lpld->s2].sector : -1;

			if((iFrontSec < 0 || !lpmwd->lpmap->sectors[iFrontSec].selected) &&
				(iBackSec < 0 || !lpmwd->lpmap->sectors[iBackSec].selected)) 
			{
				RemoveFromSelectionList(lpmwd->selection.lpsellistLinedefs, i);
				lpld->selected = 0;
			}
		}
	}
}

/* UpdateMapWindowTitle
 *   Updates a map window's title to include path and lumpname.
 *
 * Parameters:
 *   MAPWINDATA*	lpmwd	Pointer to map window data.
 *
 * Return value: None.
 */
static void UpdateMapWindowTitle(MAPWINDATA *lpmwd)
{
	LPTSTR	szTitle;
	TCHAR	szTail[12];		/* With lumpname corrected for ANSI/UNICODE-ness. */

	/* Allocate buffer for full path, and get it. */
	if(!WadIsNewByID(lpmwd->iWadID))
	{
		LPTSTR	szPath;
		WORD	cb;
		cb = GetWadPath(lpmwd->iWadID, NULL, 0);
		szPath = (LPTSTR)ProcHeapAlloc(cb * sizeof(TCHAR));
		GetWadPath(lpmwd->iWadID, szPath, cb);

		/* Allocate buffer for title (including tail), and get file title. */
		cb = GetFileTitle(szPath, NULL, 0);
		szTitle = (LPTSTR)ProcHeapAlloc((cb + strlen(lpmwd->szLumpName) + 3) * sizeof(TCHAR));
		GetFileTitle(szPath, szTitle, cb);
		ProcHeapFree(szPath);
	}
	else
	{
		szTitle = (LPTSTR)ProcHeapAlloc((UNTITLED_BUFLEN + strlen(lpmwd->szLumpName) + 3) * sizeof(TCHAR));
		LoadString(g_hInstance, IDS_UNTITLED, szTitle, UNTITLED_BUFLEN);
	}

	/* Get tail, including corrected lumpname. */
	wsprintf(szTail, TEXT(" - %hs"), lpmwd->szLumpName);
	lstrcat(szTitle, szTail);

	SetWindowText(lpmwd->hwnd, szTitle);

	ProcHeapFree(szTitle);
}


static void BuildSectorTexturePreviews(MAPWINDATA *lpmwd, SECTORDISPLAYINFO *lpsdi, DWORD dwDisplayFlags)
{
	/* Begin by assuming we need to destroy no textures. */
	lpsdi->dwDestroyTexFlags = 0;

	if(dwDisplayFlags & SDIF_CEILINGTEX)
	{
		if(GetTextureForMap(lpmwd->hwnd, lpsdi->szCeiling, &lpsdi->lptexCeiling, TF_FLAT))
			lpsdi->dwDestroyTexFlags |= SDIF_CEILINGTEX;
	}
	else
		lpsdi->lptexCeiling = NULL;

	if(dwDisplayFlags & SDIF_FLOORTEX)
	{
		if(GetTextureForMap(lpmwd->hwnd, lpsdi->szFloor, &lpsdi->lptexFloor, TF_FLAT))
			lpsdi->dwDestroyTexFlags |= SDIF_FLOORTEX;
	}
	else
		lpsdi->lptexFloor = NULL;
}


static void DestroySectorTexturePreviews(SECTORDISPLAYINFO *lpsdi, DWORD dwDisplayFlags)
{
	if(lpsdi->lptexCeiling && (dwDisplayFlags & SDIF_CEILINGTEX) && (lpsdi->dwDestroyTexFlags & SDIF_CEILINGTEX))
		DestroyTexture(lpsdi->lptexCeiling);

	if(lpsdi->lptexFloor && (dwDisplayFlags & SDIF_FLOORTEX) && (lpsdi->dwDestroyTexFlags & SDIF_FLOORTEX))
		DestroyTexture(lpsdi->lptexFloor);
}



static void BuildThingSpritePreview(MAPWINDATA *lpmwd, THINGDISPLAYINFO *lptdi, DWORD dwDisplayFlags)
{
	/* Begin by assuming we need to destroy no textures. */
	lptdi->dwDestroyTexFlags = 0;

	if(dwDisplayFlags & TDIF_SPRITE)
	{
		if(GetTextureForMap(lpmwd->hwnd, lptdi->szSprite, &lptdi->lptexSprite, TF_IMAGE))
			lptdi->dwDestroyTexFlags |= TDIF_SPRITE;
	}
	else
		lptdi->lptexSprite = NULL;
}


static void DestroyThingSpritePreview(THINGDISPLAYINFO *lptdi, DWORD dwDisplayFlags)
{
	if(lptdi->lptexSprite && (dwDisplayFlags & TDIF_SPRITE) && (lptdi->dwDestroyTexFlags & TDIF_SPRITE))
		DestroyTexture(lptdi->lptexSprite);
}



static void BuildSidedefTexturePreviews(MAPWINDATA *lpmwd, LINEDEFDISPLAYINFO *lplddi, DWORD dwDisplayFlags)
{
	/* Begin by assuming we need to destroy no textures. */
	lplddi->dwDestroyTexFlags = 0;

	/* Same for the pseudo- and blank textures. */
	lplddi->dwPseudoTexFlags = 0;
	lplddi->dwBlankTexFlags = 0;

	if(dwDisplayFlags & LDDIF_FRONTSD)
	{
		if(dwDisplayFlags & LDDIF_FRONTUPPER)
		{
			if(IsPseudoTexture(lplddi->szFrontUpper))
			{
				lplddi->lptexFrontUpper = NULL;
				lplddi->dwPseudoTexFlags |= LDDIF_FRONTUPPER;
			}
			else if(IsBlankTexture(lplddi->szFrontUpper))
			{
				lplddi->lptexFrontUpper = NULL;
				lplddi->dwBlankTexFlags |= LDDIF_FRONTUPPER;
			}
			else if(GetTextureForMap(lpmwd->hwnd, lplddi->szFrontUpper, &lplddi->lptexFrontUpper, TF_TEXTURE))
				lplddi->dwDestroyTexFlags |= LDDIF_FRONTUPPER;
		}
		else
			lplddi->lptexFrontUpper = NULL;

		if(dwDisplayFlags & LDDIF_FRONTMIDDLE)
		{
			if(IsPseudoTexture(lplddi->szFrontMiddle))
			{
				lplddi->lptexFrontMiddle = NULL;
				lplddi->dwPseudoTexFlags |= LDDIF_FRONTMIDDLE;
			}
			else if(IsBlankTexture(lplddi->szFrontMiddle))
			{
				lplddi->lptexFrontMiddle = NULL;
				lplddi->dwBlankTexFlags |= LDDIF_FRONTMIDDLE;
			}
			else if(GetTextureForMap(lpmwd->hwnd, lplddi->szFrontMiddle, &lplddi->lptexFrontMiddle, TF_TEXTURE))
				lplddi->dwDestroyTexFlags |= LDDIF_FRONTMIDDLE;
		}
		else
			lplddi->lptexFrontMiddle = NULL;

		if(dwDisplayFlags & LDDIF_FRONTLOWER)
		{
			if(IsPseudoTexture(lplddi->szFrontLower))
			{
				lplddi->lptexFrontLower = NULL;
				lplddi->dwPseudoTexFlags |= LDDIF_FRONTLOWER;
			}
			else if(IsBlankTexture(lplddi->szFrontLower))
			{
				lplddi->lptexFrontLower = NULL;
				lplddi->dwBlankTexFlags |= LDDIF_FRONTLOWER;
			}
			else if(GetTextureForMap(lpmwd->hwnd, lplddi->szFrontLower, &lplddi->lptexFrontLower, TF_TEXTURE))
				lplddi->dwDestroyTexFlags |= LDDIF_FRONTLOWER;
		}
		else
			lplddi->lptexFrontLower = NULL;
	}
	else lplddi->lptexFrontUpper = lplddi->lptexFrontMiddle = lplddi->lptexFrontLower = NULL;


	if(dwDisplayFlags & LDDIF_BACKSD)
	{
		if(dwDisplayFlags & LDDIF_BACKUPPER)
		{
			if(IsPseudoTexture(lplddi->szBackUpper))
			{
				lplddi->lptexBackUpper = NULL;
				lplddi->dwPseudoTexFlags |= LDDIF_BACKUPPER;
			}
			else if(IsBlankTexture(lplddi->szBackUpper))
			{
				lplddi->lptexBackUpper = NULL;
				lplddi->dwBlankTexFlags |= LDDIF_BACKUPPER;
			}
			else if(GetTextureForMap(lpmwd->hwnd, lplddi->szBackUpper, &lplddi->lptexBackUpper, TF_TEXTURE))
				lplddi->dwDestroyTexFlags |= LDDIF_BACKUPPER;
		}
		else
			lplddi->lptexBackUpper = NULL;

		if(dwDisplayFlags & LDDIF_BACKMIDDLE)
		{
			if(IsPseudoTexture(lplddi->szBackMiddle))
			{
				lplddi->lptexBackMiddle = NULL;
				lplddi->dwPseudoTexFlags |= LDDIF_BACKMIDDLE;
			}
			else if(IsBlankTexture(lplddi->szBackMiddle))
			{
				lplddi->lptexBackMiddle = NULL;
				lplddi->dwBlankTexFlags |= LDDIF_BACKMIDDLE;
			}
			else if(GetTextureForMap(lpmwd->hwnd, lplddi->szBackMiddle, &lplddi->lptexBackMiddle, TF_TEXTURE))
				lplddi->dwDestroyTexFlags |= LDDIF_BACKMIDDLE;
		}
		else
			lplddi->lptexBackMiddle = NULL;

		if(dwDisplayFlags & LDDIF_BACKLOWER)
		{
			if(IsPseudoTexture(lplddi->szBackLower))
			{
				lplddi->lptexBackLower = NULL;
				lplddi->dwPseudoTexFlags |= LDDIF_BACKLOWER;
			}
			else if(IsBlankTexture(lplddi->szBackLower))
			{
				lplddi->lptexBackLower = NULL;
				lplddi->dwBlankTexFlags |= LDDIF_BACKLOWER;
			}
			else if(GetTextureForMap(lpmwd->hwnd, lplddi->szBackLower, &lplddi->lptexBackLower, TF_TEXTURE))
				lplddi->dwDestroyTexFlags |= LDDIF_BACKLOWER;
		}
		else
			lplddi->lptexBackLower = NULL;
	}
	else lplddi->lptexBackUpper = lplddi->lptexBackMiddle = lplddi->lptexBackLower = NULL;
}


static void DestroySidedefTexturePreviews(LINEDEFDISPLAYINFO *lplddi, DWORD dwDisplayFlags)
{
	if(lplddi->lptexFrontUpper && (dwDisplayFlags & LDDIF_FRONTUPPER) && (lplddi->dwDestroyTexFlags & LDDIF_FRONTUPPER))
		DestroyTexture(lplddi->lptexFrontUpper);

	if(lplddi->lptexFrontMiddle && (dwDisplayFlags & LDDIF_FRONTMIDDLE) && (lplddi->dwDestroyTexFlags & LDDIF_FRONTMIDDLE))
		DestroyTexture(lplddi->lptexFrontMiddle);

	if(lplddi->lptexFrontLower && (dwDisplayFlags & LDDIF_FRONTLOWER) && (lplddi->dwDestroyTexFlags & LDDIF_FRONTLOWER))
		DestroyTexture(lplddi->lptexFrontLower);


	if(lplddi->lptexBackUpper && (dwDisplayFlags & LDDIF_BACKUPPER) && (lplddi->dwDestroyTexFlags & LDDIF_BACKUPPER))
		DestroyTexture(lplddi->lptexBackUpper);

	if(lplddi->lptexBackMiddle && (dwDisplayFlags & LDDIF_BACKMIDDLE) && (lplddi->dwDestroyTexFlags & LDDIF_BACKMIDDLE))
		DestroyTexture(lplddi->lptexBackMiddle);

	if(lplddi->lptexBackLower && (dwDisplayFlags & LDDIF_BACKLOWER) && (lplddi->dwDestroyTexFlags & LDDIF_BACKLOWER))
		DestroyTexture(lplddi->lptexBackLower);
}





/* GetTextureForMap
 *   Loads a texture from cache if possible, and then from a wad if not.
 *
 * Parameters:
 *   MAPWINDATA*	lpmwd		Map window data.
 *   LPTSTR			szTexName	Lumpname of texture.
 *   TEXTURE**		lplptex		Buffer in which to return texture. NULL if not
 *								found.
 *   TEX_FORMAT		tf			Specifies whether texture or flat.
 *
 * Return value: BOOL
 *   TRUE if memory was allocated for the texture; FALSE if not.
 *
 * Remarks:
 *   The caller must call DestroyTexture if we return TRUE.
 */
BOOL GetTextureForMap(HWND hwnd, LPCTSTR szTexName, TEXTURE **lplptex, TEX_FORMAT tf)
{
	MAPWINDATA *lpmwd = (MAPWINDATA*)GetWindowLong(hwnd, GWL_USERDATA);
	TEXCACHE *lptcHdr;

	/* Search the appropriate cache first. The WaitForSingleObject call
	 * determines whether we can write to the cache.
	 */
	switch(tf)
	{
	case TF_FLAT:
		lptcHdr = &lpmwd->tcFlatsHdr;
		*lplptex = GetTextureFromCache(lptcHdr, szTexName);
		break;
	case TF_TEXTURE:
		lptcHdr = &lpmwd->tcTexturesHdr;
		*lplptex = GetTextureFromCache(lptcHdr, szTexName);
		break;
	default:
		/* We don't cache TF_IMAGEs. */
		lptcHdr = NULL;
		break;
	}

	if(lptcHdr)
		*lplptex = GetTextureFromCache(lptcHdr, szTexName);
	else
		*lplptex = NULL;

	/* Found in cache? */
	if(*lplptex)
	{
		/* We're finished, and we didn't allocate any memory. */
		return FALSE;
	}

	/* Fall back to wad-files, and add to the cache once found. */
	if(lpmwd->iAdditionalWadID >= 0)
	{
		*lplptex = LoadTexture(GetWad(lpmwd->iAdditionalWadID), szTexName, tf, lpmwd->rgbqPalette);
		if(lptcHdr && *lplptex) AddToTextureCache(lptcHdr, szTexName, *lplptex);
	}

	if(!(*lplptex) && lpmwd->iIWadID >= 0)
	{
		*lplptex = LoadTexture(lpmwd->lpwadIWad, szTexName, tf, lpmwd->rgbqPalette);
		if(lptcHdr && *lplptex) AddToTextureCache(lptcHdr, szTexName, *lplptex);
	}

	/* If the texture is non-null and an image, the caller needs to free it. */
	return *lplptex && tf == TF_IMAGE;
}



/* IsPseudoTexture, IsBlankTexture
 *   Determines whether a texture that does not actually exist should
 *   nonetheless not be reported as missing, or whether it corresponds to the
 *   blank texture.
 *
 * Parameters:
 *   LPCTSTR		szTexName	Name of texture.
 *
 * Return value: BOOL
 *   TRUE if pseudo-texture; FALSE if not.
 *
 * Remarks:
 *   Pseudo-textures are useful for things like F_SKY1 and colourmaps. The blank
 *   texture is the one that's normally "-".
 */

#ifdef _UNICODE
static BOOL IsPseudoTextureW(LPCWSTR szTexName)
{
	char szTexNameA[9];
	wsprintfA(szTexNameA, "%ls", szTexName);
	return IsPseudoTextureA(szTexNameA);
}
#endif

static BOOL IsPseudoTextureA(LPCSTR szTexName)
{
	UNREFERENCED_PARAMETER(szTexName);
	/* TODO: Check for pseudo-textures from config. */
	return FALSE;
}

#ifdef _UNICODE
static BOOL IsBlankTextureW(LPCWSTR szTexName)
{
	char szTexNameA[9];
	wsprintfA(szTexNameA, "%ls", szTexName);
	return IsBlankTextureA(szTexNameA);
}
#endif

static BOOL IsBlankTextureA(LPCSTR szTexName)
{
	if(lstrcmpA(szTexName, "-") == 0) return TRUE;

	/* TODO: Get blank texture string from config. */

	return FALSE;
}



/* MapWinUpdateInfoBar
 *   Updates the info-bar to reflect selection or highlight.
 *
 * Parameters:
 *   MAPWINDATA*	lpmwd	Map window data.
 *
 * Return value: None.
 */
static void MapWinUpdateInfoBar(MAPWINDATA *lpmwd)
{
	/* Set the palette if necessary. */
	if(TexPreviewPaletteSetWindow() != lpmwd->hwnd)
	{
		InitAllTexPreviews(lpmwd->hwnd, lpmwd->rgbqPalette);
	}

	switch(lpmwd->highlightobj.emType)
	{
	case EM_LINES:
		{
			LINEDEFDISPLAYINFO	lddi;
			CONFIG *lpcfgLinedefs = ConfigGetSubsection(lpmwd->lpcfgMap, "__ldtypesflat");

			ResetFlatPreviews();
			ResetSpritePreview();

			GetLinedefDisplayInfo(lpmwd->lpmap, lpcfgLinedefs, lpmwd->highlightobj.iIndex, &lddi);
			BuildSidedefTexturePreviews(lpmwd, &lddi, LDDIF_ALL);
			ShowLinesInfo(&lddi, LDDIF_ALL, RequiredTextures(lpmwd->lpmap, lpmwd->highlightobj.iIndex));
			DestroySidedefTexturePreviews(&lddi, LDDIF_ALL);
		}

		break;

	case EM_SECTORS:
		{
			SECTORDISPLAYINFO	sdi;
			CONFIG *lpcfgSectors = ConfigGetSubsection(lpmwd->lpcfgMap, "sectortypes");

			ResetSidedefPreviews();
			ResetSpritePreview();

			GetSectorDisplayInfo(lpmwd->lpmap, lpcfgSectors, lpmwd->highlightobj.iIndex, &sdi);
			BuildSectorTexturePreviews(lpmwd, &sdi, SDIF_ALL);
			ShowSectorInfo(&sdi, SDIF_ALL);
			DestroySectorTexturePreviews(&sdi, SDIF_ALL);
		}

		break;

	case EM_THINGS:
		{
			THINGDISPLAYINFO	tdi;
			CONFIG *lpcfgThings = ConfigGetSubsection(lpmwd->lpcfgMap, FLAT_THING_SECTION);

			ResetSidedefPreviews();
			ResetFlatPreviews();

			GetThingDisplayInfo(lpmwd->lpmap, lpcfgThings, lpmwd->highlightobj.iIndex, &tdi);
			BuildThingSpritePreview(lpmwd, &tdi, TDIF_ALL);
			ShowThingInfo(&tdi, TDIF_ALL);
			DestroyThingSpritePreview(&tdi, TDIF_ALL);
		}

		break;

	case EM_VERTICES:
		{
			VERTEXDISPLAYINFO	vdi;

			ResetSidedefPreviews();
			ResetFlatPreviews();
			ResetSpritePreview();

			GetVertexDisplayInfo(lpmwd->lpmap, lpmwd->highlightobj.iIndex, &vdi);
			ShowVertexInfo(&vdi, VDIF_ALL);
		}

		break;

	/* Not pointing at anything. */
	case EM_MOVE:
		{
			switch(lpmwd->editmode)
			{
			case EM_ANY:
				
				/* Begin by clearing all previews. Doesn't flicker, since we
				 * only get here when the info-bar *needs* updating.
				 */
				ResetSidedefPreviews();
				ResetFlatPreviews();
				ResetSpritePreview();

				if(lpmwd->selection.lpsellistLinedefs->iDataCount > 0) ShowLinesSelectionInfo(lpmwd);
				if(lpmwd->selection.lpsellistSectors->iDataCount > 0) ShowSectorSelectionInfo(lpmwd);
				if(lpmwd->selection.lpsellistThings->iDataCount > 0) ShowThingsSelectionInfo(lpmwd);
				if(lpmwd->selection.lpsellistVertices->iDataCount > 0) ShowVerticesSelectionInfo(lpmwd);

				/* If we've got a mixed selection or no selection, hide the
				 * left-hand panels. A little inelegant, since the above calls
				 * might just have shown them.
				 */
				if((lpmwd->selection.lpsellistLinedefs->iDataCount > 0 ? 1 : 0) +
					(lpmwd->selection.lpsellistSectors->iDataCount > 0 ? 1 : 0) +
					(lpmwd->selection.lpsellistThings->iDataCount > 0 ? 1 : 0) +
					(lpmwd->selection.lpsellistVertices->iDataCount > 0 ? 1 : 0) != 1)
					InfoBarShowPanels(IBPF_ALLINFO, FALSE);

				break;

			case EM_LINES:
				if(lpmwd->selection.lpsellistLinedefs->iDataCount > 0) ShowLinesSelectionInfo(lpmwd);
				else InfoBarShowPanels(IBPF_ALL, FALSE);

				break;

			case EM_SECTORS:
				if(lpmwd->selection.lpsellistSectors->iDataCount > 0) ShowSectorSelectionInfo(lpmwd);
				else InfoBarShowPanels(IBPF_ALL, FALSE);

				break;

			case EM_THINGS:
				if(lpmwd->selection.lpsellistThings->iDataCount > 0) ShowThingsSelectionInfo(lpmwd);
				else InfoBarShowPanels(IBPF_ALL, FALSE);

				break;

			case EM_VERTICES:
				if(lpmwd->selection.lpsellistVertices->iDataCount > 0) ShowVerticesSelectionInfo(lpmwd);
				else InfoBarShowPanels(IBPF_ALL, FALSE);

				break;

			default:
				
				InfoBarShowPanels(IBPF_ALL, FALSE);
			}
		}
		
		break;
		
	default:
		break;
	}
}


/* ShowsLinesSelectionInfo, ShowsSectorSelectionInfo, ShowsThingSelectionInfo,
 * ShowsVertexSelectionInfo
 *   Updates the info-bar to reflect selections.
 *
 * Parameters:
 *   MAPWINDATA*	lpmwd	Map window data.
 *
 * Return value: None.
 */
static void ShowLinesSelectionInfo(MAPWINDATA *lpmwd)
{
	LINEDEFDISPLAYINFO lddi;
	DWORD dwDisplayFlags;
	BYTE byTexRequirementFlags;

	dwDisplayFlags = CheckLines(lpmwd->lpmap, lpmwd->selection.lpsellistLinedefs, ConfigGetSubsection(lpmwd->lpcfgMap, "__ldtypesflat"), &lddi);
	byTexRequirementFlags = CheckTextureFlags(lpmwd->lpmap, lpmwd->selection.lpsellistLinedefs);

	BuildSidedefTexturePreviews(lpmwd, &lddi, dwDisplayFlags);
	ShowLinesInfo(&lddi, dwDisplayFlags, byTexRequirementFlags);
	DestroySidedefTexturePreviews(&lddi, dwDisplayFlags);
}

static void ShowSectorSelectionInfo(MAPWINDATA *lpmwd)
{
	SECTORDISPLAYINFO sdi;
	DWORD dwDisplayFlags;

	dwDisplayFlags = CheckSectors(lpmwd->lpmap, lpmwd->selection.lpsellistSectors, ConfigGetSubsection(lpmwd->lpcfgMap, "sectortypes"), &sdi);

	BuildSectorTexturePreviews(lpmwd, &sdi, dwDisplayFlags);
	ShowSectorInfo(&sdi, dwDisplayFlags);
	DestroySectorTexturePreviews(&sdi, dwDisplayFlags);
}

static void ShowThingsSelectionInfo(MAPWINDATA *lpmwd)
{
	THINGDISPLAYINFO tdi;
	DWORD dwDisplayFlags;

	dwDisplayFlags = CheckThings(lpmwd->lpmap, lpmwd->selection.lpsellistThings, ConfigGetSubsection(lpmwd->lpcfgMap, FLAT_THING_SECTION), &tdi);

	BuildThingSpritePreview(lpmwd, &tdi, dwDisplayFlags);
	ShowThingInfo(&tdi, dwDisplayFlags);
	DestroyThingSpritePreview(&tdi, dwDisplayFlags);
}

static void ShowVerticesSelectionInfo(MAPWINDATA *lpmwd)
{
	VERTEXDISPLAYINFO vdi;
	DWORD dwDisplayFlags;

	dwDisplayFlags = CheckVertices(lpmwd->lpmap, lpmwd->selection.lpsellistVertices, &vdi);

	ShowVertexInfo(&vdi, dwDisplayFlags);
}


/* ShowsSelectionProperties
 *   Updates the info-bar to reflect selections.
 *
 * Parameters:
 *   MAPWINDATA*	lpmwd	Map window data.
 *
 * Return value: None.
 *
 * Remarks:
 *   The caller should ensure that something is in fact selected.
 */
static void ShowSelectionProperties(MAPWINDATA *lpmwd)
{
	DWORD dwFlags = 0;
	DWORD dwStartPage = 0;
	MAPPPDATA mapppdata;
	TCHAR szFromTable[64], szCaption[64];
	UINT uiStringID = 0;
	int iObject;
	

	/* Build the flags to determine which property pages to show, and which
	 * one to start on.
	 */

	/* Assume we don't need to show obj num in caption. */
	iObject = -1;

	switch(lpmwd->editmode)
	{
	case EM_SECTORS:
		dwFlags = MPPF_SECTOR | MPPF_LINE;
		dwStartPage = MPPF_SECTOR;

		if(lpmwd->selection.lpsellistSectors->iDataCount == 1)
		{
			uiStringID = IDS_INFOBAR_SCAP;
			iObject = lpmwd->selection.lpsellistSectors->lpiIndices[0];
		}
		else
		{
			uiStringID = IDS_MAPOBJECT;
		}

		break;

	case EM_LINES:
		dwFlags = MPPF_LINE;
		dwStartPage = MPPF_LINE;

		/* If we've got any sectors selected, show that page, too. */
		if(lpmwd->selection.lpsellistSectors->iDataCount > 0) dwFlags |= MPPF_SECTOR;

		if(lpmwd->selection.lpsellistLinedefs->iDataCount == 1)
		{
			uiStringID = IDS_INFOBAR_LDCAP;
			iObject = lpmwd->selection.lpsellistLinedefs->lpiIndices[0];
		}
		else
		{
			uiStringID = IDS_MAPOBJECT;
		}

		break;

	case EM_THINGS:
		dwFlags = MPPF_THING;
		dwStartPage = MPPF_THING;

		if(lpmwd->selection.lpsellistThings->iDataCount == 1)
		{
			uiStringID = IDS_INFOBAR_TCAP;
			iObject = lpmwd->selection.lpsellistThings->lpiIndices[0];
		}
		else
		{
			uiStringID = IDS_MAPOBJECT;
		}

		break;

	case EM_VERTICES:
		/* The caller should make sure only one vertex is selected. */
		dwFlags = MPPF_VERTEX;
		dwStartPage = MPPF_VERTEX;
		uiStringID = IDS_INFOBAR_SCAP;
		iObject = lpmwd->selection.lpsellistSectors->lpiIndices[0];

		break;

	case EM_ANY:
		/* This is slightly more involved. We have to check what sort of things
		 * we have selected.
		 */
		if(lpmwd->selection.lpsellistLinedefs->iDataCount > 0)	dwFlags |= MPPF_LINE;
		if(lpmwd->selection.lpsellistSectors->iDataCount > 0)	dwFlags |= MPPF_SECTOR;
		if(lpmwd->selection.lpsellistThings->iDataCount > 0)	dwFlags |= MPPF_THING;

		/* Setting the properties of multiple vertices makes no sense at all. */
		if(lpmwd->selection.lpsellistVertices->iDataCount == 1)	dwFlags |= MPPF_VERTEX;

		/* We determine the start page by the type of the object that was
		 * clicked. TODO: Does this work with context menus?
		 */
		switch(lpmwd->hobjDrag.emType)
		{
			case EM_SECTORS:	dwStartPage = MPPF_SECTOR;	break;
			case EM_LINES:		dwStartPage = MPPF_LINE;	break;
			case EM_VERTICES:	dwStartPage = MPPF_VERTEX;	break;
			case EM_THINGS:		dwStartPage = MPPF_THING;	break;
			default:			dwStartPage = (DWORD)-1;	/* Don't care. */
		}

		uiStringID = IDS_MAPOBJECT;

		break;
		
	default:
		break;
	}

	/* Caption. Fill in using determined parameters. */
	LoadString(g_hInstance, uiStringID, szFromTable, sizeof(szFromTable) / sizeof(TCHAR));
	if(iObject >= 0)
		_sntprintf(szCaption, sizeof(szCaption) / sizeof(TCHAR) - 1, szFromTable, iObject);
	else
		lstrcpyn(szCaption, szFromTable, sizeof(szCaption) / sizeof(TCHAR));

	/* Terminate string in the event that _sntprintf filled it entirely. */
	szCaption[sizeof(szCaption) / sizeof(TCHAR) - 1] = '\0';

	mapppdata.hwndMap = lpmwd->hwnd;
	mapppdata.lpmap = lpmwd->lpmap;
	mapppdata.lpselection = &lpmwd->selection;
	mapppdata.lpcfgMap = lpmwd->lpcfgMap;
	mapppdata.lptnlFlats = lpmwd->lptnlFlats;
	mapppdata.lptnlTextures = lpmwd->lptnlTextures;
	mapppdata.lphimlCacheFlats = &lpmwd->himlCacheFlats;
	mapppdata.lphimlCacheTextures = &lpmwd->himlCacheTextures;
	
	/* Make an undo. */
	CreateUndo(lpmwd, IDS_UNDO_PROPERTIES);
	
	/* Show the dialogue. If the user cancelled, withdraw the undo. */
	if(!ShowMapObjectProperties(dwFlags, dwStartPage, szCaption, &mapppdata))
		WithdrawUndo(lpmwd);
	else
	{
		ClearRedoStack(lpmwd);
		lpmwd->bMapChanged = TRUE;
	}
}



/* DoInsertOperation
 *   Inserts an object.
 *
 * Parameters:
 *   MAPWINDATA*	lpmwd		Map window data.
 *   short			xMap, yMap	Map co-ordinates for insertion.
 *
 * Return value: None.
 *
 * Remarks:
 *   This may be done in response to either a left- or right-button release.
 */
static void DoInsertOperation(MAPWINDATA *lpmwd, short xMap, short yMap)
{
	switch(lpmwd->editmode)
	{
	case EM_ANY:
	case EM_LINES:
	case EM_SECTORS:
	case EM_VERTICES:

		/* User may cancel, so don't clear redo. */
		CreateUndo(lpmwd, (lpmwd->editmode == EM_VERTICES) ? IDS_UNDO_VERTEXINSERT : IDS_UNDO_LINEDEFDRAW);

		/* Draw the first vertex. */
		DrawToNewVertex(lpmwd->lpmap, &lpmwd->drawop, xMap, yMap, lpmwd->bStitchMode);

		/* Switch to drawing submode (this is overriden in vertex mode). */
		lpmwd->submode = ESM_DRAWING;

		/* If we're in vertex mode, that was also the last. */
		if(lpmwd->editmode == EM_VERTICES)
		{
			/* Drawing this vertex finishes the drawing operation. */
			EndDrawOperation(lpmwd->lpmap, &lpmwd->drawop);
			lpmwd->submode = ESM_NONE;

			ClearRedoStack(lpmwd);
			lpmwd->bMapChanged = TRUE;
			
			UpdateStatusBar(lpmwd, SBPF_LINEDEFS | SBPF_SIDEDEFS | SBPF_VERTICES | SBPF_SECTORS);
		}

		break;

	case EM_THINGS:
		{
			/* Create an undo snapshot. */
			CreateUndo(lpmwd, (lpmwd->editmode == EM_VERTICES) ? IDS_UNDO_VERTEXINSERT : IDS_UNDO_INSERTTHING);

			InsertThing(lpmwd, xMap, yMap);
			
			/* TODO: Show dialogue if option set. */

			/* Too late to cancel now. */
			ClearRedoStack(lpmwd);

			lpmwd->submode = ESM_NONE;
			lpmwd->bMapChanged = TRUE;

			UpdateStatusBar(lpmwd, SBPF_THINGS);
		}

		break;
		
	default:
		break;
	}
}


/* EndDragOperation
 *   Called when the user releases the mouse when dragging something. Sorts
 *   the map out, cleans up and restores normal behaviour.
 *
 * Parameters:
 *   MAPWINDATA*	lpmwd		Map window data.
 *
 * Return value: None.
 */
static void EndDragOperation(MAPWINDATA *lpmwd)
{
	/* TODO: Release mouse? */
	/* TODO: Add vertices for crossing lines. */

	/* Stitch any vertices within range. */
	if(lpmwd->bStitchMode)
	{
		/* Do this before we change linedef indices. */
		LabelRSLinesEnclosure(lpmwd->lpmap, lpmwd->lplooplistRS);

		/* Stitch and split. */
		StitchDraggedVertices(lpmwd->lpmap, lpmwd->selectionAux.lpsellistVertices);

		/* We need to do this now, so that we can restore our dragged loops
		 * before checking if we've been dropped around some other lines.
		 */
		CorrectDraggedSectorReferences(lpmwd->lpmap);
	}
	/* If we're not stitching, just do the minimum to keep the map correct. */
	else DeleteZeroLengthLinedefs(lpmwd->lpmap);

	/* The above messed with indices. */
	RebuildSelection(lpmwd);


	/* This could be invalid now, so clear it in case I forget and try to use
	 * it.
	 */
	CleanAuxiliarySelection(lpmwd);

	/* Update the loop list and flags to include the newly-RS-marked lines. */
	DestroyLoopList(lpmwd->lplooplistRS);
	lpmwd->lplooplistRS = LabelRSLinesLoops(lpmwd->lpmap);

	/* Any lines inside any of our loops in their final positions also need to
	 * be marked RS.
	 */
	LabelLoopedLinesRS(lpmwd->lpmap, lpmwd->lplooplistRS);

	/* Label whether each side should keep its sector reference. */
	LabelRSLinesEnclosure(lpmwd->lpmap, lpmwd->lplooplistRS);

	/* Set the sector references on sides that weren't enclosed. */
	CorrectDraggedSectorReferences(lpmwd->lpmap);

	/* Clear the dragging flags. */
	ClearDraggingFlags(lpmwd->lpmap);

	/* Were the objects selected before we clicked on them? If not, deselect
	 * them. We use brute force since, when pasting, we don't know that the
	 * types of the objects we're dragging correspond to the mode.
	 */
	if(lpmwd->bDeselectAfterEdit) DeselectAllBruteForce(lpmwd);

	/* Free memory. */
	DestroyLoopList(lpmwd->lplooplistRS);

	/* Restore normal behaviour. */
	lpmwd->submode = ESM_NONE;
	UpdateHighlight(lpmwd, -1, -1);
	MapWinUpdateInfoBar(lpmwd);
	UpdateStatusBar(lpmwd, SBPF_LINEDEFS | SBPF_SIDEDEFS | SBPF_VERTICES | SBPF_SECTORS);
	lpmwd->bWantRedraw = TRUE;

	/* Too late to cancel now! */
	ClearRedoStack(lpmwd);
	lpmwd->bMapChanged = TRUE;
}


/* RebuildSelection
 *   Recreates the selection lists for a map window based on the selection flags
 *   in the map objects.
 *
 * Parameters:
 *   MAPWINDATA*	lpmwd		Map window data.
 *
 * Return value: None.
 *
 * Remarks:
 *   Useful following a map editing operation that changes indices.
 */
static void RebuildSelection(MAPWINDATA *lpmwd)
{
	int i;

	SELECTION_LIST *lpsellistLinedefs = lpmwd->selection.lpsellistLinedefs;
	SELECTION_LIST *lpsellistSectors = lpmwd->selection.lpsellistSectors;
	SELECTION_LIST *lpsellistThings = lpmwd->selection.lpsellistThings;
	SELECTION_LIST *lpsellistVertices = lpmwd->selection.lpsellistVertices;

	/* Begin by clearing everything. */
	ClearSelectionList(lpsellistLinedefs);
	ClearSelectionList(lpsellistSectors);
	ClearSelectionList(lpsellistThings);
	ClearSelectionList(lpsellistVertices);

	/* Reselect each type in turn. */

	for(i = 0; i < lpmwd->lpmap->iLinedefs; i++)
		if(lpmwd->lpmap->linedefs[i].selected)
			AddToSelectionList(lpsellistLinedefs, i);

	for(i = 0; i < lpmwd->lpmap->iSectors; i++)
		if(lpmwd->lpmap->sectors[i].selected)
			AddToSelectionList(lpsellistSectors, i);

	for(i = 0; i < lpmwd->lpmap->iThings; i++)
		if(lpmwd->lpmap->things[i].selected)
			AddToSelectionList(lpsellistThings, i);

	for(i = 0; i < lpmwd->lpmap->iVertices; i++)
		if(lpmwd->lpmap->vertices[i].selected)
			AddToSelectionList(lpsellistVertices, i);
}


/* CreateUndo
 *   Saves an undo snapshot.
 *
 * Parameters:
 *   MAPWINDATA*	lpmwd			Map window data.
 *   int			iStringIndex	ID of resource string describing the action.
 *
 * Return value: None.
 *
 * Remarks:
 *   The redo stack is not cleared, since the app may want to withdraw the undo
 *   if the user cancels, in which case the redo stack must be left intact.
 */
static void CreateUndo(MAPWINDATA *lpmwd, int iStringIndex)
{
	/* Create the new undo frame. If we don't already have a stack, make one. */
	if(lpmwd->lpundostackUndo)
		PushNewUndoFrame(lpmwd->lpundostackUndo, lpmwd->lpmap, iStringIndex);
	else
		lpmwd->lpundostackUndo = CreateUndoStack(lpmwd->lpmap, iStringIndex);
}


/* ClearRedoStack
 *   Clears the redo stack.
 *
 * Parameters:
 *   MAPWINDATA*	lpmwd			Map window data.
 *
 * Return value: None.
 *
 * Remarks:
 *   Call this once you're sure you want to leave that undo you just created.
 */
static void ClearRedoStack(MAPWINDATA *lpmwd)
{
	/* Clear the redo stack. */
	if(lpmwd->lpundostackRedo)
	{
		FreeUndoStack(lpmwd->lpundostackRedo);
		lpmwd->lpundostackRedo = NULL;
	}
}


/* PerformUndo
 *   Undoes the last action for an open map.
 *
 * Parameters:
 *   MAPWINDATA*	lpmwd			Map window data.
 *
 * Return value: None.
 *
 * Remarks:
 *   The item is removed from the undo stack once it's undone. The redo stack is
 *   modified accordingly.
 */
static void PerformUndo(MAPWINDATA *lpmwd)
{
	int iUndoStringIndex = GetUndoTopStringIndex(lpmwd->lpundostackUndo);

	/* Create a new redo frame, making a new stack if necessary. */
	if(lpmwd->lpundostackRedo)
		PushNewUndoFrame(lpmwd->lpundostackRedo, lpmwd->lpmap, iUndoStringIndex);
	else
		lpmwd->lpundostackRedo = CreateUndoStack(lpmwd->lpmap, iUndoStringIndex);

	PerformTopUndo(lpmwd->lpundostackUndo, lpmwd->lpmap);
	WithdrawUndo(lpmwd);
}


/* PerformRedo
 *   Redoes the last action for an open map.
 *
 * Parameters:
 *   MAPWINDATA*	lpmwd			Map window data.
 *
 * Return value: None.
 *
 * Remarks:
 *   The item is removed from the redo stack once it's redone. The undo stack is
 *   modified accordingly. This is a copy-paste hack from PerformUndo.
 */
static void PerformRedo(MAPWINDATA *lpmwd)
{
	int iUndoStringIndex = GetUndoTopStringIndex(lpmwd->lpundostackRedo);

	/* Create a new undo frame, making a new stack if necessary. */
	if(lpmwd->lpundostackUndo)
		PushNewUndoFrame(lpmwd->lpundostackUndo, lpmwd->lpmap, iUndoStringIndex);
	else
		lpmwd->lpundostackUndo = CreateUndoStack(lpmwd->lpmap, iUndoStringIndex);

	PerformTopUndo(lpmwd->lpundostackRedo, lpmwd->lpmap);
	WithdrawRedo(lpmwd);
}


/* WithdrawUndo
 *   Removes an item from the undo stack for a window.
 *
 * Parameters:
 *   MAPWINDATA*	lpmwd			Map window data.
 *
 * Return value: None.
 */
static void WithdrawUndo(MAPWINDATA *lpmwd)
{
	if(UndoStackHasOnlyOneFrame(lpmwd->lpundostackUndo))
	{
		FreeUndoStack(lpmwd->lpundostackUndo);
		lpmwd->lpundostackUndo = NULL;
	}
	else
	{
		PopUndoFrame(lpmwd->lpundostackUndo);
	}
}


/* WithdrawRedo
 *   Removes an item from the redo stack for a window.
 *
 * Parameters:
 *   MAPWINDATA*	lpmwd			Map window data.
 *
 * Return value: None.
 */
static void WithdrawRedo(MAPWINDATA *lpmwd)
{
	if(UndoStackHasOnlyOneFrame(lpmwd->lpundostackRedo))
	{
		FreeUndoStack(lpmwd->lpundostackRedo);
		lpmwd->lpundostackRedo = NULL;
	}
	else
	{
		PopUndoFrame(lpmwd->lpundostackRedo);
	}
}


/* FlipSelectionHorizontally
 *   Flips the aux. selection horizontally in the intuitive manner.
 *
 * Parameters:
 *   MAPWINDATA*	lpmwd			Map window data.
 *
 * Return value: None.
 *
 * Remarks:
 *   The auxiliary selection must be set before calling this.
 */
static void FlipSelectionHorizontally(MAPWINDATA *lpmwd)
{
	short xMin, xMax;
	int i;
	short xAxis;

	/* Firstly, calculate the boundaries. */
	FindAuxSelectionBoundaries(lpmwd, &xMin, &xMax, NULL, NULL);

	/* Now, find an axis that bisects the region. */
	xAxis = ((int)xMax + (int)xMin) / 2;

	/* Now, flip vertices and things about the axis. */
	for(i = 0; i < lpmwd->selectionAux.lpsellistVertices->iDataCount; i++)
		FlipVertexAboutVerticalAxis(lpmwd->lpmap, lpmwd->selectionAux.lpsellistVertices->lpiIndices[i], xAxis);

	for(i = 0; i < lpmwd->selectionAux.lpsellistThings->iDataCount; i++)
		FlipThingAboutVerticalAxis(lpmwd->lpmap, lpmwd->selectionAux.lpsellistThings->lpiIndices[i], xAxis);

	/* Flip selected lines. */
	FlipSelectedLinedefs(lpmwd->lpmap, lpmwd->selection.lpsellistLinedefs);
	ExchangeSelectedSidedefs(lpmwd->lpmap, lpmwd->selection.lpsellistLinedefs);
}


/* FlipSelectionVertically
 *   Flips the aux. selection vertically in the intuitive manner.
 *
 * Parameters:
 *   MAPWINDATA*	lpmwd			Map window data.
 *
 * Return value: None.
 *
 * Remarks:
 *   The auxiliary selection must be set before calling this.
 */
static void FlipSelectionVertically(MAPWINDATA *lpmwd)
{
	short yMin, yMax;
	int i;
	short yAxis;

	/* Firstly, calculate the boundaries. */
	FindAuxSelectionBoundaries(lpmwd, NULL, NULL, &yMin, &yMax);

	/* Now, find an axis that bisects the region. */
	yAxis = ((int)yMax + (int)yMin) / 2;

	/* Now, flip vertices and things about the axis. */
	for(i = 0; i < lpmwd->selectionAux.lpsellistVertices->iDataCount; i++)
		FlipVertexAboutHorizontalAxis(lpmwd->lpmap, lpmwd->selectionAux.lpsellistVertices->lpiIndices[i], yAxis);

	for(i = 0; i < lpmwd->selectionAux.lpsellistThings->iDataCount; i++)
		FlipThingAboutHorizontalAxis(lpmwd->lpmap, lpmwd->selectionAux.lpsellistThings->lpiIndices[i], yAxis);

	/* Flip selected lines. */
	FlipSelectedLinedefs(lpmwd->lpmap, lpmwd->selection.lpsellistLinedefs);
	ExchangeSelectedSidedefs(lpmwd->lpmap, lpmwd->selection.lpsellistLinedefs);
}


/* CreateAuxiliarySelection
 *   Creates an auxiliary selection structure of vertices and things from the
 *   real selection.
 *
 * Parameters:
 *   MAPWINDATA*	lpmwd			Map window data.
 *
 * Return value: None.
 *
 * Remarks:
 *   Call CleanAuxiliarySelection to free memory one you're finished.
 */
static void CreateAuxiliarySelection(MAPWINDATA *lpmwd)
{
	int i;

	/* Build our dragging selection list. */
	lpmwd->selectionAux.lpsellistVertices = AllocateSelectionList(INIT_SELLIST_SIZE);
	lpmwd->selectionAux.lpsellistThings = AllocateSelectionList(INIT_SELLIST_SIZE);

	for(i = 0; i < lpmwd->selection.lpsellistThings->iDataCount; i++)
		AddToSelectionList(lpmwd->selectionAux.lpsellistThings, lpmwd->selection.lpsellistThings->lpiIndices[i]);

	/* Add the vertices that are already selected first. */
	for(i = 0; i < lpmwd->selection.lpsellistVertices->iDataCount; i++)
		AddToSelectionList(lpmwd->selectionAux.lpsellistVertices, lpmwd->selection.lpsellistVertices->lpiIndices[i]);
	/* Now add vertices for selected linedefs. This also covers the
	 * sector case, since they force selection of linedefs.
	 */
	for(i = 0; i < lpmwd->selection.lpsellistLinedefs->iDataCount; i++)
	{
		MAPLINEDEF *lpld = &lpmwd->lpmap->linedefs[lpmwd->selection.lpsellistLinedefs->lpiIndices[i]];

		/* AddToSelectionList eats duplicates, so this is okay. */
		AddToSelectionList(lpmwd->selectionAux.lpsellistVertices, lpld->v1);
		AddToSelectionList(lpmwd->selectionAux.lpsellistVertices, lpld->v2);
	}

	/* This saves headaches later. */
	SortSelectionList(lpmwd->selectionAux.lpsellistVertices);
}


/* CreateAuxiliarySelection
 *   Frees an aux. selection.
 *
 * Parameters:
 *   MAPWINDATA*	lpmwd			Map window data.
 *
 * Return value: None.
 */
static void CleanAuxiliarySelection(MAPWINDATA *lpmwd)
{
	DestroySelectionList(lpmwd->selectionAux.lpsellistThings);
	DestroySelectionList(lpmwd->selectionAux.lpsellistVertices);
}


/* SelectionCount
 *   Returns the total number of selected objects.
 *
 * Parameters:
 *   MAPWINDATA*	lpmwd			Map window data.
 *
 * Return value: int
 *   Total number of selected objects.
 */
static __inline int SelectionCount(MAPWINDATA *lpmwd)
{
	return lpmwd->selection.lpsellistLinedefs->iDataCount +
		lpmwd->selection.lpsellistSectors->iDataCount +
		lpmwd->selection.lpsellistVertices->iDataCount +
		lpmwd->selection.lpsellistThings->iDataCount;
}


/* CentreAuxSelectionAtPoint
 *   Translates the auxiliary selection such that it's centred at a point.
 *
 * Parameters:
 *   MAPWINDATA*	lpmwd				Map window data.
 *   short			xCentre, yCentre	Point at which to centre selection.
 *
 * Return value: None.
 */
static void CentreAuxSelectionAtPoint(MAPWINDATA *lpmwd, short xCentre, short yCentre)
{
	short xMin, xMax, yMin, yMax;
	short xBoxCentre, yBoxCentre;
	int i;

	FindAuxSelectionBoundaries(lpmwd, &xMin, &xMax, &yMin, &yMax);

	/* Find the centre of the bounding box. */
	xBoxCentre = ((int)xMin + (int)xMax) / 2;
	yBoxCentre = ((int)yMin + (int)yMax) / 2;

	/* Translate the selection. */
	for(i = 0; i < lpmwd->selectionAux.lpsellistVertices->iDataCount; i++)
	{
		lpmwd->lpmap->vertices[lpmwd->selectionAux.lpsellistVertices->lpiIndices[i]].x += xCentre - xBoxCentre;
		lpmwd->lpmap->vertices[lpmwd->selectionAux.lpsellistVertices->lpiIndices[i]].y += yCentre - yBoxCentre;
	}

	for(i = 0; i < lpmwd->selectionAux.lpsellistThings->iDataCount; i++)
	{
		lpmwd->lpmap->things[lpmwd->selectionAux.lpsellistThings->lpiIndices[i]].x += xCentre - xBoxCentre;
		lpmwd->lpmap->things[lpmwd->selectionAux.lpsellistThings->lpiIndices[i]].y += yCentre - yBoxCentre;
	}
}


/* FindAuxSelectionBoundaries
 *   Finds the tightest bounding box for the auxiliarly selection.
 *
 * Parameters:
 *   MAPWINDATA*	lpmwd				Map window data.
 *   short			*lpxMin, *lpxMax, 	Used to return co-ordinates of
 *					*lpyMin, *lpyMax	the bounding box. May be NULL.
 *
 * Return value: None.
 */
static void FindAuxSelectionBoundaries(MAPWINDATA *lpmwd, short *lpxMin, short *lpxMax, short *lpyMin, short *lpyMax)
{
	int i;

	if(lpxMin) *lpxMin = 32767;
	if(lpxMax) *lpxMax = -32768;
	if(lpyMin) *lpyMin = 32767;
	if(lpyMax) *lpyMax = -32768;

	for(i = 0; i < lpmwd->selectionAux.lpsellistVertices->iDataCount; i++)
	{
		if(lpxMin) *lpxMin = min(*lpxMin, lpmwd->lpmap->vertices[lpmwd->selectionAux.lpsellistVertices->lpiIndices[i]].x);
		if(lpxMax) *lpxMax = max(*lpxMax, lpmwd->lpmap->vertices[lpmwd->selectionAux.lpsellistVertices->lpiIndices[i]].x);
		if(lpyMin) *lpyMin = min(*lpyMin, lpmwd->lpmap->vertices[lpmwd->selectionAux.lpsellistVertices->lpiIndices[i]].y);
		if(lpyMax) *lpyMax = max(*lpyMax, lpmwd->lpmap->vertices[lpmwd->selectionAux.lpsellistVertices->lpiIndices[i]].y);
	}

	for(i = 0; i < lpmwd->selectionAux.lpsellistThings->iDataCount; i++)
	{
		
		if(lpxMin) *lpxMin = min(*lpxMin, lpmwd->lpmap->things[lpmwd->selectionAux.lpsellistThings->lpiIndices[i]].x);
		if(lpxMax) *lpxMax = max(*lpxMax, lpmwd->lpmap->things[lpmwd->selectionAux.lpsellistThings->lpiIndices[i]].x);
		if(lpyMin) *lpyMin = min(*lpyMin, lpmwd->lpmap->things[lpmwd->selectionAux.lpsellistThings->lpiIndices[i]].y);
		if(lpyMax) *lpyMax = max(*lpyMax, lpmwd->lpmap->things[lpmwd->selectionAux.lpsellistThings->lpiIndices[i]].y);
	}
}


/* InsertThing
 *   Adds a new thing to a map, and initialises it with the properties of the
 *   thing last edited.
 *
 * Parameters:
 *   MAPWINDATA*		lpmwd		Map window data.
 *   unsigned short		xMap, yMap	Co-ordinates of new thing.
 *
 * Return value: int
 *   Index of new thing, or negative on error.
 */
static int InsertThing(MAPWINDATA *lpmwd, unsigned short xMap, unsigned short yMap)
{
	int iThing = AddThing(lpmwd->lpmap, xMap, yMap);
	MAPTHING *lpthing;

	/* Out of things...? */
	if(iThing < 0) return -1;

	lpthing = &lpmwd->lpmap->things[iThing];

	/* Set some initial properties. */
	lpthing->thing = lpmwd->thingLast.thing;
	lpthing->flag = lpmwd->thingLast.flag;
	lpthing->angle = lpmwd->thingLast.angle;

	SetThingPropertiesFromType(lpmwd->lpmap, iThing, ConfigGetSubsection(lpmwd->lpcfgMap, FLAT_THING_SECTION));

	return iThing;
}


/* SendUpdateWadMessage
 *   Sends a WM_UPDATEWAD message to a window.
 *
 * Parameters:
 *   HWND		hwnd	Window to send message to.
 *   LPARAM		lParam	Unused.
 *
 * Return value: BOOL
 *   Always TRUE.
 *
 * Remarks:
 *   Used as a callback by EnumChildWindowsByWad when doing a Save As.
 */
static BOOL CALLBACK SendUpdateWadMessage(HWND hwnd, LPARAM lParam)
{
	UNREFERENCED_PARAMETER(lParam);

	SendMessage(hwnd, WM_UPDATEWAD, 0, 0);

	return TRUE;
}


/* SendUpdateTitleMessage
 *   Sends a WM_UPDATETITLE message to a window.
 *
 * Parameters:
 *   HWND		hwnd	Window to send message to.
 *   LPARAM		lParam	Unused.
 *
 * Return value: BOOL
 *   Always TRUE.
 *
 * Remarks:
 *   Used as a callback by EnumChildWindowsByWad when doing a Save As.
 */
static BOOL CALLBACK SendUpdateTitleMessage(HWND hwnd, LPARAM lParam)
{
	UNREFERENCED_PARAMETER(lParam);

	SendMessage(hwnd, WM_UPDATETITLE, 0, 0);

	return TRUE;
}


/* MapWindowSaveAs
 *   Displays the Save As dialogue and saves the wad.
 *
 * Parameters:
 *   MAPWINDATA*	lpmwd	Map window data.
 *
 * Return value: None.
 */
static void MapWindowSaveAs(MAPWINDATA *lpmwd)
{
	TCHAR szFileName[MAX_PATH];
	LPTSTR szInitFileName = NULL;
	WORD cchInitFileName;

	cchInitFileName = WadIsNewByID(lpmwd->iWadID) ? 0 : GetWadPath(lpmwd->iWadID, NULL, 0);
	if(cchInitFileName > 0)
	{
		szInitFileName = ProcHeapAlloc(cchInitFileName * sizeof(TCHAR));
		GetWadPath(lpmwd->iWadID, szInitFileName, cchInitFileName);
	}
	
	/* Make sure the user doesn't cancel. */
	if(CommDlgSave(lpmwd->hwnd, szFileName, sizeof(szFileName)/sizeof(TCHAR), NULL, TEXT("Wad files (*.wad, *.srb)\0*.wad;*.srb\0All files (*.*)\0*.*\0"), TEXT("wad"), szInitFileName, OFN_EXPLORER | OFN_OVERWRITEPROMPT))
	{
		/* Update the wad structure for all open maps for this wad. */
		EnumChildWindowsByWad(lpmwd->iWadID, SendUpdateWadMessage, 0);

		/* TODO: Nicer errors. */
		if(SaveWadAs(lpmwd->iWadID, szFileName))
			MessageBoxFromStringTable(g_hwndMain, IDS_SAVEERROR, MB_ICONERROR);
		else lpmwd->bMapChanged = FALSE;

		/* Update the title for all open maps for this wad. */
		EnumChildWindowsByWad(lpmwd->iWadID, SendUpdateTitleMessage, 0);
	}

	if(szInitFileName) ProcHeapFree(szInitFileName);
}


/* MapWindowSave
 *   Saves the current map for the specified map window.
 *
 * Parameters:
 *   MAPWINDATA*	lpmwd	Map window data.
 *
 * Return value: None.
 */
static void MapWindowSave(MAPWINDATA *lpmwd)
{
	/* Do a Save As if necessary. */
	if(WadIsNewByID(lpmwd->iWadID))
		MapWindowSaveAs(lpmwd);
	else
	{
		/* TODO: Nicer error messages? */
		SendUpdateWadMessage(lpmwd->hwnd, 0);
		if(SaveWad(lpmwd->iWadID))
			MessageBoxFromStringTable(g_hwndMain, IDS_SAVEERROR, MB_ICONERROR);
		else lpmwd->bMapChanged = FALSE;
	}
}


/* SnapAuxVerticesToGrid, SnapSelectedThingsToGrid
 *   Snaps selected vertices/things to the grid.
 *
 * Parameters:
 *   MAPWINDATA*	lpmwd			Map window data.
 *
 * Return value: None.
 */
static void SnapAuxVerticesToGrid(MAPWINDATA *lpmwd)
{
	int i;
	for(i = 0; i < lpmwd->selectionAux.lpsellistVertices->iDataCount; i++)
	{
		MAPVERTEX *lpvx = &lpmwd->lpmap->vertices[lpmwd->selectionAux.lpsellistVertices->lpiIndices[i]];
		Snap(&lpvx->x, &lpvx->y, lpmwd->mapview.cxGrid, lpmwd->mapview.cyGrid);
	}

	/* Stitch if desired. */
	if(lpmwd->bStitchMode)
	{
		StitchDraggedVertices(lpmwd->lpmap, lpmwd->selectionAux.lpsellistVertices);
		CorrectDraggedSectorReferences(lpmwd->lpmap);
		ClearDraggingFlags(lpmwd->lpmap);
		RebuildSelection(lpmwd);
	}
}

static void SnapSelectedThingsToGrid(MAPWINDATA *lpmwd)
{
	int i;
	for(i = 0; i < lpmwd->selection.lpsellistThings->iDataCount; i++)
	{
		MAPTHING *lpthing = &lpmwd->lpmap->things[lpmwd->selection.lpsellistThings->lpiIndices[i]];
		Snap(&lpthing->x, &lpthing->y, lpmwd->mapview.cxGrid, lpmwd->mapview.cyGrid);
	}
}


/* EndPan
 *   Ends a panning operation, either quick-move or EM_MOVE.
 *
 * Parameters:
 *   MAPWINDATA*	lpmwd			Map window data.
 *
 * Return value: None.
 */
static void StartPan(MAPWINDATA *lpmwd)
{
	POINT ptCursor;

	/* End the draw operation if we're in one. */
	if(lpmwd->submode == ESM_DRAWING)
		EndDrawOperation(lpmwd->lpmap, &lpmwd->drawop);

	lpmwd->submode = ESM_MOVING;
	
	/* Store the cursor. */
	lpmwd->hCursor = LoadCursor(g_hInstance, MAKEINTRESOURCE(IDC_GRABCLOSED));

	/* Show the cursor if we're in the client area. */
	GetCursorPos(&ptCursor);
	if(SendMessage(lpmwd->hwnd, WM_NCHITTEST, 0, MAKELPARAM(ptCursor.x, ptCursor.y)) == HTCLIENT)
		SetCursor(lpmwd->hCursor);
}


/* EndPan
 *   Ends a panning operation, either quick-move or EM_MOVE.
 *
 * Parameters:
 *   MAPWINDATA*	lpmwd			Map window data.
 *
 * Return value: None.
 */
static void EndPan(MAPWINDATA *lpmwd)
{
	POINT pt;

	lpmwd->submode = ESM_NONE;
	
	/* Store the cursor. */
	lpmwd->hCursor = lpmwd->editmode == EM_MOVE ? LoadCursor(g_hInstance, MAKEINTRESOURCE(IDC_GRABOPEN)) : LoadCursor(NULL, IDC_ARROW);

	/* Show the cursor if we're in the client area. */
	pt.x = lpmwd->xMouseLast;
	pt.y = lpmwd->yMouseLast;
	ClientToScreen(lpmwd->hwnd, &pt);
	if(SendMessage(lpmwd->hwnd, WM_NCHITTEST, 0, MAKELPARAM(pt.x, pt.y)) == HTCLIENT)
		SetCursor(lpmwd->hCursor);
}


/* DeleteSelection
 *   Deletes the selection.
 *
 * Parameters:
 *   MAPWINDATA*	lpmwd			Map window data.
 *
 * Return value: None.
 *
 * Remarks:
 *   Clobbers the aux. selection and the VEF_LABELLED flag.
 */
static void DeleteSelection(MAPWINDATA *lpmwd)
{
	int i;

	/* Label the vertices that we should delete if they become unused. */
	CreateAuxiliarySelection(lpmwd);
	for(i = 0; i < lpmwd->selectionAux.lpsellistVertices->iDataCount; i++)
		lpmwd->lpmap->vertices[lpmwd->selectionAux.lpsellistVertices->lpiIndices[i]].editflags |= VEF_LABELLED;
	CleanAuxiliarySelection(lpmwd);

	SortSelectionList(lpmwd->selection.lpsellistLinedefs);
	for(i = lpmwd->selection.lpsellistLinedefs->iDataCount - 1; i >= 0; i--)
		DeleteLinedef(lpmwd->lpmap, lpmwd->selection.lpsellistLinedefs->lpiIndices[i]);

	SortSelectionList(lpmwd->selection.lpsellistVertices);
	for(i = lpmwd->selection.lpsellistVertices->iDataCount - 1; i >= 0; i--)
		DeleteVertex(lpmwd->lpmap, lpmwd->selection.lpsellistVertices->lpiIndices[i]);

	SortSelectionList(lpmwd->selection.lpsellistSectors);
	for(i = lpmwd->selection.lpsellistSectors->iDataCount - 1; i >= 0; i--)
		DeleteSector(lpmwd->lpmap, lpmwd->selection.lpsellistSectors->lpiIndices[i]);

	SortSelectionList(lpmwd->selection.lpsellistThings);
	for(i = lpmwd->selection.lpsellistThings->iDataCount - 1; i >= 0; i--)
		DeleteThing(lpmwd->lpmap, lpmwd->selection.lpsellistThings->lpiIndices[i]);

	RemoveUnusedVertices(lpmwd->lpmap, VEF_LABELLED);


	/* Clear labels. */
	for(i = 0; i < lpmwd->lpmap->iVertices; i++)
		lpmwd->lpmap->vertices[i].editflags &= ~VEF_LABELLED;

	RebuildSelection(lpmwd);
}
