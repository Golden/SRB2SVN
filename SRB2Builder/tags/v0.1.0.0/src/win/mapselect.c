#include <windows.h>

#include "../general.h"
#include "../../res/resource.h"
#include "../like.h"
#include "../openwads.h"
#include "../map.h"
#include "../config.h"
#include "../mapconfig.h"
#include "mapselect.h"
#include "mdiframe.h"

#include "../../wad/Wad.h"


/* Types. */

typedef struct _MAPSELECTDLGDATA
{
	int		iWad;
	char	szLumpName[9];
	CONFIG	*lpcfgMap;		/* Map config *CONTAINER*/
} MAPSELECTDLGDATA;


/* Static prototypes. */
static BOOL CALLBACK MapSelectProc(HWND hwndDlg, UINT uiMsg, WPARAM wParam, LPARAM lParam);
static void WINAPI AddMapLumpsToList(LPCSTR szLumpName, void *lpv);
static void GetListBoxSelItemTextA(HWND hwndLB, LPSTR szBuffer);
static void SetMapSelectTitle(HWND hwndDlg, int iWad);



/* SelectMap
 *   Displays the Select Map dialogue.
 *
 * Parameters:
 *   int	iWad			Wad ID for which to list maps.
 *   LPSTR	szLumpName		Buffer to return lump name into. Must be big enough.
 *   CONFIG	**lplpcfgMap	Pointer to specified map config returned here.
 *
 * Return value: BOOL
 *   TRUE if user selected a map; FALSE if cancelled.
 */
BOOL SelectMap(int iWad, LPSTR szLumpName, CONFIG **lplpcfgMap)
{
	MAPSELECTDLGDATA msdd;

	/* Specify parameters for the dialogue box. */
	msdd.iWad = iWad;

	/* Display the dialogue box. */
	if(IDCANCEL != DialogBoxParam(g_hInstance, MAKEINTRESOURCE(IDD_MAPSELECT), g_hwndMain, MapSelectProc, (LPARAM)&msdd))
	{
		/* Copy the lumpname and config address, and finish. */
		lstrcpy(szLumpName, msdd.szLumpName);
		*lplpcfgMap = msdd.lpcfgMap;
		return TRUE;	/* User clicked OK. */
	}
	
	/* User cancelled. */
	return FALSE;
}



static BOOL CALLBACK MapSelectProc(HWND hwndDlg, UINT uiMsg, WPARAM wParam, LPARAM lParam)
{
	/* There's only ever one of us at a time, so this is okay. */
	static MAPSELECTDLGDATA *s_lpmsdd;
	static HWND s_hwndMapList, s_hwndOK, s_hwndMapConfigs;
	static HBITMAP s_hbmPreview;
	static RECT s_rcPreview;

	switch(uiMsg)
	{
	case WM_INITDIALOG:
		{
			HWND hwndPreview = GetDlgItem(hwndDlg, IDC_PREVIEW);
			HDC hdc;

			/* Get the parameters. */
			s_lpmsdd = (MAPSELECTDLGDATA*)lParam;

			/* Store some things for convenience. */
			s_hwndMapList = GetDlgItem(hwndDlg, IDC_LISTMAPS);
			s_hwndOK = GetDlgItem(hwndDlg, IDOK);
			s_hwndMapConfigs = GetDlgItem(hwndDlg, IDC_COMBOMAPCONFIG);

			/* Create the monochrome preview bitmap. */
			GetClientRect(GetDlgItem(hwndDlg, IDC_PREVIEWSIZE), &s_rcPreview);
			s_hbmPreview = CreateBitmap(s_rcPreview.right - s_rcPreview.left, s_rcPreview.bottom - s_rcPreview.top, 1, 1, NULL);

			/* Clear the preview bitmap. */
			hdc = CreateCompatibleDC(NULL);
			SelectObject(hdc, s_hbmPreview);
			FillRect(hdc, &s_rcPreview, GetStockObject(BLACK_BRUSH));
			DeleteDC(hdc);

			/* Assign the bitmap to the preview static control. */
			SendMessage(hwndPreview, STM_SETIMAGE, IMAGE_BITMAP, (LPARAM)s_hbmPreview);

			/* Fill the map config list and select the first one. */
			AddMapConfigsToComboBox(s_hwndMapConfigs);
			SendMessage(s_hwndMapConfigs, CB_SETCURSEL, 0, 0);

			/* Fill the list of maps. */
			EnumLumpNames(GetWad(s_lpmsdd->iWad), AddMapLumpsToList, (void*)s_hwndMapList);

			/* Set the caption to include the name of the wad. */
			SetMapSelectTitle(hwndDlg, s_lpmsdd->iWad);

			/* If list isn't empty, select a map and enable the OK button. */
			if(SendMessage(s_hwndMapList, LB_GETCOUNT, 0, 0) > 0)
			{
				/* Select the first map in the list. */
				SendMessage(s_hwndMapList, LB_SETCURSEL, 0, 0);
				SendMessage(hwndDlg, WM_COMMAND, MAKEWPARAM(IDC_LISTMAPS, LBN_SELCHANGE), (LPARAM)s_hwndMapList);

				EnableWindow(s_hwndOK, TRUE);
			}

			return TRUE;		/* Let the system set the focus. */
		}

	/* Notification messages. */
	case WM_COMMAND:
		switch(HIWORD(wParam))
		{
		case BN_CLICKED:
			/* One of the buttons was clicked. */

			switch(LOWORD(wParam))
			{
			case IDOK:
				{
					int iIndex;

					/* Return the selected lump name. */
					GetListBoxSelItemTextA(s_hwndMapList, s_lpmsdd->szLumpName);

					/* Return the selected map config. */
					iIndex = SendMessage(s_hwndMapConfigs, CB_GETCURSEL, 0, 0);
					s_lpmsdd->lpcfgMap = (CONFIG*)SendMessage(s_hwndMapConfigs, CB_GETITEMDATA, iIndex, 0);

					EndDialog(hwndDlg, IDOK);
					return TRUE;
				}
			case IDCANCEL:
				EndDialog(hwndDlg, IDCANCEL);
				return TRUE;
			}
			break;

		case LBN_DBLCLK:
			/* An item in the map-list was double-clicked. Simulate OK press. */
			SendMessage(hwndDlg, WM_COMMAND, MAKEWPARAM(IDOK, BN_CLICKED), (LPARAM)GetDlgItem(hwndDlg, IDOK));
			return TRUE;

		case LBN_SELCHANGE:
			{
				/* List selection changed. Draw the preview. */

				char szLumpname[9];
				MAP *lpmap;

				GetListBoxSelItemTextA(s_hwndMapList, szLumpname);
				lpmap = AllocateMapStructure();

				/* We only load the necessary bits of the map. */
				if(MapLoadForPreview(lpmap, s_lpmsdd->iWad, szLumpname) >= 0)
				{
					HDC hdc;
					int i;
					short x, y;
					short xMax=-32768, yMax=-32768;
					short xMin=32767, yMin=32767;
					short cxViewport = (short)(s_rcPreview.right - s_rcPreview.left);
					short cyViewport = (short)(s_rcPreview.bottom - s_rcPreview.top);
					unsigned short cxWin, cyWin;

					hdc = CreateCompatibleDC(NULL);
					SelectObject(hdc, s_hbmPreview);
					SelectObject(hdc, GetStockObject(WHITE_PEN));

					/* Clear. Scale back to pixels first. */
					SetMapMode(hdc, MM_TEXT);
					FillRect(hdc, &s_rcPreview, GetStockObject(BLACK_BRUSH));

					/* Find necessary extent. */
					for(i=0; i < lpmap->iVertices; i++)
					{
						x = lpmap->vertices[i].x;
						y = lpmap->vertices[i].y;

						if(x > xMax) xMax = x;
						if(y > yMax) yMax = y;

						if(x < xMin) xMin = x;
						if(y < yMin) yMin = y;
					}

					//cxWin = (int)(((double)cxViewport/(double)cyViewport)) * (xMax - xMin));
					cxWin = xMax - xMin;
					cyWin = yMax - yMin;

					if((int)cxWin * cyViewport < (int)cyWin * cxViewport)
						cxWin = (int)(((double)cxViewport/(double)cyViewport) * cyWin);
					else
						cyWin = (int)(((double)cyViewport/(double)cxViewport) * cxWin);

					/* Scale. */
					SetMapMode(hdc, MM_ANISOTROPIC);
					SetWindowExtEx(hdc, (int)(1.1 * cxWin), (int)(1.1 * cyWin), NULL);
					SetWindowOrgEx(hdc, (xMax + xMin) / 2, (yMax + yMin) / 2, NULL);
					SetViewportExtEx(hdc, cxViewport, -cyViewport, NULL);
					SetViewportOrgEx(hdc, cxViewport >> 1, cyViewport >> 1, NULL);

					for(i=0; i < lpmap->iLinedefs; i++)
					{
						MoveToEx(hdc, lpmap->vertices[lpmap->linedefs[i].v1].x, lpmap->vertices[lpmap->linedefs[i].v1].y, NULL);
						LineTo(hdc, lpmap->vertices[lpmap->linedefs[i].v2].x, lpmap->vertices[lpmap->linedefs[i].v2].y);
					}

					DeleteDC(hdc);

					InvalidateRect(GetDlgItem(hwndDlg, IDC_PREVIEW), NULL, FALSE);
				}
				
				DestroyMapStructure(lpmap);
			}

		}

		/* Didn't process the WM_COMMAND if we got here. */
		break;


	case WM_DESTROY:
		/* Unassign the preview bitmap. */
		SendMessage(GetDlgItem(hwndDlg, IDC_PREVIEW), STM_SETIMAGE, IMAGE_BITMAP, (LPARAM)NULL);

		/* Destroy the bitmap. */
		DeleteObject(s_hbmPreview);

		/* Message was processed. */
		return TRUE;
	}

	/* Didn't process the message. */
	return FALSE;
}





static void WINAPI AddMapLumpsToList(LPCSTR szLumpName, void *lpv)
{
	TCHAR szLumpNameT[9];
	HWND hwndMapList = (HWND)lpv;

	/* De-ANSIfy if necessary. */
	wsprintf(szLumpNameT, TEXT("%hs"), szLumpName);

	/* If the lumpname makes it look like a map, add it to the list. */
	if(LikeA(szLumpName, "MAP##") || LikeA(szLumpName, "MAP[A-Z][0-9A-Z]") || LikeA(szLumpName, "E#M#"))
		SendMessage(hwndMapList, LB_ADDSTRING, 0, (LPARAM)szLumpNameT);
}



static void GetListBoxSelItemTextA(HWND hwndLB, LPSTR szBuffer)
{
	TCHAR szLumpNameT[9];	/* Lumpname in list by be Unicode. */

	/* Get text of selected item. */
	SendMessage(hwndLB, LB_GETTEXT, SendMessage(hwndLB, LB_GETCURSEL, 0, 0), (LPARAM)szLumpNameT);

	/* Copy from szLumpNameT to the caller's buffer, converting
	 * to ANSI if necessary.
	 */
#ifdef _UNICODE
	wsprintfA(szBuffer, "%ls", szLumpNameT);
#else
	lstrcpy(szBuffer, szLumpNameT);
#endif
}




/* SetMapSelectTitle
 *   Updates the map-select dialogue's caption.
 *
 * Parameters:
 *   HWND		hwndDlg		Window handle.
 *   int		iWad		Wad ID.
 *
 * Return value: None.
 */
static void SetMapSelectTitle(HWND hwndDlg, int iWad)
{
	TCHAR	szBaseText[64];
	LPTSTR	szPath, szTitle, szFileTitle;
	WORD	cb;

	/* Get the base text (e.g. "Select Map"). */
	LoadString(g_hInstance, IDS_MAPSELECTCAPTION, szBaseText, sizeof(szBaseText) / sizeof(TCHAR));

	/* Allocate buffer for full path, and get it. */
	cb = GetWadPath(iWad, NULL, 0);
	szPath = (LPTSTR)ProcHeapAlloc(cb * sizeof(TCHAR));
	GetWadPath(iWad, szPath, cb);

	/* Allocate buffer for file title, and get it. */
	cb = GetFileTitle(szPath, NULL, 0);
	szFileTitle = (LPTSTR)ProcHeapAlloc(cb * sizeof(TCHAR));
	GetFileTitle(szPath, szFileTitle, cb);
	ProcHeapFree(szPath);

	/* Buffer for final caption text. */
	szTitle = (LPTSTR)ProcHeapAlloc((cb + lstrlen(szBaseText) + 3) * sizeof(TCHAR));

	/* Compose the parts. */
	wsprintf(szTitle, TEXT("%s - %s"), szBaseText, szFileTitle);

	SetWindowText(hwndDlg, szTitle);

	ProcHeapFree(szFileTitle);
	ProcHeapFree(szTitle);
}
