#include <windows.h>

#include "general.h"
#include "selection.h"


/* TODO: Replace with DYNAMICINTARRAYs? Remember to enforce uniqueness if you
 * do.
 */


#define SELECTION_SIZE_INCREMENT	30


/* AllocateSelectionList
 *   Allocates a selection list structure, initialsing it.
 *
 * Parameters:
 *   int	iInitListSize		Initial length list.
 *
 * Return value: SELECTION_LIST*
 *   Pointer to new structure.
 *
 * Notes:
 *   Call DestroySelectionList to free the returned pointer.
 */
SELECTION_LIST* AllocateSelectionList(int iInitListSize)
{
	SELECTION_LIST *lpsellist;

	/* Allocate structure. */
	lpsellist = ProcHeapAlloc(sizeof(SELECTION_LIST));

	lpsellist->ciBufferLength = iInitListSize;
	lpsellist->iDataCount = 0;
	lpsellist->lpiIndices = ProcHeapAlloc(iInitListSize * sizeof(int));

	return lpsellist;
}


/* DestroySelectionList
 *   Destroys a selection list structure, including its contents.
 *
 * Parameters:
 *   SELECTION_LIST*	lpsellist	Pointer to selection list to free.
 *
 * Return value: None.
 */
void DestroySelectionList(SELECTION_LIST *lpsellist)
{
	/* Free list. */
	ProcHeapFree(lpsellist->lpiIndices);

	/* Free structure. */
	ProcHeapFree(lpsellist);
}



/* AddToSelectionList
 *   Adds an item to a selection list.
 *
 * Parameters:
 *   SELECTION_LIST*	lpsellist	Pointer to selection hash.
 *   int				iValue		New value.
 *
 * Return value: None.
 */
void AddToSelectionList(SELECTION_LIST *lpsellist, int iValue)
{
	/* Remove the item if it already exists. */
	RemoveFromSelectionList(lpsellist, iValue);

	/* List full? */
	if(lpsellist->iDataCount == lpsellist->ciBufferLength)
	{
		/* Increase size. */
		lpsellist->ciBufferLength += SELECTION_SIZE_INCREMENT;
		lpsellist->lpiIndices = ProcHeapReAlloc(lpsellist->lpiIndices, (lpsellist->ciBufferLength) * sizeof(int));
	}
	
	/* Add the new item to the end of the list and increment count. */
	lpsellist->lpiIndices[lpsellist->iDataCount++] = iValue;
}


/* RemoveFromSelectionList
 *   Removes an item from a selection list.
 *
 * Parameters:
 *   SELECTION_LIST*	lpsellist	Pointer to selection list.
 *   int				iValue		Value to remove.
 *
 * Return value: BOOL
 *   TRUE if value was found (and removed); FALSE otherwise.
 */
BOOL RemoveFromSelectionList(SELECTION_LIST *lpsellist, int iValue)
{
	int *lpiIndices;
	int i;

	/* Save some dereferencing in the loop. */
	lpiIndices = lpsellist->lpiIndices;

	/* Find whether entry's already there. */
	i = 0;
	while(i < lpsellist->iDataCount && lpiIndices[i] != iValue) i++;

	/* Did the entry already exist? */
	if(i < lpsellist->iDataCount)
	{
		if(i < lpsellist->iDataCount - 1)
		{
			/* Remove it by copying the rest of the list down on top of it. Must
			 * use MoveMemory, since the regions overlap.
			 */
			MoveMemory(&lpiIndices[i], &lpiIndices[i+1], (lpsellist->iDataCount - i - 1) * sizeof(int));
		}

		lpsellist->iDataCount--;

		return TRUE;
	}

	return FALSE;
}


/* ExistsInSelectionList
 *  Determines whether an item exists in a selection list.
 *
 * Parameters:
 *   SELECTION_LIST*	lpsellist	Pointer to selection list.
 *   int				iValue		Value to search for.
 *
 * Return value: BOOL
 *   TRUE if value was found; FALSE otherwise.
 */
BOOL ExistsInSelectionList(SELECTION_LIST *lpsellist, int iValue)
{
	int *lpiIndices;
	int i;

	/* Save some dereferencing in the loop. */
	lpiIndices = lpsellist->lpiIndices;

	/* Find whether entry's already there. */
	for(i = 0; i < lpsellist->iDataCount; i++)
		if(lpiIndices[i] == iValue) return TRUE;

	return FALSE;
}


/* SortSelectionList
 *  Sorts a selection list in ascending order.
 *
 * Parameters:
 *   SELECTION_LIST*	lpsellist	Pointer to selection list.
 *
 * Return value: None.
 */
void SortSelectionList(SELECTION_LIST *lpsellist)
{
	qsort(lpsellist->lpiIndices, lpsellist->iDataCount, sizeof(int), QsortIntegerComparison);
}
