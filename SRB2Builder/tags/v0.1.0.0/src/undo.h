#ifndef __SRB2B_UNDO_H__
#define __SRB2B_UNDO_H__

typedef struct _UNDOSTACK UNDOSTACK;

UNDOSTACK* CreateUndoStack(MAP *lpmap, int iStringIndex);
void FreeUndoStack(UNDOSTACK *lpundostack);
void PushNewUndoFrame(UNDOSTACK *lpundostack, MAP *lpmap, int iStringIndex);
int GetUndoTopStringIndex(UNDOSTACK *lpundostack);
BOOL UndoStackHasOnlyOneFrame(UNDOSTACK *lpundostack);
void PopUndoFrame(UNDOSTACK *lpundostack);
void PerformTopUndo(UNDOSTACK *lpundostack, MAP *lpmap);

#endif
