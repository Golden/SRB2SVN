#ifndef __SRB2B_CONFIG__
#define __SRB2B_CONFIG__


/* Macros. */

#ifdef _UNICODE
#define ConfigGetString ConfigGetStringW
#else
#define ConfigGetString ConfigGetStringA
#endif


/* Types. */

typedef enum _CONFIG_ENTRY_TYPE
{
	CET_SUBSECTION,
	CET_STRING,
	CET_INT,
	CET_FLOAT,
	CET_NULL
} CONFIG_ENTRY_TYPE;

/* Config-file-entry binary tree. */
typedef struct _CONFIG
{
	LPSTR				szName;
	
	CONFIG_ENTRY_TYPE	entrytype;
	union
	{
		struct _CONFIG *lpcfgSubsection;
		LPSTR			sz;
		int				i;
		float			f;
	};

	struct _CONFIG	*lpcfgLeft, *lpcfgRight;
} CONFIG;


/* Prototypes. */
CONFIG* LoadConfig(LPCTSTR szFilename);
void ConfigDestroy(CONFIG *lpcfg);
void ConfigSetString(CONFIG *lpcfgRoot, LPCSTR szName, LPCSTR szValue);
void ConfigSetFloat(CONFIG *lpcfgRoot, LPCSTR szName, float f);
void ConfigSetInteger(CONFIG *lpcfgRoot, LPCSTR szName, int i);
void ConfigSetAtom(CONFIG *lpcfgRoot, LPCSTR szName);
void ConfigSetSubsection(CONFIG *lpcfgRoot, LPCSTR szName, CONFIG *lpcfgSS);
CONFIG* ConfigAddSubsection(CONFIG *lpcfgRoot, LPCSTR szName);
BOOL ConfigNodeExists(CONFIG *lpcfgRoot, LPCSTR szName);
int ConfigGetInteger(CONFIG *lpcfgRoot, LPCSTR szName);
float ConfigGetFloat(CONFIG *lpcfgRoot, LPCSTR szName);
int ConfigGetStringLength(CONFIG *lpcfgRoot, LPCSTR szName);
BOOL ConfigGetStringA(CONFIG *lpcfgRoot, LPCSTR szName, LPSTR szBuffer, unsigned int cchBuffer);
BOOL ConfigGetStringW(CONFIG *lpcfgRoot, LPCSTR szName, LPWSTR szBuffer, unsigned int cchBuffer);
CONFIG* ConfigGetSubsection(CONFIG *lpcfgRoot, LPCSTR szName);
CONFIG* ConfigCreate(void);
CONFIG* ConfigDuplicate(CONFIG *lpcfg);
BOOL ConfigIterate(CONFIG *lpcfgRoot, BOOL (*lpfnCallback)(CONFIG*, void*), void *lpvParam);

#endif

