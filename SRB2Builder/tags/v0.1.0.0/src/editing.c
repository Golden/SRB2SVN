#include <windows.h>
#include <stdlib.h>
#include <search.h>

#include "general.h"
#include "map.h"
#include "renderer.h"
#include "mapconfig.h"
#include "selection.h"
#include "config.h"
#include "editing.h"
#include "options.h"
#include "../res/resource.h"

#include "win/infobar.h"

#include "CodeImp/ci_map.h"
#include "CodeImp/ci_math.h"
#include "CodeImp/ci_data_proto.h"



/* Types. */

struct _LOOPLIST
{
	DYNAMICINTARRAY		diarrayLinedefs, diarraySameSide;
	int					iSector;
	struct _LOOPLIST	*lplooplistNext;
};

typedef struct _SIDEFLAGMARKER
{
	int iSidedef, iFlag, iSide;
} SIDEFLAGMARKER;


typedef enum _ENUM_LDLOOKUP_TYPES
{
	LDLT_VXTOVX, LDLT_VXTOLD
} ENUM_LDLOOKUP_TYPES;


/* Macros. */
#define LDLOOKUP_INITBUFSIZE 8
#define SDPOLYARRAY_INITBUFSIZE 8
#define LINELOOPARRAY_INITBUFSIZE 8
#define DELLINES_INITBUFSIZE 4

/* Number of new vertices/lines to store per buffer increment. */
#define DRAWOP_BUFFER_COUNT_INCREMENT 64



/* Info used to find intersection of multiple objects' properties. */
typedef struct _CHECKINFO
{
	MAP		*lpmap;
	CONFIG	*lpcfg;
	BOOL	bFirstTime;
	void	*lpdi;		/* [LINEDEF/SECTOR/THING/VERTEX]DISPLAYINFO. */
	DWORD	dwDisplayFlags;
} CHECKINFO;


/* Static prototypes. */
static BOOL CheckLineCallback(int iIndex, CHECKINFO *lpci);
static BOOL CheckSectorCallback(int iIndex, CHECKINFO *lpci);
static BOOL CheckThingCallback(int iIndex, CHECKINFO *lpci);
static BOOL CheckVerticesCallback(int iIndex, CHECKINFO *lpci);
static void AddVertexToDrawList(MAP *lpmap, DRAW_OPERATION *lpdrawop, int iVertex);
static void AddLinedefToDrawList(MAP *lpmap, DRAW_OPERATION *lpdrawop, int iLinedef);
static void ExchangeSidedefs(MAP *lpmap, int iLinedef);
static int BisectLinedef(MAP *lpmap, int iLinedef);
static void GetLineSideSpot(MAP *lpmap, int iLinedef, int iSideOfLine, float fDistance, float *lpx, float *lpy);
static DYNAMICINTARRAY *BuildLDLookupTable(MAP *lpmap, int iLookupType);
static void DestroyLDLookupTable(MAP *lpmap, DYNAMICINTARRAY *lpldlookup);
static BOOL __fastcall VerticesReachableByLookup(DYNAMICINTARRAY *lpldlookup, BOOL *lpbVisited, int iVertex1, int iVertex2);
static void __fastcall PropagateNewSector(MAP *lpmap, DYNAMICINTARRAY *lpldlookup, int iLinedef, int iLinedefSide, int iNewSector, DYNAMICINTARRAY *lpdiarrayAffectedSidedefs);
static int FindNearestAdjacentLinedef(MAP *lpmap, DYNAMICINTARRAY *lpldlookup, int iLinedef, int iLinedefSide, int iWhichVertex, int *lpiLookupIndex, double *lpdAngle, BOOL (*fnCondition)(MAPLINEDEF *lpld, void *lpvParam), void *lpvParam);
static double AdjacentLinedefAngleFromFront(MAP *lpmap, int iLinedef1, int iLinedef2);
static double LinedefAngleV1(MAP *lpmap, int iLinedef);
static double LinedefAngleV2(MAP *lpmap, int iLinedef);
static void SetLinedefSidedef(MAP *lpmap, int iLinedef, int iSidedef, int iLinedefSide);
static void CopySidedefProperties(MAP *lpmap, int iOldSidedef, int iNewSidedef);
static void LineSegmentIntersection(short xA1, short yA1, short xA2, short yA2, short xB1, short yB1, short xB2, short yB2, short *lpxRet, short *lpyRet);
static void JoinSectors(MAP *lpmap, int iNewSector, int iOldSector, BOOL bMerge);
static void LabelEnclosureFromLoops(MAP *lpmap, int iLinedef, LOOPLIST *lplooplist);
static BOOL __fastcall FindLoopFromLine(MAP *lpmap, int iSourceLinedef, int iSourceSide, int iTargetLinedef, int iTargetSide, int iSector, DYNAMICINTARRAY *lpdiarrayLinedefs, DYNAMICINTARRAY *lpdiarrayOrientations, DYNAMICINTARRAY *lpldlookup);
static BOOL __fastcall FindLoopFromNextLinedef(MAP *lpmap, int iSourceLinedef, int iSourceSide, int iNextLinedef, int iNextSide, int iTargetLinedef, int iTargetSide, int iSector, DYNAMICINTARRAY *lpdiarrayLinedefs, DYNAMICINTARRAY *lpdiarrayOrientations, DYNAMICINTARRAY *lpldlookup);
static __inline void ClearLoopAndEnclosureFlags(MAP *lpmap);
static __inline void ClearEnclosureFlags(MAP *lpmap);
static __inline void ClearLoopFlags(MAP *lpmap);
static __inline void ClearRSFlag(MAP *lpmap);
static __inline void ClearLinedefsLabelFlag(MAP *lpmap);
static void ClearLineFlags(MAP *lpmap, WORD wFlags);
static void ClearVertexFlags(MAP *lpmap, WORD wFlags);
static BOOL LinedefIntersectsHalfLine(MAP *lpmap, int iLinedef, float xLine1, float yLine1, float xLine2, float yLine2, FPOINT *lpfpIntersect);
static int FindNearestLinedefOnHalfLine(MAP *lpmap, float xLine1, float yLine1, float xLine2, float yLine2);
static int GetSectorByHalfLineMethod(MAP *lpmap, FPOINT *lpfpOrigin, FPOINT *lpfpOther);
static __inline void SetThingZ(MAP *lpmap, int iThing, CONFIG *lpcfgFlatThings, WORD z);
static __inline WORD GetThingZ(MAP *lpmap, int iThing, CONFIG *lpcfgFlatThings);
static __inline void ApplyRelativeCeilingHeight(MAP *lpmap, int iSector, int iDelta);
static __inline void ApplyRelativeFloorHeight(MAP *lpmap, int iSector, int iDelta);
static __inline void ApplyRelativeBrightness(MAP *lpmap, int iSector, short nDelta);
static __inline void ApplyRelativeThingZ(MAP *lpmap, int iThing, int iDelta, CONFIG *lpcfgFlatThings);
static __inline void ApplyRelativeAngle(MAP *lpmap, int iThing, int iDelta);
static __inline void ApplyRelativeThingX(MAP *lpmap, int iThing, int iDelta);
static __inline void ApplyRelativeThingY(MAP *lpmap, int iThing, int iDelta);
static __inline void ApplyRelativeSidedefX(MAP *lpmap, int iSidedef, int iDelta);
static __inline void ApplyRelativeSidedefY(MAP *lpmap, int iSidedef, int iDelta);



/* RemoveUnusedVertices
 *   Removes all unused vertices from a map.
 *
 * Parameters:
 *   MAP*	lpmap				Map.
 *   BYTE	byRequiredFlags		Only delete vertices that have these flags set.
 *
 * Return value: int
 *   Number of vertices removed.
 */
int RemoveUnusedVertices(MAP *lpmap, BYTE byRequiredFlags)
{
	int i, iVerticesRemoved = 0;
	BOOL *lpbyUsedVertices;

	/* Trivial case. */
	if(lpmap->iVertices <= 0) return 0;

	/* Allocate storage for determining which vertices are used. */
	lpbyUsedVertices = ProcHeapAlloc(lpmap->iVertices * sizeof(BOOL));
	ZeroMemory(lpbyUsedVertices, lpmap->iVertices * sizeof(BOOL));

	/* Loop through all linedefs, marking each vertex as used. */
	for(i = 0; i < lpmap->iLinedefs; i++)
	{
		lpbyUsedVertices[lpmap->linedefs[i].v1] = TRUE;
		lpbyUsedVertices[lpmap->linedefs[i].v2] = TRUE;
	}

	/* Loop through all vertices, removing the unused ones. */
	for(i = lpmap->iVertices - 1; i >= 0; i--)
	{
		if(!lpbyUsedVertices[i] && (lpmap->vertices[i].editflags & byRequiredFlags) == byRequiredFlags)
		{
			iVerticesRemoved++;
			DeleteVertex(lpmap, i);
		}
	}

	return iVerticesRemoved;
}







/* GetLinedefDisplayInfo
 *   Builds info-bar details for one linedef.
 *
 * Parameters:
 *   MAP*					lpmap			Map.
 *   CONFIG*				lpcfgLinedefs	Flat linedef effect descriptions.
 *   int					iIndex			Index of linedef.
 *   LINEDEFDISPLAYINFO*	lplddi			Pointer to return buffer.
 *
 * Return value: None.
 */
void GetLinedefDisplayInfo(MAP *lpmap, CONFIG *lpcfgLinedefFlat, int iIndex, LINEDEFDISPLAYINFO *lplddi)
{
	MAPLINEDEF			*lpld = &lpmap->linedefs[iIndex];
	MAPVERTEX			*lpv1 = &lpmap->vertices[lpld->v1];
	MAPVERTEX			*lpv2 = &lpmap->vertices[lpld->v2];
	MAPSIDEDEF			*lpsd1 = lpld->s1 >= 0 ? &lpmap->sidedefs[lpld->s1] : NULL;
	MAPSIDEDEF			*lpsd2 = lpld->s2 >= 0 ? &lpmap->sidedefs[lpld->s2] : NULL;
	MAPSECTOR			*lps1 = lpsd1 && lpsd1->sector >= 0 && lpsd1->sector < lpmap->iSectors ? &lpmap->sectors[lpsd1->sector] : NULL;
	MAPSECTOR			*lps2 = lpsd2 && lpsd2->sector >= 0 && lpsd2->sector < lpmap->iSectors ? &lpmap->sectors[lpsd2->sector] : NULL;

	lplddi->iIndex = iIndex;
	lplddi->cxVector = lpv2->x - lpv1->x;
	lplddi->cyVector = lpv2->y - lpv1->y;
	lplddi->unTag = lpld->tag;

	/* Ellipses are handled by the control's styles. */
	GetEffectDisplayText(lpld->effect, lpcfgLinedefFlat, lplddi->szEffect, sizeof(lplddi->szEffect) / sizeof(TCHAR));
	lplddi->unEffect = lpld->effect;
	
	if((lplddi->bHasFrontSec = (lps1 != NULL)))
	{
		lplddi->unFrontSector = lpsd1->sector;
		lplddi->iFrontHeight = lps1->hceiling - lps1->hfloor;
	}

	if((lplddi->bHasBackSec = (lps2 != NULL)))
	{
		lplddi->unBackSector = lpsd2->sector;
		lplddi->iBackHeight = lps2->hceiling - lps2->hfloor;
	}

	if((lplddi->bHasFront = (lpsd1 != NULL)))
	{
		lplddi->nFrontX = lpsd1->tx;
		lplddi->nFrontY = lpsd1->ty;

		wsprintf(lplddi->szFrontUpper, TEXT("%.8hs"), lpsd1->upper);
		wsprintf(lplddi->szFrontMiddle, TEXT("%.8hs"), lpsd1->middle);
		wsprintf(lplddi->szFrontLower, TEXT("%.8hs"), lpsd1->lower);

		lplddi->szFrontUpper[8] = lplddi->szFrontMiddle[8] = lplddi->szFrontLower[8] = '\0';
	}

	if((lplddi->bHasBack = (lpsd2 != NULL)))
	{
		lplddi->nBackX = lpsd2->tx;
		lplddi->nBackY = lpsd2->ty;

		wsprintf(lplddi->szBackUpper, TEXT("%.8hs"), lpsd2->upper);
		wsprintf(lplddi->szBackMiddle, TEXT("%.8hs"), lpsd2->middle);
		wsprintf(lplddi->szBackLower, TEXT("%.8hs"), lpsd2->lower);

		lplddi->szBackUpper[8] = lplddi->szBackMiddle[8] = lplddi->szBackLower[8] = '\0';
	}
}



/* GetSectorDisplayInfo
 *   Builds info-bar details for one sector.
 *
 * Parameters:
 *   MAP*				lpmap			Map.
 *   CONFIG*			lpcfgSectors	Sector effects from map config.
 *   int				iIndex			Index of sector.
 *   SECTORDISPLAYINFO*	lpsdi			Pointer to return buffer.
 *
 * Return value: None.
 */
void GetSectorDisplayInfo(MAP *lpmap, CONFIG *lpcfgSectors, int iIndex, SECTORDISPLAYINFO *lpsdi)
{
	MAPSECTOR *lpsec = &lpmap->sectors[iIndex];

	/* Ellipses are handled by the control's styles. */
	GetEffectDisplayText(lpsec->special, lpcfgSectors, lpsdi->szEffect, sizeof(lpsdi->szEffect) / sizeof(TCHAR));
	lpsdi->unEffect = lpsec->special;

	lpsdi->iIndex = iIndex;
	lpsdi->ucBrightness = (unsigned char)lpsec->brightness;
	lpsdi->nCeiling = lpsec->hceiling;
	lpsdi->nFloor = lpsec->hfloor;
	lpsdi->unTag = lpsec->tag;

	wsprintf(lpsdi->szFloor, TEXT("%.8hs"), lpsec->tfloor);
	wsprintf(lpsdi->szCeiling, TEXT("%.8hs"), lpsec->tceiling);
	lpsdi->szFloor[8] = lpsdi->szFloor[8] = '\0';
}



/* GetThingDisplayInfo
 *   Builds info-bar details for one thing.
 *
 * Parameters:
 *   MAP*				lpmap		Map.
 *   CONFIG*			lpcfgThings	Thing properties (flat) from map config.
 *   int				iIndex		Index of thing.
 *   THINGDISPLAYINFO*	lptdi		Pointer to buffer in which to store data.
 *
 * Return value: None.
 */
void GetThingDisplayInfo(MAP *lpmap, CONFIG *lpcfgThings, int iIndex, THINGDISPLAYINFO *lptdi)
{
	MAPTHING *lpthing = &lpmap->things[iIndex];
	CONFIG *lpcfgThingProperties;
				
	GetThingTypeDisplayText(lpthing->thing, lpcfgThings, lptdi->szType, sizeof(lptdi->szType) / sizeof(TCHAR));
	lptdi->unType = lpthing->thing;

	lptdi->iIndex = iIndex;
	lptdi->x = lpthing->x;
	lptdi->y = lpthing->y;
	lptdi->z = GetThingZ(lpmap, iIndex, lpcfgThings);
	lptdi->unFlags = lpthing->flag;
	GetThingDirectionDisplayText(lpthing->angle, lptdi->szDirection, sizeof(lptdi->szDirection) / sizeof(TCHAR));
	lptdi->nDirection = lpthing->angle;

	if((lpcfgThingProperties = GetThingConfigInfo(lpcfgThings, (unsigned short)lpthing->thing)))
		ConfigGetString(lpcfgThingProperties, "sprite", lptdi->szSprite, sizeof(lptdi->szSprite) / sizeof(TCHAR));
	else lptdi->szSprite[0] = '\0';
}



/* GetVertexDisplayInfo
 *   Builds info-bar details for one vertex.
 *
 * Parameters:
 *   MAP*						lpmap	Map.
 *   int						iIndex	Index of vertex.
 *   VERTEXDISPLAYINFO*	lpvdi	Pointer to buffer in which to store data.
 *
 * Return value: None.
 */
void GetVertexDisplayInfo(MAP *lpmap, int iIndex, VERTEXDISPLAYINFO *lpvdi)
{
	MAPVERTEX *lpvx = &lpmap->vertices[iIndex];

	lpvdi->iIndex = iIndex;
	lpvdi->x = lpvx->x;
	lpvdi->y = lpvx->y;
}



/* CheckLineCallback
 *   Sets linedef display information fields and determines which of those match
 *   with the existing data.
 *
 * Parameters:
 *   int		iIndex	Index of linedef.
 *   CHECKINFO*	lpci	Existing aggregate data, validity flags, field
 *						specifying whether this is the first iteration.
 *
 * Return value: BOOL
 *   TRUE if iteration should continue; FALSE otherwise.
 *
 * Remarks:
 *   This is used as a callback by IterateOverSelection. It is handy for
 *   building the set of intersecting properties for selected linedefs.
 */
static BOOL CheckLineCallback(int iIndex, CHECKINFO *lpci)
{
	LINEDEFDISPLAYINFO lddi;
	LINEDEFDISPLAYINFO *lplddiAggregate = (LINEDEFDISPLAYINFO*)lpci->lpdi;

	/* Get all the info for the specified linedef. */
	GetLinedefDisplayInfo(lpci->lpmap, lpci->lpcfg, iIndex, &lddi);

	/* We now have different behaviour depending on whether this is the first
	 * iteration.
	 */
	if(lpci->bFirstTime)
	{
		/* If this is the first time, everything's okay, so copy it all. */
		*lplddiAggregate = lddi;
		lpci->dwDisplayFlags = LDDIF_ALL;
		lpci->bFirstTime = FALSE;
	}
	else
	{
		/* We've iterated at least once already, so find what matches. We work
		 * subtractively.
		 */
		if(lddi.iIndex != lplddiAggregate->iIndex) lpci->dwDisplayFlags &= ~LDDIF_INDEX;
		if(lddi.unEffect != lplddiAggregate->unEffect) lpci->dwDisplayFlags &= ~LDDIF_EFFECT;
		if(lddi.cxVector != lplddiAggregate->cxVector || lddi.cyVector != lplddiAggregate->cyVector) lpci->dwDisplayFlags &= ~LDDIF_VECTOR;
		if(lddi.cxVector * lddi.cxVector + lddi.cyVector * lddi.cyVector !=
			lplddiAggregate->cxVector * lplddiAggregate->cxVector + lplddiAggregate->cyVector * lplddiAggregate->cyVector)
			lpci->dwDisplayFlags &= ~LDDIF_LENGTH;
		if(lddi.unTag != lplddiAggregate->unTag) lpci->dwDisplayFlags &= ~LDDIF_TAG;

		/* Now we have to be a bit careful. We have to negotiate the Boolean
		 * fields with consideration to their meaning. */
		if(lddi.bHasFrontSec != lplddiAggregate->bHasFrontSec) lpci->dwDisplayFlags &= ~(LDDIF_FRONTHEIGHT | LDDIF_FRONTSEC);
		else if(lddi.bHasFrontSec)
		{
			if(lddi.unFrontSector != lplddiAggregate->unFrontSector) lpci->dwDisplayFlags &= ~LDDIF_FRONTSEC;
			if(lddi.iFrontHeight != lplddiAggregate->iFrontHeight) lpci->dwDisplayFlags &= ~LDDIF_FRONTHEIGHT;
		}

		if(lddi.bHasBackSec != lplddiAggregate->bHasBackSec) lpci->dwDisplayFlags &= ~(LDDIF_BACKHEIGHT | LDDIF_BACKSEC);
		else if(lddi.bHasBackSec)
		{
			if(lddi.unBackSector != lplddiAggregate->unBackSector) lpci->dwDisplayFlags &= ~LDDIF_BACKSEC;
			if(lddi.iFrontHeight != lplddiAggregate->iFrontHeight) lpci->dwDisplayFlags &= ~LDDIF_BACKHEIGHT;
		}

		if(lddi.bHasFront != lplddiAggregate->bHasFront) lpci->dwDisplayFlags &= ~(LDDIF_FRONTX | LDDIF_FRONTY | LDDIF_HASFRONT);
		else if(lddi.bHasFront)
		{
			if(lddi.nFrontX != lplddiAggregate->nFrontX) lpci->dwDisplayFlags &= ~LDDIF_FRONTX;
			if(lddi.nFrontY != lplddiAggregate->nFrontY) lpci->dwDisplayFlags &= ~LDDIF_FRONTY;


			/* Texture comparisons now. We make the additional initial check as to
			 * whether we care about them anyway, since the string comparisons are
			 * quite expensive.
			 */

			if((lpci->dwDisplayFlags & LDDIF_FRONTUPPER) && lstrcmp(lddi.szFrontUpper, lplddiAggregate->szFrontUpper))
				 lpci->dwDisplayFlags &= ~LDDIF_FRONTUPPER;

			if((lpci->dwDisplayFlags & LDDIF_FRONTMIDDLE) && lstrcmp(lddi.szFrontMiddle, lplddiAggregate->szFrontMiddle))
				 lpci->dwDisplayFlags &= ~LDDIF_FRONTMIDDLE;

			if((lpci->dwDisplayFlags & LDDIF_FRONTLOWER) && lstrcmp(lddi.szFrontLower, lplddiAggregate->szFrontLower))
				 lpci->dwDisplayFlags &= ~LDDIF_FRONTLOWER;
		}

		if(lddi.bHasBack != lplddiAggregate->bHasBack) lpci->dwDisplayFlags &= ~(LDDIF_BACKX | LDDIF_BACKY | LDDIF_HASBACK);
		else if(lddi.bHasBack)
		{
			if(lddi.nBackX != lplddiAggregate->nBackX) lpci->dwDisplayFlags &= ~LDDIF_BACKX;
			if(lddi.nBackY != lplddiAggregate->nBackY) lpci->dwDisplayFlags &= ~LDDIF_BACKY;

			/* Texture comparisons now. We make the additional initial check as to
			 * whether we care about them anyway, since the string comparisons are
			 * quite expensive.
			 */

			if((lpci->dwDisplayFlags & LDDIF_BACKUPPER) && lstrcmp(lddi.szBackUpper, lplddiAggregate->szBackUpper))
				 lpci->dwDisplayFlags &= ~LDDIF_BACKUPPER;

			if((lpci->dwDisplayFlags & LDDIF_BACKMIDDLE) && lstrcmp(lddi.szBackMiddle, lplddiAggregate->szBackMiddle))
				 lpci->dwDisplayFlags &= ~LDDIF_BACKMIDDLE;

			if((lpci->dwDisplayFlags & LDDIF_BACKLOWER) && lstrcmp(lddi.szBackLower, lplddiAggregate->szBackLower))
				 lpci->dwDisplayFlags &= ~LDDIF_BACKLOWER;
		}
	}

	/* Keep going, unless we already know we have nothing in common. */
	return (lpci->dwDisplayFlags != 0);
}


DWORD CheckLines(MAP *lpmap, SELECTION_LIST *lpsellist, CONFIG *lpcfgLinedefTypesFlat, LINEDEFDISPLAYINFO *lplddi)
{
	CHECKINFO checkinfo;
	int i;

	/* Set the first-iteration field. */
	checkinfo.bFirstTime = TRUE;
	checkinfo.lpdi = (void*)lplddi;
	checkinfo.lpmap = lpmap;
	checkinfo.lpcfg = lpcfgLinedefTypesFlat;

	for(i = 0; i < lpsellist->iDataCount; i++)
		CheckLineCallback(lpsellist->lpiIndices[i], &checkinfo);

	return checkinfo.dwDisplayFlags;
}


DWORD CheckSectors(MAP *lpmap, SELECTION_LIST *lpsellist, CONFIG *lpcfgSecTypes, SECTORDISPLAYINFO *lpsdi)
{
	CHECKINFO checkinfo;
	int i;

	/* Set the first-iteration field. */
	checkinfo.bFirstTime = TRUE;
	checkinfo.lpdi = (void*)lpsdi;
	checkinfo.lpmap = lpmap;
	checkinfo.lpcfg = lpcfgSecTypes;

	for(i = 0; i < lpsellist->iDataCount; i++)
		CheckSectorCallback(lpsellist->lpiIndices[i], &checkinfo);

	return checkinfo.dwDisplayFlags;
}


DWORD CheckThings(MAP *lpmap, SELECTION_LIST *lpsellist, CONFIG *lpcfgFlatThings, THINGDISPLAYINFO *lptdi)
{
	CHECKINFO checkinfo;
	int i;

	/* Set the first-iteration field. */
	checkinfo.bFirstTime = TRUE;
	checkinfo.lpdi = (void*)lptdi;
	checkinfo.lpmap = lpmap;
	checkinfo.lpcfg = lpcfgFlatThings;

	for(i = 0; i < lpsellist->iDataCount; i++)
		CheckThingCallback(lpsellist->lpiIndices[i], &checkinfo);

	return checkinfo.dwDisplayFlags;
}


DWORD CheckVertices(MAP *lpmap, SELECTION_LIST *lpsellist, VERTEXDISPLAYINFO *lpvdi)
{
	CHECKINFO checkinfo;
	int i;

	/* Set the first-iteration field. */
	checkinfo.bFirstTime = TRUE;
	checkinfo.lpdi = (void*)lpvdi;
	checkinfo.lpmap = lpmap;

	for(i = 0; i < lpsellist->iDataCount; i++)
		CheckVerticesCallback(lpsellist->lpiIndices[i], &checkinfo);

	return checkinfo.dwDisplayFlags;
}


/* CheckSectorCallback
 *   Sets sector display information fields and determines which of those match
 *   with the existing data.
 *
 * Parameters:
 *   int		iIndex	Index of sector.
 *   CHECKINFO*	lpci	Existing aggregate data, validity flags, field
 *						specifying whether this is the first iteration.
 *
 * Return value: BOOL
 *   TRUE if iteration should continue; FALSE otherwise.
 *
 * Remarks:
 *   This is used as a callback by IterateOverSelection. It is handy for
 *   building the set of intersecting properties for selected sectors.
 */
static BOOL CheckSectorCallback(int iIndex, CHECKINFO *lpci)
{
	SECTORDISPLAYINFO sdi;
	SECTORDISPLAYINFO *lpsdiAggregate = (SECTORDISPLAYINFO*)lpci->lpdi;

	/* Get all the info for the specified sector. */
	GetSectorDisplayInfo(lpci->lpmap, lpci->lpcfg, iIndex, &sdi);

	/* We now have different behaviour depending on whether this is the first
	 * iteration.
	 */
	if(lpci->bFirstTime)
	{
		/* If this is the first time, everything's okay, so copy it all. */
		*lpsdiAggregate = sdi;
		lpci->dwDisplayFlags = SDIF_ALL;
		lpci->bFirstTime = FALSE;
	}
	else
	{
		/* We've iterated at least once already, so find what matches. We work
		 * subtractively.
		 */
		if(sdi.iIndex != lpsdiAggregate->iIndex) lpci->dwDisplayFlags &= ~SDIF_INDEX;
		if(sdi.nCeiling != lpsdiAggregate->nCeiling) lpci->dwDisplayFlags &= ~SDIF_CEILING;
		if(sdi.nFloor != lpsdiAggregate->nFloor) lpci->dwDisplayFlags &= ~SDIF_FLOOR;
		if(sdi.nCeiling - sdi.nFloor != lpsdiAggregate->nCeiling - lpsdiAggregate->nFloor)
			lpci->dwDisplayFlags &= ~SDIF_HEIGHT;
		if(sdi.unEffect != lpsdiAggregate->unEffect) lpci->dwDisplayFlags &= ~SDIF_EFFECT;
		if(sdi.ucBrightness != lpsdiAggregate->ucBrightness) lpci->dwDisplayFlags &= ~SDIF_BRIGHTNESS;
		if(sdi.unTag != lpsdiAggregate->unTag) lpci->dwDisplayFlags &= ~SDIF_TAG;


		/* Texture comparisons now. We make the additional initial check as to
		 * whether we care about them anyway, since the string comparisons are
		 * quite expensive.
		 */

		if((lpci->dwDisplayFlags & SDIF_CEILINGTEX) && lstrcmp(sdi.szCeiling, lpsdiAggregate->szCeiling))
			 lpci->dwDisplayFlags &= ~SDIF_CEILINGTEX;

		if((lpci->dwDisplayFlags & SDIF_FLOORTEX) && lstrcmp(sdi.szFloor, lpsdiAggregate->szFloor))
			 lpci->dwDisplayFlags &= ~SDIF_FLOORTEX;
	}

	/* Keep going, unless we already know we have nothing in common. */
	return (lpci->dwDisplayFlags != 0);
}



/* CheckThingCallback
 *   Sets thing display information fields and determines which of those match
 *   with the existing data.
 *
 * Parameters:
 *   int		iIndex	Index of sector.
 *   CHECKINFO*	lpci	Existing aggregate data, validity flags, field
 *						specifying whether this is the first iteration.
 *
 * Return value: BOOL
 *   TRUE if iteration should continue; FALSE otherwise.
 *
 * Remarks:
 *   This is used as a callback by IterateOverSelection. It is handy for
 *   building the set of intersecting properties for selected things.
 */
static BOOL CheckThingCallback(int iIndex, CHECKINFO *lpci)
{
	THINGDISPLAYINFO tdi;
	THINGDISPLAYINFO *lptdiAggregate = (THINGDISPLAYINFO*)lpci->lpdi;

	/* Get all the info for the specified sector. */
	GetThingDisplayInfo(lpci->lpmap, lpci->lpcfg, iIndex, &tdi);

	/* We now have different behaviour depending on whether this is the first
	 * iteration.
	 */
	if(lpci->bFirstTime)
	{
		/* If this is the first time, everything's okay, so copy it all. */
		*lptdiAggregate = tdi;
		lpci->dwDisplayFlags = TDIF_ALL;
		lpci->bFirstTime = FALSE;
	}
	else
	{
		/* We've iterated at least once already, so find what matches. We work
		 * subtractively.
		 */
		if(tdi.iIndex != lptdiAggregate->iIndex) lpci->dwDisplayFlags &= ~TDIF_INDEX;
		if(tdi.nDirection != lptdiAggregate->nDirection) lpci->dwDisplayFlags &= ~TDIF_DIRECTION;
		if(tdi.unType != lptdiAggregate->unType) lpci->dwDisplayFlags &= ~TDIF_TYPE;
		if(tdi.unFlags != lptdiAggregate->unFlags) lpci->dwDisplayFlags &= ~TDIF_FLAGS;
		if(tdi.z != lptdiAggregate->z) lpci->dwDisplayFlags &= ~TDIF_Z;
		if(tdi.x != lptdiAggregate->x) lpci->dwDisplayFlags &= ~TDIF_X;
		if(tdi.y != lptdiAggregate->y) lpci->dwDisplayFlags &= ~TDIF_Y;


		/* Sprite comparisons now. We make the additional initial check as to
		 * whether we care about them anyway, since the string comparisons are
		 * quite expensive.
		 */

		if((lpci->dwDisplayFlags & TDIF_SPRITE) && lstrcmp(tdi.szSprite, lptdiAggregate->szSprite))
			 lpci->dwDisplayFlags &= ~TDIF_SPRITE;
	}

	/* Keep going, unless we already know we have nothing in common. */
	return (lpci->dwDisplayFlags != 0);
}

/* CheckVerticesCallback
 *   Sets vertex display information fields and determines which of those match
 *   with the existing data.
 *
 * Parameters:
 *   int		iIndex	Index of sector.
 *   CHECKINFO*	lpci	Existing aggregate data, validity flags, field
 *						specifying whether this is the first iteration.
 *
 * Return value: BOOL
 *   TRUE if iteration should continue; FALSE otherwise.
 *
 * Remarks:
 *   This is used as a callback by IterateOverSelection. It is handy for
 *   building the set of intersecting properties for selected vertices.
 */
static BOOL CheckVerticesCallback(int iIndex, CHECKINFO *lpci)
{
	VERTEXDISPLAYINFO vdi;
	VERTEXDISPLAYINFO *lpvdiAggregate = (VERTEXDISPLAYINFO*)lpci->lpdi;

	/* Get all the info for the specified sector. */
	GetVertexDisplayInfo(lpci->lpmap, iIndex, &vdi);

	/* We now have different behaviour depending on whether this is the first
	 * iteration.
	 */
	if(lpci->bFirstTime)
	{
		/* If this is the first time, everything's okay, so copy it all. */
		*lpvdiAggregate = vdi;
		lpci->dwDisplayFlags = VDIF_ALL;
		lpci->bFirstTime = FALSE;
	}
	else
	{
		/* We've iterated at least once already, so find what matches. We work
		 * subtractively.
		 */
		if(vdi.iIndex != lpvdiAggregate->iIndex) lpci->dwDisplayFlags &= ~VDIF_INDEX;
		if(vdi.x != lpvdiAggregate->x || vdi.y != lpvdiAggregate->y) lpci->dwDisplayFlags &= ~VDIF_COORDS;
	}

	/* Keep going, unless we already know we have nothing in common. */
	return (lpci->dwDisplayFlags != 0);
}


/* CheckLineFlags
 *   For the selected linedefs, determines which flags are the same, and returns
 *   an indication of such, and the corresponding values.
 *
 * Parameters:
 *   MAP			*lpmap			Pointer to map data.
 *   SELECTION_LIST	*lpsellist		List of selected lines.
 *   WORD			*lpwFlagValues	Used to return values of shared flags.
 *   WORD			*lpwFlagMask	Used to return which of the flags in the
 *									above are valid.
 *
 * Return value: None.
 */
void CheckLineFlags(MAP *lpmap, SELECTION_LIST *lpsellist, WORD *lpwFlagValues, WORD *lpwFlagMask)
{
	int i;

	if(lpsellist->iDataCount <= 0)
	{
		*lpwFlagMask = 0;
		return;
	}

	/* Initially, the flags are equal to those of the first linedef, and they're
	 * all valid.
	 */
	*lpwFlagValues = lpmap->linedefs[lpsellist->lpiIndices[0]].flags;
	*lpwFlagMask = 0xFFFF;

	/* Loop through the rest of them, removing any flags that disagree. */
	for(i = 1; i < lpsellist->iDataCount && *lpwFlagMask; i++)
		*lpwFlagMask &= ~(*lpwFlagValues ^ lpmap->linedefs[lpsellist->lpiIndices[i]].flags);
}


/* SetLineFlags
 *   For the selected linedefs, sets the specified flags.
 *
 * Parameters:
 *   MAP			*lpmap			Pointer to map data.
 *   SELECTION_LIST	*lpsellist		List of selected lines.
 *   WORD			wFlagValues		Values of flags.
 *   WORD			wFlagMask		Mask indicating which bits in wFlagValues
 *									are valid.
 *
 * Return value: None.
 */
void SetLineFlags(MAP *lpmap, SELECTION_LIST *lpsellist, WORD wFlagValues, WORD wFlagMask)
{
	int i;

	/* Repeat for each selected linedef. */
	for(i = 0; i < lpsellist->iDataCount; i++)
	{
		/* Set the flags that should be set. */
		lpmap->linedefs[lpsellist->lpiIndices[i]].flags |= wFlagValues & wFlagMask;

		/* Reset the flags that should be reset. */
		lpmap->linedefs[lpsellist->lpiIndices[i]].flags &= wFlagValues | ~wFlagMask;
	}
}


/* CheckThingFlags
 *   For the selected things, determines which flags are the same, and returns
 *   an indication of such, and the corresponding values.
 *
 * Parameters:
 *   MAP			*lpmap			Pointer to map data.
 *   SELECTION_LIST	*lpsellist		List of selected things.
 *   WORD			*lpwFlagValues	Used to return values of shared flags.
 *   WORD			*lpwFlagMask	Used to return which of the flags in the
 *									above are valid.
 *
 * Return value: None.
 */
void CheckThingFlags(MAP *lpmap, SELECTION_LIST *lpsellist, WORD *lpwFlagValues, WORD *lpwFlagMask)
{
	int i;

	if(lpsellist->iDataCount <= 0)
	{
		*lpwFlagMask = 0;
		return;
	}

	/* Initially, the flags are equal to those of the first thing, and they're
	 * all valid.
	 */
	*lpwFlagValues = lpmap->things[lpsellist->lpiIndices[0]].flag;
	*lpwFlagMask = 0xFFFF;

	/* Loop through the rest of them, removing any flags that disagree. */
	for(i = 1; i < lpsellist->iDataCount && *lpwFlagMask; i++)
		*lpwFlagMask &= ~(*lpwFlagValues ^ lpmap->things[lpsellist->lpiIndices[i]].flag);
}


/* SetThingFlags
 *   For the selected things, sets the specified flags.
 *
 * Parameters:
 *   MAP			*lpmap			Pointer to map data.
 *   SELECTION_LIST	*lpsellist		List of selected things.
 *   WORD			wFlagValues		Values of flags.
 *   WORD			wFlagMask		Mask indicating which bits in wFlagValues
 *									are valid.
 *
 * Return value: None.
 */
void SetThingFlags(MAP *lpmap, SELECTION_LIST *lpsellist, WORD wFlagValues, WORD wFlagMask)
{
	int i;

	/* Repeat for each selected linedef. */
	for(i = 0; i < lpsellist->iDataCount; i++)
	{
		/* Set the flags that should be set. */
		lpmap->things[lpsellist->lpiIndices[i]].flag |= wFlagValues & wFlagMask;

		/* Reset the flags that should be reset. */
		lpmap->things[lpsellist->lpiIndices[i]].flag &= wFlagValues | ~wFlagMask;
	}
}


/* CheckTextureFlags
 *   Determines which textures are required for all of the selected linedefs.
 *
 * Parameters:
 *   MAP			*lpmap			Pointer to map data.
 *   SELECTION_LIST	*lpsellist		List of selected linedefs.
 *
 * Return value: BYTE
 *   Flags indicating which textures are required by all lines in the selection.
 */
BYTE CheckTextureFlags(MAP *lpmap, SELECTION_LIST *lpsellist)
{
	int i;
	BYTE byFlags = 0xFF;

	/* No selection means no textures required. */
	if(lpsellist->iDataCount <= 0) return 0;

	/* Loop through the lines, removing any flags that disagree. */
	for(i = 0; i < lpsellist->iDataCount && byFlags; i++)
		byFlags &= RequiredTextures(lpmap, lpsellist->lpiIndices[i]);

	return byFlags;
}


/* NextUnusedTag
 *   Finds the first tag value that is unused in a map.
 *
 * Parameters:
 *   MAP*	lpmap	Pointer to map data.
 *
 * Return value: unsigned short
 *   Value of tag.
 */
unsigned short NextUnusedTag(MAP *lpmap)
{
	/* Fastest way I can think of doing it is sorting all used tags and finding
	 * the first hole.
	 */

	int cn = lpmap->iLinedefs + lpmap->iSectors;		/* Count of tags. */
	unsigned short *lpunTags;
	int i;
	short unNextTag;

	/* Empty map? */
	if(cn == 0) return 0;

	/* Allocate memory. */
	lpunTags = ProcHeapAlloc(cn * sizeof(unsigned short));

	/* Loop through sectors and linedefs, adding their tags to the array. */
	for(i=0; i < lpmap->iSectors; i++)
		lpunTags[i] = lpmap->sectors[i].tag;

	for(; i < cn; i++)
		lpunTags[i] = lpmap->linedefs[i - lpmap->iSectors].tag;

	/* Sort the array. */
	qsort(lpunTags, cn, sizeof(unsigned short), QsortUShortComparison);
	
	/* Find the first hole in the list. Consecutive tags may be equal or differ
	 * by 1 without creating a hole. Stop if we get to the end with no hole.
	 */
	i = 0;
	while(i < cn - 1 && lpunTags[i+1] - lpunTags[i] <= 1) i++;

	/* Return the first integer that fits in the hole, or past the end if no
	 * hole.
	 */
	unNextTag = lpunTags[i] + 1;

	ProcHeapFree(lpunTags);

	return unNextTag;
}


/* GetAdjacentSectorHeightRange
 *   Finds the range of ceiling and floor heights of sectors adjacent to the
 *   selection.
 *
 * Parameters:
 *   MAP*	lpmap			Pointer to map data.
 *   short*	lpnMaxCeil etc.	Buffers to return range in.
 *
 * Return value: BOOL
 *   FALSE if there were no sectors adjacent to the selection, or if there was
 *   no selection; TRUE otherwise.
 */
BOOL GetAdjacentSectorHeightRange(MAP *lpmap, short *lpnMaxCeil, short *lpnMinCeil, short *lpnMaxFloor, short *lpnMinFloor)
{
	short nMinFloor =  32767, nMinCeil =  32767;
	short nMaxFloor = -32768, nMaxCeil = -32768;
	int i;
	BOOL bConsideredAnySectors = FALSE;

	/* All the adjacent sectors will contain at least one selected linedef. Loop
	 * through all linedefs and look at the relevant selected ones.
	 */
	for(i = 0; i < lpmap->iLinedefs; i++)
	{
		/* Linedef is selected? */
		if(lpmap->linedefs[i].selected)
		{
			/* If a sector bounded by this ld is unselected, it's adjacent. */
			if(lpmap->linedefs[i].s1 >= 0)
			{
				MAPSECTOR *lpsec = &lpmap->sectors[lpmap->sidedefs[lpmap->linedefs[i].s1].sector];

				if(!lpsec->selected)
				{
					/* Check whether we need to update the range for this sector. */
					if(lpsec->hfloor < nMinFloor) nMinFloor = lpsec->hfloor;
					if(lpsec->hfloor > nMaxFloor) nMaxFloor = lpsec->hfloor;
					if(lpsec->hceiling < nMinCeil) nMinCeil = lpsec->hceiling;
					if(lpsec->hceiling > nMaxCeil) nMaxCeil = lpsec->hceiling;

					bConsideredAnySectors = TRUE;
				}
			}

			if(lpmap->linedefs[i].s2 >= 0)
			{
				MAPSECTOR *lpsec = &lpmap->sectors[lpmap->sidedefs[lpmap->linedefs[i].s2].sector];

				if(!lpsec->selected)
				{
					/* Check whether we need to update the range for this sector. */
					if(lpsec->hfloor < nMinFloor) nMinFloor = lpsec->hfloor;
					if(lpsec->hfloor > nMaxFloor) nMaxFloor = lpsec->hfloor;
					if(lpsec->hceiling < nMinCeil) nMinCeil = lpsec->hceiling;
					if(lpsec->hceiling > nMaxCeil) nMaxCeil = lpsec->hceiling;

					bConsideredAnySectors = TRUE;
				}
			}
		}
	}

	*lpnMaxCeil = nMaxCeil;
	*lpnMinCeil = nMinCeil;
	*lpnMaxFloor = nMaxFloor;
	*lpnMinFloor = nMinFloor;

	return bConsideredAnySectors;
}


/* GetMapRect
 *   Obtains a bounding rectangle for a map in map co-ordinates.
 *
 * Parameters:
 *   MAP*	lpmap	Pointer to map data.
 *   RECT*	lprc	Rectangle structure to return into.
 *
 * Return value: None.
 */
void GetMapRect(MAP *lpmap, RECT *lprc)
{
	int i;

	/* Sanity check: empty map? */
	if(lpmap->iVertices == 0 && lpmap->iThings == 0)
	{
		/* Return zero rectangle. */
		lprc->bottom = lprc->left = lprc->right = lprc->top = 0;
		return;
	}


	/* Only need to consider vertices and things. */

	/* Set initial rectangle to be a point at the position of the first vertex,
	 * or the first thing if there are no vertices.
	 */
	if(lpmap->iVertices > 0)
	{
		lprc->left = lprc->right = lpmap->vertices[0].x;
		lprc->bottom = lprc->top = lpmap->vertices[0].y;
	}
	else
	{
		lprc->left = lprc->right = lpmap->things[0].x;
		lprc->bottom = lprc->top = lpmap->things[0].y;
	}

	/* Loop through all things and vertices, extending the rectangle as
	 * necessary.
	 */
	for(i = 0; i < lpmap->iVertices; i++)
	{
		if(lpmap->vertices[i].x < lprc->left) lprc->left = lpmap->vertices[i].x;
		if(lpmap->vertices[i].x > lprc->right) lprc->right = lpmap->vertices[i].x;
		if(lpmap->vertices[i].y < lprc->bottom) lprc->bottom = lpmap->vertices[i].y;
		if(lpmap->vertices[i].y > lprc->top) lprc->top = lpmap->vertices[i].y;
	}

	for(i = 0; i < lpmap->iThings; i++)
	{
		if(lpmap->things[i].x < lprc->left) lprc->left = lpmap->things[i].x;
		if(lpmap->things[i].x > lprc->right) lprc->right = lpmap->things[i].x;
		if(lpmap->things[i].y < lprc->bottom) lprc->bottom = lpmap->things[i].y;
		if(lpmap->things[i].y > lprc->top) lprc->top = lpmap->things[i].y;
	}
}


/* ResetMapView
 *   Adjusts the viewport to show the entire map.
 *
 * Parameters:
 *   MAP*		lpmap		Pointer to map data.
 *   MAPVIEW*	lpmapview	Pointer to viewport data.
 *
 * Return value: None.
 */
void ResetMapView(MAP *lpmap, MAPVIEW *lpmapview)
{
	RECT rcMap;

	GetMapRect(lpmap, &rcMap);

	/* Make sure the view is at least 499 wide. This also adjusts the height,
	 * since viewport ratio is maintained. Borders are added later.
	 */
	if(rcMap.right - rcMap.left < 500)
	{
		int dx = (500 - (rcMap.right - rcMap.left)) >> 1;
		rcMap.left -= dx;
		rcMap.right += dx;
	}

	/* Zoom to the rectangle with 10% borders. */
	ZoomToRect(lpmapview, &rcMap, 10);
}



/* ApplySectorPropertiesToSelection
 *   Applies the specified properties to multiple sectors.
 *
 * Parameters:
 *   MAP*				lpmap		Pointer to map data.
 *   SELECTION_LIST*	lpsellist	Selection specifying the sectors to modify.
 *   SECTORDISPLAYINFO*	lpsdi		Structure containing properties to apply.
 *   DWORD				dwFlags		Flags specifying which fields of sdi are
 *									valid.
 *
 * Return value: None.
 */
void ApplySectorPropertiesToSelection(MAP *lpmap, SELECTION_LIST *lpsellist, SECTORDISPLAYINFO *lpsdi, DWORD dwFlags)
{
	int i;

	for(i = 0; i < lpsellist->iDataCount; i++)
		ApplySectorProperties(lpmap, lpsellist->lpiIndices[i], lpsdi, dwFlags);
}


/* ApplySectorProperties
 *   Applies the specified properties to a sector.
 *
 * Parameters:
 *   MAP*				lpmap		Pointer to map data.
 *   int				iIndex		Index of sector to modify.
 *   SECTORDISPLAYINFO*	lpsdi		Structure containing properties to apply.
 *   DWORD				dwFlags		Flags specifying which fields of sdi are
 *									valid.
 *
 * Return value: None.
 */
void ApplySectorProperties(MAP *lpmap, int iIndex, SECTORDISPLAYINFO *lpsdi, DWORD dwFlags)
{
	MAPSECTOR *lpsec = &lpmap->sectors[iIndex];
	/* Check whether each field is valid, and apply it if so. */

	if(dwFlags & SDIF_EFFECT) lpsec->special = lpsdi->unEffect;
	if(dwFlags & SDIF_CEILING) lpsec->hceiling = lpsdi->nCeiling;
	if(dwFlags & SDIF_FLOOR) lpsec->hfloor = lpsdi->nFloor;
	if(dwFlags & SDIF_TAG) lpsec->tag = lpsdi->unTag;
	if(dwFlags & SDIF_BRIGHTNESS) lpsec->brightness = lpsdi->ucBrightness;
	if(dwFlags & SDIF_CEILINGTEX)
	{
#ifdef _UNICODE
			wsprintfA(lpsec->tceiling, "%.8ls", lpsdi->szCeiling);
#else
			CopyMemory(lpsec->tceiling, lpsdi->szCeiling, sizeof(lpsec->tceiling));
#endif
	}
	if(dwFlags & SDIF_FLOORTEX)
	{
#ifdef _UNICODE
			wsprintfA(lpsec->tfloor, "%.8ls", lpsdi->szFloor);
#else
			CopyMemory(lpsec->tfloor, lpsdi->szFloor, sizeof(lpsec->tfloor));
#endif
	}
}



/* ApplyLinePropertiesToSelection
 *   Applies the specified properties to multiple linedefs, and their associated
 *   sidedefs.
 *
 * Parameters:
 *   MAP*					lpmap		Pointer to map data.
 *   SELECTION_LIST*		lpsellist	Selection specifying the linedefs to
 *										modify.
 *   LINEDEFDISPLAYINFO*	lplddi		Structure containing properties to
 *										apply.
 *   DWORD					dwFlags		Flags specifying which fields of lddi
 *										are valid.
 *
 * Return value: None.
 *
 * Remarks:
 *   Linedef flags are not handled here. See SetLineFlags.
 */
void ApplyLinePropertiesToSelection(MAP *lpmap, SELECTION_LIST *lpsellist, LINEDEFDISPLAYINFO *lplddi, DWORD dwFlags)
{
	int i;

	for(i = 0; i < lpsellist->iDataCount; i++)
		ApplyLineProperties(lpmap, lpsellist->lpiIndices[i], lplddi, dwFlags);
}


/* ApplyLineProperties
 *   Applies the specified properties to a linedef and its sidedefs.
 *
 * Parameters:
 *   MAP*					lpmap		Pointer to map data.
 *   int					iIndex		Index of linedef to modify.
 *   LINEDEFDISPLAYINFO*	lplddi		Structure containing properties to
 *										apply.
 *   DWORD					dwFlags		Flags specifying which fields of lddi
 *										are valid.
 *
 * Return value: None.
 *
 * Remarks:
 *   Linedef flags are not handled here. See SetLineFlags.
 */
void ApplyLineProperties(MAP *lpmap, int iIndex, LINEDEFDISPLAYINFO *lplddi, DWORD dwFlags)
{
	MAPLINEDEF *lpld = &lpmap->linedefs[iIndex];

	/* Check whether each field is valid, and apply it if so. */

	if(dwFlags & LDDIF_EFFECT) lpld->effect = lplddi->unEffect;
	if(dwFlags & LDDIF_TAG) lpld->tag = lplddi->unTag;

	/* Front sidedef. */
	if(lpld->s1 >= 0)
	{
		MAPSIDEDEF *lpsd = &lpmap->sidedefs[lpld->s1];

		if(dwFlags & LDDIF_FRONTX) lpsd->tx = lplddi->nFrontX;
		if(dwFlags & LDDIF_FRONTY) lpsd->ty = lplddi->nFrontY;
		if(dwFlags & LDDIF_FRONTSEC) lpsd->sector = lplddi->unFrontSector;

		if(dwFlags & LDDIF_FRONTUPPER)
		{
#ifdef _UNICODE
			wsprintfA(lpsd->upper, "%.8ls", lplddi->szFrontUpper);
#else
			CopyMemory(lpsd->upper, lplddi->szFrontUpper, sizeof(lpsd->upper));
#endif
		}

		if(dwFlags & LDDIF_FRONTMIDDLE)
		{
#ifdef _UNICODE
			wsprintfA(lpsd->middle, "%.8ls", lplddi->szFrontMiddle);
#else
			CopyMemory(lpsd->middle, lplddi->szFrontMiddle, sizeof(lpsd->middle));
#endif
		}

		if(dwFlags & LDDIF_FRONTLOWER)
		{
#ifdef _UNICODE
			wsprintfA(lpsd->lower, "%.8ls", lplddi->szFrontLower);
#else
			CopyMemory(lpsd->lower, lplddi->szFrontLower, sizeof(lpsd->lower));
#endif
		}
	}

	/* Back sidedef. */
	if(lpld->s2 >= 0)
	{
		MAPSIDEDEF *lpsd = &lpmap->sidedefs[lpld->s2];

		if(dwFlags & LDDIF_BACKX) lpsd->tx = lplddi->nBackX;
		if(dwFlags & LDDIF_BACKY) lpsd->ty = lplddi->nBackY;
		if(dwFlags & LDDIF_BACKSEC) lpsd->sector = lplddi->unBackSector;

		if(dwFlags & LDDIF_BACKUPPER)
		{
#ifdef _UNICODE
			wsprintfA(lpsd->upper, "%.8ls", lplddi->szBackUpper);
#else
			CopyMemory(lpsd->upper, lplddi->szBackUpper, sizeof(lpsd->upper));
#endif
		}

		if(dwFlags & LDDIF_BACKMIDDLE)
		{
#ifdef _UNICODE
			wsprintfA(lpsd->middle, "%.8ls", lplddi->szBackMiddle);
#else
			CopyMemory(lpsd->middle, lplddi->szBackMiddle, sizeof(lpsd->middle));
#endif
		}

		if(dwFlags & LDDIF_BACKLOWER)
		{
#ifdef _UNICODE
			wsprintfA(lpsd->lower, "%.8ls", lplddi->szBackLower);
#else
			CopyMemory(lpsd->lower, lplddi->szBackLower, sizeof(lpsd->lower));
#endif
		}
	}
}


/* ApplyThingPropertiesToSelection
 *   Applies the specified properties to multiple sectors.
 *
 * Parameters:
 *   MAP*				lpmap		Pointer to map data.
 *   SELECTION_LIST*	lpsellist	Selection specifying the sectors to modify.
 *   THINGDISPLAYINFO*	lptdi		Structure containing properties to apply.
 *   DWORD				dwFlags		Flags specifying which fields of lptdi are
 *									valid.
 *
 * Return value: None.
 */
void ApplyThingPropertiesToSelection(MAP *lpmap, SELECTION_LIST *lpsellist, THINGDISPLAYINFO *lptdi, DWORD dwFlags)
{
	int i;

	for(i = 0; i < lpsellist->iDataCount; i++)
		ApplyThingProperties(lpmap, lpsellist->lpiIndices[i], lptdi, dwFlags);
}


/* ApplyThingProperties
 *   Applies the specified properties to a thing.
 *
 * Parameters:
 *   MAP*				lpmap		Pointer to map data.
 *   int				iIndex		Index of sector to modify.
 *   THINGDISPLAYINFO*	lptdi		Structure containing properties to apply.
 *   DWORD				dwFlags		Flags specifying which fields of sdi are
 *									valid.
 *
 * Return value: None.
 */
void ApplyThingProperties(MAP *lpmap, int iIndex, THINGDISPLAYINFO *lptdi, DWORD dwFlags)
{
	MAPTHING *lpthing = &lpmap->things[iIndex];
	/* Check whether each field is valid, and apply it if so. */

	if(dwFlags & TDIF_TYPE) lpthing->thing = lptdi->unType;
	if(dwFlags & TDIF_DIRECTION) lpthing->angle = lptdi->nDirection;
	if(dwFlags & TDIF_X) lpthing->x = lptdi->x;
	if(dwFlags & TDIF_Y) lpthing->y = lptdi->y;
}


/* AddVertex
 *   Adds a new vertex to a map.
 *
 * Parameters:
 *   MAP*	lpmap		Pointer to map data.
 *   short	x, y		Co-ordinates of new vertex.
 *
 * Return value: int
 *   Index of new vertex.
 *
 * Remarks:
 *   No snapping is done here. That's the caller's problem.
 */
int AddVertex(MAP *lpmap, short x, short y)
{
	MAPVERTEX *lpvertexNew;

	/* TODO: Realloc space if necessary? */
	if(lpmap->iVertices >= 65536) return -1;

	/* Get pointer to new vertex. */
	lpvertexNew = &lpmap->vertices[lpmap->iVertices];

	/* Set position of new vertex. */
	lpvertexNew->x = x;
	lpvertexNew->y = y;

	/* Initialise other fields. */
	lpvertexNew->selected = 0;

	return lpmap->iVertices++;
}


/* AddThing
 *   Adds a new thing to a map.
 *
 * Parameters:
 *   MAP*	lpmap		Pointer to map data.
 *   short	x, y		Co-ordinates of new thing.
 *
 * Return value: int
 *   Index of new thing.
 *
 * Remarks:
 *   No snapping is done here. That's the caller's problem. Also, while some
 *   fields are set to sensible defaults to stop crashes, they're probably not
 *   really what you want.
 */
int AddThing(MAP *lpmap, short x, short y)
{
	MAPTHING *lpthing;

	/* TODO: Realloc space if necessary? */
	if(lpmap->iThings >= 65536) return -1;

	/* Get pointer to new vertex. */
	lpthing = &lpmap->things[lpmap->iThings];

	ZeroMemory(lpthing, sizeof(MAPTHING));

	/* Set position of new thing. */
	lpthing->x = x;
	lpthing->y = y;

	/* Initialise other fields. */
	lpthing->sector = IntersectSector(x, y, lpmap->vertices, lpmap->linedefs, lpmap->sidedefs, lpmap->iLinedefs, NULL, NULL);

	return lpmap->iThings++;
}


/* AddLinedef
 *   Adds a new linedef to a map.
 *
 * Parameters:
 *   MAP*	lpmap						Pointer to map data.
 *   int	iVertexStart, iVertexEnd	Vertices for linedef.
 *
 * Return value: int
 *   Index of new linedef.
 */
int AddLinedef(MAP *lpmap, int iVertexStart, int iVertexEnd)
{
	MAPLINEDEF *lplinedefNew;

	/* TODO: Realloc space if necessary? */
	if(lpmap->iLinedefs >= 65536) return -1;

	/* Get pointer to new linedef. */
	lplinedefNew = &lpmap->linedefs[lpmap->iLinedefs];

	/* Zero all fields to begin with. */
	ZeroMemory(lplinedefNew, sizeof(MAPLINEDEF));

	/* Set vertices for new linedef. */
	lplinedefNew->v1 = iVertexStart;
	lplinedefNew->v2 = iVertexEnd;

	/* Initialise other fields. */
	lplinedefNew->s1 = lplinedefNew->s2 = -1;

	return lpmap->iLinedefs++;
}


/* AddSector
 *   Adds a new sector to a map.
 *
 * Parameters:
 *   MAP*	lpmap		Pointer to map data.
 *   int	iParent		Index of parent sector from which to inherit properties.
 *						May be set to -1 for no inheritance.
 *
 * Return value: int
 *   Index of new sector.
 */
int AddSector(MAP *lpmap, int iParent)
{
	MAPSECTOR *lpsecNew;

	/* TODO: Realloc space if necessary? */
	if(lpmap->iSectors >= 65536) return -1;

	/* Get pointer to new sector. */
	lpsecNew = &lpmap->sectors[lpmap->iSectors];

	/* Inherit properties from parent. */
	if(iParent >= 0)
	{
		*lpsecNew = lpmap->sectors[iParent];
	}
	else
	{
		/* TODO: No inheritance, so set defaults. */

		/* Zero all fields to begin with. */
		ZeroMemory(lpsecNew, sizeof(MAPSECTOR));
	}

	return lpmap->iSectors++;
}


/* AddSidedef
 *   Adds a new sidedef to a map.
 *
 * Parameters:
 *   MAP*	lpmap		Pointer to map data.
 *   int	iSector		Sector to reference.
 *
 * Return value: int
 *   Index of new sidedef.
 */
int AddSidedef(MAP *lpmap, int iSector)
{
	MAPSIDEDEF *lpsdNew;

	/* TODO: Realloc space if necessary? */
	if(lpmap->iSidedefs >= 65536) return -1;

	/* Get pointer to new sector. */
	lpsdNew = &lpmap->sidedefs[lpmap->iSidedefs];

	/* Zero all fields to begin with. */
	ZeroMemory(lpsdNew, sizeof(MAPSIDEDEF));
	
	/* Set some sensible defaults. TODO. */
	lpsdNew->lower[0] = '-';
	lpsdNew->middle[0] = '-';
	lpsdNew->upper[0] = '-';
	lpsdNew->sector = iSector;

	return lpmap->iSidedefs++;
}


/* BeginDrawOperation
 *   Intialises a draw-buffer structure.
 *
 * Parameters:
 *   DRAW_OPERATION*	lpdrawop	Pointer to draw-op structure.
 *
 * Return value: None.
 *
 * Remarks:
 *   Call EndDrawOperation to free memory when you're finished.
 */
void BeginDrawOperation(DRAW_OPERATION *lpdrawop)
{
	/* Allocate buffers for new vertices and lines. */
	lpdrawop->lpiNewLines = ProcHeapAlloc(DRAWOP_BUFFER_COUNT_INCREMENT * sizeof(int));
	lpdrawop->lpiNewVertices = ProcHeapAlloc(DRAWOP_BUFFER_COUNT_INCREMENT * sizeof(int));

	/* Store buffer sizes. */
	lpdrawop->ciLineBuffer = lpdrawop->ciVertexBuffer = DRAWOP_BUFFER_COUNT_INCREMENT;

	/* They're empty to start with. */
	lpdrawop->iNewLineCount = lpdrawop->iNewVertexCount = 0;
}



/* EndDrawOperation
 *   Frees memory used in a drawing operation and unmarks 'new' flags from new
 *   lines and vertices.
 *
 * Parameters:
 *   MAP*				lpmap		Pointer to map data.
 *   DRAW_OPERATION*	lpdrawop	Pointer to draw-op structure.
 *
 * Return value: None.
 */
void EndDrawOperation(MAP *lpmap, DRAW_OPERATION *lpdrawop)
{
	int i;

	/* Unmark the new vertices. */
	for(i=0; i < lpdrawop->iNewVertexCount; i++)
		lpmap->vertices[lpdrawop->lpiNewVertices[i]].editflags &= ~VEF_NEW;

	/* Unmark the new linedefs. */
	for(i=0; i < lpdrawop->iNewLineCount; i++)
		lpmap->linedefs[lpdrawop->lpiNewLines[i]].editflags &= ~LEF_NEW;

	ProcHeapFree(lpdrawop->lpiNewLines);
	ProcHeapFree(lpdrawop->lpiNewVertices);
}



/* DrawToNewVertex
 *   Draws another vertex and adds a line between it and the previous vertex.
 *
 * Parameters:
 *   MAP*				lpmap		Pointer to map structure.
 *   DRAW_OPERATION*	lpdrawop	Pointer to draw-op structure.
 *   short				x, y		Co-ordinates of new vertex.
 *   BOOL				bStitch		Whether vertices should be stitched.
 *									Coincident vertices are always stitched.
 *
 * Return value: BOOL
 *   TRUE if drawing should continue; FALSE otherwise.
 *
 * Remarks:
 *   If a vertex already exists at (x, y), it is used instead. All the sneaky
 *   drawing stuff is/will be performed here: sector creation etc. If we return
 *   FALSE, the caller should call EndDrawOperation to clean up.
 */
BOOL DrawToNewVertex(MAP *lpmap, DRAW_OPERATION *lpdrawop, short x, short y, BOOL bStitch)
{
	int iVxDist;
	int iVertex;
	int iVertexPrev;
	int iLinedef;
	BOOL bExistingVertex;
	BOOL bKeepGoing = TRUE;

	/* Get index of previous vertex, or -1 if we're first. */
	iVertexPrev = (lpdrawop->iNewVertexCount > 0) ? lpdrawop->lpiNewVertices[lpdrawop->iNewVertexCount-1] : -1;

	/* Find the vertex nearest to where we're going. */
	iVertex = NearestVertex(lpmap, x, y, &iVxDist);

	/* Stitch if enabled, but always stitch if we're bang on. */
	bExistingVertex = (iVertex >= 0 && iVxDist <= (bStitch ? ConfigGetInteger(g_lpcfgMain, "autostitchdistance") : 0));

	/* No vertex within the stitching distance of that position yet? */
	if(!bExistingVertex)
	{
		int iNearestLD, iLDDist;

		/* Create a new vertex there. */
		iVertex = AddVertex(lpmap, x, y);

		/* This might trigger a line-split. Find the nearest linedef. */
		iNearestLD = NearestLinedef(x, y, lpmap->vertices, lpmap->linedefs, lpmap->iLinedefs, &iLDDist);

		/* Split if we're close enough. */
		if(iNearestLD >= 0 && iLDDist <= ConfigGetInteger(g_lpcfgMain, "linesplitdistance"))
		{
			SplitLinedef(lpmap, iNearestLD, iVertex);

			/* Since we're now drawing to a vertex within the existing
			 * structure, we have all the associated problems to worry about. To
			 * make sure that we handle them correctly, indicate that we're
			 * interfering with the layout.
			 */
			bExistingVertex = TRUE;
		}
	}
	else
	{
		/* Update co-ordinates to reflect those of the vertex we're using. */
		x = lpmap->vertices[iVertex].x;
		y = lpmap->vertices[iVertex].y;
	}

	/* If that wasn't the first vertex, add a linedef to it from the previous
	 * vertex.
	 */
	if(iVertexPrev >= 0)
	{
		/* Is there already a linedef between these vertices (in either direction)?
		 */
		iLinedef = FindLinedefBetweenVertices(lpmap, iVertexPrev, iVertex);
		if(iLinedef >= 0)
		{
			/* There was already a linedef there. If it's going the wrong way,
			 * flip it.
			 */
			if(lpmap->linedefs[iLinedef].v1 == iVertex)
				FlipLinedef(lpmap, iLinedef);
		}
		else
		{
			float xSideSpot, ySideSpot;
			int iParentSector;
			BOOL bNewSector;

			/* This is still strictly *ad experimentum*: if creating this
			 * linedef would cross any existing ones, split that linedef and
			 * bifurcate creation of the new one.
			 */
			int iCrossedLinedef = LinedefIntersectingSegmentAtPoint(lpmap->linedefs,
																	lpmap->vertices,
																	(float)lpmap->vertices[iVertexPrev].x,
																	(float)lpmap->vertices[iVertexPrev].y,
																	x,
																	y,
																	lpmap->iLinedefs - 1);
		
			if(iCrossedLinedef >= 0)
			{
				/* Find the point of intersection, and do two linedef insertions. */
				short xIntersect, yIntersect;
				int iIntersectVxDist;
				int iVertexIntersect;

				/* Get the point of intersection. */
				LineSegmentIntersection(lpmap->vertices[iVertexPrev].x,
										lpmap->vertices[iVertexPrev].y,
										x,
										y,
										lpmap->vertices[lpmap->linedefs[iCrossedLinedef].v1].x,
										lpmap->vertices[lpmap->linedefs[iCrossedLinedef].v1].y,
										lpmap->vertices[lpmap->linedefs[iCrossedLinedef].v2].x,
										lpmap->vertices[lpmap->linedefs[iCrossedLinedef].v2].y,
										&xIntersect, &yIntersect);

				/* Find the vertex nearest to where we're going. */
				iVertexIntersect = NearestVertex(lpmap, xIntersect, yIntersect, &iIntersectVxDist);

				/* Unless we're right on top of an existing one, add a new
				 * vertex.
				 */
				if(iVertexIntersect < 0 || iIntersectVxDist > 0)
					iVertexIntersect = AddVertex(lpmap, xIntersect, yIntersect);

				/* Split the linedef. */
				SplitLinedef(lpmap, iCrossedLinedef, iVertexIntersect);

				/* Do two linedef drawings, and then stop. */
				DrawToNewVertex(lpmap, lpdrawop, xIntersect, yIntersect, bStitch);
				return DrawToNewVertex(lpmap, lpdrawop, x, y, bStitch);
			}

			/* No previous linedef, so this might require a new sector. Must
			 * check this before adding the new linedef.
			 */
			bNewSector = bExistingVertex && VerticesReachable(lpmap, iVertex, iVertexPrev);

			/* Create a linedef from the previous vertex to the new one. */
			iLinedef = AddLinedef(lpmap, iVertexPrev, iVertex);

			/* Don't use this LD to determine what sector we're in. */
			lpmap->linedefs[iLinedef].editflags |= LEF_INVALIDSIDEDEFS;

			/* Find what sector we're within. */
			GetLineSideSpot(lpmap, iLinedef, LS_FRONT, 0, &xSideSpot, &ySideSpot);
			iParentSector = IntersectSectorFloat(xSideSpot, ySideSpot, lpmap->vertices, lpmap->linedefs, lpmap->sidedefs, lpmap->iLinedefs, LinedefNoInvalidSD, NULL);

			if(iParentSector >= 0)
			{
				/* Initially set all sidedefs to refer to parent sector. */

				/* Two new sidedefs. */
				int iSD1 = AddSidedef(lpmap, iParentSector);
				int iSD2 = AddSidedef(lpmap, iParentSector);

				/* They're within the parent sector. */
				lpmap->sidedefs[iSD1].sector = iParentSector;
				lpmap->sidedefs[iSD2].sector = iParentSector;

				/* Set them. */
				SetLinedefSidedef(lpmap, iLinedef, iSD1, LS_FRONT);
				SetLinedefSidedef(lpmap, iLinedef, iSD2, LS_BACK);
			}

			/* By this point, if the sidedefs are still -1, they ought to be
			 * that way -- at least with the current linedef configuration.
			 */
			lpmap->linedefs[iLinedef].editflags &= ~LEF_INVALIDSIDEDEFS;
			
			/* If there's no parent sector, we don't create any sidedefs. If the
			 * user closes the sector, front sds will be created then; if not,
			 * the line will just be left without sidedefs.
			 */

			if(bNewSector)
			{
				int iNewSector;
				DYNAMICINTARRAY *lpldlookup;
				DYNAMICINTARRAY diarrayNewSectorSidedefs;
				int i;
				BOOL bInsideOut;
				float xSideSpotSector, ySideSpotSector;

				/* Create a new sector. */
				iNewSector = AddSector(lpmap, iParentSector);

				/* Build vertex-to-ld lookup table. */
				lpldlookup = BuildLDLookupTable(lpmap, LDLT_VXTOLD);

				/* Create empty array for the affected sidedefs. */
				InitialiseDynamicIntArray(&diarrayNewSectorSidedefs, SDPOLYARRAY_INITBUFSIZE);

				/* Set the sidedefs to point to the new sector. */
				PropagateNewSector(lpmap, lpldlookup, iLinedef, LS_FRONT, iNewSector, &diarrayNewSectorSidedefs);

				/* Is the new sector inside-out (i.e. on the external edges of
				 * the polygon)?
				 */
				GetLineSideSpot(lpmap, iLinedef, LS_FRONT, 0.5, &xSideSpotSector, &ySideSpotSector);
				bInsideOut = !PointInSidedefs(lpmap, xSideSpotSector, ySideSpotSector, diarrayNewSectorSidedefs.lpiIndices, diarrayNewSectorSidedefs.uiCount);

				/* Loop through each of the sidedefs referring to the parent
				 * sector, and correct them if they ought to be pointing to the
				 * new one.
				 */
				for(i = 0; i < lpmap->iLinedefs; i++)
				{
					int iSDIndices[2] = {lpmap->linedefs[i].s1, lpmap->linedefs[i].s2};
					int j;

					/* If the linedef's new, we're certainly not interested. */
					if(lpmap->linedefs[i].editflags & LEF_NEW) continue;

					for(j = 0; j < 2; j++)
					{
						MAPSIDEDEF *lpsd = NULL;
						
						if(iSDIndices[j] >= 0)
							lpsd = &lpmap->sidedefs[iSDIndices[j]];

						/* Might we need to change this one? */
						if(iSDIndices[j] < 0 || lpsd->sector == iParentSector)
						{
							BOOL bInsideSidedefPoly;

							GetLineSideSpot(lpmap, i, j == 0 ? LS_FRONT : LS_BACK, 0.5, &xSideSpotSector, &ySideSpotSector);
							bInsideSidedefPoly = PointInSidedefs(lpmap, xSideSpotSector, ySideSpotSector, diarrayNewSectorSidedefs.lpiIndices, diarrayNewSectorSidedefs.uiCount);

							/* We need to change the sd to point to the new
							 * sector if the sector points inwards and the sd
							 * is inside us, or respectively outwards and
							 * outside.
							 */
							if(iSDIndices[j] >= 0)
							{
								if((!bInsideOut && bInsideSidedefPoly) || (bInsideOut && !bInsideSidedefPoly))
								{
									lpsd->sector = iNewSector;
								}
							}
							/* Use a slightly different test when one-sided: the
							 * external case doesn't apply.
							 */
							else if(!bInsideOut && bInsideSidedefPoly)
							{
								/* Need to make a new sidedef. */
								int iSidedef = AddSidedef(lpmap, iNewSector);
								lpsd = &lpmap->sidedefs[iSidedef];
								SetLinedefSidedef(lpmap, i, iSidedef, j == 0 ? LS_FRONT : LS_BACK);
							}
						}
					}	/* Both sidedefs loop. */
				}	/* Linedefs loop. */


				/* Clean up. */
				FreeDynamicIntArray(&diarrayNewSectorSidedefs);
				DestroyLDLookupTable(lpmap, lpldlookup);
			}
		}

		/* Add the linedef to the list of new ones. We do this even if it's there
		 * already. This also marks it new.
		 */
		AddLinedefToDrawList(lpmap, lpdrawop, iLinedef);

		/* Did we return to one of the vertices we've just drawn? If so, we stop
		 * drawing.
		 */
		if(lpmap->vertices[iVertex].editflags & VEF_NEW)
		{
			/* Tell the caller to stop drawing. */
			bKeepGoing = FALSE;
		}
	}

	/* Add the vertex to the list of new ones. We do this even if it's there
	 * already. This also marks it new.
	 */
	AddVertexToDrawList(lpmap, lpdrawop, iVertex);

	/* Indicate whether the drawing operation should continue. */
	return bKeepGoing;
}



/* AddVertexToDrawList, AddLinedefToDrawList
 *   Adds a vertex or linedef to the draw-list, and marks it as new.
 *
 * Parameters:
 *   MAP*				lpmap				Pointer to map structure.
 *   DRAW_OPERATION*	lpdrawop			Pointer to draw-op structure.
 *   int				iVertex/iLinedef	Index of vertex/linedef to add.
 *
 * Return value: None.
 *
 * Remarks:
 *   Largely a copy-paste job, I'm afraid.
 */
static void AddVertexToDrawList(MAP *lpmap, DRAW_OPERATION *lpdrawop, int iVertex)
{
	/* Not enough room in the buffer? */
	if(lpdrawop->ciVertexBuffer == (unsigned int)lpdrawop->iNewVertexCount)
	{
		/* Increase the size of the buffer. */
		lpdrawop->ciVertexBuffer += DRAWOP_BUFFER_COUNT_INCREMENT;
		lpdrawop->lpiNewVertices = ProcHeapReAlloc(lpdrawop->lpiNewVertices, lpdrawop->ciVertexBuffer * sizeof(int));
	}

	/* Add the vertex to the buffer and increment the count. */
	lpdrawop->lpiNewVertices[lpdrawop->iNewVertexCount++] = iVertex;

	/* Mark the vertex as new. */
	lpmap->vertices[iVertex].editflags |= VEF_NEW;
}

static void AddLinedefToDrawList(MAP *lpmap, DRAW_OPERATION *lpdrawop, int iLinedef)
{
	/* Not enough room in the buffer? */
	if(lpdrawop->ciLineBuffer == (unsigned int)lpdrawop->iNewLineCount)
	{
		/* Increase the size of the buffer. */
		lpdrawop->ciLineBuffer += DRAWOP_BUFFER_COUNT_INCREMENT;
		lpdrawop->lpiNewLines = ProcHeapReAlloc(lpdrawop->lpiNewLines, lpdrawop->ciLineBuffer * sizeof(int));
	}

	/* Add the linedef to the buffer and increment the count. */
	lpdrawop->lpiNewLines[lpdrawop->iNewLineCount++] = iLinedef;

	/* Mark the linedef as new. */
	lpmap->linedefs[iLinedef].editflags |= LEF_NEW;
}



/* FlipLinedef
 *   Flips a linedef in the intuitive way.
 *
 * Parameters:
 *   MAP*	lpmap		Pointer to map structure.
 *   int	iLinedef	Index of linedef to flip.
 *
 * Return value: None.
 */
void FlipLinedef(MAP *lpmap, int iLinedef)
{
	int iVertexIntermediate, iSidedefIntermediate;
	MAPLINEDEF *lplinedef = &lpmap->linedefs[iLinedef];

	/* Interchange vertices. */
	iVertexIntermediate = lplinedef->v1;
	lplinedef->v1 = lplinedef->v2;
	lplinedef->v2 = iVertexIntermediate;

	/* Interchange sidedefs. This means that sidedefs don't actually move, in
	 * the sense of their drawing positions.
	 */
	iSidedefIntermediate = lplinedef->s1;
	lplinedef->s1 = lplinedef->s2;
	lplinedef->s2 = iSidedefIntermediate;
}


/* FlipSelectedLinedefs
 *   Flips all linedefs in a selection.
 *
 * Parameters:
 *   MAP*				lpmap		Pointer to map structure.
 *   SELECTION_LIST*	lpsellist	Selection of linedefs to flip.
 *
 * Return value: None.
 */
void FlipSelectedLinedefs(MAP *lpmap, SELECTION_LIST* lpsellist)
{
	int i;
	for(i = 0; i < lpsellist->iDataCount; i++)
		FlipLinedef(lpmap, lpsellist->lpiIndices[i]);
}


/* ExchangeSidedefs
 *   Exchanges a linedef's sidedefs.
 *
 * Parameters:
 *   MAP*	lpmap		Pointer to map structure.
 *   int	iLinedef	Index of linedef whose sidedefs are to be exchanged.
 *
 * Return value: None.
 *
 * Remarks:
 *   This will exchange sidedefs of a single-sided linedef without complaint.
 *   I can't decide whether this is a good thing or a bad thing.
 */
static void ExchangeSidedefs(MAP *lpmap, int iLinedef)
{
	int i;
	MAPLINEDEF *lpld = &lpmap->linedefs[iLinedef];

	i = lpld->s1;
	lpld->s1 = lpld->s2;
	lpld->s2 = i;
}


/* ExchangeSelectedSidedefs
 *   Exchanges sidedefs of all linedefs in a selection.
 *
 * Parameters:
 *   MAP*				lpmap		Pointer to map structure.
 *   SELECTION_LIST*	lpsellist	Selection of linedefs whose sidedefs are to
 *									be exchanged.
 *
 * Return value: None.
 */
void ExchangeSelectedSidedefs(MAP *lpmap, SELECTION_LIST* lpsellist)
{
	int i;
	for(i = 0; i < lpsellist->iDataCount; i++)
		ExchangeSidedefs(lpmap, lpsellist->lpiIndices[i]);
}


/* BisectLinedef
 *   Creates a new vertex half-way along the length of a linedef, and splits the
 *   linedef across that vertex.
 *
 * Parameters:
 *   MAP*	lpmap		Pointer to map structure.
 *   int	iLinedef	Index of linedef to bisect.
 *
 * Return value: int
 *   Index of new linedef.
 *
 * Remarks:
 *   If there's a vertex exactly in the middle already (which admittedly there
 *   shouldn't be), it'll be used instead of creating a new one.
 */
static int BisectLinedef(MAP *lpmap, int iLinedef)
{
	int iVertex, iVxDist;
	short x, y;
	MAPVERTEX *lpvx1 = &lpmap->vertices[lpmap->linedefs[iLinedef].v1];
	MAPVERTEX *lpvx2 = &lpmap->vertices[lpmap->linedefs[iLinedef].v2];

	/* Get the midpoint of the line. */
	x = (short)(((int)lpvx1->x + (int)lpvx2->x) / 2);
	y = (short)(((int)lpvx1->y + (int)lpvx2->y) / 2);

	/* Find the vertex nearest to where we're going. */
	iVertex = NearestVertex(lpmap, x, y, &iVxDist);

	/* Unless there's already a vertex in the middle, create a new one; if there
	 * is one, though, just use it.
	 */
	if(iVertex < 0 || iVxDist > 0)
		iVertex = AddVertex(lpmap, x, y);

	/* Split the linedef at the vertex. */
	return SplitLinedef(lpmap, iLinedef, iVertex);
}


/* BisectSelectedLinedefs
 *   Bisects all linedefs in a selection.
 *
 * Parameters:
 *   MAP*				lpmap		Pointer to map structure.
 *   SELECTION_LIST*	lpsellist	Selection of linedefs to be bisected.
 *
 * Return value: None.
 *
 * Remarks:
 *   The new linedefs are added to the selection.
 */
void BisectSelectedLinedefs(MAP *lpmap, SELECTION_LIST* lpsellist)
{
	int i;
	int iNumOriginallySelected;
	int *lpiNewLines;

	/* Allocate a buffer to store the indices of the new lines. */
	lpiNewLines = ProcHeapAlloc(lpsellist->iDataCount * sizeof(int));

	for(i = 0; i < lpsellist->iDataCount; i++)
		lpiNewLines[i] = BisectLinedef(lpmap, lpsellist->lpiIndices[i]);

	/* Add all the new lines to the selection. */
	iNumOriginallySelected = lpsellist->iDataCount;
	for(i = 0; i < iNumOriginallySelected; i++)
		AddToSelectionList(lpsellist, lpiNewLines[i]);

	/* Free the buffer. */
	ProcHeapFree(lpiNewLines);
}


/* GetVertexFromPosition
 *   Returns the index of the vertex at the specified co-ordinates.
 *
 * Parameters:
 *   MAP*	lpmap	Pointer to map structure.
 *   short	x, y	Co-ordinates of desired vertex.
 *
 * Return value: int
 *   Index of vertex if found; negative otherwise.
 */
int GetVertexFromPosition(MAP *lpmap, short x, short y)
{
	int iDist;
	int iVertex = NearestVertex(lpmap, x, y, &iDist);

	/* Did we find a vertex, and if so, is it exactly where we want it? */
	if(iVertex >= 0 && lpmap->vertices[iVertex].x == x && lpmap->vertices[iVertex].y == y)
		return iVertex;

	/* No vertex at specified position. */
	return -1;
}


/* FindLinedefBetweenVertices
 *   Searches for a linedef joining two vertices in either direction.
 *
 * Parameters:
 *   MAP*	lpmap				Pointer to map structure.
 *   int	iVertexA, iVertexB	Indices of vertices the linedef must join.
 *
 * Return value: int
 *   Index of linedef if found; negative otherwise.
 *
 * Remarks:
 *   See also FindLinedefBetweenVerticesDirected, which cares which way the line
 *   goes.
 */
int FindLinedefBetweenVertices(MAP *lpmap, int iVertexA, int iVertexB)
{
	int iLinedef;

	/* Look for a vertex from A to B first. Return index if found. */
	iLinedef = FindLinedefBetweenVerticesDirected(lpmap, iVertexA, iVertexB);
	if(iLinedef >= 0) return iLinedef;

	/* None from A to B; look from B to A. */
	return FindLinedefBetweenVerticesDirected(lpmap, iVertexB, iVertexA);
}


/* FindLinedefBetweenVerticesDirected
 *   Searches for a linedef beginning at one vertex and ending at another.
 *
 * Parameters:
 *   MAP*	lpmap				Pointer to map structure.
 *   int	iVertexA, iVertexB	Indices of vertices the linedef must join.
 *
 * Return value: int
 *   Index of linedef if found; negative otherwise.
 *
 * Remarks:
 *   See also FindLinedefBetweenVertices, which doesn't care which way the line
 *   goes.
 */
int FindLinedefBetweenVerticesDirected(MAP *lpmap, int iVertex1, int iVertex2)
{
	int i;

	for(i = 0; i < lpmap->iLinedefs; i++)
	{
		/* Does the linedef connect the correct vertices in the desired dirn? */
		if(lpmap->linedefs[i].v1 == iVertex1 && lpmap->linedefs[i].v2 == iVertex2)
			return i;
	}

	/* Not found. */
	return -1;
}



/* Snap
 *   Snaps a point to a rectangular grid.
 *
 * Parameters:
 *   short				*lpx, *lpy		Pointers to co-ordinates of point to
 *										snap.
 *   unsigned short		cxGrid, cyGrid	Grid dimensions.
 *
 * Return value: None.
 */
void Snap(short *lpx, short *lpy, unsigned short cxSnap, unsigned short cySnap)
{
	if(*lpx >= 0)
		*lpx = cxSnap * ((*lpx + (cxSnap >> 1)) / cxSnap);
	else
		*lpx = cxSnap * ((*lpx - (cxSnap >> 1)) / cxSnap);

	if(*lpy >= 0)
		*lpy = cySnap * ((*lpy + (cySnap >> 1)) / cySnap);
	else
		*lpy = cySnap * ((*lpy - (cySnap >> 1)) / cySnap);
}




/* VerticesReachable
 *   Determines whether a path exists between two vertices.
 *
 * Parameters:
 *   MAP*	lpmap				Pointer to map data.
 *   int	iVertex1, iVertex2	Indices of vertices.
 *
 * Return value: TRUE if path exists; FALSE if not.
 *
 * Remarks:
 *   Just creates some data structures and calls VerticesReachableByLookup,
 *   where the reall work is done.
 */
BOOL VerticesReachable(MAP *lpmap, int iVertex1, int iVertex2)
{
	/* We do a recursive depth-first search from iVertex1 for iVertex2. We need
	 * to build a lookup table of edges first, since it would be s-l-o-w
	 * otherwise. We also need to keep track of which vertices we've visited.
	 */
	
	DYNAMICINTARRAY *lpldlookup;
	BOOL *lpbVisited = ProcHeapAlloc(lpmap->iVertices * sizeof(BOOL));
	BOOL bReachable;

	/* We haven't visited any vertices to start with. */
	ZeroMemory(lpbVisited, lpmap->iVertices * sizeof(BOOL));

	/* Build a vx-to-vx lookup table. */
	lpldlookup = BuildLDLookupTable(lpmap, LDLT_VXTOVX);

	/* Now that we've got the lookup table, we can do the actual search. */
	bReachable = VerticesReachableByLookup(lpldlookup, lpbVisited, iVertex1, iVertex2);

	/* Search is finished. Clean up. */
	DestroyLDLookupTable(lpmap, lpldlookup);

	ProcHeapFree(lpbVisited);

	return bReachable;
}



/* VerticesReachable
 *   Determines whether a path exists between two vertices.
 *
 * Parameters:
 *   MAP*	lpmap				Pointer to map data.
 *   int	iVertex1, iVertex2	Indices of vertices.
 *
 * Return value: BOOL
 *   TRUE if path exists; FALSE if not.
 *
 * Remarks:
 *   Just creates some data structures and calls VerticesReachableByLookup,
 *   where the reall work is done.
 */
static BOOL __fastcall VerticesReachableByLookup(DYNAMICINTARRAY *lpldlookup, BOOL *lpbVisited, int iVertex1, int iVertex2)
{
	int i;

	/* Termination case: we're reachable from ourselves. */
	if(iVertex1 == iVertex2) return TRUE;

	/* We've visited ourselves. */
	lpbVisited[iVertex1] = TRUE;

	/* Is it reachable from any of our as-yet-unvisited neighbours? */
	for(i = 0; (unsigned int)i < lpldlookup[iVertex1].uiCount; i++)
		if(!lpbVisited[lpldlookup[iVertex1].lpiIndices[i]] && VerticesReachableByLookup(lpldlookup, lpbVisited, lpldlookup[iVertex1].lpiIndices[i], iVertex2))
			return TRUE;

	/* Not reachable from any of our neighbours, so it isn't reachable from
	 * here.
	 */
	return FALSE;
}


/* GetLineSideSpot
 *   Finds a point a given distance normally from the centre of a linedef.
 *
 * Parameters:
 *   MAP*	lpmap				Pointer to map data.
 *   int	iLinedef			Index of linedef.
 *   int	iSideOfLine			LS_FRONT or LS_BACK: from which side the
 *								distance is to be measured.
 *   float	fDistance			Distance.
 *   float*	lpx, lpy			Point is returned here.
 *
 * Return value: None.
 */
static void GetLineSideSpot(MAP *lpmap, int iLinedef, int iSideOfLine, float fDistance, float *lpx, float *lpy)
{
	FPOINT fptCentre;
	POINT ptNormal;
	MAPLINEDEF *lpld = &lpmap->linedefs[iLinedef];
	MAPVERTEX *lpv1 = &lpmap->vertices[lpld->v1], *lpv2 = &lpmap->vertices[lpld->v2];
	float fNormalLength;

	/* Get the point of the centre of the line. */
	fptCentre.x = (float)(lpv2->x + lpv1->x) / 2;
	fptCentre.y = (float)(lpv2->y + lpv1->y) / 2;

	/* Find the normal. */
	ptNormal.x = lpv2->y - lpv1->y;
	ptNormal.y = lpv1->x - lpv2->x;
	if(iSideOfLine == LS_BACK)
	{
		ptNormal.x = -ptNormal.x;
		ptNormal.y = -ptNormal.y;
	}
	fNormalLength = (float)sqrt(ptNormal.x * ptNormal.x + ptNormal.y * ptNormal.y);

	/* Find the desired point. */
	*lpx = fptCentre.x + (ptNormal.x * fDistance) / fNormalLength;
	*lpy = fptCentre.y + (ptNormal.y * fDistance) / fNormalLength;
}



/* BuildLDLookupTable
 *   Builds a lookup table for adjacent linedefs or vertices given a vertex.
 *
 * Parameters:
 *   MAP*	lpmap			Pointer to map data.
 *   int	iLookupType		LDLT_VXTOVX or LDLT_VXTOLD
 *
 * Return value: DYNAMICINTARRAY*
 *   Pointer to new lookup table.
 *
 * Remarks:
 *   The caller should call DestroyLDLookupTable to free the allocated memory.
 */
static DYNAMICINTARRAY *BuildLDLookupTable(MAP *lpmap, int iLookupType)
{
	DYNAMICINTARRAY *lpldlookup = ProcHeapAlloc(lpmap->iVertices * sizeof(DYNAMICINTARRAY));
	int i;

	/* Allocate an initial buffer for each vertex, to contain the other vertices
	 * joined to each directly.
	 */
	for(i = 0; i < lpmap->iVertices; i++)
		InitialiseDynamicIntArray(&lpldlookup[i], LDLOOKUP_INITBUFSIZE);

	/* Now fill the table. */
	for(i = 0; i < lpmap->iLinedefs; i++)
	{
		int iVerticesToAdd[2];
		int iValues[2];
		size_t j;

		/* Are we doing vx-to-vx, or vx-to-ld? */
		switch(iLookupType)
		{
		case LDLT_VXTOVX:
			/* Add the entry. */
			iVerticesToAdd[0] = lpmap->linedefs[i].v1; iValues[0] = lpmap->linedefs[i].v2;
			iVerticesToAdd[1] = lpmap->linedefs[i].v2; iValues[1] = lpmap->linedefs[i].v1;
			break;

		case LDLT_VXTOLD:
			iVerticesToAdd[0] = lpmap->linedefs[i].v1; iValues[0] = i;
			iVerticesToAdd[1] = lpmap->linedefs[i].v2; iValues[1] = i;
			break;
		}

		for(j = 0; j < sizeof(iVerticesToAdd) / sizeof(int); j++)
		{
			/* If there's nothing to add, skip. */
			if(iVerticesToAdd[j] < 0) continue;

			AddToDynamicIntArray(&lpldlookup[iVerticesToAdd[j]], iValues[j]);
		}
	}

	return lpldlookup;
}


/* DestroyLDLookupTable
 *   Frees memory used by a lookup table.
 *
 * Parameters:
 *   MAP*		lpmap		Pointer to map used to generate table.
 *   DYNAMICINTARRAY*	lpldlookup	Pointer to table to free.
 *
 * Return value: None.
 *
 * Remarks:
 *   No vertices may be added to or removed from the map between creating an
 *   destroying the table.
 */
static void DestroyLDLookupTable(MAP *lpmap, DYNAMICINTARRAY *lpldlookup)
{
	int i;

	for(i = 0; i < lpmap->iVertices; i++)
		FreeDynamicIntArray(&lpldlookup[i]);

	ProcHeapFree(lpldlookup);
}



/* PropagateNewSector
 *   Moves around the most convex polygon from a given linedef, setting sidedefs
 *   to refer to a particular sector.
 *
 * Parameters:
 *   MAP*				lpmap			Pointer to map data.
 *   DYNAMICINTARRAY*	lpldlookup		Pointer to table linking vertices to
 *										linedefs.
 *   int				iLinedef		Linedef to start from.
 *   int				iLinedefSide	LS_FRONT or LS_BACK, as appropriate.
 *   int				iNewSector		Index of sector to propagate to sidedefs.
 *   DYNAMICINTARRAY*	lpdiarrayAffectedSidedefs
 *										Altered sidedefs are returned here. May be
 *										set to NULL if not required.
 *
 * Return value: None.
 *
 * Remarks:
 *   Behaviour is undefined in all but the case where a cycle has just been
 *   created by the addition of a new linedef.
 */
static void __fastcall PropagateNewSector(MAP *lpmap, DYNAMICINTARRAY *lpldlookup, int iLinedef, int iLinedefSide, int iNewSector, DYNAMICINTARRAY *lpdiarrayAffectedSidedefs)
{
	MAPLINEDEF *lpld = &lpmap->linedefs[iLinedef];
	int iSidedef = (iLinedefSide == LS_FRONT ? lpld->s1 : lpld->s2);
	MAPSIDEDEF *lpsd = (iSidedef >= 0 ? &lpmap->sidedefs[iSidedef] : NULL);
	int iNextLinedef;

	/* Termination case: if this side already refers to the new sector, we're
	 * finished.
	 */
	if(lpsd && lpsd->sector == iNewSector) return;

	/* No sidedef yet? Then make one, and add it to the line. */
	if(!lpsd)
	{
		iSidedef = AddSidedef(lpmap, iNewSector);
		lpsd = &lpmap->sidedefs[iSidedef];
		SetLinedefSidedef(lpmap, iLinedef, iSidedef, iLinedefSide);
	}

	/* If the caller's interested, add the sidedef's index to the array of
	 * affected sidedefs.
	 */
	if(lpdiarrayAffectedSidedefs)
		AddToDynamicIntArray(lpdiarrayAffectedSidedefs, iSidedef);
	
	/* Set the sidedef to reference the new sector. */
	lpsd->sector = iNewSector;

	/* Find the linedef adjacent to ourselves that makes the smallest angle with
	 * the appropriate side.
	 */
	iNextLinedef = FindNearestAdjacentLinedef(lpmap, lpldlookup, iLinedef, iLinedefSide, (iLinedefSide == LS_FRONT) ? 1 : 0, NULL, NULL, NULL, NULL);

	/* Is it orientated the same way we are? */
	if(lpld->v1 == lpmap->linedefs[iNextLinedef].v2 || lpld->v2 == lpmap->linedefs[iNextLinedef].v1)
		/* Recurse along the same side. */
		PropagateNewSector(lpmap, lpldlookup, iNextLinedef, iLinedefSide, iNewSector, lpdiarrayAffectedSidedefs);
	else
		/* Recurse along the other side. */
		PropagateNewSector(lpmap, lpldlookup, iNextLinedef, (iLinedefSide == LS_FRONT) ? LS_BACK : LS_FRONT, iNewSector, lpdiarrayAffectedSidedefs);
}



/* FindNearestAdjacentLinedef
 *   Finds the linedef forming the smallest angle with a linedef from a
 *   specified side.
 *
 * Parameters:
 *   MAP*				lpmap			Pointer to map data.
 *   DYNAMICINTARRAY*	lpldlookup		Pointer to table linking vertices to
 *										linedefs.
 *   int				iLinedef		Linedef to start from.
 *   int				iLinedefSide	LS_FRONT or LS_BACK, as appropriate.
 *   int				iWhichVertex	0 for v1, 1 for v2.
 *   int*				lpiLookupIndex	Index to start searching from this
 *										vertex within the lookup. Also returns
 *										the index corresponding to the line. May
 *										be NULL.
 *   double*			lpdAngle		Returns the nearest angle. May be NULL.
 *   BOOL (*)(MAPLINEDEF*, void*)
 *						fnCondition		Function evaluated on each linedef to
 *										determine whether it should be
 *										considered. May be NULL.
 *	void*				lpvParam		Parameter to fnCondition.
 *
 * Return value: int
 *   Index of nearest linedef.
 *
 * Remarks:
 *   If we're the only linedef at the appropriate vertex, then *we* are defined
 *   to be the linedef closest to ourselves. In this case, *lpiLookupIndex and
 *   *dAngle are undefined (with the exception that *dAngle < 4 * PI).
 */
static int FindNearestAdjacentLinedef(MAP *lpmap, DYNAMICINTARRAY *lpldlookup, int iLinedef, int iLinedefSide, int iWhichVertex, int *lpiLookupIndex, double *lpdAngle, BOOL (*fnCondition)(MAPLINEDEF *lpld, void *lpvParam), void *lpvParam)
{
	MAPLINEDEF *lpld = &lpmap->linedefs[iLinedef];
	int iVertex = iWhichVertex == 0 ? lpld->v1 : lpld->v2;
	int i, iNearestLinedef = iLinedef;		/* If no other ld, use self. */
	double dNearestAngle;

	/* Set something outwith the bounds. */
	dNearestAngle = 3 * PI;

	/* Enumerate all the linedefs connected to the specified vertex. */
	for(i = lpiLookupIndex ? *lpiLookupIndex : 0; (unsigned int)i < lpldlookup[iVertex].uiCount; i++)
	{
		double dAng;

		/* Skip lines we're not interested in. */
		if(fnCondition && !fnCondition(&lpmap->linedefs[lpldlookup[iVertex].lpiIndices[i]], lpvParam))
			continue;

		dAng = AdjacentLinedefAngleFromFront(lpmap, iLinedef, lpldlookup[iVertex].lpiIndices[i]);

		/* Skip ourselves and any doubled lines. */
		if(dAng == 0.0f || dAng == 2 * PI)
			continue;

		if(iLinedefSide == LS_BACK) dAng = 2 * PI - dAng;

		if(dAng < dNearestAngle)
		{
			dNearestAngle = dAng;
			if(lpiLookupIndex) *lpiLookupIndex = i;
			iNearestLinedef = lpldlookup[iVertex].lpiIndices[i];
		}
	}

	if(lpdAngle) *lpdAngle = dNearestAngle;
	return iNearestLinedef;
}



/* AdjacentLinedefAngleFromFront
 *   Finds the angle from the front of one linedef to another ld adjacent to it.
 *
 * Parameters:
 *   MAP*	lpmap		Pointer to map data.
 *   int	iLinedef1	Index of ld to measure from.
 *   int	iLinedef2	Index of ld to measure to.
 *
 * Return value: double
 *   Angle between them in radians, in [0, 2pi).
 */
static double AdjacentLinedefAngleFromFront(MAP *lpmap, int iLinedef1, int iLinedef2)
{
	MAPLINEDEF *lpld1 = &lpmap->linedefs[iLinedef1];
	MAPLINEDEF *lpld2 = &lpmap->linedefs[iLinedef2];
	double dAng1, dAng2;
	double dAng;

	/* Find which vertices they have in common. Note that when it's the first
	 * ld's V1, we've got to go clockwise: hence the dAng1 <--> iLindef2 and
	 * vice versa.
	 */
	if(lpld1->v1 == lpld2->v1)
	{
		dAng2 = LinedefAngleV1(lpmap, iLinedef1);
		dAng1 = LinedefAngleV1(lpmap, iLinedef2);
	}
	else if(lpld1->v1 == lpld2->v2)
	{
		dAng2 = LinedefAngleV1(lpmap, iLinedef1);
		dAng1 = LinedefAngleV2(lpmap, iLinedef2);
	}
	else if(lpld1->v2 == lpld2->v1)
	{
		dAng1 = LinedefAngleV2(lpmap, iLinedef1);
		dAng2 = LinedefAngleV1(lpmap, iLinedef2);
	}
	else /* if(lpld1->v2 == lpld2->v2) */
	{
		dAng1 = LinedefAngleV2(lpmap, iLinedef1);
		dAng2 = LinedefAngleV2(lpmap, iLinedef2);
	}

	dAng = dAng2 - dAng1;
	while(dAng < 0) dAng += 2 * PI;
	while(dAng >= 2 * PI) dAng -= 2 * PI;

	return dAng;
}

static double LinedefAngleV1(MAP *lpmap, int iLinedef)
{
	return atan2(lpmap->vertices[lpmap->linedefs[iLinedef].v2].y - lpmap->vertices[lpmap->linedefs[iLinedef].v1].y, lpmap->vertices[lpmap->linedefs[iLinedef].v2].x - lpmap->vertices[lpmap->linedefs[iLinedef].v1].x);
}

static double LinedefAngleV2(MAP *lpmap, int iLinedef)
{
	return atan2(lpmap->vertices[lpmap->linedefs[iLinedef].v1].y - lpmap->vertices[lpmap->linedefs[iLinedef].v2].y, lpmap->vertices[lpmap->linedefs[iLinedef].v1].x - lpmap->vertices[lpmap->linedefs[iLinedef].v2].x);
}


/* SetLinedefSidedef
 *   Sets a one of a linedef's sidedefs, and sets flags accordingly.
 *
 * Parameters:
 *   MAP*	lpmap			Pointer to map data.
 *   int	iLinedef		Index of linedef whose sidedef is to be changed.
 *   int	iSidedef		Index of sidedef.
 *   int	iLinedefSide	LS_FRONT or LS_BACK.
 *
 * Return value: None.
 */
static void SetLinedefSidedef(MAP *lpmap, int iLinedef, int iSidedef, int iLinedefSide)
{
	MAPLINEDEF *lpld = &lpmap->linedefs[iLinedef];

	if(iLinedefSide == LS_FRONT)
		lpld->s1 = iSidedef;
	else
	{
		lpld->s2 = iSidedef;

		/* If this was negative, we're no longer double-sided; otherwise, we
		 * are.
		 */
		if(iSidedef < 0)
		{
			lpld->flags &= ~LDF_TWOSIDED;
			lpld->flags |= LDF_IMPASSABLE;
		}
		else
		{
			lpld->flags |= LDF_TWOSIDED;
			lpld->flags &= ~LDF_IMPASSABLE;
		}
	}

	lpmap->sidedefs[iSidedef].linedef = iLinedef;
}


/* SplitLinedef
 *   Takes one linedef and splits it into two, connected by a specified
 *   intermediate vertex.
 *
 * Parameters:
 *   MAP*	lpmap		Pointer to map data.
 *   int	iLinedef	Index of linedef to split.
 *   int	iVertex		Index of intermediate vertex.
 *
 * Return value: int
 *   Index of new linedef.
 *
 * Remarks:
 *   This doesn't make any attempt to prevent doubled lines.
 */
int SplitLinedef(MAP *lpmap, int iLinedef, int iVertex)
{
	MAPLINEDEF *lpldOrig = &lpmap->linedefs[iLinedef];
	MAPLINEDEF *lpldNew;
	int iOrigVertex2 = lpldOrig->v2;
	int iNewLD;

	/* Make the original linedef go from its v1 to the new vertex. */
	lpldOrig->v2 = iVertex;

	/* Add a new linedef from the new vertex to the original v2. */
	iNewLD = AddLinedef(lpmap, iVertex, iOrigVertex2);
	lpldNew = &lpmap->linedefs[iNewLD];

	/* Copy the linedef properties. */
	lpldNew->effect = lpldOrig->effect;
	lpldNew->flags = lpldOrig->flags;
	lpldNew->selected = lpldOrig->selected;
	lpldNew->tag = lpldOrig->tag;
	lpldNew->editflags = lpldOrig->editflags;

	/* Create duplicated sidedefs for the new linedef. */

	if(lpldOrig->s1 >= 0)
	{
		int iNewSD = AddSidedef(lpmap, lpmap->sidedefs[lpldOrig->s1].sector);
		CopySidedefProperties(lpmap, lpldOrig->s1, iNewSD);
		SetLinedefSidedef(lpmap, iNewLD, iNewSD, LS_FRONT);
	}

	if(lpldOrig->s2 >= 0)
	{
		int iNewSD = AddSidedef(lpmap, lpmap->sidedefs[lpldOrig->s2].sector);
		CopySidedefProperties(lpmap, lpldOrig->s2, iNewSD);
		SetLinedefSidedef(lpmap, iNewLD, iNewSD, LS_BACK);
	}

	return iNewLD;
}


/* CopySidedefProperties
 *   Copies all properties from one sidedef to another.
 *
 * Parameters:
 *   MAP*	lpmap		Pointer to map data.
 *   int	iOldSidedef	Index of sidedef to copy from.
 *   int	iNewSidedef	Index of sidedef to copy to.
 *
 * Return value: None.
 */
static void CopySidedefProperties(MAP *lpmap, int iOldSidedef, int iNewSidedef)
{
	/* We can get away with a shallow copy. */
	lpmap->sidedefs[iNewSidedef] = lpmap->sidedefs[iOldSidedef];
}


/* LineSegmentIntersection
 *   Finds the intersection of two linedef segments.
 *
 * Parameters:
 *   short	(xA1, yA1), (xA2, yA2)	Endpoints of first segment.
 *   short	(xB1, yB1), (xB2, yB2)	Endpoints of second segment.
 *   short	*lpxRet, *lpyRet		Used to return point of intersection.
 *
 * Return value: None.
 *
 * Remarks:
 *   No error-checking: the caller must ensure that the segments do indeed
 *   intersect at a point.
 */
static void LineSegmentIntersection(short xA1, short yA1, short xA2, short yA2, short xB1, short yB1, short xB2, short yB2, short *lpxRet, short *lpyRet)
{
	int iGradientNumerator = (xB2 - xB1)*(yA1 - yB1) - (yB2 - yB1)*(xA1 - xB1);
	int iGradientDenominator = (yB2 - yB1)*(xA2 - xA1) - (xB2 - xB1)*(yA2 - yA1);

	*lpxRet = (iGradientDenominator * xA1 + iGradientNumerator * (xA2 - xA1)) / iGradientDenominator;
	*lpyRet = (iGradientDenominator * yA1 + iGradientNumerator * (yA2 - yA1)) / iGradientDenominator;
}


/* DeleteSector
 *   Deletes a sector from a map.
 *
 * Parameters:
 *   MAP*	lpmap		Pointer to map data.
 *   int	iSector		Sector to delete.
 *
 * Return value: None.
 *
 * Remarks:
 *   This performs a simple sector deletion: no attempt is made to keep the map
 *   correct. Also, this changes sector indices, so the selection will have to
 *   be rebuilt from flags (or cleared) after calling this function; sector
 *   references on sidedefs will be adjusted automatically, however.
 */
void DeleteSector(MAP *lpmap, int iSector)
{
	int i;

	/* Move all the sectors above this one down a space. */
	MoveMemory(&lpmap->sectors[iSector], &lpmap->sectors[iSector+1], (lpmap->iSectors - iSector - 1) * sizeof(MAPSECTOR));

	/* Decrement sector count. */
	lpmap->iSectors--;

	/* Correct sector references on sidedefs. */
	for(i = 0; i < lpmap->iSidedefs; i++)
	{
		MAPSIDEDEF *lpsd = &lpmap->sidedefs[i];

		if(lpsd->sector > iSector) lpsd->sector--;
	}
}


/* DeleteLinedef
 *   Deletes a linedef from a map.
 *
 * Parameters:
 *   MAP*	lpmap		Pointer to map data.
 *   int	iLinedef	Linedef to delete.
 *
 * Return value: None.
 *
 * Remarks:
 *   This performs a simple linedef deletion: no attempt is made to keep the map
 *   correct. Also, this changes linedef indices, so the selection will have to
 *   be rebuilt from flags (or cleared) after calling this function. Any
 *   sidedefs belonging to the line will also be deleted.
 */
void DeleteLinedef(MAP *lpmap, int iLinedef)
{
	/* Delete any sidedefs attached to the line. */
	if(lpmap->linedefs[iLinedef].s1 >= 0) DeleteSidedef(lpmap, lpmap->linedefs[iLinedef].s1);
	if(lpmap->linedefs[iLinedef].s2 >= 0) DeleteSidedef(lpmap, lpmap->linedefs[iLinedef].s2);
	
	/* Move all the linedefs above this one down a space. */
	MoveMemory(&lpmap->linedefs[iLinedef], &lpmap->linedefs[iLinedef+1], (lpmap->iLinedefs - iLinedef - 1) * sizeof(MAPLINEDEF));

	/* Decrement linedef count. */
	lpmap->iLinedefs--;
}


/* DeleteSidedef
 *   Deletes a sidedef from a map.
 *
 * Parameters:
 *   MAP*	lpmap		Pointer to map data.
 *   int	iLinedef	Sidedef to delete.
 *
 * Return value: None.
 */
void DeleteSidedef(MAP *lpmap, int iSidedef)
{	
	int i;

	/* Move all the sidedefs above this one down a space. */
	MoveMemory(&lpmap->sidedefs[iSidedef], &lpmap->sidedefs[iSidedef+1], (lpmap->iSidedefs - iSidedef - 1) * sizeof(MAPSIDEDEF));

	/* Decrement sidedef count. */
	lpmap->iSidedefs--;

	/* Correct sidedef references on linedefs. */
	for(i = 0; i < lpmap->iLinedefs; i++)
	{
		MAPLINEDEF *lpld = &lpmap->linedefs[i];

		if(lpld->s1 == iSidedef) lpld->s1 = -1;
		else if(lpld->s1 > iSidedef) lpld->s1--;

		if(lpld->s2 == iSidedef) lpld->s2 = -1;
		else if(lpld->s2 > iSidedef) lpld->s2--;
	}
}


/* DeleteVertex
 *   Deletes a vertex from a map.
 *
 * Parameters:
 *   MAP*	lpmap		Pointer to map data.
 *   int	iVertex		Vertex to delete.
 *
 * Return value: None.
 *
 * Remarks:
 *   This performs a simple vertex deletion: no attempt is made to keep the map
 *   correct. Also, this changes vertex indices, so the selection will have to
 *   be rebuilt from flags (or cleared) after calling this function; vertex
 *   references on linedefs will be adjusted automatically, however.
 */
void DeleteVertex(MAP *lpmap, int iVertex)
{
	int i;

	/* Move all the vertices above this one down a space. */
	MoveMemory(&lpmap->vertices[iVertex], &lpmap->vertices[iVertex+1], (lpmap->iVertices - iVertex - 1) * sizeof(MAPVERTEX));

	/* Decrement vertex count. */
	lpmap->iVertices--;

	/* Correct vertex references on linedefs. */
	for(i = lpmap->iLinedefs - 1; i >= 0; i--)
	{
		MAPLINEDEF *lpld = &lpmap->linedefs[i];

		if(lpld->v1 == iVertex || lpld->v2 == iVertex)
			DeleteLinedef(lpmap, i);
		else
		{
			if(lpld->v1 > iVertex) lpld->v1--;
			if(lpld->v2 > iVertex) lpld->v2--;
		}
	}
}


/* DeleteThing
 *   Deletes a thing from a map.
 *
 * Parameters:
 *   MAP*	lpmap		Pointer to map data.
 *   int	iThing		Thing to delete.
 *
 * Return value: None.
 *
 * Remarks:
 *   This changes thing indices, so the selection will have to be rebuilt from
 *   flags (or cleared) after calling this function.
 */
void DeleteThing(MAP *lpmap, int iThing)
{
	/* Move all the things above this one down a space. */
	MoveMemory(&lpmap->things[iThing], &lpmap->things[iThing+1], (lpmap->iThings - iThing - 1) * sizeof(MAPTHING));

	/* Decrement thing count. */
	lpmap->iThings--;
}


/* JoinSectors
 *   Joins two sectors into one.
 *
 * Parameters:
 *   MAP*	lpmap		Pointer to map data.
 *   int	iOldSector	Sector whose references are to be changed.
 *   int	iNewSector	Sector reference to replace iOldSector with.
 *   BOOL	bMerge		If TRUE, removes any linedefs at an interface between
 *						iOldSector and iNewSector.
 *
 * Return value: None.
 *
 * Remarks:
 *   This changes sector and (if merging) linedef indices, so the selection will
 *   have to be rebuilt from flags (or cleared) after calling this function;
 *   sector references on sidedefs will be adjusted automatically, however.
 */
static void JoinSectors(MAP *lpmap, int iNewSector, int iOldSector, BOOL bMerge)
{
	int i;

	/* If we're doing a merge, begin by getting rid of all the interface lines.
	 */
	if(bMerge)
	{
		/* We start at the end, since that means we don't need to worry about
		 * changing indices following a deletion.
		 */
		for(i = lpmap->iLinedefs - 1; i >= 0; i--)
		{
			MAPLINEDEF *lpld = &lpmap->linedefs[i];

			/* Do we have both sides, and if so, is one side part of the old
			 * sector and the other part of the new?
			 */
			if(lpld->s1 >= 0 && lpld->s2 >= 0 &&
				((lpmap->sidedefs[lpld->s1].sector == iOldSector && lpmap->sidedefs[lpld->s2].sector == iNewSector)
				 || (lpmap->sidedefs[lpld->s1].sector == iNewSector && lpmap->sidedefs[lpld->s2].sector == iOldSector)))
			{
				/* This line is on the boundary between the old and new sectors,
				 * so delete it.
				 */
				DeleteLinedef(lpmap, i);
			}

		}
	}	/* if(bMerge) */


	/* Loop through all sidedefs, replacing any references to the old sector
	 * with references to the new.
	 */
	for(i = 0; i < lpmap->iSidedefs; i++)
		if(lpmap->sidedefs[i].sector == iOldSector)
			lpmap->sidedefs[i].sector = iNewSector;

	/* Get rid of the old sector. */
	DeleteSector(lpmap, iOldSector);
}


/* JoinSelectedSectors
 *   Joins multiple sectors into one.
 *
 * Parameters:
 *   MAP*				lpmap		Pointer to map data.
 *   SELECTION_LIST*	lpsellist	Selection list, where all sectors are to be
 *									merged into the first of these.
 *   BOOL				bMerge		If TRUE, removes any linedefs at an
 *									interface between old and new sectors.
 *
 * Return value: None.
 *
 * Remarks:
 *   We mangle lpsellist, but that's alright, because this changes sector and
 *   (if merging) linedef indices, so the selections have to be rebuilt anyway.
 *   Sector references on sidedefs will be adjusted automatically.
 */
void JoinSelectedSectors(MAP *lpmap, SELECTION_LIST *lpsellist, BOOL bMerge)
{
	int i;

	/* Sanity check. */
	if(lpsellist->iDataCount < 2) return;

	/* Loop through all the 'old' sectors. */
	for(i = 1; i < lpsellist->iDataCount; i++)
	{
		int j;

		/* Join this sector to the new one. */
		JoinSectors(lpmap, lpsellist->lpiIndices[0], lpsellist->lpiIndices[i], bMerge);

		/* Correct references, since a sector's been deleted. */
		for(j = 0; j < lpsellist->iDataCount; j++)
			if(lpsellist->lpiIndices[j] > lpsellist->lpiIndices[i])
				lpsellist->lpiIndices[j]--;
	}
}


/* LabelSelectedLinesRS
 *   Labels all selected lines as needing their sectors recalculated.
 *
 * Parameters:
 *   MAP*		lpmap		Pointer to map data.
 *
 * Return value: None.
 */
void LabelSelectedLinesRS(MAP *lpmap)
{
	int i;

	for(i = 0; i < lpmap->iLinedefs; i++)
		if(lpmap->linedefs[i].selected)
			lpmap->linedefs[i].editflags |= LEF_RECALCSECTOR;
}


/* LabelRSLinesEnclosure
 *   Determines whether each side of each LEF_RECALCSECTOR line should keep its
 *   sector reference.
 *
 * Parameters:
 *   MAP*		lpmap		Pointer to map data.
 *   LOOPLIST*	lplooplist	List of loops within line marked LEF_RECALCSECTOR.
 *
 * Return value: None.
 *
 * Remarks:
 *   Call LabelRSLinesLoops first to find the loops and do the first round of
 *   labelling. We set LEF_ENCLOSEDFRONT and LEF_ENCLOSEDBACK, indicating
 *   whether the sector reference should be kept. The flags should later be
 *   cleared by a call to ClearEnclosureFlags.
 */
void LabelRSLinesEnclosure(MAP *lpmap, LOOPLIST *lplooplist)
{
	int i;

	/* Each looped side is also enclosed on *that side*. */
	for(i = 0; i < lpmap->iLinedefs; i++)
	{
		MAPLINEDEF *lpld = &lpmap->linedefs[i];
		if(lpld->editflags & LEF_RECALCSECTOR)
		{
			if(lpld->editflags & LEF_LOOPFRONT) lpld->editflags |= LEF_ENCLOSEDFRONT;
			if(lpld->editflags & LEF_LOOPBACK) lpld->editflags |= LEF_ENCLOSEDBACK;
		}
	}

	/* Furthermore, a side is enclosed if the nearest loop enclosing it is
	 * self-enclosed and of the correct sector.
	 */
	for(i = 0; i < lpmap->iLinedefs; i++)
	{
		if(lpmap->linedefs[i].editflags & LEF_RECALCSECTOR)
			LabelEnclosureFromLoops(lpmap, i, lplooplist);
	}

	/* If we get here and a line hasn't been found to be enclosed, then it isn't
	 * enclosed.
	 */
}



/* LabelEnclosureFromLoops
 *   Considers all loops enclosing a linedef, and if the tightest of these has
 *   the correct sector, then the linedef is marked as enclosed on the
 *   appropriate side.
 *
 * Parameters:
 *   MAP*		lpmap		Pointer to map data.
 *   int		iLinedef	Index of linedef.
 *   LOOPLIST*	lplooplist	Linked list of loops.
 *
 * Return value: None.
 */
static void LabelEnclosureFromLoops(MAP *lpmap, int iLinedef, LOOPLIST *lplooplist)
{
	MAPLINEDEF *lpld = &lpmap->linedefs[iLinedef];
	int i;
	const SIDEFLAGMARKER sfm[] = {{lpld->s1, LEF_ENCLOSEDFRONT, LS_FRONT}, {lpld->s2, LEF_ENCLOSEDBACK, LS_BACK}};

	/* Check the front and back. */
	for(i = 0; i < 2; i++)
	{
		if(sfm[i].iSidedef >= 0 && !(lpld->editflags & sfm[i].iFlag))
		{
			LOOPLIST *lplooplistEnclosing = NULL;
			LOOPLIST **lplplooplistEnclosingNext = &lplooplistEnclosing;
			LOOPLIST *lplooplistRover = lplooplist;
			LOOPLIST *lplooplistTightest;
			FPOINT fpt;

			/* Get a point a little away from the line. */
			GetLineSideSpot(lpmap, iLinedef, sfm[i].iSide, 0.1f, &fpt.x, &fpt.y);

			/* Find all the loops enclosing this sidedef. */
			while(lplooplistRover)
			{
				if(PointInLinedefs(lpmap, fpt.x, fpt.y, lplooplistRover->diarrayLinedefs.lpiIndices, lplooplistRover->diarrayLinedefs.uiCount))
				{
					/* We're inside this loop, so add it to the list. */
					*lplplooplistEnclosingNext = ProcHeapAlloc(sizeof(LOOPLIST));

					/* Make a shallow copy of the loop, but change the next
					 * pointer.
					 */
					**lplplooplistEnclosingNext = *lplooplistRover;
					(*lplplooplistEnclosingNext)->lplooplistNext = NULL;

					lplplooplistEnclosingNext = &(*lplplooplistEnclosingNext)->lplooplistNext;
				}

				lplooplistRover = lplooplistRover->lplooplistNext;
			}

			/* Now that we have all the enclosing loops, find the tightest
			 * one.
			 */
			lplooplistTightest = lplooplistEnclosing;
			lplooplistRover = lplooplistTightest ? lplooplistTightest->lplooplistNext : NULL;
			while(lplooplistRover)
			{
				/* There's potential for optimisation here. Firstly, we need
				 * only consider the current loop if its sector and the
				 * current tightest sector are different wrt the sector of
				 * the ld we're interested in. Then, we can propagate the
				 * tightest loop across adjacent linedefs.
				 */

				/* Get a point on the current loop, and see whether it's
				 * inside the one we're assuming to be tightest.
				 */
				GetLineSideSpot(lpmap, lplooplistRover->diarrayLinedefs.lpiIndices[0], LS_FRONT, 0.0f, &fpt.x, &fpt.y);

				if(PointInLinedefs(lpmap, fpt.x, fpt.y, lplooplistTightest->diarrayLinedefs.lpiIndices, lplooplistTightest->diarrayLinedefs.uiCount))
				{
					/* This loop's tighter! */
					lplooplistTightest = lplooplistRover;
				}

				lplooplistRover = lplooplistRover->lplooplistNext;
			}

			/* If the tightest loop is self-enclosing and matches our
			 * sector, then we're enclosed, too.
			 */
			if(lplooplistTightest && lplooplistTightest->iSector == lpmap->sidedefs[sfm[i].iSidedef].sector)
				lpld->editflags |= sfm[i].iFlag;

			/* Clean up. These are shallow copies, so we just free the
			 * nodes.
			 */
			while(lplooplistEnclosing)
			{
				lplooplistRover = lplooplistEnclosing->lplooplistNext;
				ProcHeapFree(lplooplistEnclosing);
				lplooplistEnclosing = lplooplistRover;
			}
		}
	}
}


/* LabelRSLinesLoops
 *   Finds all linedef loops within those needing sectors recalculated,
 *   labelling them and returning a list of arrays of indices.
 *
 * Parameters:
 *   MAP*		lpmap		Pointer to map data.
 *
 * Return value: LOOPLIST*
 *   Linked list of arrays of linedef indices, together with the corresponding
 *   sector index.
 *
 * Remarks:
 *   The lines are labelled with LEF_LOOPFRONT and LEF_LOOPBACK as appropriate.
 *   These are cleared by ClearEnclosureFlags. To free the memory used in the
 *   returned list, call DestroyLoopList.
 */
LOOPLIST* LabelRSLinesLoops(MAP *lpmap)
{
	LOOPLIST *lplooplist = NULL;
	LOOPLIST **lplplooplistNext = &lplooplist;
	DYNAMICINTARRAY *lpldlookup;
	int i, j;

	/* Clear any existing enclosure flags. */
	ClearEnclosureFlags(lpmap);

	/* Build vertex-to-ld lookup table. */
	lpldlookup = BuildLDLookupTable(lpmap, LDLT_VXTOLD);

	/* Loop through all labelled linedefs. */
	for(i = 0; i < lpmap->iLinedefs; i++)
	{
		MAPLINEDEF *lpld = &lpmap->linedefs[i];

		if(lpld->editflags & LEF_RECALCSECTOR)
		{
			SIDEFLAGMARKER sfm[] = {{lpld->s1, LEF_LOOPFRONT, LS_FRONT}, {lpld->s2, LEF_LOOPBACK, LS_BACK}};

			/* Check this line on each of its unmarked sides. */
			for(j = 0; j < 2; j++)
			{
				if(sfm[j].iSidedef >= 0 && !(lpld->editflags & sfm[j].iFlag))
				{
					LOOPLIST looplistNew;
					BOOL bFoundNewLoop;

					/* The details of the loop, if it exists, will be written here.
					 */
					looplistNew.iSector = lpmap->sidedefs[sfm[j].iSidedef].sector;
					InitialiseDynamicIntArray(&looplistNew.diarrayLinedefs, LINELOOPARRAY_INITBUFSIZE);
					InitialiseDynamicIntArray(&looplistNew.diarraySameSide, LINELOOPARRAY_INITBUFSIZE);
					looplistNew.lplooplistNext = NULL;


					/* Look for a loop. */
					bFoundNewLoop = FALSE;
					if(FindLoopFromLine(lpmap, i, sfm[j].iSide, i, sfm[j].iSide, looplistNew.iSector, &looplistNew.diarrayLinedefs, &looplistNew.diarraySameSide, lpldlookup))
					{
						FPOINT fpt;
						unsigned int k;

						/* We found a loop, but it might be on the wrong side.
						 * We check both sides of the line anyway, and what's
						 * more, if the loop's on the wrong side, the wrong
						 * lines may have been selected by angle. So, we only
						 * consider the loop if it's on the correct side.
						 */
						GetLineSideSpot(lpmap, i, sfm[j].iSide, 0.1f, &fpt.x, &fpt.y);
						if(PointInLinedefs(lpmap, fpt.x, fpt.y, looplistNew.diarrayLinedefs.lpiIndices, looplistNew.diarrayLinedefs.uiCount))
						{
							/* We're on the right side. Note that this loop
							 * *must* be new: we would've skipped right over the
							 * first linedef if it hadn't been.
							 */
							bFoundNewLoop = TRUE;
							
							/* Label the lines. */
							for(k = 0; k < looplistNew.diarrayLinedefs.uiCount; k++)
							{
								DWORD dwFlag;

								if(looplistNew.diarraySameSide.lpiIndices[k])
									dwFlag = sfm[j].iFlag;
								else
									dwFlag = (sfm[j].iFlag == LEF_LOOPFRONT) ? LEF_LOOPBACK : LEF_LOOPFRONT;

								lpmap->linedefs[looplistNew.diarrayLinedefs.lpiIndices[k]].editflags |= dwFlag;
							}
						}
						else
						{
							/* We're on the wrong side, so ignore this loop. */
							bFoundNewLoop = FALSE;
						}						
					}

					if(bFoundNewLoop)
					{
						/* We found a new loop, so add it to the list of loops. */
						*lplplooplistNext = ProcHeapAlloc(sizeof(LOOPLIST));
						**lplplooplistNext = looplistNew;
						lplplooplistNext = &(*lplplooplistNext)->lplooplistNext;
					}
					else
					{
						/* No loop, so free the arrays we created. */
						FreeDynamicIntArray(&looplistNew.diarrayLinedefs);
						FreeDynamicIntArray(&looplistNew.diarraySameSide);
					}
				}
			}
		}
	}

	/* Clean up. */
	DestroyLDLookupTable(lpmap, lpldlookup);

	/* Return the list. */
	return lplooplist;
}


/* LabelLoopedLinesRS
 *   Labels any linedefs inside any of a list of loops as needing its sector
 *   references recalculated.
 *
 * Parameters:
 *   MAP*		lpmap		Pointer to map data.
 *   LOOPLIST*	lplooplist	List of loops.
 *
 * Return value: None.
 *
 * Remarks:
 *   Useful for building up the RS sectors incrementally, at a drag operation's
 *   beginning and end.
 */
void LabelLoopedLinesRS(MAP *lpmap, LOOPLIST *lplooplist)
{
	LOOPLIST *lplooplistRover;
	int i;

	/* Repeat for all linedefs. */
	for(i = 0; i < lpmap->iLinedefs; i++)
	{
		/* If it's already labelled, there's no point in checking again. */
		if(!(lpmap->linedefs[i].editflags & LEF_RECALCSECTOR))
		{
			FPOINT fpt;

			/* Get a point on the line. */
			GetLineSideSpot(lpmap, i, LS_FRONT, 0.0f, &fpt.x, &fpt.y);

			/* Check all the loops. */
			lplooplistRover = lplooplist;
			while(lplooplistRover)
			{
				if(PointInLinedefs(lpmap, fpt.x, fpt.y, lplooplistRover->diarrayLinedefs.lpiIndices, lplooplistRover->diarrayLinedefs.uiCount))
				{
					/* We're inside this loop, so mark our line. */
					lpmap->linedefs[i].editflags |= LEF_RECALCSECTOR;

					/* We know we're inside one loop, so we don't care about the
					 * rest. Go to the next linedef.
					 */
					break;
				}

				lplooplistRover = lplooplistRover->lplooplistNext;
			}
		}
	}
}


/* FindLoopFromLine
 *   Attempts to find a loop from one linedef to another.
 *
 * Parameters:
 *   MAP*				lpmap				Pointer to map data.
 *   int				iSourceLinedef		Linedef to start looking from.
 *   int				iSourceSide			Side of source linedef to look from.
 *   BOOL				bSameSideAsTarget	Whether this line is orientated the
 *											same way as the target.
 *   int				iTargetLinedef		Linedef we're trying to reach.
 *   int				iTargetSide			Side of linedef we're trying to
 *											reach.
 *   int				iSector				Sector of the loop.
 *   DYNAMICINTARRAY*	lpdiarrayLinedefs	Array of linedef indices that form
 *											the loop. Filled by the function if
 *											a loop is found.
 *   DYNAMICINTARRAY*	lpdiarrayOrientations
 *											Whether each line is orientated the
 *											same way as the target.
 *   DYNAMICINTARRAY*	lpldlookup			Vertex to linedef lookup table.
 *
 * Return value: BOOL
 *   TRUE if a loop could be found; FALSE if not.
 *
 * Remarks:
 *   This doesn't do the labelling, since the entire loop must be known to do
 *   so.
 */
static BOOL __fastcall FindLoopFromLine(MAP *lpmap, int iSourceLinedef, int iSourceSide, int iTargetLinedef, int iTargetSide, int iSector, DYNAMICINTARRAY *lpdiarrayLinedefs, DYNAMICINTARRAY *lpdiarrayOrientations, DYNAMICINTARRAY *lpldlookup)
{
	/* Find the unlabelled linedef that makes the smallest angle with ourselves.
	 */
	int iLookupIndex = 0;
	double dAngle, dPrevAngle = 4 * PI;
	int iNextLinedef;
	int iNextSide;

	while(iNextLinedef = FindNearestAdjacentLinedef(lpmap, lpldlookup, iSourceLinedef, iSourceSide, (iSourceSide == LS_FRONT) ? 1 : 0, &iLookupIndex, &dAngle, LinedefDragged, NULL),
		dAngle <= dPrevAngle)
	{
		/* Next time, start looking from the next line attached to us. */
		iLookupIndex++;
		dPrevAngle = dAngle;

		/* If we're nearest to ourselves, we're at a dead end. */
		if(iNextLinedef == iSourceLinedef) return FALSE;

		/* Find which side we should consider of the next line. */

		/* Is the next line oriented the same way we are? */
		if(lpmap->linedefs[iSourceLinedef].v1 == lpmap->linedefs[iNextLinedef].v2 || lpmap->linedefs[iSourceLinedef].v2 == lpmap->linedefs[iNextLinedef].v1)
			iNextSide = iSourceSide;
		else
			iNextSide = (iSourceSide == LS_FRONT) ? LS_BACK : LS_FRONT;

		if(FindLoopFromNextLinedef(lpmap, iSourceLinedef, iSourceSide, iNextLinedef, iNextSide, iTargetLinedef, iTargetSide, iSector, lpdiarrayLinedefs, lpdiarrayOrientations, lpldlookup))
			return TRUE;
	}

	/* If we get here, none of our neighbours worked. */
	return FALSE;
}

/* A helper routine for FindLoopFromLine. */
static BOOL __fastcall FindLoopFromNextLinedef(MAP *lpmap, int iSourceLinedef, int iSourceSide, int iNextLinedef, int iNextSide, int iTargetLinedef, int iTargetSide, int iSector, DYNAMICINTARRAY *lpdiarrayLinedefs, DYNAMICINTARRAY *lpdiarrayOrientations, DYNAMICINTARRAY *lpldlookup)
{
	int iNextSidedef = (iNextSide == LS_FRONT) ? lpmap->linedefs[iNextLinedef].s1 : lpmap->linedefs[iNextLinedef].s2;

	/* Oh dear, no sidedef. This only happens in malformed maps. */
	if(iNextSidedef < 0) return FALSE;

	/* If the sectors don't match, there's no loop. */
	if(lpmap->sidedefs[iNextSidedef].sector != iSector) return FALSE;

	/* Having checked all those things, we now see no reason why this line
	 * should fail. Check whether we've completed the loop (in which case,
	 * great), or if we haven't got there yet, recur.
	 */
	if((iNextLinedef == iTargetLinedef && iNextSide == iTargetSide)
		|| FindLoopFromLine(lpmap, iNextLinedef, iNextSide, iTargetLinedef, iTargetSide, iSector, lpdiarrayLinedefs, lpdiarrayOrientations, lpldlookup))
	{
		/* We found a loop! Add the index to the array and return. */
		AddToDynamicIntArray(lpdiarrayLinedefs, iSourceLinedef);
		AddToDynamicIntArray(lpdiarrayOrientations, (iSourceSide == iTargetSide));
		return TRUE;
	}

	/* If we get here, a subsequent line failed. */
	return FALSE;
}


/* DestroyLoopList
 *   Frees memory used by a list of linedef loops.
 *
 * Parameters:
 *   LOOPLIST*	lplooplist	List to free.
 *
 * Return value: None.
 *
 * Remarks:
 *   Call on the pointer returned by LabelRSLinesLoops.
 */
void DestroyLoopList(LOOPLIST *lplooplist)
{
	LOOPLIST *lplooplistNext;

	while(lplooplist)
	{
		/* Free the arrays. */
		FreeDynamicIntArray(&lplooplist->diarrayLinedefs);
		FreeDynamicIntArray(&lplooplist->diarraySameSide);
		
		/* Free the list node. */
		lplooplistNext = lplooplist->lplooplistNext;
		ProcHeapFree(lplooplist);
		lplooplist = lplooplistNext;
	}
}


/* ClearDraggingFlags, ClearLoopAndEnclosureFlags, ClearEnclosureFlags,
 * ClearLoopFlags, ClearRSFlag, ClearLinedefsLabelFlag
 *   Clears linedef flags of various sorts.
 *
 * Parameters:
 *   MAP*		lpmap		Pointer to map data.
 *
 * Return value: None.
 */
void ClearDraggingFlags(MAP *lpmap)
{
	ClearLoopAndEnclosureFlags(lpmap);
	ClearRSFlag(lpmap);
}

static __inline void ClearLoopAndEnclosureFlags(MAP *lpmap)
{
	ClearLoopFlags(lpmap);
	ClearEnclosureFlags(lpmap);
}

static __inline void ClearEnclosureFlags(MAP *lpmap)
{
	ClearLineFlags(lpmap, LEF_LOOPFRONT | LEF_LOOPBACK | LEF_ENCLOSEDFRONT | LEF_ENCLOSEDBACK);
}

static __inline void ClearLoopFlags(MAP *lpmap)
{
	ClearLineFlags(lpmap, LEF_LOOPFRONT | LEF_LOOPBACK);
}

static __inline void ClearRSFlag(MAP *lpmap)
{
	ClearLineFlags(lpmap, LEF_RECALCSECTOR);
}

static __inline void ClearLinedefsLabelFlag(MAP *lpmap)
{
	ClearLineFlags(lpmap, LEF_LABELLED);
}


/* ClearLineFlags
 *   Clears specified linedef flags.
 *
 * Parameters:
 *   MAP*		lpmap		Pointer to map data.
 *   WORD		wFlags		FLags to clear.
 *
 * Return value: None.
 */
static void ClearLineFlags(MAP *lpmap, WORD wFlags)
{
	int i;

	for(i = 0; i < lpmap->iLinedefs; i++)
		lpmap->linedefs[i].editflags &= ~wFlags;
}


/* ClearVertexFlags
 *   Clears specified vertex flags.
 *
 * Parameters:
 *   MAP*		lpmap		Pointer to map data.
 *   WORD		wFlags		FLags to clear.
 *
 * Return value: None.
 */
static void ClearVertexFlags(MAP *lpmap, WORD wFlags)
{
	int i;

	for(i = 0; i < lpmap->iVertices; i++)
		lpmap->vertices[i].editflags &= ~wFlags;
}


/* CorrectDraggedSectorReferences
 *   Sets sector references on dragged sides that are not enclosed from sectors
 *   obtained by considering non-dragged linedefs.
 *
 * Parameters:
 *   MAP*		lpmap		Pointer to map data.
 *
 * Return value: None.
 *
 * Remarks:
 *   The labels are set at the start of a drag operation by
 *   LabelRSLinesEnclosure.
 */
void CorrectDraggedSectorReferences(MAP *lpmap)
{
	int i;

	for(i = 0; i < lpmap->iLinedefs; i++)
	{
		MAPLINEDEF *lpld = &lpmap->linedefs[i];

		if(lpld->editflags & LEF_RECALCSECTOR)
		{
			FPOINT fptOrigin;
			FPOINT fptOther;
			int iNewSector;

			/* Get point on middle of line. */
			GetLineSideSpot(lpmap, i, LS_FRONT, 0.0f, &fptOrigin.x, &fptOrigin.y);

			/* If the side isn't enclosed, set the sector reference from
			 * non-dragged lines.
			 */

			if(lpld->s1 >= 0 && !(lpld->editflags & LEF_ENCLOSEDFRONT))
			{
				GetLineSideSpot(lpmap, i, LS_FRONT, 1.0f, &fptOther.x, &fptOther.y);

				iNewSector = GetSectorByHalfLineMethod(lpmap, &fptOrigin, &fptOther);
				if(iNewSector >= 0)
					lpmap->sidedefs[lpld->s1].sector =  iNewSector;

				ClearLinedefsLabelFlag(lpmap);
			}

			if(lpld->s2 >= 0 && !(lpld->editflags & LEF_ENCLOSEDBACK))
			{
				GetLineSideSpot(lpmap, i, LS_BACK, 1.0f, &fptOther.x, &fptOther.y);

				iNewSector = GetSectorByHalfLineMethod(lpmap, &fptOrigin, &fptOther);
				if(iNewSector >= 0)
					lpmap->sidedefs[lpld->s2].sector =  iNewSector;

				ClearLinedefsLabelFlag(lpmap);
			}
		}
	}
}


/* FlipVertexAboutVerticalAxis, FlipThingAboutVerticalAxis,
 * FlipVertexAboutHorizontalAxis, FlipThingAboutHorizontalAxis
 *   Flips the appropriate sort of object about a line of constant abscissa or
 *   ordinate.
 *
 * Parameters:
 *   MAP*		lpmap		Pointer to map data.
 *   int		iVertex		Index of vertex or thing.
 *				iThing
 *   short		xAxis		Abscissa or ordinate.
 *				yAxis
 *
 * Return value: None.
 */
void FlipVertexAboutVerticalAxis(MAP *lpmap, int iVertex, short xAxis)
{
	lpmap->vertices[iVertex].x = 2 * (int)xAxis - (int)lpmap->vertices[iVertex].x;
}

void FlipThingAboutVerticalAxis(MAP *lpmap, int iThing, short xAxis)
{
	lpmap->things[iThing].x = 2 * (int)xAxis - (int)lpmap->things[iThing].x;
	lpmap->things[iThing].angle = 180 - lpmap->things[iThing].angle + ((lpmap->things[iThing].angle <= 180) ? 0 : 360);
}

void FlipVertexAboutHorizontalAxis(MAP *lpmap, int iVertex, short yAxis)
{
	lpmap->vertices[iVertex].y = 2 * (int)yAxis - (int)lpmap->vertices[iVertex].y;
}

void FlipThingAboutHorizontalAxis(MAP *lpmap, int iThing, short yAxis)
{
	lpmap->things[iThing].y = 2 * (int)yAxis - (int)lpmap->things[iThing].y;
	lpmap->things[iThing].angle = 360 - lpmap->things[iThing].angle;
	if(lpmap->things[iThing].angle == 360) lpmap->things[iThing].angle = 0;
}


/* LinedefIntersectsHalfLine
 *   Determines whether a linedef intersects a half-line.
 *
 * Parameters:
 *   MAP*		lpmap			Pointer to map data.
 *   int		iLinedef		Index of linedef.
 *   float		xLine1, yLine1	Origin of half-line.
 *   float		xLine2, yLine2	Another point on the half-line.
 *   FPOINT*	lpfpIntersect	The point of intersection is returned here, if
 *								it exists.
 *
 * Return value: BOOL
 *   TRUE if they intersect, FALSE if not.
 */
static BOOL LinedefIntersectsHalfLine(MAP *lpmap, int iLinedef, float xLine1, float yLine1, float xLine2, float yLine2, FPOINT *lpfpIntersect)
{
	float x1 = lpmap->vertices[lpmap->linedefs[iLinedef].v1].x;
	float y1 = lpmap->vertices[lpmap->linedefs[iLinedef].v1].y;
	float x2 = lpmap->vertices[lpmap->linedefs[iLinedef].v2].x;
	float y2 = lpmap->vertices[lpmap->linedefs[iLinedef].v2].y;
	float fGradientLinedef = 0.0f, fGradientHalfLine = 0.0f;
	float fMinusHLc = 0.0f;		/* Minus the y intercept. */

	if(xLine1 != xLine2)
	{
		fGradientHalfLine = (yLine2 - yLine1) / (xLine2 - xLine1);
		fMinusHLc = xLine1 * fGradientHalfLine - yLine1;
	}

	if(x2 != x1) fGradientLinedef = (y2 - y1) / (x2 - x1);

	if(fGradientLinedef != fGradientHalfLine || xLine1 == xLine2 || x1 == x2)
	{
		if(x1 != x2 && xLine1 != xLine2)
		{
			lpfpIntersect->x = (fMinusHLc + y1 - x1 * fGradientLinedef) / (fGradientHalfLine - fGradientLinedef);
			lpfpIntersect->y = fGradientHalfLine * lpfpIntersect->x - fMinusHLc;
		}
		else if(xLine1 == xLine2 && x1 == x2)	
		{
			/* Both vertical: they definitely don't intersect a point. */
			return FALSE;
		}
		else if(xLine1 == xLine2)	/* x1 != x2 */
		{
			lpfpIntersect->x = xLine1;
			lpfpIntersect->y = fGradientLinedef * (lpfpIntersect->x - x1) + y1;
		}
		else						/* x1 == x2 && xLine1 != xLine2 */
		{
			lpfpIntersect->x = x1;
			lpfpIntersect->y = fGradientHalfLine * lpfpIntersect->x - fMinusHLc;
		}
	}
	else
	{
		/* Parallel or co-incident, and neither vertical. */
		return FALSE;
	}

	/* If we're not on the linedef, then we don't intersect. */
	if(!InLDRectClosed(lpmap->linedefs, lpmap->vertices, iLinedef, lpfpIntersect->x, lpfpIntersect->y))
		return FALSE;

	/* Determine whether we're on the right half of the line. */
	if(xLine2 < xLine1) return lpfpIntersect->x < xLine1 - 0.0001f;
	if(xLine2 > xLine1) return lpfpIntersect->x > xLine1 + 0.0001f;
	if(yLine2 < yLine1) return lpfpIntersect->y < yLine1 - 0.0001f;
	return lpfpIntersect->y > yLine1 + 0.0001f;
}


/* FindNearestLinedefOnHalfLine
 *   Finds the linedef that intersects a half-line closest to its origin.
 *
 * Parameters:
 *   MAP*		lpmap			Pointer to map data.
 *   float		xLine1, yLine1	Origin of half-line.
 *   float		xLine2, yLine2	Another point on the half-line.
 *
 * Return value: int
 *   Index of linedef, or negative if none exists.
 */
static int FindNearestLinedefOnHalfLine(MAP *lpmap, float xLine1, float yLine1, float xLine2, float yLine2)
{
	int i;
	int iNearestLD = -1;
	float fMinDist = 100000.0f;

	/* Loop through all linedefs. */
	for(i = 0; i < lpmap->iLinedefs; i++)
	{
		FPOINT fpIntersect;

		if(LinedefIntersectsHalfLine(lpmap, i, xLine1, yLine1, xLine2, yLine2, &fpIntersect))
		{
			float fDist = distancef(fpIntersect.x, fpIntersect.y, xLine1, yLine1);
			if(fDist < fMinDist)
			{
				fMinDist = fDist;
				iNearestLD = i;
			}
		}
	}

	return iNearestLD;
}


/* GetSectorByHalfLineMethod
 *   Finds the sector at a point in a given direction using an algorithm
 *   involving recursive searches along half-lines.
 *
 * Parameters:
 *   MAP*		lpmap		Pointer to map data.
 *   FPOINT*	lpfpOrigin	Origin of half-line.
 *   FPOINT*	lpfpOther	Another point on the half-line.
 *
 * Return value: int
 *   Index of sector, or negative if none found.
 *
 * Remarks:
 *   Only non-dragged lines are considered. This function is intended for
 *   setting sector references following a drag.
 */
static int GetSectorByHalfLineMethod(MAP *lpmap, FPOINT *lpfpOrigin, FPOINT *lpfpOther)
{
	int iLinedef = FindNearestLinedefOnHalfLine(lpmap, lpfpOrigin->x, lpfpOrigin->y, lpfpOther->x, lpfpOther->y);

	if(iLinedef >= 0)
	{
		MAPLINEDEF *lpld = &lpmap->linedefs[iLinedef];
		FPOINT fpVx1, fpVx2;
		int iSide;

		/* If we've visited it already, give up. */
		if(lpld->editflags & LEF_LABELLED)
			return -1;

		/* Get co-ordinates of the line's vertices. */
		fpVx1.x = lpmap->vertices[lpld->v1].x;
		fpVx1.y = lpmap->vertices[lpld->v1].y;
		fpVx2.x = lpmap->vertices[lpld->v2].x;
		fpVx2.y = lpmap->vertices[lpld->v2].y;

		/* Figure out which sidedef we should be using. */
		if(side_of_lineff(fpVx1.x, fpVx1.y, fpVx2.x, fpVx2.y, lpfpOrigin->x, lpfpOrigin->y) < 0)
			iSide = LS_FRONT;
		else iSide = LS_BACK;

		/* Are we allowed to use this side? */
		if(!(lpld->editflags & LEF_RECALCSECTOR) || (lpld->editflags & (iSide == LS_FRONT ? LEF_ENCLOSEDFRONT : LEF_ENCLOSEDBACK)))
		{
			int iSidedef = (iSide == LS_FRONT) ? lpld->s1 : lpld->s2;

			/* Return the sector (unless there is none). */
			return (iSidedef >= 0) ? lpmap->sidedefs[iSidedef].sector : -1;
		}
		else
		{
			/* Can't use this line, so bifurcate along it. */
			FPOINT fpNewOrigin, fpMidPoint;
			int iSector;

			/* If we ever end up here again, give up. */
			lpld->editflags |= LEF_LABELLED;

			/* Get mid-point of line. */
			GetLineSideSpot(lpmap, iLinedef, iSide, 0.0f, &fpMidPoint.x, &fpMidPoint.y);

			/* Get a point a little away as origin of half-line. */
			GetLineSideSpot(lpmap, iLinedef, iSide, 0.1f, &fpNewOrigin.x, &fpNewOrigin.y);

			/* Try half-line parallel to linedef and towards v1 first; if that
			 * fails, try v2.
			 */
			fpVx1.x += fpNewOrigin.x - fpMidPoint.x;
			fpVx1.y += fpNewOrigin.y - fpMidPoint.y;

			iSector = GetSectorByHalfLineMethod(lpmap, &fpNewOrigin, &fpVx1);
			if(iSector >= 0) return iSector;

			fpVx2.x += fpNewOrigin.x - fpMidPoint.x;
			fpVx2.y += fpNewOrigin.y - fpMidPoint.y;

			return GetSectorByHalfLineMethod(lpmap, &fpNewOrigin, &fpVx2);
		}
	}

	/* No linedef found at all. */
	return -1;
}


/* SetThingSelectionZ
 *   For the selected things, sets the z-offset.
 *
 * Parameters:
 *   MAP*				lpmap				Pointer to map data.
 *   SELECTION_LIST*	lpsellist			List of selected things.
 *   CONFIG*			lpcfgFlatThings		Flat things section.
 *   WORD				z					Z-offset.
 *
 * Return value: None.
 */
void SetThingSelectionZ(MAP *lpmap, SELECTION_LIST *lpsellist, CONFIG *lpcfgFlatThings, WORD z)
{
	int i;

	/* Repeat for each selected thing. */
	for(i = 0; i < lpsellist->iDataCount; i++)
		SetThingZ(lpmap, lpsellist->lpiIndices[i], lpcfgFlatThings, z);
}


/* SetThingZ
 *   For the specified thing, sets the z-offset.
 *
 * Parameters:
 *   MAP*		lpmap				Pointer to map data.
 *   int		iThing				Index of thing.
 *   CONFIG*	lpcfgFlatThings		Flat things section.
 *   WORD		z					Z-offset.
 *
 * Return value: None.
 */
static __inline void SetThingZ(MAP *lpmap, int iThing, CONFIG *lpcfgFlatThings, WORD z)
{
	WORD wZFactor = GetZFactor(lpcfgFlatThings, lpmap->things[iThing].thing);

	/* First, clear all the bits that specify the z ofsset. */
	lpmap->things[iThing].flag &= wZFactor - 1;
	
	/* Now, set the bits that specify it, leaving the lower bits intact. */
	lpmap->things[iThing].flag |= z * wZFactor;
}


/* GetThingZ
 *   For the specified thing, returns the z-offset.
 *
 * Parameters:
 *   MAP*		lpmap				Pointer to map data.
 *   int		iThing				Index of thing.
 *   CONFIG*	lpcfgFlatThings		Flat things section.
 *
 * Return value: WORD
 *   Z-offset.
 */
static __inline WORD GetThingZ(MAP *lpmap, int iThing, CONFIG *lpcfgFlatThings)
{
	WORD wZFactor = GetZFactor(lpcfgFlatThings, lpmap->things[iThing].thing);

	/* Get the z from the flag. */
	return lpmap->things[iThing].flag / wZFactor;
}


/* StitchVertices
 *   Makes all lines referencing one vertex point to another.
 *
 * Parameters:
 *   MAP*	lpmap			Pointer to map data.
 *   int	iSourceVertex	Index of old vertex.
 *   inr	iTargetVertex	Index of new vertex.
 *
 * Return value: None.
 *
 * Remarks:
 *   Call CorrectDraggedSectorReferences and then ClearDraggingFlags afterwards.
 *   This fits in handily with dragging.
 */
void StitchVertices(MAP *lpmap, int iSourceVertex, int iTargetVertex)
{
	int i;
	DYNAMICINTARRAY diarrayLinesToDelete; 
 	
	/* Create a dynamic array in which to store indices of linedefs to delete. 
	 * It's not safe to delete them until we've finished looping through them 
	 * all, since we can't guarantee that they'll be the highest-numbered ones
	 * yet to be considered (in which case we could use the loop-backwards
	 * trick).
	 */ 
	InitialiseDynamicIntArray(&diarrayLinesToDelete, DELLINES_INITBUFSIZE);

	/* Loop through all linedefs and change any references to the old vertex to
	 * point to the new one.
	 */
	for(i = 0; i < lpmap->iLinedefs; i++)
	{
		MAPLINEDEF *lpld = &lpmap->linedefs[i];
		int iExistingLinedef;

		/* TODO: If the deleting linedef is single-sided, adjust the remaining
		 * linedef accordingly: make it single-sided and flip it if necessary.
		 */

		if(lpld->v1 == iSourceVertex)
		{
			if(lpld->v2 != iTargetVertex)
			{
				if((iExistingLinedef = FindLinedefBetweenVertices(lpmap, iTargetVertex, lpld->v2)) >= 0)
				{
					AddToDynamicIntArray(&diarrayLinesToDelete, iExistingLinedef);
					lpld->editflags |= LEF_RECALCSECTOR;
				}

				lpld->v1 = iTargetVertex;
			}
			else AddToDynamicIntArray(&diarrayLinesToDelete, i);
		}
		else if(lpld->v2 == iSourceVertex)
		{
			if(lpld->v1 != iTargetVertex)
			{
				if((iExistingLinedef = FindLinedefBetweenVertices(lpmap, iTargetVertex, lpld->v1)) >= 0)
				{
					AddToDynamicIntArray(&diarrayLinesToDelete, iExistingLinedef);
					lpld->editflags |= LEF_RECALCSECTOR;
				}

				lpld->v2 = iTargetVertex;
			}
			else AddToDynamicIntArray(&diarrayLinesToDelete, i);
		}
	}

	/* If the old vertex was selected, so should the new one be. */
	if(lpmap->vertices[iSourceVertex].selected)
		lpmap->vertices[iTargetVertex].selected = 1;

	/* It's safe to delete the lines now. */
	SortDynamicIntArray(&diarrayLinesToDelete);
	for(i = (int)diarrayLinesToDelete.uiCount - 1; i >= 0; i--)
		DeleteLinedef(lpmap, diarrayLinesToDelete.lpiIndices[i]);

	FreeDynamicIntArray(&diarrayLinesToDelete);

	/* We can delete the old vertex now. */
	DeleteVertex(lpmap, iSourceVertex);
}


/* StitchDraggedVertices
 *   Auto-stitches all dragged vertices.
 *
 * Parameters:
 *   MAP*				lpmap				Pointer to map data.
 *   SELECTION_LIST*	lpsellistVertices	Indices of dragged vertices.
 *
 * Return value: None.
 */
void StitchDraggedVertices(MAP *lpmap, SELECTION_LIST *lpsellistVertices)
{
	int i;
	const int iStitchDistance = ConfigGetInteger(g_lpcfgMain, "autostitchdistance");

	/* Label all the moving linedefs. */
	for(i = 0; i < lpmap->iLinedefs; i++)
	{
		MAPLINEDEF *lpld = &lpmap->linedefs[i];
		if(ExistsInSelectionList(lpsellistVertices, lpld->v1) || ExistsInSelectionList(lpsellistVertices, lpld->v2))
			lpld->editflags |= LEF_LABELLED;
	}

	/* Loop through all vertices, stitching as necessary. We loop backwards so
	 * we can delete safely. We also label them for later use.
	 */
	for(i = lpsellistVertices->iDataCount - 1; i >= 0; i--)
	{
		const int iVertex = lpsellistVertices->lpiIndices[i];
		int iDistance, iLineDistance;
		MAPVERTEX *lpvx = &lpmap->vertices[iVertex];
		int iNearestVertex = NearestOtherVertex(lpmap, iVertex, &iDistance);
		int iNearestLinedef = NearestConditionedLinedef(lpvx->x, lpvx->y, lpmap->vertices, lpmap->linedefs, lpmap->iLinedefs, &iLineDistance, LinedefNotAttachedToVertex, (void*)iVertex);

		/* Used when we snap lines to them after this loop. */
		lpvx->editflags |= VEF_LABELLED;

		if(iNearestVertex >= 0 && iDistance <= iStitchDistance)
		{
			/* Stitch vertices. */
			lpmap->vertices[iNearestVertex].editflags |= VEF_LABELLED;
			StitchVertices(lpmap, iVertex, iNearestVertex);
		}
		else if(iNearestLinedef >= 0 && iLineDistance <= iStitchDistance)
		{
			/* Split line at vertex. Do it via a new vertex so that we avoid
			 * doubled lines.
			 */
			int iNewVertex = AddVertex(lpmap, lpvx->x, lpvx->y);
			lpmap->vertices[iNewVertex].editflags |= VEF_LABELLED;
			SplitLinedef(lpmap, iNearestLinedef, iNewVertex);
			StitchVertices(lpmap, iVertex, iNewVertex);
		}
	}


	/* Check all dragged lines and see if any of them are close enough to
	 * non-dragged vertices.
	 */
	for(i = 0; i < lpmap->iLinedefs; i++)
	{
		if(lpmap->linedefs[i].editflags & LEF_LABELLED)
		{
			int iDistance;
			int iNearestStaticVertex;

			while(iNearestStaticVertex = NearestConditionedVertexToLinedef(lpmap, i, &iDistance, VertexUnlabelled, NULL),
				iNearestStaticVertex >= 0 && iDistance <= iStitchDistance)
			{
				/* Split line at vertex. Do it via a new vertex so that we avoid
				 * doubled lines.
				 */
				MAPVERTEX *lpvx = &lpmap->vertices[iNearestStaticVertex];
				int iNewVertex = AddVertex(lpmap, lpvx->x, lpvx->y);
				lpmap->vertices[iNewVertex].editflags |= VEF_LABELLED;
				SplitLinedef(lpmap, i, iNewVertex);
				StitchVertices(lpmap, iNearestStaticVertex, iNewVertex);
			}
		}
	}

	/* Remove our labelling. */
	ClearLinedefsLabelFlag(lpmap);
	ClearVertexFlags(lpmap, VEF_LABELLED);
}


/* DeleteZeroLengthLinedefs
 *   Deletes zero-length linedefs, both those with both vertices the same and
 *   both vertices co-incident.
 *
 * Parameters:
 *   MAP*				lpmap				Pointer to map data.
 *
 * Return value: None.
 *
 * Remarks:
 *   Zero-length lines interfere with our loop algorithms. So we get rid of
 *   them. I *think* they're illegal; if they're not, I suppose this'll need to
 *   go.
 */
void DeleteZeroLengthLinedefs(MAP *lpmap)
{
	int i;

	for(i = lpmap->iLinedefs - 1; i >= 0; i--)
	{
		if(lpmap->linedefs[i].v1 == lpmap->linedefs[i].v2)
			DeleteLinedef(lpmap, i);
		else if(lpmap->vertices[lpmap->linedefs[i].v1].x == lpmap->vertices[lpmap->linedefs[i].v2].x &&
			lpmap->vertices[lpmap->linedefs[i].v1].y == lpmap->vertices[lpmap->linedefs[i].v2].y)
		{
			StitchVertices(lpmap, lpmap->linedefs[i].v1, lpmap->linedefs[i].v2);

			/* This upset linedef numbering, so make sure we're still in range.
			 */
			i = min(i, lpmap->iLinedefs - 1);
		}
	}
}


/* RequiredTextures
 *   Determines which textures are necessary for a linedef.
 *
 * Parameters:
 *   MAP*	lpmap		Pointer to map data.
 *   int	iLinedef	Index of linedef.
 *
 * Return value: BYTE
 *   Combination of flags specifying the necessary textures. See
 *   ENUM_TEX_REQ_FLAGS for these.
 *
 * Remarks:
 *   TODO: Special handling for sky.
 */
BYTE RequiredTextures(MAP *lpmap, int iLinedef)
{
	BYTE byRequirementFlags = 0;
	MAPLINEDEF *lpld = &lpmap->linedefs[iLinedef];
	MAPSECTOR *lpsecFront = NULL, *lpsecBack = NULL;

	if(lpld->s1 >= 0)
		lpsecFront = &lpmap->sectors[lpmap->sidedefs[lpld->s1].sector];
	if(lpld->s2 >= 0)
		lpsecBack = &lpmap->sectors[lpmap->sidedefs[lpld->s2].sector];

	if(lpsecFront && lpsecBack)
	{
		/* If the front sector's zero-height, we don't need any textures on the
		 * front sidedef.
		 */
		if(lpsecFront->hceiling != lpsecFront->hfloor)
		{
			if(lpsecFront->hceiling > lpsecBack->hceiling)
				byRequirementFlags = TRQ_FRONTUPPER;

			if(lpsecFront->hfloor < lpsecBack->hfloor)
				byRequirementFlags |= TRQ_FRONTLOWER;
		}

		/* Do the same on the back as we did on the front. */
		if(lpsecBack->hceiling != lpsecBack->hfloor)
		{
			if(lpsecBack->hceiling > lpsecFront->hceiling)
				byRequirementFlags = TRQ_BACKUPPER;

			if(lpsecBack->hfloor < lpsecFront->hfloor)
				byRequirementFlags |= TRQ_BACKLOWER;
		}
	}
	else if(lpsecFront)
	{
		/* Single-sided on the front. */
		if(lpsecFront->hfloor != lpsecFront->hceiling)
			byRequirementFlags = TRQ_FRONTMIDDLE;
	}
	else if(lpsecBack)
	{
		/* Single-sided on the back... Erm... */
		if(lpsecBack->hfloor != lpsecBack->hceiling)
			byRequirementFlags = TRQ_BACKMIDDLE;
	}

	return byRequirementFlags;
}


/* ApplyRelativeCeilingHeight, ApplyRelativeFloorHeight
 *   Offsets ceiling or floor height of a sector.
 *
 * Parameters:
 *   MAP*	lpmap		Pointer to map data.
 *   int	iSector		Index of sector.
 *   int	iDelta		Value to add to floor/ceiling.
 *
 * Return value: None.
 *
 * Remarks:
 *   If the change takes the height out of range, it is capped at the maximum or
 *   minimum value: wraparound is prevented.
 */
static __inline void ApplyRelativeCeilingHeight(MAP *lpmap, int iSector, int iDelta)
{
	MAPSECTOR *lpsec = &lpmap->sectors[iSector];

	lpsec->hceiling = max(-32768, min(32767, lpsec->hceiling + iDelta));
}

static __inline void ApplyRelativeFloorHeight(MAP *lpmap, int iSector, int iDelta)
{
	MAPSECTOR *lpsec = &lpmap->sectors[iSector];

	lpsec->hfloor = max(-32768, min(32767, lpsec->hfloor + iDelta));
}


/* ApplyRelativeBrightness
 *   Offsets brightness of a sector.
 *
 * Parameters:
 *   MAP*	lpmap		Pointer to map data.
 *   int	iSector		Index of sector.
 *   short	nDelta		Value to add to brightness.
 *
 * Return value: None.
 *
 * Remarks:
 *   If the change takes the brightness out of range, it is capped at the
 *   maximum or minimum value: wraparound is prevented.
 */
static __inline void ApplyRelativeBrightness(MAP *lpmap, int iSector, short nDelta)
{
	MAPSECTOR *lpsec = &lpmap->sectors[iSector];

	lpsec->brightness = max(0, min(255, lpsec->brightness + nDelta));
}


/* ApplyRelativeThingZ
 *   Offsets z-offset of a thing.
 *
 * Parameters:
 *   MAP*		lpmap				Pointer to map data.
 *   int		iThing				Index of thing.
 *   int		iDelta				Value to add to z-offset.
 *   CONFIG*	lpcfgFlatThings		Flat thing config subsection.
 *
 * Return value: None.
 *
 * Remarks:
 *   If the change takes the height out of range, it is capped at the maximum or
 *   minimum value: no wraparound occurs.
 */
static __inline void ApplyRelativeThingZ(MAP *lpmap, int iThing, int iDelta, CONFIG *lpcfgFlatThings)
{
	/* When setting the z-offset, it will be further capped to the valid values
	 * for the particular type of thing.
	 */
	SetThingZ(lpmap, iThing, lpcfgFlatThings, (WORD)max(0, min(65535, GetThingZ(lpmap, iThing, lpcfgFlatThings) + iDelta)));
}


/* ApplyRelativeAngle
 *   Offsets angle of a thing.
 *
 * Parameters:
 *   MAP*		lpmap				Pointer to map data.
 *   int		iThing				Index of thing.
 *   int		iDelta				Value to add to angle.
 *
 * Return value: None.
 *
 * Remarks:
 *   If the change takes the angle out of range, it is capped at the maximum or
 *   minimum value: no wraparound occurs.
 */
static __inline void ApplyRelativeAngle(MAP *lpmap, int iThing, int iDelta)
{
	MAPTHING *lpthing = &lpmap->things[iThing];
	lpthing->angle = max(-32768, min(32767, lpthing->angle + iDelta));
}


/* ApplyRelativeThingX, ApplyRelativeThingY
 *   Offsets co-ordinates of a thing.
 *
 * Parameters:
 *   MAP*		lpmap				Pointer to map data.
 *   int		iThing				Index of thing.
 *   int		iDelta				Value to add to co-ordinate.
 *
 * Return value: None.
 *
 * Remarks:
 *   If the change takes the co-ordinate out of range, it is capped at the
 *   maximum or minimum value: no wraparound occurs.
 */
static __inline void ApplyRelativeThingX(MAP *lpmap, int iThing, int iDelta)
{
	MAPTHING *lpthing = &lpmap->things[iThing];
	lpthing->x = max(-32768, min(32767, lpthing->x + iDelta));
}

static __inline void ApplyRelativeThingY(MAP *lpmap, int iThing, int iDelta)
{
	MAPTHING *lpthing = &lpmap->things[iThing];
	lpthing->y = max(-32768, min(32767, lpthing->y + iDelta));
}


/* ApplyRelativeSidedefX, ApplyRelativeSidedefY
 *   Offsets offsets (!) of a sidedef.
 *
 * Parameters:
 *   MAP*		lpmap				Pointer to map data.
 *   int		iSidedef			Index of sidedef.
 *   int		iDelta				Value to add to offset.
 *
 * Return value: None.
 *
 * Remarks:
 *   If the change takes the co-ordinate out of range, it is capped at the
 *   maximum or minimum value: no wraparound occurs.
 */
static __inline void ApplyRelativeSidedefX(MAP *lpmap, int iSidedef, int iDelta)
{
	MAPSIDEDEF *lpsd = &lpmap->sidedefs[iSidedef];
	lpsd->tx = max(-32768, min(32767, lpsd->tx + iDelta));
}

static __inline void ApplyRelativeSidedefY(MAP *lpmap, int iSidedef, int iDelta)
{
	MAPSIDEDEF *lpsd = &lpmap->sidedefs[iSidedef];
	lpsd->ty = max(-32768, min(32767, lpsd->ty + iDelta));
}


/* ApplyRelative*Selection
 *   Offsets a property of selected objects by a given value.
 *
 * Parameters:
 *   MAP*				lpmap			Map.
 *   SELECTION_LIST*	lpsellist...	Selection
 *   int/short			iDelta/nDelta	Offset.
 *
 * Return value: None.
 */
void ApplyRelativeCeilingHeightSelection(MAP *lpmap, SELECTION_LIST *lpsellistSectors, int iDelta)
{
	int i;
	for(i = 0; i < lpsellistSectors->iDataCount; i++)
		ApplyRelativeCeilingHeight(lpmap, lpsellistSectors->lpiIndices[i], iDelta);
}

void ApplyRelativeFloorHeightSelection(MAP *lpmap, SELECTION_LIST *lpsellistSectors, int iDelta)
{
	int i;
	for(i = 0; i < lpsellistSectors->iDataCount; i++)
		ApplyRelativeFloorHeight(lpmap, lpsellistSectors->lpiIndices[i], iDelta);
}

void ApplyRelativeBrightnessSelection(MAP *lpmap, SELECTION_LIST *lpsellistSectors, short nDelta)
{
	int i;
	for(i = 0; i < lpsellistSectors->iDataCount; i++)
		ApplyRelativeBrightness(lpmap, lpsellistSectors->lpiIndices[i], nDelta);
}

void ApplyRelativeThingZSelection(MAP *lpmap, SELECTION_LIST *lpsellistThings, CONFIG *lpcfgFlatThings, int iDelta)
{
	int i;

	for(i = 0; i < lpsellistThings->iDataCount; i++)
		ApplyRelativeThingZ(lpmap, lpsellistThings->lpiIndices[i], iDelta, lpcfgFlatThings);
}

void ApplyRelativeAngleSelection(MAP *lpmap, SELECTION_LIST *lpsellistThings, int iDelta)
{
	int i;

	for(i = 0; i < lpsellistThings->iDataCount; i++)
		ApplyRelativeAngle(lpmap, lpsellistThings->lpiIndices[i], iDelta);
}

void ApplyRelativeThingXSelection(MAP *lpmap, SELECTION_LIST *lpsellistThings, int iDelta)
{
	int i;

	for(i = 0; i < lpsellistThings->iDataCount; i++)
		ApplyRelativeThingX(lpmap, lpsellistThings->lpiIndices[i], iDelta);
}

void ApplyRelativeThingYSelection(MAP *lpmap, SELECTION_LIST *lpsellistThings, int iDelta)
{
	int i;

	for(i = 0; i < lpsellistThings->iDataCount; i++)
		ApplyRelativeThingY(lpmap, lpsellistThings->lpiIndices[i], iDelta);
}

void ApplyRelativeFrontSidedefXSelection(MAP *lpmap, SELECTION_LIST *lpsellistLines, int iDelta)
{
	int i;

	for(i = 0; i < lpsellistLines->iDataCount; i++)
	{
		int iSidedef = lpmap->linedefs[lpsellistLines->lpiIndices[i]].s1;
		if(iSidedef >= 0)
			ApplyRelativeSidedefX(lpmap, iSidedef, iDelta);
	}
}

void ApplyRelativeFrontSidedefYSelection(MAP *lpmap, SELECTION_LIST *lpsellistLines, int iDelta)
{
	int i;

	for(i = 0; i < lpsellistLines->iDataCount; i++)
	{
		int iSidedef = lpmap->linedefs[lpsellistLines->lpiIndices[i]].s1;
		if(iSidedef >= 0)
			ApplyRelativeSidedefY(lpmap, iSidedef, iDelta);
	}
}

void ApplyRelativeBackSidedefXSelection(MAP *lpmap, SELECTION_LIST *lpsellistLines, int iDelta)
{
	int i;

	for(i = 0; i < lpsellistLines->iDataCount; i++)
	{
		int iSidedef = lpmap->linedefs[lpsellistLines->lpiIndices[i]].s2;
		if(iSidedef >= 0)
			ApplyRelativeSidedefX(lpmap, iSidedef, iDelta);
	}
}

void ApplyRelativeBackSidedefYSelection(MAP *lpmap, SELECTION_LIST *lpsellistLines, int iDelta)
{
	int i;

	for(i = 0; i < lpsellistLines->iDataCount; i++)
	{
		int iSidedef = lpmap->linedefs[lpsellistLines->lpiIndices[i]].s2;
		if(iSidedef >= 0)
			ApplyRelativeSidedefY(lpmap, iSidedef, iDelta);
	}
}


/* GradientSelected*
 *   Gradients a property of the selected sectors.
 *
 * Parameters:
 *   MAP*				lpmap				Pointer to map data.
 *   SELECTION_LIST*	lpsellistSectors	Selection
 *
 * Return value: None.
 */
void GradientSelectedCeilings(MAP *lpmap, SELECTION_LIST *lpsellistSectors)
{
	int i;
	int iDiff, iBase;

	/* Sanity check. */
	if(lpsellistSectors->iDataCount < 2) return;

	iBase = lpmap->sectors[lpsellistSectors->lpiIndices[0]].hceiling;
	iDiff = lpmap->sectors[lpsellistSectors->lpiIndices[lpsellistSectors->iDataCount - 1]].hceiling - iBase;

	for(i = 1; i < lpsellistSectors->iDataCount - 1; i++)
		lpmap->sectors[lpsellistSectors->lpiIndices[i]].hceiling = iBase + (iDiff * i) / (lpsellistSectors->iDataCount - 1);
}

void GradientSelectedFloors(MAP *lpmap, SELECTION_LIST *lpsellistSectors)
{
	int i;
	int iDiff, iBase;

	/* Sanity check. */
	if(lpsellistSectors->iDataCount < 2) return;

	iBase = lpmap->sectors[lpsellistSectors->lpiIndices[0]].hfloor;
	iDiff = lpmap->sectors[lpsellistSectors->lpiIndices[lpsellistSectors->iDataCount - 1]].hfloor - iBase;

	for(i = 1; i < lpsellistSectors->iDataCount - 1; i++)
		lpmap->sectors[lpsellistSectors->lpiIndices[i]].hfloor = iBase + (iDiff * i) / (lpsellistSectors->iDataCount - 1);
}

void GradientSelectedBrightnesses(MAP *lpmap, SELECTION_LIST *lpsellistSectors)
{
	int i;
	int iDiff, iBase;

	/* Sanity check. */
	if(lpsellistSectors->iDataCount < 2) return;

	iBase = lpmap->sectors[lpsellistSectors->lpiIndices[0]].brightness;
	iDiff = lpmap->sectors[lpsellistSectors->lpiIndices[lpsellistSectors->iDataCount - 1]].brightness - iBase;

	for(i = 1; i < lpsellistSectors->iDataCount - 1; i++)
		lpmap->sectors[lpsellistSectors->lpiIndices[i]].brightness = iBase + (iDiff * i) / (lpsellistSectors->iDataCount - 1);
}
