#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <windows.h>

#include "Wad.h"
#include "Util.h"


#if (defined UNICODE) || (defined MBCS)
#error "This library doesn't support multi-byte character sets."
#endif


/* Must be at least four bytes. That'd be ludicrously small, of course. */
#define LUMPBUFFERSIZE 65536


/* Static function prototypes. */
static int ReadWAD(WAD *wad, const char *filename);
static void ClearWAD(WAD *wad);
static LUMP *FindLump(WAD *wad, const char *lumpname, const char *section);
static LUMP *FindPrecedingLump(WAD *wad, const char *lumpname, const char *section);


/* NB: In any function that takes a section as an argument, NULL means "search
 *     from the beginning".
 */






/* CreateWAD
 *  Allocates a new WAD structure.
 *
 * Parameters:
 *  void
 *
 * Return value: WAD*
 *  Pointer to the new structure. NULL on error.
 *
 * Remarks:
 *  If you want to load a WAD from disc, use OpenWAD instead.
 */

WAD * CreateWAD(void)
{
	WAD *wad = (WAD*)malloc(sizeof(WAD));

	if(!wad) return NULL;

	/* No file associated with this structure. */
	wad->file = NULL;
	wad->filename = NULL;

	/* This means that if the header is ever the preceding lump, lumps to be
	 * added after the header can correctly be added at the beginning.
	 */
	*wad->lumphdr.name = '\0';

	/* No lumps yet. */
	wad->lumphdr.next = NULL;
	
	return wad;
}



/* FreeWAD
 *  Frees a WAD structure.
 *
 * Parameters:
 *  WAD *wad		Pointer to WAD structure to free.
 *
 * Return value: void
 *
 * Remarks:
 *  Frees the lot: lumps, lump metadata, base directory...
 *  Closes files, too.
 */

void FreeWAD(WAD *wad)
{
	ClearWAD(wad);

	free(wad);
}



/* OpenWAD
 *  Opens a WAD from disc.
 *
 * Parameters:
 *  const char *filename			Name of file to open.
 *
 * Return value: WAD*
 *  Pointer to the new structure. NULL on error.
 *
 * Remarks:
 *  Keeps the file open, because the whole point of writing a fresh library is
 *  to avoid loading the entire file into memory.
 */

WAD* OpenWAD(const char *filename)
{
	WAD *wad = CreateWAD();

	/* Out of memory. */
	if(!wad) return NULL;

	if(ReadWAD(wad, filename))
		return wad;
	else
	{
		FreeWAD(wad);
		return NULL;
	}
}




/* WriteWAD
 *  Writes a WAD to disc.
 *
 * Parameters:
 *  WAD *wad				Pointer to WAD structure to write.
 *  const char *filename	Filename. NULL to use existing name.
 *
 * Return value: int
 *  Nonzero on success; FALSE on error.
 *
 * Remarks:
 *  The creation date isn't preserved. Ho-hum.
 */

int WriteWAD(WAD *wad, const char *filename)
{
	int totlumps=0;
	LUMP *lump;
	FILE *newfile;
	char tempfilename[_MAX_PATH+1], tempoldfilename[_MAX_PATH+1];
	unsigned char *buffer;
	int writesucceeded;
	int curpos;
	int bytesremaining;
	char *newfilename;

	if(!wad) return FALSE;

	/* Prepare the filename. We don't use it yet, but we might as well check it
	 * now.
	 */
	if(!filename) filename = wad->filename;
	if(!filename) return FALSE;

	/* Count the lumps. */
	lump = &wad->lumphdr;
	while((lump = lump->next)) totlumps++;


	/* We open a temporary file for writing, since we want to read from
	 * the source file.
	 */


	GetTempFileName(".", "wad", 0, tempfilename);
	newfile = fopen(tempfilename, "wb");
	buffer = malloc(LUMPBUFFERSIZE);

	if(!newfile || !buffer)
	{
		if(buffer) free(buffer);

		return FALSE;
	}

	/* TODO: Error-checking on these fwrites. */

	/* Write the header. */
	if(wad->file)
	{
		fseek(wad->file, 0, SEEK_SET);
		fread(buffer, 4, 1, wad->file);		/* PWAD/IWAD */
		fwrite(buffer, 4, 1, newfile);
	}
	else
	{
		/* Assume PWAD if new file. */
		fwrite("PWAD", 4, 1, newfile);
	}

	fwrite(&totlumps, 4, 1, newfile);	/* Number of lumps. */
	*((int*)buffer) = 12;				/* Directory offset. */
	fwrite(buffer, 4, 1, newfile);

	/* Write the directory. */
	lump = wad->lumphdr.next;
	curpos = 12 + (totlumps << 4);		/* Header + directory. */
	while(lump)
	{
		writesucceeded = (fwrite(&curpos,     4, 1, newfile) == 1);
		writesucceeded = (fwrite(&lump->length, 4, 1, newfile) == 1) && writesucceeded;
		writesucceeded = (fwrite(lump->name,    8, 1, newfile) == 1) && writesucceeded;

		if(!writesucceeded)
		{
			fclose(newfile);
			remove(tempfilename);
			free(buffer);

			return FALSE;
		}

		curpos += lump->length;
		lump = lump->next;
	}

	/* Begin writing the lumps. */
	lump = wad->lumphdr.next;
	while(lump)
	{
		switch(lump->lumptype)
		{
		case LUMP_NEW:
			/* Easy. Just write the data from memory, or if it's zero-length, do
			 * nothing at all.
			 */
			writesucceeded = (lump->length == 0) || (fwrite(lump->lumpdata, lump->length, 1, newfile) == 1);
			break;

		case LUMP_FROMFILE:
			/* Read lump data from the source in LUMPBUFFERSIZE chunks,
			 * writing each to the file as we go.
			 */
			
			bytesremaining = lump->length;
			writesucceeded = TRUE;
			fseek(wad->file, lump->srcoffset, SEEK_SET);
			while(bytesremaining > 0 && writesucceeded)
			{
				writesucceeded =
					(fread(buffer, min(bytesremaining, LUMPBUFFERSIZE), 1, wad->file) == 1) &&
					(fwrite(buffer, min(bytesremaining, LUMPBUFFERSIZE), 1, newfile) == 1);
				
				bytesremaining -= LUMPBUFFERSIZE;
			}
			break;

		default:	/* Oi! */
			writesucceeded = FALSE;
		}

		if(!writesucceeded)
		{
			fclose(newfile);
			remove(tempfilename);

			free(buffer);

			return FALSE;
		}

		lump = lump->next;
	}


	/* Okay, we're finished. Close the file and rename it. */

	fclose(newfile);
	
	if(wad->file)
	{
		fclose(wad->file);

		/* If we're saving on top of the original... */
		if(strncmp(filename, wad->filename, _MAX_PATH) == 0)
		{
			/* Rename the old file for the moment, rather than deleting it, in case
			 * something goes awry.
			 */
			GetTempFileName(".", "wad", 0, tempoldfilename);
			remove(tempoldfilename);		/* I didn't actually want it created... */
			if(rename(wad->filename, tempoldfilename))
			{
				wad->file = fopen(wad->filename, "rb");
				remove(tempfilename);

				free(buffer);

				return FALSE;
			}
		}
	}

	remove(filename);	/* Delete if already exists. */
	if(rename(tempfilename, filename))
	{
		if(wad->filename) wad->file = fopen(wad->filename, "rb");
		remove(tempfilename);

		free(buffer);

		return FALSE;
	}


	/* I don't care if this fails. At least, not badly enough to write *another*
	 * cleanup clause...
	 */
	if(wad->filename && strncmp(filename, wad->filename, _MAX_PATH) == 0)
		remove(tempoldfilename);


	/* Set fields in WAD structure to refer to new file. Errors here are handled
	 * in the final return statement.
	 */
	newfilename = strdup(filename);
	if(wad->filename) free(wad->filename);
	wad->filename = newfilename;

	if(wad->filename) wad->file = fopen(wad->filename, "rb");


	/* Free memory. */
	free(buffer);

	return wad->filename && wad->file;
}





/* GetLump
 *  Retrieves data for a lump.
 *
 * Parameters:
 *  WAD *wad				Pointer to WAD structure to use.
 *  const char *lumpname	Name of lump to retrieve.
 *  const char *section		Name of section containing lump.
 *  unsigned char *buffer	Buffer in which to store data.
 *  int maxlen				Length of buffer.
 *
 * Return value: int
 *  Length of data copied on success; negative on error.
 *
 * Remarks:
 *  If the buffer's too small, the contents of the the lump
 *  will be copied as far as the buffer will allow.
 *  This is a change from the old behaviour, to allow peeking
 *  at the first few bytes to guess the type.
 */

int GetLump(WAD *wad, const char *lumpname, const char *section, unsigned char *buffer, int maxlen)
{
	LUMP *lump;
	int length;
	
	if(!wad || !lumpname || !buffer) return -1;

	/* Find lump in WAD. */
	lump = FindLump(wad, lumpname, section);
	if(!lump) return -2;

	/* Lump too big? */
	if(lump->length > maxlen) length = maxlen;
	else length = lump->length;

	/* Copy lump data into buffer. */
	switch(lump->lumptype)
	{
	case LUMP_NEW:
		/* Just copy the data. */
		memcpy(buffer, lump->lumpdata, length);
		break;

	case LUMP_FROMFILE:
		/* Seek to the correct position, and then read the data. */
		fseek(wad->file, lump->srcoffset, SEEK_SET);
		if(fread(buffer, length, 1, wad->file) != 1) return -4;
	}

	return length;
}







/* GetLumpLength
 *  Retrieves the length of a lump.
 *
 * Parameters:
 *  WAD *wad				Pointer to WAD structure to use.
 *  const char *lumpname	Name of lump to retrieve.
 *  const char *section		Name of section containing lump.
 *
 * Return value: int
 *  Lump length on success; negative on error.
 *
 * Remarks:
 *  Useful for preparing storage for GetLump.
 */

int GetLumpLength(WAD *wad, const char *lumpname, const char *section)
{
	LUMP *lump;

	if(!wad || !lumpname) return -1;

	lump = FindLump(wad, lumpname, section);
	if(lump)
		return lump->length;
	else return -2;			/* Not found. */
}




/* DeleteLump
 *  Deletes a lump.
 *
 * Parameters:
 *  WAD *wad				Pointer to WAD structure to use.
 *  const char *lumpname	Name of lump to retrieve.
 *  const char *section		Name of section containing lump.
 *
 * Return value: int
 *  TRUE on success; FALSE on error.
 */

int DeleteLump(WAD *wad, const char *lumpname, const char *section)
{
	LUMP *lump;
	LUMP *lumpfollowing;

	if(!wad || !lumpname) return FALSE;

	/* Find the lump preceding the one to delete. */
	lump = FindPrecedingLump(wad, lumpname, section);
	if(!lump) return FALSE;

	/* Free the lump's data if necessary; free the node; close the hole. */
	if(lump->next->lumptype == LUMP_NEW) free(lump->next->lumpdata);
	lumpfollowing = lump->next->next;
	free(lump->next);
	lump->next = lumpfollowing;

	return TRUE;
}






/* EnumLumpNames
 *  Calls a function for each lump name, in order.
 *
 * Parameters:
 *  WAD *wad								Pointer to WAD structure to use.
 *  void (CALLBACK *f)(const char*, void*)	Function to call.
 *  void *lpv								Parameter to callback.
 *  char *startsec, *endsec					Section delimeters. May be NULL.
 *
 * Return value: int
 *  Total number of lumps, or negative on error.
 */

int EnumLumpNamesBySection(WAD *wad, void (CALLBACK *f)(const char*, void*), void *lpv, char *startsec, char *endsec)
{
	LUMP *lump;
	int totlumps=0;
	char name[9];
	int passedstart = !startsec;


	if(!wad || !f) return -1;

	/* f expects ASCIIZ strings. */
	name[8] = '\0';



	/* Enumerate! */
	lump = wad->lumphdr.next;
	while(lump)
	{
		memcpy(name, lump->name, 8);

		/* Not reached the start of the section? */
		if(passedstart)
		{
			/* Reached the end of the section? */
			if(endsec && strncmp(lump->name, endsec, 8) == 0) break;

			f(name, lpv);
			totlumps++;
		}
		else if(strncmp(lump->name, startsec, 8) == 0) passedstart = TRUE;
		
		lump = lump->next;
	}

	return totlumps;
}






/* SetLump
 *  Sets a lump's data, creating it if necessary.
 *
 * Parameters:
 *  WAD *wad					Pointer to WAD structure to use.
 *  const char *lumpname		Name of lump.
 *  const char *section			Name of section.
 *  const unsigned char *data	Data for lump.
 *  int length					Length of data.
 *
 * Return value: int
 *  Nonzero on success; zero on error.
 *
 * Remarks:
 *  Empty lumps are allowed. If the lump doesn't exist, it's
 *  created at the start of the section; call CreateLump first
 *  if this isn't what you want (and it probably isn't...).
 */

int SetLump(WAD *wad, const char *lumpname, const char *section, const unsigned char *data, int length)
{
	LUMP *lump, *prevlump;
	unsigned char *buffer;

	if(!wad || !lumpname || (length > 0 && !data) || length < 0) return FALSE;

	/* Check whether it already exists. */
	lump = FindLump(wad, lumpname, section);
	if(lump)
	{
		/* How we do this depends on whether the lump is old or new. */
		switch(lump->lumptype)
		{
		case LUMP_NEW:
			/* Just alter the fields. */

			if(length > 0)
			{
				buffer = (unsigned char*)malloc(length);
				if(!buffer) return FALSE;
				memcpy(buffer, data, length);
			}
			else buffer = NULL;

			if(lump->lumpdata) free(lump->lumpdata);
			lump->lumpdata = buffer;

			lump->length = length;

			break;

		case LUMP_FROMFILE:
			{
				char *prevname;

				prevlump = FindPrecedingLump(wad, lumpname, section);
				DeleteLump(wad, lumpname, section);

				/* If the preceding lump was the header node or the section
				 * header, we need to add at the beginning of the section (which
				 * may be the whole wad). We set prevname to NULL in that case.
				 */

				if((!section && prevlump->name[0]) || strcmp(section, prevlump->name) == 0)
					prevname = prevlump->name;
				else prevname = NULL;

				/* If this fails, the original data's gone. Oh well. */
				if(!CreateLump(wad, lumpname, section, prevname, FALSE)) return FALSE;

				return SetLump(wad, lumpname, section, data, length);
			}
		}
	}
	else
	{
		if(!CreateLump(wad, lumpname, section, NULL, FALSE)) return FALSE;
		return SetLump(wad, lumpname, section, data, length);
	}

	return TRUE;
}




/* CreateLump
 *  Creates a lump.
 *
 * Parameters:
 *  WAD *wad				Pointer to WAD structure to use.
 *  const char *lumpname	Name of lump.
 *  const char *section		Name of section in which to create lump.
 *  const char *addafter	Lump to add after.
 *  char addatend			Non-zero if lump should be created at end; ignored
 *							if either section or addafter != NULL.
 *
 * Return value: int
 *  Nonzero on success; zero on error.
 *
 * Remarks:
 *  The lump will be empty. Set addafter to NULL to add at the
 *  beginning.
 */

int CreateLump(WAD *wad, const char *lumpname, const char *section, const char *addafter, char addatend)
{
	LUMP *lumpprev;
	LUMP *lump;

	if(!wad || !lumpname) return FALSE;

	if(addafter)
	{
		lumpprev = FindLump(wad, addafter, section);
		if(!lumpprev) return FALSE;
	}
	else if(section)
	{
		lumpprev = FindLump(wad, section, NULL);
		if(!lumpprev) return FALSE;
	}
	else
	{
		lumpprev = &wad->lumphdr;
		if(addatend)
			while(lumpprev->next) lumpprev = lumpprev->next;
	}

	/* Create the new lump where it belongs. */
	lump = (LUMP*)malloc(sizeof(LUMP));
	if(!lump) return FALSE;
	lump->next = lumpprev->next;
	lumpprev->next = lump;

	lump->lumptype = LUMP_NEW;
	lump->lumpdata = NULL;
	lump->length = 0;
	memset(lump->name, 0, 8);
	strncpy(lump->name, lumpname, 8);

	return TRUE;
}



/* WadIsNew
 * Determines whether a wad is new (i.e. created at runtime, not loaded from
 * disc).
 *
 * Parameters:
 *  WAD *wad				Pointer to WAD structure.
 *
 * Return value: BOOL
 *  TRUE if new; FALSE if not.
 */
BOOL WadIsNew(WAD *lpwad)
{
	return !lpwad->filename;
}


/* ReadWAD
 * Reads a WAD from disc into an existing structure.
 *
 * Parameters:
 *  WAD *wad				Pointer to WAD structure to read into.
 *  const char *filename	Name of file to open.
 *
 * Return value: int
 *  TRUE on success; FALSE on error.
 *
 * Remarks:
 *  Doesn't free anything first. Use CleanWAD.
 *  Doesn't clean up if it fails, either. OpenWAD does that.
 *
 * See also:
 *  OpenWAD
 */
static int ReadWAD(WAD *wad, const char *filename)
{
	int magicnum, diroffset, numbaselumps, i;
	WADDIR *wd;
	LUMP *lump;

	wad->file = fopen(filename, "rb");
	if(!wad->file)
		return FALSE;

	wad->filename = (char*)malloc((strlen(filename)+1) * sizeof(char));
	if(!wad->filename)
		return FALSE;
	strcpy(wad->filename, filename);
	
	/* Is the magic number correct? */
	fread(&magicnum, 4, 1, wad->file);
	if(magicnum != PWAD && magicnum != IWAD)
		return FALSE;

	numbaselumps = -1;		/* In case fread fails. */
	fread(&numbaselumps, 4, 1, wad->file);
	if(numbaselumps > 0)
		wd = (WADDIR*)malloc(sizeof(WADDIR) * numbaselumps);
	else
		wd = NULL;

	/* Either malloc failed or the lump-count was negative. */
	if(!wd && numbaselumps != 0)
		return FALSE;

	if(!fread(&diroffset, 4, 1, wad->file))
		return FALSE;

	fseek(wad->file, diroffset, SEEK_SET);

	if(numbaselumps != 0)
		if(!fread(wd, sizeof(WADDIR) * numbaselumps, 1, wad->file))
			return FALSE;

	
	/* Build the lump list. */
	lump = &wad->lumphdr;
	for(i = 0; i < numbaselumps; i++)
	{
		lump->next = (LUMP*)malloc(sizeof(LUMP));
		if(!lump->next)
			return -1;

		lump = lump->next;
		lump->lumptype = LUMP_FROMFILE;
		
		memcpy(lump->name, wd[i].name, 8);

		lump->length = wd[i].length;
		lump->srcoffset = wd[i].offset;
	}
	lump->next = NULL;

	if(wd) free(wd);

	return TRUE;
}




/* ClearWAD
 *  Clears a WAD structure, freeing all the data.
 *
 * Parameters:
 *  WAD *wad		Pointer to WAD structure to clear.
 *
 * Return value: void
 *
 * Remarks:
 *  Results in the structure being left in the state it was in when it was
 *  created by CreateWAD.
 *
 * See Also:
 *  FreeWAD
 */

static void ClearWAD(WAD *wad)
{
	LUMP *lump, *lumpprev;

	if(wad->file) fclose(wad->file);
	if(wad->filename) free(wad->filename);

	wad->file = NULL;
	wad->filename = NULL;
	
	/* Free all the lumps. */
	lump = wad->lumphdr.next;
	while(lump)
	{
		if(lump->lumptype == LUMP_NEW && lump->lumpdata) free(lump->lumpdata);
		lumpprev = lump;
		lump = lump->next;
		free(lumpprev);
	}

	wad->lumphdr.next = NULL;
}



/* FindLump
 *  Finds the list node for a lump.
 *
 * Parameters:
 *  WAD *wad				Pointer to WAD structure to clear.
 *  const char *lumpname	Name of lump to find.
 *  const char *section		Name of lump to begin searching from.
 *
 * Return value: LUMP*
 *  Pointer to lump node. NULL if not found.
 *
 * See Also:
 *  FindPrecedingLump
 */
static LUMP *FindLump(WAD *wad, const char *lumpname, const char *section)
{
	/* Let FPL do the work for us. Saves duplicating code. */
	LUMP *lump = FindPrecedingLump(wad, lumpname, section);
	if(lump) return lump->next;
	else return NULL;
}





/* FindPrecedingLump
 *  Finds the list node for a lump preceding that specified.
 *
 * Parameters:
 *  WAD *wad				Pointer to WAD structure to clear.
 *  const char *lumpname	Name of lump whose predecessor we're interested in.
 *  const char *section		Name of lump to begin searching from.
 *
 * Return value: LUMP*
 *  Pointer to preceding lump node; NULL if not found.
 *
 * See Also:
 *  FindLump
 */
static LUMP *FindPrecedingLump(WAD *wad, const char *lumpname, const char *section)
{
	LUMP *lump;

	if(section)
	{
		lump = FindLump(wad, section, NULL);
		if(!lump) return NULL;
	}
	else lump = &wad->lumphdr;

	while(lump->next && strncmp(lump->next->name, lumpname, 8) != 0) lump = lump->next;

	if(lump->next) return lump;
	else return NULL;
}
