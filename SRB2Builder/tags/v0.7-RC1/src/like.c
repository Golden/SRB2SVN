/* SRB2 Workbench -- A map editor for Sonic Robo Blast 2.
 * Copyright (C) 2009 Gregor Dick
 * http://workbench.srb2.org/
 *
 * Incorporates portions of Doom Builder, (C) 2003 Pascal vd Heiden.
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 * USA.
 *
 * like.c: Implements some of the semantics of Visual Basic 6's "Like" operator.
 *
 * AMDG.
 */

#include <windows.h>
#include "like.h"

BOOL LikeA(LPCSTR sz, LPCSTR szPattern)
{
	/* Loop through the pattern string. */
	while(*szPattern)
	{
		switch(*szPattern)
		{
		case '?':
			/* Any character, but must exist. */
			if(!(*sz++)) return FALSE;
			break;

		case '#':
			/* Must be a digit. */
			if(*sz < '0' || *sz > '9') return FALSE;
			sz++;
			break;

		case '[':
			{
				BOOL bNegate = FALSE;

				/* Class group. */
				szPattern++;		/* Skip opening bracket. */
				if(*szPattern == '!')	/* Negation? */
				{
					bNegate = TRUE;
					szPattern++;
				}

				while(*szPattern && *szPattern != ']')	/* Loop through class. */
				{
					if(szPattern[1] == '-')
					{
						if(!szPattern[2]) return FALSE;		/* Malformed. */
						if(*sz >= *szPattern && *sz <= szPattern[2])
						{
							/* Match in class. */
							if(bNegate) return FALSE;
							break;
						}

						/* Skip past part of char interval that's left after one
						 * advance.
						 */
						szPattern += 2;
					}
					/* Simple char. */
					else if(*szPattern == *sz)
					{
						if(bNegate) return FALSE;
						break;
					}

					/* Skip to next part of class. */
					szPattern++;
				}

				/* Did we get through the class and not find a match? */
				if(!bNegate && (!(*szPattern) || *szPattern == ']'))
					return FALSE;

				sz++;		/* Advance test string. */

				/* Read to end of class in pattern. */
				while(*szPattern && *szPattern != ']') szPattern++;
				if(!(*szPattern)) return FALSE;	/* Malformed. */
			}

			break;

		case '*':
			/* Firstly, get rid of all consecutive *s at this point. We're
			 * testing the tail of the pattern against all possible tails of the
			 * test string.
			 */
			while(*szPattern == '*') szPattern++;

			/* Test each tail until we find a match. */
			while(*sz && !LikeA(sz, szPattern)) sz++;

			/* We ALWAYS RETURN now! The recursive call took us to the end of
			 * the string. This return value is slightly sneaky. If the equality
			 * fails, *sz must be \0, since LikeA would've been tested and
			 * failed otherwise, causing another loop iteration. This indicates
			 * that we reached the end without finding a suitable tail. If,
			 * however, they're *both* \0, then we matched right to the end of
			 * the string with our *, and so we succeed.
			 */
			return *sz == *szPattern;

		default:
			/* Simple character. */
			if(*sz++ != *szPattern) return FALSE;
		}

		szPattern++;
	}

	/* Finished the pattern. */

	/* Only succeed if we've reached the end of the test string, too. */
	return !(*sz);
}
