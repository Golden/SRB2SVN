/* SRB2 Workbench -- A map editor for Sonic Robo Blast 2.
 * Copyright (C) 2009 Gregor Dick
 * http://workbench.srb2.org/
 *
 * Incorporates portions of Doom Builder, (C) 2003 Pascal vd Heiden.
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 * USA.
 *
 * wad.h: Header for wad.c.
 *
 * AMDG.
 */

#ifndef __SRB2B_WAD__
#define __SRB2B_WAD__

#include <windows.h>

#include "general.h"
#include "config.h"


typedef struct _WAD WAD;


#define CCH_LUMPNAME			8


WAD *CreateWad(void);
void FreeWad(WAD *lpwad);
WAD *OpenWad(LPCTSTR szFileName);
long CreateLump(WAD *lpwad, LPCSTR sLumpName, long iLumpIndex);
BOOL SetLump(WAD *lpwad, long iLumpIndex, const BYTE *lpbyData, long cb);
long GetLumpIndex(WAD *lpwad, long iFirstLump, long iLastLump, LPCSTR sLumpName);
long GetLumpCount(WAD *lpwad);
BOOL GetLumpNameA(WAD *lpwad, long iLumpIndex, LPSTR sLumpName);
long GetLumpLength(WAD *lpwad, long iLumpIndex);
long GetLump(WAD *lpwad, long iLumpIndex, BYTE *lpbyBuffer, long cbBuffer);
BOOL DeleteMultipleLumps(WAD *lpwad, long iFirstLump, long iLastLump);
BOOL WriteWad(WAD *lpwad, LPCTSTR szFileName, CONFIG *lpcfgChangedMaps);
WAD* DuplicateWadToMemory(WAD *lpwad);
void SetLumpName(WAD *lpwad, long iLumpIndex, LPCSTR sLumpName);


#ifdef UNICODE
static __inline BOOL GetLumpName(WAD *lpwad, long iLumpIndex, LPWSTR sLumpName)
{
	char sLumpNameA[CCH_LUMPNAME];

	if(GetLumpNameA(lpwad, iLumpIndex, sLumpNameA))
	{
		MultiByteToWideChar(CP_ACP, MB_PRECOMPOSED, sLumpNameA, NUM_ELEMENTS(sLumpNameA), sLumpName, CCH_LUMPNAME);
		return TRUE;
	}

	return FALSE;
}
#else
#define GetLumpName GetLumpNameA
#endif


#endif
