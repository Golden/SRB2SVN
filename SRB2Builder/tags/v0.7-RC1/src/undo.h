/* SRB2 Workbench -- A map editor for Sonic Robo Blast 2.
 * Copyright (C) 2009 Gregor Dick
 * http://workbench.srb2.org/
 *
 * Incorporates portions of Doom Builder, (C) 2003 Pascal vd Heiden.
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 * USA.
 *
 * undo.h: Header for undo.c.
 *
 * AMDG.
 */

#ifndef __SRB2B_UNDO_H__
#define __SRB2B_UNDO_H__

typedef struct _UNDOSTACK UNDOSTACK;

UNDOSTACK* CreateUndoStack(MAP *lpmap, int iStringIndex);
void FreeUndoStack(UNDOSTACK *lpundostack);
void PushNewUndoFrame(UNDOSTACK *lpundostack, MAP *lpmap, int iStringIndex);
int GetUndoTopStringIndex(UNDOSTACK *lpundostack);
BOOL UndoStackHasOnlyOneFrame(UNDOSTACK *lpundostack);
void PopUndoFrame(UNDOSTACK *lpundostack);
void PerformTopUndo(UNDOSTACK *lpundostack, MAP *lpmap);

#ifdef _DEBUG
int DumpUndoStack(UNDOSTACK *lpundostack, LPCTSTR szFileName);
UNDOSTACK* LoadUndoStack(LPCTSTR szFileName);
#endif

#endif
