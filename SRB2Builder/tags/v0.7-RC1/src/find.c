/* SRB2 Workbench -- A map editor for Sonic Robo Blast 2.
 * Copyright (C) 2009 Gregor Dick
 * http://workbench.srb2.org/
 *
 * Incorporates portions of Doom Builder, (C) 2003 Pascal vd Heiden.
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 * USA.
 *
 * find.c: Routines for searching maps.
 *
 * AMDG.
 */

#include <windows.h>

#include "general.h"
#include "map.h"
#include "config.h"
#include "find.h"
#include "editing.h"


/* Static prototypes. */
static BYTE VertexSatisfiesFindConditionInt(MAP *lpmap, WORD wType, WORD wCompare, int iVertex, int iValue);
static BYTE LineSatisfiesFindConditionInt(MAP *lpmap, WORD wType, WORD wCompare, int iLinedef, int iValue);
static BYTE SectorSatisfiesFindConditionInt(MAP *lpmap, WORD wType, WORD wCompare, int iSector, int iValue);
static BYTE ThingSatisfiesFindConditionInt(MAP *lpmap, WORD wType, WORD wCompare, int iThing, int iValue, CONFIG *lpcfgFlatThings);
static BOOL FindCompareInt(int iObjectValue, int iValue, WORD wCompare);
static BYTE LineSatisfiesFindConditionStr(MAP *lpmap, WORD wType, WORD wCompare, int iLinedef, LPSTR szString);
static BYTE SectorSatisfiesFindConditionStr(MAP *lpmap, WORD wType, WORD wCompare, int iSector, LPSTR szString);
static BOOL FindCompareStr(LPSTR szObjectString, LPSTR szString, WORD wCompare);
static void LineReplaceInt(MAP *lpmap, WORD wType, int iLinedef, int iValue);
static void SectorReplaceInt(MAP *lpmap, WORD wType, int iSector, int iValue);
static void ThingReplaceInt(MAP *lpmap, WORD wType, int iThing, int iValue, CONFIG *lpcfgFlatThings);
static void LineReplaceStr(MAP *lpmap, WORD wType, int iLinedef, LPSTR szString, BYTE byMatchFlags);
static void SectorReplaceStr(MAP *lpmap, WORD wType, int iSector, LPSTR szString);



/* MapObjectSatisfiesFindConditionInt
 *   Determines whether a map object satisfies a particular condition with an
 *   integer value to check.
 *
 * Parameters:
 *   MAP*		lpmap			Map
 *   WORD		wType			Type of condition: see ENUM_FIND_TYPES.
 *   WORD		wCompare		Comparative operator: see ENUM_FIND_CONDITIONS.
 *   int		iIndex			Index of object.
 *   int		iValue			Value to match.
 *   CONFIG*	lpcfgFlatThings	Flat things subsection.
 *
 * Return value: BYTE
 *   Zero if no matches; a search-type-specific non-zero value if there are any
 *   matches corresponding to the object.
 */
BYTE MapObjectSatisfiesFindConditionInt(MAP *lpmap, WORD wType, WORD wCompare, int iIndex, int iValue, CONFIG *lpcfgFlatThings)
{
	switch(wType)
	{
	case FT_VX:
		return VertexSatisfiesFindConditionInt(lpmap, wType, wCompare, iIndex, iValue);

	case FT_LINE: case FT_LINEEFFECT: case FT_LINEFLAGS: case FT_LINETAG:
	case FT_SIDEDEF:
		return LineSatisfiesFindConditionInt(lpmap, wType, wCompare, iIndex, iValue);

	case FT_SEC: case FT_SECCEIL: case FT_SECFLR: case FT_SECEFFECT:
	case FT_SECEFFECTA: case FT_SECEFFECTB: case FT_SECEFFECTC:
	case FT_SECEFFECTD: case FT_SECTAG: case FT_SECLIGHT: case FT_SECHEIGHT:
		return SectorSatisfiesFindConditionInt(lpmap, wType, wCompare, iIndex, iValue);

	case FT_THING: case FT_THINGANGLE: case FT_THINGFLAGS: case FT_THINGTYPE:
	case FT_THINGZOFF: case FT_THINGZABS:
		return ThingSatisfiesFindConditionInt(lpmap, wType, wCompare, iIndex, iValue, lpcfgFlatThings);
	}

	/* Should never reach here. */
	return 0;
}


/* MapObjectSatisfiesFindConditionStr
 *   Determines whether a map object satisfies a particular condition with a
 *   string value to check.
 *
 * Parameters:
 *   MAP*	lpmap		Map
 *   WORD	wType		Type of condition: see ENUM_FIND_TYPES.
 *   WORD	wCompare	Comparative operator: see ENUM_FIND_CONDITIONS.
 *   int	iIndex		Index of object.
 *   LPSTR	szString	String to match.
 *
 * Return value: BYTE
 *   Zero if no matches; a search-type-specific non-zero value if there are any
 *   matches corresponding to the object.
 */
BYTE MapObjectSatisfiesFindConditionStr(MAP *lpmap, WORD wType, WORD wCompare, int iIndex, LPSTR szString)
{

	switch(wType)
	{
	case FT_LINETEX:
		return LineSatisfiesFindConditionStr(lpmap, wType, wCompare, iIndex, szString);
	case FT_SECCEILFLAT: case FT_SECFLRFLAT:
		return SectorSatisfiesFindConditionStr(lpmap, wType, wCompare, iIndex, szString);
	}

	/* Should never reach here. */
	return 0;
}


/* VertexSatisfiesFindConditionInt
 *   Determines whether a vertex satisfies a particular condition.
 *
 * Parameters:
 *   MAP*	lpmap		Map
 *   WORD	wType		Type of condition: see ENUM_FIND_TYPES.
 *   WORD	wCompare	Comparative operator: see ENUM_FIND_CONDITIONS.
 *   int	iVertex		Index of vertex.
 *   int	iValue		Value to match.
 *
 * Return value: BYTE
 *   Non-zero if it matches; zero otherwise.
 */
static BYTE VertexSatisfiesFindConditionInt(MAP *lpmap, WORD wType, WORD wCompare, int iVertex, int iValue)
{
	int iObjectValue;

	UNREFERENCED_PARAMETER(lpmap);

	switch(wType)
	{
	case FT_VX:
		iObjectValue = iVertex;
		break;

	default:
		return 0;
	}

	return FindCompareInt(iObjectValue, iValue, wCompare);
}


/* LineSatisfiesFindConditionInt
 *   Determines whether a linedef satisfies a particular condition.
 *
 * Parameters:
 *   MAP*	lpmap		Map
 *   WORD	wType		Type of condition: see ENUM_FIND_TYPES.
 *   WORD	wCompare	Comparative operator: see ENUM_FIND_CONDITIONS.
 *   int	iLinedef	Index of linedef.
 *   int	iValue		Value to match.
 *
 * Return value: BYTE
 *   Zero if no matches; a search-type-specific non-zero value if there are any
 *   matches corresponding to the object.
 */
static BYTE LineSatisfiesFindConditionInt(MAP *lpmap, WORD wType, WORD wCompare, int iLinedef, int iValue)
{
	int iObjectValue;
	MAPLINEDEF *lpld = &lpmap->linedefs[iLinedef];

	switch(wType)
	{
	case FT_LINE:
		iObjectValue = iLinedef;
		break;

	case FT_LINEEFFECT:
		iObjectValue = lpld->effect;
		break;

	case FT_LINEFLAGS:
		iObjectValue = lpld->flags;
		break;

	case FT_LINETAG:
		iObjectValue = lpld->tag;
		break;

	case FT_SIDEDEF:
		{
			BYTE byMatchFlags = 0;

			/* Find which of the sidedefs match. */
			if(SidedefExists(lpmap, lpld->s1) && FindCompareInt(lpld->s1, iValue, wCompare))
				byMatchFlags |= FM_SD_FRONT;

			if(SidedefExists(lpmap, lpld->s2) && FindCompareInt(lpld->s2, iValue, wCompare))
				byMatchFlags |= FM_SD_BACK;

			return byMatchFlags;
		}

	default:
		return FALSE;
	}

	return FindCompareInt(iObjectValue, iValue, wCompare);
}


/* SectorSatisfiesFindConditionInt
 *   Determines whether a sector satisfies a particular condition.
 *
 * Parameters:
 *   MAP*	lpmap		Map
 *   WORD	wType		Type of condition: see ENUM_FIND_TYPES.
 *   WORD	wCompare	Comparative operator: see ENUM_FIND_CONDITIONS.
 *   int	iSector		Index of sector.
 *   int	iValue		Value to match.
 *
 * Return value: BYTE
 *   Non-zero if it matches; zero otherwise.
 */
static BYTE SectorSatisfiesFindConditionInt(MAP *lpmap, WORD wType, WORD wCompare, int iSector, int iValue)
{
	int iObjectValue;
	MAPSECTOR *lpsec = &lpmap->sectors[iSector];

	switch(wType)
	{
	case FT_SEC:		iObjectValue = iSector;								break;
	case FT_SECCEIL:	iObjectValue = lpsec->hceiling;						break;
	case FT_SECFLR:		iObjectValue = lpsec->hfloor;						break;
	case FT_SECEFFECT:	iObjectValue = lpsec->special;						break;
	case FT_SECEFFECTA:	iObjectValue = SECEFFECT_NYBBLE0(lpsec->special);	break;
	case FT_SECEFFECTB:	iObjectValue = SECEFFECT_NYBBLE1(lpsec->special);	break;
	case FT_SECEFFECTC:	iObjectValue = SECEFFECT_NYBBLE2(lpsec->special);	break;
	case FT_SECEFFECTD:	iObjectValue = SECEFFECT_NYBBLE3(lpsec->special);	break;
	case FT_SECTAG:		iObjectValue = lpsec->tag;							break;
	case FT_SECLIGHT:	iObjectValue = lpsec->brightness;					break;
	case FT_SECHEIGHT:	iObjectValue = lpsec->hceiling - lpsec->hfloor;		break;

	default:			return 0;
	}

	return FindCompareInt(iObjectValue, iValue, wCompare);
}


/* ThingSatisfiesFindConditionInt
 *   Determines whether a thing satisfies a particular condition.
 *
 * Parameters:
 *   MAP*		lpmap			Map
 *   WORD		wType			Type of condition: see ENUM_FIND_TYPES.
 *   WORD		wCompare		Comparative operator: see ENUM_FIND_CONDITIONS.
 *   int		iThing			Index of thing.
 *   int		iValue			Value to match.
 *   CONFIG*	lpcfgFlatThings	Flat things subsection.
 *
 * Return value: BYTE
 *   Non-zero if it matches; zero otherwise.
 */
static BYTE ThingSatisfiesFindConditionInt(MAP *lpmap, WORD wType, WORD wCompare, int iThing, int iValue, CONFIG *lpcfgFlatThings)
{
	int iObjectValue;
	MAPTHING *lpthing = &lpmap->things[iThing];

	switch(wType)
	{
	case FT_THING:			iObjectValue = iThing;			break;
	case FT_THINGANGLE:		iObjectValue = lpthing->angle;	break;
	case FT_THINGFLAGS:		iObjectValue = lpthing->flag;	break;
	case FT_THINGTYPE:		iObjectValue = lpthing->thing;	break;
	case FT_THINGZOFF:		iObjectValue = GetThingZ(lpmap, iThing, lpcfgFlatThings, FALSE); break;
	case FT_THINGZABS:		iObjectValue = GetThingZ(lpmap, iThing, lpcfgFlatThings, TRUE); break;

	default:				return 0;
	}

	return FindCompareInt(iObjectValue, iValue, wCompare);
}


/* FindCompareInt
 *   Performs an integer comparison between two integers, using the specified
 *   operation.
 *
 * Parameters:
 *   int	iObjectValue	Value from object.
 *   int	iValue			Value to compare with.
 *   WORD	wCompare		Comparative operator: see ENUM_FIND_CONDITIONS.
 *
 * Return value: TRUE
 *   TRUE if it matches; FALSE otherwise.
 */
static BOOL FindCompareInt(int iObjectValue, int iValue, WORD wCompare)
{
	switch(wCompare)
	{
	case FC_EQUAL:		return iObjectValue == iValue;
	case FC_NOTEQUAL:	return iObjectValue != iValue;
	case FC_GREATER:	return iObjectValue >  iValue;
	case FC_GREATEREQ:	return iObjectValue >= iValue;
	case FC_LESS:		return iObjectValue <  iValue;
	case FC_LESSEQ:		return iObjectValue <= iValue;
	case FC_AND:		return iObjectValue &  iValue;
	default:			return FALSE;
	}
}


/* LineSatisfiesFindConditionStr
 *   Determines whether a linedef satisfies a particular condition.
 *
 * Parameters:
 *   MAP*	lpmap		Map
 *   WORD	wType		Type of condition: see ENUM_FIND_TYPES.
 *   WORD	wCompare	Comparative operator: see ENUM_FIND_CONDITIONS.
 *   int	iLinedef	Index of linedef.
 *   LPSTR	szString	String to match.
 *
 * Return value: BYTE
 *   Zero if no matches; a bitfield specifying which textures match if there are
 *   any.
 */
static BYTE LineSatisfiesFindConditionStr(MAP *lpmap, WORD wType, WORD wCompare, int iLinedef, LPSTR szString)
{
	switch(wType)
	{
	case FT_LINETEX:
		{
			MAPLINEDEF *lpld = &lpmap->linedefs[iLinedef];
			int iSidedefs[] = {lpld->s1, lpld->s2};
			int iSide;
			BYTE byMatchFlags = 0;

			for(iSide = 0; iSide <= 1; iSide++)
			{
				/* Do we have a sidedef here? */
				if(SidedefExists(lpmap, iSidedefs[iSide]))
				{
					CHAR szUpper[TEXNAME_BUFFER_LENGTH];
					CHAR szMiddle[TEXNAME_BUFFER_LENGTH];
					CHAR szLower[TEXNAME_BUFFER_LENGTH];

					/* Make null-terminated strings from the texture names. */
					strncpy(szUpper, lpmap->sidedefs[iSidedefs[iSide]].upper, TEXNAME_WAD_BUFFER_LENGTH);
					strncpy(szMiddle, lpmap->sidedefs[iSidedefs[iSide]].middle, TEXNAME_WAD_BUFFER_LENGTH);
					strncpy(szLower, lpmap->sidedefs[iSidedefs[iSide]].lower, TEXNAME_WAD_BUFFER_LENGTH);
					szUpper[8] = szMiddle[8] = szLower[8] = '\0';

					/* Do any of the textures match? */
					if(FindCompareStr(szUpper, szString, wCompare))
						byMatchFlags |= (iSide == 0) ? FM_TEX_FRONTUPPER : FM_TEX_BACKUPPER;
					if(FindCompareStr(szMiddle, szString, wCompare))
						byMatchFlags |= (iSide == 0) ? FM_TEX_FRONTMIDDLE : FM_TEX_BACKMIDDLE;
					if(FindCompareStr(szLower, szString, wCompare))
						byMatchFlags |= (iSide == 0) ? FM_TEX_FRONTLOWER : FM_TEX_BACKLOWER;
				}
			}

			/* Return matches. */
			return byMatchFlags;
		}

	default:
		return 0;
	}
}


/* SectorSatisfiesFindConditionStr
 *   Determines whether a sector satisfies a particular condition.
 *
 * Parameters:
 *   MAP*	lpmap		Map
 *   WORD	wType		Type of condition: see ENUM_FIND_TYPES.
 *   WORD	wCompare	Comparative operator: see ENUM_FIND_CONDITIONS.
 *   int	iSector		Index of sector.
 *   LPSTR	szString	String to match.
 *
 * Return value: BYTE
 *   Non-zero if it matches; zero otherwise.
 */
static BYTE SectorSatisfiesFindConditionStr(MAP *lpmap, WORD wType, WORD wCompare, int iSector, LPSTR szString)
{
	switch(wType)
	{
	case FT_SECCEILFLAT:
	case FT_SECFLRFLAT:
		{
			MAPSECTOR *lpsec = &lpmap->sectors[iSector];
			CHAR szFlatName[TEXNAME_BUFFER_LENGTH];

			/* Make a null-terminated string from the flat name. */
			strncpy(szFlatName, wType == FT_SECCEILFLAT ? lpsec->tceiling : lpsec->tfloor, NUM_ELEMENTS(szFlatName) - 1);
			szFlatName[NUM_ELEMENTS(szFlatName) - 1] = '\0';

			/* Does the flatname match? */
			return FindCompareStr(szFlatName, szString, wCompare);
		}

	default:
		return 0;
	}
}


/* FindCompareStr
 *   Performs a case-insensitive comparison between two integers, using the
 *   specified operation with stricmp-esque lexographic ordering.
 *
 * Parameters:
 *   LPSTR	szObjectString	String from object.
 *   LPSTR	szString		String to compare with.
 *   WORD	wCompare		Comparative operator: see ENUM_FIND_CONDITIONS.
 *
 * Return value: TRUE
 *   TRUE if it matches; FALSE otherwise.
 */
static BOOL FindCompareStr(LPSTR szObjectString, LPSTR szString, WORD wCompare)
{
	int iRet = _stricmp(szObjectString, szString);

	switch(wCompare)
	{
	case FC_EQUAL:		return iRet == 0;
	case FC_NOTEQUAL:	return iRet != 0;
	case FC_GREATER:	return iRet >  0;
	case FC_GREATEREQ:	return iRet >= 0;
	case FC_LESS:		return iRet <  0;
	case FC_LESSEQ:		return iRet <= 0;
	default:			return FALSE;
	}
}



/* ReplaceInt
 *   Replaces an integer property of an object.
 *
 * Parameters:
 *   MAP*		lpmap			Map
 *   WORD		wType			Property: see ENUM_FIND_TYPES.
 *   int		iIndex			Index of object.
 *   int		iValue			New value.
 *   CONFIG*	lpcfgFlatThings	Flat things subsection.
 *
 * Return value: None.
 */
void ReplaceInt(MAP *lpmap, WORD wType, int iIndex, int iValue, CONFIG *lpcfgFlatThings)
{
	switch(wType)
	{
	case FT_LINEEFFECT: case FT_LINEFLAGS: case FT_LINETAG:
		LineReplaceInt(lpmap, wType, iIndex, iValue);
		return;

	case FT_SECCEIL: case FT_SECFLR: case FT_SECEFFECT: case FT_SECEFFECTA:
	case FT_SECEFFECTB: case FT_SECEFFECTC: case FT_SECEFFECTD: case FT_SECTAG:
	case FT_SECLIGHT:
		SectorReplaceInt(lpmap, wType, iIndex, iValue);
		return;

	case FT_THINGANGLE: case FT_THINGFLAGS: case FT_THINGTYPE:
	case FT_THINGZOFF: case FT_THINGZABS:
		ThingReplaceInt(lpmap, wType, iIndex, iValue, lpcfgFlatThings);
		return;
	}

	/* Should never reach here. */
}


/* LineReplaceInt
 *    Replaces an integer property of a line.
 *
 * Parameters:
 *   MAP*	lpmap		Map
 *   WORD	wType		Property: see ENUM_FIND_TYPES.
 *   int	iLinedef	Index of linedef.
 *   int	iValue		New value.
 *
 * Return value: None.
 */
static void LineReplaceInt(MAP *lpmap, WORD wType, int iLinedef, int iValue)
{
	MAPLINEDEF *lpld = &lpmap->linedefs[iLinedef];

	switch(wType)
	{
	case FT_LINEEFFECT:
		lpld->effect = iValue;
		break;

	case FT_LINEFLAGS:
		lpld->flags = iValue;
		break;

	case FT_LINETAG:
		lpld->tag = iValue;
		break;
	}
}


/* SectorReplaceInt
 *    Replaces an integer property of a sector.
 *
 * Parameters:
 *   MAP*	lpmap		Map
 *   WORD	wType		Property: see ENUM_FIND_TYPES.
 *   int	iSector		Index of sector.
 *   int	iValue		New value.
 *
 * Return value: None.
 */
static void SectorReplaceInt(MAP *lpmap, WORD wType, int iSector, int iValue)
{
	MAPSECTOR *lpsec = &lpmap->sectors[iSector];

	switch(wType)
	{
	case FT_SECCEIL:	lpsec->hceiling = iValue ;							break;
	case FT_SECFLR:		lpsec->hfloor = iValue ;							break;
	case FT_SECEFFECT:	lpsec->special = iValue ;							break;
	case FT_SECEFFECTA:	lpsec->special = NYBBLE0_TO_SECEFFECTMASK(iValue);	break;
	case FT_SECEFFECTB:	lpsec->special = NYBBLE1_TO_SECEFFECTMASK(iValue);	break;
	case FT_SECEFFECTC:	lpsec->special = NYBBLE2_TO_SECEFFECTMASK(iValue);	break;
	case FT_SECEFFECTD:	lpsec->special = NYBBLE3_TO_SECEFFECTMASK(iValue);	break;
	case FT_SECTAG:		lpsec->tag = iValue ;								break;
	case FT_SECLIGHT:	lpsec->brightness = iValue ;						break;
	}
}


/* ThingReplaceInt
 *    Replaces an integer property of a thing.
 *
 * Parameters:
 *   MAP*		lpmap			Map
 *   WORD		wType			Property: see ENUM_FIND_TYPES.
 *   int		iThing			Index of thing.
 *   int		iValue			New value.
 *   CONFIG*	lpcfgFlatThings	Flat things subsection.
 *
 * Return value: None.
 */
static void ThingReplaceInt(MAP *lpmap, WORD wType, int iThing, int iValue, CONFIG *lpcfgFlatThings)
{
	MAPTHING *lpthing = &lpmap->things[iThing];

	switch(wType)
	{
	case FT_THINGANGLE:		lpthing->angle = iValue;	break;
	case FT_THINGFLAGS:		lpthing->flag  = iValue;	break;
	case FT_THINGTYPE:		lpthing->thing = iValue;	break;
	case FT_THINGZOFF:		SetThingZ(lpmap, iThing, lpcfgFlatThings, (WORD)iValue, FALSE); break;
	case FT_THINGZABS:		SetThingZ(lpmap, iThing, lpcfgFlatThings, (WORD)iValue, TRUE); break;
	}
}



/* ReplaceStr
 *   Replaces string property/ies of an object.
 *
 * Parameters:
 *   MAP*	lpmap			Map.
 *   WORD	wType			Type of condition: see ENUM_FIND_TYPES.
 *   int	iIndex			Index of object.
 *   LPSTR	szString		New string.
 *   BYTE	byMatchFlags	Flags specifying which subproperties to set, if
 *							applicable.
 *
 * Return value: None.
 */
void ReplaceStr(MAP *lpmap, WORD wType, int iIndex, LPSTR szString, BYTE byMatchFlags)
{
	switch(wType)
	{
	case FT_LINETEX:
		LineReplaceStr(lpmap, wType, iIndex, szString, byMatchFlags);
		return;

	case FT_SECCEILFLAT: case FT_SECFLRFLAT:
		SectorReplaceStr(lpmap, wType, iIndex, szString);
		return;
	}
}


/* LineReplaceStr
 *   Replaces string property/ies of a linedef.
 *
 * Parameters:
 *   MAP*	lpmap			Map.
 *   WORD	wType			Type of condition: see ENUM_FIND_TYPES.
 *   int	iIndex			Index of object.
 *   LPSTR	szString		New string.
 *   BYTE	byMatchFlags	Flags specifying which subproperties to set, if
 *							applicable.
 *
 * Return value: None.
 */
static void LineReplaceStr(MAP *lpmap, WORD wType, int iLinedef, LPSTR szString, BYTE byMatchFlags)
{
	MAPLINEDEF *lpld = &lpmap->linedefs[iLinedef];

	switch(wType)
	{
	case FT_LINETEX:
		/* For each texture that matched, replace it. */
		if(byMatchFlags & FM_TEX_FRONTUPPER)
			CopyMemory(lpmap->sidedefs[lpld->s1].upper, szString, 8);
		if(byMatchFlags & FM_TEX_FRONTMIDDLE)
			CopyMemory(lpmap->sidedefs[lpld->s1].middle, szString, 8);
		if(byMatchFlags & FM_TEX_FRONTLOWER)
			CopyMemory(lpmap->sidedefs[lpld->s1].lower, szString, 8);

		if(byMatchFlags & FM_TEX_BACKUPPER)
			CopyMemory(lpmap->sidedefs[lpld->s2].upper, szString, 8);
		if(byMatchFlags & FM_TEX_BACKMIDDLE)
			CopyMemory(lpmap->sidedefs[lpld->s2].middle, szString, 8);
		if(byMatchFlags & FM_TEX_BACKLOWER)
			CopyMemory(lpmap->sidedefs[lpld->s2].lower, szString, 8);

		return;
	}
}


/* SectorReplaceStr
 *   Replaces string property/ies of a linedef.
 *
 * Parameters:
 *   MAP*	lpmap			Map.
 *   WORD	wType			Type of condition: see ENUM_FIND_TYPES.
 *   int	iIndex			Index of object.
 *   LPSTR	szString		New string.
 *
 * Return value: None.
 */
static void SectorReplaceStr(MAP *lpmap, WORD wType, int iSector, LPSTR szString)
{
	MAPSECTOR *lpsec = &lpmap->sectors[iSector];

	switch(wType)
	{
	case FT_SECCEILFLAT:
		CopyMemory(lpsec->tceiling, szString, 8);
		return;

	case FT_SECFLRFLAT:
		CopyMemory(lpsec->tfloor, szString, 8);
		return;
	}
}
