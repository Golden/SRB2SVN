/* SRB2 Workbench -- A map editor for Sonic Robo Blast 2.
 * Copyright (C) 2009 Gregor Dick
 * http://workbench.srb2.org/
 *
 * Incorporates portions of Doom Builder, (C) 2003 Pascal vd Heiden.
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 * USA.
 *
 * texture.c: Routines for dealing with Doom's various graphics formats.
 *
 * AMDG.
 */

#include <windows.h>
#include <search.h>
#include <stdio.h>
#include <tchar.h>

#include "general.h"
#include "texture.h"
#include "openwads.h"
#include "wad.h"

#include "../res/resource.h"


/* Macros. */
#define COLUMN_END				0xFF
#define TNL_BUFSIZEINCREMENT	64


/* Types. */
typedef struct _ADDTOTEXCACHEDATA
{
	TEX_FORMAT	tf;
	WAD			*lpwad;
	TEXCACHE	*lptcHdr;
	RGBQUAD		*lprgbq;
} ADDTOTEXCACHEDATA;


typedef struct _TEXTURE_PATCH_DESCRIPTOR
{
	short		xOffset, yOffset;
	short		nPatchNum;
	short		nStepDir, nColourmap;
} TEXTURE_PATCH_DESCRIPTOR;


typedef struct _TEXTURE_ENTRY
{
	char		sTexName[8];
	short		nReserved1, nReserved2;
	short		cx, cy;
	short		nReserved3, nReserved4;
	short		nNumPatches;

	/* There are variably many of these. Declared thus for convenience. */
	TEXTURE_PATCH_DESCRIPTOR	tpd[1];
} TEXTURE_ENTRY;


struct _TEXTUREN
{
	int		iNumTextures;

	/* There are variably many of these. Declared thus for convenience. */
	int		iOffsets[1];

	/* After the offsets follow iNumTextures many TEXTURE_ENTRY structures. */
};


typedef struct _DOOMPICTURE_HEADER
{
	short	cx, cy;
	short	xOffset, yOffset;

	/* There are variably many of these. Declared thus for convenience. */
	int		iDataOffsets[1];
} DOOMPICTURE_HEADER;


typedef struct _DOOMPICTURE_POST
{
	BYTE	byRowStart;
	BYTE	byNumPixels;
	BYTE	byReserved;

	/* There are variably many of these. Declared thus for convenience. */
	BYTE	byPixels[1];
} DOOMPICTURE_POST;


struct _PNAMES
{
	int			iNumPatches;

	/* There are variably many of these. Declared thus for convenience. */
	char		lpsPatchName[1][8];
};


static void EnumTextures(WAD *lpwad, TEX_FORMAT tf, void (CALLBACK *f)(const char *sTexName, void *lpvParam), void *lpvParam);
static void DoomPictureToDIB(BYTE *lpbyDIB, BYTE *lpbyDIBMask, DOOMPICTURE_HEADER *lpdp, int cbImage, short cxDIB, short cyDIB, short xOffset, short yOffset);
static int CompareTextureNames(LPCTSTR sz1, LPCTSTR sz2);
static int CompareTextureNameHeads(LPCTSTR sz1, LPCTSTR sz2);
static __inline TEXTURE_ENTRY* TextureEntryFromTextureN(TEXTUREN *lptexn, int iTexIndex);


/* LoadTexture
 *   Loads a texture from a wad.
 *
 * Parameters:
 *   TEXTURE_DIRECTORY	lptexdir	Directory from which to load the texture.
 *   LPCSTR				szTexName	Name of texture's lump.
 *   TEX_FORMAT			tf			Specifies whether flat or texture.
 *   RGBQUAD*			lprgbq		256-colour palette.
 *
 * Return value: TEXTURE*
 *   Pointer to newly-allocated texture.
 *
 * Remarks:
 *   The caller must call DestroyTexture to free the memory!
 */
TEXTURE* LoadTextureW(TEXTURE_DIRECTORY *lptexdir, LPCWSTR sTexName, TEX_FORMAT tf, RGBQUAD *lprgbq)
{
	WCHAR szTexNameW[TEXNAME_BUFFER_LENGTH];
	char szTexNameA[TEXNAME_BUFFER_LENGTH];

	/* Terminate. */
	CopyMemory(szTexNameW, sTexName, sizeof(WCHAR) * (TEXNAME_BUFFER_LENGTH - 1));
	szTexNameW[TEXNAME_BUFFER_LENGTH - 1] = L'\0';

	WideCharToMultiByte(CP_ACP, 0, szTexNameW, NUM_ELEMENTS(szTexNameW), szTexNameA, NUM_ELEMENTS(szTexNameA), NULL, NULL);
	return LoadTextureA(lptexdir, szTexNameA, tf, lprgbq);
}

TEXTURE* LoadTextureA(TEXTURE_DIRECTORY *lptexdir, LPCSTR sTexName, TEX_FORMAT tf, RGBQUAD *lprgbq)
{
	TEXTURE *lptex = NULL;
	BYTE *lpbyBits = NULL, *lpbyBitsMask = NULL;

	switch(tf)
	{
	case TF_FLAT:
		{
			int i;

			/* Repeat for each wad in directory until we find the flat. */
			for(i = 0; i < lptexdir->iNumWads && lptex == NULL; i++)
			{
				int	cb;
				long iFStart = 0, iFEnd = -1, iFlat;

				/* The marker lumps are ignored by the game itself, but we
				 * respect them anyway, because otherwise it's hard to tell
				 * what is a flat and what isn't.
				 */
#ifndef IGNOREFLATMARKERS
				/* TODO: Read the section from the config (?). */
				iFStart = GetLumpIndex(lptexdir->lplpwad[i], 0, -1, "F_START");
				if(iFStart < 0) continue;
				iFEnd = GetLumpIndex(lptexdir->lplpwad[i], iFStart, -1, "F_END");
				if(iFEnd < 0) continue;
#endif

				iFlat = GetLumpIndex(lptexdir->lplpwad[i], iFStart, iFEnd, sTexName);
				if(iFlat < 0) continue;

				cb = GetLumpLength(lptexdir->lplpwad[i], iFlat);
				if(cb <= 0) continue;

				lpbyBits = ProcHeapAlloc(cb);
				lpbyBitsMask = ProcHeapAlloc(cb);
				GetLump(lptexdir->lplpwad[i], iFlat, lpbyBits, cb);

				/* Mask is trivial. */
				ZeroMemory(lpbyBitsMask, cb);

				lptex = ProcHeapAlloc(sizeof(TEXTURE));

				/* There's probably a nice, integer way of doing this. Also
				 * notice that flat dimensions are always multiples of 4, so we
				 * have no DIB problems.
				 */
				lptex->cx = lptex->cy = (int)sqrt(cb);

				lptex->xOffset = lptex->yOffset = 0;
			}
		}

		/* Pointer to texture will be returned. */
		break;

	case TF_TEXTURE:
		{
			typedef struct _PATCH_MARKERS {WAD *lpwad; long iPStart, iPEnd;} PATCH_MARKERS;
			PATCH_MARKERS *lppm;
			TEXTURE_ENTRY *lpte;
			int cbTextureBitmap;
			int i, j;
			int iPatchSections;

			/* Find the specified texture in either TEXTURE1 or TEXTURE2. */

			/* Assume we don't find it. */
			lpte = NULL;

			/* Search TEXTURE1, and also TEXTURE2 if not found in the former. */
			for(i = 0; i < 2 && !lpte; i++)
			{
				TEXTUREN *lptexn = (i == 0) ? lptexdir->lptexnTexture1 : lptexdir->lptexnTexture2;

				if(lptexn)
				{
					for(j = 0; j < lptexn->iNumTextures; j++)
					{
						TEXTURE_ENTRY *lpteCheck = TextureEntryFromTextureN(lptexn, j);
						if(strncmp(lpteCheck->sTexName, sTexName, TEXNAME_WAD_BUFFER_LENGTH) == 0)
						{
							/* Found it! */
							lpte = lpteCheck;
							break;
						}
					}
				}
			}


			/* No match? Or bad texture descriptor? */
			if(!lpte || lpte->cx <= 0 || lpte->cy <= 0)
				break;


			/* Patch markers are ignored, apparently. */
#ifdef USEPATCHMARKERS
			/* Find the P_START and P_END lumps in each wad, if they exist. */
			lppm = ProcHeapAlloc(lptexdir->iNumWads * 2 * sizeof(PATCH_MARKERS));
			iPatchSections = 0;
			for(i = 0; i < lptexdir->iNumWads; i++)
			{
				struct {char szPStart[CCH_LUMPNAME + 1], szPEnd[CCH_LUMPNAME + 1];} patchmarkernames[] = {{"P_START", "P_END"}, {"PP_START", "PP_END"}};

				for(j = 0; j < NUM_ELEMENTS(patchmarkernames); j++)
				{
					lppm[iPatchSections].lpwad = lptexdir->lplpwad[i];
					lppm[iPatchSections].iPStart = GetLumpIndex(lptexdir->lplpwad[i], 0, -1, patchmarkernames[j].szPStart);
					lppm[iPatchSections].iPEnd = GetLumpIndex(lptexdir->lplpwad[i], lppm[iPatchSections].iPStart, -1, patchmarkernames[j].szPEnd);

					if(lppm[iPatchSections].iPStart >= 0 && lppm[iPatchSections].iPEnd >= 0)
						iPatchSections++;
				}
			}

			/* No wads containing patches? */
			if(iPatchSections == 0)
			{
				ProcHeapFree(lppm);
				break;
			}
#else
			iPatchSections = lptexdir->iNumWads;
			lppm = ProcHeapAlloc(iPatchSections * sizeof(PATCH_MARKERS));

			for(i = 0; i < lptexdir->iNumWads; i++)
			{
				lppm[i].lpwad = lptexdir->lplpwad[i];
				lppm[i].iPStart = 0;
				lppm[i].iPEnd = -1;
			}
#endif

			/* From now on, we won't fail, even if we can't find a patch or it's
			 * bad. We just won't draw it.
			 */

			/* Allocate memory for the texture. */
			lptex = ProcHeapAlloc(sizeof(TEXTURE));

			/* DIBs must have widths of a multiple of 4. */
			lptex->cx = ((lpte->cx - 1) & ~3) + 4;
			lptex->cy = lpte->cy;
			cbTextureBitmap = lptex->cx * lptex->cy;
			lptex->xOffset = lptex->yOffset = 0;

			lpbyBits = ProcHeapAlloc(cbTextureBitmap);
			lpbyBitsMask = ProcHeapAlloc(cbTextureBitmap);

			/* Clear the texture and mask bitmaps. */
			ZeroMemory(lpbyBits, cbTextureBitmap);
			FillMemory(lpbyBitsMask, cbTextureBitmap, 1);

			/* Loop for each patch in the texture. */
			for(i = 0; i < lpte->nNumPatches; i++)
			{
				/* Make sure patch is in range. And yes, there may be long many
				 * patches, but a texture can only reference short many.
				 */
				if(lpte->tpd[i].nPatchNum >= lptexdir->lppnames->iNumPatches) continue;

				/* Check in each of the wads having patches. */
				for(j = 0; j < iPatchSections; j++)
				{
					long iPatch = GetLumpIndex(lppm[j].lpwad, lppm[j].iPStart, lppm[j].iPEnd, lptexdir->lppnames->lpsPatchName[lpte->tpd[i].nPatchNum]);
					int cbPatch = GetLumpLength(lppm[j].lpwad, iPatch);

					/* Patch found? */
					if(cbPatch > 0)
					{
						DOOMPICTURE_HEADER *lpdpPatch = ProcHeapAlloc(cbPatch);

						/* Load patch. */
						GetLump(lppm[j].lpwad, iPatch, (BYTE*)lpdpPatch, cbPatch);

						/* Blit! */
						DoomPictureToDIB(lpbyBits, lpbyBitsMask, lpdpPatch, cbPatch, lptex->cx, lptex->cy, lpte->tpd[i].xOffset, lpte->tpd[i].yOffset);

						/* Free patch. */
						ProcHeapFree(lpdpPatch);

						/* Don't look in any remaining wads. */
						break;
					}
				}
			}

			/* Hooray! Finished blitting. */
		}

		/* Pointer to texture will be returned. */
		break;

	case TF_IMAGE:
		{
			int i;
			int cbImage = -1, cbTextureBitmap;
			DOOMPICTURE_HEADER *lpdp = NULL;

			/* Repeat for each wad in directory until we find the flat. */
			for(i = 0; i < lptexdir->iNumWads && lpdp == NULL; i++)
			{
				long iImage = GetLumpIndex(lptexdir->lplpwad[i], 0, -1, sTexName);
				cbImage = GetLumpLength(lptexdir->lplpwad[i], iImage);

				/* Patch not found in this wad? */
				if(cbImage <= 0) continue;

				lpdp = ProcHeapAlloc(cbImage);

				/* Load image. */
				GetLump(lptexdir->lplpwad[i], iImage, (BYTE*)lpdp, cbImage);
			}

			/* Patch not found? */
			if(!lpdp) break;

			/* Allocate memory for the texture. */
			lptex = ProcHeapAlloc(sizeof(TEXTURE));

			/* DIBs must have widths of a multiple of 4. */
			lptex->cx = ((lpdp->cx - 1) & ~3) + 4;
			lptex->cy = lpdp->cy;
			cbTextureBitmap = lptex->cx * lptex->cy;
			lptex->xOffset = lptex->yOffset = 0;

			lpbyBits = ProcHeapAlloc(cbTextureBitmap);
			lpbyBitsMask = ProcHeapAlloc(cbTextureBitmap);

			/* Clear the texture and mask bitmaps. */
			ZeroMemory(lpbyBits, cbTextureBitmap);
			FillMemory(lpbyBitsMask, cbTextureBitmap, 1);

			/* Blit! Note that the mask is trivial. */
			DoomPictureToDIB(lpbyBits, lpbyBitsMask, lpdp, cbImage, lptex->cx, lptex->cy, 0, 0);

			/* Free Doom-format image. */
			ProcHeapFree(lpdp);
		}

		/* Pointer to texture will be returned. */
		break;
	}

	if(lptex)
	{
		/* Create a DIB-section from the bits. */

		BITMAPINFO *lpbmiDI;
		BITMAPINFO *lpbmiDIBSect;
		void *lpvDIBSect;

		/* The BITMAPINFO structures require tails of n-1 palette entries. */
		lpbmiDI = ProcHeapAlloc(sizeof(BITMAPINFO) + 255 * sizeof(RGBQUAD));

		/* The extra RGBQUAD is for the monochrome mask bitmap later. */
		lpbmiDIBSect = ProcHeapAlloc(sizeof(BITMAPINFO) + sizeof(RGBQUAD));

		lpbmiDI->bmiHeader.biBitCount = 8;		/* 8bpp. */
		lpbmiDI->bmiHeader.biClrImportant = 0;	/* All are important. */
		lpbmiDI->bmiHeader.biClrUsed = 256;
		lpbmiDI->bmiHeader.biCompression = BI_RGB;
		lpbmiDI->bmiHeader.biPlanes = 1;
		lpbmiDI->bmiHeader.biSize = sizeof(BITMAPINFOHEADER);
		lpbmiDI->bmiHeader.biSizeImage = 0;
		lpbmiDI->bmiHeader.biWidth = lptex->cx;
		lpbmiDI->bmiHeader.biHeight = -lptex->cy;

		lpbmiDIBSect->bmiHeader.biBitCount = 32;		/* 32bpp. */
		lpbmiDIBSect->bmiHeader.biClrImportant = 0;	/* All are important. */
		lpbmiDIBSect->bmiHeader.biClrUsed = 0;
		lpbmiDIBSect->bmiHeader.biCompression = BI_RGB;
		lpbmiDIBSect->bmiHeader.biPlanes = 1;
		lpbmiDIBSect->bmiHeader.biSize = sizeof(BITMAPINFOHEADER);
		lpbmiDIBSect->bmiHeader.biSizeImage = 0;
		lpbmiDIBSect->bmiHeader.biWidth = lptex->cx;
		lpbmiDIBSect->bmiHeader.biHeight = lptex->cy;

		/* Copy the palette data. */
		CopyMemory(lpbmiDI->bmiColors, lprgbq, (CB_PLAYPAL / 3) * sizeof(RGBQUAD));

		/* Create bitmap for image itself. */
		lptex->hbitmap = CreateDIBSection(NULL, lpbmiDIBSect, DIB_RGB_COLORS, &lpvDIBSect, NULL, 0);

		/* Copy the data. */
		SetDIBits(NULL, lptex->hbitmap, 0, lptex->cy, lpbyBits, lpbmiDI, DIB_RGB_COLORS);

		/* Update bitmap properties for the mask. */
		lpbmiDI->bmiColors[0].rgbBlue = lpbmiDI->bmiColors[0].rgbGreen = lpbmiDI->bmiColors[0].rgbRed = 0;
		lpbmiDI->bmiColors[1].rgbBlue = lpbmiDI->bmiColors[1].rgbGreen = lpbmiDI->bmiColors[1].rgbRed = 0xFF;
		lpbmiDI->bmiHeader.biClrUsed = 2;
		lpbmiDIBSect->bmiHeader.biBitCount = 1;
		CopyMemory(lpbmiDIBSect->bmiColors, lpbmiDI->bmiColors, 2 * sizeof(RGBQUAD));

		/* Create mask bitmap. */
		lptex->hbitmapMask = CreateDIBSection(NULL, lpbmiDIBSect, DIB_RGB_COLORS, &lpvDIBSect, NULL, 0);

		/* Copy the data. */
		SetDIBits(NULL, lptex->hbitmapMask, 0, lptex->cy, lpbyBitsMask, lpbmiDI, DIB_RGB_COLORS);

		ProcHeapFree(lpbmiDIBSect);
		ProcHeapFree(lpbmiDI);
		ProcHeapFree(lpbyBits);
		ProcHeapFree(lpbyBitsMask);
	}

	return lptex;
}



/* DestroyTexture
 *   Frees a texture.
 *
 * Parameters:
 *   TEXTURE*	lptex	Texture to free.
 *
 * Return value: None.
 *
 * Remarks:
 *   The texture would normally have been allocated by LoadTexture.
 */
void DestroyTexture(TEXTURE *lptex)
{
	DeleteObject(lptex->hbitmap);
	DeleteObject(lptex->hbitmapMask);
	ProcHeapFree(lptex);
}


/* AddToTextureCache
 *   Adds a texture to a texture cache.
 *
 * Parameters:
 *   TEXCACHE*	lptcHdr		Header of texture cache.
 *   LPCTSTR		szLumpName	Name of texture.
 *   TEXTURE*	lptex		Texture to add.
 *
 * Return value: None.
 *
 * Remarks:
 *   No copying is done -- the pointer itself is used in the cache.
 */
void AddToTextureCache(TEXCACHE *lptcHdr, LPCTSTR szLumpName, TEXTURE *lptex)
{
	TEXCACHE *lptcNew = ProcHeapAlloc(sizeof(TEXCACHE));

	/* Fill data. */
	lptcNew->lptexture = lptex;

#ifdef UNICODE
	WideCharToMultiByte(CP_ACP, 0, szLumpName, -1, lptcNew->szLumpName, NUM_ELEMENTS(lptcNew->szLumpName), NULL, NULL);
#else
	strcpy(lptcNew->szLumpName, szLumpName);
#endif

	/* Add to the beginning of the list. */
	lptcNew->lptcNext = lptcHdr->lptcNext;
	lptcHdr->lptcNext = lptcNew;
}


/* EnumTextures
 *   Enumerates the names of all textures or flats in a wad.
 *
 * Parameters:
 *   WAD*			lpwad		Wad to enumerate lumps from.
 *   TEX_FORMAT		tf			Enumerate either flats or textures.
 *   BOOL (CALLBACK *)(const char*, void*)	f
 *								Callback function. The strings passed to it are
 *								not necessarily NULL-terminated.
 *   void*			lpvParam	Parameter to callback.
 *
 * Return value: None.
 */
static void EnumTextures(WAD *lpwad, TEX_FORMAT tf, void (CALLBACK *f)(const char *sTexName, void *lpvParam), void *lpvParam)
{
	switch(tf)
	{
	case TF_FLAT:
		{
			long iFStart, iFEnd, i;

			/* Find F_START and F_END. If either is missing, give up. */
			iFStart = GetLumpIndex(lpwad, 0, -1, "F_START");
			if(iFStart < 0 || (iFEnd = GetLumpIndex(lpwad, iFStart, -1, "F_END")) < 0)
				break;

			/* Loop through all the intervening lumps and call our function. */
			for(i = iFStart + 1; i < iFEnd; i++)
			{
				char sTexName[CCH_LUMPNAME];
				GetLumpNameA(lpwad, i, sTexName);
				f(sTexName, lpvParam);
			}
		}

		break;

	case TF_TEXTURE:
		{
			char sLumpName[CCH_LUMPNAME];
			int cbTexture1, cbTexture2;
			int iTexCount;
			int i;
			long iTexture1, iTexture2;

			iTexture1 = GetLumpIndex(lpwad, 0, -1, "TEXTURE1");
			iTexture2 = GetLumpIndex(lpwad, 0, -1, "TEXTURE2");
			if(iTexture1 < 0 && iTexture2 < 0) break;

			cbTexture1 = iTexture1 >= 0 ? GetLumpLength(lpwad, iTexture1) : 0;
			cbTexture2 = iTexture2 >= 0 ? GetLumpLength(lpwad, iTexture2) : 0;

			if(cbTexture1 > 0)
			{
				BYTE *lpbyTexture1 = ProcHeapAlloc(cbTexture1);

				GetLump(lpwad, iTexture1, lpbyTexture1, cbTexture1);

				iTexCount = *((int*)lpbyTexture1);

				for(i=0; i<iTexCount; i++)
				{
					/* TEXTUREn has a table of offsets into itself, after a long
					 * indicating the no. entries.
					 */
					CopyMemory(sLumpName, &lpbyTexture1[((int*)lpbyTexture1)[1 + i]], CCH_LUMPNAME);
					f(sLumpName, lpvParam);
				}

				ProcHeapFree(lpbyTexture1);
			}

			if(cbTexture2 > 0)
			{
				BYTE *lpbyTexture2 = ProcHeapAlloc(cbTexture2);

				GetLump(lpwad, iTexture2, lpbyTexture2, cbTexture2);

				iTexCount = *((int*)lpbyTexture2);

				for(i=0; i<iTexCount; i++)
				{
					/* TEXTUREn has a table of offsets into itself, after a long
					 * indicating the no. entries.
					 */
					CopyMemory(sLumpName, &lpbyTexture2[((int*)lpbyTexture2)[1 + i]], CCH_LUMPNAME);
					f(sLumpName, lpvParam);
				}

				ProcHeapFree(lpbyTexture2);
			}
		}

		break;


	default:
        break;
	}
}


/* GetTextureFromCache
 *   Searches a cache for a particular texture.
 *
 * Parameters:
 *   TEXCACHE*	lptcHdr		Cache header node.
 *   LPCTSTR	szTexName	Name of texture to search for.
 *
 * Return value: TEXTURE*
 *   Pointer to texture, or NULL if not found.
 *
 * Remarks:
 *   The cache texture itself is returned, so the caller must not destroy it.
 *   This represents a disparity with LoadTexture, which can sometimes make the
 *   calling code messy (only freeing if it wasn't in the cache), but the
 *   inefficiency of copying the texture isn't worthwhile.
 */
TEXTURE* GetTextureFromCache(TEXCACHE *lptcHdr, LPCTSTR szTexName)
{
	TEXCACHE *lptcRover = lptcHdr;
	TEXCACHE *lptcPrev = lptcHdr;

#ifdef UNICODE
	char szTexNameA[TEXNAME_BUFFER_LENGTH];
	WideCharToMultiByte(CP_ACP, 0, szTexName, -1, szTexNameA, NUM_ELEMENTS(szTexNameA), NULL, NULL);
#else
	LPCSTR szTexNameA = szTexName;
#endif

	while((lptcRover = lptcRover->lptcNext))
	{
		if(strcmp(lptcRover->szLumpName, szTexNameA) == 0)
		{
			/* Move match to front of list. */
			lptcPrev->lptcNext = lptcRover->lptcNext;
			lptcRover->lptcNext = lptcHdr->lptcNext;
			lptcHdr->lptcNext = lptcRover;

			return lptcRover->lptexture;
		}

		lptcPrev = lptcPrev->lptcNext;
	}

	return NULL;
}



/* PurgeTextureCache
 *   Frees all memory used by a texture cache.
 *
 * Parameters:
 *   TEXCACHE*	lptcHdr		Cache header node.
 *
 * Return value: None.
 */
void PurgeTextureCache(TEXCACHE *lptcHdr)
{
	/* Skip header. */
	TEXCACHE *lptcNext = lptcHdr->lptcNext;

	/* The caller expects this. */
	lptcHdr->lptcNext = NULL;

	while((lptcHdr = lptcNext))
	{
		lptcNext = lptcHdr->lptcNext;
		DestroyTexture(lptcHdr->lptexture);
		ProcHeapFree(lptcHdr);
	}
}



/* DoomPictureToDIB
 *   Converts a column-major Doom picture into a top-down DIB.
 *
 * Parameters:
 *   BYTE*			lpbyDIB				Buffer to store DIB in.
 *   BYTE*			lpbyDIBMask			Buffer to store mask in. May be NULL.
 *										Bytes are set to 0 here if they're
 *										altered in the bitmap itself; otherwise
 *										they're untouched.
 *   DOOMPICTURE*	*lpdp				Doom picture structure.
 *   int			cbImage				Length of buffer pointed to by lpdp.
 *   short			cxDIB, cyDIB		Dimensions of buffer.
 *   short			xOffset, yOffset	Offset within DIB at which to blit.
 *
 * Return value: None.
 */
static void DoomPictureToDIB(BYTE *lpbyDIB, BYTE *lpbyDIBMask, DOOMPICTURE_HEADER *lpdp, int cbImage, short cxDIB, short cyDIB, short xOffset, short yOffset)
{
	/* Part of offset into the data common to the whole patch. */
	int iPatchOffset = yOffset * cxDIB + xOffset;
	int cbTextureBitmap = cxDIB * cyDIB;
	int i;

	/* Blit patch to texture. Loop for each column. */
	int iColLimit = min(lpdp->cx, cxDIB - xOffset);

	for(i = max(0, -xOffset); i < iColLimit; i++)
	{
		/* Part of offset into the data common to this column. */
		int iColOffset = iPatchOffset + i;

		/* Find this column's first post. */
		DOOMPICTURE_POST *lppost = (DOOMPICTURE_POST*)&((BYTE*)lpdp)[lpdp->iDataOffsets[i]];

		/* Loop until we get to the column-end magic number. */
		while(lppost->byRowStart != COLUMN_END)
		{
			int iPostOffset = iColOffset + lppost->byRowStart * cxDIB;
			int j, iRowLimit;

			/* Clipping. */
			iRowLimit = min(lppost->byNumPixels, cyDIB - yOffset);

			/* Don't go past the end of the source image. This is an exceptional
			 * case.
			 */
			iRowLimit = min(iRowLimit, cbImage - lpdp->iDataOffsets[i]);

			for(j = max(0, -yOffset); j < iRowLimit; j++)
			{
				/* Skies are very strange. They seem to have another post
				 * following the normal one, which purports to have full height,
				 * but only actually lasts a byte before we see what looks like
				 * a COLUMN_END marker (even though it's in the middle of a
				 * post) whereafter you can read synchronously again. The offset
				 * directory also resyncs there. So, as a workaround, just check
				 * some buffer boundaries.
				 */
				if(iPostOffset + j * cxDIB >= cbTextureBitmap) continue;

				lpbyDIB[iPostOffset + j * cxDIB] = lppost->byPixels[j];
				if(lpbyDIBMask) lpbyDIBMask[iPostOffset + j * cxDIB] = 0;
			}

			/* Move to next post. Skip the unused byte at end. */
			lppost = (DOOMPICTURE_POST*)&lppost->byPixels[lppost->byNumPixels + 1];
		}
	}
}


/* CreateTextureNameList
 *   Creates a new texture name list structure.
 *
 * Parameters:
 *   None.
 *
 * Return value: TEXTURENAMELIST*
 *   Pointer to new structure.
 *
 * Remarks:
 *   The caller should call DestroyTextureNameList to free the memory.
 */
TEXTURENAMELIST* CreateTextureNameList(void)
{
	TEXTURENAMELIST *lptnl = ProcHeapAlloc(sizeof(TEXTURENAMELIST));

	lptnl->iEntries = 0;
	lptnl->cstrBuffer = TNL_BUFSIZEINCREMENT;
	lptnl->lpszTexNames = ProcHeapAlloc(lptnl->cstrBuffer * sizeof(lptnl->lpszTexNames[0]));

	return lptnl;
}


/* DestroyTextureNameList
 *   Destroys a texture name list structure.
 *
 * Parameters:
 *   TEXTURENAMELIST*	lptnl	List to destroy.
 *
 * Return value: None.
 */
void DestroyTextureNameList(TEXTURENAMELIST *lptnl)
{
	ProcHeapFree(lptnl->lpszTexNames);
	ProcHeapFree(lptnl);
}


/* AddTextureNameToList
 *   Adds a texture name to a texture name list.
 *
 * Parameters:
 *   const char*	sTexName		Name of texture. Not NULL-terminated.
 *   void*			lpvParam		(TEXTURENAMELIST*) List to add to.
 *
 * Return value: None.
 *
 * Remarks:
 *   The list is extended if necessary. This is intended to be used as a
 *   callback by EnumTextures.
 */
static void CALLBACK AddTextureNameToList(const char *sTexName, void *lpvParam)
{
	TEXTURENAMELIST *lptnl = (TEXTURENAMELIST*)lpvParam;
	CHAR szTexNameA[TEXNAME_BUFFER_LENGTH];
	TCHAR szTexNameT[TEXNAME_BUFFER_LENGTH];

	if(lptnl->iEntries == lptnl->cstrBuffer)
	{
		/* No more room in buffer; make it bigger. */
		lptnl->cstrBuffer += TNL_BUFSIZEINCREMENT;
		lptnl->lpszTexNames = ProcHeapReAlloc(lptnl->lpszTexNames, lptnl->cstrBuffer * sizeof(lptnl->lpszTexNames[0]));
	}

	/* Terminate the string. */
	strncpy(szTexNameA, sTexName, TEXNAME_WAD_BUFFER_LENGTH);
	szTexNameA[TEXNAME_WAD_BUFFER_LENGTH] = '\0';

	/* Convert string from ANSI if necessary. */
	wsprintf(szTexNameT, TEXT("%hs"), szTexNameA);

	/* Add to list. */
	_tcscpy(lptnl->lpszTexNames[lptnl->iEntries++], szTexNameT);
}


/* AddAllTextureNamesToList
 *   Adds all texture names from a wad to a texture name list.
 *
 * Parameters:
 *   TEXTURENAMELIST*	lptnl	List to add to.
 *   WAD*				lpwad	Wad from which to add textures.
 *   TEX_FORMAT			tf		Flats or textures?
 *
 * Return value: None.
 */
void AddAllTextureNamesToList(TEXTURENAMELIST *lptnl, WAD *lpwad, TEX_FORMAT tf)
{
	EnumTextures(lpwad, tf, AddTextureNameToList, lptnl);
}


/* SortTextureNameList, CompareTextureNames
 *   Sorts a list of texture names.
 *
 * Parameters:
 *   TEXTURENAMELIST*	lptnl	List to sort.
 *
 * Return value: None.
 */
void SortTextureNameList(TEXTURENAMELIST *lptnl)
{
	qsort(lptnl->lpszTexNames, lptnl->iEntries, sizeof(lptnl->lpszTexNames[0]), (int (*)(const void*, const void*))CompareTextureNames);
}

static int CompareTextureNames(LPCTSTR sz1, LPCTSTR sz2)
{
	return _tcscmp(sz1, sz2);
}

LPCTSTR FindFirstTexNameMatch(LPCTSTR szTexName, TEXTURENAMELIST *lptnl)
{
	unsigned int iEntries = lptnl->iEntries;
	void *ret = _lfind(szTexName, lptnl->lpszTexNames, &iEntries, sizeof(lptnl->lpszTexNames[0]), (int (*)(const void*, const void*))CompareTextureNameHeads);
	lptnl->iEntries = iEntries;
	return ret;
}

static int CompareTextureNameHeads(LPCTSTR sz1, LPCTSTR sz2)
{
	int iLen = min(_tcslen(sz1), _tcslen(sz2));
	return _tcsncmp(sz1, sz2, iLen);
}


/* StretchTextureToDC
 *   Blits a texture to a DC, shrinking it if necessary.
 *
 * Parameters:
 *   TEXTURE*				lptex		Texture to blit.
 *   HDC					hdcTarget	Target device context.
 *   int					cx, cy		Dimensions of destination.
 *   ENUM_STRETCHTEX_TYPES	stt			Whether to draw texture itself, or mask.
 *
 * Return value: None.
 */
void StretchTextureToDC(TEXTURE *lptex, HDC hdcTarget, int cx, int cy, ENUM_STRETCHTEX_TYPES stt)
{
	int xStretch, yStretch, cxStretch, cyStretch;
	HDC hdcSource = CreateCompatibleDC(NULL);
	HBITMAP hbmOld = NULL;

	/* Clear first. For the mask, use white (transparent); otherwise use grey.
	 */
	RECT rc = {0, 0, CX_TEXPREVIEW, CY_TEXPREVIEW};
	FillRect(hdcTarget, &rc, GetStockObject((stt == STT_MASKONLY) ? WHITE_BRUSH : LTGRAY_BRUSH));

	/* Keep the DC around for later. */

	if(lptex->cx <= cx && lptex->cy <= cy)
	{
		/* Texture fits within preview control */

		xStretch = (cx - lptex->cx) >> 1;
		yStretch = (cy - lptex->cy) >> 1;
		cxStretch = lptex->cx;
		cyStretch = lptex->cy;
	}
	else
	{
		/* Preview control and texture are in different ratios.
		 * Centre the latter in the former.
		 */

		if(lptex->cx * cy > lptex->cy * cx)
		{
			/* Texture is wider than it is high, with respect to the ratio of
			 * the preview control.
			 */

			cxStretch = cx;
			cyStretch = (lptex->cy * cy) / lptex->cx;
			xStretch = 0;
			yStretch = (cy - cyStretch) >> 1;
		}
		else
		{
			/* Texture is at least as high as it is wide, with respect to the
			 * ratio of the preview control.
			 */

			cyStretch = cy;
			cxStretch = (lptex->cx * cx) / lptex->cy;
			yStretch = 0;
			xStretch = (cx - cxStretch) >> 1;
		}
	}

	/* Just lose the intermediate pixels. */
	SetStretchBltMode(hdcTarget, COLORONCOLOR);

	/* If we're drawing the image masked, we need to construct an intermediate
	 * version of the texture, since we can't mask and stretch in one go.
	 */
	switch(stt)
	{
	case STT_MASKED:
		{
			HDC hdcIntermediate = CreateCompatibleDC(hdcTarget);
			HBITMAP hbmIntOld;
			HBITMAP hbmIntermediate = CreateCompatibleBitmap(hdcTarget, lptex->cx, lptex->cy);

			hbmIntOld = SelectObject(hdcIntermediate, hbmIntermediate);

			/* Make out intermediate bitmap look like the output bitmap's
			 * background.
			 */
			rc.right = lptex->cx;
			rc.bottom = lptex->cy;
			FillRect(hdcIntermediate, &rc, GetStockObject(LTGRAY_BRUSH));

			/* Mask-blit the texture onto the intermediate bitmap, so we end up with
			 * something that looks transparent for when we stretch later.
			 */

			/* First, we fill the area we want the texture to stick to with
			 * white.
			 */
			hbmOld = SelectObject(hdcSource, lptex->hbitmapMask);
			StretchBlt(	hdcTarget,
						xStretch, yStretch, cxStretch, cyStretch,
						hdcSource, 0, 0, lptex->cx, lptex->cy,
						MERGEPAINT);

			/* Then, create an intermediate bitmap consisting of the source with
			 * its masked-out pixels white.
			 */
			BitBlt(hdcIntermediate, 0, 0, lptex->cx, lptex->cy, hdcSource, 0, 0, SRCCOPY);
			SelectObject(hdcSource, lptex->hbitmap);
			BitBlt(hdcIntermediate, 0, 0, lptex->cx, lptex->cy, hdcSource, 0, 0, SRCPAINT);

			/* Finally, AND the intermediate bitmap onto the target. */
			StretchBlt(	hdcTarget,
						xStretch, yStretch, cxStretch, cyStretch,
						hdcIntermediate, 0, 0, lptex->cx, lptex->cy,
						SRCAND);

			/* And we're done! Interesting question: is there a better way?
			 * Other masking implementations I had a look at used DSna, which we
			 * didn't seem to need here.
			 */

			SelectObject(hdcIntermediate, hbmIntOld);
			DeleteObject(hbmIntermediate);
			DeleteDC(hdcIntermediate);
		}

		break;

	case STT_TEXTURE:
	case STT_MASKONLY:

		/* Blit! We already have a DC to blit from. */
		hbmOld = SelectObject(hdcSource, (stt == STT_MASKONLY) ? lptex->hbitmapMask : lptex->hbitmap);
		StretchBlt(	hdcTarget,
					xStretch, yStretch, cxStretch, cyStretch,
					hdcSource, 0, 0, lptex->cx, lptex->cy,
					SRCCOPY);

		break;
	}

	SelectObject(hdcSource, hbmOld);
	DeleteDC(hdcSource);
}


/* IsPseudoTexture, IsPseudoFlat, IsBlankTexture
 *   Determines whether a texture that does not actually exist should
 *   nonetheless not be reported as missing, or whether it corresponds to the
 *   blank texture.
 *
 * Parameters:
 *   LPCTSTR	sTexName	Name of texture. Need not be NUL-terminated.
 *
 * Return value: BOOL
 *   TRUE if pseudo-texture; FALSE if not.
 *
 * Remarks:
 *   Pseudo-textures are useful for things like F_SKY1 and colourmaps. The blank
 *   texture is the one that's normally "-".
 */

#ifdef _UNICODE
BOOL IsPseudoTextureW(LPCWSTR sTexName)
{
	WCHAR szTexNameW[TEXNAME_BUFFER_LENGTH];
	CHAR szTexNameA[TEXNAME_BUFFER_LENGTH];

	/* First make a NUL-terminated Unicode string. */
	wcsncpy(szTexNameW, sTexName, TEXNAME_WAD_BUFFER_LENGTH);

	/* Make it ANSI. */
	WideCharToMultiByte(CP_ACP, 0, szTexNameW, -1, szTexNameA, sizeof(szTexNameA), NULL, NULL);

	return IsPseudoTextureA(szTexNameA);
}
#endif

BOOL IsPseudoTextureA(LPCSTR sTexName)
{
	UNREFERENCED_PARAMETER(sTexName);
	/* TODO: Check for pseudo-textures from config. */
	return FALSE;
}

#ifdef _UNICODE
BOOL IsPseudoFlatW(LPCWSTR sTexName)
{
	WCHAR szTexNameW[TEXNAME_BUFFER_LENGTH];
	CHAR szTexNameA[TEXNAME_BUFFER_LENGTH];

	/* First make a NUL-terminated Unicode string. */
	wcsncpy(szTexNameW, sTexName, TEXNAME_WAD_BUFFER_LENGTH);

	/* Make it ANSI. */
	WideCharToMultiByte(CP_ACP, 0, szTexNameW, -1, szTexNameA, sizeof(szTexNameA), NULL, NULL);

	return IsPseudoFlatA(szTexNameA);
}
#endif

BOOL IsPseudoFlatA(LPCSTR sTexName)
{
	UNREFERENCED_PARAMETER(sTexName);
	/* TODO: Check for pseudo-flats from config. */
	return FALSE;
}

#ifdef _UNICODE
BOOL IsBlankTextureW(LPCWSTR sTexName)
{
	WCHAR szTexNameW[TEXNAME_BUFFER_LENGTH];
	CHAR szTexNameA[TEXNAME_BUFFER_LENGTH];

	/* First make a NUL-terminated Unicode string. */
	wcsncpy(szTexNameW, sTexName, TEXNAME_WAD_BUFFER_LENGTH);

	/* Make it ANSI. */
	WideCharToMultiByte(CP_ACP, 0, szTexNameW, -1, szTexNameA, sizeof(szTexNameA), NULL, NULL);

	return IsBlankTextureA(szTexNameA);
}
#endif

BOOL IsBlankTextureA(LPCSTR sTexName)
{
	if(strncmp(sTexName, BLANK_TEXTURE, TEXNAME_WAD_BUFFER_LENGTH) == 0) return TRUE;

	/* TODO: Get blank texture string from config. */

	return FALSE;
}


/* IsBonaFideTexture
 *   Determines whether a texture is a real texture.
 *
 * Parameters:
 *   LPCTSTR	sTexName	Name of texture. Need not be terminated.
 *
 * Return value: BOOL
 *   TRUE if a real texture; FALSE if not.
 */
BOOL IsBonaFideTexture(LPCTSTR sTexName)
{
	return *sTexName && !IsPseudoTexture(sTexName) && !IsBlankTexture(sTexName);
}

BOOL IsBonaFideTextureA(LPCSTR sTexName)
{
	return *sTexName && !IsPseudoTextureA(sTexName) && !IsBlankTextureA(sTexName);
}


/* CreateTextureDirectory
 *   Creates a structures from an arbitrary number of wads specifying all
 *   textures contained therein, which can then be used to load textures.
 *
 * Parameters:
 *   WAD**		lplpwad		Wads, in order of precedence.
 *   int		iNumWads	Number of wads.
 *
 * Return value: TEXTURE_DIRECTORY*
 *   Pointer to new structure.
 *
 * Remarks:
 *   Call DestroyTextureDirectory to free the returned structure.
 */
TEXTURE_DIRECTORY* CreateTextureDirectory(WAD **lplpwad, int iNumWads)
{
	TEXTURE_DIRECTORY *lptexdir = (TEXTURE_DIRECTORY*)ProcHeapAlloc(sizeof(TEXTURE_DIRECTORY));
	int i;

	/* Begin by making a copy of the wad array, since this is also the array
	 * used for finding patches, sprites and flats.
	 */
	lptexdir->lplpwad = (WAD**)ProcHeapAlloc(iNumWads * sizeof(WAD*));
	CopyMemory(lptexdir->lplpwad, lplpwad, iNumWads * sizeof(WAD*));
	lptexdir->iNumWads = iNumWads;

	lptexdir->lptexnTexture1 = lptexdir->lptexnTexture2 = NULL;
	lptexdir->lppnames = NULL;

	/* Texture name lists. We'll fill these later. */
	lptexdir->lptnlTextures = CreateTextureNameList();
	lptexdir->lptnlFlats = CreateTextureNameList();

	/* Go through the wads in turn, looking for each sort of structure if we
	 * haven't found it in an earlier wad.
	 */
	for(i = 0; i < iNumWads; i++)
	{
		int j;
		long iFStart, iFEnd;

		/* Descriptions of the lumps we're interested in and where to store them
		 * if we find them.
		 */
		struct
		{
			char	szLumpname[CCH_LUMPNAME + 1];
			LPVOID	*lplpvBuffer;
		} requiredlumps[] = {{"PNAMES", (LPVOID*)(LPVOID)&lptexdir->lppnames}, {"TEXTURE1", (LPVOID*)(LPVOID)&lptexdir->lptexnTexture1}, {"TEXTURE2", (LPVOID*)(LPVOID)&lptexdir->lptexnTexture2}};

		/* Repeat for each required lump. */
		for(j = 0; j < (int)NUM_ELEMENTS(requiredlumps); j++)
		{
			/* If we haven't found this lump already: */
			if(!*requiredlumps[j].lplpvBuffer)
			{
				long cb;
				long iIndex = GetLumpIndex(lplpwad[i], 0, -1, requiredlumps[j].szLumpname);

				if(iIndex >= 0 && (cb = GetLumpLength(lplpwad[i], iIndex)) > 0)
				{
					*requiredlumps[j].lplpvBuffer = ProcHeapAlloc(cb);
					GetLump(lplpwad[i], iIndex, *requiredlumps[j].lplpvBuffer, cb);
				}
			}
		}

		/* Add all flats from this wad to the list of flat names. */
		iFStart = GetLumpIndex(lplpwad[i], 0, -1, "F_START");
		iFEnd = GetLumpIndex(lplpwad[i], 0, -1, "F_END");

		if(iFStart >= 0 && iFEnd >= 0)
		{
			for(j = iFStart + 1; j < iFEnd; j++)
			{
				char szFlatName[TEXNAME_BUFFER_LENGTH];

				GetLumpNameA(lplpwad[i], j, szFlatName);
				AddTextureNameToList(szFlatName, lptexdir->lptnlFlats);
			}
		}
	}

	/* Add all textures to the list of names. */
	for(i = 0; i < 2; i++)
	{
		TEXTUREN *lptexn = (i == 0) ? lptexdir->lptexnTexture1 : lptexdir->lptexnTexture2;

		if(lptexn)
		{
			int j;

			for(j = 0; j < lptexn->iNumTextures; j++)
				AddTextureNameToList(TextureEntryFromTextureN(lptexn, j)->sTexName, lptexdir->lptnlTextures);
		}
	}

	/* Sort the lists now that they're populated. */
	SortTextureNameList(lptexdir->lptnlFlats);
	SortTextureNameList(lptexdir->lptnlTextures);

	return lptexdir;
}


/* DestroyTextureDirectory
 *   Frees a TEXTURE_DIRECTORY structure allocated by CreateTextureDirectory.
 *
 * Parameters:
 *   TEXTURE_DIRECTORY*		lptexdir	Structure to free.
 *
 * Return value: None.
 */
void DestroyTextureDirectory(TEXTURE_DIRECTORY *lptexdir)
{
	ProcHeapFree(lptexdir->lppnames);
	ProcHeapFree(lptexdir->lptexnTexture1);
	ProcHeapFree(lptexdir->lptexnTexture2);
	ProcHeapFree(lptexdir->lplpwad);
	DestroyTextureNameList(lptexdir->lptnlFlats);
	DestroyTextureNameList(lptexdir->lptnlTextures);

	ProcHeapFree(lptexdir);
}


/* TextureEntryFromTextureN
 *   Retrieves an entry in a TEXTUREN lump.
 *
 * Parameters:
 *   TEXTUREN*	lptexn		TEXTUREN lump.
 *   int		iTexIndex	Index of desired texture.
 *
 * Return value: TEXTURE_ENTRY*
 *   Pointer into lptexn specifying the requested texture.
 */
static __inline TEXTURE_ENTRY* TextureEntryFromTextureN(TEXTUREN *lptexn, int iTexIndex)
{
	return (TEXTURE_ENTRY*)((LPBYTE)lptexn + lptexn->iOffsets[iTexIndex]);
}


/* MakeSolidTexture
 *   Creates a preview-sized texture consisting entirely of a given colour.
 *
 * Parameters:
 *   COLORREF	Colour.
 *
 * Return value: TEXTURE*
 *   Pointer to new texture, which the caller should free in the usual way.
 */
TEXTURE* MakeSolidTexture(COLORREF cref)
{
	TEXTURE *lptex = ProcHeapAlloc(sizeof(TEXTURE));
	LPBITMAPINFO lpbmiDIBSect;
	void *lpvDIBSect;
	HDC hdc;
	HBITMAP hbmOld;
	RECT rc;
	HBRUSH hbrush;

	/* Set up the texture. */
	lptex->cx = CX_TEXPREVIEW;
	lptex->cy = CY_TEXPREVIEW;
	lptex->xOffset = lptex->yOffset = 0;

	/* Create a DIB-section. */

	/* The extra RGBQUAD is for the monochrome mask bitmap later. */
	lpbmiDIBSect = ProcHeapAlloc(sizeof(BITMAPINFO) + sizeof(RGBQUAD));

	lpbmiDIBSect->bmiHeader.biBitCount = 32;		/* 32bpp. */
	lpbmiDIBSect->bmiHeader.biClrImportant = 0;	/* All are important. */
	lpbmiDIBSect->bmiHeader.biClrUsed = 0;
	lpbmiDIBSect->bmiHeader.biCompression = BI_RGB;
	lpbmiDIBSect->bmiHeader.biPlanes = 1;
	lpbmiDIBSect->bmiHeader.biSize = sizeof(BITMAPINFOHEADER);
	lpbmiDIBSect->bmiHeader.biSizeImage = 0;
	lpbmiDIBSect->bmiHeader.biWidth = lptex->cx;
	lpbmiDIBSect->bmiHeader.biHeight = lptex->cy;

	/* Create the bitmap. */
	lptex->hbitmap = CreateDIBSection(NULL, lpbmiDIBSect, DIB_RGB_COLORS, &lpvDIBSect, NULL, 0);

	/* Set the pixels to our colour. */

	hdc = CreateCompatibleDC(NULL);
	hbmOld = SelectObject(hdc, lptex->hbitmap);

	rc.left = rc.top = 0;
	rc.right = CX_TEXPREVIEW;
	rc.bottom = CY_TEXPREVIEW;

	hbrush = CreateSolidBrush(cref);
	FillRect(hdc, &rc, hbrush);
	DeleteObject(hbrush);

	/* Update bitmap properties for the mask. */
	lpbmiDIBSect->bmiColors[0].rgbBlue = lpbmiDIBSect->bmiColors[0].rgbGreen = lpbmiDIBSect->bmiColors[0].rgbRed = 0;
	lpbmiDIBSect->bmiColors[1].rgbBlue = lpbmiDIBSect->bmiColors[1].rgbGreen = lpbmiDIBSect->bmiColors[1].rgbRed = 0xFF;
	lpbmiDIBSect->bmiHeader.biBitCount = 1;

	/* Create mask bitmap. */
	lptex->hbitmapMask = CreateDIBSection(NULL, lpbmiDIBSect, DIB_RGB_COLORS, &lpvDIBSect, NULL, 0);

	/* Mask is entirely black. */
	SelectObject(hdc, lptex->hbitmapMask);
	FillRect(hdc, &rc, GetStockObject(BLACK_BRUSH));

	SelectObject(hdc, hbmOld);
	DeleteDC(hdc);

	ProcHeapFree(lpbmiDIBSect);

	return lptex;
}
