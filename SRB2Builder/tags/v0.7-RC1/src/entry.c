/* SRB2 Workbench -- A map editor for Sonic Robo Blast 2.
 * Copyright (C) 2009 Gregor Dick
 * http://workbench.srb2.org/
 *
 * Incorporates portions of Doom Builder, (C) 2003 Pascal vd Heiden.
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 * USA.
 *
 * entry.c: Entry point and associated initialisation.
 *
 * AMDG.
 */

#include <windows.h>
#include <commctrl.h>
#include <string.h>
#include <tchar.h>

#include "general.h"
#include "../res/resource.h"
#include "renderer.h"
#include "openwads.h"
#include "options.h"
#include "keyboard.h"
#include "mapconfig.h"
#include "prefab.h"
#include "preset.h"

#include "win/mdiframe.h"
#include "win/mapwin.h"
#include "win/gendlg.h"

#include "DockWnd/DockWnd.h"


/* Sufficient extra space should be allocated at the end of the structure. */
typedef struct _CMDLINE
{
	DWORD dwFlags;

	LPTSTR szConfDir;

	UINT uiNumFiles;
	LPTSTR lpszFilenames[1];
} CMDLINE;

typedef enum _ENUM_PARAMEXPECT
{
	PEX_DEFAULT,
	PEX_CONFDIR,
	PEX_BINARYPATH
} ENUM_PARAMEXPECT;

enum ENUM_CMDLINE_FLAGS
{
	CLF_CONFDIR = 1
};

/* How many filenames to make room for in CMDLINE initially? */
#define INTITIAL_BUFFER_FILENAMES 4

static CMDLINE* ParseCommandLine(void);
static void FreeCmdLine(CMDLINE *lpcl);


#ifdef BUGTRAP

#include "../BugTrap/BugTrap.h"
#include "../BugTrap/BugTrap_FnTypes.h"

static HMODULE g_hmodBugTrap;

static void InstallExceptionHandler(void);
static void RemoveExceptionHandler(void);

#endif /* defined(BUGTRAP) */


/* WinMain
 *   Entry point.
 *
 * Parameters:
 *   HINSTANCE	hInstance			Instance handle for application.
 *   HINSTANCE  hPrevInstance		Zero.
 *   LPSTR		szCmdLine			String of all arguments passed on command
 *									line.
 *   int		iCmdShow			Constant determining initial window state.
 *
 * Return value: int
 *   Exit code.
 */

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR szCmdLineA, int iCmdShow)
{
	MSG msg;
	OSVERSIONINFO ovi;
	INITCOMMONCONTROLSEX iccx;
	CMDLINE *lpcl;

	UNREFERENCED_PARAMETER(hPrevInstance);
	UNREFERENCED_PARAMETER(szCmdLineA);

	/* Initialise globals. */
	g_hInstance = hInstance;
	g_hProcHeap = GetProcessHeap();

#ifdef BUGTRAP
	/* Install the exception handler. */
	InstallExceptionHandler();
#endif

	/* Check OS version. */
	ovi.dwOSVersionInfoSize = sizeof(ovi);
	GetVersionEx(&ovi);
	g_bWinNT = (ovi.dwPlatformId == VER_PLATFORM_WIN32_NT);

	/* XP and on required for Common Controls 6. */
	g_bHasCC6 = g_bWinNT && (ovi.dwMajorVersion > 5 || (ovi.dwMajorVersion == 5 && ovi.dwMinorVersion >= 1));


	/* Fire up COM. */
	if(FAILED(CoInitialize(NULL)))
	{
		Die(IDS_ERROR_INIT);
		return 1;
	}


	/* Do the necessary initialisation for the keyboard routines. */
	InitialiseKeyboard();


	/* Parse command line. This returns a structure detailing any files that
	 * need to be opened, since we can't do that yet. We need to free it later.
	 */
	lpcl = ParseCommandLine();

	/* Set config directory to the one specified on the command-line, or to the
	 * default if none was given.
	 */
	SetMapConfigDirectory((lpcl->dwFlags & CLF_CONFDIR) ? lpcl->szConfDir : NULL);


	/* Prepare some internal structures. */
	InitOpenWadsList();

	/* Register window classes. */
	RegisterMapWindowClass();

	/* Load the config file. */
	if(!LoadMainConfigurationFile())
	{
		/* Failed to open config file? */
		Die(IDS_ERROR_LOADCONFIG);
		return 1;
	}

	/* Load map configurations. */
	if(LoadMapConfigs() <= 0)
	{
		/* Failed to open config file? */
		Die(IDS_ERROR_LOADMAPCONFIGS);
		return 2;
	}

	/* Register our custom clipboard data types. */
	RegisterPrefabClipboardFormat();
	RegisterPresetClipboardFormat();

	/* Create the palette we use for drawing maps. */
	InitialiseRenderer();

	/* Initialise the Common Controls. */
	iccx.dwSize = sizeof(iccx);
	iccx.dwICC = ICC_BAR_CLASSES;
	InitCommonControlsEx(&iccx);

	/* Set up the docking library. */
	DockingInitialize(hInstance);

	/* Try to create the main window. */
	if(CreateMainWindow(iCmdShow) != 0) Die(IDS_CREATEWINDOW);

	/* Open files specified on the command line. */
	if(lpcl->uiNumFiles > 0)
	{
		UINT ui;
		for(ui = 0; ui < lpcl->uiNumFiles; ui++)
			OpenWadForEditing(lpcl->lpszFilenames[ui]);
	}

	FreeCmdLine(lpcl);

	/* Message loop. Exits on a WM_QUIT. */
	while(GetMessage(&msg, NULL, 0, 0))
	{
		if(!(IsWindow(g_hwndLastModelessDialogue) && IsDialogMessage(g_hwndLastModelessDialogue, &msg)) &&
			!TranslateBespokeAccelerator(&msg) &&
			!TranslateMDISysAccel(g_hwndClient, &msg))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
	}

	/* Begin cleaning up. */
	DockingUnInitialize();
	ShutdownRenderer();
	UnloadMapConfigs();
	UnloadMainConfigurationFile();

	FreeMapConfigDirectoryName();

	CoUninitialize();

#ifdef BUGTRAP
	/* Remove the exception handler and unload the library. */
	RemoveExceptionHandler();
#endif

	/* Exit code. */
	return msg.wParam;
}


#ifdef BUGTRAP

/* InstallExceptionHandler
 *   Sets up the BugTrap exception handler.
 *
 * Parameters:
 *   None.
 *
 * Return value: None.
 */
static void InstallExceptionHandler(void)
{
	BT_SETSUPPORTEMAIL lpfnBT_SetSupportEMail;
	BT_SETFLAGS lpfnBT_SetFlags;
	//BT_SETSUPPORTURL lpfnBT_SetSupportURL;
	//BT_READVERSIONINFO lpfnBT_ReadVersionInfo;
	BT_SETAPPNAME lpfnBT_SetAppName;
	BT_SETAPPVERSION lpfnBT_SetAppVersion;

	/* Loading the library installs the exception handler. */
#ifdef _UNICODE
	g_hmodBugTrap = LoadLibrary(L"BugTrapU.dll");
#else
	g_hmodBugTrap = LoadLibrary("BugTrap.dll");
#endif

	/* Get the functions. */
	lpfnBT_SetSupportEMail = (BT_SETSUPPORTEMAIL)GetProcAddress(g_hmodBugTrap, "BT_SetSupportEMail");
	lpfnBT_SetFlags = (BT_SETFLAGS)GetProcAddress(g_hmodBugTrap, "BT_SetFlags");
	//lpfnBT_SetSupportURL = (BT_SETSUPPORTURL)GetProcAddress(g_hmodBugTrap, "BT_SetSupportURL");
	//lpfnBT_ReadVersionInfo = (BT_READVERSIONINFO)GetProcAddress(g_hmodBugTrap, "BT_ReadVersionInfo");
	lpfnBT_SetAppName = (BT_SETAPPNAME)GetProcAddress(g_hmodBugTrap, "BT_SetAppName");
	lpfnBT_SetAppVersion = (BT_SETAPPVERSION)GetProcAddress(g_hmodBugTrap, "BT_SetAppVersion");

	if(g_hmodBugTrap)
	{
		lpfnBT_SetAppName(g_szAppName);
		lpfnBT_SetAppVersion(TEXT(__DATE__) TEXT(" ") TEXT(__TIME__));
		//lpfnBT_SetSupportEMail(TEXT("oogaland@gm" /* No spam, please. */ "ail.com"));
		lpfnBT_SetFlags(BTF_DETAILEDMODE | BTF_EDITMAIL | BTF_ATTACHREPORT);

		lpfnBT_SetSupportURL(TEXT("http://workbench.srb2.org/"));
		lpfnBT_ReadVersionInfo(NULL);
	}
}

/* RemoveExceptionHandler
 *   Removes the BugTrap exception handler.
 *
 * Parameters:
 *   None.
 *
 * Return value: None.
 */
static void RemoveExceptionHandler(void)
{
	if(g_hmodBugTrap) FreeLibrary(g_hmodBugTrap);
}

#endif


/* ParseCommandLine
 *   Parses the command line, returning details of the specified options.
 *
 * Parameters:
 *   None.
 *
 * Return value: CMDLINE*
 *   Structure describing the specified options.
 *
 * Remarks:
 *   Call FreeCmdLine on the returned pointer to free the memory.
 */
static CMDLINE* ParseCommandLine(void)
{
	/* Don't know whether we're allowed to modify this string, so make a copy of
	 * it.
	 */
	LPCTSTR szCmdLineOrig = GetCommandLine();
	LPTSTR szCmdLine = ProcHeapAlloc((_tcslen(szCmdLineOrig) + 1) * sizeof(TCHAR));
	LPTSTR szInCmdLine = szCmdLine;
	LPTSTR szInInCmdLine;
	ENUM_PARAMEXPECT pex = PEX_BINARYPATH;
	UINT uiBufferFilenames = INTITIAL_BUFFER_FILENAMES;
	CMDLINE *lpcl = ProcHeapAlloc(sizeof(CMDLINE) + (uiBufferFilenames - 1) * sizeof(LPTSTR));
	BOOL bLastToken = FALSE;

	/* Copy the command line. */
	_tcscpy(szCmdLine, szCmdLineOrig);

	/* No files specified to start with. */
	lpcl->uiNumFiles = 0;

	/* Zero flags. These'll be set as options are specified. */
	lpcl->dwFlags = 0;

	do
	{
		/* Find the next token. */
		while(*szInCmdLine == TEXT(' ') || *szInCmdLine == TEXT('\t'))
			szInCmdLine++;

		/* Split at whitespace or double-quote. */
		szInInCmdLine = szInCmdLine;
		if(*szInInCmdLine != TEXT('"'))
		{
			while(*szInInCmdLine && *szInInCmdLine != TEXT(' ') && *szInInCmdLine != TEXT('\t'))
				szInInCmdLine++;
		}
		else
		{
			/* Skip opening double-quote. */
			szInCmdLine++;
			szInInCmdLine++;

			/* Go to the end. */
			while(*szInInCmdLine && *szInInCmdLine != TEXT('"'))
				szInInCmdLine++;
		}

		if(*szInInCmdLine)
			*szInInCmdLine = TEXT('\0');
		else bLastToken = TRUE;

		/* Only way this can fail is if there was trailing whitespace or
		 * unmatched quotes at the end of the line.
		 */
		if(*szInCmdLine)
		{
			/* What sort of parameter were we expecting? */
			switch(pex)
			{
			case PEX_CONFDIR:
				/* Store the specified config directory. */
				lpcl->dwFlags |= CLF_CONFDIR;
				lpcl->szConfDir = ProcHeapAlloc((_tcslen(szInCmdLine) + 1) * sizeof(TCHAR));
				_tcscpy(lpcl->szConfDir, szInCmdLine);

			/* Fall through. */
			case PEX_BINARYPATH:
				/* Ignore the path to the binary (think argv[0]). */
				pex = PEX_DEFAULT;
				break;

			default:
				/* Examine what we've been given. */
				if(_tcscmp(szInCmdLine, TEXT("-c")) == 0)
					pex = PEX_CONFDIR;	/* Next param is config dir. */
				else
				{
					/* We have a filename, so add it to the list, lengthening it
					 * as required.
					 */
					if(lpcl->uiNumFiles == uiBufferFilenames)
					{
						uiBufferFilenames <<= 2;

						/* Overflow? If so, ignore the file. This is impossible
						 * for all practical purposes.
						 */
						if(uiBufferFilenames < lpcl->uiNumFiles) break;

						lpcl = ProcHeapReAlloc(lpcl, sizeof(CMDLINE) + (uiBufferFilenames - 1) * sizeof(LPTSTR));
					}

					/* Create a new buffer for the filename and copy it in. */
					lpcl->lpszFilenames[lpcl->uiNumFiles] = ProcHeapAlloc((_tcslen(szInCmdLine) + 1) * sizeof(TCHAR));
					_tcscpy(lpcl->lpszFilenames[lpcl->uiNumFiles], szInCmdLine);
					lpcl->uiNumFiles++;
				}
			}
		}

		/* Begin next time after the end of this token. Notice that we only pass
		 * the end of the string here if bLastToken is set, so we're okay.
		 */
		szInCmdLine = szInInCmdLine + 1;

	} while(!bLastToken && *szInCmdLine);

	ProcHeapFree(szCmdLine);

	return lpcl;
}


/* FreeCmdLine
 *   Frees structure created by ParseCommandLine.
 *
 * Parameters:
 *   CMDLINE*		lpcl	Structure to free.
 *
 * Return value: None.
 */
static void FreeCmdLine(CMDLINE *lpcl)
{
	UINT ui;

	/* Free the individual filenames first. */
	for(ui = 0; ui < lpcl->uiNumFiles; ui++)
		ProcHeapFree(lpcl->lpszFilenames[ui]);

	/* Did we specify a config dir? */
	if(lpcl->dwFlags & CLF_CONFDIR)
		ProcHeapFree(lpcl->szConfDir);

	/* Now the structure itself. */
	ProcHeapFree(lpcl);
}
