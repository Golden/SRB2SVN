/* SRB2 Workbench -- A map editor for Sonic Robo Blast 2.
 * Copyright (C) 2009 Gregor Dick
 * http://workbench.srb2.org/
 *
 * Incorporates portions of Doom Builder, (C) 2003 Pascal vd Heiden.
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 * USA.
 *
 * fof.c: Routines for manipulating floors-over-floors.
 *
 * AMDG.
 */

#include <windows.h>
#include <stdio.h>
#include <tchar.h>

#include "general.h"
#include "map.h"
#include "config.h"
#include "editing.h"
#include "texture.h"
#include "fof.h"
#include "mapconfig.h"
#include "wadopts.h"

#include "CodeImp/ci_data_proto.h"


#define DEFAULT_FOF_ALPHA	0x80
#define CONTROLSEC_RADIUS	32
#define LDEFFECT_NONE		0
#define LDTAG_NONE			0


static __inline DWORD GetCustomFOFFlags(MAP *lpmap, int iLinedef);
static __inline BYTE GetTranslucentFOFAlpha(MAP *lpmap, int iLinedef);
static BOOL FOFNeedsDoubleSidedef(CONFIG *lpcfgFOFTypes, short nFOFType);
static BOOL CanCreateInteriorControlSector(MAP *lpmap, int iFacingLinedef);
static void GetInteriorControlSecRect(MAP *lpmap, int iFacingLinedef, LPRECT lprc);
static BOOL CreateControlSector(MAP *lpmap, int *lpiControlSector, int *lpiLinedef, CONTROLSECPOS *lpcsp, DRAW_OPERATION *lpdrawop, CONFIG *lpcfgWadOptMap, LPCTSTR szSky);
static BOOL CreateInteriorControlSector(MAP *lpmap, int iFacingLinedef, int *lpiControlSector, int *lpiLinedef, DRAW_OPERATION *lpdrawop, CONFIG *lpcfgWadOptMap, LPCTSTR szSky);
static __inline void SetCustomFOFFlags(MAP *lpmap, int iLinedef, DWORD dwFlags);
static __inline void SetTranslucentFOFAlpha(MAP *lpmap, int iLinedef, BYTE byAlpha);



/* GetFOFProperties
 *   Retrieves the properties of a FOF given the index of a FOF linedef.
 *
 * Parameters:
 *   MAP*			lpmap			Map structure.
 *   int			iLinedef		The custom FOF's control linedef.
 *   CONFIG*		lpcfgFOFTypes	FOF effect info from the game config. If
 *									NULL, we immediately return FALSE.
 *   FOF_FLAGS*		lpff			Structure describing special FOF flag
 *									values. Ignored if lpfof is NULL.
 *   FOF*			lpfof			Structure into which the FOF's properties
 *									will be returned. May be NULL if the
 *									properties themselves are not required.
 *   int			iColourmap		Colourmap linedef effect, or negative to
 *									ignore colourmaps.
 *
 * Return value: BOOL
 *   TRUE if the specified linedef is indeed a FOF linedef; FALSE otherwise.
 *
 * Remarks:
 *   If the linedef specified is not a FOF linedef, the passed FOF structure is
 *   not written to.
 */
BOOL GetFOFProperties(MAP *lpmap, int iLinedef, CONFIG *lpcfgFOFTypes, FOF_FLAGS *lpff, FOF *lpfof, int iColourmap)
{
	CONFIG *lpcfgEffect;
	MAPLINEDEF *lpld = &lpmap->linedefs[iLinedef];

	/* Sanity check. */
	if(!lpcfgFOFTypes)
		return FALSE;

	/* Get the entry in the FOF config. */
	if((lpcfgEffect = GetFOFEffectConfig(lpcfgFOFTypes, lpld->effect)))
	{
		BOOL bCustom = ConfigGetInteger(lpcfgEffect, FOFCFG_EFFECT_CUSTOM);

		/* Sanity check. */
		if(SidedefExists(lpmap, lpld->s1) || (bCustom && !SidedefExists(lpmap, lpld->s2)))
		{
			/* If the caller is only interested in the return value, we can skip
			 * over the meat.
			 */
			if(lpfof)
			{
				int iControlSector = lpmap->sidedefs[lpld->s1].sector;
				MAPSECTOR *lpsec = &lpmap->sectors[iControlSector];

				/* Get the flags. */
				lpfof->dwFlags = bCustom ? GetCustomFOFFlags(lpmap, iLinedef) : (DWORD)ConfigGetInteger(lpcfgEffect, FOFCFG_EFFECT_FLAGS);

				/* Populate the other fields. */
				lpfof->iControlSector = iControlSector;
				lpfof->iLinedef = iLinedef;
				lpfof->nCeilingHeight = lpsec->hceiling;
				lpfof->nFloorHeight = lpsec->hfloor;
				lpfof->nBrightness = lpsec->brightness;
				lpfof->nType = lpld->effect;
				strncpy(lpfof->szCeiling, lpsec->tceiling, TEXNAME_WAD_BUFFER_LENGTH);
				strncpy(lpfof->szFloor, lpsec->tfloor, TEXNAME_WAD_BUFFER_LENGTH);
				strncpy(lpfof->szSidedefs, lpmap->sidedefs[lpld->s1].middle, TEXNAME_WAD_BUFFER_LENGTH);

				/* FOFs coming from the map are pristine. */
				lpfof->dwEditFlags = 0;

				/* Terminate the buffers. */
				lpfof->szCeiling[NUM_ELEMENTS(lpfof->szCeiling) - 1] = '\0';
				lpfof->szFloor[NUM_ELEMENTS(lpfof->szFloor) - 1] = '\0';
				lpfof->szSidedefs[NUM_ELEMENTS(lpfof->szSidedefs) - 1] = '\0';

				/* Begin by assuming no colourmap. */
				lpfof->iColourmapLinedef = -1;

				/* Look for a colourmap. TODO: Cache colourmaps? */
				if(iColourmap >= 0)
				{
					int i;

					for(i = 0; i < lpmap->iLinedefs; i++)
					{
						MAPLINEDEF *lpld = &lpmap->linedefs[i];
						if(lpld->effect == iColourmap &&
							SidedefExists(lpmap, lpld->s1) &&
							lpld->tag == lpmap->sectors[lpfof->iControlSector].tag)
						{
							char szColour[7];

							/* Mark as having a colourmap. */
							lpfof->dwEditFlags |= FEF_COLOURMAP;

							/* Save the linedef index. */
							lpfof->iColourmapLinedef = i;

							/* Calculate the colour. */
							strncpy(szColour, lpmap->sidedefs[lpld->s1].upper + 1, 6);
							szColour[NUM_ELEMENTS(szColour) - 1] = '\0';
							lpfof->crefColourmap = ColourmapColourToCOLORREF(strtol(szColour, NULL, 16));

							/* Get the alpha. */
							lpfof->byColourmapAlpha = ColourmapAlphaFromCharacter(lpmap->sidedefs[lpld->s1].upper[7]);
						}
					}
				}				

				/* Alpha. */
				if(SidedefExists(lpmap, lpmap->linedefs[iLinedef].s1) &&
					strncmp(lpmap->sidedefs[lpmap->linedefs[iLinedef].s1].upper, BLANK_TEXTURE, TEXNAME_WAD_BUFFER_LENGTH))
				{
					/* Alpha is overriden. */
					lpfof->byAlpha = ((lpff->dwMask & FFM_TRANSLUCENT) && (lpfof->dwFlags & lpff->dwTranslucent))
						? GetTranslucentFOFAlpha(lpmap, iLinedef)
						: DEFAULT_FOF_ALPHA;
					lpfof->dwEditFlags |= FEF_TRANSLUCENT;
				}
				else
				{
					/* Alpha not overriden. */
					lpfof->byAlpha = DEFAULT_FOF_ALPHA;
				}
			}

			return TRUE;
		}
	}

	/* Not a valid FOF linedef. */
	return FALSE;
}


/* GetSpecialFOFFlagValues
 *   Retrieves values of FOF flags from the config that have a special
 *   significance.
 *
 * Parameters:
 *   CONFIG*		lpcfgFOFFlags	FOF flags from the game config.
 *   FOF_FLAGS*		lpff			Structure into which to return values.
 *
 * Return value: None.
 */
void GetSpecialFOFFlagValues(CONFIG *lpcfgFOFFlags, FOF_FLAGS *lpff)
{
	CONFIG *lpcfg;

	/* Begin by indicating that no flags are valid. */
	lpff->dwMask = 0;

	/* Begin macro. */
#define ADD_FOF_FLAG(lpff, name, fffield) \
	if((lpcfg = ConfigGetSubsection(lpcfgFOFFlags, FOFCFG_FF_##name))) \
	{ \
		(lpff)->dwMask |= FFM_##name; \
		(lpff)->fffield = ConfigGetInteger(lpcfg, FOFCFG_FLAG_VALUE); \
	}
	/* End macro. */

	ADD_FOF_FLAG(lpff, EXISTS, dwExists);
	ADD_FOF_FLAG(lpff, TRANSLUCENT, dwTranslucent);

#undef ADD_FOF_FLAG
}


/* GetCustomFOFFlags
 *   Retrieves the flags encoded for a custom FOF.
 *
 * Parameters:
 *   MAP*	lpmap		Map structure.
 *   int	iLinedef	The custom FOF's control linedef.
 *
 * Return value: DWORD
 *   Flags.
 *
 * Remarks:
 *   No validation is performed: it is assumed that the specified linedef is a
 *   bona fide custom FOF.
 */
static __inline DWORD GetCustomFOFFlags(MAP *lpmap, int iLinedef)
{
	/* The flags are encoded in upper texture on the reverse of the control
	 * linedef.
	 */
	char szFlags[TEXNAME_BUFFER_LENGTH];

	/* Make a null-terminated copy of the flags. */
	strncpy(szFlags, lpmap->sidedefs[lpmap->linedefs[iLinedef].s2].upper, TEXNAME_WAD_BUFFER_LENGTH);
	szFlags[NUM_ELEMENTS(szFlags) - 1] = '\0';

	/* Get the value. */
	return strtol(szFlags, NULL, 16);
}


/* GetTranslucentFOFAlpha
 *   Retrieves the alpha value encoded in a translucent FOF.
 *
 * Parameters:
 *   MAP*	lpmap		Map structure.
 *   int	iLinedef	The custom FOF's control linedef.
 *
 * Return value: BYTE
 *   Alpha.
 *
 * Remarks:
 *   No validation is performed: it is assumed that the specified linedef is a
 *   bona fide translucent FOF.
 */
static __inline BYTE GetTranslucentFOFAlpha(MAP *lpmap, int iLinedef)
{
	/* The alpha value is encoded in upper texture on the front of the control
	 * linedef.
	 */
	char szAlpha[TEXNAME_BUFFER_LENGTH];

	/* Make a null-terminated copy of the alpha value. */
	strncpy(szAlpha, lpmap->sidedefs[lpmap->linedefs[iLinedef].s1].upper, TEXNAME_WAD_BUFFER_LENGTH);
	szAlpha[NUM_ELEMENTS(szAlpha) - 1] = '\0';

	/* If it doesn't begin with '#', it's invalid. */
	if(*szAlpha != '#') return DEFAULT_FOF_ALPHA;

	/* Get the value. */
	return (BYTE)strtol(szAlpha + 1, NULL, 10);
}


/* AllocateFOFList
 *   Allocates and initialises a FOF list.
 *
 * Parameters:
 *   None.
 *
 * Return value: FOF_LIST*
 *   Pointer to new list.
 *
 * Remarks:
 *   Call DestroyFOFList to free the allocated list.
 */
FOF_LIST* AllocateFOFList(void)
{
	FOF_LIST *lpfl = ProcHeapAlloc(sizeof(FOF_LIST));

	lpfl->flnHeader.lpflnNext = &lpfl->flnFooter;
	lpfl->flnFooter.lpflnPrev = &lpfl->flnHeader;
	lpfl->flnHeader.lpflnPrev = lpfl->flnFooter.lpflnNext = NULL;

	return lpfl;
}


/* DestroyFOFList
 *   Destroys a FOF list and its contents.
 *
 * Parameters:
 *   FOF_LIST*	lpfl	List to destroy.
 *
 * Return value: None.
 */
void DestroyFOFList(FOF_LIST *lpfl)
{
	FOF_LIST_NODE *lpfln = lpfl->flnHeader.lpflnNext;

	/* Loop until we get to the footer node. */
	while(lpfln->lpflnNext)
	{
		FOF_LIST_NODE *lpflnNext = lpfln->lpflnNext;
		ProcHeapFree(lpfln);
		lpfln = lpflnNext;
	}

	ProcHeapFree(lpfl);
}


/* AddNodeToFOFList
 *   Adds a node to the end of a FOF list.
 *
 * Parameters:
 *   FOF_LIST*	lpfl	List to which a node is to be added.
 *
 * Return value: FOF_LIST_NODE*
 *   Pointer to new node.
 *
 * Remarks:
 *   The node can be destroyed either by removing it from the list explicitly by
 *   calling RemoveNodeFromFOFList, or when the entire list is destroyed by
 *   calling DestroyFOFList.
 */
FOF_LIST_NODE* AddNodeToFOFList(FOF_LIST *lpfl)
{
	FOF_LIST_NODE* lpflnNew = ProcHeapAlloc(sizeof(FOF_LIST_NODE));

	lpflnNew->lpflnNext = &lpfl->flnFooter;
	lpflnNew->lpflnPrev = lpfl->flnFooter.lpflnPrev;
	lpfl->flnFooter.lpflnPrev->lpflnNext = lpflnNew;
	lpfl->flnFooter.lpflnPrev = lpflnNew;

	return lpflnNew;
}


/* RemoveNodeFromFOFList
 *   Removes a node from a FOF list.
 *
 * Parameters:
 *   FOF_LIST_NODE*		lpfln	Node to be removed.
 *
 * Return value: None.
 */
void RemoveNodeFromFOFList(FOF_LIST_NODE *lpfln)
{
	lpfln->lpflnNext->lpflnPrev = lpfln->lpflnPrev;
	lpfln->lpflnPrev->lpflnNext = lpfln->lpflnNext;
	ProcHeapFree(lpfln);
}


/* AddFOFsToListByTag
 *   Adds all FOFs having a certain tag to a FOF list.
 *
 * Parameters:
 *   MAP*			lpmap			Map structure.
 *   short			nTag			Tag of FOFs to be added.
 *   FOF_LIST*		lpfl			List to which FOF are to be added.
 *   CONFIG*		lpcfgFOFTypes	FOF effect info from the game config.
 *   FOF_FLAGS*		lpff			Structure describing special FOF flag
 *									values.
 *   int			iColourmap		Colourmap linedef effect, or negative to
 *									ignore colourmaps.
 *
 * Return value: None.
 */
void AddFOFsToListByTag(MAP *lpmap, short nTag, FOF_LIST *lpfl, CONFIG *lpcfgFOFTypes, FOF_FLAGS *lpff, int iColourmap)
{
	int iLinedef;

	for(iLinedef = 0; iLinedef < lpmap->iLinedefs; iLinedef++)
	{
		FOF fof;

		/* Check if this linedef has the right tag and is a FOF linedef, and if
		 * so, add a node to the list and copy in the FOF's properties.
		 */
		if(lpmap->linedefs[iLinedef].tag == nTag &&
			GetFOFProperties(lpmap, iLinedef, lpcfgFOFTypes, lpff, &fof, iColourmap))
		{
			FOF_LIST_NODE *lpfln = AddNodeToFOFList(lpfl);
			lpfln->fof = fof;
		}
	}
}


/* CanCreateControlSectorAtPoint
 *   Determines whether it's safe to create a control sector centred at a given
 *   point.
 *
 * Parameters:
 *   MAP*	lpmap				Map structure.
 *   short	xCentre, yCentre	Co-ordinates of centre.
 *
 * Return value: BOOL
 *   TRUE if a control sector can be created at the point; FALSE otherwise.
 */
static BOOL CanCreateControlSectorAtPoint(MAP *lpmap, short xCentre, short yCentre)
{
	RECT rcControlSector;

	/* Most importantly and easily-checked, we don't make control sectors inside
	 * other sectors.
	 */
	if(IntersectSector(xCentre, yCentre, lpmap, NULL, NULL) >= 0)
		return FALSE;

	rcControlSector.left = (int)xCentre - CONTROLSEC_RADIUS;
	rcControlSector.right = (int)xCentre + CONTROLSEC_RADIUS;
	rcControlSector.top = (int)yCentre - CONTROLSEC_RADIUS;
	rcControlSector.bottom = (int)yCentre + CONTROLSEC_RADIUS;

	/* Make sure we don't go off the edge of the map. */
	if(rcControlSector.left < -32768 || rcControlSector.bottom < -32768 ||
		rcControlSector.right > 32767 || rcControlSector.top > 32767)
		return FALSE;

	/* Make sure there's nothing in the way. */
	return IsRectangleUnobstructed(lpmap, (short)rcControlSector.left, (short)rcControlSector.right, (short)rcControlSector.top, (short)rcControlSector.bottom);
}


/* ApplyFOFListToMap
 *   Updates the FOFs in a list to have the specified properties.
 *
 * Parameters:
 *   MAP*				lpmap			Map structure.
 *   short				nTag			FOFs' tag.
 *   FOF_LIST*			lpfl			List of FOFs.
 *   CONFIG*			lpcfgFOFTypes	FOF types subsection from map config.
 *   FOF_FLAGS*			lpff			Structure describing special FOF flag
 *										values.
 *   CONTROLSECPOS* 	lpcsp			Structure describing position at which
 *										to start placing control sectors. Will
 *										be updated with new position.
 *   DRAW_OPERATION*	lpdrawop		Draw operation structure.
 *   CONFIG*			lpcfgWadOptMap	Map options.
 *   LPCTSTR			szSky			Sky texture name.
 *   int				iColourmap		Colourmap linedef effect, or negative to
 *										ignore colourmaps.
 *
 * Return value: BOOL
 *   TRUE on success; FALSE on failure.
 */
BOOL ApplyFOFListToMap(MAP *lpmap, short nTag, FOF_LIST *lpfl, CONFIG *lpcfgFOFTypes, FOF_FLAGS *lpff, CONTROLSECPOS *lpcsp, DRAW_OPERATION *lpdrawop, CONFIG *lpcfgWadOptMap, LPCTSTR szSky, int iColourmap)
{
	FOF_LIST_NODE *lpfln = &lpfl->flnHeader;

	/* Iterate over all FOFs in the list. */
	while(lpfln = lpfln->lpflnNext, lpfln->lpflnNext)
	{
		MAPLINEDEF *lpldControl = lpfln->fof.iLinedef >= 0 ? &lpmap->linedefs[lpfln->fof.iLinedef] : NULL;

		if(lpfln->fof.dwEditFlags & FEF_DELETE)
		{
			if(lpldControl)
			{
				/* Just remove the effect. */
				lpldControl->effect = LDEFFECT_NONE;
				lpldControl->tag = LDTAG_NONE;
			}

			/* Otherwise, do nothing -- the user created a new FOF and also
			 * deleted it.
			 */
		}
		else
			ApplyFOFToMap(lpmap, nTag, &lpfln->fof, lpcfgFOFTypes, lpff, lpcsp, lpdrawop, lpcfgWadOptMap, szSky, iColourmap);

		lpfln->fof.dwEditFlags &= ~FEF_DELETE;
	}

	/* Success! */
	return TRUE;
}


/* FOFNeedsDoubleSidedef
 *   Determines whether a particular FOF-type requires a two-sided control
 *   linedef.
 *
 * Parameters:
 *   CONFIG*	lpcfgFOFTypes	FOF types subsection from map config.
 *   short		nFOFType		FOF effect value.
 *
 * Return value: BOOL
 *   TRUE if the FOF requires two sidedefs; FALSE otherwise.
 */
static BOOL FOFNeedsDoubleSidedef(CONFIG *lpcfgFOFTypes, short nFOFType)
{
	/* Get info for this type. */
	CONFIG *lpcfgFOFEffect = GetFOFEffectConfig(lpcfgFOFTypes, nFOFType);

	return ConfigGetInteger(lpcfgFOFEffect, FOFCFG_EFFECT_CUSTOM) ||
		ConfigGetInteger(lpcfgFOFEffect, FOFCFG_EFFECT_AIRBOB) ||
		ConfigGetInteger(lpcfgFOFEffect, FOFCFG_EFFECT_RISING);
}


/* GetFOFEffectConfig
 *   Gets the subsection of the FOF properties for a particular FOF type.
 *
 * Parameters:
 *   CONFIG*	lpcfgFOFTypes	FOF types subsection from map config.
 *   short		nFOFType		FOF effect value.
 *
 * Return value: CONFIG*
 *   Pointer to subsection, or NULL if none exists.
 */
CONFIG* GetFOFEffectConfig(CONFIG *lpcfgFOFTypes, short nFOFType)
{
	/* Plenty big for the unsigned short we're actually going to stick in it. */
	TCHAR szValue[CCH_SIGNED_INT];

	_sntprintf(szValue, NUM_ELEMENTS(szValue), TEXT("%d"), nFOFType);
	return ConfigGetSubsection(lpcfgFOFTypes, szValue);
}


/* CanCreateInteriorControlSector
 *   Determines whether a control sector could be created facing a specified
 *   linedef.
 *
 * Parameters:
 *   MAP*	lpmap			Map structure.
 *   int	iFacingLinedef	Linedef next to which the control sector is desired.
 *
 * Return value: BOOL
 *   TRUE if a control sector could be created; FALSE otherwise.
 */
static BOOL CanCreateInteriorControlSector(MAP *lpmap, int iFacingLinedef)
{
	RECT rcInterior;

	/* Find where the interior sector would go. */
	GetInteriorControlSecRect(lpmap, iFacingLinedef, &rcInterior);

	/* Make sure we don't go off the edge of the map. */
	if(rcInterior.left < -32768 || rcInterior.bottom < -32768 ||
		rcInterior.right > 32767 || rcInterior.top > 32767)
		return FALSE;

	return IsRectangleUnobstructed(lpmap, (short)rcInterior.left, (short)rcInterior.right, (short)rcInterior.top, (short)rcInterior.bottom);
}


/* GetInteriorControlSecRect
 *   Obtains the rectangle where an interior control sector facing a given
 *   linedef would be created.
 *
 * Parameters:
 *   MAP*		lpmap			Map structure.
 *   int		iFacingLinedef	Linedef next to which the control sector is
 *								desired.
 *   LPRECT		lprc			Rectangle to receive co-ordinates.
 *
 * Return value: None.
 */
static void GetInteriorControlSecRect(MAP *lpmap, int iFacingLinedef, LPRECT lprc)
{
	short xCentre, yCentre;
	MAPVERTEX *lpvx1 = &lpmap->vertices[lpmap->linedefs[iFacingLinedef].v1];
	MAPVERTEX *lpvx2 = &lpmap->vertices[lpmap->linedefs[iFacingLinedef].v2];
	short cxLine = lpvx2->x - lpvx1->x;
	short cyLine = lpvx2->y - lpvx1->y;

	/* First find the centre. We do so by finding the centre of the linedef and
	 * then offseting it in the appropriate direction.
	 */

	xCentre = ((int)lpvx1->x + (int)lpvx2->x) / 2;
	yCentre = ((int)lpvx1->y + (int)lpvx2->y) / 2;

	if(cxLine > 0 && cxLine >= abs(cyLine)) yCentre -= CONTROLSEC_RADIUS;
	else if(cyLine > 0 && cyLine >= abs(cxLine)) xCentre += CONTROLSEC_RADIUS;
	else if(cxLine < 0 && -cxLine >= abs(cyLine)) yCentre += CONTROLSEC_RADIUS;
	else xCentre -= CONTROLSEC_RADIUS;

	/* Get the rectangle's co-ordinates. */
	lprc->bottom = yCentre - CONTROLSEC_RADIUS / 2;
	lprc->top = yCentre + CONTROLSEC_RADIUS / 2;
	lprc->left = xCentre - CONTROLSEC_RADIUS / 2;
	lprc->right = xCentre + CONTROLSEC_RADIUS / 2;
}


/* CreateControlSector
 *   Attempts to find somewhere that a control sector can be created, and then
 *   does so if it can.
 *
 * Parameters:
 *   MAP*				lpmap				Map structure.
 *   int*				lpiControlSector	Index of control sector is returned
 *											here.
 *   int*				lpiLinedef			Index of control linedef is returned
 *											here.
 *   CONTROLSECPOS*		lpcsp				Structure describing position at
 *											which to start placing control
 *											sectors. Will be updated with new
 *											position.
 *   DRAW_OPERATION*	lpdrawop			Draw operation.
 *   CONFIG*			lpcfgWadOptMap		Map options.
 *   LPCTSTR			szSky				Sky texture name.
 *
 * Return value: BOOL
 *   TRUE if a control sector was created successfully; FALSE otherwise.
 */
static BOOL CreateControlSector(MAP *lpmap, int *lpiControlSector, int *lpiLinedef, CONTROLSECPOS *lpcsp, DRAW_OPERATION *lpdrawop, CONFIG *lpcfgWadOptMap, LPCTSTR szSky)
{
	const short cxRowDelta = (lpcsp->csd == CSD_UP) ? 4 * CONTROLSEC_RADIUS : (lpcsp->csd == CSD_DOWN) ? -4 * CONTROLSEC_RADIUS : 0;
	const short cyRowDelta = (lpcsp->csd == CSD_RIGHT) ? 4 * CONTROLSEC_RADIUS : (lpcsp->csd == CSD_LEFT) ?  -4 * CONTROLSEC_RADIUS : 0;
	const struct { short cxOffset, cyOffset; } rowoffsets[] = {{0, 0}, {cxRowDelta, cyRowDelta}, {-cxRowDelta, -cyRowDelta}};
	
	/* NB: Reverse the senses of 'row' and 'column' in the following if we're
	 * moving up or down.
	 */

	do
	{
		int i;
		
		/* Try each element in the row. */
		for(i = 0; i < (int)NUM_ELEMENTS(rowoffsets); i++)
		{
			/* If we can make a sector here, do so and be done with it. */
			if(CanCreateControlSectorAtPoint(lpmap, (short)lpcsp->xCentre + rowoffsets[i].cxOffset, (short)lpcsp->yCentre + rowoffsets[i].cyOffset))
			{
				CreatePolygonalSector(lpmap, (short)lpcsp->xCentre + rowoffsets[i].cxOffset, (short)lpcsp->yCentre + rowoffsets[i].cyOffset, CONTROLSEC_RADIUS, 4, RT_TOEDGES, lpdrawop, lpcfgWadOptMap, szSky, FALSE, 0, 0, 0, 0);

				/* The highest-numbered items are newest. */
				*lpiControlSector = lpmap->iSectors - 1;
				*lpiLinedef = lpmap->iLinedefs - 1;

				return TRUE;
			}
		}

		/* Couldn't make a control sector in this column, so advance to the next
		 * one. Column offsets are orthogonal to row offsets.
		 */
		lpcsp->xCentre += cyRowDelta;
		lpcsp->yCentre += cxRowDelta;

	}
	while(lpcsp->xCentre >= -32768 && lpcsp->xCentre <= 32767 &&
		lpcsp->yCentre >= -32768 && lpcsp->yCentre <= 32767);

	/* If we get here, we failed. Clean up the control sector position so we
	 * have a chance of succeeding another time.
	 */
	lpcsp->xCentre = lpcsp->yCentre = 0;
	return FALSE;
}


/* CreateInteriorControlSector
 *   Attempts to find somewhere that a control sector can be created, and then
 *   does so if it can.
 *
 * Parameters:
 *   MAP*				lpmap				Map structure.
 *   int				iFacingLinedef		Linedef next to which the control
 *											sector is desired.
 *   int*				lpiControlSector	Index of control sector is returned
 *											here.
 *   int*				lpiLinedef			Index of control linedef is returned
 *											here.
 *   DRAW_OPERATION*	lpdrawop			Draw operation.
 *   CONFIG*			lpcfgWadOptMap		Map options.
 *   LPCTSTR			szSky				Sky texture name.
 *
 * Return value: BOOL
 *   TRUE if a control sector was created successfully; FALSE otherwise.
 */
static BOOL CreateInteriorControlSector(MAP *lpmap, int iFacingLinedef, int *lpiControlSector, int *lpiLinedef, DRAW_OPERATION *lpdrawop, CONFIG *lpcfgWadOptMap, LPCTSTR szSky)
{
	if(CanCreateInteriorControlSector(lpmap, iFacingLinedef))
	{
		RECT rc;
		GetInteriorControlSecRect(lpmap, iFacingLinedef, &rc);
		CreateRectangularSector(lpmap, &rc, lpdrawop, lpcfgWadOptMap, szSky);

		/* The highest-numbered items are newest. */
		*lpiControlSector = lpmap->iSectors - 1;
		*lpiLinedef = lpmap->iLinedefs - 1;

		return TRUE;
	}

	/* Failure. */
	return FALSE;
}



/* SetCustomFOFFlags
 *   Encodes the flags for a custom FOF.
 *
 * Parameters:
 *   MAP*	lpmap		Map structure.
 *   int	iLinedef	The custom FOF's control linedef.
 *   DWORD	dwFlags		The flags to encode.
 *
 * Return value: None.
 *
 * Remarks:
 *   No validation is performed: it is assumed that the specified linedef is a
 *   bona fide custom FOF.
 */
static __inline void SetCustomFOFFlags(MAP *lpmap, int iLinedef, DWORD dwFlags)
{
	char szFlags[TEXNAME_BUFFER_LENGTH];

	/* Convert the flags to a hex string, and copy them into the reverse
	 * sidedef's upper texture.
	 */
	sprintf(szFlags, "%X", (unsigned int)dwFlags);
	FillTexNameBufferA(lpmap->sidedefs[lpmap->linedefs[iLinedef].s2].upper, szFlags);
}


/* SetTranslucentFOFAlpha
 *   Encodes the alpha value in a translucent FOF.
 *
 * Parameters:
 *   MAP*	lpmap		Map structure.
 *   int	iLinedef	The custom FOF's control linedef.
 *   BYTE	byAlpha		Alpha value to encode.
 *
 * Return value: None.
 *
 * Remarks:
 *   No validation is performed: it is assumed that the specified linedef is a
 *   bona fide translucent FOF.
 */
static __inline void SetTranslucentFOFAlpha(MAP *lpmap, int iLinedef, BYTE byAlpha)
{
	/* The alpha value is encoded in upper texture on the front of the control
	 * linedef.
	 */
	char szAlpha[TEXNAME_BUFFER_LENGTH];

	/* Convert the flags to a hex string, and copy them into the reverse
	 * sidedef's upper texture.
	 */
	sprintf(szAlpha, "#%03d", byAlpha);
	FillTexNameBufferA(lpmap->sidedefs[lpmap->linedefs[iLinedef].s1].upper, szAlpha);
}


/* GetDefaultFOFProperties
 *   Sets the type, flags and alpha fields according to the default FOF
 *   specified in the config.
 *
 * Parameters:
 *   FOF*		lpfof			Structure in which properties will be returned.
 *   CONFIG*	lpcfgFOFTypes	FOF effect info from the game config.
 *
 * Return value: None.
 */
void GetDefaultFOFProperties(FOF *lpfof, CONFIG *lpcfgFOFTypes)
{
	CONFIG *lpcfgEffect;

	lpfof->nType = ConfigGetInteger(lpcfgFOFTypes, FOFCFG_DEFAULT);

	/* Info about the default type. */
	lpcfgEffect = GetFOFEffectConfig(lpcfgFOFTypes, lpfof->nType);
	
	lpfof->byAlpha = DEFAULT_FOF_ALPHA;
	lpfof->dwFlags = (DWORD)ConfigGetInteger(lpcfgEffect, FOFCFG_EFFECT_FLAGS);
	lpfof->iColourmapLinedef = -1;
}


/* GenerateColourmapString
 *   Generates a colourmap string.
 *
 * Parameters:
 *   LPTSTR		szBuffer	Buffer into which the NULL-terminated string is to
 *							be returned. Must be at least
 *							(CCH_COLOURMAP_COLOUR + 1) characters long.
 *   COLORREF	crefColour	Colour.
 *
 * Return value: None.
 *
 * Remarks:
 *   More useful for the editor than for creating FOFs themselves.
 */
void GenerateColourmapString(LPTSTR szBuffer, COLORREF crefColour)
{
	_sntprintf(szBuffer, CCH_COLOURMAP_COLOUR, TEXT("%06X"), COLORREFToColourmapColour(crefColour));
	szBuffer[CCH_COLOURMAP_COLOUR] = TEXT('\0');
}


/* GetColourmapFromString
 *   Calculates the COLORREF value for a colourmap colour string.
 *
 * Parameters:
 *   LPCTSTR	szColour	Buffer containing the NULL-terminated colourmap
 *							string.
 *
 * Return value: COLORREF
 *   Colour.
 *
 * Remarks:
 *   More useful for the editor than for interpreting FOFs themselves.
 */
COLORREF GetColourmapFromString(LPCTSTR szColour)
{
	TCHAR *lptc;
	COLORREF crefRead;
	
	if(_tcslen(szColour) != CCH_COLOURMAP_COLOUR)
		return DEFAULT_COLOURMAP_COLOUR;

	/* Attempt to read the colour. */
	crefRead = _tcstol(szColour, &lptc, 16);

	/* Return read value on success, or default on failure. */
	return (*lptc) ? DEFAULT_COLOURMAP_COLOUR : ColourmapColourToCOLORREF(crefRead);
}


/* CountFOFsInList
 *   Returns the number of FOFs in a FOF list.
 *
 * Parameters:
 *   FOF_LIST*	lpfl	FOF list.
 *
 * Return value: int
 *   Number of FOFs in list.
 */
int CountFOFsInList(FOF_LIST *lpfl)
{
	int i = 0;
	FOF_LIST_NODE *lpflnRover = &lpfl->flnHeader;

	/* Loop until the next element is the footer. */
	while((lpflnRover = lpflnRover->lpflnNext)->lpflnNext) i++;

	return i;
}


/* AddNewDefaultFOFToList
 *   Adds a new node to a FOF list containing a FOF having the default
 *   parameters.
 *
 * Parameters:
 *   FOF_LIST*	lpfl					FOF list.
 *   CONFIG*	lpcfgMap				Map config.
 *   CONFIG*	lpcfgWadOptMap			Map options from wad options.
 *   short		nContainingFloor		Floor of containing sector.
 *   short		nContainingBrightness	Brightness of containing sector.
 *
 * Return value: FOF_LIST_NODE*
 *   Pointer to new node.
 */
FOF_LIST_NODE* AddNewDefaultFOFToList(FOF_LIST *lpfl, CONFIG *lpcfgMap, CONFIG *lpcfgWadOptMap, short nContainingFloor, short nContainingBrightness)
{
	FOF_LIST_NODE *lpflnNew = AddNodeToFOFList(lpfl);
	CONFIG *lpcfgMapDefTex = ConfigGetSubsection(lpcfgWadOptMap, WADOPT_DEFAULTTEX);
	CONFIG *lpcfgDefSec = ConfigGetSubsection(lpcfgWadOptMap, WADOPT_DEFAULTSEC);

	/* Get default FOF properties. */
	GetDefaultFOFProperties(&lpflnNew->fof, ConfigGetSubsection(lpcfgMap, MAPCFG_FOFS));

	/* Fill in the per-instance stuff. */
	lpflnNew->fof.dwEditFlags = 0;
	lpflnNew->fof.iControlSector = lpflnNew->fof.iLinedef = -1;
	lpflnNew->fof.nCeilingHeight = nContainingFloor + 2 * DEFAULT_FOF_HEIGHT;
	lpflnNew->fof.nFloorHeight = nContainingFloor + DEFAULT_FOF_HEIGHT;
	lpflnNew->fof.nBrightness = nContainingBrightness;

	/* Textures. */
	ConfigGetStringA(lpcfgMapDefTex, TEXT("middle"), lpflnNew->fof.szSidedefs, TEXNAME_BUFFER_LENGTH);
	ConfigGetStringA(lpcfgDefSec, TEXT("tfloor"), lpflnNew->fof.szFloor, TEXNAME_BUFFER_LENGTH);
	ConfigGetStringA(lpcfgDefSec, TEXT("tceiling"), lpflnNew->fof.szCeiling, TEXNAME_BUFFER_LENGTH);

	return lpflnNew;
}


/* ApplyFOFListToMap
 *   Updates the FOFs in a list to have the specified properties.
 *
 * Parameters:
 *   MAP*				lpmap			Map structure.
 *   short				nTag			FOFs' tag.
 *   FOF*				lpfof			FOF to apply.
 *   CONFIG*			lpcfgFOFTypes	FOF types subsection from map config.
 *   FOF_FLAGS*			lpff			Structure describing special FOF flag
 *										values.
 *   CONTROLSECPOS* 	lpcsp			Structure describing position at which
 *										to start placing control sectors. Will
 *										be updated with new position.
 *   DRAW_OPERATION*	lpdrawop		Draw operation structure.
 *   CONFIG*			lpcfgWadOptMap	Map options.
 *   LPCTSTR			szSky			Sky texture name.
 *   int				iColourmap		Colourmap linedef effect, or negative to
 *										ignore colourmaps.
 *
 * Return value: BOOL
 *   TRUE on success; FALSE on failure.
 */
BOOL ApplyFOFToMap(MAP *lpmap, short nTag, FOF *lpfof, CONFIG *lpcfgFOFTypes, FOF_FLAGS *lpff, CONTROLSECPOS *lpcsp, DRAW_OPERATION *lpdrawop, CONFIG *lpcfgWadOptMap, LPCTSTR szSky, int iColourmap)
{
	MAPLINEDEF *lpldControl = lpfof->iLinedef >= 0 ? &lpmap->linedefs[lpfof->iLinedef] : NULL;
	MAPSECTOR *lpsecControl = lpfof->iControlSector >= 0 ? &lpmap->sectors[lpfof->iControlSector] : NULL;
	MAPSECTOR *lpsecExterior = NULL;

	BOOL bNeedDoubleSided = FOFNeedsDoubleSidedef(lpcfgFOFTypes, lpfof->nType);
	BOOL bCreateNew = FALSE;
	CONFIG *lpcfgFOFEffect = GetFOFEffectConfig(lpcfgFOFTypes, lpfof->nType);

	/* Determine whether we need to make a new control sector. */
	if(lpfof->iLinedef < 0)
		bCreateNew = TRUE;
	else if(bNeedDoubleSided && !SidedefExists(lpmap, lpldControl->s2))
	{
		/* Need to create an interior control sector, so remove the
		 * existing effect.
		  */
		lpldControl->effect = LDEFFECT_NONE;
		lpldControl->tag = LDTAG_NONE;

		/* If there's no room inside the current control sector, create
		 * completely new outer as well as an inner control sector
		 * later.
		 */
		if(!CanCreateInteriorControlSector(lpmap, lpfof->iLinedef))
			bCreateNew = TRUE;
	}

	/* Make a new control sector if necessary. */
	if(bCreateNew)
	{
		if(!CreateControlSector(lpmap, &lpfof->iControlSector, &lpfof->iLinedef, lpcsp, lpdrawop, lpcfgWadOptMap, szSky))
			return FALSE;

		lpldControl = &lpmap->linedefs[lpfof->iLinedef];
		lpsecControl = &lpmap->sectors[lpfof->iControlSector];
	}

	/* Make an interior control sector if necessary. */
	if(bNeedDoubleSided && !SidedefExists(lpmap, lpldControl->s2))
	{
		/* Remember the exterior sector. */
		lpsecExterior = SidedefExists(lpmap, lpldControl->s1) ? &lpmap->sectors[lpmap->sidedefs[lpldControl->s1].sector] : NULL;

		/* Attempt to make the interior sector. */
		if(!CreateInteriorControlSector(lpmap, lpfof->iLinedef, &lpfof->iControlSector, &lpfof->iLinedef, lpdrawop, lpcfgWadOptMap, szSky))
			return FALSE;

		lpldControl = &lpmap->linedefs[lpfof->iLinedef];
		lpsecControl = &lpmap->sectors[lpfof->iControlSector];
	}

	/* Apply the properties to the control components. */
	lpldControl->effect = lpfof->nType;
	lpldControl->tag = nTag;
	lpsecControl->brightness = lpfof->nBrightness;
	lpsecControl->hceiling = lpfof->nCeilingHeight;
	lpsecControl->hfloor = lpfof->nFloorHeight;
	FillTexNameBufferA(lpsecControl->tceiling, lpfof->szCeiling);
	FillTexNameBufferA(lpsecControl->tfloor, lpfof->szFloor);
	FillTexNameBufferA(lpmap->sidedefs[lpldControl->s1].middle, lpfof->szSidedefs);

	/* If we have a *new* exterior sector, set its planes' heights. */
	if(lpsecExterior)
	{
		lpsecExterior->hceiling = lpsecControl->hceiling;
		lpsecExterior->hfloor = lpsecControl->hfloor;
	}

	/* In some cases we have to perform some special handling. */

	/* Custom FOFs require that we encode their flags. */
	if(ConfigGetInteger(lpcfgFOFEffect, FOFCFG_EFFECT_CUSTOM))
		SetCustomFOFFlags(lpmap, lpfof->iLinedef, lpfof->dwFlags);

	/* Translucent FOFs require their alphas to be encoded. */
	if((lpff->dwMask & FFM_TRANSLUCENT) &&
		(lpfof->dwFlags & lpff->dwTranslucent))
	{
		if(lpfof->dwEditFlags & FEF_TRANSLUCENT)
			SetTranslucentFOFAlpha(lpmap, lpfof->iLinedef, lpfof->byAlpha);
		else if(SidedefExists(lpmap, lpmap->linedefs[lpfof->iLinedef].s1))
			FillTexNameBufferA(lpmap->sidedefs[lpmap->linedefs[lpfof->iLinedef].s1].upper, BLANK_TEXTURE);
	}

	/* Do we need a colourmap? */
	if((lpfof->dwEditFlags & FEF_COLOURMAP))
	{
		if(iColourmap >= 0)
		{
			MAPLINEDEF *lpldCMap;
			MAPSIDEDEF *lpsdCMap;

			/* Create a new control sector for the colourmap if
			 * necessary.
			 */
			if(lpfof->iColourmapLinedef < 0)
			{
				short nColourmapTag = lpsecControl->tag != 0 ? lpsecControl->tag : NextUnusedTag(lpmap);
				int iCMapCSec;

				/* Make the control sector. */
				if(!CreateControlSector(lpmap, &iCMapCSec, &lpfof->iColourmapLinedef, lpcsp, lpdrawop, lpcfgWadOptMap, szSky))
					return FALSE;

				/* Tag them together. */
				lpldCMap = &lpmap->linedefs[lpfof->iColourmapLinedef];
				lpsecControl->tag = lpldCMap->tag = nColourmapTag;
			}
			else lpldCMap = &lpmap->linedefs[lpfof->iColourmapLinedef];

			lpsdCMap = &lpmap->sidedefs[lpldCMap->s1];
			lpldCMap->effect = (short)iColourmap;

			/* Encode the colourmap. */
			lpsdCMap->upper[0] = '#';
			_snprintf(lpsdCMap->upper + 1, 6, "%06X", COLORREFToColourmapColour(lpfof->crefColourmap));
			lpsdCMap->upper[7] = ColourmapCharacterFromAlpha(lpfof->byColourmapAlpha);
		}
	}
	else if(lpfof->iColourmapLinedef >= 0)
	{
		/* Remove any colourmap effect. */
		lpmap->linedefs[lpfof->iColourmapLinedef].effect = LDEFFECT_NONE;
		lpmap->linedefs[lpfof->iColourmapLinedef].tag = 0;
	}

	/* Success! */
	return TRUE;
}


/* IsValidColourmap
 *   Validates a full colourmap string (i.e. with # and alpha).
 *
 * Parameters:
 *   LPCTSTR	szColourmap		Buffer containing the NULL-terminated colourmap
 *								string.
 *
 * Return value: BOOL
 *   TRUE if valid; FALSE otherwise.
 */
BOOL IsValidColourmap(LPCTSTR szColourmap)
{
	int i;
	
	/* Make sure we start with # and are the right length. */
	if(szColourmap[0] != TEXT('#') || _tcslen(szColourmap) != TEXNAME_WAD_BUFFER_LENGTH)
		return FALSE;

	/* Check the hex digits. */
	for(i = 0; i < 6; i++)
	{
		TCHAR tcUpper = _totupper(szColourmap[i + 1]);
		if(!_istdigit(tcUpper) && (tcUpper < 'A' || tcUpper > 'F'))
			return FALSE;
	}

	/* All that remains is to check the alpha. */
	return _istalnum(szColourmap[TEXNAME_WAD_BUFFER_LENGTH - 1]);
}


/* ValidateFOFConfig
 *   Validates the FOF section of a map config.
 *
 * Parameters:
 *   CONFIG*	lpcfgMap	Map config.
 *
 * Return value: BOOL
 *   TRUE if valid; FALSE otherwise.
 */
BOOL ValidateFOFConfig(CONFIG *lpcfgMap)
{
	CONFIG *lpcfgFOFs = ConfigGetSubsection(lpcfgMap, MAPCFG_FOFS);

	/* If we don't have a FOF section at all, then we can't fail. */
	if(lpcfgFOFs)
	{
		CONFIG *lpcfgFlags = ConfigGetSubsection(lpcfgFOFs, MAPCFG_FOF_FLAGS);
		CONFIG *lpcfgTypes = ConfigGetSubsection(lpcfgFOFs, MAPCFG_FOF_TYPES);
		CONFIG *lpcfgDefaultCustom;
		TCHAR szValue[CCH_SIGNED_INT];

		/* Make sure we have both of the main subsections. */
		if(!lpcfgFlags || !lpcfgTypes)
			return FALSE;

		/* We need a default. */
		if(!ConfigNodeExists(lpcfgFOFs, FOFCFG_DEFAULT))
			return FALSE;

		/* Make sure the default is valid. */
		_sntprintf(szValue, NUM_ELEMENTS(szValue), TEXT("%d"), ConfigGetInteger(lpcfgFOFs, FOFCFG_DEFAULT));
		if(!ConfigGetSubsection(lpcfgTypes, szValue))
			return FALSE;

		/* We need a default custom type, too. */
		if(!ConfigNodeExists(lpcfgFOFs, FOFCFG_DEFAULTCUSTOM))
			return FALSE;

		/* Make sure the default custom type is valid. */
		_sntprintf(szValue, NUM_ELEMENTS(szValue), TEXT("%d"), ConfigGetInteger(lpcfgFOFs, FOFCFG_DEFAULTCUSTOM));
		if(!(lpcfgDefaultCustom = ConfigGetSubsection(lpcfgTypes, szValue)) || !ConfigGetInteger(lpcfgDefaultCustom, FOFCFG_EFFECT_CUSTOM))
			return FALSE;
	}

	/* If we get here, we're valid. */
	return TRUE;
}
