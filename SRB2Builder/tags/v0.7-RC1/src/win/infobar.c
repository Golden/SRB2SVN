/* SRB2 Workbench -- A map editor for Sonic Robo Blast 2.
 * Copyright (C) 2009 Gregor Dick
 * http://workbench.srb2.org/
 *
 * Incorporates portions of Doom Builder, (C) 2003 Pascal vd Heiden.
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 * USA.
 *
 * infobar.c: Implements the "Info Bar" -- the window showing selection
 * information.
 *
 * AMDG.
 */

#include <windows.h>
#include <tchar.h>
#include <stdio.h>

#include "../general.h"
#include "../texture.h"
#include "../editing.h"
#include "../config.h"
#include "../options.h"
#include "../../res/resource.h"

#include "mdiframe.h"
#include "infobar.h"
#include "gendlg.h"
#include "editdlg.h"

#include "../DockWnd/DockWnd.h"


/* Macros. */
#define CX_INFOBAR	834
#define CY_INFOBAR	114


/* Types. */

typedef struct _TEXPANEL_DATA
{
	HBITMAP		hbmTex1, hbmTex2, hbmTex3;
	RGBQUAD		rgbq[CB_PLAYPAL / 3];
	char		cNotifyID[3];
} TEXPANEL_DATA;


/* Globals. */
static HWND g_hwndInfoBarDock;
static HWND g_hwndLDPanelH;
static HWND g_hwndSecPanelH;
static HWND g_hwndThingPanelH;
static HWND g_hwndVxPanelH;
static HWND g_hwndSDFront, g_hwndSDBack;
static HWND g_hwndFlats;
static HWND g_hwndThingSprite;
static DWORD g_dwPanels, g_dwPanelMask = 0xFFFFFFFF, g_dwForcePanels;

/* This is the window that has control of the info-bar. */
static HWND g_hwndIBMapWin;


/* Static prototypes. */
static INT_PTR CALLBACK InfoBarDlgProc(HWND hwndDlg, UINT uiMsg, WPARAM wParam, LPARAM lParam);
static INT_PTR CALLBACK TexPreviewBarDlgProc(HWND hwndDlg, UINT uiMsg, WPARAM wParam, LPARAM lParam);
static void SetInfoBarMode(ENUM_EDITMODE emInfoBarMode);
static void WINAPI InfoBarResize(DOCKINFO *lpdi, RECT *lprc);



/* CreateInfoBarWindow
 *   Creates the info-bar tool window.
 *
 * Parameters: None.
 *
 * Return value: BOOL
 *   TRUE on success; FALSE on error.
 *
 * Remarks:
 *   g_hwndMain isn't set yet!
 */
BOOL CreateInfoBarWindow(void)
{
	DOCKINFO *lpdi;
	TCHAR	szCaption[64];
	char	cNotifyIDs[][3] =
	{
		{TP_FRONTUPPER, TP_FRONTMIDDLE, TP_FRONTLOWER},
		{TP_BACKUPPER, TP_BACKMIDDLE, TP_BACKLOWER},
		{TP_CEILING, TP_FLOOR},
		{TP_SPRITE}
	};

	/* General initialisation. */
	g_hwndIBMapWin = NULL;

	/* Allocate a docking structure. This'll be freed automatically by the lib.
	 */
	lpdi = DockingAlloc((char)DWS_DOCKED_BOTTOM, &g_dwc);

	/* Hide the docking container if the user clicks the Close button, rather
	 * than destroying the window. Don't allow resizing. DON'T Destroy the
	 * dialogue when the docking container is destroyed.
	 */
	lpdi->dwStyle |= DWS_NODESTROY | DWS_NORESIZE;
	lpdi->dwStyle &= ~(DWS_ALLOW_DOCKLEFT | DWS_ALLOW_DOCKRIGHT);

	/* Set dimensions. */
	lpdi->cxFloating = CX_INFOBAR;
	lpdi->cyFloating = CY_INFOBAR;
	lpdi->nDockedSize = CY_INFOBAR;

	/* Handles positioning of children. */
	lpdi->DockResize = InfoBarResize;

	/* Create the docking window. */
	LoadString(g_hInstance, IDS_INFOBAR_CAPTION, szCaption, sizeof(szCaption) / sizeof(TCHAR));
	DockingCreateFrame(lpdi, szCaption);

	/* Now that we've got a frame, we can create the windows that will appear
	 * inside it.
	 */

	g_hwndInfoBarDock = lpdi->hwnd;

	/* Information panels. */
	g_hwndLDPanelH = CreateDialog(g_hInstance, MAKEINTRESOURCE(IDD_INFOBAR_LDPANEL), g_hwndInfoBarDock, InfoBarDlgProc);
	g_hwndSecPanelH = CreateDialog(g_hInstance, MAKEINTRESOURCE(IDD_INFOBAR_SECPANEL), g_hwndInfoBarDock, InfoBarDlgProc);
	g_hwndThingPanelH = CreateDialog(g_hInstance, MAKEINTRESOURCE(IDD_INFOBAR_THINGPANEL), g_hwndInfoBarDock, InfoBarDlgProc);
	g_hwndVxPanelH = CreateDialog(g_hInstance, MAKEINTRESOURCE(IDD_INFOBAR_VXPANEL), g_hwndInfoBarDock, InfoBarDlgProc);

	/* Texture panels. */
	g_hwndSDFront = CreateDialogParam(g_hInstance, MAKEINTRESOURCE(IDD_INFOBAR_SDTEXTURES), g_hwndInfoBarDock, TexPreviewBarDlgProc, (LPARAM)cNotifyIDs[0]);
	g_hwndSDBack = CreateDialogParam(g_hInstance, MAKEINTRESOURCE(IDD_INFOBAR_SDTEXTURES), g_hwndInfoBarDock, TexPreviewBarDlgProc, (LPARAM)cNotifyIDs[1]);
	g_hwndFlats = CreateDialogParam(g_hInstance, MAKEINTRESOURCE(IDD_INFOBAR_SECFLATS), g_hwndInfoBarDock, TexPreviewBarDlgProc, (LPARAM)cNotifyIDs[2]);
	g_hwndThingSprite = CreateDialogParam(g_hInstance, MAKEINTRESOURCE(IDD_INFOBAR_THINGSPRITE), g_hwndInfoBarDock, TexPreviewBarDlgProc, (LPARAM)cNotifyIDs[3]);


	/* Set the captions of the sidedef panels, since they came from a generic
	 * template.
	 */
	LoadString(g_hInstance, IDS_INFOBAR_SDFRONTCAPTION, szCaption, sizeof(szCaption) / sizeof(TCHAR));
	SetDlgItemText(g_hwndSDFront, IDC_SD_FRAME, szCaption);
	LoadString(g_hInstance, IDS_INFOBAR_SDBACKCAPTION, szCaption, sizeof(szCaption) / sizeof(TCHAR));
	SetDlgItemText(g_hwndSDBack, IDC_SD_FRAME, szCaption);

	DockingShowFrame(lpdi);

	return TRUE;
}



/* InfoBarDlgProc
 *   Dialogue procedure for the info-bar dialogue.
 *
 * Parameters:
 *   HWND	hwndDlg		Handle to dialogue window.
 *   HWND	uiMsg		Window message ID.
 *   WPARAM	wParam		Message-specific.
 *   LPARAM lParam		Message-specific.
 *
 * Return value: INT_PTR
 *   TRUE if message was processed; FALSE otherwise.
 *
 * Remarks:
 *   This is the procedure for the windows *inside* the docking container.
 */
static INT_PTR CALLBACK InfoBarDlgProc(HWND hwndDlg, UINT uiMsg, WPARAM wParam, LPARAM lParam)
{
	UNREFERENCED_PARAMETER(hwndDlg);
	UNREFERENCED_PARAMETER(wParam);
	UNREFERENCED_PARAMETER(lParam);
	switch(uiMsg)
	{
	case WM_INITDIALOG:
		/* Let the system set the focus. */
		return TRUE;

	case WM_ACTIVATE:
		if(wParam == WA_INACTIVE)
			g_hwndLastModelessDialogue = NULL;
		else g_hwndLastModelessDialogue = hwndDlg;

		/* Allow default processing. */
		return FALSE;

	case WM_NCHITTEST:
		{
			/* Get default HT result. */
			LRESULT lrHT = DefWindowProc(hwndDlg, uiMsg, wParam, lParam);

			/* If we're in the client are, fall through to the parent. */
			SetWindowLong(hwndDlg, DWL_MSGRESULT, (lrHT == HTCLIENT) ? HTTRANSPARENT : lrHT);
		}

		return TRUE;
	}

	return FALSE;
}



/* TexPreviewBarDlgProc
 *   Dialogue procedure for the info-bar panels that show texture previews.
 *
 * Parameters:
 *   HWND	hwndDlg		Handle to dialogue window.
 *   HWND	uiMsg		Window message ID.
 *   WPARAM	wParam		Message-specific.
 *   LPARAM lParam		Message-specific.
 *
 * Return value: INT_PTR
 *   TRUE if message was processed; FALSE otherwise.
 *
 * Remarks:
 *   Delegates to InfoBarDlgProc for most things. The messages to send to the
 *   MDI window when the previews are clicked is determined by the creation
 *   parameters. Also used for thing sprite previews, even if they aren't
 *   strictly textures.
 */
static INT_PTR CALLBACK TexPreviewBarDlgProc(HWND hwndDlg, UINT uiMsg, WPARAM wParam, LPARAM lParam)
{
	TEXPANEL_DATA *lptpd;

	switch(uiMsg)
	{
	case WM_INITDIALOG:
		{
			HWND hwndPreviews[] = {GetDlgItem(hwndDlg, IDC_TEX1), GetDlgItem(hwndDlg, IDC_TEX2), GetDlgItem(hwndDlg, IDC_TEX3)};
			TPPDATA *lptppdata;
			int i;

			/* Allocate data particular to this dialogue. */
			lptpd = ProcHeapAlloc(sizeof(TEXPANEL_DATA));
			SetWindowLong(hwndDlg, GWL_USERDATA, (LONG)lptpd);

			/* Initialise bitmaps to NULL. */
			lptpd->hbmTex1 = lptpd->hbmTex2 = lptpd->hbmTex3 = NULL;

			/* Get IDs for previews. */
			CopyMemory(lptpd->cNotifyID, (char*)lParam, sizeof(lptpd->cNotifyID) / sizeof(char));

			/* Subclass the previews. */
			for(i = 0; i < (int)(sizeof(hwndPreviews)/sizeof(HWND)); i++)
			{
				if(IsWindow(hwndPreviews[i]))
				{
					lptppdata = ProcHeapAlloc(sizeof(TPPDATA));
					lptppdata->wndprocStatic = (WNDPROC)GetWindowLong(hwndPreviews[i], GWL_WNDPROC);
					SetWindowLong(hwndPreviews[i], GWL_USERDATA, (LONG)lptppdata);
					SetWindowLong(hwndPreviews[i], GWL_WNDPROC, (LONG)TexPreviewProc);
				}
			}

			/* The info-bar proc might want to do some things. */
			break;
		}


	case WM_INFOBAR_INITTEXBITMAPS:
		/* wParam		DWORD		Mask identifying required images.
		 *								Bit 0	...	IDC_TEX1
		 *								Bit 1	...	IDC_TEX2
		 *								Bit 2	...	IDC_TEX3
		 * lParam		RGBQUAD*	Palette data.
		 */

		{
			HDC hdc = GetDC(hwndDlg);
			lptpd = (TEXPANEL_DATA*)GetWindowLong(hwndDlg, GWL_USERDATA);

			if(wParam & 1)
			{
				if(lptpd->hbmTex1) DeleteObject(lptpd->hbmTex1);
				lptpd->hbmTex1 = CreateCompatibleBitmap(hdc, CX_TEXPREVIEW, CY_TEXPREVIEW);
				SendDlgItemMessage(hwndDlg, IDC_TEX1, STM_SETIMAGE, IMAGE_BITMAP, (LPARAM)lptpd->hbmTex1);
			}

			if(wParam & 2)
			{
				if(lptpd->hbmTex2) DeleteObject(lptpd->hbmTex2);
				lptpd->hbmTex2 = CreateCompatibleBitmap(hdc, CX_TEXPREVIEW, CY_TEXPREVIEW);
				SendDlgItemMessage(hwndDlg, IDC_TEX2, STM_SETIMAGE, IMAGE_BITMAP, (LPARAM)lptpd->hbmTex2);
			}

			if(wParam & 4)
			{
				if(lptpd->hbmTex3) DeleteObject(lptpd->hbmTex3);
				lptpd->hbmTex3 = CreateCompatibleBitmap(hdc, CX_TEXPREVIEW, CY_TEXPREVIEW);
				SendDlgItemMessage(hwndDlg, IDC_TEX3, STM_SETIMAGE, IMAGE_BITMAP, (LPARAM)lptpd->hbmTex3);
			}

			ReleaseDC(hwndDlg, hdc);

			/* Store the palette. */
			CopyMemory(lptpd->rgbq, (RGBQUAD*)lParam, sizeof(lptpd->rgbq));
		}

		/* InfoBarProc doesn't care about this message. */
		return TRUE;


	case WM_INFOBAR_SETTEXPREVIEW:
		/* LOWORD(wParam)	short		Index of preview bitmap to set.
		 * HIWORD(wParam)	BOOL		Denotes nature of lParam.
		 * lParam			[Various]	Texture structure containing bits etc.
		 *								if HIWORD(wParam) is TRUE; a bitmap ID
		 *								if it is FALSE.
		 */

		{
			HBITMAP	hbmTarget = NULL;
			HDC		hdcTarget;

			/* Get window data. */
			lptpd = (TEXPANEL_DATA*)GetWindowLong(hwndDlg, GWL_USERDATA);

			/* Get the bitmap handle for the specified preview image. */
			switch(LOWORD(wParam))
			{
			case 1:
				hbmTarget = lptpd->hbmTex1;
				InvalidateRect(GetDlgItem(hwndDlg, IDC_TEX1), NULL, FALSE);
				break;
			case 2:
				hbmTarget = lptpd->hbmTex2;
				InvalidateRect(GetDlgItem(hwndDlg, IDC_TEX2), NULL, FALSE);
				break;
			case 3:
				hbmTarget = lptpd->hbmTex3;
				InvalidateRect(GetDlgItem(hwndDlg, IDC_TEX3), NULL, FALSE);
				break;
			}

			/* Create a device context to draw onto. */
			hdcTarget = CreateCompatibleDC(NULL);
			SelectObject(hdcTarget, hbmTarget);

			/* Blit the specified bitmap to the target. How we do this depends
			 * on the nature of the bitmap passed in.
			 */
			if(HIWORD(wParam))
			{
				/* lParam is a texture structure. */
				TEXTURE *lptex = (TEXTURE*)lParam;
				StretchTextureToDC(lptex, hdcTarget, CX_TEXPREVIEW, CY_TEXPREVIEW, STT_MASKED);
			}
			else
			{
				/* lParam is a resource ID. */
				HBITMAP hbmSource = LoadBitmap(g_hInstance, MAKEINTRESOURCE(lParam));
				HDC		hdcSource = CreateCompatibleDC(NULL);

				SelectObject(hdcSource, hbmSource);

				/* We don't stretch resource bitmaps. */
				BitBlt(hdcTarget, 0, 0, CX_TEXPREVIEW, CY_TEXPREVIEW, hdcSource, 0, 0, SRCCOPY);

				DeleteDC(hdcSource);
				DeleteObject(hbmSource);
			}

			DeleteDC(hdcTarget);
		}

		/* InfoBarProc doesn't care about this message. */
		return TRUE;

	case WM_COMMAND:
		switch(LOWORD(wParam))
		{
		case IDC_TEX1:
		case IDC_TEX2:
		case IDC_TEX3:
			{
				TEXPANEL_DATA *lptpd = (TEXPANEL_DATA*)GetWindowLong(hwndDlg, GWL_USERDATA);
				int iIDIndex = (LOWORD(wParam) == IDC_TEX1) ? 0 : ((LOWORD(wParam) == IDC_TEX2) ? 1 : 2);

				HWND hwndChild = (HWND)SendMessage(g_hwndClient, WM_MDIGETACTIVE, 0, 0);
				if(IsWindow(hwndChild)) TexPreviewClicked(hwndChild, lptpd->cNotifyID[iIDIndex]);

				return TRUE;
			}
		}

		break;

	case WM_DESTROY:
		/* Clean up our data. */
		lptpd = (TEXPANEL_DATA*)GetWindowLong(hwndDlg, GWL_USERDATA);

		if(lptpd->hbmTex1) DeleteObject(lptpd->hbmTex1);
		if(lptpd->hbmTex2) DeleteObject(lptpd->hbmTex2);
		if(lptpd->hbmTex3) DeleteObject(lptpd->hbmTex3);

		ProcHeapFree(lptpd);
	}

	/* Delegate to the generic dialogue-procedure for info-bar children. */
	return InfoBarDlgProc(hwndDlg, uiMsg, wParam, lParam);
}




/* InfoBarResize
 *   Handles layout of info-bar's child controls.
 *
 * Parameters:
 *   DOCKINFO*		lpdi	Information about the dock window.
 *   RECT*			lprc	Rectangle describing client area of dock window.
 *
 * Return value: None.
 *
 * Remarks:
 *   Called by the docking lib whenever the docking window is resized.
 */
static void WINAPI InfoBarResize(DOCKINFO *lpdi, RECT *lprc)
{
	unsigned short cxSD, cxFlats, cxSprite;
	int xCumulativeLeft = lprc->left;
	RECT rc;

	UNREFERENCED_PARAMETER(lpdi);

	/* Align info frames to the right of the grip. */
	SetWindowPos(g_hwndLDPanelH, NULL, xCumulativeLeft, lprc->top, 0, 0, SWP_NOZORDER|SWP_NOSIZE);
	SetWindowPos(g_hwndSecPanelH, NULL, xCumulativeLeft, lprc->top, 0, 0, SWP_NOZORDER|SWP_NOSIZE);
	SetWindowPos(g_hwndThingPanelH, NULL, xCumulativeLeft, lprc->top, 0, 0, SWP_NOZORDER|SWP_NOSIZE);
	SetWindowPos(g_hwndVxPanelH, NULL, xCumulativeLeft, lprc->top, 0, 0, SWP_NOZORDER|SWP_NOSIZE);

	/* Now we check which texture frames we ought to be displaying, and show
	 * them in a row. We need to find their widths, first.
	 */
	GetWindowRect(g_hwndSDBack, &rc);
	cxSD = (unsigned short)(rc.right - rc.left);
	GetWindowRect(g_hwndFlats, &rc);
	cxFlats = (unsigned short)(rc.right - rc.left);
	GetWindowRect(g_hwndThingSprite, &rc);
	cxSprite = (unsigned short)(rc.right - rc.left);

	/* Keep track of the LHS of the next window to display. */
	if(g_dwPanels & IBPF_ALLINFO)
	{
		GetWindowRect(g_hwndLDPanelH, &rc);
		xCumulativeLeft += (unsigned short)(rc.right - rc.left);
	}

	if(g_dwPanels & IBPF_SIDEDEF)
	{
		SetWindowPos(g_hwndSDFront, NULL, xCumulativeLeft, lprc->top, 0, 0, SWP_NOZORDER|SWP_NOSIZE);
		SetWindowPos(g_hwndSDBack,  NULL, xCumulativeLeft + cxSD, lprc->top, 0, 0, SWP_NOZORDER|SWP_NOSIZE);

		InvalidateRect(g_hwndSDFront, NULL, TRUE);
		InvalidateRect(g_hwndSDBack, NULL, TRUE);

		xCumulativeLeft += 2*cxSD;
	}

	if(g_dwPanels & IBPF_FLAT)
	{
		SetWindowPos(g_hwndFlats, NULL, xCumulativeLeft, lprc->top, 0, 0, SWP_NOZORDER|SWP_NOSIZE);

		InvalidateRect(g_hwndFlats, NULL, TRUE);

		xCumulativeLeft += cxFlats;
	}

	if(g_dwPanels & IBPF_SPRITE)
	{
		SetWindowPos(g_hwndThingSprite, NULL, xCumulativeLeft, lprc->top, 0, 0, SWP_NOZORDER|SWP_NOSIZE);

		InvalidateRect(g_hwndThingSprite, NULL, TRUE);

		xCumulativeLeft += cxSprite;
	}
}


/* DestroyInfoBarWindow
 *   Destroys the info-bar window.
 *
 * Parameters:
 *   None.
 *
 * Return value: None.
 *
 * Remarks:
 *   This destroys the docking container and the dialogues themselves. Since we
 *   have different vertical and horizontal dialogues, we have to do it
 *   manually.
 */
void DestroyInfoBarWindow(void)
{
	if(IsWindow(g_hwndInfoBarDock)) DestroyWindow(g_hwndInfoBarDock);
	if(IsWindow(g_hwndLDPanelH)) DestroyWindow(g_hwndLDPanelH);
	if(IsWindow(g_hwndSecPanelH)) DestroyWindow(g_hwndSecPanelH);
	if(IsWindow(g_hwndThingPanelH)) DestroyWindow(g_hwndSecPanelH);
}



/* ShowLinesInfo
 *   Displays linedef information in the info-bar.
 *
 * Parameters:
 *   LINEDEFDISPLAYINFO*	lplddi					Information to display.
 *   DWORD					dwDisplayFlags			Flags determining what info
 *													to show.
 *   BYTE					byTexRequirementFlags	Flags indicating which
 *													textures are required.
 *
 * Return value: None.
 *
 * Remarks:
 *   Shows the correct frames, but doesn't mess with positioning.
 */
void ShowLinesInfo(LINEDEFDISPLAYINFO *lplddi, DWORD dwDisplayFlags, BYTE byTexRequirementFlags)
{
	TCHAR szBuffer[64];
	TCHAR szFromTable[64];

	/* Show the correct frames. */
	if(g_dwPanels & g_dwPanelMask & IBPF_LINEDEF)
	{
		ShowWindow(g_hwndLDPanelH, SW_SHOW);
		InfoBarShowPanels(IBPF_SECTOR | IBPF_THING | IBPF_VERTEX, FALSE);
	}

	if(g_dwPanels & g_dwPanelMask & IBPF_SIDEDEF)
	{
		/* The logic for whether to show a sidedef panel is a little involved.
		 * In short, we hide the panel for a sidedef that doesn't exist, unless
		 * we're in Any Object mode, when we never hide anything.
		 */

		if((lplddi->bHasFront && (dwDisplayFlags & LDDIF_FRONTSD)) || (g_dwForcePanels & IBPF_SIDEDEF)) ShowWindow(g_hwndSDFront, SW_SHOW);
		else ShowWindow(g_hwndSDFront, SW_HIDE);

		if((lplddi->bHasBack && (dwDisplayFlags & LDDIF_BACKSD)) || (g_dwForcePanels & IBPF_SIDEDEF)) ShowWindow(g_hwndSDBack, SW_SHOW);
		else ShowWindow(g_hwndSDBack, SW_HIDE);
	}


	/* Frame caption. */
	if(dwDisplayFlags & LDDIF_INDEX)
	{
		LoadString(g_hInstance, IDS_INFOBAR_LDCAP, szFromTable, sizeof(szFromTable) / sizeof(TCHAR));
		_sntprintf(szBuffer, sizeof(szBuffer) / sizeof(TCHAR) - 1, szFromTable, lplddi->iIndex);
		szBuffer[sizeof(szBuffer) / sizeof(TCHAR) - 1] = '\0';
	}
	else
	{
		/* Multiple linedefs. */
		LoadString(g_hInstance, IDS_INFOBAR_LDCAPMULTI, szBuffer, sizeof(szBuffer) / sizeof(TCHAR));
	}

	SetDlgItemText(g_hwndLDPanelH, IDC_LD_FRAME, szBuffer);


	/* Effect. */
	if(dwDisplayFlags & LDDIF_EFFECT)
		SetDlgItemText(g_hwndLDPanelH, IDC_LD_EFFECT, lplddi->szEffect);
	else SetDlgItemText(g_hwndLDPanelH, IDC_LD_EFFECT, TEXT(""));


	/* Vector. */
	if(dwDisplayFlags & LDDIF_VECTOR)
		_sntprintf(szBuffer, sizeof(szBuffer) / sizeof(TCHAR), TEXT("(%d, %d)"), lplddi->cxVector, lplddi->cyVector);
	else
		_tcscpy(szBuffer, TEXT(""));
	SetDlgItemText(g_hwndLDPanelH, IDC_LD_VECTOR, szBuffer);


	/* Length. */
	if(dwDisplayFlags & LDDIF_LENGTH)
		SetDlgItemInt(g_hwndLDPanelH, IDC_LD_LENGTH, (unsigned int)ROUND(sqrt(lplddi->cxVector * lplddi->cxVector + lplddi->cyVector * lplddi->cyVector)), FALSE);
	else
		SetDlgItemText(g_hwndLDPanelH, IDC_LD_LENGTH, TEXT(""));


	/* Tag. */
	if(dwDisplayFlags & LDDIF_TAG)
		SetDlgItemInt(g_hwndLDPanelH, IDC_LD_TAG, lplddi->unTag, FALSE);
	else
		SetDlgItemText(g_hwndLDPanelH, IDC_LD_TAG, TEXT(""));


	/* Front sector index. */
	if(dwDisplayFlags & LDDIF_FRONTSEC)
	{
		if(lplddi->bHasFrontSec)
			SetDlgItemInt(g_hwndLDPanelH, IDC_LD_FRONTSEC, lplddi->unFrontSector, FALSE);
		else
			SetDlgItemText(g_hwndLDPanelH, IDC_LD_FRONTSEC, TEXT("-"));
	}
	else
		SetDlgItemText(g_hwndLDPanelH, IDC_LD_FRONTSEC, TEXT(""));


	/* Back sector index. */
	if(dwDisplayFlags & LDDIF_BACKSEC)
	{
		if(lplddi->bHasBackSec)
			SetDlgItemInt(g_hwndLDPanelH, IDC_LD_BACKSEC, lplddi->unBackSector, FALSE);
		else
			SetDlgItemText(g_hwndLDPanelH, IDC_LD_BACKSEC, TEXT("-"));
	}
	else
		SetDlgItemText(g_hwndLDPanelH, IDC_LD_BACKSEC, TEXT(""));


	/* Front sector height. */
	if(dwDisplayFlags & LDDIF_FRONTHEIGHT)
	{
		if(lplddi->bHasFrontSec)
			SetDlgItemInt(g_hwndLDPanelH, IDC_LD_FRONTHEIGHT, lplddi->iFrontHeight, TRUE);
		else
			SetDlgItemText(g_hwndLDPanelH, IDC_LD_FRONTHEIGHT, TEXT("-"));
	}
	else
		SetDlgItemText(g_hwndLDPanelH, IDC_LD_FRONTHEIGHT, TEXT(""));


	/* Back sector height. */
	if(dwDisplayFlags & LDDIF_BACKHEIGHT)
	{
		if(lplddi->bHasBackSec)
			SetDlgItemInt(g_hwndLDPanelH, IDC_LD_BACKHEIGHT, lplddi->iBackHeight, TRUE);
		else
			SetDlgItemText(g_hwndLDPanelH, IDC_LD_BACKHEIGHT, TEXT("-"));
	}
	else
		SetDlgItemText(g_hwndLDPanelH, IDC_LD_BACKHEIGHT, TEXT(""));


	/* Front X offset. */
	if(dwDisplayFlags & LDDIF_FRONTX)
	{
		if(lplddi->bHasFront)
			SetDlgItemInt(g_hwndLDPanelH, IDC_LD_FRONTX, lplddi->nFrontX, TRUE);
		else
			SetDlgItemText(g_hwndLDPanelH, IDC_LD_FRONTX, TEXT("-"));
	}
	else
		SetDlgItemText(g_hwndLDPanelH, IDC_LD_FRONTX, TEXT(""));


	/* Back X offset. */
	if(dwDisplayFlags & LDDIF_BACKX)
	{
		if(lplddi->bHasBack)
			SetDlgItemInt(g_hwndLDPanelH, IDC_LD_BACKX, lplddi->nBackX, TRUE);
		else
			SetDlgItemText(g_hwndLDPanelH, IDC_LD_BACKX, TEXT("-"));
	}
	else
		SetDlgItemText(g_hwndLDPanelH, IDC_LD_BACKX, TEXT(""));


	/* Front Y offset. */
	if(dwDisplayFlags & LDDIF_FRONTY)
	{
		if(lplddi->bHasFront)
			SetDlgItemInt(g_hwndLDPanelH, IDC_LD_FRONTY, lplddi->nFrontY, TRUE);
		else
			SetDlgItemText(g_hwndLDPanelH, IDC_LD_FRONTY, TEXT("-"));
	}
	else
		SetDlgItemText(g_hwndLDPanelH, IDC_LD_FRONTY, TEXT(""));


	/* Back Y offset. */
	if(dwDisplayFlags & LDDIF_BACKY)
	{
		if(lplddi->bHasBack)
			SetDlgItemInt(g_hwndLDPanelH, IDC_LD_BACKY, lplddi->nBackY, TRUE);
		else
			SetDlgItemText(g_hwndLDPanelH, IDC_LD_BACKY, TEXT("-"));
	}
	else
		SetDlgItemText(g_hwndLDPanelH, IDC_LD_BACKY, TEXT(""));


	/* Front sidedef. */

	if((dwDisplayFlags & LDDIF_FRONTSD) && (dwDisplayFlags & LDDIF_FRONTUPPER) && lplddi->bHasFront)
	{
		SetDlgItemText(g_hwndSDFront, IDC_TEXNAME1, lplddi->szFrontUpper);

		if(lplddi->lptexFrontUpper)
			SendMessage(g_hwndSDFront, WM_INFOBAR_SETTEXPREVIEW, MAKEWPARAM(1, TRUE), (LPARAM)lplddi->lptexFrontUpper);
		else if(lplddi->dwBlankTexFlags & LDDIF_FRONTUPPER && byTexRequirementFlags & LDTF_FRONTUPPER)
			SendMessage(g_hwndSDFront, WM_INFOBAR_SETTEXPREVIEW, MAKEWPARAM(1, FALSE), IDB_MISSINGTEXTURE);
		else if(lplddi->dwPseudoTexFlags & LDDIF_FRONTUPPER || lplddi->dwBlankTexFlags & LDDIF_FRONTUPPER)
			SendMessage(g_hwndSDFront, WM_INFOBAR_SETTEXPREVIEW, MAKEWPARAM(1, FALSE), IDB_NOTEXTURE);
		else
			SendMessage(g_hwndSDFront, WM_INFOBAR_SETTEXPREVIEW, MAKEWPARAM(1, FALSE), IDB_UNKNOWNTEXTURE);
	}
	else
	{
		SetDlgItemText(g_hwndSDFront, IDC_TEXNAME1, TEXT(""));
		SendMessage(g_hwndSDFront, WM_INFOBAR_SETTEXPREVIEW, MAKEWPARAM(1, FALSE), IDB_NOTEXTURE);
	}


	if((dwDisplayFlags & LDDIF_FRONTSD) && (dwDisplayFlags & LDDIF_FRONTMIDDLE) && lplddi->bHasFront)
	{
		SetDlgItemText(g_hwndSDFront, IDC_TEXNAME2, lplddi->szFrontMiddle);

		if(lplddi->lptexFrontMiddle)
			SendMessage(g_hwndSDFront, WM_INFOBAR_SETTEXPREVIEW, MAKEWPARAM(2, TRUE), (LPARAM)lplddi->lptexFrontMiddle);
		else if(lplddi->dwBlankTexFlags & LDDIF_FRONTMIDDLE && byTexRequirementFlags & LDTF_FRONTMIDDLE)
			SendMessage(g_hwndSDFront, WM_INFOBAR_SETTEXPREVIEW, MAKEWPARAM(2, FALSE), IDB_MISSINGTEXTURE);
		else if(lplddi->dwPseudoTexFlags & LDDIF_FRONTMIDDLE || lplddi->dwBlankTexFlags & LDDIF_FRONTMIDDLE)
			SendMessage(g_hwndSDFront, WM_INFOBAR_SETTEXPREVIEW, MAKEWPARAM(2, FALSE), IDB_NOTEXTURE);
		else
			SendMessage(g_hwndSDFront, WM_INFOBAR_SETTEXPREVIEW, MAKEWPARAM(2, FALSE), IDB_UNKNOWNTEXTURE);
	}
	else
	{
		SetDlgItemText(g_hwndSDFront, IDC_TEXNAME2, TEXT(""));
		SendMessage(g_hwndSDFront, WM_INFOBAR_SETTEXPREVIEW, MAKEWPARAM(2, FALSE), IDB_NOTEXTURE);
	}


	if((dwDisplayFlags & LDDIF_FRONTSD) && (dwDisplayFlags & LDDIF_FRONTLOWER) && lplddi->bHasFront)
	{
		SetDlgItemText(g_hwndSDFront, IDC_TEXNAME3, lplddi->szFrontLower);

		if(lplddi->lptexFrontLower)
			SendMessage(g_hwndSDFront, WM_INFOBAR_SETTEXPREVIEW, MAKEWPARAM(3, TRUE), (LPARAM)lplddi->lptexFrontLower);
		else if(lplddi->dwBlankTexFlags & LDDIF_FRONTLOWER && byTexRequirementFlags & LDTF_FRONTLOWER)
			SendMessage(g_hwndSDFront, WM_INFOBAR_SETTEXPREVIEW, MAKEWPARAM(3, FALSE), IDB_MISSINGTEXTURE);
		else if(lplddi->dwPseudoTexFlags & LDDIF_FRONTLOWER || lplddi->dwBlankTexFlags & LDDIF_FRONTLOWER)
			SendMessage(g_hwndSDFront, WM_INFOBAR_SETTEXPREVIEW, MAKEWPARAM(3, FALSE), IDB_NOTEXTURE);
		else
			SendMessage(g_hwndSDFront, WM_INFOBAR_SETTEXPREVIEW, MAKEWPARAM(3, FALSE), IDB_UNKNOWNTEXTURE);
	}
	else
	{
		SetDlgItemText(g_hwndSDFront, IDC_TEXNAME3, TEXT(""));
		SendMessage(g_hwndSDFront, WM_INFOBAR_SETTEXPREVIEW, MAKEWPARAM(3, FALSE), IDB_NOTEXTURE);
	}


	/* Back sidedef. */

	if((dwDisplayFlags & LDDIF_FRONTSD) && (dwDisplayFlags & LDDIF_BACKUPPER) && lplddi->bHasBack)
	{
		SetDlgItemText(g_hwndSDBack, IDC_TEXNAME1, lplddi->szBackUpper);

		if(lplddi->lptexBackUpper)
			SendMessage(g_hwndSDBack, WM_INFOBAR_SETTEXPREVIEW, MAKEWPARAM(1, TRUE), (LPARAM)lplddi->lptexBackUpper);
		else if(lplddi->dwBlankTexFlags & LDDIF_BACKUPPER && byTexRequirementFlags & LDTF_BACKUPPER)
			SendMessage(g_hwndSDBack, WM_INFOBAR_SETTEXPREVIEW, MAKEWPARAM(1, FALSE), IDB_MISSINGTEXTURE);
		else if(lplddi->dwPseudoTexFlags & LDDIF_BACKUPPER || lplddi->dwBlankTexFlags & LDDIF_BACKUPPER)
			SendMessage(g_hwndSDBack, WM_INFOBAR_SETTEXPREVIEW, MAKEWPARAM(1, FALSE), IDB_NOTEXTURE);
		else
			SendMessage(g_hwndSDBack, WM_INFOBAR_SETTEXPREVIEW, MAKEWPARAM(1, FALSE), IDB_UNKNOWNTEXTURE);
	}
	else
	{
		SetDlgItemText(g_hwndSDBack, IDC_TEXNAME1, TEXT(""));
		SendMessage(g_hwndSDBack, WM_INFOBAR_SETTEXPREVIEW, MAKEWPARAM(1, FALSE), IDB_NOTEXTURE);
	}


	if((dwDisplayFlags & LDDIF_FRONTSD) && (dwDisplayFlags & LDDIF_BACKMIDDLE) && lplddi->bHasBack)
	{
		SetDlgItemText(g_hwndSDBack, IDC_TEXNAME2, lplddi->szBackMiddle);

		if(lplddi->lptexBackMiddle)
			SendMessage(g_hwndSDBack, WM_INFOBAR_SETTEXPREVIEW, MAKEWPARAM(2, TRUE), (LPARAM)lplddi->lptexBackMiddle);
		else if(lplddi->dwBlankTexFlags & LDDIF_BACKMIDDLE && byTexRequirementFlags & LDTF_BACKMIDDLE)
			SendMessage(g_hwndSDBack, WM_INFOBAR_SETTEXPREVIEW, MAKEWPARAM(2, FALSE), IDB_MISSINGTEXTURE);
		else if(lplddi->dwPseudoTexFlags & LDDIF_BACKMIDDLE || lplddi->dwBlankTexFlags & LDDIF_BACKMIDDLE)
			SendMessage(g_hwndSDBack, WM_INFOBAR_SETTEXPREVIEW, MAKEWPARAM(2, FALSE), IDB_NOTEXTURE);
		else
			SendMessage(g_hwndSDBack, WM_INFOBAR_SETTEXPREVIEW, MAKEWPARAM(2, FALSE), IDB_UNKNOWNTEXTURE);
	}
	else
	{
		SetDlgItemText(g_hwndSDBack, IDC_TEXNAME2, TEXT(""));
		SendMessage(g_hwndSDBack, WM_INFOBAR_SETTEXPREVIEW, MAKEWPARAM(2, FALSE), IDB_NOTEXTURE);
	}


	if((dwDisplayFlags & LDDIF_FRONTSD) && (dwDisplayFlags & LDDIF_BACKLOWER) && lplddi->bHasBack)
	{
		SetDlgItemText(g_hwndSDBack, IDC_TEXNAME3, lplddi->szBackLower);

		if(lplddi->lptexBackLower)
			SendMessage(g_hwndSDBack, WM_INFOBAR_SETTEXPREVIEW, MAKEWPARAM(3, TRUE), (LPARAM)lplddi->lptexBackLower);
		else if(lplddi->dwBlankTexFlags & LDDIF_BACKLOWER && byTexRequirementFlags & LDTF_BACKLOWER)
			SendMessage(g_hwndSDBack, WM_INFOBAR_SETTEXPREVIEW, MAKEWPARAM(3, FALSE), IDB_MISSINGTEXTURE);
		else if(lplddi->dwPseudoTexFlags & LDDIF_BACKLOWER || lplddi->dwBlankTexFlags & LDDIF_BACKLOWER)
			SendMessage(g_hwndSDBack, WM_INFOBAR_SETTEXPREVIEW, MAKEWPARAM(3, FALSE), IDB_NOTEXTURE);
		else
			SendMessage(g_hwndSDBack, WM_INFOBAR_SETTEXPREVIEW, MAKEWPARAM(3, FALSE), IDB_UNKNOWNTEXTURE);
	}
	else
	{
		SetDlgItemText(g_hwndSDBack, IDC_TEXNAME3, TEXT(""));
		SendMessage(g_hwndSDBack, WM_INFOBAR_SETTEXPREVIEW, MAKEWPARAM(3, FALSE), IDB_NOTEXTURE);
	}
}


/* ShowSectorInfo
 *   Displays sector information in the info-bar.
 *
 * Parameters:
 *   SECTORDISPLAYINFO*	lpsdi			Information to display.
 *   DWORD				dwDisplayFlags	Flags determining what info to show.
 *
 * Return value: None.
 *
 * Remarks:
 *   Shows the correct frames, but doesn't mess with positioning.
 */
void ShowSectorInfo(SECTORDISPLAYINFO *lpsdi, DWORD dwDisplayFlags)
{
	TCHAR szBuffer[64];
	TCHAR szFromTable[64];

	/* Show the correct frames. */
	if(g_dwPanels & g_dwPanelMask & IBPF_SECTOR)
	{
		ShowWindow(g_hwndSecPanelH, SW_SHOW);
		InfoBarShowPanels(IBPF_LINEDEF | IBPF_THING | IBPF_VERTEX, FALSE);
	}
	if(g_dwPanels & g_dwPanelMask & IBPF_FLAT)
	{
		ShowWindow(g_hwndFlats, SW_SHOW);
	}


	/* Frame caption. */
	if(dwDisplayFlags & SDIF_INDEX)
	{
		LoadString(g_hInstance, IDS_INFOBAR_SCAP, szFromTable, sizeof(szFromTable) / sizeof(TCHAR));
		_sntprintf(szBuffer, sizeof(szBuffer) / sizeof(TCHAR) - 1, szFromTable, lpsdi->iIndex);
		szBuffer[sizeof(szBuffer) / sizeof(TCHAR) - 1] = '\0';
	}
	else
	{
		/* Multiple sectors. */
		LoadString(g_hInstance, IDS_INFOBAR_SCAPMULTI, szBuffer, sizeof(szBuffer) / sizeof(TCHAR));
	}

	SetDlgItemText(g_hwndSecPanelH, IDC_SEC_FRAME, szBuffer);


	/* Effect. */
	if(dwDisplayFlags & SDIF_EFFECT)
		SetDlgItemText(g_hwndSecPanelH, IDC_SEC_EFFECT, lpsdi->szEffect);
	else SetDlgItemText(g_hwndSecPanelH, IDC_SEC_EFFECT, TEXT(""));


	/* Ceiling. */
	if(dwDisplayFlags & SDIF_CEILING)
		SetDlgItemInt(g_hwndSecPanelH, IDC_SEC_CEILING, lpsdi->nCeiling, TRUE);
	else
		SetDlgItemText(g_hwndSecPanelH, IDC_SEC_CEILING, TEXT(""));


	/* Floor. */
	if(dwDisplayFlags & SDIF_FLOOR)
		SetDlgItemInt(g_hwndSecPanelH, IDC_SEC_FLOOR, lpsdi->nFloor, TRUE);
	else
		SetDlgItemText(g_hwndSecPanelH, IDC_SEC_FLOOR, TEXT(""));


	/* Height. */
	if(dwDisplayFlags & SDIF_HEIGHT)
		SetDlgItemInt(g_hwndSecPanelH, IDC_SEC_HEIGHT, lpsdi->nCeiling - lpsdi->nFloor, TRUE);
	else
		SetDlgItemText(g_hwndSecPanelH, IDC_SEC_HEIGHT, TEXT(""));


	/* Brightness. */
	if(dwDisplayFlags & SDIF_BRIGHTNESS)
		SetDlgItemInt(g_hwndSecPanelH, IDC_SEC_BRIGHTNESS, lpsdi->ucBrightness, FALSE);
	else
		SetDlgItemText(g_hwndSecPanelH, IDC_SEC_BRIGHTNESS, TEXT(""));


	/* Tag. */
	if(dwDisplayFlags & SDIF_TAG)
		SetDlgItemInt(g_hwndSecPanelH, IDC_SEC_TAG, lpsdi->unTag, FALSE);
	else
		SetDlgItemText(g_hwndSecPanelH, IDC_SEC_TAG, TEXT(""));


	/* Ceiling texture. */
	if(dwDisplayFlags & SDIF_CEILINGTEX)
	{
		SetDlgItemText(g_hwndFlats, IDC_TEXNAME1, lpsdi->szCeiling);

		if(lpsdi->lptexCeiling)
			SendMessage(g_hwndFlats, WM_INFOBAR_SETTEXPREVIEW, MAKEWPARAM(1, TRUE), (LPARAM)lpsdi->lptexCeiling);
		else
			SendMessage(g_hwndFlats, WM_INFOBAR_SETTEXPREVIEW, MAKEWPARAM(1, FALSE), IDB_UNKNOWNFLAT);
	}
	else
	{
		SetDlgItemText(g_hwndFlats, IDC_TEXNAME1, TEXT(""));
		SendMessage(g_hwndFlats, WM_INFOBAR_SETTEXPREVIEW, MAKEWPARAM(1, FALSE), IDB_NOTEXTURE);
	}


	/* Floor texture. */
	if(dwDisplayFlags & SDIF_FLOORTEX)
	{
		SetDlgItemText(g_hwndFlats, IDC_TEXNAME2, lpsdi->szFloor);

		if(lpsdi->lptexFloor)
			SendMessage(g_hwndFlats, WM_INFOBAR_SETTEXPREVIEW, MAKEWPARAM(2, TRUE), (LPARAM)lpsdi->lptexFloor);
		else
			SendMessage(g_hwndFlats, WM_INFOBAR_SETTEXPREVIEW, MAKEWPARAM(2, FALSE), IDB_UNKNOWNFLAT);
	}
	else
	{
		SetDlgItemText(g_hwndFlats, IDC_TEXNAME2, TEXT(""));
		SendMessage(g_hwndFlats, WM_INFOBAR_SETTEXPREVIEW, MAKEWPARAM(2, FALSE), IDB_NOTEXTURE);
	}
}




/* ShowThingInfo
 *   Displays thing information in the info-bar.
 *
 * Parameters:
 *   THINGDISPLAYINFO*	lptdi			Information to display.
 *   DWORD				dwDisplayFlags	Flags determining what info to show.
 *
 * Return value: None.
 *
 * Remarks:
 *   Shows the correct frames, but doesn't mess with positioning.
 */
void ShowThingInfo(THINGDISPLAYINFO *lptdi, DWORD dwDisplayFlags)
{
	TCHAR szBuffer[64];
	TCHAR szFromTable[64];

	/* Show the correct frames. */
	if(g_dwPanels & g_dwPanelMask & IBPF_THING)
	{
		ShowWindow(g_hwndThingPanelH, SW_SHOW);
		InfoBarShowPanels(IBPF_SECTOR | IBPF_LINEDEF | IBPF_VERTEX, FALSE);
	}

	if(g_dwPanels & g_dwPanelMask & IBPF_SPRITE)
	{
		ShowWindow(g_hwndThingSprite, SW_SHOW);
	}


	/* Frame caption. */
	if(dwDisplayFlags & TDIF_INDEX)
	{
		LoadString(g_hInstance, IDS_INFOBAR_TCAP, szFromTable, sizeof(szFromTable) / sizeof(TCHAR));
		_sntprintf(szBuffer, sizeof(szBuffer) / sizeof(TCHAR) - 1, szFromTable, lptdi->iIndex);
		szBuffer[sizeof(szBuffer) / sizeof(TCHAR) - 1] = '\0';
	}
	else
	{
		/* Multiple sectors. */
		LoadString(g_hInstance, IDS_INFOBAR_TCAPMULTI, szBuffer, sizeof(szBuffer) / sizeof(TCHAR));
	}

	SetDlgItemText(g_hwndThingPanelH, IDC_THING_FRAME, szBuffer);


	/* Type. */
	if(dwDisplayFlags & TDIF_TYPE)
		SetDlgItemText(g_hwndThingPanelH, IDC_THING_TYPE, lptdi->szType);
	else SetDlgItemText(g_hwndThingPanelH, IDC_THING_TYPE, TEXT(""));


	/* Co-ordinates. */
	if((dwDisplayFlags & TDIF_X) && (dwDisplayFlags & TDIF_Y))
	{
		_sntprintf(szBuffer, sizeof(szBuffer) / sizeof(TCHAR) - 1, TEXT("(%d, %d)"), lptdi->x, lptdi->y);
		szBuffer[sizeof(szBuffer) / sizeof(TCHAR) - 1] = '\0';
		SetDlgItemText(g_hwndThingPanelH, IDC_THING_COORDS, szBuffer);
	}
	else
		SetDlgItemText(g_hwndThingPanelH, IDC_THING_COORDS, TEXT(""));


	/* Z-offset. */
	if(dwDisplayFlags & TDIF_Z)
		SetDlgItemInt(g_hwndThingPanelH, IDC_THING_Z, lptdi->z, FALSE);
	else
		SetDlgItemText(g_hwndThingPanelH, IDC_THING_Z, TEXT(""));


	/* Flags. */
	if(dwDisplayFlags & TDIF_FLAGS)
	{
		_sntprintf(szBuffer, sizeof(szBuffer) / sizeof(TCHAR) - 1, TEXT("%04X (%d)"), lptdi->unFlags, lptdi->unFlags);
		szBuffer[sizeof(szBuffer) / sizeof(TCHAR) - 1] = '\0';
		SetDlgItemText(g_hwndThingPanelH, IDC_THING_FLAGS, szBuffer);
	}
	else
		SetDlgItemText(g_hwndThingPanelH, IDC_THING_FLAGS, TEXT(""));


	/* Direction. */
	if(dwDisplayFlags & TDIF_DIRECTION)
		SetDlgItemText(g_hwndThingPanelH, IDC_THING_DIRECTION, lptdi->szDirection);
	else SetDlgItemText(g_hwndThingPanelH, IDC_THING_DIRECTION, TEXT(""));


	/* Sprite. */
	if(dwDisplayFlags & TDIF_SPRITE)
	{
		SetDlgItemText(g_hwndThingSprite, IDC_TEXNAME1, lptdi->szSprite);

		if(lptdi->lptexSprite)
			SendMessage(g_hwndThingSprite, WM_INFOBAR_SETTEXPREVIEW, MAKEWPARAM(1, TRUE), (LPARAM)lptdi->lptexSprite);
		else
			SendMessage(g_hwndThingSprite, WM_INFOBAR_SETTEXPREVIEW, MAKEWPARAM(1, FALSE), *lptdi->szSprite ? IDB_UNKNOWNIMAGE : IDB_NOTEXTURE);
	}
	else
	{
		SetDlgItemText(g_hwndThingSprite, IDC_TEXNAME1, TEXT(""));
		SendMessage(g_hwndThingSprite, WM_INFOBAR_SETTEXPREVIEW, MAKEWPARAM(1, FALSE), IDB_NOTEXTURE);
	}

	/* Parameter. */
	if(dwDisplayFlags & TDIF_PARAM)
		SetDlgItemInt(g_hwndThingPanelH, IDC_THING_PARAM, lptdi->byParam, FALSE);
	else
		SetDlgItemText(g_hwndThingPanelH, IDC_THING_PARAM, TEXT(""));
}



/* ShowVertexInfo
 *   Displays vertex information in the info-bar.
 *
 * Parameters:
 *   VERTEXDISPLAYINFO*	lpvdi			Information to display.
 *   DWORD				dwDisplayFlags	Flags determining what info to show.
 *
 * Return value: None.
 *
 * Remarks:
 *   Shows the correct frames, but doesn't mess with positioning.
 */
void ShowVertexInfo(VERTEXDISPLAYINFO *lpvdi, DWORD dwDisplayFlags)
{
	TCHAR szBuffer[64];
	TCHAR szFromTable[64];

	/* Show the correct frames. */
	if(g_dwPanels & g_dwPanelMask & IBPF_VERTEX)
	{
		ShowWindow(g_hwndVxPanelH, SW_SHOW);
		InfoBarShowPanels(IBPF_SECTOR | IBPF_THING | IBPF_LINEDEF, FALSE);
	}


	/* Frame caption. */
	if(dwDisplayFlags & VDIF_INDEX)
	{
		LoadString(g_hInstance, IDS_INFOBAR_VCAP, szFromTable, sizeof(szFromTable) / sizeof(TCHAR));
		_sntprintf(szBuffer, sizeof(szBuffer) / sizeof(TCHAR) - 1, szFromTable, lpvdi->iIndex);
		szBuffer[sizeof(szBuffer) / sizeof(TCHAR) - 1] = '\0';
	}
	else
	{
		/* Multiple sectors. */
		LoadString(g_hInstance, IDS_INFOBAR_VCAPMULTI, szBuffer, sizeof(szBuffer) / sizeof(TCHAR));
	}

	SetDlgItemText(g_hwndVxPanelH, IDC_VX_FRAME, szBuffer);


	/* Co-ordinates. */
	if(dwDisplayFlags & VDIF_COORDS)
	{
		_sntprintf(szBuffer, sizeof(szBuffer) / sizeof(TCHAR) - 1, TEXT("(%d, %d)"), lpvdi->x, lpvdi->y);
		szBuffer[sizeof(szBuffer) / sizeof(TCHAR) - 1] = '\0';
		SetDlgItemText(g_hwndVxPanelH, IDC_VX_COORDS, szBuffer);
	}
	else
		SetDlgItemText(g_hwndVxPanelH, IDC_VX_COORDS, TEXT(""));
}



/* ResetSidedefPreviews, ResetFlatPreviews, ResetSpritePreview
 *   Clears texture previews on the info bar.
 *
 * Parameters:
 *   None.
 *
 * Return value: None.
 */
void ResetSidedefPreviews(void)
{
	/* Front. */
	SetDlgItemText(g_hwndSDFront, IDC_TEXNAME1, TEXT(""));
	SetDlgItemText(g_hwndSDFront, IDC_TEXNAME2, TEXT(""));
	SetDlgItemText(g_hwndSDFront, IDC_TEXNAME3, TEXT(""));

	SendMessage(g_hwndSDFront, WM_INFOBAR_SETTEXPREVIEW, MAKEWPARAM(1, FALSE), IDB_NOTEXTURE);
	SendMessage(g_hwndSDFront, WM_INFOBAR_SETTEXPREVIEW, MAKEWPARAM(2, FALSE), IDB_NOTEXTURE);
	SendMessage(g_hwndSDFront, WM_INFOBAR_SETTEXPREVIEW, MAKEWPARAM(3, FALSE), IDB_NOTEXTURE);

	/* Back. */
	SetDlgItemText(g_hwndSDBack, IDC_TEXNAME1, TEXT(""));
	SetDlgItemText(g_hwndSDBack, IDC_TEXNAME2, TEXT(""));
	SetDlgItemText(g_hwndSDBack, IDC_TEXNAME3, TEXT(""));

	SendMessage(g_hwndSDBack, WM_INFOBAR_SETTEXPREVIEW, MAKEWPARAM(1, FALSE), IDB_NOTEXTURE);
	SendMessage(g_hwndSDBack, WM_INFOBAR_SETTEXPREVIEW, MAKEWPARAM(2, FALSE), IDB_NOTEXTURE);
	SendMessage(g_hwndSDBack, WM_INFOBAR_SETTEXPREVIEW, MAKEWPARAM(3, FALSE), IDB_NOTEXTURE);
}

void ResetFlatPreviews(void)
{
	SetDlgItemText(g_hwndFlats, IDC_TEXNAME1, TEXT(""));
	SetDlgItemText(g_hwndFlats, IDC_TEXNAME2, TEXT(""));

	SendMessage(g_hwndFlats, WM_INFOBAR_SETTEXPREVIEW, MAKEWPARAM(1, FALSE), IDB_NOTEXTURE);
	SendMessage(g_hwndFlats, WM_INFOBAR_SETTEXPREVIEW, MAKEWPARAM(2, FALSE), IDB_NOTEXTURE);
}

void ResetSpritePreview(void)
{
	SetDlgItemText(g_hwndThingSprite, IDC_TEXNAME1, TEXT(""));
	SendMessage(g_hwndThingSprite, WM_INFOBAR_SETTEXPREVIEW, MAKEWPARAM(1, FALSE), IDB_NOTEXTURE);
}


/* InfoBarShowPanels
 *   Shows/hides panels on the info-bar.
 *
 * Parameters:
 *   DWORD	dwFlags		Flags specifying which panels to hide.
 *   BOOL	bShow		TRUE to show; FALSE to hide.
 *
 * Return value: None.
 */
void InfoBarShowPanels(DWORD dwFlags, BOOL bShow)
{
	int iCmdShow = bShow ? SW_SHOW : SW_HIDE;

	if(dwFlags & IBPF_LINEDEF) ShowWindow(g_hwndLDPanelH, iCmdShow);
	if(dwFlags & IBPF_SECTOR) ShowWindow(g_hwndSecPanelH, iCmdShow);
	if(dwFlags & IBPF_THING) ShowWindow(g_hwndThingPanelH, iCmdShow);
	if(dwFlags & IBPF_VERTEX) ShowWindow(g_hwndVxPanelH, iCmdShow);

	if(dwFlags & IBPF_SIDEDEF)
	{
		ShowWindow(g_hwndSDBack, iCmdShow);
		ShowWindow(g_hwndSDFront, iCmdShow);
	}

	if(dwFlags & IBPF_FLAT) ShowWindow(g_hwndFlats, iCmdShow);
	if(dwFlags & IBPF_SPRITE) ShowWindow(g_hwndThingSprite, iCmdShow);
}




/* SetInfoBarMode
 *   Sets flags specifying which panels may be displayed according to the
 *   editing mode.
 *
 * Parameters:
 *   ENUM_EDITMODE		emInfoBarMode	Mode for which to choose panels.
 *
 * Return value: None.
 *
 * Use emInfoBarMode == EM_INVALID to force an update next time.
 */
static void SetInfoBarMode(ENUM_EDITMODE emInfoBarMode)
{
	static ENUM_EDITMODE s_emCurrent = EM_INVALID;

	/* Don't do anything if we're already in the correct mode. */
	if(s_emCurrent == emInfoBarMode) return;
	s_emCurrent = emInfoBarMode;

	/* Assume we don't force any panels. */
	g_dwForcePanels = 0;

	switch(emInfoBarMode)
	{
	case EM_ANY:
		g_dwPanels = IBPF_SIDEDEF | IBPF_FLAT | IBPF_SPRITE;

		/* EM_ANY always shows all these panels. */
		InfoBarShowPanels(g_dwPanels, TRUE);

		if(ConfigGetInteger(g_lpcfgMain, OPT_DETAILSANY))
		{
			/* Show the left-hand panels. */
			g_dwPanels |= IBPF_ALLINFO;
		}

		g_dwForcePanels = g_dwPanels;

		break;
	case EM_LINES:
		g_dwPanels = IBPF_SIDEDEF | IBPF_LINEDEF;
		break;
	case EM_SECTORS:
		g_dwPanels = IBPF_FLAT | IBPF_SECTOR;
		break;
	case EM_THINGS:
		g_dwPanels = IBPF_SPRITE | IBPF_THING;
		break;
	case EM_VERTICES:
		g_dwPanels = IBPF_VERTEX;
		break;
	default:
        break;
	}

	/* Hide unnecessary panels. */
	InfoBarShowPanels(~g_dwPanels, FALSE);

	SetWindowPos(g_hwndInfoBarDock, NULL, 0, 0, 0, 0, SWP_NOZORDER|SWP_NOSIZE|SWP_NOMOVE|SWP_FRAMECHANGED);
}



/* InfoBarTakeControl
 *   Initialises the info-bar for a particular map window.
 *
 * Parameters:
 *   HWND			hwnd		Map window. Used to determine who's in control
 *								at any point in the future.
 *   ENUM_EDITMODE	editmode	Map window's edit-mode.
 *   RGBQUAD*		lprgbqPal	256-colour palette.
 *
 * Return value: None.
 */
void InfoBarTakeControl(HWND hwnd, ENUM_EDITMODE editmode, RGBQUAD *lprgbqPal)
{
	SetInfoBarMode(editmode);

	if(g_hwndIBMapWin != hwnd)
	{
		/* wParam is a mask specifying which of the three previews to initialise. */
		SendMessage(g_hwndSDFront, WM_INFOBAR_INITTEXBITMAPS, 4|2|1, (LPARAM)lprgbqPal);
		SendMessage(g_hwndSDBack, WM_INFOBAR_INITTEXBITMAPS, 4|2|1, (LPARAM)lprgbqPal);
		SendMessage(g_hwndFlats, WM_INFOBAR_INITTEXBITMAPS, 2|1, (LPARAM)lprgbqPal);
		SendMessage(g_hwndThingSprite, WM_INFOBAR_INITTEXBITMAPS, 1, (LPARAM)lprgbqPal);

		g_hwndIBMapWin = hwnd;
	}
}


/* InfoBarEnableDetailsPanels, InfoBarDisableDetailsPanels
 *   Enables/disables the displaying of the details panels separately from
 *   whether they're otherwise required for this mode.
 *
 * Parameters:
 *   HWND		hwnd		Map window. Used to determine who set the palette at
 *							any point in the future.
 *   RGBQUAD*	lprgbqPal	256-colour palette.
 *
 * Return value: None.
 */
void InfoBarEnableDetailsPanels(void) { g_dwPanelMask |= IBPF_ALLINFO; }
void InfoBarDisableDetailsPanels(void) { g_dwPanelMask &= ~IBPF_ALLINFO; }


/* InfoBarOptionsChanged
 *   Informs the info-bar that preferences governing its appearance might have
 *   changed.
 *
 * Parameters:
 *   None.
 *
 * Return value: None.
 */
void InfoBarOptionsChanged(void)
{
	/* Make sure the info-bar rearranges itself next time. */
	SetInfoBarMode(EM_INVALID);
}


/* IsInfoBarVisible: Returns whether the info-bar is visible. */
BOOL IsInfoBarVisible(void) { return IsWindowVisible(g_hwndInfoBarDock); }

/* ToggleInfoBarVisibility: Toggles info-bar visibility. */
void ToggleInfoBarVisibility(void)
{
	if(IsInfoBarVisible()) SendMessage(g_hwndInfoBarDock, WM_CLOSE, 0, 0);
	else DockingShowFrame(DockingIsToolWindow(NULL, g_hwndInfoBarDock));
}

/* Get/SetInfoBarState: Gets/sets info-bar state for saving across sessions. */
void GetInfoBarState(LPDOCKSAVESTATE lpdss) { DockingSavePlacement(DockingIsToolWindow(NULL, g_hwndInfoBarDock), lpdss); }
void SetInfoBarState(LPDOCKSAVESTATE lpdss) { DockingLoadPlacement(DockingIsToolWindow(NULL, g_hwndInfoBarDock), lpdss); }


/* GetInfoBarDI
 * Gets the DOCKINFO structure for the info-bar.
 *
 * Parameters:
 *   None.
 *
 * Return value: LPDOCKINFO
 *   Pointer to DOCKINFO structure for the info-bar.
 *
 * Remarks:
 *   Routines outside this file should not be messing with the structure, but
 *   sometimes it's necessary for aggregating DI structures across docking
 *   windows and passing them back into another routine in here.
 */
LPDOCKINFO GetInfoBarDI(void)
{
	return DockingIsToolWindow(NULL, g_hwndInfoBarDock);
}


/* InfoBarSetDataFlags
 *   Sets flags specifying which extra values are to be shown for map objects.
 *
 * Parameters:
 *   DWORD	dwFlags		Flags.
 *
 * Return value: None.
 */
void InfoBarSetDataFlags(DWORD dwFlags)
{
	ShowWindow(GetDlgItem(g_hwndThingPanelH, IDC_STATIC_PARAMLABEL), (dwFlags & IBDF_THING_PARAM) ? SW_SHOW : SW_HIDE);
	ShowWindow(GetDlgItem(g_hwndThingPanelH, IDC_THING_PARAM), (dwFlags & IBDF_THING_PARAM) ? SW_SHOW : SW_HIDE);
}
