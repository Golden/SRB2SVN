/* SRB2 Workbench -- A map editor for Sonic Robo Blast 2.
 * Copyright (C) 2009 Gregor Dick
 * http://workbench.srb2.org/
 *
 * Incorporates portions of Doom Builder, (C) 2003 Pascal vd Heiden.
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 * USA.
 *
 * texbrowser.h: Header for texbrowser.c.
 *
 * AMDG.
 */

#ifndef __SRB2B_TEXBROWSER__
#define __SRB2B_TEXBROWSER__

#include <windows.h>

#include "../general.h"
#include "../texture.h"

enum ENUM_SELECT_TEXTURE_FLAGS
{
	SLT_FORCE_USED = 1,
	SLT_NO_NULL = 2
};

BOOL SelectTexture(HWND hwndParent, HWND hwndMap, TEX_FORMAT tf, HIMAGELIST* lphimlCache, TEXTURENAMELIST *lptnlAll, CONFIG *lpcfgUsed, DWORD dwFlags, LPTSTR szTexName);

#endif
