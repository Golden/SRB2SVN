/* SRB2 Workbench -- A map editor for Sonic Robo Blast 2.
 * Copyright (C) 2009 Gregor Dick
 * http://workbench.srb2.org/
 *
 * Incorporates portions of Doom Builder, (C) 2003 Pascal vd Heiden.
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 * USA.
 *
 * mdiframe.h: Header for mdiframe.c.
 *
 * AMDG.
 */

#ifndef __SRB2B_MDIFRAME__
#define __SRB2B_MDIFRAME__

#include <windows.h>

#include "../config.h"

#include "../DockWnd/DockWnd.h"


/* Number of characters (inc. terminator) for "Untitled" string. */
#define UNTITLED_BUFLEN 32


/* Types. */
enum ENUM_CHILD_IDS
{
	IDC_STATUSBAR,
	IDC_TOOLBAR,
};

typedef enum _ENUM_OMIW_RETURN
{
	OMIWRET_NEWWINDOW,
	OMIWRET_EXISTINGWINDOW,
	OMIWRET_FAILED
} ENUM_OMIW_RETURN;

/* Status bar panels. */
enum ENUM_SBPANELS
{
	SBP_MODE = 0,
	SBP_SECTORS,
	SBP_LINEDEFS,
	SBP_SIDEDEFS,
	SBP_VERTICES,
	SBP_THINGS,
	SBP_GRID,
	SBP_ZOOM,
	SBP_COORD,
	SBP_MAPCONFIG,
	SBP_LAST
};

/* Menu positions for the no-document menu bar. */
enum ENUM_NODOC_MENUPOS
{
	FILEMENUPOSNODOC = 0,
	WINDOWMENUPOSNODOC = 0,		/* Dummy. */
	VIEWMENUPOSNODOC = 1,
	TOOLSMENUPOSNODOC = 2,
	HELPMENUPOSNODOC = 3
};

/* Menu positions for the map-window menu bar. */
enum ENUM_MAP_MENUPOS
{
	FILEMENUPOS = 0,
	EDITMENUPOS,
	VIEWMENUPOS,
	LINESMENUPOS,
	SECTORSMENUPOS,
	VERTICESMENUPOS,
	THINGSMENUPOS,
	TOOLSMENUPOS,
	WINDOWMENUPOS,
	HELPMENUPOS,
#ifdef _DEBUG
	DEBUGMENUPOS,
#endif
	MAPMENUCOUNT
};


/* Corresponding flags. */
#define SBPF_MODE		(1 << SBP_MODE)
#define SBPF_SECTORS	(1 << SBP_SECTORS)
#define SBPF_LINEDEFS	(1 << SBP_LINEDEFS)
#define SBPF_SIDEDEFS	(1 << SBP_SIDEDEFS)
#define SBPF_VERTICES	(1 << SBP_VERTICES)
#define SBPF_THINGS		(1 << SBP_THINGS)
#define SBPF_GRID		(1 << SBP_GRID)
#define SBPF_ZOOM		(1 << SBP_ZOOM)
#define SBPF_COORD		(1 << SBP_COORD)
#define SBPF_MAPCONFIG	(1 << SBP_MAPCONFIG)


/* Globals. */
extern HWND g_hwndMain, g_hwndClient;
extern HMENU g_hmenuNodoc, g_hmenuMap;
extern HMENU g_hmenuNodocWin, g_hmenuMapSubmenus[];
extern HWND g_hwndStatusBar;
extern DOCKWNDCONTEXT g_dwc;


/* Function prototypes. */
int CreateMainWindow(int iCmdShow);
void DestroyMainWindow(void);
BOOL CALLBACK CloseEnumProc(HWND hwnd, LPARAM lParam);
BOOL OpenWadForEditing(LPCTSTR szFilename);
ENUM_OMIW_RETURN OpenMapInWindow(int iWad, LPSTR szLumpName, CONFIG *lpcfgMap);
void StatusBarNoWindow(void);
void DeferTakeFocus(void);
void UpdateMenuShortcutKeys(HMENU hmenu);


#endif
