/* SRB2 Workbench -- A map editor for Sonic Robo Blast 2.
 * Copyright (C) 2009 Gregor Dick
 * http://workbench.srb2.org/
 *
 * Incorporates portions of Doom Builder, (C) 2003 Pascal vd Heiden.
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 * USA.
 *
 * fofdlg.c: Implements the FOF Editor dialogue.
 *
 * AMDG.
 */

#include <windows.h>
#include <commctrl.h>
#include <stdio.h>
#include <tchar.h>

#include "../general.h"
#include "../config.h"
#include "../options.h"
#include "../fof.h"
#include "../mapconfig.h"
#include "../cdlgwrapper.h"

#include "mdiframe.h"
#include "fofdlg.h"
#include "gendlg.h"
#include "editdlg.h"
#include "texbrowser.h"

#include "../../res/resource.h"

#ifndef ListView_SetCheckState //Alam: WineLib does not have the ListView_*CheckState defines, why?
#define ListView_SetCheckState(hwnd,iIndex,bCheck) \
    ListView_SetItemState(hwnd,iIndex,INDEXTOSTATEIMAGEMASK((bCheck)+1),LVIS_STATEIMAGEMASK)
#endif
#ifndef ListView_GetCheckState //Alam: let remove the 2 defines after commctrl.h patch hits the Wine GIT tree
#define ListView_GetCheckState(hwnd,iIndex) \
    ((((UINT)(ListView_GetItemState(hwnd,iIndex,LVIS_STATEIMAGEMASK)))>>12)-1)
#endif


#define CX_FOFICON				16
#define CY_FOFICON				16
#define CCH_FOFLIST_COLHDR		64
#define CCH_FOFEFFECT			128
#define CX_FOFLIST_TYPE			320


/* Subitem- and column-indices for the FOF-list. */
enum ENUM_FOFLISTSUBITEMS
{
	FLSI_TYPE,
	FLSI_TOP,
	FLSI_BOTTOM
};

typedef enum _ENUM_FOF_HEIGHT_TYPES
{
	FHT_ABSOLUTE,
	FHT_REL_FLOOR,
	FHT_REL_CEIL,
	FHT_REL_OTHERPLANE
} ENUM_FOF_HEIGHT_TYPES;

typedef enum _ENUM_FOF_PLANE
{
	FP_CEILING,
	FP_FLOOR
} ENUM_FOF_PLANE;


typedef struct _AFTTCBDATA
{
	CONFIG	*lpcfgMap;
	HWND	hwndCombo;
} AFTTCBDATA;

typedef struct _AFFTLDATA
{
	HWND	hwndListView;
	BOOL	bDescription;
} AFFTLDATA;

typedef struct _FOFHEIGHTDLGDATA
{
	ENUM_FOF_HEIGHT_TYPES	fht;
	int						iHeight;	/* In the format determined by fht. */
	ENUM_FOF_PLANE			fp;
} FOFHEIGHTDLGDATA;


static INT_PTR CALLBACK FOFListDlgProc(HWND hwndDlg, UINT uiMsg, WPARAM wParam, LPARAM lParam);
static FOF_LIST_NODE* GetSelectedFOFListNode(HWND hwndDlg);
static void AddFOFNodeToListView(HWND hwndList, FOF_LIST_NODE *lpfln, CONFIG *lpcfgMap, int iIconIndex);
static void UpdateFOFListEntry(HWND hwndList, int iItem, FOF *lpfof, CONFIG *lpcfgMap);
static INT_PTR CALLBACK FOFEditorDlgProc(HWND hwndDlg, UINT uiMsg, WPARAM wParam, LPARAM lParam);
static BOOL AddFOFFlagToList(CONFIG *lpcfgFlagSSCont, void *lpvData);
static BOOL AddFOFTypeToComboBox(CONFIG *lpcfgFOFType, void *lpvData);
static void UpdateFOFFlagsFromSelection(HWND hwndDlg, FOFEDITORDLGDATA *lpfedd);
static void SelectSpecifiedFOFFlags(HWND hwndFlagList, DWORD dwFlags);
static DWORD GetSelectedFOFFlags(HWND hwndListView, FOF_FLAGS *lpff);
static BOOL ShowFOFHeightDlg(HWND hwndParent, FOFHEIGHTDLGDATA *lpfhdd);
static INT_PTR CALLBACK FOFHeightDlgProc(HWND hwndDlg, UINT uiMsg, WPARAM wParam, LPARAM lParam);
static short GetEffectiveFOFPlaneHeight(HWND hwndDlg, FOFEDITORDLGDATA *lpfedd, ENUM_FOF_PLANE fp);
static short GetEffectiveFOFBrightness(HWND hwndDlg, FOFEDITORDLGDATA *lpfedd);
static void EnableOrDisableFOFAlpha(HWND hwndDlg, FOF_FLAGS *lpff);
static CONFIG* GetSelectedFOFInfo(HWND hwndDlg, FOFEDITORDLGDATA *lpfedd);


/* ShowFOFListDlg
 *   Dialogue proc for FOF-list dialouge box.
 *
 * Parameters:
 *   FOFLISTDLGDATA*	lpfldd	Parameters for the dialogue.
 *
 * Return value: BOOL
 *   TRUE if the dialogue was OKed, or FALSE if it was cancelled.
 */
BOOL ShowFOFListDlg(FOFLISTDLGDATA *lpfldd)
{
	return DialogBoxParam(g_hInstance, MAKEINTRESOURCE(IDD_FOF_SELECT), g_hwndMain, FOFListDlgProc, (LPARAM)lpfldd);
}


/* FOFListDlgProc
 *   Dialogue proc for FOF-list dialouge box.
 *
 * Parameters:
 *   HWND	hwndDlg		Dialogue window handle.
 *   UINT	uiMsg		Message identifier.
 *   WPARAM wParam		Message-specific.
 *   LPARAM lParam		Message-specific.
 *
 * Return value: INT_PTR
 *   TRUE if the message was processed; FALSE otherwise.
 */
static INT_PTR CALLBACK FOFListDlgProc(HWND hwndDlg, UINT uiMsg, WPARAM wParam, LPARAM lParam)
{
	static FOFLISTDLGDATA *s_lpfldd = NULL;
	static int s_iIconIndex = 0;

	switch(uiMsg)
	{
	case WM_INITDIALOG:
		{
			HIMAGELIST himl;
			HWND hwndList = GetDlgItem(hwndDlg, IDC_LIST_FOFS);
			LVCOLUMN lvcol;
			TCHAR szColHeader[CCH_FOFLIST_COLHDR];
			FOF_LIST_NODE *lpfln;

			/* Centre the dialogue. */
			CentreWindowInParent(hwndDlg);

			/* Get dialogue parameters. */
			s_lpfldd = (FOFLISTDLGDATA*)lParam;

			/* Disable Delete button. */
			EnableWindow(GetDlgItem(hwndDlg, IDC_BUTTON_DELETEFOF), FALSE);

			/* The image-list will be destroyed automatically by the control
			 * when it is destroyed itself.
			 */
			himl = ImageList_Create(CX_FOFICON, CY_FOFICON, ILC_COLOR32 | ILC_MASK, 1, 0);

			/* Add the FOF icon to the image-list and associate image-list with
			 * list-view.
			 */
			s_iIconIndex = ImageList_AddIcon(himl, LoadIcon(g_hInstance, MAKEINTRESOURCE(IDI_FOF)));
			ListView_SetImageList(hwndList, himl, LVSIL_SMALL);

			/* Create the columns in the list view control. */
			lvcol.pszText = szColHeader;
			lvcol.mask = LVCF_SUBITEM | LVCF_TEXT;

			lvcol.iSubItem = FLSI_TYPE;
			LoadString(g_hInstance, IDS_FOFLIST_COL_TYPE, szColHeader, NUM_ELEMENTS(szColHeader));
			ListView_InsertColumn(hwndList, FLSI_TYPE, &lvcol);

			lvcol.iSubItem = FLSI_TOP;
			LoadString(g_hInstance, IDS_FOFLIST_COL_TOP, szColHeader, NUM_ELEMENTS(szColHeader));
			ListView_InsertColumn(hwndList, FLSI_TOP, &lvcol);

			lvcol.iSubItem = FLSI_BOTTOM;
			LoadString(g_hInstance, IDS_FOFLIST_COL_BOTTOM, szColHeader, NUM_ELEMENTS(szColHeader));
			ListView_InsertColumn(hwndList, FLSI_BOTTOM, &lvcol);

			/* Full row select. */
			ListView_SetExtendedListViewStyle(hwndList, LVS_EX_FULLROWSELECT);

			/* Add the FOFs to the list. */
			for(lpfln = s_lpfldd->lpfl->flnHeader.lpflnNext; lpfln->lpflnNext; lpfln = lpfln->lpflnNext)
				AddFOFNodeToListView(hwndList, lpfln, s_lpfldd->lpcfgMap, s_iIconIndex);

			/* Resize the columns now that we've added the FOFs. */
			ListView_SetColumnWidth(hwndList, FLSI_TYPE, CX_FOFLIST_TYPE);
			ListView_SetColumnWidth(hwndList, FLSI_TOP, LVSCW_AUTOSIZE_USEHEADER);
			ListView_SetColumnWidth(hwndList, FLSI_BOTTOM, LVSCW_AUTOSIZE_USEHEADER);
		}

		/* Let the system set the focus. */
		return TRUE;

	case WM_COMMAND:
		switch(LOWORD(wParam))
		{
		case IDOK:
			EndDialog(hwndDlg, TRUE);
			return TRUE;

		case IDCANCEL:
			EndDialog(hwndDlg, FALSE);
			return TRUE;

		case IDC_BUTTON_ADDFOF:
			{
				/* Make a new FOF with default properties. */
				FOF_LIST_NODE *lpflnNew = AddNewDefaultFOFToList(
					s_lpfldd->lpfl,
					s_lpfldd->lpcfgMap,
					s_lpfldd->lpcfgWadOptMap,
					s_lpfldd->nContainingFloor,
					s_lpfldd->nContainingBrightness);
				
				/* Add to the list-view. */
				AddFOFNodeToListView(GetDlgItem(hwndDlg, IDC_LIST_FOFS), lpflnNew, s_lpfldd->lpcfgMap, s_iIconIndex);
			}

			return TRUE;

		case IDC_BUTTON_DELETEFOF:
			{
				FOF_LIST_NODE *lpfln = GetSelectedFOFListNode(hwndDlg);
				HWND hwndList = GetDlgItem(hwndDlg, IDC_LIST_FOFS);
				int iItem = ListView_GetNextItem(hwndList, -1, LVIS_SELECTED);

				if(lpfln)
				{
					/* Mark the FOF for deletion, and remove the list-view
					 * entry.
					 */
					lpfln->fof.dwEditFlags |= FEF_DELETE;
					ListView_DeleteItem(hwndList, iItem);
				}
			}

			return TRUE;
		}

		break;

	case WM_NOTIFY:
		if(((LPNMHDR)lParam)->idFrom == IDC_LIST_FOFS)
		{
			LPNMLISTVIEW lpnmlistview = (LPNMLISTVIEW)lParam;
			HWND hwndList = lpnmlistview->hdr.hwndFrom;

			switch(lpnmlistview->hdr.code)
			{
			case NM_DBLCLK:
				{
					/* User double-clicked a FOF. Edit it. */
					LPNMITEMACTIVATE lpnmia = (LPNMITEMACTIVATE)lParam;

					if(lpnmia->iItem >= 0)
					{
						LVITEM lvitem;
						FOFEDITORDLGDATA fedd;

						lvitem.iItem = lpnmia->iItem;
						lvitem.iSubItem = 0;
						lvitem.mask = LVIF_PARAM;

						ListView_GetItem(hwndList, &lvitem);
						
						fedd.hwndMap = s_lpfldd->hwndMap;
						fedd.lpcfgMap = s_lpfldd->lpcfgMap;
						fedd.lpcfgUsedFlats = s_lpfldd->lpcfgUsedFlats;
						fedd.lpcfgUsedTextures = s_lpfldd->lpcfgUsedTextures;
						fedd.lpff = s_lpfldd->lpff;
						fedd.lpfof = &((FOF_LIST_NODE*)lvitem.lParam)->fof;
						fedd.lphimlCacheFlats = s_lpfldd->lphimlCacheFlats;
						fedd.lphimlCacheTextures = s_lpfldd->lphimlCacheTextures;
						fedd.lptnlFlats = s_lpfldd->lptnlFlats;
						fedd.lptnlTextures = s_lpfldd->lptnlTextures;
						fedd.nContainingCeil = s_lpfldd->nContainingCeiling;
						fedd.nContainingFloor = s_lpfldd->nContainingFloor;
						fedd.dwFlags = FEDF_HASCONTSECPROPS;

						/* Show the FOF editor and update the list if necessary. */
						if(ShowFOFEditorDlg(hwndDlg, &fedd))
							UpdateFOFListEntry(hwndList, lvitem.iItem, fedd.lpfof, s_lpfldd->lpcfgMap);
					}
				}

				return TRUE;

			case LVN_ITEMCHANGED:
				if(lpnmlistview->uChanged & LVIF_STATE)
				{
					if(lpnmlistview->uNewState & LVIS_SELECTED)
					{
						/* Selection should always be visible. */
						ListView_EnsureVisible(lpnmlistview->hdr.hwndFrom, lpnmlistview->iItem, FALSE);

						/* Enable the buttons as appropriate. */
						EnableWindow(GetDlgItem(hwndDlg, IDC_BUTTON_DELETEFOF), TRUE);
					}
					else
						EnableWindow(GetDlgItem(hwndDlg, IDC_BUTTON_DELETEFOF), FALSE);

					return TRUE;
				}

				break;
			}
		}

		break;
	}

	/* Didn't process message. */
	return FALSE;
}


/* GetSelectedFOFListNode
 *   Gets the FOF_LIST_NODE structure for the selected FOF.
 *
 * Parameters:
 *   HWND	hwndDlg		FOF dialogue window handle.
 *
 * Return value: FOF_LIST_NODE*
 *   Pointer to list node, or NULL if nothing is selected.
 */
static FOF_LIST_NODE* GetSelectedFOFListNode(HWND hwndDlg)
{
	HWND hwndList = GetDlgItem(hwndDlg, IDC_LIST_FOFS);
	int iItem = ListView_GetNextItem(hwndList, -1, LVIS_SELECTED);

	/* Anything selected? */
	if(iItem >= 0)
	{
		LVITEM lvitem;

		lvitem.iItem = iItem;
		lvitem.iSubItem = 0;
		lvitem.mask = LVIF_PARAM;

		ListView_GetItem(hwndList, &lvitem);

		return (FOF_LIST_NODE*)lvitem.lParam;
	}

	/* Nothing selected. */
	return NULL;
}


/* AddFOFNodeToList
 *   Creates an entry in the FOF list-view corresponding to a given FOF-list
 *   node.
 *
 * Parameters:
 *   HWND				hwndList		List-view window handle.
 *   FOF_LIST_NODE*		lpfln			FOF-list node.
 *   CONFIG*			lpcfgMap		Map config.
 *   int				iIconIndex		Icon index.
 *
 * Return value: None.
 */
static void AddFOFNodeToListView(HWND hwndList, FOF_LIST_NODE *lpfln, CONFIG *lpcfgMap, int iIconIndex)
{
	LVITEM lvitem;

	lvitem.mask = LVIF_PARAM | LVIF_IMAGE;
	lvitem.iItem = 0;
	lvitem.iSubItem = 0;
	lvitem.lParam = (LPARAM)lpfln;
	lvitem.iImage = iIconIndex;

	/* Add item and set its text. */
	UpdateFOFListEntry(hwndList, ListView_InsertItem(hwndList, &lvitem), &lpfln->fof, lpcfgMap);
}


/* UpdateFOFListEntry
 *   Updates the item and subitem text for an entry in the list of FOFs.
 *
 * Parameters:
 *   HWND		hwndList		List-view window handle.
 *   int		iItem			Index of FOF in the list.
 *   FOF*		lpfof			FOF.
 *   CONFIG*	lpcfgMap		Map config.
 *
 * Return value: None.
 */
static void UpdateFOFListEntry(HWND hwndList, int iItem, FOF *lpfof, CONFIG *lpcfgMap)
{
	TCHAR szType[CCH_FOFEFFECT];
	TCHAR szPlanes[CCH_SIGNED_INT];

	/* Get flat linedef config section. */
	CONFIG *lpcfgFlatLinedefs = ConfigGetSubsection(lpcfgMap, FLAT_LINE_SECTION);

	/* Get the text to display for the FOF. */
	GetEffectDisplayText(lpfof->nType, lpcfgFlatLinedefs, szType, NUM_ELEMENTS(szType), FALSE);
	ListView_SetItemText(hwndList, iItem, FLSI_TYPE, szType);

	/* Set bottom and top heights. */

	szPlanes[NUM_ELEMENTS(szPlanes) - 1] = TEXT('\0');

	_sntprintf(szPlanes, NUM_ELEMENTS(szPlanes) - 1, TEXT("%d"), lpfof->nCeilingHeight);
	ListView_SetItemText(hwndList, iItem, FLSI_TOP, szPlanes);

	_sntprintf(szPlanes, NUM_ELEMENTS(szPlanes) - 1, TEXT("%d"), lpfof->nFloorHeight);
	ListView_SetItemText(hwndList, iItem, FLSI_BOTTOM, szPlanes);
}


/* ShowFOFEditorDlg
 *   Dialogue proc for FOF-editor dialouge box.
 *
 * Parameters:
 *   HWND				hwndParent	Parent window.
 *   FOFEDITORDLGDATA*	lpfedd		Parameters for the dialogue.
 *
 * Return value: BOOL
 *   TRUE if the dialogue was OKed, or FALSE if it was cancelled.
 */
BOOL ShowFOFEditorDlg(HWND hwndParent, FOFEDITORDLGDATA *lpfedd)
{
	return DialogBoxParam(g_hInstance, MAKEINTRESOURCE(IDD_FOF_EDITOR), hwndParent, FOFEditorDlgProc, (LPARAM)lpfedd);
}



/* FOFEditorDlgProc
 *   Dialogue proc for FOF-editor dialouge box.
 *
 * Parameters:
 *   HWND	hwndDlg		Dialogue window handle.
 *   UINT	uiMsg		Message identifier.
 *   WPARAM wParam		Message-specific.
 *   LPARAM lParam		Message-specific.
 *
 * Return value: INT_PTR
 *   TRUE if the message was processed; FALSE otherwise.
 */
static INT_PTR CALLBACK FOFEditorDlgProc(HWND hwndDlg, UINT uiMsg, WPARAM wParam, LPARAM lParam)
{
	static TPINFO s_tpinfo[] =
	{
		{IDC_TEX_CEIL,		IDC_EDIT_TCEIL,		TF_FLAT,	NULL,	NULL,	NULL},
		{IDC_TEX_FLOOR,		IDC_EDIT_TFLOOR,	TF_FLAT,	NULL,	NULL,	NULL},
		{IDC_TEX_SIDES,		IDC_EDIT_SIDES,		TF_TEXTURE,	NULL,	NULL,	NULL},
	};

	static FOFEDITORDLGDATA *s_lpfedd = NULL;
	static ENUM_FOF_HEIGHT_TYPES s_fhtCeil = FHT_ABSOLUTE;
	static ENUM_FOF_HEIGHT_TYPES s_fhtFloor = FHT_ABSOLUTE;
	static HBRUSH s_hbrushEdit = NULL;
	static BOOL s_bSuppressForceCustom = FALSE;

	switch(uiMsg)
	{
	case WM_INITDIALOG:
		{
			HDC hdcDlg;
			UDACCEL udaccel[2];
			int i;
			CONFIG *lpcfgFOFs, *lpcfgFOFTypes = NULL;
			HWND hwndFlagList = GetDlgItem(hwndDlg, IDC_LIST_FOFFLAGS);
			AFTTCBDATA afttcbdata;
			TCHAR szColourmap[TEXNAME_BUFFER_LENGTH];
			AFFTLDATA afftldata;

			CentreWindowInParent(hwndDlg);

			/* Get the dialogue data. */
			s_lpfedd = (FOFEDITORDLGDATA*)lParam;

			/* Get FOF info from map config. */
			lpcfgFOFs = ConfigGetSubsection(s_lpfedd->lpcfgMap, MAPCFG_FOFS);
			if(lpcfgFOFs) lpcfgFOFTypes = ConfigGetSubsection(lpcfgFOFs, MAPCFG_FOF_TYPES);

			if(!lpcfgFOFs || !lpcfgFOFTypes)
			{
				MessageBoxFromStringTable(g_hwndMain, IDS_ERROR_NOFOFCFG, MB_ICONERROR);
				EndDialog(hwndDlg, FALSE);
			}

			/* Subclass the texture preview controls and create their preview
			 * bitmaps.
			 */
			hdcDlg = GetDC(hwndDlg);
			for(i = 0; i < (int)NUM_ELEMENTS(s_tpinfo); i++)
			{
				HWND hwndTex = GetDlgItem(hwndDlg, s_tpinfo[i].iTPID);

				/* Allocate memory for the preview window. It's freed when the
				 * window's destroyed.
				 */
				TPPDATA *lptppd = ProcHeapAlloc(sizeof(TPPDATA));

				lptppd->wndprocStatic = (WNDPROC)GetWindowLong(hwndTex, GWL_WNDPROC);

				SetWindowLong(hwndTex, GWL_USERDATA, (LONG)lptppd);
				SetWindowLong(hwndTex, GWL_WNDPROC, (LONG)TexPreviewProc);

				/* Create preview bitmap. */
				s_tpinfo[i].hbitmap = CreateCompatibleBitmap(hdcDlg, CX_TEXPREVIEW, CY_TEXPREVIEW);
				SendDlgItemMessage(hwndDlg, s_tpinfo[i].iTPID, STM_SETIMAGE, IMAGE_BITMAP, (LPARAM)s_tpinfo[i].hbitmap);

				/* Set the preview to something sensible in case we never touch
				 * it again.
				 */
				SetTexturePreviewImage(s_lpfedd->hwndMap, hwndTex, TEXT(""), TF_TEXTURE, s_tpinfo[i].hbitmap, FALSE);
			}

			ReleaseDC(hwndDlg, hdcDlg);

			if(ConfigGetInteger(g_lpcfgMain, OPT_AUTOCOMPLETETEX))
			{
				/* Create texture enumerators and shell autocomplete objects and
				 * associate them with the edit boxes.
				 */
				for(i = 0; i < (int)NUM_ELEMENTS(s_tpinfo); i++)
				{
					if((s_tpinfo[i].lptexenum = CreateTexEnum()) && (s_tpinfo[i].lpac2 = CreateAutoComplete()))
					{
						SetTexEnumList(s_tpinfo[i].lptexenum, (s_tpinfo[i].iEditID == IDC_EDIT_SIDES) ? s_lpfedd->lptnlTextures : s_lpfedd->lptnlFlats);
						s_tpinfo[i].lpac2->lpVtbl->Init(s_tpinfo[i].lpac2, GetDlgItem(hwndDlg, s_tpinfo[i].iEditID), (IUnknown*)s_tpinfo[i].lptexenum, NULL, NULL);
					}
				}
			}

			ListView_SetExtendedListViewStyleEx(hwndFlagList, LVS_EX_CHECKBOXES | LVS_EX_INFOTIP, LVS_EX_CHECKBOXES | LVS_EX_INFOTIP);

			/* Populate the flag-list. */
			afftldata.bDescription = TRUE;
			afftldata.hwndListView = hwndFlagList;
			ConfigIterate(ConfigGetSubsection(lpcfgFOFs, MAPCFG_FOF_FLAGS), AddFOFFlagToList, &afftldata);

			/* Select flags according to the FOF we're editing. Only really
			 * necessary if we're a custom FOF, but there's no harm and checking
			 * is a hassle.
			 */
			SelectSpecifiedFOFFlags(hwndFlagList, s_lpfedd->lpfof->dwFlags);

			/* Populate the type combo-box. */
			afttcbdata.hwndCombo = GetDlgItem(hwndDlg, IDC_COMBO_FOFTYPE);
			afttcbdata.lpcfgMap = s_lpfedd->lpcfgMap;

			ConfigIterate(lpcfgFOFTypes, AddFOFTypeToComboBox, (void*)&afttcbdata);

			/* Initialise the type combo box. */
			ComboBoxSearchByItemData(afttcbdata.hwndCombo, s_lpfedd->lpfof->nType, TRUE);

			/* Set the checkboxes accordingly, without messing about with the
			 * selected FOF type.
			 */
			s_bSuppressForceCustom = TRUE;
			UpdateFOFFlagsFromSelection(hwndDlg, s_lpfedd);
			s_bSuppressForceCustom = FALSE;

			/* Check the Alpha box if FEF_TRANSLUCENT is set. This is okay even
			 * if the particular effect doesn't allow translucency.
			 */
			CheckDlgButton(hwndDlg, IDC_CHECK_TRANS, (s_lpfedd->lpfof->dwEditFlags & FEF_TRANSLUCENT) ? BST_CHECKED : BST_UNCHECKED);

			/* Enable or disable translucency controls as appropriate. */
			EnableOrDisableFOFAlpha(hwndDlg, s_lpfedd->lpff);

			/* If we don't have containing sector info, disable the buttons for
			 * relative calculation.
			 */
			if(!(s_lpfedd->dwFlags & FEDF_HASCONTSECPROPS))
			{
				EnableWindow(GetDlgItem(hwndDlg, IDC_BUTTON_ADJUST_CEIL), FALSE);
				EnableWindow(GetDlgItem(hwndDlg, IDC_BUTTON_ADJUST_FLOOR), FALSE);
			}

			/* Default to showing descriptions. */
			CheckRadioButton(hwndDlg, IDC_RADIO_FLAGDESC, IDC_RADIO_FLAGNAME, IDC_RADIO_FLAGDESC);

			/* Reset height calculation modes. */
			s_fhtCeil = FHT_ABSOLUTE;
			s_fhtFloor = FHT_ABSOLUTE;

			/* Set up the spinners. */

			udaccel[0].nInc = 1;
			udaccel[0].nSec = 0;
			udaccel[1].nInc = 8;
			udaccel[1].nSec = 2;

			SendDlgItemMessage(hwndDlg, IDC_SPIN_HCEIL, UDM_SETRANGE32, -32768, 32767);
			SendDlgItemMessage(hwndDlg, IDC_SPIN_HCEIL, UDM_SETACCEL, NUM_ELEMENTS(udaccel), (LPARAM)udaccel);

			SendDlgItemMessage(hwndDlg, IDC_SPIN_HFLOOR, UDM_SETRANGE32, -32768, 32767);
			SendDlgItemMessage(hwndDlg, IDC_SPIN_HFLOOR, UDM_SETACCEL, NUM_ELEMENTS(udaccel), (LPARAM)udaccel);

			SendDlgItemMessage(hwndDlg, IDC_SPIN_BRIGHTNESS, UDM_SETRANGE32, 0, 255);
			SendDlgItemMessage(hwndDlg, IDC_SPIN_BRIGHTNESS, UDM_SETACCEL, NUM_ELEMENTS(udaccel), (LPARAM)udaccel);

			SendDlgItemMessage(hwndDlg, IDC_SPIN_COLOURMAP_ALPHA, UDM_SETRANGE32, COLOURMAP_ALPHA_MIN, COLOURMAP_ALPHA_MAX);
			SendDlgItemMessage(hwndDlg, IDC_SPIN_COLOURMAP_ALPHA, UDM_SETACCEL, NUM_ELEMENTS(udaccel), (LPARAM)udaccel);

			SendDlgItemMessage(hwndDlg, IDC_SPIN_TRANS_ALPHA, UDM_SETRANGE32, TRANS_ALPHA_MIN, TRANS_ALPHA_MAX);
			SendDlgItemMessage(hwndDlg, IDC_SPIN_TRANS_ALPHA, UDM_SETACCEL, NUM_ELEMENTS(udaccel), (LPARAM)udaccel);


			/* Initialise controls with the FOF's properties. */
			SetDlgItemANSIText(hwndDlg, IDC_EDIT_TCEIL, s_lpfedd->lpfof->szCeiling);
			SetDlgItemANSIText(hwndDlg, IDC_EDIT_TFLOOR, s_lpfedd->lpfof->szFloor);
			SetDlgItemANSIText(hwndDlg, IDC_EDIT_SIDES, s_lpfedd->lpfof->szSidedefs);
			SetDlgItemInt(hwndDlg, IDC_EDIT_HCEIL, s_lpfedd->lpfof->nCeilingHeight, TRUE);
			SetDlgItemInt(hwndDlg, IDC_EDIT_HFLOOR, s_lpfedd->lpfof->nFloorHeight, TRUE);
			SetDlgItemInt(hwndDlg, IDC_EDIT_BRIGHTNESS, s_lpfedd->lpfof->nBrightness, FALSE);
			SetDlgItemInt(hwndDlg, IDC_EDIT_TRANS_ALPHA, s_lpfedd->lpfof->byAlpha, FALSE);

			/* Colourmap? */
			if(s_lpfedd->lpfof->dwEditFlags & FEF_COLOURMAP)
			{
				CheckDlgButton(hwndDlg, IDC_CHECK_COLOURMAP, BST_CHECKED);
				GenerateColourmapString(szColourmap, s_lpfedd->lpfof->crefColourmap);
				SetDlgItemInt(hwndDlg, IDC_EDIT_COLOURMAP_ALPHA, s_lpfedd->lpfof->byColourmapAlpha, FALSE);
			}
			else
			{
				GenerateColourmapString(szColourmap, DEFAULT_COLOURMAP_COLOUR);
				SetDlgItemInt(hwndDlg, IDC_EDIT_COLOURMAP_ALPHA, DEFAULT_COLOURMAP_ALPHA, FALSE);
				EnableWindow(GetDlgItem(hwndDlg, IDC_EDIT_COLOURMAP_COLOUR), FALSE);
				EnableWindow(GetDlgItem(hwndDlg, IDC_EDIT_COLOURMAP_ALPHA), FALSE);
				EnableWindow(GetDlgItem(hwndDlg, IDC_BUTTON_COLOURMAP_COLOUR), FALSE);
				EnableWindow(GetDlgItem(hwndDlg, IDC_SPIN_COLOURMAP_ALPHA), FALSE);
			}

			SetDlgItemText(hwndDlg, IDC_EDIT_COLOURMAP_COLOUR, szColourmap);

			/* Create the brush for the colourmap control. */
			s_hbrushEdit = CreateSolidBrush(DEFAULT_COLOURMAP_COLOUR);
		}

		/* Let the system set the focus. */
		return TRUE;

	case WM_COMMAND:

		switch(LOWORD(wParam))
		{
		case IDC_TEX_CEIL:
		case IDC_TEX_FLOOR:
		case IDC_TEX_SIDES:

			switch(HIWORD(wParam))
			{
			case BN_CLICKED:
				{
					TCHAR szTexName[TEXNAME_BUFFER_LENGTH];
					int iPreviewIndex;
					BOOL bTexOKed;

					/* Find the corresponding edit box. */
					for(iPreviewIndex = 0; s_tpinfo[iPreviewIndex].iTPID != LOWORD(wParam); iPreviewIndex++);

					/* Get the current flat so it's selected initially. */
					GetDlgItemText(hwndDlg, s_tpinfo[iPreviewIndex].iEditID, szTexName, TEXNAME_BUFFER_LENGTH);

					/* Show the texture browser. */
					if(s_tpinfo[iPreviewIndex].tf == TF_TEXTURE)
						bTexOKed = SelectTexture(hwndDlg, s_lpfedd->hwndMap, TF_TEXTURE, s_lpfedd->lphimlCacheTextures, s_lpfedd->lptnlTextures, s_lpfedd->lpcfgUsedTextures, 0, szTexName);
					else
						bTexOKed = SelectTexture(hwndDlg, s_lpfedd->hwndMap, TF_FLAT, s_lpfedd->lphimlCacheFlats, s_lpfedd->lptnlFlats, s_lpfedd->lpcfgUsedFlats, 0, szTexName);

					if(bTexOKed)
					{
						/* User didn't cancel. Update the edit-box. */
						SetDlgItemText(hwndDlg, s_tpinfo[iPreviewIndex].iEditID, szTexName);
					}

					return TRUE;
				}
			}

			break;
		

		case IDC_EDIT_TCEIL:
		case IDC_EDIT_TFLOOR:
		case IDC_EDIT_SIDES:

			switch(HIWORD(wParam))
			{
			case EN_CHANGE:
				{
					TCHAR szEditText[TEXNAME_BUFFER_LENGTH];
					int iPreviewIndex;

					/* Find the corresponding preview control. */
					for(iPreviewIndex = 0; s_tpinfo[iPreviewIndex].iEditID != LOWORD(wParam); iPreviewIndex++);

					/* Get the texture name. */
					GetWindowText((HWND)lParam, szEditText, TEXNAME_BUFFER_LENGTH);

					/* Update the texture preview image. */
					SetTexturePreviewImage
						(s_lpfedd->hwndMap,
						 GetDlgItem(hwndDlg, s_tpinfo[iPreviewIndex].iTPID),
						 szEditText,
						 s_tpinfo[iPreviewIndex].tf,
						 s_tpinfo[iPreviewIndex].hbitmap,
						 FALSE);

					return TRUE;
				}

				return TRUE;
			}

			break;

			/* Begin macro definition. */
#define BOUNDEDIT(name) \
		case IDC_EDIT_##name: \
			switch(HIWORD(wParam)) \
			{ \
			case EN_KILLFOCUS: \
				{ \
					int iMin, iMax; \
					\
					SendDlgItemMessage(hwndDlg, IDC_SPIN_##name, UDM_GETRANGE32, (WPARAM)&iMin, (LPARAM)&iMax); \
					BoundEditBox((HWND)lParam, iMin, iMax, FALSE, TRUE); \
				} \
				\
				return TRUE; \
			} \
			\
			break; \
			/* End macro definition. */

			/* Bound those edit boxes! */
			BOUNDEDIT(HCEIL);
			BOUNDEDIT(HFLOOR);
			BOUNDEDIT(BRIGHTNESS);
			BOUNDEDIT(COLOURMAP_ALPHA);
			BOUNDEDIT(TRANS_ALPHA);
#undef BOUNDEDIT

		case IDOK:
			/* Get FOF properties from the dialogue's controls. */

			GetDlgItemANSIText(hwndDlg, IDC_EDIT_TCEIL, s_lpfedd->lpfof->szCeiling, TEXNAME_BUFFER_LENGTH);
			GetDlgItemANSIText(hwndDlg, IDC_EDIT_TFLOOR, s_lpfedd->lpfof->szFloor, TEXNAME_BUFFER_LENGTH);
			GetDlgItemANSIText(hwndDlg, IDC_EDIT_SIDES, s_lpfedd->lpfof->szSidedefs, TEXNAME_BUFFER_LENGTH);
			s_lpfedd->lpfof->nCeilingHeight = GetEffectiveFOFPlaneHeight(hwndDlg, s_lpfedd, FP_CEILING);
			s_lpfedd->lpfof->nFloorHeight = GetEffectiveFOFPlaneHeight(hwndDlg, s_lpfedd, FP_FLOOR);
			s_lpfedd->lpfof->nCeilingHeight = max(s_lpfedd->lpfof->nCeilingHeight, s_lpfedd->lpfof->nFloorHeight);
			s_lpfedd->lpfof->nBrightness = GetEffectiveFOFBrightness(hwndDlg, s_lpfedd);
			s_lpfedd->lpfof->byAlpha = (BYTE)GetDlgItemInt(hwndDlg, IDC_EDIT_TRANS_ALPHA, NULL, FALSE);
			s_lpfedd->lpfof->nType = (short)SendDlgItemMessage(hwndDlg, IDC_COMBO_FOFTYPE, CB_GETITEMDATA, SendDlgItemMessage(hwndDlg, IDC_COMBO_FOFTYPE, CB_GETCURSEL, 0, 0), 0);
			s_lpfedd->lpfof->dwFlags = GetSelectedFOFFlags(GetDlgItem(hwndDlg, IDC_LIST_FOFFLAGS), s_lpfedd->lpff);

			if(IsDlgButtonChecked(hwndDlg, IDC_CHECK_COLOURMAP))
			{
				TCHAR szColour[CCH_COLOURMAP_COLOUR + 1];
				GetDlgItemText(hwndDlg, IDC_EDIT_COLOURMAP_COLOUR, szColour, NUM_ELEMENTS(szColour));
				s_lpfedd->lpfof->crefColourmap = GetColourmapFromString(szColour);
				s_lpfedd->lpfof->byColourmapAlpha = GetDlgItemInt(hwndDlg, IDC_EDIT_COLOURMAP_ALPHA, NULL, FALSE);
				s_lpfedd->lpfof->dwEditFlags |= FEF_COLOURMAP;
			}
			else s_lpfedd->lpfof->dwEditFlags &= ~FEF_COLOURMAP;

			if((s_lpfedd->lpff->dwMask & FFM_TRANSLUCENT) &&
				(s_lpfedd->lpfof->dwFlags & s_lpfedd->lpff->dwTranslucent) &&
				IsDlgButtonChecked(hwndDlg, IDC_CHECK_TRANS))
				s_lpfedd->lpfof->dwEditFlags |= FEF_TRANSLUCENT;
			else
				s_lpfedd->lpfof->dwEditFlags &= ~FEF_TRANSLUCENT;

			EndDialog(hwndDlg, TRUE);
			return TRUE;

		case IDCANCEL:
			EndDialog(hwndDlg, FALSE);
			return TRUE;

		case IDC_BUTTON_ADJUST_CEIL:
		case IDC_BUTTON_ADJUST_FLOOR:
			{
				FOFHEIGHTDLGDATA fhdd;

				/* Ceiling or floor? */
				BOOL bCeiling = (LOWORD(wParam) == IDC_BUTTON_ADJUST_CEIL);

				/* Heights relative to which we might calculate. */
				short nOppPlane = GetEffectiveFOFPlaneHeight(hwndDlg, s_lpfedd, bCeiling ? FP_FLOOR : FP_CEILING);

				/* Get the existing relativity of the height. */
				fhdd.fht = bCeiling ? s_fhtCeil : s_fhtFloor;

				/* Indicate whether ceiling or floor. */
				fhdd.fp = bCeiling ? FP_CEILING : FP_FLOOR;

				/* Get the current specified height for the plane we're editing,
				 * and then adjust it in reverse according to the relativity.
				 */
				fhdd.iHeight = GetEffectiveFOFPlaneHeight(hwndDlg, s_lpfedd, fhdd.fp);
				switch(fhdd.fht)
				{
					case FHT_REL_CEIL:
						fhdd.iHeight = s_lpfedd->nContainingCeil - fhdd.iHeight;
						break;
					case FHT_REL_FLOOR:
						fhdd.iHeight -= s_lpfedd->nContainingFloor;
						break;
					case FHT_REL_OTHERPLANE:
						if(bCeiling) fhdd.iHeight -= nOppPlane;
						else fhdd.iHeight = nOppPlane - fhdd.iHeight;
						break;
					default:
						/* Absolute. Do nothing, but placate the compiler. */
						break;
				}

				/* Show the dialogue. */
				if(ShowFOFHeightDlg(hwndDlg, &fhdd))
				{
					/* User OKed. Update the height in the editor. */
					switch(fhdd.fht)
					{
						case FHT_REL_CEIL:
							fhdd.iHeight = s_lpfedd->nContainingCeil - fhdd.iHeight;
							break;
						case FHT_REL_FLOOR:
							fhdd.iHeight += s_lpfedd->nContainingFloor;
							break;
						case FHT_REL_OTHERPLANE:
							if(bCeiling) fhdd.iHeight += nOppPlane;
							else fhdd.iHeight = nOppPlane - fhdd.iHeight;
							break;
						default:
							/* Absolute. Do nothing, but placate the compiler. */
							break;
					}

					/* Bound. */
					fhdd.iHeight = max(-32768, min(32767, fhdd.iHeight));

					/* Set the text in the edit box. */
					SetDlgItemInt(hwndDlg, bCeiling ? IDC_EDIT_HCEIL : IDC_EDIT_HFLOOR, fhdd.iHeight, TRUE);

					/* Remember the relativity. */
					if(bCeiling) s_fhtCeil = fhdd.fht;
					else s_fhtFloor = fhdd.fht;
				}
			}

			return TRUE;

		case IDC_BUTTON_COLOURMAP_COLOUR:
			{
				TCHAR szColour[CCH_COLOURMAP_COLOUR + 1];
				COLORREF crefColour;
				static COLORREF s_rgbCustom[16] = {0};

				/* Get existing colour. */
				GetDlgItemText(hwndDlg, IDC_EDIT_COLOURMAP_COLOUR, szColour, NUM_ELEMENTS(szColour));
				crefColour = GetColourmapFromString(szColour);

				if(CommDlgColour(hwndDlg, &crefColour, s_rgbCustom))
				{
					/* OKed, so get string and set edit text. */
					GenerateColourmapString(szColour, crefColour);
					SetDlgItemText(hwndDlg, IDC_EDIT_COLOURMAP_COLOUR, szColour);
				}
			}

			return TRUE;

		case IDC_COMBO_FOFTYPE:
			if(HIWORD(wParam) == CBN_SELCHANGE)
			{
				/* Prevent reciprocal changes. */
				s_bSuppressForceCustom = TRUE;

				/* Set the checkboxes in the flag-list and update other controls. */
				UpdateFOFFlagsFromSelection(hwndDlg, s_lpfedd);
				EnableOrDisableFOFAlpha(hwndDlg, s_lpfedd->lpff);

				s_bSuppressForceCustom = FALSE;

				return TRUE;
			}

			break;

		case IDC_CHECK_COLOURMAP:
			if(HIWORD(wParam) == BN_CLICKED)
			{
				BOOL bEnable = IsDlgButtonChecked(hwndDlg, IDC_CHECK_COLOURMAP);

				if(!bEnable)
				{
					int iID;

					/* Move the focus off our controls. */
					while(iID = GetDlgCtrlID(GetFocus()),
						iID == IDC_EDIT_COLOURMAP_COLOUR ||
						iID == IDC_EDIT_COLOURMAP_ALPHA ||
						iID == IDC_BUTTON_COLOURMAP_COLOUR ||
						iID == IDC_SPIN_COLOURMAP_ALPHA)
					{
						SendMessage(hwndDlg, WM_NEXTDLGCTL, 0, 0);
					}
				}

				/* Enable/disable the controls as necessary. */
				EnableWindow(GetDlgItem(hwndDlg, IDC_EDIT_COLOURMAP_COLOUR), bEnable);
				EnableWindow(GetDlgItem(hwndDlg, IDC_EDIT_COLOURMAP_ALPHA), bEnable);
				EnableWindow(GetDlgItem(hwndDlg, IDC_BUTTON_COLOURMAP_COLOUR), bEnable);
				EnableWindow(GetDlgItem(hwndDlg, IDC_SPIN_COLOURMAP_ALPHA), bEnable);

				return TRUE;
			}

			break;

		case IDC_CHECK_TRANS:
			if(HIWORD(wParam) == BN_CLICKED)
			{
				EnableOrDisableFOFAlpha(hwndDlg, s_lpfedd->lpff);
				return 0;
			}

			break;

		case IDC_EDIT_COLOURMAP_COLOUR:
			if(HIWORD(wParam) == EN_CHANGE)
			{
				/* Repaint the entire control. */
				InvalidateRect((HWND)lParam, NULL, TRUE);
				return TRUE;
			}

			break;

		case IDC_RADIO_FLAGNAME:
		case IDC_RADIO_FLAGDESC:
			{
				/* Reopolulate the flag list with the correct captions. */

				AFFTLDATA afftldata;
				DWORD dwFlags;

				/* Don't mess with the selected FOF type. */
				s_bSuppressForceCustom = TRUE;

				afftldata.bDescription = IsDlgButtonChecked(hwndDlg, IDC_RADIO_FLAGDESC);
				afftldata.hwndListView = GetDlgItem(hwndDlg, IDC_LIST_FOFFLAGS);

				/* Remember the flags. */
				dwFlags = GetSelectedFOFFlags(afftldata.hwndListView, s_lpfedd->lpff);

				/* Repopulate. */
				ListView_DeleteAllItems(afftldata.hwndListView);
				ConfigIterate(ConfigGetSubsection(ConfigGetSubsection(s_lpfedd->lpcfgMap, MAPCFG_FOFS), MAPCFG_FOF_FLAGS), AddFOFFlagToList, &afftldata);

				/* Restore flags. */
				SelectSpecifiedFOFFlags(afftldata.hwndListView, dwFlags);

				s_bSuppressForceCustom = FALSE;
			}

			return TRUE;
		}

		/* Didn't process WM_COMMAND. */
		break;

	case WM_NOTIFY:
		switch(((LPNMHDR)lParam)->idFrom)
		{
		case IDC_LIST_FOFFLAGS:
			{
				LPNMLISTVIEW lpnmlistview = (LPNMLISTVIEW)lParam;

				switch(lpnmlistview->hdr.code)
				{
				case LVN_ITEMCHANGED:
					/* Has an item's check-state changed? */
					if((lpnmlistview->uChanged & LVIF_STATE) && (lpnmlistview->uOldState & 0xF000) != (lpnmlistview->uNewState & 0xF000))
					{
						CONFIG *lpcfgSelectedFOF;

						/* Enable or disable the translucency controls depending
						 * on whether the specified FOF supports translucency.
						 */
						EnableOrDisableFOFAlpha(hwndDlg, s_lpfedd->lpff);

						/* Make sure we have a custom FOF, unless the flags
						 * changed due to the user selecting a FOF type.
						 */
						if(!s_bSuppressForceCustom && (lpcfgSelectedFOF = GetSelectedFOFInfo(hwndDlg, s_lpfedd)))
						{
							/* If the selected type is not custom, select the
							 * default custom type.
							 */
							if(!ConfigGetInteger(lpcfgSelectedFOF, FOFCFG_EFFECT_CUSTOM))
							{
								ComboBoxSearchByItemData(
									GetDlgItem(hwndDlg, IDC_COMBO_FOFTYPE),
									ConfigGetInteger(ConfigGetSubsection(s_lpfedd->lpcfgMap, MAPCFG_FOFS), FOFCFG_DEFAULTCUSTOM),
									TRUE);
							}
						}

						return 0;
					}

					break;
				}
			}

			break;
		}

		/* Didn't process WM_NOTIFY. */
		break;

	case WM_DESTROY:
		{
			int i;

			/* Destroy preview bitmaps and clean up autocomplete. */
			for(i = 0; i < (int)NUM_ELEMENTS(s_tpinfo); i++)
			{
				DeleteObject(s_tpinfo[i].hbitmap);
				if(s_tpinfo[i].lpac2) s_tpinfo[i].lpac2->lpVtbl->Release(s_tpinfo[i].lpac2);
				if(s_tpinfo[i].lptexenum) DestroyTexEnum(s_tpinfo[i].lptexenum);
			}

			/* Clean up the colourmap brush. */
			if(s_hbrushEdit)
			{
				DeleteObject(s_hbrushEdit);
				s_hbrushEdit = NULL;
			}
		}

		return TRUE;

	case WM_CTLCOLOREDIT:
		if(s_hbrushEdit && GetDlgCtrlID((HWND)lParam) == IDC_EDIT_COLOURMAP_COLOUR)
		{
			TCHAR szColour[CCH_COLOURMAP_COLOUR + 1];
			COLORREF crefColour;

			/* Get the colour. */
			GetWindowText((HWND)lParam, szColour, NUM_ELEMENTS(szColour));
			crefColour = GetColourmapFromString(szColour);

			DeleteObject(s_hbrushEdit);
			SetBkMode((HDC)wParam, TRANSPARENT);
			
			/* Choose white or black text using a weighted luminance comparison
			 * found in a comment on The Old New Thing:
			 * http://blogs.msdn.com/oldnewthing/archive/2004/05/26/142276.aspx
			 */
			SetTextColor((HDC)wParam, (2 * GetRValue(crefColour) + 5 * GetGValue(crefColour) + GetBValue(crefColour) > 0x400) ?
				RGB(0, 0, 0) :
				RGB(0xFF, 0xFF, 0xFF));

			return (INT_PTR)(s_hbrushEdit = CreateSolidBrush(crefColour));
		}

		break;
	}

	/* Didn't process message. */
	return FALSE;
}


/* AddFOFFlagToList
 *   Adds a FOF flag to a list-view control.
 *
 * Parameters:
 *   CONFIG		*lpcfgFlagSSCont	Subsection container for flag info.
 *   void*		lpvData				(AFFTLDATA*) Extra parameters.
 *
 * Return value: BOOL
 *   Always TRUE.
 *
 * Remarks:
 *   Used as a callback function by ConfigIterate.
 */
static BOOL AddFOFFlagToList(CONFIG *lpcfgFlagSSCont, void *lpvData)
{
	AFFTLDATA *lpafftldata = (AFFTLDATA*)lpvData;
	LVITEM lvitem;
	DWORD cchEffect;
	LPTSTR szEffect = NULL;

	/* Sanity check. */
	if(lpcfgFlagSSCont->entrytype != CET_SUBSECTION)
		return TRUE;

	/* Ignore FF_EXISTS. */
	if(_tcsicmp(lpcfgFlagSSCont->szName, FOFCFG_FF_EXISTS) == 0)
		return TRUE;

	/* Get effect text. */
	if(lpafftldata->bDescription)
	{
		cchEffect = ConfigGetStringLength(lpcfgFlagSSCont->lpcfgSubsection, FOFCFG_FLAG_TEXT) + 1;
		if(cchEffect > 1)
		{
			szEffect = ProcHeapAlloc(cchEffect * sizeof(TCHAR));
			ConfigGetString(lpcfgFlagSSCont->lpcfgSubsection, FOFCFG_FLAG_TEXT, szEffect, cchEffect);
		}
		else return TRUE;
	}

	lvitem.mask = LVIF_TEXT | LVIF_PARAM;
	lvitem.iItem = 0;
	lvitem.iSubItem = 0;
	lvitem.pszText = lpafftldata->bDescription ? szEffect : lpcfgFlagSSCont->szName;
	lvitem.lParam = ConfigGetInteger(lpcfgFlagSSCont->lpcfgSubsection, FOFCFG_FLAG_VALUE);
	ListView_InsertItem(lpafftldata->hwndListView, &lvitem);

	if(szEffect)
		ProcHeapFree(szEffect);

	return TRUE;
}


/* AddFOFTypeToComboBox
 *   Adds a FOF type to a combo-box control.
 *
 * Parameters:
 *   CONFIG		*lpcfgFOFType	Subsection container for flag info.
 *   void*		lpvData			(AFTTCBDATA*) Map config and combo box handle.
 *
 * Return value: BOOL
 *   Always TRUE.
 *
 * Remarks:
 *   Used as a callback function by ConfigIterate.
 */
static BOOL AddFOFTypeToComboBox(CONFIG *lpcfgFOFType, void *lpvData)
{
	AFTTCBDATA *lpafttcbdata = (AFTTCBDATA*)lpvData;
	CONFIG *lpcfgFlatLinedefs = ConfigGetSubsection(lpafttcbdata->lpcfgMap, FLAT_LINE_SECTION);
	TCHAR szType[CCH_FOFEFFECT];
	unsigned short unEffect = (unsigned short)_tcstol(lpcfgFOFType->szName, NULL, 10);
	int iIndex;

	/* Sanity check. */
	if(lpcfgFOFType->entrytype != CET_SUBSECTION)
		return TRUE;

	/* Get effect text for the combo-box. */
	GetEffectDisplayText(unEffect, lpcfgFlatLinedefs, szType, NUM_ELEMENTS(szType), FALSE);

	iIndex = SendMessage(lpafttcbdata->hwndCombo, CB_ADDSTRING, 0, (LPARAM)szType);
	SendMessage(lpafttcbdata->hwndCombo, CB_SETITEMDATA, iIndex, unEffect);

	return TRUE;
}


/* UpdateFOFFlagsFromSelection
 *   Updates the checkboxes in the flag-list based on the selected FOF effect.
 *
 * Parameters:
 *   HWND				hwndDlg		FOF editor dialogue handle.
 *   FOFEDITORDLGDATA*	lpfedd		Dialogoue instance data.
 *
 * Return value: None.
 */
static void UpdateFOFFlagsFromSelection(HWND hwndDlg, FOFEDITORDLGDATA *lpfedd)
{
	CONFIG *lpcfgSelFOFType;

	/* Get the properties of the selected FOF type. */
	if((lpcfgSelFOFType = GetSelectedFOFInfo(hwndDlg, lpfedd)))
	{
		BOOL bCustom = ConfigGetInteger(lpcfgSelFOFType, FOFCFG_EFFECT_CUSTOM);
		HWND hwndFlagList = GetDlgItem(hwndDlg, IDC_LIST_FOFFLAGS);

		if(!bCustom)
			SelectSpecifiedFOFFlags(hwndFlagList, ConfigGetInteger(lpcfgSelFOFType, FOFCFG_EFFECT_FLAGS));
	}
}


/* SelectSpecifiedFOFFlags
 *   Updates the checkboxes in the flag-list to match specified flags.
 *
 * Parameters:
 *   HWND	hwndFlagList	List-view.
 *   DWORD	dwFlags			Flags to select.
 *
 * Return value: None.
 */
static void SelectSpecifiedFOFFlags(HWND hwndFlagList, DWORD dwFlags)
{
	const int iFlagCount = ListView_GetItemCount(hwndFlagList);
	LVITEM lvitem;

	lvitem.mask = LVIF_PARAM;
	lvitem.iSubItem = 0;

	/* Iterate through the flags, setting checkboxes accordingly. */
	for(lvitem.iItem = 0; lvitem.iItem < iFlagCount; lvitem.iItem++)
	{
		ListView_GetItem(hwndFlagList, &lvitem);
		ListView_SetCheckState(hwndFlagList, lvitem.iItem, !!(lvitem.lParam & dwFlags));
	}
}
	

/* GetSelectedFOFFlags
 *   Obtains the values of flags specified in the FOF-flags list-view.
 *
 * Parameters:
 *   HWND			hwndListView	Handle to list control.
 *   FOF_FLAGS*		lpff			FOF flag info.
 *
 * Return value: DWORD
 *   Selected flags.
 */
static DWORD GetSelectedFOFFlags(HWND hwndListView, FOF_FLAGS *lpff)
{
	int i;
	DWORD dwFlags = (lpff->dwMask & FFM_EXISTS) ? lpff->dwExists : 0;

	/* How many flags in our list? */
	const int iCountItems = ListView_GetItemCount(hwndListView);

	/* Repeat for each flag. */
	for(i = 0; i < iCountItems; i++)
	{
		/* If the checkbox is on, set the flag in the field. */
		if(ListView_GetCheckState(hwndListView, i))
		{
			LVITEM lvitem;

			/* Get the flag value. */
			lvitem.iItem = i;
			lvitem.iSubItem = 0;
			lvitem.mask = LVIF_PARAM;
			ListView_GetItem(hwndListView, &lvitem);

			/* Set the appropriate bit. */
			dwFlags |= (DWORD)lvitem.lParam;
		}
	}

	return dwFlags;
}


/* ShowFOFHeightDlg
 *   Dialogue proc for FOF-height dialouge box.
 *
 * Parameters:
 *   HWND				hwndParent	Parent window handle.
 *   FOFHEIGHTDLGDATA*	lpfhdd		Parameters for the dialogue.
 *
 * Return value: BOOL
 *   TRUE if the dialogue was OKed, or FALSE if it was cancelled.
 */
static BOOL ShowFOFHeightDlg(HWND hwndParent, FOFHEIGHTDLGDATA *lpfhdd)
{
	return DialogBoxParam(g_hInstance, MAKEINTRESOURCE(IDD_FOF_HEIGHT), hwndParent, FOFHeightDlgProc, (LPARAM)lpfhdd);
}


/* FOFHeightDlgProc
 *   Dialogue proc for FOF-height dialouge box.
 *
 * Parameters:
 *   HWND	hwndDlg		Dialogue window handle.
 *   UINT	uiMsg		Message identifier.
 *   WPARAM wParam		Message-specific.
 *   LPARAM lParam		Message-specific.
 *
 * Return value: INT_PTR
 *   TRUE if the message was processed; FALSE otherwise.
 */
static INT_PTR CALLBACK FOFHeightDlgProc(HWND hwndDlg, UINT uiMsg, WPARAM wParam, LPARAM lParam)
{
	static FOFHEIGHTDLGDATA *s_lpfhdd = NULL;

	switch(uiMsg)
	{
	case WM_INITDIALOG:
		{
			UDACCEL udaccel[2];
			TCHAR szOppPlane[64];
			int iRadioButton;

			/* Centre the dialogue. */
			CentreWindowInParent(hwndDlg);

			/* Get dialogue parameters. */
			s_lpfhdd = (FOFHEIGHTDLGDATA*)lParam;

			/* Set up the spinners. */

			udaccel[0].nInc = 1;
			udaccel[0].nSec = 0;
			udaccel[1].nInc = 8;
			udaccel[1].nSec = 2;

			/* The value is *relative*, so we need nearly twice the range. */
			SendDlgItemMessage(hwndDlg, IDC_SPIN_HEIGHT, UDM_SETRANGE32, -65535, 65535);
			SendDlgItemMessage(hwndDlg, IDC_SPIN_HEIGHT, UDM_SETACCEL, NUM_ELEMENTS(udaccel), (LPARAM)udaccel);

			/* Select the appropriate radio button. */
			switch(s_lpfhdd->fht)
			{
				case FHT_REL_FLOOR:			iRadioButton = IDC_RADIO_RELFLOOR;			break;
				case FHT_REL_CEIL:			iRadioButton = IDC_RADIO_RELCEIL;			break;
				case FHT_REL_OTHERPLANE:	iRadioButton = IDC_RADIO_RELOTHERPLANE;		break;
				default:					iRadioButton = IDC_RADIO_ABSOLUTE;
			}

			CheckRadioButton(hwndDlg, IDC_RADIO_ABSOLUTE, IDC_RADIO_RELOTHERPLANE, iRadioButton);

			/* Set the value. */
			SetDlgItemInt(hwndDlg, IDC_EDIT_HEIGHT, s_lpfhdd->iHeight, TRUE);

			/* Get the appropriate string for the last radio button. */
			LoadString(g_hInstance, (s_lpfhdd->fp == FP_CEILING) ? IDS_FOFHEIGHT_ABOVEBOTTOM : IDS_FOFHEIGHT_BELOWTOP, szOppPlane, NUM_ELEMENTS(szOppPlane));
			SetDlgItemText(hwndDlg, IDC_RADIO_RELOTHERPLANE, szOppPlane);
		}

		/* Let the system set the focus. */
		return TRUE;

	case WM_COMMAND:
		switch(LOWORD(wParam))
		{
		case IDC_EDIT_HEIGHT:
			if(HIWORD(wParam) == EN_KILLFOCUS)
			{
				int iMin, iMax;
				
				SendDlgItemMessage(hwndDlg, IDC_SPIN_HEIGHT, UDM_GETRANGE32, (WPARAM)&iMin, (LPARAM)&iMax);
				BoundEditBox((HWND)lParam, iMin, iMax, FALSE, FALSE);

				return TRUE;
			}

			break;

		case IDOK:

			s_lpfhdd->iHeight = GetDlgItemInt(hwndDlg, IDC_EDIT_HEIGHT, NULL, TRUE);

			if(IsDlgButtonChecked(hwndDlg, IDC_RADIO_RELFLOOR)) s_lpfhdd->fht = FHT_REL_FLOOR;
			else if(IsDlgButtonChecked(hwndDlg, IDC_RADIO_RELCEIL)) s_lpfhdd->fht = FHT_REL_CEIL;
			else if(IsDlgButtonChecked(hwndDlg, IDC_RADIO_RELOTHERPLANE)) s_lpfhdd->fht = FHT_REL_OTHERPLANE;
			else s_lpfhdd->fht = FHT_ABSOLUTE;

			EndDialog(hwndDlg, TRUE);

			return TRUE;

		case IDCANCEL:
			EndDialog(hwndDlg, FALSE);
			return TRUE;
		}

		/* Didn't process message. */
		break;
	}

	/* Didn't process message. */
	return FALSE;
}


/* GetEffectiveFOFPlaneHeight
 *   Retrieves the height of one of a FOF's planes as specified in the FOF
 *   editor, after taking relative heights into account.
 *
 * Parameters:
 *   HWND				hwndDlg		FOF editor dialogue handle.
 *   FOFEDITORDLGDATA*	lpfedd		FOF editor dialogue data.
 *   ENUM_FOF_PLANE		fp			Whether floor or ceiling.
 *
 * Return value: short
 *   Effective height.
 */
static short GetEffectiveFOFPlaneHeight(HWND hwndDlg, FOFEDITORDLGDATA *lpfedd, ENUM_FOF_PLANE fp)
{
	TCHAR szBuffer[CCH_SIGNED_INT];
	int iHeight;

	/* Get the text from the appropriate edit control. */
	GetDlgItemText(hwndDlg, (fp == FP_CEILING) ? IDC_EDIT_HCEIL : IDC_EDIT_HFLOOR, szBuffer, NUM_ELEMENTS(szBuffer));

	if(STRING_RELATIVE(szBuffer))
		iHeight = ((fp == FP_CEILING) ? lpfedd->lpfof->nCeilingHeight : lpfedd->lpfof->nFloorHeight) + _tcstol(&szBuffer[1], NULL, 10);
	else iHeight = _tcstol(szBuffer, NULL, 10);

	/* Bound. */
	return (short)max(-32768, min(32767, iHeight));
}


/* GetEffectiveFOFBrightness
 *   Retrieves the brightness of one of a FOF as specified in the FOF editor,
 *   after taking relative values into account.
 *
 * Parameters:
 *   HWND				hwndDlg		FOF editor dialogue handle.
 *   FOFEDITORDLGDATA*	lpfedd		FOF editor dialogue data.
 *
 * Return value: short
 *   Effective height.
 */
static short GetEffectiveFOFBrightness(HWND hwndDlg, FOFEDITORDLGDATA *lpfedd)
{
	TCHAR szBuffer[CCH_SIGNED_INT];
	int iBrightness;

	/* Get the text from the appropriate edit control. */
	GetDlgItemText(hwndDlg, IDC_EDIT_BRIGHTNESS, szBuffer, NUM_ELEMENTS(szBuffer));

	if(STRING_RELATIVE(szBuffer))
		iBrightness = lpfedd->lpfof->nBrightness + _tcstol(&szBuffer[1], NULL, 10);
	else iBrightness = (short)_tcstol(szBuffer, NULL, 10);

	/* Bound. */
	return max(0, min(255, iBrightness));
}


/* EnableOrDisableFOFAlpha
 *   Enables or disables translucency controls on the FOF editor dialogue.
 *
 * Parameters:
 *   HWND			hwndDlg		FOF editor dialogue handle.
 *   FOF_FLAGS*		lpfff		FOF flag values.
 *
 * Return value: short
 *   Effective height.
 */
static void EnableOrDisableFOFAlpha(HWND hwndDlg, FOF_FLAGS *lpff)
{
	BOOL bEnableCheck = (lpff->dwMask & FFM_TRANSLUCENT) && (GetSelectedFOFFlags(GetDlgItem(hwndDlg, IDC_LIST_FOFFLAGS), lpff) & lpff->dwTranslucent);
	BOOL bEnableEdit = bEnableCheck && IsDlgButtonChecked(hwndDlg, IDC_CHECK_TRANS);

	if(!bEnableEdit)	/* => !bEnableCheck */
	{
		int iID;

		/* Move the focus off our controls. */
		while(iID = GetDlgCtrlID(GetFocus()),
			(!bEnableCheck && iID == IDC_CHECK_TRANS) ||
			iID == IDC_EDIT_TRANS_ALPHA ||
			iID == IDC_SPIN_TRANS_ALPHA)
		{
			SendMessage(hwndDlg, WM_NEXTDLGCTL, 0, 0);
		}
	}

	EnableWindow(GetDlgItem(hwndDlg, IDC_CHECK_TRANS), bEnableCheck);
	EnableWindow(GetDlgItem(hwndDlg, IDC_EDIT_TRANS_ALPHA), bEnableEdit);
	EnableWindow(GetDlgItem(hwndDlg, IDC_SPIN_TRANS_ALPHA), bEnableEdit);
}


/* GetSelectedFOFInfo
 *   Retrieves the information from the map config for the FOF type selected in
 *   the combo box.
 *
 * Parameters:
 *   HWND				hwndDlg		FOF editor dialogue handle.
 *   FOFEDITORDLGDATA*	lpfedd		FOF editor dialogue data.
 *
 * Return value: CONFIG*
 *   FOF info, or NULL if not found.
 */
static CONFIG* GetSelectedFOFInfo(HWND hwndDlg, FOFEDITORDLGDATA *lpfedd)
{
	HWND hwndCombo = GetDlgItem(hwndDlg, IDC_COMBO_FOFTYPE);
	CONFIG *lpcfgFOFTypes = ConfigGetSubsection(ConfigGetSubsection(lpfedd->lpcfgMap, MAPCFG_FOFS), MAPCFG_FOF_TYPES);
	int iEffect = SendMessage(hwndCombo, CB_GETITEMDATA, SendMessage(hwndCombo, CB_GETCURSEL, 0, 0), 0);
	TCHAR szEffect[CCH_SIGNED_INT];

	_sntprintf(szEffect, NUM_ELEMENTS(szEffect), TEXT("%d"), iEffect);
	szEffect[NUM_ELEMENTS(szEffect) - 1] = TEXT('\0');

	/* Get the properties of the selected FOF type. */
	return ConfigGetSubsection(lpcfgFOFTypes, szEffect);
}
