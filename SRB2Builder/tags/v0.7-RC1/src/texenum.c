/* SRB2 Workbench -- A map editor for Sonic Robo Blast 2.
 * Copyright (C) 2009 Gregor Dick
 * http://workbench.srb2.org/
 *
 * Incorporates portions of Doom Builder, (C) 2003 Pascal vd Heiden.
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 * USA.
 *
 * texenum.c: Implements various COM interfaces for auto-completion.
 *
 * AMDG.
 */

#include <windows.h>
#include <tchar.h>
#include <objbase.h>
#include <shldisp.h>
#include <shlguid.h>

#include "general.h"
#include "texture.h"
#include "texenum.h"
#include "../res/resource.h"


/* MinGW doesn't have this. It's actually defined in an enum by MS's headers, so
 * the ifndef is somewhat redundant and we end up defining it using the
 * preprocessor anyway, but no harm done.
 */
#ifndef ACO_UPDOWNKEYDROPSLIST
#define ACO_UPDOWNKEYDROPSLIST	0x20
#endif


/* CTexEnum COM object, implementing IEnumString. */
typedef struct CTexEnum
{
	IEnumStringVtbl		*lpVtbl;

	DWORD				dwRefCount;

	int					iNumTextures;
	int					iCurrentTexture;
	LPOLESTR			*lpszTextures;
} CTexEnum;


static CTexEnum* CreateCTexEnum(void);
static void FreeTextureNames(LPTEXENUM lptexenum);

static HRESULT STDMETHODCALLTYPE QueryInterface(IEnumString *this, REFIID riidInterfaceGUID, void **lplpvInterface);
static ULONG STDMETHODCALLTYPE AddRef(IEnumString *this);
static ULONG STDMETHODCALLTYPE Release(IEnumString *this);
static HRESULT STDMETHODCALLTYPE Next(IEnumString *this, ULONG cstr, LPOLESTR *lpsz, ULONG *lpcstrFetched);
static HRESULT STDMETHODCALLTYPE Skip(IEnumString *this, ULONG cstr);
static HRESULT STDMETHODCALLTYPE Reset(IEnumString *this);
static HRESULT STDMETHODCALLTYPE Clone(IEnumString *this, IEnumString **lplpenumstring);


/* IEnumString VTable. */
static IEnumStringVtbl g_CTexEnum_Vtbl =
{
	QueryInterface,
	AddRef,
	Release,
	Next,
	Skip,
	Reset,
	Clone
};


/* CreateTexEnum
 *   Creates a new texture enumerator.
 *
 * Parameters:
 *   None.
 *
 * Return value: LPTEXENUM
 *   Pointer to enumerator, or NULL on error.
 *
 * Remarks:
 *   This is the outside world's method of getting a reference to an enumerator
 *   object. It is created with one reference. If we fail, we initiate the fatal
 *   error procedure. Call DestroyTexEnum to release the reference.
 */
LPTEXENUM CreateTexEnum(void)
{
	CTexEnum* lptexenum = CreateCTexEnum();

	if(!lptexenum)
	{
		Die(IDS_MEMORY);
		return NULL;
	}

	/* Give the outside world one and only one reference. People outside the app
	 * (i.e. the shell) might take out additional references, but they should
	 * release them, too, and since we use explicit destruction (which is really
	 * just a release), one reference is all we'll ever need.
	 */
	lptexenum->lpVtbl->AddRef((IEnumString*)lptexenum);

	return lptexenum;
}


/* CreateCTexEnum
 *   Creates a new CTexEnum object.
 *
 * Parameters:
 *   None.
 *
 * Return value: CTexEnum*
 *   Pointer to object, or NULL on error.
 *
 * Remarks:
 *   This is the internal method of creating an enumerator. It is initialised
 *   and has zero references, so call AddRef on the returned object before
 *   giving it to anyone. The object will free itself when the last reference is
 *   released.
 */
static CTexEnum* CreateCTexEnum(void)
{
	CTexEnum *lptexenum = CoTaskMemAlloc(sizeof(CTexEnum));

	if(lptexenum)
	{
		lptexenum->lpVtbl = &g_CTexEnum_Vtbl;
		lptexenum->lpszTextures = NULL;
		lptexenum->iCurrentTexture = lptexenum->iNumTextures = 0;

		/* No initial references; the caller should call AddRef. */
		lptexenum->dwRefCount = 0;
	}

	return lptexenum;
}


/* DestroyTexEnum
 *   Destroys a texture enumerator created with CreateTexEnum.
 *
 * Parameters:
 *   LPTEXENUM	lptexenum	Enumerator to destroy.
 *
 * Return value: None.
 *
 * Remarks:
 *   This is the outside world's method of releasing its sole reference to an
 *   enumerator object created by CreateTexEnum. This should be the only
 *   outstanding reference by the time we're called, so this really does result
 *   in the destruction of the object.
 */
void DestroyTexEnum(LPTEXENUM lptexenum)
{
	lptexenum->lpVtbl->Release((IEnumString*)lptexenum);
}


/* SetTexEnumList
 *   Sets the list of textures to be enumerated by a texture enumerator.
 *
 * Parameters:
 *   LPTEXENUM			lptexenum	Enumerator.
 *   TEXTURENAMELIST*	lptnl		*Sorted* list of texture names.
 *
 * Return value: None.
 *
 * Remarks:
 *   Duplicates are discarded gracefully.
 */
void SetTexEnumList(LPTEXENUM lptexenum, TEXTURENAMELIST *lptnl)
{
	int i;

	FreeTextureNames(lptexenum);

	/* Allocate room for the array of string pointers. If there are duplicates,
	 * this will overestimate, but no harm done.
	 */
	lptexenum->lpszTextures = CoTaskMemAlloc(lptnl->iEntries * sizeof(LPOLESTR*));
	if(!lptexenum->lpszTextures) Die(IDS_MEMORY);

	/* Copy the texture names into our array, skipping duplicates. */
	lptexenum->iNumTextures = 0;
	for(i = 0; i < lptnl->iEntries; i++)
	{
		/* The list is sorted, so we have a duplicate iff we're the same as the
		 * next.
		 */
		if(i == 0 || _tcscmp(lptnl->lpszTexNames[i], lptnl->lpszTexNames[i-1]))
		{
			unsigned int cchTexName = _tcslen(lptnl->lpszTexNames[i]) + 1;
			LPOLESTR szTexName = CoTaskMemAlloc(cchTexName * sizeof(OLECHAR));
			if(!szTexName) Die(IDS_MEMORY);

#ifdef UNICODE
			wcscpy(szTexName, lptnl->lpszTexNames[i]);
#else
			MultiByteToWideChar(CP_ACP, MB_PRECOMPOSED, lptnl->lpszTexNames[i], -1, szTexName, cchTexName);
#endif

			lptexenum->lpszTextures[lptexenum->iNumTextures++] = szTexName;
		}
	}
}


/* FreeTextureNames
 *   Frees the internal texture names for an enumerator, if any exist.
 *
 * Parameters:
 *   LPTEXENUM	lptexenum	Enumerator.
 *
 * Return value: None.
 *
 * Remarks:
 *   This is called by SetTexEnumList before it sets the new lists.
 */
static void FreeTextureNames(LPTEXENUM lptexenum)
{
	int i;

	if(lptexenum->lpszTextures)
	{
		/* Free existing textures. */
		for(i = 0; i < lptexenum->iNumTextures; i++)
			CoTaskMemFree(lptexenum->lpszTextures[i]);

		CoTaskMemFree(lptexenum->lpszTextures);
	}
}


/* CreateAutoComplete
 *   Creates an instance of the shell's implementation of IAutoComplete2.
 *
 * Parameters:
 *   None.
 *
 * Return value: IAutoComplete2*
 *   Pointer to autocomplete interface, or NULL on error.
 */
IAutoComplete2* CreateAutoComplete(void)
{
	IAutoComplete2 *lpac2;

	/* Get an IAutoComplete2 pointer to an autocomplete object. */
	if(FAILED(CoCreateInstance(&CLSID_AutoComplete, NULL, CLSCTX_INPROC_SERVER, &IID_IAutoComplete2, (LPVOID)&lpac2)))
		return NULL;

	lpac2->lpVtbl->SetOptions(lpac2, ACO_AUTOSUGGEST | ACO_UPDOWNKEYDROPSLIST);

	return lpac2;
}



/******************************************************************************
 * IEnumString implementation.
 ******************************************************************************/


/* IEnumString::QueryInterface
 *   Retrieves an interface reference.
 *
 * Parameters:
 *   IEnumString*	this				Pointer to an enumerator.
 *   REFIID			riidInterfaceGUID	GUID of desired interface.
 *   void**			lplpvInterface		Pointer in which to return the reference
 *										to the interface.
 *
 * Return value: HRESULT
 *   S_OK on success; E_NOINTERFACE if the requested interface is not available.
 */
static HRESULT STDMETHODCALLTYPE QueryInterface(IEnumString *this, REFIID riidInterfaceGUID, void **lplpvInterface)
{
	*lplpvInterface = NULL;

	/* If we've been asked for anything other than IUnknown or IEnumString,
	 * fail.
	 */
	if(!IsEqualIID(riidInterfaceGUID, &IID_IUnknown) && !IsEqualIID(riidInterfaceGUID, &IID_IEnumString))
		return E_NOINTERFACE;

	/* Succeed. */
	*lplpvInterface = this;
	this->lpVtbl->AddRef(this);

	return S_OK;
}


/* IEnumString::AddRef
 *   Takes out another reference on an existing interface pointer.
 *
 * Parameters:
 *   IEnumString*	this	Pointer to an enumerator.
 *
 * Return value: ULONG
 *   New number of references outstanding on the interface.
 */
static ULONG STDMETHODCALLTYPE AddRef(IEnumString *this)
{
	return ++((CTexEnum*)this)->dwRefCount;
}


/* IEnumString::Release
 *   Releases a reference to an interface.
 *
 * Parameters:
 *   IEnumString*	this	Pointer to an enumerator.
 *
 * Return value: ULONG
 *   New number of references outstanding on the interface.
 *
 * Remarks:
 *   The outside world should not call this method directly, but rather should
 *   call DestroyTexEnum.
 */
static ULONG STDMETHODCALLTYPE Release(IEnumString *this)
{
	CTexEnum *lptexenum = (CTexEnum*)this;

	lptexenum->dwRefCount--;

	/* If the last reference has been release, free the object. */
	if(lptexenum->dwRefCount == 0)
	{
		FreeTextureNames(lptexenum);
		CoTaskMemFree(lptexenum);
		return 0;
	}

	return lptexenum->dwRefCount;
}


/* IEnumString::Next
 *   Enumerates a specified number of texture names.
 *
 * Parameters:
 *   IEnumString*	this			Pointer to an enumerator.
 *   ULONG			cstr			Requested number of texture names.
 *   LPOLESTR*		lpsz			Buffer into which to return pointers to the
 *									names.
 *   ULONG*			lpcstrFetched	Buffer into which to return the number of
 *									strings enumerated; may be NULL if
 *									cstr == 1.
 *
 * Return value: HRESULT
 *   S_OK if cstr names were enumerated; S_FALSE otherwise.
 */
static HRESULT STDMETHODCALLTYPE Next(IEnumString *this, ULONG cstr, LPOLESTR *lpsz, ULONG *lpcstrFetched)
{
	CTexEnum *lptexenum = (CTexEnum*)this;
	ULONG cstrActual = min(cstr, (ULONG)(lptexenum->iNumTextures - lptexenum->iCurrentTexture));

	/* If the caller wants to know how many we retrieved, tell it. */
	if(lpcstrFetched) *lpcstrFetched = cstrActual;

	/* If we have any left, copy their addresses in. */
	if(cstrActual > 0)
	{
		ULONG i;

		for(i = 0; i < cstrActual; i++)
		{
			/* Allocate memory for the string. The caller frees it, apparently;
			 * this is how the one other implementation I've seen does it, and
			 * it crashes with a heap assertion if I don't allocate a new block
			 * (possibly because it's trying to free in the middle of my heap
			 * block), but I can't find anything about this in the specs.
			 * Perhaps it's normal for enumerators to create new instances?
			 */
			lpsz[i] = CoTaskMemAlloc((wcslen(lptexenum->lpszTextures[lptexenum->iCurrentTexture + i]) + 1) * sizeof(OLECHAR));

			if(!lpsz[i])
			{
				/* Memory allocation failed, so record that we only returned as
				 * many strings as we have so far, and then stop enumerating.
				 */
				cstrActual = i;
				break;
			}

			/* Copy it in. */
			wcscpy(lpsz[i], lptexenum->lpszTextures[lptexenum->iCurrentTexture + i]);
		}

		lptexenum->iCurrentTexture += cstrActual;
	}

	return cstrActual == cstr ? S_OK : S_FALSE;
}


/* IEnumString::Skip
 *   Skips a specified number of texture names.
 *
 * Parameters:
 *   IEnumString*	this	Pointer to an enumerator.
 *   ULONG			cstr	Number of texture names to skip.
 *
 * Return value: HRESULT
 *   S_OK if cstr names were skipped; S_FALSE otherwise.
 */
static HRESULT STDMETHODCALLTYPE Skip(IEnumString *this, ULONG cstr)
{
	CTexEnum *lptexenum = (CTexEnum*)this;
	ULONG cstrActual = min(cstr, (ULONG)(lptexenum->iNumTextures - lptexenum->iCurrentTexture));
	lptexenum->iCurrentTexture += cstrActual;
	return cstrActual == cstr ? S_OK : S_FALSE;
}


/* IEnumString::Reset
 *   Resets enumerator so that subsequent enumerations begin from the first
 *   string.
 *
 * Parameters:
 *   IEnumString*	this	Pointer to an enumerator.
 *
 * Return value: HRESULT
 *   S_OK.
 */
static HRESULT STDMETHODCALLTYPE Reset(IEnumString *this)
{
	((CTexEnum*)this)->iCurrentTexture = 0;
	return S_OK;
}


/* IEnumString::Clone
 *   Obtains a reference to a new IEnumString with the same enumeration state
 *   as the object itself.
 *
 * Parameters:
 *   IEnumString*	this			Pointer to an enumerator.
 *   IEnumString**	lplpenumstring	Buffer into which to return reference to
 *									clone.
 *
 * Return value: HRESULT
 *   S_OK if successful; E_OUTOFMEMORY if allocation failed; E_INVALIDARG if a
 *   NULL pointer was passed in.
 */
static HRESULT STDMETHODCALLTYPE Clone(IEnumString *this, IEnumString **lplpenumstring)
{
	CTexEnum *lptexenum = (CTexEnum*)this;
	CTexEnum *lptexenumClone;

	/* Sanity check. */
	if(!lplpenumstring)
		return E_INVALIDARG;

	/* Attempt to create a new enumerator. */
	lptexenumClone = CreateCTexEnum();

	/* Failed? */
	if(!lptexenumClone)
		return E_OUTOFMEMORY;

	/* We need one reference to the clone. */
	lptexenumClone->lpVtbl->AddRef((IEnumString*)lptexenumClone);

	/* Set the clone's enumeration state to be the same as ours. */
	lptexenumClone->iCurrentTexture = lptexenum->iCurrentTexture;
	lptexenumClone->iNumTextures = lptexenum->iNumTextures;
	lptexenumClone->lpszTextures = lptexenum->lpszTextures;

	return S_OK;
}
