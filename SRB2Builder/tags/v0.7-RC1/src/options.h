/* SRB2 Workbench -- A map editor for Sonic Robo Blast 2.
 * Copyright (C) 2009 Gregor Dick
 * http://workbench.srb2.org/
 *
 * Incorporates portions of Doom Builder, (C) 2003 Pascal vd Heiden.
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 * USA.
 *
 * options.h: Header for options.c.
 *
 * AMDG.
 */

#ifndef __SRB2B_OPTIONS__
#define __SRB2B_OPTIONS__

#include "config.h"

#include "DockWnd/DockWnd.h"


/* Sections. */
#define OPT_RECENT		TEXT("recent")
#define OPT_PALETTE		TEXT("palette")
#define OPT_SHORTCUTS	TEXT("shortcuts")
#define OPT_GAMECONFIGS	TEXT("gameconfigs")
#define OPT_DEFAULTTEX	TEXT("defaulttexture")
#define OPT_DEFAULTSEC	TEXT("defaultsector")
#define OPT_NODEBUILDER	TEXT("nodebuilder")
#define OPT_WIKI		TEXT("wiki")
#define OPT_WINDOW		TEXT("window")

/* Top-level options. */
#define OPT_ADDITIVESELECT		TEXT("additiveselect")
#define OPT_AUTOCOMPLETETEX		TEXT("autocompletetex")
#define OPT_AUTOSTITCHDISTANCE	TEXT("autostitchdistance")
#define OPT_CONTEXTCROSS		TEXT("contextcross")
#define OPT_CONTEXTMENUS		TEXT("contextmenus")
#define OPT_CXGRID				TEXT("cxgrid")				/* Cached in MAPVIEW, written when closed. */
#define OPT_CYGRID				TEXT("cygrid")				/* Cached in MAPVIEW, written when closed. */
#define OPT_DEFAULTSNAP			TEXT("defaultsnap")			/* Cached in MAPVIEW, written when closed. */
#define OPT_DEFAULTSTITCH		TEXT("defaultstitch")		/* Cached in MAPVIEW, written when closed. */
#define OPT_DETAILSANY			TEXT("detailsany")
#define OPT_DRAWAXES			TEXT("drawaxes")			/* Cached in MAPVIEW, written when closed. */
#define OPT_GRID64SHOW			TEXT("grid64show")			/* Cached in MAPVIEW, written when closed. */
#define OPT_GRIDSHOW			TEXT("gridshow")			/* Cached in MAPVIEW, written when closed. */
#define OPT_INDICATORSIZE		TEXT("indicatorsize")		/* Cached in RENDEROPTS. */
#define OPT_LINEMODEVERTICES	TEXT("linemodevertices")	/* Cached in RENDEROPTS. */
#define OPT_LINESPLITDISTANCE	TEXT("linesplitdistance")
#define OPT_NEWSECFRONT			TEXT("newsecfront")
#define OPT_NEWSECTORDIALOGUE	TEXT("newsectordialogue")
#define OPT_NEWTHINGDIALOGUE	TEXT("newthingdialogue")
#define OPT_NOTHINGDESELECTS	TEXT("nothingdeselects")
#define OPT_PERSISTENTOPENWAD	TEXT("persistentopenwad")
#define OPT_SCROLLSPEED			TEXT("scrollspeed")
#define OPT_SECMODEVERTICES		TEXT("secmodevertices")		/* Cached in RENDEROPTS. */
#define OPT_TEXBROWSERTAB		TEXT("texbrowsertab")
#define OPT_TYPE				TEXT("type")
#define OPT_UNDOSTITCH			TEXT("undostitch")
#define OPT_USEDONLY			TEXT("usedonly")
#define OPT_VERTEXSIZE			TEXT("vertexsize")			/* Cached in RENDEROPTS. */
#define OPT_VERTEXSNAPDISTANCE	TEXT("vertexsnapdistance")	/* Cached in MAPVIEW, but not written back. */
#define OPT_LINESNAPDISTANCE	TEXT("linesnapdistance")	/* Cached in MAPVIEW, but not written back. */
#define OPT_XOFFGRID			TEXT("xoffgrid")			/* Cached in MAPVIEW, written when closed. */
#define OPT_YOFFGRID			TEXT("yoffgrid")			/* Cached in MAPVIEW, written when closed. */
#define OPT_ZOOMMOUSE			TEXT("zoommouse")
#define OPT_ZOOMSPEED			TEXT("zoomspeed")
#define OPT_LINECOLOURFLAGS		TEXT("linecolourflags")		/* Cached in MAPVIEW, but not written back. */
#define OPT_DRAGTHRESHOLD		TEXT("dragthreshold")
#define OPT_REMEMBERWINDOWPOS	TEXT("rememberwindowpos")
#define OPT_EFFECTNUMBERS		TEXT("effectnumbers")
#define OPT_SHOWNIGHTSPATH		TEXT("shownightspath")

#define OPT_MAX_AUTOSTITCHDISTANCE	255
#define OPT_MIN_AUTOSTITCHDISTANCE	0
#define OPT_MAX_LINESPLITDISTANCE	255
#define OPT_MIN_LINESPLITDISTANCE	0
#define OPT_MAX_VERTEXSNAPDISTANCE	255
#define OPT_MIN_VERTEXSNAPDISTANCE	0
#define OPT_MAX_LINESNAPDISTANCE	255
#define OPT_MIN_LINESNAPDISTANCE	0
#define OPT_MAX_SCROLLSPEED			100
#define OPT_MIN_SCROLLSPEED			1
#define OPT_MAX_ZOOMSPEED			1000
#define OPT_MIN_ZOOMSPEED			1
#define OPT_MAX_INDICATORSIZE		100
#define OPT_MIN_INDICATORSIZE		1
#define OPT_MAX_VERTEXSIZE			100
#define OPT_MIN_VERTEXSIZE			1
#define OPT_MAX_DRAGTHRESHOLD		32
#define OPT_MIN_DRAGTHRESHOLD		0

#define TESTCFG_RENDERER	TEXT("renderer")
#define TESTCFG_WINDOWED	TEXT("windowed")
#define TESTCFG_SFX			TEXT("sfx")
#define TESTCFG_MUSIC		TEXT("music")
#define TESTCFG_MUSICTYPE	TEXT("musictype")
#define TESTCFG_PARAMS		TEXT("params")
#define TESTCFG_SKIN		TEXT("skin")
#define TESTCFG_GAMETYPE	TEXT("gametype")
#define TESTCFG_DIFFICULTY	TEXT("difficulty")

#define NBCFG_BINARY		TEXT("binary")
#define NBCFG_ARGS			TEXT("arguments")

#define WIKICFG_QUERYURLFMT	TEXT("queryurlfmt")
#define WIKICFG_MAINPAGE	TEXT("mainpage")

#define DOCKSTATECFG_TOOLBAR	TEXT("toolbar")
#define DOCKSTATECFG_INFOBAR	TEXT("infobar")
#define DOCKSTATECFG_WADLIST	TEXT("wadlist")

#define WINCFG_X		TEXT("x")
#define WINCFG_Y		TEXT("y")
#define WINCFG_CX		TEXT("width")
#define WINCFG_CY		TEXT("height")
#define WINCFG_STATE	TEXT("state")


#define MRUENTRIES 10

/* The wheel is treated like a keypress. */
#define MOUSE_SCROLL_UP 4008
#define MOUSE_SCROLL_DOWN 4009


/* Types. */

/* Every time you update this table, alter the corresponding menu code map! */
typedef enum _SHORTCUTCODES
{
	SCK_EDITQUICKMOVE,
	SCK_ZOOMIN,
	SCK_ZOOMOUT,
	SCK_CENTREVIEW,
	SCK_EDITMOVE,
	SCK_EDITANY,
	SCK_EDITLINES,
	SCK_EDITSECTORS,
	SCK_EDITVERTICES,
	SCK_EDITTHINGS,
	SCK_EDIT3D,
	SCK_EDITSNAPTOGRID,
	SCK_FLIPLINEDEFS,
	SCK_FLIPSIDEDEFS,
	SCK_SPLITLINEDEFS,
	SCK_JOINSECTORS,
	SCK_MERGESECTORS,
	SCK_UNDO,
	SCK_REDO,
	SCK_FLIPHORIZ,
	SCK_FLIPVERT,
	SCK_COPY,
	SCK_PASTE,
	SCK_SAVEAS,
	SCK_INCFLOOR,
	SCK_DECFLOOR,
	SCK_INCCEIL,
	SCK_DECCEIL,
	SCK_INCLIGHT,
	SCK_DECLIGHT,
	SCK_INCTHINGZ,
	SCK_DECTHINGZ,
	SCK_SNAPSELECTION,
	SCK_GRADIENTFLOORS,
	SCK_GRADIENTCEILINGS,
	SCK_GRADIENTBRIGHTNESS,
	SCK_INSERT,
	SCK_DCKINSERT,
	SCK_EDITCUT,
	SCK_EDITDELETE,
	SCK_CANCEL,
	SCK_GRADIENTTHINGZ,
	SCK_ROTATE,
	SCK_RESIZE,
	SCK_THINGROTACW,
	SCK_THINGROTCW,
	SCK_SELECTALL,
	SCK_SELECTNONE,
	SCK_INVERTSELECTION,
	SCK_COPYPROPS,
	SCK_PASTEPROPS,
	SCK_GRIDINC,
	SCK_GRIDDEC,
	SCK_FIND,
	SCK_REPLACE,
	SCK_NEW,
	SCK_OPEN,
	SCK_SAVE,
	SCK_MAPOPTIONS,
	SCK_MOUSEROTATE,
	SCK_STITCHVERTICES,
	SCK_SCROLL_UP,
	SCK_SCROLL_DOWN,
	SCK_SCROLL_LEFT,
	SCK_SCROLL_RIGHT,
	SCK_TEST,
	SCK_QUICKTEST,
	SCK_IDENTSECTORS,
	SCK_CENTREVIEWSEL,
	SCK_MAKESINGLE,
	SCK_MAKEDOUBLE,
	SCK_REMOVEINTERIOR,
	SCK_RESOLVESEC,
	SCK_BACKTRACKDRAW,
	SCK_SELECTTAGDUAL,
	SCK_EDITFOF,
	SCK_QUICKDRAG,
	SCK_QUICKFOF,
	SCK_OPTIONS,
	SCK_ERRORCHECKER,
	SCK_PROPERTIES,
	SCK_TOGGLEVXSNAP,
	SCK_TOGGLELINESNAP,
	SCK_TOGGLEAUTOSTITCH,
	SCK_EDITHEADER,
	SCK_FIXMISSTEX,
	SCK_AUTOALIGN,
	SCK_DELUNUSEDVX,
	SCK_MAX
	/* _SCK_ */
} SHORTCUTCODES;
/* Every time you update this table, alter the corresponding menu code map! */


typedef struct _RENDEREROPTIONS
{
	int		iIndicatorSize;
	int		iVertexSize;
	BOOL	bVerticesInLinesMode;
	BOOL	bVerticesInSectorsMode;
} RENDEREROPTIONS;



/* extern globals. */
extern CONFIG *g_lpcfgMain;
extern int g_iShortcutCodes[SCK_MAX];
extern RENDEREROPTIONS g_rendopts;

/* Prototypes. */
BOOL LoadMainConfigurationFile(void);
void UnloadMainConfigurationFile(void);
void MRUAdd(LPCTSTR szFilename);
CONFIG* GetOptionsForGame(CONFIG *lpcfgMap);
CONFIG* GetTestingOptions(CONFIG *lpcfgMainGame);
void SetDockWndPosOptions(LPDOCKSAVESTATE lpdss, LPCTSTR szSection);
BOOL GetDockWndPosOptions(LPDOCKSAVESTATE lpdss, LPCTSTR szSection);
BOOL EnsureHaveNodebuilder(void);

/* Inline functions. */
static __inline void MRUClear(void)
{
	ConfigSetSubsection(g_lpcfgMain, OPT_RECENT, ConfigCreate());
}

#endif
