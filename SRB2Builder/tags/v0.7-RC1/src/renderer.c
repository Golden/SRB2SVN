/* SRB2 Workbench -- A map editor for Sonic Robo Blast 2.
 * Copyright (C) 2009 Gregor Dick
 * http://workbench.srb2.org/
 *
 * Incorporates portions of Doom Builder, (C) 2003 Pascal vd Heiden.
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 * USA.
 *
 * renderer.c: Contains both low-level routines for managing the OpenGL surface
 * and drawing primitives, and higher-level functions for rendering map objects
 * and even the entire map.
 *
 * AMDG.
 */

#include <windows.h>
#include <math.h>
#include <stdio.h>
#include <stdarg.h>
#include <tchar.h>

#include <GL/gl.h>
#include <GL/glu.h>

#include "general.h"
#include "maptypes.h"
#include "../res/resource.h"
#include "editing.h"
#include "config.h"
#include "options.h"
#include "renderer.h"
#include "selection.h"

#include "CodeImp/ci_math.h"


/* Used when constructing the palette. */
#define PALETTE_MIX_THINGSCOLOR 0.4
#define PALETTE_MIX_ORIGINALCOLOR 0.4
#define PALETTE_MIX_SELECTIONCOLOR 0.8

#define THING_INSERT_COLOUR	0x808080

#define CROSS_MAP_SIZE 12

#define GL_COLOUR_FROM_INDEX(byColour) (glColor3ub(g_rgbqPalette[byColour].rgbRed, g_rgbqPalette[byColour].rgbGreen, g_rgbqPalette[byColour].rgbBlue))
#define GL_CLEAR_COLOUR_FROM_INDEX(byColour) (glClearColor(g_rgbqPalette[byColour].rgbRed / 255.0f, g_rgbqPalette[byColour].rgbGreen / 255.0f, g_rgbqPalette[byColour].rgbBlue / 255.0f, 0.0f))


/* Axis transfer line record. */
typedef struct _AXISTRANSFERLINE
{
	short	nIndex;
	int		iThing;
} AXISTRANSFERLINE;


/* Globals. */
RGBQUAD g_rgbqPalette[CLR_MAX];	/* Renderer palette. */
GLuint	g_uiFontDLBase;

static HGLRC g_hrc = NULL;


/* Static prototypes. */
static __inline RGBQUAD LongToRGBQUAD(int i);
static void RenderGrid(MAPVIEW *lpmapview, unsigned short cx, unsigned short cy, BYTE byColour);
static BOOL InitGLRC(HDC hdc);
static BOOL CreateGLFont(void);
static __inline void DestroyGLFont(void);
static __inline int DimmedColour(int iColour);
static void Render_AllLinedefs(MAPVIEW *lpmapview, MAP *lpmap, int iIndicatorLength, short nHighlightTag, DWORD dwLineColourFlags);
static void Render_AllVertices(MAP *lpmap, float fVertexSize);
static void Render_NewVertices(MAP *lpmap, float fVertexSize);
static void Render_AllThings(MAP *lpmap, float fImageSize, float fZoom, GLuint uiThingTex, BYTE byFlags);
static __inline int DetermineLinedefColour(MAPLINEDEF *ld, MAP *lpmap, short nHighlightTag, DWORD dwLineColourFlags);
static __inline BOOL TagHighlightsLine(MAPLINEDEF *lpld, MAP *lpmap, short nHighlightTag, DWORD dwLineColourFlags);
static void Render_NiGHTSPath(MAP *lpmap, MAP_RENDERINFO *lpmri);
static int QsortATLComparison(const void *lpvATL1, const void *lpvATL2);



/* InitialiseRenderer
 *   Sets up the renderer during application initialisation.
 *
 * Parameters:
 *   None.
 *
 * Return value: BOOL
 *   TRUE on success; FALSE on failure.
 *
 * Remarks:
 *   Called by main before creating any windows.
 */
BOOL InitialiseRenderer(void)
{
	LoadUserPalette();

	return TRUE;
}


/* ShutdownRenderer
 *   Sets up the renderer during application initialisation.
 *
 * Parameters:
 *   None.
 *
 * Return value: None
 *
 * Remarks:
 *   Called by main shortly before we leave.
 */
void ShutdownRenderer(void)
{
	DestroyGLFont();
	wglDeleteContext(g_hrc);

	SaveUserPalette();
}

/* LoadUserPalette
 *   Loads the renderer palette from the config file.
 *
 * Parameters:
 *   None.
 *
 * Return value: None.
 */
void LoadUserPalette(void)
{
	/* First, get the palette section from the config. */
	CONFIG *lpcfgPal = ConfigGetSubsection(g_lpcfgMain, OPT_PALETTE);

	/* Read in all the colour from the config, converting them to the required
	 * format.
	 */
	g_rgbqPalette[CLR_BACKGROUND] = LongToRGBQUAD(ConfigGetInteger(lpcfgPal, TEXT("CLR_BACKGROUND")));
	g_rgbqPalette[CLR_VERTEX] = LongToRGBQUAD(ConfigGetInteger(lpcfgPal, TEXT("CLR_VERTEX")));
	g_rgbqPalette[CLR_VERTEXSELECTED] = LongToRGBQUAD(ConfigGetInteger(lpcfgPal, TEXT("CLR_VERTEXSELECTED")));
	g_rgbqPalette[CLR_VERTEXHIGHLIGHT] = LongToRGBQUAD(ConfigGetInteger(lpcfgPal, TEXT("CLR_VERTEXHIGHLIGHT")));
	g_rgbqPalette[CLR_LINE] = LongToRGBQUAD(ConfigGetInteger(lpcfgPal, TEXT("CLR_LINE")));
	g_rgbqPalette[CLR_LINEDOUBLE] = LongToRGBQUAD(ConfigGetInteger(lpcfgPal, TEXT("CLR_LINEDOUBLE")));
	g_rgbqPalette[CLR_LINESPECIAL] = LongToRGBQUAD(ConfigGetInteger(lpcfgPal, TEXT("CLR_LINESPECIAL")));
	g_rgbqPalette[CLR_LINESPECIALDOUBLE] = LongToRGBQUAD(ConfigGetInteger(lpcfgPal, TEXT("CLR_LINESPECIALDOUBLE")));
	g_rgbqPalette[CLR_LINESELECTED] = LongToRGBQUAD(ConfigGetInteger(lpcfgPal, TEXT("CLR_LINESELECTED")));
	g_rgbqPalette[CLR_LINEHIGHLIGHT] = LongToRGBQUAD(ConfigGetInteger(lpcfgPal, TEXT("CLR_LINEHIGHLIGHT")));
	g_rgbqPalette[CLR_SECTORTAG] = LongToRGBQUAD(ConfigGetInteger(lpcfgPal, TEXT("CLR_SECTORTAG")));
	g_rgbqPalette[CLR_THINGUNKNOWN] = LongToRGBQUAD(ConfigGetInteger(lpcfgPal, TEXT("CLR_THINGUNKNOWN")));
	g_rgbqPalette[CLR_THINGSELECTED] = LongToRGBQUAD(ConfigGetInteger(lpcfgPal, TEXT("CLR_THINGSELECTED")));
	g_rgbqPalette[CLR_THINGHIGHLIGHT] = LongToRGBQUAD(ConfigGetInteger(lpcfgPal, TEXT("CLR_THINGHIGHLIGHT")));
	g_rgbqPalette[CLR_GRID] = LongToRGBQUAD(ConfigGetInteger(lpcfgPal, TEXT("CLR_GRID")));
	g_rgbqPalette[CLR_GRID64] = LongToRGBQUAD(ConfigGetInteger(lpcfgPal, TEXT("CLR_GRID64")));
	g_rgbqPalette[CLR_LINEBLOCKSOUND] = LongToRGBQUAD(ConfigGetInteger(lpcfgPal, TEXT("CLR_LINEBLOCKSOUND")));
	g_rgbqPalette[CLR_MAPBOUNDARY] = LongToRGBQUAD(ConfigGetInteger(lpcfgPal, TEXT("CLR_MAPBOUNDARY")));
	g_rgbqPalette[CLR_AXES] = LongToRGBQUAD(ConfigGetInteger(lpcfgPal, TEXT("CLR_AXES")));
	g_rgbqPalette[CLR_ZEROHEIGHTLINE] = LongToRGBQUAD(ConfigGetInteger(lpcfgPal, TEXT("CLR_ZEROHEIGHTLINE")));
	g_rgbqPalette[CLR_FOFSECTOR] = LongToRGBQUAD(ConfigGetInteger(lpcfgPal, TEXT("CLR_FOFSECTOR")));
	g_rgbqPalette[CLR_TEXBROWSER] = LongToRGBQUAD(ConfigGetInteger(lpcfgPal, TEXT("CLR_TEXBROWSER")));
	g_rgbqPalette[CLR_LASSO] = LongToRGBQUAD(ConfigGetInteger(lpcfgPal, TEXT("CLR_LASSO")));
	g_rgbqPalette[CLR_NIGHTS] = LongToRGBQUAD(ConfigGetInteger(lpcfgPal, TEXT("CLR_NIGHTS")));

	g_rgbqPalette[CLR_BLACK] = LongToRGBQUAD(0);
}

/* SaveUserPalette
 *   Copies the user palette back into the config.
 *
 * Parameters:
 *   None.
 *
 * Return value: None.
 */
void SaveUserPalette(void)
{
	/* First, get the palette section from the config. */
	CONFIG *lpcfgPal = ConfigGetSubsection(g_lpcfgMain, OPT_PALETTE);

	/* Mollify the compiler. */
	int *lpiPal = (int*)g_rgbqPalette;

	ConfigSetInteger(lpcfgPal, TEXT("CLR_BACKGROUND"), lpiPal[CLR_BACKGROUND]);
	ConfigSetInteger(lpcfgPal, TEXT("CLR_VERTEX"), lpiPal[CLR_VERTEX]);
	ConfigSetInteger(lpcfgPal, TEXT("CLR_VERTEXSELECTED"), lpiPal[CLR_VERTEXSELECTED]);
	ConfigSetInteger(lpcfgPal, TEXT("CLR_VERTEXHIGHLIGHT"), lpiPal[CLR_VERTEXHIGHLIGHT]);
	ConfigSetInteger(lpcfgPal, TEXT("CLR_LINE"), lpiPal[CLR_LINE]);
	ConfigSetInteger(lpcfgPal, TEXT("CLR_LINEDOUBLE"), lpiPal[CLR_LINEDOUBLE]);
	ConfigSetInteger(lpcfgPal, TEXT("CLR_LINESPECIAL"), lpiPal[CLR_LINESPECIAL]);
	ConfigSetInteger(lpcfgPal, TEXT("CLR_LINESPECIALDOUBLE"), lpiPal[CLR_LINESPECIALDOUBLE]);
	ConfigSetInteger(lpcfgPal, TEXT("CLR_LINESELECTED"), lpiPal[CLR_LINESELECTED]);
	ConfigSetInteger(lpcfgPal, TEXT("CLR_LINEHIGHLIGHT"), lpiPal[CLR_LINEHIGHLIGHT]);
	ConfigSetInteger(lpcfgPal, TEXT("CLR_SECTORTAG"), lpiPal[CLR_SECTORTAG]);
	ConfigSetInteger(lpcfgPal, TEXT("CLR_THINGUNKNOWN"), lpiPal[CLR_THINGUNKNOWN]);
	ConfigSetInteger(lpcfgPal, TEXT("CLR_THINGSELECTED"), lpiPal[CLR_THINGSELECTED]);
	ConfigSetInteger(lpcfgPal, TEXT("CLR_THINGHIGHLIGHT"), lpiPal[CLR_THINGHIGHLIGHT]);
	ConfigSetInteger(lpcfgPal, TEXT("CLR_GRID"), lpiPal[CLR_GRID]);
	ConfigSetInteger(lpcfgPal, TEXT("CLR_GRID64"), lpiPal[CLR_GRID64]);
	ConfigSetInteger(lpcfgPal, TEXT("CLR_LINEBLOCKSOUND"), lpiPal[CLR_LINEBLOCKSOUND]);
	ConfigSetInteger(lpcfgPal, TEXT("CLR_MAPBOUNDARY"), lpiPal[CLR_MAPBOUNDARY]);
	ConfigSetInteger(lpcfgPal, TEXT("CLR_AXES"), lpiPal[CLR_AXES]);
	ConfigSetInteger(lpcfgPal, TEXT("CLR_ZEROHEIGHTLINE"), lpiPal[CLR_ZEROHEIGHTLINE]);
	ConfigSetInteger(lpcfgPal, TEXT("CLR_FOFSECTOR"), lpiPal[CLR_FOFSECTOR]);
	ConfigSetInteger(lpcfgPal, TEXT("CLR_TEXBROWSER"), lpiPal[CLR_TEXBROWSER]);
	ConfigSetInteger(lpcfgPal, TEXT("CLR_LASSO"), lpiPal[CLR_LASSO]);
	ConfigSetInteger(lpcfgPal, TEXT("CLR_NIGHTS"), lpiPal[CLR_NIGHTS]);
}


/* LongToRGBQUAD
 *   Converts an RGB integer to an RGBQUAD.
 *
 * Parameters:
 *   int	i	Integer with B in bits 16..23, G in 8..15 and R in 0..7.
 *
 * Return value: RGBQUAD
 *   Equivalent RGBQUAD.
 */
static __inline RGBQUAD LongToRGBQUAD(int i)
{
	DWORD dw = i & 0xFFFFFF;

	/* The compiler takes a bit of persuading to return a DWORD as an RGBQUAD. */
	return *((RGBQUAD*)&dw);
}




/* RedrawMap
 *   Redraws the map to the backbuffer.
 *
 * Parameters:
 *   MAP*				lpmap		Map to redraw.
 *   MAP_RENDERINFO*	lpmri		Parameters affecting what to draw.
 *
 * Return value: None
 */
void RedrawMap(MAP *lpmap, MAP_RENDERINFO *lpmri)
{
	BOOL bWantVertices = FALSE;
	MAPVIEW *lpmapview = lpmri->lpmapview;
	DWORD dwLineColourFlags;

	GL_CLEAR_COLOUR_FROM_INDEX(CLR_BACKGROUND);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glLoadIdentity();
	glScalef(lpmapview->fZoom, -lpmapview->fZoom, 1.0f);
	glTranslatef(-lpmapview->xLeft, -lpmapview->yTop, 0.0f);


	/* Disable antialiasing while we draw the grid. */
	glDisable(GL_LINE_SMOOTH);

	/* User-grid and 64-grid. */
	if(lpmapview->bShowGrid)
		RenderGrid(lpmapview, lpmapview->cxGrid, lpmapview->cyGrid, CLR_GRID);

	if(lpmapview->bShow64Grid && (lpmapview->cxGrid <= 64 || lpmapview->cyGrid <= 64))
		RenderGrid(lpmapview, 64, 64, CLR_GRID64);


	/* Map boundaries. */
	Render_LineF(-32768, -32768, 32767, -32768, CLR_MAPBOUNDARY);
	Render_LineF(32767, -32768, 32767, 32767, CLR_MAPBOUNDARY);
	Render_LineF(32767, 32767, -32768, 32767, CLR_MAPBOUNDARY);
	Render_LineF(-32768, 32767, -32768, -32768, CLR_MAPBOUNDARY);


	/* Axes. */
	if(lpmapview->bShowAxes)
	{
		Render_LineF(0, lpmapview->yTop, 0, lpmapview->yTop - lpmapview->cyDrawSurface / lpmapview->fZoom, CLR_AXES);
		Render_LineF(lpmapview->xLeft, 0, lpmapview->xLeft + lpmapview->cxDrawSurface / lpmapview->fZoom, 0, CLR_AXES);
	}


	/* Re-enable antialiasing. */
	glEnable(GL_LINE_SMOOTH);

	/* Get flags for line-drawing. */
	dwLineColourFlags = lpmapview->dwLineColourFlags & LCF_DIRECTPASS_MASK;

	/* If we tag brethren, tag both sectors and lines; otherwise, tag only the
	 * sort opposing the highlight type.
	 */
	if(lpmapview->dwLineColourFlags & LCF_TAGBRETHREN)
		dwLineColourFlags |= LCF_TAGSECTORS | LCF_TAGLINES;
	else
		dwLineColourFlags |= (lpmri->emHighlightType == EM_SECTORS) ? LCF_TAGLINES : LCF_TAGSECTORS;

	/* Render all the linedefs, in their appropriate colour. */
	Render_AllLinedefs(lpmapview, lpmap, lpmapview->iIndicatorSize, lpmri->nHighlightTag, dwLineColourFlags);


	/* Mode-specific stuff, including mode-dependent submode things. */
	switch(lpmri->editmode)
	{
	case EM_LINES:
		if(g_rendopts.bVerticesInLinesMode)
		{
			bWantVertices = TRUE;
			Render_AllVertices(lpmap, lpmapview->fVertexSize);
		}

		Render_AllThings(lpmap, lpmapview->fThingSize, lpmapview->fZoom, lpmapview->uiThingTex, TRF_DIMMED);
		break;

	case EM_ANY:
		bWantVertices = TRUE;
		Render_AllVertices(lpmap, lpmapview->fVertexSize);
		Render_AllThings(lpmap, lpmapview->fThingSize, lpmapview->fZoom, lpmapview->uiThingTex, 0);
		break;

	case EM_SECTORS:
	case EM_MOVE:
		if(g_rendopts.bVerticesInSectorsMode)
		{
			bWantVertices = TRUE;
			Render_AllVertices(lpmap, lpmapview->fVertexSize);
		}

		Render_AllThings(lpmap, lpmapview->fThingSize, lpmapview->fZoom, lpmapview->uiThingTex, TRF_DIMMED);
		break;

	case EM_VERTICES:
		bWantVertices = TRUE;
		Render_AllVertices(lpmap, lpmapview->fVertexSize);
		Render_AllThings(lpmap, lpmapview->fThingSize, lpmapview->fZoom, lpmapview->uiThingTex, TRF_DIMMED);
		break;

	case EM_THINGS:
		Render_AllThings(lpmap, lpmapview->fThingSize, lpmapview->fZoom, lpmapview->uiThingTex, 0);
		break;

	default:
        break;
	}

	if(lpmri->dwFlags & RF_NIGHTS)
		Render_NiGHTSPath(lpmap, lpmri);

	/* Submode-specific stuff that doesn't depend on mode. */
	switch(lpmri->submode)
	{
	case ESM_DRAWING:
		switch(lpmri->insertmode)
		{
		case IM_PATH:

			/* Render the drawing line from the last-drawn vertex to the mouse.
			 */
			Render_LinedefLineF(lpmri->ptSrc.x, lpmri->ptSrc.y, lpmri->ptDest.x, lpmri->ptDest.y, CLR_LINEHIGHLIGHT, lpmapview->iIndicatorSize);
			Render_LineLengthF(lpmapview, lpmri->ptSrc.x, lpmri->ptSrc.y, lpmri->ptDest.x, lpmri->ptDest.y, CLR_LINEHIGHLIGHT);

			break;

		case IM_DCK:

			/* Render the drawing lines for the rectangle. */

			Render_LinedefLineF(lpmri->ptSrc.x, lpmri->ptSrc.y, lpmri->ptDest.x, lpmri->ptSrc.y, CLR_LINEHIGHLIGHT, 0);
			Render_LinedefLineF(lpmri->ptSrc.x, lpmri->ptSrc.y, lpmri->ptSrc.x, lpmri->ptDest.y, CLR_LINEHIGHLIGHT, 0);
			Render_LinedefLineF(lpmri->ptDest.x, lpmri->ptSrc.y, lpmri->ptDest.x, lpmri->ptDest.y, CLR_LINEHIGHLIGHT, 0);
			Render_LinedefLineF(lpmri->ptSrc.x, lpmri->ptDest.y, lpmri->ptDest.x, lpmri->ptDest.y, CLR_LINEHIGHLIGHT, 0);

			Render_LineLengthF(lpmapview, lpmri->ptSrc.x, lpmri->ptSrc.y, lpmri->ptDest.x, lpmri->ptSrc.y, CLR_LINEHIGHLIGHT);
			Render_LineLengthF(lpmapview, lpmri->ptSrc.x, lpmri->ptSrc.y, lpmri->ptSrc.x, lpmri->ptDest.y, CLR_LINEHIGHLIGHT);
			Render_LineLengthF(lpmapview, lpmri->ptDest.x, lpmri->ptSrc.y, lpmri->ptDest.x, lpmri->ptDest.y, CLR_LINEHIGHLIGHT);
			Render_LineLengthF(lpmapview, lpmri->ptSrc.x, lpmri->ptDest.y, lpmri->ptDest.x, lpmri->ptDest.y, CLR_LINEHIGHLIGHT);

			break;

		default:
			break;
		}

		/* If we *wouldn't* normally renderer vertices, render the new ones
		 * specially.
		 */
		if(!bWantVertices)
			Render_NewVertices(lpmap, lpmapview->fVertexSize);

		/* Fall through. */
	case ESM_INSERTING:
		/* Draw pseudo-vertices that don't actually exist yet. */
		if(lpmri->insertmode != IM_THING)
		{
			Render_SquareF((float)lpmri->ptDest.x, (float)lpmri->ptDest.y, lpmapview->fVertexSize, CLR_VERTEXHIGHLIGHT);

			if(lpmri->insertmode == IM_DCK)
			{
				Render_SquareF((float)lpmri->ptSrc.x, (float)lpmri->ptDest.y, lpmapview->fVertexSize, CLR_VERTEXHIGHLIGHT);
				Render_SquareF((float)lpmri->ptDest.x, (float)lpmri->ptSrc.y, lpmapview->fVertexSize, CLR_VERTEXHIGHLIGHT);
			}
		}
		else
			Render_ThingF((float)lpmri->ptDest.x, (float)lpmri->ptDest.y, -1, THING_INSERT_COLOUR, lpmapview->fThingSize, lpmapview->fZoom, lpmapview->uiThingTex);

		break;

	case ESM_ROTATING:
		{
			int i;

			for(i = 0; i < lpmri->lpselection->lpsellistThings->iDataCount; i++)
			{
				Render_LineF(
					lpmap->things[lpmri->lpselection->lpsellistThings->lpiIndices[i]].x,
					lpmap->things[lpmri->lpselection->lpsellistThings->lpiIndices[i]].y,
					(float)lpmri->ptDest.x,
					(float)lpmri->ptDest.y,
					CLR_THINGSELECTED);

				Render_LineLengthF(
					lpmapview,
					lpmap->things[lpmri->lpselection->lpsellistThings->lpiIndices[i]].x,
					lpmap->things[lpmri->lpselection->lpsellistThings->lpiIndices[i]].y,
					lpmri->ptDest.x,
					lpmri->ptDest.y,
					CLR_THINGSELECTED);
			}
		}

		break;

	case ESM_SELECTING:
		Render_OutlineRect(lpmri->fptSrc.x, lpmri->fptSrc.y, lpmri->fptDest.x, lpmri->fptDest.y, CLR_LASSO);
		break;

	default:
        break;
	}

	/* Cross for indicating the position at which an action will occur. */
	if(lpmri->dwFlags & RF_CROSS)
	{
		float fBoundingBoxRadius = CROSS_MAP_SIZE / (2*lpmapview->fZoom);
		Render_LineF(lpmri->ptsCross.x - fBoundingBoxRadius, lpmri->ptsCross.y - fBoundingBoxRadius, lpmri->ptsCross.x + fBoundingBoxRadius, lpmri->ptsCross.y + fBoundingBoxRadius, CLR_LINE);
		Render_LineF(lpmri->ptsCross.x + fBoundingBoxRadius, lpmri->ptsCross.y - fBoundingBoxRadius, lpmri->ptsCross.x - fBoundingBoxRadius, lpmri->ptsCross.y + fBoundingBoxRadius, CLR_LINE);
	}
}


/* SetZoom
 *   Alters the zoom for a map view.
 *
 * Parameters:
 *   MAPVIEW*	lpmapview	View settings (dimensions, zoom etc.)
 *   float		fZoom		New zoom level.
 *
 * Return value: None
 *
 * Remarks:
 *   Adjusts other parameters that depend on zoom. Call this whenever the
 *   vertex-size, line-size etc. options change. It's safe to call with the
 *   current zoom.
 */
void SetZoom(MAPVIEW *lpmapview, float fZoom)
{
	lpmapview->fZoom = fZoom;
	lpmapview->iIndicatorSize = (int)ceil((1/fZoom + 0.1f) * g_rendopts.iIndicatorSize);

	lpmapview->fVertexSize = (float)g_rendopts.iVertexSize / (float)sqrt(fZoom);

	lpmapview->fThingSize = (1 + (float)ceil(8 * fZoom)) / fZoom;
    if(lpmapview->fThingSize > 32 / fZoom)
		lpmapview->fThingSize = 32 / fZoom;
}


/* RenderGrid
 *   Renders a rectangular grid.
 *
 * Parameters:
 *   MAPVIEW*			lpmapview	View settings (dimensions, zoom etc.)
 *   unsigned short		cx			Horizontal grid spacing.
 *   unsigned short		cy			Vertical grid spacing.
 *   BYTE				byColour	Colour to draw grid.
 *
 * Return value: None
 */
static void RenderGrid(MAPVIEW *lpmapview, unsigned short cx, unsigned short cy, BYTE byColour)
{
	unsigned short iOffset;
	int i;
	int iGridStart, iGridEnd;


	/* Make sure the grid isn't too dense. */
	if(cx * lpmapview->fZoom > 4)
	{
		/* X offset. */
		iOffset = lpmapview->xGridOffset % cx;

		/* Determine horizontal start and end. */
		iGridStart = ((int)(lpmapview->xLeft - cx - iOffset) / cx) * cx + iOffset;
		iGridEnd = ((int)(lpmapview->xLeft + lpmapview->cxDrawSurface / lpmapview->fZoom + cx - iOffset) / cx) * cx + iOffset;

		/* Vertical lines. */
		for(i = iGridStart; i <= iGridEnd; i += cx)
			Render_LineF((float)i, lpmapview->yTop, (float)i, lpmapview->yTop - lpmapview->cyDrawSurface / lpmapview->fZoom, byColour);
	}

	/* Make sure the grid isn't too dense. */
	if(cy * lpmapview->fZoom > 4)
	{
		/* X offset. */
		iOffset = lpmapview->yGridOffset % cy;

		/* Determine vertical start and end. */
		iGridStart = ((int)(-lpmapview->yTop - cy - iOffset) / cy) * cy + iOffset;
		iGridEnd = ((int)(-lpmapview->yTop + lpmapview->cyDrawSurface / lpmapview->fZoom + cy - iOffset) / cy) * cy + iOffset;

		/* Horizontal lines. */
		for(i = iGridStart; i <= iGridEnd; i += cy)
			Render_LineF(lpmapview->xLeft, (float)-i, lpmapview->xLeft + lpmapview->cxDrawSurface / lpmapview->fZoom, (float)-i, byColour);
	}
}


/* InitGLRC
 *   Initialises OpenGL and creates a rendering context.
 *
 * Parameters:
 *   HDC	hdc		Handle to a DC that's had its pixel format set.
 *
 * Return value: BOOL
 *   TRUE on success; FALSE on error.
 */
static BOOL InitGLRC(HDC hdc)
{
	BOOL bFontSucceeded;

	/* Get a rendering context and make it current. */
	g_hrc = wglCreateContext(hdc);
	wglMakeCurrent(hdc, g_hrc);

	glShadeModel(GL_SMOOTH);
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);				/* Black Background */
	glClearDepth(1.0f);

	/* Anti-aliasing. */
	glEnable(GL_LINE_SMOOTH);
	glHint(GL_LINE_SMOOTH_HINT, GL_NICEST);

	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);

	/* Try to create the font. */
	bFontSucceeded = CreateGLFont();

	/* Rendering context no longer current. */
	wglMakeCurrent(NULL, NULL);

	return bFontSucceeded;
}


/* InitGL
 *   Prepares a DC for OpenGL rendering.
 *
 * Parameters:
 *   HDC	hdc		Handle to device context.
 *
 * Return value: BOOL
 *   TRUE on success; FALSE on failure. We can only fail the first time we're
 *   called.
 *
 * Remarks:
 *   The rendering context must be made current for the DC before any GL calls
 *   can be made.
 */
BOOL InitGL(HDC hdc)
{
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormat;

	/* Set the pixel format. */
	ZeroMemory(&pfd, sizeof(pfd));
	pfd.nSize = sizeof(pfd);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cDepthBits = 16;

	iPixelFormat = ChoosePixelFormat(hdc, &pfd);
	iPixelFormat = SetPixelFormat(hdc, iPixelFormat, &pfd);

	/* If we don't have a rendering context yet, make one. We use our DC so that
	 * the rendering context has the correct pixel format, but it's
	 * interchangeable between DCs with the same pixel format.
	 */
	if(!g_hrc)
	{
		/* Try to initialise the renderer. */
		if(!InitGLRC(hdc))
			return FALSE;
	}

	return TRUE;
}


/* RendererMakeDCCurrent
 *   Makes a DC the current target for OpenGL rendering.
 *
 * Parameters:
 *   HDC	hdc		Handle to device context.
 *
 * Return value: None
 *
 * Remarks:
 *   The DC must be passed to InitGLDC before calling this function. You'll
 *   probably also want to call ResizeGLScene after switching DCs.
 */
void RendererMakeDCCurrent(HDC hdc)
{
	wglMakeCurrent(hdc, g_hrc);
}


/* LoadThingCircleTexture
 *   Loads the texture used to make things look round.
 *
 * Parameters:
 *   None.
 *
 * Return value: GLuint
 *   Texture name.
 *
 * Remarks:
 *   Each child window gets its own copy of the texture.
 */
GLuint LoadThingCircleTexture(GLvoid)
{
	HBITMAP hbm;
	BYTE lpbyBits[64][64][4];
	GLuint uiTexName;
	int i;

	/* Get bitmap. */
	hbm = LoadImage(g_hInstance, MAKEINTRESOURCE(IDB_THINGCIRCLE), IMAGE_BITMAP, 0, 0, 0);


	GetBitmapBits(hbm, 4096*4, lpbyBits);
	DeleteObject(hbm);

	/* Set alpha to equal intensity. TODO: Work out a way of storing an RGBA
	 * bitmap as a resource. Maybe just the raw bits?
	 */
	for(i=0; i < 4096; i++)
	{
		((BYTE*)lpbyBits)[4*i+3] = ((BYTE*)lpbyBits)[4*i];
	}

	/* Make texture. */
	glGenTextures(1, &uiTexName);
	glBindTexture(GL_TEXTURE_2D, uiTexName);
	glTexImage2D(GL_TEXTURE_2D, 0, 4, 64, 64, 0, GL_RGBA, GL_UNSIGNED_BYTE, lpbyBits);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	return uiTexName;
}


/* ReSizeGLScene
 *   Resizes the current rendering surface.
 *
 * Parameters:
 *   GLsizei		cx			Width.
 *   GLsizei		cy			Height.
 *
 * Return value: None
 */
GLvoid ReSizeGLScene(GLsizei cx, GLsizei cy)
{
	static GLsizei s_cxLast = 0, s_cyLast = 0;

	/* If we're unchanged, do nothing. */
	if(s_cxLast == cx && s_cyLast == cy)
		return;

	/* Remember where we are for next time. */
	s_cxLast = cx;
	s_cyLast = cy;

	/* Avoid divide-by-zero. */
	if(cy == 0) cy = 1;

	/* Reset the viewport. */
	glViewport(0, 0, cx, cy);

	/* This keeps the scissor box in sync with the window. */
	glScissor(0, 0, cx, cy);

	/* Select and reset the projection matrix. */
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	/* Orthographic view. */
	glOrtho(0.0f, cx, cy, 0.0f, -1.0f, 1.0f);

	/* Select and reset the modelview matrix. */
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
}


/* Render_LineF
 *   Renders a line.
 *
 * Parameters:
 *   float		x1, y1, x2, y2		Endpoint co-ordinates.
 *   BYTE		byColour			Colour index.
 *
 * Return value: None
 */
void __fastcall Render_LineF(float x1, float y1, float x2, float y2, BYTE byColour)
{
	GL_COLOUR_FROM_INDEX(byColour);
	glBegin(GL_LINES);
		glVertex2f(x1, y1);
		glVertex2f(x2, y2);
	glEnd();
}


/* Render_SquareF
 *   Renders a square.
 *
 * Parameters:
 *   float		xCentre, yCentre	Co-ordinates of centre.
 *   float		fHalfSize			Perpendicular distance from centre to edge.
 *   BYTE		byColour			Colour index.
 *
 * Return value: None
 */
void __fastcall Render_SquareF(float xCentre, float yCentre, float fHalfSize, BYTE byColour)
{
	GL_COLOUR_FROM_INDEX(byColour);

	glBegin(GL_TRIANGLE_FAN);
		glVertex2f(xCentre + fHalfSize, yCentre - fHalfSize);
		glVertex2f(xCentre + fHalfSize, yCentre + fHalfSize);
		glVertex2f(xCentre - fHalfSize, yCentre + fHalfSize);
		glVertex2f(xCentre - fHalfSize, yCentre - fHalfSize);
	glEnd();
}

/* Render_Rect
 *   Renders a rectangle.
 *
 * Parameters:
 *   float		x1, y1, x2, y2		Co-ordinates of opposite vertices.
 *   BYTE		byColour			Colour index.
 *
 * Return value: None
 */
void __fastcall Render_Rect(float x1, float y1, float x2, float y2, BYTE byColour)
{
	GL_COLOUR_FROM_INDEX(byColour);

	glBegin(GL_TRIANGLE_FAN);
		glVertex2f(x1, y1);
		glVertex2f(x1, y2);
		glVertex2f(x2, y2);
		glVertex2f(x2, y1);
	glEnd();
}

/* Render_Rect
 *   Renders an outlined rectangle.
 *
 * Parameters:
 *   float		x1, y1, x2, y2		Co-ordinates of opposite vertices.
 *   BYTE		byColour			Colour index.
 *
 * Return value: None
 */
void __fastcall Render_OutlineRect(float x1, float y1, float x2, float y2, BYTE byColour)
{
	GL_COLOUR_FROM_INDEX(byColour);

	glBegin(GL_LINES);

		glVertex2f(x1, y1);
		glVertex2f(x1, y2);

		glVertex2f(x1, y2);
		glVertex2f(x2, y2);

		glVertex2f(x2, y2);
		glVertex2f(x2, y1);

		glVertex2f(x2, y1);
		glVertex2f(x1, y1);

	glEnd();
}


/* Render_ThingPalettedF, Render_ThingF
 *   Renders a thing.
 *
 * Parameters:
 *   float		xCentre, yCentre	Co-ordinates of centre.
 *   int		iAngle				Angle, or negative for a square instead of an
 *									arrow.
 *   [BYTE		byPalColour]		Index of palette colour.
 *   [int		iColour]			RGBQUAD colour.
 *   float		fSize				Radius.
 *   GLuint		uiThingTex			ID of texture used to draw thing.
 *
 * Return value: None
 */
void __fastcall Render_ThingPalettedF(float xCentre, float yCentre, int iAngle, BYTE byPalColour, float fSize, float fZoom, GLuint uiThingTex)
{
	Render_ThingF(xCentre, yCentre, iAngle, *(int*)&g_rgbqPalette[byPalColour], fSize, fZoom, uiThingTex);
}

void __fastcall Render_ThingF(float xCentre, float yCentre, int iAngle, int iColour, float fSize, float fZoom, GLuint uiThingTex)
{
	Render_DiscF(xCentre, yCentre, fSize, iColour, uiThingTex);

	/* Render the arrow/dot only if we're close enough. */
	if(fZoom >= 0.2f)
	{
		if(iAngle >= 0)
		{
			glColor3f(0.0f, 0.0f, 0.0f);

			glPushMatrix();
			glTranslatef(xCentre, yCentre, 0.0f);
			glRotatef((float)iAngle, 0.0f, 0.0f, 1.0f);

			glBegin(GL_LINES);

				/* Bar. */
				glVertex2f(-fSize * 0.5f, 0.0f);
				glVertex2f(fSize * 0.5f, 0.0f);

				/* Pointy bit. */
				glVertex2f(fSize * 0.5f, 0.0f);
				glVertex2f(fSize * 0.1f, fSize * 0.4f);

				glVertex2f(fSize * 0.5f, 0.0f);
				glVertex2f(fSize * 0.1f, -fSize * 0.4f);

			glEnd();

			glPopMatrix();
		}
		else
		{
			Render_DiscF(xCentre, yCentre, fSize / 3, 0, uiThingTex);
		}
	}
}


/* These are just here to let the thing link. */
void __fastcall Render_BoxF(int xCentre, int yCentre, int iRadius, byte byColour)
{
	UNREFERENCED_PARAMETER(xCentre);
	UNREFERENCED_PARAMETER(yCentre);
	UNREFERENCED_PARAMETER(iRadius);
	UNREFERENCED_PARAMETER(byColour);
}

void __fastcall Render_DiscF(float xCentre, float yCentre, float fRadius, int iColour, GLuint uiThingTex)
{
	/* We render the circle by texturing a square. */
	glColor3ub((BYTE)((iColour >> 16) & 0xFF), (BYTE)((iColour >> 8) & 0xFF), (BYTE)(iColour & 0xFF));

	glEnable(GL_TEXTURE_2D);

	glBindTexture(GL_TEXTURE_2D, uiThingTex);

	glBegin(GL_TRIANGLE_FAN);
		glTexCoord2f(1.0f, 0.0f); glVertex2f(xCentre + fRadius, yCentre - fRadius);
		glTexCoord2f(1.0f, 1.0f); glVertex2f(xCentre + fRadius, yCentre + fRadius);
		glTexCoord2f(0.0f, 1.0f); glVertex2f(xCentre - fRadius, yCentre + fRadius);
		glTexCoord2f(0.0f, 0.0f); glVertex2f(xCentre - fRadius, yCentre - fRadius);
	glEnd();

	glDisable(GL_TEXTURE_2D);
}

void __fastcall Render_CirclePalettedF(float xCentre, float yCentre, float fRadius, BYTE byPalColour)
{
	Render_CircleF(xCentre, yCentre, fRadius, *(int*)&g_rgbqPalette[byPalColour]);
}

/* Adapted from http://forum.gamedeception.net/showthread.php?t=4723. */
void __fastcall Render_CircleF(float xCentre, float yCentre, float fRadius, int iColour)
{
    float fAngle;
	int i;

    glColor3ub((BYTE)((iColour >> 16) & 0xFF), (BYTE)((iColour >> 8) & 0xFF), (BYTE)(iColour & 0xFF));
    glBegin(GL_LINE_LOOP);
		for(i = 0; i < 100; i++)
		{
			fAngle = i * 2 * PI/100;
			glVertex2f(xCentre + ((float)cos(fAngle) * fRadius), yCentre + ((float)sin(fAngle) * fRadius));
		}
    glEnd();
}


void __fastcall Render_BitmapF(byte* bitmap, int width, int height, int sx, int sy, int sw, int sh, int tx, int ty, BYTE c1, BYTE c2)
{
	UNREFERENCED_PARAMETER(bitmap);
	UNREFERENCED_PARAMETER(width);
	UNREFERENCED_PARAMETER(height);
	UNREFERENCED_PARAMETER(sx);
	UNREFERENCED_PARAMETER(sy);
	UNREFERENCED_PARAMETER(sw);
	UNREFERENCED_PARAMETER(sh);
	UNREFERENCED_PARAMETER(tx);
	UNREFERENCED_PARAMETER(ty);
	UNREFERENCED_PARAMETER(c1);
	UNREFERENCED_PARAMETER(c2);
}
void __fastcall Render_ScaledBitmapF(byte* bitmap, int width, int height, int sx, int sy, int sw, int sh, int tx, int ty, BYTE c1, BYTE c2)
{
	UNREFERENCED_PARAMETER(bitmap);
	UNREFERENCED_PARAMETER(width);
	UNREFERENCED_PARAMETER(height);
	UNREFERENCED_PARAMETER(sx);
	UNREFERENCED_PARAMETER(sy);
	UNREFERENCED_PARAMETER(sw);
	UNREFERENCED_PARAMETER(sh);
	UNREFERENCED_PARAMETER(tx);
	UNREFERENCED_PARAMETER(ty);
	UNREFERENCED_PARAMETER(c1);
	UNREFERENCED_PARAMETER(c2);
}



/* ZoomToRect
 *   Adjusts the viewport s.t. it centres on a given rectangle, and contains the
 *   entire rectangle.
 *
 * Parameters:
 *   MAPVIEW*	lpmapview			Map view structure.
 *   RECT*		lprc				Rectangle to centre on.
 *   int		iBorderPercentage	Percentage border to add. 100 means 50% of
 *									rect's dimension at each side on dominant
 *									axis.
 *
 * Return value: None.
 */
void ZoomToRect(MAPVIEW *lpmapview, RECT *lprc, int iBorderPercentage)
{
	float fZoom;

	/* Which axis is dominant? */
	if(lpmapview->cyDrawSurface * (lprc->right - lprc->left) > lpmapview->cxDrawSurface * (lprc->top - lprc->bottom))
	{
		/* Horizontal axis. */
		fZoom = (float)lpmapview->cxDrawSurface / (float)(lprc->right - lprc->left);
	}
	else
	{
		/* Vertical axis. */
		fZoom = (float)lpmapview->cyDrawSurface / (float)(lprc->top - lprc->bottom);
	}

	/* Add border. */
	fZoom *= 100.0f;
	fZoom /= (float)(100 + iBorderPercentage);

	/* Set new zoom. */
	SetZoom(lpmapview, fZoom);

	/* Centre view on centre of rectangle. */
	CentreViewAt(lpmapview, (lprc->right + lprc->left) / 2, (lprc->top + lprc->bottom) / 2);
}


/* CentreViewAt
 *   Centres the view at a point.
 *
 * Parameters:
 *   MAPVIEW*	lpmapview	Map view structure.
 *   int		x, y		Point to centre on.
 *
 * Return value: None.
 */
void CentreViewAt(MAPVIEW *lpmapview, int x, int y)
{
	lpmapview->xLeft = x - lpmapview->cxDrawSurface / (lpmapview->fZoom * 2.0f);
	lpmapview->yTop = y + lpmapview->cyDrawSurface / (lpmapview->fZoom * 2.0f);
}


/* CreateGLFont
 *   Creates display lists containing glyphs of the font to be used on the
 *   rendering surface.
 *
 * Parameters: None.
 *
 * Return value: BOOL
 *   TRUE on success; FALSE on error.
 */
static BOOL CreateGLFont(void)
{
	HFONT hfontNew, hfontOld;
	HDC hdc;

	/* Create a font object. */
	hfontNew = CreateFont(-RENDERER_FONT_HEIGHT_PX, 0, 0, 0,
		FW_NORMAL,
		FALSE, FALSE, FALSE,
		ANSI_CHARSET,
		OUT_TT_PRECIS,
		CLIP_DEFAULT_PRECIS,
		ANTIALIASED_QUALITY,
		DEFAULT_PITCH | FF_DONTCARE,
		RENDERER_FONT_FACE);

	/* Make sure it worked. Maybe they don't have the font. */
	if(!hfontNew) return FALSE;

	/* Initialise display list for font. */
	g_uiFontDLBase = glGenLists(RENDERER_FONT_NUM_GLYPHS);

	hdc = CreateCompatibleDC(NULL);
	hfontOld = SelectObject(hdc, hfontNew);

	/* Create the glyphs in their display lists. */
	wglUseFontBitmaps(hdc, RENDERER_FONT_FIRST_GLYPH, RENDERER_FONT_NUM_GLYPHS, g_uiFontDLBase);

	/* Finished. Tidy up. */
	SelectObject(hdc, hfontOld);
	DeleteDC(hdc);
	DeleteObject(hfontNew);

	/* Success. */
	return TRUE;
}


/* DestroyGLFont
 *   Frees the renderer font display lists.
 *
 * Parameters: None.
 *
 * Return value: None.
 */
static __inline void DestroyGLFont(void)
{
	glDeleteLists(g_uiFontDLBase, RENDERER_FONT_NUM_GLYPHS);
}


/* Render_Printf
 *   Outputs text to the OpenGL surface, printf-style.
 *
 * Parameters:
 *   MAPVIEW*	lpmapview	Map-view structure.
 *   float		x, y		Co-ordinates.
 *   BYTE		byColour	Paletted colour.
 *   LPCTSTR	szFormat	printf-esque format string.
 *				...			Format arguments.
 *
 * Return value: None.
 *
 * Remarks:
 *   Adapted from NeHe Tutorial no. 13.
 */
void Render_Printf(MAPVIEW *lpmapview, float x, float y, BYTE byColour, LPCTSTR szFormat, ...)
{
	TCHAR szBuffer[RENDERER_PRINTF_BUF_SIZE];
	va_list valistArgs;
	GLfloat xInitRaster, yInitRaster;

	/* Process the format string. */
	va_start(valistArgs, szFormat);
	_vsntprintf(szBuffer, sizeof(szBuffer) / sizeof(TCHAR), szFormat, valistArgs);
	va_end(valistArgs);

	/* Because of the way raster ops work, colour has to be set before the
	 * raster position.
	 */
	GL_COLOUR_FROM_INDEX(byColour);

	/* We can't set the raster position blindly, since it might be outside the
	 * view volume. Instead, use this trick for the OpenGL FAQ section 10.070.
	 */
	xInitRaster = lpmapview->xLeft + lpmapview->cxDrawSurface / (2*lpmapview->fZoom);
	yInitRaster = lpmapview->yTop - lpmapview->cyDrawSurface / (2*lpmapview->fZoom);
	glRasterPos2f(xInitRaster, yInitRaster);
	glBitmap(0, 0, 0, 0, (x - xInitRaster) * lpmapview->fZoom, (y - yInitRaster) * lpmapview->fZoom, NULL);

	/* Remember this so we can clean up behind ourselves. */
	glPushAttrib(GL_LIST_BIT);

	/* Offset things so that our first glyph maps to zero. */
	glListBase(g_uiFontDLBase - RENDERER_FONT_FIRST_GLYPH);
#ifndef _UNICODE
	glCallLists(strlen(szBuffer), GL_UNSIGNED_BYTE, szBuffer);
#else
	glCallLists(wcslen(szBuffer), GL_UNSIGNED_SHORT, szBuffer);
#endif

	/* Restore the list bit. */
	glPopAttrib();
}


/* DimmedColour
 *   Dims an RGB colour, e.g. for things in non-things modes.
 *
 * Parameters:
 *   int	iColour		Colour to dim.
 *
 * Return value: int
 *   Dimmed colour.
 */
static __inline int DimmedColour(int iColour)
{
	return (iColour >> 1) & 0xFF7F7F7F;
}


/* Render_AllLinedefs
 *   Renders all linedefs in their appropriate colours.
 *
 * Parameters:
 *   MAPVIEW*	lpmapview			Map view information.
 *   MAP*		lpmap				Map structure.
 *   int		iIndicatorLength	Line-side indicator length.
 *   short		nHighlightTag		Tag to highlight, or zero for none.
 *   DWORD		dwLineColourFlags	Flags affecting line colourings. See
 *									ENUM_LINE_COLOUR_FLAGS for possible values.
 *
 * Return value: None.
 *
 * Remarks:
 *   Adapted from Doom Builder.
 */
static void Render_AllLinedefs(MAPVIEW *lpmapview, MAP *lpmap, int iIndicatorLength, short nHighlightTag, DWORD dwLineColourFlags)
{
	int iColour, iLinedef;

	for(iLinedef = 0; iLinedef < lpmap->iLinedefs; iLinedef++)
	{
		MAPLINEDEF *lpld = &lpmap->linedefs[iLinedef];
		MAPVERTEX *lpvx1 = &lpmap->vertices[lpld->v1];
		MAPVERTEX *lpvx2 = &lpmap->vertices[lpld->v2];

		/* Determine linedef colour. */
		if(lpld->highlight) iColour = CLR_LINEHIGHLIGHT;
		else if(lpld->selected || (lpld->editflags & LEF_NEW)) iColour = CLR_LINESELECTED;
		else iColour = DetermineLinedefColour(lpld, lpmap, nHighlightTag, dwLineColourFlags);

		/* Render the linedef. */
		Render_LinedefLineF(lpvx1->x, lpvx1->y, lpvx2->x, lpvx2->y, iColour, iIndicatorLength);

		/* If it's changing, render its length, too. */
		if(lpld->editflags & (LEF_LENGTHCHANGING | LEF_NEW))
			Render_LineLengthF(lpmapview, lpvx1->x, lpvx1->y, lpvx2->x, lpvx2->y, CLR_LINEHIGHLIGHT);
	}
}



/* Render_AllVertices
 *   Renders all vertices.
 *
 * Parameters:
 *   MAP*		lpmap			Map structure.
 *   float		fVertexSize		Size of vertices.
 *
 * Return value: None.
 *
 * Remarks:
 *   Adapted from Doom Builder.
 */
static void Render_AllVertices(MAP *lpmap, float fVertexSize)
{
	int i;

	for(i = 0; i < lpmap->iVertices; i++)
	{
		MAPVERTEX *lpvx = &lpmap->vertices[i];

		/* Assume normal vertex colouring. */
		BYTE byColour = CLR_VERTEX;

		/* Determine the colour. */
		if(lpvx->highlight)
			byColour = CLR_VERTEXHIGHLIGHT;
		else if(lpvx->selected || (lpvx->editflags & VEF_NEW))
			byColour = CLR_VERTEXSELECTED;

		/* Draw the vertex in the appropriate colour. */
		Render_SquareF(lpvx->x, lpvx->y, fVertexSize, byColour);
	}
}


/* Render_NewVertices
 *   Renders all vertices marked VEF_NEW.
 *
 * Parameters:
 *   MAP*		lpmap			Map structure.
 *   float		fVertexSize		Size of vertices.
 *
 * Return value: None.
 *
 * Remarks:
 *   Adapted from Doom Builder.
 */
static void Render_NewVertices(MAP *lpmap, float fVertexSize)
{
	int i;

	for(i = 0; i < lpmap->iVertices; i++)
	{
		MAPVERTEX *lpvx = &lpmap->vertices[i];

		/* Render the vertex iff it's new. */
		if(lpvx->editflags & VEF_NEW)
			Render_SquareF(lpvx->x, lpvx->y, fVertexSize, CLR_VERTEXSELECTED);
	}
}


/* Render_AllThings
 *   Renders all things in the colours cached in the thing structures.
 *
 * Parameters:
 *   MAP*		lpmap			Map structure.
 *   float		fImageSize		Thing GL texture size.
 *   float		fZoom			Current zoom level.
 *   GLuint		uiThingTex		Index of thing GL texture.
 *   BYTE		byFlags			See ENUM_THINGRENDERFLAGS for values.
 *
 * Return value: None.
 *
 * Remarks:
 *   Adapted from Doom Builder.
 */
static void Render_AllThings(MAP *lpmap, float fImageSize, float fZoom, GLuint uiThingTex, BYTE byFlags)
{
	int i;

	for(i = 0; i < lpmap->iThings; i++)
	{
		MAPTHING *lpthing = &lpmap->things[i];
		short nAngle = lpthing->arrow ? lpthing->angle % 360 : -1;

		if(lpthing->highlight)
		{
			/* Render the thing with the highlight palette colour. */
			Render_ThingPalettedF(lpthing->x, lpthing->y, nAngle, CLR_THINGHIGHLIGHT, fImageSize, fZoom, uiThingTex);
		}
		else if(lpthing->selected)
		{
			/* Render the thing with the selection palette colour. */
			Render_ThingPalettedF(lpthing->x, lpthing->y, nAngle, CLR_THINGSELECTED, fImageSize, fZoom, uiThingTex);
		}
		else
		{
			int iColour = (byFlags & TRF_DIMMED) ? DimmedColour(lpthing->color) : lpthing->color;

			/* Render the thing with the colour cached in the structure, dimmed
			 * if requested.
			 */
			Render_ThingF(lpthing->x, lpthing->y, nAngle, iColour, fImageSize, fZoom, uiThingTex);
		}
	}
}


/* DetermineLinedefColour
 *   Determines the appropriate colour for a linedef.
 *
 * Parameters:
 *   MAPLINEDEF*	lpld				Linedef whose colour is to be
 *										determined.
 *   MAP*			lpmap				Map structure.
 *   short			nHighlightTag		Tag to highlight, or zero for none.
 *   DWORD			dwLineColourFlags	Flags affecting line colourings. See
 *										ENUM_LINE_COLOUR_FLAGS for possible
 *										values.
 *
 * Return value: int
 *   Index of palette colour.
 *
 * Remarks:
 *   Adapted from Doom Builder.
 */
static __inline int DetermineLinedefColour(MAPLINEDEF *lpld, MAP *lpmap, short nHighlightTag, DWORD dwLineColourFlags)
{
	/* Any sector containing a FOF gets the FOF colour, if this feature is
	 * enabled.
	 */
	if((dwLineColourFlags & LCF_COLOURFOFLINES) &&
		((SidedefExists(lpmap, lpld->s1) && (lpmap->sectors[lpmap->sidedefs[lpld->s1].sector].editflags & SED_HASFOF)) ||
		(SidedefExists(lpmap, lpld->s2) && (lpmap->sectors[lpmap->sidedefs[lpld->s2].sector].editflags & SED_HASFOF))))
		return CLR_FOFSECTOR;

	/* Do we match the highlighted tag? */
	else if(nHighlightTag != 0 && TagHighlightsLine(lpld, lpmap, nHighlightTag, dwLineColourFlags))
		return CLR_SECTORTAG;

	/* Is the line genuinely impassable or single-sided? */
	else if((lpld->flags & LDF_IMPASSABLE) || ((lpld->flags & LDF_DOUBLESIDED) == 0))
	{
		if(lpld->effect != 0)
			return CLR_LINESPECIAL;
		else
			return CLR_LINE;
	}
	else
	{
		if(lpld->effect != 0)
			return CLR_LINESPECIALDOUBLE;

		else if(lpld->flags & LDF_BLOCKSOUND)
			return CLR_LINEBLOCKSOUND;

		/* Check faux-impassable and zero-height if we can. */
		else if((lpld->flags & LDF_DOUBLESIDED) &&
			SidedefExists(lpmap, lpld->s1) && SidedefExists(lpmap, lpld->s2) &&
			lpmap->sidedefs[lpld->s1].sector >= 0 && lpmap->sidedefs[lpld->s2].sector >= 0)
		{
			MAPSECTOR *front = &lpmap->sectors[lpmap->sidedefs[lpld->s1].sector];
			MAPSECTOR *back = &lpmap->sectors[lpmap->sidedefs[lpld->s2].sector];

			/* If either side has same floor and ceiling, draw impassable.
			 * Otherwise, if both sides have same floor, draw zero-height.
			 * Otherwise, draw two-sided.
			 */
			if((dwLineColourFlags & LCF_FAUXIMPASSABLE) && (front->hceiling == front->hfloor || back->hceiling == back->hfloor))
				return CLR_LINE;
			else if((dwLineColourFlags & LCF_ZEROHEIGHT) && front->hceiling == back->hceiling && front->hfloor == back->hfloor)
				return CLR_ZEROHEIGHTLINE;
			else
				return CLR_LINEDOUBLE;
		}
		else
		{
			/* Double-sided and passable even by SRB2's standards. */
			return CLR_LINEDOUBLE;
		}
	}
}


/* TagHighlightsLine
 *   Determines whether a line should be highlighted if the specified tag is to
 *   be highlighted.
 *
 * Parameters:
 *   MAPLINEDEF*	lpld				Linedef.
 *   MAP*			lpmap				Map structure.
 *   short			nHighlightTag		Tag to highlight, or zero for none.
 *   DWORD			dwLineColourFlags	Flags affecting line colourings. See
 *										ENUM_LINE_COLOUR_FLAGS for possible
 *										values.
 *
 * Return value: BOOL
 *   Whether this line should be tag-highlighted.
 *
 * Remarks:
 *   Adapted from Doom Builder.
 */
static __inline BOOL TagHighlightsLine(MAPLINEDEF *lpld, MAP *lpmap, short nHighlightTag, DWORD dwLineColourFlags)
{
	return ((dwLineColourFlags & LCF_TAGLINES) && lpld->tag == nHighlightTag) ||
		((dwLineColourFlags & LCF_TAGSECTORS) &&
		((SidedefExists(lpmap, lpld->s1) && lpmap->sidedefs[lpld->s1].sector >= 0 && nHighlightTag == lpmap->sectors[lpmap->sidedefs[lpld->s1].sector].tag) ||
		(SidedefExists(lpmap, lpld->s2) && lpmap->sidedefs[lpld->s2].sector >= 0 && nHighlightTag == lpmap->sectors[lpmap->sidedefs[lpld->s2].sector].tag)));
}




/* Render_LinedefLineF
 *   Renders a linedef line.
 *
 * Parameters:
 *   int	x1, y1, x2, y2		Endpoint co-ordinates.
 *   BYTE	byColour			Colour index.
 *   int	iIndicatorLength	Length of indicator.
 *
 * Return value: None
 *
 * Remarks:
 *   Taken from Doom Builder with only cosmetic changes.
 */
void __fastcall Render_LinedefLineF(int x1, int y1, int x2, int y2, BYTE byColour, int iIndicatorLength)
{
	/* Render linedef line */
	Render_LineF((float)x1, (float)y1, (float)x2, (float)y2, byColour);

	/* Render indicator? */
	if(iIndicatorLength > 0)
	{
		/* Middle of indicator line */
		float xMidpoint = (float)(x2 - x1) * 0.5f;
		float yMidpoint = (float)(-y2 + y1) * 0.5f;

		/* Indicator line begin coordinates */
		float xIndicator1 = x1 + xMidpoint;
		float yIndicator1 = -y1 + yMidpoint;
		float xIndicator2;
		float yIndicator2;

		/* Normalize slope and calculate coordinates */
		float fLength = (float)sqrt(xMidpoint * xMidpoint + yMidpoint * yMidpoint);

		if(fLength != 0)
		{
			yIndicator2 = yIndicator1 + iIndicatorLength * (xMidpoint / fLength);
			xIndicator2 = xIndicator1 - iIndicatorLength * (yMidpoint / fLength);
		}
		else
		{
			xIndicator2 = xIndicator1;
			yIndicator2 = yIndicator1;
		}

		/* Render indicator line */
		Render_LineF(xIndicator1, -yIndicator1, xIndicator2, -yIndicator2, byColour);
	}
}



/* Render_LinedefLineF
 *   Renders a single line's length, in its middle.
 *
 * Parameters:
 *   MAPVIEW*	lpmapview		Map view data.
 *   int		x1, y1, x2, y2	Endpoint co-ordinates.
 *   BYTE		byColour		Colour index.
 *
 * Return value: None
 *
 * Remarks:
 *   Adapted from Doom Builder.
 */
void __fastcall Render_LineLengthF(MAPVIEW *lpmapview, int x1, int y1, int x2, int y2, BYTE byColour)
{
	float cx, cy;
	float x, y;
	int iLength;

	// Half the lengths of the line.
	cx = (x2 - x1) / 2.0f;
	cy = (y2 - y1) / 2.0f;

	// Length of line
	iLength = (int)floor(0.5f + distancei(x1, y1, x2, y2));

	if(iLength == 0) return;

	// Start offset for text
	x = x1 + (cx - (((RENDERER_FONT_WIDTH_PX / 2.0f) * (float)ceil(log10(iLength)) / lpmapview->fZoom) / 2));
	y = y1 + cy;

	/* Draw the text on top of a background rectangle. */
	Render_Rect(x, y - 1/lpmapview->fZoom, x + RENDERER_FONT_WIDTH_PX * (float)ceil(log10(iLength)) / lpmapview->fZoom, y + (RENDERER_FONT_HEIGHT_PX - 1) / lpmapview->fZoom, CLR_BACKGROUND);
	Render_Printf(lpmapview, x, y, byColour, TEXT("%d"), iLength);
}


/* Render_NiGHTSPath
 *   Renders all circles and transfer lines.
 *
 * Parameters:
 *   MAP*				lpmap	Map.
 *   MAP_RENDERINFO*	lpmri	Map rendering information.
 *
 * Return value: None.
 *
 * Remarks:
 *   We must have valid NiGHTS settings in *lpmri.
 */
static void Render_NiGHTSPath(MAP *lpmap, MAP_RENDERINFO *lpmri)
{
	int i;

	/* Completely arbitrary first estimate. */
	int iRecordsAllocated = lpmap->iThings / 16 + 1;
	int iRecordsUsed = 0;

	AXISTRANSFERLINE *lpatl = ProcHeapAlloc(iRecordsAllocated * sizeof(AXISTRANSFERLINE));

	/* Find all axis transfer lines. */
	for(i = 0; i < lpmap->iThings; i++)
	{
		if(lpmap->things[i].thing == lpmri->iATLine)
		{
			/* Add to array. */
			while(iRecordsUsed >= iRecordsAllocated)
			{
				iRecordsAllocated <<= 1;
				lpatl = ProcHeapReAlloc(lpatl, iRecordsAllocated * sizeof(AXISTRANSFERLINE));
			}

			lpatl[iRecordsUsed].iThing = i;
			lpatl[iRecordsUsed].nIndex = lpmap->things[i].flag;

			iRecordsUsed++;
		}
	}

	/* Sort array of transfer lines. */
	qsort(lpatl, iRecordsUsed, sizeof(AXISTRANSFERLINE), QsortATLComparison);

	/* For each ATL, look for its partner, and draw the line. */
	i = 0;
	while(i < iRecordsUsed - 1)
	{
		int iNext = i;
		while(iNext < iRecordsUsed - 1 && lpatl[++iNext].nIndex <= lpatl[i].nIndex);

		if(iNext < iRecordsUsed && lpatl[iNext].nIndex == lpatl[i].nIndex + 1)
		{
			Render_LineF(lpmap->things[lpatl[i].iThing].x, lpmap->things[lpatl[i].iThing].y, lpmap->things[lpatl[iNext].iThing].x, lpmap->things[lpatl[iNext].iThing].y, CLR_NIGHTS);

			/* Start looking for partners for the one beyond iNext. */
			i = iNext + 1;
		}
		else
		{
			/* No partner, so start looking for partners for iNext. */
			i = iNext;
		}
	}

	ProcHeapFree(lpatl);

	/* Circles now. */
	for(i = 0; i < lpmap->iThings; i++)
	{
		if(lpmap->things[i].circle)
			Render_CirclePalettedF(lpmap->things[i].x, lpmap->things[i].y, (float)(lpmap->things[i].angle & 0x3FFF), CLR_NIGHTS);
	}
}


static int QsortATLComparison(const void *lpvATL1, const void *lpvATL2)
{
	return (int)((AXISTRANSFERLINE*)lpvATL1)->nIndex - ((AXISTRANSFERLINE*)lpvATL2)->nIndex;
}
