/* SRB2 Workbench -- A map editor for Sonic Robo Blast 2.
 * Copyright (C) 2009 Gregor Dick
 * http://workbench.srb2.org/
 *
 * Incorporates portions of Doom Builder, (C) 2003 Pascal vd Heiden.
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 * USA.
 *
 * openwads.c: Routines for managing the list of open wads.
 *
 * AMDG.
 */

#include <windows.h>
#include <string.h>
#include <tchar.h>

#include "general.h"
#include "maptypes.h"
#include "openwads.h"
#include "options.h"
#include "config.h"
#include "mapconfig.h"
#include "wadopts.h"
#include "../res/resource.h"

#include "win/mdiframe.h"
#include "win/mapwin.h"
#include "win/wadlist.h"

typedef struct _OPENWAD OPENWAD;

struct _OPENWAD
{
	WAD				*lpwad;
	unsigned int	uiReferenceCount;
	int				iID;
	LPTSTR			szFilename;
	BOOL			bNonMapDataChanged;
	CONFIG			*lpcfgWad;
	OPENWAD			*lpowNext;
};

/* Used when enumerating over a certain wad's windows. */
typedef struct _WADWINENUM
{
	WNDENUMPROC	lpEnumProc;
	LPARAM		lParam;
	int			iID;
} WADWINENUM;


static BOOL CALLBACK WinWadCallbackChain(HWND hwnd, LPARAM lParam);
static int AddWad(WAD *lpwad, LPCTSTR szFilename);
static OPENWAD* GetOWFromPath(LPCTSTR szPath);
static OPENWAD* GetOWByID(int iWad);
static __inline BOOL WadIsNew(OPENWAD *lpow);
static __inline BOOL WadRefersToFile(OPENWAD *lpow, LPCTSTR szFileName);
static int BuildNodeBuilderCommandLine(LPCTSTR szFormat, LPCTSTR szBinary, LPCTSTR szFilename, LPCTSTR szLumpname, LPTSTR szBuffer, int cchBuffer);
static BOOL AddUserWadRef(int iWad, void *lpv);
static BOOL ReleaseUserWad(int iWad, void *lpv);


/* Linked list of all open wads. */
OPENWAD g_owOpenWadsHdr, *g_lpowEnd;



/* LoadWad
 *   Loads a wad from disc, if not already open.
 *
 * Parameters:
 *   LPCTSTR		szPath		Path to file to load.
 *
 * Return value: int
 *   ID of wad on success; negative on error.
 *
 * Notes:
 *   If the wad library runs out of memory, we return an error code.
 */
int LoadWad(LPCTSTR szPath)
{
	WAD *lpwad;
	OPENWAD *lpow;

	/* Is the file already open? */
	lpow = GetOWFromPath(szPath);
	if(lpow)
	{
		/* If so, increment the reference count and return the ID. */
		lpow->uiReferenceCount++;
		return lpow->iID;
	}

	/* Not open yet: actually load the wad now. */
	lpwad = OpenWad(szPath);

	if(!lpwad)
	{
		/* Failed to open the wad. Bad file/sharing violation? */
		return -1;
	}

	return AddWad(lpwad, szPath);
}


/* GetWadIDFromPath
 *   Returns the open-wad structure for a given path.
 *
 * Parameters:
 *   LPCTSTR		szPath		Path to check for.
 *
 * Return value: int
 *   Address of open-wad struct on success; NULL if not found.
 *
 * Notes:
 *   Handy for checking whether a certain file is loaded.
 */
static OPENWAD* GetOWFromPath(LPCTSTR szPath)
{
	OPENWAD *lpowRover;

	lpowRover = g_owOpenWadsHdr.lpowNext;
	while(lpowRover && !WadRefersToFile(lpowRover, szPath))
		lpowRover = lpowRover->lpowNext;

	return lpowRover;
}


/* GetWadFilename
 *   Gets the filename to the wad file for an open wad.
 *
 * Parameters:
 *   int		iWad		ID of wad.
 *   LPTSTR		szFilename	Buffer to store path in.
 *   WORD		cchBuffer	Length of buffer in characters, including
 *							terminator.
 *
 * Return value: int
 *   Total number of characters required to store full path, including room for
 *   terminator. Negative on error.
 *
 * Remarks:
 *   Calling with cbBuffer == 0 will simply return the required length,
 *   including room for terminator.
 */
int GetWadFilename(int iWad, LPTSTR szFilename, WORD cchBuffer)
{
	OPENWAD *lpow = GetOWByID(iWad);

	if(lpow && lpow->szFilename)
	{
		if(cchBuffer > 0)
		{
			_tcsncpy(szFilename, lpow->szFilename, cchBuffer - 1);
			szFilename[cchBuffer - 1] = TEXT('\0');
			return min(cchBuffer, _tcslen(lpow->szFilename) + 1);
		}
		else return _tcslen(lpow->szFilename) + 1;
	}

	return -1;
}


/* AddWad
 *   Adds a wad structure to the list of open wads.
 *
 * Parameters:
 *   WAD		*lpwad		Wad to add.
 *   LPCTSTR	szFilename	Filename. May be NULL.
 *
 * Return value: int
 *   ID of wad on success; negative on error.
 */
static int AddWad(WAD *lpwad, LPCTSTR szFilename)
{
	static int iNextWadID = 0;
	OPENWAD *lpowNew;

	if(!lpwad) return -1;

	lpowNew = ProcHeapAlloc(sizeof(OPENWAD));

	lpowNew->lpwad = lpwad;
	lpowNew->iID = iNextWadID++;

	if(szFilename)
	{
		lpowNew->szFilename = ProcHeapAlloc((_tcslen(szFilename) + 1) * sizeof(TCHAR));
		_tcscpy(lpowNew->szFilename, szFilename);
	}
	else lpowNew->szFilename = NULL;

	/* Get the wad's options, creating new ones if necessary. */
	lpowNew->lpcfgWad = szFilename ? LoadWadOptions(szFilename) : NewWadOptions();

	lpowNew->lpowNext = NULL;

	/* One reference initially. */
	lpowNew->uiReferenceCount = 1;
	lpowNew->bNonMapDataChanged = FALSE;
	g_lpowEnd->lpowNext = lpowNew;
	g_lpowEnd = lpowNew;

	return lpowNew->iID;
}

/* NewWad
 *   Creates a new wad and opens it for editing.
 *
 * Parameters:
 *   None
 *
 * Return value: int
 *   ID of wad on success; negative on error.
 *
 * Notes:
 *   If the wad library runs out of memory, we return an error code.
 */
int NewWad(void)
{
	WAD *lpwad = CreateWad();

	if(!lpwad)
	{
		/* Out of memory. Abort. */
		Die(IDS_MEMORY);
		return -1;
	}

	return AddWad(lpwad, NULL);
}


/* SaveWadAs
 *   Saves a wad with the supplied filename.
 *
 * Parameters:
 *   int		iWad				Index of wad to save.
 *   LPTSTR		szFileName			Filename. NULL to use existing name.
 *   CONFIG*	lpcfgChangedMaps	Config containing changed lumpnames on which
 *									to invoke the external nodebuilder. May be
 *									NULL if not required.
 *
 * Return value: int
 *   Zero on success; non-zero on error.
 */
int SaveWadAs(int iWad, LPTSTR szFileName, CONFIG *lpcfgChangedMaps)
{
	BOOL bRet;
	OPENWAD *lpow = GetOWByID(iWad);

	/* Not found? */
	if(!lpow) return 2;

	/* Update the filename if necessary. */
	if(szFileName)
	{
		if(lpow->szFilename) ProcHeapFree(lpow->szFilename);
		lpow->szFilename = ProcHeapAlloc((_tcslen(szFileName) + 1) * sizeof(TCHAR));
		_tcscpy(lpow->szFilename, szFileName);
	}

	/* Specified to save to existing file, but we're new. */
	if(!lpow->szFilename) return 3;

	bRet = WriteWad(lpow->lpwad, lpow->szFilename, lpcfgChangedMaps);

	if(bRet)
	{
		/* Success! */
		lpow->bNonMapDataChanged = FALSE;
		return 0;
	}
	else return 1;
}


/* GetOWByID
 *   Gets a pointer to an OPENWAD structure given its ID.
 *
 * Parameters:
 *   int		iWad		ID of wad to find.
 *
 * Return value: OPENWAD*
 *   Pointer to structure, or NULL if not found.
 */
static OPENWAD* GetOWByID(int iWad)
{
	OPENWAD *lpowRover;

	lpowRover = g_owOpenWadsHdr.lpowNext;
	while(lpowRover && lpowRover->iID != iWad) lpowRover = lpowRover->lpowNext;

	return lpowRover;
}


/* GetWad
 *   Gets a pointer to wad structure given its ID.
 *
 * Parameters:
 *   int		iWad		ID of wad to find.
 *
 * Return value: WAD*
 *   Pointer to wad structure, or NULL if not found.
 */
WAD *GetWad(int iWad)
{
	OPENWAD *lpow = GetOWByID(iWad);
	return lpow ? lpow->lpwad : NULL;
}


/* IncrementWadReferenceCount
 *   Increments the reference count for an open wad.
 *
 * Parameters:
 *   int		iWad		ID of wad.
 *
 * Return value: None.
 *
 * Remarks:
 *   Sometimes we want two references, and there's really no point in calling
 *   LoadWad twice. We might not even have the path handy by the time we decide
 *   we want another reference.
 */
void IncrementWadReferenceCount(int iWad)
{
	OPENWAD *lpow;
	if((lpow = GetOWByID(iWad))) lpow->uiReferenceCount++;
}


/* InitOpenWadsList
 *   Called at app startup to set up some pointers.
 *
 * Parameters:
 *   None.
 *
 * Return value:
 *   None.
 */
void InitOpenWadsList(void)
{
	g_owOpenWadsHdr.lpowNext = NULL;
	g_owOpenWadsHdr.lpwad = NULL;
	g_owOpenWadsHdr.uiReferenceCount = (unsigned)-1;
	g_owOpenWadsHdr.iID = -1;
	g_owOpenWadsHdr.szFilename = NULL;
	g_lpowEnd = &g_owOpenWadsHdr;
}


/* ReleaseWad
 *   Releases a reference to an open wad, and frees it if this was the last.
 *
 * Parameters:
 *   int	iID		ID of wad to release.
 *
 * Return value: int
 *   Zero on success; negative on error.
 */
int ReleaseWad(int iID)
{
	OPENWAD *lpow = &g_owOpenWadsHdr;
	OPENWAD *lpowPrev;

	while(lpowPrev = lpow, (lpow = lpow->lpowNext))
	{
		/* Do we have a match? */
		if(lpow->iID == iID)
		{
			/* Decrement the reference count. */
			lpow->uiReferenceCount--;

			/* Have all references been released? */
			if(lpow->uiReferenceCount == 0)
			{
				/* Save it first if necessary and desired. */
				if(lpow->bNonMapDataChanged)
				{
					TCHAR szSaveChanges[1024];
					int iHeadLength;

					/* Get filename first. */
					*szSaveChanges = TEXT('\0');

					if(lpow->szFilename)
						GetFileTitle(lpow->szFilename, szSaveChanges, NUM_ELEMENTS(szSaveChanges));
					else
						LoadString(g_hInstance, IDS_UNTITLED, szSaveChanges, NUM_ELEMENTS(szSaveChanges));

					iHeadLength = _tcslen(szSaveChanges);
					LoadString(g_hInstance, IDS_SAVECHANGES, szSaveChanges + iHeadLength, NUM_ELEMENTS(szSaveChanges) - iHeadLength);

					if(IDYES == MessageBox(g_hwndMain, szSaveChanges, g_szAppName, MB_ICONEXCLAMATION | MB_YESNO))
						SaveWad(iID, NULL);
				}

				/* Close the hole in the list. */
				lpowPrev->lpowNext = lpow->lpowNext;
				if(!lpowPrev->lpowNext) g_lpowEnd = lpowPrev;

				/* Close the wad itself. */
				FreeWad(lpow->lpwad);

				/* Free wad options. */
				ConfigDestroy(lpow->lpcfgWad);

				/* Remove the wad from the list if it's there. */
				RemoveFromWadList(iID);

				/* Free the memory used by our structure. */
				ProcHeapFree(lpow);
			}

			/* Done! */
			return 0;
		}
	}

	/* Not found. */
	return -1;
}


/* EnumChildWindowsByWad
 *   Calls a function for each child window belonging to a specified wad.
 *
 * Parameters:
 *   int			iID			ID of wad.
 *   WNDENUMPROC	lpEnumFunc	Function to call.
 *   LPARAM			lParam		Extra parameter to pass to enum function.
 *
 * Return value:
 *   None
 */
void EnumChildWindowsByWad(int iID, WNDENUMPROC lpEnumFunc, LPARAM lParam)
{
	WADWINENUM wwe;

	wwe.lParam = lParam;
	wwe.lpEnumProc = lpEnumFunc;
	wwe.iID = iID;

	EnumChildWindows(g_hwndClient, WinWadCallbackChain, (LPARAM)&wwe);
}

/* WinWadCallbackChain
 *   Does EnumChildWindowsByWad's dirty work for it.
 *
 * Parameters:
 *   HWND	hwnd	Window to check.
 *   LPARAM	lParam	Pointer to data to use for real enum function.
 *
 * Return value:
 *   TRUE if enumeration should continue; FALSE otherwise.
 */
static BOOL CALLBACK WinWadCallbackChain(HWND hwnd, LPARAM lParam)
{
	WADWINENUM *lpwwe = (WADWINENUM*)lParam;

	/* Belongs to us? */
	if(MapWinBelongsToWad(hwnd, lpwwe->iID))
	{
		/* If so, call the real enum procedure. */
		return lpwwe->lpEnumProc(hwnd, lpwwe->lParam);
	}
	else return TRUE;
}


/* WinWadCallbackChain
 *   Does EnumChildWindowsByWad's dirty work for it.
 *
 * Parameters:
 *   HWND	hwnd	Window to check.
 *   LPARAM	lParam	Pointer to data to use for real enum function.
 *
 * Return value:
 *   TRUE if enumeration should continue; FALSE otherwise.
 */
int CountWadWindows(int iWadID)
{
	int i = 0;
	EnumChildWindowsByWad(iWadID, IncrementInteger, (LPARAM)&i);
	return i;
}



/* WadIsNewByID
 *   Determines whether a wad is new (i.e. created at runtime, not loaded from
 *   disc).
 *
 * Parameters:
 *   WAD*	lpwad		Pointer to WAD structure.
 *
 * Return value: BOOL
 *   TRUE if new; FALSE if not.
 */
BOOL WadIsNewByID(int iWad)
{
	return WadIsNew(GetOWByID(iWad));
}

static __inline BOOL WadIsNew(OPENWAD *lpow)
{
	return !lpow || !lpow->szFilename;
}


/* WadRefersToFile
 *   Determines whether a wad was opened from a particular file.
 *
 * Parameters:
 *   WAD*		lpow		OPENWAD structure.
 *   LPCTSTR	szFileName	Filename.
 *
 * Return value: BOOL
 *   TRUE if wad is from the file; FALSE if not.
 */
static __inline BOOL WadRefersToFile(OPENWAD *lpow, LPCTSTR szFileName)
{
	return PathsReferToSameFile(lpow->szFilename, szFileName);
}


/* BuildNodeBuilderCommandLine
 *   Builds a command-line string for testing a map using SRB2.
 *
 * Parameters:
 *   LPCTSTR	szFormat	Format string. %F is replaced by the filename and %L
 *							by the lumpname.
 *   LPCTSTR	szBinary	Nodebuilder binary.
 *   LPCTSTR	szFilename	Wad filename.
 *   LPCTSTR	szLumpname	Lumpname.
 *   LPTSTR		szBuffer	Buffer into which to return the string.
 *   int		cchBuffer	Length of buffer, including room for terminator.
 *
 * Return value: int
 *   Number of characters written, including the terminator, if successful, or
 *   zero if the buffer is not large enough.
 *
 * Remarks:
 *   Intended to be used on the temporary file created while saving a wad.
 */
static int BuildNodeBuilderCommandLine(LPCTSTR szFormat, LPCTSTR szBinary, LPCTSTR szFilename, LPCTSTR szLumpname, LPTSTR szBuffer, int cchBuffer)
{
	int cchBufferInit = cchBuffer;
	int cchBinary = _tcslen(szBinary);

	/* Always output the binary name and a space first. */
	cchBuffer -= cchBinary + 1;
	if(cchBuffer <= 0) return 0;
	_tcscpy(szBuffer, szBinary);
	szBuffer[cchBinary] = TEXT(' ');
	szBuffer += cchBinary + 1;

	/* Work our way through the format string, filling out the format
	 * specifiers.
	 */
	while(*szFormat)
	{
		int iCharsToCopy = 0;
		LPCTSTR szCopyFrom;

		/* Determine whether we're dealing with a format field. */
		if(*szFormat == TEXT('%'))
		{
			/* The next character is the important one. */
			switch(szFormat[1])
			{
			case TEXT('F'):
				/* Filename. */
				szFormat++;
				szCopyFrom = szFilename;
				iCharsToCopy = _tcslen(szFilename);
				break;

			case TEXT('L'):
				/* Lumpname. */
				szFormat++;
				szCopyFrom = szLumpname;
				iCharsToCopy = _tcslen(szLumpname);
				break;

			case TEXT('%'):
				/* Really want a %. Start from the *second* %. */
				szFormat++;
				szCopyFrom = szFormat;
				iCharsToCopy = 1;
				break;

			default:
				/* Malformed field. Assume we want a real % but forgot to say
				 * %%. Don't advance the format string.
				 */
				szCopyFrom = szFormat;
				iCharsToCopy = 1;
				break;
			}
		}
		else
		{
			/* Normal character. */
			szCopyFrom = szFormat;
			iCharsToCopy = 1;
		}

		/* Have we run out of space? */
		cchBuffer -= iCharsToCopy;
		if(cchBuffer <= 0)
			return 0;

		/* Copy the requested character(s). */
		for(; iCharsToCopy > 0; iCharsToCopy--)
			*szBuffer++ = *szCopyFrom++;

		/* Advance the format string by one character. If this was a format
		 * field, we've already skipped over the %.
		 */
		szFormat++;
	}

	/* Terminate the string. */
	if(cchBuffer > 0)
	{
		*szBuffer = TEXT('\0');
		cchBuffer--;
	}
	else return 0;

	/* Return number of characters written, including the terminator. */
	return cchBufferInit - cchBuffer;
}


/* BuildNodesExternal
 *   Invokes the external nodebuilder on a map.
 *
 * Parameters:
 *   LPCTSTR	szFilename	Wad filename.
 *   LPCTSTR	szLumpname	Lumpname.
 *
 * Return value: int
 *   Negative if the nodebuilder could not be invoked; zero if the exit code of
 *   the nodebuilder process is zero; strictly positive otherwise.
 *
 * Remarks:
 *   Intended to be used on the temporary file created while saving a wad.
 */
int BuildNodesExternal(LPCTSTR szFilename, LPCTSTR szLumpname)
{
	int cchBuffer = 1024;
	LPTSTR szArguments, szFormat, szBinary, szBinaryPath, szBinaryFilenamePart;
	int cchFormat, cchBinary, cchBinaryFull, iCPRet;
	STARTUPINFO si;
	PROCESS_INFORMATION pi;

	/* Get the nodebuilder options. */
	CONFIG *lpcfgNB = ConfigGetSubsection(g_lpcfgMain, OPT_NODEBUILDER);
	if(!lpcfgNB) return -1;

	/* Get the binary name and the format string for the args. */
	cchBinary = ConfigGetStringLength(lpcfgNB, NBCFG_BINARY) + 1;
	if(cchBinary == 1)
		return -2;
	szBinary = ProcHeapAlloc(cchBinary * sizeof(TCHAR));
	ConfigGetString(lpcfgNB, NBCFG_BINARY, szBinary, cchBinary);

	cchFormat = ConfigGetStringLength(lpcfgNB, NBCFG_ARGS) + 1;
	if(cchFormat == 1)
		return -3;
	szFormat = ProcHeapAlloc(cchFormat * sizeof(TCHAR));
	ConfigGetString(lpcfgNB, NBCFG_ARGS, szFormat, cchFormat);

	/* Generate the command-line arguments, making the buffer bigger and bigger
	 * until it's big enough.
	 */
	while(szArguments = ProcHeapAlloc(cchBuffer * sizeof(TCHAR)),
		BuildNodeBuilderCommandLine(szFormat, szBinary, szFilename, szLumpname, szArguments, cchBuffer) == 0)
	{
		cchBuffer <<= 1;
		ProcHeapFree(szArguments);
	}

	/* Finished with the format string now. */
	ProcHeapFree(szFormat);

	/* Get the working directory, by getting the full path to the binary and
	 * terminating the string where the filename part begins.
	 */
	cchBinaryFull = GetFullPathName(szBinary, 0, TEXT(""), NULL);
	szBinaryPath = ProcHeapAlloc((cchBinaryFull + 1) * sizeof(TCHAR));
	GetFullPathName(szBinary, cchBinaryFull, szBinaryPath, &szBinaryFilenamePart);
	*szBinaryFilenamePart = TEXT('\0');

	/* Get ready to create the process. */
	si.cb = sizeof(STARTUPINFO);
	si.cbReserved2 = 0;
	si.lpReserved = NULL;
	si.lpReserved2 = NULL;
	si.dwFlags = 0;
	si.lpDesktop = NULL;
	si.lpTitle = NULL;

	/* Go! */
	iCPRet = CreateProcess(szBinary, szArguments, NULL, NULL, FALSE, 0, NULL, szBinaryPath, &si, &pi);

	ProcHeapFree(szArguments);
	ProcHeapFree(szBinary);
	ProcHeapFree(szBinaryPath);

	/* Did we fail to create the process? */
	if(!iCPRet) return -4;

	/* Don't need the handle to the thread. */
	CloseHandle(pi.hThread);

	/* Wait for the process to finish. */
	WaitForSingleObject(pi.hProcess, INFINITE);
	CloseHandle(pi.hProcess);

	return 0;
}


/* BuildNodesExternalFromConfig
 *   Runs the external nodebuilder on a map specified in a config structure.
 *
 * Parameters:
 *   CONFIG*	lpcfgType		A CET_NULL node whose name is a lumpname.
 *   void*		lpvFilename		(LPCTSTR) Filename of wad.
 *
 * Return value: BOOL
 *   TRUE if iteration should continue; FALSE otherwise.
 *
 * Remarks:
 *   Used as a callback by ConfigIterate from WriteWad.
 */
BOOL BuildNodesExternalFromConfig(CONFIG *lpcfgNode, void *lpvFilename)
{
	int iRet;

	/* Attempt to build the nodes. */
	iRet = BuildNodesExternal((LPCTSTR)lpvFilename, lpcfgNode->szName);

	if(iRet < 0)
	{
		/* Couldn't invoke the nodebuilder. */
		MessageBoxFromStringTable(g_hwndMain, IDS_ERROR_INVOKENB, MB_ICONERROR);

		/* Don't bother trying any more maps. */
		return FALSE;
	}
	else return TRUE;
}


/* SetWadNonMapModified
 *   Sets an open wad's non-map-data-modified flag.
 *
 * Parameters:
 *   int	iWad	ID of wad.
 *
 * Return value: int
 *   Zero on success; nonzero on error.
 */
int SetWadNonMapModified(int iWad)
{
	OPENWAD *lpow = GetOWByID(iWad);

	if(!lpow) return 1;

	lpow->bNonMapDataChanged = TRUE;
	return 0;
}


/* DeleteMapFromOpenWad
 *   Deletes a map from an open wad, closing open windows first if necessary.
 *
 * Parameters:
 *   int	iWad		ID of wad.
 *   LPSTR	szLumpname	Name of map to delete.
 *
 * Return value: None.
 *
 * Remarks:
 *   If the map doesn't exist, nothing happens. If the map is open and changed,
 *   the user is NOT given the chance to save.
 */
void DeleteMapFromOpenWad(int iWad, LPCSTR szLumpname)
{
	HWND hwndMap = FindMapWindowByLumpname(iWad, szLumpname);
	WAD *lpwad = GetWad(iWad);

	if(hwndMap) ForceMapWindowClose(hwndMap);

	/* Delete the map, and if it actually exists, mark the wad as changed. */
	if(DeleteMap(lpwad, szLumpname, GetDefaultMapConfigForWad(iWad)) == 0)
		SetWadNonMapModified(iWad);
}


/* GetWadOptions
 *   Retrieves the existing wad options structure for an open wad.
 *
 * Parameters:
 *   int	iWad		ID of wad.
 *
 * Return value: CONFIG*
 *   Wad options, or NULL on error.
 */
CONFIG* GetWadOptions(int iWad)
{
	OPENWAD *lpow = GetOWByID(iWad);

	if(lpow) return lpow->lpcfgWad;
	return NULL;
}


/* CloseWad
 *   Attempts to close all open maps for a wad, and removes the wad from the wad
 *   list if successful.
 *
 * Parameters:
 *   int	iWad		ID of wad.
 *   void*	lpv			Ignored.
 *
 * Return value: BOOL
 *   TRUE if all maps closed; FALSE otherwise.
 *
 * Remarks:
 *   This closes the wad from the *user's* point of view. If there are
 *   internal references to the wad (e.g. for IWADs), these are preserved.
 */
BOOL CloseWad(int iWad, void *lpv)
{
	UNREFERENCED_PARAMETER(lpv);

	/* Prompt to close all the windows. */
	EnumChildWindowsByWad(iWad, CloseEnumProc, 0);

	/* Did we close all maps? */
	if(CountWadWindows(iWad) == 0)
	{
		/* If the wad is persistent, we need to free the extra reference. */
		if(ConfigGetInteger(g_lpcfgMain, OPT_PERSISTENTOPENWAD))
			ReleaseWad(iWad);

		/* Remove the wad from the list if it's there. If all references have
		 * been freed, it'll already have gone, but this isn't a problem.
		 */
		RemoveFromWadList(iWad);

		/* All maps closed. */
		return TRUE;
	}

	/* Not all maps closed. */
	return FALSE;
}


/* GetWadTitle
 *   Retrieves the display-title for a wad.
 *
 * Parameters:
 *   int		iWad		ID of wad.
 *   LPTSTR		szBuffer	Buffer in which title is to be stored.
 *   WORD		cchBuffer	Size of buffer.
 *
 * Return value: int
 *   If cchBuffer is nonzero, the return value is the number of characters
 *   written to the buffer; if cchBuffer is zero, it is the size of a buffer in
 *   characters at least big enough to hold the title. Both include the
 *   terminator.
 *
 * Remarks:
 *   If the buffer is too small, the title is copied as far as possible.
 */
int GetWadTitle(int iWad, LPTSTR szBuffer, WORD cchBuffer)
{
	int cchFilename;
	WORD cchTitle;

	/* Get file title. */
	cchFilename = GetWadFilename(iWad, NULL, 0);
	if(cchFilename > 0)
	{
		/* We have a filename, so get it. */
		LPTSTR szFilename = ProcHeapAlloc(cchFilename * sizeof(TCHAR));
		GetWadFilename(iWad, szFilename, cchFilename);

		/* Check whether we actually want the title or are just querying the
		 * length.
		 */
		if(cchBuffer > 0)
		{
			cchTitle = GetFileTitle(szFilename, szBuffer, cchBuffer);
		}
		else cchTitle = GetFileTitle(szFilename, NULL, 0);

		ProcHeapFree(szFilename);
	}
	else
	{
		/* No filename. Get "Untitled" string. */

		/* Check whether we actually want the title or are just querying the
		 * length.
		 */
		if(cchBuffer > 0)
		{
			LoadString(g_hInstance, IDS_UNTITLED, szBuffer, UNTITLED_BUFLEN);
			cchTitle = _tcslen(szBuffer);
		}
		else cchTitle = UNTITLED_BUFLEN;
	}

	return cchTitle;
}


/* EnumOpenWads
 *   Enumerates all open wads.
 *
 * Parameters:
 *   BOOL (*) (int, void*)		lpfn		Callback function.
 *   void*						lpvParam	Parameter for callback.
 *
 * Return value: BOOL
 *   FALSE if the callback instructed premature termination of enumeration; TRUE
 *   otherwise.
 *
 * Remarks:
 *   The callback should return TRUE to continue enumeration, or FALSE to end it
 *   immediately.
 */
BOOL EnumOpenWads(BOOL (*lpfn)(int iWad, void* lpvParam), void *lpvParam)
{
	OPENWAD *lpowRover = &g_owOpenWadsHdr;

	/* We need a separate pointer here in case the callback interferes with the
	 * the list of open wads.
	 */
	OPENWAD *lpowNext = lpowRover->lpowNext;

	while((lpowRover = lpowNext))
	{
		lpowNext = lpowRover->lpowNext;

		if(!lpfn(lpowRover->iID, lpvParam))
			return FALSE;
	}

	return TRUE;
}


/* AddAllUserWadsRef, ReleaseAllUserWads, AddUserWadRef, ReleaseUserWad
 *   Routines for incrementing or decrementing reference counts of all open
 *   "user wads", i.e. those in the wad list.
 *
 * Parameters for the latter two:
 *   int	iWad	Wad ID.
 *   void*	lpv		Ignored.
 *
 * Return value for the latter two: BOOL
 *   Always TRUE.
 *
 * Remarks:
 *   Intended to be used when OPT_PERSISTENTOPENWAD changes.
 */
void AddAllUserWadsRef(void) { EnumOpenWads(AddUserWadRef, NULL); }
void ReleaseAllUserWads(void) { EnumOpenWads(ReleaseUserWad, NULL); }

static BOOL AddUserWadRef(int iWad, void *lpv)
{
	UNREFERENCED_PARAMETER(lpv);
	if(WadIsInList(iWad)) IncrementWadReferenceCount(iWad);
	return TRUE;
}

static BOOL ReleaseUserWad(int iWad, void *lpv)
{
	UNREFERENCED_PARAMETER(lpv);
	if(WadIsInList(iWad)) ReleaseWad(iWad);
	return TRUE;
}
