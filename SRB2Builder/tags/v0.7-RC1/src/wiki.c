/* SRB2 Workbench -- A map editor for Sonic Robo Blast 2.
 * Copyright (C) 2009 Gregor Dick
 * http://workbench.srb2.org/
 *
 * Incorporates portions of Doom Builder, (C) 2003 Pascal vd Heiden.
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 * USA.
 *
 * wiki.c: Routines for building Wiki queries and invoking a web browser to
 * submit them.
 *
 * AMDG.
 */

#include <windows.h>
#include <tchar.h>
#include <stdio.h>

#include "general.h"
#include "config.h"
#include "options.h"
#include "wiki.h"
#include "mapconfig.h"

#include "win/mdiframe.h"

#include "../res/resource.h"


static void WikiQuery(LPCTSTR szQuery);


/* WikiQuery
 *   Opens the Wiki at the URL specified in the config file, replacing the
 *   format field in the format string with the supplied query.
 *
 * Parameters:
 *   LPCTSTR	szQuery		Query string.
 *
 * Return value: None.
 */
static void WikiQuery(LPCTSTR szQuery)
{
	CONFIG *lpcfgWiki = ConfigGetSubsection(g_lpcfgMain, OPT_WIKI);

	UINT cchQueryFmt = ConfigGetStringLength(lpcfgWiki, WIKICFG_QUERYURLFMT);
	LPTSTR szQueryFmt = ProcHeapAlloc((cchQueryFmt + 1) * sizeof(TCHAR));

	/* Make a string big enough for the expanded URL. */
	UINT cchURL = cchQueryFmt + _tcslen(szQuery) + 1;
	LPTSTR szURL = ProcHeapAlloc(cchURL * sizeof(TCHAR));

	/* Get the format string. */
	ConfigGetString(lpcfgWiki, WIKICFG_QUERYURLFMT, szQueryFmt, cchQueryFmt + 1);

	/* Expand the query placeholder. */
	_sntprintf(szURL, cchURL, szQueryFmt, szQuery);

	/* Attempt to open the URL. */
	if(!(*szURL) || (int)ShellExecute(NULL, NULL, szURL, NULL, NULL, SW_SHOW) <= 32)
		MessageBoxFromStringTable(g_hwndMain, IDS_ERROR_WIKI, MB_ICONERROR);

	/* Free buffers. */
	ProcHeapFree(szQueryFmt);
	ProcHeapFree(szURL);
}


/* WikiMainPage
 *   Opens the Wiki's main page.
 *
 * Parameters:
 *   None.
 *
 * Return value: None.
 */
void WikiMainPage(void)
{
	CONFIG *lpcfgWiki = ConfigGetSubsection(g_lpcfgMain, OPT_WIKI);
	UINT cchBuffer = ConfigGetStringLength(lpcfgWiki, WIKICFG_MAINPAGE) + 1;
	LPTSTR szMainPage = ProcHeapAlloc(cchBuffer * sizeof(TCHAR));
	ConfigGetString(lpcfgWiki, WIKICFG_MAINPAGE, szMainPage, cchBuffer);
	WikiQuery(szMainPage);
	ProcHeapFree(szMainPage);
}


/* WikiEffectQuery
 *   Searches the Wiki for a Linedef, Sector or Thing type.
 *
 * Parameters:
 *   CONFIG*			lpcfgMap		Map config.
 *   ENUM_WIKIQUERIES	wikiquery		Query type: ld, sec, thing.
 *   int				iIndex			Effect value.
 *
 * Return value: None.
 */
void WikiEffectQuery(CONFIG *lpcfgMap, ENUM_WIKIQUERIES wikiquery, int iIndex)
{
	CONFIG *lpcfgWiki = ConfigGetSubsection(lpcfgMap, MAPCFG_WIKI);

	if(lpcfgWiki)
	{
		LPCTSTR szFormatName = NULL;
		DWORD cchFormat, cchQuery;
		LPTSTR szFormat;
		LPTSTR szQuery;

		/* What sort of thing are we looking up? */
		switch(wikiquery)
		{
			case WIKIQUERY_LDEFFECT:	szFormatName = WIKI_LDEFFECT;	break;
			case WIKIQUERY_SECEFFECT:	szFormatName = WIKI_SECEFFECT;	break;
			case WIKIQUERY_THING:		szFormatName = WIKI_THING;		break;
		}

		/* Get the appropriate format string. */
		cchFormat = ConfigGetStringLength(lpcfgWiki, szFormatName) + 1;
		if(cchFormat == 0) return;
		szFormat = ProcHeapAlloc(cchFormat * sizeof(TCHAR));
		ConfigGetString(lpcfgWiki, szFormatName, szFormat, cchFormat);

		/* Fill out the format field with the index of the item we're interested
		 * in.
		 */
		cchQuery = cchFormat + 11;
		szQuery = ProcHeapAlloc(cchQuery * sizeof(TCHAR));
		_sntprintf(szQuery, cchQuery, szFormat, iIndex);

		WikiQuery(szQuery);

		/* Done! Tidy up. */
		ProcHeapFree(szFormat);
		ProcHeapFree(szQuery);
	}
}


/* UntaintWikiOptions
 *   Makes sure the Wiki settings aren't dangerous.
 *
 * Parameters:
 *   None.
 *
 * Return value: None.
 *
 * Remarks:
 *   Call this when loading the options initially, and after the user has
 *   changed them.
 */
void UntaintWikiOptions(void)
{
	CONFIG *lpcfgWiki = ConfigGetSubsection(g_lpcfgMain, OPT_WIKI);

	/* Allocate twice as many characters as we need, which is (more than) enough
	 * to replace every possible errant %.
	 */
	UINT uiInitialLength = ConfigGetStringLength(lpcfgWiki, WIKICFG_QUERYURLFMT);
	UINT cchBuffer = (uiInitialLength << 1) + 1;
	LPTSTR szWikiURLFormat = ProcHeapAlloc(cchBuffer * sizeof(TCHAR));
	LPTSTR szWikiURLFmtFinal = szWikiURLFormat;
	LPTSTR szInitialFormat = szWikiURLFormat + uiInitialLength;

	/* We're only allowed one %s. */
	BOOL bFoundFormatField = FALSE;

	/* Put the initial string at the *end* of the buffer. */
	ConfigGetString(lpcfgWiki, WIKICFG_QUERYURLFMT, szInitialFormat, uiInitialLength + 1);

	/* Make sure that it begins http:// */
	if(_tcsncmp(szInitialFormat, TEXT("http://"), 7) == 0)
	{
		/* Copy the string to the beginning of the buffer, replacing anything
		 * that's unsafe. The lengths of the strings are such that we never
		 * overwrite a character in the original string until we're finished
		 * with it.
		 *
		 * A quick word on the syntax of the format string: Only %s means
		 * anything special, and that's allowed only once. We don't even do %%:
		 * that sequence is translated to *two* % signs in the output; it's not
		 * an escape sequence.
		 */
		while(*szInitialFormat) switch(*szInitialFormat)
		{
		case TEXT('%'):
			/* We found a percentage sign. It's the next character that determines
			 * what we have to do.
			 */

			if(szInitialFormat[1] != TEXT('s') || bFoundFormatField)
				/* We actually want a % here. */
				*szWikiURLFormat++ = TEXT('%');
			/* Otherwise, it's our format field, but don't allow it next time. */
			else bFoundFormatField = TRUE;

			/* Fall through. */
		default:
			/* Just copy the character. */
			*szWikiURLFormat++ = *szInitialFormat++;
		}
	}

	/* Terminate the string. */
	*szWikiURLFormat = TEXT('\0');

	/* Write it back to the config. */
	ConfigSetString(lpcfgWiki, WIKICFG_QUERYURLFMT, szWikiURLFmtFinal);

	/* Free the buffer. */
	ProcHeapFree(szWikiURLFmtFinal);
}
