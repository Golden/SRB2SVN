/* SRB2 Workbench -- A map editor for Sonic Robo Blast 2.
 * Copyright (C) 2009 Gregor Dick
 * http://workbench.srb2.org/
 *
 * Incorporates portions of Doom Builder, (C) 2003 Pascal vd Heiden.
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 * USA.
 *
 * errorcheck.c: Routines to check a map's integrity.
 *
 * AMDG.
 */

#include <windows.h>

#include "general.h"
#include "map.h"
#include "errorcheck.h"
#include "editing.h"

#define INIT_UNCLOSED_SECTOR_DESCRIPTORS 8


/* FindNextCoincidentVertex
 *   Finds a vertex coincident to a given vertex and having a higher index, if
 *   such exists.
 *
 * Parameters:
 *   MAP*	lpmap		Map.
 *   int	iVertex		Index of vertex.
 *
 * Return value: int
 *   Index of coincident vertex, or negative if none exists.
 */
int FindNextCoincidentVertex(MAP *lpmap, int iVertex)
{
	int i;

	for(i = iVertex + 1; i < lpmap->iVertices; i++)
		if(lpmap->vertices[i].x == lpmap->vertices[iVertex].x &&
			lpmap->vertices[i].y == lpmap->vertices[iVertex].y)
			return i;

	return -1;
}


/* UsedVertexMatrix, UsedSidedefMatrix, UsedSectorMatrix
 *   Generates an array indicating which vertices/sidedefs/sectors in a map are
 *   used.
 *
 * Parameters:
 *   MAP*	lpmap	Map.
 *
 * Return value: BOOL*
 *   Array with entry i TRUE iff vertex/sidedef/sector i is used.
 *
 * Remarks:
 *   Call FreeUsedMapItemMatrix to free the returned pointer.
 */
BOOL* UsedVertexMatrix(MAP *lpmap)
{
	/* Allocate storage for determining which vertices are used. */
	BOOL *lpbUsedVertices = ProcHeapAlloc(lpmap->iVertices * sizeof(BOOL));
	int i;

	ZeroMemory(lpbUsedVertices, lpmap->iVertices * sizeof(BOOL));

	/* Loop through all linedefs, marking each vertex as used. */
	for(i = 0; i < lpmap->iLinedefs; i++)
	{
		lpbUsedVertices[lpmap->linedefs[i].v1] = TRUE;
		lpbUsedVertices[lpmap->linedefs[i].v2] = TRUE;
	}

	return lpbUsedVertices;
}

BOOL* UsedSidedefMatrix(MAP *lpmap)
{
	/* Allocate storage for determining which sidedefs are used. */
	BOOL *lpbUsedSidedefs = ProcHeapAlloc(lpmap->iSidedefs * sizeof(BOOL));
	int i;

	ZeroMemory(lpbUsedSidedefs, lpmap->iSidedefs * sizeof(BOOL));

	/* Loop through all linedefs, marking each sidedef as used. */
	for(i = 0; i < lpmap->iLinedefs; i++)
	{
		if(SidedefExists(lpmap, lpmap->linedefs[i].s1))
			lpbUsedSidedefs[lpmap->linedefs[i].s1] = TRUE;

		if(SidedefExists(lpmap, lpmap->linedefs[i].s2))
			lpbUsedSidedefs[lpmap->linedefs[i].s2] = TRUE;
	}

	return lpbUsedSidedefs;
}

BOOL* UsedSectorMatrix(MAP *lpmap)
{
	/* Allocate storage for determining which sectors are used. */
	BOOL *lpbUsedSectors = ProcHeapAlloc(lpmap->iSectors * sizeof(BOOL));
	int i;

	ZeroMemory(lpbUsedSectors, lpmap->iSectors * sizeof(BOOL));

	/* Loop through all sidedefs, marking their sectors as used. */
	for(i = 0; i < lpmap->iSidedefs; i++)
		if(lpmap->sidedefs[i].sector >= 0)
			lpbUsedSectors[lpmap->sidedefs[i].sector] = TRUE;

	return lpbUsedSectors;
}


/* LinedefSidedefMismatches
 *   Returns flags indicating whether a linedef is missing any sidedefs or has
 *   any it shouldn't.
 *
 * Parameters:
 *   MAP*	lpmap		Map.
 *   int	iLinedef	Index of linedef.
 *
 * Return value: BYTE
 *   Flags corresponding to enum ENUM_SIDEDEF_ERROR_FLAGS indicating sidedef
 *   mismatches.
 */
BYTE LinedefSidedefMismatches(MAP *lpmap, int iLinedef)
{
	MAPLINEDEF *lpld = &lpmap->linedefs[iLinedef];
	BYTE byFlags = 0;

	/* All linedefs should have a front sidedef. */
	if(!SidedefExists(lpmap, lpld->s1)) byFlags |= SEF_FRONT_MISSING;

	/* Linedefs should have back sidedefs iff they're double-sided. */
	if(lpld->flags & LDF_DOUBLESIDED)
	{
		if(!SidedefExists(lpmap, lpld->s2)) byFlags |= SEF_BACK_MISSING;
	}
	else if(SidedefExists(lpmap, lpld->s2)) byFlags |= SEF_BACK_SUPERFLUOUS;

	return byFlags;
}


/* MissingTexFlags
 *   Returns flags indicating whether a linedef is missing any textures.
 *
 * Parameters:
 *   MAP*		lpmap		Map.
 *   int		iLinedef	Index of linedef.
 *   LPCTSTR	szSky		Sky texture name.
 *
 * Return value: BYTE
 *   Flags corresponding to enum ENUM_LDTEX_FLAGS indicating missing textures.
 */
BYTE MissingTexFlags(MAP *lpmap, int iLinedef, LPCTSTR szSky)
{
	MAPLINEDEF *lpld = &lpmap->linedefs[iLinedef];

	/* Determine which textures are required. */
	BYTE byReqFlags = RequiredTextures(lpmap, iLinedef, szSky);

	/* Mask out those we already have. */

	if(SidedefExists(lpmap, lpld->s1))
	{
		MAPSIDEDEF *lpsd = &lpmap->sidedefs[lpld->s1];

		if((byReqFlags & LDTF_FRONTUPPER) && IsBonaFideTextureA(lpsd->upper))
			byReqFlags &= ~LDTF_FRONTUPPER;

		if((byReqFlags & LDTF_FRONTMIDDLE) && IsBonaFideTextureA(lpsd->middle))
			byReqFlags &= ~LDTF_FRONTMIDDLE;

		if((byReqFlags & LDTF_FRONTLOWER) && IsBonaFideTextureA(lpsd->lower))
			byReqFlags &= ~LDTF_FRONTLOWER;
	}

	if(SidedefExists(lpmap, lpld->s2))
	{
		MAPSIDEDEF *lpsd = &lpmap->sidedefs[lpld->s2];

		if((byReqFlags & LDTF_BACKUPPER) && IsBonaFideTextureA(lpsd->upper))
			byReqFlags &= ~LDTF_BACKUPPER;

		if((byReqFlags & LDTF_BACKMIDDLE) && IsBonaFideTextureA(lpsd->middle))
			byReqFlags &= ~LDTF_BACKMIDDLE;

		if((byReqFlags & LDTF_BACKLOWER) && IsBonaFideTextureA(lpsd->lower))
			byReqFlags &= ~LDTF_BACKLOWER;
	}

	return byReqFlags;
}


/* FindCrossingLinedef
 *   Finds the first linedef crossing a given linedef from an arbitrary starting
 *   point.
 *
 * Parameters:
 *   MAP*		lpmap			Map.
 *   int		iLinedef		Index of linedef which we are interested in
 *								finding lines that cross.
 *   int		iLinedefBegin	Start checking lines from here.
 *
 * Return value: int
 *   Index of crossing linedef, or negative if none found.
 */
int FindCrossingLinedef(MAP *lpmap, int iLinedef, int iLinedefBegin)
{
	int iVertex1 = lpmap->linedefs[iLinedef].v1;
	int iVertex2 = lpmap->linedefs[iLinedef].v2;
	int i;

	float fGradientSource = 0;
	float fNumAdd = 0;
	float x, y;

	/* Calculate gradient etc. of the top linedef. */
	float x1 = lpmap->vertices[iVertex1].x;
	float y1 = lpmap->vertices[iVertex1].y;
	float x2 = lpmap->vertices[iVertex2].x;
	float y2 = lpmap->vertices[iVertex2].y;

	if(x1 != x2)
	{
		fGradientSource = (y2 - y1) / (x2 - x1);
		fNumAdd = x1 * fGradientSource - y1;
	}

	/* Check each linedef that we've been asked to, until we find one that
	 * crossses us.
	 */
	for(i = iLinedefBegin; i < lpmap->iLinedefs; i++)
	{
		/* Calculate gradient of this linedef. */
		float X1 = lpmap->vertices[lpmap->linedefs[i].v1].x;
		float Y1 = lpmap->vertices[lpmap->linedefs[i].v1].y;
		float X2 = lpmap->vertices[lpmap->linedefs[i].v2].x;
		float Y2 = lpmap->vertices[lpmap->linedefs[i].v2].y;
		float fGradientRover = 0;

		if(X2 != X1) fGradientRover = (Y2 - Y1) / (X2 - X1);

		if(fGradientRover != fGradientSource || x1 == x2 || X1 == X2)
		{
			if(X1 != X2 && x1 != x2)
			{
				x = (fNumAdd + Y1 - X1 * fGradientRover) / (fGradientSource - fGradientRover);
				y = fGradientSource * x - fNumAdd;
			}
			/* Both vertical? */
			else if(x1 == x2 && X1 == X2)
			{
				if(x1 != X1) continue;		/* Parallel! */
				else						/* Coincident, but not necessarily congruent. */
				{
					/* Handle this seperately. It's easier. */
					if((y1 < Y1 && y1 > Y2) || (y1 > Y1 && y1 < Y2)) return i;
					else continue;
				}
			}
			else if(x1 == x2)				/* X1 != X2 */
			{
				x = x1;
				y = fGradientRover * (x - X1) + Y1;
			}
			else							/* X1 == X2 && x1 != x2 */
			{
				x = X1;
				y = fGradientSource * x - fNumAdd;
			}
		}
		/* Parallel or co-incident, and neither vertical. */
		else
		{
			/* Parallel? */
			if(y1 - fGradientSource * x1 != Y1 - fGradientSource * X1) continue;
			else
			{
				/* We only need to test one co-ordinate. */
				if((y1 < Y1 && y1 > Y2) || (y1 > Y1 && y1 < Y2)) return i;
				else continue;
			}

		}


		/* Check if in range (and hence crossing). */
		if(InLDRectOpen(lpmap->linedefs, lpmap->vertices, iLinedef, x, y) &&
			InLDRectOpen(lpmap->linedefs, lpmap->vertices, i, x, y))
		{
			/* But not congruent... */
			if( !(((lpmap->linedefs[i].v1 == iVertex1) && (lpmap->linedefs[i].v2 == iVertex2)) ||
				  ((lpmap->linedefs[i].v1 == iVertex2) && (lpmap->linedefs[i].v2 == iVertex1))))
			{
				/* If this is not the same linedef as the source, then this is a
				 * crossed linedef.
				 */
				if(i != iLinedef) return i;
			}
		}
	}

	/* Return -1 (nothing found). */
	return -1;
}


/* FindUnclosedSectors
 *   Finds all pairs of adjacent linedefs with mismatched sector references.
 *
 * Parameters:
 *   MAP*							lpmap		Map.
 *   UNCLOSED_SECTOR_DESCRIPTOR**	lplpusd		Address into which to return a
 *												pointer to an array of
 *												descriptors.
 *
 * Return value: int
 *   Number of unclosed sectors.
 *
 * Remarks:
 *   Call FreeUnclosedSectorDescriptors on *lplpusd, even if we return zero.
 */
int FindUnclosedSectors(MAP *lpmap, UNCLOSED_SECTOR_DESCRIPTOR** lplpusd)
{
	int iNumFound = 0, iAllocated = INIT_UNCLOSED_SECTOR_DESCRIPTORS;
	int iVertex;
	DYNAMICINTARRAY *lpldlookup;

	/* Build a vertex-to-line lookup table so that we can consider adjoining
	 * linedefs.
	 */
	lpldlookup = BuildLDLookupTable(lpmap, LDLT_VXTOLD);

	/* Allocate initial buffer. */
	*lplpusd = ProcHeapAlloc(iAllocated * sizeof(UNCLOSED_SECTOR_DESCRIPTOR));

	/* Consider the linedefs at each vertex. */
	for(iVertex = 0; iVertex < lpmap->iVertices; iVertex++)
	{
		if(lpldlookup[iVertex].uiCount > 0)
		{
			/* Begin with the first linedef. */
			int iLinedef = lpldlookup[iVertex].lpiIndices[0];
			int iFirstLinedef = iLinedef;
			int iSide = LS_FRONT;

			/* Look round linedefs at this vertex until we get back to
			 * ourselves.
			 */
			do
			{
				int iWhichVertexSrc = lpmap->linedefs[iLinedef].v1 == iVertex ? 0 : 1;
				int iWhichVertexAdj;
				BOOL bSameSide;
				int iLinedefSrc = iLinedef;
				int iSidedefSrc = (iSide == LS_FRONT) ? lpmap->linedefs[iLinedef].s1 : lpmap->linedefs[iLinedef].s2;
				int iSidedefAdj;
				MAPSIDEDEF *lpsdSrc, *lpsdAdj;

				iLinedef = FindNearestAdjacentLinedef(lpmap, lpldlookup, iLinedef, iSide, iWhichVertexSrc, NULL, NULL, NULL, NULL);
				iWhichVertexAdj = lpmap->linedefs[iLinedef].v1 == iVertex ? 0 : 1;
				bSameSide = (iWhichVertexSrc != iWhichVertexAdj);

				/* Get index of the sidedef of interest on the adjacent line. */
				iSidedefAdj = (bSameSide ^ (iSide == LS_BACK)) ? lpmap->linedefs[iLinedef].s1 : lpmap->linedefs[iLinedef].s2;

				lpsdSrc = SidedefExists(lpmap, iSidedefSrc) ? &lpmap->sidedefs[iSidedefSrc] : NULL;
				lpsdAdj = SidedefExists(lpmap, iSidedefAdj) ? &lpmap->sidedefs[iSidedefAdj] : NULL;

				/* If precisely one of the sides doesn't exist, or if they both
				 * exist and have different sectors, then we're unclosed.
				 */
				if((lpsdSrc || lpsdAdj) && (!(lpsdSrc && lpsdAdj) || lpsdSrc->sector != lpsdAdj->sector))
				{
					/* Make sure there's enough room. */
					if(iNumFound >= iAllocated)
						*lplpusd = ProcHeapReAlloc(*lplpusd, (iAllocated <<= 1) * sizeof(UNCLOSED_SECTOR_DESCRIPTOR));

					(*lplpusd)[iNumFound].iLinedefSrc = iLinedefSrc;
					(*lplpusd)[iNumFound].iLinedefAdj = iLinedef;
					(*lplpusd)[iNumFound].iSideSrc = iSide;

					iNumFound++;
				}

				/* Look from the other side next time if we matched on the
				 * *same* side this time.
				 */
				if(bSameSide) iSide = (iSide == LS_FRONT) ? LS_BACK : LS_FRONT;
			} while(iLinedef != iFirstLinedef);
		}
	}

	DestroyLDLookupTable(lpmap, lpldlookup);

	return iNumFound;
}


/* FixCrossingLinedefs
 *   Fixes a pair of crossing linedefs, adjusting sector references as
 *   necessary.
 *
 * Parameters:
 *   MAP*	lpmap					Map.
 *   int	iLinedef1, iLinedef2	Indices of crossing linedefs.
 *
 * Return value: None.
 *
 * Remarks:
 *   The linedefs must indeed intersect: no validation is performed. The RS
 *   flags and LEF_LABELLED should be clear when called.
 */
void FixCrossingLinedefs(MAP *lpmap, int iLinedef1, int iLinedef2)
{
	short xIntersect, yIntersect;
	MAPLINEDEF *lpld1 = &lpmap->linedefs[iLinedef1];
	MAPLINEDEF *lpld2 = &lpmap->linedefs[iLinedef2];
	MAPLINEDEF *lpldNew1 = NULL, *lpldNew2 = NULL;
	int iNewLinedef1, iNewLinedef2;

	/* Find where they cross. */
	LineSegmentIntersection(
		lpmap->vertices[lpld1->v1].x,
		lpmap->vertices[lpld1->v1].y,
		lpmap->vertices[lpld1->v2].x,
		lpmap->vertices[lpld1->v2].y,
		lpmap->vertices[lpld2->v1].x,
		lpmap->vertices[lpld2->v1].y,
		lpmap->vertices[lpld2->v2].x,
		lpmap->vertices[lpld2->v2].y,
		&xIntersect,
		&yIntersect);

	/* Split the lines where they cross. */
	iNewLinedef1 = SplitLinedefAtPoint(lpmap, iLinedef1, xIntersect, yIntersect);
	iNewLinedef2 = SplitLinedefAtPoint(lpmap, iLinedef2, xIntersect, yIntersect);

	/* Trivial, weird, kind of wrong, but harmless. */
	if(iNewLinedef1 < 0 && iNewLinedef2 < 0)
		return;

	if(iNewLinedef1 >= 0) lpldNew1 = &lpmap->linedefs[iNewLinedef1];
	if(iNewLinedef2 >= 0) lpldNew2 = &lpmap->linedefs[iNewLinedef2];

	/* Mark these as needing fixed. */
	lpld1->editflags |= LEF_RECALCSECTOR;
	lpld2->editflags |= LEF_RECALCSECTOR;
	if(lpldNew1) lpldNew1->editflags |= LEF_RECALCSECTOR;
	if(lpldNew2) lpldNew2->editflags |= LEF_RECALCSECTOR;

	/* Fix the sector references. */
	CorrectRSSectorReferences(lpmap);

	/* Clean up. */
	lpld1->editflags &= ~LEF_RECALCSECTOR;
	lpld2->editflags &= ~LEF_RECALCSECTOR;
	if(lpldNew1) lpldNew1->editflags &= ~LEF_RECALCSECTOR;
	if(lpldNew2) lpldNew2->editflags &= ~LEF_RECALCSECTOR;
}


/* FixBrokenSectorFromLinedef
 *   Fixes broken sector references by propagating a linedef's sector over its
 *   neighbours.
 *
 * Parameters:
 *   MAP*	lpmap			Map.
 *   int	iLinedef		Linedef whose sector is to be propagated.
 *   int	iLinedefSide	Side of linedef.
 *
 * Return value: None.
 *
 * Remarks:
 *   The LEF_VISITED_* flags must be clear when calling.
 */
void FixBrokenSectorFromLinedef(MAP *lpmap, int iLinedef, int iLinedefSide)
{
	int iNewSector = -1;		/* We'll set this the first time around. */
	DYNAMICINTARRAY *lpldlookup = BuildLDLookupTable(lpmap, LDLT_VXTOLD);

	while(!(lpmap->linedefs[iLinedef].editflags & ((iLinedefSide == LS_FRONT) ? LEF_VISITED_FRONT : LEF_VISITED_BACK)))
	{
		MAPLINEDEF *lpld = &lpmap->linedefs[iLinedef];
		int iSidedef = (iLinedefSide == LS_FRONT ? lpld->s1 : lpld->s2);
		MAPSIDEDEF *lpsd = (SidedefExists(lpmap, iSidedef) ? &lpmap->sidedefs[iSidedef] : NULL);

		/* Mark the line as visited on the appropriate side. */
		lpld->editflags |= (iLinedefSide == LS_FRONT) ? LEF_VISITED_FRONT : LEF_VISITED_BACK;

		/* If we know which sector to set, do so. */
		if(iNewSector >= 0)
		{
			/* If we've been told to put a sector on the back of the linedef but we
			 * don't yet have a front sidedef, then we flip the linedef and put the
			 * sector on the front.
			 */
			if(iLinedefSide == LS_BACK && !SidedefExists(lpmap, lpld->s1))
			{
				FlipLinedef(lpmap, iLinedef);
				iLinedefSide = LS_FRONT;

				/* The sidedef vars are still correct. */
			}

			/* No sidedef yet? Then make one, and add it to the line. */
			if(!lpsd)
			{
				iSidedef = AddSidedef(lpmap, iNewSector);
				lpsd = &lpmap->sidedefs[iSidedef];
				SetLinedefSidedef(lpmap, iLinedef, iSidedef, iLinedefSide);
			}

			/* Set the sidedef to reference the new sector. */
			lpsd->sector = iNewSector;
		}
		/* If we don't know which sector to set, use this one and move on. */
		else if(lpsd)
		{
			iNewSector = lpsd->sector;
		}
		/* First line must have a sidedef on the appropriate side. */
		else break;

		/* Find the linedef adjacent to ourselves that makes the smallest angle with
		 * the appropriate side. If we're nearest to ourselves, the recursive call
		 * will terminate the algorithm.
		 */
		iLinedef = FindNearestAdjacentLinedef(lpmap, lpldlookup, iLinedef, iLinedefSide, (iLinedefSide == LS_FRONT) ? 1 : 0, NULL, NULL, NULL, NULL);

		/* If the orientation of the next line is opposite to our own, adjust
		 * things.
		 */
		if(lpld->v1 != lpmap->linedefs[iLinedef].v2 && lpld->v2 != lpmap->linedefs[iLinedef].v1)
		{
			/* Recurse along the other side. */
			iLinedefSide = (iLinedefSide == LS_FRONT) ? LS_BACK : LS_FRONT;
		}
	}

	ClearLineFlags(lpmap, LEF_VISITED);
	DestroyLDLookupTable(lpmap, lpldlookup);
}
