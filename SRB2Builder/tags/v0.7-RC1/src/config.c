/* SRB2 Workbench -- A map editor for Sonic Robo Blast 2.
 * Copyright (C) 2009 Gregor Dick
 * http://workbench.srb2.org/
 *
 * Incorporates portions of Doom Builder, (C) 2003 Pascal vd Heiden.
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 * USA.
 *
 * config.c: Implements hierarchical associative arrays using -- for the moment,
 * unbalanced -- binary trees. These are referred to as "configs" for historical
 * reasons. The format used for storing these in files is very similar to Doom
 * Builder's, although the code is original.
 *
 * AMDG.
 */


#include <windows.h>
#include <stdio.h>
#include <tchar.h>
#include <ctype.h>

#include "general.h"
#include "config.h"
#include "../res/resource.h"

#ifndef _TEOF
#ifdef _UNICODE
#define _TEOF WEOF
#else
#define _TEOF EOF
#endif
#endif


/* Types */
typedef struct _CWEDATA
{
	FILE	*lpfile;
	int		iTabs;
} CWEDATA;


/* Macros. */
#define NODEROOT			TEXT("node-root")
#define FLOAT_INITBUFLEN	16
#define UCS2_BOM			0xFEFF
#define CONFIG_NEWLINE		TEXT("\r\n")


/* Static prototypes. */
static CONFIG* CreateConfigNode(LPCTSTR szName);
static void DestroyConfigNode(CONFIG *lpcfg);
static CONFIG* ParseConfigSection(LPTSTR *lpszFile);
static BOOL ParseAndAddConfigStatement(CONFIG *lpcfgRoot, LPTSTR *lpszFile);
static BOOL ParseConfigStringValue(LPTSTR *lpsz);
static CONFIG_ENTRY_TYPE ParseConfigNumericValue(LPTSTR *lpsz, int *lpi, float *lpf);
static BOOL ReadPastString(LPCTSTR *lpsz, LPCTSTR szToken);
static BOOL ReadUntilAnyChar(LPCTSTR *lpsz, LPCTSTR szCharSet);
static BOOL ReadPastAnyChar(LPCTSTR *lpsz, LPCTSTR szCharSet);
static BOOL CharBelongsToSet(TCHAR tc, LPCTSTR szCharSet);
static BOOL EatCommentsAndWhitespace(LPCTSTR *lpsz);
static BOOL AddConfigNode(CONFIG *lpcfgRoot, CONFIG *lpcfgNew);
static void DestroyConfigNodeData(CONFIG *lpcfg);
static CONFIG* FindConfigNode(CONFIG *lpcfgRoot, LPCTSTR szName);
static BOOL ConfigWriteSubsection(FILE *lpfile, CONFIG *lpcfgSubsectionRoot, int iTabs);
static BOOL ConfigWriteEntry(CONFIG *lpcfgNode, void *lpvParam);


/* ConfigLoad
 *   Loads a Doom Builder-esque config file from disc.
 *
 * Parameters:
 *   LPCTSTR	szFilename		File to load.
 *
 * Return value:
 *   Pointer to new config structure, or NULL on error.
 *
 * Notes:
 *   The app should never manipulate the returned structure directly. It should
 *   only ever be passed to functions in this file.
 */
CONFIG* ConfigLoad(LPCTSTR szFilename)
{
	CONFIG *lpcfg;
	HANDLE hFile = CreateFile(szFilename, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING, FILE_FLAG_SEQUENTIAL_SCAN, NULL);
	LPVOID lpvRawFile;
	LPTSTR szFile, szFileRover;
	DWORD dwFileLength;
	DWORD dwBytesRead;

	/* File not found? */
	if(hFile == INVALID_HANDLE_VALUE) return NULL;

	/* Get file length. */
	dwFileLength = GetFileSize(hFile, NULL);
	if(!dwFileLength)		/* Zero-length file. */
	{
		CloseHandle(hFile);
		return NULL;
	}

	/* Load the whole lot into memory -- we only use any of the extra bytes if
	 * we're able to use the file exactly as it is on disc; see the Unicode
	 * conversion code below.
	 */
	lpvRawFile = ProcHeapAlloc(dwFileLength + 2);
	ReadFile(hFile, lpvRawFile, dwFileLength, &dwBytesRead, NULL);
	CloseHandle(hFile);

	if(dwBytesRead != dwFileLength)
	{
		/* Failed during read. */
		ProcHeapFree(lpvRawFile);
		return NULL;
	}

	/* Allocate buffer for string representing the file in the appropriate
	 * character set.
	 */

#ifdef UNICODE
	if(!IsTextUnicode(lpvRawFile, dwFileLength, NULL))
	{
		/* We're a Unicode app but we loaded an ANSI file, so convert it up. */
		szFile = ProcHeapAlloc((dwFileLength + 1) * sizeof(WCHAR));
		MultiByteToWideChar(CP_ACP, MB_PRECOMPOSED, lpvRawFile, dwFileLength, szFile, dwFileLength + 1);
		szFile[dwFileLength] = TEXT('\0');

		ProcHeapFree(lpvRawFile);
	}
	else
#endif
	{
		/* The file type matches the app type, so just use what we already have
		 * in the buffer.
		 */
		szFile = (LPTSTR)lpvRawFile;
		szFile[dwFileLength / sizeof(TCHAR)] = TEXT('\0');

		/* Don't free lpvRawFile, since we're still using that buffer in szFile.
		 */
	}

	/* We've loaded the data; get cracking with parsing it. */

	szFileRover = szFile;

#ifdef UNICODE
	/* Skip the BOM. */
	if(szFileRover[0] == UCS2_BOM) szFileRover++;
#endif

	/* Treat the whole thing as a section. */
	lpcfg = ParseConfigSection(&szFileRover);

	ProcHeapFree(szFile);

	/* If we failed, this is NULL, which is our error return anyway. */
	return lpcfg;
}


/* ParseConfigSection
 *   Generates a config structure for a section of a config file.
 *
 * Parameters:
 *   LPTSTR		*lpszFile		Address of buffer containing section.
 *
 * Return value:
 *   Pointer to new config structure, or NULL on error.
 *
 * Notes:
 *   A section is bounded either by {} or by the beginning and end of the
 *   buffer. The function advances the pointer that's passed so the caller can
 *   resume parsing where we left off.
 */
static CONFIG* ParseConfigSection(LPTSTR *lpszFile)
{
	CONFIG *lpcfg;
	BOOL bEndSection = FALSE, bError = FALSE;

	/* In the event that we actually have an entry called NODEROOT, the root
	 * will, in fact, be used to store it. Having it begin with 'n' also makes
	 * it nice for balancing the tree.
	 */
	lpcfg = ConfigCreate();

	while(!bEndSection && !bError)
	{
		EatCommentsAndWhitespace((LPCTSTR*)lpszFile);

		switch(**lpszFile)
		{
		/* Finished! As a special case, don't advance at '\0': this stops
		 * badly-formed configs from crashing.
		 */
		case TEXT('}'):
			(*lpszFile)++;
		case TEXT('\0'):
			bEndSection = TRUE;
			break;

		/* Skip over whitespace and extraneous semicolons. */
		case TEXT(' '): case TEXT('\n'): case TEXT('\r'):
		case TEXT('\t'): case TEXT(';'):
			(*lpszFile)++;
			break;

		/* Comments are now processed elsewhere. */

		/* Nothing special; must be an identifier. */
		default:

			/* Delegate the task of creating and adding the node. */
			if(!ParseAndAddConfigStatement(lpcfg, lpszFile))	/* Bad statement. */
				bError = TRUE;

		}	/* switch(*(*lpszFile)) */
	}		/* while(!bEndSection && !bError) */

	/* If something went wrong somewhere, give up entirely on the section. */
	if(bError)
	{
		ConfigDestroy(lpcfg);
		return NULL;
	}

	return lpcfg;
}


/* ParseAndAddConfigStatement
 *   Parses a statement and adds the required node.
 *
 * Parameters:
 *   CONFIG		*lpcfgRoot		Root of tree to which the statement pertains.
 *   LPTSTR		*lpszFile		Address of buffer containing section.
 *
 * Return value: BOOL
 *   TRUE on success; FALSE on error.
 *
 * Notes:
 *   TEXT('Statement') includes subsections.
 */
static BOOL ParseAndAddConfigStatement(CONFIG *lpcfgRoot, LPTSTR *lpszFile)
{
	LPTSTR szID, szEndID;
	TCHAR tcOperator;

	szID = *lpszFile;

	/* We're at the beginning of an identifier, which ends when we reach
	 * whitespace or an operator. Move past the end.
	 */
	while(!CharBelongsToSet(**lpszFile, TEXT(" \n\r\t={;"))) (*lpszFile)++;

	/* Zero-length identifier. */
	if(*lpszFile == szID) return FALSE;

	/* Eat whitespace between identifier and operator. */
	szEndID = *lpszFile;
	EatCommentsAndWhitespace((LPCTSTR*)lpszFile);
	tcOperator = *(*lpszFile)++;

	*szEndID = TEXT('\0');	/* Terminate identifier. */



	/* Determine whether it's a subsection, assignment, or atomic entry. */
	if(tcOperator == TEXT('{'))		/* Subsection. */
	{
		CONFIG *lpcfgSubsectContainer;

		lpcfgSubsectContainer = CreateConfigNode(szID);
		lpcfgSubsectContainer->entrytype = CET_SUBSECTION;
		lpcfgSubsectContainer->lpcfgSubsection = ParseConfigSection(lpszFile);

		if(!lpcfgSubsectContainer->lpcfgSubsection)
		{
			/* Clean up -- no actual data to free, just node metadata. */
			lpcfgSubsectContainer->entrytype = CET_NULL;
			DestroyConfigNode(lpcfgSubsectContainer);
			return FALSE;
		}

		/* Section is okay. Try to add it. */
		if(!AddConfigNode(lpcfgRoot, lpcfgSubsectContainer))
		{
			/* The section already existed. The new section was put in its old
			 * node. Free our node.
			 */
			DestroyConfigNode(lpcfgSubsectContainer);
		}
	}
	else if(tcOperator == TEXT(';'))	/* Null entry. */
	{
		/* We're almost there now. Add atomic entry to the config. */
		ConfigSetAtom(lpcfgRoot, szID);
	}
	else if(tcOperator == TEXT('='))		/* Assignment. */
	{
		/* Move to the beginning of the value to be assigned. */
		EatCommentsAndWhitespace((LPCTSTR*)lpszFile);

		/* String? */
		if(**lpszFile == TEXT('"'))
		{
			LPCTSTR szValue;

			/* Skip opening quote. */
			(*lpszFile)++;
			szValue = *lpszFile;

			/* Make sure string is valid. */
			if(!ParseConfigStringValue(lpszFile)) return FALSE;

			/* We're almost there now. Set the string in the config. */
			ConfigSetString(lpcfgRoot, szID, szValue);
		}
		else	/* Number. */
		{
			CONFIG_ENTRY_TYPE cet;
			int i;
			float f = 0.0f;

			/* Determine the type of number. */
			cet = ParseConfigNumericValue(lpszFile, &i, &f);

			/* Syntax error? */
			if(cet == CET_NULL) return FALSE;

			/* Which type are we dealing with? */
			switch(cet)
			{
			case CET_INT:
				/* We're almost there now. Set the int in the config. */
				ConfigSetInteger(lpcfgRoot, szID, i);
				break;

			case CET_FLOAT:
				/* We're almost there now. Set the float in the config. */
				ConfigSetFloat(lpcfgRoot, szID, f);

			default:
                break;
			}
		}
	}
	else return FALSE;		/* Not assignment, subsect or atom. */

	/* Success! */
	return TRUE;
}


/* ParseConfigStringValue
 *   Parses an escaped, quoted string for assignment.
 *
 * Parameters:
 *   LPTSTR*	lpsz	Address of pointer to first ch. of string (after quote).
 *
 * Return value: BOOL
 *   TRUE on success; FALSE on error.
 *
 * Notes:
 *   The pointer's advanced beyond the semicolon.
 */
static BOOL ParseConfigStringValue(LPTSTR *lpsz)
{
	LPTSTR szStart;
	int iReducedChars;

	szStart = *lpsz;

	/* We have a little work to do. We'll do it in two passes. First, make sure
	 * the string is closed, and terminate it at the endquote.
	 */
	while(**lpsz && **lpsz != TEXT('"'))
	{
		/* Skip over escape characters. */
		if(**lpsz == TEXT('\\'))
		{
			(*lpsz)++;
			if(isdigit(**lpsz)) *lpsz += 2;	/* Skip 3-digit code. */
		}

		/* Next char. */
		(*lpsz)++;
	}

	if(!(**lpsz)) return FALSE;	/* Unterminated. */

	/* Replace the endquote with \0, and move on. */
	*(*lpsz)++ = TEXT('\0');

	/* Eat whitespace. */
	EatCommentsAndWhitespace((LPCTSTR*)lpsz);

	/* Must be semicolon now. */
	if(*(*lpsz)++ != TEXT(';')) return FALSE;

	/* Now we tidy up the string, replacing escaped things. */
	iReducedChars = 0;
	while(szStart[iReducedChars])
	{
		if(szStart[iReducedChars] == TEXT('\\'))
		{
			/* Move to char after backspace. */
			iReducedChars++;

			/* \nnn pattern? */
			if(_istdigit(szStart[iReducedChars]))
			{
				TCHAR szNum[4];

				/* Copy the digits in. */
				szNum[0] = szStart[iReducedChars];
				szNum[1] = szStart[iReducedChars+1];
				szNum[2] = szStart[iReducedChars+2];
				szNum[3] = TEXT('\0');

				/* Must all be digits. */
				if(!_istdigit(szNum[1]) || !_istdigit(szNum[2])) return FALSE;

				/* Get the character and then skip over. */
				*szStart = _ttoi(szNum);
				iReducedChars += 2;
			}
			else	/* Single escape char. */
			{
				switch(szStart[iReducedChars])
				{
				case TEXT('n'):
					*szStart = 0x10;	/* Really a linefeed, not CRLF. */
					break;
				case TEXT('r'):
					*szStart = TEXT('\r');
					break;
				case TEXT('t'):
					*szStart = TEXT('\t');
					break;

				/* Unrecognised (including " and \), so just output the character. */
				default:
					*szStart = szStart[iReducedChars];
				}
			}
		}
		/* Just copy, and even then, only if necessary. */
		else if(iReducedChars > 0) *szStart = szStart[iReducedChars];

		szStart++;
	}

	/* Since we copied backwards, we may still have garbage left at the end of
	 * the buffer. Terminate the string.
	 */
	*szStart = TEXT('\0');

	return TRUE;
}


/* ParseConfigNumericValue
 *   Parses a numeric value (float or int) for assignment.
 *
 * Parameters:
 *   LPTSTR		*lpsz		Address of pointer to first ch. of string (after
 *							quote).
 *   int		*lpi		Pointer to int to set if type is int.
 *   float		*lpf		Pointer to float to set if type is float.
 *
 * Return value: CONFIG_ENTRY_TYPE
 *   CET_FLOAT if num was a float, CET_INT if it was an int, and CET_NULL on
 *   error.
 *
 * Notes:
 *   The pointer's advanced beyond the semicolon.
 */
static CONFIG_ENTRY_TYPE ParseConfigNumericValue(LPTSTR *lpsz, int *lpi, float *lpf)
{
	LPCTSTR szStart = *lpsz;
	CONFIG_ENTRY_TYPE cet;

	/* First, validate and determine type. */

	/* We're allowed one leading minus sign. */
	if(**lpsz == TEXT('-')) (*lpsz)++;

	/* Pass over initial digits. */
	while(_istdigit(**lpsz)) (*lpsz)++;

	/* Float? */
	if(**lpsz == TEXT('.')) cet = CET_FLOAT;
	else cet = CET_INT;

	/* Eat whitespace. */
	EatCommentsAndWhitespace((LPCTSTR*)lpsz);

	/* Must be at semicolon now. */
	if(**lpsz != TEXT(';')) return CET_NULL;

	/* Terminate for reading string. */
	*(*lpsz)++ = TEXT('\0');

	if(cet == CET_FLOAT)
		*lpf = (float)_tcstod(szStart, NULL);
	else
		*lpi = _ttoi(szStart);

	return cet;
}



/* ReadPastString, ReadUntilAnyChar, ReadPastAnyChar, CharBelongsToSet
 *   String-parsing utilities.
 *
 * Parameters:
 *   Varying, but intuitively comprehensible.
 *
 * Return value: BOOL
 *   TRUE if found; FALSE otherwise.
 */
static BOOL ReadPastString(LPCTSTR *lpsz, LPCTSTR szToken)
{
	BOOL bFound = TRUE;		/* Assume that we find the token. */
	int iTokenLength = _tcslen(szToken);

	/* If weTEXT('re at the end, signal that we didn')t find it. Otherwise, keep going
	 * until we do find it, or reach the end.
	 */
	while((**lpsz || (bFound = FALSE)) && _tcsncmp(*lpsz, szToken, iTokenLength)) (*lpsz)++;

	if(bFound) *lpsz += _tcslen(szToken);
	return bFound;
}

static BOOL ReadUntilAnyChar(LPCTSTR *lpsz, LPCTSTR szCharSet)
{
	BOOL bFound = TRUE;		/* Assume that we find the token. */

	/* If weTEXT('re at the end, signal that we didn')t match. Otherwise, keep going
	 * until we do match, or reach the end.
	 */
	while((**lpsz || (bFound = FALSE)) && !CharBelongsToSet(**lpsz, szCharSet)) (*lpsz)++;

	return bFound;
}

static BOOL ReadPastAnyChar(LPCTSTR *lpsz, LPCTSTR szCharSet)
{
	BOOL bFound = TRUE;		/* Assume that we find the token. */

	/* If weTEXT('re at the end, signal that we didn')t match. Otherwise, keep going
	 * until we do match, or reach the end.
	 */
	while((**lpsz || (bFound = FALSE)) && CharBelongsToSet(**lpsz, szCharSet)) (*lpsz)++;

	return bFound;
}

static BOOL CharBelongsToSet(TCHAR tc, LPCTSTR szCharSet)
{
	BOOL bMatch = TRUE;

	/* If we're at the end, signal that we didn't match. Otherwise, keep going
	 * until we do match, or reach the end.
	 */
	while((*szCharSet || (bMatch = FALSE)) && *szCharSet++ != tc);
	return bMatch;
}



/* EatCommentsAndWhitespace
 *   Reads past any comments and whitespace.
 *
 * Parameters:
 *   LPCTSTR*		lpsz	Pointer to pointer to beginning of string to parse.
 *
 * Return value: BOOL
 *   FALSE if an unterminated block comment occurred; TRUE otherwise.
 */
static BOOL EatCommentsAndWhitespace(LPCTSTR *lpsz)
{
	ReadPastAnyChar(lpsz, TEXT(" \n\r\t"));

	/* We're at the end of leading whitespace. Are we at a comment? */
	if(**lpsz == TEXT('/'))
	{
		/* Block comment. */
		if((*lpsz)[1] == TEXT('*'))
		{
			(*lpsz) += 2;	/* Skip initial TEXT('/') and TEXT('*'). */
			if(!ReadPastString(lpsz, TEXT("*/"))) return FALSE;

			/* Passed the comment -- eat s'more! */
			return EatCommentsAndWhitespace(lpsz);
		}
		/* Line comment. */
		else if((*lpsz)[1] == TEXT('/'))
		{
			(*lpsz) += 2;
			/* Pass next newline. */
			ReadUntilAnyChar(lpsz, TEXT("\n\r"));
			ReadPastAnyChar(lpsz, TEXT("\n\r"));

			/* Passed the comment -- eat s'more! */
			return EatCommentsAndWhitespace(lpsz);
		}
	}

	/* No problems. */
	return TRUE;
}


/* ConfigSetString, ConfigSetFloat, ConfigSetInteger, ConfigSetAtom
 *   Sets the value of a config entry, adding it if it doesn't exist.
 *
 * Parameters:
 *   CONFIG*	lpcfgRoot	Root of config tree.
 *   LPCTSTR	szName		Name of element to set.
 *   LPCTSTR	szValue		\
 *   float		f			-	Values to set.
 *   int		i			/
 *
 * Return value:
 *   None.
 */
#ifdef UNICODE
void ConfigSetStringA(CONFIG *lpcfgRoot, LPCWSTR szName, LPCSTR szValue)
{
	int cchValue = strlen(szValue) + 1;
	LPWSTR szValueW = ProcHeapAlloc(sizeof(WCHAR) * cchValue);

	MultiByteToWideChar(CP_ACP, MB_PRECOMPOSED, szValue, -1, szValueW, cchValue);
	ConfigSetString(lpcfgRoot, szName, szValueW);

	ProcHeapFree(szValueW);
}
#endif

void ConfigSetString(CONFIG *lpcfgRoot, LPCTSTR szName, LPCTSTR szValue)
{
	CONFIG *lpcfgNew = CreateConfigNode(szName);
	int iStrLen = _tcslen(szValue) + 1;

	lpcfgNew->entrytype = CET_STRING;
	lpcfgNew->sz = ProcHeapAlloc(iStrLen * sizeof(TCHAR));
	CopyMemory(lpcfgNew->sz, szValue, iStrLen * sizeof(TCHAR));

	if(!AddConfigNode(lpcfgRoot, lpcfgNew))
	{
		/* Node of that name already existed; free the new node. */
		DestroyConfigNode(lpcfgNew);
	}
}

void ConfigSetFloat(CONFIG *lpcfgRoot, LPCTSTR szName, float f)
{
	CONFIG *lpcfgNew = CreateConfigNode(szName);

	lpcfgNew->entrytype = CET_FLOAT;
	lpcfgNew->f = f;

	if(!AddConfigNode(lpcfgRoot, lpcfgNew))
	{
		/* Node of that name already existed; free the new node. */
		DestroyConfigNode(lpcfgNew);
	}
}

void ConfigSetInteger(CONFIG *lpcfgRoot, LPCTSTR szName, int i)
{
	CONFIG *lpcfgNew = CreateConfigNode(szName);

	lpcfgNew->entrytype = CET_INT;
	lpcfgNew->i = i;

	if(!AddConfigNode(lpcfgRoot, lpcfgNew))
	{
		/* Node of that name already existed; free the new node. */
		DestroyConfigNode(lpcfgNew);
	}
}

#ifdef UNICODE
void ConfigSetAtomA(CONFIG *lpcfgRoot, LPCSTR szName)
{
	int cchName = strlen(szName) + 1;
	LPWSTR szNameW = ProcHeapAlloc(sizeof(WCHAR) * cchName);

	MultiByteToWideChar(CP_ACP, MB_PRECOMPOSED, szName, -1, szNameW, cchName);
	ConfigSetAtom(lpcfgRoot, szNameW);

	ProcHeapFree(szNameW);
}
#endif

void ConfigSetAtom(CONFIG *lpcfgRoot, LPCTSTR szName)
{
	CONFIG *lpcfgNew = CreateConfigNode(szName);

	lpcfgNew->entrytype = CET_NULL;

	if(!AddConfigNode(lpcfgRoot, lpcfgNew))
	{
		/* Node of that name already existed; free the new node. */
		DestroyConfigNode(lpcfgNew);
	}
}



/* ConfigSetSubsection
 *   Adds a config to another config as a subsection.
 *
 * Parameters:
 *   CONFIG		*lpcfgRoot	Root of config tree.
 *   LPCTSTR	szName		Name of subsection.
 *   CONFIG		*lpcfgSS	Subsection tree.
 *
 * Return value: None.
 */
void ConfigSetSubsection(CONFIG *lpcfgRoot, LPCTSTR szName, CONFIG *lpcfgSS)
{
	CONFIG *lpcfgSubsectContainer = CreateConfigNode(szName);

	/* This is for the container node, remember. */
	lpcfgSubsectContainer->entrytype = CET_SUBSECTION;

	/* Store it once in the container node. */
	lpcfgSubsectContainer->lpcfgSubsection = lpcfgSS;

	/* Add new subsection, and check whether a node of that name already
	 * existed.
	 */
	if(!AddConfigNode(lpcfgRoot, lpcfgSubsectContainer))
	{
		/* The section already existed. The new section was put in its old
		 * node. Free our node.
		 */
		DestroyConfigNode(lpcfgSubsectContainer);
	}
}


/* ConfigAddSubsection
 *   Creates a new subsection and adds it to the tree.
 *
 * Parameters:
 *   CONFIG		*lpcfgRoot	Root of config tree.
 *   LPCTSTR	szName		Name of subsection to create.
 *
 * Return value:
 *   Pointer to the root (not the container!) of the new subsection.
 */
CONFIG* ConfigAddSubsection(CONFIG *lpcfgRoot, LPCTSTR szName)
{
	CONFIG *lpcfgSubsectContainer = CreateConfigNode(szName);
	CONFIG *lpcfgNewSubsection;

	/* This is for the container node, remember. */
	lpcfgSubsectContainer->entrytype = CET_SUBSECTION;

	/* Store it once in the container node, and once for returning (since we
	 * might have to destroy the container).
	 */
	lpcfgNewSubsection = lpcfgSubsectContainer->lpcfgSubsection = ConfigCreate();

	/* Add new subsection, and check whether a node of that name already
	 * existed.
	 */
	if(!AddConfigNode(lpcfgRoot, lpcfgSubsectContainer))
	{
		/* The section already existed. The new section was put in its old
		 * node. Free our node.
		 */
		DestroyConfigNode(lpcfgSubsectContainer);
	}

	return lpcfgNewSubsection;
}


/* CreateConfigNode
 *   Creates a node in a config tree.
 *
 * Parameters:
 *   LPCTSTR	szName		Name of node.
 *
 * Return value: CONFIG*
 *   Pointer to new config node.
 *
 * Notes:
 *   The new node has a null type.
 */
static CONFIG* CreateConfigNode(LPCTSTR szName)
{
	CONFIG *lpcfg = ProcHeapAlloc(sizeof(CONFIG));

	lpcfg->szName = ProcHeapAlloc((_tcslen(szName) + 1) * sizeof(TCHAR));
	_tcscpy(lpcfg->szName, szName);
	lpcfg->entrytype = CET_NULL;
	lpcfg->lpcfgLeft = lpcfg->lpcfgRight = NULL;

	return lpcfg;
}


/* AddConfigNode
 *   Adds a node to a config tree.
 *
 * Parameters:
 *   CONFIG		*lpcfgRoot	Root of tree to add to.
 *   CONFIG		*lpcfgNew	Node to add.
 *
 * Return value: BOOL
 *   FALSE if a node with the same name as the new one already existed; TRUE
 *   otherwise.
 *
 * Notes:
 *   If a node with the same name as the new one already exists, the *existing*
 *   node is altered, so the app should free the node it passed in, assuming it
 *   doesnTEXT('t need it any more (since it')s not actually part of the tree). Also,
 *   in these circumstances, the passed-in node may be changed.
 */
static BOOL AddConfigNode(CONFIG *lpcfgRoot, CONFIG *lpcfgNew)
{
	int iStrCmpResult;

	/* The conditions for loop termination are sufficiently diverse that this is
	 * probably the neatest way of doing it. It's just binary tree node
	 * insertion, anyway.
	 */
	while(TRUE)
	{
		iStrCmpResult = _tcsicmp(lpcfgNew->szName, lpcfgRoot->szName);

		if(!iStrCmpResult)
		{
			/* A node with this name already exists, so alter that one. */
			DestroyConfigNodeData(lpcfgRoot);

			/* We reuse the memory from the new node. */
			switch(lpcfgNew->entrytype)
			{
			case CET_INT:
				lpcfgRoot->i = lpcfgNew->i;
				break;

			case CET_FLOAT:
				lpcfgRoot->f = lpcfgNew->f;
				break;

			case CET_STRING:
				lpcfgRoot->sz = lpcfgNew->sz;
				break;

			case CET_SUBSECTION:
				lpcfgRoot->lpcfgSubsection = lpcfgNew->lpcfgSubsection;
				break;

			default:
                break;
			}

			lpcfgRoot->entrytype = lpcfgNew->entrytype;

			/* Set the TEXT('new') node to a null node, since we're using its data. */
			lpcfgNew->entrytype = CET_NULL;

			return FALSE;		/* Node has been added by alteration. */
		}
		else if(iStrCmpResult < 0)
		{
			/* New node belongs on left. */
			if(lpcfgRoot->lpcfgLeft)
			{
				/* Repeat everything for the left branch. */
				lpcfgRoot = lpcfgRoot->lpcfgLeft;
			}
			else
			{
				/* We're empty on the left, so just add ourselves here. */
				lpcfgRoot->lpcfgLeft = lpcfgNew;
				return TRUE;		/* New node added. */
			}
		}
		else	/* iStrCmpResult > 0 */
		{
			/* New node belongs on right. */
			if(lpcfgRoot->lpcfgRight)
			{
				/* Repeat everything for the right branch. */
				lpcfgRoot = lpcfgRoot->lpcfgRight;
			}
			else
			{
				/* We're empty on the right, so just add ourselves here. */
				lpcfgRoot->lpcfgRight = lpcfgNew;
				return TRUE;		/* New node added. */
			}
		}
	}
}


/* ConfigDestroy
 *   Frees a config node tree.
 *
 * Parameters:
 *   CONFIG		*lpcfg	Root node of tree to free.
 *
 * Return value: None.
 *
 * Notes:
 *   This just recurses over the tree, calling DestroyConfigNode for each node.
 */
void ConfigDestroy(CONFIG *lpcfg)
{
	if(lpcfg->lpcfgLeft) ConfigDestroy(lpcfg->lpcfgLeft);
	if(lpcfg->lpcfgRight) ConfigDestroy(lpcfg->lpcfgRight);
	DestroyConfigNode(lpcfg);
}


/* DestroyConfigNode
 *   Frees a config node and its data.
 *
 * Parameters:
 *   CONFIG		*lpcfg	Node to free.
 *
 * Return value: None.
 *
 * Notes:
 *   This doesn't free any children. Use DestroyConfig for that.
 */
static void DestroyConfigNode(CONFIG *lpcfg)
{
	/* Clean up the contents of the node. */
	DestroyConfigNodeData(lpcfg);

	/* Free the name storage. */
	ProcHeapFree(lpcfg->szName);

	/* Free the node itself. */
	ProcHeapFree(lpcfg);
}

/* DestroyConfigNodeData
 *   Frees a config node's data.
 *
 * Parameters:
 *   CONFIG		*lpcfg	Node whose data is to be freed.
 *
 * Return value: None.
 *
 * Notes:
 *   If this is a subsection, that entire tree is freed.
 */
static void DestroyConfigNodeData(CONFIG *lpcfg)
{
	/* If the memory for this node's data was allocated dynamically, free it. */
	switch(lpcfg->entrytype)
	{
	case CET_STRING:
		ProcHeapFree(lpcfg->sz);
		break;
	case CET_SUBSECTION:
		ConfigDestroy(lpcfg->lpcfgSubsection);
		break;

	default:
        break;
	}
}


/* FindConfigNode
 *   Searches a tree for a given node.
 *
 * Parameters:
 *   CONFIG*	lpcfgRoot	Root of tree to search.
 *   LPCTSTR	szName		Name of node to find.
 *
 * Return value: CONFIG*
 *   Address of matching node, or NULL if not found.
 */
static CONFIG* FindConfigNode(CONFIG *lpcfgRoot, LPCTSTR szName)
{
	int iStrCmpResult;

	/* Stop when the branch where the node *would* be is empty. */
	while(lpcfgRoot)
	{
		iStrCmpResult = _tcsicmp(szName, lpcfgRoot->szName);

		if(!iStrCmpResult)
		{
			/* Found it! */
			return lpcfgRoot;
		}
		else if(iStrCmpResult < 0)
		{
			/* Node belongs on left. */
			lpcfgRoot = lpcfgRoot->lpcfgLeft;
		}
		else	/* iStrCmpResult > 0 */
		{
			/* Node belongs on right. */
			lpcfgRoot = lpcfgRoot->lpcfgRight;
		}
	}

	return NULL;
}

/* ConfigNodeExists
 *   Searches a tree for a given node.
 *
 * Parameters:
 *   CONFIG*	lpcfgRoot	Root of tree to search.
 *   LPCTSTR	szName		Name of node to find.
 *
 * Return value: BOOL
 *   TRUE if node found, or FALSE if not.
 */
BOOL ConfigNodeExists(CONFIG *lpcfgRoot, LPCTSTR szName)
{
	return FindConfigNode(lpcfgRoot, szName) != NULL;
}


/* ConfigGetInteger
 *   Gets an integer from a config structure.
 *
 * Parameters:
 *   CONFIG*	lpcfgRoot	Root of config tree.
 *   LPCTSTR	szName		Name of node whose value is to be retrieved.
 *
 * Return value: int
 *   Value of node, or zero if not found.
 *
 * Notes:
 *   Floats are casted to ints. Non-numbers have an integer value of zero.
 */
int ConfigGetInteger(CONFIG *lpcfgRoot, LPCTSTR szName)
{
	CONFIG *lpcfg = FindConfigNode(lpcfgRoot, szName);

	/* Not found. */
	if(!lpcfg)
		return 0;

	switch(lpcfg->entrytype)
	{
	case CET_INT:
		return lpcfg->i;
	case CET_FLOAT:
		return (int)lpcfg->f;
	default:
		return 0;
	}
}


/* ConfigGetFloat
 *   Gets a float from a config structure.
 *
 * Parameters:
 *   CONFIG*	lpcfgRoot	Root of config tree.
 *   LPCTSTR	szName		Name of node whose value is to be retrieved.
 *
 * Return value: float
 *   Value of node, or zero if not found.
 *
 * Notes:
 *   Ints are casted to floats. Non-numbers have an integer value of zero.
 */
float ConfigGetFloat(CONFIG *lpcfgRoot, LPCTSTR szName)
{
	CONFIG *lpcfg = FindConfigNode(lpcfgRoot, szName);

	/* Not found. */
	if(!lpcfg)
		return 0;

	switch(lpcfg->entrytype)
	{
	case CET_FLOAT:
		return lpcfg->f;
	case CET_INT:
		return (float)lpcfg->i;
	default:
		return 0;
	}
}


/* ConfigGetStringLength
 *   Gets the length of a string from within a config structure.
 *
 * Parameters:
 *   CONFIG*	lpcfgRoot	Root of config tree.
 *   LPCTSTR	szName		Name of node whose string length is to be retrieved.
 *
 * Return value: int
 *   Length of string, or zero if not found.
 *
 * Notes:
 *   Non-strings are considered to have a length of zero.
 */
int ConfigGetStringLength(CONFIG *lpcfgRoot, LPCTSTR szName)
{
	CONFIG *lpcfg = FindConfigNode(lpcfgRoot, szName);

	/* Not found. */
	if(!lpcfg)
		return 0;

	switch(lpcfg->entrytype)
	{
	case CET_STRING:
		return _tcslen(lpcfg->sz);
	default:
		return 0;
	}
}


/* ConfigGetString
 *   Gets a string from within a config structure.
 *
 * Parameters:
 *   CONFIG			*lpcfgRoot	Root of config tree.
 *   LPCTSTR		szName		Name of node whose string is to be retrieved.
 *   LPTSTR			szBuffer	Buffer in which to store string data.
 *   unsigned int	cchBuffer	Length of buffer, including room for termiator.
 *
 * Return value: BOOL
 *   TRUE if node exists and sufficient room in buffer; FALSE otherwise.
 *
 * Notes:
 *   Non-strings put an empty string in the buffer. If the buffer is too small,
 *   as much will be copied in as possible.
 */
#ifdef UNICODE
BOOL ConfigGetStringA(CONFIG *lpcfgRoot, LPCWSTR szName, LPSTR szBuffer, unsigned int cchBuffer)
{
	LPWSTR szBufferW = ProcHeapAlloc(sizeof(WCHAR) * cchBuffer);

	BOOL bRet = ConfigGetString(lpcfgRoot, szName, szBufferW, cchBuffer);

	if(bRet)
		WideCharToMultiByte(CP_ACP, 0, szBufferW, -1, szBuffer, cchBuffer, NULL, NULL);

	ProcHeapFree(szBufferW);

	return bRet;
}
#endif


BOOL ConfigGetString(CONFIG *lpcfgRoot, LPCTSTR szName, LPTSTR szBuffer, unsigned int cchBuffer)
{
	CONFIG *lpcfg;

	/* Buffer must be at least one char long. */
	if(cchBuffer == 0) return FALSE;

	lpcfg = FindConfigNode(lpcfgRoot, szName);

	/* Not found. */
	if(!lpcfg)
		return FALSE;

	switch(lpcfg->entrytype)
	{
	case CET_STRING:

		_sntprintf(szBuffer, cchBuffer - 1, TEXT("%s"), lpcfg->sz);

		/* _snwprintf doesn't terminate if buffer full. */
		szBuffer[cchBuffer-1] = TEXT('\0');

		return (unsigned int)_tcslen(lpcfg->sz) < cchBuffer;

	default:
		*szBuffer = TEXT('\0');
		return TRUE;	/* Even though it's not a string, still succeed. */
	}
}


/* ConfigGetSubsection
 *   Gets a subsection from within a config structure.
 *
 * Parameters:
 *   CONFIG*	lpcfgRoot	Root of config tree.
 *   LPCTSTR	szName		Name of node whose subsect. is to be retrieved.
 *
 * Return value: CONFIG*
 *   Pointer to root (not container!) of subsection, or NULL if the specified
 *   entry is not a subsection or another error occurs.
 */
CONFIG* ConfigGetSubsection(CONFIG *lpcfgRoot, LPCTSTR szName)
{
	CONFIG *lpcfg;

	lpcfg = FindConfigNode(lpcfgRoot, szName);

	/* Not found, or not a subsection. */
	if(!lpcfg || lpcfg->entrytype != CET_SUBSECTION)
		return NULL;

	return lpcfg->lpcfgSubsection;
}



/* ConfigCreate
 *   Creates a new root node.
 *
 * Parameters: None.
 *
 * Return value: CONFIG*
 *   Pointer to new root.
 *
 * Remarks:
 *   This is what the rest of the world should use to create a blank config. If
 *   they want to load a config from disc, they should use ConfigLoad instead.
 */
CONFIG *ConfigCreate(void)
{
	return CreateConfigNode(NODEROOT);
}



/* ConfigDuplicate
 *   Duplicates a config node and its descendants.
 *
 * Parameters:
 *   CONFIG*	lpcfg	Config to duplicate.
 *
 * Return value: CONFIG*
 *   Pointer to duplicate.
 *
 * Remarks:
 *   Map configs, in particular, want two copies of thing data in different
 *   places. We can't just have two pointers to the same location, or we'd
 *   explode when we tried to free the config.
 *     The values of the undefined data in the original config is undefined in
 *   the new, i.e. there's no guarantee that the fluff is copied, whatever it
 *   may be.
 */
CONFIG* ConfigDuplicate(CONFIG *lpcfg)
{
	CONFIG *lpcfgNew;

	/* Makes the recursion slightly neater. */
	if(!lpcfg) return NULL;

	/* New node. This duplicates the name, of course. */
	lpcfgNew = CreateConfigNode(lpcfg->szName);

	/* Duplicate the contents and descendants. */
	lpcfgNew->entrytype = lpcfg->entrytype;
	lpcfgNew->lpcfgLeft = ConfigDuplicate(lpcfg->lpcfgLeft);
	lpcfgNew->lpcfgRight = ConfigDuplicate(lpcfg->lpcfgRight);

	/* If the value has memory associated with it, duplicate that, too. */
	switch(lpcfg->entrytype)
	{
	case CET_STRING:
		lpcfgNew->sz = ProcHeapAlloc((_tcslen(lpcfg->sz) + 1) * sizeof(TCHAR));
		CopyMemory(lpcfgNew->sz, lpcfg->sz, (_tcslen(lpcfg->sz) + 1) * sizeof(TCHAR));
		break;
	case CET_SUBSECTION:
		lpcfgNew->lpcfgSubsection = ConfigDuplicate(lpcfg->lpcfgSubsection);
		break;
	case CET_INT:
		lpcfgNew->i = lpcfg->i;
		break;
	case CET_FLOAT:
		lpcfgNew->f = lpcfg->f;
		break;

	default:
        break;
	}

	return lpcfgNew;
}



/* ConfigIterate
 *  Calls a function for each item in a config tree.
 *
 * Parameters:
 *   CONFIG*					lpcfgRoot		Pointer to root of config.
 *   BOOL (*)(CONFIG*, void*)	lpfnCallback	Function to call for each item.
 *   void*						lpvParam		Extra parameter to callback.
 *
 * Return value: BOOL
 *   FALSE if iteration was terminated by the callback function; TRUE otherwise.
 */
BOOL ConfigIterate(CONFIG *lpcfgRoot, BOOL (*lpfnCallback)(CONFIG*, void*), void *lpvParam)
{
	while(lpcfgRoot)
	{
		/* Loop to the left and recur to the right. More efficient than just
		 * recurring down both sides.
		 */

		if(lpcfgRoot->lpcfgLeft)
			if(!lpfnCallback(lpcfgRoot->lpcfgLeft, lpvParam)) return FALSE;
		if(lpcfgRoot->lpcfgRight)
		{
			if(!lpfnCallback(lpcfgRoot->lpcfgRight, lpvParam)) return FALSE;
			ConfigIterate(lpcfgRoot->lpcfgRight, lpfnCallback, lpvParam);
		}

		lpcfgRoot = lpcfgRoot->lpcfgLeft;
	}

	return TRUE;
}


/* ConfigWrite
 *  Writes a config to a file.
 *
 * Parameters:
 *   CONFIG*	lpcfg		Root of config to write.
 *   LPCTSTR	szFilename	Filename.
 *
 * Return value: int
 *   Zero on success; nonzero on error.
 */
int ConfigWrite(CONFIG *lpcfg, LPCTSTR szFilename)
{
	/* Open in binary mode. We need this to handle Unicode correctly. An
	 * annoying side-effect is that we need to handle newlines manually.
	 */
	FILE *lpfile = _tfopen(szFilename, TEXT("wb"));

	int iRet = 0;

	/* Couldn't create file? */
	if(!lpfile) return 1;

#ifdef UNICODE
	/* Write the BOM. */
	fputwc(UCS2_BOM, lpfile);
#endif

	/* Write the entire config. Zero tabs to start with. */
	if(!ConfigWriteSubsection(lpfile, lpcfg, 0))
		iRet = 2;

	/* Tidy up and return. */
	fclose(lpfile);
	return iRet;
}


/* ConfigWriteSubsection
 *  Writes a config subsection to a file.
 *
 * Parameters:
 *   FILE*		lpfile					File handle to write to.
 *   CONFIG*	lpcfgSubsectionRoot		Root of subsection to write.
 *   int		iTabs					Number of tabs required at start of each
 *										line.
 *
 * Return value: BOOL
 *   TRUE on success; FALSE on error.
 */
static BOOL ConfigWriteSubsection(FILE *lpfile, CONFIG *lpcfgSubsectionRoot, int iTabs)
{
	CWEDATA cwedata = {lpfile, iTabs};

	/* Write the subsection to the file. */
	return ConfigIterate(lpcfgSubsectionRoot, ConfigWriteEntry, &cwedata);
}


/* ConfigWriteEntry
 *  Writes a config entry to a file.
 *
 * Parameters:
 *   CONFIG*	lpcfgNode	Config node to write.
 *   void*		lpvParam	(CWEDATA*) File handle and number of tabs per line.
 *
 * Return value: BOOL
 *   TRUE on success; FALSE on error.
 *
 * Remarks:
 *   Designed to be used with ConfigIterate. If a subsection is encountered,
 *   ConfigWriteSubsection is called, which triggers another ConfigIterate.
 */
static BOOL ConfigWriteEntry(CONFIG *lpcfgNode, void *lpvParam)
{
	int i;
	CWEDATA *lpcwedata = (CWEDATA*)lpvParam;

	/* Put an extra newline before the start of a subsection. */
	if(lpcfgNode->entrytype == CET_SUBSECTION && _fputts(CONFIG_NEWLINE, lpcwedata->lpfile) == _TEOF)
		return FALSE;

	/* Write the tabs first of all. */
	for(i = 0; i < lpcwedata->iTabs; i++)
		if(_fputtc(TEXT('\t'), lpcwedata->lpfile) == _TEOF)
			return FALSE;

	/* Write the entry's name. */
	if(_fputts(lpcfgNode->szName, lpcwedata->lpfile) == _TEOF)
		return FALSE;

	/* Subsections work a little differently. */
	if(lpcfgNode->entrytype == CET_SUBSECTION)
	{
		if(_fputts(CONFIG_NEWLINE, lpcwedata->lpfile) == _TEOF)
			return FALSE;

		for(i = 0; i < lpcwedata->iTabs; i++)
			if(_fputtc(TEXT('\t'), lpcwedata->lpfile) == _TEOF)
				return FALSE;

		if(_fputts(TEXT("{") CONFIG_NEWLINE, lpcwedata->lpfile) == _TEOF)
			return FALSE;

		ConfigWriteSubsection(lpcwedata->lpfile, lpcfgNode->lpcfgSubsection, lpcwedata->iTabs + 1);

		for(i = 0; i < lpcwedata->iTabs; i++)
			if(_fputtc(TEXT('\t'), lpcwedata->lpfile) == _TEOF)
				return FALSE;

		if(_fputts(TEXT("}") CONFIG_NEWLINE CONFIG_NEWLINE, lpcwedata->lpfile) == _TEOF)
			return FALSE;

		return TRUE;
	}

	/* Not a subsection, so write TEXT(" = "). */
	if(_fputts(TEXT(" = "), lpcwedata->lpfile) == _TEOF)
		return FALSE;

	/* Each entry type is written differently. */
	switch(lpcfgNode->entrytype)
	{
	case CET_STRING:
		{
			int iLength = _tcslen(lpcfgNode->sz);

			/* Write opening double-quote. */
			if(_fputtc(TEXT('\"'), lpcwedata->lpfile) == _TEOF)
				return FALSE;

			/* Write the string itself, character by character, so that we can
			 * process escaped characters correctly.
			 */
			for(i = 0; i < iLength; i++)
			{
				/* Escape if necessary. */
				switch(lpcfgNode->sz[i])
				{
				case TEXT('\t'):
					if(_fputts(TEXT("\\t"), lpcwedata->lpfile) == _TEOF)
						return FALSE;
					break;

				case TEXT('\n'):
					if(_fputts(TEXT("\\n"), lpcwedata->lpfile) == _TEOF)
						return FALSE;
					break;

				case TEXT('\r'):
					if(_fputts(TEXT("\\r"), lpcwedata->lpfile) == _TEOF)
						return FALSE;
					break;

				case TEXT('\"'):
					if(_fputts(TEXT("\\\""), lpcwedata->lpfile) == _TEOF)
						return FALSE;
					break;

				case TEXT('\\'):
					if(_fputts(TEXT("\\\\"), lpcwedata->lpfile) == _TEOF)
						return FALSE;
					break;

				default:
					/* Nothing special; just write the character. */
					if(_fputtc(lpcfgNode->sz[i], lpcwedata->lpfile) == _TEOF)
						return FALSE;
				}
			}

			/* Write closing double-quote. */
			if(_fputtc(TEXT('\"'), lpcwedata->lpfile) == _TEOF)
				return FALSE;
		}

		break;

	case CET_FLOAT:
		{
			int iBufLen = FLOAT_INITBUFLEN;
			LPTSTR szFloat = ProcHeapAlloc(iBufLen * sizeof(TCHAR));

			/* Convert the float to a string, allocating more space if
			 * necessary.
			 */
			while(_sntprintf(szFloat, iBufLen - 1, TEXT("%f"), lpcfgNode->f) < 0)
			{
				ProcHeapFree(szFloat);
				iBufLen <<= 1;
				szFloat = ProcHeapAlloc(iBufLen * sizeof(TCHAR));
			}

			/* Terminate the buffer, since _snprintf might not have. */
			szFloat[iBufLen - 1] = TEXT('\0');

			/* Write the string representation of the number. */
			if(_fputts(szFloat, lpcwedata->lpfile) == _TEOF)
				return FALSE;

			ProcHeapFree(szFloat);
		}

		break;

	case CET_INT:
		{
			TCHAR szInt[CCH_SIGNED_INT];

			/* Convert the int to a string. */
			_sntprintf(szInt, NUM_ELEMENTS(szInt), TEXT("%d"), lpcfgNode->i);

			/* Write the string representation of the number. */
			if(_fputts(szInt, lpcwedata->lpfile) == _TEOF)
				return FALSE;
		}

		break;

		/* Don't do anything for CET_NULL, and we've handled CET_SUBSECTION. */
	default:
		break;
	}

	/* Finish this line. */
	if(_fputts(TEXT(";") CONFIG_NEWLINE, lpcwedata->lpfile) == _TEOF)
		return FALSE;

	/* Finito. */
	return TRUE;
}


/* ConfigDeleteNode
 *   Removes a node from a config tree.
 *
 * Parameters:
 *   CONFIG*	lpcfgRoot	Root of tree to search.
 *   LPCTSTR	szName		Name of node to remove.
 *
 * Return value: None.
 *
 * Remarks:
 *   Safe if the node doesn't exist.
 */
void ConfigDeleteNode(CONFIG *lpcfgRoot, LPCTSTR szName)
{
	CONFIG *lpcfgParent = NULL, *lpcfgToDelete;
	int iStrCmpResult;

	/* Stop when we find it. */
	while((iStrCmpResult = _tcsicmp(szName, lpcfgRoot->szName)))
	{
		lpcfgParent = lpcfgRoot;

		if(iStrCmpResult < 0)
		{
			/* Node belongs on left. */
			lpcfgRoot = lpcfgRoot->lpcfgLeft;
		}
		else	/* iStrCmpResult > 0 */
		{
			/* Node belongs on right. */
			lpcfgRoot = lpcfgRoot->lpcfgRight;
		}
	}

	/* Someone's passed in TEXT("node-root") or a bad CONFIG*. */
	if(!lpcfgParent)
		return;

	lpcfgToDelete = lpcfgRoot;

	/* Fix up the children of the deleted node. */
	while(lpcfgRoot)
	{
		if(lpcfgParent->lpcfgLeft == lpcfgRoot)
		{
			lpcfgParent->lpcfgLeft = lpcfgRoot->lpcfgLeft;
			lpcfgParent = lpcfgRoot;
			lpcfgRoot = lpcfgRoot->lpcfgRight;
		}
		else
		{
			lpcfgParent->lpcfgRight = lpcfgRoot->lpcfgRight;
			lpcfgParent = lpcfgRoot;
			lpcfgRoot = lpcfgRoot->lpcfgLeft;
		}
	}

	/* Finally destroy the node. */
	DestroyConfigNode(lpcfgToDelete);
}
