/* SRB2 Workbench -- A map editor for Sonic Robo Blast 2.
 * Copyright (C) 2009 Gregor Dick
 * http://workbench.srb2.org/
 *
 * Incorporates portions of Doom Builder, (C) 2003 Pascal vd Heiden.
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 * USA.
 *
 * map.h: Header for map.c.
 *
 * AMDG.
 */

#ifndef __SRB2B_MAP__
#define __SRB2B_MAP__

#include "maptypes.h"
#include "config.h"
#include "wad.h"
#include "like.h"

/* Function prototypes. */
MAP *AllocateMapStructure(void);
void DestroyMapStructure(MAP *lpmap);
int MapLoad(MAP *lpmap, int iWad, LPCSTR szLumpname, DWORD dwFlags);
void UpdateMap(MAP *lpmap, int iWad, LPCSTR szLumpname, CONFIG *lpcfgMap);
void UpdateMapToWadStructure(MAP *lpmap, WAD *lpwad, LPCSTR szLumpname, CONFIG *lpcfgMap);
void IterateMapLumpNames(int iWad, void (*fnCallback)(LPCTSTR, void*), void *lpvParam);
int RenameMapInWad(int iWad, LPCSTR sOldName, LPCSTR sNewName);
int MapNumberFromLumpname(LPCSTR sLumpname);
int MapLumpnameFromNumber(int iMapNumber, LPSTR szLumpname);
int GetFirstFreeMapLumpname(WAD *lpwad, LPSTR szLumpname);
int DeleteMap(WAD *lpwad, LPCSTR szLumpname, CONFIG *lpcfgMap);
void DecodeThingTypeNybble(MAP *lpmap);
void EncodeThingTypeNybble(MAP *lpmap);


/* Macros. */
#define SECEFFECT_NYBBLE0(x) ((x) & 0xF)
#define SECEFFECT_NYBBLE1(x) (((x) & 0xF0) >> 4)
#define SECEFFECT_NYBBLE2(x) (((x) & 0xF00) >> 8)
#define SECEFFECT_NYBBLE3(x) (((x) & 0xF000) >> 12)

#define NYBBLE0_TO_SECEFFECTMASK(x) ((x) & 0xF)
#define NYBBLE1_TO_SECEFFECTMASK(x) (((x) & 0xF) << 4)
#define NYBBLE2_TO_SECEFFECTMASK(x) (((x) & 0xF) << 8)
#define NYBBLE3_TO_SECEFFECTMASK(x) (((x) & 0xF) << 12)

#define INVALID_SIDEDEF ((unsigned short)65535)
#define MAPNUMBER_MIN 1
#define MAPNUMBER_MAX 1035

#define MAX_LINEDEFS	65536
#define MAX_SIDEDEFS	65536
#define MAX_SECTORS		65536
#define MAX_VERTICES	65536
#define MAX_THINGS		65536

#define DEFAULT_MAP_LUMPNAME	"MAP01"


/* Linedef flags. */
enum ENUM_LINEDEFFLAGS
{
	LDF_IMPASSABLE		= 1,
	LDF_DOUBLESIDED		= 4,
	LDF_UPPERUNPEGGED	= 8,
	LDF_LOWERUNPEGGED	= 16,
	LDF_BLOCKSOUND		= 64
};


/* Thing flags. */
enum ENUM_THINGFLAGS
{
	TF_DEAF		= 8,
	TF_MULTI	= 16
};


/* Flags for MapLoad. */
enum ENUM_MAPLOADFLAGS
{
	MLF_LOADSECTORS		= 1,
	MLF_LOADLINEDEFS	= 2,
	MLF_LOADSIDEDEFS	= 4,
	MLF_LOADVERTICES	= 8,
	MLF_LOADTHINGS		= 0x10,
	MLF_DECODETHINGS	= 0x20,
	MLF_CLEAN			= 0x40
};

#define MLF_ALL			(MLF_LOADSECTORS | MLF_LOADLINEDEFS | MLF_LOADSIDEDEFS | MLF_LOADVERTICES | MLF_LOADTHINGS | MLF_CLEAN)
#define MLF_PREVIEW		(MLF_LOADLINEDEFS | MLF_LOADVERTICES)


/* Inline functions. */

static __inline BOOL LinedefBelongsToSector(MAP *lpmap, int iLinedefIndex, int iSectorIndex);
static __inline BOOL SidedefExists(MAP *lpmap, unsigned short unSidedef);
#ifdef UNICODE
static __inline BOOL IsMapLumpnameW(LPCWSTR szLumpName);
#endif
static __inline BOOL IsMapLumpnameA(LPCSTR szLumpName);


static __inline BOOL LinedefBelongsToSector(MAP *lpmap, int iLinedefIndex, int iSectorIndex)
{
	int s1 = lpmap->linedefs[iLinedefIndex].s1, s2 = lpmap->linedefs[iLinedefIndex].s2;
	if(s1 != INVALID_SIDEDEF && lpmap->sidedefs[s1].sector == iSectorIndex) return TRUE;
	if(s2 != INVALID_SIDEDEF && lpmap->sidedefs[s2].sector == iSectorIndex) return TRUE;
	return FALSE;
}

static __inline BOOL SidedefExists(MAP *lpmap, unsigned short unSidedef)
{
	return unSidedef != INVALID_SIDEDEF && unSidedef < lpmap->iSidedefs;
}


#ifdef UNICODE

static __inline BOOL IsMapLumpnameW(LPCWSTR szLumpName)
{
	CHAR szLumpNameA[CCH_LUMPNAME + 1];
	WideCharToMultiByte(CP_ACP, 0, szLumpName, -1, szLumpNameA, NUM_ELEMENTS(szLumpNameA), NULL, NULL);
	return IsMapLumpnameA(szLumpNameA);
}

#define IsMapLumpname IsMapLumpnameW

#else

#define IsMapLumpname IsMapLumpnameA

#endif


static __inline BOOL IsMapLumpnameA(LPCSTR szLumpName)
{
	return LikeA(szLumpName, "MAP##") || LikeA(szLumpName, "MAP[A-Z][0-9A-Z]") || LikeA(szLumpName, "E#M#");
}


#endif
