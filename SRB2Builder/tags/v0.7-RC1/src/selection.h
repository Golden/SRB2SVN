/* SRB2 Workbench -- A map editor for Sonic Robo Blast 2.
 * Copyright (C) 2009 Gregor Dick
 * http://workbench.srb2.org/
 *
 * Incorporates portions of Doom Builder, (C) 2003 Pascal vd Heiden.
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 * USA.
 *
 * selection.h: Header for selection.c.
 *
 * AMDG.
 */

#ifndef __SRB2B_SELECTION__
#define __SRB2B_SELECTION__

/* Types. */
typedef struct _SELECTION_LIST
{
	int ciBufferLength;
	int iDataCount;
	int *lpiIndices;
} SELECTION_LIST;

typedef struct _SELECTION
{
	SELECTION_LIST *lpsellistSectors, *lpsellistLinedefs, *lpsellistVertices, *lpsellistThings;
} SELECTION;


/* Function prototypes. */
SELECTION_LIST* AllocateSelectionList(int iInitListSize);
void DestroySelectionList(SELECTION_LIST *lpsellist);
void AddToSelectionList(SELECTION_LIST *lpsellist, int iValue);
void AddToSelectionListWithRepetition(SELECTION_LIST *lpsellist, int iValue);
BOOL RemoveFromSelectionList(SELECTION_LIST *lpsellist, int iValue);
BOOL ExistsInSelectionList(SELECTION_LIST *lpsellist, int iValue);
void SortSelectionList(SELECTION_LIST *lpsellist);

#if 0
SELECTION_LIST* DuplicateSelectionList(SELECTION_LIST* lpsellist);
#endif

/* Inline functions. */

/* ClearSelectionList
 *   Removes all items from a selection list.
 *
 * Parameters:
 *   SELECTION_LIST*	lpsellist	Pointer to selection list to empty.
 *
 * Return value: None.
 */
static __inline void ClearSelectionList(SELECTION_LIST *lpsellist)
{
	lpsellist->iDataCount = 0;
}


#endif
