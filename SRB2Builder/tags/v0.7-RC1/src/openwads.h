/* SRB2 Workbench -- A map editor for Sonic Robo Blast 2.
 * Copyright (C) 2009 Gregor Dick
 * http://workbench.srb2.org/
 *
 * Incorporates portions of Doom Builder, (C) 2003 Pascal vd Heiden.
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 * USA.
 *
 * openwads.h: Header for openwads.c.
 *
 * AMDG.
 */

#ifndef __SRB2B_OPENWADS__
#define __SRB2B_OPENWADS__

#include "wad.h"
#include "config.h"



/* Function prototypes. */
int LoadWad(LPCTSTR szPath);
void EnumChildWindowsByWad(int iID, WNDENUMPROC lpEnumFunc, LPARAM lParam);
int CountWadWindows(int iWadID);
int ReleaseWad(int iID);
void AddWadRef(int iWad);
void InitOpenWadsList(void);
int NewWad(void);
WAD* GetWad(int iWad);
void IncrementWadReferenceCount(int iWad);
int GetWadFilename(int iWad, LPTSTR szFilename, WORD cchBuffer);
int SaveWadAs(int iWad, LPTSTR szFileName, CONFIG *lpcfgChangedMaps);
BOOL WadIsNewByID(int iWad);
int BuildNodesExternal(LPCTSTR szFilename, LPCTSTR szLumpname);
BOOL BuildNodesExternalFromConfig(CONFIG *lpcfgNode, void *lpvFilename);
int SetWadNonMapModified(int iWad);
void DeleteMapFromOpenWad(int iWad, LPCSTR szLumpname);
CONFIG* GetWadOptions(int iWad);
BOOL CloseWad(int iWad, void *lpv);
int GetWadTitle(int iWad, LPTSTR szBuffer, WORD cchBuffer);
BOOL EnumOpenWads(BOOL (*lpfn)(int iWad, void* lpvParam), void *lpvParam);
void AddAllUserWadsRef(void);
void ReleaseAllUserWads(void);



/* Inline functions. */
static __inline int SaveWad(int iWad, CONFIG *lpcfgChangedMaps)
{
	return SaveWadAs(iWad, NULL, lpcfgChangedMaps);
}

#endif	/* __SRB2B_OPENWADS__ */
