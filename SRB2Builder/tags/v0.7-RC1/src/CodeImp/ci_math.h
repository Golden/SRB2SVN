/* SRB2 Workbench -- A map editor for Sonic Robo Blast 2.
 * Copyright (C) 2009 Gregor Dick
 * http://workbench.srb2.org/
 *
 * Incorporates portions of Doom Builder, (C) 2003 Pascal vd Heiden.
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 * USA.
 *
 * ci_math.h: Mathematical routines. Adapted from Doom Builder.
 *
 * AMDG.
 */

#include <windows.h>

// Include math library
#include <math.h>

// Definitions
#define MAXQUADRANT			32767
#define MAXQUADRANTf		32767.0f
#define ENDLESS_DISTANCE	2147483640			// Max int
#define SMALL_FLOAT			0.00000001f
#define PI					3.141592653589793f



// distancei: Returns the distance between two points
//----------------------------------------------------------------------------
static __inline float distancei(const int x1, const int y1, const int x2, const int y2)
{
	int x3 = x2 - x1;
	int y3 = y2 - y1;

	if((x3 > MAXQUADRANT) || (y3 > MAXQUADRANT) ||
	   (x3 < -MAXQUADRANT) || (y3 < -MAXQUADRANT))
		return (float)sqrt((double)x3 * (double)x3 + (double)y3 * (double)y3);
	else
		return (float)sqrt((float)x3 * (float)x3 + (float)y3 * (float)y3);
}


// distancef: Returns the distance between two points
//----------------------------------------------------------------------------
static __inline float distancef(const float x1, const float y1, const float x2, const float y2)
{
	float x3 = x2 - x1;
	float y3 = y2 - y1;

	if((x3 > MAXQUADRANTf) || (y3 > MAXQUADRANTf) ||
	   (x3 < -MAXQUADRANTf) || (y3 < -MAXQUADRANTf))
		return (float)sqrt((double)x3 * (double)x3 + (double)y3 * (double)y3);
	else
		return (float)sqrt(x3 * x3 + y3 * y3);
}


// PtInFRect: Checks if a point intersects a rectangle
//----------------------------------------------------------------------------
static __inline BOOL PtInFRect(float x, float y, FRECT *lpfrc)
{
	return (x >= lpfrc->left) && (x <= lpfrc->right) && (y >= lpfrc->top) && (y <= lpfrc->bottom);
}


// side_of_line: Calculates on which side of the line a vertex is
// returns < 0 for front (right) side, > 0 for back (left) side and 0 for on the line
//----------------------------------------------------------------------------
static __inline float side_of_lineii(int x1, int y1, int x2, int y2, int vx, int vy)
{
	return (float)(vy - y1) * (float)(x2 - x1) - (float)(vx - x1) * (float)(y2 - y1);
}


// side_of_line: Calculates on which side of the line a vertex is
// returns < 0 for front (right) side, > 0 for back (left) side and 0 for on the line
//----------------------------------------------------------------------------
static __inline float side_of_lineff(float x1, float y1, float x2, float y2, float vx, float vy)
{
	return (vy - y1) * (x2 - x1) - (vx - x1) * (y2 - y1);
}


// point_near_line: Returns TRUE when a point is in the bounding box of a line
// Usefull for optimization nefore distance_to_line when the max distance is known
//----------------------------------------------------------------------------
static __inline int point_near_line(float x1, float y1, float x2, float y2, float vx, float vy, int maxdistance)
{
	float rleft, rright, rtop, rbottom;

	// Create rect coordinates
	if(x1 > x2)
	{
		rleft = x2 - (float)maxdistance;
		rright = x1 + (float)maxdistance;
	}
	else
	{
		rleft = x1 - (float)maxdistance;
		rright = x2 + (float)maxdistance;
	}
	if(y1 > y2)
	{
		rtop = y2 - (float)maxdistance;
		rbottom = y1 + (float)maxdistance;
	}
	else
	{
		rtop = y1 - (float)maxdistance;
		rbottom = y2 + (float)maxdistance;
	}

	// Return TRUE when point is in rect
	return (vx >= rleft) && (vx <= rright) && (vy >= rtop) && (vy <= rbottom);
}


// nearest_point_on_line: Finds the point on a line closest to another point
//----------------------------------------------------------------------------
static __inline void nearest_point_on_line(float x1, float y1, float x2, float y2, float vx, float vy, float *lpix, float *lpiy)
{
	float u;

	// Get length of linedef
	float ld = distancef(x1, y1, x2, y2);

	// Check if not zero length
	if(fabs(ld) > SMALL_FLOAT)
	{
		float lbound, ubound;
		// Get the intersection offset
		u = ((vx - x1) * (x2 - x1) + (vy - y1) * (y2 - y1)) / (ld * ld);

		// Limit intersection offset to the line
		lbound = 1 / ld;
		ubound = 1 - lbound;
		if(u < lbound) u = lbound;
		if(u > ubound) u = ubound;
	}
	else
	{
		u = 0.0f;
	}

	// Calculate intersection point
	*lpix = x1 + u * (x2 - x1);
	*lpiy = y1 + u * (y2 - y1);
}


// distance_to_line: Calculates the shortest distance between a vertex and a line
//----------------------------------------------------------------------------
static __inline float distance_to_line(float x1, float y1, float x2, float y2, float vx, float vy)
{
	float ix, iy;

	// Find the nearest point on the line.
	nearest_point_on_line(x1, y1, x2, y2, vx, vy, &ix, &iy);

	// Return distance between intersection and point
	// which is the shortest distance to the line
	return distancef(vx, vy, ix, iy);
}


// DistanceToLinedef: Calculates the shortest distance between a point and a
// linedef
//----------------------------------------------------------------------------
static __inline float DistanceToLinedef(MAP *lpmap, int iLinedef, float x, float y)
{
	MAPLINEDEF *lpld = &lpmap->linedefs[iLinedef];
	MAPVERTEX *lpvx1 = &lpmap->vertices[lpld->v1];
	MAPVERTEX *lpvx2 = &lpmap->vertices[lpld->v2];

	return distance_to_line(lpvx1->x, lpvx1->y, lpvx2->x, lpvx2->y, x, y);
}
