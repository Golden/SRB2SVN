/* SRB2 Workbench -- A map editor for Sonic Robo Blast 2.
 * Copyright (C) 2009 Gregor Dick
 * http://workbench.srb2.org/
 *
 * Incorporates portions of Doom Builder, (C) 2003 Pascal vd Heiden.
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 * USA.
 *
 * mapconfig.h: Header for mapconfig.c.
 *
 * AMDG.
 */

#ifndef __SRB2B_MAPCONFIG__
#define __SRB2B_MAPCONFIG__

#include <windows.h>

#include "config.h"
#include "map.h"


#define MAPCFG_GAME			TEXT("game")
#define MAPCFG_ID			TEXT("id")

#define FLAT_THING_SECTION	TEXT("__things")
#define FLAT_LINE_SECTION	TEXT("__ldtypesflat")

#define MAPCFG_WIKI			TEXT("wiki")
#define WIKI_LDEFFECT		TEXT("ldeffect")
#define WIKI_SECEFFECT		TEXT("seceffect")
#define WIKI_THING			TEXT("thing")

#define GAMECFG_IWAD		TEXT("iwad")
#define GAMECFG_BINARY		TEXT("binary")

#define MAPCFG_HEADERFLAGS	TEXT("headerflags")
#define MAPCFG_TOL			TEXT("leveltypes")
#define MAPCFG_WEATHER		TEXT("weather")

#define MAPCFG_FOFS			TEXT("fofs")
#define MAPCFG_FOF_TYPES	TEXT("specials")
#define MAPCFG_FOF_FLAGS	TEXT("flags")

#define MAPCFG_THINGEFFECTNYBBLE	TEXT("thingeffectnybble")

#define MAPCFG_NIGHTS		TEXT("nights")
#define MAPCFG_NIGHTS_AT	TEXT("axistransfer")
#define MAPCFG_NIGHTS_ATL	TEXT("axistransferline")


typedef enum _ENUM_ADDMAPCONFIG
{
	AMC_COMBOBOX,
	AMC_LISTBOX
} ENUM_ADDMAPCONFIG;


int LoadMapConfigs(void);
void UnloadMapConfigs(void);
void AddMapConfigsToList(HWND hwnd, ENUM_ADDMAPCONFIG amcListType);
CONFIG* GetThingConfigInfo(CONFIG *lpcfgThings, unsigned short unType);
void GetThingTypeDisplayText(unsigned short unType, CONFIG *lpcfgThings, LPTSTR szBuffer, unsigned short cchBuffer);
void GetThingDirectionDisplayText(short nDirection, LPTSTR szBuffer, unsigned short cbBuffer);
void GetEffectDisplayText(unsigned short unEffect, CONFIG *lpcfgSectors, LPTSTR szBuffer, unsigned short cchBuffer, BOOL bNybble);
void SetAllThingPropertiesFromType(MAP *lpmap, CONFIG *lpcfgFlatThings);
WORD GetZFactor(CONFIG *lpcfgFlatThings, unsigned short unType);
void SetThingPropertiesFromType(MAP *lpmap, int iThing, CONFIG *lpcfgFlatThings);
int GetIWadForConfig(CONFIG *lpcfgMap);
CONFIG *GetDefaultMapConfig(void);
CONFIG* GetMapConfigByID(LPCTSTR szMapConfigID);
int BrowseForIWAD(LPTSTR szFilename, DWORD cchFilename);
int BrowseForBinary(LPTSTR szFilename, DWORD cchFilename);
void SetMapConfigDirectory(LPCTSTR szDir);
void FreeMapConfigDirectoryName(void);

#endif
