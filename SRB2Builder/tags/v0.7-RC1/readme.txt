SRB2 Workbench 0.7 RC1 - README.TXT
http://workbench.srb2.org/
oogaland_NO@SPAM_gmail.com
---------------------------------------------------

This is a RELEASE CANDIDATE. As far as I'm concerned, it's ready to go, but this
is the final round of testing out in the wild. Please report any bugs to me.
Thanks!

SRB2 Workbench is a map editor for Sonic Robo Blast 2 <http://www.srb2.org/>.
This is version 0.7, which is pretty much complete, with the notable exception
of a 3D editing mode.


System Requirements
-------------------

Supported operating systems: Windows XP, Windows Server 2003, Windows Vista.
It should also work fine on Windows 2000 and NT 4.0, although I haven't tested
those. The supplied binary is Unicode, but users of Windows 98 and ME could try
building their own (untested) ANSI binaries -- see 'Source Code' below.

I can't really put a figure on RAM, disk space and CPU requirements, but suffice
it to say that they're pretty modest.


User Guide
----------

No formal user guide exists yet. I plan to post some tips and tutorials at
http://workbench.srb2.org/ over time, however. Check out the SRB2 Wiki at
http://wiki.srb2.org/, too.


Source Code
-----------

Source code is available in an SVN repository at
<svn://code.srb2.org/SRB2Builder>. A workspace for Visual C++ 10, a Dev-C++
4.9.9.2 project and a Makefile for GNU make with GCC (MinGW) are provided. A
reasonably up-to-date version of the Win32 Platform SDK is required. A VC6
workspace is also included, but the version of the Platform SDK needed does
not support VC6.


Contributors
------------

SRB2 Workbench was written by Gregor Dick. Enormous thanks are due to Rob
Tisdell for testing and suggestions. It owes a large part of its design, and a
bit of its code, to Pascal vd Heiden's Doom Builder. It also incorporates a
window-docking library by James Brown and Jeff Glatt, and a bug-reporting
library by Maksim Pyatkovskiy. Thanks also to Alam Arias for writing a Makefile
and fixing GCC warnings. Some toolbar icons have been taken or adapted from Mark
James' (http://www.famfamfam.com/) "Silk" and "Mini" icon sets; some others are
by "Nev3r"; and the document icon was adapted from one of "kearone's" GNOME
icons.

ZenNode, which is packaged with Workbench's installer, is copyright (C) 2004
Mark Rousseau.


Licence
-------

SRB2 Workbench is licensed under version 2 of the GNU General Public License.
See the file named COPYING for details.
