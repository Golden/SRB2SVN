/* SRB2 Workbench -- A map editor for Sonic Robo Blast 2.
 * Copyright (C) 2009 Gregor Dick
 * http://workbench.srb2.org/
 *
 * Incorporates portions of Doom Builder, (C) 2003 Pascal vd Heiden.
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 * USA.
 *
 * BugTrap_FnTypes.h: Some typedefs for interfacing with BugTrap.
 *
 * AMDG.
 */

#ifndef __SRB2B_BUGTRAP_FNTYPES__
#define __SRB2B_BUGTRAP_FNTYPES__

typedef BOOL (APIENTRY *BT_READVERSIONINFO)(HMODULE hModule);
typedef void (APIENTRY *BT_SETSUPPORTURL)(LPCTSTR pszSupportURL);
typedef void (APIENTRY *BT_SETSUPPORTEMAIL)(LPCTSTR pszSupportEMail);
typedef void (APIENTRY *BT_SETFLAGS)(DWORD dwFlags);
typedef void (APIENTRY *BT_SETAPPNAME)(LPCTSTR pszAppName);
typedef void (APIENTRY *BT_SETAPPVERSION)(LPCTSTR pszAppVersion);

#endif
