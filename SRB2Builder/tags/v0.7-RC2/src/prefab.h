/* SRB2 Workbench -- A map editor for Sonic Robo Blast 2.
 * Copyright (C) 2009 Gregor Dick
 * http://workbench.srb2.org/
 *
 * Incorporates portions of Doom Builder, (C) 2003 Pascal vd Heiden.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 * USA.
 *
 * prefab.h: Header for prefab.c.
 *
 * AMDG.
 */

#ifndef __SRB2B_PREFAB__
#define __SRB2B_PREFAB__

#include "map.h"


#define FreePrefab(lpprefab) ProcHeapFree(lpprefab)

typedef struct _PREFAB PREFAB;

PREFAB* CreatePrefabFromSelection(MAP *lpmap);
void InsertPrefab(MAP *lpmap, PREFAB *lpprefab);
void RegisterPrefabClipboardFormat(void);
BOOL ClipboardContainsPrefab(void);
int CopyPrefabToClipboard(PREFAB *lpprefab);
PREFAB* PastePrefabFromClipboard(void);

#endif
