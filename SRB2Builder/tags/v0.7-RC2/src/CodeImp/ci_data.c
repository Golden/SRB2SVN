/* SRB2 Workbench -- A map editor for Sonic Robo Blast 2.
 * Copyright (C) 2009 Gregor Dick
 * http://workbench.srb2.org/
 *
 * Incorporates portions of Doom Builder, (C) 2003 Pascal vd Heiden.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 * USA.
 *
 * ci_data.c: Assorted map routines, mostly of a geometrical flavour. Adapted
 * from Doom Builder.
 *
 * AMDG.
 */

// Definitions
#define WIN32_LEAN_AND_MEAN

// Includes
#include <windows.h>
#include <stdlib.h>

#include "../general.h"
#include "../maptypes.h"
#include "../editing.h"
#include "../renderer.h"

#include "ci_math.h"
#include "ci_data_proto.h"


static __inline int InRect(float x1rect, float y1rect, float x2rect, float y2rect, float x, float y);


// ResetSelections: Sets all selected properties to 0
//-----------------------------------------------------------------------------
void ResetSelections(MAPTHING* things, int numthings, MAPLINEDEF* linedefs, int numlinedefs,
							  MAPVERTEX* vertices, int numvertices, MAPSECTOR* sectors, int numsectors)
{
	int i;

	// Reset all selected properties
	if(numthings) for(i = 0; i < numthings; i++) things[i].selected = 0;
	if(numlinedefs) for(i = 0; i < numlinedefs; i++) linedefs[i].selected = 0;
	if(numvertices) for(i = 0; i < numvertices; i++) vertices[i].selected = 0;
	if(numsectors) for(i = 0; i < numsectors; i++) sectors[i].selected = 0;
}


// NearestConditionedVertex: Returns the nearest vertex index that satisfies a
// given condition.
//----------------------------------------------------------------------------
int NearestConditionedVertex(MAP *lpmap, MAPOBJGRID *lpmog, int x, int y, int *lpiDist, BOOL (*fnCondition)(MAP *lpmap, int iVertex, void *lpvParam), void *lpvParam)
{
	int iNearest = -1;
	int iMatchDistance = INT_MAX;

	if(lpmog)
	{
		RECT rcBound = {x, y, x, y};
		iNearest = FindNearestObjectInMOG(lpmap, lpmog, &rcBound, PointToCellMetric, NULL, NearestConditionedVertexInCell, fnCondition, lpvParam, FNF_OBJISRECT);
		if(iNearest >= 0)
			iMatchDistance = (int)distancei(lpmap->vertices[iNearest].x, lpmap->vertices[iNearest].y, x, y);
	}
	else
	{
		int iVertex;

		/* Not using a MOG, so check all vertices. */
		for(iVertex = 0; iVertex < lpmap->iVertices; iVertex++)
		{
			if(!fnCondition || fnCondition(lpmap, iVertex, lpvParam))
			{
				int iNewDistance = (int)distancei(lpmap->vertices[iVertex].x, lpmap->vertices[iVertex].y, x, y);

				/* Are we nearer? */
				if(iNewDistance < iMatchDistance)
				{
					iNearest = iVertex;
					iMatchDistance = iNewDistance;
				}
			}
		}
	}

	// Return result
	*lpiDist = iMatchDistance;
	return iNearest;
}




// NearestOtherVertex: Returns the vertex index nearest to given vertex index
//----------------------------------------------------------------------------
int NearestOtherVertex(MAP *lpmap, int vx, int *lpiDist)
{
	int foundvertex = -1;
	int founddistance = INT_MAX;
	int d, v;
	int x = (int)lpmap->vertices[vx].x;
	int y = (int)lpmap->vertices[vx].y;

	// Go for all vertices before vx
	for(v = 0; v < vx; v++)
	{
		// Calculate distance
		d = (int)distancei(lpmap->vertices[v].x, lpmap->vertices[v].y, x, y);

		// Check if closer
		if(d < founddistance)
		{
			// Found a closer match
			foundvertex = v;
			founddistance = d;
		}
	}

	// Go for all vertices after vx
	for(v = vx + 1; v < lpmap->iVertices; v++)
	{
		// Calculate distance
		d = (int)distancei(lpmap->vertices[v].x, lpmap->vertices[v].y, x, y);

		// Check if closer
		if(d < founddistance)
		{
			// Found a closer match
			foundvertex = v;
			founddistance = d;
		}
	}

	// Return result
	*lpiDist = founddistance;
	return foundvertex;
}


// NearestConditionedVertexToLinedef: Returns the index of the vertex nearest to
// a linedef, where the vertex satisfies a given condition.
//----------------------------------------------------------------------------
int NearestConditionedVertexToLinedef(MAP *lpmap, MAPOBJGRID *lpmog, int iLinedef, int *lpiDist, BOOL (*fnCondition)(MAP *lpmap, int iIndex, void *lpvParam), void *lpvParam)
{
	int iNearest = -1;
	int iMatchDistance = INT_MAX;

	if(lpmog)
	{
		iNearest = FindNearestObjectInMOG(lpmap, lpmog, &iLinedef, LinedefToCellMetric, GetLinedefBounds, NearestConditionedVertexInCellToLinedef, fnCondition, lpvParam, 0);
		if(iNearest >= 0)
		{
			MAPVERTEX *lpvxNearest = &lpmap->vertices[iNearest];
			iMatchDistance = (int)DistanceToLinedef(lpmap, iLinedef, (float)lpvxNearest->x, (float)lpvxNearest->y);
		}
	}
	else
	{
		int d, v;

		// Go for all vertices
		for(v = 0; v < lpmap->iVertices; v++)
		{
			if(v != lpmap->linedefs[iLinedef].v1 &&
				v != lpmap->linedefs[iLinedef].v2 &&
				fnCondition(lpmap, v, lpvParam))
			{
				// Calculate distance
				d = (int)DistanceToLinedef(lpmap, iLinedef, (float)lpmap->vertices[v].x, (float)lpmap->vertices[v].y);

				// Check if closer
				if(d < iMatchDistance)
				{
					// Found a closer match
					iNearest = v;
					iMatchDistance = d;
				}
			}
		}
	}

	// Return result
	*lpiDist = iMatchDistance;
	return iNearest;
}



// LinedefIntersectingSegment: Returns the index of a linedef intersecting the
// specified line segment, with the definition of 'intersecting' admitting
// coincidence according to bAdmitCoincidence.
//----------------------------------------------------------------------------
int LinedefIntersectingSegment(MAP* lpmap, float x1, float y1, float x2, float y2, BOOL bAdmitCoincidence)
{
	// To ensure that they aren't congruent.
	int iLinedef;

	float m = 0, M = 0;		// Gradients.
	float numadd = 0;		// Add to numerator.
	float x, y;				// Point of intersection.
	float X1, Y1, X2, Y2;	// Co-ord of linedef ld's vertices.

	if(x1 != x2)
	{
		m = (y2 - y1) / (x2 - x1);
		numadd = x1 * m - y1;
	}

	// Go for all linedefs.
	for(iLinedef = 0; iLinedef < lpmap->iLinedefs; iLinedef++)
	{
		// Calculate gradient of this ld.
		MAPLINEDEF *lpld = &lpmap->linedefs[iLinedef];
		X1 = lpmap->vertices[lpld->v1].x;
		Y1 = lpmap->vertices[lpld->v1].y;
		X2 = lpmap->vertices[lpld->v2].x;
		Y2 = lpmap->vertices[lpld->v2].y;

		if(X2 != X1) M = (Y2 - Y1) / (X2 - X1);

		if(M != m || x1 == x2 || X1 == X2)
		{
			if(X1 != X2 && x1 != x2)
			{
				x = (numadd + Y1 - X1 * M) / (m-M);
				y = m * x - numadd;
			}
			else if(x1 == x2 && X1 == X2)	// Both vertical.
			{
				// They definitely don't intersect a point, but they might be
				// coincident.
				if(bAdmitCoincidence && x1 == X1 &&
					(
						(y1 - Y1) * (y1 - Y2) <= 0 || (y2 - Y1) * (y2 - Y2) <= 0 ||
						(Y1 - y1) * (Y1 - y2) <= 0 || (Y2 - y1) * (Y2 - y2) <= 0
					))
					return iLinedef;

				continue;
			}
			else if(x1 == x2)				// X1 != X2
			{
				x = x1;
				y = M * (x - X1) + Y1;
			}
			else							// X1 == X2 && x1 != x2
			{
				x = X1;
				y = m * x - numadd;
			}
		}
		else		// Parallel or co-incident, and neither vertical.
		{
			// They definitely don't intersect at a point, but they might be
			// coincident.
			if(bAdmitCoincidence && y1 - m * x1 == Y1 - M * X1 &&
				(
					(y1 - Y1) * (y1 - Y2) <= 0 || (y2 - Y1) * (y2 - Y2) <= 0 ||
					(Y1 - y1) * (Y1 - y2) <= 0 || (Y2 - y1) * (Y2 - y2) <= 0
				)
				&&
				(
					(x1 - X1) * (x1 - X2) <= 0 || (x2 - X1) * (x2 - X2) <= 0 ||
					(X1 - x1) * (X1 - x2) <= 0 || (X2 - x1) * (X2 - x2) <= 0
				))
				return iLinedef;

			continue;
		}


		// Check if in range (hence crossing)
		if(InRect(x1, y1, x2, y2, x, y) && InRect(X1, Y1, X2, Y2, x, y))
		{
			// this is not the same linedef as the source,
			// so this is a crossed linedef
			return iLinedef;
		}
	}

	// Return -1 (nothing found)
	return -1;
}

static __inline int InRect(float x1rect, float y1rect, float x2rect, float y2rect, float x, float y)
{
	int x1 = (int)(x1rect * 10 + 0.5);
	int y1 = (int)(y1rect * 10 + 0.5);
	int x2 = (int)(x2rect * 10 + 0.5);
	int y2 = (int)(y2rect * 10 + 0.5);
	int t;
	int X=(int)(x*10 + 0.5), Y=(int)(y*10 + 0.5);

	if(x1 > x2) {t=x1; x1=x2; x2=t;}
	if(y1 > y2) {t=y1; y1=y2; y2=t;}

	return (X >= x1 && X <= x2 && Y > y1 && Y < y2) || (X > x1 && X < x2 && Y >= y1 && Y <= y2);
}


// NearestThing: Returns the nearest thing index
//----------------------------------------------------------------------------
int NearestThing(MAP *lpmap, int x, int y, int *lpiDist)
{
	int iNearestThing = -1;
	int iNearestDistance = INT_MAX;
	int iThing;

	// Go for all things
	for(iThing = 0; iThing < lpmap->iThings; iThing++)
	{
		MAPTHING *lpthing = &lpmap->things[iThing];

		// Calculate distance
		int iDistance = (int)distancei(lpthing->x, lpthing->y, x, y);

		// Check if closer
		if(iDistance < iNearestDistance)
		{
			// Found a closer match
			iNearestThing = iThing;
			iNearestDistance = iDistance;
		}
	}

	// Return result
	*lpiDist = iNearestDistance;
	return iNearestThing;
}



// NearestLinedef: Returns the nearest linedef index
//----------------------------------------------------------------------------
int __fastcall NearestLinedef(MAP *lpmap, MAPOBJGRID *lpmog, int x, int y, int *lpiDist)
{
	return NearestConditionedLinedef(lpmap, lpmog, x, y, lpiDist, NULL, NULL);
}

// Floating-point version of the above.
static __inline int NearestLinedefFloat(float x, float y, MAP *lpmap, int *lpiDist)
{
	int foundlinedef = -1;
	float founddistance = 1000000;
	float d;
	int l;

	// Go for all linedefs
	for(l = 0; l < lpmap->iLinedefs; l++)
	{
		// Get shortest distance to linedef
		d = DistanceToLinedef(lpmap, l, x, y);

		// Check if closer but 'within' range
		if(d < founddistance)
		{
			// Found a closer match
			foundlinedef = l;
			founddistance = d;
		}
	}

	// Return result
	*lpiDist = (int)founddistance;
	return foundlinedef;
}


// NearestConditionedLinedef: Returns the nearest linedef index satisfying a
// particular condition.
//----------------------------------------------------------------------------
int NearestConditionedLinedef(MAP *lpmap, MAPOBJGRID *lpmog, int x, int y, int *lpiDist, BOOL (*fnCondition)(MAP *lpmap, int iLinedef, void *lpvParam), void *lpvParam)
{
	int iNearest = -1;
	float fMatchDistance = 1000000;
	float fx = (float)x, fy = (float)y;
	float d;
	int l;

	if(lpmog)
	{
		RECT rcBound = {x, y, x, y};
		iNearest = FindNearestObjectInMOG(lpmap, lpmog, &rcBound, PointToCellMetric, NULL, NearestConditionedLinedefInCell, fnCondition, lpvParam, FNF_OBJISRECT);
		if(iNearest >= 0)
			fMatchDistance = DistanceToLinedef(lpmap, iNearest, fx, fy);
	}
	else
	{
		// Go for all linedefs
		for(l = 0; l < lpmap->iLinedefs; l++)
		{
			// Check if satisfies condition
			if(!fnCondition || fnCondition(lpmap, l, lpvParam))
			{
				// Get linedef vertices
				MAPVERTEX* v1 = &lpmap->vertices[lpmap->linedefs[l].v1];
				MAPVERTEX* v2 = &lpmap->vertices[lpmap->linedefs[l].v2];

				// Check if point is near the line
				if(point_near_line(v1->x, v1->y, v2->x, v2->y, fx, fy, ENDLESS_DISTANCE))
				{
					// Get shortest distance to linedef
					d = distance_to_line(v1->x, v1->y, v2->x, v2->y, fx, fy);

					// Check if closer but 'within' range
					if(d < fMatchDistance)
					{
						// Found a closer match
						iNearest = l;
						fMatchDistance = d;
					}
				}
			}
		}
	}

	// Return result
	*lpiDist = (int)fMatchDistance;
	return iNearest;
}

// Flaoting-point version of the above.
static int NearestConditionedLinedefFloat(float x, float y, MAP *lpmap, int *lpiDist, BOOL (*fnCondition)(MAP *lpmap, int iLinedef, void *lpvParam), void *lpvParam)
{
	int foundlinedef = -1;
	float founddistance = 1000000;
	float d;
	int l;

	// Go for all linedefs
	for(l = 0; l < lpmap->iLinedefs; l++)
	{
		// Check if satisfies condition
		if(!fnCondition || fnCondition(lpmap, l, lpvParam))
		{
			// Get linedef vertices
			MAPVERTEX* v1 = &lpmap->vertices[lpmap->linedefs[l].v1];
			MAPVERTEX* v2 = &lpmap->vertices[lpmap->linedefs[l].v2];

			// Check if point is near the line
			if(point_near_line(v1->x, v1->y, v2->x, v2->y, x, y, ENDLESS_DISTANCE))
			{
				// Get shortest distance to linedef
				d = distance_to_line(v1->x, v1->y, v2->x, v2->y, x, y);

				// Check if closer but 'within' range
				if(d < founddistance)
				{
					// Found a closer match
					foundlinedef = l;
					founddistance = d;
				}
			}
		}
	}

	// Return result
	*lpiDist = (int)founddistance;
	return foundlinedef;
}


// LinedefNoInvalidSD: Returns whether a linedef has valid sidedefs.
//----------------------------------------------------------------------------
BOOL LinedefNoInvalidSD(MAP *lpmap, int iLinedef, void *lpvParam)
{
	UNREFERENCED_PARAMETER(lpvParam);
	return !(lpmap->linedefs[iLinedef].editflags & LEF_INVALIDSIDEDEFS);
}


// LinedefUnlabelled: Returns whether a linedef is unlabelled.
//----------------------------------------------------------------------------
BOOL LinedefUnlabelled(MAP *lpmap, int iLinedef, void *lpvParam)
{
	UNREFERENCED_PARAMETER(lpvParam);
	return !(lpmap->linedefs[iLinedef].editflags & LEF_LABELLED);
}


// LinedefNotAttachedToVertex: Returns whether a linedef is attached to a
// given vertex.
//----------------------------------------------------------------------------
BOOL LinedefNotAttachedToVertex(MAP *lpmap, int iLinedef, void *lpvParam)
{
	MAPLINEDEF *lpld = &lpmap->linedefs[iLinedef];
	return (lpld->v1 != (int)lpvParam && lpld->v2 != (int)lpvParam);
}


// VertexUnlabelled: Returns whether a vertex is unlabelled.
//----------------------------------------------------------------------------
BOOL VertexUnlabelled(MAP *lpmap, int iVertex, void *lpvParam)
{
	UNREFERENCED_PARAMETER(lpvParam);
	return !(lpmap->vertices[iVertex].editflags & VEF_LABELLED);
}

// VertexUnlabelledAndNotAttToLine: Returns whether a vertex is unlabelled and
// not attached to the specified line.
//----------------------------------------------------------------------------
BOOL VertexUnlabelledAndNotAttToLine(MAP *lpmap, int iVertex, void *lpviLinedef)
{
	return VertexUnlabelled(lpmap, iVertex, NULL) && LinedefNotAttachedToVertex(lpmap, (int)lpviLinedef, (void*)iVertex);
}


// VertexNotDragging: Returns whether a vertex is NOT being dragged.
//----------------------------------------------------------------------------
BOOL VertexNotDragging(MAP *lpmap, int iVertex, void *lpvParam)
{
	UNREFERENCED_PARAMETER(lpvParam);
	return !(lpmap->vertices[iVertex].editflags & VEF_DRAGGING);
}


// VertexOnSegment: Returns whether a vertex lies on a given line segment.
//----------------------------------------------------------------------------
BOOL VertexOnSegment(MAP *lpmap, int iVertex, void *lpvLineSegment)
{
	LINESEGMENT *lplinesegment = (LINESEGMENT*)lpvLineSegment;
	MAPVERTEX *lpvx = &lpmap->vertices[iVertex];

	/* Firstly, is it in the rectangle? */
	if(InRect(lplinesegment->x1, lplinesegment->y1, lplinesegment->x2, lplinesegment->y2, lpvx->x, lpvx->y))
	{
		/* Check the vertical case first. */
		if(lplinesegment->x1 == lplinesegment->x2)
			return TRUE;	/* We already know we're in range. */

		/* Iff the gradients match, we're on the segment. */
		return (float)(lplinesegment->y2 - lplinesegment->y1) / (lplinesegment->x2 - lplinesegment->x1) ==
			(float)(lpvx->y - lplinesegment->y1) / (lpvx->x - lplinesegment->x1);
	}

	return FALSE;
}



// IntersectSector: Returns the intersecting sector index
//----------------------------------------------------------------------------
int IntersectSector(int x, int y, MAP *lpmap, BOOL (*fnCondition)(MAP *lpmap, int iLinedef, void *lpvParam), void *lpvParam)
{
	int nld;

	// Perform additional check if necessary
	if(fnCondition)
	{
		// Get the nearest linedef with valid sidedefs
		int lddist = 0;
		nld = NearestConditionedLinedef(lpmap, NULL, x, y, &lddist, fnCondition, lpvParam);
	}
	else
	{
		// Lets get the nearest linedef
		int lddist = 0;
		nld = NearestLinedef(lpmap, NULL, x, y, &lddist);
	}

	// Only continue if any linedefs found
	if(nld > -1)
	{
		// Get the vertices
		MAPVERTEX* v1 = &lpmap->vertices[lpmap->linedefs[nld].v1];
		MAPVERTEX* v2 = &lpmap->vertices[lpmap->linedefs[nld].v2];

		// Check the side of the line
		if(side_of_lineii((int)v1->x, (int)v1->y, (int)v2->x, (int)v2->y, x, y) < 0)
		{
			// Front side (sidedef 1)
			// Return the referenced sector number if a sidedef is referenced
			if(SidedefExists(lpmap, lpmap->linedefs[nld].s1))
				return lpmap->sidedefs[lpmap->linedefs[nld].s1].sector;
			else return -1;
		}
		else
		{
			// Back side (sidedef 2)
			// Return the referenced sector number if a sidedef is referenced
			if(SidedefExists(lpmap, lpmap->linedefs[nld].s2))
				return lpmap->sidedefs[lpmap->linedefs[nld].s2].sector;
			else return -1;
		}
	}
	else
	{
		// Nothing intersected
		return -1;
	}
}

// IntersectSectorFloat: Returns the intersecting sector index from a floating
//                       point co-ordinate. For lovers of one-fracunit-wide
//                       sectors.
//----------------------------------------------------------------------------
int IntersectSectorFloat(float x, float y, MAP *lpmap, BOOL (*fnCondition)(MAP *lpmap, int iLinedef, void *lpvParam), void *lpvParam)
{
	int nld;

	// Perform additional check if necessary
	if(fnCondition)
	{
		// Get the nearest linedef with valid sidedefs
		int lddist = 0;
		nld = NearestConditionedLinedefFloat(x, y, lpmap, &lddist, fnCondition, lpvParam);
	}
	else
	{
		// Let's get the nearest linedef
		int lddist = 0;
		nld = NearestLinedefFloat(x, y, lpmap, &lddist);
	}

	// Only continue if any linedefs found
	if(nld > -1)
	{
		// Get the vertices
		MAPVERTEX* v1 = &lpmap->vertices[lpmap->linedefs[nld].v1];
		MAPVERTEX* v2 = &lpmap->vertices[lpmap->linedefs[nld].v2];

		// Check the side of the line
		if(side_of_lineii((int)v1->x, (int)v1->y, (int)v2->x, (int)v2->y, (int)x, (int)y) < 0)
		{
			// Front side (sidedef 1)
			// Return the referenced sector number if a sidedef is referenced
			if(SidedefExists(lpmap, lpmap->linedefs[nld].s1))
				return lpmap->sidedefs[lpmap->linedefs[nld].s1].sector;
			else return -1;
		}
		else
		{
			// Back side (sidedef 2)
			// Return the referenced sector number if a sidedef is referenced
			if(SidedefExists(lpmap, lpmap->linedefs[nld].s2))
				return lpmap->sidedefs[lpmap->linedefs[nld].s2].sector;
			else return -1;
		}
	}
	else
	{
		// Nothing intersected
		return -1;
	}
}


// pointinsidedefs: This tests if a point lies within or on the edge of
// a polygon of sidedefs
//----------------------------------------------------------------------------
BOOL PointInSidedefs(MAP *lpmap, float x, float y, int* sideslist, int numsides, int *lpiSidesToLines)
{
	int *lpiLines = ProcHeapAlloc(numsides * sizeof(int));
	int i;
	BOOL bRet;

	for(i = 0; i < numsides; i++)
		lpiLines[i] = lpiSidesToLines[sideslist[i]];

	bRet = PointInLinedefs(lpmap, x, y, lpiLines, numsides);

	ProcHeapFree(lpiLines);

	return bRet;
}

BOOL PointInLinedefs(MAP *lpmap, float x, float y, int* lineslist, int numlines)
{
	int inside = 0;
	int i;

	// Go for all linedefs
	for(i = 0; i < numlines; i++)
	{
		// Get the line vertices
		int v1 = lpmap->linedefs[lineslist[i]].v1;
		int v2 = lpmap->linedefs[lineslist[i]].v2;
		int x1, y1, x2, y2;

		// Get coordinates
		x1 = lpmap->vertices[v1].x;
		y1 = lpmap->vertices[v1].y;
		x2 = lpmap->vertices[v2].x;
		y2 = lpmap->vertices[v2].y;

		// If the given point is one of the polygon vertices,
		// then the point is always considered on the edge of the polygon
		if((x == x1 && y == y1) || (x == x2 && y == y2)) return TRUE;

		// Check if the linedef crosses the point's horizontal axis
		if(((y1 < y) && (y2 >= y)) || ((y2 < y) && (y1 >= y)))
		{
			// Check if the linedef is at the right of the point
			if(x <= (float)((x2 - x1) * (y - y1)) / (y2 - y1) + x1) inside++;
		}
	}

	// Return result
	return (inside & 1);
}
