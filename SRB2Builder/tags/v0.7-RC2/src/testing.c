/* SRB2 Workbench -- A map editor for Sonic Robo Blast 2.
 * Copyright (C) 2009 Gregor Dick
 * http://workbench.srb2.org/
 *
 * Incorporates portions of Doom Builder, (C) 2003 Pascal vd Heiden.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 * USA.
 *
 * testing.c: Routines for invoking games in order to test maps.
 *
 * AMDG.
 */

#include <windows.h>
#include <stdio.h>
#include <tchar.h>
#include <ctype.h>

#include "general.h"
#include "wad.h"
#include "testing.h"
#include "config.h"
#include "options.h"
#include "map.h"
#include "wadopts.h"

#define TEST_FORMAT_STRING			TEXT("\"%s\" -warp %d -skill %d -gametype %d +skin %s %s %s %s %s %s %s -file \"%s\"")
#define TEST_ADDWAD_FORMAT_STRING	TEXT(" -file \"%s\"")
#define TEST_CMDLINE_INITSIZE		1024


static int BuildSRB2TestCommandLine(LPCTSTR szBinary, LPCTSTR szWadFile, LPCTSTR szAddWadFile, CONFIG *lpcfgTest, CONFIG *lpcfgWadOptsMap, int iMapNumber, LPTSTR szBuffer, int cchBuffer);
static __inline void SetTestInProgress(void);
static __inline void FinishTest(void);

static BOOL g_bTesting = FALSE;


/* BuildSRB2TestCommandLine
 *   Builds a command-line string for testing a map using SRB2.
 *
 * Parameters:
 *   LPCTSTR	szBinary			Path to SRB2 binary.
 *   LPCTSTR	szWadFile			Wad to test.
 *   LPCTSTR	szAddWadFile		Additional wad file to add. May be NULL if
 *									none required.
 *   CONFIG*	lpcfgTest			Config structure with testing parameters.
 *   CONFIG*	lpcfgWadOptsMap		Map options.
 *   int		iMapNumber			Number of map to test.
 *   LPTSTR		szBuffer			Buffer into which to return the string.
 *   int		cchBuffer			Length of buffer, including room for
 *									terminator.
 *
 * Return value: int
 *   Number of characters written, including the terminator, if successful; zero
 *   if the buffer is not large enough; negative on any other error.
 *
 * Remarks:
 *   The string is always terminated.
 */
static int BuildSRB2TestCommandLine(LPCTSTR szBinary, LPCTSTR szWadFile, LPCTSTR szAddWadFile, CONFIG *lpcfgTest, CONFIG *lpcfgWadOptsMap, int iMapNumber, LPTSTR szBuffer, int cchBuffer)
{
	int cchMain;
	int cchSkin, cchParams;
	LPTSTR szSkin, szParams;
	char cGametype;

	/* Because _sntprintf doesn't always terminate strings, do it ourselves. */
	szBuffer[cchBuffer - 1] = TEXT('\0');

	/* Get strings from the test config. */
	cchSkin = ConfigGetStringLength(lpcfgTest, TESTCFG_SKIN) + 1;
	if(cchSkin > 1)
	{
		szSkin = ProcHeapAlloc(cchSkin * sizeof(TCHAR));
		ConfigGetString(lpcfgTest, TESTCFG_SKIN, szSkin, cchSkin);
	}
	else
		return -1;

	/* We get the params even if they're empty. */
	cchParams = ConfigGetStringLength(lpcfgTest, TESTCFG_PARAMS) + 1;
	szParams = ProcHeapAlloc(cchParams * sizeof(TCHAR));
	ConfigGetString(lpcfgTest, TESTCFG_PARAMS, szParams, cchParams);

	/* Fall back on the map-default gametype if necessary. */
	cGametype = ConfigGetInteger(lpcfgTest, TESTCFG_GAMETYPE);
	if(cGametype == GT_DEFAULT) cGametype = ConfigGetInteger(lpcfgWadOptsMap, WADOPT_MAP_GAMETYPE);

	/* Write the string! */
	cchMain = _sntprintf(szBuffer, cchBuffer - 1, TEST_FORMAT_STRING,
		szBinary,
		iMapNumber,
		ConfigGetInteger(lpcfgTest, TESTCFG_DIFFICULTY),
		(cGametype != GT_SINGLEPLAYER) ? cGametype : GT_COOP,
		szSkin,
		ConfigGetInteger(lpcfgTest, TESTCFG_RENDERER) ? TEXT("-opengl") : TEXT(""),
		ConfigGetInteger(lpcfgTest, TESTCFG_WINDOWED) ? TEXT("-win") : TEXT(""),
		!ConfigGetInteger(lpcfgTest, TESTCFG_SFX) ? TEXT("-nosound") : TEXT(""),
		!ConfigGetInteger(lpcfgTest, TESTCFG_MUSIC) ? TEXT("-nomusic") : (ConfigGetInteger(lpcfgTest, TESTCFG_MUSICTYPE) ? TEXT("-nodigmusic") : TEXT("")),
		(cGametype != GT_SINGLEPLAYER) ? TEXT("-server") : TEXT(""),
		szParams,
		szWadFile);

	ProcHeapFree(szSkin);
	ProcHeapFree(szParams);

	/* Not enough space in our buffer? */
	if(cchMain < 0) return 0;

	/* Add the additional wad if desired. */
	if(szAddWadFile)
	{
		int cchAdd = _sntprintf(szBuffer + cchMain, cchBuffer - cchMain - 1, TEST_ADDWAD_FORMAT_STRING, szAddWadFile);
		if(cchAdd < 0)
			return 0;

		cchMain += cchAdd;
	}

	/* Return no. chars written, including terminator. */
	return cchMain + 1;
}


/* TestMapWithSRB2
 *   Tests a map!
 *
 * Parameters:
 *   LPCTSTR	szWadFile			Wad to test.
 *   LPCSTR		sMapLumpname		Lumpname of map to test. Must contain a
 *									valid lumpname, and need not be terminated.
 *   LPCTSTR	szBinary			Path to SRB2 binary.
 *   LPCTSTR	szAddWadFile		Additional wad file to add. May be NULL if
 *									none required.
 *   CONFIG*	lpcfgTest			Config structure with testing parameters.
 *   CONFIG*	lpcfgWadOptsMap		Map options.
 *
 * Return value: HANDLE
 *   Handle to process on success, or NULL on error.
 */
HANDLE TestMapWithSRB2(LPCTSTR szWadFile, LPCSTR sMapLumpname, LPCTSTR szBinary, LPCTSTR szAddWadFile, CONFIG *lpcfgTest, CONFIG *lpcfgWadOptsMap)
{
	UINT cchCmdLine = TEST_CMDLINE_INITSIZE;
	LPTSTR szCmdLine;
	int iMapNumber = MapNumberFromLumpname(sMapLumpname);
	STARTUPINFO si;
	PROCESS_INFORMATION pi;
	int iCPRet;
	LPTSTR szBinaryPath, szBinaryFilenamePart;
	UINT cchBinaryFull;

	/* If there's already a test in progress, don't start another one. */
	if(TestInProgress()) return NULL;

	/* Make the buffer ever bigger until we can fit the string in it. */
	while(szCmdLine = ProcHeapAlloc(cchCmdLine * sizeof(TCHAR)),
		BuildSRB2TestCommandLine(szBinary, szWadFile, szAddWadFile, lpcfgTest, lpcfgWadOptsMap, iMapNumber, szCmdLine, cchCmdLine) <= 0)
	{
		cchCmdLine <<= 1;
		ProcHeapFree(szCmdLine);
	}

	/* Get the current directory, by getting the full path to the binary and
	 * terminating the string where the filename part begins.
	 */
	cchBinaryFull = GetFullPathName(szBinary, 0, TEXT(""), NULL);
	szBinaryPath = ProcHeapAlloc((cchBinaryFull + 1) * sizeof(TCHAR));
	GetFullPathName(szBinary, cchBinaryFull, szBinaryPath, &szBinaryFilenamePart);
	*szBinaryFilenamePart = TEXT('\0');

	/* Get ready to create the process. */
	si.cb = sizeof(STARTUPINFO);
	si.cbReserved2 = 0;
	si.lpReserved = NULL;
	si.lpReserved2 = NULL;
	si.dwFlags = 0;
	si.lpDesktop = NULL;
	si.lpTitle = NULL;

	/* Go! */
	iCPRet = CreateProcess(szBinary, szCmdLine, NULL, NULL, FALSE, 0, NULL, szBinaryPath, &si, &pi);

	ProcHeapFree(szCmdLine);
	ProcHeapFree(szBinaryPath);

	/* Did we fail to create the process? */
	if(!iCPRet) return NULL;

	/* Set a flag to indicate that we're testing. */
	SetTestInProgress();

	/* Don't need the handle to the thread. */
	CloseHandle(pi.hThread);

	/* Return the handle to the process. */
	return pi.hProcess;
}


/* Some functions for setting/reading the test-in-progress flag. */
BOOL TestInProgress(void) { return g_bTesting; }
static __inline void SetTestInProgress(void) { g_bTesting = TRUE; }
static __inline void FinishTest(void) { g_bTesting = FALSE; }


/* TestThreadProc
 *   A thread procedure that sits and waits for a testing process to finish, and
 *   then deletes the temporary wad file and signals that testing has completed.
 *
 * Parameters:
 *   void*	lpvTestData		(TESTTHREADDATA*) Handle to process and filename.
 *							This buffer should be allocated by ProcHeapAlloc,
 *							and will be freed when the function returns.
 *
 * Return value: None.
 */
void TestThreadProc(void *lpvTestData)
{
	TESTTHREADDATA *lpttdata = (TESTTHREADDATA*)lpvTestData;

	/* Block until the testing process ends. */
	WaitForSingleObject(lpttdata->hProcess, INFINITE);

	/* Delete the temprary file. */
	DeleteFile(lpttdata->szFilename);

	/* Free the testing data. */
	ProcHeapFree(lpttdata);

	/* Signal that we've finished testing. */
	FinishTest();
}
