/* SRB2 Workbench -- A map editor for Sonic Robo Blast 2.
 * Copyright (C) 2009 Gregor Dick
 * http://workbench.srb2.org/
 *
 * Incorporates portions of Doom Builder, (C) 2003 Pascal vd Heiden.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 * USA.
 *
 * cdlgwrapper.c: A wrapper for comdlg32.dll functions.
 *
 * AMDG.
 */

#include <windows.h>
#include <commdlg.h>
#include <string.h>
#include <tchar.h>

#include "cdlgwrapper.h"
#include "general.h"


/* CommDlgOpen
 *   Displays the Open common dialogue box.
 *
 * Parameters:
 *   HWND		hwnd				Parent window.
 *   LPSTR		szFileNameReturn	Filename is returned here.
 *   UINT		cchFileNameReturn	Length of filename buffer.
 *   LPCTSTR	szTitle				Title of dialogue.
 *   LPCTSTR	szFilter			Type filter string.
 *   LPCTSTR	szDefExt			Default extension, without the dot.
 *   LPCTSTR	szInitFileName		Initial filename.
 *   int		iFlags				Additional flags for the comdlg32 library
 *									call. See its documentation.
 *
 * Return value: int
 *   Nonzero if the user selects a file and OKs; zero if cancelled or on error.
 *
 * Remarks:
 *   Even if the user cancels, szFileNameReturn may be clobbered.
 */
int CommDlgOpen(HWND hwnd, LPTSTR szFileNameReturn, UINT cchFileNameReturn, LPCTSTR szTitle, LPCTSTR szFilter, LPCTSTR szDefExt, LPCTSTR szInitFilename, int iFlags)
{
	OPENFILENAME ofn;

	_tcsncpy(szFileNameReturn, szInitFilename ? szInitFilename : TEXT(""), cchFileNameReturn - 1);
	szFileNameReturn[cchFileNameReturn - 1] = TEXT('\0');

	ofn.Flags = iFlags;
	ofn.hwndOwner = hwnd;
	ofn.lpstrFilter = szFilter;
	ofn.nFilterIndex = 1;
	ofn.lpstrDefExt = szDefExt;
	ofn.lpstrFile = szFileNameReturn;
	ofn.nMaxFile = cchFileNameReturn;
	ofn.lpfnHook = NULL;
	ofn.lpstrCustomFilter = NULL;
	ofn.lpstrFileTitle = NULL;
	ofn.lpstrInitialDir = NULL;
	ofn.lpTemplateName = NULL;
	ofn.lpstrTitle = szTitle;
	ofn.lStructSize = sizeof(ofn);

	return GetOpenFileName(&ofn);
}


/* CommDlgSave
 *   Displays the Save As common dialogue box.
 *
 * Parameters:
 *   HWND		hwnd				Parent window.
 *   LPSTR		szFileNameReturn	Filename is returned here.
 *   UINT		cchFileNameReturn	Length of filename buffer.
 *   LPCTSTR	szTitle				Title of dialogue.
 *   LPCTSTR	szFilter			Type filter string.
 *   LPCTSTR	szDefExt			Default extension, without the dot.
 *   LPCTSTR	szInitFileName		Initial filename.
 *   int		iFlags				Additional flags for the comdlg32 library
 *									call. See its documentation.
 *
 * Return value: int
 *   Nonzero if the user selects a file and OKs; zero if cancelled or on error.
 */
int CommDlgSave(HWND hwnd, LPTSTR szFileNameReturn, UINT cchFileNameReturn, LPCTSTR szTitle, LPCTSTR szFilter, LPCTSTR szDefExt, LPCTSTR szInitFilename, int iFlags)
{
	OPENFILENAME ofn;

	_tcsncpy(szFileNameReturn, szInitFilename ? szInitFilename : TEXT(""), cchFileNameReturn - 1);
	szFileNameReturn[cchFileNameReturn - 1] = TEXT('\0');

	ofn.Flags = iFlags;
	ofn.hwndOwner = hwnd;
	ofn.lpstrFilter = szFilter;
	ofn.nFilterIndex = 1;
	ofn.lpstrDefExt = szDefExt;
	ofn.lpstrFile = szFileNameReturn;
	ofn.nMaxFile = cchFileNameReturn;
	ofn.lpfnHook = NULL;
	ofn.lpstrCustomFilter = NULL;
	ofn.lpstrFileTitle = NULL;
	ofn.lpstrInitialDir = NULL;
	ofn.lpTemplateName = NULL;
	ofn.lpstrTitle = szTitle;
	ofn.lStructSize = sizeof(ofn);

	return GetSaveFileName(&ofn);
}


/* CommDlgColour
 *   Displays the Colour common dialogue box.
 *
 * Parameters:
 *   HWND		hwnd			Parent window.
 *   COLORREF*	lprgb			Initial colour and returned colour.
 *   COLORREF*	lprgbCustom		Array of 16 custom colours.
 *
 * Return value: int
 *   Nonzero if the user selects a colour and OKs; zero if cancelled or on
 *   error.
 *
 * Remarks:
 *   If we return zero, the value of *lprgb is preserved.
 */
int CommDlgColour(HWND hwnd, COLORREF* lprgb, COLORREF* lprgbCustom)
{
	CHOOSECOLOR cclr;

	cclr.lStructSize = sizeof(cclr);
	cclr.hwndOwner = hwnd;
	cclr.rgbResult = *lprgb;
	cclr.lpCustColors = lprgbCustom;
	cclr.Flags = CC_SOLIDCOLOR | CC_FULLOPEN | CC_RGBINIT;

	if(ChooseColor(&cclr))
	{
		*lprgb = cclr.rgbResult;
		return 1;
	}
	else return 0;
}
