/* SRB2 Workbench -- A map editor for Sonic Robo Blast 2.
 * Copyright (C) 2009 Gregor Dick
 * http://workbench.srb2.org/
 *
 * Incorporates portions of Doom Builder, (C) 2003 Pascal vd Heiden.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 * USA.
 *
 * undo.c: Implements undo and redo history.
 *
 * AMDG.
 */

#include <windows.h>

#include "general.h"
#include "map.h"
#include "undo.h"
#include "maptypes.h"


/* Sizes of map objects that matter for equality. */
#define CB_COMPARE_VERTEX	((DWORD)&((MAPVERTEX*)0)->VERTEX_UNDO_IGNORE_FROM)
#define CB_COMPARE_LINEDEF	((DWORD)&((MAPLINEDEF*)0)->LINEDEF_UNDO_IGNORE_FROM)
#define CB_COMPARE_SECTOR	((DWORD)&((MAPSECTOR*)0)->SECTOR_UNDO_IGNORE_FROM)
#define CB_COMPARE_THING	((DWORD)&((MAPTHING*)0)->THING_UNDO_IGNORE_FROM)
#define CB_COMPARE_SIDEDEF	(sizeof(MAPSIDEDEF))


/* Types. */

typedef struct _UNDOMEM
{
	void	*lpv;
	struct _UNDOMEM *lpundomemNext;
} UNDOMEM;

typedef struct _UNDOITEMLIST
{
	unsigned int	uiFirstIndex, uiLastIndex;
	void			*lpvBase;

	struct _UNDOITEMLIST *lpuilNext;
} UNDOITEMLIST;

typedef struct _UNDOFRAME
{
	int		iSectors, iLinedefs, iSidedefs, iVertices, iThings;

	int		iStringIndex;

	UNDOITEMLIST	*lpuilSectors, *lpuilLinedefs, *lpuilSidedefs;
	UNDOITEMLIST	*lpuilVertices, *lpuilThings;

	struct _UNDOFRAME	*lpundoframeNext;
	struct _UNDOFRAME	*lpundoframePrev;
} UNDOFRAME;

struct _UNDOSTACK
{
	UNDOMEM		undomemHdr, *lpundomemLast;
	UNDOFRAME	undoframeBase, *lpundoframeLast;
};


/* Static prototypes. */

static UNDOITEMLIST* CreateUndoListNodeFromData(UNDOSTACK *lpundostack, unsigned int uiFirstIndex, unsigned int uiLastIndex, unsigned int cbRecord, void *lpvBase);
static void FreeUndoFrameItems(UNDOFRAME *lpundoframe);
static void FreeUndoItemList(UNDOITEMLIST *lpuil);
static UNDOITEMLIST* CreateUndoItemList(UNDOSTACK *lpundostack, UNDOITEMLIST *lpuilLastFrameList, void *lpvData, UINT uiRecords, UINT cbRecord, UINT cbCompare);
static void ApplyUndoItemList(UNDOITEMLIST *lpuil, BYTE *lpbyData, UINT cbRecord);
static void PerformUndoFrame(UNDOFRAME *lpundoframe, MAP *lpmap);

#ifdef _DEBUG
static __inline DWORD UndoFrameDumpSize(UNDOFRAME *lpuf);
#endif


/* CreateUndoStack
 *   Creates a new undo stack for a map.
 *
 * Parameters:
 *   MAP*			lpmap		Map.
 *
 * Return value: UNDOSTACK*
 *   Pointer to new undo stack.
 */
UNDOSTACK* CreateUndoStack(MAP *lpmap, int iStringIndex)
{
	UNDOSTACK *lpundostack = (UNDOSTACK*)ProcHeapAlloc(sizeof(UNDOSTACK));

	/* Set up the last pointers. */
	lpundostack->lpundoframeLast = &lpundostack->undoframeBase;
	lpundostack->lpundomemLast = &lpundostack->undomemHdr;

	/* No memory allocated initially. */
	lpundostack->undomemHdr.lpundomemNext = NULL;

	/* The base frame is the only one to start with, and is the entire map. */
	lpundostack->undoframeBase.lpundoframeNext = NULL;
	lpundostack->undoframeBase.lpundoframePrev = NULL;

	/* Set the description. */
	lpundostack->undoframeBase.iStringIndex = iStringIndex;

	/* Store the counts. */
	lpundostack->undoframeBase.iSectors = lpmap->iSectors;
	lpundostack->undoframeBase.iLinedefs = lpmap->iLinedefs;
	lpundostack->undoframeBase.iSidedefs = lpmap->iSidedefs;
	lpundostack->undoframeBase.iVertices = lpmap->iVertices;
	lpundostack->undoframeBase.iThings = lpmap->iThings;

	/* Copy the data into the base frame. */
	lpundostack->undoframeBase.lpuilSectors = lpmap->iSectors > 0 ? CreateUndoListNodeFromData(lpundostack, 0, lpmap->iSectors - 1, sizeof(MAPSECTOR), lpmap->sectors) : NULL;
	lpundostack->undoframeBase.lpuilLinedefs = lpmap->iLinedefs > 0 ? CreateUndoListNodeFromData(lpundostack, 0, lpmap->iLinedefs - 1, sizeof(MAPLINEDEF), lpmap->linedefs) : NULL;
	lpundostack->undoframeBase.lpuilSidedefs = lpmap->iSidedefs > 0 ? CreateUndoListNodeFromData(lpundostack, 0, lpmap->iSidedefs - 1, sizeof(MAPSIDEDEF), lpmap->sidedefs) : NULL;
	lpundostack->undoframeBase.lpuilVertices = lpmap->iVertices > 0 ? CreateUndoListNodeFromData(lpundostack, 0, lpmap->iVertices - 1, sizeof(MAPVERTEX), lpmap->vertices) : NULL;
	lpundostack->undoframeBase.lpuilThings = lpmap->iThings > 0 ? CreateUndoListNodeFromData(lpundostack, 0, lpmap->iThings - 1, sizeof(MAPTHING), lpmap->things) : NULL;

	/* Done! Return the new stack. */
	return lpundostack;
}


/* CreateUndoListNodeFromData
 *   Creates a new undo items list node.
 *
 * Parameters:
 *   UNDOSTACK*		lpundostack		Undo stack with which to associate the
 *									allocated memory.
 *   unsigned int	uiFirstIndex	Index of first item to copy.
 *   unsigned int	uiLastIndex		Index of last item to copy.
 *   unsigned int	cbRecord		Size of each item, in bytes.
 *   void*			lpvBase			Buffer containing the data to copy.
 *
 * Return value: UNDOITEMLIST*
 *   Pointer to new undo item list node.
 *
 * Remarks:
 *   The node is not added to any list; the pointer to the stack passed in is
 *   just so that the memory allocated for the data can be associated with the
 *   stack.
 */
static UNDOITEMLIST* CreateUndoListNodeFromData(UNDOSTACK *lpundostack, unsigned int uiFirstIndex, unsigned int uiLastIndex, unsigned int cbRecord, void *lpvBase)
{
	UNDOITEMLIST *lpuil;		/* The new node. */
	unsigned int cbData = (uiLastIndex - uiFirstIndex + 1) * cbRecord;

	/* Add a node to the list of allocated memory. */
	lpundostack->lpundomemLast->lpundomemNext = (UNDOMEM*)ProcHeapAlloc(sizeof(UNDOMEM));
	lpundostack->lpundomemLast = lpundostack->lpundomemLast->lpundomemNext;
	lpundostack->lpundomemLast->lpundomemNext = NULL;

	/* Allocate memory for storage, and copy the data. */
	lpundostack->lpundomemLast->lpv = ProcHeapAlloc(cbData);
	CopyMemory(lpundostack->lpundomemLast->lpv, (BYTE*)lpvBase + uiFirstIndex * cbRecord, cbData);

	/* Allocate a new list node and set the pointer to the data. */
	lpuil = (UNDOITEMLIST*)ProcHeapAlloc(sizeof(UNDOITEMLIST));
	lpuil->lpuilNext = NULL;
	lpuil->uiFirstIndex = uiFirstIndex;
	lpuil->uiLastIndex = uiLastIndex;
	lpuil->lpvBase = lpundostack->lpundomemLast->lpv;

	return lpuil;
}


/* FreeUndoStack
 *   Destroys an undo stack and all associated memory.
 *
 * Parameters:
 *   UNDOSTACK*		lpundostack		Stack to free.
 *
 * Return value: None.
 *
 * Remarks:
 *   The stack structure itself is freed.
 */
void FreeUndoStack(UNDOSTACK *lpundostack)
{
	UNDOMEM *lpundomem = lpundostack->undomemHdr.lpundomemNext;
	UNDOMEM *lpundomemNext;
	UNDOFRAME *lpundoframe, *lpundoframeNext;

	/* Free all the memory we allocated for map data. */
	while(lpundomem)
	{
		ProcHeapFree(lpundomem->lpv);
		lpundomemNext = lpundomem->lpundomemNext;
		ProcHeapFree(lpundomem);
		lpundomem = lpundomemNext;
	}

	/* Free the memory used by the base frame, but not the frame itself. */
	FreeUndoFrameItems(&lpundostack->undoframeBase);

	/* Free all the remaining frames and their data. */
	lpundoframe = lpundostack->undoframeBase.lpundoframeNext;
	while(lpundoframe)
	{
		FreeUndoFrameItems(lpundoframe);
		lpundoframeNext = lpundoframe->lpundoframeNext;
		ProcHeapFree(lpundoframe);
		lpundoframe = lpundoframeNext;
	}

	/* Free the stack itself. */
	ProcHeapFree(lpundostack);
}


/* FreeUndoFrameItems
 *   Frees all item lists associated with an undo frame.
 *
 * Parameters:
 *   UNDOFRAME*		lpundoframe		Frame whose items are to be freed.
 *
 * Return value: None.
 *
 * Remarks:
 *   The memory used in the item lists for the map data is not freed, as many
 *   frames may point to the same buffer.
 */
static void FreeUndoFrameItems(UNDOFRAME *lpundoframe)
{
	/* Free the lists for each map type. */
	FreeUndoItemList(lpundoframe->lpuilSectors);
	FreeUndoItemList(lpundoframe->lpuilLinedefs);
	FreeUndoItemList(lpundoframe->lpuilSidedefs);
	FreeUndoItemList(lpundoframe->lpuilVertices);
	FreeUndoItemList(lpundoframe->lpuilThings);
}


/* FreeUndoItemList
 *   Frees an undo item list.
 *
 * Parameters:
 *   UNDOITEMLIST*	lpuil	List to be freed.
 *
 * Return value: None.
 *
 * Remarks:
 *   The memory used in the item lists for the map data is not freed, as many
 *   frames may point to the same buffer.
 */
static void FreeUndoItemList(UNDOITEMLIST *lpuil)
{
	UNDOITEMLIST *lpuilNext;

	while(lpuil)
	{
		lpuilNext = lpuil->lpuilNext;
		ProcHeapFree(lpuil);
		lpuil = lpuilNext;
	}
}


/* PushNewUndoFrame
 *   Creates a new undo frame from a map, and adds it to an undo stack.
 *
 * Parameters:
 *   UNDOSTACK*	lpundostack		Undo stack to add to.
 *   MAP*		lpmap			Pointer to map.
 *   int		iStringIndex	ID of resource string to display in menus.
 *
 * Return value: None.
 */
void PushNewUndoFrame(UNDOSTACK *lpundostack, MAP *lpmap, int iStringIndex)
{
	UNDOFRAME *lpundoframe = (UNDOFRAME*)ProcHeapAlloc(sizeof(UNDOFRAME));

	/* Store the numbers of each type of object. */
	lpundoframe->iSectors = lpmap->iSectors;
	lpundoframe->iLinedefs = lpmap->iLinedefs;
	lpundoframe->iSidedefs = lpmap->iSidedefs;
	lpundoframe->iVertices = lpmap->iVertices;
	lpundoframe->iThings = lpmap->iThings;

	/* Store the string index. */
	lpundoframe->iStringIndex = iStringIndex;

	/* Build the item lists for each type of object. */
	lpundoframe->lpuilSectors = CreateUndoItemList(lpundostack, lpundostack->lpundoframeLast->lpuilSectors, lpmap->sectors, lpmap->iSectors, sizeof(MAPSECTOR), CB_COMPARE_SECTOR);
	lpundoframe->lpuilLinedefs = CreateUndoItemList(lpundostack, lpundostack->lpundoframeLast->lpuilLinedefs, lpmap->linedefs, lpmap->iLinedefs, sizeof(MAPLINEDEF), CB_COMPARE_LINEDEF);
	lpundoframe->lpuilSidedefs = CreateUndoItemList(lpundostack, lpundostack->lpundoframeLast->lpuilSidedefs, lpmap->sidedefs, lpmap->iSidedefs, sizeof(MAPSIDEDEF), CB_COMPARE_SIDEDEF);
	lpundoframe->lpuilVertices = CreateUndoItemList(lpundostack, lpundostack->lpundoframeLast->lpuilVertices, lpmap->vertices, lpmap->iVertices, sizeof(MAPVERTEX), CB_COMPARE_VERTEX);
	lpundoframe->lpuilThings = CreateUndoItemList(lpundostack, lpundostack->lpundoframeLast->lpuilThings, lpmap->things, lpmap->iThings, sizeof(MAPTHING), CB_COMPARE_THING);

	/* Add the new frame to the list. */
	lpundoframe->lpundoframePrev = lpundostack->lpundoframeLast;
	lpundoframe->lpundoframeNext = NULL;
	lpundostack->lpundoframeLast->lpundoframeNext = lpundoframe;
	lpundostack->lpundoframeLast = lpundoframe;
}



/* CreateUndoItemList
 *   Creates a new undo item list from a block of data wrt a previous list.
 *
 * Parameters:
 *   UNDOSTACK*		lpundostack			Undo stack to associate the allocated
 *										memory with.
 *   UNDOITEMLIST*	lpuilLastFrameList	Corresponding list in the previous frame.
 *   void*			lpvData				Data for the items in the new list.
 *   UINT			uiRecords			Number of objects in the current state.
 *   UINT			cbRecord			Size of each object.
 *   UNIT			cbCompare			The number of bytes at the start of each
 *										object that matter for equality.
 *
 * Return value: UNDOITEMLIST*
 *   Pointer to new item list.
 */
static UNDOITEMLIST* CreateUndoItemList(UNDOSTACK *lpundostack, UNDOITEMLIST *lpuilLastFrameList, void *lpvData, UINT uiRecords, UINT cbRecord, UINT cbCompare)
{
	BOOL bCurrentMatchState;
	BOOL bStartNewNode = TRUE;
	unsigned int ui, uiLow;

	UNDOITEMLIST *lpuilNew = NULL;
	UNDOITEMLIST *lpuilCurrent;
	UNDOITEMLIST **lplpuilNext = &lpuilNew;

	ui = uiLow = 0;

	while(lpuilLastFrameList && ui < uiRecords)
	{
		BOOL bNextMatchState;
		BOOL bDifferentMatchStates = FALSE;

		if(bStartNewNode)
		{
			/* Start a new node. */
			uiLow = ui;

			/* Assume we don't need a new node next time. */
			bStartNewNode = FALSE;
		}

		/* Do we match? */
		bCurrentMatchState = (memcmp((BYTE*)lpvData + cbRecord * ui, (BYTE*)lpuilLastFrameList->lpvBase + cbRecord * (ui - lpuilLastFrameList->uiFirstIndex), cbCompare) == 0);

		/* Find the next match state, if such a thing exists. */
		if(ui < uiRecords - 1 && ui < lpuilLastFrameList->uiLastIndex)
		{
			bNextMatchState = (memcmp((BYTE*)lpvData + cbRecord * (ui + 1), (BYTE*)lpuilLastFrameList->lpvBase + cbRecord * (ui + 1 - lpuilLastFrameList->uiFirstIndex), cbCompare) == 0);
			bDifferentMatchStates = (bNextMatchState && !bCurrentMatchState) || (!bNextMatchState && bCurrentMatchState);
		}

		/* Need to finish this node? */
		if(ui == uiRecords - 1 || lpuilLastFrameList->uiLastIndex == ui || bDifferentMatchStates)
		{
			/* This is the last element for this node, so start a new one next
			 * time.
			 */
			bStartNewNode = TRUE;

			/* Set the data pointer. How we do this depends on whether it's data
			 * that's the same as the previous frame.
			 */
			if(bCurrentMatchState)
			{
				/* We're same as the frame below, so just calculate an offset
				 * from the base.
				 */
				lpuilCurrent = *lplpuilNext = (UNDOITEMLIST*)ProcHeapAlloc(sizeof(UNDOITEMLIST));
				lplpuilNext = &lpuilCurrent->lpuilNext;
				lpuilCurrent->lpuilNext = NULL;
				lpuilCurrent->uiFirstIndex = uiLow;
				lpuilCurrent->uiLastIndex = ui;
				lpuilCurrent->lpvBase = (BYTE*)lpuilLastFrameList->lpvBase + (uiLow - lpuilLastFrameList->uiFirstIndex) * cbRecord;
			}
			else
			{
				/* This node is for objects that have changed since the last
				 * frame, so make a copy of them.
				 */
				lpuilCurrent = *lplpuilNext = CreateUndoListNodeFromData(lpundostack, uiLow, ui, cbRecord, lpvData);
				lplpuilNext = &lpuilCurrent->lpuilNext;
			}

			/* Additionally advance to the next section if we've reached the
			 * end of this one.
			 */
			if(lpuilLastFrameList->uiLastIndex == ui)
				lpuilLastFrameList = lpuilLastFrameList->lpuilNext;
		}

		/* Next object. */
		ui++;
	}

	/* NB: bCurrentMatchState is undefined if we've run out of records. */

	/* If there are still any objects remaining, they can't correspond to any
	 * from the previous frame (since in this case all the frames have gone),
	 * so copy them.
	 */
	if(ui < uiRecords)
		lpuilCurrent = *lplpuilNext = CreateUndoListNodeFromData(lpundostack, ui, uiRecords - 1, cbRecord, lpvData);

	/* Done! Return the beginning of the list. */
	return lpuilNew;
}


/* GetUndoTopStringIndex
 *   Retrieves resource string index associated with frame at the top of an undo
 *   stack.
 *
 * Parameters:
 *   UNDOSTACK*		lpundostack		Undo stack.
 *
 * Return value: int
 *   Index of string.
 */
int GetUndoTopStringIndex(UNDOSTACK *lpundostack)
{
	return lpundostack->lpundoframeLast->iStringIndex;
}


/* UndoStackHasOnlyOneFrame
 *   Determines whether an undo stack has only one frame.
 *
 * Parameters:
 *   UNDOSTACK*		lpundostack		Undo stack.
 *
 * Return value: BOOL
 *   TRUE if the stack has only one frame; FALSE otherwise.
 */
BOOL UndoStackHasOnlyOneFrame(UNDOSTACK *lpundostack)
{
	return !lpundostack->lpundoframeLast->lpundoframePrev;
}


/* PopUndoFrame
 *   Removes a frame from the top of an undo stack.
 *
 * Parameters:
 *   UNDOSTACK*		lpundostack		Undo stack.
 *
 * Return value: None.
 *
 * Remarks:
 *   The last frame must not be popped! Call UndoStackHasOnlyOneFrame first.
 */
void PopUndoFrame(UNDOSTACK *lpundostack)
{
	UNDOFRAME *lpundoframePrev = lpundostack->lpundoframeLast->lpundoframePrev;

	FreeUndoFrameItems(lpundostack->lpundoframeLast);
	ProcHeapFree(lpundostack->lpundoframeLast);

	/* This is why popping the last frame is not allowed... */
	lpundoframePrev->lpundoframeNext = NULL;
	lpundostack->lpundoframeLast = lpundoframePrev;
}


/* PerformTopUndo
 *   Applies the top undo frame of a stack to a map.
 *
 * Parameters:
 *   UNDOSTACK*		lpundostack		Undo stack.
 *   MAP*			lpmap			Map.
 *
 * Return value: None.
 *
 * Remarks:
 *   The frame is NOT popped after it's been applied. This is because when the
 *   last frame has been undone, the entire stack structure must be destroyed.
 */
void PerformTopUndo(UNDOSTACK *lpundostack, MAP *lpmap)
{
	PerformUndoFrame(lpundostack->lpundoframeLast, lpmap);
}


/* PerformUndoFrame
 *   Applies an undo frame to a map.
 *
 * Parameters:
 *   UNDOFRAME*		lpundoframe		Undo frame.
 *   MAP*			lpmap			Map.
 *
 * Return value: None.
 *
 * Remarks:
 *   To perform the top undo of a stack, call PerformTopUndo. This routine is
 *   only separate from the that one to allow tricks such as dumping the stack
 *   to a file.
 */
static void PerformUndoFrame(UNDOFRAME *lpundoframe, MAP *lpmap)
{
	lpmap->iLinedefs = lpundoframe->iLinedefs;
	lpmap->iSectors = lpundoframe->iSectors;
	lpmap->iSidedefs = lpundoframe->iSidedefs;
	lpmap->iVertices = lpundoframe->iVertices;
	lpmap->iThings = lpundoframe->iThings;

	ApplyUndoItemList(lpundoframe->lpuilLinedefs, (BYTE*)lpmap->linedefs, sizeof(MAPLINEDEF));
	ApplyUndoItemList(lpundoframe->lpuilSectors, (BYTE*)lpmap->sectors, sizeof(MAPSECTOR));
	ApplyUndoItemList(lpundoframe->lpuilSidedefs, (BYTE*)lpmap->sidedefs, sizeof(MAPSIDEDEF));
	ApplyUndoItemList(lpundoframe->lpuilVertices, (BYTE*)lpmap->vertices, sizeof(MAPVERTEX));
	ApplyUndoItemList(lpundoframe->lpuilThings, (BYTE*)lpmap->things, sizeof(MAPTHING));
}


/* ApplyUndoItemList
 *   Applies data in an undo item list to a buffer.
 *
 * Parameters:
 *   UNDOITEMLIST*	lpuil		Undo item list.
 *   BYTE*			lpbyData	Map data buffer.
 *   UINT			cbRecord	Size of each object.
 *
 * Return value: None.
 *
 * Remarks:
 *   The frame is NOT popped after it's been applied. This is because when the
 *   last frame has been undone, the entire stack structure must be destroyed.
 */
static void ApplyUndoItemList(UNDOITEMLIST *lpuil, BYTE *lpbyData, UINT cbRecord)
{
	while(lpuil)
	{
		CopyMemory(lpbyData + lpuil->uiFirstIndex * cbRecord, lpuil->lpvBase, (lpuil->uiLastIndex - lpuil->uiFirstIndex + 1) * cbRecord);
		lpuil = lpuil->lpuilNext;
	}
}


#ifdef _DEBUG

int DumpUndoStack(UNDOSTACK *lpundostack, LPCTSTR szFileName)
{
	MAP *lpmap = AllocateMapStructure();
	UNDOFRAME *lpufStart, *lpufPrev;
	HANDLE hFile;

	/* Don't exceed 64 MB. */
	DWORD cbRemaining = 1 << 26;

	/* Find where to begin so that we fit in the space available. */
	lpufPrev = lpundostack->lpundoframeLast;
	lpufStart = NULL;
	while(lpufPrev && cbRemaining > 0)
	{
		DWORD cbRequired = UndoFrameDumpSize(lpufPrev);

		if(cbRequired > cbRemaining)
		{
			/* There's not enough room to write this frame, so we can't fit any
			 * more.
			 */
			break;
		}

		/* There is enough room for this frame. */
		cbRemaining -= cbRequired;
		lpufStart = lpufPrev;
		lpufPrev = lpufPrev->lpundoframePrev;
	}

	/* Sanity check: not enough room for even one frame!? */
	if(!lpufStart) return 1;

	hFile = CreateFile(szFileName, GENERIC_WRITE, 0, NULL, OPEN_ALWAYS, FILE_ATTRIBUTE_ARCHIVE, NULL);
	if(hFile == INVALID_HANDLE_VALUE) return 2;

	/* Write the frames! */
	while(lpufStart)
	{
		DWORD cbWritten;
		int i;

		/* Apply to the dummy map. */
		PerformUndoFrame(lpufStart, lpmap);

		WriteFile(hFile, &lpufStart->iStringIndex, sizeof(int), &cbWritten, NULL);

		WriteFile(hFile, &lpmap->iSectors, sizeof(int), &cbWritten, NULL);
		for(i = 0; i < lpmap->iSectors; i++)
			WriteFile(hFile, &lpmap->sectors[i], SECTORRECORDSIZE, &cbWritten, NULL);

		WriteFile(hFile, &lpmap->iLinedefs, sizeof(int), &cbWritten, NULL);
		for(i = 0; i < lpmap->iLinedefs; i++)
			WriteFile(hFile, &lpmap->linedefs[i], LINEDEFRECORDSIZE, &cbWritten, NULL);

		WriteFile(hFile, &lpmap->iSidedefs, sizeof(int), &cbWritten, NULL);
		for(i = 0; i < lpmap->iSidedefs; i++)
			WriteFile(hFile, &lpmap->sidedefs[i], SIDEDEFRECORDSIZE, &cbWritten, NULL);

		WriteFile(hFile, &lpmap->iVertices, sizeof(int), &cbWritten, NULL);
		for(i = 0; i < lpmap->iVertices; i++)
			WriteFile(hFile, &lpmap->vertices[i], VERTEXRECORDSIZE, &cbWritten, NULL);

		WriteFile(hFile, &lpmap->iThings, sizeof(int), &cbWritten, NULL);
		for(i = 0; i < lpmap->iThings; i++)
			WriteFile(hFile, &lpmap->things[i], THINGRECORDSIZE, &cbWritten, NULL);

		lpufStart = lpufStart->lpundoframeNext;
	}

	SetEndOfFile(hFile);
	CloseHandle(hFile);


	DestroyMapStructure(lpmap);
	return 0;
}

static __inline DWORD UndoFrameDumpSize(UNDOFRAME *lpuf)
{
	return
		sizeof(int) * 6 /* 5 types of object + string index */
		+ lpuf->iSectors * SECTORRECORDSIZE
		+ lpuf->iLinedefs * LINEDEFRECORDSIZE
		+ lpuf->iSidedefs * SIDEDEFRECORDSIZE
		+ lpuf->iVertices * VERTEXRECORDSIZE
		+ lpuf->iThings * THINGRECORDSIZE;
}


UNDOSTACK* LoadUndoStack(LPCTSTR szFileName)
{
	UNDOSTACK *lpus = NULL;
	HANDLE hFile = CreateFile(szFileName, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING, FILE_FLAG_SEQUENTIAL_SCAN, NULL);
	DWORD cbFileSize;
	MAP *lpmap = AllocateMapStructure();

	if(hFile == INVALID_HANDLE_VALUE) return NULL;

	cbFileSize = GetFileSize(hFile, NULL);

	while(SetFilePointer(hFile, 0, NULL, FILE_CURRENT) < cbFileSize)
	{
		DWORD cbRead;
		int iStringIndex, i;
		BYTE *lpbyBuffer;
		DWORD cbBuffer;

		/* Get index of string from the start of the frame. */
		ReadFile(hFile, &iStringIndex, sizeof(int), &cbRead, NULL);
		if(cbRead != sizeof(int)) break;

		SetFilePointer(hFile, 0, NULL, FILE_CURRENT);

		/* Read the number of sectors and then the sectors themselves. */
		ReadFile(hFile, &lpmap->iSectors, sizeof(int), &cbRead, NULL);
		if(cbRead != sizeof(int)) break;

		SetFilePointer(hFile, 0, NULL, FILE_CURRENT);

		cbBuffer = SECTORRECORDSIZE * lpmap->iSectors;
		if(cbBuffer > 0)
		{
			lpbyBuffer = ProcHeapAlloc(cbBuffer);

			ReadFile(hFile, lpbyBuffer, cbBuffer, &cbRead, NULL);

			for(i = 0; i < lpmap->iSectors; i++)
				CopyMemory(&lpmap->sectors[i], lpbyBuffer + SECTORRECORDSIZE * i, SECTORRECORDSIZE);

			ProcHeapFree(lpbyBuffer);
			if(cbRead != cbBuffer) break;
		}

		SetFilePointer(hFile, 0, NULL, FILE_CURRENT);

		/* Read the number of linedefs and then the linedefs themselves. */
		ReadFile(hFile, &lpmap->iLinedefs, sizeof(int), &cbRead, NULL);
		if(cbRead != sizeof(int)) break;

		cbBuffer = LINEDEFRECORDSIZE * lpmap->iLinedefs;
		if(cbBuffer > 0)
		{
			lpbyBuffer = ProcHeapAlloc(cbBuffer);

			ReadFile(hFile, lpbyBuffer, cbBuffer, &cbRead, NULL);

			for(i = 0; i < lpmap->iLinedefs; i++)
				CopyMemory(&lpmap->linedefs[i], lpbyBuffer + LINEDEFRECORDSIZE * i, LINEDEFRECORDSIZE);

			ProcHeapFree(lpbyBuffer);
			if(cbRead != cbBuffer) break;
		}

		/* Read the number of sidedefs and then the sidedefs themselves. */
		ReadFile(hFile, &lpmap->iSidedefs, sizeof(int), &cbRead, NULL);
		if(cbRead != sizeof(int)) break;

		cbBuffer = SIDEDEFRECORDSIZE * lpmap->iSidedefs;
		if(cbBuffer > 0)
		{
			lpbyBuffer = ProcHeapAlloc(cbBuffer);

			ReadFile(hFile, lpbyBuffer, cbBuffer, &cbRead, NULL);

			for(i = 0; i < lpmap->iSidedefs; i++)
				CopyMemory(&lpmap->sidedefs[i], lpbyBuffer + SIDEDEFRECORDSIZE * i, SIDEDEFRECORDSIZE);

			ProcHeapFree(lpbyBuffer);
			if(cbRead != cbBuffer) break;
		}

		/* Read the number of vertices and then the vertices themselves. */
		ReadFile(hFile, &lpmap->iVertices, sizeof(int), &cbRead, NULL);
		if(cbRead != sizeof(int)) break;

		cbBuffer = VERTEXRECORDSIZE * lpmap->iVertices;
		if(cbBuffer > 0)
		{
			lpbyBuffer = ProcHeapAlloc(cbBuffer);

			ReadFile(hFile, lpbyBuffer, cbBuffer, &cbRead, NULL);

			for(i = 0; i < lpmap->iVertices; i++)
				CopyMemory(&lpmap->vertices[i], lpbyBuffer + VERTEXRECORDSIZE * i, VERTEXRECORDSIZE);

			ProcHeapFree(lpbyBuffer);
			if(cbRead != cbBuffer) break;
		}

		/* Read the number of things and then the things themselves. */
		ReadFile(hFile, &lpmap->iThings, sizeof(int), &cbRead, NULL);
		if(cbRead != sizeof(int)) break;

		cbBuffer = THINGRECORDSIZE * lpmap->iThings;
		if(cbBuffer > 0)
		{
			lpbyBuffer = ProcHeapAlloc(cbBuffer);

			ReadFile(hFile, lpbyBuffer, cbBuffer, &cbRead, NULL);

			for(i = 0; i < lpmap->iThings; i++)
				CopyMemory(&lpmap->things[i], lpbyBuffer + THINGRECORDSIZE * i, THINGRECORDSIZE);

			ProcHeapFree(lpbyBuffer);
			if(cbRead != cbBuffer) break;
		}


		if(lpus)
			PushNewUndoFrame(lpus, lpmap, iStringIndex);
		else
			lpus = CreateUndoStack(lpmap, iStringIndex);
	}

	DestroyMapStructure(lpmap);
	CloseHandle(hFile);

	return lpus;
}

#endif
