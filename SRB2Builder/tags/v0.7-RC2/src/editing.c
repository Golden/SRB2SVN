/* SRB2 Workbench -- A map editor for Sonic Robo Blast 2.
 * Copyright (C) 2009 Gregor Dick
 * http://workbench.srb2.org/
 *
 * Incorporates portions of Doom Builder, (C) 2003 Pascal vd Heiden.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 * USA.
 *
 * editing.c: Routines for manipulating the contents of maps. This is where most
 * of the hard work is done.
 *
 * AMDG.
 */

#include <windows.h>
#include <stdlib.h>
#include <search.h>
#include <string.h>
#include <tchar.h>
#include <math.h>

#include "general.h"
#include "map.h"
#include "renderer.h"
#include "mapconfig.h"
#include "selection.h"
#include "config.h"
#include "editing.h"
#include "options.h"
#include "wadopts.h"
#include "errorcheck.h"
#include "../res/resource.h"

#include "win/infobar.h"
#include "win/mapwin.h"

#include "CodeImp/ci_math.h"
#include "CodeImp/ci_data_proto.h"



/* Types. */

struct _LOOPLIST
{
	DYNAMICINTARRAY		diarrayLinedefs, diarraySameSide;
	int					iFirstLineSide;	/* Only used by ResolveSector. */
	int					iSector;
	struct _LOOPLIST	*lplooplistNext;
};

typedef struct _SIDEFLAGMARKER
{
	int		iSidedef;
	DWORD	dwFlag;
	int		iSide;
} SIDEFLAGMARKER;

struct _MAPOBJGRID
{
	int		cxGrid, cyGrid;
	int		iColumns, iRows;
	short	xOrigin, yOrigin;

	DYNAMICINTARRAY		*lpdiarrayGrid;
};

/* Ordered passes. */
enum ENUM_PASSES
{
	FIRST_PASS = 0,
	SECOND_PASS = 1
};

/* Ways in which GetSectorByHalfLineMethod can fail to find a sector. */
enum ENUM_GSBHLM_FAILURES
{
	NOSEC_SPACE = -1,
	NOSEC_INFINITY = -2,
	NOSEC_STARVATION = -3
};


/* Macros. */
#define LDLOOKUP_INITBUFSIZE 8
#define SDPOLYARRAY_INITBUFSIZE 8
#define LINELOOPARRAY_INITBUFSIZE 8
#define DELLINES_INITBUFSIZE 4
#define MERGEALL_INITLISTLENGTH 8
#define DELVERTICES_INITBUFSIZE 4
#define DRAWOPSTATESTACK_INITBUFSIZE 8

/* Number of new vertices/lines to store per buffer increment. */
#define DRAWOP_BUFFER_COUNT_INCREMENT 64

/* Resp. size of cells and min. initial buffer size of cells in a MAPOBJGRID. */
#define MAPOBJGRID_CELL_SIZE		512
#define MIN_MOG_CELL_ENTRIES		4

#define GET_MOG_CELL(lpmog, col, row)	((lpmog)->lpdiarrayGrid + (lpmog)->iRows * (col) + (row))

/* Remove this to fall back to naive testing. */
#define USE_MOG



/* Info used to find intersection of multiple objects' properties. */
typedef struct _CHECKINFO
{
	MAP		*lpmap;
	CONFIG	*lpcfg;
	BOOL	bFirstTime;
	void	*lpdi;		/* [LINEDEF/SECTOR/THING/VERTEX]DISPLAYINFO. */
	DWORD	dwDisplayFlags;
} CHECKINFO;


/* Static prototypes. */
static BOOL CheckLineCallback(int iIndex, CHECKINFO *lpci);
static BOOL CheckSectorCallback(int iIndex, CHECKINFO *lpci, BOOL bNybble);
static BOOL CheckThingCallback(int iIndex, CHECKINFO *lpci);
static BOOL CheckVerticesCallback(int iIndex, CHECKINFO *lpci);
static void AddVertexToDrawList(MAP *lpmap, DRAW_OPERATION *lpdrawop, int iVertex);
static void AddLinedefToDrawList(MAP *lpmap, DRAW_OPERATION *lpdrawop, int iLinedef);
static void ExchangeSidedefs(MAP *lpmap, int iLinedef);
static int BisectLinedef(MAP *lpmap, int iLinedef);
static void GetLineSideSpot(MAP *lpmap, int iLinedef, int iSideOfLine, float fDistance, float *lpx, float *lpy);
static BOOL VerticesReachable(MAP *lpmap, DYNAMICINTARRAY *lpldlookup, int iVertex1, int iVertex2, int iMinTimes);
static BOOL __fastcall VerticesReachableInternal(DYNAMICINTARRAY *lpldlookup, BOOL *lpbVisited, int iVertex1, int iVertex2);
static void __fastcall PropagateNewSector(MAP *lpmap, DYNAMICINTARRAY *lpldlookup, int iLinedef, int iLinedefSide, int iNewSector, DYNAMICINTARRAY *lpdiarrayAffectedSidedefs);
static double AdjacentLinedefAngleFromFront(MAP *lpmap, int iLinedef1, int iLinedef2);
static double LinedefAngleV1(MAP *lpmap, int iLinedef);
static double LinedefAngleV2(MAP *lpmap, int iLinedef);
static void CopySidedefProperties(MAP *lpmap, int iOldSidedef, int iNewSidedef);
static void JoinSectors(MAP *lpmap, int iNewSector, int iOldSector, BOOL bMerge);
static void LabelEnclosureFromLoops(MAP *lpmap, int iLinedef, LOOPLIST *lplooplist);
static __inline BOOL FindLoopFromLine(MAP *lpmap, int iSourceLinedef, int iSourceSide, int iTargetLinedef, int iTargetSide, int iSector, DYNAMICINTARRAY *lpdiarrayLinedefs, DYNAMICINTARRAY *lpdiarrayOrientations, DYNAMICINTARRAY *lpldlookup, BOOL (*fnCondition)(MAPLINEDEF *lpld, void *lpvParam));
static BOOL __fastcall FindLoopFromLine_Body(MAP *lpmap, int iSourceLinedef, int iSourceSide, int iTargetLinedef, int iTargetSide, int iSector, DYNAMICINTARRAY *lpdiarrayLinedefs, DYNAMICINTARRAY *lpdiarrayOrientations, DYNAMICINTARRAY *lpldlookup, BOOL (*fnCondition)(MAPLINEDEF *lpld, void *lpvParam), DYNAMICINTARRAY *lpdiarrayLabelled);
static BOOL __fastcall FindLoopFromNextLinedef(MAP *lpmap, int iSourceLinedef, int iSourceSide, int iNextLinedef, int iNextSide, int iTargetLinedef, int iTargetSide, int iSector, DYNAMICINTARRAY *lpdiarrayLinedefs, DYNAMICINTARRAY *lpdiarrayOrientations, DYNAMICINTARRAY *lpldlookup, BOOL (*fnCondition)(MAPLINEDEF *lpld, void *lpvParam), DYNAMICINTARRAY *lpdiarrayLabelled);
static __inline void ClearLoopAndEnclosureFlags(MAP *lpmap);
static __inline void ClearEnclosureFlags(MAP *lpmap);
static __inline void ClearLoopFlags(MAP *lpmap);
static __inline void ClearNewLoopFlags(MAP *lpmap);
static __inline void ClearRSFlag(MAP *lpmap);
static __inline void ClearLinedefsLabelFlag(MAP *lpmap);
static __inline void ClearChangingFlag(MAP *lpmap);
static void ClearVertexFlags(MAP *lpmap, WORD wFlags);
static BOOL LinedefIntersectsHalfLine(MAP *lpmap, int iLinedef, int xLine1, int yLine1, int xLine2, int yLine2, FPOINT *lpfpIntersect);
static int FindNearestLinedefOnHalfLine(MAP *lpmap, MAPOBJGRID *lpmogLinedefs, int xLine1, int yLine1, int xLine2, int yLine2);
static int GetSectorByHalfLineMethod(MAP *lpmap, MAPOBJGRID *lpmogLinedefs, POINT *lpptOrigin, POINT *lpptOther);
static __inline void ApplyRelativeCeilingHeight(MAP *lpmap, int iSector, int iDelta);
static __inline void ApplyRelativeFloorHeight(MAP *lpmap, int iSector, int iDelta);
static __inline void ApplyRelativeBrightness(MAP *lpmap, int iSector, short nDelta);
static __inline void ApplyRelativeThingZ(MAP *lpmap, int iThing, int iDelta, CONFIG *lpcfgFlatThings);
static __inline void ApplyRelativeAngle(MAP *lpmap, int iThing, int iDelta);
static __inline void ApplyRelativeThingX(MAP *lpmap, int iThing, int iDelta);
static __inline void ApplyRelativeThingY(MAP *lpmap, int iThing, int iDelta);
static __inline void ApplyRelativeThingFlags(MAP *lpmap, int iThing, int iDelta);
static __inline void ApplyRelativeSidedefX(MAP *lpmap, int iSidedef, int iDelta);
static __inline void ApplyRelativeSidedefY(MAP *lpmap, int iSidedef, int iDelta);
static void AutoalignRecursion(MAP *lpmap, HWND hwndMap, int iStartLinedef, BYTE byTexFlags, BOOL bInSelection, DYNAMICINTARRAY *lpdiarrayLDLookup, BOOL bLabelOnChangeOnly);
static BYTE AlignAdjacentTextures(MAP *lpmap, HWND hwndMap, int iSourceLinedef, int iAlignLinedef, BYTE byTexFlags, int iWhichVertex);
static __inline int LinedefLength(MAP *lpmap, int iLinedef);
static BOOL CalculateSurfaceTexturesAlignment(MAPSECTOR *lpsecSource, MAPSECTOR *lpsecSourceReverse, MAPSECTOR *lpsecAlign, MAPSECTOR *lpsecAlignReverse, int iTexSource, int iTexAlign, WORD wFlagsSource, WORD wFlagsAlign, int *lpiVertOffset);
static __inline BYTE VisitedFlagsToTexFlags(DWORD dwLineEditFlags);
static __inline DWORD TexFlagsToVisitedFlags(BYTE byTexFlags);
static __inline BOOL SectorsEqual(MAP *lpmap, int iSector1, int iSector2);
static BOOL FindHighOnlyIDSectorSet(MAP *lpmap, int iSector, BOOL bInSelection, DYNAMICINTARRAY *lpdiarray);
static __inline int SideOfLinedef(MAP *lpmap, int iLinedef, FPOINT *lpfp);
static __inline int SideOfLinedefInt2Coords(MAP *lpmap, int iLinedef, POINT *lppt);
static void ApplySectorDefaults(MAP *lpmap, CONFIG *lpcfgWadOptMap, int iSector);
static __inline BOOL PointOnLineVertex(MAP *lpmap, int iLinedef, FPOINT *lpfp);
static void MakeLineSingleSided(MAP *lpmap, int iLinedef);
static void MakeLineDoubleSided(MAP *lpmap, int iLinedef, CONFIG *lpcfgWadOptMap);
static void RemoveSectorInterior(MAP *lpmap, int iSector);
static __inline void InitOrphanVertexCheck(MAP *lpmap, DYNAMICINTARRAY **lplpdiarrayLDLookupBefore);
static void DeleteOrphanedVertices(MAP *lpmap, DYNAMICINTARRAY *lpdiarrayLDLookupBefore);
static void ResolveSector(MAP *lpmap, int iSector);
static MAPOBJGRID* AllocateMapObjGrid(MAP *lpmap, int iPopEstimate);
static __inline int GetMOGColFromAbscissa(MAPOBJGRID *lpmog, short x);
static __inline int GetMOGRowFromOrdinate(MAPOBJGRID *lpmog, short y);
static __inline DYNAMICINTARRAY* GetMOGCellFromPoint(MAPOBJGRID *lpmog, short x, short y);
static __inline void AddVertexToMOG(MAPOBJGRID *lpmog, MAP *lpmap, int iVertex);
void AddLinedefToMOG(MAPOBJGRID *lpmog, MAP *lpmap, int iLinedef);
static int MOGCellDistFromPoint(MAPOBJGRID *lpmog, short nCol, short nRow, short x, short y);
static __inline void GetMOGCellRect(MAPOBJGRID *lpmog, short nCol, short nRow, LPRECT lprc);
static BOOL GetNextLineSegmentMOGCell(MAPOBJGRID *lpmog, int x1, int y1, int x2, int y2, short *lpnCol, short *lpnRow, float *lpfDistance, BOOL bInfinite);
static __inline void GetOrthHLPointsIn2Coords(MAP *lpmap, int iLinedef, int iLineSide, POINT *lpptOrigin, POINT *lpptOther);


/* RemoveUnusedVertices
 *   Removes all unused vertices from a map.
 *
 * Parameters:
 *   MAP*	lpmap				Map.
 *   BYTE	byRequiredFlags		Only delete vertices that have these flags set.
 *
 * Return value: int
 *   Number of vertices removed.
 */
int RemoveUnusedVertices(MAP *lpmap, BYTE byRequiredFlags)
{
	int i, iVerticesRemoved = 0;
	BOOL *lpbUsedVertices;

	/* Trivial case. */
	if(lpmap->iVertices <= 0) return 0;

	/* Determine which vertices are used. */
	lpbUsedVertices = UsedVertexMatrix(lpmap);

	/* Loop through all vertices, removing the unused ones. */
	for(i = lpmap->iVertices - 1; i >= 0; i--)
	{
		if(!lpbUsedVertices[i] && (lpmap->vertices[i].editflags & byRequiredFlags) == byRequiredFlags)
		{
			iVerticesRemoved++;
			DeleteVertex(lpmap, i);
		}
	}

	FreeUsedMapObjectMatrix(lpbUsedVertices);

	return iVerticesRemoved;
}







/* GetLinedefDisplayInfo
 *   Builds info-bar details for one linedef.
 *
 * Parameters:
 *   MAP*					lpmap			Map.
 *   CONFIG*				lpcfgLinedefs	Flat linedef effect descriptions.
 *   int					iIndex			Index of linedef.
 *   LINEDEFDISPLAYINFO*	lplddi			Pointer to return buffer.
 *
 * Return value: None.
 */
void GetLinedefDisplayInfo(MAP *lpmap, CONFIG *lpcfgLinedefFlat, int iIndex, LINEDEFDISPLAYINFO *lplddi)
{
	MAPLINEDEF			*lpld = &lpmap->linedefs[iIndex];
	MAPVERTEX			*lpv1 = &lpmap->vertices[lpld->v1];
	MAPVERTEX			*lpv2 = &lpmap->vertices[lpld->v2];
	MAPSIDEDEF			*lpsd1 = SidedefExists(lpmap, lpld->s1) ? &lpmap->sidedefs[lpld->s1] : NULL;
	MAPSIDEDEF			*lpsd2 = SidedefExists(lpmap, lpld->s2) ? &lpmap->sidedefs[lpld->s2] : NULL;
	MAPSECTOR			*lps1 = lpsd1 && lpsd1->sector >= 0 && lpsd1->sector < lpmap->iSectors ? &lpmap->sectors[lpsd1->sector] : NULL;
	MAPSECTOR			*lps2 = lpsd2 && lpsd2->sector >= 0 && lpsd2->sector < lpmap->iSectors ? &lpmap->sectors[lpsd2->sector] : NULL;

	lplddi->iIndex = iIndex;
	lplddi->cxVector = lpv2->x - lpv1->x;
	lplddi->cyVector = lpv2->y - lpv1->y;
	lplddi->unTag = lpld->tag;

	/* Ellipses are handled by the control's styles. */
	GetEffectDisplayText(lpld->effect, lpcfgLinedefFlat, lplddi->szEffect, sizeof(lplddi->szEffect) / sizeof(TCHAR), FALSE);
	lplddi->unEffect = lpld->effect;

	if((lplddi->bHasFrontSec = (lps1 != NULL)))
	{
		lplddi->unFrontSector = lpsd1->sector;
		lplddi->iFrontHeight = lps1->hceiling - lps1->hfloor;
	}

	if((lplddi->bHasBackSec = (lps2 != NULL)))
	{
		lplddi->unBackSector = lpsd2->sector;
		lplddi->iBackHeight = lps2->hceiling - lps2->hfloor;
	}

	if((lplddi->bHasFront = (lpsd1 != NULL)))
	{
		lplddi->nFrontX = lpsd1->tx;
		lplddi->nFrontY = lpsd1->ty;

		wsprintf(lplddi->szFrontUpper, TEXT("%.8hs"), lpsd1->upper);
		wsprintf(lplddi->szFrontMiddle, TEXT("%.8hs"), lpsd1->middle);
		wsprintf(lplddi->szFrontLower, TEXT("%.8hs"), lpsd1->lower);

		lplddi->szFrontUpper[8] = lplddi->szFrontMiddle[8] = lplddi->szFrontLower[8] = '\0';
	}

	if((lplddi->bHasBack = (lpsd2 != NULL)))
	{
		lplddi->nBackX = lpsd2->tx;
		lplddi->nBackY = lpsd2->ty;

		wsprintf(lplddi->szBackUpper, TEXT("%.8hs"), lpsd2->upper);
		wsprintf(lplddi->szBackMiddle, TEXT("%.8hs"), lpsd2->middle);
		wsprintf(lplddi->szBackLower, TEXT("%.8hs"), lpsd2->lower);

		lplddi->szBackUpper[8] = lplddi->szBackMiddle[8] = lplddi->szBackLower[8] = '\0';
	}
}



/* GetSectorDisplayInfo
 *   Builds info-bar details for one sector.
 *
 * Parameters:
 *   MAP*				lpmap			Map.
 *   CONFIG*			lpcfgSectors	Sector effects from map config.
 *   int				iIndex			Index of sector.
 *   SECTORDISPLAYINFO*	lpsdi			Pointer to return buffer.
 *   BOOL				bNybble			Whether effects are encoded by nybble.
 *
 * Return value: None.
 */
void GetSectorDisplayInfo(MAP *lpmap, CONFIG *lpcfgSectors, int iIndex, SECTORDISPLAYINFO *lpsdi, BOOL bNybble)
{
	MAPSECTOR *lpsec = &lpmap->sectors[iIndex];

	/* Ellipses are handled by the control's styles. */
	GetEffectDisplayText(lpsec->special, lpcfgSectors, lpsdi->szEffect, sizeof(lpsdi->szEffect) / sizeof(TCHAR), bNybble);
	lpsdi->unEffect = lpsec->special;

	lpsdi->iIndex = iIndex;
	lpsdi->ucBrightness = (unsigned char)lpsec->brightness;
	lpsdi->nCeiling = lpsec->hceiling;
	lpsdi->nFloor = lpsec->hfloor;
	lpsdi->unTag = lpsec->tag;

	wsprintf(lpsdi->szFloor, TEXT("%.8hs"), lpsec->tfloor);
	wsprintf(lpsdi->szCeiling, TEXT("%.8hs"), lpsec->tceiling);
	lpsdi->szFloor[8] = lpsdi->szFloor[8] = '\0';
}



/* GetThingDisplayInfo
 *   Builds info-bar details for one thing.
 *
 * Parameters:
 *   MAP*				lpmap		Map.
 *   CONFIG*			lpcfgThings	Thing properties (flat) from map config.
 *   int				iIndex		Index of thing.
 *   THINGDISPLAYINFO*	lptdi		Pointer to buffer in which to store data.
 *
 * Return value: None.
 */
void GetThingDisplayInfo(MAP *lpmap, CONFIG *lpcfgThings, int iIndex, THINGDISPLAYINFO *lptdi)
{
	MAPTHING *lpthing = &lpmap->things[iIndex];
	CONFIG *lpcfgThingProperties;

	GetThingTypeDisplayText(lpthing->thing, lpcfgThings, lptdi->szType, sizeof(lptdi->szType) / sizeof(TCHAR));
	lptdi->unType = lpthing->thing;

	lptdi->iIndex = iIndex;
	lptdi->x = lpthing->x;
	lptdi->y = lpthing->y;
	lptdi->z = (unsigned short)GetThingZ(lpmap, iIndex, lpcfgThings, FALSE);
	lptdi->unFlags = lpthing->flag;
	GetThingDirectionDisplayText(lpthing->angle, lptdi->szDirection, sizeof(lptdi->szDirection) / sizeof(TCHAR));
	lptdi->nDirection = lpthing->angle;
	lptdi->byParam = lpthing->typeuppernybble;

	if((lpcfgThingProperties = GetThingConfigInfo(lpcfgThings, (unsigned short)lpthing->thing)))
		ConfigGetString(lpcfgThingProperties, TEXT("sprite"), lptdi->szSprite, sizeof(lptdi->szSprite) / sizeof(TCHAR));
	else lptdi->szSprite[0] = '\0';
}



/* GetVertexDisplayInfo
 *   Builds info-bar details for one vertex.
 *
 * Parameters:
 *   MAP*						lpmap	Map.
 *   int						iIndex	Index of vertex.
 *   VERTEXDISPLAYINFO*	lpvdi	Pointer to buffer in which to store data.
 *
 * Return value: None.
 */
void GetVertexDisplayInfo(MAP *lpmap, int iIndex, VERTEXDISPLAYINFO *lpvdi)
{
	MAPVERTEX *lpvx = &lpmap->vertices[iIndex];

	lpvdi->iIndex = iIndex;
	lpvdi->x = lpvx->x;
	lpvdi->y = lpvx->y;
}



/* CheckLineCallback
 *   Sets linedef display information fields and determines which of those match
 *   with the existing data.
 *
 * Parameters:
 *   int		iIndex	Index of linedef.
 *   CHECKINFO*	lpci	Existing aggregate data, validity flags, field
 *						specifying whether this is the first iteration.
 *
 * Return value: BOOL
 *   TRUE if iteration should continue; FALSE otherwise.
 *
 * Remarks:
 *   This is used as a callback by IterateOverSelection. It is handy for
 *   building the set of intersecting properties for selected linedefs.
 */
static BOOL CheckLineCallback(int iIndex, CHECKINFO *lpci)
{
	LINEDEFDISPLAYINFO lddi;
	LINEDEFDISPLAYINFO *lplddiAggregate = (LINEDEFDISPLAYINFO*)lpci->lpdi;

	/* Get all the info for the specified linedef. */
	GetLinedefDisplayInfo(lpci->lpmap, lpci->lpcfg, iIndex, &lddi);

	/* We now have different behaviour depending on whether this is the first
	 * iteration.
	 */
	if(lpci->bFirstTime)
	{
		/* If this is the first time, everything's okay, so copy it all. */
		*lplddiAggregate = lddi;
		lpci->dwDisplayFlags = LDDIF_ALL;
		lpci->bFirstTime = FALSE;
	}
	else
	{
		/* We've iterated at least once already, so find what matches. We work
		 * subtractively.
		 */
		if(lddi.iIndex != lplddiAggregate->iIndex) lpci->dwDisplayFlags &= ~LDDIF_INDEX;
		if(lddi.unEffect != lplddiAggregate->unEffect) lpci->dwDisplayFlags &= ~LDDIF_EFFECT;
		if(lddi.cxVector != lplddiAggregate->cxVector || lddi.cyVector != lplddiAggregate->cyVector) lpci->dwDisplayFlags &= ~LDDIF_VECTOR;
		if(lddi.cxVector * lddi.cxVector + lddi.cyVector * lddi.cyVector !=
			lplddiAggregate->cxVector * lplddiAggregate->cxVector + lplddiAggregate->cyVector * lplddiAggregate->cyVector)
			lpci->dwDisplayFlags &= ~LDDIF_LENGTH;
		if(lddi.unTag != lplddiAggregate->unTag) lpci->dwDisplayFlags &= ~LDDIF_TAG;

		/* Now we have to be a bit careful. We have to negotiate the Boolean
		 * fields with consideration to their meaning. */
		if(lddi.bHasFrontSec != lplddiAggregate->bHasFrontSec) lpci->dwDisplayFlags &= ~(LDDIF_FRONTHEIGHT | LDDIF_FRONTSEC);
		else if(lddi.bHasFrontSec)
		{
			if(lddi.unFrontSector != lplddiAggregate->unFrontSector) lpci->dwDisplayFlags &= ~LDDIF_FRONTSEC;
			if(lddi.iFrontHeight != lplddiAggregate->iFrontHeight) lpci->dwDisplayFlags &= ~LDDIF_FRONTHEIGHT;
		}

		if(lddi.bHasBackSec != lplddiAggregate->bHasBackSec) lpci->dwDisplayFlags &= ~(LDDIF_BACKHEIGHT | LDDIF_BACKSEC);
		else if(lddi.bHasBackSec)
		{
			if(lddi.unBackSector != lplddiAggregate->unBackSector) lpci->dwDisplayFlags &= ~LDDIF_BACKSEC;
			if(lddi.iFrontHeight != lplddiAggregate->iFrontHeight) lpci->dwDisplayFlags &= ~LDDIF_BACKHEIGHT;
		}

		if(lddi.bHasFront != lplddiAggregate->bHasFront) lpci->dwDisplayFlags &= ~(LDDIF_FRONTX | LDDIF_FRONTY | LDDIF_HASFRONT);
		else if(lddi.bHasFront)
		{
			if(lddi.nFrontX != lplddiAggregate->nFrontX) lpci->dwDisplayFlags &= ~LDDIF_FRONTX;
			if(lddi.nFrontY != lplddiAggregate->nFrontY) lpci->dwDisplayFlags &= ~LDDIF_FRONTY;


			/* Texture comparisons now. We make the additional initial check as to
			 * whether we care about them anyway, since the string comparisons are
			 * quite expensive.
			 */

			if((lpci->dwDisplayFlags & LDDIF_FRONTUPPER) && _tcscmp(lddi.szFrontUpper, lplddiAggregate->szFrontUpper))
				 lpci->dwDisplayFlags &= ~LDDIF_FRONTUPPER;

			if((lpci->dwDisplayFlags & LDDIF_FRONTMIDDLE) && _tcscmp(lddi.szFrontMiddle, lplddiAggregate->szFrontMiddle))
				 lpci->dwDisplayFlags &= ~LDDIF_FRONTMIDDLE;

			if((lpci->dwDisplayFlags & LDDIF_FRONTLOWER) && _tcscmp(lddi.szFrontLower, lplddiAggregate->szFrontLower))
				 lpci->dwDisplayFlags &= ~LDDIF_FRONTLOWER;
		}

		if(lddi.bHasBack != lplddiAggregate->bHasBack) lpci->dwDisplayFlags &= ~(LDDIF_BACKX | LDDIF_BACKY | LDDIF_HASBACK);
		else if(lddi.bHasBack)
		{
			if(lddi.nBackX != lplddiAggregate->nBackX) lpci->dwDisplayFlags &= ~LDDIF_BACKX;
			if(lddi.nBackY != lplddiAggregate->nBackY) lpci->dwDisplayFlags &= ~LDDIF_BACKY;

			/* Texture comparisons now. We make the additional initial check as to
			 * whether we care about them anyway, since the string comparisons are
			 * quite expensive.
			 */

			if((lpci->dwDisplayFlags & LDDIF_BACKUPPER) && _tcscmp(lddi.szBackUpper, lplddiAggregate->szBackUpper))
				 lpci->dwDisplayFlags &= ~LDDIF_BACKUPPER;

			if((lpci->dwDisplayFlags & LDDIF_BACKMIDDLE) && _tcscmp(lddi.szBackMiddle, lplddiAggregate->szBackMiddle))
				 lpci->dwDisplayFlags &= ~LDDIF_BACKMIDDLE;

			if((lpci->dwDisplayFlags & LDDIF_BACKLOWER) && _tcscmp(lddi.szBackLower, lplddiAggregate->szBackLower))
				 lpci->dwDisplayFlags &= ~LDDIF_BACKLOWER;
		}
	}

	/* Keep going, unless we already know we have nothing in common. */
	return (lpci->dwDisplayFlags != 0);
}


DWORD CheckLines(MAP *lpmap, SELECTION_LIST *lpsellist, CONFIG *lpcfgLinedefTypesFlat, LINEDEFDISPLAYINFO *lplddi)
{
	CHECKINFO checkinfo;
	int i;

	/* Set the first-iteration field. */
	checkinfo.bFirstTime = TRUE;
	checkinfo.lpdi = (void*)lplddi;
	checkinfo.lpmap = lpmap;
	checkinfo.lpcfg = lpcfgLinedefTypesFlat;
	checkinfo.dwDisplayFlags = 0;


	for(i = 0; i < lpsellist->iDataCount; i++)
		CheckLineCallback(lpsellist->lpiIndices[i], &checkinfo);

	return checkinfo.dwDisplayFlags;
}


DWORD CheckSectors(MAP *lpmap, SELECTION_LIST *lpsellist, CONFIG *lpcfgSecTypes, SECTORDISPLAYINFO *lpsdi, BOOL bNybble)
{
	CHECKINFO checkinfo;
	int i;

	/* Set the first-iteration field. */
	checkinfo.bFirstTime = TRUE;
	checkinfo.lpdi = (void*)lpsdi;
	checkinfo.lpmap = lpmap;
	checkinfo.lpcfg = lpcfgSecTypes;
	checkinfo.dwDisplayFlags = 0;


	for(i = 0; i < lpsellist->iDataCount; i++)
		CheckSectorCallback(lpsellist->lpiIndices[i], &checkinfo, bNybble);

	return checkinfo.dwDisplayFlags;
}


DWORD CheckThings(MAP *lpmap, SELECTION_LIST *lpsellist, CONFIG *lpcfgFlatThings, THINGDISPLAYINFO *lptdi)
{
	CHECKINFO checkinfo;
	int i;

	/* Set the first-iteration field. */
	checkinfo.bFirstTime = TRUE;
	checkinfo.lpdi = (void*)lptdi;
	checkinfo.lpmap = lpmap;
	checkinfo.lpcfg = lpcfgFlatThings;
	checkinfo.dwDisplayFlags = 0;


	for(i = 0; i < lpsellist->iDataCount; i++)
		CheckThingCallback(lpsellist->lpiIndices[i], &checkinfo);

	return checkinfo.dwDisplayFlags;
}


DWORD CheckVertices(MAP *lpmap, SELECTION_LIST *lpsellist, VERTEXDISPLAYINFO *lpvdi)
{
	CHECKINFO checkinfo;
	int i;

	/* Set the first-iteration field. */
	checkinfo.bFirstTime = TRUE;
	checkinfo.lpdi = (void*)lpvdi;
	checkinfo.lpmap = lpmap;
	checkinfo.lpcfg = NULL;
	checkinfo.dwDisplayFlags = 0;

	for(i = 0; i < lpsellist->iDataCount; i++)
		CheckVerticesCallback(lpsellist->lpiIndices[i], &checkinfo);

	return checkinfo.dwDisplayFlags;
}


/* CheckSectorCallback
 *   Sets sector display information fields and determines which of those match
 *   with the existing data.
 *
 * Parameters:
 *   int			iIndex		Index of sector.
 *   CHECKINFO*		lpci		Existing aggregate data, validity flags, field
 *								specifying whether this is the first iteration.
 *   BOOL			bNybble		Whether effects are encoded by nybble.
 *
 * Return value: BOOL
 *   TRUE if iteration should continue; FALSE otherwise.
 *
 * Remarks:
 *   This is used as a callback by IterateOverSelection. It is handy for
 *   building the set of intersecting properties for selected sectors.
 */
static BOOL CheckSectorCallback(int iIndex, CHECKINFO *lpci, BOOL bNybble)
{
	SECTORDISPLAYINFO sdi;
	SECTORDISPLAYINFO *lpsdiAggregate = (SECTORDISPLAYINFO*)lpci->lpdi;

	/* Get all the info for the specified sector. */
	GetSectorDisplayInfo(lpci->lpmap, lpci->lpcfg, iIndex, &sdi, bNybble);

	/* We now have different behaviour depending on whether this is the first
	 * iteration.
	 */
	if(lpci->bFirstTime)
	{
		/* If this is the first time, everything's okay, so copy it all. */
		*lpsdiAggregate = sdi;
		lpci->dwDisplayFlags = SDIF_ALL;
		lpci->bFirstTime = FALSE;
	}
	else
	{
		/* We've iterated at least once already, so find what matches. We work
		 * subtractively.
		 */
		if(sdi.iIndex != lpsdiAggregate->iIndex) lpci->dwDisplayFlags &= ~SDIF_INDEX;
		if(sdi.nCeiling != lpsdiAggregate->nCeiling) lpci->dwDisplayFlags &= ~SDIF_CEILING;
		if(sdi.nFloor != lpsdiAggregate->nFloor) lpci->dwDisplayFlags &= ~SDIF_FLOOR;
		if(sdi.nCeiling - sdi.nFloor != lpsdiAggregate->nCeiling - lpsdiAggregate->nFloor)
			lpci->dwDisplayFlags &= ~SDIF_HEIGHT;
		if(sdi.unEffect != lpsdiAggregate->unEffect) lpci->dwDisplayFlags &= ~SDIF_EFFECT;
		if(sdi.ucBrightness != lpsdiAggregate->ucBrightness) lpci->dwDisplayFlags &= ~SDIF_BRIGHTNESS;
		if(sdi.unTag != lpsdiAggregate->unTag) lpci->dwDisplayFlags &= ~SDIF_TAG;


		/* Texture comparisons now. We make the additional initial check as to
		 * whether we care about them anyway, since the string comparisons are
		 * quite expensive.
		 */

		if((lpci->dwDisplayFlags & SDIF_CEILINGTEX) && _tcscmp(sdi.szCeiling, lpsdiAggregate->szCeiling))
			 lpci->dwDisplayFlags &= ~SDIF_CEILINGTEX;

		if((lpci->dwDisplayFlags & SDIF_FLOORTEX) && _tcscmp(sdi.szFloor, lpsdiAggregate->szFloor))
			 lpci->dwDisplayFlags &= ~SDIF_FLOORTEX;
	}

	/* Keep going, unless we already know we have nothing in common. */
	return (lpci->dwDisplayFlags != 0);
}



/* CheckThingCallback
 *   Sets thing display information fields and determines which of those match
 *   with the existing data.
 *
 * Parameters:
 *   int		iIndex	Index of sector.
 *   CHECKINFO*	lpci	Existing aggregate data, validity flags, field
 *						specifying whether this is the first iteration.
 *
 * Return value: BOOL
 *   TRUE if iteration should continue; FALSE otherwise.
 *
 * Remarks:
 *   This is used as a callback by IterateOverSelection. It is handy for
 *   building the set of intersecting properties for selected things.
 */
static BOOL CheckThingCallback(int iIndex, CHECKINFO *lpci)
{
	THINGDISPLAYINFO tdi;
	THINGDISPLAYINFO *lptdiAggregate = (THINGDISPLAYINFO*)lpci->lpdi;

	/* Get all the info for the specified sector. */
	GetThingDisplayInfo(lpci->lpmap, lpci->lpcfg, iIndex, &tdi);

	/* We now have different behaviour depending on whether this is the first
	 * iteration.
	 */
	if(lpci->bFirstTime)
	{
		/* If this is the first time, everything's okay, so copy it all. */
		*lptdiAggregate = tdi;
		lpci->dwDisplayFlags = TDIF_ALL;
		lpci->bFirstTime = FALSE;
	}
	else
	{
		/* We've iterated at least once already, so find what matches. We work
		 * subtractively.
		 */
		if(tdi.iIndex != lptdiAggregate->iIndex) lpci->dwDisplayFlags &= ~TDIF_INDEX;
		if(tdi.nDirection != lptdiAggregate->nDirection) lpci->dwDisplayFlags &= ~TDIF_DIRECTION;
		if(tdi.unType != lptdiAggregate->unType) lpci->dwDisplayFlags &= ~TDIF_TYPE;
		if(tdi.unFlags != lptdiAggregate->unFlags) lpci->dwDisplayFlags &= ~TDIF_FLAGS;
		if(tdi.z != lptdiAggregate->z) lpci->dwDisplayFlags &= ~TDIF_Z;
		if(tdi.x != lptdiAggregate->x) lpci->dwDisplayFlags &= ~TDIF_X;
		if(tdi.y != lptdiAggregate->y) lpci->dwDisplayFlags &= ~TDIF_Y;
		if(tdi.byParam != lptdiAggregate->byParam) lpci->dwDisplayFlags &= ~TDIF_PARAM;


		/* Sprite comparisons now. We make the additional initial check as to
		 * whether we care about them anyway, since the string comparisons are
		 * quite expensive.
		 */

		if((lpci->dwDisplayFlags & TDIF_SPRITE) && _tcscmp(tdi.szSprite, lptdiAggregate->szSprite))
			 lpci->dwDisplayFlags &= ~TDIF_SPRITE;
	}

	/* Keep going, unless we already know we have nothing in common. */
	return (lpci->dwDisplayFlags != 0);
}

/* CheckVerticesCallback
 *   Sets vertex display information fields and determines which of those match
 *   with the existing data.
 *
 * Parameters:
 *   int		iIndex	Index of sector.
 *   CHECKINFO*	lpci	Existing aggregate data, validity flags, field
 *						specifying whether this is the first iteration.
 *
 * Return value: BOOL
 *   TRUE if iteration should continue; FALSE otherwise.
 *
 * Remarks:
 *   This is used as a callback by IterateOverSelection. It is handy for
 *   building the set of intersecting properties for selected vertices.
 */
static BOOL CheckVerticesCallback(int iIndex, CHECKINFO *lpci)
{
	VERTEXDISPLAYINFO vdi;
	VERTEXDISPLAYINFO *lpvdiAggregate = (VERTEXDISPLAYINFO*)lpci->lpdi;

	/* Get all the info for the specified sector. */
	GetVertexDisplayInfo(lpci->lpmap, iIndex, &vdi);

	/* We now have different behaviour depending on whether this is the first
	 * iteration.
	 */
	if(lpci->bFirstTime)
	{
		/* If this is the first time, everything's okay, so copy it all. */
		*lpvdiAggregate = vdi;
		lpci->dwDisplayFlags = VDIF_ALL;
		lpci->bFirstTime = FALSE;
	}
	else
	{
		/* We've iterated at least once already, so find what matches. We work
		 * subtractively.
		 */
		if(vdi.iIndex != lpvdiAggregate->iIndex) lpci->dwDisplayFlags &= ~VDIF_INDEX;
		if(vdi.x != lpvdiAggregate->x || vdi.y != lpvdiAggregate->y) lpci->dwDisplayFlags &= ~VDIF_COORDS;
	}

	/* Keep going, unless we already know we have nothing in common. */
	return (lpci->dwDisplayFlags != 0);
}


/* CheckLineFlags
 *   For the selected linedefs, determines which flags are the same, and returns
 *   an indication of such, and the corresponding values.
 *
 * Parameters:
 *   MAP			*lpmap			Pointer to map data.
 *   SELECTION_LIST	*lpsellist		List of selected lines.
 *   WORD			*lpwFlagValues	Used to return values of shared flags.
 *   WORD			*lpwFlagMask	Used to return which of the flags in the
 *									above are valid.
 *
 * Return value: None.
 */
void CheckLineFlags(MAP *lpmap, SELECTION_LIST *lpsellist, WORD *lpwFlagValues, WORD *lpwFlagMask)
{
	int i;

	if(lpsellist->iDataCount <= 0)
	{
		*lpwFlagMask = 0;
		return;
	}

	/* Initially, the flags are equal to those of the first linedef, and they're
	 * all valid.
	 */
	*lpwFlagValues = lpmap->linedefs[lpsellist->lpiIndices[0]].flags;
	*lpwFlagMask = 0xFFFF;

	/* Loop through the rest of them, removing any flags that disagree. */
	for(i = 1; i < lpsellist->iDataCount && *lpwFlagMask; i++)
		*lpwFlagMask &= ~(*lpwFlagValues ^ lpmap->linedefs[lpsellist->lpiIndices[i]].flags);
}


/* SetLineFlags
 *   For the selected linedefs, sets the specified flags.
 *
 * Parameters:
 *   MAP			*lpmap			Pointer to map data.
 *   SELECTION_LIST	*lpsellist		List of selected lines.
 *   WORD			wFlagValues		Values of flags.
 *   WORD			wFlagMask		Mask indicating which bits in wFlagValues
 *									are valid.
 *
 * Return value: None.
 */
void SetLineFlags(MAP *lpmap, SELECTION_LIST *lpsellist, WORD wFlagValues, WORD wFlagMask)
{
	int i;

	/* Repeat for each selected linedef. */
	for(i = 0; i < lpsellist->iDataCount; i++)
	{
		/* Set the flags that should be set. */
		lpmap->linedefs[lpsellist->lpiIndices[i]].flags |= wFlagValues & wFlagMask;

		/* Reset the flags that should be reset. */
		lpmap->linedefs[lpsellist->lpiIndices[i]].flags &= wFlagValues | ~wFlagMask;
	}
}


/* CheckThingFlags
 *   For the selected things, determines which flags are the same, and returns
 *   an indication of such, and the corresponding values.
 *
 * Parameters:
 *   MAP			*lpmap			Pointer to map data.
 *   SELECTION_LIST	*lpsellist		List of selected things.
 *   WORD			*lpwFlagValues	Used to return values of shared flags.
 *   WORD			*lpwFlagMask	Used to return which of the flags in the
 *									above are valid.
 *
 * Return value: None.
 */
void CheckThingFlags(MAP *lpmap, SELECTION_LIST *lpsellist, WORD *lpwFlagValues, WORD *lpwFlagMask)
{
	int i;

	if(lpsellist->iDataCount <= 0)
	{
		*lpwFlagMask = 0;
		return;
	}

	/* Initially, the flags are equal to those of the first thing, and they're
	 * all valid.
	 */
	*lpwFlagValues = lpmap->things[lpsellist->lpiIndices[0]].flag;
	*lpwFlagMask = 0xFFFF;

	/* Loop through the rest of them, removing any flags that disagree. */
	for(i = 1; i < lpsellist->iDataCount && *lpwFlagMask; i++)
		*lpwFlagMask &= ~(*lpwFlagValues ^ lpmap->things[lpsellist->lpiIndices[i]].flag);
}


/* SetThingFlags
 *   For the selected things, sets the specified flags.
 *
 * Parameters:
 *   MAP			*lpmap			Pointer to map data.
 *   SELECTION_LIST	*lpsellist		List of selected things.
 *   WORD			wFlagValues		Values of flags.
 *   WORD			wFlagMask		Mask indicating which bits in wFlagValues
 *									are valid.
 *
 * Return value: None.
 */
void SetThingFlags(MAP *lpmap, SELECTION_LIST *lpsellist, WORD wFlagValues, WORD wFlagMask)
{
	int i;

	/* Repeat for each selected linedef. */
	for(i = 0; i < lpsellist->iDataCount; i++)
	{
		/* Reset the flags that should be reset. */
		lpmap->things[lpsellist->lpiIndices[i]].flag &= ~wFlagMask;

		/* Set the flags that should be set. */
		lpmap->things[lpsellist->lpiIndices[i]].flag |= wFlagValues;
	}
}


/* CheckTextureFlags
 *   Determines which textures are required for all of the selected linedefs.
 *
 * Parameters:
 *   MAP			*lpmap			Pointer to map data.
 *   SELECTION_LIST	*lpsellist		List of selected linedefs.
 *   LPCTSTR		szSky			Sky texture, or empty string for none.
 *
 * Return value: BYTE
 *   Flags indicating which textures are required by all lines in the selection.
 */
BYTE CheckTextureFlags(MAP *lpmap, SELECTION_LIST *lpsellist, LPCTSTR szSky)
{
	int i;
	BYTE byFlags = 0xFF;

	/* No selection means no textures required. */
	if(lpsellist->iDataCount <= 0) return 0;

	/* Loop through the lines, removing any flags that disagree. */
	for(i = 0; i < lpsellist->iDataCount && byFlags; i++)
		byFlags &= RequiredTextures(lpmap, lpsellist->lpiIndices[i], szSky);

	return byFlags;
}


/* NextUnusedTag
 *   Finds the first tag value that is unused in a map.
 *
 * Parameters:
 *   MAP*	lpmap	Pointer to map data.
 *
 * Return value: unsigned short
 *   Value of tag.
 */
unsigned short NextUnusedTag(MAP *lpmap)
{
	/* Fastest way I can think of doing it is sorting all used tags and finding
	 * the first hole.
	 */

	int cn = lpmap->iLinedefs + lpmap->iSectors;		/* Count of tags. */
	unsigned short *lpunTags;
	int i;
	short unNextTag;

	/* Empty map? */
	if(cn == 0) return 0;

	/* Allocate memory. */
	lpunTags = ProcHeapAlloc(cn * sizeof(unsigned short));

	/* Loop through sectors and linedefs, adding their tags to the array. */
	for(i=0; i < lpmap->iSectors; i++)
		lpunTags[i] = lpmap->sectors[i].tag;

	for(; i < cn; i++)
		lpunTags[i] = lpmap->linedefs[i - lpmap->iSectors].tag;

	/* Sort the array. */
	qsort(lpunTags, cn, sizeof(unsigned short), QsortUShortComparison);

	/* Find the first hole in the list. Consecutive tags may be equal or differ
	 * by 1 without creating a hole. Stop if we get to the end with no hole.
	 */
	i = 0;
	while(i < cn - 1 && lpunTags[i+1] - lpunTags[i] <= 1) i++;

	/* Return the first integer that fits in the hole, or past the end if no
	 * hole.
	 */
	unNextTag = lpunTags[i] + 1;

	ProcHeapFree(lpunTags);

	return unNextTag;
}


/* GetAdjacentSectorHeightRange
 *   Finds the range of ceiling and floor heights of sectors adjacent to the
 *   selection.
 *
 * Parameters:
 *   MAP*	lpmap			Pointer to map data.
 *   short*	lpnMaxCeil etc.	Buffers to return range in.
 *
 * Return value: BOOL
 *   FALSE if there were no sectors adjacent to the selection, or if there was
 *   no selection; TRUE otherwise.
 */
BOOL GetAdjacentSectorHeightRange(MAP *lpmap, short *lpnMaxCeil, short *lpnMinCeil, short *lpnMaxFloor, short *lpnMinFloor)
{
	short nMinFloor =  32767, nMinCeil =  32767;
	short nMaxFloor = -32768, nMaxCeil = -32768;
	int i;
	BOOL bConsideredAnySectors = FALSE;

	/* All the adjacent sectors will contain at least one selected linedef. Loop
	 * through all linedefs and look at the relevant selected ones.
	 */
	for(i = 0; i < lpmap->iLinedefs; i++)
	{
		/* Linedef is selected? */
		if(lpmap->linedefs[i].selected)
		{
			/* If a sector bounded by this ld is unselected, it's adjacent. */
			if(SidedefExists(lpmap, lpmap->linedefs[i].s1))
			{
				MAPSECTOR *lpsec = &lpmap->sectors[lpmap->sidedefs[lpmap->linedefs[i].s1].sector];

				if(!lpsec->selected)
				{
					/* Check whether we need to update the range for this sector. */
					if(lpsec->hfloor < nMinFloor) nMinFloor = lpsec->hfloor;
					if(lpsec->hfloor > nMaxFloor) nMaxFloor = lpsec->hfloor;
					if(lpsec->hceiling < nMinCeil) nMinCeil = lpsec->hceiling;
					if(lpsec->hceiling > nMaxCeil) nMaxCeil = lpsec->hceiling;

					bConsideredAnySectors = TRUE;
				}
			}

			if(SidedefExists(lpmap, lpmap->linedefs[i].s2))
			{
				MAPSECTOR *lpsec = &lpmap->sectors[lpmap->sidedefs[lpmap->linedefs[i].s2].sector];

				if(!lpsec->selected)
				{
					/* Check whether we need to update the range for this sector. */
					if(lpsec->hfloor < nMinFloor) nMinFloor = lpsec->hfloor;
					if(lpsec->hfloor > nMaxFloor) nMaxFloor = lpsec->hfloor;
					if(lpsec->hceiling < nMinCeil) nMinCeil = lpsec->hceiling;
					if(lpsec->hceiling > nMaxCeil) nMaxCeil = lpsec->hceiling;

					bConsideredAnySectors = TRUE;
				}
			}
		}
	}

	*lpnMaxCeil = nMaxCeil;
	*lpnMinCeil = nMinCeil;
	*lpnMaxFloor = nMaxFloor;
	*lpnMinFloor = nMinFloor;

	return bConsideredAnySectors;
}


/* GetMapRect
 *   Obtains a bounding rectangle for a map in map co-ordinates.
 *
 * Parameters:
 *   MAP*	lpmap	Pointer to map data.
 *   RECT*	lprc	Rectangle structure to return into.
 *
 * Return value: None.
 */
void GetMapRect(MAP *lpmap, RECT *lprc)
{
	int i;

	/* Sanity check: empty map? */
	if(lpmap->iVertices == 0 && lpmap->iThings == 0)
	{
		/* Return zero rectangle. */
		lprc->bottom = lprc->left = lprc->right = lprc->top = 0;
		return;
	}


	/* Only need to consider vertices and things. */

	/* Set initial rectangle to be a point at the position of the first vertex,
	 * or the first thing if there are no vertices.
	 */
	if(lpmap->iVertices > 0)
	{
		lprc->left = lprc->right = lpmap->vertices[0].x;
		lprc->bottom = lprc->top = lpmap->vertices[0].y;
	}
	else
	{
		lprc->left = lprc->right = lpmap->things[0].x;
		lprc->bottom = lprc->top = lpmap->things[0].y;
	}

	/* Loop through all things and vertices, extending the rectangle as
	 * necessary.
	 */
	for(i = 0; i < lpmap->iVertices; i++)
	{
		if(lpmap->vertices[i].x < lprc->left) lprc->left = lpmap->vertices[i].x;
		if(lpmap->vertices[i].x > lprc->right) lprc->right = lpmap->vertices[i].x;
		if(lpmap->vertices[i].y < lprc->bottom) lprc->bottom = lpmap->vertices[i].y;
		if(lpmap->vertices[i].y > lprc->top) lprc->top = lpmap->vertices[i].y;
	}

	for(i = 0; i < lpmap->iThings; i++)
	{
		if(lpmap->things[i].x < lprc->left) lprc->left = lpmap->things[i].x;
		if(lpmap->things[i].x > lprc->right) lprc->right = lpmap->things[i].x;
		if(lpmap->things[i].y < lprc->bottom) lprc->bottom = lpmap->things[i].y;
		if(lpmap->things[i].y > lprc->top) lprc->top = lpmap->things[i].y;
	}
}


/* GetSelectionRect
 *   Obtains a bounding rectangle for a selection in map co-ordinates, using
 *   only selected things and vertices.
 *
 * Parameters:
 *   MAP*		lpmap			Pointer to map data.
 *   SELECTION*	lpselection		Selection.
 *   RECT*		lprc			Rectangle structure to return into.
 *
 * Return value: None.
 *
 * Remarks:
 *   Intended for use with an auxiliary selection.
 */
void GetSelectionRect(MAP *lpmap, SELECTION *lpselection, RECT *lprc)
{
	int i;
	int *lpiVertexIndices = lpselection->lpsellistVertices->lpiIndices;
	int *lpiThingIndices = lpselection->lpsellistThings->lpiIndices;

	/* Sanity check: empty selection? */
	if(lpselection->lpsellistVertices->iDataCount == 0 && lpselection->lpsellistThings->iDataCount == 0)
	{
		/* Return zero rectangle. */
		lprc->bottom = lprc->left = lprc->right = lprc->top = 0;
		return;
	}

	/* Set initial rectangle to be a point at the position of the first vertex,
	 * or the first thing if there are no vertices.
	 */
	if(lpselection->lpsellistVertices->iDataCount > 0)
	{
		lprc->left = lprc->right = lpmap->vertices[lpiVertexIndices[0]].x;
		lprc->bottom = lprc->top = lpmap->vertices[lpiVertexIndices[0]].y;
	}
	else
	{
		lprc->left = lprc->right = lpmap->things[lpiThingIndices[0]].x;
		lprc->bottom = lprc->top = lpmap->things[lpiThingIndices[0]].y;
	}

	/* Loop through all things and vertices, extending the rectangle as
	 * necessary.
	 */
	for(i = 0; i < lpselection->lpsellistVertices->iDataCount; i++)
	{
		MAPVERTEX *lpvx = &lpmap->vertices[lpiVertexIndices[i]];

		if(lpvx->x < lprc->left) lprc->left = lpvx->x;
		if(lpvx->x > lprc->right) lprc->right = lpvx->x;
		if(lpvx->y < lprc->bottom) lprc->bottom = lpvx->y;
		if(lpvx->y > lprc->top) lprc->top = lpvx->y;
	}

	for(i = 0; i < lpselection->lpsellistThings->iDataCount; i++)
	{
		MAPTHING *lpthing = &lpmap->things[lpiThingIndices[i]];

		if(lpthing->x < lprc->left) lprc->left = lpthing->x;
		if(lpthing->x > lprc->right) lprc->right = lpthing->x;
		if(lpthing->y < lprc->bottom) lprc->bottom = lpthing->y;
		if(lpthing->y > lprc->top) lprc->top = lpthing->y;
	}
}


/* ApplySectorPropertiesToSelection
 *   Applies the specified properties to multiple sectors.
 *
 * Parameters:
 *   MAP*				lpmap		Pointer to map data.
 *   SELECTION_LIST*	lpsellist	Selection specifying the sectors to modify.
 *   SECTORDISPLAYINFO*	lpsdi		Structure containing properties to apply.
 *   DWORD				dwFlags		Flags specifying which fields of sdi are
 *									valid.
 *
 * Return value: None.
 */
void ApplySectorPropertiesToSelection(MAP *lpmap, SELECTION_LIST *lpsellist, SECTORDISPLAYINFO *lpsdi, DWORD dwFlags)
{
	int i;

	for(i = 0; i < lpsellist->iDataCount; i++)
		ApplySectorProperties(lpmap, lpsellist->lpiIndices[i], lpsdi, dwFlags);
}


/* ApplySectorProperties
 *   Applies the specified properties to a sector.
 *
 * Parameters:
 *   MAP*				lpmap		Pointer to map data.
 *   int				iIndex		Index of sector to modify.
 *   SECTORDISPLAYINFO*	lpsdi		Structure containing properties to apply.
 *   DWORD				dwFlags		Flags specifying which fields of sdi are
 *									valid.
 *
 * Return value: None.
 */
void ApplySectorProperties(MAP *lpmap, int iIndex, SECTORDISPLAYINFO *lpsdi, DWORD dwFlags)
{
	MAPSECTOR *lpsec = &lpmap->sectors[iIndex];
	/* Check whether each field is valid, and apply it if so. */

	if(dwFlags & SDIF_EFFECT) lpsec->special = lpsdi->unEffect;
	if(dwFlags & SDIF_CEILING) lpsec->hceiling = lpsdi->nCeiling;
	if(dwFlags & SDIF_FLOOR) lpsec->hfloor = lpsdi->nFloor;
	if(dwFlags & SDIF_TAG) lpsec->tag = lpsdi->unTag;
	if(dwFlags & SDIF_BRIGHTNESS) lpsec->brightness = lpsdi->ucBrightness;

	if(dwFlags & SDIF_CEILINGTEX)
		FillTexNameBuffer(lpsec->tceiling, lpsdi->szCeiling);

	if(dwFlags & SDIF_FLOORTEX)
		FillTexNameBuffer(lpsec->tfloor, lpsdi->szFloor);
}



/* ApplyLinePropertiesToSelection
 *   Applies the specified properties to multiple linedefs, and their associated
 *   sidedefs.
 *
 * Parameters:
 *   MAP*					lpmap		Pointer to map data.
 *   SELECTION_LIST*		lpsellist	Selection specifying the linedefs to
 *										modify.
 *   LINEDEFDISPLAYINFO*	lplddi		Structure containing properties to
 *										apply.
 *   DWORD					dwFlags		Flags specifying which fields of lddi
 *										are valid.
 *
 * Return value: None.
 *
 * Remarks:
 *   Linedef flags are not handled here. See SetLineFlags.
 */
void ApplyLinePropertiesToSelection(MAP *lpmap, SELECTION_LIST *lpsellist, LINEDEFDISPLAYINFO *lplddi, DWORD dwFlags)
{
	int i;

	for(i = 0; i < lpsellist->iDataCount; i++)
		ApplyLineProperties(lpmap, lpsellist->lpiIndices[i], lplddi, dwFlags);
}


/* ApplyLineProperties
 *   Applies the specified properties to a linedef and its sidedefs.
 *
 * Parameters:
 *   MAP*					lpmap		Pointer to map data.
 *   int					iIndex		Index of linedef to modify.
 *   LINEDEFDISPLAYINFO*	lplddi		Structure containing properties to
 *										apply.
 *   DWORD					dwFlags		Flags specifying which fields of lddi
 *										are valid.
 *
 * Return value: None.
 *
 * Remarks:
 *   Linedef flags are not handled here. See SetLineFlags.
 */
void ApplyLineProperties(MAP *lpmap, int iIndex, LINEDEFDISPLAYINFO *lplddi, DWORD dwFlags)
{
	MAPLINEDEF *lpld = &lpmap->linedefs[iIndex];

	/* Check whether each field is valid, and apply it if so. */

	if(dwFlags & LDDIF_EFFECT) lpld->effect = lplddi->unEffect;
	if(dwFlags & LDDIF_TAG) lpld->tag = lplddi->unTag;

	/* Front sidedef. */
	if(SidedefExists(lpmap, lpld->s1))
	{
		MAPSIDEDEF *lpsd = &lpmap->sidedefs[lpld->s1];

		if(dwFlags & LDDIF_FRONTX) lpsd->tx = lplddi->nFrontX;
		if(dwFlags & LDDIF_FRONTY) lpsd->ty = lplddi->nFrontY;
		if(dwFlags & LDDIF_FRONTSEC) lpsd->sector = lplddi->unFrontSector;

		if(dwFlags & LDDIF_FRONTUPPER)
			FillTexNameBuffer(lpsd->upper, lplddi->szFrontUpper);

		if(dwFlags & LDDIF_FRONTMIDDLE)
			FillTexNameBuffer(lpsd->middle, lplddi->szFrontMiddle);

		if(dwFlags & LDDIF_FRONTLOWER)
			FillTexNameBuffer(lpsd->lower, lplddi->szFrontLower);
	}

	/* Back sidedef. */
	if(SidedefExists(lpmap, lpld->s2))
	{
		MAPSIDEDEF *lpsd = &lpmap->sidedefs[lpld->s2];

		if(dwFlags & LDDIF_BACKX) lpsd->tx = lplddi->nBackX;
		if(dwFlags & LDDIF_BACKY) lpsd->ty = lplddi->nBackY;
		if(dwFlags & LDDIF_BACKSEC) lpsd->sector = lplddi->unBackSector;

		if(dwFlags & LDDIF_BACKUPPER)
			FillTexNameBuffer(lpsd->upper, lplddi->szBackUpper);

		if(dwFlags & LDDIF_BACKMIDDLE)
			FillTexNameBuffer(lpsd->middle, lplddi->szBackMiddle);

		if(dwFlags & LDDIF_BACKLOWER)
			FillTexNameBuffer(lpsd->lower, lplddi->szBackLower);
	}
}


/* ApplyThingPropertiesToSelection
 *   Applies the specified properties to multiple sectors.
 *
 * Parameters:
 *   MAP*				lpmap				Pointer to map data.
 *   SELECTION_LIST*	lpsellist			Selection specifying the sectors to
 *											modify.
 *   THINGDISPLAYINFO*	lptdi				Structure containing properties to
 *											apply.
 *   DWORD				dwFlags				Flags specifying which fields of
 *											lptdi are valid.
 *   CONFIG*			lpcfgFlatThings		Flat things subsection.
 *
 * Return value: None.
 *
 * Remarks:
 *   Also updates the fields that are dependent on the thing type: colour etc.
 */
void ApplyThingPropertiesToSelection(MAP *lpmap, SELECTION_LIST *lpsellist, THINGDISPLAYINFO *lptdi, DWORD dwFlags, CONFIG *lpcfgFlatThings)
{
	int i;

	for(i = 0; i < lpsellist->iDataCount; i++)
	{
		ApplyThingProperties(lpmap, lpsellist->lpiIndices[i], lptdi, dwFlags);

		/* The type might have changed, so update properties dependent on this.
		 */
		SetThingPropertiesFromType(lpmap, lpsellist->lpiIndices[i], lpcfgFlatThings);
	}
}


/* ApplyThingProperties
 *   Applies the specified properties to a thing.
 *
 * Parameters:
 *   MAP*				lpmap		Pointer to map data.
 *   int				iIndex		Index of sector to modify.
 *   THINGDISPLAYINFO*	lptdi		Structure containing properties to apply.
 *   DWORD				dwFlags		Flags specifying which fields of sdi are
 *									valid.
 *
 * Return value: None.
 */
void ApplyThingProperties(MAP *lpmap, int iIndex, THINGDISPLAYINFO *lptdi, DWORD dwFlags)
{
	MAPTHING *lpthing = &lpmap->things[iIndex];
	/* Check whether each field is valid, and apply it if so. */

	if(dwFlags & TDIF_TYPE) lpthing->thing = lptdi->unType;
	if(dwFlags & TDIF_DIRECTION) lpthing->angle = lptdi->nDirection;
	if(dwFlags & TDIF_X) lpthing->x = lptdi->x;
	if(dwFlags & TDIF_Y) lpthing->y = lptdi->y;
	if(dwFlags & TDIF_PARAM) lpthing->typeuppernybble = lptdi->byParam;
}


/* AddVertex
 *   Adds a new vertex to a map.
 *
 * Parameters:
 *   MAP*	lpmap		Pointer to map data.
 *   short	x, y		Co-ordinates of new vertex.
 *
 * Return value: int
 *   Index of new vertex.
 *
 * Remarks:
 *   No snapping is done here. That's the caller's problem.
 */
int AddVertex(MAP *lpmap, short x, short y)
{
	MAPVERTEX *lpvertexNew;

	/* TODO: Realloc space if necessary? */
	if(lpmap->iVertices >= MAX_VERTICES) return -1;

	/* Get pointer to new vertex. */
	lpvertexNew = &lpmap->vertices[lpmap->iVertices];

	/* Initialise fields that should be zero. */
	ZeroMemory(lpvertexNew, sizeof(MAPVERTEX));

	/* Set position of new vertex. */
	lpvertexNew->x = x;
	lpvertexNew->y = y;

	return lpmap->iVertices++;
}


/* AddThing
 *   Adds a new thing to a map.
 *
 * Parameters:
 *   MAP*	lpmap		Pointer to map data.
 *   short	x, y		Co-ordinates of new thing.
 *
 * Return value: int
 *   Index of new thing.
 *
 * Remarks:
 *   No snapping is done here. That's the caller's problem. Also, while some
 *   fields are set to sensible defaults to stop crashes, they're probably not
 *   really what you want.
 */
int AddThing(MAP *lpmap, short x, short y)
{
	MAPTHING *lpthing;

	/* TODO: Realloc space if necessary? */
	if(lpmap->iThings >= MAX_THINGS) return -1;

	/* Get pointer to new vertex. */
	lpthing = &lpmap->things[lpmap->iThings];

	ZeroMemory(lpthing, sizeof(MAPTHING));

	/* Set position of new thing. */
	lpthing->x = x;
	lpthing->y = y;

	/* We don't actually use this field yet. */
#if 0
	/* Initialise other fields. */
	lpthing->sector = IntersectSector(x, y, lpmap, NULL, NULL);
#endif

	return lpmap->iThings++;
}


/* AddLinedef
 *   Adds a new linedef to a map.
 *
 * Parameters:
 *   MAP*	lpmap						Pointer to map data.
 *   int	iVertexStart, iVertexEnd	Vertices for linedef.
 *
 * Return value: int
 *   Index of new linedef.
 */
int AddLinedef(MAP *lpmap, int iVertexStart, int iVertexEnd)
{
	MAPLINEDEF *lplinedefNew;

	/* TODO: Realloc space if necessary? */
	if(lpmap->iLinedefs >= MAX_LINEDEFS) return -1;

	/* Get pointer to new linedef. */
	lplinedefNew = &lpmap->linedefs[lpmap->iLinedefs];

	/* Zero all fields to begin with. */
	ZeroMemory(lplinedefNew, sizeof(MAPLINEDEF));

	/* Set vertices for new linedef. */
	lplinedefNew->v1 = iVertexStart;
	lplinedefNew->v2 = iVertexEnd;

	/* Initialise other fields. */
	lplinedefNew->s1 = lplinedefNew->s2 = INVALID_SIDEDEF;

	return lpmap->iLinedefs++;
}


/* AddSector
 *   Adds a new sector to a map.
 *
 * Parameters:
 *   MAP*	lpmap		Pointer to map data.
 *
 * Return value: int
 *   Index of new sector.
 */
int AddSector(MAP *lpmap)
{
	MAPSECTOR *lpsecNew;

	/* TODO: Realloc space if necessary? */
	if(lpmap->iSectors >= MAX_SECTORS) return -1;

	/* Get pointer to new sector. */
	lpsecNew = &lpmap->sectors[lpmap->iSectors];

	/* Initialise it. */
	ZeroMemory(lpsecNew, sizeof(MAPSECTOR));

	return lpmap->iSectors++;
}


/* AddSidedef
 *   Adds a new sidedef to a map.
 *
 * Parameters:
 *   MAP*	lpmap		Pointer to map data.
 *   int	iSector		Sector to reference.
 *
 * Return value: int
 *   Index of new sidedef.
 */
int AddSidedef(MAP *lpmap, int iSector)
{
	MAPSIDEDEF *lpsdNew;

	/* TODO: Realloc space if necessary? */
	if(lpmap->iSidedefs >= MAX_SIDEDEFS) return INVALID_SIDEDEF;

	/* Get pointer to new sector. */
	lpsdNew = &lpmap->sidedefs[lpmap->iSidedefs];

	/* Zero all fields to begin with. */
	ZeroMemory(lpsdNew, sizeof(MAPSIDEDEF));

	/* Set some sensible defaults. */
	lpsdNew->lower[0] = '-';
	lpsdNew->middle[0] = '-';
	lpsdNew->upper[0] = '-';
	lpsdNew->sector = iSector;

	return lpmap->iSidedefs++;
}


/* BeginDrawOperation
 *   Intialises a draw-buffer structure.
 *
 * Parameters:
 *   DRAW_OPERATION*	lpdrawop	Pointer to draw-op structure.
 *
 * Return value: None.
 *
 * Remarks:
 *   Call EndDrawOperation to free memory when you're finished.
 */
void BeginDrawOperation(DRAW_OPERATION *lpdrawop, MAP *lpmap)
{
	/* Allocate buffers for new vertices and lines. */
	lpdrawop->lpiNewLines = ProcHeapAlloc(DRAWOP_BUFFER_COUNT_INCREMENT * sizeof(int));
	lpdrawop->lpiNewVertices = ProcHeapAlloc(DRAWOP_BUFFER_COUNT_INCREMENT * sizeof(int));

	/* Store buffer sizes. */
	lpdrawop->ciLineBuffer = lpdrawop->ciVertexBuffer = DRAWOP_BUFFER_COUNT_INCREMENT;

	/* They're empty to start with. */
	lpdrawop->iNewLineCount = lpdrawop->iNewVertexCount = 0;

	/* Initialise state stacks. */
	InitialiseDynamicIntArray(&lpdrawop->diarrayLDStateStack, DRAWOPSTATESTACK_INITBUFSIZE);
	InitialiseDynamicIntArray(&lpdrawop->diarrayVxStateStack, DRAWOPSTATESTACK_INITBUFSIZE);

	/* Create MOGs. */
	lpdrawop->lpmogVertices = CreateVertexMOG(lpmap);
	lpdrawop->lpmogLinedefs = CreateLinedefMOG(lpmap);
}



/* EndDrawOperation
 *   Frees memory used in a drawing operation, sets default textures on new
 *   sidedefs and unmarks 'new' flags from new lines and vertices.
 *
 * Parameters:
 *   MAP*				lpmap			Pointer to map data.
 *   DRAW_OPERATION*	lpdrawop		Pointer to draw-op structure.
 *   CONFIG*			lpcfgWadOptMap	Wad options for map, for defaults.
 *   LPCTSTR			szSky			Sky flat, or empty string for none.
 *
 * Return value: None.
 */
void EndDrawOperation(MAP *lpmap, DRAW_OPERATION *lpdrawop, CONFIG *lpcfgWadOptMap, LPCTSTR szSky)
{
	int i;
	CONFIG *lpcfgMapDefTex = ConfigGetSubsection(lpcfgWadOptMap, WADOPT_DEFAULTTEX);

	for(i = 0; i < lpdrawop->iNewLineCount; i++)
	{
		MAPLINEDEF *lpld = &lpmap->linedefs[lpdrawop->lpiNewLines[i]];
		BYTE byReqFlags = RequiredTextures(lpmap, lpdrawop->lpiNewLines[i], szSky);
		TCHAR szTexName[TEXNAME_BUFFER_LENGTH];

		/* Despite our best efforts, we're sometimes left with single-sided
		 * lines with only a back sidedef at this point. Fix them.
		 */
		if(!SidedefExists(lpmap, lpld->s1) && SidedefExists(lpmap, lpld->s2))
			FlipLinedef(lpmap, lpdrawop->lpiNewLines[i]);

		/* Set missing textures. */

		if(byReqFlags & (LDTF_FRONTUPPER | LDTF_BACKUPPER))
		{
			ConfigGetString(lpcfgMapDefTex, TEXT("upper"), szTexName, TEXNAME_BUFFER_LENGTH);

			if(byReqFlags & LDTF_FRONTUPPER)
				FillTexNameBuffer(lpmap->sidedefs[lpld->s1].upper, szTexName);

			if(byReqFlags & LDTF_BACKUPPER)
				FillTexNameBuffer(lpmap->sidedefs[lpld->s2].upper, szTexName);
		}

		if(byReqFlags & (LDTF_FRONTMIDDLE | LDTF_BACKMIDDLE))
		{
			ConfigGetString(lpcfgMapDefTex, TEXT("middle"), szTexName, TEXNAME_BUFFER_LENGTH);

			if(byReqFlags & LDTF_FRONTMIDDLE)
				FillTexNameBuffer(lpmap->sidedefs[lpld->s1].middle, szTexName);

			if(byReqFlags & LDTF_BACKMIDDLE)
				FillTexNameBuffer(lpmap->sidedefs[lpld->s2].middle, szTexName);
		}

		if(byReqFlags & (LDTF_FRONTLOWER | LDTF_BACKLOWER))
		{
			ConfigGetString(lpcfgMapDefTex, TEXT("lower"), szTexName, TEXNAME_BUFFER_LENGTH);

			if(byReqFlags & LDTF_FRONTLOWER)
				FillTexNameBuffer(lpmap->sidedefs[lpld->s1].lower, szTexName);

			if(byReqFlags & LDTF_BACKLOWER)
				FillTexNameBuffer(lpmap->sidedefs[lpld->s2].lower, szTexName);
		}
	}

	/* Unmark the new vertices. */
	for(i = 0; i < lpdrawop->iNewVertexCount; i++)
		lpmap->vertices[lpdrawop->lpiNewVertices[i]].editflags &= ~VEF_NEW;

	/* Unmark the new linedefs. */
	for(i = 0; i < lpdrawop->iNewLineCount; i++)
		lpmap->linedefs[lpdrawop->lpiNewLines[i]].editflags &= ~(LEF_NEW | LEF_INVALIDSIDEDEFS);

	ProcHeapFree(lpdrawop->lpiNewLines);
	ProcHeapFree(lpdrawop->lpiNewVertices);

	FreeDynamicIntArray(&lpdrawop->diarrayLDStateStack);
	FreeDynamicIntArray(&lpdrawop->diarrayVxStateStack);

	if(lpdrawop->lpmogVertices)
		DestroyMapObjGrid(lpdrawop->lpmogVertices);

	if(lpdrawop->lpmogLinedefs)
		DestroyMapObjGrid(lpdrawop->lpmogLinedefs);
}



/* DrawToNewVertex
 *   Draws another vertex and adds a line between it and the previous vertex.
 *
 * Parameters:
 *   MAP*				lpmap			Pointer to map structure.
 *   DRAW_OPERATION*	lpdrawop		Pointer to draw-op structure.
 *   CONFIG*			lpcfgWadOptMap	Wad options for map, for defaults.
 *   short				x, y			Co-ordinates of new vertex.
 *   BOOL				bStitch			Whether vertices should be stitched.
 *										Coincident vertices are always stitched.
 *
 * Return value: BOOL
 *   TRUE if drawing should continue; FALSE otherwise.
 *
 * Remarks:
 *   If a vertex already exists at (x, y), it is used instead. All the sneaky
 *   drawing stuff is/will be performed here: sector creation etc. If we return
 *   FALSE, the caller should call EndDrawOperation to clean up.
 */
BOOL DrawToNewVertex(MAP *lpmap, DRAW_OPERATION *lpdrawop, CONFIG *lpcfgWadOptMap, short x, short y, BOOL bStitch)
{
	int iVxDist;
	int iVertex;
	int iVertexPrev;
	int iLinedef;
	BOOL bExistingVertex;
	BOOL bKeepGoing = TRUE;

	/* Get index of previous vertex, or -1 if we're first. */
	iVertexPrev = (lpdrawop->iNewVertexCount > 0) ? lpdrawop->lpiNewVertices[lpdrawop->iNewVertexCount-1] : -1;

	/* Find the vertex nearest to where we're going. */
	iVertex = NearestVertex(lpmap, lpdrawop->lpmogVertices, x, y, &iVxDist);

	/* Stitch if enabled, but always stitch if we're bang on. */
	bExistingVertex = (iVertex >= 0 && iVxDist <= (bStitch ? ConfigGetInteger(g_lpcfgMain, OPT_AUTOSTITCHDISTANCE) : 0));

	/* No vertex within the stitching distance of that position yet? */
	if(!bExistingVertex)
	{
		int iNearestLD, iLDDist;

		/* Create a new vertex there. */
		iVertex = AddVertex(lpmap, x, y);

		/* Add it to the MOG if we have one. */
		if(lpdrawop->lpmogVertices) AddVertexToMOG(lpdrawop->lpmogVertices, lpmap, iVertex);

		/* This might trigger a line-split. Find the nearest linedef. */
		iNearestLD = NearestLinedef(lpmap, lpdrawop->lpmogLinedefs, x, y, &iLDDist);

		/* Split if we're close enough. */
		if(iNearestLD >= 0 && iLDDist <= ConfigGetInteger(g_lpcfgMain, OPT_LINESPLITDISTANCE))
		{
			int iNewLinedef = SplitLinedef(lpmap, iNearestLD, iVertex);

			/* Add *both* linedefs to the MOG if we have one -- this will result
			 * in two entries for the existing LD.
			 */
			if(lpdrawop->lpmogLinedefs)
			{
				AddLinedefToMOG(lpdrawop->lpmogLinedefs, lpmap, iNearestLD);
				AddLinedefToMOG(lpdrawop->lpmogLinedefs, lpmap, iNewLinedef);
			}

			/* If the linedef we split was newly-drawn, add the new linedef we
			 * just made to the list of new lines.
			 */
			if(lpmap->linedefs[iNearestLD].editflags & LEF_NEW)
				AddLinedefToDrawList(lpmap, lpdrawop, iNewLinedef);

			/* Since we're now drawing to a vertex within the existing
			 * structure, we have all the associated problems to worry about. To
			 * make sure that we handle them correctly, indicate that we're
			 * interfering with the layout.
			 */
			bExistingVertex = TRUE;
		}
	}
	else
	{
		/* Update co-ordinates to reflect those of the vertex we're using. */
		x = lpmap->vertices[iVertex].x;
		y = lpmap->vertices[iVertex].y;
	}

	/* If we clicked the last-drawn vertex, just stop. */
	if(iVertexPrev == iVertex && iVertex >= 0)
	{
		bKeepGoing = FALSE;
	}
	/* If that wasn't the first vertex, add a linedef to it from the previous
	 * vertex.
	 */
	else if(iVertexPrev >= 0)
	{
		/* Is there already a linedef between these vertices (in either direction)?
		 */
		iLinedef = FindLinedefBetweenVertices(lpmap, iVertexPrev, iVertex);
		if(iLinedef >= 0)
		{
			/* There was already a linedef there. If it's going the wrong way,
			 * flip it.
			 */
			if(lpmap->linedefs[iLinedef].v1 == iVertex)
				FlipLinedef(lpmap, iLinedef);

			/* Add the linedef to the list of new ones. We do this even if it's
			 * there already. This also marks it new.
			 */
			AddLinedefToDrawList(lpmap, lpdrawop, iLinedef);
		}
		else
		{
			float xSideSpot, ySideSpot;
			int iParentSector;
			BOOL bNewSector;
			int iVertexIntersect;
			int iIntersectVxDist;

			/* This is still strictly *ad experimentum*: if creating this
			 * linedef would cross any existing ones, split that linedef and
			 * bifurcate creation of the new one.
			 */
			int iCrossedLinedef = LinedefIntersectingSegment(
				lpmap,
				(float)lpmap->vertices[iVertexPrev].x,
				(float)lpmap->vertices[iVertexPrev].y,
				x,
				y,
				FALSE);

			if(iCrossedLinedef >= 0)
			{
				/* Find the point of intersection, and do two linedef insertions. */
				short xIntersect, yIntersect;
				int iNewLinedef;

				/* Get the point of intersection. */
				LineSegmentIntersection(lpmap->vertices[iVertexPrev].x,
										lpmap->vertices[iVertexPrev].y,
										x,
										y,
										lpmap->vertices[lpmap->linedefs[iCrossedLinedef].v1].x,
										lpmap->vertices[lpmap->linedefs[iCrossedLinedef].v1].y,
										lpmap->vertices[lpmap->linedefs[iCrossedLinedef].v2].x,
										lpmap->vertices[lpmap->linedefs[iCrossedLinedef].v2].y,
										&xIntersect, &yIntersect);

				/* Find the vertex nearest to where we're going. */
				iVertexIntersect = NearestVertex(lpmap, lpdrawop->lpmogVertices, xIntersect, yIntersect, &iIntersectVxDist);

				/* Unless we're right on top of an existing one, add a new
				 * vertex.
				 */
				if(iVertexIntersect < 0 || iIntersectVxDist > 0)
				{
					iVertexIntersect = AddVertex(lpmap, xIntersect, yIntersect);
					if(lpdrawop->lpmogVertices) AddVertexToMOG(lpdrawop->lpmogVertices, lpmap, iVertexIntersect);
				}

				/* Split the crossed linedef. */
				iNewLinedef = SplitLinedef(lpmap, iCrossedLinedef, iVertexIntersect);

				/* Add both linedefs to the MOG if we have one. */
				if(lpdrawop->lpmogLinedefs)
				{
					AddLinedefToMOG(lpdrawop->lpmogLinedefs, lpmap, iCrossedLinedef);
					AddLinedefToMOG(lpdrawop->lpmogLinedefs, lpmap, iNewLinedef);
				}

				/* If we were split from an LEF_NEW linedef, add the new linedef
				 * to the drawing list.
				 */
				if(lpmap->linedefs[iCrossedLinedef].editflags & LEF_NEW)
					AddLinedefToDrawList(lpmap, lpdrawop, iNewLinedef);
			}
			else
			{
				/* We don't cross any other lines, but we might hit a vertex. */

				LINESEGMENT linesegment;

				linesegment.x1 = lpmap->vertices[iVertexPrev].x;
				linesegment.y1 = lpmap->vertices[iVertexPrev].y;
				linesegment.x2 = x;
				linesegment.y2 = y;

				iVertexIntersect = NearestConditionedVertex(lpmap, lpdrawop->lpmogVertices, linesegment.x1, linesegment.y1, &iIntersectVxDist, VertexOnSegment, &linesegment);
			}

			/* Did we cross a line or a vertex? */
			if(iVertexIntersect >= 0)
			{
				/* Do two linedef drawings, and then stop. */
				DrawToNewVertex(lpmap, lpdrawop, lpcfgWadOptMap, lpmap->vertices[iVertexIntersect].x, lpmap->vertices[iVertexIntersect].y, bStitch);
				return DrawToNewVertex(lpmap, lpdrawop, lpcfgWadOptMap, x, y, bStitch);
			}

			/* No previous linedef, so this might require a new sector. Must
			 * check this before adding the new linedef.
			 */
			if(bExistingVertex)
			{
				DYNAMICINTARRAY *lpldlookupVx = BuildLDLookupTable(lpmap, LDLT_VXTOVX);
				bNewSector = VerticesReachable(lpmap, lpldlookupVx, iVertex, iVertexPrev, 1);
				DestroyLDLookupTable(lpmap, lpldlookupVx);
			}
			else bNewSector = FALSE;

			/* Create a linedef from the previous vertex to the new one. */
			iLinedef = AddLinedef(lpmap, iVertexPrev, iVertex);

			/* Add the linedef to the MOG if we have one. */
			if(lpdrawop->lpmogLinedefs) AddLinedefToMOG(lpdrawop->lpmogLinedefs, lpmap, iLinedef);

			/* Add the linedef to the list of new ones. We do this even if it's
			 * there already. This also marks it new.
			 */
			AddLinedefToDrawList(lpmap, lpdrawop, iLinedef);

			/* Don't use this LD to determine what sector we're in. */
			lpmap->linedefs[iLinedef].editflags |= LEF_INVALIDSIDEDEFS;

			/* Find which sector we're within. */
			GetLineSideSpot(lpmap, iLinedef, LS_FRONT, 0, &xSideSpot, &ySideSpot);
			iParentSector = IntersectSectorFloat(xSideSpot, ySideSpot, lpmap, LinedefNoInvalidSD, NULL);

			if(iParentSector >= 0)
			{
				/* Initially set all sidedefs to refer to parent sector. */

				/* Two new sidedefs. */
				int iSD1 = AddSidedef(lpmap, iParentSector);
				int iSD2 = AddSidedef(lpmap, iParentSector);

				/* They're within the parent sector. */
				lpmap->sidedefs[iSD1].sector = iParentSector;
				lpmap->sidedefs[iSD2].sector = iParentSector;

				/* Set them. */
				SetLinedefSidedef(lpmap, iLinedef, iSD1, LS_FRONT);
				SetLinedefSidedef(lpmap, iLinedef, iSD2, LS_BACK);
			}

			/* We don't clear LEF_INVALIDSIDEDEFS until we set sidedefs or we
			 * finish drawing.
			 */

			/* If there's no parent sector, we don't create any sidedefs. If the
			 * user closes the sector, front sds will be created then; if not,
			 * the line will just be left without sidedefs.
			 */

			if(bNewSector)
			{
				int iNewSector;
				DYNAMICINTARRAY *lpldlookupLD;
				DYNAMICINTARRAY diarrayNewSectorSidedefs;
				int i;
				BOOL bInsideOut;
				float xSideSpotSector, ySideSpotSector;
				int iFacingLinedef;
				int iLineSideForSector;
				POINT ptOrigin, ptOther;
				int iPreferredSide, iNonPreferredSide;
				int *lpiSidesToLines;

				/* Create a new sector. */
				iNewSector = AddSector(lpmap);

				/* Set the new sector's properties, from the parent sector if
				 * such exists, or otherwise from the defaults.
				 */
				if(iParentSector >= 0)
					lpmap->sectors[iNewSector] = lpmap->sectors[iParentSector];
				else
					ApplySectorDefaults(lpmap, lpcfgWadOptMap, iNewSector);

				/* Build vertex-to-ld lookup table. */
				lpldlookupLD = BuildLDLookupTable(lpmap, LDLT_VXTOLD);

				/* Create empty array for the affected sidedefs. */
				InitialiseDynamicIntArray(&diarrayNewSectorSidedefs, SDPOLYARRAY_INITBUFSIZE);

				if((lpmap->vertices[iVertex].editflags & VEF_NEW) &&
					!ConfigGetInteger(g_lpcfgMain, OPT_NEWSECFRONT))
				{
					float x, y;
					GetLineSideSpot(lpmap, iLinedef, LS_FRONT, 0.1f, &x, &y);
					iPreferredSide = PointInLinedefs(lpmap, x, y, lpdrawop->lpiNewLines, lpdrawop->iNewLineCount) ? LS_FRONT : LS_BACK;
				}
				else iPreferredSide = LS_FRONT;

				iNonPreferredSide = (iPreferredSide == LS_FRONT) ? LS_BACK : LS_FRONT;

				/* Determine whether the line is pointing into space. */
				GetOrthHLPointsIn2Coords(lpmap, iLinedef, iPreferredSide, &ptOrigin, &ptOther);
				iFacingLinedef = FindNearestLinedefOnHalfLine(lpmap, lpdrawop->lpmogLinedefs, ptOrigin.x, ptOrigin.y, ptOther.x, ptOther.y);

				/* Begin by assuming we can't make the sector on this side. */
				iLineSideForSector = iNonPreferredSide;

				/* Make sure we're not pointing right off the end of the map. */
				if(iFacingLinedef >= 0)
				{
					/* We're facing a linedef. We'll create a sector on this
					 * side iff we form an inward loop with that linedef.
					 */
					DYNAMICINTARRAY diarrayLoop;

					InitialiseDynamicIntArray(&diarrayLoop, LINELOOPARRAY_INITBUFSIZE);

					if(FindLoopFromLine(lpmap, iLinedef, iPreferredSide, iLinedef, iPreferredSide, -1, &diarrayLoop, NULL, lpldlookupLD, NULL))
					{
						/* We found a loop. Check whether it's inward-facing. */
						FPOINT fptMidPoint, fptFacing;

						GetLineSideSpot(lpmap, iLinedef, LS_FRONT, 0.0f, &fptMidPoint.x, &fptMidPoint.y);
						GetLineSideSpot(lpmap, iFacingLinedef, LS_FRONT, 0.0f, &fptFacing.x, &fptFacing.y);

						fptMidPoint.x = (fptMidPoint.x + fptFacing.x) / 2;
						fptMidPoint.y = (fptMidPoint.y + fptFacing.y) / 2;

						if(PointInLinedefs(lpmap, fptMidPoint.x, fptMidPoint.y, diarrayLoop.lpiIndices, diarrayLoop.uiCount))
							iLineSideForSector = iPreferredSide;
					}
				}

				/* Set the sidedefs to point to the new sector. If our linedef
				 * is pointing into space, we put the new sector on its back,
				 * but it'll be flipped by the propagation routine.
				 */
				PropagateNewSector(lpmap, lpldlookupLD, iLinedef, iLineSideForSector, iNewSector, &diarrayNewSectorSidedefs);

				/* Since we might have flipped some linedefs, correct our belief
				 * about on which side the new sector was created.
				 */
				iLineSideForSector = (lpmap->sidedefs[lpmap->linedefs[iLinedef].s1].sector == iNewSector) ? LS_FRONT : LS_BACK;

				/* Builds sides -> lines table. */
				lpiSidesToLines = ProcHeapAlloc(lpmap->iSidedefs * sizeof(int));
				for(i = 0; i < lpmap->iLinedefs; i++)
				{
					if(SidedefExists(lpmap, lpmap->linedefs[i].s1)) lpiSidesToLines[lpmap->linedefs[i].s1] = i;
					if(SidedefExists(lpmap, lpmap->linedefs[i].s2)) lpiSidesToLines[lpmap->linedefs[i].s2] = i;
				}

				/* Is the new sector inside-out (i.e. on the external edges of
				 * the polygon)?
				 */
				GetLineSideSpot(lpmap, iLinedef, iLineSideForSector, 0.5, &xSideSpotSector, &ySideSpotSector);
				bInsideOut = !PointInSidedefs(lpmap, xSideSpotSector, ySideSpotSector, diarrayNewSectorSidedefs.lpiIndices, diarrayNewSectorSidedefs.uiCount, lpiSidesToLines);

				/* Loop through each of the sidedefs referring to the parent
				 * sector, and correct them if they ought to be pointing to the
				 * new one.
				 */
				for(i = 0; i < lpmap->iLinedefs; i++)
				{
					int iSDIndices[2] = {lpmap->linedefs[i].s1, lpmap->linedefs[i].s2};
					int j;

					/* If the linedef's new, we're certainly not interested. */
					if(lpmap->linedefs[i].editflags & LEF_NEW) continue;

					for(j = 0; j < 2; j++)
					{
						MAPSIDEDEF *lpsd = NULL;

						if(SidedefExists(lpmap, iSDIndices[j]))
							lpsd = &lpmap->sidedefs[iSDIndices[j]];

						/* Might we need to change this one? */
						if(!lpsd || lpsd->sector == iParentSector)
						{
							BOOL bInsideSidedefPoly;

							GetLineSideSpot(lpmap, i, j == 0 ? LS_FRONT : LS_BACK, 0.5, &xSideSpotSector, &ySideSpotSector);
							bInsideSidedefPoly = PointInSidedefs(lpmap, xSideSpotSector, ySideSpotSector, diarrayNewSectorSidedefs.lpiIndices, diarrayNewSectorSidedefs.uiCount, lpiSidesToLines);

							/* We need to change the sd to point to the new
							 * sector if the sector points inwards and the sd
							 * is inside us, or respectively outwards and
							 * outside.
							 */
							if(lpsd)
							{
								if((!bInsideOut && bInsideSidedefPoly) || (bInsideOut && !bInsideSidedefPoly))
								{
									lpsd->sector = iNewSector;
								}
							}
							/* Use a slightly different test when one-sided: the
							 * external case doesn't apply.
							 */
							else if(!bInsideOut && bInsideSidedefPoly)
							{
								/* Need to make a new sidedef. */
								int iSidedef = AddSidedef(lpmap, iNewSector);
								lpsd = &lpmap->sidedefs[iSidedef];
								SetLinedefSidedef(lpmap, i, iSidedef, j == 0 ? LS_FRONT : LS_BACK);

								/* Update the sides -> lines table. */
								lpiSidesToLines = ProcHeapReAlloc(lpiSidesToLines, lpmap->iSidedefs * sizeof(int));
								lpiSidesToLines[iSidedef] = i;
							}
						}
					}	/* Both sidedefs loop. */
				}	/* Linedefs loop. */


				/* Clean up. */
				ProcHeapFree(lpiSidesToLines);
				FreeDynamicIntArray(&diarrayNewSectorSidedefs);
				DestroyLDLookupTable(lpmap, lpldlookupLD);
			}
		}

		/* Did we return to one of the vertices we've just drawn? If so, we stop
		 * drawing.
		 */
		if(lpmap->vertices[iVertex].editflags & VEF_NEW)
		{
			/* Tell the caller to stop drawing. */
			bKeepGoing = FALSE;
		}
	}

	/* Add the vertex to the list of new ones. We do this even if it's there
	 * already. This also marks it new.
	 */
	AddVertexToDrawList(lpmap, lpdrawop, iVertex);

	/* Indicate whether the drawing operation should continue. */
	return bKeepGoing;
}



/* AddVertexToDrawList, AddLinedefToDrawList
 *   Adds a vertex or linedef to the draw-list, and marks it as new.
 *
 * Parameters:
 *   MAP*				lpmap				Pointer to map structure.
 *   DRAW_OPERATION*	lpdrawop			Pointer to draw-op structure.
 *   int				iVertex/iLinedef	Index of vertex/linedef to add.
 *
 * Return value: None.
 *
 * Remarks:
 *   Largely a copy-paste job, I'm afraid.
 */
static void AddVertexToDrawList(MAP *lpmap, DRAW_OPERATION *lpdrawop, int iVertex)
{
	/* Not enough room in the buffer? */
	if(lpdrawop->ciVertexBuffer == (unsigned int)lpdrawop->iNewVertexCount)
	{
		/* Increase the size of the buffer. */
		lpdrawop->ciVertexBuffer += DRAWOP_BUFFER_COUNT_INCREMENT;
		lpdrawop->lpiNewVertices = ProcHeapReAlloc(lpdrawop->lpiNewVertices, lpdrawop->ciVertexBuffer * sizeof(int));
	}

	/* Add the vertex to the buffer and increment the count. */
	lpdrawop->lpiNewVertices[lpdrawop->iNewVertexCount++] = iVertex;

	/* Mark the vertex as new. */
	lpmap->vertices[iVertex].editflags |= VEF_NEW;
}

static void AddLinedefToDrawList(MAP *lpmap, DRAW_OPERATION *lpdrawop, int iLinedef)
{
	/* Not enough room in the buffer? */
	if(lpdrawop->ciLineBuffer == (unsigned int)lpdrawop->iNewLineCount)
	{
		/* Increase the size of the buffer. */
		lpdrawop->ciLineBuffer += DRAWOP_BUFFER_COUNT_INCREMENT;
		lpdrawop->lpiNewLines = ProcHeapReAlloc(lpdrawop->lpiNewLines, lpdrawop->ciLineBuffer * sizeof(int));
	}

	/* Add the linedef to the buffer and increment the count. */
	lpdrawop->lpiNewLines[lpdrawop->iNewLineCount++] = iLinedef;

	/* Mark the linedef as new. */
	lpmap->linedefs[iLinedef].editflags |= LEF_NEW;
}



/* FlipLinedef
 *   Flips a linedef in the intuitive way.
 *
 * Parameters:
 *   MAP*	lpmap		Pointer to map structure.
 *   int	iLinedef	Index of linedef to flip.
 *
 * Return value: None.
 */
void FlipLinedef(MAP *lpmap, int iLinedef)
{
	int iVertexIntermediate, iSidedefIntermediate;
	MAPLINEDEF *lplinedef = &lpmap->linedefs[iLinedef];

	/* Interchange vertices. */
	iVertexIntermediate = lplinedef->v1;
	lplinedef->v1 = lplinedef->v2;
	lplinedef->v2 = iVertexIntermediate;

	/* Interchange sidedefs. This means that sidedefs don't actually move, in
	 * the sense of their drawing positions.
	 */
	iSidedefIntermediate = lplinedef->s1;
	lplinedef->s1 = lplinedef->s2;
	lplinedef->s2 = iSidedefIntermediate;
}


/* FlipSelectedLinedefs
 *   Flips all linedefs in a selection.
 *
 * Parameters:
 *   MAP*				lpmap		Pointer to map structure.
 *   SELECTION_LIST*	lpsellist	Selection of linedefs to flip.
 *
 * Return value: None.
 */
void FlipSelectedLinedefs(MAP *lpmap, SELECTION_LIST* lpsellist)
{
	int i;
	for(i = 0; i < lpsellist->iDataCount; i++)
		FlipLinedef(lpmap, lpsellist->lpiIndices[i]);
}


/* ExchangeSidedefs
 *   Exchanges a linedef's sidedefs.
 *
 * Parameters:
 *   MAP*	lpmap		Pointer to map structure.
 *   int	iLinedef	Index of linedef whose sidedefs are to be exchanged.
 *
 * Return value: None.
 *
 * Remarks:
 *   This will exchange sidedefs of a single-sided linedef without complaint.
 *   I can't decide whether this is a good thing or a bad thing.
 */
static void ExchangeSidedefs(MAP *lpmap, int iLinedef)
{
	int i;
	MAPLINEDEF *lpld = &lpmap->linedefs[iLinedef];

	i = lpld->s1;
	lpld->s1 = lpld->s2;
	lpld->s2 = i;
}


/* ExchangeSelectedSidedefs
 *   Exchanges sidedefs of all linedefs in a selection.
 *
 * Parameters:
 *   MAP*				lpmap		Pointer to map structure.
 *   SELECTION_LIST*	lpsellist	Selection of linedefs whose sidedefs are to
 *									be exchanged.
 *
 * Return value: None.
 */
void ExchangeSelectedSidedefs(MAP *lpmap, SELECTION_LIST* lpsellist)
{
	int i;
	for(i = 0; i < lpsellist->iDataCount; i++)
		ExchangeSidedefs(lpmap, lpsellist->lpiIndices[i]);
}


/* BisectLinedef
 *   Creates a new vertex half-way along the length of a linedef, and splits the
 *   linedef across that vertex.
 *
 * Parameters:
 *   MAP*	lpmap		Pointer to map structure.
 *   int	iLinedef	Index of linedef to bisect.
 *
 * Return value: int
 *   Index of new linedef, or negative if none created.
 *
 * Remarks:
 *   If there's a vertex exactly in the middle already (which admittedly there
 *   shouldn't be), it'll be used instead of creating a new one.
 */
static __inline int BisectLinedef(MAP *lpmap, int iLinedef)
{
	MAPLINEDEF *lpld = &lpmap->linedefs[iLinedef];
	MAPVERTEX *lpvx1 = &lpmap->vertices[lpld->v1];
	MAPVERTEX *lpvx2 = &lpmap->vertices[lpld->v2];

	/* Split at midpoint of the line. */
	return SplitLinedefAtPoint(lpmap, iLinedef,
		(short)(((int)lpvx1->x + (int)lpvx2->x) / 2),
		(short)(((int)lpvx1->y + (int)lpvx2->y) / 2));
}


/* SplitLinedefAtPoint
 *   Creates a new vertex at the specified point, and splits the linedef across
 *   that vertex.
 *
 * Parameters:
 *   MAP*	lpmap		Pointer to map structure.
 *   int	iLinedef	Index of linedef to bisect.
 *   short	x, y		Point at whcih to split.
 *
 * Return value: int
 *   Index of new linedef, or negative if none created.
 *
 * Remarks:
 *   If there's a vertex exactly at the point already, it'll be used instead of
 *   creating a new one.
 */
int SplitLinedefAtPoint(MAP *lpmap, int iLinedef, short x, short y)
{
	int iVertex, iVxDist;
	MAPLINEDEF *lpld = &lpmap->linedefs[iLinedef];

	/* Find the vertex nearest to where we're going. */
	iVertex = NearestVertex(lpmap, NULL, x, y, &iVxDist);

	/* Unless there's already a vertex in the middle, create a new one; if there
	 * is one, though, just use it.
	 */
	if(iVertex < 0 || iVxDist > 0)
		iVertex = AddVertex(lpmap, x, y);
	else if(iVertex == lpld->v1 || iVertex == lpld->v2)
	{
		/* If we're right on one of OUR vertices, then there's nothing we can
		 * do.
		 */
		return -1;
	}

	/* Split the linedef at the vertex. */
	return SplitLinedef(lpmap, iLinedef, iVertex);
}


/* BisectSelectedLinedefs
 *   Bisects all linedefs in a selection.
 *
 * Parameters:
 *   MAP*				lpmap		Pointer to map structure.
 *   SELECTION_LIST*	lpsellist	Selection of linedefs to be bisected.
 *
 * Return value: None.
 *
 * Remarks:
 *   The new linedefs are added to the selection.
 */
void BisectSelectedLinedefs(MAP *lpmap, SELECTION_LIST* lpsellist)
{
	int i;
	int iNumOriginallySelected;
	int *lpiNewLines;

	/* Allocate a buffer to store the indices of the new lines. */
	lpiNewLines = ProcHeapAlloc(lpsellist->iDataCount * sizeof(int));

	/* Note that BisectLinedef may return -1. */
	for(i = 0; i < lpsellist->iDataCount; i++)
		lpiNewLines[i] = BisectLinedef(lpmap, lpsellist->lpiIndices[i]);

	/* Add all the new lines to the selection. */
	iNumOriginallySelected = lpsellist->iDataCount;
	for(i = 0; i < iNumOriginallySelected; i++)
		if(lpiNewLines[i] >= 0)
			AddToSelectionList(lpsellist, lpiNewLines[i]);

	/* Free the buffer. */
	ProcHeapFree(lpiNewLines);
}


/* GetVertexFromPosition
 *   Returns the index of the vertex at the specified co-ordinates.
 *
 * Parameters:
 *   MAP*	lpmap	Pointer to map structure.
 *   short	x, y	Co-ordinates of desired vertex.
 *
 * Return value: int
 *   Index of vertex if found; negative otherwise.
 */
int GetVertexFromPosition(MAP *lpmap, short x, short y)
{
	int iDist;
	int iVertex = NearestVertex(lpmap, NULL, x, y, &iDist);

	/* Did we find a vertex, and if so, is it exactly where we want it? */
	if(iVertex >= 0 && lpmap->vertices[iVertex].x == x && lpmap->vertices[iVertex].y == y)
		return iVertex;

	/* No vertex at specified position. */
	return -1;
}


/* FindLinedefBetweenVertices
 *   Searches for a linedef joining two vertices in either direction.
 *
 * Parameters:
 *   MAP*	lpmap				Pointer to map structure.
 *   int	iVertexA, iVertexB	Indices of vertices the linedef must join.
 *
 * Return value: int
 *   Index of linedef if found; negative otherwise.
 *
 * Remarks:
 *   See also FindLinedefBetweenVerticesDirected, which cares which way the line
 *   goes.
 */
int FindLinedefBetweenVertices(MAP *lpmap, int iVertexA, int iVertexB)
{
	int iLinedef;

	/* Look for a vertex from A to B first. Return index if found. */
	iLinedef = FindLinedefBetweenVerticesDirected(lpmap, iVertexA, iVertexB);
	if(iLinedef >= 0) return iLinedef;

	/* None from A to B; look from B to A. */
	return FindLinedefBetweenVerticesDirected(lpmap, iVertexB, iVertexA);
}


/* FindLinedefBetweenVerticesDirected
 *   Searches for a linedef beginning at one vertex and ending at another.
 *
 * Parameters:
 *   MAP*	lpmap				Pointer to map structure.
 *   int	iVertexA, iVertexB	Indices of vertices the linedef must join.
 *
 * Return value: int
 *   Index of linedef if found; negative otherwise.
 *
 * Remarks:
 *   See also FindLinedefBetweenVertices, which doesn't care which way the line
 *   goes.
 */
int FindLinedefBetweenVerticesDirected(MAP *lpmap, int iVertex1, int iVertex2)
{
	int i;

	for(i = 0; i < lpmap->iLinedefs; i++)
	{
		/* Does the linedef connect the correct vertices in the desired dirn? */
		if(lpmap->linedefs[i].v1 == iVertex1 && lpmap->linedefs[i].v2 == iVertex2)
			return i;
	}

	/* Not found. */
	return -1;
}



/* Snap
 *   Snaps a point to a rectangular grid.
 *
 * Parameters:
 *   short				*lpx, *lpy			Pointers to co-ordinates of point to
 *											snap.
 *   unsigned short		cxGrid, cyGrid		Grid dimensions.
 *   short				xOffset, yOffset	Grid offset.
 *
 * Return value: None.
 */
void Snap(short *lpx, short *lpy, unsigned short cxSnap, unsigned short cySnap, short xOffset, short yOffset)
{
	int x = *lpx, y = *lpy;

	/* Adjust for grid offset. */
	x -= xOffset;
	y -= yOffset;

	if(x >= 0)
		x = cxSnap * ((x + (cxSnap >> 1)) / cxSnap);
	else
		x = cxSnap * ((x - (cxSnap >> 1)) / cxSnap);

	if(y >= 0)
		y = cySnap * ((y + (cySnap >> 1)) / cySnap);
	else
		y = cySnap * ((y - (cySnap >> 1)) / cySnap);

	/* Restore grid offset. */
	x += xOffset;
	y += yOffset;

	/* Snapping can take us out-of-bounds, so correct for this. */
	while(x > 32767) x -= cxSnap;
	while(x < -32768) x += cxSnap;
	while(y > 32767) y -= cySnap;
	while(y < -32768) y += cySnap;

	*lpx = x;
	*lpy = y;
}




/* VerticesReachable
 *   Determines whether a certain number of vertex- and line-wise disjoint path
 *   exists between two vertices.
 *
 * Parameters:
 *   MAP*				lpmap				Pointer to map data.
 *   DYNAMICINTARRAY*	lpldlookup			Vertex-to-vertex lookup table.
 *   int				iVertex1, iVertex2	Indices of vertices.
 *   int				iMinTimes			Minimum number of disjoint paths
 *											required.
 *
 * Return value: TRUE if path exists; FALSE if not.
 *
 * Remarks:
 *   Just creates some data structures and calls VerticesReachableByLookup,
 *   where the real work is done.
 */
BOOL VerticesReachable(MAP *lpmap, DYNAMICINTARRAY *lpldlookup, int iVertex1, int iVertex2, int iMinTimes)
{
	/* We do a recursive depth-first search from iVertex1 for iVertex2. We need
	 * to keep track of which vertices we've visited.
	 */

	BOOL *lpbVisited = ProcHeapAlloc(lpmap->iVertices * sizeof(BOOL));
	BOOL bReachable = TRUE;
	DYNAMICINTARRAY *lpldCopy = DeepCopyLDLookupTable(lpmap, lpldlookup);
	int i;

	/* We haven't visited any vertices to start with. */
	ZeroMemory(lpbVisited, lpmap->iVertices * sizeof(BOOL));

	/* Now that we've got the lookup table, we can do the actual search. We use
	 * the same visited array between calls so we can find disjoint paths.
	 */
	for(i = 0; bReachable && i < iMinTimes; i++)
		bReachable = VerticesReachableInternal(lpldCopy, lpbVisited, iVertex1, iVertex2);

	DestroyLDLookupTable(lpmap, lpldCopy);
	ProcHeapFree(lpbVisited);

	return bReachable;
}



/* VerticesReachableInternal
 *   Helper routine for VerticesReachable.
 *
 * Parameters:
 *   DYNAMICINTARRAY*	lpldlookup			Vertex-to-vertex lookup table,
 *											which will be modified.
 *   BOOL*				lpbVisited			Array indicating which vertices
 *											have been visited.
 *   int				iVertex1, iVertex2	Indices of vertices.
 *
 * Return value: BOOL
 *   TRUE if path exists; FALSE if not.
 *
 * Remarks:
 *   Don't call this directly; call VerticesReachable instead. If a path is
 *   found, the lookup table will have adjacencies removed along that path.
 */
static BOOL __fastcall VerticesReachableInternal(DYNAMICINTARRAY *lpldlookup, BOOL *lpbVisited, int iVertex1, int iVertex2)
{
	int i;

	/* Termination case: we're reachable from ourselves. Do this *before*
	 * marking ourselves as visited, so that we can check for multiple paths.
	 */
	if(iVertex1 == iVertex2) return TRUE;

	/* We've visited ourselves. */
	lpbVisited[iVertex1] = TRUE;

	/* Is it reachable from any of our as-yet-unvisited neighbours? */
	for(i = 0; (unsigned int)i < lpldlookup[iVertex1].uiCount; i++)
	{
		if(lpldlookup[iVertex1].lpiIndices[i] >= 0 &&
			!lpbVisited[lpldlookup[iVertex1].lpiIndices[i]] &&
			VerticesReachableInternal(lpldlookup, lpbVisited, lpldlookup[iVertex1].lpiIndices[i], iVertex2))
		{
			/* Burn your bridges! */
			lpldlookup[iVertex1].lpiIndices[i] = -1;

			return TRUE;
		}
	}

	/* Not reachable from any of our neighbours, so it isn't reachable from
	 * here.
	 */
	return FALSE;
}


/* GetLineSideSpot
 *   Finds a point a given distance normally from the centre of a linedef.
 *
 * Parameters:
 *   MAP*	lpmap				Pointer to map data.
 *   int	iLinedef			Index of linedef.
 *   int	iSideOfLine			LS_FRONT or LS_BACK: from which side the
 *								distance is to be measured.
 *   float	fDistance			Distance.
 *   float*	lpx, lpy			Point is returned here.
 *
 * Return value: None.
 */
static void GetLineSideSpot(MAP *lpmap, int iLinedef, int iSideOfLine, float fDistance, float *lpx, float *lpy)
{
	FPOINT fptCentre;
	POINT ptNormal;
	MAPLINEDEF *lpld = &lpmap->linedefs[iLinedef];
	MAPVERTEX *lpv1 = &lpmap->vertices[lpld->v1], *lpv2 = &lpmap->vertices[lpld->v2];
	float fNormalLength;

	/* Get the point of the centre of the line. */
	fptCentre.x = (float)(lpv2->x + lpv1->x) / 2;
	fptCentre.y = (float)(lpv2->y + lpv1->y) / 2;

	/* Find the normal. */
	ptNormal.x = lpv2->y - lpv1->y;
	ptNormal.y = lpv1->x - lpv2->x;
	if(iSideOfLine == LS_BACK)
	{
		ptNormal.x = -ptNormal.x;
		ptNormal.y = -ptNormal.y;
	}
	fNormalLength = (float)sqrt(ptNormal.x * ptNormal.x + ptNormal.y * ptNormal.y);

	/* Find the desired point. */
	*lpx = fptCentre.x + (ptNormal.x * fDistance) / fNormalLength;
	*lpy = fptCentre.y + (ptNormal.y * fDistance) / fNormalLength;
}



/* BuildLDLookupTable
 *   Builds a lookup table for adjacent linedefs or vertices given a vertex.
 *
 * Parameters:
 *   MAP*	lpmap			Pointer to map data.
 *   int	iLookupType		LDLT_VXTOVX or LDLT_VXTOLD
 *
 * Return value: DYNAMICINTARRAY*
 *   Pointer to new lookup table.
 *
 * Remarks:
 *   The caller should call DestroyLDLookupTable to free the allocated memory.
 *   Linedefs labelled LEF_NOGRAPH are ignored.
 */
DYNAMICINTARRAY *BuildLDLookupTable(MAP *lpmap, int iLookupType)
{
	DYNAMICINTARRAY *lpldlookup = ProcHeapAlloc(lpmap->iVertices * sizeof(DYNAMICINTARRAY));
	int i;

	/* Allocate an initial buffer for each vertex, to contain the other vertices
	 * joined to each directly.
	 */
	for(i = 0; i < lpmap->iVertices; i++)
		InitialiseDynamicIntArray(&lpldlookup[i], LDLOOKUP_INITBUFSIZE);

	/* Now fill the table. */
	for(i = 0; i < lpmap->iLinedefs; i++)
	{
		int iVerticesToAdd[2];
		int iValues[2];
		size_t j;
		MAPLINEDEF *lpld = &lpmap->linedefs[i];

		/* Are we interested in this line? */
		if(!(lpld->editflags & LEF_NOGRAPH))
		{
			/* Are we doing vx-to-vx, or vx-to-ld? */
			switch(iLookupType)
			{
			case LDLT_VXTOVX:
				/* Add the entry. */
				iVerticesToAdd[0] = lpld->v1; iValues[0] = lpld->v2;
				iVerticesToAdd[1] = lpld->v2; iValues[1] = lpld->v1;
				break;

			case LDLT_VXTOLD:
				iVerticesToAdd[0] = lpld->v1; iValues[0] = i;
				iVerticesToAdd[1] = lpld->v2; iValues[1] = i;
				break;
			}

			for(j = 0; j < sizeof(iVerticesToAdd) / sizeof(int); j++)
			{
				/* If there's nothing to add, skip. */
				if(iVerticesToAdd[j] < 0) continue;

				AddToDynamicIntArray(&lpldlookup[iVerticesToAdd[j]], iValues[j]);
			}
		}
	}

	return lpldlookup;
}


/* DestroyLDLookupTable
 *   Frees memory used by a lookup table.
 *
 * Parameters:
 *   MAP*				lpmap		Pointer to map used to generate table.
 *   DYNAMICINTARRAY*	lpldlookup	Pointer to table to free.
 *
 * Return value: None.
 *
 * Remarks:
 *   No vertices may be added to or removed from the map between creating an
 *   destroying the table.
 */
void DestroyLDLookupTable(MAP *lpmap, DYNAMICINTARRAY *lpldlookup)
{
	int i;

	for(i = 0; i < lpmap->iVertices; i++)
		FreeDynamicIntArray(&lpldlookup[i]);

	ProcHeapFree(lpldlookup);
}


/* DeepCopyLDLookupTable
 *   Makes a deep copy of a lookup table.
 *
 * Parameters:
 *   MAP*				lpmap			Pointer to map used to generate table.
 *   DYNAMICINTARRAY*	lpldlookupSrc	Pointer to table to copy.
 *
 * Return value: DYNAMICINTARRAY*
 *   Copied table.
 *
 * Remarks:
 *   Free this table like any other.
 */
DYNAMICINTARRAY* DeepCopyLDLookupTable(MAP *lpmap, DYNAMICINTARRAY *lpldlookupSrc)
{
	DYNAMICINTARRAY* lpldlookupNew = ProcHeapAlloc(lpmap->iVertices * sizeof(DYNAMICINTARRAY));
	int i;

	/* Notice that the DIarrays don't need intialised before being copied over.
	 */

	for(i = 0; i < lpmap->iVertices; i++)
		DeepCopyDynamicIntArray(&lpldlookupNew[i], &lpldlookupSrc[i]);

	return lpldlookupNew;
}



/* PropagateNewSector
 *   Moves around the most convex polygon from a given linedef, setting sidedefs
 *   to refer to a particular sector.
 *
 * Parameters:
 *   MAP*				lpmap			Pointer to map data.
 *   DYNAMICINTARRAY*	lpldlookup		Pointer to table linking vertices to
 *										linedefs.
 *   int				iLinedef		Linedef to start from.
 *   int				iLinedefSide	LS_FRONT or LS_BACK, as appropriate.
 *   int				iNewSector		Index of sector to propagate to sidedefs.
 *   DYNAMICINTARRAY*	lpdiarrayAffectedSidedefs
 *										Altered sidedefs are returned here. May be
 *										set to NULL if not required.
 *
 * Return value: None.
 */
static void __fastcall PropagateNewSector(MAP *lpmap, DYNAMICINTARRAY *lpldlookup, int iLinedef, int iLinedefSide, int iNewSector, DYNAMICINTARRAY *lpdiarrayAffectedSidedefs)
{
	MAPLINEDEF *lpld = &lpmap->linedefs[iLinedef];
	int iSidedef = (iLinedefSide == LS_FRONT ? lpld->s1 : lpld->s2);
	MAPSIDEDEF *lpsd = (SidedefExists(lpmap, iSidedef) ? &lpmap->sidedefs[iSidedef] : NULL);
	int iNextLinedef;

	/* Termination case: if this side already refers to the new sector, we're
	 * finished.
	 */
	if(lpsd && lpsd->sector == iNewSector) return;

	/* If we've been told to put a sector on the back of the linedef but we
	 * don't yet have a front sidedef, then we flip the linedef and put the
	 * sector on the front.
	 */
	if(iLinedefSide == LS_BACK && !SidedefExists(lpmap, lpld->s1))
	{
		FlipLinedef(lpmap, iLinedef);
		iLinedefSide = LS_FRONT;

		/* The sidedef vars are still correct. */
	}

	/* No sidedef yet? Then make one, and add it to the line. */
	if(!lpsd)
	{
		iSidedef = AddSidedef(lpmap, iNewSector);
		lpsd = &lpmap->sidedefs[iSidedef];
		SetLinedefSidedef(lpmap, iLinedef, iSidedef, iLinedefSide);
	}

	/* We've touched the sidedefs now. */
	lpld->editflags &= ~LEF_INVALIDSIDEDEFS;

	/* If the caller's interested, add the sidedef's index to the array of
	 * affected sidedefs.
	 */
	if(lpdiarrayAffectedSidedefs)
		AddToDynamicIntArray(lpdiarrayAffectedSidedefs, iSidedef);

	/* Set the sidedef to reference the new sector. */
	lpsd->sector = iNewSector;

	/* Find the linedef adjacent to ourselves that makes the smallest angle with
	 * the appropriate side. If we're nearest to ourselves, the recursive call
	 * will terminate the algorithm.
	 */
	iNextLinedef = FindNearestAdjacentLinedef(lpmap, lpldlookup, iLinedef, iLinedefSide, (iLinedefSide == LS_FRONT) ? 1 : 0, NULL, NULL, NULL, NULL);

	/* Is it orientated the same way we are? */
	if(lpld->v1 == lpmap->linedefs[iNextLinedef].v2 || lpld->v2 == lpmap->linedefs[iNextLinedef].v1)
		/* Recurse along the same side. */
		PropagateNewSector(lpmap, lpldlookup, iNextLinedef, iLinedefSide, iNewSector, lpdiarrayAffectedSidedefs);
	else
		/* Recurse along the other side. */
		PropagateNewSector(lpmap, lpldlookup, iNextLinedef, (iLinedefSide == LS_FRONT) ? LS_BACK : LS_FRONT, iNewSector, lpdiarrayAffectedSidedefs);
}



/* FindNearestAdjacentLinedef
 *   Finds the linedef forming the smallest angle with a linedef from a
 *   specified side.
 *
 * Parameters:
 *   MAP*				lpmap			Pointer to map data.
 *   DYNAMICINTARRAY*	lpldlookup		Pointer to table linking vertices to
 *										linedefs.
 *   int				iLinedef		Linedef to start from.
 *   int				iLinedefSide	LS_FRONT or LS_BACK, as appropriate.
 *   int				iWhichVertex	0 for v1, 1 for v2.
 *   int*				lpiLookupIndex	Index to start searching from this
 *										vertex within the lookup. Also returns
 *										the index corresponding to the line. May
 *										be NULL.
 *   double*			lpdAngle		Returns the nearest angle. May be NULL.
 *   BOOL (*)(MAPLINEDEF*, void*)
 *						fnCondition		Function evaluated on each linedef to
 *										determine whether it should be
 *										considered. May be NULL.
 *	void*				lpvParam		Parameter to fnCondition.
 *
 * Return value: int
 *   Index of nearest linedef.
 *
 * Remarks:
 *   If we're the only linedef at the appropriate vertex, then *we* are defined
 *   to be the linedef closest to ourselves. In this case, *lpiLookupIndex and
 *   *dAngle are undefined (with the exception that *dAngle < 4 * PI).
 */
int FindNearestAdjacentLinedef(MAP *lpmap, DYNAMICINTARRAY *lpldlookup, int iLinedef, int iLinedefSide, int iWhichVertex, int *lpiLookupIndex, double *lpdAngle, BOOL (*fnCondition)(MAPLINEDEF *lpld, void *lpvParam), void *lpvParam)
{
	MAPLINEDEF *lpld = &lpmap->linedefs[iLinedef];
	int iVertex = iWhichVertex == 0 ? lpld->v1 : lpld->v2;
	int i, iNearestLinedef = iLinedef;		/* If no other ld, use self. */
	double dNearestAngle;

	/* Set something outwith the bounds. */
	dNearestAngle = 3 * PI;

	/* Enumerate all the linedefs connected to the specified vertex. */
	for(i = lpiLookupIndex ? *lpiLookupIndex : 0; (unsigned int)i < lpldlookup[iVertex].uiCount; i++)
	{
		double dAng;
		int iLinedefCheck = lpldlookup[iVertex].lpiIndices[i];
		MAPLINEDEF *lpldCheck = &lpmap->linedefs[iLinedefCheck];

		/* Skip lines we're not interested in, either because they're ourselves
		 * or because they fail the condition.
		 */
		if(iLinedef == iLinedefCheck || (fnCondition && !fnCondition(lpldCheck, lpvParam)))
			continue;

		dAng = AdjacentLinedefAngleFromFront(lpmap, iLinedef, iLinedefCheck);

		if(iLinedefSide == LS_BACK) dAng = 2 * PI - dAng;

		if(dAng < dNearestAngle)
		{
			dNearestAngle = dAng;
			if(lpiLookupIndex) *lpiLookupIndex = i;
			iNearestLinedef = iLinedefCheck;
		}
	}

	if(lpdAngle) *lpdAngle = dNearestAngle;
	return iNearestLinedef;
}



/* AdjacentLinedefAngleFromFront
 *   Finds the angle from the front of one linedef to another ld adjacent to it.
 *
 * Parameters:
 *   MAP*	lpmap		Pointer to map data.
 *   int	iLinedef1	Index of ld to measure from.
 *   int	iLinedef2	Index of ld to measure to.
 *
 * Return value: double
 *   Angle between them in radians, in [0, 2pi).
 */
static double AdjacentLinedefAngleFromFront(MAP *lpmap, int iLinedef1, int iLinedef2)
{
	MAPLINEDEF *lpld1 = &lpmap->linedefs[iLinedef1];
	MAPLINEDEF *lpld2 = &lpmap->linedefs[iLinedef2];
	double dAng1, dAng2;
	double dAng;

	/* Find which vertices they have in common. Note that when it's the first
	 * ld's V1, we've got to go clockwise: hence the dAng1 <--> iLindef2 and
	 * vice versa.
	 */
	if(lpld1->v1 == lpld2->v1)
	{
		dAng2 = LinedefAngleV1(lpmap, iLinedef1);
		dAng1 = LinedefAngleV1(lpmap, iLinedef2);
	}
	else if(lpld1->v1 == lpld2->v2)
	{
		dAng2 = LinedefAngleV1(lpmap, iLinedef1);
		dAng1 = LinedefAngleV2(lpmap, iLinedef2);
	}
	else if(lpld1->v2 == lpld2->v1)
	{
		dAng1 = LinedefAngleV2(lpmap, iLinedef1);
		dAng2 = LinedefAngleV1(lpmap, iLinedef2);
	}
	else /* if(lpld1->v2 == lpld2->v2) */
	{
		dAng1 = LinedefAngleV2(lpmap, iLinedef1);
		dAng2 = LinedefAngleV2(lpmap, iLinedef2);
	}

	dAng = dAng2 - dAng1;
	while(dAng < 0) dAng += 2 * PI;
	while(dAng >= 2 * PI) dAng -= 2 * PI;

	return dAng;
}

static double LinedefAngleV1(MAP *lpmap, int iLinedef)
{
	return atan2(lpmap->vertices[lpmap->linedefs[iLinedef].v2].y - lpmap->vertices[lpmap->linedefs[iLinedef].v1].y, lpmap->vertices[lpmap->linedefs[iLinedef].v2].x - lpmap->vertices[lpmap->linedefs[iLinedef].v1].x);
}

static double LinedefAngleV2(MAP *lpmap, int iLinedef)
{
	return atan2(lpmap->vertices[lpmap->linedefs[iLinedef].v1].y - lpmap->vertices[lpmap->linedefs[iLinedef].v2].y, lpmap->vertices[lpmap->linedefs[iLinedef].v1].x - lpmap->vertices[lpmap->linedefs[iLinedef].v2].x);
}


/* SetLinedefSidedef
 *   Sets a one of a linedef's sidedefs, and sets flags accordingly.
 *
 * Parameters:
 *   MAP*	lpmap			Pointer to map data.
 *   int	iLinedef		Index of linedef whose sidedef is to be changed.
 *   int	iSidedef		Index of sidedef.
 *   int	iLinedefSide	LS_FRONT or LS_BACK.
 *
 * Return value: None.
 */
void SetLinedefSidedef(MAP *lpmap, int iLinedef, int iSidedef, int iLinedefSide)
{
	MAPLINEDEF *lpld = &lpmap->linedefs[iLinedef];

	if(iLinedefSide == LS_FRONT)
		lpld->s1 = iSidedef;
	else
	{
		lpld->s2 = iSidedef;
	}

	/* Correct sidedness. Note that setting a front sidedef can in fact change
	 * sidedness, due to prefabs (and perverse users) giving rise to lines
	 * single-sided on the back.
	 */
	if(iSidedef == INVALID_SIDEDEF || !SidedefExists(lpmap, lpld->s2))
	{
		lpld->flags &= ~LDF_DOUBLESIDED;
		lpld->flags |= LDF_IMPASSABLE;
	}
	else
	{
		lpld->flags |= LDF_DOUBLESIDED;
		lpld->flags &= ~LDF_IMPASSABLE;
	}
}


/* SplitLinedef
 *   Takes one linedef and splits it into two, connected by a specified
 *   intermediate vertex.
 *
 * Parameters:
 *   MAP*	lpmap		Pointer to map data.
 *   int	iLinedef	Index of linedef to split.
 *   int	iVertex		Index of intermediate vertex.
 *
 * Return value: int
 *   Index of new linedef.
 *
 * Remarks:
 *   This doesn't make any attempt to prevent doubled lines.
 */
int SplitLinedef(MAP *lpmap, int iLinedef, int iVertex)
{
	MAPLINEDEF *lpldOrig = &lpmap->linedefs[iLinedef];
	MAPLINEDEF *lpldNew;
	int iOrigVertex2 = lpldOrig->v2;
	int iNewLD;

	/* Make the original linedef go from its v1 to the new vertex. */
	lpldOrig->v2 = iVertex;

	/* Add a new linedef from the new vertex to the original v2. */
	iNewLD = AddLinedef(lpmap, iVertex, iOrigVertex2);
	lpldNew = &lpmap->linedefs[iNewLD];

	/* Copy the linedef properties. */
	lpldNew->effect = lpldOrig->effect;
	lpldNew->flags = lpldOrig->flags;
	lpldNew->selected = lpldOrig->selected;
	lpldNew->tag = lpldOrig->tag;
	lpldNew->editflags = lpldOrig->editflags;

	/* Create duplicated sidedefs for the new linedef. */

	if(SidedefExists(lpmap, lpldOrig->s1))
	{
		int iNewSD = AddSidedef(lpmap, lpmap->sidedefs[lpldOrig->s1].sector);
		CopySidedefProperties(lpmap, lpldOrig->s1, iNewSD);
		SetLinedefSidedef(lpmap, iNewLD, iNewSD, LS_FRONT);
	}

	if(SidedefExists(lpmap, lpldOrig->s2))
	{
		int iNewSD = AddSidedef(lpmap, lpmap->sidedefs[lpldOrig->s2].sector);
		CopySidedefProperties(lpmap, lpldOrig->s2, iNewSD);
		SetLinedefSidedef(lpmap, iNewLD, iNewSD, LS_BACK);
	}

	return iNewLD;
}


/* CopySidedefProperties
 *   Copies all properties from one sidedef to another.
 *
 * Parameters:
 *   MAP*	lpmap		Pointer to map data.
 *   int	iOldSidedef	Index of sidedef to copy from.
 *   int	iNewSidedef	Index of sidedef to copy to.
 *
 * Return value: None.
 */
static void CopySidedefProperties(MAP *lpmap, int iOldSidedef, int iNewSidedef)
{
	/* We can get away with a shallow copy. */
	lpmap->sidedefs[iNewSidedef] = lpmap->sidedefs[iOldSidedef];
}


/* LineSegmentIntersection
 *   Finds the intersection of two linedef segments.
 *
 * Parameters:
 *   short	(xA1, yA1), (xA2, yA2)	Endpoints of first segment.
 *   short	(xB1, yB1), (xB2, yB2)	Endpoints of second segment.
 *   short	*lpxRet, *lpyRet		Used to return point of intersection.
 *
 * Return value: None.
 *
 * Remarks:
 *   No error-checking: the caller must ensure that the segments do indeed
 *   intersect at a point.
 */
void LineSegmentIntersection(short xA1, short yA1, short xA2, short yA2, short xB1, short yB1, short xB2, short yB2, short *lpxRet, short *lpyRet)
{
	int iGradientNumerator = ((int)xB2 - xB1)*((int)yA1 - yB1) - ((int)yB2 - yB1)*((int)xA1 - xB1);
	int iGradientDenominator = ((int)yB2 - yB1)*((int)xA2 - xA1) - ((int)xB2 - xB1)*((int)yA2 - yA1);

	*lpxRet = (short)(((LONGLONG)iGradientDenominator * xA1 + (LONGLONG)iGradientNumerator * ((int)xA2 - xA1)) / iGradientDenominator);
	*lpyRet = (short)(((LONGLONG)iGradientDenominator * yA1 + (LONGLONG)iGradientNumerator * ((int)yA2 - yA1)) / iGradientDenominator);
}


/* DeleteSector
 *   Deletes a sector from a map.
 *
 * Parameters:
 *   MAP*	lpmap		Pointer to map data.
 *   int	iSector		Sector to delete.
 *
 * Return value: None.
 *
 * Remarks:
 *   This performs a simple sector deletion: no attempt is made to keep the map
 *   correct. Also, this changes sector indices, so the selection will have to
 *   be rebuilt from flags (or cleared) after calling this function; sector
 *   references on sidedefs will be adjusted automatically, however.
 */
void DeleteSector(MAP *lpmap, int iSector)
{
	int i;

	/* Move all the sectors above this one down a space. */
	MoveMemory(&lpmap->sectors[iSector], &lpmap->sectors[iSector+1], (lpmap->iSectors - iSector - 1) * sizeof(MAPSECTOR));

	/* Decrement sector count. */
	lpmap->iSectors--;

	/* Correct sector references on sidedefs. */
	for(i = 0; i < lpmap->iSidedefs; i++)
	{
		MAPSIDEDEF *lpsd = &lpmap->sidedefs[i];

		if(lpsd->sector > iSector) lpsd->sector--;
	}
}


/* DeleteLinedef
 *   Deletes a linedef from a map.
 *
 * Parameters:
 *   MAP*	lpmap		Pointer to map data.
 *   int	iLinedef	Linedef to delete.
 *
 * Return value: None.
 *
 * Remarks:
 *   This performs a simple linedef deletion: no attempt is made to keep the map
 *   correct. Also, this changes linedef indices, so the selection will have to
 *   be rebuilt from flags (or cleared) after calling this function. Any
 *   sidedefs belonging to the line will also be deleted.
 */
void DeleteLinedef(MAP *lpmap, int iLinedef)
{
	/* Delete any sidedefs attached to the line. */
	if(SidedefExists(lpmap, lpmap->linedefs[iLinedef].s1)) DeleteSidedef(lpmap, lpmap->linedefs[iLinedef].s1);
	if(SidedefExists(lpmap, lpmap->linedefs[iLinedef].s2)) DeleteSidedef(lpmap, lpmap->linedefs[iLinedef].s2);

	/* Move all the linedefs above this one down a space. */
	MoveMemory(&lpmap->linedefs[iLinedef], &lpmap->linedefs[iLinedef+1], (lpmap->iLinedefs - iLinedef - 1) * sizeof(MAPLINEDEF));

	/* Decrement linedef count. */
	lpmap->iLinedefs--;
}


/* DeleteSidedef
 *   Deletes a sidedef from a map.
 *
 * Parameters:
 *   MAP*	lpmap		Pointer to map data.
 *   int	iLinedef	Sidedef to delete.
 *
 * Return value: None.
 */
void DeleteSidedef(MAP *lpmap, int iSidedef)
{
	int i;

	/* Move all the sidedefs above this one down a space. */
	MoveMemory(&lpmap->sidedefs[iSidedef], &lpmap->sidedefs[iSidedef+1], (lpmap->iSidedefs - iSidedef - 1) * sizeof(MAPSIDEDEF));

	/* Correct sidedef references on linedefs. */
	for(i = 0; i < lpmap->iLinedefs; i++)
	{
		MAPLINEDEF *lpld = &lpmap->linedefs[i];

		if(lpld->s1 == iSidedef) lpld->s1 = INVALID_SIDEDEF;
		else if(SidedefExists(lpmap, lpld->s1) && lpld->s1 > iSidedef) lpld->s1--;

		if(lpld->s2 == iSidedef) lpld->s2 = INVALID_SIDEDEF;
		else if(SidedefExists(lpmap, lpld->s2) && lpld->s2 > iSidedef) lpld->s2--;
	}

	/* Decrement sidedef count. */
	lpmap->iSidedefs--;
}


/* DeleteVertex
 *   Deletes a vertex from a map.
 *
 * Parameters:
 *   MAP*	lpmap		Pointer to map data.
 *   int	iVertex		Vertex to delete.
 *
 * Return value: None.
 *
 * Remarks:
 *   This performs a simple vertex deletion: no attempt is made to keep the map
 *   correct. Also, this changes vertex indices, so the selection will have to
 *   be rebuilt from flags (or cleared) after calling this function; vertex
 *   references on linedefs will be adjusted automatically, however.
 */
void DeleteVertex(MAP *lpmap, int iVertex)
{
	int i;

	/* Move all the vertices above this one down a space. */
	MoveMemory(&lpmap->vertices[iVertex], &lpmap->vertices[iVertex+1], (lpmap->iVertices - iVertex - 1) * sizeof(MAPVERTEX));

	/* Decrement vertex count. */
	lpmap->iVertices--;

	/* Correct vertex references on linedefs. */
	for(i = lpmap->iLinedefs - 1; i >= 0; i--)
	{
		MAPLINEDEF *lpld = &lpmap->linedefs[i];

		if(lpld->v1 == iVertex || lpld->v2 == iVertex)
			DeleteLinedef(lpmap, i);
		else
		{
			if(lpld->v1 > iVertex) lpld->v1--;
			if(lpld->v2 > iVertex) lpld->v2--;
		}
	}
}


/* DeleteThing
 *   Deletes a thing from a map.
 *
 * Parameters:
 *   MAP*	lpmap		Pointer to map data.
 *   int	iThing		Thing to delete.
 *
 * Return value: None.
 *
 * Remarks:
 *   This changes thing indices, so the selection will have to be rebuilt from
 *   flags (or cleared) after calling this function.
 */
void DeleteThing(MAP *lpmap, int iThing)
{
	/* Move all the things above this one down a space. */
	MoveMemory(&lpmap->things[iThing], &lpmap->things[iThing+1], (lpmap->iThings - iThing - 1) * sizeof(MAPTHING));

	/* Decrement thing count. */
	lpmap->iThings--;
}


/* JoinSectors
 *   Joins two sectors into one.
 *
 * Parameters:
 *   MAP*	lpmap		Pointer to map data.
 *   int	iOldSector	Sector whose references are to be changed.
 *   int	iNewSector	Sector reference to replace iOldSector with.
 *   BOOL	bMerge		If TRUE, removes any linedefs at an interface between
 *						iOldSector and iNewSector.
 *
 * Return value: None.
 *
 * Remarks:
 *   This changes sector and (if merging) linedef indices, so the selection will
 *   have to be rebuilt from flags (or cleared) after calling this function;
 *   sector references on sidedefs will be adjusted automatically, however.
 */
static void JoinSectors(MAP *lpmap, int iNewSector, int iOldSector, BOOL bMerge)
{
	int i;

	/* If we're doing a merge, begin by getting rid of all the interface lines.
	 */
	if(bMerge)
	{
		/* We start at the end, since that means we don't need to worry about
		 * changing indices following a deletion.
		 */
		for(i = lpmap->iLinedefs - 1; i >= 0; i--)
		{
			MAPLINEDEF *lpld = &lpmap->linedefs[i];

			/* Do we have both sides, and if so, is one side part of the old
			 * sector and the other part of the new?
			 */
			if(SidedefExists(lpmap, lpld->s1) && SidedefExists(lpmap, lpld->s2) &&
				((lpmap->sidedefs[lpld->s1].sector == iOldSector && lpmap->sidedefs[lpld->s2].sector == iNewSector)
				 || (lpmap->sidedefs[lpld->s1].sector == iNewSector && lpmap->sidedefs[lpld->s2].sector == iOldSector)))
			{
				/* This line is on the boundary between the old and new sectors,
				 * so delete it.
				 */
				DeleteLinedef(lpmap, i);
			}

		}
	}	/* if(bMerge) */


	/* Loop through all sidedefs, replacing any references to the old sector
	 * with references to the new.
	 */
	for(i = 0; i < lpmap->iSidedefs; i++)
		if(lpmap->sidedefs[i].sector == iOldSector)
			lpmap->sidedefs[i].sector = iNewSector;

	/* Get rid of the old sector. */
	DeleteSector(lpmap, iOldSector);
}


/* JoinSelectedSectors
 *   Joins multiple sectors into one.
 *
 * Parameters:
 *   MAP*				lpmap		Pointer to map data.
 *   SELECTION_LIST*	lpsellist	Selection list, where all sectors are to be
 *									merged into the last of these.
 *   BOOL				bMerge		If TRUE, removes any linedefs at an
 *									interface between old and new sectors.
 *
 * Return value: None.
 *
 * Remarks:
 *   We mangle lpsellist, but that's alright, because this changes sector and
 *   (if merging) linedef indices, so the selections have to be rebuilt anyway.
 *   Sector references on sidedefs will be adjusted automatically.
 */
void JoinSelectedSectors(MAP *lpmap, SELECTION_LIST *lpsellist, BOOL bMerge)
{
	int i;
	DYNAMICINTARRAY *lpdiarrayLDLookupBefore = NULL;

	/* Sanity check. */
	if(lpsellist->iDataCount < 2) return;

	/* If we're merging, we need to keep track of orphaned vertices. */
	if(bMerge)
		InitOrphanVertexCheck(lpmap, &lpdiarrayLDLookupBefore);

	/* Loop through all the 'old' sectors. */
	for(i = lpsellist->iDataCount - 2; i >= 0; i--)
	{
		int j;

		/* Join this sector to the new one. */
		JoinSectors(lpmap, lpsellist->lpiIndices[lpsellist->iDataCount - 1], lpsellist->lpiIndices[i], bMerge);

		/* Correct references, since a sector's been deleted. */
		for(j = 0; j < lpsellist->iDataCount; j++)
			if(lpsellist->lpiIndices[j] > lpsellist->lpiIndices[i])
				lpsellist->lpiIndices[j]--;
	}

	/* Delete any orphaned vertices if we're merging. */
	if(bMerge)
		DeleteOrphanedVertices(lpmap, lpdiarrayLDLookupBefore);
}


/* LabelSelectedLinesRS
 *   Labels all selected lines as needing their sectors recalculated (on both
 *   sides).
 *
 * Parameters:
 *   MAP*		lpmap		Pointer to map data.
 *
 * Return value: None.
 */
void LabelSelectedLinesRS(MAP *lpmap)
{
	int i;

	for(i = 0; i < lpmap->iLinedefs; i++)
		if(lpmap->linedefs[i].selected)
			lpmap->linedefs[i].editflags |= LEF_RECALCSECTOR;
}


/* LabelRSLinesEnclosure
 *   Determines whether each side of each LEF_RECALCSECTOR line should keep its
 *   sector reference, on the appropriate sides.
 *
 * Parameters:
 *   MAP*		lpmap		Pointer to map data.
 *   LOOPLIST*	lplooplist	List of loops having lines marked with any
 *							LEF_RECALCSECTOR bit.
 *
 * Return value: None.
 *
 * Remarks:
 *   Call LabelRSLinesLoops first to find the loops and do the first round of
 *   labelling. We set LEF_ENCLOSEDFRONT and LEF_ENCLOSEDBACK, indicating
 *   whether the sector reference should be kept. The flags should later be
 *   cleared by a call to ClearEnclosureFlags.
 */
void LabelRSLinesEnclosure(MAP *lpmap, LOOPLIST *lplooplist)
{
	int i;

	/* Clear any existing enclosure flags. */
	ClearEnclosureFlags(lpmap);

	/* A side is enclosed if the nearest loop enclosing it is self-enclosed and
	 * of the correct sector.
	 */
	for(i = 0; i < lpmap->iLinedefs; i++)
	{
		if(lpmap->linedefs[i].editflags & LEF_RECALCSECTOR)
			LabelEnclosureFromLoops(lpmap, i, lplooplist);
	}
}



/* LabelEnclosureFromLoops
 *   Considers all loops enclosing a linedef, and if the tightest of these has
 *   the correct sector, then the linedef is marked as enclosed on the
 *   appropriate side.
 *
 * Parameters:
 *   MAP*		lpmap		Pointer to map data.
 *   int		iLinedef	Index of linedef.
 *   LOOPLIST*	lplooplist	Linked list of loops.
 *
 * Return value: None.
 */
static void LabelEnclosureFromLoops(MAP *lpmap, int iLinedef, LOOPLIST *lplooplist)
{
	MAPLINEDEF *lpld = &lpmap->linedefs[iLinedef];
	int i;
	const SIDEFLAGMARKER sfm[] = {{lpld->s1, LEF_ENCLOSEDFRONT, LS_FRONT}, {lpld->s2, LEF_ENCLOSEDBACK, LS_BACK}};

	/* Check the front and back. */
	for(i = 0; i < 2; i++)
	{
		if(SidedefExists(lpmap, sfm[i].iSidedef) && !(lpld->editflags & sfm[i].dwFlag))
		{
			LOOPLIST *lplooplistEnclosing = NULL;
			LOOPLIST **lplplooplistEnclosingNext = &lplooplistEnclosing;
			LOOPLIST *lplooplistRover = lplooplist;
			LOOPLIST *lplooplistTightest;
			FPOINT fpt;

			/* Get a point a little away from the line. */
			GetLineSideSpot(lpmap, iLinedef, sfm[i].iSide, 0.1f, &fpt.x, &fpt.y);

			/* Find all the loops enclosing this sidedef. */
			while(lplooplistRover)
			{
				if(PointInLinedefs(lpmap, fpt.x, fpt.y, lplooplistRover->diarrayLinedefs.lpiIndices, lplooplistRover->diarrayLinedefs.uiCount))
				{
					/* We're inside this loop, so add it to the list. */
					*lplplooplistEnclosingNext = ProcHeapAlloc(sizeof(LOOPLIST));

					/* Make a shallow copy of the loop, but change the next
					 * pointer.
					 */
					**lplplooplistEnclosingNext = *lplooplistRover;
					(*lplplooplistEnclosingNext)->lplooplistNext = NULL;

					lplplooplistEnclosingNext = &(*lplplooplistEnclosingNext)->lplooplistNext;
				}

				lplooplistRover = lplooplistRover->lplooplistNext;
			}

			/* Now that we have all the enclosing loops, find the tightest
			 * one.
			 */
			lplooplistTightest = lplooplistEnclosing;
			lplooplistRover = lplooplistTightest ? lplooplistTightest->lplooplistNext : NULL;
			while(lplooplistRover)
			{
				/* There's potential for optimisation here. Firstly, we need
				 * only consider the current loop if its sector and the
				 * current tightest sector are different wrt the sector of
				 * the ld we're interested in. Then, we can propagate the
				 * tightest loop across adjacent linedefs.
				 */

				/* Get a point on the current loop, and see whether it's
				 * inside the one we're assuming to be tightest.
				 */
				GetLineSideSpot(lpmap, lplooplistRover->diarrayLinedefs.lpiIndices[0], LS_FRONT, 0.0f, &fpt.x, &fpt.y);

				if(PointInLinedefs(lpmap, fpt.x, fpt.y, lplooplistTightest->diarrayLinedefs.lpiIndices, lplooplistTightest->diarrayLinedefs.uiCount))
				{
					/* This loop's tighter! */
					lplooplistTightest = lplooplistRover;
				}

				lplooplistRover = lplooplistRover->lplooplistNext;
			}

			/* If the tightest loop is self-enclosing and matches our
			 * sector, then we're enclosed, too.
			 */
			if(lplooplistTightest && lplooplistTightest->iSector == lpmap->sidedefs[sfm[i].iSidedef].sector)
				lpld->editflags |= sfm[i].dwFlag;

			/* Clean up. These are shallow copies, so we just free the
			 * nodes.
			 */
			while(lplooplistEnclosing)
			{
				lplooplistRover = lplooplistEnclosing->lplooplistNext;
				ProcHeapFree(lplooplistEnclosing);
				lplooplistEnclosing = lplooplistRover;
			}
		}
	}
}


/* LabelRSLinesLoops
 *   Finds all linedef loops, depending on the parameters, containing at least
 *   one RS line or consisting entirely of such lines, labelling them as looped
 *   (and RS if not already) and returning a list of arrays of indices.
 *
 * Parameters:
 *   MAP*		lpmap		Pointer to map data.
 *   BYTE		byFlags		Set LRL_ALLOWPARTIAL to allow loops in which only
 *							one line need be RS.
 *
 * Return value: LOOPLIST*
 *   Linked list of arrays of linedef indices, together with the corresponding
 *   sector index.
 *
 * Remarks:
 *   The lines are labelled with LEF_LOOPFRONT and LEF_LOOPBACK as appropriate.
 *   These are cleared by ClearEnclosureFlags. To free the memory used in the
 *   returned list, call DestroyLoopList.
 */
LOOPLIST* LabelRSLinesLoops(MAP *lpmap, BYTE byFlags)
{
	LOOPLIST *lplooplist = NULL;
	LOOPLIST **lplplooplistNext = &lplooplist;
	DYNAMICINTARRAY *lpldlookup;
	DYNAMICINTARRAY diarrayLinedefs;
	int i, j;

	/* Build vertex-to-ld lookup table. */
	lpldlookup = BuildLDLookupTable(lpmap, LDLT_VXTOLD);

	/* We loop through all the linedefs once and then revisit any that we had to
	 * mark LEF_RECALCSECTOR on our travels which were not previously so marked.
	 * Begin simply with all the linedefs.
	 */
	InitialiseDynamicIntArray(&diarrayLinedefs, lpmap->iLinedefs * 2);
	for(i = 0; i < lpmap->iLinedefs; i++)
		AddToDynamicIntArray(&diarrayLinedefs, i);

	/* Loop through all labelled linedefs. */
	for(i = 0; i < (int)diarrayLinedefs.uiCount; i++)
	{
		int iLinedef = diarrayLinedefs.lpiIndices[i];
		MAPLINEDEF *lpld = &lpmap->linedefs[iLinedef];

		SIDEFLAGMARKER sfm[] =
		{
			{lpld->s1, LEF_LOOPFRONT | LEF_NEWLOOPFRONT | LEF_RSFRONT, LS_FRONT},
			{lpld->s2, LEF_LOOPBACK | LEF_NEWLOOPBACK | LEF_RSBACK, LS_BACK}
		};

		/* Check this line on each of its unmarked sides. */
		for(j = 0; j < 2; j++)
		{
			/* The flags bit: we're not interested if the corresponding
			 * LEF_NEWLOOP* flag is already set, meaning we've already
			 * included it in a loop in this call to LabelRSLinesLoops.
			 */
			if((lpld->editflags & sfm[j].dwFlag & LEF_RECALCSECTOR) &&
				SidedefExists(lpmap, sfm[j].iSidedef) &&
				!(lpld->editflags & sfm[j].dwFlag & (LEF_NEWLOOPFRONT | LEF_NEWLOOPBACK)))
			{
				LOOPLIST looplistNew;
				BOOL bFoundNewLoop;

				/* The details of the loop, if it exists, will be written here.
				 */
				looplistNew.iSector = lpmap->sidedefs[sfm[j].iSidedef].sector;
				InitialiseDynamicIntArray(&looplistNew.diarrayLinedefs, LINELOOPARRAY_INITBUFSIZE);
				InitialiseDynamicIntArray(&looplistNew.diarraySameSide, LINELOOPARRAY_INITBUFSIZE);
				looplistNew.lplooplistNext = NULL;


				/* Look for a loop. */
				bFoundNewLoop = FALSE;
				if(FindLoopFromLine(lpmap, iLinedef, sfm[j].iSide, iLinedef, sfm[j].iSide, looplistNew.iSector, &looplistNew.diarrayLinedefs, &looplistNew.diarraySameSide, lpldlookup, NULL))
				{
					FPOINT fpt;
					unsigned int k;
					BOOL bRSOK = TRUE;

					/* Make sure they're all RS if that's what we want. */
					if(!(byFlags & LRL_ALLOWPARTIAL))
					{
						for(k = 0; bRSOK && k < looplistNew.diarrayLinedefs.uiCount; k++)
							if(!(lpmap->linedefs[looplistNew.diarrayLinedefs.lpiIndices[k]].editflags & LEF_RECALCSECTOR))
								bRSOK = FALSE;
					}

					if(bRSOK)
					{
						/* We found a loop, but it might be on the wrong side.
						 * We check both sides of the line anyway, and what's
						 * more, if the loop's on the wrong side, the wrong
						 * lines may have been selected by angle. So, we only
						 * consider the loop if it's on the correct side.
						 */
						GetLineSideSpot(lpmap, iLinedef, sfm[j].iSide, 0.1f, &fpt.x, &fpt.y);
						if(PointInLinedefs(lpmap, fpt.x, fpt.y, looplistNew.diarrayLinedefs.lpiIndices, looplistNew.diarrayLinedefs.uiCount))
						{
							/* We're on the right side. Note that this loop
							 * *must* be new: we would've skipped right over the
							 * first linedef if it hadn't been.
							 */
							bFoundNewLoop = TRUE;

							/* Label the lines. */
							for(k = 0; k < looplistNew.diarrayLinedefs.uiCount; k++)
							{
								DWORD dwFlag;

								if(looplistNew.diarraySameSide.lpiIndices[k])
									dwFlag = sfm[j].dwFlag;
								else
									dwFlag = sfm[1-j].dwFlag;

								/* If this isn't already RS on this side, we'll need
								 * to come back to it later.
								 */
								if(!(lpmap->linedefs[looplistNew.diarrayLinedefs.lpiIndices[k]].editflags & dwFlag & LEF_RECALCSECTOR))
									AddToDynamicIntArray(&diarrayLinedefs, looplistNew.diarrayLinedefs.lpiIndices[k]);

								lpmap->linedefs[looplistNew.diarrayLinedefs.lpiIndices[k]].editflags |= dwFlag;
							}
						}
					}
				}

				if(bFoundNewLoop)
				{
					/* We found a new loop, so add it to the list of loops. */
					*lplplooplistNext = ProcHeapAlloc(sizeof(LOOPLIST));
					**lplplooplistNext = looplistNew;
					lplplooplistNext = &(*lplplooplistNext)->lplooplistNext;
				}
				else
				{
					/* No loop, so free the arrays we created. */
					FreeDynamicIntArray(&looplistNew.diarrayLinedefs);
					FreeDynamicIntArray(&looplistNew.diarraySameSide);
				}
			}
		}
	}

	/* Clean up. */
	DestroyLDLookupTable(lpmap, lpldlookup);
	FreeDynamicIntArray(&diarrayLinedefs);
	ClearNewLoopFlags(lpmap);

	/* Return the list. */
	return lplooplist;
}


/* LabelLoopedLinesRS
 *   Labels any linedefs inside any of a list of loops as needing its sector
 *   references recalculated.
 *
 * Parameters:
 *   MAP*		lpmap		Pointer to map data.
 *   LOOPLIST*	lplooplist	List of loops.
 *
 * Return value: None.
 *
 * Remarks:
 *   Useful for building up the RS sectors incrementally, at a drag operation's
 *   beginning and end.
 */
void LabelLoopedLinesRS(MAP *lpmap, LOOPLIST *lplooplist)
{
	LOOPLIST *lplooplistRover;
	int i;

	/* Repeat for all linedefs. */
	for(i = 0; i < lpmap->iLinedefs; i++)
	{
		/* If it's already labelled on both sides, there's no point in checking
		 * again.
		 */
		if((lpmap->linedefs[i].editflags & LEF_RECALCSECTOR) != LEF_RECALCSECTOR)
		{
			FPOINT fpt;

			/* Get a point on the line. */
			GetLineSideSpot(lpmap, i, LS_FRONT, 0.0f, &fpt.x, &fpt.y);

			/* Check all the loops. */
			lplooplistRover = lplooplist;
			while(lplooplistRover)
			{
				if(PointInLinedefs(lpmap, fpt.x, fpt.y, lplooplistRover->diarrayLinedefs.lpiIndices, lplooplistRover->diarrayLinedefs.uiCount))
				{
					/* We're inside this loop, so mark our line. */
					lpmap->linedefs[i].editflags |= LEF_RECALCSECTOR;

					/* We know we're inside one loop, so we don't care about the
					 * rest. Go to the next linedef.
					 */
					break;
				}

				lplooplistRover = lplooplistRover->lplooplistNext;
			}
		}
	}
}


/* FindLoopFromLine, FindLoopFromLine_Body
 *   Attempts to find a loop from one linedef to another.
 *
 * Parameters:
 *   MAP*				lpmap				Pointer to map data.
 *   int				iSourceLinedef		Linedef to start looking from.
 *   int				iSourceSide			Side of source linedef to look from.
 *   int				iTargetLinedef		Linedef we're trying to reach.
 *   int				iTargetSide			Side of linedef we're trying to
 *											reach.
 *   int				iSector				Sector of the loop, or negative to
 *											ignore sectors.
 *   DYNAMICINTARRAY*	lpdiarrayLinedefs	Array of linedef indices that form
 *											the loop. Filled by the function if
 *											a loop is found.
 *   DYNAMICINTARRAY*	lpdiarrayOrientations
 *											Array of boolean values indicating
 *											whether the line at the
 *											corresponding position in the loop
 *											orientated the same way as the
 *											source linedef. May be NULL.
 *   DYNAMICINTARRAY*	lpldlookup			Vertex-to-linedef lookup table.
 *   BOOL (*)(MAPLINEDEF*, void*)	fnCondition		Condition to test for
 *													matching lines, or NULL if
 *													not required.
 *
 *  For FindLoopFromLine_Body only:
 *   DYNAMICINTARRAY*	lpdiarrayLabelled	Initialised DIarray to be filled
 *											with indicies of lines we marked
 *											LEF_LABELLED for easy cleaning by
 *											the caller.
 *
 * Return value: BOOL
 *   TRUE if a loop could be found; FALSE if not.
 *
 * Remarks:
 *   This doesn't do the loop labelling, since the entire loop must be known to
 *   do so. FindLoopFromLine is just a stub that calls FindLoopFromLine_Body
 *   (which itself should not be called directly); the former does some cleanup
 *   that the latter, due to its recursive nature, can't. Also, we use the
 *   LEF_LABELLED flag, which must be clear before calling.
 */
static __inline BOOL FindLoopFromLine(MAP *lpmap, int iSourceLinedef, int iSourceSide, int iTargetLinedef, int iTargetSide, int iSector, DYNAMICINTARRAY *lpdiarrayLinedefs, DYNAMICINTARRAY *lpdiarrayOrientations, DYNAMICINTARRAY *lpldlookup, BOOL (*fnCondition)(MAPLINEDEF *lpld, void *lpvParam))
{
	DYNAMICINTARRAY diarrayLabelledLines;
	BOOL bRet;
	int i;

	/* Initialise array of lines to clean. */
	InitialiseDynamicIntArray(&diarrayLabelledLines, 8);

	/* Do the actual business of finding the loop. */
	bRet = FindLoopFromLine_Body(lpmap, iSourceLinedef, iSourceSide, iTargetLinedef, iTargetSide, iSector, lpdiarrayLinedefs, lpdiarrayOrientations, lpldlookup, fnCondition, &diarrayLabelledLines);

	/* Tidy up. */
	for(i = 0; i < (int)diarrayLabelledLines.uiCount; i++)
		lpmap->linedefs[diarrayLabelledLines.lpiIndices[i]].editflags &= ~LEF_LABELLED;

	return bRet;
}

static BOOL __fastcall FindLoopFromLine_Body(MAP *lpmap, int iSourceLinedef, int iSourceSide, int iTargetLinedef, int iTargetSide, int iSector, DYNAMICINTARRAY *lpdiarrayLinedefs, DYNAMICINTARRAY *lpdiarrayOrientations, DYNAMICINTARRAY *lpldlookup, BOOL (*fnCondition)(MAPLINEDEF *lpld, void *lpvParam), DYNAMICINTARRAY *lpdiarrayLabelled)
{
	/* Find the unlabelled linedef that makes the smallest angle with ourselves.
	 */
	int iLookupIndex = 0;
	double dAngle, dPrevAngle = 4 * PI;
	int iNextLinedef;
	int iNextSide;

	while(iNextLinedef = FindNearestAdjacentLinedef(lpmap, lpldlookup, iSourceLinedef, iSourceSide, (iSourceSide == LS_FRONT) ? 1 : 0, &iLookupIndex, &dAngle, fnCondition, NULL),
		dAngle <= dPrevAngle)
	{
		MAPLINEDEF *lpldSource = &lpmap->linedefs[iSourceLinedef];
		MAPLINEDEF *lpldNext = &lpmap->linedefs[iNextLinedef];

		/* Next time, start looking from the next line attached to us. */
		iLookupIndex++;
		dPrevAngle = dAngle;

		/* If we're nearest to ourselves, we're at a dead end. */
		if(iNextLinedef == iSourceLinedef) return FALSE;

		/* If we've been here already, we're not interested. This will only ever
		 * happen with doubled lines.
		 */
		if(lpldNext->editflags & LEF_LABELLED)
			continue;

		/* Label this line, so we don't visit it again. */
		lpldNext->editflags |= LEF_LABELLED;
		AddToDynamicIntArray(lpdiarrayLabelled, iNextLinedef);

		/* Find which side we should consider of the next line. */

		/* Is the next line oriented the same way we are? */
		if(lpldSource->v1 == lpldNext->v2 || lpldSource->v2 == lpldNext->v1)
			iNextSide = iSourceSide;
		else
			iNextSide = (iSourceSide == LS_FRONT) ? LS_BACK : LS_FRONT;

		if(FindLoopFromNextLinedef(lpmap, iSourceLinedef, iSourceSide, iNextLinedef, iNextSide, iTargetLinedef, iTargetSide, iSector, lpdiarrayLinedefs, lpdiarrayOrientations, lpldlookup, fnCondition, lpdiarrayLabelled))
			return TRUE;
	}

	/* If we get here, none of our neighbours worked. */
	return FALSE;
}

/* A helper routine for FindLoopFromLine. */
static BOOL __fastcall FindLoopFromNextLinedef(MAP *lpmap, int iSourceLinedef, int iSourceSide, int iNextLinedef, int iNextSide, int iTargetLinedef, int iTargetSide, int iSector, DYNAMICINTARRAY *lpdiarrayLinedefs, DYNAMICINTARRAY *lpdiarrayOrientations, DYNAMICINTARRAY *lpldlookup, BOOL (*fnCondition)(MAPLINEDEF *lpld, void *lpvParam), DYNAMICINTARRAY *lpdiarrayLabelled)
{
	int iNextSidedef = (iNextSide == LS_FRONT) ? lpmap->linedefs[iNextLinedef].s1 : lpmap->linedefs[iNextLinedef].s2;

	/* Oh dear, no sidedef. This only happens in malformed maps. */
	if(iNextSidedef < 0) return FALSE;

	/* If we require sectors to match and they don't, there's no loop. */
	if(iSector >= 0 && lpmap->sidedefs[iNextSidedef].sector != iSector) return FALSE;

	/* Having checked all those things, we now see no reason why this line
	 * should fail. Check whether we've completed the loop (in which case,
	 * great), or if we haven't got there yet, recur.
	 */
	if((iNextLinedef == iTargetLinedef && iNextSide == iTargetSide)
		|| FindLoopFromLine_Body(lpmap, iNextLinedef, iNextSide, iTargetLinedef, iTargetSide, iSector, lpdiarrayLinedefs, lpdiarrayOrientations, lpldlookup, fnCondition, lpdiarrayLabelled))
	{
		/* We found a loop! Add the index to the array and return. */
		AddToDynamicIntArray(lpdiarrayLinedefs, iSourceLinedef);
		if(lpdiarrayOrientations) AddToDynamicIntArray(lpdiarrayOrientations, (iSourceSide == iTargetSide));
		return TRUE;
	}

	/* If we get here, a subsequent line failed. */
	return FALSE;
}


/* DestroyLoopList
 *   Frees memory used by a list of linedef loops.
 *
 * Parameters:
 *   LOOPLIST*	lplooplist	List to free.
 *
 * Return value: None.
 *
 * Remarks:
 *   Call on the pointer returned by LabelRSLinesLoops.
 */
void DestroyLoopList(LOOPLIST *lplooplist)
{
	LOOPLIST *lplooplistNext;

	while(lplooplist)
	{
		/* Free the arrays. */
		FreeDynamicIntArray(&lplooplist->diarrayLinedefs);
		FreeDynamicIntArray(&lplooplist->diarraySameSide);

		/* Free the list node. */
		lplooplistNext = lplooplist->lplooplistNext;
		ProcHeapFree(lplooplist);
		lplooplist = lplooplistNext;
	}
}


/* ClearDraggingFlags, ClearLoopAndEnclosureFlags, ClearEnclosureFlags,
 * ClearLoopFlags, ClearRSFlag, ClearLinedefsLabelFlag
 *   Clears linedef flags of various sorts.
 *
 * Parameters:
 *   MAP*		lpmap		Pointer to map data.
 *
 * Return value: None.
 */
void ClearDraggingFlags(MAP *lpmap)
{
	ClearChangingFlag(lpmap);
	ClearLoopAndEnclosureFlags(lpmap);
	ClearRSFlag(lpmap);

	ClearVertexFlags(lpmap, VEF_DRAGGING);
}

static __inline void ClearLoopAndEnclosureFlags(MAP *lpmap)
{
	ClearLoopFlags(lpmap);
	ClearEnclosureFlags(lpmap);
}

static __inline void ClearEnclosureFlags(MAP *lpmap)
{
	ClearLineFlags(lpmap, LEF_ENCLOSEDFRONT | LEF_ENCLOSEDBACK);
}

static __inline void ClearLoopFlags(MAP *lpmap)
{
	ClearLineFlags(lpmap, LEF_LOOPFRONT | LEF_LOOPBACK);
}

static __inline void ClearNewLoopFlags(MAP *lpmap)
{
	ClearLineFlags(lpmap, LEF_NEWLOOPFRONT | LEF_NEWLOOPBACK);
}

static __inline void ClearRSFlag(MAP *lpmap)
{
	ClearLineFlags(lpmap, LEF_RECALCSECTOR);
}

static __inline void ClearLinedefsLabelFlag(MAP *lpmap)
{
	ClearLineFlags(lpmap, LEF_LABELLED);
}

static __inline void ClearChangingFlag(MAP *lpmap)
{
	ClearLineFlags(lpmap, LEF_LENGTHCHANGING);
}


/* ClearLineFlags
 *   Clears specified linedef flags.
 *
 * Parameters:
 *   MAP*		lpmap		Pointer to map data.
 *   DWORD		dwFlags		FLags to clear.
 *
 * Return value: None.
 */
void ClearLineFlags(MAP *lpmap, DWORD dwFlags)
{
	int i;

	for(i = 0; i < lpmap->iLinedefs; i++)
		lpmap->linedefs[i].editflags &= ~dwFlags;
}


/* ClearVertexFlags
 *   Clears specified vertex flags.
 *
 * Parameters:
 *   MAP*		lpmap		Pointer to map data.
 *   WORD		wFlags		FLags to clear.
 *
 * Return value: None.
 */
static void ClearVertexFlags(MAP *lpmap, WORD wFlags)
{
	int i;

	for(i = 0; i < lpmap->iVertices; i++)
		lpmap->vertices[i].editflags &= ~wFlags;
}


/* CorrectRSSectorReferences
 *   Sets sector references on dragged sides that are not enclosed from sectors
 *   obtained by considering non-dragged linedefs.
 *
 * Parameters:
 *   MAP*		lpmap		Pointer to map data.
 *
 * Return value: None.
 *
 * Remarks:
 *   The labels are set at the start of a drag operation by
 *   LabelRSLinesEnclosure. In the case of stitched vertices, only
 *   LEF_RECALCSECTOR bits are set, but this is sufficient in this case.
 */
void CorrectRSSectorReferences(MAP *lpmap)
{
	int i, iPass;
	MAPOBJGRID *lpmogLinedefs = CreateLinedefMOG(lpmap);

	for(iPass = FIRST_PASS; iPass <= SECOND_PASS; iPass++)
	{
		for(i = 0; i < lpmap->iLinedefs; i++)
		{
			MAPLINEDEF *lpld = &lpmap->linedefs[i];
			POINT ptOrigin;
			POINT ptOther;
			int iNewSector;

			/* If the side isn't enclosed, set the sector reference from
			 * non-dragged lines.
			 */

			if((lpld->editflags & LEF_RSFRONT) &&
				(iPass == FIRST_PASS || lpld->editflags & LEF_2PASSFRONT) &&
				!(lpld->editflags & LEF_ENCLOSEDFRONT))
			{
				GetOrthHLPointsIn2Coords(lpmap, i, LS_FRONT, &ptOrigin, &ptOther);

				iNewSector = GetSectorByHalfLineMethod(lpmap, lpmogLinedefs, &ptOrigin, &ptOther);
				if(iNewSector >= 0)
				{
					if(SidedefExists(lpmap, lpld->s1))
						lpmap->sidedefs[lpld->s1].sector = iNewSector;
					else
					{
						int iNewSidedef = AddSidedef(lpmap, iNewSector);
						if(iNewSidedef != INVALID_SIDEDEF)
							SetLinedefSidedef(lpmap, i, iNewSidedef, LS_FRONT);
					}

					lpld->editflags |= LEF_NEWSECSETFRONT;
				}
				else if(SidedefExists(lpmap, lpld->s1))
				{
					if(iPass == FIRST_PASS)
					{
						/* Suspect line, so flag to look at later. */
						lpld->editflags |= LEF_2PASSFRONT;
					}
					/* Never delete if we fail due to starvation. Somewhat
					 * experimental.
					 */
					else if(iNewSector != NOSEC_STARVATION && SidedefExists(lpmap, lpld->s2))
					{
						/* Couldn't find a sector on either pass, so remove the
						 * redundant sidedef and flip the linedef.
						 */
						FlipLinedef(lpmap, i);
						DeleteSidedef(lpmap, lpld->s2);
						SetLinedefSidedef(lpmap, i, INVALID_SIDEDEF, LS_BACK);
					}
				}
				/* We never delete our *only* sidedef. */

				ClearLinedefsLabelFlag(lpmap);
			}

			if((lpld->editflags & LEF_RSBACK) &&
				(iPass == FIRST_PASS || lpld->editflags & LEF_2PASSBACK) &&
				!(lpld->editflags & LEF_ENCLOSEDBACK))
			{
				GetOrthHLPointsIn2Coords(lpmap, i, LS_BACK, &ptOrigin, &ptOther);

				iNewSector = GetSectorByHalfLineMethod(lpmap, lpmogLinedefs, &ptOrigin, &ptOther);
				if(iNewSector >= 0)
				{
					if(SidedefExists(lpmap, lpld->s2))
						lpmap->sidedefs[lpld->s2].sector = iNewSector;
					else
					{
						int iNewSidedef = AddSidedef(lpmap, iNewSector);
						if(iNewSidedef != INVALID_SIDEDEF)
							SetLinedefSidedef(lpmap, i, iNewSidedef, LS_BACK);
					}

					lpld->editflags |= LEF_NEWSECSETBACK;
				}
				else if(SidedefExists(lpmap, lpld->s2))
				{
					if(iPass == FIRST_PASS)
					{
						/* Suspect line, so flag to look at later. */
						lpld->editflags |= LEF_2PASSBACK;
					}
					/* Never delete if we fail due to starvation. Somewhat
					 * experimental.
					 */
					else if(iNewSector != NOSEC_STARVATION)
					{
						/* Couldn't find a sector on either pass, so remove the
						 * redundant sidedef.
						 */
						DeleteSidedef(lpmap, lpld->s2);
						SetLinedefSidedef(lpmap, i, INVALID_SIDEDEF, LS_BACK);
					}
				}

				ClearLinedefsLabelFlag(lpmap);
			}
		}
	}

	ClearLineFlags(lpmap, LEF_2PASSFRONT | LEF_2PASSBACK | LEF_NEWSECSETFRONT | LEF_NEWSECSETBACK);

	if(lpmogLinedefs)
		DestroyMapObjGrid(lpmogLinedefs);
}


/* FlipVertexAboutVerticalAxis, FlipThingAboutVerticalAxis,
 * FlipVertexAboutHorizontalAxis, FlipThingAboutHorizontalAxis
 *   Flips the appropriate sort of object about a line of constant abscissa or
 *   ordinate.
 *
 * Parameters:
 *   MAP*		lpmap		Pointer to map data.
 *   int		iVertex		Index of vertex or thing.
 *				iThing
 *   short		xAxis		Abscissa or ordinate.
 *				yAxis
 *
 * Return value: None.
 */
void FlipVertexAboutVerticalAxis(MAP *lpmap, int iVertex, short xAxis)
{
	lpmap->vertices[iVertex].x = 2 * (int)xAxis - (int)lpmap->vertices[iVertex].x;
}

void FlipThingAboutVerticalAxis(MAP *lpmap, int iThing, short xAxis)
{
	lpmap->things[iThing].x = 2 * (int)xAxis - (int)lpmap->things[iThing].x;
	lpmap->things[iThing].angle = 180 - lpmap->things[iThing].angle + ((lpmap->things[iThing].angle <= 180) ? 0 : 360);
}

void FlipVertexAboutHorizontalAxis(MAP *lpmap, int iVertex, short yAxis)
{
	lpmap->vertices[iVertex].y = 2 * (int)yAxis - (int)lpmap->vertices[iVertex].y;
}

void FlipThingAboutHorizontalAxis(MAP *lpmap, int iThing, short yAxis)
{
	lpmap->things[iThing].y = 2 * (int)yAxis - (int)lpmap->things[iThing].y;
	lpmap->things[iThing].angle = 360 - lpmap->things[iThing].angle;
	if(lpmap->things[iThing].angle == 360) lpmap->things[iThing].angle = 0;
}


/* LinedefIntersectsHalfLine
 *   Determines whether a linedef intersects a half-line.
 *
 * Parameters:
 *   MAP*		lpmap			Pointer to map data.
 *   int		iLinedef		Index of linedef.
 *   int		xLine1, yLine1	Origin of half-line in 2coords.
 *   int		xLine2, yLine2	Another point on the half-line in 2coords.
 *   FPOINT*	lpfpIntersect	The point of intersection is returned here, if
 *								it exists, in 1coords.
 *
 * Return value: BOOL
 *   TRUE if they intersect, FALSE if not.
 */
static BOOL LinedefIntersectsHalfLine(MAP *lpmap, int iLinedef, int xLine1, int yLine1, int xLine2, int yLine2, FPOINT *lpfpIntersect)
{
	/* Vector orthogonal to direction vector of half-line. */
	int cxHalfDirOrth = yLine1 - yLine2;
	int cyHalfDirOrth = xLine2 - xLine1;

	/* Co-ordinates of origin of linedef wrt origin of half-line. */
	int xLDOrigin = 2 * lpmap->vertices[lpmap->linedefs[iLinedef].v1].x - xLine1;
	int yLDOrigin = 2 * lpmap->vertices[lpmap->linedefs[iLinedef].v1].y - yLine1;

	/* Direction vector for linedef, with length equal to that of the linedef,
	 * which is important when testing the parameter range.
	 */
	int cxLinedef = 2 * ((int)lpmap->vertices[lpmap->linedefs[iLinedef].v2].x - lpmap->vertices[lpmap->linedefs[iLinedef].v1].x);
	int cyLinedef = 2 * ((int)lpmap->vertices[lpmap->linedefs[iLinedef].v2].y - lpmap->vertices[lpmap->linedefs[iLinedef].v1].y);

	LONGLONG i64DotOrth;
	LONGLONG i64LinedefParamNumerator;
	LONGLONG i64HalfLineParamNumerator;


	/* The point at where the lines intersect, if they do so at a point, has as
	 * its parameter in the vector equation of the linedef,
	 *     -LDOrigin . HLDirectionOrth
	 *    -----------------------------
	 *    LDDirection . HLDirectionOrth ,
	 *
	 * where we observe that the denominator is nonzero iff the lines do indeed
	 * intersect at a point. Furthermore, since the LD dir vector was
	 * constructed so as to have length equal to that of the linedef, the
	 * intersection point lies on the linedef iff the parameter lies in [0,1].
	 */

	i64DotOrth = (LONGLONG)cxHalfDirOrth * (LONGLONG)cxLinedef + (LONGLONG)cyHalfDirOrth * (LONGLONG)cyLinedef;

	/* Parallel? */
	if(i64DotOrth == 0)
		return FALSE;

	/* Compute parameter as detailed above. */
	i64LinedefParamNumerator = -((LONGLONG)cxHalfDirOrth * (LONGLONG)xLDOrigin + (LONGLONG)cyHalfDirOrth * (LONGLONG)yLDOrigin);

	/* Ensure we lie on the linedef itself, and not its extension as a line. */
	if(((i64LinedefParamNumerator < 0) ^ (i64DotOrth < 0)) ||
		((i64LinedefParamNumerator > i64DotOrth) ^ (i64DotOrth < 0)))
	{
		return FALSE;
	}

	/* Make sure we lie on the half-line, and not its extension in the other
	 * direction. We do this by calculating the parameter on the half-line in a
	 * similar manner to the above. We use orthogonal co-ordinates because it
	 * happens to save us some calculation.
	 */
	i64HalfLineParamNumerator = -(LONGLONG)yLDOrigin * (LONGLONG)cxLinedef + (LONGLONG)xLDOrigin * (LONGLONG)cyLinedef;
	if(i64HalfLineParamNumerator == 0 ||
		((i64HalfLineParamNumerator < 0) ^ (i64DotOrth < 0)))
	{
		return FALSE;
	}

	/* We intersect. Compute the point at which we do so, in 1coords. */
	lpfpIntersect->x = (float)(xLine1 + xLDOrigin + (i64LinedefParamNumerator * cxLinedef) / (float)i64DotOrth) / 2;
	lpfpIntersect->y = (float)(yLine1 + yLDOrigin + (i64LinedefParamNumerator * cyLinedef) / (float)i64DotOrth) / 2;

	return TRUE;
}


/* FindNearestLinedefOnHalfLine
 *   Finds the linedef that intersects a half-line closest to its origin.
 *
 * Parameters:
 *   MAP*			lpmap			Pointer to map data.
 *   MAPOBJGRID*	lpmogLinedefs	MOG of linedefs. May be NULL.
 *   int			xLine1, yLine1	Origin of half-line in 2coords.
 *   int			xLine2, yLine2	Another point on the half-line in 2coords.
 *
 * Return value: int
 *   Index of linedef, or negative if none exists.
 */
static int FindNearestLinedefOnHalfLine(MAP *lpmap, MAPOBJGRID *lpmogLinedefs, int xLine1, int yLine1, int xLine2, int yLine2)
{
	int i;
	int iNearestLD = -1;
	float fMinDist = 100000.0f;
	float fNextCellDist;
	short nCol = 0, nRow = 0;
	const int xLine1_1C = xLine1 / 2;
	const int yLine1_1C = yLine1 / 2;
	const int xLine2_1C = xLine2 / 2;
	const int yLine2_1C = yLine2 / 2;

	/* If we have a MOG, find out which cell we start in. */
	if(lpmogLinedefs)
	{
		nCol = GetMOGColFromAbscissa(lpmogLinedefs, xLine1_1C);
		nRow = GetMOGRowFromOrdinate(lpmogLinedefs, yLine1_1C);
	}

	do
	{
		/* If we have a MOG, loop through lines in the current cell; otherwise,
		 * loop through all linedefs.
		 */

		DYNAMICINTARRAY *lpdiarrayCell = lpmogLinedefs ? GET_MOG_CELL(lpmogLinedefs, nCol, nRow) : NULL;
		int iUpperBound = lpmogLinedefs ? (int)lpdiarrayCell->uiCount : lpmap->iLinedefs;

		for(i = 0; i < iUpperBound; i++)
		{
			/* Handle the extra indirection for a MOG. */
			int iLinedef = lpmogLinedefs ? lpdiarrayCell->lpiIndices[i] : i;
			FPOINT fpIntersect;
			int yAdj1 = yLine1, yAdj2 = yLine2, xAdj1 = xLine1, xAdj2 = xLine2;
			BOOL bIntersect = LinedefIntersectsHalfLine(lpmap, iLinedef, xAdj1, yAdj1, xAdj2, yAdj2, &fpIntersect);

			if(bIntersect && PointOnLineVertex(lpmap, iLinedef, &fpIntersect))
			{
				const int MIN_SEGMENT_L1LENGTH = 1000;

				/* We hit a vertex, so perturb the endpoint a little. We make
				 * the point a bit further away first, if necessary.
				 */
				if(abs(yLine2 - yLine1) + abs(xLine2 - xLine1) < MIN_SEGMENT_L1LENGTH)
				{
					int iScale = MIN_SEGMENT_L1LENGTH / (abs(yLine2 - yLine1) + abs(xLine1 - xLine2));
					xAdj2 += iScale * (xLine2 - xLine1);
					yAdj2 += iScale * (yLine2 - yLine1);
				}

				/* Perturb! */
				xAdj2++;

				bIntersect = LinedefIntersectsHalfLine(lpmap, iLinedef, xAdj1, yAdj1, xAdj2, yAdj2, &fpIntersect);
			}

			if(bIntersect)
			{
				float fDist = distancef(fpIntersect.x, fpIntersect.y, (float)xAdj1 / 2, (float)yAdj1 / 2);
				if(fDist < fMinDist)
				{
					fMinDist = fDist;
					iNearestLD = iLinedef;
				}
			}
		}

		/* If we have a MOG and haven't found anything yet, try to advance to
		 * the next cell.
		 */
	} while(lpmogLinedefs &&
		GetNextLineSegmentMOGCell(lpmogLinedefs, xLine1_1C, yLine1_1C, xLine2_1C, yLine2_1C, &nCol, &nRow, &fNextCellDist, TRUE) &&
		(iNearestLD < 0 || fNextCellDist < fMinDist));

	return iNearestLD;
}


/* GetSectorByHalfLineMethod
 *   Finds the sector at a point in a given direction using an algorithm
 *   involving recursive searches along half-lines.
 *
 * Parameters:
 *   MAP*			lpmap			Pointer to map data.
 *   MAPOBJGRID*	lpmogLinedefs	MOG of linedefs. May be NULL.
 *   POINT*			lpptOrigin		Origin of half-line, in doubled
 *									co-ordinates.
 *   POINT*			lpptOther		Another point on the half-line, in doubled
 *									co-ordinates.
 *
 * Return value: int
 *   Index of sector if found, NOSEC_SPACE if we're pointing at a null sidedef,
 *   NOSEC_INFINITY if we're facing into the deep beyond and NOSEC_STARVATION if
 *   we gave up. These sentinel values are all negative.
 *
 * Remarks:
 *   Only non-dragged lines are considered. This function is intended for
 *   setting sector references following a drag.
 *
 *   We set LEF_LABELLED on our travels. Clean up behind us!
 */
static int GetSectorByHalfLineMethod(MAP *lpmap, MAPOBJGRID *lpmogLinedefs, POINT *lpptOrigin, POINT *lpptOther)
{
	int iLinedef = FindNearestLinedefOnHalfLine(lpmap, lpmogLinedefs, lpptOrigin->x, lpptOrigin->y, lpptOther->x, lpptOther->y);

	if(iLinedef >= 0)
	{
		MAPLINEDEF *lpld = &lpmap->linedefs[iLinedef];
		int iSide;

		/* If we've visited it already, give up. */
		if(lpld->editflags & LEF_LABELLED)
			return NOSEC_STARVATION;

		/* Figure out which sidedef we should be using. */
		iSide = SideOfLinedefInt2Coords(lpmap, iLinedef, lpptOrigin);

		/* Are we allowed to use this side? */
		if(!(lpld->editflags & (iSide == LS_FRONT ? LEF_RSFRONT : LEF_RSBACK)) ||
			(lpld->editflags & (iSide == LS_FRONT ? (LEF_ENCLOSEDFRONT | LEF_NEWSECSETFRONT) : (LEF_ENCLOSEDBACK | LEF_NEWSECSETBACK))))
		{
			int iSidedef = (iSide == LS_FRONT) ? lpld->s1 : lpld->s2;

			/* Return the sector (unless there is none). */
			return SidedefExists(lpmap, (unsigned short)iSidedef) ? lpmap->sidedefs[iSidedef].sector : NOSEC_SPACE;
		}
		else
		{
			/* Can't use this line, so bifurcate along it -- but not quite
			 * parallel!
			 */
			POINT ptMidPoint;
			POINT ptVx1, ptVx2;
			POINT vecLinedef, vecLDOrtho;
			int iSector;
			const int SCALE_FACTOR = 100;

			/* If we ever end up here again, give up. */
			lpld->editflags |= LEF_LABELLED;

			/* Get co-ordinates of the line's vertices in 2coords. */
			ptVx1.x = 2 * lpmap->vertices[lpld->v1].x;
			ptVx1.y = 2 * lpmap->vertices[lpld->v1].y;
			ptVx2.x = 2 * lpmap->vertices[lpld->v2].x;
			ptVx2.y = 2 * lpmap->vertices[lpld->v2].y;

			/* Linedef vectors in 2coords. */
			vecLinedef.x = ptVx2.x - ptVx1.x;
			vecLinedef.y = ptVx2.y - ptVx1.y;
			vecLDOrtho.x = (iSide == LS_FRONT ? 1 : -1) * vecLinedef.y;
			vecLDOrtho.y = (iSide == LS_FRONT ? -1 : 1) * vecLinedef.x;

			/* Get mid-point of line in 2coords. */
			ptMidPoint.x = (int)lpmap->vertices[lpld->v1].x + lpmap->vertices[lpld->v2].x;
			ptMidPoint.y = (int)lpmap->vertices[lpld->v1].y + lpmap->vertices[lpld->v2].y;

			/* Try half-line almost-parallel to linedef and towards v1 first;
			 * if that fails, try v2.
			 */
			ptVx1.x += -SCALE_FACTOR * vecLinedef.x + vecLDOrtho.x;
			ptVx1.y += -SCALE_FACTOR * vecLinedef.y + vecLDOrtho.y;

			iSector = GetSectorByHalfLineMethod(lpmap, lpmogLinedefs, &ptMidPoint, &ptVx1);
			if(iSector >= 0)
				return iSector;

			ptVx2.x += SCALE_FACTOR * vecLinedef.x + vecLDOrtho.x;
			ptVx2.y += SCALE_FACTOR * vecLinedef.y + vecLDOrtho.y;

			return GetSectorByHalfLineMethod(lpmap, lpmogLinedefs, &ptMidPoint, &ptVx2);
		}
	}

	/* No linedef found at all. */
	return NOSEC_INFINITY;
}


/* SetThingSelectionZ
 *   For the selected things, sets the z-offset.
 *
 * Parameters:
 *   MAP*				lpmap				Pointer to map data.
 *   SELECTION_LIST*	lpsellist			List of selected things.
 *   CONFIG*			lpcfgFlatThings		Flat things section.
 *   WORD				z					Z-co-ordinate.
 *   BOOL				bAbsolute			Whether the co-ordinate is absolute.
 *
 * Return value: None.
 */
void SetThingSelectionZ(MAP *lpmap, SELECTION_LIST *lpsellist, CONFIG *lpcfgFlatThings, WORD z, BOOL bAbsolute)
{
	int i;

	/* Repeat for each selected thing. */
	for(i = 0; i < lpsellist->iDataCount; i++)
		SetThingZ(lpmap, lpsellist->lpiIndices[i], lpcfgFlatThings, z, bAbsolute);
}


/* SetThingZ
 *   For the specified thing, sets the z-offset.
 *
 * Parameters:
 *   MAP*		lpmap				Pointer to map data.
 *   int		iThing				Index of thing.
 *   CONFIG*	lpcfgFlatThings		Flat things section.
 *   WORD		z					Z-co-ordinate.
 *   BOOL		bAbsolute			Whether the co-ordinate is absolute.
 *
 * Return value: None.
 */
__inline void SetThingZ(MAP *lpmap, int iThing, CONFIG *lpcfgFlatThings, WORD z, BOOL bAbsolute)
{
	WORD wZFactor = GetZFactor(lpcfgFlatThings, lpmap->things[iThing].thing);

	/* First, clear all the bits that specify the z ofsset. */
	lpmap->things[iThing].flag &= wZFactor - 1;

	/* Adjust if absolute. */
	if(bAbsolute)
	{
		int iSector = IntersectSector(lpmap->things[iThing].x, lpmap->things[iThing].y, lpmap, NULL, NULL);
		if(iSector >= 0) z = max(0, (int)z - lpmap->sectors[iSector].hfloor);
	}

	/* Now, set the bits that specify it, leaving the lower bits intact. */
	lpmap->things[iThing].flag |= z * wZFactor;
}


/* GetThingZ
 *   For the specified thing, returns the z-offset.
 *
 * Parameters:
 *   MAP*		lpmap				Pointer to map data.
 *   int		iThing				Index of thing.
 *   CONFIG*	lpcfgFlatThings		Flat things section.
 *   BOOL		bAbsolute			Whether the returned value should be
 *									absolute.
 *
 * Return value: int
 *   Z-offset, or absolute z.
 */
int GetThingZ(MAP *lpmap, int iThing, CONFIG *lpcfgFlatThings, BOOL bAbsolute)
{
	WORD wZFactor = GetZFactor(lpcfgFlatThings, lpmap->things[iThing].thing);
	int z;

	/* Get the z from the flag. */
	z = lpmap->things[iThing].flag / wZFactor;

	/* Adjust if absolute. */
	if(bAbsolute)
	{
		int iSector = IntersectSector(lpmap->things[iThing].x, lpmap->things[iThing].y, lpmap, NULL, NULL);
		if(iSector >= 0) z += lpmap->sectors[iSector].hfloor;
	}

	return z;
}


/* StitchMultipleVertices
 *   Makes all lines referencing any number of vertices point to one other.
 *
 * Parameters:
 *   MAP*			lpmap				Pointer to map data.
 *   const int*		lpiSrcVertices		Indices of old vertices.
 *   unsigned int	uiNumSrcVertices	Number of old vertices.
 *   int			iTargetVertex		Index of new vertex.
 *   DWORD			dwFlags				Flags. See SMVF_* in editing.h.
 *
 * Return value: None.
 *
 * Remarks:
 *   LEF_DELETE must be clear before calling this function. We also set
 *   LEF_RECALCSECTOR for the *caller* to deal with; this would normally be done
 *   by calling CorrectRSSectorReferences.
 */
void StitchMultipleVertices(MAP *lpmap, const int *lpiSrcVertices, unsigned int uiNumSrcVertices, int iTargetVertex, DWORD dwFlags)
{
	int i;

	/* Loop through all linedefs and change any references to the old vertex to
	 * point to the new one. Lines that should be deleted, either because they
	 * would become zero-length or because another one would land on top of
	 * them, are labelled and then deleted later.
	 */
	for(i = 0; i < lpmap->iLinedefs; i++)
	{
		MAPLINEDEF *lpld = &lpmap->linedefs[i];

		if(!(lpld->editflags & LEF_DELETE))
		{
			int iExistingLinedef;

			/* Need these so we can take their addresses. */
			int iV1 = lpld->v1;
			int iV2 = lpld->v2;

			/* This is where we delete lines made redundant by stitching. */

			if(_lfind(&iV1, lpiSrcVertices, &uiNumSrcVertices, sizeof(int), QsortIntegerComparison))
			{
				/* We're stitching our V1 to the target vertex. */

				if(lpld->v2 != iTargetVertex)
				{
					/* Are we going to land on top of another line? */
					if((iExistingLinedef = FindLinedefBetweenVertices(lpmap, iTargetVertex, lpld->v2)) >= 0)
					{
						/* Delete the existing linedef. */
						lpmap->linedefs[iExistingLinedef].editflags |= LEF_DELETE;

						/* Fix the sector references on ourselves. */
						lpld->editflags |= LEF_RECALCSECTOR;
					}

					lpld->v1 = iTargetVertex;
				}
				/* This line would become zero-length. Delete it. */
				else lpld->editflags |= LEF_DELETE;
			}
			else if(_lfind(&iV2, lpiSrcVertices, &uiNumSrcVertices, sizeof(int), QsortIntegerComparison))
			{
				/* We're stitching our V2 to the target vertex. */

				if(lpld->v1 != iTargetVertex)
				{
					/* Are we going to land on top of another line? */
					if((iExistingLinedef = FindLinedefBetweenVertices(lpmap, iTargetVertex, lpld->v1)) >= 0)
					{
						/* Delete the existing linedef. */
						lpmap->linedefs[iExistingLinedef].editflags |= LEF_DELETE;

						/* Fix the sector references on ourselves. */
						lpld->editflags |= LEF_RECALCSECTOR;
					}

					lpld->v2 = iTargetVertex;
				}
				/* This line would become zero-length. Delete it. */
				else lpld->editflags |= LEF_DELETE;
			}
		}
	}

	/* If any of the old vertices were selected, so should the new one be. */
	for(i = 0; (unsigned int)i < uiNumSrcVertices; i++)
		if(lpmap->vertices[lpiSrcVertices[i]].selected)
		{
			lpmap->vertices[iTargetVertex].selected = SLF_SELECTED;
			break;
		}

	/* It's safe to delete the lines now. */
	for(i = lpmap->iLinedefs - 1; i >= 0; i--)
		if(lpmap->linedefs[i].editflags & LEF_DELETE)
			DeleteLinedef(lpmap, i);

	/* We can delete the old vertices now, if the caller wants us to. */
	if(dwFlags & SMVF_DELETE)
	{
		/* Copy the indices, since we need to sort them. */
		int *lpiSrcCopy = ProcHeapAlloc(uiNumSrcVertices * sizeof(int));
		CopyMemory(lpiSrcCopy, lpiSrcVertices, uiNumSrcVertices * sizeof(int));
		qsort(lpiSrcCopy, uiNumSrcVertices, sizeof(int), QsortIntegerComparison);

		for(i = (signed int)(uiNumSrcVertices - 1); i >= 0; i--)
			DeleteVertex(lpmap, lpiSrcCopy[i]);

		ProcHeapFree(lpiSrcCopy);
	}

	/* Leave RS labels for the caller. We don't clear LEF_DELETE, since we
	 * deleted all of those lines.
	 */
}


/* StitchDraggedVertices
 *   Auto-stitches all dragged vertices.
 *
 * Parameters:
 *   MAP*				lpmap				Pointer to map data.
 *   SELECTION_LIST*	lpsellistVertices	Indices of dragged vertices.
 *
 * Return value: BOOL
 *   TRUE if any stitching was performed; FALSE otherwise.
 *
 * Remarks:
 *   Call CorrectRSSectorReferences and then ClearDraggingFlags afterwards.
 *   This fits in handily with dragging.
 */
BOOL StitchDraggedVertices(MAP *lpmap, SELECTION_LIST *lpsellistVertices)
{
	int i;
	const int iStitchDistance = ConfigGetInteger(g_lpcfgMain, OPT_AUTOSTITCHDISTANCE);
	BOOL bMapChanged = FALSE;
	MAPOBJGRID *lpmogUndraggedVertices, *lpmogLinedefs;
	DYNAMICINTARRAY diarrayVerticesToDelete;

	/* Sanity check. */
	if(lpsellistVertices->iDataCount <= 0) return FALSE;

	/* Label all the moving linedefs. */
	for(i = 0; i < lpmap->iLinedefs; i++)
	{
		MAPLINEDEF *lpld = &lpmap->linedefs[i];
		if(ExistsInSelectionList(lpsellistVertices, lpld->v1) || ExistsInSelectionList(lpsellistVertices, lpld->v2))
			lpld->editflags |= LEF_LABELLED;
	}

	/* Dragged vertices should not snap to one another. */
	for(i = 0; i < lpsellistVertices->iDataCount; i++)
		lpmap->vertices[lpsellistVertices->lpiIndices[i]].editflags |= VEF_LABELLED;

	/* We have to delete vertices en masse to preserve the MOG. The initial
	 * buffer size here is just a handy guess; usually too big but potentially
	 * too small.
	 */
	InitialiseDynamicIntArray(&diarrayVerticesToDelete, lpsellistVertices->iDataCount);

	/* Get a rough idea of where all the lines and vertices are. */
	lpmogUndraggedVertices = CreateConditionedVertexMOG(lpmap, VertexUnlabelled, NULL);
	lpmogLinedefs = CreateLinedefMOG(lpmap);

	/* Loop through all vertices, stitching as necessary. We loop backwards so
	 * we can delete safely. We also label them for later use.
	 */
	for(i = lpsellistVertices->iDataCount - 1; i >= 0; i--)
	{
		const int iVertex = lpsellistVertices->lpiIndices[i];
		int iDistance, iLineDistance;
		MAPVERTEX *lpvx = &lpmap->vertices[iVertex];

		/* NB: We still need to do a conditioned search, as vertices may become
		 * labelled after being added to the MOG.
		 */
		int iNearestVertex = NearestConditionedVertex(lpmap, lpmogUndraggedVertices, lpvx->x, lpvx->y, &iDistance, VertexUnlabelled, NULL);
		int iNearestLinedef = NearestConditionedLinedef(lpmap, lpmogLinedefs, lpvx->x, lpvx->y, &iLineDistance, LinedefUnlabelled, (void*)iVertex);

		if(iNearestVertex >= 0 && iDistance <= iStitchDistance)
		{
			/* Replace references to the existing vertex, and delete it later. */
			lpmap->vertices[iNearestVertex].editflags |= VEF_LABELLED;
			ReplaceVertexReferences(lpmap, iVertex, iNearestVertex);
			AddToDynamicIntArray(&diarrayVerticesToDelete, iVertex);

			bMapChanged = TRUE;
		}
		else if(iNearestLinedef >= 0 && iLineDistance <= iStitchDistance)
		{
			/* Split line at vertex. Do it via a new vertex so that we avoid
			 * doubled lines.
			 */
			int iNewVertex = AddVertex(lpmap, lpvx->x, lpvx->y);

			/* Don't add it to the MOG, since we won't need it again. */

			lpmap->vertices[iNewVertex].editflags |= VEF_LABELLED;
			SplitLinedef(lpmap, iNearestLinedef, iNewVertex);
			ReplaceVertexReferences(lpmap, iVertex, iNewVertex);
			AddToDynamicIntArray(&diarrayVerticesToDelete, iVertex);
			bMapChanged = TRUE;
		}
	}


	/* Check all dragged lines and see if any of them are close enough to
	 * non-dragged vertices.
	 */
	for(i = 0; i < lpmap->iLinedefs; i++)
	{
		if(lpmap->linedefs[i].editflags & LEF_LABELLED)
		{
			int iDistance;
			int iNearestStaticVertex;

			/* NB: We still need to do a conditioned search, as vertices may
			 * become labelled after being added to the MOG.
			 */
			while(iNearestStaticVertex = NearestConditionedVertexToLinedef(lpmap, lpmogUndraggedVertices, i, &iDistance, VertexUnlabelledAndNotAttToLine, (void*)i),
				iNearestStaticVertex >= 0 && iDistance <= iStitchDistance)
			{
				/* Split line at vertex. Do it via a new vertex so that we avoid
				 * doubled lines.
				 */
				MAPVERTEX *lpvx = &lpmap->vertices[iNearestStaticVertex];
				int iNewVertex = AddVertex(lpmap, lpvx->x, lpvx->y);

				/* Don't add it to the MOG, since we won't need it again. */

				lpmap->vertices[iNewVertex].editflags |= VEF_LABELLED;
				lpmap->vertices[iNearestStaticVertex].editflags |= VEF_LABELLED;
				SplitLinedef(lpmap, i, iNewVertex);
				ReplaceVertexReferences(lpmap, iNearestStaticVertex, iNewVertex);
				AddToDynamicIntArray(&diarrayVerticesToDelete, iNearestStaticVertex);
				bMapChanged = TRUE;
			}
		}
	}

	/* Finished with the MOGs now. */
	if(lpmogUndraggedVertices)
		DestroyMapObjGrid(lpmogUndraggedVertices);

	if(lpmogLinedefs)
		DestroyMapObjGrid(lpmogLinedefs);

	/* Delete all vertices now that we no longer care about changing indices. */
	SortDynamicIntArray(&diarrayVerticesToDelete);
	for(i = (int)diarrayVerticesToDelete.uiCount - 1; i >= 0; i--)
		DeleteVertex(lpmap, diarrayVerticesToDelete.lpiIndices[i]);

	/* Remove our labelling. */
	ClearLinedefsLabelFlag(lpmap);
	ClearVertexFlags(lpmap, VEF_LABELLED);

	return bMapChanged;
}


/* DeleteZeroLengthLinedefs
 *   Deletes zero-length linedefs, both those with both vertices the same and
 *   both vertices co-incident.
 *
 * Parameters:
 *   MAP*				lpmap				Pointer to map data.
 *
 * Return value: None.
 *
 * Remarks:
 *   Zero-length lines interfere with our loop algorithms. So we get rid of
 *   them. I *think* they're illegal; if they're not, I suppose this'll need to
 *   go.
 */
void DeleteZeroLengthLinedefs(MAP *lpmap)
{
	int i;

	for(i = lpmap->iLinedefs - 1; i >= 0; i--)
	{
		if(lpmap->linedefs[i].v1 == lpmap->linedefs[i].v2)
			DeleteLinedef(lpmap, i);
		else if(lpmap->vertices[lpmap->linedefs[i].v1].x == lpmap->vertices[lpmap->linedefs[i].v2].x &&
			lpmap->vertices[lpmap->linedefs[i].v1].y == lpmap->vertices[lpmap->linedefs[i].v2].y)
		{
			StitchVertices(lpmap, lpmap->linedefs[i].v1, lpmap->linedefs[i].v2);

			/* This upset linedef numbering, so make sure we're still in range.
			 */
			i = min(i, lpmap->iLinedefs - 1);
		}
	}
}


/* RequiredTextures
 *   Determines which textures are necessary for a linedef.
 *
 * Parameters:
 *   MAP*		lpmap		Pointer to map data.
 *   int		iLinedef	Index of linedef.
 *   LPCTSTR	szSky		Name of sky texture. Empty string for none.
 *
 * Return value: BYTE
 *   Combination of flags specifying the necessary textures. See
 *   ENUM_LDTEX_FLAGS for these.
 */
BYTE RequiredTextures(MAP *lpmap, int iLinedef, LPCTSTR szSky)
{
	BYTE byRequirementFlags = 0;
	MAPLINEDEF *lpld = &lpmap->linedefs[iLinedef];
	MAPSECTOR *lpsecFront = NULL, *lpsecBack = NULL;
	BOOL bFrontSkyCeil = FALSE, bFrontSkyFloor = FALSE;
	BOOL bBackSkyCeil = FALSE, bBackSkyFloor = FALSE;

#ifdef UNICODE
	char szSkyA[TEXNAME_BUFFER_LENGTH];
	WideCharToMultiByte(CP_ACP, 0, szSky, -1, szSkyA, NUM_ELEMENTS(szSkyA), NULL, NULL);
#else
	LPCSTR szSkyA = szSky;
#endif

	if(SidedefExists(lpmap, lpld->s1))
	{
		lpsecFront = &lpmap->sectors[lpmap->sidedefs[lpld->s1].sector];
		if(*szSky)
		{
			bFrontSkyCeil = !strcmp(lpsecFront->tceiling, szSkyA);
			bFrontSkyFloor = !strcmp(lpsecFront->tfloor, szSkyA);
		}
	}

	if(SidedefExists(lpmap, lpld->s2))
	{
		lpsecBack = &lpmap->sectors[lpmap->sidedefs[lpld->s2].sector];
		if(*szSky)
		{
			bBackSkyCeil = !strcmp(lpsecBack->tceiling, szSkyA);
			bBackSkyFloor = !strcmp(lpsecBack->tfloor, szSkyA);
		}
	}

	if(lpsecFront && lpsecBack)
	{
		/* If the front sector's zero-height, we don't need any textures on the
		 * front sidedef.
		 */
		if(lpsecFront->hceiling != lpsecFront->hfloor)
		{
			if(!bBackSkyCeil && lpsecFront->hceiling > lpsecBack->hceiling)
				byRequirementFlags = LDTF_FRONTUPPER;

			if(!bBackSkyFloor && lpsecFront->hfloor < lpsecBack->hfloor)
				byRequirementFlags |= LDTF_FRONTLOWER;
		}

		/* Do the same on the back as we did on the front. */
		if(lpsecBack->hceiling != lpsecBack->hfloor)
		{
			if(!bFrontSkyCeil && lpsecBack->hceiling > lpsecFront->hceiling)
				byRequirementFlags = LDTF_BACKUPPER;

			if(!bFrontSkyFloor && lpsecBack->hfloor < lpsecFront->hfloor)
				byRequirementFlags |= LDTF_BACKLOWER;
		}
	}
	else if(lpsecFront)
	{
		/* Single-sided on the front. */
		if(lpsecFront->hfloor != lpsecFront->hceiling)
			byRequirementFlags = LDTF_FRONTMIDDLE;
	}
	else if(lpsecBack)
	{
		/* Single-sided on the back... Erm... */
		if(lpsecBack->hfloor != lpsecBack->hceiling)
			byRequirementFlags = LDTF_BACKMIDDLE;
	}

	return byRequirementFlags;
}


/* ApplyRelativeCeilingHeight, ApplyRelativeFloorHeight
 *   Offsets ceiling or floor height of a sector.
 *
 * Parameters:
 *   MAP*	lpmap		Pointer to map data.
 *   int	iSector		Index of sector.
 *   int	iDelta		Value to add to floor/ceiling.
 *
 * Return value: None.
 *
 * Remarks:
 *   If the change takes the height out of range, it is capped at the maximum or
 *   minimum value: wraparound is prevented.
 */
static __inline void ApplyRelativeCeilingHeight(MAP *lpmap, int iSector, int iDelta)
{
	MAPSECTOR *lpsec = &lpmap->sectors[iSector];

	lpsec->hceiling = max(-32768, min(32767, lpsec->hceiling + iDelta));
}

static __inline void ApplyRelativeFloorHeight(MAP *lpmap, int iSector, int iDelta)
{
	MAPSECTOR *lpsec = &lpmap->sectors[iSector];

	lpsec->hfloor = max(-32768, min(32767, lpsec->hfloor + iDelta));
}


/* ApplyRelativeBrightness
 *   Offsets brightness of a sector.
 *
 * Parameters:
 *   MAP*	lpmap		Pointer to map data.
 *   int	iSector		Index of sector.
 *   short	nDelta		Value to add to brightness.
 *
 * Return value: None.
 *
 * Remarks:
 *   If the change takes the brightness out of range, it is capped at the
 *   maximum or minimum value: wraparound is prevented.
 */
static __inline void ApplyRelativeBrightness(MAP *lpmap, int iSector, short nDelta)
{
	MAPSECTOR *lpsec = &lpmap->sectors[iSector];

	lpsec->brightness = max(0, min(255, lpsec->brightness + nDelta));
}


/* ApplyRelativeThingZ
 *   Offsets z-offset of a thing.
 *
 * Parameters:
 *   MAP*		lpmap				Pointer to map data.
 *   int		iThing				Index of thing.
 *   int		iDelta				Value to add to z-offset.
 *   CONFIG*	lpcfgFlatThings		Flat thing config subsection.
 *
 * Return value: None.
 *
 * Remarks:
 *   If the change takes the height out of range, it is capped at the maximum or
 *   minimum value: no wraparound occurs.
 */
static __inline void ApplyRelativeThingZ(MAP *lpmap, int iThing, int iDelta, CONFIG *lpcfgFlatThings)
{
	/* When setting the z-offset, it will be further capped to the valid values
	 * for the particular type of thing.
	 */
	SetThingZ(lpmap, iThing, lpcfgFlatThings, (WORD)max(0, min(65535, GetThingZ(lpmap, iThing, lpcfgFlatThings, FALSE) + iDelta)), FALSE);
}


/* ApplyRelativeAngle
 *   Offsets angle of a thing.
 *
 * Parameters:
 *   MAP*		lpmap				Pointer to map data.
 *   int		iThing				Index of thing.
 *   int		iDelta				Value to add to angle.
 *
 * Return value: None.
 *
 * Remarks:
 *   If the change takes the angle out of range, it is capped at the maximum or
 *   minimum value: no wraparound occurs.
 */
static __inline void ApplyRelativeAngle(MAP *lpmap, int iThing, int iDelta)
{
	MAPTHING *lpthing = &lpmap->things[iThing];
	lpthing->angle = max(-32768, min(32767, lpthing->angle + iDelta));
}


/* ApplyRelativeThingX, ApplyRelativeThingY, ApplyRelativeThingFlags
 *   Offsets co-ordinates or flags of a thing.
 *
 * Parameters:
 *   MAP*		lpmap				Pointer to map data.
 *   int		iThing				Index of thing.
 *   int		iDelta				Value to add to co-ordinate.
 *
 * Return value: None.
 *
 * Remarks:
 *   If the change takes the co-ordinate out of range, it is capped at the
 *   maximum or minimum value: no wraparound occurs.
 */
static __inline void ApplyRelativeThingX(MAP *lpmap, int iThing, int iDelta)
{
	MAPTHING *lpthing = &lpmap->things[iThing];
	lpthing->x = max(-32768, min(32767, lpthing->x + iDelta));
}

static __inline void ApplyRelativeThingY(MAP *lpmap, int iThing, int iDelta)
{
	MAPTHING *lpthing = &lpmap->things[iThing];
	lpthing->y = max(-32768, min(32767, lpthing->y + iDelta));
}

static __inline void ApplyRelativeThingFlags(MAP *lpmap, int iThing, int iDelta)
{
	MAPTHING *lpthing = &lpmap->things[iThing];
	lpthing->flag = max(0, min(65535, lpthing->flag + iDelta));
}


/* ApplyRelativeSidedefX, ApplyRelativeSidedefY
 *   Offsets offsets (!) of a sidedef.
 *
 * Parameters:
 *   MAP*		lpmap				Pointer to map data.
 *   int		iSidedef			Index of sidedef.
 *   int		iDelta				Value to add to offset.
 *
 * Return value: None.
 *
 * Remarks:
 *   If the change takes the co-ordinate out of range, it is capped at the
 *   maximum or minimum value: no wraparound occurs.
 */
static __inline void ApplyRelativeSidedefX(MAP *lpmap, int iSidedef, int iDelta)
{
	MAPSIDEDEF *lpsd = &lpmap->sidedefs[iSidedef];
	lpsd->tx = max(-32768, min(32767, lpsd->tx + iDelta));
}

static __inline void ApplyRelativeSidedefY(MAP *lpmap, int iSidedef, int iDelta)
{
	MAPSIDEDEF *lpsd = &lpmap->sidedefs[iSidedef];
	lpsd->ty = max(-32768, min(32767, lpsd->ty + iDelta));
}


/* ApplyRelative*Selection
 *   Offsets a property of selected objects by a given value.
 *
 * Parameters:
 *   MAP*				lpmap			Map.
 *   SELECTION_LIST*	lpsellist...	Selection
 *   int/short			iDelta/nDelta	Offset.
 *
 * Return value: None.
 */
void ApplyRelativeCeilingHeightSelection(MAP *lpmap, SELECTION_LIST *lpsellistSectors, int iDelta)
{
	int i;
	for(i = 0; i < lpsellistSectors->iDataCount; i++)
		ApplyRelativeCeilingHeight(lpmap, lpsellistSectors->lpiIndices[i], iDelta);
}

void ApplyRelativeFloorHeightSelection(MAP *lpmap, SELECTION_LIST *lpsellistSectors, int iDelta)
{
	int i;
	for(i = 0; i < lpsellistSectors->iDataCount; i++)
		ApplyRelativeFloorHeight(lpmap, lpsellistSectors->lpiIndices[i], iDelta);
}

void ApplyRelativeBrightnessSelection(MAP *lpmap, SELECTION_LIST *lpsellistSectors, short nDelta)
{
	int i;
	for(i = 0; i < lpsellistSectors->iDataCount; i++)
		ApplyRelativeBrightness(lpmap, lpsellistSectors->lpiIndices[i], nDelta);
}

void ApplyRelativeThingZSelection(MAP *lpmap, SELECTION_LIST *lpsellistThings, CONFIG *lpcfgFlatThings, int iDelta)
{
	int i;

	for(i = 0; i < lpsellistThings->iDataCount; i++)
		ApplyRelativeThingZ(lpmap, lpsellistThings->lpiIndices[i], iDelta, lpcfgFlatThings);
}

void ApplyRelativeAngleSelection(MAP *lpmap, SELECTION_LIST *lpsellistThings, int iDelta)
{
	int i;

	for(i = 0; i < lpsellistThings->iDataCount; i++)
		ApplyRelativeAngle(lpmap, lpsellistThings->lpiIndices[i], iDelta);
}

void ApplyRelativeThingXSelection(MAP *lpmap, SELECTION_LIST *lpsellistThings, int iDelta)
{
	int i;

	for(i = 0; i < lpsellistThings->iDataCount; i++)
		ApplyRelativeThingX(lpmap, lpsellistThings->lpiIndices[i], iDelta);
}

void ApplyRelativeThingYSelection(MAP *lpmap, SELECTION_LIST *lpsellistThings, int iDelta)
{
	int i;

	for(i = 0; i < lpsellistThings->iDataCount; i++)
		ApplyRelativeThingY(lpmap, lpsellistThings->lpiIndices[i], iDelta);
}

void ApplyRelativeThingFlagsSelection(MAP *lpmap, SELECTION_LIST *lpsellistThings, int iDelta)
{
	int i;

	for(i = 0; i < lpsellistThings->iDataCount; i++)
		ApplyRelativeThingFlags(lpmap, lpsellistThings->lpiIndices[i], iDelta);
}

void ApplyRelativeFrontSidedefXSelection(MAP *lpmap, SELECTION_LIST *lpsellistLines, int iDelta)
{
	int i;

	for(i = 0; i < lpsellistLines->iDataCount; i++)
	{
		int iSidedef = lpmap->linedefs[lpsellistLines->lpiIndices[i]].s1;
		if(SidedefExists(lpmap, iSidedef))
			ApplyRelativeSidedefX(lpmap, iSidedef, iDelta);
	}
}

void ApplyRelativeFrontSidedefYSelection(MAP *lpmap, SELECTION_LIST *lpsellistLines, int iDelta)
{
	int i;

	for(i = 0; i < lpsellistLines->iDataCount; i++)
	{
		int iSidedef = lpmap->linedefs[lpsellistLines->lpiIndices[i]].s1;
		if(SidedefExists(lpmap, iSidedef))
			ApplyRelativeSidedefY(lpmap, iSidedef, iDelta);
	}
}

void ApplyRelativeBackSidedefXSelection(MAP *lpmap, SELECTION_LIST *lpsellistLines, int iDelta)
{
	int i;

	for(i = 0; i < lpsellistLines->iDataCount; i++)
	{
		int iSidedef = lpmap->linedefs[lpsellistLines->lpiIndices[i]].s2;
		if(SidedefExists(lpmap, iSidedef))
			ApplyRelativeSidedefX(lpmap, iSidedef, iDelta);
	}
}

void ApplyRelativeBackSidedefYSelection(MAP *lpmap, SELECTION_LIST *lpsellistLines, int iDelta)
{
	int i;

	for(i = 0; i < lpsellistLines->iDataCount; i++)
	{
		int iSidedef = lpmap->linedefs[lpsellistLines->lpiIndices[i]].s2;
		if(SidedefExists(lpmap, iSidedef))
			ApplyRelativeSidedefY(lpmap, iSidedef, iDelta);
	}
}


/* GradientSelected*
 *   Gradients a property of the selected sectors.
 *
 * Parameters:
 *   MAP*				lpmap				Pointer to map data.
 *   SELECTION_LIST*	lpsellistSectors	Selection.
 *
 * Return value: None.
 */
void GradientSelectedCeilings(MAP *lpmap, SELECTION_LIST *lpsellistSectors)
{
	int i;
	int iDiff, iBase;

	/* Sanity check. */
	if(lpsellistSectors->iDataCount < 2) return;

	iBase = lpmap->sectors[lpsellistSectors->lpiIndices[0]].hceiling;
	iDiff = lpmap->sectors[lpsellistSectors->lpiIndices[lpsellistSectors->iDataCount - 1]].hceiling - iBase;

	for(i = 1; i < lpsellistSectors->iDataCount - 1; i++)
		lpmap->sectors[lpsellistSectors->lpiIndices[i]].hceiling = iBase + (iDiff * i) / (lpsellistSectors->iDataCount - 1);
}

void GradientSelectedFloors(MAP *lpmap, SELECTION_LIST *lpsellistSectors)
{
	int i;
	int iDiff, iBase;

	/* Sanity check. */
	if(lpsellistSectors->iDataCount < 2) return;

	iBase = lpmap->sectors[lpsellistSectors->lpiIndices[0]].hfloor;
	iDiff = lpmap->sectors[lpsellistSectors->lpiIndices[lpsellistSectors->iDataCount - 1]].hfloor - iBase;

	for(i = 1; i < lpsellistSectors->iDataCount - 1; i++)
		lpmap->sectors[lpsellistSectors->lpiIndices[i]].hfloor = iBase + (iDiff * i) / (lpsellistSectors->iDataCount - 1);
}

void GradientSelectedBrightnesses(MAP *lpmap, SELECTION_LIST *lpsellistSectors)
{
	int i;
	int iDiff, iBase;

	/* Sanity check. */
	if(lpsellistSectors->iDataCount < 2) return;

	iBase = lpmap->sectors[lpsellistSectors->lpiIndices[0]].brightness;
	iDiff = lpmap->sectors[lpsellistSectors->lpiIndices[lpsellistSectors->iDataCount - 1]].brightness - iBase;

	for(i = 1; i < lpsellistSectors->iDataCount - 1; i++)
		lpmap->sectors[lpsellistSectors->lpiIndices[i]].brightness = iBase + (iDiff * i) / (lpsellistSectors->iDataCount - 1);
}


/* GradientSelectedThingZ
 *   Gradients the heights of selected things.
 *
 * Parameters:
 *   MAP*				lpmap				Pointer to map data.
 *   SELECTION_LIST*	lpsellistThings		Selection.
 *   CONFIG*			lpcfgFlatThings		Flat things subsection.
 *
 * Return value: None.
 */
void GradientSelectedThingZ(MAP *lpmap, SELECTION_LIST *lpsellistThings, CONFIG *lpcfgFlatThings)
{
	int i;
	int iDiff, iBase;

	/* Sanity check. */
	if(lpsellistThings->iDataCount < 2) return;

	iBase = GetThingZ(lpmap, lpsellistThings->lpiIndices[0], lpcfgFlatThings, FALSE);
	iDiff = GetThingZ(lpmap, lpsellistThings->lpiIndices[lpsellistThings->iDataCount - 1], lpcfgFlatThings, FALSE) - iBase;

	for(i = 1; i < lpsellistThings->iDataCount - 1; i++)
		SetThingZ(lpmap, lpsellistThings->lpiIndices[i], lpcfgFlatThings, (WORD)(iBase + (iDiff * i) / (lpsellistThings->iDataCount - 1)), FALSE);
}


/* RotateVertexAboutPoint
 *   Rotates a vertex about a point.
 *
 * Parameters:
 *   MAP*	lpmap		Pointer to map data.
 *   int	iVertex		Index of vertex to rotate.
 *   short	x, y		Point about which to rotate.
 *   float	fAngle		Angle through which to rotate, in degrees.
 *
 * Return value: None.
 */
void RotateVertexAboutPoint(MAP *lpmap, int iVertex, short x, short y, float fAngle)
{
	MAPVERTEX *lpvx = &lpmap->vertices[iVertex];
	short xTranslated = lpvx->x - x;
	short yTranslated = lpvx->y - y;
	double dCosTheta = cos(fAngle * PI / 180.0);
	double dSinTheta = sin(fAngle * PI / 180.0);

	/* Apply the appropriate rotation matrix to the translated point, and
	 * translate back again.
	 */
	lpvx->x = (short)(dCosTheta * xTranslated - dSinTheta * yTranslated) + x;
	lpvx->y = (short)(dSinTheta * xTranslated + dCosTheta * yTranslated) + y;
}


/* RotateThingAboutPoint
 *   Rotates a thing about a point.
 *
 * Parameters:
 *   MAP*	lpmap		Pointer to map data.
 *   int	iThing		Index of thing to rotate.
 *   short	x, y		Point about which to rotate.
 *   float	fAngle		Angle through which to rotate.
 *
 * Return value: None.
 */
void RotateThingAboutPoint(MAP *lpmap, int iThing, short x, short y, float fAngle)
{
	MAPTHING *lpthing = &lpmap->things[iThing];
	short xTranslated = lpthing->x - x;
	short yTranslated = lpthing->y - y;
	double dCosTheta = cos(fAngle * PI / 180.0);
	double dSinTheta = sin(fAngle * PI / 180.0);

	/* Apply the appropriate rotation matrix to the translated point, and
	 * translate back again.
	 */
	lpthing->x = (short)(dCosTheta * xTranslated - dSinTheta * yTranslated) + x;
	lpthing->y = (short)(dSinTheta * xTranslated + dCosTheta * yTranslated) + y;
}


/* RotateThingDirection
 *   Rotates a thing's direction by the specified angle, trying to be clever
 *   about whether we should stay from 0 to 359.
 *
 * Parameters:
 *   MAP*	lpmap		Pointer to map data.
 *   int	iThing		Index of thing to rotate.
 *   int	iAngle		Angle through which to rotate.
 *
 * Return value: None.
 */
void RotateThingDirection(MAP *lpmap, int iThing, int iAngle)
{
	MAPTHING *lpthing = &lpmap->things[iThing];

	/* Should we take the residue (mod 360)? */
	if(lpthing->angle < 360 && lpthing->angle >= 0 && iAngle < 360 && iAngle > -360)
	{
		lpthing->angle += iAngle;
		lpthing->angle %= 360;

		if(lpthing->angle < 0) lpthing->angle += 360;
	}
	else lpthing->angle += iAngle;
}


/* DilateVertexWrtFixedPoint
 *   Dilates a vertex with respect to a fixed point.
 *
 * Parameters:
 *   MAP*	lpmap		Pointer to map data.
 *   int	iVertex		Index of vertex to dilate.
 *   short	x, y		Fixed point.
 *   float  fFactor		Dilation percentage.
 *
 * Return value: None.
 */
void DilateVertexWrtFixedPoint(MAP *lpmap, int iVertex, short x, short y, float fFactor)
{
	MAPVERTEX *lpvx = &lpmap->vertices[iVertex];

	/* Translate, dilate, translate back. */
	lpvx->x = (short)((double)(fFactor * (int)(lpvx->x - x)) / 100.0 + x);
	lpvx->y = (short)((double)(fFactor * (int)(lpvx->y - y)) / 100.0 + y);
}


/* DilateThingWrtFixedPoint
 *   Dilates a thing with respect to a fixed point.
 *
 * Parameters:
 *   MAP*	lpmap		Pointer to map data.
 *   int	iThing		Index of thing to dilate.
 *   short	x, y		Fixed point.
 *   float  fFactor		Dilation percentage.
 *
 * Return value: None.
 */
void DilateThingWrtFixedPoint(MAP *lpmap, int iThing, short x, short y, float fFactor)
{
	MAPTHING *lpthing = &lpmap->things[iThing];

	/* Translate, dilate, translate back. */
	lpthing->x = (short)((double)(fFactor * (int)(lpthing->x - x)) / 100.0 + x);
	lpthing->y = (short)((double)(fFactor * (int)(lpthing->y - y)) / 100.0 + y);
}


/* RotateThingToPoint
 *   Rotates a thing to face a point.
 *
 * Parameters:
 *   MAP*	lpmap		Pointer to map data.
 *   int	iThing		Index of thing to rotate.
 *   short	x, y		Point to which to rotate.
 *
 * Return value: None.
 */
void RotateThingToPoint(MAP *lpmap, int iThing, short x, short y)
{
	MAPTHING *lpthing = &lpmap->things[iThing];
	short cx = x - lpthing->x;
	short cy = y - lpthing->y;

	/* Can't look at my feet... */
	if(cx == 0 && cy == 0) return;

	/* Calculate and set the angle to the point. */
	lpthing->angle = (short)((180 * atan2(cy, cx)) / PI);

	if(lpthing->angle < 0) lpthing->angle += 360;
}


/* LabelChangingLines
 *   Labels any lines whose lengths might change due to dragging.
 *
 * Parameters:
 *   MAP*				lpmap				Pointer to map data.
 *   SELECTION_LIST*	lpsellistVertices	Selected vertices, most likely
 *											coming from the aux. selection.
 *
 * Return value: None.
 */
void LabelChangingLines(MAP *lpmap, SELECTION_LIST *lpsellistVertices)
{
	int i;
	BOOL *lpbMovingVertices = ProcHeapAlloc(lpmap->iVertices * sizeof(BOOL));

	/* Find which vertices are moving. */
	ZeroMemory(lpbMovingVertices, lpmap->iVertices * sizeof(BOOL));
	for(i = 0; i < lpsellistVertices->iDataCount; i++)
		lpbMovingVertices[lpsellistVertices->lpiIndices[i]] = TRUE;

	for(i = 0; i < lpmap->iLinedefs; i++)
	{
		MAPLINEDEF *lpld = &lpmap->linedefs[i];

		/* Logical XOR of the vertices' moving status. */
		if(!lpbMovingVertices[lpld->v1] != !lpbMovingVertices[lpld->v2])
			lpld->editflags |= LEF_LENGTHCHANGING;
	}

	ProcHeapFree(lpbMovingVertices);
}


/* LabelDraggingVertices
 *   Labels all vertices as being dragged.
 *
 * Parameters:
 *   MAP*				lpmap				Pointer to map data.
 *   SELECTION_LIST*	lpsellistVertices	Selected vertices, most likely
 *											coming from the aux. selection.
 *
 * Return value: None.
 */
void LabelDraggingVertices(MAP *lpmap, SELECTION_LIST *lpsellistVertices)
{
	int i;
	for(i = 0; i < lpsellistVertices->iDataCount; i++)
		lpmap->vertices[lpsellistVertices->lpiIndices[i]].editflags |= VEF_DRAGGING;
}


/* AutoalignTexturesFromLinedef
 *   Aligns textures on sidedefs beginning at a specified linedef.
 *
 * Parameters:
 *   MAP*	lpmap			Pointer to map data.
 *   HWND	hwndMap			Map window handle.
 *   int	iStartLinedef	Linedef to begin aligning from.
 *   BYTE	byTexFlags		ENUM_LDTEX_FLAGS specifying which textures to align.
 *   BOOL	bInSelection	Whether to consider selected lines only.
 *
 * Return value: None.
 */
void AutoalignTexturesFromLinedef(MAP *lpmap, HWND hwndMap, int iStartLinedef, BYTE byTexFlags, BOOL bInSelection)
{
	DYNAMICINTARRAY *lpdiarrayLDLookup = BuildLDLookupTable(lpmap, LDLT_VXTOLD);

	/* Start the recursion. */
	AutoalignRecursion(lpmap, hwndMap, iStartLinedef, byTexFlags, bInSelection, lpdiarrayLDLookup, FALSE);

	/* Clear the flags we set. */
	ClearLineFlags(lpmap, LEF_VISITED);

	DestroyLDLookupTable(lpmap, lpdiarrayLDLookup);
}


/* AutoalignAllTextures
 *   Aligns textures on sidedefs belonging to all (optionally selected)
 *   linedefs.
 *
 * Parameters:
 *   MAP*	lpmap			Pointer to map data.
 *   HWND	hwndMap			Map window handle.
 *   BYTE	byTexFlags		ENUM_LDTEX_FLAGS specifying which textures to align.
 *   BOOL	bInSelection	Whether to consider selected lines only.
 *
 * Return value: None.
 */
void AutoalignAllTextures(MAP *lpmap, HWND hwndMap, BYTE byTexFlags, BOOL bInSelection)
{
	DYNAMICINTARRAY *lpdiarrayLDLookup = BuildLDLookupTable(lpmap, LDLT_VXTOLD);
	int i;
	DWORD dwVisitedFlags = TexFlagsToVisitedFlags(byTexFlags);

	/* Repeat for each linedef. */
	for(i = 0; i < lpmap->iLinedefs; i++)
	{
		MAPLINEDEF *lpld = &lpmap->linedefs[i];

		/* Is there anything left that we can attempt to align to? */
		if((lpld->editflags & dwVisitedFlags) != dwVisitedFlags && (!bInSelection || lpld->selected))
		{
			BYTE byMaskedTexFlags = byTexFlags & (VisitedFlagsToTexFlags(lpld->editflags) ^ LDTF_ALL);
			lpld->editflags |= TexFlagsToVisitedFlags(byMaskedTexFlags);

			/* Start the recursion. */
			AutoalignRecursion(
				lpmap,
				hwndMap,
				i,
				byMaskedTexFlags,
				bInSelection,
				lpdiarrayLDLookup,
				TRUE);
		}
	}

	/* Clear the flags we set. */
	ClearLineFlags(lpmap, LEF_VISITED);

	DestroyLDLookupTable(lpmap, lpdiarrayLDLookup);
}


/* AutoalignRecursion
 *   Does the recursive donkey work for aligning textures on sidedefs beginning
 *   at a specified linedef.
 *
 * Parameters:
 *   MAP*				lpmap				Pointer to map data.
 *   HWND				hwndMap				Map window handle.
 *   int				iStartLinedef		Linedef to begin aligning from.
 *   BYTE				byTexFlags			ENUM_LDTEX_FLAGS specifying which
 *											textures to align.
 *   BOOL				bInSelection		Whether to consider selected lines
 *											only.
 *   DYNAMICINTARRAY*	lpdiarrayLDLookup	Vertex-to-linedef lookup table.
 *   BOOL				bLabelOnChangeOnly	If TRUE, lines are marked as visited
 *											only when they are changed, and then
 *											only for the changed textures.
 *
 * Return value: None.
 */
static void AutoalignRecursion(MAP *lpmap, HWND hwndMap, int iStartLinedef, BYTE byTexFlags, BOOL bInSelection, DYNAMICINTARRAY *lpdiarrayLDLookup, BOOL bLabelOnChangeOnly)
{
	int i, iV;
	const int iVertices[2] = {lpmap->linedefs[iStartLinedef].v1, lpmap->linedefs[iStartLinedef].v2};

	/* Quick termination case. Not necessary, but more efficient. */
	if(!byTexFlags) return;

	/* Label ourselves so we don't end up back here again later. */
	if(!bLabelOnChangeOnly)
		lpmap->linedefs[iStartLinedef].editflags |= LEF_VISITED;

	/* Repeat for each line adjacent to ourselves which we haven't already
	 * visited and, if required, is selected.
	 */
	for(iV = 0; iV < (int)(sizeof(iVertices)/sizeof(int)); iV++)
		for(i = 0; i < (int)lpdiarrayLDLookup[iVertices[iV]].uiCount; i++)
		{
			int iLinedef = lpdiarrayLDLookup[iVertices[iV]].lpiIndices[i];
			MAPLINEDEF *lpld = &lpmap->linedefs[iLinedef];
			int iSide;
			const DWORD dwVisitedFlagsSide[] = {LEF_VISITED_FRONT, LEF_VISITED_BACK};
			const BYTE byTexFlagsSide[] = {LDTF_FRONT, LDTF_BACK};
			BYTE byChangedTexFlags = 0;

			/* Are we interested in this line? */
			if(iLinedef != iStartLinedef && (!bInSelection || lpld->selected))
			{
				for(iSide = 0; iSide < 2; iSide++)
				{
					/* Are we interested in this side? */
					if(!(lpld->editflags & dwVisitedFlagsSide[iSide]))
					{
						/* Align to each of the necessary textures, taking a
						 * note of which textures we had to move.
						 */
						byChangedTexFlags |= AlignAdjacentTextures(lpmap, hwndMap, iStartLinedef, iLinedef, byTexFlags & byTexFlagsSide[iSide], iV);

						if(bLabelOnChangeOnly)
							lpld->editflags |= TexFlagsToVisitedFlags(byChangedTexFlags);
					}
				}

				/* Recur! On the next line, we're concerned with the textures we
				 * changed. byTexFlags only has meaning for iStartLinedef.
				 */
				AutoalignRecursion(lpmap, hwndMap, iLinedef, byChangedTexFlags, bInSelection, lpdiarrayLDLookup, bLabelOnChangeOnly);
			}
		}
}


/* AlignAdjacentTextures
 *   Aligns specified textures on two adjacent linedefs.
 *
 * Parameters:
 *   MAP*				lpmap				Pointer to map data.
 *   HWND				hwndMap				Map window handle.
 *   int				iSourceLinedef		Linedef to begin aligning from.
 *   int				iAlignLinedef		Linedef to align.
 *   BYTE				byTexFlags			ENUM_LDTEX_FLAGS specifying which
 *											textures to align.
 *   int				iWhichVertex		0 or 1 to indicate respectively that
 *											the linedefs are connected at the
 *											source linedef's V1 or V2.
 *
 * Return value: BYTE
 *   ENUM_LDTEX_FLAGS indicating which textures on the aligning linedef were
 *   aligned.
 */
static BYTE AlignAdjacentTextures(MAP *lpmap, HWND hwndMap, int iSourceLinedef, int iAlignLinedef, BYTE byTexFlags, int iWhichVertex)
{
	const BYTE byAllTexFlags[][3] = {{LDTF_FRONTUPPER, LDTF_FRONTMIDDLE, LDTF_FRONTLOWER}, {LDTF_BACKUPPER, LDTF_BACKMIDDLE, LDTF_BACKLOWER}};
	MAPLINEDEF *lpldSource = &lpmap->linedefs[iSourceLinedef];
	MAPLINEDEF *lpldAlign = &lpmap->linedefs[iAlignLinedef];
	BOOL bFaceSameWay = (lpldSource->v1 == lpldAlign->v2 || lpldSource->v2 == lpldAlign->v1);
	MAPSIDEDEF *lpsdSource[] = {NULL, NULL};
	MAPSIDEDEF *lpsdAlignAdj[] = {NULL, NULL};
	MAPSECTOR *lpsecSource[] = {NULL, NULL};
	MAPSECTOR *lpsecAlignAdj[] = {NULL, NULL};
	const int iSourceLength = LinedefLength(lpmap, iSourceLinedef);
	const int iAlignLength = LinedefLength(lpmap, iAlignLinedef);
	LPSTR sSourceTextures[][3] = {{NULL, NULL, NULL}, {NULL, NULL, NULL}};
	LPSTR sAlignTextures[][3] = {{NULL, NULL, NULL}, {NULL, NULL, NULL}};
	LPSTR sMatchTextures[] = {NULL, NULL};
	int i, iTexture;
	BYTE byAligned = 0;



	/* Get pointers to the various sectors and sidedefs, or leave as NULL if
	 * they don't apply.
	 */

	for(i = 0; i < 2; i++)
	{
		int iSourceSD = (i == LS_FRONT) ? lpldSource->s1 : lpldSource->s2;

		/* If lines are facing opposite ways, we want the sidedefs for the line
		 * *not* specified by i.
		 */
		int iAlignSD = ((i == LS_FRONT) ^ !bFaceSameWay) ? lpldAlign->s1 : lpldAlign->s2;

		if(SidedefExists(lpmap, iSourceSD))
		{
			lpsdSource[i] = &lpmap->sidedefs[iSourceSD];
			sSourceTextures[i][SDT_UPPER] = lpsdSource[i]->upper;
			sSourceTextures[i][SDT_MIDDLE] = lpsdSource[i]->middle;
			sSourceTextures[i][SDT_LOWER] = lpsdSource[i]->lower;

			if(lpsdSource[i]->sector >= 0)
				lpsecSource[i] = &lpmap->sectors[lpsdSource[i]->sector];
		}

		if(SidedefExists(lpmap, iAlignSD))
		{
			lpsdAlignAdj[i] = &lpmap->sidedefs[iAlignSD];
			sAlignTextures[i][SDT_UPPER] = lpsdAlignAdj[i]->upper;
			sAlignTextures[i][SDT_MIDDLE] = lpsdAlignAdj[i]->middle;
			sAlignTextures[i][SDT_LOWER] = lpsdAlignAdj[i]->lower;

			if(lpsdAlignAdj[i]->sector >= 0)
				lpsecAlignAdj[i] = &lpmap->sectors[lpsdAlignAdj[i]->sector];
		}
	}


	/* Now we handle the actual aligning. For each of the textures to align to
	 * on the source linedef, find those adjacent to it on the other linedef,
	 * and then offset them.
	 */

	for(i = 0; i < 2; i++)
	{
		BOOL bAligned = FALSE;
		BOOL bPreserveVertAlignment = FALSE;

		/* Two-sided sidedefs with their main texture set should have their
		 * vertical alignment preserved.
		 */
		if(lpsdAlignAdj[i] && IsBonaFideTextureA(lpsdAlignAdj[i]->middle))
			bPreserveVertAlignment = TRUE;

		/* We must actually have the sidedefs. */
		if(lpsdSource[i] && lpsdAlignAdj[i])
		{
			/* Repeat for each of upper, middle and lower. */
			for(iTexture = 0; iTexture < 3; iTexture++)
			{
				if(byTexFlags & byAllTexFlags[i][iTexture])
				{
					TEXTURE *lptex;
					BOOL bNeedFree;

					/* Get terminated texture name. */
					CHAR szTexName[TEXNAME_BUFFER_LENGTH] = {0};
					CopyMemory(szTexName, sSourceTextures[i][iTexture], TEXNAME_WAD_BUFFER_LENGTH);

					bNeedFree = GetTextureForMapA(hwndMap, szTexName, &lptex, TF_TEXTURE);

					if(lptex)
					{
						int iAlignTexture;

						/* Now repeat for each of upper, middle and lower on the
						 * alignment linedef.
						 */
						for(iAlignTexture = 0; iAlignTexture < 3; iAlignTexture++)
						{
							int iOffset;

							if(
								/* Texture names must match. */
								strncmp(sSourceTextures[i][iTexture], sAlignTextures[i][iAlignTexture], TEXNAME_WAD_BUFFER_LENGTH) == 0
								&&
								/* The textures must also be touching. */
								CalculateSurfaceTexturesAlignment(
									lpsecSource[i],
									lpsecSource[1 - i],
									lpsecAlignAdj[i],
									lpsecAlignAdj[1 - i],
									iTexture,
									iAlignTexture,
									lpldSource->flags,
									lpldAlign->flags,
									&iOffset)
								)
							{
								/* Textures match: fantastic. We can do our
								 * aligning now.
								 */
								sMatchTextures[i] = sSourceTextures[i][iTexture];

								/* The left-right orientation determines in
								 * which direction we align.
								 */
								if((iWhichVertex == 1) ^ (i == LS_BACK))
									lpsdAlignAdj[i]->tx = (lpsdSource[i]->tx + iSourceLength) % lptex->cx;
								else
									lpsdAlignAdj[i]->tx = (lpsdSource[i]->tx - iAlignLength) % lptex->cx;

								if(!bPreserveVertAlignment)
									lpsdAlignAdj[i]->ty = (lpsdSource[i]->ty + iOffset) % lptex->cy;

								/* Signal that we've aligned on this side. */
								bAligned = TRUE;
								break;
							}
						}

						if(bNeedFree) DestroyTexture(lptex);

						/* If we found where to align, skip straight to next
						 * side, or finish.
						 */
						if(bAligned) break;
					}
				}

			}	/* Upper, middle, lower loop of aligning sidedef. */
		}
	}

	/* Fill in the flags from the texture names. */
	for(i = 0; i < 2; i++)
	{
		/* Did we do any aligning on this side? */
		if(sMatchTextures[i])
		{
			for(iTexture = 0; iTexture < 3; iTexture++)
			{
				/* Do we match? */
				if(sAlignTextures[i][iTexture] &&
					strncmp(sMatchTextures[i], sAlignTextures[i][iTexture], TEXNAME_WAD_BUFFER_LENGTH) == 0)
					byAligned |= byAllTexFlags[i][iTexture];
			}
		}
	}

	/* Finished! Return flags indicating which textures we aligned. */
	return byAligned;
}


/* LinedefLength
 *   Calculates the length of a linedef.
 *
 * Parameters:
 *   MAP*	lpmap		Map data.
 *   int	iLinedef	Linedef index.
 *
 * Return value: int
 *   Linedef length.
 */
static __inline int LinedefLength(MAP *lpmap, int iLinedef)
{
	return (int)floor(0.5f + distancei(
		lpmap->vertices[lpmap->linedefs[iLinedef].v1].x,
		lpmap->vertices[lpmap->linedefs[iLinedef].v1].y,
		lpmap->vertices[lpmap->linedefs[iLinedef].v2].x,
		lpmap->vertices[lpmap->linedefs[iLinedef].v2].y));
}


/* CalculateSurfaceTexturesAlignment
 *   Determines whether textures on specified surfaces of two adjacent linedefs
 *   are touching, and if they are, determines the alignment offset between
 *   them.
 *
 * Parameters:
 *   MAPSECTOR*		lpsecSource			Sector into which the source sidedef
 *										faces.
 *   MAPSECTOR*		lpsecSourceReverse	Sector on the reverse of the source
 *										sidedef.
 *   MAPSECTOR*		lpsecAlign			Sector into which the sidedef to be
 *										aligned faces.
 *   MAPSECTOR*		lpsecAlignReverse	Sector on the reverse of the sidedef to
 *										be aligned.
 *   int			iTexSource			ENUM_SDTFLAGS specifying whether the
 *										upper, middle or lower texture is being
 *										aligned to on the source sidedef.
 *   int			iTexAlign			As iTexSource for the sidedef to be
 *										aligned.
 *   WORD			wFlagsSource		Linedef flags for the line to which the
 *										source sidedef belongs.
 *   WORD			wFlagsAlign			Linedef flags for the line to which the
 *										sidedef to be aligned belongs.
 *   int*			lpiVertOffset		Pointer to integer in which to return
 *										the alignment offset.
 *
 * Return value: BOOL
 *   TRUE if the specified textures touch; FALSE otherwise.
 *
 * Remarks:
 *   If the textures do not touch, *lpiVertOffset is not set.
 */
static BOOL CalculateSurfaceTexturesAlignment(MAPSECTOR *lpsecSource, MAPSECTOR *lpsecSourceReverse, MAPSECTOR *lpsecAlign, MAPSECTOR *lpsecAlignReverse, int iTexSource, int iTexAlign, WORD wFlagsSource, WORD wFlagsAlign, int *lpiVertOffset)
{
	int iAnchorPair[] = {0, 0};
	BOOL bSet = FALSE;
	MAPSECTOR *lpsecFrontPair[] = {lpsecSource, lpsecAlign};
	MAPSECTOR *lpsecReversePair[] = {lpsecSourceReverse, lpsecAlignReverse};
	WORD wFlagsPair[] = {wFlagsSource, wFlagsAlign};
	int iTexPair[] = {iTexSource, iTexAlign};
	int i;

	/* Sanity check. All lines have front sectors! */
	if(!lpsecSource || !lpsecAlign) return FALSE;

	/*
	 * Here's how pegging affects the anchor point for the textures:
	 *
	 *					UPPER			MIDDLE			LOWER
	 *		NORMAL		Reverse ceil	Reverse ceil	Reverse floor
	 *		UNPEGGED	Front ceil		Reverse floor	Front ceil
	 *
	 * Recall also that 'upper unpegged' affects the upper texture and 'lower
	 * unpegged' affects the middle and lower textures.
	 */

	/* Repeat for each of the linedefs. */
	for(i = 0; i < 2; i++)
	{
		switch(iTexPair[i])
		{
		case SDT_UPPER:
			/* Pegging determines which sector affects the texture position. */
			if(wFlagsPair[i] & LDF_UPPERUNPEGGED) iAnchorPair[i] = lpsecFrontPair[i]->hceiling;
			else
			{
				if(!lpsecReversePair[i]) return FALSE;
				iAnchorPair[i] = lpsecReversePair[i]->hceiling;
			}

			break;

		case SDT_MIDDLE:
			/* Pegging determines whether the texture's anchored to the ceiling or
			 * the floor.
			 */
			if(!lpsecReversePair[i]) return FALSE;
			if(wFlagsPair[i] & LDF_LOWERUNPEGGED) iAnchorPair[i] = lpsecReversePair[i]->hfloor;
			else iAnchorPair[i] = lpsecReversePair[i]->hceiling;
			break;

		case SDT_LOWER:
			/* Pegging determines which sector affects the texture position, as well
			 * as whether it's the ceiling or the floor.
			 */
			if(wFlagsPair[i] & LDF_LOWERUNPEGGED) iAnchorPair[i] = lpsecFrontPair[i]->hceiling;
			else
			{
				if(!lpsecReversePair[i]) return FALSE;
				iAnchorPair[i] = lpsecReversePair[i]->hfloor;
			}
		}
	}


	/* Adjacent upper and lower textures always touch. */
	if(iTexSource == iTexAlign && (iTexSource == SDT_UPPER || iTexSource == SDT_LOWER))
		bSet = TRUE;

	/* For the other sorts, we check the positions to see whether they touch. */

	else if(iTexSource == SDT_UPPER)
	{
		if(iTexAlign == SDT_MIDDLE && lpsecAlignReverse->hceiling >= lpsecSourceReverse->hceiling)
			bSet = TRUE;

		if(iTexAlign == SDT_LOWER && lpsecAlignReverse->hfloor >= lpsecSourceReverse->hceiling)
			bSet = TRUE;
	}
	else if(iTexSource == SDT_MIDDLE)
	{
		if(iTexAlign == SDT_UPPER && lpsecAlignReverse->hceiling <= lpsecSourceReverse->hceiling)
			bSet = TRUE;

		if(iTexAlign == SDT_MIDDLE && lpsecAlignReverse->hceiling >= lpsecSourceReverse->hfloor && lpsecAlignReverse->hfloor <= lpsecSourceReverse->hceiling)
			bSet = TRUE;

		if(iTexAlign == SDT_LOWER && lpsecAlignReverse->hfloor >= lpsecSourceReverse->hfloor)
			bSet = TRUE;
	}
	else if(iTexSource == SDT_LOWER)
	{
		if(iTexAlign == SDT_UPPER && lpsecAlignReverse->hceiling <= lpsecSourceReverse->hfloor)
			bSet = TRUE;

		if(iTexAlign == SDT_MIDDLE && lpsecAlignReverse->hfloor <= lpsecSourceReverse->hfloor)
			bSet = TRUE;
	}

	if(bSet)
	{
		*lpiVertOffset = iAnchorPair[0] - iAnchorPair[1];
		return TRUE;
	}

	/* Not touching. */
	return FALSE;
}


/* VisitedFlagsToTexFlags, TexFlagsToVisitedFlags
 *   Converts LEF_VISITED_* flags to LDTF_* flags, and vice versa.
 *
 * Parameters:
 *   DWORD	wLineEditFlags	Linedef editing flags.
 *    or
 *   BYTE	byTexFlags		Texture flags.
 *
 * Return value: BYTE or DWORD
 *   The corresponding flags in the other form.
 */
static __inline BYTE VisitedFlagsToTexFlags(DWORD dwLineEditFlags)
{
	return (BYTE)(((dwLineEditFlags & LEF_VISITED) >> 8) & LDTF_ALL);
}

static __inline DWORD TexFlagsToVisitedFlags(BYTE byTexFlags)
{
	return ((byTexFlags & LDTF_ALL) << 8) & LEF_VISITED;
}


/* FixMissingTextures
 *   Sets missing textures to specified textures.
 *
 * Parameters:
 *   MAP*		lpmap							Pointer to map data.
 *   LPCTSTR	szUpper, szMiddle, szLower		Names of texture to fill in the
 *												missing spots.
 *   LPCTSTR	szSky							Sky flat, or empty string for
 *												none.
 *   BOOL		bInSelection					Whether to work within the
 *												selection only.
 *
 * Return value: None.
 */
void FixMissingTextures(MAP *lpmap, LPCTSTR szUpper, LPCTSTR szMiddle, LPCTSTR szLower, LPCTSTR szSky, BOOL bInSelection)
{
	int i;

	/* Repeat for each linedef. */
	for(i = 0; i < lpmap->iLinedefs; i++)
	{
		/* If we want the line to be selected, make sure it is. */
		if(!bInSelection || lpmap->linedefs[i].selected)
			FixMissingTexturesLinedef(lpmap, i, szUpper, szMiddle, szLower, szSky);
	}
}


/* FixMissingTexturesLinedef
 *   Sets missing textures on one linedef to specified textures.
 *
 * Parameters:
 *   MAP*		lpmap							Pointer to map data.
 *   int		iLinedef						Index of linedef.
 *   LPCTSTR	szUpper, szMiddle, szLower		Names of texture to fill in the
 *												missing spots.
 *   LPCTSTR	szSky							Sky flat, or empty string for
 *												none.
 *
 * Return value: None.
 */
void FixMissingTexturesLinedef(MAP *lpmap, int iLinedef, LPCTSTR szUpper, LPCTSTR szMiddle, LPCTSTR szLower, LPCTSTR szSky)
{
	MAPLINEDEF *lpld = &lpmap->linedefs[iLinedef];

	/* Find which textures this line should have. */
	BYTE byReqFlags = RequiredTextures(lpmap, iLinedef, szSky);

	/* Fill in missing textures on the front sidedef, if we have one. */
	if(SidedefExists(lpmap, lpld->s1))
	{
		MAPSIDEDEF *lpsd = &lpmap->sidedefs[lpld->s1];

		if((byReqFlags & LDTF_FRONTUPPER) && !IsBonaFideTextureA(lpsd->upper))
			FillTexNameBuffer(lpsd->upper, szUpper);

		if((byReqFlags & LDTF_FRONTMIDDLE) && !IsBonaFideTextureA(lpsd->middle))
			FillTexNameBuffer(lpsd->middle, szMiddle);

		if((byReqFlags & LDTF_FRONTLOWER) && !IsBonaFideTextureA(lpsd->lower))
			FillTexNameBuffer(lpsd->lower, szLower);
	}

	/* Do the same for the back sidedef. */
	if(SidedefExists(lpmap, lpld->s2))
	{
		MAPSIDEDEF *lpsd = &lpmap->sidedefs[lpld->s2];

		if((byReqFlags & LDTF_BACKUPPER) && !IsBonaFideTextureA(lpsd->upper))
			FillTexNameBuffer(lpsd->upper, szUpper);

		if((byReqFlags & LDTF_BACKMIDDLE) && !IsBonaFideTextureA(lpsd->middle))
			FillTexNameBuffer(lpsd->middle, szMiddle);

		if((byReqFlags & LDTF_BACKLOWER) && !IsBonaFideTextureA(lpsd->lower))
			FillTexNameBuffer(lpsd->lower, szLower);
	}
}

/* FillTexNameBuffer
 *   Sets a texture name in a sidedef-format buffer, suitably padded.
 *
 * Parameters:
 *   LPSTR		sTexBuf		Texture buffer to copy name into.
 *   LPCTSTR	szTexName	NUL-terminated texture name.
 *
 * Return value: None.
 */
#ifdef _UNICODE
void FillTexNameBufferW(LPSTR sTexBuf, LPCWSTR szTexName)
{
	/* Fill with zeroes first. */
	ZeroMemory(sTexBuf, TEXNAME_WAD_BUFFER_LENGTH);

	/* Handily, this doesn't terminate it if it fills the buffer. Neat! */
	WideCharToMultiByte(CP_ACP, 0, szTexName, -1, sTexBuf, TEXNAME_WAD_BUFFER_LENGTH, NULL, NULL);
}
#endif

void FillTexNameBufferA(LPSTR sTexBuf, LPCSTR szTexName)
{
	LPCSTR szInTexName = szTexName;

	/* Fill with zeroes first. */
	ZeroMemory(sTexBuf, TEXNAME_WAD_BUFFER_LENGTH);

	while(szInTexName - szTexName < TEXNAME_WAD_BUFFER_LENGTH && (*sTexBuf++ = *szInTexName++));
}



/* SectorsEqual
 *   Determines whether two sectors have identical properties.
 *
 * Parameters:
 *   MAP*	lpmap		Pointer to map data.
 *   int	iSector1	Index of one sector.
 *   int	iSector2	Index of the other sector.
 *
 * Return value: BOOL
 *   TRUE if the sectors are equal; FALSE otherwise.
 */
static __inline BOOL SectorsEqual(MAP *lpmap, int iSector1, int iSector2)
{
	return !memcmp(&lpmap->sectors[iSector1], &lpmap->sectors[iSector2], SECTORRECORDSIZE);
}


/* FindIdenticalSectorSet
 *   Attempts to find a set of identical sectors in a map.
 *
 * Parameters:
 *   MAP*				lpmap			Pointer to map data.
 *   int				iStartSector	Index of sector from which to begin
 *										looking.
 *   BOOL				bInSelection	Whether to merge within the selection
 *										only.
 *   DYNAMICINTARRAY*	lpdiarray		Array into which to return set
 *
 * Return value: int
 *   Index of lowest-numbered sector in the set, or negative if no set is found.
 *
 * Remarks:
 *   The array is NOT cleared if no set is found. All possible identical sets
 *   are considered: the start sector specifies only where to begin looking
 *   from, not that sectors below it should be excluded.
 */
int FindIdenticalSectorSet(MAP *lpmap, int iStartSector, BOOL bInSelection, DYNAMICINTARRAY *lpdiarray)
{
	int i;

	/* Loop through each possible start sector, starting with the specified
	 * initial start sector.
	 */
	for(i = 0; i < lpmap->iSectors; i++)
	{
		if(!bInSelection || lpmap->sectors[i].selected)
		{
			int iSector = (i + iStartSector) % lpmap->iSectors;
			if(FindHighOnlyIDSectorSet(lpmap, iSector, bInSelection, lpdiarray))
				return iSector;
		}
	}

	return -1;
}


/* FindHighOnlyIDSectorSet
 *   Attempts to find a set of identical sectors in a map containing a specified
 *   sector such that all sectors in the set have an index at least as high as
 *   that sector and that there are no sectors identical to it with an index of
 *   less than that sector.
 *
 * Parameters:
 *   MAP*				lpmap			Pointer to map data.
 *   int				iSector			Index of sector.
 *   BOOL				bInSelection	Whether to merge within the selection
 *										only.
 *   DYNAMICINTARRAY*	lpdiarray		Array into which to return set.
 *
 * Return value: BOOL
 *   TRUE if such a set is found; FALSE otherwise.
 *
 * Remarks:
 *   The selection list is NOT cleared if no set is found.
 */
static BOOL FindHighOnlyIDSectorSet(MAP *lpmap, int iSector, BOOL bInSelection, DYNAMICINTARRAY *lpdiarray)
{
	int i;
	BOOL bFound = FALSE;

	/* First check there are no matches below. */
	for(i = 0; i < iSector; i++)
		if((!bInSelection || lpmap->sectors[i].selected) &&
			SectorsEqual(lpmap, iSector, i))
			return FALSE;

	/* Look for matches above. */
	for(i++; i < lpmap->iSectors; i++)
		if((!bInSelection || lpmap->sectors[i].selected) &&
			SectorsEqual(lpmap, iSector, i))
		{
			bFound = TRUE;
			AddToDynamicIntArray(lpdiarray, i);
		}

	/* If we have any match, we also need to add the initial sector. */
	if(bFound)
		AddToDynamicIntArray(lpdiarray, iSector);

	return bFound;
}


/* MergeIdenticalSectors
 *   Merges all sets of identical sectors, optionally within the selection only.
 *
 * Parameters:
 *   MAP*	lpmap			Pointer to map data.
 *   BOOL	bInSelection	Whether to merge within the selection only.
 *
 * Return value: int
 *   Number of sets of sectors merged.
 */
int MergeIdenticalSectors(MAP *lpmap, BOOL bInSelection)
{
	SELECTION_LIST *lpsellist = AllocateSelectionList(MERGEALL_INITLISTLENGTH);
	DYNAMICINTARRAY diarray;

	/* Number of merged sets. */
	int iMerged = 0;

	InitialiseDynamicIntArray(&diarray, MERGEALL_INITLISTLENGTH);

	/* Repeat while we still have identical sets. */
	while(ClearDynamicIntArray(&diarray),
		FindIdenticalSectorSet(lpmap, 0, bInSelection, &diarray) >= 0)
	{
		int i;

		/* Copy from array to selection list. */
		ClearSelectionList(lpsellist);
		for(i = 0; (unsigned int)i < diarray.uiCount; i++)
			AddToSelectionList(lpsellist, diarray.lpiIndices[i]);

		/* Merge! */
		JoinSelectedSectors(lpmap, lpsellist, TRUE);
		iMerged++;
	}

	/* Clean up. */
	DestroySelectionList(lpsellist);
	FreeDynamicIntArray(&diarray);

	return iMerged;
}


/* SideOfLinedef
 * SideOfLinedefInt2Coords
 *   Determines on which side of a linedef a point falls.
 *
 * Parameters:
 *   MAP*			lpmap		Pointer to map data.
 *   int			iLinedef	Index of linedef.
 *   POINT/FPOINT*	lppt/lpfp	Point of interest, in 2Coords for
 *								SideOfLinedefInt2Coords.
 *
 * Return value: int
 *   Side of linedef.
 */
static __inline int SideOfLinedef(MAP *lpmap, int iLinedef, FPOINT *lpfp)
{
	MAPVERTEX *lpvx1 = &lpmap->vertices[lpmap->linedefs[iLinedef].v1];
	MAPVERTEX *lpvx2 = &lpmap->vertices[lpmap->linedefs[iLinedef].v2];

	if(side_of_lineff((float)lpvx1->x, (float)lpvx1->y, (float)lpvx2->x, (float)lpvx2->y, lpfp->x, lpfp->y) < 0)
		return LS_FRONT;
	return LS_BACK;
}

static __inline int SideOfLinedefInt2Coords(MAP *lpmap, int iLinedef, POINT *lppt)
{
	MAPVERTEX *lpvx1 = &lpmap->vertices[lpmap->linedefs[iLinedef].v1];
	MAPVERTEX *lpvx2 = &lpmap->vertices[lpmap->linedefs[iLinedef].v2];

	if(side_of_lineii(2 * lpvx1->x, 2 * lpvx1->y, 2 * lpvx2->x, 2 * lpvx2->y, lppt->x, lppt->y) < 0)
		return LS_FRONT;
	return LS_BACK;
}

/* ApplySectorDefaults
 *   Sets a sector's properties to the defaults.
 *
 * Parameters:
 *   MAP*		lpmap			Pointer to map data.
 *   CONFIG*	lpcfgWadOptMap	Map subsection of wad options.
 *   int		iSector			Index of sector.
 *
 * Return value: None.
 */
static void ApplySectorDefaults(MAP *lpmap, CONFIG *lpcfgWadOptMap, int iSector)
{
	MAPSECTOR *lpsec = &lpmap->sectors[iSector];
	TCHAR szTexName[TEXNAME_BUFFER_LENGTH];

	CONFIG *lpcfgMapDefSec = ConfigGetSubsection(lpcfgWadOptMap, WADOPT_DEFAULTSEC);

	/* Zero everything that we don't set. */
	ZeroMemory(lpsec, sizeof(MAPSECTOR));

	ConfigGetString(lpcfgMapDefSec, TEXT("tceiling"), szTexName, TEXNAME_BUFFER_LENGTH);
	FillTexNameBuffer(lpsec->tceiling, szTexName);

	ConfigGetString(lpcfgMapDefSec, TEXT("tfloor"), szTexName, TEXNAME_BUFFER_LENGTH);
	FillTexNameBuffer(lpsec->tfloor, szTexName);

	lpsec->hceiling = ConfigGetInteger(lpcfgMapDefSec, TEXT("hceiling"));
	lpsec->hfloor = ConfigGetInteger(lpcfgMapDefSec, TEXT("hfloor"));
	lpsec->brightness = ConfigGetInteger(lpcfgMapDefSec, TEXT("brightness"));
}


/* CreateUsedTexturesConfig, CreateUsedFlatsConfig
 *   Creates a config structure of used texture/flat names.
 *
 * Parameters:
 *   MAP*		lpmap	Pointer to map data.
 *   CONFIG*	lpcfg	Config to fill.
 *
 * Return value: CONFIG*
 *   Config containing used names, or NULL if none used.
 *
 * Remarks:
 *   The caller must destroy the returned config if it is non-NULL.
 */
CONFIG* CreateUsedTexturesConfig(MAP *lpmap)
{
	int i;
	CONFIG *lpcfg = ConfigCreate();

	for(i = 0; i < lpmap->iSidedefs; i++)
	{
		MAPSIDEDEF *lpsd = &lpmap->sidedefs[i];
		const char* szTexNames[] = {lpsd->upper, lpsd->middle, lpsd->lower};
		int j;

		for(j = 0; j < (int)NUM_ELEMENTS(szTexNames); j++)
		{
			TCHAR szTexNameTerminated[TEXNAME_BUFFER_LENGTH];

#ifdef UNICODE
			MultiByteToWideChar(CP_ACP, MB_PRECOMPOSED, szTexNames[j], TEXNAME_WAD_BUFFER_LENGTH, szTexNameTerminated, NUM_ELEMENTS(szTexNameTerminated));
#else
			CopyMemory(szTexNameTerminated, szTexNames[j], TEXNAME_WAD_BUFFER_LENGTH);
#endif
			szTexNameTerminated[NUM_ELEMENTS(szTexNameTerminated) - 1] = TEXT('\0');

			if(IsBonaFideTexture(szTexNameTerminated))
				ConfigSetAtom(lpcfg, szTexNameTerminated);
		}
	}

	if(ConfigIsEmpty(lpcfg))
	{
		ConfigDestroy(lpcfg);
		return NULL;
	}

	return lpcfg;
}

CONFIG* CreateUsedFlatsConfig(MAP *lpmap)
{
	int i;
	CONFIG *lpcfg = ConfigCreate();

	for(i = 0; i < lpmap->iSectors; i++)
	{
		MAPSECTOR *lpsec = &lpmap->sectors[i];
		const char* szTexNames[] = {lpsec->tceiling, lpsec->tfloor};
		int j;

		for(j = 0; j < (int)NUM_ELEMENTS(szTexNames); j++)
		{
			TCHAR szTexNameTerminated[TEXNAME_BUFFER_LENGTH];

#ifdef UNICODE
			MultiByteToWideChar(CP_ACP, MB_PRECOMPOSED, szTexNames[j], TEXNAME_WAD_BUFFER_LENGTH, szTexNameTerminated, NUM_ELEMENTS(szTexNameTerminated));
#else
			CopyMemory(szTexNameTerminated, szTexNames[j], TEXNAME_WAD_BUFFER_LENGTH);
#endif
			szTexNameTerminated[NUM_ELEMENTS(szTexNameTerminated) - 1] = TEXT('\0');

			ConfigSetAtom(lpcfg, szTexNameTerminated);
		}
	}

	if(ConfigIsEmpty(lpcfg))
	{
		ConfigDestroy(lpcfg);
		return NULL;
	}

	return lpcfg;
}


/* PointOnLineVertex
 *   Determines whether a point lies on a vertex of a given line.
 *
 * Parameters:
 *   MAP*		lpmap		Pointer to map data.
 *   int		iLinedef	Index of linedef.
 *   FPOINT*	lpfp		Point.
 *
 * Return value: BOOL
 *   TRUE if point is on a vertex; FALSE otherwise.
 */
static __inline BOOL PointOnLineVertex(MAP *lpmap, int iLinedef, FPOINT *lpfp)
{
	MAPVERTEX *lpvx1 = &lpmap->vertices[lpmap->linedefs[iLinedef].v1];
	MAPVERTEX *lpvx2 = &lpmap->vertices[lpmap->linedefs[iLinedef].v2];

	return (fabs(lpfp->x - lpvx1->x) < 0.005f && fabs(lpfp->y - lpvx1->y) < 0.005f) ||
		(fabs(lpfp->x - lpvx2->x) < 0.005f && fabs(lpfp->y - lpvx2->y) < 0.005f);
}


#ifdef _DEBUG
BOOL VerifyEditFlagIntegrity(MAP *lpmap)
{
	int i;

	for(i = 0; i < lpmap->iLinedefs; i++)
		if(lpmap->linedefs[i].editflags) return FALSE;

	for(i = 0; i < lpmap->iVertices; i++)
		if(lpmap->vertices[i].editflags) return FALSE;

	for(i = 0; i < lpmap->iSectors; i++)
		if(lpmap->sectors[i].editflags) return FALSE;

	return TRUE;
}
#endif


/* MakeLineSingleSided
 *   Removes the back sidedef from a line, if it exists.
 *
 * Parameters:
 *   MAP*		lpmap		Pointer to map data.
 *   int		iLinedef	Index of linedef.
 *
 * Return value: None.
 *
 * Remarks:
 *   If the back sidedef does not exist (even if it's not INVALID_SIDEDEF),
 *   the linedef is left untouched.
 */
static void MakeLineSingleSided(MAP *lpmap, int iLinedef)
{
	int iSidedef = lpmap->linedefs[iLinedef].s2;

	/* Sanity check. */
	if(SidedefExists(lpmap, iSidedef))
	{
		/* Detach the sidedef from the line. */
		SetLinedefSidedef(lpmap, iLinedef, INVALID_SIDEDEF, LS_BACK);

		/* Delete the sidedef. */
		DeleteSidedef(lpmap, iSidedef);
	}
}


/* MakeSelectedLinesSingleSided
 *   Makes all linedefs in a selection single-sided.
 *
 * Parameters:
 *   MAP*				lpmap		Pointer to map structure.
 *   SELECTION_LIST*	lpsellist	Selection of linedefs.
 *
 * Return value: None.
 */
void MakeSelectedLinesSingleSided(MAP *lpmap, SELECTION_LIST* lpsellist)
{
	int i;
	for(i = 0; i < lpsellist->iDataCount; i++)
		MakeLineSingleSided(lpmap, lpsellist->lpiIndices[i]);
}


/* MakeLineDoubleSided
 *   Adds a new sidedef on the reverse of a single-sided line and adjusts sector
 *   references accordingly.
 *
 * Parameters:
 *   MAP*		lpmap			Pointer to map data.
 *   int		iLinedef		Index of linedef.
 *   CONFIG*	lpcfgWadOptMap	Map options, for sector defaults.
 *
 * Return value: None.
 *
 * Remarks:
 *   If a valid back sidedef already exists, nothing happens. Otherwise, a new
 *   sidedef is created and is set to point into either an appropriate sector or
 *   into a new one if none exists.
 */
static void MakeLineDoubleSided(MAP *lpmap, int iLinedef, CONFIG *lpcfgWadOptMap)
{
	MAPLINEDEF *lpld = &lpmap->linedefs[iLinedef];

	/* Sanity check. */
	if(!SidedefExists(lpmap, lpld->s2))
	{
		DYNAMICINTARRAY *lpldlookup;
		int iSector = FindLineSideSector(lpmap, iLinedef, LS_BACK);

		/* If we didn't find a sector, create one. */
		if(iSector < 0)
		{
			iSector = AddSector(lpmap);
			ApplySectorDefaults(lpmap, lpcfgWadOptMap, iSector);
		}

		/* Create a new sidedef and attach it to the line. The sector reference
		 * will be set by PropagateNewSector.
		 */
		SetLinedefSidedef(lpmap, iLinedef, AddSidedef(lpmap, -1), LS_BACK);

		/* Propagate the sector to our neighbours. */
		lpldlookup = BuildLDLookupTable(lpmap, LDLT_VXTOLD);
		PropagateNewSector(lpmap, lpldlookup, iLinedef, LS_BACK, iSector, NULL);
		DestroyLDLookupTable(lpmap, lpldlookup);
	}
}


/* MakeSelectedLinesDoubleSided
 *   Makes all linedefs in a selection single-sided.
 *
 * Parameters:
 *   MAP*				lpmap			Pointer to map structure.
 *   SELECTION_LIST*	lpsellist		Selection of linedefs.
 *   CONFIG*			lpcfgWadOptMap	Map options, for sector defaults.
 *
 * Return value: None.
 */
void MakeSelectedLinesDoubleSided(MAP *lpmap, SELECTION_LIST* lpsellist, CONFIG *lpcfgWadOptMap)
{
	int i;
	for(i = 0; i < lpsellist->iDataCount; i++)
		MakeLineDoubleSided(lpmap, lpsellist->lpiIndices[i], lpcfgWadOptMap);
}


/* FindLineSideSector
 *   Uses the half-line method to determine which sector should be referenced by
 *   a given side of a line.
 *
 * Parameters:
 *   MAP*	lpmap			Pointer to map structure.
 *   int	iLinedef		Index of linedef.
 *   int	iLinedefSide	The side of interest.
 *
 * Return value: int
 *   Index of sector, or negative if none found.
 *
 * Remarks:
 *   The source line is not excluded from the reference determination algorithm.
 *   LEF_LABELLED is cleared by this function. The RS flags are handled
 *   correctly.
 */
int FindLineSideSector(MAP *lpmap, int iLinedef, int iLinedefSide)
{
	POINT ptOrigin, ptOther;
	int iSector;
	MAPOBJGRID *lpmogLinedefs = CreateLinedefMOG(lpmap);

	/* Get point on middle of line. */
	GetOrthHLPointsIn2Coords(lpmap, iLinedef, iLinedefSide, &ptOrigin, &ptOther);
	iSector = GetSectorByHalfLineMethod(lpmap, lpmogLinedefs, &ptOrigin, &ptOther);

	ClearLinedefsLabelFlag(lpmap);

	if(lpmogLinedefs)
		DestroyMapObjGrid(lpmogLinedefs);

	return iSector;
}


/* RemoveSectorInterior
 *   Removes the interior of a sector, flipping and deleting lines as necessary,
 *   and then deleting the sector.
 *
 * Parameters:
 *   MAP*	lpmap		Pointer to map structure.
 *   int	iSector		Sector whose interior is to be removed.
 *
 * Return value: None.
 */
static void RemoveSectorInterior(MAP *lpmap, int iSector)
{
	int iLinedef;

	/* We might delete linedefs, so loop through backwards. */
	for(iLinedef = lpmap->iLinedefs - 1; iLinedef >= 0; iLinedef--)
	{
		MAPLINEDEF *lpld = &lpmap->linedefs[iLinedef];

		/* Check whether we belong to the sector on each side. */
		BOOL bFront = SidedefExists(lpmap, lpld->s1) && lpmap->sidedefs[lpld->s1].sector == iSector;
		BOOL bBack = SidedefExists(lpmap, lpld->s2) && lpmap->sidedefs[lpld->s2].sector == iSector;

		if(bFront && (bBack || !SidedefExists(lpmap, lpld->s2)))
		{
			/* If we belong to the sector on both sides, or on our only side,
			 * this line is now redundant. Delete it.
			 */
			DeleteLinedef(lpmap, iLinedef);
		}
		else if(bFront)
		{
			/* If we belong on the front only, flip over so that we belong on
			 * the back only, and then remove the back.
			 */
			FlipLinedef(lpmap, iLinedef);
			MakeLineSingleSided(lpmap, iLinedef);
		}
		else if(bBack)
		{
			/* If we belong on the back only, just remove the back. */
			MakeLineSingleSided(lpmap, iLinedef);
		}
	}

	DeleteSector(lpmap, iSector);
}


/* RemoveSelectedSectorInteriors
 *   Removes the interiors of and deletes selected sectors.
 *
 * Parameters:
 *   MAP*				lpmap			Pointer to map structure.
 *   SELECTION_LIST*	lpsellist		Selection of sectors.
 *
 * Return value: None.
 *
 * Remarks:
 *   The indices in lpsellist will be reordered, but this shouldn't be a problem
 *   since they're meaningless by the time the function returns anyway.
 */
void RemoveSelectedSectorInteriors(MAP *lpmap, SELECTION_LIST* lpsellist)
{
	int i;
	DYNAMICINTARRAY *lpdiarrayLDLookupBefore;

	/* Remember the vertex state. */
	InitOrphanVertexCheck(lpmap, &lpdiarrayLDLookupBefore);

	/* Sort the sector indices. */
	qsort(lpsellist->lpiIndices, lpsellist->iDataCount, sizeof(int), QsortIntegerComparison);

	/* Loop backwards through the sector indices, so as not to disrupt things
	 * when we delete them.
	 */
	for(i = lpsellist->iDataCount - 1; i >= 0; i--)
		RemoveSectorInterior(lpmap, lpsellist->lpiIndices[i]);

	/* Delete any orphaned vertices. */
	DeleteOrphanedVertices(lpmap, lpdiarrayLDLookupBefore);
}


/* InitOrphanVertexCheck
 *   Saves the state of vertices in order that orphaned vertices can be moved at
 *   at a later point.
 *
 * Parameters:
 *   MAP*				lpmap						Pointer to map structure.
 *   DYNAMICINTARRAY**	lplpdiarrayLDLookupBefore	State is allocated and
 *													stored here.
 *
 * Return value: None.
 *
 * Remarks:
 *   Must be followed by a call to DeleteOrphanedVertices at some point. Don't
 *   modify the vertices before doing so.
 */
static __inline void InitOrphanVertexCheck(MAP *lpmap, DYNAMICINTARRAY **lplpdiarrayLDLookupBefore)
{
	*lplpdiarrayLDLookupBefore = BuildLDLookupTable(lpmap, LDLT_VXTOLD);
}


/* DeleteOrphanedVertices
 *   Deletes vertices orphaned since a previous arbitrary state.
 *
 * Parameters:
 *   MAP*				lpmap						Pointer to map structure.
 *   DYNAMICINTARRAY**	lplpdiarrayLDLookupBefore	Previous vertex state.
 *
 * Return value: None.
 *
 * Remarks:
 *   Must be preceded by a call to InitOrphanVertexCheck at some point, after
 *   which the vertices must not be tampered with until this function is called.
 *   Memory allocated by InitOrphanVertexCheck is freed here.
 */
static void DeleteOrphanedVertices(MAP *lpmap, DYNAMICINTARRAY *lpdiarrayLDLookupBefore)
{
	DYNAMICINTARRAY *lpdiarrayLDLookupAfter;
	DYNAMICINTARRAY diarrayVerticesToDelete;
	int i;

	/* Build a new vx-to-ld table to find which vertices have become orphaned.
	 */
	lpdiarrayLDLookupAfter = BuildLDLookupTable(lpmap, LDLT_VXTOLD);

	/* Delete orphaned vertices. We're not allowed to modify the vertices
	 * between creating and destroying the tables, so make a note of their
	 * indices and delete them later.
	 */
	InitialiseDynamicIntArray(&diarrayVerticesToDelete, DELVERTICES_INITBUFSIZE);
	for(i = 0; i < lpmap->iVertices; i++)
		if(lpdiarrayLDLookupAfter[i].uiCount == 0 && lpdiarrayLDLookupBefore[i].uiCount != 0)
			AddToDynamicIntArray(&diarrayVerticesToDelete, i);

	/* Destroy the tables to allow us to delete the vertices. */
	DestroyLDLookupTable(lpmap, lpdiarrayLDLookupBefore);
	DestroyLDLookupTable(lpmap, lpdiarrayLDLookupAfter);

	/* Delete the vertices. */
	for(i = (int)(diarrayVerticesToDelete.uiCount - 1); i >= 0; i--)
		DeleteVertex(lpmap, diarrayVerticesToDelete.lpiIndices[i]);

	/* Free the list of vertices. */
	FreeDynamicIntArray(&diarrayVerticesToDelete);
}


/* ResolveSector
 *   Resolves a sector into connected components.
 *
 * Parameters:
 *   MAP*	lpmap		Pointer to map structure.
 *   int	iSector		Sector to resolve.
 *
 * Return value: None.
 *
 * Remarks:
 *   We call FindLoopFromLine, so make sure LEF_LABELLED is clear. We guarantee
 *   that new sectors created won't interfere with the indices of existing ones.
 */
static void ResolveSector(MAP *lpmap, int iSector)
{
	LOOPLIST *lplooplistInwards = NULL, *lplooplistOutwards = NULL;
	LOOPLIST **lplplooplistInwardsNext = &lplooplistInwards, **lplplooplistOutwardsNext = &lplooplistOutwards;
	DYNAMICINTARRAY *lpldlookup;
	int i;

	/* Build vertex-to-ld lookup table. */
	lpldlookup = BuildLDLookupTable(lpmap, LDLT_VXTOLD);

	for(i = 0; i < lpmap->iLinedefs; i++)
	{
		SIDEFLAGMARKER sfm[] = {{lpmap->linedefs[i].s1, LEF_LOOPFRONT, LS_FRONT}, {lpmap->linedefs[i].s2, LEF_LOOPBACK, LS_BACK}};
		int j;

		/* Repeat for each side belonging to us. */
		for(j = 0; j < (int)NUM_ELEMENTS(sfm); j++)
		{
			if(SidedefExists(lpmap, sfm[j].iSidedef) &&
				lpmap->sidedefs[sfm[j].iSidedef].sector == iSector &&
				!(lpmap->linedefs[i].editflags & sfm[j].dwFlag))
			{
				LOOPLIST looplistNew;

				looplistNew.iSector = iSector;
				InitialiseDynamicIntArray(&looplistNew.diarrayLinedefs, LINELOOPARRAY_INITBUFSIZE);
				InitialiseDynamicIntArray(&looplistNew.diarraySameSide, LINELOOPARRAY_INITBUFSIZE);
				looplistNew.lplooplistNext = NULL;

				if(FindLoopFromLine(lpmap, i, sfm[j].iSide, i, sfm[j].iSide, iSector, &looplistNew.diarrayLinedefs, &looplistNew.diarraySameSide, lpldlookup, NULL))
				{
					/* We found a loop from this line. Add it to the appropriate
					 * list and label all its lines.
					 */
					float x, y;
					LOOPLIST ***lplplplooplistNext, *lplooplistNewCopy;
					unsigned int k;

					GetLineSideSpot(lpmap, i, sfm[j].iSide, 0.1f, &x, &y);

					if(PointInLinedefs(lpmap, x, y, looplistNew.diarrayLinedefs.lpiIndices, looplistNew.diarrayLinedefs.uiCount))
						lplplplooplistNext = &lplplooplistInwardsNext;
					else
						lplplplooplistNext = &lplplooplistOutwardsNext;

					**lplplplooplistNext = lplooplistNewCopy = ProcHeapAlloc(sizeof(LOOPLIST));
					*lplooplistNewCopy = looplistNew;
					lplooplistNewCopy->lplooplistNext = NULL;
					*lplplplooplistNext = &lplooplistNewCopy->lplooplistNext;

					lplooplistNewCopy->iFirstLineSide = sfm[j].iSide;

					/* Label the lines in the loop. */
					for(k = 0; k < looplistNew.diarrayLinedefs.uiCount; k++)
					{
						DWORD dwFlag;

						if(looplistNew.diarraySameSide.lpiIndices[k])
							dwFlag = sfm[j].dwFlag;
						else
							dwFlag = sfm[1-j].dwFlag;

						lpmap->linedefs[looplistNew.diarrayLinedefs.lpiIndices[k]].editflags |= dwFlag;
					}

					/* Don't free the arrays in looplistNew. */
				}
				else
				{
					/* Not in a loop, so pretend it's an outward-facing loop in
					 * its own right.
					 */
					LOOPLIST *lplooplistSingleton = ProcHeapAlloc(sizeof(LOOPLIST));
					lplooplistSingleton->iSector = iSector;
					lplooplistSingleton->iFirstLineSide = sfm[j].iSide;
					lplooplistSingleton->lplooplistNext = NULL;
					InitialiseDynamicIntArray(&lplooplistSingleton->diarrayLinedefs, 1);
					InitialiseDynamicIntArray(&lplooplistSingleton->diarraySameSide, 1);
					AddToDynamicIntArray(&lplooplistSingleton->diarrayLinedefs, i);
					AddToDynamicIntArray(&lplooplistSingleton->diarraySameSide, TRUE);

					*lplplooplistOutwardsNext = lplooplistSingleton;
					lplplooplistOutwardsNext = &(*lplplooplistOutwardsNext)->lplooplistNext;

					lpmap->linedefs[i].editflags |= sfm[j].dwFlag;

					/* FindLoopFromLine found no loop, so free the arrays we
					 * created.
					 */
					FreeDynamicIntArray(&looplistNew.diarrayLinedefs);
					FreeDynamicIntArray(&looplistNew.diarraySameSide);
				}
			}
		}
	}

	/* Trivial case: no inward loops means we're finished. */
	if(lplooplistInwards)
	{
		/* Each inward loop becomes a sector in its own right. Leave the first
		 * with the index of the original sector.
		 */
		LOOPLIST *lplooplistRover = lplooplistInwards->lplooplistNext;
		int iStraySector = -1;

		while(lplooplistRover)
		{
			BOOL bBack = (lplooplistRover->iFirstLineSide == LS_BACK);

			/* Create new sector with same properties as old. */
			lplooplistRover->iSector = AddSector(lpmap);
			lpmap->sectors[lplooplistRover->iSector] = lpmap->sectors[iSector];

			/* Make lines refer to new sector. */
			for(i = 0; i < (int)lplooplistRover->diarrayLinedefs.uiCount; i++)
			{
				MAPLINEDEF *lpld = &lpmap->linedefs[lplooplistRover->diarrayLinedefs.lpiIndices[i]];
				lpmap->sidedefs[(!!lplooplistRover->diarraySameSide.lpiIndices[i] ^ bBack) ? lpld->s1 : lpld->s2].sector = lplooplistRover->iSector;
			}

			lplooplistRover = lplooplistRover->lplooplistNext;
		}

		/* For each outward loop, find the tightest enclosing inward loop. If
		 * such exists, then the outward loop belongs to that sector; if not, it
		 * belongs to a new sector for all such 'stray' outward loops.
		 */
		lplooplistRover = lplooplistOutwards;

		while(lplooplistRover)
		{
			LOOPLIST *lplooplistRoverInwards = lplooplistInwards;
			LOOPLIST *lplooplistTightestEnclosing = NULL;
			BOOL bBack = (lplooplistRover->iFirstLineSide == LS_BACK);

			/* Get a point a smidgen outside the outward loop to see whether it
			 * (and hence the whole loop) lies in the inward loop.
			 */

			float x, y;

			/* Get the point on one side; if it's inside the loop itself, take a
			 * point on the other.
			 */
			GetLineSideSpot(lpmap, lplooplistRover->diarrayLinedefs.lpiIndices[0], LS_FRONT, 0.01f, &x, &y);
			if(PointInLinedefs(lpmap, x, y, lplooplistRover->diarrayLinedefs.lpiIndices, lplooplistRover->diarrayLinedefs.uiCount))
				GetLineSideSpot(lpmap, lplooplistRover->diarrayLinedefs.lpiIndices[0], LS_BACK, 0.01f, &x, &y);

			while(lplooplistRoverInwards)
			{
				if(PointInLinedefs(lpmap, x, y, lplooplistRoverInwards->diarrayLinedefs.lpiIndices, lplooplistRoverInwards->diarrayLinedefs.uiCount))
				{
					float xTightest, yTightest;

					/* Inward loop encloses, but is it the tightest so far? */
					if(!lplooplistTightestEnclosing ||
						(GetLineSideSpot(lpmap, lplooplistTightestEnclosing->diarrayLinedefs.lpiIndices[0], LS_FRONT, 0.0f, &xTightest, &yTightest),
						!PointInLinedefs(lpmap, xTightest, yTightest, lplooplistRoverInwards->diarrayLinedefs.lpiIndices, lplooplistRoverInwards->diarrayLinedefs.uiCount)))
					{
						/* We're tighter! */
						lplooplistTightestEnclosing = lplooplistRoverInwards;
					}
				}

				lplooplistRoverInwards = lplooplistRoverInwards->lplooplistNext;
			}

			/* Did we find an enclosing loop? */
			if(lplooplistTightestEnclosing)
			{
				/* Inherit sector from that loop. */
				lplooplistRover->iSector = lplooplistTightestEnclosing->iSector;
			}
			else
			{
				/* Use the stray sector. */

				/* Allocate it if we've not already done so. */
				if(iStraySector < 0)
				{
					iStraySector = AddSector(lpmap);
					lpmap->sectors[iStraySector] = lpmap->sectors[iSector];
				}

				lplooplistRover->iSector = iStraySector;
			}

			/* Make lines refer to new sector. */
			for(i = 0; i < (int)lplooplistRover->diarrayLinedefs.uiCount; i++)
			{
				MAPLINEDEF *lpld = &lpmap->linedefs[lplooplistRover->diarrayLinedefs.lpiIndices[i]];
				lpmap->sidedefs[(!!lplooplistRover->diarraySameSide.lpiIndices[i] ^ bBack) ? lpld->s1 : lpld->s2].sector = lplooplistRover->iSector;
			}

			lplooplistRover = lplooplistRover->lplooplistNext;
		}
	}

	DestroyLDLookupTable(lpmap, lpldlookup);
	DestroyLoopList(lplooplistInwards);
	DestroyLoopList(lplooplistOutwards);
	ClearLoopFlags(lpmap);
}


/* ResolveSelectedSectors
 *   Resolves selected sectors into connected components.
 *
 * Parameters:
 *   MAP*				lpmap			Pointer to map structure.
 *   SELECTION_LIST*	lpsellist		Selection of sectors.
 *
 * Return value: None.
 */
void ResolveSelectedSectors(MAP *lpmap, SELECTION_LIST* lpsellist)
{
	int i;

	for(i = 0; i < lpsellist->iDataCount; i++)
		ResolveSector(lpmap, lpsellist->lpiIndices[i]);
}


/* CorrectRSLinesSidedness
 *   Corrects any RS lines that are single-sided on the back.
 *
 * Parameters:
 *   MAP*	lpmap			Pointer to map structure.
 *
 * Return value: None.
 *
 * Remarks:
 *   Useful after pasting.
 */
void CorrectRSLinesSidedness(MAP *lpmap)
{
	int i;

	for(i = 0; i < lpmap->iLinedefs; i++)
	{
		MAPLINEDEF *lpld = &lpmap->linedefs[i];

		if((lpld->editflags & LEF_RECALCSECTOR) &&
			!(lpld->flags & LDF_DOUBLESIDED) &&
			!SidedefExists(lpmap, lpld->s1))
		{
			FlipLinedef(lpmap, i);
		}
	}
}


/* NearestPointOnSnappableLine
 *   Finds an integral approximation to the nearest point on any line which we
 *   can snap to to a given point.
 *
 * Parameters:
 *   MAP*	lpmap				Pointer to map structure.
 *   int	xSrc, ySrc			Point wrt which to minimise distance.
 *   int	*lpxLine, *lpyLine	Nearest point is returned here.
 *
 * Return value: BOOL
 *   TRUE if a point was found; FALSE otherwise.
 */
BOOL NearestPointOnSnappableLine(MAP *lpmap, int xSrc, int ySrc, int *lpxLine, int *lpyLine)
{
	int i;
	FPOINT fptNearest = {0.0f, 0.0f};

	if(lpmap->iLinedefs == 0)
		return FALSE;

	for(i = 0; i < lpmap->iLinedefs; i++)
	{
		if(!lpmap->linedefs[i].selected && !(lpmap->linedefs[i].editflags & LEF_LENGTHCHANGING))
		{
			FPOINT fptCurrent;
			MAPVERTEX *lpvx1 = &lpmap->vertices[lpmap->linedefs[i].v1];
			MAPVERTEX *lpvx2 = &lpmap->vertices[lpmap->linedefs[i].v2];

			/* Find nearest point on this line. */
			nearest_point_on_line((float)lpvx1->x, (float)lpvx1->y, (float)lpvx2->x, (float)lpvx2->y, (float)xSrc, (float)ySrc, &fptCurrent.x, &fptCurrent.y);

			/* We're nearest so far if we're first or nearer than previous best. */
			if(i == 0 || distancef((float)xSrc, (float)ySrc, fptCurrent.x, fptCurrent.y) < distancef((float)xSrc, (float)ySrc, fptNearest.x, fptNearest.y))
				fptNearest = fptCurrent;
		}
	}

	*lpxLine = (int)fptNearest.x;
	*lpyLine = (int)fptNearest.y;

	return TRUE;
}


/* IsRectangleUnobstructed
 *   Determines whether a rectangle is free from (partial) lines and vertices.
 *
 * Parameters:
 *   MAP*	lpmap			Pointer to map structure.
 *   short	xLeft, xRight,	Co-ordinates of rectangle.
 *			yTop, yBottom
 *
 * Return value: BOOL
 *   TRUE if rectangle is unobstructed; FALSE otherwise.
 */
BOOL IsRectangleUnobstructed(MAP *lpmap, short xLeft, short xRight, short yTop, short yBottom)
{
	int iVertex;

	/* First, make sure there are no linedefs intersecting any of our edges. */
	if(LinedefIntersectingSegment(lpmap, xLeft, yTop, xRight, yTop, TRUE) >= 0) return FALSE;
	if(LinedefIntersectingSegment(lpmap, xLeft, yBottom, xRight, yBottom, TRUE) >= 0) return FALSE;
	if(LinedefIntersectingSegment(lpmap, xLeft, yTop, xLeft, yBottom, TRUE) >= 0) return FALSE;
	if(LinedefIntersectingSegment(lpmap, xRight, yTop, xRight, yBottom, TRUE) >= 0) return FALSE;

	/* Now make sure there are no vertices inside the rectangle. */
	for(iVertex = 0; iVertex < lpmap->iVertices; iVertex++)
	{
		MAPVERTEX *lpvx = &lpmap->vertices[iVertex];

		if(lpvx->x >= xLeft && lpvx->x <= xRight && lpvx->y <= yTop && lpvx->y >= yBottom)
			return FALSE;
	}

	return TRUE;
}


/* CreatePolygonalSector
 *   Creates a polygonal sector with a given radius and number of edges.
 *
 * Parameters:
 *   MAP*				lpmap				Pointer to map structure.
 *   short				xCentre, yCentre	Co-ordinates of centre.
 *   short				nRadius				Radius.
 *   short				nEdges				Number of edges.
 *   ENUM_RADIUS_TYPE	radiustype			Whether the radius is measured to
 *											the edges or to the vertices.
 *   DRAW_OPERATION*	lpdrawop			Draw operation structure.
 *   CONFIG*			lpcfgWadOptMap		Map options.
 *   LPCTSTR			szSky				Sky texture name.
 *   BOOL				bSnapToGrid			Whether to snap the vertices to the
 *											grid.
 *   short				cxGrid, cyGrid,		Grid parameters. Ignore if
 *						xGridOffset,		bSnapToGrid is FALSE.
 *						yGridOffset
 *
 * Return value: None.
 *
 * Remarks:
 *   A draw operation is begun and ended by this function.
 */
void CreatePolygonalSector(MAP *lpmap, short xCentre, short yCentre, short nRadius, short nEdges, ENUM_RADIUS_TYPE radiustype, DRAW_OPERATION *lpdrawop, CONFIG *lpcfgWadOptMap, LPCTSTR szSky, BOOL bSnapToGrid, short cxGrid, short cyGrid, short xGridOffset, short yGridOffset)
{
	/* Calculate the real radius. */
	int i;
	float fRadius = radiustype == RT_TOEDGES ? nRadius / (float)cos(PI / nEdges) : nRadius;

	BeginDrawOperation(lpdrawop, lpmap);

	/* Loop round the vertices, making the first one *twice* - this
	 * completes the sector.
	 */
	for(i = 0; i <= nEdges; i++)
	{
		short x, y;

		x = max(-32768, min(32767, Round(xCentre + fRadius * cos((2 * i + 1) * PI / nEdges))));
		y = max(-32768, min(32767, Round(yCentre - fRadius * sin((2 * i + 1) * PI / nEdges))));

		/* This is a real snap-to-grid, not a snap as we draw, which would
		 * require the various flags.
		 */
		if(bSnapToGrid)
			Snap(&x, &y, cxGrid, cyGrid, xGridOffset, yGridOffset);

		DrawToNewVertex(lpmap, lpdrawop, lpcfgWadOptMap, x, y, FALSE);
	}

	EndDrawOperation(lpmap, lpdrawop, lpcfgWadOptMap, szSky);
}



/* CreateRectangularSector
 *   Creates a rectangular sector.
 *
 * Parameters:
 *   MAP*				lpmap			Pointer to map structure.
 *   LPRECT				lprc			Rectangle of sector.
 *   DRAW_OPERATION*	lpdrawop		Draw operation structure.
 *   CONFIG*			lpcfgWadOptMap	Map options.
 *   LPCTSTR			szSky			Sky texture name.
 *
 * Return value: None.
 *
 * Remarks:
 *   A draw operation is begun and ended by this function. Not helpful for DCK
 *   drawing, where the first vertex has already been drawn.
 */
void CreateRectangularSector(MAP *lpmap, LPRECT lprc, DRAW_OPERATION *lpdrawop, CONFIG *lpcfgWadOptMap, LPCTSTR szSky)
{
	BeginDrawOperation(lpdrawop, lpmap);

	/* Go all the way round, finishing where we started. */
	DrawToNewVertex(lpmap, lpdrawop, lpcfgWadOptMap, (short)lprc->left, (short)lprc->top, FALSE);
	DrawToNewVertex(lpmap, lpdrawop, lpcfgWadOptMap, (short)lprc->right, (short)lprc->top, FALSE);
	DrawToNewVertex(lpmap, lpdrawop, lpcfgWadOptMap, (short)lprc->right, (short)lprc->bottom, FALSE);
	DrawToNewVertex(lpmap, lpdrawop, lpcfgWadOptMap, (short)lprc->left, (short)lprc->bottom, FALSE);
	DrawToNewVertex(lpmap, lpdrawop, lpcfgWadOptMap, (short)lprc->left, (short)lprc->top, FALSE);

	EndDrawOperation(lpmap, lpdrawop, lpcfgWadOptMap, szSky);
}


/* FindMapBoundaries
 *   Finds the tightest bounding box for the entire map.
 *
 * Parameters:
 *   MAP*			lpmap	Map.
 *   SMALL_RECT*	*lpsrc	Used to return co-ordinates of bounding box.
 *
 * Return value: None.
 */
static void FindMapBoundaries(MAP *lpmap, SMALL_RECT *lpsrc)
{
	int i;

	lpsrc->Left = lpsrc->Bottom = lpsrc->Right = lpsrc->Top = 0;

	for(i = 0; i < lpmap->iLinedefs; i++)
	{
		lpsrc->Left = min(lpsrc->Left, lpmap->vertices[i].x);
		lpsrc->Right = max(lpsrc->Right, lpmap->vertices[i].x);
		lpsrc->Bottom = min(lpsrc->Bottom, lpmap->vertices[i].y);
		lpsrc->Top = max(lpsrc->Top, lpmap->vertices[i].y);
	}

	for(i = 0; i < lpmap->iLinedefs; i++)
	{
		lpsrc->Left = min(lpsrc->Left, lpmap->things[i].x);
		lpsrc->Right = max(lpsrc->Right, lpmap->things[i].x);
		lpsrc->Bottom = min(lpsrc->Bottom, lpmap->things[i].y);
		lpsrc->Top = max(lpsrc->Top, lpmap->things[i].y);
	}
}


/* AllocateMapObjGrid
 *   Allocates a map-object -grid structure, with the appropriate cell layout
 *   but with no contents.
 *
 * Parameters:
 *   MAP*	lpmap			Map.
 *   int	iPopEstimate	Estimate of number of objects eventually to be
 *							included in the MOG.
 *
 * Return value: MAPOBJGRID*
 *   Pointer to the grid.
 *
 * Remarks:
 *   Free the returned pointer by calling DestroyMapGrid.
 */
static MAPOBJGRID* AllocateMapObjGrid(MAP *lpmap, int iPopEstimate)
{
#ifdef USE_MOG
	/* Parameters for divvying up the map. */
	const int ELEMENTSPERCELL = 16;

	MAPOBJGRID *lpmog = ProcHeapAlloc(sizeof(MAPOBJGRID));
	SMALL_RECT src;
	int i, j;
	UINT ciInit;
	int iDesiredSquareEdgeCells = (int)sqrt(iPopEstimate / ELEMENTSPERCELL);
	float fWidthToHeightRatio;

	FindMapBoundaries(lpmap, &src);
	fWidthToHeightRatio = (float)fabs((float)(src.Right - src.Left) / max(1, src.Top - src.Bottom));

	lpmog->iRows = max(1, (int)(iDesiredSquareEdgeCells * fWidthToHeightRatio));
	lpmog->iColumns = max(1, (int)(iDesiredSquareEdgeCells / fWidthToHeightRatio));

	/* Divvy up the map. */
	lpmog->cxGrid = (src.Right - src.Left - 1) / lpmog->iColumns + 1;
	lpmog->cyGrid = (src.Top - src.Bottom - 1) / lpmog->iRows + 1;

	if(lpmog->cxGrid <= 0) lpmog->cxGrid = 1;
	if(lpmog->cyGrid <= 0) lpmog->cyGrid = 1;

	/* Origin. */
	lpmog->xOrigin = src.Left;
	lpmog->yOrigin = src.Bottom;

	/* Initial number of entries per cell. */
	ciInit = max(MIN_MOG_CELL_ENTRIES, iPopEstimate / (lpmog->iRows * lpmog->iColumns));

	/* Make the cells themselves. */
	lpmog->lpdiarrayGrid = ProcHeapAlloc(sizeof(DYNAMICINTARRAY) * lpmog->iColumns * lpmog->iRows);
	for(i = 0; i < lpmog->iColumns; i++)
		for(j = 0; j < lpmog->iRows; j++)
			InitialiseDynamicIntArray(GET_MOG_CELL(lpmog, i, j), ciInit);

	return lpmog;
#else
	UNREFERENCED_PARAMETER(lpmap);
	UNREFERENCED_PARAMETER(iPopEstimate);
	(void)AllocateMapObjGrid;
	(void)FindMapBoundaries;
	return NULL;
#endif
}


/* DestroyMapObjGrid
 *   Destroys a map-object grid.
 *
 * Parameters:
 *   MAPOBJGRID*	lpmog	Grid to free.
 *
 * Return value: None.
 */
void DestroyMapObjGrid(MAPOBJGRID *lpmog)
{
	int i, j;

	for(i = 0; i < lpmog->iColumns; i++)
		for(j = 0; j < lpmog->iRows; j++)
			FreeDynamicIntArray(GET_MOG_CELL(lpmog, i, j));

	ProcHeapFree(lpmog->lpdiarrayGrid);
	ProcHeapFree(lpmog);
}


/* GetMOGCellFromPoint et al.
 *   Retrieves the cell from a map-object grid containing a point.
 *
 * Parameters:
 *   MAPOBJGRID*	lpmog	Grid.
 *   shrot			x, y	Point.
 *
 * Return value: DYNAMICINTARRAY*
 *   Pointer to the cell.
 *
 * Remarks:
 *   Points outside the boundary of the map at the time the grid was created
 *   will fall into the nearest cell on the edge of the grid.
 */
static __inline int GetMOGColFromAbscissa(MAPOBJGRID *lpmog, short x)
{
	return max(0, min(lpmog->iColumns - 1, (x - (int)lpmog->xOrigin) / lpmog->cxGrid));
}

static __inline int GetMOGRowFromOrdinate(MAPOBJGRID *lpmog, short y)
{
	return max(0, min(lpmog->iRows - 1, (y - (int)lpmog->yOrigin) / lpmog->cyGrid));
}

static __inline DYNAMICINTARRAY* GetMOGCellFromPoint(MAPOBJGRID *lpmog, short x, short y)
{
	return GET_MOG_CELL(lpmog, GetMOGColFromAbscissa(lpmog, x), GetMOGRowFromOrdinate(lpmog, y));
}


/* AddVertexToMOG
 *   Adds a vertex to a map-object grid.
 *
 * Parameters:
 *   MAPOBJGRID*	lpmog		Grid
 *   MAP*			lpmap		Map.
 *   int			iVertex		Index of vertex.
 *
 * Return value: None.
 */
static __inline void AddVertexToMOG(MAPOBJGRID *lpmog, MAP *lpmap, int iVertex)
{
	AddToDynamicIntArray(GetMOGCellFromPoint(lpmog, lpmap->vertices[iVertex].x, lpmap->vertices[iVertex].y), iVertex);
}


/* AddLinedefToMOG
 *   Adds a linedef to a map-object grid.
 *
 * Parameters:
 *   MAPOBJGRID*	lpmog		Grid
 *   MAP*			lpmap		Map.
 *   int			iLinedef	Index of linedef.
 *
 * Return value: None.
 */
void AddLinedefToMOG(MAPOBJGRID *lpmog, MAP *lpmap, int iLinedef)
{
	/* We add the linedef to *every* cell it touches. */

	MAPVERTEX *lpvx1 = &lpmap->vertices[lpmap->linedefs[iLinedef].v1];
	MAPVERTEX *lpvx2 = &lpmap->vertices[lpmap->linedefs[iLinedef].v2];
	short nCol = GetMOGColFromAbscissa(lpmog, lpvx1->x);
	short nRow = GetMOGRowFromOrdinate(lpmog, lpvx1->y);

	do
	{
		AddToDynamicIntArray(GET_MOG_CELL(lpmog, nCol, nRow), iLinedef);
	} while(GetNextLineSegmentMOGCell(lpmog, lpvx1->x, lpvx1->y, lpvx2->x, lpvx2->y, &nCol, &nRow, NULL, FALSE));
}


/* MOGCellDistFromPoint
 *   Finds the distance from a point to a cell.
 *
 * Parameters:
 *   MAPOBJGRID*	lpmog		Grid
 *   short			nCol, nRow	Co-ordinates of cell.
 *   short			x, y		Co-ordinates of point.
 *
 * Return value: int
 *   Distance from point to cell.
 */
static int MOGCellDistFromPoint(MAPOBJGRID *lpmog, short nCol, short nRow, short x, short y)
{
	RECT rcCell;
	POINTS ptsNearest;

	/* Get extent of cell. */
	GetMOGCellRect(lpmog, nCol, nRow, &rcCell);

	/* Find nearest point in cell. */
	ptsNearest.x = (SHORT)((x >= rcCell.left) ? ((x <= rcCell.right) ? x : rcCell.right) : rcCell.left);
	ptsNearest.y = (SHORT)((y >= rcCell.bottom) ? ((y <= rcCell.top) ? y : rcCell.top) : rcCell.bottom);

	return (int)distancei(x, y, ptsNearest.x, ptsNearest.y);
}


/* GetMOGCellRect
 *   Retrieves the boundaries of a cell.
 *
 * Parameters:
 *   MAPOBJGRID*	lpmog		Grid
 *   short			nCol, nRow	Co-ordinates of cell.
 *   LPRECT			lprc		Rectangle to fill with boundaries.
 *
 * Return value: None.
 *
 * Remarks:
 *   The boundaries are included in the cell.
 */
static __inline void GetMOGCellRect(MAPOBJGRID *lpmog, short nCol, short nRow, LPRECT lprc)
{
	/* Get extent of cell. */
	lprc->left = (nCol == 0) ? -32768 : lpmog->xOrigin + nCol * lpmog->cxGrid;
	lprc->right = (nCol == lpmog->iColumns - 1) ? 32767 : (lpmog->xOrigin + (nCol + 1) * lpmog->cxGrid - 1);
	lprc->bottom = (nRow == 0) ? -32768 : lpmog->yOrigin + nRow * lpmog->cyGrid;
	lprc->top = (nRow == lpmog->iRows - 1) ? 32767 : (lpmog->yOrigin + (nRow + 1) * lpmog->cyGrid - 1);
}


/* FindNearestObjectInMOG
 *   Finds the nearest object to a specified object using a MOG.
 *
 * Parameters:
 *   MAP*					lpmap				Map.
 *   MAPOBJGRID*			lpmog				Grid.
 *   void*					plvSourceObject		Index or location of source
 *												object, according to dwFlags.
 *   LPFNOBJ_CELL_METRIC	lpfnObjCellMetric	Object-cell metric.
 *   LPFNOBJ_REP_RECT		lpfnObjRepRect		Representative box function.
 *   LPFNNEAREST_IN_CELL	lpfnNearestInCell	Finds nearest object in a cell
 *												to a point.
 *   LPFNOBJ_CONDITION		lpfnObjCondition	Condition on matching objects.
 *												May be NULL.
 *   void*					lpvConditionParam	Parameter to lpfnObjCondition.
 *   DWORD					dwFlags				See ENUM_FNOIMOG_FLAGS.
 *
 * Return value: int
 *   Index of nearest object, or negative if none found.
 *
 * Remarks:
 *   This is one of those functions that makes me see the wisdom in object
 *   orientation.
 */
int FindNearestObjectInMOG(MAP *lpmap,
							MAPOBJGRID *lpmog,
							void *lpvSourceObject,
							LPFNOBJ_CELL_METRIC lpfnObjCellMetric,
							LPFNOBJ_REP_RECT lpfnObjRepRect,
							LPFNNEAREST_IN_CELL lpfnNearestInCell,
							LPFNOBJ_CONDITION lpfnObjCondition,
							void *lpvConditionParam,
							DWORD dwFlags)
{
	RECT rcBound;
	short nCellsFromCentre = 0;
	QUEUE *lpqueue = AllocateQueue();

	/* Assume no match. */
	int iMatch = -1;
	int iDistance = INT_MAX;
	BOOL bFirstMatch = TRUE;

	/* Find the cell we should start in. */
	if(dwFlags & FNF_OBJISRECT)
		CopyMemory(&rcBound, lpvSourceObject, sizeof(RECT));
	else
	{
		int iSourceObject;
		CopyMemory(&iSourceObject, lpvSourceObject, sizeof(int));
		lpfnObjRepRect(lpmap, iSourceObject, &rcBound);
	}

	/* Convert map co-ordinates to grid co-ordinates. */
	rcBound.left = GetMOGColFromAbscissa(lpmog, (short)rcBound.left);
	rcBound.bottom = GetMOGRowFromOrdinate(lpmog, (short)rcBound.bottom);
	rcBound.top = GetMOGRowFromOrdinate(lpmog, (short)rcBound.top);
	rcBound.right = GetMOGColFromAbscissa(lpmog, (short)rcBound.right);

	/* Begin the search loop. */
	do
	{
		/* Consider all cells in queue. */
		while(!QueueIsEmpty(lpqueue))
		{
			LPPOINT lppt = DeQueue(lpqueue);
			int iNewDistance = INT_MAX;
			int iNearestInCell = lpfnNearestInCell(lpmap, lpmog, (short)lppt->x, (short)lppt->y, lpvSourceObject, &iNewDistance, lpfnObjCondition, lpvConditionParam);

			/* Found a better match? */
			if(iNearestInCell >= 0 && iNewDistance < iDistance)
			{
				iMatch = iNearestInCell;
				iDistance = iNewDistance;
			}

			ProcHeapFree(lppt);
		}

		/* If we found a match for the first time, enqueue all other
		 * possibilities.
		 */
		if(iMatch >= 0 && bFirstMatch)
		{
			int iRow, iCol;

			bFirstMatch = FALSE;

			/* Okay, not pretty. If we can make some assumptions about the
			 * metric, we can do better than this. TODO?
			 */
			for(iCol = 0; iCol < lpmog->iColumns; iCol++)
			{
				for(iRow = 0; iRow < lpmog->iRows; iRow++)
				{
					if(GET_MOG_CELL(lpmog, iCol, iRow)->uiCount > 0 && lpfnObjCellMetric(lpmap, lpmog, iCol, iRow, lpvSourceObject) < iDistance)
					{
						/* This will be freed when it's de-queued. */
						LPPOINT lppt = ProcHeapAlloc(sizeof(POINT));
						lppt->x = iCol;
						lppt->y = iRow;
						EnQueue(lpqueue, lppt);
					}
				}
			}
		}
		else if(iMatch < 0)
		{
			/* No matches found at all yet. Enqueue the next ring. */

			int iRow;
			int iStartRow, iEndRow;

			iStartRow = max(0, rcBound.bottom - nCellsFromCentre);
			iEndRow = min(lpmog->iRows - 1, rcBound.top + nCellsFromCentre);

			for(iRow = iStartRow; iRow <= iEndRow; iRow++)
			{
				/* If this is a bona fide first or last row, or we're still in
				 * the centre, we enqueue every cell. In this case, we know that
				 * all our cells exist.
				 */
				if(nCellsFromCentre == 0 ||
					rcBound.bottom - iRow == nCellsFromCentre ||
					iRow - rcBound.top == nCellsFromCentre)
				{
					int iCol;
					int iStartCol, iEndCol;

					iStartCol = max(0, rcBound.left - nCellsFromCentre);
					iEndCol = min(lpmog->iColumns - 1, rcBound.right + nCellsFromCentre);

					for(iCol = iStartCol; iCol <= iEndCol; iCol++)
					{
						if(GET_MOG_CELL(lpmog, iCol, iRow)->uiCount > 0)
						{
							/* This will be freed when it's de-queued. */
							LPPOINT lppt = ProcHeapAlloc(sizeof(POINT));
							lppt->x = iCol;
							lppt->y = iRow;
							EnQueue(lpqueue, lppt);
						}
					}
				}
				else
				{
					/* Otherwise, we're in an intermediate row, so just enqueue
					 * the end ones, and only if they exist.
					 */

					/* Points that we allocated are freed when they're
					 * de-queued.
					 */
					LPPOINT lppt;

					if(rcBound.left - nCellsFromCentre >= 0 &&
						GET_MOG_CELL(lpmog, rcBound.left - nCellsFromCentre, iRow)->uiCount > 0)
					{
						lppt = ProcHeapAlloc(sizeof(POINT));
						lppt->x = rcBound.left - nCellsFromCentre;
						lppt->y = iRow;
						EnQueue(lpqueue, lppt);
					}

					if(rcBound.right + nCellsFromCentre < lpmog->iColumns &&
						GET_MOG_CELL(lpmog, rcBound.right + nCellsFromCentre, iRow)->uiCount > 0)
					{
						lppt = ProcHeapAlloc(sizeof(POINT));
						lppt->x = rcBound.right + nCellsFromCentre;
						lppt->y = iRow;
						EnQueue(lpqueue, lppt);
					}
				}
			}

			/* Move out by one cell. */
			nCellsFromCentre++;
		}

	/* Stop searching when the queue is empty, i.e. when we've exhausted all
	 * cells in which we could possibly have a match.
	 */
	} while(!QueueIsEmpty(lpqueue));

	DestroyQueue(lpqueue);

	return iMatch;
}


/* CreateVertexMOG
 *   Quick helper routine to create a MOG and fill it with vertices.
 *
 * Parameters:
 *   MAP*	lpmap	Map.
 *
 * Return value: MAPOBJGRID*
 *   Pointer to populated MOG.
 *
 * Remarks:
 *   Free the MOG in the usual manner.
 */
MAPOBJGRID* CreateVertexMOG(MAP *lpmap)
{
#ifdef USE_MOG
	int i;
	MAPOBJGRID *lpmogVertices = AllocateMapObjGrid(lpmap, lpmap->iVertices);

	/* Add all the vertices to the grid. */
	for(i = 0; i < lpmap->iVertices; i++)
		AddVertexToMOG(lpmogVertices, lpmap, i);

	return lpmogVertices;
#else
	UNREFERENCED_PARAMETER(lpmap);
	return NULL;
#endif
}


/* CreateConditionedVertexMOG
 *   Creates a MOG filled with certain vertices.
 *
 * Parameters:
 *   MAP*							lpmap			Map.
 *   BOOL (*)(MAP*, int, void*)		lpfnCondition	Condition. May be NULL.
 *   void*							lpvParam		Parameter to lpfnCondition.
 *
 * Return value: MAPOBJGRID*
 *   Pointer to populated MOG.
 *
 * Remarks:
 *   Free the MOG in the usual manner.
 */
MAPOBJGRID* CreateConditionedVertexMOG(MAP *lpmap, BOOL (*lpfnCondition)(MAP* lpmap, int iVertex, void *lpvParam), void* lpvParam)
{
#ifdef USE_MOG
	const int SAMPLESIZE = 16;

	int i, iSampleMatch;
	MAPOBJGRID *lpmogVertices;

	/* Sample some vertices to estimate how many we'll have in total. */
	iSampleMatch = 0;
	for(i = 0; i < SAMPLESIZE; i++)
	{
		if(!lpfnCondition || lpfnCondition(lpmap, (i * lpmap->iVertices) / SAMPLESIZE, lpvParam))
			iSampleMatch++;
	}

	lpmogVertices = AllocateMapObjGrid(lpmap, (iSampleMatch * lpmap->iVertices) / SAMPLESIZE);

	/* Add matching vertices to the grid. */
	for(i = 0; i < lpmap->iVertices; i++)
		if(!lpfnCondition || lpfnCondition(lpmap, i, lpvParam))
			AddVertexToMOG(lpmogVertices, lpmap, i);

	return lpmogVertices;
#else
	UNREFERENCED_PARAMETER(lpmap);
	UNREFERENCED_PARAMETER(lpfnCondition);
	UNREFERENCED_PARAMETER(lpvParam);
	return NULL;
#endif
}


/* CreateLinedefMOG
 *  Quick helper routine to create a MOG and fill it with linedefs.
 *
 * Parameters:
 *   MAP*	lpmap	Map.
 *
 * Return value: MAPOBJGRID*
 *   Pointer to populated MOG.
 *
 * Remarks:
 *   Free the MOG in the usual manner.
 */
MAPOBJGRID* CreateLinedefMOG(MAP *lpmap)
{
#ifdef USE_MOG
	int i;
	MAPOBJGRID *lpmogLinedefs = AllocateMapObjGrid(lpmap, lpmap->iLinedefs);

	/* Add all the linedefs to the grid. */
	for(i = 0; i < lpmap->iLinedefs; i++)
		AddLinedefToMOG(lpmogLinedefs, lpmap, i);

	return lpmogLinedefs;
#else
	UNREFERENCED_PARAMETER(lpmap);
	return NULL;
#endif
}


/* PointToCellMetric
 *   Essentially MOGCellDistFromPoint, but dressed up as an LPFNOBJ_CELL_METRIC.
 *
 * Parameters:
 *   MAP*			lpmap			Map. Ignored.
 *   MAPOBJGRID*	lpmog			Map object grid.
 *   short			nCol, nRow		Co-ordinates of cell.
 *   void*			lpvPoint		A POINTS*, not assumed to be aligned.
 *
 * Return value: int
 *   Distance from point to cell.
 */
int PointToCellMetric(MAP *lpmap, MAPOBJGRID *lpmog, short nCol, short nRow, void *lpvPoint)
{
	POINTS pts;

	UNREFERENCED_PARAMETER(lpmap);

	CopyMemory(&pts, lpvPoint, sizeof(POINTS));
	return MOGCellDistFromPoint(lpmog, nCol, nRow, pts.x, pts.y);
}


/* LinedefToCellMetric
 *   LPFNOBJ_CELL_METRIC for finding distance from a linedef to a cell.
 *
 * Parameters:
 *   MAP*			lpmap			Map.
 *   MAPOBJGRID*	lpmog			Map object grid.
 *   short			nCol, nRow		Co-ordinates of cell.
 *   void*			lpvLinedef		Index of linedef, not assumed to be aligned.
 *
 * Return value: int
 *   Distance from point to cell.
 */
int LinedefToCellMetric(MAP *lpmap, MAPOBJGRID *lpmog, short nCol, short nRow, void *lpvLinedef)
{
	int iLinedef, iNewDist;
	int iDistance = INT_MAX;
	float xLinedef1, yLinedef1;
	float xLinedef2, yLinedef2;
	MAPVERTEX *lpvx1, *lpvx2;
	RECT rcCell;

	/* Get linedef index. */
	CopyMemory(&iLinedef, lpvLinedef, sizeof(int));

	lpvx1 = &lpmap->vertices[lpmap->linedefs[iLinedef].v1];
	lpvx2 = &lpmap->vertices[lpmap->linedefs[iLinedef].v2];

	xLinedef1 = (float)lpvx1->x;
	yLinedef1 = (float)lpvx1->y;
	xLinedef2 = (float)lpvx2->x;
	yLinedef2 = (float)lpvx2->y;

	/* The shortest chord joining the two must either pass through one of the
	 * cell's vertices or one of the linedef's. Calculate all six possibilities
	 * and take the minimum.
	 */

	/* Linedef's vertices first. */
	iNewDist = MOGCellDistFromPoint(lpmog, nCol, nRow, lpvx1->x, lpvx1->y);
	iDistance = min(iDistance, iNewDist);

	iNewDist = MOGCellDistFromPoint(lpmog, nCol, nRow, lpvx2->x, lpvx2->y);
	iDistance = min(iDistance, iNewDist);

	/* Now check the cell's vertices. */
	GetMOGCellRect(lpmog, nCol, nRow, &rcCell);

	iNewDist = (int)distance_to_line(xLinedef1, yLinedef1, xLinedef2, yLinedef2, (float)rcCell.left, (float)rcCell.bottom);
	iDistance = min(iDistance, iNewDist);

	iNewDist = (int)distance_to_line(xLinedef1, yLinedef1, xLinedef2, yLinedef2, (float)rcCell.left, (float)rcCell.top);
	iDistance = min(iDistance, iNewDist);

	iNewDist = (int)distance_to_line(xLinedef1, yLinedef1, xLinedef2, yLinedef2, (float)rcCell.right, (float)rcCell.bottom);
	iDistance = min(iDistance, iNewDist);

	iNewDist = (int)distance_to_line(xLinedef1, yLinedef1, xLinedef2, yLinedef2, (float)rcCell.right, (float)rcCell.top);
	iDistance = min(iDistance, iNewDist);

	return iDistance;
}


/* NearestConditionedVertexInCell
 *   Finds the nearest vertex in a cell to a point. Useful as an
 *   LPFNNEAREST_IN_CELL.
 *
 * Parameters:
 *   MAP*					lpmap				Map. Not used by us; may be used
 *												by lpfnObjCondition.
 *   MAPOBJGRID*			lpmog				Map object grid.
 *   short					nCol, nRow			Co-ordinates of cell.
 *   void*					lpvRect				(RECT*) Bounding rectangle. Only
 *												left and bottom members used.
 *   int*					lpiDist				Used to return matching
 *												distance.
 *   LPFNOBJ_CONDITION*		lpfnObjCondition	Condition on matching vertices.
 *												May be NULL.
 *   void*					lpvParam			Parameter to lpfnObjCondition.
 *
 * Return value: int
 *   Index of nearest matching vertex in cell, or negative if none found.
 *
 * Remarks:
 *   *lpiDist is clobbered even if no vertex is found.
 */
int NearestConditionedVertexInCell(MAP *lpmap, MAPOBJGRID *lpmog, short nCol, short nRow, void *lpvRect, int *lpiDist, LPFNOBJ_CONDITION lpfnObjCondition, void *lpvParam)
{
	int i;
	int iMatchDistance = INT_MAX;
	int iNearest = -1;
	DYNAMICINTARRAY *lpdiarrayCell = GET_MOG_CELL(lpmog, nCol, nRow);
	RECT rcBound;
	POINTS ptsPoint;

	CopyMemory(&rcBound, lpvRect, sizeof(RECT));
	ptsPoint.x = (short)rcBound.left;
	ptsPoint.y = (short)rcBound.bottom;

	/* Check all vertices in cell. */
	for(i = 0; i < (int)lpdiarrayCell->uiCount; i++)
	{
		int iVertex = lpdiarrayCell->lpiIndices[i];

		if(!lpfnObjCondition || lpfnObjCondition(lpmap, iVertex, lpvParam))
		{
			int iNewDistance = (int)distancei(lpmap->vertices[iVertex].x, lpmap->vertices[iVertex].y, ptsPoint.x, ptsPoint.y);

			/* Are we nearer? */
			if(iNewDistance < iMatchDistance)
			{
				iNearest = iVertex;
				iMatchDistance = iNewDistance;
			}
		}
	}

	*lpiDist = iMatchDistance;
	return iNearest;
}


/* NearestConditionedVertexInCellToLinedef
 *   Finds the nearest vertex in a cell to a linedef. Useful as an
 *   LPFNNEAREST_IN_CELL.
 *
 * Parameters:
 *   MAP*					lpmap				Map. Ignored.
 *   MAPOBJGRID*			lpmog				Map object grid.
 *   short					nCol, nRow			Co-ordinates of cell.
 *   void*					lpvLinedef			(int*) Index of linedef.
 *   int*					lpiDist				Used to return matching
 *												distance.
 *   LPFNOBJ_CONDITION*		lpfnObjCondition	Condition on matching vertices.
 *												May be NULL.
 *   void*					lpvParam			Parameter to lpfnObjCondition.
 *
 * Return value: int
 *   Index of nearest matching vertex in cell, or negative if none found.
 *
 * Remarks:
 *   *lpiDist is clobbered even if no vertex is found.
 */
int NearestConditionedVertexInCellToLinedef(MAP *lpmap, MAPOBJGRID *lpmog, short nCol, short nRow, void *lpvLinedef, int *lpiDist, LPFNOBJ_CONDITION lpfnObjCondition, void *lpvParam)
{
	int i;
	int iMatchDistance = INT_MAX;
	int iNearest = -1;
	DYNAMICINTARRAY *lpdiarrayCell = GET_MOG_CELL(lpmog, nCol, nRow);
	int iLinedef;
	int x1, y1, x2, y2;

	CopyMemory(&iLinedef, lpvLinedef, sizeof(int));

	x1 = lpmap->vertices[lpmap->linedefs[iLinedef].v1].x;
	y1 = lpmap->vertices[lpmap->linedefs[iLinedef].v1].y;
	x2 = lpmap->vertices[lpmap->linedefs[iLinedef].v2].x;
	y2 = lpmap->vertices[lpmap->linedefs[iLinedef].v2].y;


	/* Check all vertices in cell. */
	for(i = 0; i < (int)lpdiarrayCell->uiCount; i++)
	{
		int iVertex = lpdiarrayCell->lpiIndices[i];

		if(!lpfnObjCondition || lpfnObjCondition(lpmap, iVertex, lpvParam))
		{
			int iNewDistance = (int)distance_to_line((float)x1, (float)y1, (float)x2, (float)y2, (float)lpmap->vertices[iVertex].x, (float)lpmap->vertices[iVertex].y);

			/* Are we nearer? */
			if(iNewDistance < iMatchDistance)
			{
				iNearest = iVertex;
				iMatchDistance = iNewDistance;
			}
		}
	}

	*lpiDist = iMatchDistance;
	return iNearest;
}


/* NearestConditionedLinedefInCell
 *   Finds the nearest linedef in a cell to a point. Useful as an
 *   LPFNNEAREST_IN_CELL.
 *
 * Parameters:
 *   MAP*					lpmap				Map. Not used by us; may be used
 *												by lpfnObjCondition.
 *   MAPOBJGRID*			lpmog				Map object grid.
 *   short					nCol, nRow			Co-ordinates of cell.
 *   void*					lpvRect				(RECT*) Bounding rectangle. Only
 *												left and bottom members used.
 *   int*					lpiDist				Used to return matching
 *												distance.
 *   LPFNOBJ_CONDITION*		lpfnObjCondition	Condition on matching linedefs.
 *												May be NULL.
 *   void*					lpvParam			Parameter to lpfnObjCondition.
 *
 * Return value: int
 *   Index of nearest matching linedef in cell, or negative if none found.
 *
 * Remarks:
 *   *lpiDist is clobbered even if no linedef is found.
 */
int NearestConditionedLinedefInCell(MAP *lpmap, MAPOBJGRID *lpmog, short nCol, short nRow, void *lpvRect, int *lpiDist, LPFNOBJ_CONDITION lpfnObjCondition, void *lpvParam)
{
	int i;
	int iMatchDistance = INT_MAX;
	int iNearest = -1;
	DYNAMICINTARRAY *lpdiarrayCell = GET_MOG_CELL(lpmog, nCol, nRow);
	RECT rcBound;
	POINTS ptsPoint;

	CopyMemory(&rcBound, lpvRect, sizeof(RECT));
	ptsPoint.x = (short)rcBound.left;
	ptsPoint.y = (short)rcBound.bottom;

	/* Check all linedefs in cell. */
	for(i = 0; i < (int)lpdiarrayCell->uiCount; i++)
	{
		int iLinedef = lpdiarrayCell->lpiIndices[i];

		if(!lpfnObjCondition || lpfnObjCondition(lpmap, iLinedef, lpvParam))
		{
			int iNewDistance = (int)DistanceToLinedef(lpmap, iLinedef, ptsPoint.x, ptsPoint.y);

			/* Are we nearer? */
			if(iNewDistance < iMatchDistance)
			{
				iNearest = iLinedef;
				iMatchDistance = iNewDistance;
			}
		}
	}

	*lpiDist = iMatchDistance;
	return iNearest;
}



/* GetLinedefBounds
 *   Obtains the bounding box for a linedef. Useful as an LPFNOBJ_REP_RECT.
 *
 * Parameters:
 *   MAP*	lpmap		Map.
 *   int	iLinedef	Index of linedef.
 *   RECT*	lprc		Rectangle to receive bounding box.
 *
 * Return value: None.
 *
 * Remarks:
 *   The rectangle will have left <= right and bottom <= top.
 */
void GetLinedefBounds(MAP *lpmap, int iLinedef, RECT *lprc)
{
	MAPVERTEX *lpvx1 = &lpmap->vertices[lpmap->linedefs[iLinedef].v1];
	MAPVERTEX *lpvx2 = &lpmap->vertices[lpmap->linedefs[iLinedef].v2];

	lprc->left = min(lpvx1->x, lpvx2->x);
	lprc->right = max(lpvx1->x, lpvx2->x);
	lprc->bottom = min(lpvx1->y, lpvx2->y);
	lprc->top = max(lpvx1->y, lpvx2->y);
}


/* GetNextLineSegmentMOGCell
 *   Determine the next MOG cell intersected by a line segment.
 *
 * Parameters:
 *   MAPOBJGRID*	lpmog				MOG.
 *   int			x1, y1, x2, y2		Directed co-ordinates of segment.
 *   short			*lpnCol, *lpnRow	Current and new MOG co-ordinates.
 *   float			*lpfDistance		Distance to point at which we intersect
 *										the boundary, if we do. May be NULL.
 *   BOOL			bInfinite			Whether to extend the segment to a half-
 *										line.
 *
 * Return value: BOOL
 *   TRUE if the line segment hits another cell; FALSE otherwise.
 *
 * Remarks:
 *   *lpnCol, *lpnRow, *lpfDistance may be clobbered even if we return FALSE.
 */
static BOOL GetNextLineSegmentMOGCell(MAPOBJGRID *lpmog, int x1, int y1, int x2, int y2, short *lpnCol, short *lpnRow, float *lpfDistance, BOOL bInfinite)
{
	RECT rcCell;

	/* Next row and column to be hit, although these probably don't happen
	 * simultaneously. They also might be non-existent cells beyond the edge.
	 */
	short nColNextOffset;
	short nRowNextOffset;
	short xBoundary, yBoundary;
	int iDiscriminant;

	GetMOGCellRect(lpmog, *lpnCol, *lpnRow, &rcCell);

	if(x2 > x1)
	{
		nColNextOffset = 1;
		xBoundary = (short)rcCell.right;
	}
	else if(x2 < x1)
	{
		nColNextOffset = -1;
		xBoundary = (short)rcCell.left;
	}
	else
	{
		nColNextOffset = 0;
		xBoundary = 0;
	}

	if(y2 > y1)
	{
		nRowNextOffset = 1;
		yBoundary = (short)rcCell.top;
	}
	else if(y2 < y1)
	{
		nRowNextOffset = -1;
		yBoundary = (short)rcCell.bottom;
	}
	else
	{
		nRowNextOffset = 0;
		yBoundary = 0;
	}

	/* Don't allow us off the edge of the map. */
	if(*lpnCol + nColNextOffset < 0 || *lpnCol + nColNextOffset >= lpmog->iColumns)
		nColNextOffset = 0;
	if(*lpnRow + nRowNextOffset < 0 || *lpnRow + nRowNextOffset >= lpmog->iRows)
		nRowNextOffset = 0;


	/* Special handling for lines parallel to axes. */

	if(nColNextOffset == 0)
	{
		/* Point... */
		if(nRowNextOffset == 0) return FALSE;

		/* Vertical. */
		if(bInfinite || nRowNextOffset * y2 >= nRowNextOffset * yBoundary)
		{
			(*lpnRow) += nRowNextOffset;
			if(lpfDistance) *lpfDistance = (float)abs(yBoundary - y1);
			return TRUE;
		}

		return FALSE;
	}
	else if(nRowNextOffset == 0)
	{
		/* Horizontal. */
		if(bInfinite || nColNextOffset * x2 >= nColNextOffset * xBoundary)
		{
			(*lpnCol) += nColNextOffset;
			if(lpfDistance) *lpfDistance = (float)abs(xBoundary - x1);
			return TRUE;
		}

		return FALSE;
	}

	/* Oblique. When we reach the column boundary, where will we be with respect
	 * to the row boundary?
	 */
	iDiscriminant = nColNextOffset * nRowNextOffset * ((y2 - y1) * (xBoundary - x1) + (x2 - x1) * (y1 - yBoundary));

	if(iDiscriminant >= 0)
	{
		if(!bInfinite && y2 * nRowNextOffset < yBoundary * nRowNextOffset)
		{
			/* In the through-the-vertex case, we can still try the other way.
			 */
			if(iDiscriminant != 0) return FALSE;
		}
		else
		{
			(*lpnRow) += nRowNextOffset;

			if(lpfDistance)
			{
				int cyIntersect = yBoundary - y1;
				float cxIntersect = cyIntersect * (x2 - x1) / (float)(y2 - y1);

				*lpfDistance = (float)sqrt(cxIntersect * cxIntersect + cyIntersect * cyIntersect);
			}
		}
	}

	if(iDiscriminant <= 0)
	{
		if(!bInfinite && x2 * nColNextOffset < xBoundary * nColNextOffset) return FALSE;
		(*lpnCol) += nColNextOffset;

		/* This is being recalculated unnecessarily if we go through the vertex.
		 */
		if(lpfDistance)
		{
			int cxIntersect = xBoundary - x1;
			float cyIntersect = cxIntersect * (y2 - y1) / (float)(x2 - x1);
			*lpfDistance = (float)sqrt(cyIntersect * cyIntersect + cxIntersect * cxIntersect);
		}
	}

	return TRUE;
}


/* RecalcDrawOpMOGs
 *   Refreshes the MOGs cached in a DRAW_OPERATION.
 *
 * Parameters:
 *   MAP*				lpmap		Map
 *   DRAW_OPERATION*	lpdrawop	Draw operation.
 *
 * Return value: None.
 *
 * Remarks:
 *   The MOGs become unrecoverably invalid when backtracking, so this is useful
 *   in those circumstances.
 */
void RecalcDrawOpMOGs(MAP *lpmap, DRAW_OPERATION *lpdrawop)
{
	if(lpdrawop->lpmogLinedefs)
	{
		DestroyMapObjGrid(lpdrawop->lpmogLinedefs);
		lpdrawop->lpmogLinedefs = CreateLinedefMOG(lpmap);
	}

	if(lpdrawop->lpmogVertices)
	{
		DestroyMapObjGrid(lpdrawop->lpmogVertices);
		lpdrawop->lpmogVertices = CreateVertexMOG(lpmap);
	}
}


/* GetOrthHLPointsIn2Coords
 *   Gets the midpoint of a line, and some point orthogonally away from that
 *   midpoint, all in 2coords.
 *
 * Parameters:
 *   MAP*		lpmap		Map.
 *   int		iLinedef	Linedef.
 *   POINT*		lpptOrigin	Midpoint is returned here.
 *   POINT*		lpptOther	Other point is returned here.
 *
 * Return value: None.
 *
 * Remarks:
 *   Useful before calling GetSectorByHalfLineMethod or similar.
 */
static __inline void GetOrthHLPointsIn2Coords(MAP *lpmap, int iLinedef, int iLineSide, POINT *lpptOrigin, POINT *lpptOther)
{
	const MAPLINEDEF *lpld = &lpmap->linedefs[iLinedef];

	/* Get mid-point of line in 2coords. */
	lpptOrigin->x = (int)lpmap->vertices[lpld->v1].x + lpmap->vertices[lpld->v2].x;
	lpptOrigin->y = (int)lpmap->vertices[lpld->v1].y + lpmap->vertices[lpld->v2].y;

	/* Linedef vectors in 2coords. */
	lpptOther->x = lpptOrigin->x + (iLineSide == LS_FRONT ? 1 : -1) * 2 * ((int)lpmap->vertices[lpld->v2].y - lpmap->vertices[lpld->v1].y);
	lpptOther->y = lpptOrigin->y + (iLineSide == LS_FRONT ? -1 : 1) * 2 * ((int)lpmap->vertices[lpld->v2].x - lpmap->vertices[lpld->v1].x);
}
