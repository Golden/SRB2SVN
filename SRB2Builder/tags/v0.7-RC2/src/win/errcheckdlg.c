/* SRB2 Workbench -- A map editor for Sonic Robo Blast 2.
 * Copyright (C) 2009 Gregor Dick
 * http://workbench.srb2.org/
 *
 * Incorporates portions of Doom Builder, (C) 2003 Pascal vd Heiden.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 * USA.
 *
 * errcheckdlg.c: UI for the error-checker.
 *
 * AMDG.
 */

#include <windows.h>
#include <commctrl.h>
#include <tchar.h>
#include <stdio.h>

#include "../general.h"
#include "../map.h"
#include "../errorcheck.h"
#include "../editing.h"

#include "mdiframe.h"
#include "errcheckdlg.h"
#include "gendlg.h"
#include "mapwin.h"

#include "../../res/resource.h"


typedef struct _ERRCHKDLGDATA
{
	HWND		hwndMap;
	MAP			*lpmap;
	LPCTSTR		szLower, szMiddle, szUpper;
	LPCTSTR		szSky;
} ERRCHKDLGDATA;

typedef struct _MAP_ERROR_DESCRIPTOR
{
	BYTE	byErrorType;
	int		iParam1, iParam2, iParam3;
} MAP_ERROR_DESCRIPTOR;

typedef struct _MAP_ERROR_LIST
{
	unsigned int			uiErrors, uiAllocated;
	MAP_ERROR_DESCRIPTOR*	lpmed;
} MAP_ERROR_LIST;


typedef enum _ENUM_MAP_ERROR_TYPES
{
	MET_COINCIDENT_VERTICES,
	MET_UNUSED_VERTEX,
	MET_ZEROLENGTH_LINE,
	MET_STRAY_THING,
	MET_NEGHEIGHT_SECTOR,
	MET_MISSINGFRONT_LINE,
	MET_MISSINGBACK_LINE,
	MET_SUPERFLUOUSBACK_LINE,
	MET_UNUSED_SIDEDEF,
	MET_UNUSED_SECTOR,
	MET_MISSING_TEXTURE,
	MET_CROSSING_LINE,
	MET_UNCLOSED_SECTOR,
	MET_COUNT
} ENUM_MAP_ERROR_TYPES;

typedef enum _ENUM_MAP_ERROR_SEVERITIES
{
	MES_WARNING,
	MES_ERROR
} ENUM_MAP_ERROR_SEVERITIES;

typedef struct _ERRORCLASSINFO
{
	DWORD						dwTitleID, dwFormatID;
	ENUM_MAP_ERROR_SEVERITIES	severity;
	ENUM_EDITMODE				emSelType;
	int							iSelCount;
} ERRORCLASSINFO;

/* These correspond to image-list indices. */
enum MAP_ERROR_ICONS
{
	MEI_WARNING = 0,
	MEI_ERROR = 1
};


static const ERRORCLASSINFO g_errorclassinfo[] =
{
	{IDS_ERRCHK_COINCVX_TITLE, IDS_ERRCHK_COINCVX_FORMAT, MES_ERROR, EM_VERTICES, 2},		/* MET_COINCIDENT_VERTICES */
	{IDS_ERRCHK_UNUSEDVX_TITLE, IDS_ERRCHK_UNUSEDVX_FORMAT, MES_WARNING, EM_VERTICES, 1},	/* MET_UNUSED_VERTEX */
	{IDS_ERRCHK_ZEROLD_TITLE, IDS_ERRCHK_ZEROLD_FORMAT, MES_ERROR, EM_LINES, 1},			/* MET_ZEROLENGTH_LINE */
	{IDS_ERRCHK_STRAYTHING_TITLE, IDS_ERRCHK_STRAYTHING_FORMAT, MES_ERROR, EM_THINGS, 1},	/* MET_STRAY_THING */
	{IDS_ERRCHK_NEGSEC_TITLE, IDS_ERRCHK_NEGSEC_FORMAT, MES_ERROR, EM_SECTORS, 1},			/* MET_NEGHEIGHT_SECTOR */
	{IDS_ERRCHK_MISSINGSD_TITLE, IDS_ERRCHK_LDNOFRONT_FORMAT, MES_ERROR, EM_LINES, 1},		/* MET_MISSINGFRONT_LINE */
	{IDS_ERRCHK_MISSINGSD_TITLE, IDS_ERRCHK_LDNOBACK_FORMAT, MES_ERROR, EM_LINES, 1},		/* MET_MISSINGBACK_LINE */
	{IDS_ERRCHK_EXTRASD_TITLE, IDS_ERRCHK_LDEXTRABACK_FORMAT, MES_ERROR, EM_LINES, 1},		/* MET_SUPERFLUOUSBACK_LINE */
	{IDS_ERRCHK_UNUSEDSD_TITLE, IDS_ERRCHK_UNUSEDSD_FORMAT, MES_WARNING, EM_ANY, 0},		/* MET_UNUSED_SIDEDEF */
	{IDS_ERRCHK_UNUSEDSEC_TITLE, IDS_ERRCHK_UNUSEDSEC_FORMAT, MES_WARNING, EM_ANY, 0},		/* MET_UNUSED_SECTOR */
	{IDS_ERRCHK_MISSTEX_TITLE, IDS_ERRCHK_MISSTEX_FORMAT, MES_WARNING, EM_LINES, 1},		/* MET_MISSING_TEXTURE */
	{IDS_ERRCHK_CROSSLD_TITLE, IDS_ERRCHK_CROSSLD_FORMAT, MES_ERROR, EM_LINES, 2},			/* MET_CROSSING_LINE */
	{IDS_ERRCHK_UNCLOSED_TITLE, IDS_ERRCHK_UNCLOSED_FORMAT, MES_ERROR, EM_LINES, 2},		/* MET_UNCLOSED_SECTOR */
};


#define INIT_ERROR_DESCRIPTORS	32
#define CCH_ERRCHK_COLHDR		64


static __inline void CleanMapErrorList(MAP_ERROR_LIST *lpmel) { ProcHeapFree(lpmel->lpmed); }


static INT_PTR CALLBACK ErrorCheckerDlgProc(HWND hwndDlg, UINT uiMsg, WPARAM wParam, LPARAM lParam);
static void FindAllMapErrors(MAP *lpmap, MAP_ERROR_LIST *lpmel, LPCTSTR szSky);
static __inline void AddToMapErrorList(MAP_ERROR_LIST *lpmel, MAP_ERROR_DESCRIPTOR *lpmedNew);
static void PopulateMapErrorList(HWND hwndListView, MAP_ERROR_LIST *lpmel);
static void SelectErrorObjects(HWND hwndMap, MAP_ERROR_LIST *lpmel, int iErrorIndex);
static void FixError(MAP *lpmap, MAP_ERROR_LIST *lpmel, int iErrorIndex, LPCTSTR szLower, LPCTSTR szMiddle, LPCTSTR szUpper, LPCTSTR szSky);



/* ShowErrorCheckerDlg
 *   Shows the Find dialogue.
 *
 * Parameters:
 *   HWND		hwndMap		Map window handle.
 *   MAP*		lpmap		Map.
 *   LPCTSTR	szLower		\
 *   LPCTSTR	szMiddle	|>	Textures to use where missing.
 *   LPCTSTR	szUpper		/
 *   LPCTSTR	szSky		Sky texture name.
 *
 * Return value: BOOL
 *   TRUE if changes were made to the map; FALSE otherwise.
 */
BOOL ShowErrorCheckerDlg(HWND hwndMap, MAP *lpmap, LPCTSTR szLower, LPCTSTR szMiddle, LPCTSTR szUpper, LPCTSTR szSky)
{
	ERRCHKDLGDATA ecdd;

	ecdd.hwndMap = hwndMap;
	ecdd.lpmap = lpmap;
	ecdd.szLower = szLower;
	ecdd.szMiddle = szMiddle;
	ecdd.szUpper = szUpper;
	ecdd.szSky = szSky;

	return DialogBoxParam(g_hInstance, MAKEINTRESOURCE(IDD_ERRORCHECKER), g_hwndMain, ErrorCheckerDlgProc, (LPARAM)&ecdd);
}


/* ErrorCheckerDlgProc
 *   Dialogue procedure for the Error Checker dialogue.
 *
 * Parameters:
 *   HWND	hwndDlg		Handle to dialogue window.
 *   HWND	uiMsg		Window message ID.
 *   WPARAM	wParam		Message-specific.
 *   LPARAM lParam		Message-specific.
 *
 * Return value: INT_PTR
 *   TRUE if message was processed; FALSE otherwise.
 */
static INT_PTR CALLBACK ErrorCheckerDlgProc(HWND hwndDlg, UINT uiMsg, WPARAM wParam, LPARAM lParam)
{
	static ERRCHKDLGDATA *s_lpecdd;
	static BOOL s_bMapChanged;
	static MAP_ERROR_LIST s_mel;

	switch(uiMsg)
	{
	case WM_INITDIALOG:
		{
			LVCOLUMN lvcol;
			HWND hwndErrorList = GetDlgItem(hwndDlg, IDC_LIST_ERRORS);
			TCHAR szColHeader[CCH_ERRCHK_COLHDR];
			HIMAGELIST himl;
			const int cxIcon = GetSystemMetrics(SM_CXSMICON);
			const int cyIcon = GetSystemMetrics(SM_CYSMICON);

			s_lpecdd = (ERRCHKDLGDATA*)lParam;
			s_bMapChanged = FALSE;

			/* Create the columns in the list view control. */
			lvcol.pszText = szColHeader;
			lvcol.mask = LVCF_SUBITEM | LVCF_TEXT;

			lvcol.iSubItem = 0;
			LoadString(g_hInstance, IDS_ERRCHK_COL_ERROR, szColHeader, NUM_ELEMENTS(szColHeader));
			ListView_InsertColumn(hwndErrorList, 0, &lvcol);

			lvcol.iSubItem = 1;
			LoadString(g_hInstance, IDS_ERRCHK_COL_DESC, szColHeader, NUM_ELEMENTS(szColHeader));
			ListView_InsertColumn(hwndErrorList, 1, &lvcol);

			/* Full row select. */
			ListView_SetExtendedListViewStyleEx(hwndErrorList, LVS_EX_FULLROWSELECT, LVS_EX_FULLROWSELECT);

			/* Create the image list. The order of addition is imporant. */
			himl = ImageList_Create(cxIcon, cyIcon, ILC_COLOR32, 2, 0);
			ImageList_AddIcon(himl, LoadImage(NULL, IDI_WARNING, IMAGE_ICON, cxIcon, cyIcon, LR_SHARED));
			ImageList_AddIcon(himl, LoadImage(NULL, IDI_ERROR, IMAGE_ICON, cxIcon, cyIcon, LR_SHARED));
			ListView_SetImageList(hwndErrorList, himl, LVSIL_SMALL);


			/* Analyse the map. */
			FindAllMapErrors(s_lpecdd->lpmap, &s_mel, s_lpecdd->szSky);
			PopulateMapErrorList(GetDlgItem(hwndDlg, IDC_LIST_ERRORS), &s_mel);


			/* Make the columns big enough. */
			ListView_SetColumnWidth(hwndErrorList, 0, LVSCW_AUTOSIZE_USEHEADER);
			ListView_SetColumnWidth(hwndErrorList, 1, LVSCW_AUTOSIZE_USEHEADER);

			/* Disable the buttons to begin with. */
			EnableWindow(GetDlgItem(hwndDlg, IDC_BUTTON_VIEW), FALSE);
			EnableWindow(GetDlgItem(hwndDlg, IDC_BUTTON_FIX), FALSE);

			CentreWindowInParent(hwndDlg);
		}

		/* Let the system set the focus. */
		return TRUE;

	case WM_COMMAND:
		switch(LOWORD(wParam))
		{
		case IDC_BUTTON_VIEW:
			{
				int iSelected = ListView_GetSelectionMark(GetDlgItem(hwndDlg, IDC_LIST_ERRORS));

				if(iSelected >= 0)
					SelectErrorObjects(s_lpecdd->hwndMap, &s_mel, iSelected);
			}

			return TRUE;

		case IDC_BUTTON_FIX:
			{
				int iSelected = ListView_GetSelectionMark(GetDlgItem(hwndDlg, IDC_LIST_ERRORS));

				if(iSelected >= 0)
				{
					HCURSOR hcurLast = SetCursor(LoadCursor(NULL, IDC_WAIT));

					FixError(s_lpecdd->lpmap, &s_mel, iSelected, s_lpecdd->szLower, s_lpecdd->szMiddle, s_lpecdd->szUpper, s_lpecdd->szSky);

					s_bMapChanged = TRUE;

					/* Need to reanalyse, now that indices might have changed.
					 */
					CleanMapErrorList(&s_mel);
					FindAllMapErrors(s_lpecdd->lpmap, &s_mel, s_lpecdd->szSky);
					PopulateMapErrorList(GetDlgItem(hwndDlg, IDC_LIST_ERRORS), &s_mel);

					/* Disable the buttons. */
					EnableWindow(GetDlgItem(hwndDlg, IDC_BUTTON_VIEW), FALSE);
					EnableWindow(GetDlgItem(hwndDlg, IDC_BUTTON_FIX), FALSE);

					SetCursor(hcurLast);
				}
			}

			return TRUE;

		case IDCANCEL:
			CleanMapErrorList(&s_mel);
			ImageList_Destroy(ListView_GetImageList(GetDlgItem(hwndDlg, IDC_LIST_ERRORS), LVSIL_SMALL));
			EndDialog(hwndDlg, s_bMapChanged);
			return TRUE;
		}

		break;

	case WM_NOTIFY:
		if(((LPNMHDR)lParam)->idFrom == IDC_LIST_ERRORS)
		{
			LPNMLISTVIEW lpnmlistview = (LPNMLISTVIEW)lParam;

			switch(lpnmlistview->hdr.code)
			{
			case NM_DBLCLK:
				{
					/* User double-clicked an error item. Select the map
					 * objects.
					 */
					LPNMITEMACTIVATE lpnmia = (LPNMITEMACTIVATE)lParam;

					if(lpnmia->iItem >= 0)
						SelectErrorObjects(s_lpecdd->hwndMap, &s_mel, lpnmia->iItem);
				}

				return TRUE;

			case LVN_ITEMCHANGED:
				if(lpnmlistview->uChanged & LVIF_STATE)
				{
					if(lpnmlistview->uNewState & LVIS_SELECTED)
					{
						const ERRORCLASSINFO *lpeci = &g_errorclassinfo[s_mel.lpmed[lpnmlistview->iItem].byErrorType];

						/* Selection should always be visible. */
						ListView_EnsureVisible(lpnmlistview->hdr.hwndFrom, lpnmlistview->iItem, FALSE);

						/* Enable the buttons as appropriate. */
						EnableWindow(GetDlgItem(hwndDlg, IDC_BUTTON_VIEW), lpeci->emSelType != EM_ANY);
						EnableWindow(GetDlgItem(hwndDlg, IDC_BUTTON_FIX), TRUE);
					}
					else
					{
						EnableWindow(GetDlgItem(hwndDlg, IDC_BUTTON_VIEW), FALSE);
						EnableWindow(GetDlgItem(hwndDlg, IDC_BUTTON_FIX), FALSE);
					}

					return TRUE;
				}

				break;
			}
		}

		break;
	}

	return FALSE;
}


/* FindAllMapErrors
 *   Generates a description of all errors in a map.
 *
 * Parameters:
 *   MAP*				lpmap	Map.
 *   MAP_ERROR_LIST*	lpmel	List to populate.
 *   LPCTSTR			szSky	Sky texture name.
 *
 * Return value: None.
 *
 * Remarks:
 *   Call CleanMapErrorList on lpmel to free allocated memory, even if no errors
 *   are found.
 */
static void FindAllMapErrors(MAP *lpmap, MAP_ERROR_LIST *lpmel, LPCTSTR szSky)
{
	int i, iUnclosed;
	BOOL *lpbUsed;
	UNCLOSED_SECTOR_DESCRIPTOR *lpusd;

	lpmel->uiErrors = 0;
	lpmel->uiAllocated = INIT_ERROR_DESCRIPTORS;

	/* Allocate an initial error buffer. It'll be expanded as necessary. */
	lpmel->lpmed = ProcHeapAlloc(INIT_ERROR_DESCRIPTORS * sizeof(MAP_ERROR_DESCRIPTOR));

	/* Find negative-height sectors. */
	for(i = 0; i < lpmap->iSectors; i++)
	{
		if(SectorHasNegativeHeight(lpmap, i))
		{
			MAP_ERROR_DESCRIPTOR med = {MET_NEGHEIGHT_SECTOR, i, -1, -1};
			AddToMapErrorList(lpmel, &med);
		}
	}

	/* Find unclosed sectors. */
	iUnclosed = FindUnclosedSectors(lpmap, &lpusd);
	for(i = 0; i < iUnclosed; i++)
	{
		MAP_ERROR_DESCRIPTOR med = {MET_UNCLOSED_SECTOR, lpusd[i].iLinedefSrc, lpusd[i].iLinedefAdj, lpusd[i].iSideSrc};
		AddToMapErrorList(lpmel, &med);
	}

	FreeUnclosedSectorDescriptors(lpusd);

	/* Find unused sectors. */
	lpbUsed = UsedSectorMatrix(lpmap);
	for(i = 0; i < lpmap->iSectors; i++)
	{
		if(!lpbUsed[i])
		{
			MAP_ERROR_DESCRIPTOR med = {MET_UNUSED_SECTOR, i, -1, -1};
			AddToMapErrorList(lpmel, &med);
		}
	}

	FreeUsedMapObjectMatrix(lpbUsed);

	/* Find linedefs with sidedef mismatches. */
	for(i = 0; i < lpmap->iLinedefs; i++)
	{
		BYTE byFlags = LinedefSidedefMismatches(lpmap, i);

		if(byFlags & SEF_FRONT_MISSING)
		{
			MAP_ERROR_DESCRIPTOR med = {MET_MISSINGFRONT_LINE, i, -1, -1};
			AddToMapErrorList(lpmel, &med);
		}

		if(byFlags & SEF_BACK_MISSING)
		{
			MAP_ERROR_DESCRIPTOR med = {MET_MISSINGBACK_LINE, i, -1, -1};
			AddToMapErrorList(lpmel, &med);
		}

		if(byFlags & SEF_BACK_SUPERFLUOUS)
		{
			MAP_ERROR_DESCRIPTOR med = {MET_SUPERFLUOUSBACK_LINE, i, -1, -1};
			AddToMapErrorList(lpmel, &med);
		}
	}

	/* Find zero-length lines. */
	for(i = 0; i < lpmap->iLinedefs; i++)
	{
		if(LineIsZeroLength(lpmap, i))
		{
			MAP_ERROR_DESCRIPTOR med = {MET_ZEROLENGTH_LINE, i, -1, -1};
			AddToMapErrorList(lpmel, &med);
		}
	}

	/* Find missing textures. */
	for(i = 0; i < lpmap->iLinedefs; i++)
	{
		BYTE byMissingFlags = MissingTexFlags(lpmap, i, szSky);

		if(byMissingFlags)
		{
			MAP_ERROR_DESCRIPTOR med = {MET_MISSING_TEXTURE, i, byMissingFlags, -1};
			AddToMapErrorList(lpmel, &med);
		}
	}

	/* Find crossing lines. */
	for(i = 0; i < lpmap->iLinedefs; i++)
	{
		int iCrossedLinedef = i + 1;

		/* Find all lines crossing this one having a higher index than
		 * ourselves.
		 */
		while(iCrossedLinedef >= 0 && iCrossedLinedef < lpmap->iLinedefs)
		{
			iCrossedLinedef = FindCrossingLinedef(lpmap, i, iCrossedLinedef);
			if(iCrossedLinedef >= 0)
			{
				MAP_ERROR_DESCRIPTOR med = {MET_CROSSING_LINE, i, iCrossedLinedef, -1};
				AddToMapErrorList(lpmel, &med);

				/* Start looking from the next one next time. */
				iCrossedLinedef++;
			}
		}
	}

	/* Find unused sidedefs. */
	lpbUsed = UsedSidedefMatrix(lpmap);
	for(i = 0; i < lpmap->iSidedefs; i++)
	{
		if(!lpbUsed[i])
		{
			MAP_ERROR_DESCRIPTOR med = {MET_UNUSED_SIDEDEF, i, -1, -1};
			AddToMapErrorList(lpmel, &med);
		}
	}

	FreeUsedMapObjectMatrix(lpbUsed);

	/* Find coincident vertices. */
	for(i = 0; i < lpmap->iVertices; i++)
	{
		int iCoincidentVertex = FindNextCoincidentVertex(lpmap, i);

		/* If we found one, add it to the array. */
		if(iCoincidentVertex >= 0)
		{
			MAP_ERROR_DESCRIPTOR med = {MET_COINCIDENT_VERTICES, i, iCoincidentVertex, -1};
			AddToMapErrorList(lpmel, &med);
		}
	}

	/* Find unused vertices. */
	lpbUsed = UsedVertexMatrix(lpmap);
	for(i = 0; i < lpmap->iVertices; i++)
	{
		/* Add each unused vertex to the array. */
		if(!lpbUsed[i])
		{
			MAP_ERROR_DESCRIPTOR med = {MET_UNUSED_VERTEX, i, -1, -1};
			AddToMapErrorList(lpmel, &med);
		}
	}

	FreeUsedMapObjectMatrix(lpbUsed);

	/* Find stray things. */
	for(i = 0; i < lpmap->iThings; i++)
	{
		if(!ThingInLevel(lpmap, i))
		{
			MAP_ERROR_DESCRIPTOR med = {MET_STRAY_THING, i, -1, -1};
			AddToMapErrorList(lpmel, &med);
		}
	}
}


/* AddToMapErrorList
 *   Adds an entry to a map error list.
 *
 * Parameters:
 *   MAP_ERROR_LIST*		lpmel		List to which to add the new entry.
 *   MAP_ERROR_DESCRIPTOR*	lpmedNew	New entry.
 *
 * Return value: None.
 */
static __inline void AddToMapErrorList(MAP_ERROR_LIST *lpmel, MAP_ERROR_DESCRIPTOR *lpmedNew)
{
	/* Keep doubling the size of the buffer until it's big enough. No more than
	 * once, we'd hope...
	 */
	while(lpmel->uiErrors >= lpmel->uiAllocated)
		lpmel->lpmed = ProcHeapReAlloc(lpmel->lpmed, (lpmel->uiAllocated <<= 1) * sizeof(MAP_ERROR_DESCRIPTOR));

	/* Add the new entry. */
	lpmel->lpmed[lpmel->uiErrors++] = *lpmedNew;
}


/* PopulateMapErrorList
 *   Fills the error list-view control with entries from an error list.
 *
 * Parameters:
 *   HWND				hwndListView	Window handle of list-view control.
 *   MAP_ERROR_LIST*	lpmel			List of errors.
 *
 * Return value: None.
 */
static void PopulateMapErrorList(HWND hwndListView, MAP_ERROR_LIST *lpmel)
{
	LVITEM lvitem;
	unsigned int ui;
	TCHAR szText[1024];

	/* Clear the list first. */
	ListView_DeleteAllItems(hwndListView);

	/* We always use the same buffer. */
	lvitem.pszText = szText;

	for(ui = 0; ui < lpmel->uiErrors; ui++)
	{
		const ERRORCLASSINFO *lpeci = &g_errorclassinfo[lpmel->lpmed[ui].byErrorType];
		TCHAR szFormat[1024];

		lvitem.iItem = (int)ui;

		/* Add the first column's entry. */
		lvitem.mask = LVIF_TEXT | LVIF_IMAGE;
		lvitem.iImage = lpeci->severity == MES_WARNING ? MEI_WARNING : MEI_ERROR;
		lvitem.iSubItem = 0;
		LoadString(g_hInstance, lpeci->dwTitleID, szText, NUM_ELEMENTS(szText));

		ListView_InsertItem(hwndListView, &lvitem);

		/* Add the second column's entry. */
		lvitem.mask = LVIF_TEXT;
		lvitem.iSubItem = 1;
		LoadString(g_hInstance, lpeci->dwFormatID, szFormat, NUM_ELEMENTS(szFormat));

		/* Fill out the format string. */
		if(lpmel->lpmed[ui].byErrorType == MET_UNCLOSED_SECTOR)
		{
			TCHAR szSide[32];
			LoadString(g_hInstance, (lpmel->lpmed[ui].iParam3 == LS_FRONT) ? IDS_ERRCHK_FRONT : IDS_ERRCHK_BACK, szSide, NUM_ELEMENTS(szSide));

			_sntprintf(szText, NUM_ELEMENTS(szText) - 1, szFormat, szSide, lpmel->lpmed[ui].iParam1, lpmel->lpmed[ui].iParam2);
		}
		else
			_sntprintf(szText, NUM_ELEMENTS(szText) - 1, szFormat, lpmel->lpmed[ui].iParam1, lpmel->lpmed[ui].iParam2);

		szText[NUM_ELEMENTS(szText) - 1] = TEXT('\0');

		ListView_SetItem(hwndListView, &lvitem);
	}
}


/* SelectErrorObjects
 *   Selects objects corresponding to a certain found error, and zooms to the
 *   selection.
 *
 * Parameters:
 *   HWND				hwndMap			Map window handle.
 *   MAP_ERROR_LIST*	lpmel			List of errors.
 *   int				iErrorIndex		Index of error in list.
 *
 * Return value: None.
 *
 * Remarks:
 *   This is safe even if the particular error doesn't have anything to select.
 *   In this case, the selection will remain unchanged.
 */
static void SelectErrorObjects(HWND hwndMap, MAP_ERROR_LIST *lpmel, int iErrorIndex)
{
	/* Whatever happens, we'll either be selecting object(s) specified by the
	 * first one or two parameters.
	 */
	int iIndices[] = {lpmel->lpmed[iErrorIndex].iParam1, lpmel->lpmed[iErrorIndex].iParam2};
	const ERRORCLASSINFO *lpeci = &g_errorclassinfo[lpmel->lpmed[iErrorIndex].byErrorType];

	if(lpeci->iSelCount > 0)
	{
		ExtSetSelection(hwndMap, iIndices, lpeci->iSelCount, lpeci->emSelType);
		ZoomToSelection(hwndMap);
	}
}


/* FixError
 *   Attempts a fix for the specified error.
 *
 * Parameters:
 *   MAP*				lpmap			Map.
 *   MAP_ERROR_LIST*	lpmel			List of errors.
 *   int				iErrorIndex		Index of error in list.
 *   LPCTSTR			szLower			\
 *   LPCTSTR			szMiddle		|>	Textures to use where missing.
 *   LPCTSTR			szUpper			/
 *   LPCTSTR			szSky			Sky texture name.
 *
 * Return value: None.
 *
 * Remarks:
 *   This might result in map-object indices changing.
 */
static void FixError(MAP *lpmap, MAP_ERROR_LIST *lpmel, int iErrorIndex, LPCTSTR szLower, LPCTSTR szMiddle, LPCTSTR szUpper, LPCTSTR szSky)
{
	MAP_ERROR_DESCRIPTOR *lpmed = &lpmel->lpmed[iErrorIndex];

	switch(lpmed->byErrorType)
	{
	case MET_COINCIDENT_VERTICES:
		StitchVertices(lpmap, lpmed->iParam1, lpmed->iParam2);
		break;

	case MET_UNUSED_VERTEX:
		DeleteVertex(lpmap, lpmed->iParam1);
		break;

	case MET_ZEROLENGTH_LINE:
		DeleteLinedef(lpmap, lpmed->iParam1);
		break;

	case MET_STRAY_THING:
		DeleteThing(lpmap, lpmed->iParam1);
		break;

	case MET_NEGHEIGHT_SECTOR:
		lpmap->sectors[lpmed->iParam1].hceiling = lpmap->sectors[lpmed->iParam1].hfloor;
		break;

	case MET_SUPERFLUOUSBACK_LINE:
		DeleteSidedef(lpmap, lpmap->linedefs[lpmed->iParam1].s2);
		SetLinedefSidedef(lpmap, lpmed->iParam1, INVALID_SIDEDEF, LS_BACK);
		break;

	case MET_UNUSED_SIDEDEF:
		DeleteSidedef(lpmap, lpmed->iParam1);
		break;

	case MET_UNUSED_SECTOR:
		DeleteSector(lpmap, lpmed->iParam1);
		break;

	case MET_MISSING_TEXTURE:
		FixMissingTexturesLinedef(lpmap, lpmed->iParam1, szUpper, szMiddle, szLower, szSky);
		break;

	case MET_CROSSING_LINE:
		FixCrossingLinedefs(lpmap, lpmed->iParam1, lpmed->iParam2);
		break;

	case MET_UNCLOSED_SECTOR:
		FixBrokenSectorFromLinedef(lpmap, lpmed->iParam1, lpmed->iParam3);
		break;

	case MET_MISSINGFRONT_LINE:
	case MET_MISSINGBACK_LINE:
		{
			int iSide = (lpmed->byErrorType == MET_MISSINGFRONT_LINE) ? LS_FRONT : LS_BACK;
			int iSector = FindLineSideSector(lpmap, lpmed->iParam1, iSide);

			if(iSector >= 0)
				SetLinedefSidedef(lpmap, lpmed->iParam1, AddSidedef(lpmap, iSector), iSide);
		}

		break;

	default:
		break;
	}
}
