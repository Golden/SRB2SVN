/* SRB2 Workbench -- A map editor for Sonic Robo Blast 2.
 * Copyright (C) 2009 Gregor Dick
 * http://workbench.srb2.org/
 *
 * Incorporates portions of Doom Builder, (C) 2003 Pascal vd Heiden.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 * USA.
 *
 * editdlg.h: Header for editdlg.c.
 *
 * AMDG.
 */

#ifndef __SRB2B_EDITDLG__
#define __SRB2B_EDITDLG__

#include <windows.h>
#include <objbase.h>
#include <shldisp.h>
#include <shlwapi.h>

#include "../map.h"
#include "../selection.h"
#include "../config.h"
#include "../texture.h"
#include "../texenum.h"

#include "mapwin.h"


/* Macros. */
#define STRING_RELATIVE(sz) ((*(sz) == '+' && (sz)[1] == '+') || (*(sz) == '-' && (sz)[1] == '-'))


/* Types. */
typedef struct _MAPPPDATA
{
	HWND				hwndMap;
	MAP					*lpmap;
	SELECTION			*lpselection;
	CONFIG				*lpcfgMap, *lpcfgWadOptMap;
	TEXTURENAMELIST		*lptnlFlats, *lptnlTextures;
	HIMAGELIST			*lphimlCacheFlats, *lphimlCacheTextures;
	BOOL				bOKed;
	HMENU				hmenuWikiPopup;
	CONFIG				*lpcfgUsedFlats, *lpcfgUsedTextures;
	LPCTSTR				szSky;
	MAPTHING			*lpthingLast;
} MAPPPDATA;

enum ENUM_MAPPPFLAGS
{
	MPPF_SECTOR = 1,
	MPPF_LINE = 2,
	MPPF_THING = 4,
	MPPF_VERTEX = 8
};


typedef struct _INSERTSECTORDLGDATA
{
	short	nRadius;
	short	nEdges;
	BOOL	bSnap;
	char	cRadiusType;
} INSERTSECTORDLGDATA;


typedef struct _ROTATEDLGDATA
{
	float	fAngle;
	BOOL	bSnap;
	char	cCentreType;
	BOOL	bAlterThingAngles;
	BOOL	bThingsSelected;
	BOOL	bVerticesSelected;
} ROTATEDLGDATA;

enum ENUM_CENTRE_TYPE
{
	CT_BOUNDINGBOX, CT_THING, CT_VERTEX
};


typedef struct _RESIZEDLGDATA
{
	float	fFactor;
	BOOL	bSnap;
	char	cCentreType;
	BOOL	bThingsSelected;
	BOOL	bVerticesSelected;
	int		iMaxRatio;
} RESIZEDLGDATA;


typedef struct _ALIGNDLGDATA
{
	/* On the way in, these specify whether the options should be *enabled*; on
	 * the way out, they specify whether they were *selected*.
	 */
	BOOL	bFromThisLine;
	BOOL	bInSelection;

	/* This is used for returning only. */
	BYTE	byTexFlags;
} ALIGNDLGDATA;


typedef struct _MISSTEXDLGDATA
{
	/* On the way in, this specifies whether the option should be *enabled*; on
	 * the way out, it specifies whether it was *selected*.
	 */
	BOOL	bInSelection;

	/* Texture names. */
	TCHAR	szUpper[TEXNAME_BUFFER_LENGTH];
	TCHAR	szMiddle[TEXNAME_BUFFER_LENGTH];
	TCHAR	szLower[TEXNAME_BUFFER_LENGTH];

	/* These are needed to show the texture selection dialogue. */
	HWND			hwndMap;
	HIMAGELIST		*lphimlCache;
	TEXTURENAMELIST	*lptnlAllTextures;
	CONFIG			*lpcfgUsedTextures;
} MISSTEXDLGDATA;


typedef struct _TPINFO
{
	int				iTPID, iEditID;
	TEX_FORMAT		tf;
	HBITMAP			hbitmap;
	IAutoComplete2*	lpac2;
	LPTEXENUM		lptexenum;
} TPINFO;


typedef struct _TPPDATA
{
	WNDPROC	wndprocStatic;
} TPPDATA;



/* Prototypes. */
BOOL ShowMapObjectProperties(DWORD dwPageFlags, DWORD dwStartPage, LPCTSTR szCaption, MAPPPDATA *lpmapppdata);
LRESULT CALLBACK TexPreviewProc(HWND hwnd, UINT uiMsg, WPARAM wParam, LPARAM lParam);
BOOL InsertSectorDlg(HWND hwndParent, INSERTSECTORDLGDATA *lpisdd);
BOOL RotateDlg(HWND hwndParent, ROTATEDLGDATA *lprotdd);
BOOL ResizeDlg(HWND hwndParent, RESIZEDLGDATA *lprszdd);
BYTE SelectPresetTypes(HWND hwndParent, BYTE byObjectFlags);
BOOL AlignDlg(HWND hwndParent, ALIGNDLGDATA *lpaligndd);
BOOL MissingTexDlg(HWND hwndParent, MISSTEXDLGDATA *lpmtdd);
int SelectSectorEffectDlg(HWND hwndParent, CONFIG *lpcfgMap, int iInitialSelection, int iNybble);
int SelectThingDlg(HWND hwndParent, CONFIG *lpcfgMap, int iInitialSelection);
int SelectLDEffectDlg(HWND hwndParent, CONFIG *lpcfgMap, int iInitialSelection);
void SetTexturePreviewImage(HWND hwndMap, HWND hwndCtrl, LPCTSTR szTexName, TEX_FORMAT tf, HBITMAP hbmPreview, BOOL bRequired);


#endif

