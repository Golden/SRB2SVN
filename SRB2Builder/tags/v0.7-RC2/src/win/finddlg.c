/* SRB2 Workbench -- A map editor for Sonic Robo Blast 2.
 * Copyright (C) 2009 Gregor Dick
 * http://workbench.srb2.org/
 *
 * Incorporates portions of Doom Builder, (C) 2003 Pascal vd Heiden.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 * USA.
 *
 * finddlg.c: UI logic for Find/Replace dialogues.
 *
 * AMDG.
 */

#include <windows.h>
#include <commctrl.h>
#include <objbase.h>
#include <shldisp.h>
#include <stdio.h>
#include <string.h>
#include <tchar.h>

#include "../general.h"
#include "../find.h"
#include "../map.h"
#include "../options.h"
#include "../texenum.h"

#include "../../res/resource.h"

#include "mdiframe.h"
#include "mapwin.h"
#include "finddlg.h"
#include "texbrowser.h"
#include "editdlg.h"
#include "gendlg.h"


#define FINDSTRBUFSIZE 64
#define FCCOMBO_EQPOS 0
#define CCH_MATCHMSG 128


#define WM_UPDATEAUTOCOMPLETE WM_APP


/* Types. */
typedef struct _COMBOENTRY
{
	WORD wType, wStringID;
} COMBOENTRY;

typedef struct _FINDDLGDATA
{
	HWND			hwndMap;
	BOOL			bUseNybbledSectorEffects;
	TEXTURENAMELIST	*lptnlFlatsAll, *lptnlTexAll;
	CONFIG			*lpcfgFlatsUsed, *lpcfgTexUsed;
	HIMAGELIST		*lphimlFlatCache, *lphimlTexCache;
	CONFIG			*lpcfgMap;
	BOOL			bMapChanged;
} FINDDLGDATA;


static INT_PTR CALLBACK FindDlgProc(HWND hwndDlg, UINT uiMsg, WPARAM wParam, LPARAM lParam);
static INT_PTR CALLBACK ReplaceDlgProc(HWND hwndDlg, UINT uiMsg, WPARAM wParam, LPARAM lParam);
static INT_PTR FindReplaceCommonDlgProc(HWND hwndDlg, UINT uiMsg, WPARAM wParam, LPARAM lParam);
static void PopulateFindObjectTypeListCommon(HWND hwndCombo);
static void PopulateFindObjectTypeListFindOnly(HWND hwndCombo);
static void PopulateFindObjectTypeListSecEffectWord(HWND hwndCombo);
static void PopulateFindObjectTypeListSecEffectNybble(HWND hwndCombo);
static void PopulateFindConditionList(HWND hwndCombo);
static void AddTypesToComboBox(HWND hwndCombo, const COMBOENTRY *lpce, int iNumEntries);
static void UpdateFindControlStates(HWND hwndDlg, BOOL bReplace);


/* ShowFindDlg
 *   Shows the Find dialogue.
 *
 * Parameters:
 *   HWND				hwndMap								Map window handle.
 *   BOOL				bUseNybbledSectorEffects			Whether to use 1.1's
 *															new effect format.
 *   TEXTURENAMELIST	*lptnlFlatsAll, *lptnlTexAll		Names of all flats
 *															and textures.
 *   CONFIG				*lpcfgFlatsUsed, *lpcfgTexUsed		Names of used flats
 *															and textures.
 *   HIMAGELIST			*lphimlFlatCache, *lphimlTexCache	Cache image lists.
 *   CONFIG				*lpcfgMap							Map configuration.
 *
 * Return value: None.
 */
void ShowFindDlg(HWND hwndMap, BOOL bUseNybbledSectorEffects, TEXTURENAMELIST *lptnlFlatsAll, TEXTURENAMELIST *lptnlTexAll, CONFIG *lpcfgFlatsUsed, CONFIG *lpcfgTexUsed, HIMAGELIST *lphimlFlatCache, HIMAGELIST *lphimlTexCache, CONFIG *lpcfgMap)
{
	FINDDLGDATA fdd;

	/* Set up parameters. */
	fdd.bUseNybbledSectorEffects = bUseNybbledSectorEffects;
	fdd.hwndMap = hwndMap;
	fdd.lphimlFlatCache = lphimlFlatCache;
	fdd.lphimlTexCache = lphimlTexCache;
	fdd.lptnlFlatsAll = lptnlFlatsAll;
	fdd.lptnlTexAll = lptnlTexAll;
	fdd.lpcfgFlatsUsed = lpcfgFlatsUsed;
	fdd.lpcfgTexUsed = lpcfgTexUsed;
	fdd.lpcfgMap = lpcfgMap;

	DialogBoxParam(g_hInstance, MAKEINTRESOURCE(IDD_FIND), g_hwndMain, FindDlgProc, (LPARAM)&fdd);
}


/* ShowReplaceDlg
 *   Shows the Replace dialogue.
 *
 * Parameters:
 *   HWND				hwndMap								Map window handle.
 *   BOOL				bUseNybbledSectorEffects			Whether to use 1.1's
 *															new effect format.
 *   TEXTURENAMELIST	*lptnlFlatsAll, *lptnlTexAll		Names of all flats
 *															and textures.
 *   CONFIG				*lpcfgFlatsUsed, *lpcfgTexUsed		Names of used flats
 *															and textures.
 *   HIMAGELIST			*lphimlFlatCache, *lphimlTexCache	Cache image lists.
 *   CONFIG				*lpcfgMap							Map configuration.
 *
 * Return value: BOOL
 *   FALSE if dialogue cancelled by the user; TRUE otherwise.
 */
BOOL ShowReplaceDlg(HWND hwndMap, BOOL bUseNybbledSectorEffects, TEXTURENAMELIST *lptnlFlatsAll, TEXTURENAMELIST *lptnlTexAll, CONFIG *lpcfgFlatsUsed, CONFIG *lpcfgTexUsed, HIMAGELIST *lphimlFlatCache, HIMAGELIST *lphimlTexCache, CONFIG *lpcfgMap)
{
	FINDDLGDATA fdd;

	/* Set up parameters. */
	fdd.bUseNybbledSectorEffects = bUseNybbledSectorEffects;
	fdd.hwndMap = hwndMap;
	fdd.lphimlFlatCache = lphimlFlatCache;
	fdd.lphimlTexCache = lphimlTexCache;
	fdd.lptnlFlatsAll = lptnlFlatsAll;
	fdd.lptnlTexAll = lptnlTexAll;
	fdd.lpcfgFlatsUsed = lpcfgFlatsUsed;
	fdd.lpcfgTexUsed = lpcfgTexUsed;
	fdd.lpcfgMap = lpcfgMap;

	return DialogBoxParam(g_hInstance, MAKEINTRESOURCE(IDD_REPLACE), g_hwndMain, ReplaceDlgProc, (LPARAM)&fdd);
}



/* FindDlgProc
 *   Dialogue procedure for the Find dialogue.
 *
 * Parameters:
 *   HWND	hwndDlg		Handle to dialogue window.
 *   HWND	uiMsg		Window message ID.
 *   WPARAM	wParam		Message-specific.
 *   LPARAM lParam		Message-specific.
 *
 * Return value: INT_PTR
 *   TRUE if message was processed; FALSE otherwise.
 *
 * Remarks:
 *   Things common to the "Find" and "Find and Replace" dialogues are handled in
 *   FindReplaceCommonDlgProc.
 */
static INT_PTR CALLBACK FindDlgProc(HWND hwndDlg, UINT uiMsg, WPARAM wParam, LPARAM lParam)
{
	switch(uiMsg)
	{
	case WM_INITDIALOG:

		/* Select the default radio button for selection behaviour. */
		CheckRadioButton(hwndDlg, IDC_RADIO_SELECTALL, IDC_RADIO_SELECTADD, IDC_RADIO_SELECTALL);

		/* Add the search types that only we support. */
		PopulateFindObjectTypeListFindOnly(GetDlgItem(hwndDlg, IDC_COMBO_TYPE));

		/* Do the common stuff. */
		FindReplaceCommonDlgProc(hwndDlg, WM_INITDIALOG, wParam, lParam);

		/* Enable/disable controls, as appropriate. */
		UpdateFindControlStates(hwndDlg, FALSE);

		/* Let the system set the focus. */
		return TRUE;

	case WM_COMMAND:
		switch(LOWORD(wParam))
		{
		case IDC_BUTTON_FIND:
			{
				WORD wFindType = (WORD)SendDlgItemMessage(hwndDlg, IDC_COMBO_TYPE, CB_GETITEMDATA, SendDlgItemMessage(hwndDlg, IDC_COMBO_TYPE, CB_GETCURSEL, 0, 0), 0);
				WORD wCompare = (WORD)SendDlgItemMessage(hwndDlg, IDC_COMBO_CONDITION, CB_GETITEMDATA, SendDlgItemMessage(hwndDlg, IDC_COMBO_CONDITION, CB_GETCURSEL, 0, 0), 0);
				FINDDLGDATA *lpfdd = (FINDDLGDATA*)GetWindowLong(hwndDlg, GWL_USERDATA);
				BOOL bInSelection = (IsDlgButtonChecked(hwndDlg, IDC_RADIO_SELECTWITHIN) != BST_UNCHECKED);
				int iMatches = 0;
				TCHAR szMatchFmt[CCH_MATCHMSG];
				TCHAR szMatchMsg[CCH_MATCHMSG];

				/* Clear existing selection if necessary. */
				if(IsDlgButtonChecked(hwndDlg, IDC_RADIO_SELECTALL) != BST_UNCHECKED)
					DeselectAllByHandle(lpfdd->hwndMap);

				/* Go! */
				switch(wFindType)
				{
				case FT_VX: case FT_LINE: case FT_LINEEFFECT: case FT_LINEFLAGS:
				case FT_LINETAG: case FT_SEC: case FT_SECCEIL: case FT_SECFLR:
				case FT_SECTAG: case FT_SECLIGHT: case FT_SECHEIGHT:
				case FT_SIDEDEF: case FT_THING: case FT_THINGANGLE:
				case FT_THINGFLAGS: case FT_THINGZOFF: case FT_THINGZABS:
				case FT_SECEFFECT: case FT_SECEFFECTA: case FT_SECEFFECTB:
				case FT_SECEFFECTC: case FT_SECEFFECTD: case FT_THINGTYPE:
					/* Numeric. */
					{
						int iValue = GetDlgItemInt(hwndDlg, IDC_EDIT_VALUE, NULL, TRUE);
						iMatches = FindReplaceInt(lpfdd->hwndMap, wFindType, wCompare, iValue, 0, bInSelection, FFL_SELECT);
					}

					break;

				case FT_LINETEX: case FT_SECCEILFLAT: case FT_SECFLRFLAT:
					/* String. */
					{
						TCHAR szString[TEXNAME_BUFFER_LENGTH];
						char szStringA[TEXNAME_BUFFER_LENGTH];

						GetDlgItemText(hwndDlg, IDC_EDIT_VALUE, szString, NUM_ELEMENTS(szString));
#ifdef UNICODE
						WideCharToMultiByte(CP_ACP, 0, szString, -1, szStringA, NUM_ELEMENTS(szStringA), NULL, NULL);
#else
						strcpy(szStringA, szString);
#endif

						iMatches = FindReplaceStr(lpfdd->hwndMap, wFindType, wCompare, szStringA, NULL, bInSelection, FFL_SELECT);
					}
				}

				if(iMatches > 0)
					ZoomToSelection(lpfdd->hwndMap);

				/* Report the number of matches. */
				LoadString(g_hInstance, IDS_FINDMATCH_FORMAT, szMatchFmt, NUM_ELEMENTS(szMatchFmt));
				_sntprintf(szMatchMsg, NUM_ELEMENTS(szMatchMsg), szMatchFmt, iMatches);
				szMatchMsg[NUM_ELEMENTS(szMatchMsg) - 1] = TEXT('\0');
				MessageBox(hwndDlg, szMatchMsg, g_szAppName, MB_ICONINFORMATION);
			}

			return TRUE;

		case IDC_COMBO_TYPE:
			/* Enable/disable controls, as appropriate. */
			if(HIWORD(wParam) == CBN_SELCHANGE)
			{
				UpdateFindControlStates(hwndDlg, FALSE);
				return TRUE;
			}

			break;
		}

		/* Didn't process message. */
		break;
	}

	/* Didn't do anything ourselves, so see if the stuff we share with Replace
	 * has anything it wants to say.
	 */
	return FindReplaceCommonDlgProc(hwndDlg, uiMsg, wParam, lParam);
}


/* ReplaceDlgProc
 *   Dialogue procedure for the Find and Replace dialogue.
 *
 * Parameters:
 *   HWND	hwndDlg		Handle to dialogue window.
 *   HWND	uiMsg		Window message ID.
 *   WPARAM	wParam		Message-specific.
 *   LPARAM lParam		Message-specific.
 *
 * Return value: INT_PTR
 *   TRUE if message was processed; FALSE otherwise.
 *
 * Remarks:
 *   Things common to the "Find" and "Find and Replace" dialogues are handled in
 *   FindReplaceCommonDlgProc.
 */
static INT_PTR CALLBACK ReplaceDlgProc(HWND hwndDlg, UINT uiMsg, WPARAM wParam, LPARAM lParam)
{
	static IAutoComplete2 *s_lpac2Replace = NULL;
	static LPTEXENUM s_lptexenumReplace = NULL;

	switch(uiMsg)
	{
	case WM_INITDIALOG:
		{
			UDACCEL udaccel[] = {{0, 1}, {1, 8}};

			/* Set up the spinner for the replacement value. */
			SendDlgItemMessage(hwndDlg, IDC_SPIN_NEWVALUE, UDM_SETRANGE32, -32768, 65535);
			SendDlgItemMessage(hwndDlg, IDC_SPIN_NEWVALUE, UDM_SETACCEL, sizeof(udaccel) / sizeof(UDACCEL), (WPARAM)udaccel);

			if(ConfigGetInteger(g_lpcfgMain, OPT_AUTOCOMPLETETEX))
			{
				/* Create texture enumerators and shell autocomplete objects and
				 * associate them with the edit boxes.
				 */

				if((s_lptexenumReplace = CreateTexEnum()) && (s_lpac2Replace = CreateAutoComplete()))
				{
					/* Behaviour is slightly different from most autocomplete
					 * configurations here: we don't initialise the texture
					 * enumerator, and we disable autocompletion. This'll all
					 * be reset as necessary.
					 */
					s_lpac2Replace->lpVtbl->Init(s_lpac2Replace, GetDlgItem(hwndDlg, IDC_EDIT_NEWVALUE), (IUnknown*)s_lptexenumReplace, NULL, NULL);
					s_lpac2Replace->lpVtbl->Enable(s_lpac2Replace, FALSE);
				}
			}

			/* Do the common stuff. */
			FindReplaceCommonDlgProc(hwndDlg, WM_INITDIALOG, wParam, lParam);

			/* Enable/disable controls, as appropriate. */
			UpdateFindControlStates(hwndDlg, TRUE);
		}


		/* Let the system set the focus. */
		return TRUE;

	case WM_COMMAND:
		switch(LOWORD(wParam))
		{
		case IDC_BUTTON_REPLACE:
			{
				WORD wFindType = (WORD)SendDlgItemMessage(hwndDlg, IDC_COMBO_TYPE, CB_GETITEMDATA, SendDlgItemMessage(hwndDlg, IDC_COMBO_TYPE, CB_GETCURSEL, 0, 0), 0);
				WORD wCompare = (WORD)SendDlgItemMessage(hwndDlg, IDC_COMBO_CONDITION, CB_GETITEMDATA, SendDlgItemMessage(hwndDlg, IDC_COMBO_CONDITION, CB_GETCURSEL, 0, 0), 0);
				FINDDLGDATA *lpfdd = (FINDDLGDATA*)GetWindowLong(hwndDlg, GWL_USERDATA);
				BOOL bSelect = (IsDlgButtonChecked(hwndDlg, IDC_CHECK_SELECT) != BST_UNCHECKED);
				BOOL bInSelection = (IsDlgButtonChecked(hwndDlg, IDC_CHECK_WITHIN) != BST_UNCHECKED);
				int iMatches = 0;
				TCHAR szMatchFmt[CCH_MATCHMSG];
				TCHAR szMatchMsg[CCH_MATCHMSG];

				/* Clear existing selection if necessary. */
				if(bSelect && !bInSelection && IsDlgButtonChecked(hwndDlg, IDC_CHECK_ADDITIVE) != BST_CHECKED)
					DeselectAllByHandle(lpfdd->hwndMap);

				/* Go! */
				switch(wFindType)
				{
				case FT_LINEEFFECT: case FT_LINEFLAGS: case FT_LINETAG:
				case FT_SECCEIL: case FT_SECFLR: case FT_SECTAG:
				case FT_SECLIGHT: case FT_THINGANGLE: case FT_THINGFLAGS:
				case FT_THINGZOFF: case FT_THINGZABS: case FT_SECEFFECT:
				case FT_SECEFFECTA: case FT_SECEFFECTB: case FT_SECEFFECTC:
				case FT_SECEFFECTD: case FT_THINGTYPE:
					/* Numeric. */
					{
						int iValue = GetDlgItemInt(hwndDlg, IDC_EDIT_VALUE, NULL, TRUE);
						int iReplace = GetDlgItemInt(hwndDlg, IDC_EDIT_NEWVALUE, NULL, TRUE);

						iMatches = FindReplaceInt(lpfdd->hwndMap, wFindType, wCompare, iValue, iReplace, bInSelection, (bSelect ? FFL_SELECT : 0) | FFL_REPLACE);
					}

					break;

				case FT_LINETEX: case FT_SECCEILFLAT: case FT_SECFLRFLAT:
					/* String. */
					{
						TCHAR szString[TEXNAME_BUFFER_LENGTH], szReplace[TEXNAME_BUFFER_LENGTH];
						CHAR szStringA[TEXNAME_BUFFER_LENGTH], szReplaceA[TEXNAME_BUFFER_LENGTH];

						GetDlgItemText(hwndDlg, IDC_EDIT_VALUE, szString, sizeof(szString) / sizeof(TCHAR));
						GetDlgItemText(hwndDlg, IDC_EDIT_NEWVALUE, szReplace, sizeof(szReplace) / sizeof(TCHAR));

#ifdef UNICODE
						WideCharToMultiByte(CP_ACP, 0, szString, -1, szStringA, NUM_ELEMENTS(szStringA), NULL, NULL);
						WideCharToMultiByte(CP_ACP, 0, szReplace, -1, szReplaceA, NUM_ELEMENTS(szReplaceA), NULL, NULL);
#else
						strcpy(szStringA, szString);
						strcpy(szReplaceA, szReplace);
#endif

						iMatches = FindReplaceStr(lpfdd->hwndMap, wFindType, wCompare, szStringA, szReplaceA, bInSelection, (bSelect ? FFL_SELECT : 0) | FFL_REPLACE);
					}
				}

				if(bSelect && iMatches > 0)
					ZoomToSelection(lpfdd->hwndMap);

				/* Report the number of matches. */
				LoadString(g_hInstance, IDS_FINDMATCH_FORMAT, szMatchFmt, NUM_ELEMENTS(szMatchFmt));
				_sntprintf(szMatchMsg, NUM_ELEMENTS(szMatchMsg), szMatchFmt, iMatches);
				szMatchMsg[NUM_ELEMENTS(szMatchMsg) - 1] = TEXT('\0');
				MessageBox(hwndDlg, szMatchMsg, g_szAppName, MB_ICONINFORMATION);

				/* Did we make any changes? */
				if(iMatches > 0) lpfdd->bMapChanged = TRUE;

				return TRUE;
			}

		case IDC_COMBO_TYPE:
			/* Enable/disable controls, as appropriate. */
			if(HIWORD(wParam) == CBN_SELCHANGE)
			{
				UpdateFindControlStates(hwndDlg, TRUE);
				return TRUE;
			}

			break;

		case IDC_CHECK_SELECT:
			/* Enable/disable controls, as appropriate. */
			UpdateFindControlStates(hwndDlg, TRUE);
			return TRUE;
		}

		/* Didn't process message. */
		break;

	case WM_UPDATEAUTOCOMPLETE:
		{
			WORD wFindType = (WORD)SendDlgItemMessage(hwndDlg, IDC_COMBO_TYPE, CB_GETITEMDATA, SendDlgItemMessage(hwndDlg, IDC_COMBO_TYPE, CB_GETCURSEL, 0, 0), 0);
			FINDDLGDATA *lpfdd = (FINDDLGDATA*)GetWindowLong(hwndDlg, GWL_USERDATA);

			switch(wFindType)
			{
			case FT_LINETEX:
				SetTexEnumList(s_lptexenumReplace, lpfdd->lptnlTexAll);
				s_lpac2Replace->lpVtbl->Enable(s_lpac2Replace, TRUE);
				break;

			case FT_SECCEILFLAT:
			case FT_SECFLRFLAT:
				SetTexEnumList(s_lptexenumReplace, lpfdd->lptnlFlatsAll);
				s_lpac2Replace->lpVtbl->Enable(s_lpac2Replace, TRUE);
				break;

			default:
				s_lpac2Replace->lpVtbl->Enable(s_lpac2Replace, FALSE);
				break;
			}
		}

		/* Pass the baton to the common procedure. */
		break;

	case WM_DESTROY:
		/* Clean up. */
		if(s_lpac2Replace) s_lpac2Replace->lpVtbl->Release(s_lpac2Replace);
		if(s_lptexenumReplace) DestroyTexEnum(s_lptexenumReplace);

		/* Pass the baton to the common procedure. */
		break;
	}

	/* Didn't do anything ourselves, so see if the stuff we share with Replace
	 * has anything it wants to say.
	 */
	return FindReplaceCommonDlgProc(hwndDlg, uiMsg, wParam, lParam);
}


/* FindReplaceCommonDlgProc
 *   Dialogue procedure for things common to the "Find" and "Find and Replace"
 *   dialogues.
 *
 * Parameters:
 *   HWND	hwndDlg		Handle to dialogue window.
 *   HWND	uiMsg		Window message ID.
 *   WPARAM	wParam		Message-specific.
 *   LPARAM lParam		Message-specific.
 *
 * Return value: INT_PTR
 *   TRUE if message was processed; FALSE otherwise.
 *
 * Remarks:
 *   This is called by each of the F/FR dialogues' procs. It's never used
 *   directly as a dialog proc.
 */
static INT_PTR FindReplaceCommonDlgProc(HWND hwndDlg, UINT uiMsg, WPARAM wParam, LPARAM lParam)
{
	static IAutoComplete2 *s_lpac2Find = NULL;
	static LPTEXENUM s_lptexenumFind = NULL;

	switch(uiMsg)
	{
	case WM_INITDIALOG:
		{
			FINDDLGDATA *lpfdd = (FINDDLGDATA*)lParam;
			UDACCEL udaccel[] = {{0, 1}, {1, 8}};
			HWND hwndComboType = GetDlgItem(hwndDlg, IDC_COMBO_TYPE);

			/* Centre in parent. */
			CentreWindowInParent(hwndDlg);

			/* Make a copy of the data and store it for later. */
			lpfdd = ProcHeapAlloc(sizeof(FINDDLGDATA));
			*lpfdd = *(FINDDLGDATA*)lParam;
			SetWindowLong(hwndDlg, GWL_USERDATA, (LONG)lpfdd);

			/* Assume we don't change the map. */
			lpfdd->bMapChanged = FALSE;

			/* Populate the lists of object types and contional operators. */
			PopulateFindObjectTypeListCommon(hwndComboType);
			if(lpfdd->bUseNybbledSectorEffects)
				PopulateFindObjectTypeListSecEffectNybble(hwndComboType);
			else
				PopulateFindObjectTypeListSecEffectWord(hwndComboType);

			PopulateFindConditionList(GetDlgItem(hwndDlg, IDC_COMBO_CONDITION));

			/* Select the first item in each. */
			SendDlgItemMessage(hwndDlg, IDC_COMBO_TYPE, CB_SETCURSEL, 0, 0);
			SendDlgItemMessage(hwndDlg, IDC_COMBO_CONDITION, CB_SETCURSEL, FCCOMBO_EQPOS, 0);

			/* Set up the spinner for the search value. */
			SendDlgItemMessage(hwndDlg, IDC_SPIN_VALUE, UDM_SETRANGE32, -32768, 65535);
			SendDlgItemMessage(hwndDlg, IDC_SPIN_VALUE, UDM_SETACCEL, sizeof(udaccel) / sizeof(UDACCEL), (WPARAM)udaccel);

			if(ConfigGetInteger(g_lpcfgMain, OPT_AUTOCOMPLETETEX))
			{
				/* Create texture enumerators and shell autocomplete objects and
				 * associate them with the edit boxes.
				 */

				if((s_lptexenumFind = CreateTexEnum()) && (s_lpac2Find = CreateAutoComplete()))
				{
					/* Behaviour is slightly different from most autocomplete
					 * configurations here: we don't initialise the texture
					 * enumerator, and we disable autocompletion. This'll all
					 * be reset as necessary.
					 */
					s_lpac2Find->lpVtbl->Init(s_lpac2Find, GetDlgItem(hwndDlg, IDC_EDIT_VALUE), (IUnknown*)s_lptexenumFind, NULL, NULL);
					s_lpac2Find->lpVtbl->Enable(s_lpac2Find, FALSE);
				}
			}
		}

		/* Let the system set the focus. */
		return TRUE;

	case WM_COMMAND:
		switch(LOWORD(wParam))
		{
		case IDC_BUTTON_SELECT:
		case IDC_BUTTON_NEWSELECT:
			/* Select value to search for. */
			{
				WORD wFindType = (WORD)SendDlgItemMessage(hwndDlg, IDC_COMBO_TYPE, CB_GETITEMDATA, SendDlgItemMessage(hwndDlg, IDC_COMBO_TYPE, CB_GETCURSEL, 0, 0), 0);
				FINDDLGDATA *lpfdd = (FINDDLGDATA*)GetWindowLong(hwndDlg, GWL_USERDATA);
				TCHAR szEditBoxText[TEXNAME_BUFFER_LENGTH];
				int iValue;
				BOOL bUpdateText = FALSE;
				BOOL bTextual = FALSE;
				BOOL bTranslated;

				/* Are we working with the old value or the new? */
				int iDlgItem = LOWORD(wParam) == IDC_BUTTON_SELECT ? IDC_EDIT_VALUE : IDC_EDIT_NEWVALUE;

				/* Get text and its numeric value. Only one of these will
				 * actually be used.
				 */
				GetDlgItemText(hwndDlg, iDlgItem, szEditBoxText, sizeof(szEditBoxText)/sizeof(TCHAR));
				iValue = GetDlgItemInt(hwndDlg, iDlgItem, &bTranslated, TRUE);
				if(!bTranslated) iValue = -1;

				switch(wFindType)
				{
				case FT_LINETEX:
					bTextual = TRUE;
					bUpdateText = SelectTexture(hwndDlg, lpfdd->hwndMap, TF_TEXTURE, lpfdd->lphimlTexCache, lpfdd->lptnlTexAll, lpfdd->lpcfgTexUsed, 0, szEditBoxText);
					break;

				case FT_SECCEILFLAT: case FT_SECFLRFLAT:
					bTextual = TRUE;
					bUpdateText = SelectTexture(hwndDlg, lpfdd->hwndMap, TF_FLAT, lpfdd->lphimlFlatCache, lpfdd->lptnlFlatsAll, lpfdd->lpcfgFlatsUsed, 0, szEditBoxText);
					break;

				case FT_SECEFFECT:
				case FT_SECEFFECTA:
					iValue = SelectSectorEffectDlg(hwndDlg, lpfdd->lpcfgMap, iValue, 0);
					bUpdateText = (iValue >= 0);
					break;

				case FT_SECEFFECTB:
					iValue = SelectSectorEffectDlg(hwndDlg, lpfdd->lpcfgMap, iValue, 1);
					bUpdateText = (iValue >= 0);
					break;

				case FT_SECEFFECTC:
					iValue = SelectSectorEffectDlg(hwndDlg, lpfdd->lpcfgMap, iValue, 2);
					bUpdateText = (iValue >= 0);
					break;

				case FT_SECEFFECTD:
					iValue = SelectSectorEffectDlg(hwndDlg, lpfdd->lpcfgMap, iValue, 3);
					bUpdateText = (iValue >= 0);
					break;

				case FT_LINEEFFECT:
					iValue = SelectLDEffectDlg(hwndDlg, lpfdd->lpcfgMap, iValue);
					bUpdateText = (iValue >= 0);
					break;

				case FT_THINGTYPE:
					iValue = SelectThingDlg(hwndDlg, lpfdd->lpcfgMap, iValue);
					bUpdateText = (iValue >= 0);
					break;
				}

				if(bUpdateText)
				{
					if(bTextual)
						SetDlgItemText(hwndDlg, iDlgItem, szEditBoxText);
					else
						SetDlgItemInt(hwndDlg, iDlgItem, iValue, TRUE);
				}
			}

			return TRUE;

		case IDCANCEL:
			/* This is Close, rather than Cancel. */
			EndDialog(hwndDlg, ((FINDDLGDATA*)GetWindowLong(hwndDlg, GWL_USERDATA))->bMapChanged);
			return TRUE;
		}

		break;

	case WM_UPDATEAUTOCOMPLETE:
		{
			WORD wFindType = (WORD)SendDlgItemMessage(hwndDlg, IDC_COMBO_TYPE, CB_GETITEMDATA, SendDlgItemMessage(hwndDlg, IDC_COMBO_TYPE, CB_GETCURSEL, 0, 0), 0);
			FINDDLGDATA *lpfdd = (FINDDLGDATA*)GetWindowLong(hwndDlg, GWL_USERDATA);

			switch(wFindType)
			{
			case FT_LINETEX:
				SetTexEnumList(s_lptexenumFind, lpfdd->lptnlTexAll);
				s_lpac2Find->lpVtbl->Enable(s_lpac2Find, TRUE);
				break;

			case FT_SECCEILFLAT:
			case FT_SECFLRFLAT:
				SetTexEnumList(s_lptexenumFind, lpfdd->lptnlFlatsAll);
				s_lpac2Find->lpVtbl->Enable(s_lpac2Find, TRUE);
				break;

			default:
				s_lpac2Find->lpVtbl->Enable(s_lpac2Find, FALSE);
				break;
			}
		}

		return TRUE;

	case WM_DESTROY:
		/* Clean up. */
		ProcHeapFree((FINDDLGDATA*)GetWindowLong(hwndDlg, GWL_USERDATA));

		if(s_lpac2Find) s_lpac2Find->lpVtbl->Release(s_lpac2Find);
		if(s_lptexenumFind) DestroyTexEnum(s_lptexenumFind);

		return TRUE;
	}

	/* Didn't process message. */
	return FALSE;
}


/* PopulateFindObjectTypeList*
 *   Various functions for filling the list of search types in the F/FR
 *   dialogues.
 *
 * Parameters:
 *   HWND	hwndCombo					Combo box for types.
 *
 * Return value: None.
 */
static void PopulateFindObjectTypeListCommon(HWND hwndCombo)
{
	const COMBOENTRY ceCommon[] =
	{
		{FT_LINEEFFECT, IDS_FIND_LINEEFFECT},
		{FT_LINEFLAGS, IDS_FIND_LINEFLAGS},
		{FT_LINETAG, IDS_FIND_LINETAG},
		{FT_LINETEX, IDS_FIND_LINETEX},
		{FT_SECCEIL, IDS_FIND_SECCEIL},
		{FT_SECFLR, IDS_FIND_SECFLR},
		{FT_SECTAG, IDS_FIND_SECTAG},
		{FT_SECCEILFLAT, IDS_FIND_SECCEILFLAT},
		{FT_SECFLRFLAT, IDS_FIND_SECFLRFLAT},
		{FT_SECLIGHT, IDS_FIND_SECLIGHT},
		{FT_THINGANGLE, IDS_FIND_THINGANGLE},
		{FT_THINGFLAGS, IDS_FIND_THINGFLAGS},
		{FT_THINGTYPE, IDS_FIND_THINGTYPE},
		{FT_THINGZOFF, IDS_FIND_THINGZOFF},
		{FT_THINGZABS, IDS_FIND_THINGZABS}
	};

	/* Add the common types to the list. */
	AddTypesToComboBox(hwndCombo, ceCommon, sizeof(ceCommon) / sizeof(COMBOENTRY));
}

static void PopulateFindObjectTypeListFindOnly(HWND hwndCombo)
{
	const COMBOENTRY ceFindOnly[] =
	{
		{FT_VX, IDS_FIND_VX},
		{FT_LINE, IDS_FIND_LINE},
		{FT_SEC, IDS_FIND_SEC},
		{FT_SECHEIGHT, IDS_FIND_SECHEIGHT},
		{FT_SIDEDEF, IDS_FIND_SIDEDEF},
		{FT_THING, IDS_FIND_THING}
	};

	/* Add the types to the list that only apply to Find (and not FR). */
	AddTypesToComboBox(hwndCombo, ceFindOnly, sizeof(ceFindOnly) / sizeof(COMBOENTRY));
}

static void PopulateFindObjectTypeListSecEffectWord(HWND hwndCombo)
{
	const COMBOENTRY ceSecEffectWord = {FT_SECEFFECT, IDS_FIND_SECEFFECT};

	/* Add the 1.09-style sector effect type. */
	AddTypesToComboBox(hwndCombo, &ceSecEffectWord, 1);
}

static void PopulateFindObjectTypeListSecEffectNybble(HWND hwndCombo)
{
	const COMBOENTRY ceSecEffectNybble[] =
	{
		{FT_SECEFFECTA, IDS_FIND_SECEFFECTA},
		{FT_SECEFFECTB, IDS_FIND_SECEFFECTB},
		{FT_SECEFFECTC, IDS_FIND_SECEFFECTC},
		{FT_SECEFFECTD, IDS_FIND_SECEFFECTD}
	};

	/* Add the 1.1-style sector effect type. */
	AddTypesToComboBox(hwndCombo, ceSecEffectNybble, sizeof(ceSecEffectNybble) / sizeof(COMBOENTRY));
}


/* PopulateFindConditionList
 *   Fills the list of conditions in the F/FR dialogues.
 *
 * Parameters:
 *   HWND	hwndCombo					Combo box for types.
 *   BOOL	bUseNybbledSectorEffects	Whether to use 1.1's new effect format.
 *
 * Return value: None.
 */
static void PopulateFindConditionList(HWND hwndCombo)
{
	const COMBOENTRY ceConditions[] =
	{
		{FC_EQUAL, IDS_FINDCON_EQUAL},
		{FC_LESS, IDS_FINDCON_LESS},
		{FC_GREATER, IDS_FINDCON_GREATER},
		{FC_LESSEQ, IDS_FINDCON_LESSEQ},
		{FC_GREATEREQ, IDS_FINDCON_GREATEREQ},
		{FC_NOTEQUAL, IDS_FINDCON_NOTEQUAL},
		{FC_AND, IDS_FINDCON_AND}
	};

	AddTypesToComboBox(hwndCombo, ceConditions, sizeof(ceConditions) / sizeof(COMBOENTRY));
}


/* AddTypesToComboBox
 *   Adds provided search types to combo box.
 *
 * Parameters:
 *   HWND				hwndCombo	Combo box for types.
 *   const COMBOENTRY*	lpce		Types to add.
 *   int				iNumTypes	Number of entries in lpce.
 *
 * Return value: None.
 */
static void AddTypesToComboBox(HWND hwndCombo, const COMBOENTRY *lpce, int iNumEntries)
{
	int i;

	for(i = 0; i < iNumEntries; i++)
	{
		TCHAR szFindString[FINDSTRBUFSIZE];
		int iNewIndex;

		/* Add a new string. */
		LoadString(g_hInstance, lpce[i].wStringID, szFindString, sizeof(szFindString) / sizeof(TCHAR));
		iNewIndex = SendMessage(hwndCombo, CB_ADDSTRING, 0, (LPARAM)szFindString);

		/* Set its type. */
		SendMessage(hwndCombo, CB_SETITEMDATA, iNewIndex, lpce[i].wType);
	}
}


/* UpdateFindControlStates
 *   Enables/disables controls on the F/FR dialogue depending on what search
 *   type is selected.
 *
 * Parameters:
 *   HWND	hwndDlg		Window handle for dialogue.
 *   BOOL	bReplace	Whether we're dealing with a Replace dialogue.
 *
 * Return value: None.
 */
static void UpdateFindControlStates(HWND hwndDlg, BOOL bReplace)
{
	int iIndex = SendDlgItemMessage(hwndDlg, IDC_COMBO_TYPE, CB_GETCURSEL, 0, 0);
	WORD wFindType = (WORD)SendDlgItemMessage(hwndDlg, IDC_COMBO_TYPE, CB_GETITEMDATA, iIndex, 0);

	/* Depending on the search types, some options might not make sense. */
	switch(wFindType)
	{
	case FT_VX: case FT_LINE: case FT_LINETAG: case FT_LINEFLAGS: case FT_SEC:
	case FT_SECCEIL: case FT_SECFLR: case FT_SECTAG: case FT_SECLIGHT:
	case FT_SECHEIGHT: case FT_SIDEDEF: case FT_THING: case FT_THINGANGLE:
	case FT_THINGFLAGS: case FT_THINGZOFF: case FT_THINGZABS:

		/* Can't select. */
		EnableWindow(GetDlgItem(hwndDlg, IDC_SPIN_VALUE), TRUE);
		EnableWindow(GetDlgItem(hwndDlg, IDC_BUTTON_SELECT), FALSE);

		if(bReplace)
		{
			EnableWindow(GetDlgItem(hwndDlg, IDC_SPIN_NEWVALUE), TRUE);
			EnableWindow(GetDlgItem(hwndDlg, IDC_BUTTON_NEWSELECT), FALSE);
		}

		break;

	case FT_LINEEFFECT: case FT_SECEFFECT: case FT_SECEFFECTA:
	case FT_SECEFFECTB: case FT_SECEFFECTC: case FT_SECEFFECTD:
	case FT_THINGTYPE:

		/* Numeric, can select. */
		EnableWindow(GetDlgItem(hwndDlg, IDC_SPIN_VALUE), TRUE);
		EnableWindow(GetDlgItem(hwndDlg, IDC_BUTTON_SELECT), TRUE);

		if(bReplace)
		{
			EnableWindow(GetDlgItem(hwndDlg, IDC_SPIN_NEWVALUE), TRUE);
			EnableWindow(GetDlgItem(hwndDlg, IDC_BUTTON_NEWSELECT), TRUE);
		}

		break;

	case FT_LINETEX: case FT_SECCEILFLAT: case FT_SECFLRFLAT:

		/* Not numeric, can select. */
		EnableWindow(GetDlgItem(hwndDlg, IDC_SPIN_VALUE), FALSE);
		EnableWindow(GetDlgItem(hwndDlg, IDC_BUTTON_SELECT), TRUE);

		if(bReplace)
		{
			EnableWindow(GetDlgItem(hwndDlg, IDC_SPIN_NEWVALUE), FALSE);
			EnableWindow(GetDlgItem(hwndDlg, IDC_BUTTON_NEWSELECT), TRUE);
		}

		break;
	}

	/* If we're a Replace dialogue, then we also need to examine the checkboxes.
	 */
	if(bReplace)
		EnableWindow(GetDlgItem(hwndDlg, IDC_CHECK_ADDITIVE), IsDlgButtonChecked(hwndDlg, IDC_CHECK_SELECT) != BST_UNCHECKED);

	/* Update the autocomplete states. We need the help of the window
	 * procedure(s) here.
	 */
	SendMessage(hwndDlg, WM_UPDATEAUTOCOMPLETE, 0, 0);
}
