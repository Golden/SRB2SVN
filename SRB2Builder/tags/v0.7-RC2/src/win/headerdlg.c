/* SRB2 Workbench -- A map editor for Sonic Robo Blast 2.
 * Copyright (C) 2009 Gregor Dick
 * http://workbench.srb2.org/
 *
 * Incorporates portions of Doom Builder, (C) 2003 Pascal vd Heiden.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 * USA.
 *
 * headerdlg.c: Map Header Editor dialogue box logic.
 *
 * AMDG.
 */

#include <windows.h>
#include <commctrl.h>
#include <stdio.h>
#include <tchar.h>

#include "../general.h"
#include "../soc.h"
#include "../mapconfig.h"
#include "../texture.h"
#include "../config.h"

#include "../../res/resource.h"

#include "headerdlg.h"
#include "gendlg.h"
#include "mdiframe.h"
#include "editdlg.h"
#include "texbrowser.h"

#ifndef ListView_SetCheckState //Alam: WineLib does not have the ListView_*CheckState defines, why?
#define ListView_SetCheckState(hwnd,iIndex,bCheck) \
    ListView_SetItemState(hwnd,iIndex,INDEXTOSTATEIMAGEMASK((bCheck)+1),LVIS_STATEIMAGEMASK)
#endif
#ifndef ListView_GetCheckState //Alam: let remove the 2 defines after commctrl.h patch hits the Wine GIT tree
#define ListView_GetCheckState(hwnd,iIndex) \
    ((((UINT)(ListView_GetItemState(hwnd,iIndex,LVIS_STATEIMAGEMASK)))>>12)-1)
#endif


typedef struct _EDITMAPHEADERDLGDATA
{
	HWND				hwndMap;
	TEXTURENAMELIST*	lptnlAll;
	HIMAGELIST*			lphimlCache;
	MAPHEADER*			lpmapheader;
	DWORD				dwFlags;
	CONFIG*				lpcfgLevelTypes;
	CONFIG*				lpcfgWeatherTypes;
} EDITMAPHEADERDLGDATA;

typedef struct _ALTTLDATA
{
	HWND		hwndListView;
	DWORD		dwTOLFlags;
} ALTTLDATA;


static INT_PTR CALLBACK EditMapHeaderDlgProc(HWND hwndDlg, UINT uiMsg, WPARAM wParam, LPARAM lParam);
static BOOL AddLevelTypeToList(CONFIG *lpcfgLevelType, void *lpvData);
static BOOL AddWeatherTypeToComboBox(CONFIG *lpcfgWeatherType, void *lpvData);
static void EnableDisableHeaderControlsFromState(HWND hwndDlg);
static DWORD GetTOLFlagsFromList(HWND hwndListView);
static CONFIG* CreateSkyCollection(TEXTURENAMELIST *lptnl);



/* EditMapHeader
 *   Shows the "Edit Map Header" dialogue.
 *
 * Parameters:
 *   HWND				hwndMap				Map window handle.
 *   TEXTURENAMELIST*	lptnlAllTextures	List of all textures.
 *   HIMAGELIST*		lphimlCache			Texture cache image list.
 *   MAPHEADER*			lpmapheader			Map header used to initialise
 *											dialogue and to return results.
 *   CONFIG*			lpcfgMap			Map config.
 *
 * Return value: BOOL
 *   TRUE if OKed; FALSE if cancelled.
 *
 * Remarks:
 *   If the user cancels, *lpmapheader is left untouched. If the user OKs,
 *   however, in the memory allocated withing *lpmapheader for its strings could
 *   be freed and perhaps new memory allocated. The flags in *lpmapheader will
 *   be set correctly to indicate what needs to be freed by the caller, so
 *   FreeMapHeaderStrings can (and should) be called.
 */
BOOL EditMapHeader(HWND hwndMap, TEXTURENAMELIST *lptnlAllTextures, HIMAGELIST *lphimlCache, MAPHEADER *lpmapheader, CONFIG *lpcfgMap)
{
	EDITMAPHEADERDLGDATA emhdd = {hwndMap, lptnlAllTextures, lphimlCache, lpmapheader, ConfigGetInteger(lpcfgMap, MAPCFG_HEADERFLAGS), ConfigGetSubsection(lpcfgMap, MAPCFG_TOL), ConfigGetSubsection(lpcfgMap, MAPCFG_WEATHER)};
	return DialogBoxParam(g_hInstance, MAKEINTRESOURCE(IDD_HEADER), g_hwndMain, EditMapHeaderDlgProc, (LPARAM)&emhdd);
}


/* EditMapHeaderDlgProc
 *   Dialogue proc for map-header editor.
 *
 * Parameters:
 *   HWND		hwndDlg		Dialogue window handle.
 *   UINT		uiMsg		Message identifier.
 *   WPARAM 	wParam		Message-specific.
 *   LPARAM 	lParam		Message-specific.
 *
 * Return value: INT_PTR
 *   TRUE if the message was processed; FALSE otherwise.
 */
static INT_PTR CALLBACK EditMapHeaderDlgProc(HWND hwndDlg, UINT uiMsg, WPARAM wParam, LPARAM lParam)
{
	static EDITMAPHEADERDLGDATA *s_lpemhdd = NULL;
	static HBITMAP s_hbmSky = NULL;
	static CONFIG *s_lpcfgSkies = NULL;

	switch(uiMsg)
	{
	case WM_INITDIALOG:
		{
			UDACCEL udaccel[2];
			int i;
			HWND hwndTOLList = GetDlgItem(hwndDlg, IDC_LIST_TOL);
			LVCOLUMN lvcol;
			int iNumWeatherTypes;
			TPPDATA *lptppd;
			HWND hwndTex;
			HDC hdcDlg;

			int iSpinnerIDs[] =
			{
				IDC_SPIN_ACT, IDC_SPIN_MUSICSLOT, IDC_SPIN_NEXTLEVEL,
				IDC_SPIN_FORCECHARACTER, IDC_SPIN_COUNTDOWN, IDC_SPIN_SKYNUM,
				IDC_SPIN_CUTSCENENUM, IDC_SPIN_PRECUTSCENENUM
			};

			/* For brevity. */
			MAPHEADER *lpmh;

			CentreWindowInParent(hwndDlg);

			/* Get the pointer to the data from the caller. */
			s_lpemhdd = (EDITMAPHEADERDLGDATA*)lParam;
			lpmh = s_lpemhdd->lpmapheader;

			/* Enable/disable controls for features that this game does(n't)
			 * support.
			 */
			EnableWindow(GetDlgItem(hwndDlg, IDC_CHECK_MAPCREDITS), (s_lpemhdd->dwFlags & MHF_MAPCREDITS) ? BST_CHECKED : BST_UNCHECKED);
			EnableWindow(GetDlgItem(hwndDlg, IDC_CHECK_TIMEOFDAY), (s_lpemhdd->dwFlags & MHF_TIMEOFDAY) ? BST_CHECKED : BST_UNCHECKED);
			EnableWindow(GetDlgItem(hwndDlg, IDC_CHECK_RUNSOC), (s_lpemhdd->dwFlags & MHF_RUNSOC) ? BST_CHECKED : BST_UNCHECKED);
			EnableWindow(GetDlgItem(hwndDlg, IDC_CHECK_SUBTITLE), (s_lpemhdd->dwFlags & MHF_SUBTITLE) ? BST_CHECKED : BST_UNCHECKED);
			EnableWindow(GetDlgItem(hwndDlg, IDC_CHECK_SPEEDMUSIC), (s_lpemhdd->dwFlags & MHF_SPEEDMUSIC) ? BST_CHECKED : BST_UNCHECKED);

			/* Set up spinner controls. */
			udaccel[0].nInc = 1;
			udaccel[0].nSec = 0;
			udaccel[1].nInc = 8;
			udaccel[1].nSec = 2;

			for(i = 0; i < (int)NUM_ELEMENTS(iSpinnerIDs); i++)
			{
				SendDlgItemMessage(hwndDlg, iSpinnerIDs[i], UDM_SETRANGE32, 0, 0x7FFFFFFF);
				SendDlgItemMessage(hwndDlg, iSpinnerIDs[i], UDM_SETACCEL, NUM_ELEMENTS(udaccel), (LPARAM)udaccel);
			}

			/* Create a collection of sky names. */
			s_lpcfgSkies = CreateSkyCollection(s_lpemhdd->lptnlAll);

			/* Allocate memory for the preview window. It's freed when the
			 * window's destroyed.
			 */
			lptppd = ProcHeapAlloc(sizeof(TPPDATA));
			hwndTex = GetDlgItem(hwndDlg, IDC_TEX_SKY);

			lptppd->wndprocStatic = (WNDPROC)GetWindowLong(hwndTex, GWL_WNDPROC);

			SetWindowLong(hwndTex, GWL_USERDATA, (LONG)lptppd);
			SetWindowLong(hwndTex, GWL_WNDPROC, (LONG)TexPreviewProc);

			/* Create preview bitmap. */
			hdcDlg = GetDC(hwndDlg);
			s_hbmSky = CreateCompatibleBitmap(hdcDlg, CX_TEXPREVIEW, CY_TEXPREVIEW);
			ReleaseDC(hwndDlg, hdcDlg);
			SendMessage(hwndTex, STM_SETIMAGE, IMAGE_BITMAP, (LPARAM)s_hbmSky);

			/* Set up the TOL list. */
			lvcol.mask = LVCF_SUBITEM;
			lvcol.iSubItem = 0;
			ListView_InsertColumn(hwndTOLList, 0, &lvcol);
			ListView_SetExtendedListViewStyleEx(hwndTOLList, LVS_EX_CHECKBOXES, LVS_EX_CHECKBOXES);
			ListView_SetColumnWidth(hwndTOLList, 0, LVSCW_AUTOSIZE_USEHEADER);

			/* Populate weather combo and TOL list, also initialising the
			 * latter.
			 */
			if(s_lpemhdd->lpcfgLevelTypes)
			{
				ALTTLDATA altldata;
				altldata.hwndListView = GetDlgItem(hwndDlg, IDC_LIST_TOL);
				altldata.dwTOLFlags = lpmh->dwTypeOfLevel;
				ConfigIterate(s_lpemhdd->lpcfgLevelTypes, AddLevelTypeToList, (void*)&altldata);
			}

			if(s_lpemhdd->lpcfgWeatherTypes)
				ConfigIterate(s_lpemhdd->lpcfgWeatherTypes, AddWeatherTypeToComboBox, (void*)GetDlgItem(hwndDlg, IDC_COMBO_WEATHER));

			/* Initialise the weather combo box. */
			iNumWeatherTypes = SendDlgItemMessage(hwndDlg, IDC_COMBO_WEATHER, CB_GETCOUNT, 0, 0);
			for(i = 0; i < iNumWeatherTypes; i++)
			{
				int iListWeather = SendDlgItemMessage(hwndDlg, IDC_COMBO_WEATHER, CB_GETITEMDATA, i, 0);

				/* Set weather zero by default. */
				if(iListWeather == 0)
				{
					SendDlgItemMessage(hwndDlg, IDC_COMBO_WEATHER, CB_SETCURSEL, i, 0);
					if(!(lpmh->dwMask & MHF_WEATHER)) break;
				}

				/* If we match the specified weather, select and stop. */
				if((lpmh->dwMask & MHF_WEATHER) && iListWeather == lpmh->iWeather)
				{
					SendDlgItemMessage(hwndDlg, IDC_COMBO_WEATHER, CB_SETCURSEL, i, 0);
					break;
				}
			}

			/* Initialise the controls. */

			CheckDlgButton(hwndDlg, IDC_CHECK_SHOWNETGAME, ((lpmh->dwMask & MHF_HIDDEN) && lpmh->bHidden) ? BST_UNCHECKED : BST_CHECKED);
			CheckDlgButton(hwndDlg, IDC_CHECK_LEVELSELECT, ((lpmh->dwMask & MHF_LEVELSELECT) && lpmh->bLevelSelect) ? BST_CHECKED : BST_UNCHECKED);
			CheckDlgButton(hwndDlg, IDC_CHECK_RELOAD, ((lpmh->dwMask & MHF_NORELOAD) && lpmh->bNoReload) ? BST_UNCHECKED : BST_CHECKED);
			CheckDlgButton(hwndDlg, IDC_CHECK_SSMUSIC, ((lpmh->dwMask & MHF_NOSSMUSIC) && lpmh->bNoSSMusic) ? BST_UNCHECKED : BST_CHECKED);
			CheckDlgButton(hwndDlg, IDC_CHECK_ZONE, ((lpmh->dwMask & MHF_NOZONE) && lpmh->bNoZone) ? BST_UNCHECKED : BST_CHECKED);

			if((lpmh->dwMask & MHF_SCRIPTISLUMP) && (lpmh->dwMask & MHF_SCRIPTNAME))
				CheckRadioButton(hwndDlg, IDC_RADIO_SCRIPTISLUMP, IDC_RADIO_SCRIPTISFILENAME, lpmh->bScriptIsLump ? IDC_RADIO_SCRIPTISLUMP : IDC_RADIO_SCRIPTISFILENAME);
			else
				CheckRadioButton(hwndDlg, IDC_RADIO_SCRIPTISLUMP, IDC_RADIO_SCRIPTISFILENAME, IDC_RADIO_SCRIPTISLUMP);

			CheckDlgButton(hwndDlg, IDC_CHECK_SPEEDMUSIC, ((lpmh->dwMask & s_lpemhdd->dwFlags & MHF_SPEEDMUSIC) && lpmh->bSpeedMusic) ? BST_CHECKED : BST_UNCHECKED);
			CheckDlgButton(hwndDlg, IDC_CHECK_TIMEATTACK, ((lpmh->dwMask & MHF_TIMEATTACK) && lpmh->bTimeAttack) ? BST_CHECKED : BST_UNCHECKED);
			CheckDlgButton(hwndDlg, IDC_CHECK_TIMEOFDAY, ((lpmh->dwMask & s_lpemhdd->dwFlags & MHF_TIMEOFDAY) && lpmh->bTimeOfDay) ? BST_CHECKED : BST_UNCHECKED);

			if(lpmh->dwMask & MHF_ACT)
			{
				CheckDlgButton(hwndDlg, IDC_CHECK_ACT, lpmh->iAct == 0 ? BST_UNCHECKED : BST_CHECKED);
				SetDlgItemInt(hwndDlg, IDC_EDIT_ACT, lpmh->iAct, FALSE);
			}

			if((lpmh->dwMask & MHF_COUNTDOWN) && lpmh->iCountdown != 0)
			{
				CheckDlgButton(hwndDlg, IDC_CHECK_COUNTDOWN, BST_CHECKED);
				SetDlgItemInt(hwndDlg, IDC_EDIT_COUNTDOWN, lpmh->iCountdown, FALSE);
			}

			if((lpmh->dwMask & MHF_CUTSCENENUM) && lpmh->iCutsceneNum != 0)
			{
				CheckDlgButton(hwndDlg, IDC_CHECK_CUTSCENENUM, BST_CHECKED);
				SetDlgItemInt(hwndDlg, IDC_EDIT_CUTSCENENUM, lpmh->iCutsceneNum, FALSE);
			}

			if((lpmh->dwMask & MHF_FORCECHARACTER) && lpmh->iForceCharacter != FORCECHAR_NONE)
			{
				CheckDlgButton(hwndDlg, IDC_CHECK_FORCECHARACTER, BST_CHECKED);
				SetDlgItemInt(hwndDlg, IDC_EDIT_FORCECHARACTER, lpmh->iForceCharacter, FALSE);
			}

			if(lpmh->dwMask & MHF_MUSICSLOT)
				SetDlgItemInt(hwndDlg, IDC_EDIT_MUSICSLOT, lpmh->iMusicSlot, FALSE);

			if(lpmh->dwMask & MHF_NEXTLEVEL)
				SetDlgItemInt(hwndDlg, IDC_EDIT_NEXTLEVEL, lpmh->iNextLevel, FALSE);

			if((lpmh->dwMask & MHF_PRECUTSCENENUM) && lpmh->iPreCutsceneNum != 0)
			{
				CheckDlgButton(hwndDlg, IDC_CHECK_PRECUTSCENENUM, BST_CHECKED);
				SetDlgItemInt(hwndDlg, IDC_EDIT_PRECUTSCENENUM, lpmh->iPreCutsceneNum, FALSE);
			}

			if(lpmh->dwMask & MHF_SKYNUM)
				SetDlgItemInt(hwndDlg, IDC_EDIT_SKYNUM, lpmh->iSkyNum, FALSE);

			if((lpmh->dwMask & MHF_LEVELNAME) && lpmh->szLevelName)
				SetDlgItemANSIText(hwndDlg, IDC_EDIT_LEVELNAME, lpmh->szLevelName);

			if(lpmh->dwMask & MHF_INTERSCREEN)
			{
				CheckDlgButton(hwndDlg, IDC_CHECK_INTERSCREEN, BST_CHECKED);
				SetDlgItemANSIText(hwndDlg, IDC_EDIT_INTERSCREEN, lpmh->szInterScreen);
			}

			if((lpmh->dwMask & s_lpemhdd->dwFlags & MHF_MAPCREDITS) && lpmh->szMapCredits)
			{
				CheckDlgButton(hwndDlg, IDC_CHECK_MAPCREDITS, BST_CHECKED);
				SetDlgItemANSIText(hwndDlg, IDC_EDIT_MAPCREDITS, lpmh->szMapCredits);
			}

			if((lpmh->dwMask & s_lpemhdd->dwFlags & MHF_RUNSOC) && lpmh->szRunSOC)
			{
				CheckDlgButton(hwndDlg, IDC_CHECK_RUNSOC, BST_CHECKED);
				SetDlgItemANSIText(hwndDlg, IDC_EDIT_RUNSOC, lpmh->szRunSOC);
			}

			if((lpmh->dwMask & MHF_SCRIPTNAME) && lpmh->szScriptName)
			{
				CheckDlgButton(hwndDlg, IDC_CHECK_SCRIPTNAME, BST_CHECKED);
				SetDlgItemANSIText(hwndDlg, IDC_EDIT_SCRIPTNAME, lpmh->szScriptName);
			}

			if((lpmh->dwMask & s_lpemhdd->dwFlags & MHF_SUBTITLE) && lpmh->szSubtitle)
			{
				CheckDlgButton(hwndDlg, IDC_CHECK_SUBTITLE, BST_CHECKED);
				SetDlgItemANSIText(hwndDlg, IDC_EDIT_SUBTITLE, lpmh->szSubtitle);
			}

			/* Enable/disable controls depending on which options were specified
			 * in the header.
			 */
			EnableDisableHeaderControlsFromState(hwndDlg);
		}

		/* Let the system set the focus. */
		return TRUE;

	case WM_COMMAND:
		switch(LOWORD(wParam))
		{
		case IDOK:
			{
				MAPHEADER* const lpmh = s_lpemhdd->lpmapheader;
				DWORD cch;
				int iIndex;

				FreeMapHeaderStrings(lpmh);

				/* Populate the header from the controls. */
				lpmh->bHidden = !IsDlgButtonChecked(hwndDlg, IDC_CHECK_SHOWNETGAME);
				lpmh->bLevelSelect = IsDlgButtonChecked(hwndDlg, IDC_CHECK_LEVELSELECT);
				lpmh->bNoReload = !IsDlgButtonChecked(hwndDlg, IDC_CHECK_RELOAD);
				lpmh->bNoSSMusic = !IsDlgButtonChecked(hwndDlg, IDC_CHECK_SSMUSIC);
				lpmh->bNoZone = !IsDlgButtonChecked(hwndDlg, IDC_CHECK_ZONE);
				lpmh->bTimeAttack = IsDlgButtonChecked(hwndDlg, IDC_CHECK_TIMEATTACK);

				/* We always set the act, even if it's unchecked. */
				lpmh->iAct = IsDlgButtonChecked(hwndDlg, IDC_CHECK_ACT) ? GetDlgItemInt(hwndDlg, IDC_EDIT_ACT, NULL, FALSE) : 0;
				lpmh->iMusicSlot = GetDlgItemInt(hwndDlg, IDC_EDIT_MUSICSLOT, NULL, FALSE);
				lpmh->iNextLevel = GetDlgItemInt(hwndDlg, IDC_EDIT_NEXTLEVEL, NULL, FALSE);
				lpmh->iSkyNum = GetDlgItemInt(hwndDlg, IDC_EDIT_SKYNUM, NULL, FALSE);

				/* We always set weather, too. */
				iIndex = SendDlgItemMessage(hwndDlg, IDC_COMBO_WEATHER, CB_GETCURSEL, 0, 0);
				lpmh->iWeather = SendDlgItemMessage(hwndDlg, IDC_COMBO_WEATHER, CB_GETITEMDATA, iIndex, 0);

				lpmh->dwTypeOfLevel = GetTOLFlagsFromList(GetDlgItem(hwndDlg, IDC_LIST_TOL));

				/* We set these above. */
				lpmh->dwMask = MHF_HIDDEN | MHF_LEVELSELECT | MHF_NORELOAD |
					MHF_NOSSMUSIC | MHF_NOZONE | MHF_TIMEATTACK | MHF_ACT |
					MHF_MUSICSLOT | MHF_NEXTLEVEL | MHF_SKYNUM | MHF_WEATHER |
					MHF_TYPEOFLEVEL;

				if(s_lpemhdd->dwFlags & MHF_SPEEDMUSIC)
				{
					lpmh->bSpeedMusic = IsDlgButtonChecked(hwndDlg, IDC_CHECK_SPEEDMUSIC);
					lpmh->dwMask |= MHF_SPEEDMUSIC;
				}

				if(s_lpemhdd->dwFlags & MHF_TIMEOFDAY)
				{
					lpmh->bTimeOfDay = IsDlgButtonChecked(hwndDlg, IDC_CHECK_TIMEOFDAY);
					lpmh->dwMask |= MHF_TIMEOFDAY;
				}

				if(IsDlgButtonChecked(hwndDlg, IDC_CHECK_COUNTDOWN))
				{
					lpmh->iCountdown = GetDlgItemInt(hwndDlg, IDC_EDIT_COUNTDOWN, NULL, FALSE);
					lpmh->dwMask |= MHF_COUNTDOWN;
				}

				if(IsDlgButtonChecked(hwndDlg, IDC_CHECK_CUTSCENENUM))
				{
					lpmh->iCutsceneNum = GetDlgItemInt(hwndDlg, IDC_EDIT_CUTSCENENUM, NULL, FALSE);
					lpmh->dwMask |= MHF_CUTSCENENUM;
				}

				if(IsDlgButtonChecked(hwndDlg, IDC_CHECK_FORCECHARACTER))
				{
					lpmh->iForceCharacter = GetDlgItemInt(hwndDlg, IDC_EDIT_FORCECHARACTER, NULL, FALSE);
					lpmh->dwMask |= MHF_FORCECHARACTER;
				}

				if(IsDlgButtonChecked(hwndDlg, IDC_CHECK_PRECUTSCENENUM))
				{
					lpmh->iPreCutsceneNum = GetDlgItemInt(hwndDlg, IDC_EDIT_PRECUTSCENENUM, NULL, FALSE);
					lpmh->dwMask |= MHF_PRECUTSCENENUM;
				}

				if(IsDlgButtonChecked(hwndDlg, IDC_CHECK_INTERSCREEN) && GetWindowTextLength(GetDlgItem(hwndDlg, IDC_EDIT_INTERSCREEN)) > 0)
				{
					GetDlgItemANSIText(hwndDlg, IDC_EDIT_INTERSCREEN, lpmh->szInterScreen, NUM_ELEMENTS(lpmh->szInterScreen));
					lpmh->dwMask |= MHF_INTERSCREEN;
				}

				if((cch = GetWindowTextLength(GetDlgItem(hwndDlg, IDC_EDIT_LEVELNAME))) > 0)
				{
					lpmh->szLevelName = ProcHeapAlloc((cch + 1) * sizeof(char));
					GetDlgItemANSIText(hwndDlg, IDC_EDIT_LEVELNAME, lpmh->szLevelName, cch + 1);
					lpmh->dwMask |= MHF_LEVELNAME;
				}

				if((s_lpemhdd->dwFlags & MHF_MAPCREDITS) &&
					IsDlgButtonChecked(hwndDlg, IDC_CHECK_MAPCREDITS) &&
					(cch = GetWindowTextLength(GetDlgItem(hwndDlg, IDC_EDIT_MAPCREDITS))) > 0)
				{
					lpmh->szMapCredits = ProcHeapAlloc((cch + 1) * sizeof(char));
					GetDlgItemANSIText(hwndDlg, IDC_EDIT_MAPCREDITS, lpmh->szMapCredits, cch + 1);
					lpmh->dwMask |= MHF_MAPCREDITS;
				}

				if((s_lpemhdd->dwFlags & MHF_RUNSOC) &&
					IsDlgButtonChecked(hwndDlg, IDC_CHECK_RUNSOC) &&
					(cch = GetWindowTextLength(GetDlgItem(hwndDlg, IDC_EDIT_RUNSOC))) > 0)
				{
					lpmh->szRunSOC = ProcHeapAlloc((cch + 1) * sizeof(char));
					GetDlgItemANSIText(hwndDlg, IDC_EDIT_RUNSOC, lpmh->szRunSOC, cch + 1);
					lpmh->dwMask |= MHF_RUNSOC;
				}

				if(IsDlgButtonChecked(hwndDlg, IDC_CHECK_SCRIPTNAME) &&
					(cch = GetWindowTextLength(GetDlgItem(hwndDlg, IDC_EDIT_SCRIPTNAME))) > 0)
				{
					lpmh->bScriptIsLump = IsDlgButtonChecked(hwndDlg, IDC_RADIO_SCRIPTISLUMP);
					lpmh->szScriptName = ProcHeapAlloc((cch + 1) * sizeof(char));
					GetDlgItemANSIText(hwndDlg, IDC_EDIT_SCRIPTNAME, lpmh->szScriptName, cch + 1);
					lpmh->dwMask |= MHF_SCRIPTISLUMP | MHF_SCRIPTNAME;
				}

				if((s_lpemhdd->dwFlags & MHF_SUBTITLE) &&
					IsDlgButtonChecked(hwndDlg, IDC_CHECK_SUBTITLE) &&
					(cch = GetWindowTextLength(GetDlgItem(hwndDlg, IDC_EDIT_SUBTITLE))) > 0)
				{
					lpmh->szSubtitle = ProcHeapAlloc((cch + 1) * sizeof(char));
					GetDlgItemANSIText(hwndDlg, IDC_EDIT_SUBTITLE, lpmh->szSubtitle, cch + 1);
					lpmh->dwMask |= MHF_SUBTITLE;
				}

				EndDialog(hwndDlg, TRUE);
			}

			return TRUE;

		case IDCANCEL:
			EndDialog(hwndDlg, FALSE);
			return TRUE;

		case IDC_TEX_SKY:
			if(HIWORD(wParam) == BN_CLICKED)
			{
				TCHAR szSky[TEXNAME_BUFFER_LENGTH];

				/* Get the current flat so it's selected initially. */
				_sntprintf(szSky, NUM_ELEMENTS(szSky), TEXT("SKY%d"), GetDlgItemInt(hwndDlg, IDC_EDIT_SKYNUM, NULL, FALSE));
				szSky[NUM_ELEMENTS(szSky) - 1] = TEXT('\0');

				/* Show the texture browser. */
				if(SelectTexture(hwndDlg, s_lpemhdd->hwndMap, TF_TEXTURE, s_lpemhdd->lphimlCache, s_lpemhdd->lptnlAll, s_lpcfgSkies, SLT_FORCE_USED | SLT_NO_NULL, szSky))
				{
					/* User didn't cancel. Update the edit-box. */
					SetDlgItemText(hwndDlg, IDC_EDIT_SKYNUM, szSky + 3);
				}

				return TRUE;
			}

			break;

		case IDC_EDIT_SKYNUM:
			if(s_hbmSky && s_lpcfgSkies && HIWORD(wParam) == EN_CHANGE)
			{
				TCHAR szSky[TEXNAME_BUFFER_LENGTH];

				/* Generate sky texture name. */
				_sntprintf(szSky, NUM_ELEMENTS(szSky), TEXT("SKY%d"), GetDlgItemInt(hwndDlg, IDC_EDIT_SKYNUM, NULL, FALSE));
				szSky[NUM_ELEMENTS(szSky) - 1] = TEXT('\0');

				/* Update the preview control. */
				SetTexturePreviewImage
					(s_lpemhdd->hwndMap,
					 GetDlgItem(hwndDlg, IDC_TEX_SKY),
					 szSky,
					 TF_TEXTURE,
					 s_hbmSky,
					 FALSE);

				return TRUE;
			}

			break;

		case IDC_CHECK_SUBTITLE:
		case IDC_CHECK_ACT:
		case IDC_CHECK_FORCECHARACTER:
		case IDC_CHECK_COUNTDOWN:
		case IDC_CHECK_INTERSCREEN:
		case IDC_CHECK_SCRIPTNAME:
		case IDC_CHECK_RUNSOC:
		case IDC_CHECK_MAPCREDITS:
		case IDC_CHECK_CUTSCENENUM:
		case IDC_CHECK_PRECUTSCENENUM:
			EnableDisableHeaderControlsFromState(hwndDlg);
			return TRUE;
		}

		/* Didn't process message. */
		break;

	case WM_DESTROY:
		if(s_hbmSky) DeleteObject(s_hbmSky);
		if(s_lpcfgSkies) ConfigDestroy(s_lpcfgSkies);
		return TRUE;
	}

	/* Didn't process message. */
	return FALSE;
}


/* AddLevelTypeToList
 *   Adds a level type to a list-view control and initialises its checked state.
 *
 * Parameters:
 *   CONFIG		*lpcfgLevelType		Config node containing TOL info.
 *   void*		lpvData				(ALTTLDATA*) Data including window handle of
 *									list and TOL values for initialisation.
 *
 * Return value: BOOL
 *   Always TRUE.
 *
 * Remarks:
 *   Used as a callback function by ConfigIterate.
 */
static BOOL AddLevelTypeToList(CONFIG *lpcfgLevelType, void *lpvData)
{
	ALTTLDATA *lpaltldata = (ALTTLDATA*)lpvData;
	LVITEM lvitem;
	int iIndex;

	lvitem.mask = LVIF_TEXT | LVIF_PARAM;
	lvitem.iItem = 0;
	lvitem.iSubItem = 0;
	lvitem.pszText = lpcfgLevelType->sz;
	lvitem.lParam = _tcstol(lpcfgLevelType->szName, NULL, 10);

	iIndex = ListView_InsertItem(lpaltldata->hwndListView, &lvitem);
	ListView_SetCheckState(lpaltldata->hwndListView, iIndex, !!(lpaltldata->dwTOLFlags & lvitem.lParam));

	return TRUE;
}


/* AddWeatherTypeToComboBox
 *   Adds a weather type to a combo box control.
 *
 * Parameters:
 *   CONFIG		*lpcfgWeatherType	Config node containing weather info.
 *   void*		lpvData				(HWND) Combo box handle.
 *
 * Return value: BOOL
 *   Always TRUE.
 *
 * Remarks:
 *   Used as a callback function by ConfigIterate.
 */
static BOOL AddWeatherTypeToComboBox(CONFIG *lpcfgWeatherType, void *lpvData)
{
	int iIndex = SendMessage((HWND)lpvData, CB_ADDSTRING, 0, (LPARAM)lpcfgWeatherType->sz);
	SendMessage((HWND)lpvData, CB_SETITEMDATA, iIndex, _tcstol(lpcfgWeatherType->szName, NULL, 10));

	return TRUE;
}


/* EnableDisableHeaderControlsFromState
 *   Enables or disables certain controls on the Header dialogue depending on
 *   the state of the corresponding checkboxes.
 *
 * Parameters:
 *   HWND	hwndDlg		Header dialogue handle.
 *
 * Return value: None.
 */
static void EnableDisableHeaderControlsFromState(HWND hwndDlg)
{
	BOOL bScript = IsDlgButtonChecked(hwndDlg, IDC_CHECK_SCRIPTNAME);

	EnableWindow(GetDlgItem(hwndDlg, IDC_EDIT_SUBTITLE), IsDlgButtonChecked(hwndDlg, IDC_CHECK_SUBTITLE));
	EnableWindow(GetDlgItem(hwndDlg, IDC_EDIT_ACT), IsDlgButtonChecked(hwndDlg, IDC_CHECK_ACT));
	EnableWindow(GetDlgItem(hwndDlg, IDC_EDIT_FORCECHARACTER), IsDlgButtonChecked(hwndDlg, IDC_CHECK_FORCECHARACTER));
	EnableWindow(GetDlgItem(hwndDlg, IDC_EDIT_COUNTDOWN), IsDlgButtonChecked(hwndDlg, IDC_CHECK_COUNTDOWN));
	EnableWindow(GetDlgItem(hwndDlg, IDC_EDIT_INTERSCREEN), IsDlgButtonChecked(hwndDlg, IDC_CHECK_INTERSCREEN));
	EnableWindow(GetDlgItem(hwndDlg, IDC_EDIT_SCRIPTNAME), bScript);
	EnableWindow(GetDlgItem(hwndDlg, IDC_RADIO_SCRIPTISLUMP), bScript);
	EnableWindow(GetDlgItem(hwndDlg, IDC_RADIO_SCRIPTISFILENAME), bScript);
	EnableWindow(GetDlgItem(hwndDlg, IDC_EDIT_RUNSOC), IsDlgButtonChecked(hwndDlg, IDC_CHECK_RUNSOC));
	EnableWindow(GetDlgItem(hwndDlg, IDC_EDIT_MAPCREDITS), IsDlgButtonChecked(hwndDlg, IDC_CHECK_MAPCREDITS));
	EnableWindow(GetDlgItem(hwndDlg, IDC_EDIT_CUTSCENENUM), IsDlgButtonChecked(hwndDlg, IDC_CHECK_CUTSCENENUM));
	EnableWindow(GetDlgItem(hwndDlg, IDC_EDIT_PRECUTSCENENUM), IsDlgButtonChecked(hwndDlg, IDC_CHECK_PRECUTSCENENUM));
}


/* GetTOLFlagsFromList
 *   Gets the TOL flags corresponding to the checked items in the TOL list.
 *
 * Parameters:
 *   HWND	hwndListView	TOL list.
 *
 * Return value: DWORD
 *   Type-of-level flags.
 */
static DWORD GetTOLFlagsFromList(HWND hwndListView)
{
	int i, iCount;
	DWORD dwFlags = 0;

	iCount = ListView_GetItemCount(hwndListView);
	for(i = 0; i < iCount; i++)
	{
		LVITEM lvitem;

		lvitem.iItem = i;
		lvitem.iSubItem = 0;
		lvitem.mask = LVIF_PARAM;
		ListView_GetItem(hwndListView, &lvitem);

		/* Is it checked? If so, set the bit. */
		if(ListView_GetCheckState(hwndListView, i)) dwFlags |= lvitem.lParam;
	}

	return dwFlags;
}


/* CreateSkyCollection
 *   Creates a collection of skies from a list of textures.
 *
 * Parameters:
 *   TEXTURENAMELIST*	lptnl	List of texture names.
 *
 * Return value: CONFIG*
 *   Collection of skies.
 *
 * Remarks:
 *   Call ConfigDestroy on the returned pointer as with any config.
 */
static CONFIG* CreateSkyCollection(TEXTURENAMELIST *lptnl)
{
	CONFIG *lpcfgSkies = ConfigCreate();
	int i;

	/* Loop through all textures, looking for skies. */
	for(i = 0; i < lptnl->iEntries; i++)
	{
		/* If this texture is a sky, add it to the list. */
		if(_tcsncmp(TEXT("SKY"), lptnl->lpszTexNames[i], 3) == 0)
			ConfigSetAtom(lpcfgSkies, lptnl->lpszTexNames[i]);
	}

	return lpcfgSkies;
}
